<h2><a id="how-we-disclose-information">How we disclose information</a></h2>

<h3>Information Disclosed for Business or Commercial Purposes in the Last 12 Months, and Categories of Parties Disclosed To</h3>

<#list disclosureCategories as disclosureCategory>
   <p>We may disclose the following personal information about you when you ${disclosureCategory.collectionGroupLabel}:</p>

   <table>
     <thead>
       <tr>
         <th>Personal Information Category</th>
         <th>Categories of Service Providers</th>
         <th>Categories of Third Parties</th>
       </tr>
     </thead>
     <tbody>
        <#list disclosureCategory.picGroups as disclosureGroup>
          <tr>
            <td>${disclosureGroup.picGroup}</td>
            <td>${disclosureGroup.serviceCategories}</td>
            <td>${disclosureGroup.thirdPartyCategories}</td>
          </tr>
        </#list>
     </tbody>
   </table>
</#list>
