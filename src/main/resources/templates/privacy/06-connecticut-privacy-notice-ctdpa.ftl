<h2><a id="ctdpa-privacy-notice">Connecticut Privacy Notice (CTDPA)</a></h2>
${privacyNoticeIntroText}

<h3><a id="ctdpa-collection-and-disclosure-of-personal-information">Collection and Disclosure of Personal Information</a></h3>
<p>
The personal data we collect is described above in <strong><a href="#information-we-collect">Information we collect</a></strong>. The personal data we disclose to third parties and the categories of third parties
to whom we disclose personal data is described above in <strong><a href="#how-we-disclose-information">How we disclose information</a></strong>. The length of time for which we retain personal information is described above in <strong><a href="#how-long-we-keep-your-data">How long we keep your data.</a></strong>.
</p>

<h3><a id="ctdpa-data-selling-and-targeted-advertising">Data “Selling” and Targeted Advertising</a></h3>
<#if includeSelling>
  <p>We "sell" personal data to third parties, as that term is defined in the CTDPA.</p>
</#if>

<#if sharingGroups?has_content>
  <p>
    We process personal data for purposes of targeted advertising (as defined in the CTDPA), including online identifiers and internet
    activity. This allows us to show you ads that are more relevant to you.
  </p>

</#if>
<#if includeSelling || sharingGroups?has_content>
  <p>
    <a style="color: #007BBD" href="${optOutUrl}">You may opt-out of these data practices here</a>.
  </p>
</#if>
<#if !includeSelling && !sharingGroups?has_content>
  <p>We do not sell personal data or process personal data for targeted advertising.</p>
</#if>


<h3><a id="ctdpa-profiling">Profiling</a></h3>
<p>The CTDPA gives consumers the right to opt out of automated profiling that produces legal or similarly significant effects, such as approval for a loan, employment, or insurance.</p>

<#if createsProfiles>
  <p><a style="color: #007BBD" href="${optOutUrl}">You may opt-out of these data practices here</a>.</p>
<#else>
  <p>We do not profile consumers in furtherance of decisions that produce legal or similarly significant effects.</p>
</#if>


<h3><a id="ctdpa-rights">CTDPA Rights</a></h3>

<p>
Your CTDPA rights are described below. <a style="color: #007BBD" href="${privacyRequestUrl}">Make a Privacy Request by clicking here or the link at the top of this page</a>. <#if hasPhysicalLocation && privacyRequestTollFreeNumber?has_content>
    Alternatively, you can make a request by calling us toll-free at ${privacyRequestTollFreeNumber}.
  </#if>
</p>

<h4><a id="ctdpa-right-to-access">Right to Access</a></h4>
<p>
You have the right to confirm whether we are processing personal data about you and to access such data. Where processing
is carried out by automated means, you have a right to receive a copy of your personal data in a portable and readily usable
format that allows you to transmit your data to another controller.
</p>

<p>If you make a Request to Know more than twice in a 12-month period, we may require you to pay a small fee for this service.</p>

<h4><a id="ctdpa-right-to-delete">Right to Delete</a></h4>
<p>
  You have the right to request that we delete any personal data provided by or obtained about you. We will permanently delete any such personal data
  from our records and direct our processors to do the same. However, we may retain your personal data if it is necessary for certain purposes,
  including the following:</p>
  <ul>
    <li>To comply with legal obligations</li>
    <li>To comply with an official investigation or cooperate with law-enforcement agencies</li>
    <li>To establish or defend legal claims</li>
    <li>To complete an obligation to you that you have requested</li>
    <li>To respond to security incidents, fraud, harassment, and other similar activity</li>
    <li>To identify and repair technical errors</li>
    <li>To conduct internal research to develop, improve, and repair our products and services</li>
    <li>For internal operations that are reasonably aligned with your expectations</li>
  </ul>
<p>Any personal data retained for these purposes will not be processed for other purposes.</p>

<h4><a id="ctdpa-right-to-non-discrimination">Right to Non-Discrimination</a></h4>

<p>If you exercise your CTDPA consumer rights:</p>
<ul>
  <li>We will not deny goods or services to you</li>
  <li>We will not charge you different prices or rates for goods or services</li>
  <li>We will not provide a different level or quality of goods or services to you</li>
</ul>

<p>
  However, we may offer a different price, rate, level, quality, or selection of products or services if your personal data is
  required in order to provide those products or services and you have exercised your right to opt out, or the offer is related
  to a voluntary loyalty or rewards program.
</p>

<h4><a id="ctdpa-right-to-opt-out">Right to Opt-Out</a></h4>

<#if optOutUrl?has_content>
  <p>
    You have the right to opt-out of any selling of your personal data, processing of your personal data for purposes of targeted
    advertising, or profiling in furtherance of decisions that produce legal or similarly significant effects for you.
  </p>
  <a style="color: #007BBD" href="${optOutUrl}">You may exercise your right to opt-out</a>.
<#else>
  <p>
    We do not sell your personal data, process it for targeted advertising, or engage in profiling in this way.
  </p>
</#if>

<p>
    <u>Authorized Agents.</u>
    You may authorize an agent to submit a Request to Opt-Out on your behalf, including through a technology such as a web link, browser setting,
    or global device setting. We will comply with such requests if we are able to authenticate your identity and the agent’s authority to act on
    your behalf.
</p>

<h4><a id="ctdpa-right-to-correct">Right to Correct</a></h4>
<p>You have the right to correct inaccuracies in your personal data, taking into account the nature of the data and our purposes for processing it.</p>

<h4><a id="ctdpa-authenticating-your-request">Authenticating Your Request</a></h4>
<p>
  Once we receive your request, we will verify the information you provided by matching the information that we have collected. If we cannot
  authenticate your request, we may ask for additional information from you. If you are unable to provide additional information, or we are unable to
  authenticate the request using commercially reasonable efforts, we may deny your request. Authentication is not required for a Request to Opt-Out,
  but we may deny the request if we have a good faith, reasonable, and documented belief that the request is fraudulent.
</p>

<h4><a id="ctdpa-right-to-appeal">Right to Appeal</a></h4>
<p>If we decline to take action in response to any of your privacy requests, you have the right to appeal that decision within a reasonable amount of time, but no later than 90 days from the date of our decision. <a style="color: #007BBD" href="${privacyRequestUrl}">To submit a request for appeal, click here</a> and select "Appeal a Decision" in the request type drop-down.</p>
<p>
  If you believe your rights have been violated and you are not able to resolve the issue directly with us, you may file a complaint with
  the <a href="https://www.dir.ct.gov/ag/complaint/" target="_blank" rel="nofollow">Connecticut Attorney General’s Office</a>.
</p>
