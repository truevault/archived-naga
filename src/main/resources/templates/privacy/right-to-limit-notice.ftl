<p>
  We use the following sensitive personal information to provide you with products and services and other uses approved by the CCPA: ${sensitiveCollection}
</p>

<p>
  In some cases, we may use this information to infer characteristics about you. In the course of doing so we may disclose your sensitive personal
  information to our service providers and/or contractors. You have the right to limit our use of your sensitive personal information to uses that
  are necessary to provide you with products and services. To exercise your right, submit a request below. A consumer or their authorized agent
  may instead submit a Request to Limit via email to <a href="mailto:${privacyRequestEmail}">${privacyRequestEmail}</a>.
</p>
