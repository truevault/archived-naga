<h2><a id="information-we-collect">Information we collect</a></h2>

<#if collectionGroupDetails??>
<h3><a id="information-you-provide-to-us">Information you provide to us</a></h3>
<#list collectionGroupDetails as groupDetail>
    <p>
        We collect the personal information you provide to us when you ${groupDetail.collectionGroupLabel}. The
        categories of information we may collect include:
    </p>

    <ul>
        <#list groupDetail.picGroups as picGroup>
            <li>${picGroup}</li>
        </#list>
    </ul>
</#list>

<p>To the extent we process deidentified personal information, we will make no attempt to reidentify such data.</p>
</#if>

<h3><a id="information-collected-automatically">Information collected automatically</a></h3>

<p>
  We automatically collect internet or other electronic information about you when you visit our website, such as IP
  address, browsing history and interactions with our website.

  <#if hasGeolocationCollection>
  We also collect geolocation data.
  </#if>

  This data may be collected using browser cookies and
  other tracking technologies.
</p>

<#if cookiePolicy??>
<p>
    <u>Browser Cookies.</u>
    ${cookiePolicy}
</p>
</#if>

<p>
    <u>Opt-Out Preference Signals.</u>
    Your browser settings may allow you to automatically transmit an opt-out preference signal, such as the Global Privacy Control (GPC) signal,
    to online services you visit. When we detect such signal, we place a U.S. Privacy String setting in your browser so that any third party who
    respects that signal will not track your activity on our website. Your request to opt-out of sale/sharing will be linked to your browser
    identifier only and not linked to any account information because the connection between your browser and the account is not known to us.
    GPC is supported by certain internet browsers or as a browser extension. You can find out how to enable GPC
    <a href="https://globalprivacycontrol.org/" target="_blank" rel="nofollow">here</a>.
</p>

<#if collectionSources?has_content>
<h3><a id="information-from-other-sources">Information from other sources</a></h3>
    <p>We may collect personal information about you from third-party sources, including ${collectionSources}.</p>

    <#if hasCollectionDetails>
        <#list collectionCategoryLabels as label, categories>
            <p><strong>${label}</strong></p>
            <ul>
                <#list categories as category>
                    <#list category as group>
                        <li>${group}</li>
                    </#list>
                </#list>
            </ul>
        </#list>
    </#if>
</#if>

<#if retentionPolicy?has_content>
<h3><a id="how-long-we-keep-your-data">How long we keep your data</a></h3>

<p>${retentionPolicy}
    <#if retentionDetails?has_content>
        We generally retain data according to the guidelines below.</p>

        <table>
            <thead>
            <tr>
                <th width="40%">Type of Data</th>
                <th width="60%">Retention Period</th>
            </tr>
            </thead>
            <tbody>
            <#list retentionDetails as detail>
                <tr>
                <td>${detail.label}</td>
                <td>${detail.userText}</td>
                </tr>
            </#list>
            </tbody>
        </table>
    <#else>
      </p>
    </#if>

</#if>

<h3><a id="purposes-of-processing">Purposes of Processing</a></h3>
<p>We process personal information for the following purposes:</p>
<ul>
  <#list purposeLabels as label>
    <li>${label}</li>
  </#list>
</ul>
