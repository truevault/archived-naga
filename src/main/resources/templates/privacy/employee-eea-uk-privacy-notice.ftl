<h2><a id="gdpr-employee-privacy-notice">GDPR Privacy Notice</a></h2>

${privacyNoticeIntroText}

<h3><a id="gdpr-employee-personal-data-we-collect-from-you">Personal data we collect from you</a></h3>
<p>We collect personal data from you, including ${collectionLabel}</p>

<#if collectionSources?has_content>
<h3><a id="gdpr-employee-information-from-other-sources">Information from other sources</a></h3>

<p>We may collect personal information about you from third-party sources, including ${collectionSources}.
</#if>

<h3><a id="gdpr-employee-personal-data-disclosed-to-processors">Personal data disclosed to processors</a></h3>
<p>
  We use third party vendors to assist us with employment functions, such as the administration of benefits. We may
  disclose your personal data to the following categories of processors: ${disclosureCategories}
</p>

<h3><a id="gdpr-employee-legitimate-interests">Legitimate Interests</a></h3>
<p>We process your personal data on the lawful basis of our legitimate interests, and specifically for purposes of employment.</p>

<#if purposeLabels?has_content>
<p>We may also process personal data on the basis of the following legitimate interests: ${purposeLabels}</p>
</#if>

<h3><a id="gdpr-employee-data-retention">Data Retention</a></h3>
<p>We do not retain data for any longer than is necessary for the purposes described in this Policy.</p>

<#if isAutomatedDecisionMaking>
  <h3><a id="gdpr-employee-automated-decision-making">Automated Decision Making</a></h3>
  <p>We engage in automated decision making.</p>
</#if>

<h3><a id="gdpr-employee-international-data-transfers">International Data Transfers</a></h3>
<#if isInternationalDataTransfers>
<p>
  We may send the personal data of individuals in the EEA/UK to third countries, including the United States, where it may be stored or processed, for example on our service providers’ cloud servers. When we transfer personal data, we rely either on Adequacy Decisions as adopted by the European Commission (EC) on the basis of Article 45 of Regulation (EU) 2016/679 (GDPR), Standard Contractual Clauses (SCCs) issued by the EC or International Data Transfer Agreements (IDTAs) approved by the UK Information Commissioner’s Office. Data protection authorities have determined that the SCCs and IDTAs provide sufficient safeguards to protect personal data transferred outside the EEA/UK. You may read more about the SCCs and IDTAs at the following links:
</p>
<ul>
  <li><a href="https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/standard-contractual-clauses-scc_en" style="color: #007BBD" target="_blank" rel="nofollow">https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/standard-contractual-clauses-scc_en</a></li>
  <li><a href="https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/international-data-transfer-agreement-and-guidance/" style="color: #007BBD" target="_blank" rel="nofollow">https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/international-data-transfer-agreement-and-guidance/</a></li>
</ul>
<#else>
<p>
  ${orgName} is not established within the EEA/UK and our website servers are located in the United States, which has not been the subject of an adequacy decision by European data protection authorities. By interacting with this website, you are transmitting your personal data to the United States.
</p>
</#if>

<h3><a id="gdpr-employee-privacy-rights">Privacy Rights</a></h3>
<p>
  Individuals in the EEA/UK have the following rights regarding their personal data.

  <#if privacyRequestUrl?has_content>
  <a style="color: #007BBD" href="${privacyRequestUrl}">Make a Privacy Request by clicking here or the link at the top of this page</a>. Once you submit a request, we will verify your identity and process your request in most cases within 30 days.
  </#if>
</p>

<p><strong>Right to access.</strong> You have the right to request a copy of the personal data we hold about you.</p>

<p><strong>Right of portability.</strong> You have the right to ask us to transfer your data to another party.</p>

<p><strong>Right to rectification.</strong> You have the right to request that we rectify any incorrect information we have about you.</p>

<p><strong>Right of erasure.</strong> You have the right to request that we erase (delete) any personal information we hold about you.</p>

<#if isAutomatedDecisionMaking>
  <p><strong>Right not to be subject to automated decision making.</strong> You have the right to be removed from automated decision making that produces legal or similar effects.</p>
</#if>

<p>
  <strong>Right to lodge a complaint with a supervisory authority.</strong> You have a right to lodge a complaint with a supervisory authority. For more information, you can visit the
  Information Commissioner’s Office website at <a href="https://ico.org.uk/" target="_blank" rel="nofollow">https://ico.org.uk/</a>, or see a list of EU Data Protection Authorities at
  <a href="https://www.gdprregister.eu/gdpr/dpa-gdpr/" target="_blank" rel="nofollow">https://www.gdprregister.eu/gdpr/dpa-gdpr/</a>.
</p>

<h3><a id="gdpr-employee-inquiries">Inquiries</a></h3>
<h4><a id="gdpr-employee-controller-contact-information">Controller contact information</a></h4>
<p>${orgName}</p>
<p><a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a></p>

<#if dpoName?has_content && dpoEmail?has_content>
  <h4><a id="gdpr-employee-data-protection-officer">Data Protection Officer information</a></h4>
  <p>${dpoName}</p>
  <p><a href="mailto:${dpoEmail}" style="color: #007BBD" target="_blank">${dpoEmail}</a></p>
</#if>
