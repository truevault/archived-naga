<h2><a id="vcdpa-privacy-notice">Virginia Privacy Notice (VCDPA)</a></h2>
${privacyNoticeIntroText}

<h3><a id="vcdpa-collection-and-disclosure-of-personal-information">Collection and Disclosure of Personal Information</a></h3>
<p>
The personal data we collect is described above in <strong><a href="#information-we-collect">Information we collect</a></strong>. The personal data we disclose to third parties and the categories of third parties
to whom we disclose personal data is described above in <strong><a href="#how-we-disclose-information">How we disclose information</a></strong>. The length of time for which we retain personal information is
described above in <strong><a href="#how-long-we-keep-your-data">How long we keep your data</a></strong>. Our purposes for processing data are described above in <strong><a href="#purposes-of-processing">Purposes of processing</a></strong>.
</p>

<h3><a id="vcdpa-data-selling-and-targeted-advertising">Data “Selling” and Targeted Advertising</a></h3>
<#if sellsInExchangeForMoney>
  <p>We "sell" personal data to third parties, as that term is defined in the VCDPA.</p>
</#if>

<#if sharingGroups?has_content>
  <p>
    We process personal data for purposes of targeted advertising (as defined in the VCDPA), including online identifiers and internet
    activity. This allows us to show you ads that are more relevant to you.
  </p>

  <p>
    <a style="color: #007BBD" href="${optOutUrl}">You may opt-out of these data practices here</a>.
  </p>
<#else>
  <p>We do not sell personal data or process personal data for targeted advertising.</p>
</#if>


<h3><a id="vcdpa-profiling">Profiling</a></h3>
<p>The VCDPA gives consumers the right to opt out of automated profiling that produces legal or similarly significant effects, such as approval for a loan, employment, or insurance.</p>

<#if createsProfiles>
  <p><a style="color: #007BBD" href="${optOutUrl}">You may opt-out of these data practices here</a>.</p>
<#else>
  <p>We do not profile consumers in furtherance of decisions that produce legal or similarly significant effects.</p>
</#if>


<h3><a id="vcdpa-rights">VCDPA Rights</a></h3>

<p>
Your VCDPA rights are described below. <a style="color: #007BBD" href="${privacyRequestUrl}">Make a Privacy Request by clicking here or the link at the top of this page</a>. <#if hasPhysicalLocation && privacyRequestTollFreeNumber?has_content>
    Alternatively, you can make a request by calling us toll-free at ${privacyRequestTollFreeNumber}.
  </#if>
</p>

<h4><a id="vcdpa-right-to-access">Right to Access</a></h4>
<p>
You have the right to confirm whether we are processing personal data about you and to access such data. Where processing
is carried out by automated means, you have a right to receive a copy of your personal data in a portable and readily usable
format that allows you to transmit your data to another controller.
</p>

<p>
If you make an Access Request more than twice in a 12-month period, or we determine the request is manifestly unfounded or excessive,
we may require you to pay a small fee for this service.
</p>

<h4><a id="vcdpa-right-to-delete">Right to Delete</a></h4>
<p>
  You have the right to request that we delete any personal data provided by or obtained about you. Subject to certain limitations, we will permanently delete
  any such personal data from our records and direct our processors to do the same.</p>

<h4><a id="vcdpa-right-to-non-discrimination">Right to Non-Discrimination</a></h4>

<p>If you exercise your VCDPA consumer rights:</p>
<ul>
  <li>We will not deny goods or services to you</li>
  <li>We will not charge you different prices or rates for goods or services</li>
  <li>We will not provide a different level or quality of goods or services to you</li>
</ul>

<p>
  However, we may offer a different price, rate, level, quality, or selection of products or services if your personal data is
  required in order to provide those products or services and you have exercised your right to opt out, or the offer is related
  to a voluntary loyalty or rewards program.
</p>

<h4><a id="vcdpa-right-to-opt-out">Right to Opt-Out</a></h4>

<p>Sale of Personal Data:<br />
<#if optOutUrl?has_content && sellsInExchangeForMoney>
  <a style="color: #007BBD" href="${optOutUrl}">Exercise your right to opt-out here</a>.
<#else>
  We do not sell your personal data, as defined by the VCDPA.
</#if>
</p>

<p>Targeted Advertising:<br />
<#if optOutUrl?has_content && processesForTargetedAds>
  <a style="color: #007BBD" href="${optOutUrl}">Exercise your right to opt-out here</a>.
<#else>
  We do not process your personal data for the purpose of targeted advertising.
</#if>
</p>

<p>Profiling:<br />
<#if optOutUrl?has_content && createsProfiles>
  <a style="color: #007BBD" href="${optOutUrl}">Exercise your right to opt-out here</a>.
<#else>
  We do not profile consumers in furtherance of decisions that produce legal or similarly significant effects.
</#if>
</p>

<h4><a id="vcdpa-right-to-correct">Right to Correct</a></h4>
<p>You have the right to correct inaccuracies in your personal data, taking into account the nature of the data and our purposes for processing it.</p>

<h4><a id="vcdpa-authenticating-your-request">Authenticating Your Request</a></h4>
<p>Once we receive your request, we will verify the information you provided by matching the information that we have collected. If we cannot authenticate your request, we may ask for additional information from you. If you are unable to provide additional information, or we are unable to authenticate the request using commercially reasonable efforts, we may deny your request.</p>

<h4><a id="vcdpa-right-to-appeal">Right to Appeal</a></h4>
<p>If we decline to take action in response to any of your privacy requests, you have the right to appeal that decision within a reasonable amount of time, but no later than 90 days from the date of our decision. <a style="color: #007BBD" href="${privacyRequestUrl}">To submit a request for appeal, click here</a> and select "Appeal a Decision" in the request type drop-down.</p>
<p>
  If you believe your rights have been violated and you are not able to resolve the issue directly with us, you may file a complaint with
  the <a href="https://www.oag.state.va.us/consumer-protection/index.php/file-a-complaint" target="_blank" rel="nofollow">Virginia Attorney General’s Office</a>.
</p>
