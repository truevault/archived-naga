<h2><a id="ccpa-employee-notice-to-group">Notice to ${consumerGroup.name}</a></h2>
<#if customMessage?has_content>
<div>${customMessage}</div>
</#if>

${privacyNoticeIntroText}

<h3><a id="ccpa-employee-personal-information-we-collect">Personal Information We Collect</a></h3>
<p>We collect the following categories of information, as defined in the CCPA:</p>

<ul>
  ${collectCategories}
</ul>

<h3><a id="ccpa-employee-sources-of-personal-information">Sources of Personal Information</a></h3>

<p>We collect personal information directly from you when you provide it to us.
<#if collectionSources?has_content>
We may also collect personal information about you from other sources, including ${collectionSources}.
</#if>
</p>

<#if retentionPeriod?has_content>
<h3><a id="ccpa-employee-how-long-we-keep-your-data">How Long We Keep Your Data</a></h3>

<div>${retentionPeriod}</div>
</#if>

<h3><a id="ccpa-employee-purposes-for-collecting-information">Business <#if isCommercial>and Commercial </#if>Purposes for Collecting Information</a></h3>
<p>We collect this information for the following purpose(s):</p>
<ul>
  <li>Employment-Related</li>
  ${employmentRelated}
</ul>

<p>
<#if isCommercial>
We may also use the personal information we collect about you for commercial purposes.
<#else>
We do not use your personal information for commercial purposes.
</#if>
</p>

<h3><a id="ccpa-employee-how-we-disclose-personal-information">How We Disclose Personal Information</a></h3>

<p>In the last 12 months, we may have disclosed personal information about you to the following categories of parties:</p>

<table>
 <thead>
   <tr>
     <th>Personal Information Disclosed</th>
     <th>Recipient (by Category)</th>
   </tr>
 </thead>
 <tbody>
    <#list disclosureCategories as disclosureGroup>
      <tr>
        <td>${disclosureGroup.picGroup}</td>
        <td>${disclosureGroup.recipientCategories}</td>
      </tr>
    </#list>
 </tbody>
</table>

<#if !isSelling && !isSharing>
<p>We do not "sell" or "share" your personal information as defined in the CCPA</p>
<#else>
<#if isSelling>
<p>We may "sell" (as defined in the CCPA) the following personal information about you: ${sellingCategories}</p>
</#if>
<#if isSharing>
<p>We may "share" (as defined in the CCPA) the following personal information about you: ${sharingCategories}</p>
</#if>
</#if>

<h3><a id="ccpa-employee-your-ccpa-rights">Your CCPA Rights</a></h3>
<p>
  You can make a Request to Know, Request to Correct, or a Request to Delete under the CCPA by submitting a Privacy Request by
  clicking
  <a style="color: #007BBD" href="${privacyRequestUrl}">here</a>
  or emailing <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a>.

  <#if hasPhysicalLocation && privacyRequestTollFreeNumber?has_content>
    Alternatively, you can make a request by calling us toll-free at ${privacyRequestTollFreeNumber}.
  </#if>
</p>

<h4><a id="ccpa-employee-right-to-know-and-access">Right to Know and Access</a></h4>

<p>You have the right to request to know and access the following about the personal information we have collected about you in the past 12 months:</p>

<ul>
  <li>the categories and specific pieces of personal information we have collected about you</li>
  <li>the categories of sources from which we collect personal information about you</li>
  <li>the business or commercial purposes for which we collect, sell, or share personal information</li>
  <li>the categories of third parties with whom we disclose the information</li>
  <li>the categories of personal information about you that we have sold, shared, or disclosed for a business purpose, and the categories of third parties to whom we have sold, shared, or disclosed that information for a business purpose</li>
</ul>

<p>The information we would provide to you in response to a Request to Know Categories is contained in this Privacy Notice. To receive a copy of the specific personal information we have about you, submit a Request to Know or Access via the link above. If you make a Request to Know more than twice in a 12-month period, or we determine the request is manifestly unfounded or excessive, we may require you to pay a small fee for this service.</p>

<h4><a id="ccpa-employee-right-to-delete">Right to Delete</a></h4>
<p>You have the right to request that we delete any personal information about you that you have provided to us. Subject to certain limitations, we will delete from our records your requested personal information and direct our service providers to do the same.</p>

<h4><a id="ccpa-employee-right-to-non-discrimination">Right to Non-Discrimination</a></h4>
<p>We will not discriminate or retaliate against you for your exercise of your CCPA rights.</p>

<h4>Right to Opt-Out</h4>
<#if (isSelling || isSharing) && optOutUrl?has_content>
  <p>You have the right to opt-out of any selling and sharing of your personal information.</p>
  <p><a style="color: #007BBD" href="${optOutUrl}">You may exercise your right to opt-out here</a>.</p>
<#else>
  <p>We do not sell or share any personal information to third parties.</p>
</#if>

<h3><a id="ccpa-employee-request-verification">Request Verification</a></h3>
<p>Before we can respond to a Request to Know or Request to Delete, we will need to verify that you are the consumer who is the subject of the CCPA request. Verification is important for preventing fraudulent requests and identity theft. Requests to Opt-Out do not require verification.</p>
<p>Typically, identity verification will require you to confirm certain information about yourself based on information we have already collected. For example, we will ask you to verify that you have access to the email address we have on file for you. If we cannot verify your identity based on our records, we cannot fulfill your CCPA request. If you submit a request not through our webform, we will ask you to provide the same information requested in the webform.</p>
<p>For a request that seeks specific personal information, we ask that you sign a declaration stating that you are the consumer whose personal information is the subject of the request, as required by the CCPA.</p>
<p>In some cases, we may have no reasonable method by which we can verify a consumer's identity. For example:</p>

<ul>
  <li>If a consumer submits a request but we have not collected any personal information about that consumer, we cannot verify the request.</li>
  <li>If the only data we have collected about a consumer is gathered through website cookies (i.e. the consumer visited our website but had no other interaction with us), we are unable to reasonably associate a requester with any data collected; therefore, we cannot verify the request.</li>
</ul>

<h4><a id="ccpa-employee-authorized-agent">Authorized Agent</a></h4>

<p>
  A California resident's authorized agent may submit a rights request under the CCPA by emailing us at
  <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a>.
  Requests submitted by an authorized agent will still require verification of the consumer who is the subject of the request in accordance with the process
  described above. We will also ask for proof that the consumer who is the subject of the request authorized an agent to submit a privacy request on their
  behalf by either verifying their own identity with us directly or directly confirming with us that they provided the authorized agent permission to submit
  the request. An authorized agent that has power of attorney pursuant to California Probate Code section 4121 to 4130 may also submit proof of statutory
  power of attorney, which does not require separate consumer verification.
</p>

<p>If you have trouble accessing this notice, please contact us at <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a>.</p>

<h4><a id="ccpa-employee-contact-us">Contact Us</a></h4>
<p>If you have any privacy-related questions or have trouble accessing this notice, please email <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a>.</p>
