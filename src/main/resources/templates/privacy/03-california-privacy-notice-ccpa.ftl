<h2><a id="ccpa-privacy-notice">California Privacy Notice (CCPA)</a></h2>

${privacyNoticeIntroText}

<h3><a id="ccpa-collection-and-disclosure-of-personal-information">Collection and Disclosure of Personal Information</a></h3>

<p>
  The personal information we collect is described above in <strong><a href="#information-we-collect">Information we collect</a></strong>. The personal
  information we disclose for business or commercial purposes is described above in <strong><a href="#how-we-disclose-information">How we disclose
  information</a></strong>. The length of time for which we retain personal information is described above in <strong><a href="#how-long-we-keep-your-data">How
  long we keep your data</a></strong>. Our purposes for processing data are described above in <strong><a href="#purposes-of-processing">Purposes of processing</a></strong>.
</p>

<h3><a id="ccpa-information-sharing-and-selling">Information “Sharing” and “Selling”</a></h3>
<#if hasOptOut>
  <#if sharingGroups?has_content>
  <p>
    We “share” certain personal information with third party ad networks for purposes of behavioral advertising,
    including: ${sharingGroups}. This allows us to show you ads that are more relevant to you.
  </p>
  </#if>

  <#if includeSelling>
  <p>
      We use third party data analytics providers and this may be considered a “sale” of information under the CCPA.
  </p>
  </#if>

  <#if optOutUrl?has_content>
    <p>
      <a style="color: #007BBD" href="${optOutUrl}">You may opt-out of these data practices here</a>.
    </p>
  </#if>
<#else>
  <p>We do not sell or share any personal information to any third parties.</p>
</#if>

<#if noUnderageSellingOrSharing>
<p>We do not knowingly sell or share (for cross-context behavioral advertising) the personal information of consumers under 16 years of age.</p>
</#if>

<#if hasSpiRequest>
<h3><a id="ccpa-how-we-use-sensitive-personal-information">How We Use Sensitive Personal Information</a></h3>

<p>We use the following sensitive personal information to provide you with products and services and other uses approved by the
CCPA: ${spiCategoryList}. In some cases, we may use this information to infer characteristics about you. In the course of
doing so we may disclose your sensitive personal information to our service providers and/or contractors. You have the right to
limit our use of your sensitive personal information to uses that are necessary to provide you with products and services.
<a style="color: #007BBD" href="${spiRequestUrl}" target="_blank">To exercise your right, submit a request</a>. Alternatively,
you or your authorized agent may submit a Request to Limit via email to <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a>.</p>
</#if>

<h3><a id="ccpa-rights">CCPA Rights</a></h3>
<p>
  Your CCPA rights are described below. <a style="color: #007BBD" href="${privacyRequestUrl}">Make a Privacy Request by clicking here or the link at the top of this page</a> or by emailing us at <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a>.

  <#if hasPhysicalLocation && privacyRequestTollFreeNumber?has_content>
    Alternatively, you can make a request by calling us toll-free at ${privacyRequestTollFreeNumber}.
  </#if>
</p>

<h4><a id="ccpa-right-to-know-and-access">Right to Know and Access</a></h4>

<p>You have the right to request to know and access the following about the personal information we have collected about you in the past 12 months:</p>

<ul>
  <li>the categories and specific pieces of personal information we have collected about you</li>
  <li>the categories of sources from which we collect personal information about you</li>
  <li>the business or commercial purposes for which we collect, sell, or share personal information</li>
  <li>the categories of third parties with whom we disclose the information</li>
  <li>the categories of personal information about you that we have sold, shared, or disclosed for a business purpose, and the categories of third parties to whom we have sold, shared, or disclosed that information for a business purpose</li>
</ul>

<p>The information we would provide to you in response to a Request to Know Categories is contained in this Privacy Notice. To receive a copy of the specific personal information we have about you, submit a Request to Know or Access via the link above. If you make a Request to Know more than twice in a 12-month period, or we determine the request is manifestly unfounded or excessive, we may require you to pay a small fee for this service.</p>

<h4><a id="ccpa-right-to-delete">Right to Delete</a></h4>
<p>You have the right to request that we delete any personal information about you that you have provided to us. Subject to certain limitations, we will delete from our records your requested personal information and direct our service providers to do the same.</p>

<h4><a id="ccpa-right-to-non-discrimination">Right to Non-Discrimination</a></h4>
<p>If you exercise your CCPA consumer rights:</p>
<ul>
  <li>We will not deny goods or services to you</li>
  <li>We will not charge you different prices or rates for goods or services, including through the use of discounts or other benefits or penalties</li>
  <li>We will not provide a different level or quality of goods or services to you</li>
  <li>We will not suggest that you may receive a different price or rate for goods or services or a different level or quality of goods or services</li>
  <li>We will not retaliate against you, as an employee, applicant for employment, or independent contractor</li>
</ul>

<p>We may still offer you certain financial incentives permitted by the CCPA such as discounts, rewards, premium features, or loyalty accounts that can result in different prices, rates, or quality levels.</p>

<h4><a id="ccpa-right-to-opt-out">Right to Opt-Out</a></h4>
<#if optOutUrl?has_content>
  <p>You have the right to opt-out of any selling and sharing of your personal information.</p>
  <p>
    <a style="color: #007BBD" href="${optOutUrl}">You may exercise your right to opt-out here</a>.
  </p>
<#else>
  <p>We do not sell or share any personal information to third parties.</p>
</#if>

<#if rightToOptIn>
  <h4><a id="ccpa-right-to-opt-in">Right to Opt-In</a></h4>

  <p>If you are a consumer between the ages of 13 and 15, please email us at <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a> if you would like to opt-in to the sale of information.</p>
  <p>If you are the parent or legal guardian of a consumer under the age of 13, and would like to opt the consumer into the sale of information, please email us at <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a> so we can verify your relationship to the consumer.</p>
</#if>

<h4><a id="ccpa-right-to-correct">Right to Correct</a></h4>
  <p>You have the right to correct inaccuracies in your personal data, taking into account the nature of the data and our purposes for processing it.</p>

<h3><a id="ccpa-request-verification">Request Verification</a></h3>
<p>Before we can respond to a Request to Know or Request to Delete, we will need to verify that you are the consumer who is the subject of the CCPA request. Verification is important for preventing fraudulent requests and identity theft. Requests to Opt-Out do not require verification.</p>
<p>Typically, identity verification will require you to confirm certain information about yourself based on information we have already collected. For example, we will ask you to verify that you have access to the email address we have on file for you. If we cannot verify your identity based on our records, we cannot fulfill your CCPA request. If you submit a request not through our webform, we will ask you to provide the same information requested in the webform.</p>
<p>For a request that seeks specific personal information, we ask that you sign a declaration stating that you are the consumer whose personal information is the subject of the request, as required by the CCPA.</p>
<p>In some cases, we may have no reasonable method by which we can verify a consumer's identity. For example:</p>

<ul>
  <li>If a consumer submits a request but we have not collected any personal information about that consumer, we cannot verify the request.</li>
  <li>If the only data we have collected about a consumer is gathered through website cookies (i.e. the consumer visited our website but had no other interaction with us), we are unable to reasonably associate a requester with any data collected; therefore, we cannot verify the request.</li>
  <#if isServiceProvider>
    <li>If we process a consumer’s information as a service provider to another organization, we are not permitted to respond in a substantive way to that consumer’s CCPA request. We will, however, assist the organization in fulfilling your privacy request.</li>
  </#if>
</ul>

<h4><a id="ccpa-authorized-agent">Authorized Agent</a></h4>

<p>
  A California resident's authorized agent may submit a rights request under the CCPA by emailing us at
  <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a>.
  Requests submitted by an authorized agent will still require verification of the consumer who is the subject of the request in accordance with the process
  described above. We will also ask for proof that the consumer who is the subject of the request authorized an agent to submit a privacy request on their
  behalf by either verifying their own identity with us directly or directly confirming with us that they provided the authorized agent permission to submit
  the request. An authorized agent that has power of attorney pursuant to California Probate Code section 4121 to 4130 may also submit proof of statutory
  power of attorney, which does not require separate consumer verification.
</p>

<p>If you have trouble accessing this notice, please contact us at <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a>.</p>

<h4><a id="ccpa-contact-us">Contact Us</a></h4>
<p>If you have any privacy-related questions or have trouble accessing this notice, please email <a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a>.</p>
