<h2><a id="gdpr-privacy-notice">EEA/UK Privacy Notice (GDPR)</a></h2>

${privacyNoticeIntroText}

<h3><a id="gdpr-collection-and-disclosure-of-personal-data">Collection and Disclosure of Personal Data</a></h3>

<p>
The personal data we collect is described above in <strong><a href="#information-we-collect">Information we collect</a></strong>. The personal data we
disclose for business or commercial purposes is described above in <strong><a href="#how-we-disclose-information">How we disclose information</a></strong>.
The length of time for which we retain personal data is described above in <strong><a href="#how-long-we-keep-your-data">How long we keep your data</a></strong>.
Our purposes for processing data are described above in <strong><a href="#purposes-of-processing">Purposes of processing</a></strong>.
</p>

<#if controllers?has_content>
  <#assign controllerVendors = controllers?map(c -> c.vendor)?filter(v->v??)>
  <p>
    We may disclose your personal information to the following third party controllers for business purposes:
    ${controllerVendors?map(v -> v.name)?join(", ")}. To understand how these parties handle your data, please refer to their respective
    privacy policies.
  </p>
</#if>

<h3><a id="gdpr-lawful-bases-and-legitimate-interests">Lawful Bases and Legitimate Interests</a></h3>
<p>We process personal data on the following lawful bases:</p>

<ul>
  <li>Complying with legal obligations</li>
  <li>Fulfilling contracts</li>
  <li>Consent</li>
  <li>Legitimate interests</li>
  <#if hasLawfulBasisPublicInterest>
    <li>Public interest</li>
  </#if>
  <#if hasLawfulBasisVitalInterest>
    <li>Vital interest of the individual</li>
  </#if>

</ul>

<#if activities?has_content>
<p>
  Where we process personal data on the basis of our legitimate interests, we pursue the following interests: ${activities}.
</p>
</#if>

<#if isAutomatedDecisionMaking>
  <h3><a id="gdpr-automated-decision-making">Automated Decision Making</a></h3>
  <p>We engage in automated decision making.</p>
</#if>

<h3><a id="gdpr-international-data-transfers">International Data Transfers</a></h3>
<#if isInternationalDataTransfers>
<p>
  We may send the personal data of individuals in the EEA/UK to third countries, including the United States, where it may be stored or processed, for example on our service providers’ cloud servers. When we transfer personal data, we rely either on Adequacy Decisions as adopted by the European Commission (EC) on the basis of Article 45 of Regulation (EU) 2016/679 (GDPR), Standard Contractual Clauses (SCCs) issued by the EC or International Data Transfer Agreements (IDTAs) approved by the UK Information Commissioner’s Office. Data protection authorities have determined that the SCCs and IDTAs provide sufficient safeguards to protect personal data transferred outside the EEA/UK. You may read more about the SCCs and IDTAs at the following links:
</p>
<ul>
  <li><a href="https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/standard-contractual-clauses-scc_en" style="color: #007BBD" target="_blank" rel="nofollow">https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/standard-contractual-clauses-scc_en</a></li>
  <li><a href="https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/international-data-transfer-agreement-and-guidance/" style="color: #007BBD" target="_blank" rel="nofollow">https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/international-data-transfer-agreement-and-guidance/</a></li>
</ul>
<#else>
<p>
  ${orgName} is not established within the EEA/UK and our website servers are located in the United States, which has not been the subject of an adequacy decision by European data protection authorities. By interacting with this website, you are transmitting your personal data to the United States.
</p>
</#if>

<h3><a id="gdpr-privacy-rights">Privacy Rights</a></h3>
<p>
  Individuals in the EEA/UK have the following rights regarding their personal data.

  <#if privacyRequestUrl?has_content>
  <a style="color: #007BBD" href="${privacyRequestUrl}">Make a Privacy Request by clicking here or the link at the top of this page</a>. Once you submit a request, we will verify your identity and process your request in most cases within 30 days.
  </#if>
</p>

<p><strong>Right to access.</strong> You have the right to request a copy of the personal data we hold about you.</p>

<p><strong>Right of portability.</strong> You have the right to ask us to transfer your data to another party.</p>

<p><strong>Right to rectification.</strong> You have the right to request that we rectify any incorrect information we have about you.</p>

<p><strong>Right of erasure.</strong> You have the right to request that we erase (delete) any personal information we hold about you.</p>

<#if isAutomatedDecisionMaking>
  <p><strong>Right not to be subject to automated decision making.</strong> You have the right to be removed from automated decision making that produces legal or similar effects.</p>
</#if>

<p>
  <strong>Right to lodge a complaint with a supervisory authority.</strong> You have a right to lodge a complaint with a supervisory authority. For more information, you can visit the
  Information Commissioner’s Office website at <a href="https://ico.org.uk/" target="_blank" rel="nofollow">https://ico.org.uk/</a>, or see a list of EU Data Protection Authorities at
  <a href="https://www.gdprregister.eu/gdpr/dpa-gdpr/" target="_blank" rel="nofollow">https://www.gdprregister.eu/gdpr/dpa-gdpr/</a>.
</p>

<h3><a id="gdpr-inquiries">Inquiries</a></h3>

<h4><a id="gdpr-controller-contact-information">Controller contact information</a></h4>
<p>${orgName}</p>
<p><a href="mailto:${privacyRequestEmail}" style="color: #007BBD" target="_blank">${privacyRequestEmail}</a></p>

<#if dpoName?has_content && dpoEmail?has_content>
  <h4><a id="gdpr-data-protection-officer-information">Data Protection Officer information</a></h4>
  <p>${dpoName}</p>
  <p><a href="mailto:${dpoEmail}" style="color: #007BBD" target="_blank">${dpoEmail}</a></p>
</#if>
