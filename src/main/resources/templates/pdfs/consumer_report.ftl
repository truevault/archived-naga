<header class="page_header">
  <h1 class="page_header__title">Consumer History</h1>
  <h2 class="page_header__subtitle">${consumerEmail}</h2>
</header>

<main>
  <#list requests as request>
    <#assign hasSubjectName = request.subjectFirstName?? && request.subjectLastName??>
    <#assign subjectName = hasSubjectName?then("${request.subjectFirstName} ${request.subjectLastName}", "Website Visitor")>

    <article class="data_request_entry">
      <header class="data_request_entry__header">
        <time class="data_request_entry__request_date">${request.requestDate?datetime.iso?date}</time>

        <h4 class="data_request_entry__title">${request.dataRequestType.name}</h4>
        <#if hasSubjectName>
          <h5 class="data_request_entry__subtitle">
            Consumer Name: <span class="data_request_entry__subject_name">${subjectName}</span>
          </h5>
        </#if>

        <h5 class="data_request_entry__subtitle">
          Request ID: <code class="data_request_entry__request_id">${request.id}</code>
        </h5>
      </header>

      <section class="data_request_entry__body">
        <dl class="data_request_entry__events">
          <#list request.events as event>
            <#if event.isUserFacingEvent() == false>
              <#continue>
            </#if>

            <#assign actorName = event.isRequesterAction()?then(subjectName, "${event.actor.firstName} ${event.actor.lastName!}")>

            <dt class="data_request_entry__event_title">
              ${event.eventTypeTitle} by ${actorName} on ${event.eventDate?datetime.iso?date}
            </dt>

            <dd class="data_request_entry__event_details">
              <p>${event.eventTypeDescription}</p>

              <#if event.message?? && event.eventType != "CREATED">
                <p><strong>Message:</strong> ${event.message}</p>
              </#if>
            </dd>
          </#list>
        </dl>
      </section>
    </article>
  </#list>
</main>
