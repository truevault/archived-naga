<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <link
          href="https://fonts.googleapis.com/css?family=Roboto+Mono:300,400|Roboto:300,400,500,700,400italic,500italic,700italic,900black&display=swap"
          rel="stylesheet"
        />

        <link href="${cssPath}" rel="stylesheet" />

        <style>
            body, html {
                font-family: Roboto;
            }
        </style>
    </head>
    <body>
        <img class="logo" src="${logoPath}" />
        ${body}
    </body>
</html>
