<#macro number_card number="00" label="items" color="red" cardLink="#" polarisDomain="">
  <style>
    .number_card {
      height:  70px;
    }

    .number_card__number_container {
      vertical-align: middle;
      width: 50%;
      text-align: right;
      padding-right:  10px;
    }

    .number_card__number {
      font-size:  36px;
    }

    .number_card__label_container {
      vertical-align: middle;
      width: 50%;
      line-height: 1.125;
    }

    .number_card__label {
      font-size: 14px;
      line-height: 1;
    }

    .number_card__arrow_container {
      padding-left:  5px;
      width:  24px;
      text-align: right;
      vertical-align: middle;
    }

    .number_card__arrow_container svg {
      vertical-align: middle;
      display: inline-block;
    }

    .number_card__link {
      text-decoration: none;
      color: inherit;
    }

    @media only screen and (max-width:600px) {
      .number_card__number_container {
        width:  auto !important;
        text-align:  left !important;
      }

      .number_card__label_container {
        width:  auto !important;
        text-align:  left !important;
      }
    }
  </style>

  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="bg-${color} rounded number_card w-100 margin-b-sm">
    <tr>
      <td class="padding-sm padding-xs-">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="w-100">
          <tr>
            <td class="number_card__number_container">
              <span class="number_card__number ${color}">
                <a class="number_card__link" href="${cardLink}">${number}</a>
              </span>
            </td>
            <td class="number_card__label_container">
              <span class="number_card__label text-${color}-dark">
                <a class="number_card__link" href="${cardLink}">${label}</a>
              </span>
            </td>

            <td class="number_card__arrow_container ${color}">
              <a class="number_card__link" href="${cardLink}">
                <img src="${polarisDomain}/assets/images/email_templates/weekly_digest/right_arrow-${color}.png" width="24" height="24" />
              </a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</#macro>

<#macro link_row label="Test" subtitle="due 3 days ago" color="red" rowLink="#" polarisDomain="">
  <style>
    .link_row {
      height:  40px;
    }

    .link_row__label_container {
      vertical-align: middle;
      width: 80%;
      text-align: left;
      padding-right:  10px;
    }

    .link_row__label {
      font-size:  16px;
    }

    .link_row__subtitle_container {
      vertical-align: middle;
      width: 20%;
      text-align: right;
      line-height: 1.125;
    }

    .link_row__subtitle {
      font-size: 14px;
      line-height: 1;
    }

    .link_row__arrow_container {
      padding-left:  5px;
      width:  24px;
      text-align: right;
      vertical-align: middle;
    }

    .link_row__arrow_container svg {
      vertical-align: middle;
      display: inline-block;
    }

    .link_row__link {
      text-decoration: none;
      color: inherit;
    }

    @media only screen and (max-width:450px) {
      .link_row__subtitle {
        display:  none !important;
      }
    }
  </style>


  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="bg-${color} rounded link_row w-100 margin-b-sm">
    <tr>
      <td class="padding-sm">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="w-100">
          <tr>
            <td class="link_row__label_container">
              <span class="link_row__label ${color}">
                <a class="link_row__link" href="${rowLink}">${label}</a></span>
            </td>
            <td class="link_row__subtitle_container">
              <span class="link_row__subtitle text-${color}-dark">
                <a class="link_row__link" href="${rowLink}">${subtitle}</a>
              </span>
            </td>

            <td class="link_row__arrow_container ${color}">
              <a class="link_row__link" href="${rowLink}">
                <img src="${polarisDomain}/assets/images/email_templates/weekly_digest/right_arrow-${color}.png" width="24" height="24" />
              </a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</#macro>
