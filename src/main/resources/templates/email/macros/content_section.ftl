<#macro content_section content="">
    <table border="0" cellpadding="0" cellspacing="0" align="center" class="content container" width="100%" role="presentation">
        <tbody>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="presentation">
                        <tr>
                            <td class="padding">
                                ${content}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</#macro>
