<p>Hello there,</p>

<p>
  You recently requested to reset your password for your TrueVault account. Use the button
  below to reset it. <b>This password reset is only valid for the next 24 hours.</b>
</p>

<div class="text-center w-100 margin-v-lg">
  <a href="${resetURL}" class="button">
    Reset your password
  </a>
</div>

<p>
  For security, this request was received from IP address ${ipAddress}. If you did not request a
  password reset, please ignore this email or reach out to your IT team.
</p>

<p class="small">
  If you’re having trouble with the button above, copy and paste the URL below into your web
  browser: <a href="${resetURL}">${resetURL}</a>
</p>
