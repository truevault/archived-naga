<tr>
    <td class="padding">
        <p>A new message was received in your privacy inbox ${email}.</p>
        <ul>
            <li>From: ${from}</li>
            <li>Subject: ${subject}</li>
        </ul>
    </td>
</tr>
<#if mineParseError == true>
<tr>
    <td class="padding">
        <p>
            <strong>Attention:</strong>
            We could not auto-create a privacy request from this Mine email due to a parsing issue.
            Please manually create the request in your Polaris inbox by clicking the "+ New Request" button
            and entering the details.
        </p>
    </td>
</tr>
</#if>

