<#import "macros/content_section.ftl" as custom>

<#assign body>
    <@custom.content_section content=body />
</#assign>

<#include "base-empty.ftl">
