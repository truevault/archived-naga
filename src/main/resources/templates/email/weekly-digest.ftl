<#import "macros/content_section.ftl" as custom>
<#import "macros/number_card.ftl" as nc>

<#assign top_section>
  <h3 class="h5">Privacy Requests</h3>

  <table class="row" border="0" cellspacing="0" cellpadding="0" width="100%" role="presentation">
    <tr>
      <td class="col-6">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td class="padding-xs-0 padding-sm-0 padding-r-sm">
              <@nc.number_card number=overdueCount label="Overdue requests" color="red" cardLink=overdueLink polarisDomain=polarisDomain />
            </td>
          </tr>
        </table>
      </td>

      <td class="col-6">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td class="padding-xs-0 padding-sm-0 padding-h-sm">
              <@nc.number_card number=openCount label="Open requests" color="blue" cardLink=openLink polarisDomain=polarisDomain />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="col-6">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td class="padding-xs-0 padding-sm-0 padding-r-sm">
              <@nc.number_card number=pendingCount label="Pending requests" color="gray" cardLink=pendingLink polarisDomain=polarisDomain />
            </td>
          </tr>
        </table>
      </td>

      <td class="col-6">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td class="padding-xs-0 padding-sm-0 padding-l-sm">
              <@nc.number_card number=autoclosedCount label="Automatically closed" color="yellow" cardLink=closedLink polarisDomain=polarisDomain />
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <br/>

  <table class="row" border="0" cellspacing="0" cellpadding="0" width="100%" role="presentation">
    <tr>
      <td class="col-12 small text-muted">
        <p>You can make a one-time extension for Requests to Delete and Requests to Know in Polaris if you need additional time.</p>
      </td>
    </tr>
  </table>
</#assign>

<#assign bottom_section>
  <h3 class="h5">Open Tasks</h3>

  <table class="row" border="0" cellspacing="0" cellpadding="0" width="100%" role="presentation">
    <tr>
      <td>
        <#if tasks?has_content>
          <#list tasks as task>
            <@nc.link_row label=task.label subtitle=task.due color=task.color rowLink=task.link polarisDomain=polarisDomain />
          </#list>
        <#else>
          <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="presentation">
            <tr>
              <td class="text-center">
                <h4>No open tasks. Woohoo&nbsp;🙌</h4>
              </td>
            </tr>
          </table>
        </#if>
      </td>
    </tr>
  </table>
</#assign>

<#assign email_section>
  <#assign autoEmails=receivedEmails?filter(x -> x.createdPrivacyRequest)>
  <#assign openEmails=receivedEmails?filter(x -> !x.createdPrivacyRequest)>

  <h3 class="h5">New  Privacy Emails</h3>

  <table class="row" border="0" cellspacing="0" cellpadding="0" width="100%" role="presentation">
    <tr>
      <td>
        <#if receivedEmails?has_content>
          <p>
            <strong>${organizationName}</strong> received <strong>${receivedEmails?size}</strong> new
            ${(receivedEmails?size == 1)?then('email', 'emails')} to <strong>${privacyEmailAddress!"your privacy inbox"}</strong> this week. ${autoEmails?size}
            ${(autoEmails?size == 1)?then('was automatically turned into a privacy request', 'were automatically turned into privacy requests')}.
          </p>

          <#if autoEmails?has_content>
            <ul>
              <#list autoEmails as email>
                <li>
                  <strong>${email.receivedDate}</strong>: ${email.subject}
                </li>
              </#list>
            </ul>
          </#if>

          <p>The other ${openEmails?size} ${(openEmails?size == 1)?then('is', 'are')} listed below:</p>

          <#if openEmails?has_content>
            <ul>
              <#list openEmails as email>
                <li>
                  <strong>${email.receivedDate}</strong>: ${email.subject}
                </li>
              </#list>
            </ul>
          </#if>
        <#else>
          <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="presentation">
            <tr>
              <td class="text-center">
                <h4>No new privacy emails.</h4>
              </td>
            </tr>
          </table>
        </#if>
      </td>
    </tr>
  </table>
</#assign>

<table border="0" cellpadding="0" cellspacing="0" align="center" class="container margin-b-lg" width="600" role="presentation">
  <tr>
    <td>
      <h2 class="h4">👋&nbsp;${recipientName}, here’s the weekly snapshot for ${organizationName}</h2>
    </td>
  </tr>
</table>

<div class="margin-b-lg">
  <@custom.content_section content=top_section />
</div>

<div class="margin-b-lg">
  <@custom.content_section content=bottom_section />
</div>

<div class="margin-b-lg">
  <@custom.content_section content=email_section />
</div>


