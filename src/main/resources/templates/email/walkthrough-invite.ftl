<p>Hello,</p>
<p>Looks like ${org_name} received its first privacy request. You may now schedule a call with us to help with the processing of this request. During the call, a member from our product team will be on standby should you have any questions or concerns while processing the request.</p>
<p>You may schedule the call <a href='https://calendly.com/truevault/privacy-request-walkthrough'>here</a>.</p>
