<p>Before we can process your ${type}, we need to confirm this is you. Click below to verify your email address. If you don’t click the link within ${dueDateDays} days, your request will be closed.</p>

<div class="text-center w-100 margin-v-lg">
  <a href="${confirmationUrl}" class="button">
    Verify your email address
  </a>
</div>

<p>${orgName} Privacy Team</p>
