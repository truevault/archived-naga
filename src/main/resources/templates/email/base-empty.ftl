<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <#include "partials/stylesheet.ftl">
    </head>

    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

        <!-- WRAPPER -->
        <table id="wrapper" border="0" cellspacing="0" cellpadding="0" width="100%" height="100%" role="presentation">
            <tbody>
                <tr>
                    <td class="padding-xs-sm padding-sm-sm padding-0">
                        <br/>
                        <!-- HEADER -->

                        <table border="0" cellpadding="0" cellspacing="0" align="center" id="header" class="container" width="600" role="presentation">
                            <tbody>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" align="center" id="header" class="row" width="600" role="presentation">
                                            <tbody>
                                                <tr>
                                                    <td class="col-6 text-sm-center text-xs-center" style="vertical-align: middle;">
                                                        <#include "partials/truevault_logo.ftl">
                                                    </td>
                                                    <td class="col-6 text-sm-center text-xs-center text-right" style="vertical-align: middle;">
                                                        <#if date?has_content>
                                                            <span class="secondary text-muted">${date}</span>
                                                        </#if>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br/>

                        <!-- CONTENT -->
                        ${body}
                        <!-- END CONTENT -->

                        <br/>

                        <#include "partials/truevault_footer.ftl">
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- END WRAPPER -->


        <#if after??>
        ${after}
        </#if>
    </body>
</html>
