<p>Hi ${(firstName)!"there"},</p>

<p>
  We need additional time to process your ${type} and may need up to an additional ${extensionDays} days
  (for a total of 90 days from the date you submitted your request).<br/>
  <br/>
  We require additional time in order to ${noticeMsg}.

</p>

<br/>

<p>
  Thank you,
  <br/>
  <br/>
  <#noautoesc>
  ${signature}
  </#noautoesc>
</p>
