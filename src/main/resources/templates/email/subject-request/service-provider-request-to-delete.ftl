<p>
  We have received a ${type} from a consumer whose personal information
  is stored in your system. We need your help to fulfill our legal obligation to
  delete the personal information associated with the consumer. Please delete personal
  information associated with the person below.
</p>

<p>
  Consumer name and email address:
  <br />
  ${fullName}
  <br />
  ${subjectEmailAddress}
</p>
