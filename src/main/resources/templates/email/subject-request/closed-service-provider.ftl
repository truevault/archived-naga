<#import "../macros/custom-text-macro.ftlh" as custom>

<p>Hi ${(firstName)!"there"},</p>

<p>
  We received your ${type}. However, we cannot act on your request because we process your
  information only in our role as a service provider to another organization. You can
  exercise your rights by directing your request to the organization with whom you have a
  direct relationship.
</p>

<#if isGdpr>
  <p> 
    You have a right to lodge a complaint with a supervisory authority. For more information,
    you can visit the Information Commissioner’s Office website at https://ico.org.uk/, or see
    a list of EU Data Protection Authorities at https://www.gdprregister.eu/gdpr/dpa-gdpr/.
  <p>
</#if>

<@custom.text text=customText/>

<p>
  Thank you,
  <br/>
  <br/>
    <#noautoesc>
  ${signature}
  </#noautoesc>
</p>
