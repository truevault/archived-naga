<p>
  We have received a ${regulationName} ${longType} from a consumer
  whose personal information may be stored in your system.
  Please limit the use of this consumer's Sensitive Personal Information
  to uses allowed under the CCPA, including where necessary to
  provide products or services requested by the consumer.
</p>

<p>
  Consumer name and email address:
  <br />
  ${fullName}
  <br />
  ${subjectEmailAddress}
</p>
