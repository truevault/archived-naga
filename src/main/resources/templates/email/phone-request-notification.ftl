<p>Hello,</p>
<p>A CCPA privacy request was submitted to your organization via your toll-free number.</p>

<table>
    <tbody>
        <tr>
            <th class="padding-h-sm" scope="row">Request Type</th>
            <td class="padding-h-sm">${requestType}</td>
        </tr>
        <tr>
            <th class="padding-h-sm" scope="row">Date submitted</th>
            <td class="padding-h-sm">${dateSubmitted}</td>
        </tr>
        <tr>
            <th class="padding-h-sm" scope="row">Recording of consumer's name</th>
            <td class="padding-h-sm">
                <a href="${consumerNameRecording}">Listen</a>
            </td>
        </tr>
        <tr>
            <th class="padding-h-sm" scope="row">Recording of consumer's email</th>
            <td class="padding-h-sm">
                <a href="${consumerEmailRecording}">Listen</a>
            </td>
        </tr>
        <tr>
            <th class="padding-h-sm" scope="row">Consumer Phone Number</th>
            <td class="padding-h-sm">
                ${consumerPhoneNumber}
            </td>
        </tr>
    </tbody>
</table>
<br>
<p> You can manually add this privacy request to Polaris by clicking the <strong>New Request</strong> button in your <a href="${polarisDomain}/requests/active">Request Inbox</a>.</p>
