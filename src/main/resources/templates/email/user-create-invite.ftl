<p>Hello there,</p>

<p>
  You've been invited to use TrueVault Polaris. Use the button below to set up your account and get started:
</p>


<div class="text-center w-100 margin-v-lg">
  <a href="${inviteURL}" class="button">
    Set up account
  </a>
</div>

<p>Welcome aboard!</p>

<p class="small">
    If you&rsquo;re having trouble with the button above, copy and paste the URL below into your
    web browser: <a href="${inviteURL}">${inviteURL}</a>
</p>
