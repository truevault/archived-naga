<table
  border="0"
  cellpadding="0"
  cellspacing="0"
  align="center"
  id="footer"
  class="container text-center"
  width="600"
  role="presentation"
>
  <tbody>
    <tr>
      <td>
        <table
          border="0"
          cellpadding="0"
          cellspacing="0"
          class="row w-100"
          width="100%"
          role="presentation"
        >
          <tbody>
            <tr>
              <td class="col-12 text-center">
                <p>
                  If you have any questions, email us at
                  <a href="mailto:help@truevault.com">help@truevault.com</a>
                </p>
                <br />
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table
          border="0"
          cellpadding="0"
          cellspacing="0"
          class="row w-100"
          width="100%"
          role="presentation"
        >
          <tbody>
            <tr>
              <td class="col-2">&nbsp;</td>
              <td class="col-8 secondary text-center text-muted">
                <p>
                  This email is sent to users of TrueVault Polaris.<br/>
                  Made by TrueVault, Inc.<br/>
                  2261 Market Street #4949, San Francisco, CA 94114
                </p>
              </td>
              <td class="col-2">&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
