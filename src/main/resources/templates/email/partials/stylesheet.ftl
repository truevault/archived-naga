<style type="text/css">
  /**
   * CSS Reset - Starter Styles Which Help Improve Consistency Across Platforms
   */

  #outlook a{padding:0}body,html{height:100%}body{width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}#backgroundTable{width:100%!important;line-height:100%!important;margin:0;padding:0}img{outline:0;text-decoration:none;-ms-interpolation-mode:bicubic;vertical-align:middle}a img{border:none}.image_fix{display:block}table td{border-collapse:collapse;vertical-align:top}table{border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;}#wrapper,body,html{background-color:#FFF;width:100%}body,html,table{text-align:left}.ExternalClass,blockquote,label,p,pre{margin:0 0 1em}

  /* Core Styles */

  html, body, #wrapper {
      background-color: #f5f7fb;
      font-size: 16px;
  }

  html, body, table {
      font-family: helvetica, arial, sans-serif;
      line-height: 1.625;
      color: #1a1726;
  }

  .container {
      width: 600px;
      margin: 0 auto;
      text-align: left;
  }

  .row {
      mso-table-lspace:0;
      mso-table-rspace:0;
      width: 100%;
  }

  /* Spacing Reset */

  p, blockquote, ul, ol, dl, table, pre { margin-top: 0; margin-bottom: 1em; }
  p:last-child,
  ul:last-child,
  ol:last-child,
  dl:last-child { margin-bottom: 0; }

  hr, .hr {
      display: block;
      height: 1px;
      background: none;
      border-top: solid 1px #ddd;
      width:100%;
      margin: 1.5em 0;
  }

  /* Headings */

  h1, h2, h3, h4, h5, h6,
  .h1, .h2, .h3, .h4, .h5, .h6 {
      font-family: inherit;
      font-weight: 500;
      line-height: 1.1;
      color: inherit;
      margin-top: 1em;
      margin-bottom: 15px;
  }

  h1:first-child, h2:first-child, h3:first-child, h4:first-child, h5:first-child, h6:first-child,
  .h1:first-child, .h2:first-child, .h3:first-child, .h4:first-child, .h5:first-child, .h6:first-child {
      margin-top: 0;
  }

  h1, .h1 { font-size: 2.51em; }
  h2, .h2 { font-size: 2.15em; }
  h3, .h3 { font-size: 1.71em; }
  h4, .h4 { font-size: 1.28em; }
  h5, .h5 { font-size: 1em; }
  h6, .h6 { font-size: .85em; }

  /* Links */

  a, a:active, a:visited         { color: #007bbd; }
  h1 a,h2 a,h3 a,h4 a,h5 a,h6 a  { color: #007bbd; }

  /* Alignment Classes */

  .text-left   { text-align: left; }
  .text-right  { text-align: right; }
  .text-center { text-align: center;}

  .align-top    { vertical-align: top; }
  .align-bottom { vertical-align: bottom; }
  .align-middle { vertical-align: middle; }

  /* Blockquote */

  blockquote {
      padding: 12px 24px;
      margin: 0 0 20px;
      border-left: 2px solid #EEE;
  }

  blockquote.blockquote-reverse {
      padding-right: 12px;
      padding-left: 0;
      border-right: 2px solid #EEE;
      border-left: 0;
      text-align: right;
  }

  blockquote p:last-child {
      margin-bottom: 0;
  }

  /* List Classes */

  .list-unstyled {
      margin: 0;
      padding: 0;
      list-style-type: none;
  }

  dt {
      font-weight: 700;
  }

  dd {
      margin-left: 0;
  }

  /* Code and Pre */

  pre, code {
      font: .85em Monaco, "Courier New", "DejaVu Sans Mono", "Bitstream Vera Sans Mono", monospace;
      border: 0;
  }

  pre {
      margin: 0;
      margin-bottom: 1.5em;
      background-color: #eee;
      padding: 1em;
      color: #666;
      overflow: auto;
  }

  code {
      background-clip: padding-box;
      background-color: #eee;
      color: #666;
      padding: .125em .25em;
      border-radius: 3px;
  }

  /* Tables */

  table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
  table {
      border-collapse: collapse;
      border-spacing: 0;
      padding: 0;
      margin: 0;
  }

  .table {
      width: 100%;
      background-color: transparent;
  }

  .table > thead > tr > th,
  .table > tbody > tr > th,
  .table > tfoot > tr > th,
  .table > thead > tr > td,
  .table > tbody > tr > td,
  .table > tfoot > tr > td {
      padding: 8px;
      vertical-align: top;
      border-top: 1px solid #DDD;
  }

  .table-striped > tbody > tr:nth-child(odd) > td,
  .table-striped > tbody > tr:nth-child(odd) > th {
      background-color: #F9F9F9;
  }

  .table-bordered {
      border: 1px solid #DDD;
  }

  .table-condensed > thead > tr > th,
  .table-condensed > tbody > tr > th,
  .table-condensed > tfoot > tr > th,
  .table-condensed > thead > tr > td,
  .table-condensed > tbody > tr > td,
  .table-condensed > tfoot > tr > td {
      padding: 5px;
  }

  /* Utility Classes */

  .text-primary          { color: #5331C6; }
  .text-gray, .gray     { color: #726d83; }
  .text-muted, .muted     { color: #918ca1; }
  .text-success, .green  { color: #017F4D; }
  .text-info, .blue      { color: #007bbd; }
  .text-warning, .yellow { color: #F6E183; }
  .text-danger, .red     { color: #bd192c; }

  .text-weight-strong    { font-weight: strong }

  .text-red-dark { color: #52000a; }
  .text-blue-dark { color: #002e47; }
  .text-gray-dark { color: #1a1726; }

  .bg-primary             { background-color: #5331C6; color: white; }
  .bg-muted, .bg-gray     { background-color: #f5f4f9; }
  .bg-success, .bg-green  { background-color: #DFF0D8; }
  .bg-info, .bg-blue      { background-color: #effafe; }
  .bg-warning, .bg-yellow { background-color: #FCF8E3; }
  .bg-danger, .bg-red     { background-color: #ffebed; }
  .bg-none                { background-color: transparent !important; }

  .serif      { font-family: Georgia, "Times New Roman", Times, serif;}
  .sans-serif { font-family: Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif; }
  .monospace  { font-family: Menlo, Monaco, Consolas, "Courier New", monospace; }

  small, .small { font-size: 0.75em; }
  .secondary    { font-size: 0.85em; }
  .large        { font-size: 1.15em; }
  .xl           { font-size: 1.50em; }

  .padding-0    { padding: 0; }
  .padding      { padding: 24px; }
  .padding-sm   { padding: 12px; }
  .padding-lg   { padding: 48px; }
  .padding-v    { padding: 24px 0; }
  .padding-h    { padding: 0 24px; }
  .padding-v-sm { padding: 12px 0; }
  .padding-h-sm { padding: 0 12px }
  .padding-v-lg { padding: 48px 0; }
  .padding-h-lg { padding: 0 48px; }
  .padding-r { padding-right: 24px; }
  .padding-r-sm { padding-right: 12px; }
  .padding-r-lg { padding-right: 48px; }
  .padding-l { padding-left: 24px; }
  .padding-l-sm { padding-left: 12px; }
  .padding-l-lg { padding-left: 48px; }

  .margin      { margin: 24px; }
  .margin-sm   { margin: 12px }
  .margin-lg   { margin: 48px; }
  .margin-v    { margin: 24px 0; }
  .margin-h    { margin: 0 24px; }
  .margin-v-sm { margin: 12px0; }
  .margin-h-sm { margin: 0 12px }
  .margin-v-lg { margin: 48px 0; }
  .margin-h-lg { margin: 0 48px; }
  .margin-b { margin-bottom: 24px; }
  .margin-b-sm { margin-bottom: 12px; }
  .margin-b-lg { margin-bottom: 12px; }
  .margin-t { margin-top: 24px; }
  .margin-t-sm { margin-top: 12px; }
  .margin-t-lg { margin-top: 12px; }

  /* Media Styles */

  .col-1,
  .col-2,
  .col-3,
  .col-4,
  .col-5,
  .col-6,
  .col-7,
  .col-8,
  .col-9,
  .col-10,
  .col-11,
  .col-12 { min-height: 1px; }

  .col-1  { max-width: 8.333%; width: 8.333%; }
  .col-2  { max-width: 16.666%; width: 16.666%; }
  .col-3  { max-width: 25%; width: 25%; }
  .col-4  { max-width: 33.333%; width: 33.333%; }
  .col-5  { max-width: 41.666%; width: 41.666%; }
  .col-6  { max-width: 50%; width: 50%; }
  .col-7  { max-width: 58.333%; width: 58.333%; }
  .col-8  { max-width: 66.666%; width: 66.666%; }
  .col-9  { max-width: 75%; width: 75%; }
  .col-10 { max-width: 83.333%; width: 83.333%; }
  .col-11 { max-width: 91.666%; width: 91.666%; }
  .col-12 { max-width: 100%; width: 100%; }

  .col-spacer { width: 16px }

  .w-100 { width: 100% }
  .w-75 { width: 75% }
  .w-50 { width: 50% }
  .w-25 { width: 25% }

  .h-100 { height: 100% }
  .h-75 { height: 75% }
  .h-50 { height: 50% }
  .h-25 { height: 25% }

  @media only screen and (max-width:600px) {
      table.container,
      table.row td.col-1,
      table.row td.col-2,
      table.row td.col-3,
      table.row td.col-4,
      table.row td.col-5,
      table.row td.col-6,
      table.row td.col-7,
      table.row td.col-8,
      table.row td.col-9,
      table.row td.col-10,
      table.row td.col-11,
      table.row td.col-12 {
          width: 100% !important;
          max-width: none !important;
      }

      table.row td.col-1,
      table.row td.col-2,
      table.row td.col-3,
      table.row td.col-4,
      table.row td.col-5,
      table.row td.col-6,
      table.row td.col-7,
      table.row td.col-8,
      table.row td.col-9,
      table.row td.col-10,
      table.row td.col-11,
      table.row td.col-12 {
          display: block;
          -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;
      }

      .padding-sm-0    { padding: 0 !important; }
      .padding-sm-sm   { padding: 12px !important; }
      .padding-sm-lg   { padding: 48px !important; }
      .padding-sm-v    { padding: 24px 0 !important; }
      .padding-sm-h    { padding: 0 24px !important; }
      .padding-sm-v-sm { padding: 12px 0 !important; }
      .padding-sm-h-sm { padding: 0 12px !important; }
      .padding-sm-v-lg { padding: 48px 0 !important; }
      .padding-sm-h-lg { padding: 0 48px !important; }
      .padding-sm-r    { padding-right: 24px !important; }
      .padding-sm-r-sm { padding-right: 12px !important; }
      .padding-sm-r-lg { padding-right: 48px !important; }
      .padding-sm-l    { padding-left: 24px !important; }
      .padding-sm-l-sm { padding-left: 12px !important; }
      .padding-sm-l-lg { padding-left: 48px !important; }

      .text-sm-left   { text-align: left !important; }
      .text-sm-right  { text-align: right !important; }
      .text-sm-center { text-align: center !important;}
  }

  @media only screen and (max-width:450px) {
      table.container,
      table.row td.col-1,
      table.row td.col-2,
      table.row td.col-3,
      table.row td.col-4,
      table.row td.col-5,
      table.row td.col-6,
      table.row td.col-7,
      table.row td.col-8,
      table.row td.col-9,
      table.row td.col-10,
      table.row td.col-11,
      table.row td.col-12 {
          width: 100% !important;
          max-width: none !important;
      }

      table.row td.col-1,
      table.row td.col-2,
      table.row td.col-3,
      table.row td.col-4,
      table.row td.col-5,
      table.row td.col-6,
      table.row td.col-7,
      table.row td.col-8,
      table.row td.col-9,
      table.row td.col-10,
      table.row td.col-11,
      table.row td.col-12 {
          display: block;
          -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;
      }


      h1, .h1 { font-size: 2.15em !important; }
      h2, .h2 { font-size: 1.85em !important; }
      h3, .h3 { font-size: 1.35em !important; }
      h4, .h4 { font-size: 1.15em !important; }
      h5, .h5 { font-size: .95em !important; }
      h6, .h6 { font-size: .85em !important; }

      .padding-xs-0    { padding: 0 !important; }
      .padding-xs-sm   { padding: 12px !important; }
      .padding-xs-lg   { padding: 48px !important; }
      .padding-xs-v    { padding: 24px 0 !important; }
      .padding-xs-h    { padding: 0 24px !important; }
      .padding-xs-v-sm { padding: 12px 0 !important; }
      .padding-xs-h-sm { padding: 0 12px !important; }
      .padding-xs-v-lg { padding: 48px 0 !important; }
      .padding-xs-h-lg { padding: 0 48px !important; }
      .padding-xs-r    { padding-right: 24px !important; }
      .padding-xs-r-sm { padding-right: 12px !important; }
      .padding-xs-r-lg { padding-right: 48px !important; }
      .padding-xs-l    { padding-left: 24px !important; }
      .padding-xs-l-sm { padding-left: 12px !important; }
      .padding-xs-l-lg { padding-left: 48px !important; }

      .text-xs-left   { text-align: left !important; }
      .text-xs-right  { text-align: right !important; }
      .text-xs-center { text-align: center !important;}
  }

  /* Custom Styles */
  .content {
      background-color: #fff;
  }

  .button {
    background-color:#5331c6;
    border-radius:3px;
    color:#ffffff;
    display:inline-block;
    line-height:40px;
    text-align:center;
    text-decoration:none;
    min-width:  140px;
    padding-left: 12px;
    padding-right: 12px;
    -webkit-text-size-adjust:none;
  }

  .rounded {
    border-radius:  3px;
  }
</style>
