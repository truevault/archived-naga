<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <#include "partials/stylesheet.ftl">
    </head>

    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <!-- WRAPPER -->
        <table id="wrapper" border="0" cellspacing="0" cellpadding="0" width="100%" height="100%" role="presentation">
            <tbody>
                <tr>
                    <td class="padding-xs-sm padding-sm-sm padding-0">
                        <br/>

                        <!-- CONTENT -->
                        <table border="0" cellpadding="0" cellspacing="0" align="center" id="content" class="content container" width="600" role="presentation">
                            <tbody>
                                <tr>
                                    <td>
                                        <table class="row" border="0" cellspacing="0" cellpadding="0" width="100%" role="presentation">
                                            <tr>
                                                <td class="col-2">&nbsp;</td>
                                                <td class="col-8 padding text-center">
                                                    <img style="max-height: 100px; display: inline-block;" src="${headerImageUrl}">
                                                </td>
                                                <td class="col-2">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <table class="row" border="0" cellspacing="0" cellpadding="0" role="presentation">
                                            <tr>
                                                <td class="col-12 padding">
                                                    ${body}
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- END CONTENT -->

                        <br/>

                        <table border="0" cellpadding="0" cellspacing="0" align="center" id="footer" class="container secondary text-muted text-center" width="600" role="presentation">
                            <tbody>
                                <tr>
                                    <td class="col-2">&nbsp;</td>
                                    <td class="col-8">
                                        <p>
                                            ${physicalAddress}
                                        </p>
                                    </td>
                                    <td class="col-2">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- END WRAPPER -->

        <#if after??>
        ${after}
        </#if>
    </body>
</html>
