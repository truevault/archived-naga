<p>Hello,</p>

<p>
  Your privacy email account (${privacyEmail}) is disconnected from Polaris.
</p>

<p class="text-weight-strong"><strong>Please reconnect your email account in order to receive privacy requests.</strong></p>


<div class="text-center w-100 margin-v-lg">
  <a href="${reconnectUrl}" class="button">
    Reconnect Email
  </a>
</div>
