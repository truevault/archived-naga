<tr>
  <td>
    <table style="border-collapse: collapse">
      <tr style="border-bottom: 1px solid black; font-weight: 500">
        <th>
          Question
        </th>
        <th>
          Answer
        </th>
      </tr>
      <#list questions as question>
        <tr style="border-bottom: 1px solid black">
            <td style="padding: 4px">
                ${question.question}
            </td>
            <td style="padding: 4px">
                ${question.answer}
            </td>
        </tr>
      </#list>
    </table>
  </td>
</tr>