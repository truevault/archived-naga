<p style="margin-bottom: 0px;">${messageText}</p>

<div class="text-center w-100 margin-v-lg">
  <a href="${requestURL}" class="button">
    View Request
  </a>
</div>

<hr class="margin-v-lg" />

<table class="small">
    <tbody>
        <tr>
            <td style="text-align: right; font-weight: bold;">
                Request ID
            </td>
            <td rowspan="99" width="3"></td>
            <td>
                ${requestID}
            </td>
        </tr>
        <tr>
            <td style="text-align: right; font-weight: bold;">
                Request Type
            </td>
            <td>
                ${requestType}
            </td>
        </tr>
        <tr>
            <td style="text-align: right; font-weight: bold;">
                Request Date
            </td>
            <td>
                ${requestDate}
            </td>
        </tr>
        <tr>
            <td style="text-align: right; font-weight: bold;">
                Due Date
            </td>
            <td>
                ${dueDate}
            </td>
        </tr>

        <#if source?has_content>
            <tr>
                <td style="text-align: right; font-weight: bold;">
                    Source
                </td>
                <td>
                    ${source}
                </td>
            </tr>
        </#if>

        <tr>
            <td style="text-align: right; font-weight: bold;">
                Organization
            </td>
            <td>
                ${organization}
            </td>
        </tr>
    </tbody>
</table>
