-- POLARIS-2594
ALTER TABLE data_subject_request ADD CONSTRAINT fk_duplicates_request_id FOREIGN KEY (duplicates_request_id) REFERENCES data_subject_request (id);
