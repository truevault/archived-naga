ALTER TABLE data_subject_type
  ADD COLUMN verification_progress VARCHAR DEFAULT 'NOT_STARTED',
  ADD COLUMN is_verifiable BOOLEAN NOT NULL DEFAULT TRUE,
  ADD COLUMN verification_personal_information_category_id UUID,
  ADD COLUMN verification_instruction VARCHAR;
