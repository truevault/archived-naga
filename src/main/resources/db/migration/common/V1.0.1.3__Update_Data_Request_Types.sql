ALTER TABLE data_request_type
  ADD COLUMN polaris_submit_visible BOOLEAN NOT NULL DEFAULT TRUE;

update data_request_type
  set name = 'Request to Know',
    slug = 'RIGHT-TO-KNOW',
    polaris_submit_visible = true,
    consumer_submit_visible = true
  where slug = 'RIGHT-TO-KNOW-SPECIFIC';

update data_request_type
  set name = 'Request to Know (Categories)',
    polaris_submit_visible = false,
    consumer_submit_visible = false
  where slug = 'RIGHT-TO-KNOW-CATEGORIES';

