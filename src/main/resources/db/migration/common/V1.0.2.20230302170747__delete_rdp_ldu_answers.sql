--POLARIS-2414 (Bugfix-2512) -- Deleting answers related to LDU and RDP since even if we bring these questions back, the terms will likely have changed and need reanswering

DELETE FROM organization_survey_question WHERE slug = 'facebook-ldu' or slug = 'google-rdp';
