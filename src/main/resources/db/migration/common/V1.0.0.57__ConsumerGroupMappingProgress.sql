ALTER TABLE data_subject_type ADD COLUMN mapping_progress VARCHAR;

-- Set all current org types to DONE
UPDATE data_subject_type SET mapping_progress = 'DONE';

ALTER TABLE data_subject_type ALTER COLUMN mapping_progress SET NOT NULL;
