-- POLARIS-2176: Add consumer context to track employee vs consumer data recipients

ALTER TABLE organization_data_recipient ADD COLUMN consumer_context VARCHAR(255);

CREATE INDEX organization_data_recipient_consumer_context_idx ON organization_data_recipient(consumer_context);
