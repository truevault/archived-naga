UPDATE business_purpose SET
  name = 'Provide Products or Services',
  description = 'Customer service, for example'
WHERE name = 'Provide Goods or Services';

UPDATE business_purpose SET
  description = 'Advertising or other promotion of your product/service'
WHERE name = 'Sales and Marketing';

UPDATE business_purpose SET
  name = 'Error Management',
  description = 'Debugging, diagnostics, crash reports, etc.'
WHERE name = 'Identify and Repair Errors';

UPDATE business_purpose SET
  name = 'Quality Assurance',
  description = 'Improving the quality or safety of your product/service'
WHERE name = 'Verify or Maintain the Quality of Goods or Services';

UPDATE business_purpose SET
  name = 'Security',
  description = 'Preventing fraudulent activity, etc.'
WHERE name = 'Detect and Prevent Security Incidents';

UPDATE business_purpose SET
  description = 'Technological development or demonstration'
WHERE name = 'Internal Research';

UPDATE business_purpose SET
  description = 'Displaying contextual ads or determining content to show, for example'
WHERE name = 'Short-Term Transient Use';

UPDATE business_purpose SET
  description = 'Counting ad impressions or unique visitors to your website, for example'
WHERE name = 'Audit Current Interactions';

