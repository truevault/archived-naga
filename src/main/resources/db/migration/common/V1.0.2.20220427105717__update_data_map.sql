ALTER TABLE personal_information_category
  ADD COLUMN pic_key VARCHAR;

ALTER TABLE personal_information_category_group RENAME TO pic_group;
ALTER TABLE pic_group
  ADD COLUMN group_key VARCHAR;

ALTER TABLE data_subject_type RENAME TO collection_group;
ALTER TABLE collection_group RENAME COLUMN type TO name;
ALTER TABLE collection_group
  ALTER COLUMN organization_id SET NOT NULL,
  DROP COLUMN inaccessible_records_description,
  DROP COLUMN privacy_center_enabled,
  DROP COLUMN internal_storage_is_accessible,
  DROP COLUMN internal_storage_is_delete;

-- RENAMES OF data_subject type ids
ALTER TABLE consumer_group_data_request_type RENAME COLUMN data_subject_type_id TO collection_group_id;
ALTER TABLE consumer_group_mapping_progress RENAME COLUMN data_subject_type_id TO collection_group_id;
ALTER TABLE consumer_group_organization_vendor RENAME COLUMN data_subject_type_id TO collection_group_id;
ALTER TABLE data_subject_request_consumer_groups RENAME COLUMN data_subject_type_id TO collection_group_id;
ALTER TABLE deprecated_organization_service_data_subject_type RENAME to legacy_organization_service_collection_group;
ALTER TABLE legacy_organization_service_collection_group RENAME COLUMN data_subject_type_id TO collection_group_id;
ALTER TABLE legacy_data_subject_request RENAME COLUMN data_subject_type_id TO collection_group_id;
ALTER TABLE processing_instructions RENAME COLUMN data_subject_type_id TO collection_group_id;

ALTER TABLE organization_data_subject_type_business_purpose RENAME TO collection_group_business_purpose;
ALTER TABLE collection_group_business_purpose RENAME COLUMN data_subject_type_id TO collection_group_id;
ALTER TABLE collection_group_business_purpose
  DROP COLUMN organization_id;

ALTER TABLE organization_data_subject_type_personal_information_category RENAME TO collection_group_collected_pic;
ALTER TABLE collection_group_collected_pic RENAME COLUMN data_subject_type_id to collection_group_id;
ALTER TABLE collection_group_collected_pic RENAME COLUMN personal_information_category_id to pic_id;
ALTER TABLE collection_group_collected_pic
  DROP COLUMN organization_id,
  DROP COLUMN is_stored_internally,
  DROP COLUMN is_accessible;

ALTER TABLE organization_service_personal_information_category RENAME TO data_recipient_disclosed_pic;
ALTER TABLE data_recipient_disclosed_pic RENAME COLUMN organization_data_subject_type_personal_information_category_id to collected_pic_id;
ALTER TABLE data_recipient_disclosed_pic
  DROP COLUMN exchange_type,
  DROP COLUMN is_retrieval_source;


ALTER TABLE vendor RENAME COLUMN recommended_personal_information_categories to recommended_pic_ids;