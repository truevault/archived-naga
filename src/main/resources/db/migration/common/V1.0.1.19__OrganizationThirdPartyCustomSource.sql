CREATE TABLE organization_custom_third_party_source (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id UUID,
  name VARCHAR,

  FOREIGN KEY (organization_id) REFERENCES organization (id),

  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,

  UNIQUE (organization_id, name)
  
);