ALTER TABLE collection_group
  ADD COLUMN collection_group_type VARCHAR,
  ADD COLUMN autocreation_slug VARCHAR;

-- migrate the existing collection group types
UPDATE collection_group cg
SET collection_group_type = UPPER(REPLACE(t.slug, '-', '_'))
FROM consumer_group_type t
WHERE t.id = cg.consumer_group_type_id;

UPDATE collection_group SET collection_group_type = 'BUSINESS_TO_CONSUMER' WHERE collection_group_type IS NULL;

ALTER TABLE collection_group ALTER COLUMN collection_group_type SET NOT NULL;
ALTER TABLE collection_group DROP COLUMN consumer_group_type_id;
DROP TABLE consumer_group_type;

-- Add the field to track all autocreations
ALTER TABLE organization ADD COLUMN autocreated_collection_group_slugs TEXT;

-- Some basic "consumer_group" -> "collection_group" cleanup
ALTER TABLE data_subject_request RENAME COLUMN consumer_group_result TO collection_group_result;
ALTER TABLE organization RENAME COLUMN request_processing_instructions_consumer_group TO request_processing_instructions_collection_group;
ALTER TABLE privacy_request_retention_reason RENAME COLUMN consumer_group_id TO collection_group_id;
ALTER TABLE request_handling_instruction_progress RENAME COLUMN consumer_group_progress TO collection_group_progress;

ALTER TABLE consumer_group_mapping_progress RENAME TO collection_group_mapping_progress;
ALTER TABLE consumer_group_data_request_type RENAME TO collection_group_data_request_type;
ALTER TABLE data_subject_request_consumer_groups RENAME TO data_subject_request_collection_groups;