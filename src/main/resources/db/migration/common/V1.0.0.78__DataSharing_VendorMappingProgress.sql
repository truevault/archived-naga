ALTER TABLE organization_service ADD COLUMN mapping_progress VARCHAR DEFAULT 'NOT_STARTED';

ALTER TABLE organization
  ADD COLUMN exceptions_mapping_progress VARCHAR DEFAULT 'NOT_STARTED',
  ADD COLUMN cookies_mapping_progress VARCHAR DEFAULT 'NOT_STARTED';

-- If an organization is on Data Selling move them to Data Sharing
UPDATE organization SET get_compliant_progress = 'DATA_SHARING' WHERE get_compliant_progress = 'DATA_SELLING';

-- Set any organization's progress for what was formerly data selling to "DONE" if they've moved past that step
UPDATE organization
  SET exceptions_mapping_progress = 'DONE', cookies_mapping_progress = 'DONE'
  WHERE get_compliant_progress IN ('PRIVACY_NOTICES', 'PRIVACY_REQUESTS', 'COMPLIANCE_CHECKLIST', 'COMPLETE');

ALTER TABLE consumer_group_organization_vendor DROP COLUMN mapping_progress;
