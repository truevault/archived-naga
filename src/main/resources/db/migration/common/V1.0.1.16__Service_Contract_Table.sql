CREATE TABLE vendor_contract (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  name VARCHAR(255) NOT NULL,
  slug VARCHAR(255) NOT NULL,
  display_order INTEGER,
  tooltip_text TEXT,
  contract_url TEXT,
  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE vendor_contract_reviewed (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  name VARCHAR(255) NOT NULL,
  slug VARCHAR(255) NOT NULL,
  display_order INTEGER,
  tooltip_text TEXT,
  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL
);

ALTER TABLE organization_service
  ADD COLUMN vendor_contract_id UUID DEFAULT NULL,
  ADD COLUMN vendor_contract_reviewed_id UUID DEFAULT NULL,
  ADD COLUMN public_tos_url VARCHAR(255) DEFAULT NULL,
  ALTER COLUMN contacted SET DEFAULT NULL,
  ADD CONSTRAINT fk_vendor_contract_id  FOREIGN KEY (vendor_contract_id) REFERENCES vendor_contract (id),
  ADD CONSTRAINT fk_vendor_contract_reviewed_id  FOREIGN KEY (vendor_contract_reviewed_id) REFERENCES vendor_contract_reviewed (id);

ALTER TABLE service
  ADD COLUMN vendor_contract_id UUID DEFAULT NULL,
  ADD COLUMN vendor_contract_reviewed_id UUID DEFAULT NULL,
  ADD CONSTRAINT fk_vendor_contract_id  FOREIGN KEY (vendor_contract_id) REFERENCES vendor_contract (id),
  ADD CONSTRAINT fk_vendor_contract_reviewed_id  FOREIGN KEY (vendor_contract_reviewed_id) REFERENCES vendor_contract_reviewed (id);


INSERT INTO vendor_contract(slug, name, display_order, tooltip_text) VALUES
  ('found-contract', 'I found our contract', 1, 'I found our contract'),
  ('use-public', 'Use public terms of service', 2, 'Use public terms of service');

INSERT INTO vendor_contract_reviewed(slug, name, display_order, tooltip_text) VALUES
  ('provider-language-found', 'Service provider language found', 1, 'Service provider language found'),
  ('provider-language-not-found', 'Service provider language not found', 2, 'Service provider language not found');