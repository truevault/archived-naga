-- POLARIS-2612 - add new processing activity attributes

ALTER TABLE default_processing_activity
  ADD COLUMN display_order INTEGER,
  -- indicates that this activity should no longer be shown in the UI unless it was previously selected
  ADD COLUMN deprecated BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN help_text TEXT;


COMMENT ON COLUMN default_processing_activity.deprecated IS 'Indicates that this activity should no longer be shown in the UI unless it was previously selected';