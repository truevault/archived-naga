CREATE TABLE gdpr_cookie_consent_tracking (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id UUID NOT NULL,
  ip_v4 VARCHAR,
  ip_v6 VARCHAR,
  source_url TEXT NOT NULL,
  user_agent TEXT,
  key TEXT NOT NULL UNIQUE,
  consent_state TEXT NOT NULL,
  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,

  FOREIGN KEY (organization_id) REFERENCES organization (id)
);