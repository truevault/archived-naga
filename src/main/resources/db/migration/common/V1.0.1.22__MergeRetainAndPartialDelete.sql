UPDATE request_handling_instructions
  SET processing_method = 'RETAIN'
  WHERE processing_method = 'PARTIAL_DELETE';
