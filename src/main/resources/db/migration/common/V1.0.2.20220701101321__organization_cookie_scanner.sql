CREATE TABLE organization_cookie_scan (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),

    organization_id UUID NOT NULL,

    status VARCHAR NOT NULL,
    scan_url VARCHAR NOT NULL,

    scan_started_at TIMESTAMP,
    scan_ended_at TIMESTAMP,

    scan_error TEXT,

    FOREIGN KEY (organization_id) REFERENCES organization (id)
);

CREATE TABLE organization_cookie_data_recipients(
    organization_id UUID NOT NULL,
    name VARCHAR NOT NULL,
    domain VARCHAR NOT NULL,
    data_recipient_id INTEGER NOT NULL,

    FOREIGN KEY (organization_id, name, domain) REFERENCES organization_cookie (organization_id, name, domain),
    FOREIGN KEY (data_recipient_id) REFERENCES organization_data_recipient (id),
    PRIMARY KEY (organization_id, name, domain, data_recipient_id)
);


ALTER TABLE organization_cookie ADD column categories TEXT;