-- POLARIS-2015 - Organization Drafts
CREATE TABLE organization_draft (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id UUID NOT NULL,
  schema_version VARCHAR(20) NOT NULL,
  data JSONB,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  last_updated_at TIMESTAMP WITH TIME ZONE,
  deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
  FOREIGN KEY (organization_id) REFERENCES organization(id)
);

CREATE INDEX idx_org_draft_on_schema_version ON organization_draft (schema_version);
CREATE INDEX idx_org_draft_on_organization_id ON organization_draft (organization_id);
