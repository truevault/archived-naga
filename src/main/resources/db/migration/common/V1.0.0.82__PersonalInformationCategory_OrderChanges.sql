UPDATE personal_information_category SET group_display_order = 1 WHERE name = 'Interactions with websites, apps or ads';
UPDATE personal_information_category SET group_display_order = 2 WHERE name = 'Email open and click-through rates';
UPDATE personal_information_category SET group_display_order = 3 WHERE name = 'Browsing history';
UPDATE personal_information_category SET group_display_order = 4 WHERE name = 'Search history';