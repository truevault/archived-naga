ALTER TABLE privacy_center
  ADD COLUMN dpo_officer_does_not_apply BOOL NOT NULL DEFAULT false;
