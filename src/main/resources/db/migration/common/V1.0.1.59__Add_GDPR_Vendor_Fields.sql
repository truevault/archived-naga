ALTER TABLE vendor
  ADD COLUMN gdpr_multi_purpose_vendor BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN gdpr_lawful_basis VARCHAR;