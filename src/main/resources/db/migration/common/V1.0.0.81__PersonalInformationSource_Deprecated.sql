ALTER TABLE personal_information_source ADD COLUMN deprecated BOOLEAN DEFAULT FALSE;

UPDATE personal_information_source SET deprecated = TRUE WHERE name in ('Other Data (Inference)', 'Consumer''s Employer');
