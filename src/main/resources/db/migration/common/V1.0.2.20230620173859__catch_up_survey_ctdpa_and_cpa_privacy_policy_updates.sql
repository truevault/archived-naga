-- POLARIS-2756 - Add more editable introductory sections
ALTER TABLE privacy_center
    ADD COLUMN cpa_privacy_notice_intro_text TEXT,
    ADD COLUMN ctdpa_privacy_notice_intro_text TEXT;

INSERT INTO privacy_center_custom_text (privacy_center_id, slug, value)
SELECT id as privacy_center_id, 'cpa-intro' as slug, cpa_privacy_notice_intro_text as value
FROM privacy_center
WHERE cpa_privacy_notice_intro_text IS NOT NULL;

INSERT INTO privacy_center_custom_text (privacy_center_id, slug, value)
SELECT id as privacy_center_id, 'ctdpa-intro' as slug, ctdpa_privacy_notice_intro_text as value
FROM privacy_center
WHERE ctdpa_privacy_notice_intro_text IS NOT NULL;
