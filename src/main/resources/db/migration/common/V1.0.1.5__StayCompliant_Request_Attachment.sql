ALTER TABLE data_subject_request
  ADD COLUMN attachment_id uuid,
  ADD CONSTRAINT data_subject_request_attachment_id_fkey FOREIGN KEY (attachment_id) REFERENCES public.attachment(id);