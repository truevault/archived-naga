-- Corrects compliance version on orgs who have completed the Q1 2023 Survey.
UPDATE organization
SET
	compliance_version = 'c2023_1',
	current_survey = NULL
WHERE id IN (
	SELECT
		o.id
	FROM organization o
	INNER JOIN organization_survey_progress osp
		ON osp.organization_id = o.id
	WHERE
		osp.survey_slug = 's2023_1' AND
		o.current_survey = 's2023_1' AND
		osp.current_step = 'Tasks' AND
		o.compliance_version = 'c2022_1'
);
