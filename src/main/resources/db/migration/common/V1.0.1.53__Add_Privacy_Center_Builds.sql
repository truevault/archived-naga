CREATE TABLE privacy_center_build (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  privacy_center_id UUID NOT NULL,
  requested_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  sent_build_message_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
  source VARCHAR(255),

  FOREIGN KEY (privacy_center_id) REFERENCES privacy_center (id)
);
