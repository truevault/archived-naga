-- POLARIS-1963: Update Personal Information Categories for CPRA/Virginia Laws
--
-- This updates categories which are not already considered sensitive to be
-- in line with the list provided in POLARIS-1963. The remaining categories
-- from the ticket are already considered sensitive in the database.

UPDATE personal_information_category SET is_sensitive = FALSE WHERE name IN (
 'Union membership',
 'Racial or ethnic origin',
 'Religious or philosophical beliefs',
 'Contents of mail, email or text messages',
 'Genetic data'
);
