DROP TABLE template;

CREATE TABLE request_template (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  name VARCHAR NOT NULL,
  slug VARCHAR NOT NULL,
  template VARCHAR NOT NULL,

  UNIQUE (slug),

  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE request_template_request_type (
  id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  request_template_id UUID NOT NULL,
  data_request_type_id UUID,

  FOREIGN KEY (request_template_id) REFERENCES request_template (id),
  FOREIGN KEY (data_request_type_id) REFERENCES data_request_type (id),

  UNIQUE (request_template_id, data_request_type_id)
);

CREATE TABLE request_template_request_state (
  id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  request_template_id UUID NOT NULL,
  request_state VARCHAR NOT NULL,

  FOREIGN KEY (request_template_id) REFERENCES request_template (id),

  UNIQUE (request_template_id, request_state)
);
