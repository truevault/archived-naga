CREATE TABLE processing_instructions (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id UUID,
  data_subject_type_id UUID NOT NULL,
  data_request_type VARCHAR NOT NULL,
  instruction VARCHAR NOT NULL,
  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  FOREIGN KEY (data_subject_type_id) REFERENCES data_subject_type (id),
  UNIQUE (data_subject_type_id, data_request_type)
);
