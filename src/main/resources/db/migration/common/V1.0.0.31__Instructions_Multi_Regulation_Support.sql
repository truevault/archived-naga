ALTER TABLE processing_instructions ALTER COLUMN data_request_type DROP NOT NULL;

UPDATE organization_regulation SET identity_verification_instruction = o.identity_verification_message
FROM organization o
  JOIN regulation r on r.name = 'CCPA'
WHERE organization_regulation.organization_id = o.id
  AND organization_regulation.regulation_id = r.id;
