UPDATE personal_information_source SET name = 'Consumer' WHERE name = 'Consumer (Direct)';

ALTER TABLE organization_data_subject_type_personal_information_source
  DROP CONSTRAINT organization_data_subject_typ_organization_id_data_subject_key1;

UPDATE organization_data_subject_type_personal_information_source AS opis
  SET personal_information_source_id = pis_new.id
FROM personal_information_source pis_old
  JOIN personal_information_source pis_new ON pis_new.name = 'Consumer'
WHERE pis_old.name = 'Consumer (Automatic)'
  AND pis_old.id = opis.personal_information_source_id;

DELETE FROM organization_data_subject_type_personal_information_source a
  USING organization_data_subject_type_personal_information_source b
WHERE a.organization_id = b.organization_id
  AND a.data_subject_type_id = b.data_subject_type_id
  AND a.personal_information_source_id = b.personal_information_source_id
  AND a.id < b.id;

CREATE UNIQUE INDEX organization_data_subject_typ_organization_id_data_subject_key1
  ON organization_data_subject_type_personal_information_source (
    organization_id, data_subject_type_id, personal_information_source_id
  );

ALTER TABLE personal_information_source ADD COLUMN display_order INT NOT NULL DEFAULT 0;
UPDATE personal_information_source SET display_order = 1 WHERE name = 'Consumer';
UPDATE personal_information_source SET display_order = 5 WHERE name = 'Data Broker';
UPDATE personal_information_source SET display_order = 6 WHERE name = 'Data Analytics Provider';
UPDATE personal_information_source SET display_order = 7 WHERE name = 'Ad Network';
UPDATE personal_information_source SET display_order = 8 WHERE name = 'Social Network';
UPDATE personal_information_source SET display_order = 9 WHERE name = 'Internet Service Provider';
UPDATE personal_information_source SET display_order = 10 WHERE name = 'Government Entity';
UPDATE personal_information_source SET display_order = 11 WHERE name = 'Operating System';

INSERT INTO personal_information_source (name, display_order) VALUES
  ('Other Users', 2),
  ('Other Customers', 3),
  ('Consumer''s Employer', 4),
  ('Other Data (inference)', 12);

DELETE FROM personal_information_source WHERE name = 'Consumer (Automatic)';

ALTER TABLE organization_data_subject_type_personal_information_source
  ADD COLUMN custom_name VARCHAR,
  ALTER COLUMN personal_information_source_id DROP NOT NULL;
