-- POLARIS-2022 - Migrate Collection Purposes to Organization Level instead of Collection Group Level
CREATE TABLE organization_business_purpose(
  organization_id UUID NOT NULL,
  business_purpose_id UUID NOT NULL,
  business_purpose_type VARCHAR(50) NOT NULL,

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  FOREIGN KEY (business_purpose_id) REFERENCES business_purpose (id),
  PRIMARY KEY (organization_id, business_purpose_id, business_purpose_type)
);

CREATE INDEX organization_business_purpose_organization_id_idx ON organization_business_purpose(organization_id);

-- Now migrate all old data
insert into organization_business_purpose(organization_id, business_purpose_id, business_purpose_type)
select cg.organization_id, cgbp.business_purpose_id, 'CONSUMER' as business_purpose_type
from collection_group_business_purpose cgbp
inner join collection_group cg on cg.id = cgbp.collection_group_id
where cg.collection_group_type != 'EMPLOYMENT' AND cgbp.deleted_at is null
group by cg.organization_id, cgbp.business_purpose_id;

insert into organization_business_purpose(organization_id, business_purpose_id, business_purpose_type)
select cg.organization_id, cgbp.business_purpose_id, 'EMPLOYMENT' as business_purpose_type
from collection_group_business_purpose cgbp
inner join collection_group cg on cg.id = cgbp.collection_group_id
where cg.collection_group_type = 'EMPLOYMENT' AND cgbp.deleted_at is null
group by cg.organization_id, cgbp.business_purpose_id;