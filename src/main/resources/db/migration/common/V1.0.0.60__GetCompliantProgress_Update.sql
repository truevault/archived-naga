ALTER TABLE organization ALTER COLUMN get_compliant_progress SET DEFAULT 'CONSUMER_GROUPS';
UPDATE organization SET get_compliant_progress = 'CONSUMER_GROUPS';
