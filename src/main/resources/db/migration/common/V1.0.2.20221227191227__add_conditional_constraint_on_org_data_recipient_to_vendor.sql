-- POLARIS-2282: Add uniqueness to prevent duplicate default vendors

CREATE UNIQUE INDEX organization_data_recipient_vendor_id_org_id_unq_idx ON organization_data_recipient(vendor_id, organization_id)
WHERE deleted_at IS NULL;

