-- POLARIS-1961 - CPRA + VA Updates: Purpose Mapping & Privacy Concepts

-- rename lawful_bases -> gdpr_lawful_bases;
ALTER TABLE processing_activity ADD COLUMN gdpr_lawful_bases TEXT;
UPDATE processing_activity SET gdpr_lawful_bases = lawful_bases;

ALTER TABLE processing_activity ADD COLUMN processing_regions TEXT;

-- rename mandatory_lawful_bases -> gdpr_mandatory_lawful_bases ; optional_lawful_bases -> gdpr_optional_lawful_bases
ALTER TABLE default_processing_activity
  ADD COLUMN gdpr_mandatory_lawful_bases TEXT,
  ADD COLUMN gdpr_optional_lawful_bases TEXT;
UPDATE default_processing_activity SET gdpr_mandatory_lawful_bases = mandatory_lawful_bases, gdpr_optional_lawful_bases = optional_lawful_bases;

ALTER TABLE default_processing_activity
  ADD COLUMN material_icon_key VARCHAR(100);

UPDATE processing_activity SET processing_regions = '["EEA_UK"]';

CREATE TABLE processing_activity_pic(
  processing_activity_id UUID NOT NULL,
  pic_id UUID NOT NULL,

  PRIMARY KEY (processing_activity_id, pic_id),
  FOREIGN KEY (processing_activity_id) REFERENCES processing_activity(id),
  FOREIGN KEY (pic_id) REFERENCES personal_information_category(id)
);