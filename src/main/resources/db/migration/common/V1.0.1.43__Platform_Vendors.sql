ALTER TABLE vendor
  ADD COLUMN is_platform BOOLEAN,
  ADD COLUMN is_standalone BOOLEAN;

UPDATE vendor SET is_platform = TRUE WHERE vendor_key = 'shopify';

CREATE TABLE platform_app_installation (
  platform_id INTEGER NOT NULL,
  app_id INTEGER NOT NULL,

  FOREIGN KEY (platform_id) REFERENCES organization_data_recipient (id),
  FOREIGN KEY (app_id) REFERENCES organization_data_recipient (id),

  PRIMARY KEY (platform_id, app_id)
);