ALTER TABLE organization_service ADD COLUMN contacted BOOLEAN DEFAULT FALSE;

INSERT INTO service_type (slug, name, tooltip_text, display_order)
  VALUES ('needs-review', 'Needs Review', 'Needs Review', 5);
