ALTER TABLE organization
  ADD COLUMN cookie_policy TEXT DEFAULT 'We use cookies to create a better experience for you on our site. For example, cookies prevent you from having to login repeatedly, and they help us remember items you''ve added to your cart. We also use third-party cookies, which are cookies placed by third parties for advertising and analytics purposes. You can control these cookies through your browser settings.';


