-- POLARIS-2267 Adding state to is_upload_vendor, so that the changes in stay_compliant are tracked
ALTER TABLE organization_data_recipient
    ADD COLUMN is_upload_vendor BOOLEAN;

UPDATE
    organization_data_recipient odr
SET is_upload_vendor = TRUE
FROM (SELECT Unnest(
               String_to_array(
                       Replace(Replace(Replace(answer, '"', ''), '[', ''), ']', ''), ','
                   ):: int[]
           ) AS upload_vendor_id,
       organization_id
FROM organization_survey_question
WHERE slug = 'vendor-upload-data-selection'
  AND answer != '["none"]') AS data_table
WHERE odr.organization_id = data_table.organization_id
  AND odr.id = data_table.upload_vendor_id
  AND is_upload_vendor = FALSE;