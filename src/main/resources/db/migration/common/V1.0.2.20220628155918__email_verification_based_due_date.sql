ALTER TABLE data_subject_request ADD COLUMN email_verified_date TIMESTAMP WITH TIME ZONE;
ALTER TABLE data_subject_request ALTER COLUMN request_due_date DROP NOT NULL;
ALTER TABLE data_subject_request ALTER COLUMN request_original_due_date DROP NOT NULL;
ALTER TABLE data_subject_request ALTER COLUMN request_type SET NOT NULL;