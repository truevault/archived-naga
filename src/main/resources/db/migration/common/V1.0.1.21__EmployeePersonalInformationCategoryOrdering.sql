ALTER TABLE personal_information_category_group
  ADD COLUMN employee_display_order INTEGER NOT NULL DEFAULT 0;

UPDATE personal_information_category_group
  SET employee_display_order = 1
  WHERE id = '894fdfa8-a251-40c2-bcfd-6d7b2a992159'; -- Personal Identifiers

UPDATE personal_information_category_group
  SET employee_display_order = 2
  WHERE id = 'c14e3a58-56dd-46bd-9ed8-7020f873d324'; -- Professional

UPDATE personal_information_category_group
  SET employee_display_order = 3
  WHERE id = 'f915c79c-a389-4653-82b2-2a53f13c33af'; -- Medical

UPDATE personal_information_category_group
  SET employee_display_order = 4
  WHERE id = 'f638f8ca-771b-4b21-9c50-3878f098c9e3'; -- Characteristics of Protected Classifications

UPDATE personal_information_category_group
  SET employee_display_order = 5
  WHERE id = '098cc93b-527b-4109-a7c8-804032cc3cd4'; -- Online Identifiers

UPDATE personal_information_category_group
  SET employee_display_order = 6
  WHERE id = 'fbc84cd9-37d7-473e-a134-3d6ae5360dd7'; -- Internet Activity

UPDATE personal_information_category_group
  SET employee_display_order = 7
  WHERE id = 'eafb566d-1d10-4ee3-8020-d78296ffedc2'; -- Commercial and Financial Information

UPDATE personal_information_category_group
  SET employee_display_order = 8
  WHERE id = '2853077e-4c57-4da4-b296-0a1f837dfb36'; -- Biometric

UPDATE personal_information_category_group
  SET employee_display_order = 9
  WHERE id = 'f757a822-e32f-44f4-b84f-f3247376c3ff'; -- Geolocation

UPDATE personal_information_category_group
  SET employee_display_order = 10
  WHERE id = '5042db51-9ab6-445c-971b-86e23fc15125'; -- Other Physical and Audio

UPDATE personal_information_category_group
  SET employee_display_order = 11
  WHERE id = '93af7008-25ce-4dde-bae9-b07db4569ba2'; -- Other Sensitive Data

UPDATE personal_information_category_group
  SET employee_display_order = 12
  WHERE id = 'ce680dfb-548e-4adf-b88e-f602bc96dfe5'; -- Inferences