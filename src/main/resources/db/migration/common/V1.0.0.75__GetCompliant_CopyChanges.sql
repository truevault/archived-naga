UPDATE personal_information_category
  SET description = 'Cookies, beacons, pixel tags, mobile ad identifiers, or similar technology used to collect, disclose, or sell personal information'
  WHERE name = 'Unique personal identifier';

UPDATE personal_information_category
  SET description = ''
  WHERE name = 'Age';

UPDATE business_purpose
  SET description = 'E.g., providing customer service related to a product or service'
  WHERE name = 'Provide Goods or Services';

UPDATE business_purpose
  SET description = 'E.g., advertising or other promotion of a product or service'
  WHERE name = 'Sales and Marketing';

UPDATE data_request_type SET name = 'Request to Know Specific Information' WHERE slug = 'RIGHT-TO-KNOW-SPECIFIC';
