-- Hotfix: POLARIS-2222 Address performance issues

CREATE INDEX ON collection_group(organization_id, collection_group_type);
CREATE INDEX ON data_subject_request(organization_id);