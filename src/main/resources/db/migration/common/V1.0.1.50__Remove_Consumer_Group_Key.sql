DROP INDEX idx_data_subject_type_org_type;

CREATE UNIQUE INDEX idx_unique_request_handling_instructions ON "request_handling_instructions" USING btree(organization_service_id) WHERE deleted_at IS NOT NULL;
