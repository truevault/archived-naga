-- POLARIS-2086: Add new table for storing received emails

CREATE TABLE privacy_email (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id UUID NOT NULL,
  subject TEXT NOT NULL,
  received_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
  created_privacy_request BOOLEAN NOT NULL DEFAULT FALSE,
  FOREIGN KEY (organization_id) REFERENCES organization(id)
);

CREATE INDEX idx_privacy_email_organization_id_received_at ON privacy_email (organization_id, received_at);
