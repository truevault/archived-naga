update service
  set service_provider_recommendation = case service_provider_recommendation
    when 'YES' then 'SERVICE_PROVIDER'
    else 'NONE'
  end;