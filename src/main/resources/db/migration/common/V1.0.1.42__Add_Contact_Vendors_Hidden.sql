ALTER TABLE
  data_subject_request
ADD COLUMN contact_vendors_ever_hidden boolean null,
ADD COLUMN contact_vendors_ever_shown boolean null;
