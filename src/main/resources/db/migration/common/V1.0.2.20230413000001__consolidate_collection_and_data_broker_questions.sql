-- POLARIS-2595 Consolidate two business survey question answers to one (if purchases-consumer-data = true, then make consumer-collection-for-other-businesses true)
-- Then delete the purchases-consumer-data question
UPDATE organization_survey_question s
SET answer = (
  SELECT answer
  FROM organization_survey_question d
  WHERE d.organization_id = s.organization_id AND survey_name = 'business-survey' AND slug = 'purchases-consumer-data'
)
WHERE survey_name = 'business-survey' AND slug = 'consumer-collection-for-other-businesses' AND answer = 'false';
