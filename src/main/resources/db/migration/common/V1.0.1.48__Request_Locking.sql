ALTER TABLE data_subject_request
  ADD COLUMN vendor_contacted BOOLEAN NULL DEFAULT FALSE;