-- POLARIS-1987 - add Multistate feature
INSERT INTO feature (name, description, enabled_by_default) VALUES ('Multistate', 'Does the organization subscribe to the US Multistate feature set', false);
