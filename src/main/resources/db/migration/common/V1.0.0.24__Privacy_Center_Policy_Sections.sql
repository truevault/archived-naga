CREATE TABLE privacy_center_policy_section (
  id UUID NOT NULL DEFAULT uuid_generate_v4(),
  privacy_center_id UUID NOT NULL,
  display_order INT NOT NULL DEFAULT 0,
  name VARCHAR NOT NULL DEFAULT '',
  anchor VARCHAR NOT NULL DEFAULT '',
  body VARCHAR NOT NULL DEFAULT '',
  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,

  FOREIGN KEY (privacy_center_id) REFERENCES privacy_center (id),
  UNIQUE(privacy_center_id, name, anchor)
);

ALTER TABLE privacy_center ADD COLUMN policy_last_updated DATE DEFAULT CURRENT_DATE NOT NULL;

INSERT INTO privacy_center_policy_section (
  privacy_center_id,
  display_order,
  name,
  anchor,
  body
)
SELECT
  pc.id,
  1,
  'Overview',
  'overview',
  pc.privacy_policy_markdown
FROM privacy_center pc
WHERE pc.privacy_policy_markdown IS NOT NULL AND pc.privacy_policy_markdown != '';
