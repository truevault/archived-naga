-- POLARIS-2315: Add custom field to be displayed on Org's employee privacy notices
ALTER TABLE organization ADD COLUMN employee_privacy_notice_custom TEXT;