ALTER TABLE organization ADD COLUMN header_image_url VARCHAR NOT NULL DEFAULT '';
ALTER TABLE organization ADD COLUMN physical_address VARCHAR NOT NULL DEFAULT '';
ALTER TABLE organization ADD COLUMN message_signature VARCHAR NOT NULL DEFAULT '';

UPDATE organization SET header_image_url = om.header_image_url, physical_address = om.physical_address
FROM (
    SELECT organization_id, min(header_image_url) as header_image_url, min(physical_address) as physical_address
    FROM organization_mailbox
    GROUP BY organization_id
) om
WHERE om.organization_id = organization.id;
