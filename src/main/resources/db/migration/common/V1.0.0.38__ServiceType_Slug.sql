ALTER TABLE service_type ADD COLUMN slug VARCHAR;
UPDATE service_type SET slug = LOWER(REPLACE(name, ' ', '-'));
ALTER TABLE service_type ALTER COLUMN slug SET NOT NULL;
