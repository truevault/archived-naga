-- POLARIS-2345: update pic group ordering (place GDPR Sensitive Data directly after Other Sensitive Data)

-- de-dupe and reset display_order
UPDATE pic_group SET display_order = 1 WHERE name = 'Personal Identifiers';
UPDATE pic_group SET display_order = 2 WHERE name = 'Online Identifiers';
UPDATE pic_group SET display_order = 3 WHERE name = 'Internet Activity';
UPDATE pic_group SET display_order = 4 WHERE name = 'Commercial and Financial Information';
UPDATE pic_group SET display_order = 5 WHERE name = 'Biometric Information';
UPDATE pic_group SET display_order = 6 WHERE name = 'Geolocation Information';
UPDATE pic_group SET display_order = 7 WHERE name = 'Physical and Audio Data';
UPDATE pic_group SET display_order = 8 WHERE name = 'Characteristics of Protected Classifications';
UPDATE pic_group SET display_order = 9 WHERE name = 'Government Identifier';
UPDATE pic_group SET display_order = 10 WHERE name = 'Other Sensitive Data';
UPDATE pic_group SET display_order = 11 WHERE name = 'GDPR Sensitive Data';
UPDATE pic_group SET display_order = 12 WHERE name = 'Medical';
UPDATE pic_group SET display_order = 13 WHERE name = 'Professional';
UPDATE pic_group SET display_order = 14 WHERE name = 'Education Information';
UPDATE pic_group SET display_order = 15 WHERE name = 'Inferences';

-- de-dupe and reset employee_display_order
UPDATE pic_group SET employee_display_order = 0 WHERE name = 'Government Identifier';
UPDATE pic_group SET employee_display_order = 0 WHERE name = 'Education Information';
UPDATE pic_group SET employee_display_order = 1 WHERE name = 'Personal Identifiers';
UPDATE pic_group SET employee_display_order = 2 WHERE name = 'Professional';
UPDATE pic_group SET employee_display_order = 3 WHERE name = 'Medical';
UPDATE pic_group SET employee_display_order = 4 WHERE name = 'Characteristics of Protected Classifications';
UPDATE pic_group SET employee_display_order = 5 WHERE name = 'Online Identifiers';
UPDATE pic_group SET employee_display_order = 6 WHERE name = 'Internet Activity';
UPDATE pic_group SET employee_display_order = 7 WHERE name = 'Commercial and Financial Information';
UPDATE pic_group SET employee_display_order = 8 WHERE name = 'Biometric Information';
UPDATE pic_group SET employee_display_order = 9 WHERE name = 'Geolocation Information';
UPDATE pic_group SET employee_display_order = 10 WHERE name = 'Physical and Audio Data';
UPDATE pic_group SET employee_display_order = 11 WHERE name = 'Other Sensitive Data';
UPDATE pic_group SET employee_display_order = 12 WHERE name = 'GDPR Sensitive Data';
UPDATE pic_group SET employee_display_order = 13 WHERE name = 'Inferences';
