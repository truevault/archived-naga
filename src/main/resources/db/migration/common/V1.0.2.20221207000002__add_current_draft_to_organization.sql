-- POLARIS-2015 - Organization Drafts
ALTER TABLE organization
  ADD COLUMN current_draft_id UUID,
  ADD COLUMN compliance_version VARCHAR(20);

CREATE INDEX idx_on_compliance_version ON organization (compliance_version);

ALTER TABLE organization ADD CONSTRAINT current_draft_id_fkey
  FOREIGN KEY (current_draft_id) REFERENCES organization_draft(id);
