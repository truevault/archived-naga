ALTER TABLE data_subject_request ALTER COLUMN request_date TYPE TIMESTAMP WITH TIME ZONE;
ALTER TABLE data_subject_request ALTER COLUMN request_due_date TYPE TIMESTAMP WITH TIME ZONE;
