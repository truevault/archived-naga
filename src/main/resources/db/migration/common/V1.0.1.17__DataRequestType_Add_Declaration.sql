ALTER TABLE data_request_type
  ADD COLUMN declaration_text VARCHAR DEFAULT NULL;

UPDATE data_request_type
  SET declaration_text = 'I declare under penalty of purjury that the above information is true and correct and that I am the person whose personal information is the subject of this request.'
  WHERE slug = 'RIGHT-TO-KNOW';

UPDATE data_request_type
  SET declaration_text = 'By submitting a Request to Delete, you are asking us to permanently erase any personal information we have about you, including any information you have provided to us or that we have collected in order to provide a product or service to you. Please confirm you would like us to delete your personal information.'
  WHERE slug = 'RIGHT-TO-DELETION';
