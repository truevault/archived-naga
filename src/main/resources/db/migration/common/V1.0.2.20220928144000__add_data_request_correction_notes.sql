ALTER TABLE data_subject_request
ADD COLUMN correction_request TEXT,
ADD COLUMN correction_finished BOOLEAN;