INSERT INTO data_request_type (slug, name, description, regulation_id)
SELECT 'CCPA-OPT-OUT', 'Request to Opt-Out', 'Request to Opt Out of the sale of your information to third parties', r.id
FROM regulation r
WHERE r.slug = 'CCPA';

ALTER TABLE data_subject_request ADD COLUMN subject_ip_address VARCHAR;
ALTER TABLE data_subject_request ALTER COLUMN subject_first_name DROP NOT NULL;
ALTER TABLE data_subject_request ALTER COLUMN subject_last_name DROP NOT NULL;
ALTER TABLE data_subject_request ALTER COLUMN data_subject_type_id DROP NOT NULL;

ALTER TABLE organization_regulation ADD COLUMN settings VARCHAR NOT NULL DEFAULT '{}';
