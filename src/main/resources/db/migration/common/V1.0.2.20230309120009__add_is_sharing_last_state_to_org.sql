-- POLARIS-2267 - Adding fields to track changes in the sharing/selling state of the org for updating related tasks
ALTER TABLE organization ADD COLUMN is_sharing_last_state BOOLEAN DEFAULT false;
ALTER TABLE organization ADD COLUMN is_selling_last_state BOOLEAN DEFAULT false;

-- Update is_selling_last state to true if that is the current state of ccpa_is_selling
UPDATE
    organization o
SET
    is_selling_last_state = TRUE
FROM
    organization_data_recipient odr
WHERE
    odr.organization_id = o.id
    AND odr.ccpa_is_selling
    AND o.is_selling_last_state = FALSE;

-- Update is_sharing_last_state to true if that is the current state of ccpa_is_sharing
UPDATE
    organization o
SET
    is_sharing_last_state = TRUE
FROM
    organization_data_recipient odr
WHERE
  odr.organization_id = o.id
  AND (odr.ccpa_is_sharing OR odr.uses_custom_audience)
  AND o.is_sharing_last_state = FALSE;