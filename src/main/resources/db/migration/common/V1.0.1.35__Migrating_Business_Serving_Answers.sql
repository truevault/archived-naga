UPDATE organization_survey_question AS o
SET survey_name = 'business-survey'
WHERE
  survey_name = 'intro-survey'
  AND slug = 'products-services-individuals-and-or-organizations';

ALTER TABLE organization
  ALTER COLUMN get_compliant_progress SET DEFAULT 'BUSINESS_SURVEY',
  ALTER COLUMN is_service_provider DROP NOT NULL,
  ALTER COLUMN is_service_provider SET DEFAULT NULL;