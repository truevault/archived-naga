-- POLARIS-2747 Add Website Audit
CREATE TABLE organization_website_audit (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),

    organization_id UUID NOT NULL,

    status VARCHAR NOT NULL,
    scan_url VARCHAR NOT NULL,

    scan_started_at TIMESTAMP,
    scan_ended_at TIMESTAMP,

    scan_error TEXT,

    privacy_policy_link_text_valid BOOLEAN,
    privacy_policy_link_url_valid BOOLEAN,

    ca_privacy_notice_link_text_valid BOOLEAN,
    ca_privacy_notice_link_url_valid BOOLEAN,

    opt_out_link_text_valid BOOLEAN,
    opt_out_link_url_valid BOOLEAN,
    opt_out_link_your_privacy_choices_icon BOOLEAN,

    polaris_js_setup BOOLEAN,
    polaris_js_before_google_analytics BOOLEAN,
    polaris_js_no_defer BOOLEAN,

    FOREIGN KEY (organization_id) REFERENCES organization (id)
);
