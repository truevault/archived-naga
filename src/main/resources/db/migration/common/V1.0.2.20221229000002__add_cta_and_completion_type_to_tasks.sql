-- TODO: POLARIS-2171: Update tasks to support CTA and differentiated completion type

ALTER TABLE task
  ADD COLUMN call_to_action TEXT,
  ADD COLUMN call_to_action_url TEXT,
  ADD COLUMN completion_type VARCHAR(255);
