-- Sources
UPDATE personal_information_source SET name = 'Consumer Directly' WHERE name = 'Consumer';
DELETE FROM personal_information_source WHERE name = 'Other Customers';
UPDATE personal_information_source SET name = 'Other Consumers' WHERE name = 'Other Users';
UPDATE personal_information_source SET display_order = 3 WHERE name = 'Consumer''s Employer';
UPDATE personal_information_source SET name = 'Other Data (Inference)', display_order = 4 WHERE name = 'Other Data (inference)';
UPDATE personal_information_source SET display_order = 9 WHERE name = 'Operating System';
UPDATE personal_information_source SET display_order = 10 WHERE name = 'Internet Service Provider';
UPDATE personal_information_source SET display_order = 11 WHERE name = 'Government Entity';

UPDATE personal_information_source SET description = 'Information collected directly from consumers' WHERE name = 'Consumer Directly';
UPDATE personal_information_source SET description = 'Information about consumers from different consumers' WHERE name = 'Other Consumers';
UPDATE personal_information_source SET description = 'Information about consumers from their employer' WHERE name = 'Consumer''s Employer';
UPDATE personal_information_source SET description = 'Inferences about consumers derived from other data, like age' WHERE name = 'Other Data (Inference)';
UPDATE personal_information_source SET description = 'Information purchased from data brokers, like ZoomInfo' WHERE name = 'Data Broker';
UPDATE personal_information_source SET description = 'Information from an analytics provider, like Google Analytics' WHERE name = 'Data Analytics Provider';
UPDATE personal_information_source SET description = 'Information from an ad network, like Facebook Ads' WHERE name = 'Ad Network';
UPDATE personal_information_source SET description = 'Information from a social network, like Twitter ' WHERE name = 'Social Network';
UPDATE personal_information_source SET description = 'Information from operating systems, like Windows' WHERE name = 'Operating System';
UPDATE personal_information_source SET description = 'Information from an ISP, like Comcast' WHERE name = 'Internet Service Provider';
UPDATE personal_information_source SET description = 'Information from agencies like public schools or the IRS' WHERE name = 'Government Entity';

-- Purposes
-- Add display order
ALTER TABLE business_purpose ADD COLUMN display_order INTEGER NOT NULL DEFAULT 0;

-- Update names
UPDATE business_purpose SET name = 'Provide Goods or Services' WHERE name = 'Provide Services or Goods';
UPDATE business_purpose SET name = 'Verify or Maintain the Quality of Goods or Services' WHERE name = 'Verify or Maintain Quality or Safety of Services';
UPDATE business_purpose SET name = 'Short-Term Transient Use' WHERE name = 'For Short-Term Transient Use';
UPDATE business_purpose SET name = 'Audit Current Interactions' WHERE name = 'Audit Customer Interactions';
UPDATE business_purpose SET name = 'Employment-Related' WHERE name = 'Employment';

-- Add order and descriptions
UPDATE business_purpose SET display_order = 1, description = 'E.g., providing customer service related to product/service' WHERE name = 'Provide Goods or Services';
UPDATE business_purpose SET display_order = 2, description = 'E.g., advertising or other promotion of your product/service' WHERE name = 'Sales and Marketing';
UPDATE business_purpose SET display_order = 3, description = 'E.g., debugging, diagnostics, and crash reports' WHERE name = 'Identify and Repair Errors';
UPDATE business_purpose SET display_order = 4, description = 'E.g., improving the quality or safety of your product/service' WHERE name = 'Verify or Maintain the Quality of Goods or Services';
UPDATE business_purpose SET display_order = 5, description = 'E.g., detecting security incidents, preventing fraudulent activity' WHERE name = 'Detect and Prevent Security Incidents';
UPDATE business_purpose SET display_order = 6, description = 'E.g., technological development or demonstration' WHERE name = 'Internal Research';
UPDATE business_purpose SET display_order = 7, description = 'E.g., displaying contextual ads or determining content to show' WHERE name = 'Short-Term Transient Use';
UPDATE business_purpose SET display_order = 8, description = 'E.g., counting ad impressions or unique visitors to your website' WHERE name = 'Audit Current Interactions';
UPDATE business_purpose SET display_order = 9, description = 'Used only for employment purposes like hiring and benefits' WHERE name = 'Employment-Related';
