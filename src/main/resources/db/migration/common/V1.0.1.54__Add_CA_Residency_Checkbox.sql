ALTER TABLE privacy_center
  ADD COLUMN ca_residency_confirmation BOOLEAN NOT NULL DEFAULT TRUE;