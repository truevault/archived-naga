-- POLARIS-2743 - Data Collection Updates for Midyear Survey

UPDATE personal_information_category
  SET
    name = 'Data revealing racial or ethnic origin',
    pic_key = 'racial-or-ethnic-origin'
  WHERE name = 'Racial or ethnic origin';

UPDATE personal_information_category
  SET
    name = 'Data revealing religious or philosophical beliefs',
    pic_key = 'religious-or-philosophical-beliefs'
  WHERE name = 'Religious or philosophical beliefs';

UPDATE personal_information_category
  SET
    name = 'Precise geolocation'
  WHERE name = 'Geolocation data';

UPDATE personal_information_category
  SET
    name = 'Citizenship or citizenship status',
    pic_key = 'citizenship-status'
  WHERE name = 'Citizenship status';

UPDATE personal_information_category
  SET
    name = 'Data concerning sex life or sexual orientation',
    pic_key = 'sexual-orientation'
  WHERE name = 'Sexual orientation';

INSERT INTO personal_information_category (name, group_id, is_sensitive, pic_key, is_ccpa, is_gdpr, group_display_order)
VALUES (
  'Data from a known child (under 13 years old)',
  (SELECT id
    FROM pic_group
    WHERE name = 'Other Sensitive Data'),
  TRUE,
  'data-from-child-under-13',
  TRUE,
  FALSE,
  7
);

INSERT INTO personal_information_category (name, group_id, is_sensitive, pic_key, is_ccpa, is_gdpr, group_display_order, description)
VALUES (
  'Geolocation information',
  (SELECT id
    FROM pic_group
    WHERE name = 'Geolocation Information'),
  FALSE,
  'geolocation-information',
  TRUE,
  FALSE,
  2,
  'Any data that relates to a specific consumer’s broader geographic location, such as city and state. This does not include postal address or precise geolocation.'
);
