CREATE TABLE organization_cookie(
  organization_id UUID NOT NULL,

  name VARCHAR NOT NULL,
  domain VARCHAR NOT NULL,

  first_scanned_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
  last_scanned_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,

  last_scanned_url VARCHAR,
  last_scanned_value VARCHAR,
  last_scanned_path VARCHAR,
  last_scanned_expires VARCHAR,
  last_scanned_http_only BOOLEAN,
  last_scanned_secure BOOLEAN,
  last_scanned_session BOOLEAN,
  last_scanned_same_site VARCHAR,
  last_scanned_priority VARCHAR,
  last_scanned_same_party BOOLEAN,

  is_snoozed BOOLEAN NOT NULL DEFAULT FALSE,
  is_hidden BOOLEAN NOT NULL DEFAULT FALSE,

  PRIMARY KEY (organization_id, name, domain),
  FOREIGN KEY (organization_id) REFERENCES organization (id)
)

