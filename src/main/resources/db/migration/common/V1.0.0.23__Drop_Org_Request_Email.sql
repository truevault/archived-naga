-- This column is no longer used after V1.0.0.22
-- Note that the other "moved" columns from that version _are_ still used as of this migration

ALTER TABLE organization
DROP COLUMN IF EXISTS request_email;
