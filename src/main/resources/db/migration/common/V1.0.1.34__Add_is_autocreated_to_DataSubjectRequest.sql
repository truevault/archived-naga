ALTER TABLE
  data_subject_request
ADD
  COLUMN is_autocreated BOOLEAN NULL;

UPDATE
  data_subject_request
SET
  is_autocreated = false;

ALTER TABLE
  data_subject_request
ALTER COLUMN
  is_autocreated
SET
  NOT NULL;