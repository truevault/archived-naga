ALTER TABLE consumer_group_organization_vendor ADD COLUMN is_delete BOOLEAN DEFAULT FALSE;
ALTER TABLE data_subject_type ADD COLUMN internal_storage_is_delete BOOLEAN DEFAULT FALSE;
