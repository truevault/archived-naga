ALTER TABLE service
  ADD COLUMN service_provider_recommendation VARCHAR DEFAULT 'NO',
  ADD COLUMN service_provider_language_url VARCHAR,
  ADD COLUMN dpa_applicable BOOLEAN DEFAULT FALSE,
  ADD COLUMN dpa_url VARCHAR,
  ADD COLUMN contact_url VARCHAR;
