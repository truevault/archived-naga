ALTER TABLE data_subject_type
  DROP COLUMN verification_progress,
  DROP COLUMN is_verifiable,
  DROP COLUMN verification_personal_information_category_id,
  DROP COLUMN verification_instruction;