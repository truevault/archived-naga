CREATE TABLE vendor_default_retention_reasons (
  vendor_id UUID NOT NULL,
  retention_reason_id UUID NOT NULL,

  PRIMARY KEY (vendor_id, retention_reason_id),

  FOREIGN KEY (vendor_id) REFERENCES vendor (id),
  FOREIGN KEY (retention_reason_id) REFERENCES retention_reason (id)
);