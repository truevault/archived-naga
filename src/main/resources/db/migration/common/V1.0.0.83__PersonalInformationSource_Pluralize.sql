UPDATE personal_information_source SET name = 'Consumers' WHERE name = 'Consumer';
UPDATE personal_information_source SET name = 'Data Brokers' WHERE name = 'Data Broker';
UPDATE personal_information_source SET name = 'Data Analytics Providers' WHERE name = 'Data Analytics Provider';
UPDATE personal_information_source SET name = 'Ad Networks' WHERE name = 'Ad Network';
UPDATE personal_information_source SET name = 'Social Networks' WHERE name = 'Social Network';
UPDATE personal_information_source SET name = 'Internet Service Providers' WHERE name = 'Internet Service Provider';
UPDATE personal_information_source SET name = 'Government Entities' WHERE name = 'Government Entity';
UPDATE personal_information_source SET name = 'Operating Systems' WHERE name = 'Operating System';