UPDATE vendor SET is_standalone = TRUE WHERE name in ('TrueVault Safe', 'TrueVault Polaris', 'Google Analytics');
UPDATE vendor SET is_standalone = TRUE WHERE recipient_category in ('Web Hosting Service', 'IT Infrastructure Service');
