ALTER TABLE organization
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

 ALTER TABLE privacy_center
   ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE organization_user
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE polaris_user
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;



ALTER TABLE polaris_user
  DROP CONSTRAINT polaris_user_email_key;

ALTER TABLE privacy_center
  DROP CONSTRAINT privacy_center_custom_url_key;