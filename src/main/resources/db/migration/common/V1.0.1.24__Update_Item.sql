-- Migrate all 'data-is-not-provided-by-the-consumer' retention reasons to 'reasonable-internal-uses'
UPDATE privacy_request_retention_reason
SET retention_reason_id = (
    SELECT id
    FROM retention_reason
    WHERE slug = 'reasonable-internal-uses'
)
WHERE retention_reason_id IN (
  SELECT id
  FROM retention_reason
  WHERE slug = 'data-is-not-provided-by-the-consumer'
);

DELETE FROM retention_reason where slug = 'data-is-not-provided-by-the-consumer';