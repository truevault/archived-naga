
UPDATE organization SET gdpr_retention_policy = concat('We do not retain data for any longer than is necessary for the purposes described in this Policy. ', gdpr_retention_policy);

ALTER TABLE organization
  ALTER COLUMN gdpr_retention_policy SET DEFAULT 'We do not retain data for any longer than is necessary for the purposes described in this Policy.';