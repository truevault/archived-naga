-- Remove organization-service compliance tables; only organization_service_personal_information_category will be recreated below
DROP TABLE organization_service_personal_information_category;
DROP TABLE organization_service_personal_information_source;
DROP TABLE organization_service_business_purpose;

-- Remove all service level compliance settings; all settings will be at organization-data-subject-type level only
DROP TABLE service_personal_information_source;
DROP TABLE service_personal_information_category;
DROP TABLE service_service_business_purpose;
DROP TABLE service_data_subject_type;

-- Renaming this table to just business_purpose
DROP TABLE service_business_purpose;

-- Remove tables for already removed models
DROP TABLE organization_service_organization_team;
DROP TABLE organization_team;

-- Add new category group table
CREATE TABLE personal_information_category_group(
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name VARCHAR NOT NULL,
    display_order INT NOT NULL DEFAULT 0
);
INSERT INTO personal_information_category_group (id, name, display_order)
VALUES
('894fdfa8-a251-40c2-bcfd-6d7b2a992159', 'Personal Identifiers', 1),
('098cc93b-527b-4109-a7c8-804032cc3cd4', 'Online Identifiers', 2),
('fbc84cd9-37d7-473e-a134-3d6ae5360dd7', 'Internet or Electronic Activity', 3),
('eafb566d-1d10-4ee3-8020-d78296ffedc2', 'Financial Information', 4),
('23169390-d485-40a9-a8c1-2acca084efcc', 'Commercial Information', 5),
('ce680dfb-548e-4adf-b88e-f602bc96dfe5', 'Inferences', 6),
('c14e3a58-56dd-46bd-9ed8-7020f873d324', 'Professional Information', 7),
('f915c79c-a389-4653-82b2-2a53f13c33af', 'Health or Medical Information', 8),
('6e042826-254b-4194-a46c-1acac6cc6ac5', 'Government Identifier', 9),
('f638f8ca-771b-4b21-9c50-3878f098c9e3', 'Protected Class Information', 10),
('f757a822-e32f-44f4-b84f-f3247376c3ff', 'Geolocation Data', 11),
('f42d7bb4-f7d1-420b-8b21-1a4a4e40087a', 'Education Information', 12),
('d4c06e24-c2af-4995-a292-b611b701beec', 'Miscellaneous Identifier', 13),
('2853077e-4c57-4da4-b296-0a1f837dfb36', 'Biometric Information', 14),
('5042db51-9ab6-445c-971b-86e23fc15125', 'Sensory Information', 15);

-- Update personal_information_category columns and load with default data
DELETE FROM personal_information_category;
ALTER TABLE personal_information_category ADD COLUMN group_id UUID NOT NULL;
ALTER TABLE personal_information_category ADD CONSTRAINT personal_information_category_group_fkey
    FOREIGN KEY (group_id) REFERENCES personal_information_category_group(id);
ALTER TABLE personal_information_category ADD COLUMN description VARCHAR;
ALTER TABLE personal_information_category DROP COLUMN tooltip_text;
INSERT INTO personal_information_category (group_id, name, description) VALUES
('894fdfa8-a251-40c2-bcfd-6d7b2a992159','Name or alias','Real (legal) name or Alias'),
('894fdfa8-a251-40c2-bcfd-6d7b2a992159','Email address',''),
('894fdfa8-a251-40c2-bcfd-6d7b2a992159','Telephone number',''),
('894fdfa8-a251-40c2-bcfd-6d7b2a992159','Postal address',''),
('098cc93b-527b-4109-a7c8-804032cc3cd4','IP Address',''),
('098cc93b-527b-4109-a7c8-804032cc3cd4','Unique identifier','Persistent identifiers, including cookies, pixels, web beacons (or similar tracking technology), or device ID'),
('098cc93b-527b-4109-a7c8-804032cc3cd4','Other online identifier','E.g., MAC address, device fingerprints, username, social media account handle'),
('fbc84cd9-37d7-473e-a134-3d6ae5360dd7','Internet or electronic activity','Browsing history, Search history, Interactions with websites, applications, or advertisements'),
('eafb566d-1d10-4ee3-8020-d78296ffedc2','Bank account number',''),
('eafb566d-1d10-4ee3-8020-d78296ffedc2','Credit card or debit card number',''),
('eafb566d-1d10-4ee3-8020-d78296ffedc2','Account name','A name associated with a financial account, such as a family trust, a joint account name, etc.'),
('eafb566d-1d10-4ee3-8020-d78296ffedc2','Insurance policy number',''),
('eafb566d-1d10-4ee3-8020-d78296ffedc2','Other financial information','Other financial information not covered in other categories'),
('23169390-d485-40a9-a8c1-2acca084efcc','Records of personal property',''),
('23169390-d485-40a9-a8c1-2acca084efcc','Purchase history',''),
('23169390-d485-40a9-a8c1-2acca084efcc','Considered purchases',''),
('23169390-d485-40a9-a8c1-2acca084efcc','Purchasing tendencies','Other purchasing patterns'),
('ce680dfb-548e-4adf-b88e-f602bc96dfe5','Inferences',' Inferences from personal information to create a profile, such as preferences, characteristics, psychological trends, predispositions, behavior, attitudes, intelligence, abilities, aptitudes'),
('c14e3a58-56dd-46bd-9ed8-7020f873d324','Professional information','Employer, employment history, salary, professional affiliations'),
('c14e3a58-56dd-46bd-9ed8-7020f873d324','Employment-related information','Information collected by a business for hiring or employment purposes, such as to provide employment benefits; this would include, for example, emergency contact information'),
('f915c79c-a389-4653-82b2-2a53f13c33af','Medical information','Medical conditions, prescriptions'),
('f915c79c-a389-4653-82b2-2a53f13c33af','Health insurance information','Group number, insurance policy number'),
('6e042826-254b-4194-a46c-1acac6cc6ac5','Social security number',''),
('6e042826-254b-4194-a46c-1acac6cc6ac5','Driver''s license number',''),
('6e042826-254b-4194-a46c-1acac6cc6ac5','Passport number',''),
('6e042826-254b-4194-a46c-1acac6cc6ac5','Other government-issued identifier','Other government-issued ID number (ex: state voter ID number, Alien Registration Number (or "A-number"))'),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Race or color',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Sex or gender','Includes pregnancy, childbirth, breastfeeding and/ or related medical conditions'),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Sexual orientation',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Gender identity or gender expression',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Age','Age 40 or older'),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Religion','Includes religious dress and grooming practices'),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','National origin',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Citizenship status',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Disability','Mental and physical including HIV/AIDS, cancer, and genetic characteristics'),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Medical condition','Requests for family care leave, leave for serious health conditions, or pregnancy disability leave'),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Genetic information',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Marital status',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Familial status','Whether a person has children or is pregnant; relevant for housing only'),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Military or veteran status',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Political affiliation or activities',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Status as a victim of sexual assault, domestic violence, or stalking',''),
('f638f8ca-771b-4b21-9c50-3878f098c9e3','Membership in a trade union','GDPR Only'),
('f757a822-e32f-44f4-b84f-f3247376c3ff','Geolocation Data','GPS Coordinates'),
('f42d7bb4-f7d1-420b-8b21-1a4a4e40087a','Education Information','Non-public files, records and documents maintained by an educational agency or institution'),
('d4c06e24-c2af-4995-a292-b611b701beec','Signature','Signature scan'),
('d4c06e24-c2af-4995-a292-b611b701beec','Physical characteristics or descriptions','Hair color, eye color, height, weight, physical measurements '),
('2853077e-4c57-4da4-b296-0a1f837dfb36','Biometric information','Fingerprints, Facial patterns, Unique voice characteristics, Iris patterns'),
('5042db51-9ab6-445c-971b-86e23fc15125','Audio information','Voicemail or voice recording'),
('5042db51-9ab6-445c-971b-86e23fc15125','Electronic information','Ex: EKG or ECG'),
('5042db51-9ab6-445c-971b-86e23fc15125','Visual information','Photos or videos'),
('5042db51-9ab6-445c-971b-86e23fc15125','Thermal information','Body thermal pattern'),
('5042db51-9ab6-445c-971b-86e23fc15125','Olfactory information','');


-- Update personal_information_source columns and load with default data
DELETE FROM personal_information_source;
ALTER TABLE personal_information_source DROP COLUMN tooltip_text;
ALTER TABLE personal_information_source ADD COLUMN description VARCHAR;
INSERT INTO personal_information_source (name) VALUES
('Consumer (Direct)'),
('Consumer (Automatic)'),
('Ad Network'),
('Internet Service Provider'),
('Government Entity'),
('Operating System'),
('Social Network'),
('Data Broker');

-- Update personal_information_category columns and load with default data
CREATE TABLE business_purpose (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name VARCHAR,
    description VARCHAR,
    created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL
);
INSERT INTO business_purpose (name) VALUES
('Provide Services or Goods'),
('Detect and Prevent Security Incidents'),
('Identify and Repair Errors'),
('For Short-Term Transient Use'),
('Audit Customer Interactions'),
('Internal Research'),
('Verify or Maintain Quality or Safety of Services'),
('Employment'),
('Sales and Marketing');

CREATE TABLE organization_data_subject_type_personal_information_category (
    id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    organization_id UUID NOT NULL,
    data_subject_type_id UUID NOT NULL,
    personal_information_category_id UUID NOT NULL,

    UNIQUE (organization_id, data_subject_type_id, personal_information_category_id),
    FOREIGN KEY (organization_id) REFERENCES organization (id),
    FOREIGN KEY (data_subject_type_id) REFERENCES data_subject_type (id),
    FOREIGN KEY (personal_information_category_id) REFERENCES personal_information_category (id)
);

CREATE TABLE organization_data_subject_type_personal_information_source (
    id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    organization_id UUID NOT NULL,
    data_subject_type_id UUID NOT NULL,
    personal_information_source_id UUID NOT NULL,

    UNIQUE (organization_id, data_subject_type_id, personal_information_source_id),
    FOREIGN KEY (organization_id) REFERENCES organization (id),
    FOREIGN KEY (data_subject_type_id) REFERENCES data_subject_type (id),
    FOREIGN KEY (personal_information_source_id) REFERENCES personal_information_source (id)
);

CREATE TABLE organization_data_subject_type_business_purpose (
    id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    organization_id UUID NOT NULL,
    data_subject_type_id UUID NOT NULL,
    business_purpose_id UUID NOT NULL,

    UNIQUE (organization_id, data_subject_type_id, business_purpose_id),
    FOREIGN KEY (organization_id) REFERENCES organization (id),
    FOREIGN KEY (data_subject_type_id) REFERENCES data_subject_type (id),
    FOREIGN KEY (business_purpose_id) REFERENCES business_purpose (id)
);

CREATE TABLE organization_service_personal_information_category (
    id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    organization_data_subject_type_personal_information_category_id INT NOT NULL,
    service_id UUID NOT NULL,

    UNIQUE (organization_data_subject_type_personal_information_category_id, service_id),
    FOREIGN KEY (organization_data_subject_type_personal_information_category_id) REFERENCES organization_data_subject_type_personal_information_category (id),
    FOREIGN KEY (service_id) REFERENCES service (id)
);
