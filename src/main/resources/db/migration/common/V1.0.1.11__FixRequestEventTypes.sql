update data_subject_request_event e
  set event_type = case r.substate
    when 'PROCESSED_AS_SERVICE_PROVIDER' then 'CLOSED_SERVICE_PROVIDER'
    when 'SUBJECT_NOT_FOUND' then 'CLOSED_NOT_FOUND'
    when 'SUBJECT_NOT_VERIFIED' then 'CLOSED_NOT_VERIFIED'
    when 'COMPLETED' then 'CLOSED'
  end
from data_subject_request r
  inner join legacy_data_subject_request lr on lr.id = r.id
where r.id = e.data_subject_request_id
  and e.event_type = 'CLOSED'
  and r.substate is not null;
