ALTER TABLE service RENAME TO vendor;
ALTER TABLE vendor RENAME COLUMN category TO recipient_category;
ALTER TABLE vendor
  ADD COLUMN source_category VARCHAR,
  ADD COLUMN is_data_recipient BOOLEAN DEFAULT false,
  ADD COLUMN is_data_source BOOLEAN DEFAULT false;

-- all current vendors are data recipients
UPDATE vendor SET
  is_data_recipient = TRUE,
  source_category = recipient_category;

ALTER TABLE organization_service RENAME TO organization_data_recipient;
ALTER TABLE organization_data_recipient RENAME COLUMN service_id TO vendor_id;

CREATE TABLE organization_data_source(
  organization_id UUID NOT NULL,
  vendor_id UUID NOT NULL,

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  FOREIGN KEY (vendor_id) REFERENCES vendor (id),
  PRIMARY KEY (organization_id, vendor_id)
);


INSERT INTO vendor (name, vendor_key, is_data_source, service_provider_recommendation) VALUES
  ('Consumer Directly', 'source-consumer-directly', true, 'NONE'),
  ('Ad Networks', 'source-ad-networks', true, 'NONE'),
  ('Data Analytics Providers', 'source-data-analytics-provider', true, 'NONE'),
  ('Operating Systems', 'source-operating-systems', true, 'NONE'),
  ('Social Networks', 'source-social-networks', true, 'NONE'),
  ('Data Brokers', 'source-data-brokers', true, 'NONE'),
  ('Other Individuals', 'source-other-individuals', true, 'NONE');

-- migrate all of our old sources data

WITH v as (
  SELECT v.id
  FROM vendor v
  WHERE v.vendor_key = 'source-data-analytics-provider'
)
INSERT INTO organization_data_source (organization_id, vendor_id)
SELECT DISTINCT(org.id) as org_id, v.id as vendor_id
FROM personal_information_source pis
INNER JOIN organization_data_subject_type_personal_information_source odstpis ON odstpis.personal_information_source_id = pis.id
INNER JOIN organization org ON org.id = odstpis.organization_id
INNER JOIN v ON true
WHERE pis.name = 'Data Analytics Providers';

WITH v as (
  SELECT v.id
  FROM vendor v
  WHERE v.vendor_key = 'source-operating-systems'
)
INSERT INTO organization_data_source (organization_id, vendor_id)
SELECT DISTINCT(org.id) as org_id, v.id as vendor_id
FROM personal_information_source pis
INNER JOIN organization_data_subject_type_personal_information_source odstpis ON odstpis.personal_information_source_id = pis.id
INNER JOIN organization org ON org.id = odstpis.organization_id
INNER JOIN v ON true
WHERE pis.name = 'Operating Systems';

WITH v as (
  SELECT v.id
  FROM vendor v
  WHERE v.vendor_key = 'source-ad-networks'
)
INSERT INTO organization_data_source (organization_id, vendor_id)
SELECT DISTINCT(org.id) as org_id, v.id as vendor_id
FROM personal_information_source pis
INNER JOIN organization_data_subject_type_personal_information_source odstpis ON odstpis.personal_information_source_id = pis.id
INNER JOIN organization org ON org.id = odstpis.organization_id
INNER JOIN v ON true
WHERE pis.name = 'Ad Networks';

WITH v as (
  SELECT v.id
  FROM vendor v
  WHERE v.vendor_key = 'source-social-networks'
)
INSERT INTO organization_data_source (organization_id, vendor_id)
SELECT DISTINCT(org.id) as org_id, v.id as vendor_id
FROM personal_information_source pis
INNER JOIN organization_data_subject_type_personal_information_source odstpis ON odstpis.personal_information_source_id = pis.id
INNER JOIN organization org ON org.id = odstpis.organization_id
INNER JOIN v ON true
WHERE pis.name = 'Social Networks';

WITH v as (
  SELECT v.id
  FROM vendor v
  WHERE v.vendor_key = 'source-data-brokers'
)
INSERT INTO organization_data_source (organization_id, vendor_id)
SELECT DISTINCT(org.id) as org_id, v.id as vendor_id
FROM personal_information_source pis
INNER JOIN organization_data_subject_type_personal_information_source odstpis ON odstpis.personal_information_source_id = pis.id
INNER JOIN organization org ON org.id = odstpis.organization_id
INNER JOIN v ON true
WHERE pis.name = 'Data Brokers';

WITH v as (
  SELECT v.id
  FROM vendor v
  WHERE v.vendor_key = 'source-other-individuals'
)
INSERT INTO organization_data_source (organization_id, vendor_id)
SELECT DISTINCT(org.id) as org_id, v.id as vendor_id
FROM personal_information_source pis
INNER JOIN organization_data_subject_type_personal_information_source odstpis ON odstpis.personal_information_source_id = pis.id
INNER JOIN organization org ON org.id = odstpis.organization_id
INNER JOIN v ON true
WHERE pis.name = 'Other Consumers';

WITH v as (
  SELECT v.id
  FROM vendor v
  WHERE v.vendor_key = 'source-consumer-directly'
)
INSERT INTO organization_data_source (organization_id, vendor_id)
SELECT DISTINCT org.id as org_id, v.id as vendor_id
FROM organization org
INNER JOIN organization_survey_question osq ON osq.organization_id = org.id
INNER JOIN v on true
WHERE
  osq.survey_name = 'intro-survey'
  AND osq.slug = 'products-services-individuals-and-or-organizations'
  AND (osq.answer = 'individuals' OR osq.answer ='individuals-and-organizations');



-- drop existing sources

DROP TABLE organization_data_subject_type_personal_information_source;
DROP TABLE personal_information_source;
DROP TABLE organization_custom_third_party_source;