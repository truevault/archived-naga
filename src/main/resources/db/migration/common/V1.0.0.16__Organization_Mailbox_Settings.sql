ALTER TABLE organization_mailbox
ADD COLUMN header_image_url VARCHAR(2048) NOT NULL DEFAULT '';

ALTER TABLE organization_mailbox
ADD COLUMN physical_address VARCHAR(2048) NOT NULL DEFAULT '';
