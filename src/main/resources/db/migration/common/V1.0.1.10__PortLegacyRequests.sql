insert into data_subject_request (
  id,
  public_id,
  reply_key,
  organization_id,
  state,
  state_history,
  substate,
  subject_first_name,
  subject_last_name,
  subject_email_address,
  subject_phone_number,
  subject_mailing_address,
  request_date,
  request_due_date,
  request_original_due_date,
  created_at,
  closed_at,
  data_request_type_id,
  subject_ip_address,
  self_declaration_provided,
  confirmation_token,
  email_verified,
  submission_method,
  submission_source
)
select
  dsr.id,
  dsr.public_id,
  dsr.reply_key,
  dsr.organization_id,
  case dsr.state
    when 'CLOSED' then 'CLOSED'
    when 'PROCESSING' then 'CONSUMER_GROUP_SELECTION'
    when 'PENDING_EMAIL_VERIFICATION' then 'PROCESSING'
    when 'NEW' then 'PROCESSING'
    when 'IDENTITY_VERIFICATION' then 'PROCESSING'
    else NULL
  end as state,
  replace(replace(replace(replace(replace(dsr.state_history,
    'CLOSED', 'CLOSED'),
    'PROCESSING', 'CONSUMER_GROUP_SELECTION'),
    'PENDING_EMAIL_VERIFICATION', 'PROCESSING'),
    'NEW', 'PROCESSING'),
    'IDENTITY_VERIFICATION', 'PROCESSING') as state_history,
    case dsr.substate
      when 'DID_NOT_PROCESS' then 'SUBJECT_NOT_FOUND'
      when 'UNABLE_TO_VERIFY' then 'SUBJECT_NOT_VERIFIED'
      when 'COMPLETED' then 'COMPLETED'
      else NULL
    end as substate,
  dsr.subject_first_name,
  dsr.subject_last_name,
  dsr.subject_email_address,
  dsr.subject_phone_number,
  dsr.subject_mailing_address,
  dsr.request_date,
  dsr.request_due_date,
  dsr.request_due_date,
  dsr.created_at,
  dsr.closed_at,
  dsr.data_request_type_id,
  dsr.subject_ip_address,
  dsr.self_declaration_provided,
  dsr.confirmation_token,
  dsr.email_verified,
  dsr.submission_method,
  case
    when dsre.user_id is not null then 'PRIVACY_CENTER'
    else 'POLARIS'
  end submission_source
from legacy_data_subject_request dsr
  inner join legacy_data_subject_request_event dsre on dsre.data_subject_request_id = dsr.id and dsre.event_type = 'CREATED'
  left join data_request_type drt on drt.id = dsr.data_request_type_id
where (
    dsr.state not in ('PENDING_EMAIL_VERIFICATION', 'NEW', 'IDENTITY_VERIFICATION')
    or drt.slug = 'CCPA-OPT-OUT'
  );


insert into data_subject_request_consumer_groups (data_subject_request_id, data_subject_type_id)
select ldsr.id, ldsr.data_subject_type_id
from legacy_data_subject_request ldsr
inner join data_subject_request dsr on dsr.id = ldsr.id
where ldsr.data_subject_type_id IS NOT NULL;




insert into data_subject_request_event (id, data_subject_request_id, user_id, event_type, message, is_data_subject, created_at)
select ldsre.id, ldsre.data_subject_request_id, ldsre.user_id, ldsre.event_type, ldsre.message, ldsre.is_data_subject, ldsre.created_at
from legacy_data_subject_request_event ldsre
-- join to the current, to ensure that the current request has been migrated
inner join data_subject_request dsr on ldsre.data_subject_request_id = dsr.id;



insert into data_subject_request_event_attachment(id, data_subject_request_event_id, attachment_id, created_at)
select id, data_subject_request_event_id, attachment_id, created_at
from legacy_data_subject_request_event_attachment;
