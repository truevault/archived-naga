INSERT INTO organization_vendor_consumer_group (organization_id, vendor_id, consumer_group_id, no_data_mapping)
(SELECT
  o.id as organization_id,
  s.id as vendor_id,
  cg.id as consumer_group_id,
  TRUE as no_data_mapping
FROM organization_service os
INNER JOIN organization o ON o.id = os.organization_id
INNER JOIN service s ON s.id = os.service_id
INNER JOIN data_subject_type cg ON o.id = cg.organization_id
INNER JOIN consumer_group_type cgt ON cgt.id = cg.consumer_group_type_id
LEFT JOIN organization_data_subject_type_personal_information_category odstpic ON odstpic.organization_id = o.id AND odstpic.data_subject_type_id = cg.id
LEFT JOIN organization_service_personal_information_category ospic ON ospic.organization_data_subject_type_personal_information_category_id = odstpic.id AND ospic.service_id = s.id
WHERE
  os.mapping_progress = 'DONE'
  AND cgt.slug != 'employment'
GROUP BY o.id, o.name, s.id, s.name, cg.id, cg.type
HAVING count(ospic.id) = 0)
ON CONFLICT (organization_id, vendor_id, consumer_group_id) DO NOTHING;
