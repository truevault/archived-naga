ALTER TABLE data_subject_type ADD COLUMN privacy_notice_progress VARCHAR DEFAULT 'NEEDS_REVIEW';

ALTER TABLE organization
  ADD COLUMN ca_privacy_notice_progress VARCHAR DEFAULT 'NEEDS_REVIEW',
  ADD COLUMN opt_out_privacy_notice_progress VARCHAR DEFAULT 'NEEDS_REVIEW';
