ALTER TABLE organization
  ADD COLUMN dev_instructions_uuid UUID NOT NULL DEFAULT uuid_generate_v4(),
  ADD COLUMN hr_instructions_uuid UUID NOT NULL DEFAULT uuid_generate_v4();