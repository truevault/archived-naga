UPDATE personal_information_category
SET description = 'Select this even if you use a third party payment processor and/or you store only the last 4 digits of credit card numbers'
WHERE pic_key = 'credit-debit-card-number';
