-- Create the new columns in privacy_center

ALTER TABLE privacy_center
ADD COLUMN privacy_policy_url TEXT DEFAULT NULL,
ADD COLUMN request_toll_free_phone_number VARCHAR(255) DEFAULT NULL,
ADD COLUMN request_form_url TEXT DEFAULT NULL,
ADD COLUMN request_email VARCHAR(255) DEFAULT NULL,
ADD COLUMN logo_url VARCHAR(2048) DEFAULT NULL;

-- Update the child rows with data from the parent

UPDATE privacy_center
  SET (
    logo_url,
    privacy_policy_url,
    request_toll_free_phone_number,
    request_form_url,
    request_email
  ) = (
    organization.logo_url,
    organization.privacy_policy_url,
    organization.request_toll_free_phone_number,
    organization.request_form_url,
    organization.request_email
  )
  FROM organization
  WHERE privacy_center.organization_id = organization.id;
