UPDATE personal_information_category
  SET description = 'Includes interactions with your website, app, or ads, or other websites, apps, or ads'
  WHERE name = 'Interactions with websites, apps or ads';

UPDATE personal_information_category
  SET description = 'All products or services purchased, obtained, or considered, whether or not from your business'
  WHERE name = 'Purchases';
