UPDATE personal_information_category
  SET group_id = (SELECT id FROM pic_group WHERE name = 'Other Sensitive Data')
  WHERE name = 'Data concerning sex life or sexual orientation';