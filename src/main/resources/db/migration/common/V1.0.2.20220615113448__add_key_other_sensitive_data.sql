UPDATE pic_group
  SET group_key = 'other-sensitive-data', display_order = '15', employee_display_order = '15'
  WHERE name = 'Other sensitive data';

UPDATE personal_information_category
  SET pic_key = 'financial-account-number'
  WHERE name = 'Bank or other financial account number';

UPDATE personal_information_category
  SET pic_key = 'financial-account-creds'
  WHERE name = 'Account security or access credentials';

UPDATE personal_information_category
  SET pic_key = 'health-insurance-id'
  WHERE name = 'Health insurance or medical ID number';

UPDATE personal_information_category
  SET pic_key = 'account-login'
  WHERE name = 'Account login';
