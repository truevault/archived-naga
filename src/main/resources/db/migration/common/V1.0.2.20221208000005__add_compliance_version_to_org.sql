-- POLARIS-2148 - Add Catch Up Survey Status
ALTER TABLE organization
  ADD COLUMN current_survey VARCHAR(255);