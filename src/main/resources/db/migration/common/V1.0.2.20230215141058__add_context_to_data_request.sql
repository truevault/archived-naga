-- POLARIS-2432 Add the option to submit HR requests from the privacy center

ALTER TABLE data_subject_request ADD COLUMN context VARCHAR(50) DEFAULT 'CONSUMER';