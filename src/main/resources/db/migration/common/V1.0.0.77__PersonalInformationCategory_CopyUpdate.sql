UPDATE personal_information_category
  SET description = 'Any additional information not already selected that your business collects about a person acting as a job applicant to, or employee or contractor of, your business, including information necessary to administer employment benefits (e.g., emergency contact information)'
  WHERE name = 'Employment-related information';
