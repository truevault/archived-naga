-- POLARIS-2413 POLARIS-2455 - Add a `email_verified_manually` flag to the data request type
ALTER TABLE data_subject_request ADD COLUMN email_verified_automatically BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE data_subject_request SET email_verified_automatically = email_verified;

ALTER TABLE data_subject_request
  ADD COLUMN email_verified_manually BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN email_verified_manually_by UUID,
  ADD CONSTRAINT data_subject_request_user_id_fkey FOREIGN KEY (email_verified_manually_by) REFERENCES polaris_user(id);