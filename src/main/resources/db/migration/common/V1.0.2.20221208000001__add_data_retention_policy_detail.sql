-- POLARIS-1957 - data retention policy
CREATE TABLE data_retention_policy_detail (
  organization_id UUID NOT NULL,
  type VARCHAR(100) NOT NULL,
  description TEXT NOT NULL,

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  PRIMARY KEY (organization_id, type)
);

CREATE INDEX data_retention_policy_detail_organization_id_idx ON data_retention_policy_detail (organization_id);

CREATE TABLE data_retention_policy_detail_pic (
  organization_id UUID NOT NULL,
  type VARCHAR(100) NOT NULL,
  pic_id UUID NOT NULL,

  FOREIGN KEY (organization_id, type) REFERENCES data_retention_policy_detail (organization_id, type),
  FOREIGN KEY (pic_id) REFERENCES personal_information_category (id)
);

CREATE INDEX data_retention_policy_detail_pic_organization_id_idx ON data_retention_policy_detail_pic (organization_id);