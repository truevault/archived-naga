CREATE TABLE default_processing_activity (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  name varchar NOT NULL,
  slug varchar UNIQUE,
  mandatory_lawful_bases TEXT,
  optional_lawful_bases TEXT,
  deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL
);

CREATE TABLE processing_activity (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id UUID NOT NULL,
  name varchar NOT NULL,
  lawful_bases TEXT,
  default_processing_activity_id UUID,

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  FOREIGN KEY (default_processing_activity_id) REFERENCES default_processing_activity (id)
);