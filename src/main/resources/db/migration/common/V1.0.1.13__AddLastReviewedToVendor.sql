ALTER TABLE service
  ADD COLUMN last_reviewed
    timestamp with time zone;

ALTER TABLE service
  ALTER COLUMN last_reviewed
  SET DEFAULT CURRENT_TIMESTAMP;

update service set last_reviewed = '2021-06-30';