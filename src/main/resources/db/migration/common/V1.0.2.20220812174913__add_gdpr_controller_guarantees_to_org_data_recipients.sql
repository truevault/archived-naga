ALTER TABLE organization_data_recipient
  ADD COLUMN gdpr_controller_guarantee_url TEXT,
  ADD COLUMN gdpr_controller_guarantee_file_name TEXT,
  ADD COLUMN gdpr_controller_guarantee_file_key TEXT,
  ADD COLUMN gdpr_contact_processor_status TEXT;
