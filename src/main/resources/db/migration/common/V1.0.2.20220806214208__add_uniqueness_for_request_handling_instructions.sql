-- Remove the bad version of the index
DROP INDEX idx_unique_request_handling_instructions;

-- Add the correct version of this index
CREATE UNIQUE INDEX idx_unique_request_handling_instructions
  ON request_handling_instructions (organization_id, organization_service_id, request_type)
  WHERE deleted_at IS NULL;

