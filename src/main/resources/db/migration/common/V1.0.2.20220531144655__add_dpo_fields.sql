ALTER TABLE privacy_center
  ADD COLUMN dpo_officer_name VARCHAR,
  ADD COLUMN dpo_officer_email VARCHAR,
  ADD COLUMN dpo_officer_phone VARCHAR,
  ADD COLUMN dpo_officer_address VARCHAR;