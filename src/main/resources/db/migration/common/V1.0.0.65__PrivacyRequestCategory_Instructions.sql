ALTER TABLE personal_information_category ADD column is_sensitive BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE personal_information_category SET is_sensitive = TRUE
WHERE name IN (
  'Social security number',
  'Driver''s license number',
  'California ID card number',
  'Passport number',
  'Tax ID number',
  'Military ID number',
  'Bank or other financial account number',
  'Account security or access credentials',
  'Fingerprints',
  'Face, hand, palm prints or vein patterns',
  'Voice recordings',
  'DNA (deoxyribonucleic acid)',
  'Imagery of the iris or retina',
  'Keystroke patterns or rhythms',
  'Sleep, health, or exercise data',
  'Health insurance or medical ID number'
);

ALTER TABLE organization_data_subject_type_personal_information_category
  ADD COLUMN is_stored_internally BOOLEAN DEFAULT FALSE,
  ADD COLUMN is_accessible BOOLEAN DEFAULT TRUE;

ALTER TABLE organization_service_personal_information_category
  ADD COLUMN is_retrieval_source BOOLEAN DEFAULT FALSE;

ALTER TABLE data_subject_type ADD COLUMN inaccessible_records_description VARCHAR;
