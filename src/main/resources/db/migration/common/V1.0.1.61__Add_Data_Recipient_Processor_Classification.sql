ALTER TABLE organization_data_recipient
  ADD COLUMN gdpr_processor_setting VARCHAR,
  ADD COLUMN gdpr_processor_guarantee_url VARCHAR,
  ADD COLUMN gdpr_processor_guarantee_file_name TEXT,
  ADD COLUMN gdpr_processor_guarantee_file_key VARCHAR;