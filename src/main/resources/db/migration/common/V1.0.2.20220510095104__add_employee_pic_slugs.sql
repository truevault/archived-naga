UPDATE personal_information_category SET pic_key = 'medical-information' WHERE name = 'Medical information';
UPDATE personal_information_category SET pic_key = 'protected-class-medical-condition' WHERE name = 'Medical condition';
UPDATE personal_information_category SET pic_key = 'protected-class-age' WHERE name = 'Age';
UPDATE personal_information_category SET pic_key = 'protected-class-sex-gender' WHERE name = 'Sex or gender';
UPDATE personal_information_category SET pic_key = 'protected-class-genetic-info' WHERE name = 'Genetic information';
UPDATE personal_information_category SET pic_key = 'protected-class-familial-status' WHERE name = 'Familial status';
