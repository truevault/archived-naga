-- POLARIS-1875, POLARIS-2080 - Drop column previously renamed
ALTER TABLE organization_data_recipient DROP COLUMN hidden_from_manual_settings;
