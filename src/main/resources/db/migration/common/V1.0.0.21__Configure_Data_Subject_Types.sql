CREATE TABLE organization_data_subject_type_settings (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    data_subject_type_id UUID NOT NULL,
    enabled BOOLEAN NOT NULL DEFAULT TRUE,
    privacy_center_enabled BOOLEAN NOT NULL DEFAULT TRUE,

    FOREIGN KEY (organization_id) REFERENCES organization (id),
    FOREIGN KEY (data_subject_type_id) REFERENCES data_subject_type (id)
);
CREATE UNIQUE INDEX idx_organization_data_subject_type_settings ON organization_data_subject_type_settings (organization_id, data_subject_type_id);