-- POLARIS-2771 - Privacy Center copy updates

UPDATE personal_information_category
  SET name = 'Inferences created from other personal information collected'
  WHERE name = 'Inferences from other data';