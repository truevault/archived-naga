ALTER TABLE data_subject_type DROP COLUMN parent_data_subject_type_id;
ALTER TABLE data_subject_type ADD COLUMN enabled BOOLEAN NOT NULL DEFAULT true;
ALTER TABLE data_subject_type ADD COLUMN privacy_center_enabled BOOLEAN NOT NULL DEFAULT true;

-- Add system-wide types to each organization as specific types
INSERT INTO data_subject_type (organization_id, type)
SELECT o.id, dst.type
FROM data_subject_type dst
    CROSS JOIN organization o
WHERE dst.organization_id IS NULL;

-- Migrate all references to system-wide types to their org-specific counterparts
UPDATE data_subject_request AS dsr SET data_subject_type_id = dst_new.id
FROM data_subject_type dst_old
    JOIN data_subject_type dst_new
        ON dst_old.type = dst_new.type
WHERE dst_old.id = dsr.data_subject_type_id
    AND dsr.organization_id = dst_new.organization_id
    AND dst_old.organization_id IS NULL;

UPDATE organization_service_data_subject_type AS osdst SET data_subject_type_id = dst_new.id
FROM organization_service os, data_subject_type dst_old
    JOIN data_subject_type dst_new
        ON dst_old.type = dst_new.type
WHERE dst_old.id = osdst.data_subject_type_id
    AND os.id = osdst.organization_service_id
    AND os.organization_id = dst_new.organization_id
    AND dst_old.organization_id IS NULL;

UPDATE processing_instructions AS pi SET data_subject_type_id = dst_new.id
FROM data_subject_type dst_old
    JOIN data_subject_type dst_new
        ON dst_old.type = dst_new.type
WHERE dst_old.id = pi.data_subject_type_id
    AND pi.organization_id = dst_new.organization_id
    AND dst_old.organization_id IS NULL;

UPDATE organization_data_subject_type_settings AS odsts SET data_subject_type_id = dst_new.id
FROM data_subject_type dst_old
    JOIN data_subject_type dst_new
        ON dst_old.type = dst_new.type
WHERE dst_old.id = odsts.data_subject_type_id
    AND odsts.organization_id = dst_new.organization_id
    AND dst_old.organization_id IS NULL;

UPDATE organization_data_subject_type_business_purpose AS odstbp SET data_subject_type_id = dst_new.id
FROM data_subject_type dst_old
    JOIN data_subject_type dst_new
        ON dst_old.type = dst_new.type
WHERE dst_old.id = odstbp.data_subject_type_id
    AND odstbp.organization_id = dst_new.organization_id
    AND dst_old.organization_id IS NULL;

UPDATE organization_data_subject_type_personal_information_category AS odstpic SET data_subject_type_id = dst_new.id
FROM data_subject_type dst_old
    JOIN data_subject_type dst_new
        ON dst_old.type = dst_new.type
WHERE dst_old.id = odstpic.data_subject_type_id
    AND odstpic.organization_id = dst_new.organization_id
    AND dst_old.organization_id IS NULL;

UPDATE organization_data_subject_type_personal_information_source AS odstpis SET data_subject_type_id = dst_new.id
FROM data_subject_type dst_old
    JOIN data_subject_type dst_new
        ON dst_old.type = dst_new.type
WHERE dst_old.id = odstpis.data_subject_type_id
    AND odstpis.organization_id = dst_new.organization_id
    AND dst_old.organization_id IS NULL;

-- Migrate enabled flags to the data_subject_type table
UPDATE data_subject_type AS dst SET enabled = odsts.enabled, privacy_center_enabled = odsts.privacy_center_enabled
FROM organization_data_subject_type_settings odsts
WHERE odsts.data_subject_type_id = dst.id;

-- Drop now unused settings table
DROP TABLE organization_data_subject_type_settings;

-- Delete unused system types
DELETE FROM data_subject_type WHERE organization_id IS NULL;
