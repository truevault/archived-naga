ALTER TABLE collection_group
  ADD COLUMN gdpr_eea_uk_data_collection BOOLEAN;

ALTER TABLE vendor
  ADD COLUMN gdpr_has_scc_recommendation BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN gdpr_scc_documentation_url TEXT;

ALTER TABLE organization_data_recipient
  ADD COLUMN gdpr_has_scc_setting BOOLEAN,
  ADD COLUMN gdpr_scc_url VARCHAR,
  ADD COLUMN gdpr_scc_file_name TEXT,
  ADD COLUMN gdpr_scc_file_key VARCHAR;