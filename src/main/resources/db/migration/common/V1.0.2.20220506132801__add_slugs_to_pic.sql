UPDATE personal_information_category SET pic_key = 'name' WHERE name = 'Name';
UPDATE personal_information_category SET pic_key = 'email-address' WHERE name = 'Email address';
UPDATE personal_information_category SET pic_key = 'postal-address' WHERE name = 'Postal address';
UPDATE personal_information_category SET pic_key = 'phone-number' WHERE name = 'Telephone number';
UPDATE personal_information_category SET pic_key = 'credit-debit-card-number' WHERE name = 'Credit card or debit card number';
UPDATE personal_information_category SET pic_key = 'purchases' WHERE name = 'Purchases';

UPDATE personal_information_category SET pic_key = 'ssn' WHERE name = 'Social security number';
UPDATE personal_information_category SET pic_key = 'passport-number' WHERE name = 'Passport number';
UPDATE personal_information_category SET pic_key = 'driver-license-number' WHERE name = 'Driver''s license number';
UPDATE personal_information_category SET pic_key = 'california-id-number' WHERE name = 'California ID card number';
UPDATE personal_information_category SET pic_key = 'military-id-number' WHERE name = 'Military ID number';
UPDATE personal_information_category SET pic_key = 'tax-id-number' WHERE name = 'Tax ID number';

UPDATE personal_information_category SET pic_key = 'fingerprints' WHERE name = 'Fingerprints';
UPDATE personal_information_category SET pic_key = 'face-hand-palm-vein-patterns' WHERE name = 'Face, hand, palm prints or vein patterns';
UPDATE personal_information_category SET pic_key = 'voice-recordings' WHERE name = 'Voice recordings';
UPDATE personal_information_category SET pic_key = 'dna' WHERE name = 'DNA (deoxyribonucleic acid)';
UPDATE personal_information_category SET pic_key = 'iris-retina-imagery' WHERE name = 'Imagery of the iris or retina';
UPDATE personal_information_category SET pic_key = 'keystroke-patterns' WHERE name = 'Keystroke patterns or rhythms';
UPDATE personal_information_category SET pic_key = 'health-data' WHERE name = 'Sleep, health, or exercise data';

UPDATE personal_information_category SET deprecated = true WHERE name = 'Name or alias';
UPDATE personal_information_category SET deprecated = true WHERE name = 'Alias';
UPDATE personal_information_category SET deprecated = true WHERE name = 'Other identifier';

UPDATE pic_group SET group_key = 'online-identifiers' WHERE name = 'Online Identifiers';
UPDATE pic_group SET group_key = 'internet-activity' WHERE name = 'Internet Activity';
UPDATE pic_group SET group_key = 'geolocation' WHERE name = 'Geolocation Information';
UPDATE pic_group SET group_key = 'inferences' WHERE name = 'Inferences';
UPDATE pic_group SET group_key = 'personal-identifiers' WHERE name = 'Personal Identifiers'