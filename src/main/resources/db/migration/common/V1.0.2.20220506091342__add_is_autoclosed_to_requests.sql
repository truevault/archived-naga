ALTER TABLE data_subject_request
ADD COLUMN is_autoclosed BOOLEAN NULL;

UPDATE data_subject_request
SET is_autoclosed = false;

ALTER TABLE data_subject_request
ALTER COLUMN is_autoclosed
SET NOT NULL;