-- Category group updates
UPDATE personal_information_category_group
  SET name = 'Internet Activity'
  WHERE id = 'fbc84cd9-37d7-473e-a134-3d6ae5360dd7';

UPDATE personal_information_category_group
  SET name = 'Commercial and Financial Information'
  WHERE id = 'eafb566d-1d10-4ee3-8020-d78296ffedc2';

UPDATE personal_information_category_group
  SET name = 'Biometric', display_order = 5
  WHERE id = '2853077e-4c57-4da4-b296-0a1f837dfb36';

UPDATE personal_information_category_group
  SET name = 'Geolocation', display_order = 6
  WHERE id = 'f757a822-e32f-44f4-b84f-f3247376c3ff';

UPDATE personal_information_category_group
  SET name = 'Other Physical and Audio', display_order = 7
  WHERE id = '5042db51-9ab6-445c-971b-86e23fc15125';

UPDATE personal_information_category_group
  SET name = 'Characteristics of Protected Classifications', display_order = 8
  WHERE id = 'f638f8ca-771b-4b21-9c50-3878f098c9e3';

INSERT INTO personal_information_category_group (id, name, display_order)
VALUES ('93af7008-25ce-4dde-bae9-b07db4569ba2', 'Other Sensitive Data', 9);

UPDATE personal_information_category_group
  SET name = 'Medical', display_order = 10
  WHERE id = 'f915c79c-a389-4653-82b2-2a53f13c33af';

UPDATE personal_information_category_group
  SET name = 'Professional', display_order = 11
  WHERE id = 'c14e3a58-56dd-46bd-9ed8-7020f873d324';

UPDATE personal_information_category_group
  SET display_order = 12
  WHERE id = 'ce680dfb-548e-4adf-b88e-f602bc96dfe5';

-- Category grouping updates
UPDATE personal_information_category
  SET group_id = 'eafb566d-1d10-4ee3-8020-d78296ffedc2'
  WHERE group_id = '23169390-d485-40a9-a8c1-2acca084efcc';

UPDATE personal_information_category
  SET group_id = '5042db51-9ab6-445c-971b-86e23fc15125'
  WHERE name = 'Physical characteristics or descriptions';

UPDATE personal_information_category
  SET group_id = 'eafb566d-1d10-4ee3-8020-d78296ffedc2'
  WHERE name = 'Signature';

UPDATE personal_information_category
  SET group_id = '894fdfa8-a251-40c2-bcfd-6d7b2a992159'
  WHERE name IN (
  'Social security number',
  'Driver''s license number',
  'Passport number',
  'Other government-issued identifier'
);

UPDATE personal_information_category
  SET group_id = 'c14e3a58-56dd-46bd-9ed8-7020f873d324'
  WHERE name = 'Education Information';

-- Category group cleanup
DELETE FROM personal_information_category_group
  WHERE id IN (
    '23169390-d485-40a9-a8c1-2acca084efcc',
    'd4c06e24-c2af-4995-a292-b611b701beec'
  );

-- New Categories
INSERT INTO personal_information_category (group_id, name, description) VALUES
('894fdfa8-a251-40c2-bcfd-6d7b2a992159', 'Name', ''),
('894fdfa8-a251-40c2-bcfd-6d7b2a992159', 'Alias', ''),
('894fdfa8-a251-40c2-bcfd-6d7b2a992159', 'California ID card number', ''),
('894fdfa8-a251-40c2-bcfd-6d7b2a992159', 'Tax ID number', ''),
('894fdfa8-a251-40c2-bcfd-6d7b2a992159', 'Military ID number', ''),
--('894fdfa8-a251-40c2-bcfd-6d7b2a992159', 'Other identifier', 'Other identifiers that do not fit into the above categories '),
('098cc93b-527b-4109-a7c8-804032cc3cd4', 'Device identifier', 'Device fingerprinting or other identifier '),
('098cc93b-527b-4109-a7c8-804032cc3cd4', 'Customer number', ''),
('098cc93b-527b-4109-a7c8-804032cc3cd4', 'Unique pseudonym', ''),
('098cc93b-527b-4109-a7c8-804032cc3cd4', 'User alias', ''),
('098cc93b-527b-4109-a7c8-804032cc3cd4', 'Online identifier', 'Username, social media handle'),
('098cc93b-527b-4109-a7c8-804032cc3cd4', 'Account login', 'If collected with a security or access code, password, or other access credentials'),
('098cc93b-527b-4109-a7c8-804032cc3cd4', 'Other persistent identifiers', 'Other identifiers used to identify a consumer or device that do not fit into the above categories'),
('fbc84cd9-37d7-473e-a134-3d6ae5360dd7', 'Browsing history', ''),
('fbc84cd9-37d7-473e-a134-3d6ae5360dd7', 'Search history', ''),
('fbc84cd9-37d7-473e-a134-3d6ae5360dd7', 'Interactions with websites, apps or ads', ''),
('fbc84cd9-37d7-473e-a134-3d6ae5360dd7', 'Email open and click-through rates', ''),
('eafb566d-1d10-4ee3-8020-d78296ffedc2', 'Purchases', 'Products or services purchased, obtained, or considered'),
('eafb566d-1d10-4ee3-8020-d78296ffedc2', 'Account security or access credentials', 'Including passcode, password, or other credentials, if collected along with credit or debit card or bank account number'),
('2853077e-4c57-4da4-b296-0a1f837dfb36', 'Fingerprints', ''),
('2853077e-4c57-4da4-b296-0a1f837dfb36', 'Face, hand, palm prints or vein patterns', ''),
('2853077e-4c57-4da4-b296-0a1f837dfb36', 'Voice recordings', ''),
('2853077e-4c57-4da4-b296-0a1f837dfb36', 'DNA (deoxyribonucleic acid)', ''),
('2853077e-4c57-4da4-b296-0a1f837dfb36', 'Imagery of the iris or retina', ''),
('2853077e-4c57-4da4-b296-0a1f837dfb36', 'Keystroke patterns or rhythms', ''),
('2853077e-4c57-4da4-b296-0a1f837dfb36', 'Sleep, health, or exercise data', ''),
('93af7008-25ce-4dde-bae9-b07db4569ba2', 'Union membership', ''),
('93af7008-25ce-4dde-bae9-b07db4569ba2', 'Racial or ethnic origin', ''),
('93af7008-25ce-4dde-bae9-b07db4569ba2', 'Religious or philosophical beliefs', ''),
('93af7008-25ce-4dde-bae9-b07db4569ba2', 'Contents of mail, email or text messages', 'When your business is not the intended recipient'),
('93af7008-25ce-4dde-bae9-b07db4569ba2', 'Genetic data', '');

-- Category Name Updates
UPDATE personal_information_category
  SET name = 'Unique personal identifier'
  WHERE name = 'Unique identifier';

UPDATE personal_information_category
  SET name = 'Other purchasing behavior'
  WHERE name = 'Purchasing tendencies';

UPDATE personal_information_category
  SET name = 'Bank or other financial account number'
  WHERE name = 'Bank account number';

UPDATE personal_information_category
  SET name = 'Geolocation data'
  WHERE name = 'Geolocation Data';

UPDATE personal_information_category
  SET name = 'Health insurance or medical ID number'
  WHERE name = 'Health insurance information';

UPDATE personal_information_category
  SET name = 'Inferences from other data'
  WHERE name = 'Inferences';

UPDATE personal_information_category
  SET name = 'IP address'
  WHERE name = 'IP Address';

UPDATE personal_information_category
  SET name = 'Other identifier'
  WHERE name = 'Other government-issued identifier';

UPDATE personal_information_category
  SET name = 'Gender identity or expression'
  WHERE name = 'Gender identity or gender expression';

UPDATE personal_information_category
  SET name = 'Education information'
  WHERE name = 'Education Information';

-- Category description, order, and deprecation updates
ALTER TABLE personal_information_category
  ADD COLUMN group_display_order INTEGER NOT NULL DEFAULT 0,
  ADD COLUMN deprecated BOOLEAN DEFAULT FALSE;

UPDATE personal_information_category SET deprecated = TRUE, group_display_order = 99
  WHERE LOWER(name) IN (
    'name or alias', 'other online identifier', 'social media handle',
    'internet or electronic activity', 'considered purchases', 'purchase history',
    'biometric information', 'membership in a trade union'
  );

UPDATE personal_information_category SET group_display_order = 1, description = '' WHERE name = 'Name';
UPDATE personal_information_category SET group_display_order = 2, description = '' WHERE name = 'Alias';
UPDATE personal_information_category SET group_display_order = 3, description = '' WHERE name = 'Email address';
UPDATE personal_information_category SET group_display_order = 4, description = '' WHERE name = 'Postal address';
UPDATE personal_information_category SET group_display_order = 5, description = '' WHERE name = 'Telephone number';
UPDATE personal_information_category SET group_display_order = 6, description = '' WHERE name = 'Social security number';
UPDATE personal_information_category SET group_display_order = 7, description = '' WHERE name = 'Driver''s license number';
UPDATE personal_information_category SET group_display_order = 8, description = '' WHERE name = 'California ID card number';
UPDATE personal_information_category SET group_display_order = 9, description = '' WHERE name = 'Passport number';
UPDATE personal_information_category SET group_display_order = 10, description = '' WHERE name = 'Tax ID number';
UPDATE personal_information_category SET group_display_order = 11, description = '' WHERE name = 'Military ID number';
UPDATE personal_information_category SET group_display_order = 12, description = 'Other identifiers that do not fit into the above categories' WHERE name = 'Other identifier';
UPDATE personal_information_category SET group_display_order = 1, description = 'If your website uses cookies, beacons, pixel tags, mobile ad identifiers, or similar technology to collect, disclose, or sell personal information' WHERE name = 'Unique personal identifier';
UPDATE personal_information_category SET group_display_order = 2, description = 'Device fingerprinting or other identifier' WHERE name = 'Device identifier';
UPDATE personal_information_category SET group_display_order = 3, description = '' WHERE name = 'IP address';
UPDATE personal_information_category SET group_display_order = 4, description = '' WHERE name = 'Customer number';
UPDATE personal_information_category SET group_display_order = 5, description = '' WHERE name = 'Unique pseudonym';
UPDATE personal_information_category SET group_display_order = 6, description = '' WHERE name = 'User alias';
UPDATE personal_information_category SET group_display_order = 7, description = 'Username, social media handle' WHERE name = 'Online identifier';
UPDATE personal_information_category SET group_display_order = 8, description = 'If collected with a security or access code, password, or other access credentials' WHERE name = 'Account login';
UPDATE personal_information_category SET group_display_order = 9, description = 'Other identifiers used to identify a consumer or device that do not fit into the above categories' WHERE name = 'Other persistent identifiers';
UPDATE personal_information_category SET group_display_order = 1, description = '' WHERE name = 'Browsing history';
UPDATE personal_information_category SET group_display_order = 2, description = '' WHERE name = 'Search history';
UPDATE personal_information_category SET group_display_order = 3, description = '' WHERE name = 'Interactions with websites, apps or ads';
UPDATE personal_information_category SET group_display_order = 4, description = '' WHERE name = 'Email open and click-through rates';
UPDATE personal_information_category SET group_display_order = 1, description = 'Products or services purchased, obtained, or considered' WHERE name = 'Purchases';
UPDATE personal_information_category SET group_display_order = 2, description = 'Other purchasing or consuming histories or tendencies' WHERE name = 'Other purchasing behavior';
UPDATE personal_information_category SET group_display_order = 3, description = '' WHERE name = 'Credit card or debit card number';
UPDATE personal_information_category SET group_display_order = 4, description = '' WHERE name = 'Bank or other financial account number';
UPDATE personal_information_category SET group_display_order = 5, description = 'Including passcode, password, or other credentials, if collected along with credit or debit card or bank account number' WHERE name = 'Account security or access credentials';
UPDATE personal_information_category SET group_display_order = 6, description = 'A name associated with a financial account such as a family trust' WHERE name = 'Account name';
UPDATE personal_information_category SET group_display_order = 7, description = '' WHERE name = 'Records of personal property';
UPDATE personal_information_category SET group_display_order = 8, description = '' WHERE name = 'Insurance policy number';
UPDATE personal_information_category SET group_display_order = 9, description = 'Or signature scan' WHERE name = 'Signature';
UPDATE personal_information_category SET group_display_order = 10, description = 'Other information that does not fit into the above categories' WHERE name = 'Other financial information';
UPDATE personal_information_category SET group_display_order = 1, description = '' WHERE name = 'Fingerprints';
UPDATE personal_information_category SET group_display_order = 2, description = '' WHERE name = 'Face, hand, palm prints or vein patterns';
UPDATE personal_information_category SET group_display_order = 3, description = 'From which an identifier template can be extracted' WHERE name = 'Voice recordings';
UPDATE personal_information_category SET group_display_order = 4, description = '' WHERE name = 'DNA (deoxyribonucleic acid)';
UPDATE personal_information_category SET group_display_order = 5, description = '' WHERE name = 'Imagery of the iris or retina';
UPDATE personal_information_category SET group_display_order = 6, description = '' WHERE name = 'Keystroke patterns or rhythms';
UPDATE personal_information_category SET group_display_order = 7, description = 'If they contain identifying information' WHERE name = 'Sleep, health, or exercise data';
UPDATE personal_information_category SET group_display_order = 1, description = 'Data generated by a consumer device used to locate a consumer within a geographic area that is equal to or less than the area of a circle with a radius of 1,850 feet' WHERE name = 'Geolocation data';
UPDATE personal_information_category SET group_display_order = 1, description = 'Size, height, weight, body measurements, eye color, hair color, skin color, or other physical characteristics or descriptions' WHERE name = 'Physical characteristics or descriptions';
UPDATE personal_information_category SET group_display_order = 2, description = 'Audio or voice recordings, if kept in a searchable manner and linked to an identifiable person' WHERE name = 'Audio information';
UPDATE personal_information_category SET group_display_order = 3, description = 'Photos, videos, or other visual information' WHERE name = 'Visual information';
UPDATE personal_information_category SET group_display_order = 4, description = 'EKG, ECG, or other electronic information' WHERE name = 'Electronic information';
UPDATE personal_information_category SET group_display_order = 5, description = '' WHERE name = 'Olfactory information';
UPDATE personal_information_category SET group_display_order = 6, description = 'Body thermal patterns or other thermal information' WHERE name = 'Thermal information';
UPDATE personal_information_category SET group_display_order = 1, description = '40 years or older' WHERE name = 'Age';
UPDATE personal_information_category SET group_display_order = 2, description = 'Includes pregnancy, childbirth, breastfeeding and/or related medical conditions' WHERE name = 'Sex or gender';
UPDATE personal_information_category SET group_display_order = 3, description = '' WHERE name = 'Race or color';
UPDATE personal_information_category SET group_display_order = 4, description = '' WHERE name = 'National origin';
UPDATE personal_information_category SET group_display_order = 5, description = '' WHERE name = 'Citizenship status';
UPDATE personal_information_category SET group_display_order = 6, description = '' WHERE name = 'Sexual orientation';
UPDATE personal_information_category SET group_display_order = 7, description = '' WHERE name = 'Gender identity or expression';
UPDATE personal_information_category SET group_display_order = 8, description = 'Includes religious dress and grooming practices' WHERE name = 'Religion';
UPDATE personal_information_category SET group_display_order = 9, description = '' WHERE name = 'Genetic information';
UPDATE personal_information_category SET group_display_order = 10, description = 'If you provide housing' WHERE name = 'Familial status';
UPDATE personal_information_category SET group_display_order = 11, description = '' WHERE name = 'Marital status';
UPDATE personal_information_category SET group_display_order = 12, description = 'Includes requests for family care leave, leave for serious health conditions, or pregnancy disability leave' WHERE name = 'Medical condition';
UPDATE personal_information_category SET group_display_order = 13, description = 'Includes mental and physical including HIV/AIDS, cancer, and genetic characteristics' WHERE name = 'Disability';
UPDATE personal_information_category SET group_display_order = 14, description = '' WHERE name = 'Military or veteran status';
UPDATE personal_information_category SET group_display_order = 15, description = '' WHERE name = 'Political affiliation or activities';
UPDATE personal_information_category SET group_display_order = 16, description = '' WHERE name = 'Status as a victim of sexual assault, domestic violence, or stalking';
UPDATE personal_information_category SET group_display_order = 1, description = '' WHERE name = 'Union membership';
UPDATE personal_information_category SET group_display_order = 2, description = '' WHERE name = 'Racial or ethnic origin';
UPDATE personal_information_category SET group_display_order = 3, description = '' WHERE name = 'Religious or philosophical beliefs';
UPDATE personal_information_category SET group_display_order = 4, description = 'When your business is not the intended recipient' WHERE name = 'Contents of mail, email or text messages';
UPDATE personal_information_category SET group_display_order = 5, description = '' WHERE name = 'Genetic data';
UPDATE personal_information_category SET group_display_order = 1, description = 'Health conditions, prescriptions' WHERE name = 'Medical information';
UPDATE personal_information_category SET group_display_order = 2, description = 'Policy or group number' WHERE name = 'Health insurance or medical ID number';
UPDATE personal_information_category SET group_display_order = 1, description = 'Non-public records from an educational agency or institution that directly or indirectly identify a student or the student’s family members' WHERE name = 'Education information';
UPDATE personal_information_category SET group_display_order = 2, description = 'Job history, professional certifications or affiliations, education history, degrees awarded, or similar professional information' WHERE name = 'Professional information';
UPDATE personal_information_category SET group_display_order = 3, description = 'Information collected about a person acting as a job applicant to, or employee or contractor of, your business, including information necessary to administer employment benefits' WHERE name = 'Employment-related information';
UPDATE personal_information_category SET group_display_order = 1, description = 'Information, data, assumptions, or conclusions drawn from any other personal information to create a profile about a consumer reflecting the consumer’s preferences, characteristics, psychological trends, predispositions, behavior, attitudes, intelligence, abilities, or aptitudes' WHERE name = 'Inferences from other data';