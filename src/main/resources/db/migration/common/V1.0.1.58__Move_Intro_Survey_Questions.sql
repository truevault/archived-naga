ALTER TABLE data_subject_type
  ADD COLUMN description TEXT DEFAULT NULL;

UPDATE organization_survey_question
SET survey_name = 'business-survey'
WHERE survey_name = 'intro-survey' AND
  (slug = 'business-buy-from-data-brokers' OR
   slug = 'business-physical-location' OR
   slug = 'business-employees-ca' OR
   slug = 'business-contractors-ca'
  );

UPDATE organization
SET get_compliant_progress = 'BUSINESS_SURVEY'
WHERE get_compliant_progress ='INTRO_SURVEY';