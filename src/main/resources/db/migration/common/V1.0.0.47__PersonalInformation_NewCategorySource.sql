INSERT INTO personal_information_category (group_id, name, description)
    -- Online Identifier Group
    VALUES ('098cc93b-527b-4109-a7c8-804032cc3cd4', 'Social media handle', '');

UPDATE personal_information_category
    SET description = 'E.g., MAC address, device fingerprints, username'
WHERE group_id = '098cc93b-527b-4109-a7c8-804032cc3cd4' and name = 'Other online identifier';

INSERT INTO personal_information_source (name) VALUES ('Data Analytics Provider');
