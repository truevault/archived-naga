-- POLARIS-2099 track whether a polaris user is able to delete data for a specific data recipient
ALTER TABLE organization_data_recipient ADD COLUMN data_deletability TEXT;