CREATE TABLE privacy_center (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  public_id VARCHAR(16) UNIQUE NOT NULL,
  organization_id UUID NOT NULL,
  name VARCHAR(50) NOT NULL,
  favicon_url VARCHAR(2048),
  privacy_policy_markdown TEXT,
  default_subdomain VARCHAR(255) UNIQUE NOT NULL,
  cloudfront_distribution VARCHAR(255),
  custom_url VARCHAR(2048) UNIQUE,
  custom_url_status VARCHAR(50),
  created_at TIMESTAMP DEFAULT NOW()
);
