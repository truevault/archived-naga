ALTER TABLE data_subject_request ADD confirmation_token uuid NOT NULL DEFAULT uuid_generate_v4();
CREATE UNIQUE INDEX confirmation_token ON data_subject_request (confirmation_token);
