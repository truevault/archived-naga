ALTER TABLE personal_information_category ADD COLUMN is_ccpa BOOLEAN DEFAULT TRUE;
ALTER TABLE personal_information_category ADD COLUMN is_gdpr BOOLEAN DEFAULT FALSE;

INSERT INTO pic_group (id, name, display_order, use_for_notice_disclosure, employee_display_order, group_key) VALUES
  ('8de5ed83-c979-418b-9b64-35c601f0960e', 'GDPR Sensitive Data', 14, true, 14, 'gdpr-sensitive-data');

INSERT INTO personal_information_category (name, group_id, description, group_display_order, is_sensitive, always_preserve_casing, pic_key, is_ccpa, is_gdpr) VALUES
  ('Genetics or biometric data', '8de5ed83-c979-418b-9b64-35c601f0960e', 'E.g., DNA, fingerprints, facial images, or other physical or physiological data that, through specific technical processing, can establish individual identity', 0, true, false, 'gdpr-genetics', false, true),
  ('Data revealing political, philosophical or religious beliefs, trade-union membership, or racial or ethnic origin', '8de5ed83-c979-418b-9b64-35c601f0960e', NULL, 1, true, false, 'gdpr-data-revealing-beliefs', false, true),
  ('Data concerning health or sex life or sexual orientation', '8de5ed83-c979-418b-9b64-35c601f0960e', NULL, 2, true, false, 'gdpr-health-or-sex-data', false, true),
  ('Personal data relating to criminal conviction and offenses', '8de5ed83-c979-418b-9b64-35c601f0960e', NULL, 3, true, false, 'gdpr-criminal-offenses', false, true);