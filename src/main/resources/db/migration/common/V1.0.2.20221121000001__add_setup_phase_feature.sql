-- POLARIS-2135 - add SetupPhase feature (for pre-onboarding setup)
INSERT INTO feature (name, description, enabled_by_default) VALUES ('SetupPhase', 'Setup Phase (for pre-onboarding setup)', false);
