UPDATE personal_information_category
SET
    description = 'From an educational institution that identifies a student or their family members',
    name = 'Non-public education records'
WHERE name = 'Education information';
