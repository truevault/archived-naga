ALTER TABLE data_request_type ADD COLUMN consumer_submit_visible BOOLEAN DEFAULT TRUE;

UPDATE data_request_type SET consumer_submit_visible = FALSE
WHERE slug IN ('RIGHT-TO-KNOW-SPECIFIC', 'CCPA-OPT-OUT');
