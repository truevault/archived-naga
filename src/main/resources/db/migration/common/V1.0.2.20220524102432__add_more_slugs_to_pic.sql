
UPDATE personal_information_category SET pic_key = 'inferences' WHERE name = 'Inferences from other data';
UPDATE personal_information_category SET pic_key = 'other-identifier' WHERE name = 'Other identifier';
UPDATE personal_information_category SET pic_key = 'professional-info' WHERE name = 'Professional information';
UPDATE personal_information_category SET pic_key = 'non-public-edu-records' WHERE name = 'Non-public education records';
UPDATE personal_information_category SET pic_key = 'employment-related-info' WHERE name = 'Employment-related information';

UPDATE pic_group SET group_key = 'protected-classifications' WHERE name = 'Characteristics of Protected Classifications';
UPDATE pic_group SET group_key = 'biometric-info' WHERE name = 'Biometric Information';
