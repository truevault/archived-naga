-- POLARIS-2414 Add tracking of usesCustomAudience directly to data recipient so that it can be updated from stay compliant in addition to get compliant

ALTER TABLE organization_data_recipient ADD COLUMN uses_custom_audience BOOLEAN DEFAULT false;