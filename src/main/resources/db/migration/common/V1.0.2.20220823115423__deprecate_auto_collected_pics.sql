-- POLARIS-1730
-- This script will mark all the old PICs in the PIC Groups Online Identifiers
-- and Internet Activity as deprecated, and create 2 new consolidate PICs for
-- those groups.

-- Deprecate the split-up categories for Internet Activity & Online Identifiers

UPDATE personal_information_category
SET deprecated = TRUE,
                 group_display_order = 99
WHERE LOWER(name) IN ('internet or electronic activity',
                      'social media handle',
                      'other online identifier',
                      'device identifier',
                      'customer number',
                      'unique pseudonym',
                      'user alias',
                      'online identifier',
                      'other persistent identifiers',
                      'unique personal identifier',
                      'account login',
                      'ip address',
                      'interactions with websites, apps or ads',
                      'email open and click-through rates',
                      'browsing history',
                      'search history');

 -- Create new unified categories for Internet Activity & Online Identifiers

INSERT INTO personal_information_category (name, group_id, description, is_sensitive, pic_key, is_ccpa, is_gdpr)
VALUES ('Online Identifiers',
          (SELECT id
           FROM pic_group
           WHERE name = 'Online Identifiers'), 'E.g., IP address; device identifier; or other online, persistent, or unique identifiers, such as browser cookies',
                                               FALSE,
                                               'online-identifiers',
                                               TRUE,
                                               FALSE);


INSERT INTO personal_information_category (name, group_id, description, is_sensitive, pic_key, is_ccpa, is_gdpr)
VALUES ('Internet Activity',
          (SELECT id
           FROM pic_group
           WHERE name = 'Internet Activity'), 'E.g., browsing or search history; interactions with websites, apps or ads; or email open and click-through rates',
                                              FALSE,
                                              'internet-activity',
                                              TRUE,
                                              FALSE);

