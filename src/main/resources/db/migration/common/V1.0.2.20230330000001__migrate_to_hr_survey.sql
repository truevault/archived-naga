-- POLARIS-2166 - We're combining all employment questions into a single survey, and removing them from the business and EEA/UK surveys

UPDATE organization_survey_question SET survey_name = 'hr-survey' WHERE survey_name = 'business-survey' AND slug IN ('business-employees-ca', 'business-contractors-ca', 'business-job-applicants-ca');
UPDATE organization_survey_question SET survey_name = 'hr-survey' WHERE survey_name = 'eea-uk-survey' AND slug IN ('employees-eea-uk');
