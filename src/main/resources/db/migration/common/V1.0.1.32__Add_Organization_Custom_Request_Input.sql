ALTER TABLE data_subject_request
  ADD COLUMN organization_custom_input VARCHAR DEFAULT NULL;

ALTER TABLE organization
  ADD COLUMN custom_field_enabled BOOLEAN DEFAULT false,
  ADD COLUMN custom_field_label VARCHAR DEFAULT NULL,
  ADD COLUMN custom_field_help VARCHAR DEFAULT NULL;