ALTER TABLE privacy_center ALTER COLUMN policy_last_updated DROP NOT NULL;
ALTER TABLE privacy_center ALTER COLUMN policy_last_updated DROP DEFAULT;