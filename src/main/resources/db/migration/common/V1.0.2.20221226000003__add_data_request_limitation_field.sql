-- POLARIS-1965 - Right To Limit requests
ALTER TABLE data_subject_request
ADD COLUMN limitation_finished BOOLEAN;