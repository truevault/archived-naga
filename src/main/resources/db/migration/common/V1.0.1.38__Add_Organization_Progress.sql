CREATE TABLE organization_progress (
  organization_id UUID NOT NULL,
  progress_key VARCHAR NOT NULL,
  progress_value VARCHAR NOT NULL,

  PRIMARY KEY (organization_id, progress_key)
);