-- Set the ONLINE_SHOPPERS autocreation slug for any org that doesn't currently have that set
UPDATE collection_group SET autocreation_slug = 'ONLINE_SHOPPERS' WHERE id IN
  (select oldest_consumer_group.id
  from organization o
  join (select distinct on (organization_id) cg.* from collection_group cg where cg.collection_group_type = 'BUSINESS_TO_CONSUMER' order by organization_id, created_at, name asc) as oldest_consumer_group
    on o.id = oldest_consumer_group.organization_id
  where (o.autocreated_collection_group_slugs is null or o.autocreated_collection_group_slugs = '[]') AND oldest_consumer_group.autocreation_slug IS NULL);

-- Set the B2B prospects autocreation slug for any org that doesn't currently have that set
UPDATE collection_group SET autocreation_slug = 'B2B_PROSPECTS' WHERE id IN
  (select emp.id
  from organization o
  join (select distinct on (organization_id) cg.* from collection_group cg where cg.collection_group_type = 'BUSINESS_TO_BUSINESS' AND cg.name = 'B2B Prospects' order by organization_id, created_at, name asc) as emp
    on o.id = emp.organization_id
  where (o.autocreated_collection_group_slugs is null or o.autocreated_collection_group_slugs = '[]') AND emp.autocreation_slug IS NULL);

-- Set the EEA_UK_EMPLOYEE autocreation slug for any org that doesn't currently have that set
UPDATE collection_group SET autocreation_slug = 'EEA_UK_EMPLOYEE' WHERE id IN
  (select eea_uk_emp.id
  from organization o
  join (select distinct on (organization_id) cg.* from collection_group cg where cg.collection_group_type = 'EMPLOYMENT' AND cg.name = 'EEA/UK Employees' order by organization_id, created_at, name asc) as eea_uk_emp
    on o.id = eea_uk_emp.organization_id
  where (o.autocreated_collection_group_slugs is null or o.autocreated_collection_group_slugs = '[]') AND eea_uk_emp.autocreation_slug IS NULL);

-- Set the California Employees autocreation slug for any org that doesn't currently have that set
UPDATE collection_group SET autocreation_slug = 'CA_EMPLOYEES' WHERE id IN
  (select emp.id
  from organization o
  join (select distinct on (organization_id) cg.* from collection_group cg where cg.collection_group_type = 'EMPLOYMENT' AND cg.name = 'California Employees' order by organization_id, created_at, name asc) as emp
    on o.id = emp.organization_id
  where (o.autocreated_collection_group_slugs is null or o.autocreated_collection_group_slugs = '[]') AND emp.autocreation_slug IS NULL);

-- Set the California Contractors autocreation slug for any org that doesn't currently have that set
UPDATE collection_group SET autocreation_slug = 'CA_CONTRACTORS' WHERE id IN
  (select emp.id
  from organization o
  join (select distinct on (organization_id) cg.* from collection_group cg where cg.collection_group_type = 'EMPLOYMENT' AND cg.name = 'California Contractors' order by organization_id, created_at, name asc) as emp
    on o.id = emp.organization_id
  where (o.autocreated_collection_group_slugs is null or o.autocreated_collection_group_slugs = '[]') AND emp.autocreation_slug IS NULL);

-- Set the California Job Applicants autocreation slug for any org that doesn't currently have that set
UPDATE collection_group SET autocreation_slug = 'CA_JOB_APPLICANTS' WHERE id IN
  (select emp.id
  from organization o
  join (select distinct on (organization_id) cg.* from collection_group cg where cg.collection_group_type = 'EMPLOYMENT' AND cg.name = 'California Job Applicants' order by organization_id, created_at, name asc) as emp
    on o.id = emp.organization_id
  where (o.autocreated_collection_group_slugs is null or o.autocreated_collection_group_slugs = '[]') AND emp.autocreation_slug IS NULL);


-- Update the autocreated collection group slugs values where the org's autocreated slugs are currently blank
UPDATE organization org SET autocreated_collection_group_slugs =
  (select '[' || STRING_AGG('"' || cg.autocreation_slug || '"', ',') || ']' as autocreated_collection_group_slugs
  from organization o
  inner join collection_group cg on cg.organization_id = o.id
  where cg.autocreation_slug is not null and o.id = org.id
  group by o.id)
WHERE (org.autocreated_collection_group_slugs is null or org.autocreated_collection_group_slugs = '[]');