-- POLARIS-2169: Add consumer context to data source

ALTER TABLE organization_data_source ADD COLUMN consumer_context VARCHAR(255);

CREATE INDEX organization_data_source_consumer_context_idx ON organization_data_source(consumer_context);

