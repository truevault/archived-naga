CREATE TABLE consumer_group_type (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  slug VARCHAR NOT NULL,
  name VARCHAR NOT NULL,
  description VARCHAR,
  display_order INT NOT NULL DEFAULT 0,

  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,

  UNIQUE(name)
);

INSERT INTO consumer_group_type (slug, name, description, display_order)
VALUES
  ('business-to-consumer', 'Business-to-Consumer', 'Business to Consumer', 1),
  ('business-to-business', 'Business-to-Business', 'Business to Business', 2),
  ('employment', 'Employment', 'Employment', 3);

ALTER TABLE data_subject_type ADD COLUMN consumer_group_type_id UUID;
ALTER TABLE data_subject_type ADD CONSTRAINT consumer_group_type_fkey
  FOREIGN KEY (consumer_group_type_id) REFERENCES consumer_group_type(id);
