ALTER TABLE organization_data_recipient ADD COLUMN automatically_classified BOOLEAN NOT NULL DEFAULT FALSE;
UPDATE organization_data_recipient SET automatically_classified = hidden_from_manual_settings;

-- In the future we will need to run:
-- ALTER TABLE organization_data_recipient DROP COLUMN hidden_from_manual_settings;
