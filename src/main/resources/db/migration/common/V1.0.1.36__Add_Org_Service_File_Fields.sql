ALTER TABLE organization_service
  ADD COLUMN tos_file_name TEXT NULL,
  ADD COLUMN tos_file_key VARCHAR(2048) NULL;