ALTER TABLE collection_group
  ADD COLUMN processing_regions TEXT,
  ADD COLUMN created_as_data_subject BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE collection_group SET processing_regions = '["UNITED_STATES"]';