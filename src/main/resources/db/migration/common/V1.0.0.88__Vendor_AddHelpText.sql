ALTER TABLE service
ADD COLUMN helptext_adnetwork_sharing TEXT DEFAULT NULL,
ADD COLUMN helptext_intentional_interaction_disclosure TEXT DEFAULT NULL;
