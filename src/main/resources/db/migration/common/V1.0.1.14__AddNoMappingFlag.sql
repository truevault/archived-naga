ALTER TABLE organization_service_data_subject_type RENAME TO deprecated_organization_service_data_subject_type;

CREATE TABLE organization_vendor_consumer_group (
    organization_id uuid NOT NULL,
    vendor_id UUID NOT NULL,
    consumer_group_id UUID NOT NULL,

    no_data_mapping BOOLEAN DEFAULT NULL,

    PRIMARY KEY(organization_id, vendor_id, consumer_group_id),

    FOREIGN KEY (organization_id) REFERENCES organization(id),
    FOREIGN KEY (vendor_id) REFERENCES service(id),
    FOREIGN KEY (consumer_group_id) REFERENCES data_subject_type(id)
);