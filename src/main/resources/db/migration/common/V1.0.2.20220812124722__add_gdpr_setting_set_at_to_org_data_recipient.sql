ALTER TABLE organization_data_recipient
  ADD COLUMN gdpr_processor_setting_set_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;
