ALTER TABLE organization
  ADD COLUMN request_processing_instructions_consumer_group TEXT,
  ADD COLUMN request_processing_instructions_opt_out TEXT;