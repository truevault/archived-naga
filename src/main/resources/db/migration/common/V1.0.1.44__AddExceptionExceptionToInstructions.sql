ALTER TABLE request_handling_instructions
  ADD COLUMN has_retention_exceptions BOOL;
