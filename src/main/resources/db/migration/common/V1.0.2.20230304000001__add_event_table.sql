-- POLARIS-1952 - event tracking
CREATE TABLE event(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id UUID,
  user_id UUID,

  event_name VARCHAR(255) NOT NULL,
  event_data TEXT,

  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  FOREIGN KEY (user_id) REFERENCES polaris_user (id)
);