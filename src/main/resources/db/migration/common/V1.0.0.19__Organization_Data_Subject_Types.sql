ALTER TABLE data_subject_type ADD COLUMN organization_id UUID;
ALTER TABLE data_subject_type ADD COLUMN parent_data_subject_type_id UUID;
ALTER TABLE data_subject_type DROP CONSTRAINT data_subject_type_type_key;
CREATE UNIQUE INDEX idx_data_subject_type_org_type ON data_subject_type (organization_id, type);
