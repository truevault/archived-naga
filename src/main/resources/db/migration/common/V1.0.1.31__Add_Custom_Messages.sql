
CREATE TABLE custom_message (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id UUID NOT NULL,
  message_type VARCHAR(50),
  custom_text VARCHAR(300),
  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  last_updated_at  TIMESTAMP WITH TIME ZONE,

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  UNIQUE(organization_id, message_type)
);
