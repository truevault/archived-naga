ALTER TABLE organization_service
  ADD COLUMN ccpa_is_selling BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN ccpa_is_cookie_sharing BOOLEAN NOT NULL DEFAULT FALSE;

ALTER TABLE organization_service_personal_information_category ADD COLUMN exchange_type VARCHAR NOT NULL DEFAULT 'DISCLOSURE';
