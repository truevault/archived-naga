-- POLARIS-2191 Add Deleted PIC to request handling instructions
ALTER TABLE request_handling_instructions ADD COLUMN deleted_pic_ids TEXT;
