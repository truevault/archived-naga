-- POLARIS-1962: Add affirmations to request handling instructions
ALTER TABLE request_handling_instructions ADD COLUMN exceptions_affirmed BOOLEAN NOT NULL DEFAULT FALSE;
