UPDATE data_request_type SET name = 'Request to Know - Categories'
WHERE slug = 'RIGHT-TO-KNOW-CATEGORIES';
UPDATE data_request_type SET name = 'Request to Know - Specific Information'
WHERE slug = 'RIGHT-TO-KNOW-SPECIFIC';
UPDATE data_request_type SET name = 'Request to Delete'
WHERE slug = 'RIGHT-TO-DELETION';
