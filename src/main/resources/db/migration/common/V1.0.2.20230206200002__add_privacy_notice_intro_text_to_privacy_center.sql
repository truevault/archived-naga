-- POLARIS-2416 Customize the intro text for the various notice sections
ALTER TABLE privacy_center
    ADD COLUMN ccpa_privacy_notice_intro_text TEXT,
    ADD COLUMN vcdpa_privacy_notice_intro_text TEXT,
    ADD COLUMN gdpr_privacy_notice_intro_text TEXT;
