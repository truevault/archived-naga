-- POLARIS-2903
--- UPDATE all privacy center dates to 6-30-2023 for CT and CO laws
UPDATE privacy_center as pc
  SET
    policy_last_updated = TIMESTAMP '2023-06-30 11:00:00+00'
  WHERE pc.policy_last_updated <= TIMESTAMP '2023-06-30' AND
	pc.organization_id in (
		SELECT o.id
		FROM organization as o
		WHERE
      o.name NOT ILIKE '%Deprecated%' AND
      o.name NOT ILIKE '%Test%' AND
      o.name NOT ILIKE '%DELETE%' AND
      o.name NOT ILIKE 'ZZZ %' AND
      o.name NOT ILIKE 'Z %' AND
      o.name NOT ILIKE '%Admin%' AND
      o.deleted_at IS NULL
	);
