ALTER TABLE organization
  ADD COLUMN training_complete boolean,
  ADD COLUMN walkthrough_email_sent boolean
