-- POLARIS-2451 - Add more editable introductory sections

CREATE TABLE privacy_center_custom_text(
  privacy_center_id UUID NOT NULL,
  slug VARCHAR(255) NOT NULL,
  value TEXT NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,

  PRIMARY KEY(privacy_center_id, slug),
  FOREIGN KEY (privacy_center_id) REFERENCES privacy_center (id)
);

INSERT INTO privacy_center_custom_text (privacy_center_id, slug, value)
SELECT id as privacy_center_id, 'ccpa-intro' as slug, ccpa_privacy_notice_intro_text as value
FROM privacy_center
WHERE ccpa_privacy_notice_intro_text IS NOT NULL;

INSERT INTO privacy_center_custom_text (privacy_center_id, slug, value)
SELECT id as privacy_center_id, 'vcdpa-intro' as slug, vcdpa_privacy_notice_intro_text as value
FROM privacy_center
WHERE vcdpa_privacy_notice_intro_text IS NOT NULL;

INSERT INTO privacy_center_custom_text (privacy_center_id, slug, value)
SELECT id as privacy_center_id, 'gdpr-intro' as slug, gdpr_privacy_notice_intro_text as value
FROM privacy_center
WHERE gdpr_privacy_notice_intro_text IS NOT NULL;

INSERT INTO privacy_center_custom_text (privacy_center_id, slug, value)
SELECT pc.id as privacy_center_id, 'retention-intro' as slug, CONCAT('<p>', o.gdpr_retention_policy, '</p>') as value
FROM organization o
INNER JOIN privacy_center pc ON pc.organization_id = o.id
WHERE o.gdpr_retention_policy IS NOT NULL AND pc.deleted_at IS NULL;