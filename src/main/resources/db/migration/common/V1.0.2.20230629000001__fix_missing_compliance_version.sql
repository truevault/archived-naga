-- Sets a default compliance version on orgs in Stay Compliant.
UPDATE organization SET compliance_version = 'c2022_1' WHERE id IN (
	SELECT
		o.id
	FROM organization o
	LEFT JOIN (
		SELECT
			of.organization_id as org_id,
			of.enabled as get_compliant_phase
		FROM organization_feature of
		INNER JOIN feature f
			ON of.feature_id = f.id
		WHERE
			f.name = 'GetCompliantPhase'
	) ff
		ON ff.org_id = o.id
	LEFT JOIN (
		SELECT
			of.organization_id as org_id,
			of.enabled as setup_phase
		FROM organization_feature of
		INNER JOIN feature f
			ON of.feature_id = f.id
		WHERE
			f.name = 'SetupPhase'
	) ff2
		ON ff2.org_id = o.id
	WHERE
		o.compliance_version IS NULL AND
		o.name NOT ILIKE '%Deprecated%' AND
		o.name NOT ILIKE '%Test%' AND
		o.name NOT ILIKE '%DELETE%' AND
		o.name NOT ILIKE 'ZZZ %' AND
		o.name NOT ILIKE 'Z %' AND
		o.name NOT ILIKE '%Admin%' AND
		(ff.get_compliant_phase = FALSE) AND
		(ff2.setup_phase IS NULL OR ff2.setup_phase = FALSE)
);
