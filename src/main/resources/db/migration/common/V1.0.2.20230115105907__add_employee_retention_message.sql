-- POLARIS-1510: Add optional employee retention period to employee notice
ALTER TABLE organization ADD COLUMN employee_privacy_notice_retention_period TEXT;