ALTER TABLE data_subject_type
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE consumer_group_mapping_progress
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE data_subject_request_consumer_groups
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE organization_data_subject_type_business_purpose
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE organization_data_subject_type_personal_information_category
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE organization_vendor_consumer_group
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;
