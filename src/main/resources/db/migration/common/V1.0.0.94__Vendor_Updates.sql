ALTER TABLE service
  ADD COLUMN vendor_key VARCHAR(255),
  ADD COLUMN aliases TEXT,
  ADD COLUMN ccpa_request_to_know_link TEXT,
  ADD COLUMN ccpa_request_to_delete_link TEXT;

COMMENT ON COLUMN service.vendor_key IS 'this key is used by the application code to identity this vendor';