ALTER TABLE data_subject_request ADD COLUMN request_original_due_date TIMESTAMP WITH TIME ZONE;
UPDATE data_subject_request set request_original_due_date = request_due_date;
ALTER TABLE data_subject_request ALTER COLUMN request_original_due_date set NOT NULL;
