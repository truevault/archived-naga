-- POLARIS-2318 - Allow users to show the header for privacy center sections

ALTER TABLE privacy_center_policy_section ADD COLUMN show_header BOOLEAN NOT NULL DEFAULT FALSE;