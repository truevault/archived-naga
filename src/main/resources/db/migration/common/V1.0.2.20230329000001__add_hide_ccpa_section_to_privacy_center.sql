-- POLARIS-2599 - add hide_ccpa_section flag to privacy_center (introduced for Sunday Riley UK)
ALTER TABLE privacy_center ADD COLUMN hide_ccpa_section BOOLEAN NOT NULL DEFAULT FALSE;
