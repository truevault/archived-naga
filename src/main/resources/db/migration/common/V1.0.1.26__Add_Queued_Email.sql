
CREATE TABLE queued_email (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  data_subject_request_id UUID,
  recipient_email_address TEXT,
  sender_email_address TEXT,
  subject TEXT,
  message TEXT,
  status VARCHAR(50) ,
  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  last_updated_at  TIMESTAMP WITH TIME ZONE,


  FOREIGN KEY (data_subject_request_id) REFERENCES data_subject_request (id)
);