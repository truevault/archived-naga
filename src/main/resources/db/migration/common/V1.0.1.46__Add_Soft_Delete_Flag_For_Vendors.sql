ALTER TABLE organization_data_recipient
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE consumer_group_organization_vendor
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE platform_app_installation
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE privacy_request_retention_reason
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE request_handling_instructions
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

ALTER TABLE vendor
  ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL;

  -- drop the unique constraint
ALTER TABLE organization_data_recipient
  DROP CONSTRAINT organization_service_organization_id_service_id_key;
