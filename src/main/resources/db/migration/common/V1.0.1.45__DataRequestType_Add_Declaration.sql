UPDATE data_request_type
  SET declaration_text = 'I declare under penalty of perjury that the above information is true and correct and that I am the person whose personal information is the subject of this request.'
  WHERE slug = 'RIGHT-TO-KNOW';
