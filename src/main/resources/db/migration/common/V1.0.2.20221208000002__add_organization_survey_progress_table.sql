-- POLARIS-2102 Track generic survey progress
CREATE TABLE organization_survey_progress(
  organization_id UUID NOT NULL,
  survey_slug VARCHAR(255) NOT NULL,

  current_step VARCHAR(255),

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  PRIMARY KEY(organization_id, survey_slug)
);

CREATE INDEX organization_survey_progress_organization_id_idx ON organization_survey_progress(organization_id, survey_slug);

INSERT INTO organization_survey_progress(organization_id, survey_slug, current_step)
SELECT id as organization_id, 'onboarding' as survey_slug, get_compliant_progress as current_step
FROM organization
WHERE get_compliant_progress IS NOT NULL;