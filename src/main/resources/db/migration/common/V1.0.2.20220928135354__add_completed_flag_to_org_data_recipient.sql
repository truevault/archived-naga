ALTER TABLE organization_data_recipient
ADD COLUMN completed BOOLEAN NOT NULL DEFAULT TRUE;