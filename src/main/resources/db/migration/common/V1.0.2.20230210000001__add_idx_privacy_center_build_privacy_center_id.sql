-- POLARIS-2314/POLARIS-2466 - add index on privacy_center_build.privacy_center_id
CREATE INDEX idx_privacy_center_build_privacy_center_id ON privacy_center_build (privacy_center_id);
