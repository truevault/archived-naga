-- POLARIS-2116 - rename "Consumer Directly" to "Consumer"

UPDATE vendor SET name = 'Consumer' WHERE vendor_key = 'source-consumer-directly' AND name = 'Consumer Directly';