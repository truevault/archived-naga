-- POLARIS-2416 Customize the intro text for the privacy notice for each consumer group
ALTER TABLE collection_group ADD COLUMN privacy_notice_intro_text TEXT NULL;