CREATE TABLE login_activity (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  email VARCHAR(255) NOT NULL,
  success BOOLEAN NOT NULL,
  user_id UUID DEFAULT NULL,
  organization_id UUID DEFAULT NULL,

  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,

  FOREIGN KEY (user_id) REFERENCES polaris_user (id)
);
CREATE INDEX ON login_activity (email);
CREATE INDEX ON login_activity (user_id);
CREATE INDEX ON login_activity (organization_id);
CREATE INDEX ON login_activity (email, user_id, organization_id);
