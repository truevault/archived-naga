ALTER TABLE polaris_user
ADD COLUMN active_organization_id UUID DEFAULT NULL,
ADD CONSTRAINT fk_polaris_user_active_organization_id  FOREIGN KEY (active_organization_id) REFERENCES organization (id)
