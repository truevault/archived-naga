ALTER TABLE vendor
  ADD COLUMN is_migration_only  BOOLEAN NOT NULL DEFAULT FALSE;

 UPDATE vendor 
 	SET is_migration_only = TRUE 
 WHERE vendor_key in 
 	( 'source-ad-networks',  
 	  'source-data-analytics-provider',
 	  'source-operating-systems',
 	  'source-social-networks',
 	  'source-data-brokers'
 	);