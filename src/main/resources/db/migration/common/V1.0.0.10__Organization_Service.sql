
-- join between (organizations, data subject type) and system_id
CREATE TABLE organization_service (
  id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,

  organization_id UUID NOT NULL,
--   data_subject_type_id UUID,
  service_id UUID NOT NULL,
  service_type_id UUID,

  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,

  UNIQUE (organization_id, service_id),
  FOREIGN KEY (organization_id) REFERENCES organization (id),
--   FOREIGN KEY (data_subject_type_id) REFERENCES data_subject_type (id),
  FOREIGN KEY (service_id) REFERENCES service (id),
  FOREIGN KEY (service_type_id) REFERENCES service_type (id)
);

-- join org_services to service_business_purpose
CREATE TABLE organization_service_business_purpose (
    id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    organization_service_id INTEGER NOT NULL,
    service_business_purpose_id UUID NOT NULL,

    FOREIGN KEY (organization_service_id) REFERENCES organization_service (id),
    FOREIGN KEY (service_business_purpose_id) REFERENCES service_business_purpose (id)
);

-- join org_services to service_business_purpose
CREATE TABLE organization_service_organization_team (
   organization_service_id INTEGER NOT NULL,
   organization_team_id UUID NOT NULL,

   FOREIGN KEY (organization_service_id) REFERENCES organization_service (id),
   FOREIGN KEY (organization_team_id) REFERENCES organization_team (id)
);

-- join org_services to data_subject_type
CREATE TABLE organization_service_data_subject_type (
    id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    organization_service_id INTEGER NOT NULL,
    data_subject_type_id UUID NOT NULL,

    request_type VARCHAR(50) NOT NULL DEFAULT 'ACCESS',
    request_action VARCHAR(50) NOT NULL,

    UNIQUE (organization_service_id, data_subject_type_id, request_type),
    FOREIGN KEY (organization_service_id) REFERENCES organization_service (id),
    FOREIGN KEY (data_subject_type_id) REFERENCES data_subject_type (id)
);

CREATE TABLE organization_service_personal_information_category (
    id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    organization_service_data_subject_type_id INTEGER NOT NULL,
    personal_information_category_id UUID NOT NULL,

    UNIQUE (organization_service_data_subject_type_id, personal_information_category_id),
    FOREIGN KEY (organization_service_data_subject_type_id) REFERENCES organization_service_data_subject_type (id),
    FOREIGN KEY (personal_information_category_id) REFERENCES personal_information_category (id)
);

CREATE TABLE organization_service_personal_information_source (
    id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    organization_service_data_subject_type_id INTEGER NOT NULL,
    personal_information_source_id UUID NOT NULL,

    UNIQUE (organization_service_data_subject_type_id, personal_information_source_id),
    FOREIGN KEY (organization_service_data_subject_type_id) REFERENCES organization_service_data_subject_type (id),
    FOREIGN KEY (personal_information_source_id) REFERENCES personal_information_source (id)
);