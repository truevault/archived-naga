CREATE TABLE task(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id UUID NOT NULL,

  task_key VARCHAR(50) NOT NULL,
  name VARCHAR NOT NULL,
  description TEXT,

  created_at  TIMESTAMP WITH TIME ZONE  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  due_at TIMESTAMP WITH TIME ZONE,
  completed_at TIMESTAMP WITH TIME ZONE,
  completed_by UUID,

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  FOREIGN KEY (completed_by) REFERENCES polaris_user (id)
);
