-- POLARIS-2191 - Add new do not contact options and reasons; clear old settings

-- add new thing for everyone with specific tasks
INSERT INTO task (organization_id, task_key, name, description, completion_type)
SELECT DISTINCT rhi.organization_id, 'UPDATE_DO_NOT_CONTACT_THIRD_PARTY', 'CPRA Regulation Changes: Review Third Party Settings', 'Recent regulatory changes require you to review and update your contact settings for Third Parties for which you previously selected “Do Not Contact.” Visit <a href="/organization/requestHandling/requestToDelete">Manage Request to Delete Settings</a> and click into the highlighted Data Recipients to review their settings.', 'MANUAL'
FROM request_handling_instructions rhi
INNER JOIN organization_data_recipient odr on odr.id = rhi.organization_service_id
INNER JOIN vendor v on v.id = odr.vendor_id
LEFT JOIN service_type t on t.id = v.service_type_id
WHERE
  rhi.delete_do_not_contact IS NOT NULL
  AND rhi.request_type = 'DELETE'
  AND (t.slug = 'third-party' OR v.data_recipient_type = 'third_party_recipient');

-- clear all prior "do not contact" settings (per JIRA)
UPDATE request_handling_instructions SET delete_do_not_contact = NULL;


ALTER TABLE request_handling_instructions
  ADD COLUMN do_not_contact_reason VARCHAR(50),
  ADD COLUMN do_not_contact_explanation TEXT;
