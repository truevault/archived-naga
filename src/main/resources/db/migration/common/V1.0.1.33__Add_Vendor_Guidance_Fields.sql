ALTER TABLE service
  ADD COLUMN general_guidance TEXT NULL,
  ADD COLUMN special_circumstances_guidance TEXT NULL,
  ADD COLUMN recommended_personal_information_categories TEXT NULL;