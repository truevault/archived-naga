CREATE TABLE cookie_recommendation (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT NOT NULL,
  domain TEXT NOT NULL,
  categories TEXT NOT NULL,
  vendor_id UUID REFERENCES vendor(id),
  notes TEXT NOT NULL default '',
  deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL
);

CREATE INDEX idx_cookie_recommendation_vendor ON cookie_recommendation (vendor_id);
