ALTER TABLE personal_information_category
ADD COLUMN always_preserve_casing BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE personal_information_category
SET always_preserve_casing = TRUE
WHERE name IN ('California ID card number', 'IP address', 'DNA (deoxyribonucleic acid)');
