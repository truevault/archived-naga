-- TODO: POLARIS-2443 Update the financial incentives to be stored/edited in a WSYIWYG

ALTER TABLE organization ADD COLUMN financial_incentive_details TEXT;

with u as (
  with summary as
  (
    select answer, organization_id
    from organization_survey_question
    where survey_name = 'consumer-collection-incentives-survey' and  slug = 'summary-of-financial-incentive'
  ) ,
  desc_of_incentive as
  (
    select answer, organization_id
    from organization_survey_question
    where survey_name = 'consumer-collection-incentives-survey' and slug = 'description-of-financial-incentive'
  ),
  desc_of_optin as
  (
    select answer, organization_id
    from organization_survey_question
    where survey_name = 'consumer-collection-incentives-survey' and slug = 'description-of-optin'
  ),
  withdraw as
  (
    select answer, organization_id
    from organization_survey_question
    where survey_name = 'consumer-collection-incentives-survey' and slug = 'right-to-withdraw'
  ),
  value_of_data as
  (
    select answer, organization_id
    from organization_survey_question
    where survey_name = 'consumer-collection-incentives-survey' and slug = 'value-of-consumer-data'
  )
  select
    CONCAT('<p>', CONCAT_WS(' ', summary.answer, desc_of_incentive.answer, desc_of_optin.answer, withdraw.answer, value_of_data.answer), '</p>') as result,
    summary.organization_id
  FROM summary
  LEFT JOIN desc_of_incentive on summary.organization_id = desc_of_incentive.organization_id
  LEFT JOIN desc_of_optin on summary.organization_id = desc_of_optin.organization_id
  LEFT JOIN withdraw on summary.organization_id = withdraw.organization_id
  LEFT JOIN value_of_data on summary.organization_id = value_of_data.organization_id
)
UPDATE organization
SET financial_incentive_details = u.result
FROM u
WHERE organization.id = u.organization_id;

