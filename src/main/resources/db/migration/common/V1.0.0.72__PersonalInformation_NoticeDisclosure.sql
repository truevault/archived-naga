ALTER TABLE personal_information_source
  ADD COLUMN disclosure_name VARCHAR;

UPDATE personal_information_source SET disclosure_name = 'You (the consumer)' where name = 'Consumer Directly';

ALTER TABLE personal_information_category_group
  ADD COLUMN use_for_notice_disclosure BOOLEAN DEFAULT TRUE;

UPDATE personal_information_category_group SET name = 'Biometric Information' WHERE name = 'Biometric';
UPDATE personal_information_category_group SET name = 'Physical and Audio Data' WHERE name = 'Other Physical and Audio';
UPDATE personal_information_category_group SET name = 'Geolocation Information' WHERE name = 'Geolocation';

UPDATE personal_information_category_group SET use_for_notice_disclosure = FALSE
WHERE name IN (
  'Geolocation Information',
  'Physical and Audio Data',
  'Medical',
  'Professional',
  'Inferences'
);
