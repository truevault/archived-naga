ALTER TABLE organization_service_data_subject_type DROP COLUMN request_type;
ALTER TABLE processing_instructions DROP COLUMN data_request_type;
ALTER TABLE data_subject_request DROP COLUMN request_type;
