UPDATE personal_information_category
SET name = 'Account Access Credentials', description='Exclude account information stored in a secure manner, such as one-way encrypted data'
WHERE pic_key = 'account-login';

