DROP TABLE collection_group_data_request_type;

UPDATE data_request_type SET slug = 'GDPR_RECTIFICATION' WHERE slug = 'RECTIFICATION';
UPDATE data_request_type SET slug = 'GDPR_ERASURE' WHERE slug = 'ERASURE';
UPDATE data_request_type SET slug = 'GDPR_ACCESS' WHERE slug = 'ACCESS';
UPDATE data_request_type SET slug = 'GDPR_OBJECTION' WHERE slug = 'OBJECTION';
UPDATE data_request_type SET slug = 'CCPA_OPT_OUT' WHERE slug = 'CCPA-OPT-OUT';
UPDATE data_request_type SET slug = 'CCPA_RIGHT_TO_KNOW_CATEGORIES' WHERE slug = 'RIGHT-TO-KNOW-CATEGORIES';
UPDATE data_request_type SET slug = 'CCPA_RIGHT_TO_DELETE' WHERE slug = 'RIGHT-TO-DELETION';
UPDATE data_request_type SET slug = 'CCPA_RIGHT_TO_KNOW' WHERE slug = 'RIGHT-TO-KNOW';

ALTER TABLE data_subject_request ADD COLUMN request_type VARCHAR;
ALTER TABLE legacy_data_subject_request ADD COLUMN request_type VARCHAR;
ALTER TABLE legacy_organization_service_collection_group ADD COLUMN request_type VARCHAR;
ALTER TABLE processing_instructions ADD COLUMN request_type VARCHAR;
ALTER TABLE request_template_request_type ADD COLUMN request_type VARCHAR;
ALTER TABLE data_request_type_request_state_help_article ADD COLUMN request_type VARCHAR;

UPDATE data_subject_request o SET request_type = t.slug FROM data_request_type t WHERE t.id = o.data_request_type_id;
UPDATE legacy_data_subject_request o SET request_type = t.slug FROM data_request_type t WHERE t.id = o.data_request_type_id;
UPDATE legacy_organization_service_collection_group o SET request_type = t.slug FROM data_request_type t WHERE t.id = o.data_request_type_id;
UPDATE processing_instructions o SET request_type = t.slug FROM data_request_type t WHERE t.id = o.data_request_type_id;
UPDATE request_template_request_type o SET request_type = t.slug FROM data_request_type t WHERE t.id = o.data_request_type_id;
UPDATE data_request_type_request_state_help_article o SET request_type = t.slug FROM data_request_type t WHERE t.id = o.data_request_type_id;

ALTER TABLE data_subject_request DROP COLUMN data_request_type_id;
ALTER TABLE legacy_data_subject_request DROP COLUMN data_request_type_id;
ALTER TABLE legacy_organization_service_collection_group DROP COLUMN data_request_type_id;
ALTER TABLE processing_instructions DROP COLUMN data_request_type_id;
ALTER TABLE request_template_request_type DROP COLUMN data_request_type_id;
ALTER TABLE data_request_type_request_state_help_article DROP COLUMN data_request_type_id;

ALTER TABLE data_request_type RENAME TO legacy_data_request_type;
ALTER TABLE request_template RENAME TO legacy_request_template;
ALTER TABLE request_template_request_state RENAME TO legacy_request_template_request_state;
ALTER TABLE request_template_request_type RENAME TO legacy_request_template_request_type;