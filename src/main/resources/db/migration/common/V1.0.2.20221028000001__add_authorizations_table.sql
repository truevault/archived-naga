ALTER TABLE organization
DROP COLUMN ga_service_account_token,
DROP COLUMN ga_service_account;

CREATE TABLE organization_auth_token(
  organization_id UUID,
  provider VARCHAR(255) NOT NULL,
  auth TEXT NOT NULL,
  refresh TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  expires_at TIMESTAMP WITH TIME ZONE,

  FOREIGN KEY (organization_id) REFERENCES organization (id),
  PRIMARY KEY (provider, organization_id)
);