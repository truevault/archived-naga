ALTER TABLE vendor
  ADD COLUMN gdpr_processor_recommendation VARCHAR(20),
  ADD COLUMN gdpr_processor_language_url TEXT,
  ADD COLUMN gdpr_dpa_applicable BOOLEAN,
  ADD COLUMN gdpr_dpa_url TEXT,
  ADD COLUMN gdpr_international_data_transfer_rules_apply BOOLEAN,
  ADD COLUMN gdpr_processing_purpose VARCHAR(255),
  ADD COLUMN gdpr_last_reviewed TIMESTAMP WITH TIME ZONE,
  ADD COLUMN gdpr_access_instructions TEXT;

INSERT INTO feature (name, description, enabled_by_default) VALUES ('GDPR', 'Does the organization subscribe to the GDPR feature set', false);
