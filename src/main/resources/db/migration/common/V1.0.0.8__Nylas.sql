-- We first set the default to sendgrid so any existing customers will use that, then we update the default
-- to nylas for all future customers.
ALTER TABLE organization
ADD COLUMN send_method VARCHAR(20) NOT NULL DEFAULT 'SENDGRID';

ALTER TABLE organization
ALTER COLUMN send_method SET DEFAULT 'NYLAS';

-- mailboxes associated with a given organization
CREATE TABLE organization_mailbox(
    id UUID PRIMARY KEY,
    organization_id UUID,

    status VARCHAR(50) NOT NULL,
    type VARCHAR(50) NOT NULL,
    mailbox VARCHAR(255) NOT NULL,

    nylas_access_token VARCHAR(255),
    nylas_account_id VARCHAR(255) UNIQUE NOT NULL,

    FOREIGN KEY (organization_id) REFERENCES organization (id)
)