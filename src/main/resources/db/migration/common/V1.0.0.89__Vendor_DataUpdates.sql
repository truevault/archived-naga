ALTER TABLE service
ADD COLUMN data_recipient_type VARCHAR(40) NOT NULL DEFAULT 'vendor';

ALTER TABLE organization_service
ADD COLUMN hidden_from_manual_settings BOOLEAN NOT NULL DEFAULT FALSE,
ADD COLUMN ccpa_is_sharing BOOLEAN NOT NULL DEFAULT FALSE;