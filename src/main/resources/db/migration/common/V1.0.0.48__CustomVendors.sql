ALTER TABLE service
    ADD COLUMN organization_id UUID,
    ADD COLUMN email VARCHAR,
    ALTER COLUMN url DROP NOT NULL;

UPDATE service_type set display_order = 1 where slug = 'service-provider';
UPDATE service_type set name = 'Third Party', tooltip_text = 'Third Party', display_order = 3 where slug = 'third-party';
UPDATE service_type set name = 'Contractor', slug = 'contractor', tooltip_text = 'Contractor', display_order = 2 where slug = 'certified-person';
