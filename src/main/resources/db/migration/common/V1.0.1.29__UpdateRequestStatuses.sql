update data_subject_request set state = 'VERIFY_CONSUMER' where state = 'CONSUMER_GROUP_SELECTION';


with u as (
  select dsr.id
  from data_subject_request as dsr
  join data_request_type as type on type.id = dsr.data_request_type_id
  where type.slug = 'CCPA-OPT-OUT'
  and (dsr.state = 'VERIFY_CONSUMER' or dsr.state='PROCESSING')
)
update data_subject_request
set state = 'REMOVE_CONSUMER'
from u
where data_subject_request.id = u.id;

with u as (
  select dsr.id
  from data_subject_request as dsr
  join data_request_type as type on type.id = dsr.data_request_type_id
  where type.slug = 'RIGHT-TO-KNOW'
  and (dsr.state='PROCESSING')
)
update data_subject_request
set state = 'RETRIEVE_DATA'
from u
where data_subject_request.id = u.id;

with u as (
  select dsr.id
  from data_subject_request as dsr
  join data_request_type as type on type.id = dsr.data_request_type_id
  where type.slug = 'RIGHT-TO-DELETION'
  and (dsr.state='PROCESSING')
)
update data_subject_request
set state = 'DELETE_INFO'
from u
where data_subject_request.id = u.id;