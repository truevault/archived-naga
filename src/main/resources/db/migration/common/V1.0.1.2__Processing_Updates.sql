CREATE TABLE data_subject_request_vendors (
  data_subject_request_id UUID NOT NULL,
  vendor_id UUID NOT NULL,

  processed BOOLEAN NOT NULL DEFAULT FALSE,
  processed_at TIMESTAMP WITH TIME ZONE,

  flow_down_processed BOOLEAN NOT NULL DEFAULT FALSE,
  flow_down_processed_at TIMESTAMP WITH TIME ZONE,

  PRIMARY KEY(data_subject_request_id, vendor_id),
  FOREIGN KEY (data_subject_request_id) REFERENCES data_subject_request (id),
  FOREIGN KEY (vendor_id) REFERENCES service (id)
);
