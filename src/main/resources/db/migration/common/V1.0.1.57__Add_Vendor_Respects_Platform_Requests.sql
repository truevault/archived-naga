ALTER TABLE vendor
  ADD COLUMN respects_platform_requests BOOLEAN NOT NULL DEFAULT FALSE;