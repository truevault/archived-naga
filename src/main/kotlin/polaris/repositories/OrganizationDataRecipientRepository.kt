package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.models.entity.vendor.Vendor
import java.util.*

interface OrganizationDataRecipientRepository : CustomRepository<OrganizationDataRecipient, Int> {
    fun findFirstById(id: Int): OrganizationDataRecipient
    fun findAllByOrganization(organization: Organization): List<OrganizationDataRecipient>
    fun findAllByOrganizationAndCompleted(organization: Organization, completed: Boolean): List<OrganizationDataRecipient>

    fun findAllByOrganizationAndVendorIn(organization: Organization, vendors: List<Vendor>): List<OrganizationDataRecipient>
    fun findByOrganizationAndVendor(organization: Organization, vendor: Vendor): OrganizationDataRecipient?
    fun findAllByGdprProcessorSetting(setting: GDPRProcessorRecommendation): List<OrganizationDataRecipient>
}
