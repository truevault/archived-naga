package polaris.repositories

import polaris.models.entity.feature.Feature
import java.util.*

interface FeatureRepository : CustomRepository<Feature, UUID> {
    fun findByName(name: String): Feature?
}
