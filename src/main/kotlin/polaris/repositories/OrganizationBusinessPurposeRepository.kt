package polaris.repositories

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.transaction.annotation.Transactional
import polaris.models.entity.compliance.BusinessPurpose
import polaris.models.entity.organization.BusinessPurposeType
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationBusinessPurpose
import java.util.*

interface OrganizationBusinessPurposeRepository : CustomRepository<OrganizationBusinessPurpose, UUID> {
    fun findAllByOrganization(organization: Organization): List<OrganizationBusinessPurpose>
    fun findAllByOrganizationAndBusinessPurposeType(organization: Organization, businessPurposeType: BusinessPurposeType): List<OrganizationBusinessPurpose>
    fun findByOrganizationAndBusinessPurposeAndBusinessPurposeType(organization: Organization, businessPurpose: BusinessPurpose, businessPurposeType: BusinessPurposeType): OrganizationBusinessPurpose?

    @Modifying
    @Transactional
    @Query(
        nativeQuery = true,
        value = "INSERT INTO organization_business_purpose " +
            "(organization_id, business_purpose_id, business_purpose_type) " +
            "VALUES (:organizationId, :purposeId, :type) " +
            "ON CONFLICT DO NOTHING"
    )
    fun insertUniquely(organizationId: UUID, purposeId: UUID, type: String = BusinessPurposeType.CONSUMER.toString())
}
