package polaris.repositories

import polaris.models.entity.regulation.Regulation
import java.util.*

interface RegulationRepository : CustomRepository<Regulation, UUID> {
    fun findByName(name: String): Regulation?
    fun findBySlug(slug: String): Regulation?
}
