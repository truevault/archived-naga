package polaris.repositories

import polaris.models.entity.compliance.PICGroup
import java.util.*

interface PICGroupRepo : CustomRepository<PICGroup, UUID> {
    fun findAllByIdIn(ids: List<UUID>): List<PICGroup>
}
