package polaris.repositories

import polaris.models.entity.organization.Event
import java.util.*

interface EventRepository : CustomRepository<Event, UUID>
