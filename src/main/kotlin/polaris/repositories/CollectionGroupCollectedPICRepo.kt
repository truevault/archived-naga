package polaris.repositories

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.transaction.annotation.Transactional
import polaris.models.entity.compliance.PersonalInformationCategory
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.CollectionGroupCollectedPIC
import polaris.models.entity.organization.Organization
import java.util.UUID

interface CollectionGroupCollectedPICRepo :
    CustomRepository<CollectionGroupCollectedPIC, UUID> {
    fun existsByCollectionGroup(collectionGroup: CollectionGroup): Boolean
    fun findAllByCollectionGroup_Organization(organization: Organization): List<CollectionGroupCollectedPIC>
    fun findAllByCollectionGroupIn(collectionGroups: List<CollectionGroup>): List<CollectionGroupCollectedPIC>
    fun findAllByCollectionGroup(collectionGroup: CollectionGroup): List<CollectionGroupCollectedPIC>

    fun findByCollectionGroupAndPic(collectionGroup: CollectionGroup, pic: PersonalInformationCategory): CollectionGroupCollectedPIC?
    fun findAllByCollectionGroup_OrganizationAndPic_PicKeyIn(org: Organization, picSlugs: List<String>): List<CollectionGroupCollectedPIC>

    @Modifying
    @Transactional
    @Query(
        nativeQuery = true,
        value = "INSERT INTO collection_group_collected_pic " +
            "(collection_group_id, pic_id) " +
            "VALUES (:collectionGroupId, :picId) " +
            "ON CONFLICT DO NOTHING"
    )
    fun insertUniquely(collectionGroupId: UUID, picId: UUID)
}
