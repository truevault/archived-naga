package polaris.repositories

import polaris.models.entity.vendor.VendorClassification
import java.util.*

interface VendorClassificationRepository : CustomRepository<VendorClassification, UUID> {
    fun findBySlug(slug: String): VendorClassification?
}
