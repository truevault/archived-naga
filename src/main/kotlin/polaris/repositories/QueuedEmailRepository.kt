package polaris.repositories

import polaris.models.entity.email.QueuedEmail
import polaris.models.entity.email.QueuedEmailStatusType
import java.util.*

interface QueuedEmailRepository : CustomRepository<QueuedEmail, UUID> {
    fun findByStatus(status: QueuedEmailStatusType): List<QueuedEmail>
}
