package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.user.User
import java.util.*

interface UserRepository : CustomRepository<User, UUID> {
    fun findByUpdatePasswordId(updatePasswordId: UUID): User?
    fun findByEmail(email: String): User?
    fun findAllByActiveOrganization(org: Organization): List<User>

//    @Query("""
//        SELECT user FROM User user
//        INNER JOIN OrganizationUser organization_user ON organization_user.user = user
//        WHERE organization_user.status = :status AND user.email = :email
//    """)
//    fun findByEmailAndStatus(email: String, status: UserStatus = UserStatus.ACTIVE): User?

//    @Query("""/*
//        SELECT user FROM User user
//        INNER JOIN OrganizationUser organization_user ON organization_user.user = user
//        WHERE organization_user.organization = :organization
//    """)
//    fun findAllByOrganization(organization: Organization): List<User>*/
}
