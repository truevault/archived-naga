package polaris.repositories

import polaris.models.entity.GdprCookieConsentTracking
import java.util.*

interface GdprCookieConsentTrackingRepo : CustomRepository<GdprCookieConsentTracking, UUID> {
    fun findByKey(key: String): GdprCookieConsentTracking?
}
