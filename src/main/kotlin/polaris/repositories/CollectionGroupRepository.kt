package polaris.repositories

import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.Organization
import java.util.*

interface CollectionGroupRepository : CustomRepository<CollectionGroup, UUID> {
    fun findByNameAndOrganization(name: String, organization: Organization?): CollectionGroup?
    fun findAllByOrganization(organization: Organization?): List<CollectionGroup>
    fun findAllByOrganizationAndCollectionGroupTypeIn(organization: Organization?, type: List<CollectionGroupType>): List<CollectionGroup>
}
