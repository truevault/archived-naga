package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationCookieScan
import java.util.UUID

interface OrganizationCookieScanRepository : CustomRepository<OrganizationCookieScan, UUID> {
    fun findAllByOrganization(organization: Organization): List<OrganizationCookieScan>
    fun findAllByOrganizationAndScanUrl(organization: Organization, scanUrl: String): List<OrganizationCookieScan>
}
