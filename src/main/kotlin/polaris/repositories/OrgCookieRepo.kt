package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationCookie
import java.util.*

interface OrgCookieRepo : CustomRepository<OrganizationCookie, UUID> {
    fun findByOrganizationAndNameAndDomain(organization: Organization, name: String, domain: String): OrganizationCookie?
    fun findAllByOrganization(organization: Organization): List<OrganizationCookie>
}
