package polaris.repositories

import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.DataSourceCollectionGroup
import polaris.models.entity.organization.Organization
import polaris.models.entity.vendor.Vendor
import java.util.UUID

interface DataSourceCollectionGroupRepo : CustomRepository<DataSourceCollectionGroup, UUID> {
    fun findAllByCollectionGroup_OrganizationAndVendor(organization: Organization, vendor: Vendor): List<DataSourceCollectionGroup>
    fun findAllByCollectionGroupIn(collectionGroups: List<CollectionGroup>): List<DataSourceCollectionGroup>
    // fun findAllByCollectionGroup(collectionGroup: CollectionGroup): List<DataSourceCollectionGroup>
    fun findByVendorIdAndCollectionGroupId(vendorId: UUID, consumerGroupId: UUID): DataSourceCollectionGroup?
    // fun findAllByCollectionGroup_OrganizationAndVendor(organization: Organization, vendor: Vendor): List<DataSourceCollectionGroup>
    //
    // fun findAllByCollectionGroupIdAndNoDataDisclosure(consumerGroupId: UUID, noDataDisclosure: Boolean): List<DataSourceCollectionGroup>
}
