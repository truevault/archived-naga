package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import java.util.*

interface TaskRepository : CustomRepository<Task, UUID> {
    fun findByOrganizationAndId(organization: Organization, id: UUID): Task?

    fun findAllByOrganizationAndCompletedAtIsNull(organization: Organization): List<Task>
    fun findAllByOrganizationAndCompletedAtIsNotNull(organization: Organization): List<Task>

    fun findByOrganizationAndTaskKeyAndCompletedAtIsNull(organization: Organization, taskKey: TaskKey): Task?
}
