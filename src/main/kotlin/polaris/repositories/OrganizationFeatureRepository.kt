package polaris.repositories

import polaris.models.entity.feature.Feature
import polaris.models.entity.feature.OrganizationFeature
import polaris.models.entity.organization.Organization
import java.util.*

interface OrganizationFeatureRepository : CustomRepository<OrganizationFeature, UUID> {
    fun findByOrganizationAndFeature(organization: Organization, feature: Feature): OrganizationFeature?
}
