package polaris.repositories

import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataRequestTypeRequestStateHelpArticle

interface DataRequestTypeRequestStateHelpArticleRepository : CustomRepository<DataRequestTypeRequestStateHelpArticle, Int> {
    fun findByRequestTypeAndRequestState(requestType: DataRequestType, requestState: DataRequestState): DataRequestTypeRequestStateHelpArticle?
}
