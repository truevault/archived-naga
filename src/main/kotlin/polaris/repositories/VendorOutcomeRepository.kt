package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.request.VendorOutcome
import polaris.models.entity.vendor.Vendor
import java.util.*

interface VendorOutcomeRepository : CustomRepository<VendorOutcome, UUID> {
    fun findByDataSubjectRequestAndVendor(dataSubjectRequest: DataSubjectRequest, vendor: Vendor): VendorOutcome?
    fun existsByVendorAndDataSubjectRequest_Organization(vendor: Vendor, organization: Organization): Boolean
}
