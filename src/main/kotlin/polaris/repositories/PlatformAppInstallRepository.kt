package polaris.repositories

import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.PlatformAppInstall
import java.util.*

interface PlatformAppInstallRepository : CustomRepository<PlatformAppInstall, Int> {
    fun findAllByPlatform(platform: OrganizationDataRecipient): List<PlatformAppInstall>
    fun findAllByApp(app: OrganizationDataRecipient): List<PlatformAppInstall>
}
