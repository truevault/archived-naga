package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationAuthToken
import polaris.models.entity.organization.OrganizationAuthTokenId
import polaris.models.entity.organization.OrganizationAuthTokenProvider

interface OrganizationAuthTokenRepository : CustomRepository<OrganizationAuthToken, OrganizationAuthTokenId> {
    fun findByOrganizationAndProvider(organization: Organization, provider: OrganizationAuthTokenProvider): OrganizationAuthToken?
}
