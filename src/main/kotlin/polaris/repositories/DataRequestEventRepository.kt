package polaris.repositories

import polaris.models.entity.request.DataSubjectRequestEvent

interface DataRequestEventRepository : CustomRepository<DataSubjectRequestEvent, Int>
