package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.user.Invitation
import polaris.models.entity.user.InvitationStatus
import java.util.*

interface InvitationRepository : CustomRepository<Invitation, UUID> {
    fun findByIdAndStatus(id: UUID, status: InvitationStatus): Invitation?
    fun findAllByOrganization(org: Organization): List<Invitation>
}
