package polaris.repositories

import org.springframework.data.jpa.repository.Query
import polaris.models.entity.organization.OrganizationMailbox
import java.util.UUID

interface OrganizationMailboxRepository : CustomRepository<OrganizationMailbox, UUID> {
    fun findByNylasAccountId(nylasAccountId: String): OrganizationMailbox?

    @Query(
        "SELECT DISTINCT mb" +
            " FROM OrganizationMailbox mb" +
            "   JOIN FETCH mb.organization o" +
            "   LEFT JOIN FETCH o.organizationUsers ou" +
            "   LEFT JOIN FETCH ou.user" +
            " WHERE mb.status <> 'INACTIVE'" +
            "   AND mb.organization IS NOT NULL" +
            "   AND mb.organization.deletedAt IS NULL" +
            " ORDER BY o.name"
    )
    fun findAllActiveMailboxes(): List<OrganizationMailbox>
}
