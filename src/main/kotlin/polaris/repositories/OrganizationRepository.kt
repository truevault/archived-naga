package polaris.repositories

import polaris.models.OrganizationPublicId
import polaris.models.entity.organization.Organization
import java.util.*

interface OrganizationRepository : CustomRepository<Organization, UUID> {
    fun findByPublicId(publicId: OrganizationPublicId): Organization?
    fun findAllByIdIn(ids: List<UUID>): List<Organization>
    fun findByRequestTollFreePhoneNumber(phoneNumber: String): Organization?

    fun findByDevInstructionsUuid(id: UUID): Organization?
    fun findByHrInstructionsUuid(id: UUID): Organization?
}
