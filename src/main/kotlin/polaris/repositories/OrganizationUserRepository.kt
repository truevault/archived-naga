package polaris.repositories

import org.springframework.data.repository.CrudRepository
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationUser
import polaris.models.entity.user.User

interface OrganizationUserRepository : CrudRepository<OrganizationUser, Int> {
    fun findByUserAndOrganization(user: User, organization: Organization): OrganizationUser?

    fun findAllByOrganization(organization: Organization): List<OrganizationUser>
}
