package polaris.repositories

import polaris.models.entity.compliance.PersonalInformationType
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.CollectionGroupMappingProgress

interface CollectionGroupMappingProgressRepository : CustomRepository<CollectionGroupMappingProgress, Int> {
    fun findAllByCollectionGroup(collectionGroup: CollectionGroup): List<CollectionGroupMappingProgress>

    fun findByCollectionGroupAndPersonalInformationType(
        collectionGroup: CollectionGroup,
        personalInformationType: PersonalInformationType
    ): CollectionGroupMappingProgress?
}
