package polaris.repositories

import polaris.models.entity.organization.*
import java.util.*

interface CustomMessageRepository : CustomRepository<CustomMessage, UUID> {
    fun findByOrganizationAndMessageType(organization: Organization, messageType: CustomMessageTypeEnum): CustomMessage?
}
