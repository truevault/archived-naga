package polaris.repositories

import polaris.models.entity.compliance.BusinessPurpose
import java.util.*

interface BusinessPurposeRepository : CustomRepository<BusinessPurpose, UUID> {
    fun findByName(name: String): BusinessPurpose?
}
