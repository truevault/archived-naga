package polaris.repositories

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import polaris.models.DataRequestPublicId
import polaris.models.entity.organization.Organization
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import java.time.ZonedDateTime
import java.util.UUID

interface DataRequestRepository : CustomRepository<DataSubjectRequest, UUID> {
    fun findByPublicId(publicId: DataRequestPublicId): DataSubjectRequest?

    fun findAllByDuplicatesRequest(duplicatesRequest: DataSubjectRequest): List<DataSubjectRequest>

    fun findByReplyKey(replyKey: UUID): DataSubjectRequest?

    fun findByOrganizationAndPublicId(organization: Organization, publicId: DataRequestPublicId): DataSubjectRequest?

    fun findByOrganizationAndStateIsIn(organization: Organization, states: List<DataRequestState>): List<DataSubjectRequest>
    fun findByOrganizationAndStateIsIn(organization: Organization, states: List<DataRequestState>, pageable: Pageable): Page<DataSubjectRequest>
    fun findAllByOrganizationAndIsAutoclosed(organization: Organization, isAutoClosed: Boolean): List<DataSubjectRequest>

    fun findByOrganizationAndStateIsInAndRequestTypeIn(organization: Organization, states: List<DataRequestState>, requestType: List<DataRequestType>): List<DataSubjectRequest>
    fun findByOrganizationAndStateIsInAndRequestType(organization: Organization, states: List<DataRequestState>, requestType: DataRequestType, pageable: Pageable): Page<DataSubjectRequest>

    fun findByConfirmationToken(confirmationToken: UUID): DataSubjectRequest?

    fun findAllByOrganizationAndSubjectEmailAddress(organization: Organization, subjectEmailAddress: String): List<DataSubjectRequest>

    @Query(
        "SELECT dsr from DataSubjectRequest dsr " +
            "WHERE dsr.state = 'PENDING_EMAIL_VERIFICATION' " +
            "AND (dsr.requestDueDate <= :time OR (dsr.requestDueDate is NULL AND dsr.requestDate <= :backupTime)) " +
            "AND dsr.organization.deletedAt IS NULL"
    )
    fun findAutoCloseable(time: ZonedDateTime, backupTime: ZonedDateTime): List<DataSubjectRequest>
}
