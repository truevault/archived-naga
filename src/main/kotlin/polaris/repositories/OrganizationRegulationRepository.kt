package polaris.repositories

import org.springframework.data.repository.CrudRepository
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationRegulation
import polaris.models.entity.regulation.Regulation

interface OrganizationRegulationRepository : CrudRepository<OrganizationRegulation, Int> {
    fun findByOrganization(organization: Organization): List<OrganizationRegulation>
    fun findByOrganizationAndRegulation(organization: Organization, regulation: Regulation): OrganizationRegulation?
}
