package polaris.repositories

import polaris.models.PrivacyCenterPublicId
import polaris.models.entity.organization.Organization
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.util.PolarisException
import java.util.*

interface PrivacyCenterRepository : CustomRepository<PrivacyCenter, UUID> {
    fun findByPublicId(publicId: PrivacyCenterPublicId): PrivacyCenter?

    fun findByOrganizationAndPublicId(organization: Organization, publicId: PrivacyCenterPublicId): PrivacyCenter?

    fun findAllByOrganizationId(organizationId: String): List<PrivacyCenter>

    fun findAllByOrganization(organization: Organization): List<PrivacyCenter>

    fun findOneByOrganizationId(organizationId: String): PrivacyCenter {
        val pcs = findAllByOrganizationId(organizationId)

        if (pcs.size != 1) {
            throw PolarisException("Found ${pcs.size} Privacy Centers")
        }

        return pcs.get(0)
    }

    fun findByRequestEmail(requestEmail: String): PrivacyCenter?

    fun findOneByOrganization(organization: Organization): PrivacyCenter {
        val pcs = findAllByOrganization(organization)

        if (pcs.size != 1) {
            throw PolarisException("Found ${pcs.size} Privacy Centers")
        }

        return pcs.get(0)
    }
}
