package polaris.repositories

import polaris.models.entity.compliance.RetentionReason
import java.util.*

interface RetentionReasonRepository : CustomRepository<RetentionReason, UUID> {
    fun findBySlug(slug: String): RetentionReason?
    fun findAllBySlugIn(slug: List<String>): List<RetentionReason>
}
