package polaris.repositories

import org.springframework.data.repository.CrudRepository
import polaris.models.entity.user.LoginActivity
import java.util.UUID

interface LoginActivityRepository : CrudRepository<LoginActivity, UUID>
