package polaris.repositories

import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.PrivacyRequestRetentionReason

interface PrivacyRequestRetentionReasonRepository : CustomRepository<PrivacyRequestRetentionReason, Int> {
    fun findAllByCollectionGroup(collectionGroup: CollectionGroup): List<PrivacyRequestRetentionReason>

    fun findAllByCollectionGroupAndOrganizationDataRecipient(
        collectionGroup: CollectionGroup,
        organizationDataRecipient: OrganizationDataRecipient?
    ): List<PrivacyRequestRetentionReason>
}
