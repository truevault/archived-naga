package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataSource
import polaris.models.entity.vendor.Vendor
import java.util.*

interface OrganizationDataSourceRepository : CustomRepository<OrganizationDataSource, UUID> {
    fun findAllByOrganization(organization: Organization): List<OrganizationDataSource>
    fun findByOrganizationAndVendor(organization: Organization, vendor: Vendor): OrganizationDataSource?
    fun findByOrganizationAndVendor_VendorKey(organization: Organization, key: String): OrganizationDataSource?
}
