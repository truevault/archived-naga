package polaris.repositories

import polaris.models.entity.compliance.PICGroup
import polaris.models.entity.compliance.PersonalInformationCategory
import java.util.*

interface PersonalInformationCategoryRepository : CustomRepository<PersonalInformationCategory, UUID> {
    fun findByName(name: String): PersonalInformationCategory
    fun findByNameIn(names: List<String>): List<PersonalInformationCategory>?

    fun findAllByIdIn(ids: List<UUID>): List<PersonalInformationCategory>

    fun findByPicKey(picKey: String): PersonalInformationCategory?

    fun findAllByGroup(group: PICGroup): List<PersonalInformationCategory>
    fun findAllByDeprecated(deprecated: Boolean): List<PersonalInformationCategory>
}
