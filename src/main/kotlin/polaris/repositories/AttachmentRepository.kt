package polaris.repositories

import polaris.models.entity.attachment.Attachment
import java.util.*

interface AttachmentRepository : CustomRepository<Attachment, UUID>
