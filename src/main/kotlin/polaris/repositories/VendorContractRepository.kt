package polaris.repositories

import polaris.models.entity.vendor.VendorContract
import java.util.*

interface VendorContractRepository : CustomRepository<VendorContract, UUID> {
    fun findBySlug(slug: String): VendorContract?
}
