package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationComplianceChecklistItem

interface OrganizationComplianceChecklistItemRepository : CustomRepository<OrganizationComplianceChecklistItem, Int> {
    fun findAllByOrganization(organization: Organization): List<OrganizationComplianceChecklistItem>
}
