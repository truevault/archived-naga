package polaris.repositories

import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.DataSourceReceivedPIC
import polaris.models.entity.organization.Organization
import polaris.models.entity.vendor.Vendor
import java.util.*

interface DataSourceReceivedPICRepo :
    CustomRepository<DataSourceReceivedPIC, UUID> {
    // fun findAllByCollectedPIC_CollectionGroup_Organization(organization: Organization): List<DataRecipientDisclosedPIC>
    //
    // fun findAllByCollectedPIC_CollectionGroup_OrganizationAndVendor(
    //     organization: Organization,
    //     vendor: Vendor
    // ): List<DataRecipientDisclosedPIC>
    //
    // fun findAllByCollectedPIC_CollectionGroup(collectionGroup: CollectionGroup): List<DataRecipientDisclosedPIC>
    //
    fun findAllByCollectedPIC_CollectionGroupAndVendor(collectionGroup: CollectionGroup, vendor: Vendor): List<DataSourceReceivedPIC>

    fun findAllByCollectedPIC_CollectionGroup_OrganizationAndVendor(org: Organization, vendor: Vendor): List<DataSourceReceivedPIC>
    //
    // @Query(
    //     """
    // SELECT exchanged FROM DataRecipientDisclosedPIC exchanged
    // INNER JOIN CollectionGroupCollectedPIC collected on exchanged.collectedPIC = collected
    // INNER JOIN CollectionGroup g on collected.collectionGroup = g
    // WHERE g.organization = :organization AND collected.collectionGroup IN :collectionGroups
    // """
    // )
    // fun findForOrganizationAndConsumerGroups(organization: Organization, collectionGroups: List<CollectionGroup>): List<DataRecipientDisclosedPIC>
    //
    // @Query(
    //     """
    // SELECT exchanged FROM DataRecipientDisclosedPIC exchanged
    // INNER JOIN CollectionGroupCollectedPIC collected on exchanged.collectedPIC = collected
    // INNER JOIN CollectionGroup g on collected.collectionGroup = g
    // WHERE g.organization = :organization
    // """
    // )
    // fun findForOrganization(organization: Organization): List<DataRecipientDisclosedPIC>
}
