package polaris.repositories

import polaris.models.entity.organization.DefaultProcessingActivity
import java.util.UUID

interface DefaultProcessingActivityRepo :
    CustomRepository<DefaultProcessingActivity, UUID>
