package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationWebsiteAudit
import java.util.UUID

interface OrganizationWebsiteAuditRepository : CustomRepository<OrganizationWebsiteAudit, UUID> {
    fun findAllByOrganization(organization: Organization): List<OrganizationWebsiteAudit>
    fun findAllById(id: UUID): List<OrganizationWebsiteAudit>
}
