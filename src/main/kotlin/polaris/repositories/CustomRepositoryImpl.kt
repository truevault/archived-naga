
package polaris.repositories

import org.springframework.data.jpa.repository.support.JpaEntityInformation
import org.springframework.data.jpa.repository.support.SimpleJpaRepository
import org.springframework.transaction.annotation.Transactional
import java.io.Serializable
import javax.persistence.EntityManager

open class CustomRepositoryImpl<T, ID : Serializable>(
    val entityInformation: JpaEntityInformation<T, ID>,
    private val entityManager: EntityManager
) : SimpleJpaRepository<T, ID>(entityInformation, entityManager), CustomRepository<T, ID> {

    @Override
    @Transactional
    override fun refresh(t: T) {
        entityManager.refresh(t)
    }
}
