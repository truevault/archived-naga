package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationSurveyQuestion

interface OrganizationSurveyQuestionRepository : CustomRepository<OrganizationSurveyQuestion, Int> {
    fun findAllByOrganization(organization: Organization): List<OrganizationSurveyQuestion>
    fun findAllByOrganizationAndSurveyName(
        organization: Organization,
        surveyName: String
    ): List<OrganizationSurveyQuestion>

    fun findAllByOrganizationAndSurveyNameAndSlugIn(
        organization: Organization,
        surveyName: String,
        slugs: List<String>
    ): List<OrganizationSurveyQuestion>

    fun findByOrganizationAndSurveyNameAndSlug(organization: Organization, surveyName: String, slug: String): OrganizationSurveyQuestion?

    fun findAllByOrganizationAndSlug(
        organization: Organization,
        slug: String
    ): List<OrganizationSurveyQuestion>
}
