package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.RequestHandlingInstruction
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod
import java.util.*

interface RequestHandlingInstructionsRepository : CustomRepository<RequestHandlingInstruction, UUID> {
    fun findAllByOrganization(organization: Organization): List<RequestHandlingInstruction>
    fun findAllByOrganizationAndRequestType(
        organization: Organization,
        dataRequestType: DataRequestInstructionType
    ): List<RequestHandlingInstruction>

    fun findAllByOrganizationAndRequestTypeAndProcessingMethod(
        organization: Organization,
        dataRequestType: DataRequestInstructionType,
        processingMethod: ProcessingMethod
    ): List<RequestHandlingInstruction>

    fun findAllByOrganizationAndOrganizationDataRecipient(
        organization: Organization,
        organizationDataRecipient: OrganizationDataRecipient
    ): List<RequestHandlingInstruction>

    fun findByIdAndOrganization(
        instructionId: UUID,
        organization: Organization
    ): RequestHandlingInstruction?

    fun findByOrganizationAndRequestType(
        organization: Organization,
        dataRequestType: DataRequestInstructionType
    ): RequestHandlingInstruction?

    fun findByOrganizationAndRequestTypeAndOrganizationDataRecipient(
        organization: Organization,
        dataRequestType: DataRequestInstructionType,
        organizationDataRecipient: OrganizationDataRecipient
    ): RequestHandlingInstruction?

    fun findByOrganizationAndOrganizationDataRecipient(
        organization: Organization,
        organizationDataRecipient: OrganizationDataRecipient
    ): List<RequestHandlingInstruction>

    fun countByOrganizationAndOrganizationDataRecipient(
        organization: Organization,
        organizationDataRecipient: OrganizationDataRecipient
    ): Long
}
