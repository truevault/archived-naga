package polaris.repositories

import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.DataRecipientCollectionGroup
import polaris.models.entity.organization.Organization
import polaris.models.entity.vendor.Vendor
import java.util.UUID

interface DataRecipientCollectionGroupRepo : CustomRepository<DataRecipientCollectionGroup, UUID> {
    fun findAllByCollectionGroupIn(collectionGroups: List<CollectionGroup>): List<DataRecipientCollectionGroup>
    fun findAllByCollectionGroup(collectionGroup: CollectionGroup): List<DataRecipientCollectionGroup>
    fun findByVendorIdAndCollectionGroupId(vendorId: UUID, consumerGroupId: UUID): DataRecipientCollectionGroup?
    fun findAllByCollectionGroup_OrganizationAndVendor(organization: Organization, vendor: Vendor): List<DataRecipientCollectionGroup>

    fun findAllByCollectionGroupIdAndNoDataDisclosure(consumerGroupId: UUID, noDataDisclosure: Boolean): List<DataRecipientCollectionGroup>
}
