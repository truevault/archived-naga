package polaris.repositories

import org.springframework.data.repository.CrudRepository
import polaris.models.entity.organization.OrganizationProgress
import java.util.*

interface OrganizationProgressRepository : CrudRepository<OrganizationProgress, Int> {
    fun findByOrganizationId(organizationId: UUID): List<OrganizationProgress>
    fun findByOrganizationIdAndProgressKey(organizationId: UUID, progressKey: String): OrganizationProgress?
    fun findByOrganizationIdAndProgressKeyIn(organizationId: UUID, progressKeys: List<String>): List<OrganizationProgress>
}
