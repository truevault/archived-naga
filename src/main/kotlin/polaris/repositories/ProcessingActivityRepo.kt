package polaris.repositories

import polaris.models.entity.organization.DefaultProcessingActivity
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.ProcessingActivity
import java.util.UUID

interface ProcessingActivityRepo :
    CustomRepository<ProcessingActivity, UUID> {
    fun findAllByOrganization(org: Organization): List<ProcessingActivity>
    fun findByOrganizationAndDefaultProcessingActivity(org: Organization, def: DefaultProcessingActivity): ProcessingActivity?
}
