package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationVendorClient
import java.util.*

interface OrganizationVendorClientRepository : CustomRepository<OrganizationVendorClient, Int> {
    fun findAllByVendorOrganizationIn(organizations: List<Organization>): List<OrganizationVendorClient>
}
