package polaris.repositories

import polaris.models.entity.privacycenter.PrivacyCenterPolicySection
import java.util.*

interface PrivacyCenterPolicySectionRepository : CustomRepository<PrivacyCenterPolicySection, UUID>
