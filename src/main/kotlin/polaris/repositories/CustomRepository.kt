package polaris.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.data.repository.PagingAndSortingRepository
import java.io.Serializable

@NoRepositoryBean
interface CustomRepository<T, ID : Serializable?> :
    CrudRepository<T, ID>,
    JpaRepository<T, ID>,
    PagingAndSortingRepository<T, ID>,
    JpaSpecificationExecutor<T> {
    fun refresh(t: T)
}
