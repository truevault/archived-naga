package polaris.repositories

import polaris.models.entity.organization.DataRetentionPolicyDetail
import polaris.models.entity.organization.DataRetentionPolicyDetailCategory
import polaris.models.entity.organization.Organization
import java.util.*

interface DataRetentionPolicyDetailRepository : CustomRepository<DataRetentionPolicyDetail, UUID> {
    fun findAllByOrganization(organization: Organization): List<DataRetentionPolicyDetail>
    fun findByOrganizationAndType(organization: Organization, type: DataRetentionPolicyDetailCategory): DataRetentionPolicyDetail?
}
