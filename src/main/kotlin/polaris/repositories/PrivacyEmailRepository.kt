package polaris.repositories

import polaris.models.entity.organization.PrivacyEmail
import java.util.*

interface PrivacyEmailRepository : CustomRepository<PrivacyEmail, UUID>
