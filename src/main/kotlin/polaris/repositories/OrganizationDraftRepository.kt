package polaris.repositories

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDraft
import java.util.*

interface OrganizationDraftRepository : CustomRepository<OrganizationDraft, UUID> {
    fun findAllByOrganization(org: Organization): List<OrganizationDraft>
}
