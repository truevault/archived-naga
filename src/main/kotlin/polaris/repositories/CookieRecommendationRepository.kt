package polaris.repositories

import polaris.models.entity.cookieRecommendation.CookieRecommendation
import polaris.models.entity.vendor.Vendor
import java.util.*

interface CookieRecommendationRepository : CustomRepository<CookieRecommendation, UUID> {
    fun findAllByVendor(vendor: Vendor): List<CookieRecommendation>
    fun findAllByName(name: String): List<CookieRecommendation>
}
