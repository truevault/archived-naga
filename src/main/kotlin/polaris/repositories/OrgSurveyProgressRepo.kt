package polaris.repositories

import polaris.models.dto.SurveySlug
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationSurveyProgress
import java.util.*

interface OrgSurveyProgressRepo : CustomRepository<OrganizationSurveyProgress, UUID> {
    fun findByOrganizationAndSurveySlug(organization: Organization, surveySlug: SurveySlug): OrganizationSurveyProgress?
}
