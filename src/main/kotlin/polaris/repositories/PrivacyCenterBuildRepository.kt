package polaris.repositories

import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.privacycenter.PrivacyCenterBuild
import java.util.UUID

interface PrivacyCenterBuildRepository : CustomRepository<PrivacyCenterBuild, UUID> {
    fun existsByPrivacyCenterAndSourceAndSentBuildMessageAtIsNull(privacyCenter: PrivacyCenter, source: String): Boolean
    fun findAllBySentBuildMessageAtIsNull(): List<PrivacyCenterBuild>
}
