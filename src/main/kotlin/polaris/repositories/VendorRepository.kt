package polaris.repositories

import org.springframework.data.jpa.repository.Query
import polaris.models.entity.organization.Organization
import polaris.models.entity.vendor.DataRecipientType
import polaris.models.entity.vendor.Vendor
import java.util.*

interface VendorRepository : CustomRepository<Vendor, UUID> {
    fun findAllByOrganizationIsNull(): List<Vendor>

    fun findAllByIdIn(ids: List<UUID>): List<Vendor>

    fun findByVendorKey(vendorKey: String): Vendor?
    fun findByVendorKeyAndOrganizationIsNull(vendorKey: String): Vendor?

    @Query("SELECT vendor FROM Vendor vendor WHERE ( vendor.organization IS NULL OR vendor.organization = :organization ) AND vendor.isMigrationOnly = false")
    fun findAllByOrganization(organization: Organization): List<Vendor>

    @Query("SELECT vendor FROM Vendor vendor WHERE (vendor.organization IS NULL OR vendor.organization = :organization) AND vendor.dataRecipientType = :dataRecipientType AND vendor.isMigrationOnly = false")
    fun findAllByOrganizationAndDataRecipientType(organization: Organization, dataRecipientType: DataRecipientType): List<Vendor>

    @Query("SELECT DISTINCT v.recipientCategory FROM Vendor v WHERE v.recipientCategory IS NOT NULL AND (v.organization IS NULL OR v.organization = :organization) AND v.isMigrationOnly = false")
    fun findDistinctRecipientCategories(organization: Organization?): List<String>

    @Query("SELECT DISTINCT v.name FROM Vendor v WHERE v.name IS NOT NULL AND (v.organization IS NULL OR v.organization = :organization) AND v.isMigrationOnly = false")
    fun findDistinctNames(organization: Organization?): List<String>

    @Query("SELECT DISTINCT vendor FROM Vendor vendor WHERE (vendor.organization IS NULL OR vendor.organization = :organization) AND LOWER(vendor.name) = LOWER(:name) AND vendor.isMigrationOnly = false")
    fun findAllByOrganizationAndName(organization: Organization?, name: String): List<Vendor>
}
