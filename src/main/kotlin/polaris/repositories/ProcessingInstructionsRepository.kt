package polaris.repositories

import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.ProcessingInstruction
import polaris.models.entity.request.DataRequestType
import java.util.*

interface ProcessingInstructionsRepository : CustomRepository<ProcessingInstruction, UUID> {
    fun findByOrganizationAndCollectionGroupAndRequestType(
        organization: Organization,
        collectionGroup: CollectionGroup,
        requestType: DataRequestType
    ): ProcessingInstruction?

    fun findAllByOrganization(organization: Organization): List<ProcessingInstruction>
    fun findAllByOrganizationAndCollectionGroup(
        organization: Organization,
        collectionGroup: CollectionGroup
    ): List<ProcessingInstruction>
}
