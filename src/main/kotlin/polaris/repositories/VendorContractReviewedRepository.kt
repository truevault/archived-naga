package polaris.repositories

import polaris.models.entity.vendor.VendorContractReviewed
import java.util.*

interface VendorContractReviewedRepository : CustomRepository<VendorContractReviewed, UUID> {
    fun findBySlug(slug: String): VendorContractReviewed?
}
