package polaris

import net.javacrumbs.shedlock.core.LockProvider
import net.javacrumbs.shedlock.provider.jdbctemplate.JdbcTemplateLockProvider
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.core.Ordered
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import polaris.repositories.CustomRepositoryImpl
import java.util.TimeZone
import javax.annotation.PostConstruct
import javax.sql.DataSource

// Entry point
@SpringBootApplication
@EnableAsync
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "10m")
@EnableJpaRepositories(repositoryBaseClass = CustomRepositoryImpl::class)
class Application(
    @Value("\${polaris.domain}") private val polarisDomain: String,
    @Value("\${polaris.allowedOrigin}") private val polarisAllowedOrigin: String
) {
    companion object {
        private val logger = LoggerFactory.getLogger(Application::class.java)
    }

    @Bean
    fun passwordEncoder() = BCryptPasswordEncoder()

    @Bean
    @Profile("ci")
    fun cleanMigrateStrategy(): FlywayMigrationStrategy? {
        return FlywayMigrationStrategy { flyway ->
            flyway.clean()
            flyway.migrate()
        }
    }

    @Bean
    fun corsFilter(): FilterRegistrationBean<*>? {
        val hostedConfig = CorsConfiguration()
        hostedConfig.allowCredentials = false
        hostedConfig.allowedOrigins = listOf("*")
        hostedConfig.allowedHeaders = listOf("*")
        hostedConfig.allowedMethods = listOf("*")

        val config = CorsConfiguration()
        config.allowCredentials = true
        val allowedOrigin = if (polarisAllowedOrigin.isBlank()) polarisDomain else polarisAllowedOrigin
        val listAllowedOrigin = allowedOrigin.split(",")
        config.allowedOrigins = listOf(
            "http://dockerhost:5000",
            "http://dockerhost:5002",
            "http://localhost:5002",
            "http://localhost:5000",
            "https://polaris.truevaultstaging.com",
            "https://polaris.truevault.com",
            "https://admin-polaris.truevault.com",
            "https://admin-polaris.truevaultstaging.com",
            *listAllowedOrigin.toTypedArray()
        )
        config.allowedHeaders = listOf("*")
        config.allowedMethods = listOf("*")
        logger.info("allowed origins: ${config.allowedOrigins}")

        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/api/hosted/**", hostedConfig)
        source.registerCorsConfiguration("/**", config)

        val bean = FilterRegistrationBean(CorsFilter(source))
        bean.order = Ordered.HIGHEST_PRECEDENCE

        return bean
    }

    @PostConstruct
    fun setDefaultTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    }

    @Bean
    fun lockProvider(dataSource: DataSource): LockProvider {
        return JdbcTemplateLockProvider(
            JdbcTemplateLockProvider.Configuration.builder()
                .withJdbcTemplate(JdbcTemplate(dataSource))
                .usingDbTime()
                .build()
        )
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}
