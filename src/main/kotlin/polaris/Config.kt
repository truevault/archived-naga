package polaris

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import polaris.interceptors.ThrottleInterceptor

@Configuration
class Config : WebMvcConfigurer {
    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(ThrottleInterceptor()).addPathPatterns("/**")
    }
}
