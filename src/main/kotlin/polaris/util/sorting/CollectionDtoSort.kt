package polaris.util.sorting

import polaris.models.dto.compliance.CollectionDto
import polaris.models.entity.compliance.CollectionGroupType

class CollectionDtoSort {

    companion object : Comparator<CollectionDto> {
        override fun compare(a: CollectionDto, b: CollectionDto): Int {
            return if (a.collectionGroupType == b.collectionGroupType) {
                if (a.collectionGroupName < b.collectionGroupName) {
                    -1
                } else if (a.collectionGroupName == b.collectionGroupName) {
                    0
                } else {
                    1
                }
            } else {
                if (a.collectionGroupType === CollectionGroupType.BUSINESS_TO_CONSUMER) {
                    -1
                } else {
                    1
                }
            }
        }
    }
}
