package polaris.util.sorting

import polaris.models.dto.compliance.DMCollectionGroupDto
import polaris.models.entity.compliance.CollectionGroupType

class DMCollectionGroupDtoSort {
    companion object : Comparator<DMCollectionGroupDto> {
        override fun compare(a: DMCollectionGroupDto, b: DMCollectionGroupDto): Int {

            return if (a.type == b.type) { // I tried to remove this duplication, but kotlin resisted
                if (a.name < b.name) { // Data classes are not allowed to inherit and generics don't seem to work either
                    -1
                } else if (a.name == b.name) {
                    0
                } else {
                    1
                }
            } else {
                if (a.type === CollectionGroupType.BUSINESS_TO_CONSUMER) {
                    -1
                } else {
                    1
                }
            }
        }
    }
}
