package polaris.util.sorting

import polaris.models.dto.compliance.CategoryDto

class CategoryDtoSort {
    companion object : Comparator<CategoryDto> {
        override fun compare(a: CategoryDto, b: CategoryDto): Int {
            return compareBy<CategoryDto> { it.picGroupOrder }
                .thenBy { it.order }
                .thenBy { it.name }
                .compare(a, b)
        }
    }
}
