package polaris.util.sorting

import polaris.models.UUIDString
import polaris.models.dto.compliance.FullDataMapDto
import polaris.models.dto.compliance.PICGroupDto
import polaris.models.entity.compliance.PICGroup
import polaris.models.entity.compliance.PersonalInformationCategory

class PICGroupSort {
    companion object : Comparator<PICGroup> {
        override fun compare(a: PICGroup, b: PICGroup): Int {
            return compareBy<PICGroup> { it.displayOrder }.thenBy { it.name }.compare(a, b)
        }
    }
}

class PICGroupDtoSort {
    companion object : Comparator<PICGroupDto> {
        override fun compare(a: PICGroupDto, b: PICGroupDto): Int {
            return compareBy<PICGroupDto> { it.order }.thenBy { it.name }.compare(a, b)
        }

        fun compareByGroupID(dataMap: FullDataMapDto): Comparator<UUIDString> {
            return Comparator { a, b ->
                val groupA = dataMap.picGroups[a]
                val groupB = dataMap.picGroups[b]

                if (groupA == null || groupB == null) {
                    return@Comparator 0
                }

                return@Comparator compare(groupA, groupB)
            }
        }
    }
}

class PICSort {
    companion object : Comparator<PersonalInformationCategory> {
        override fun compare(a: PersonalInformationCategory, b: PersonalInformationCategory): Int {
            return compareBy<PersonalInformationCategory> { it.group?.displayOrder }.thenBy(PersonalInformationCategory::groupDisplayOrder)
                .thenBy { it.name }
                .compare(a, b)
        }
    }
}
