package polaris.util

object EmailParser {

    /**
     * separates the message from an email body
     */
    fun parseMessage(body: String): String {
        var message = body

        // Split Message into its parts
        val parts = splitMessageOnWrote(message)
        message = parts.first()

        // Remove Extra Spacing
        message = removeLeadingBRs(message)
        message = removeTrailingBRs(message)
        message = message.trim()

        return message
    }

    // Split messages into their various parts of the chain
    // Sample -> On Tue, Mar 3, 2020 at 9:22 AM Eric Greer &lt;<a href="mailto:eric&#64;apsis.io" rel="nofollow">eric&#64;apsis.io</a>&gt; wrote:<br />
    private fun splitMessageOnWrote(message: String) = message.split("""(On .+? wrote:)""".toRegex())

    private fun removeTrailingBRs(message: String) = message.replace("""(<br\s*\/?\s*>\s*)+$""".toRegex(), "")
    private fun removeLeadingBRs(message: String) = message.replace("""^(<br\s*\/?\s*>\s*)+""".toRegex(), "")
}
