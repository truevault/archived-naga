package polaris.util.converters

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.LoggerFactory
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.CookieCategory
import polaris.models.entity.organization.LawfulBasis
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.vendor.ProcessingRegion
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
abstract class EnumListConverter<T : Enum<T>>(val enumClass: Class<T>, val default: List<T> = emptyList()) : AttributeConverter<List<T>, String> {

    companion object {
        val log = LoggerFactory.getLogger(EnumListConverter::class.java)
    }

    override fun convertToDatabaseColumn(attribute: List<T>?): String {
        val result = try {
            jacksonObjectMapper().writeValueAsString(attribute)
        } catch (e: JsonProcessingException) {
            try {
                jacksonObjectMapper().writeValueAsString(default)
            } catch (e: JsonProcessingException) {
                "[]"
            }
        }

        return result
    }

    override fun convertToEntityAttribute(dbData: String?): List<T> {
        if (dbData == null) { return default }

        val result: List<T> = try {
            val asStrings = jacksonObjectMapper().readValue<List<String>>(dbData)
            val res = asStrings.map { java.lang.Enum.valueOf(enumClass, it) }

            res
        } catch (e: JsonProcessingException) {
            default
        } catch (e: java.lang.IllegalArgumentException) {
            default
        }

        return result
    }
}

@Converter
class DataRequestStateHistoryCon : EnumListConverter<DataRequestState>(DataRequestState::class.java)

@Converter
class ProcessingRegionListCon : EnumListConverter<ProcessingRegion>(ProcessingRegion::class.java, listOf(ProcessingRegion.UNITED_STATES, ProcessingRegion.EEA_UK))

@Converter
class CollectionContextListCon : EnumListConverter<CollectionContext>(CollectionContext::class.java, listOf(CollectionContext.CONSUMER))

@Converter
class LawfulBasisLisCon : EnumListConverter<LawfulBasis>(LawfulBasis::class.java)

@Converter
class AutocreatedCollectionGroupSlugListCon : EnumListConverter<AutocreatedCollectionGroupSlug>(AutocreatedCollectionGroupSlug::class.java)

@Converter
class CookieCategoryListCon : EnumListConverter<CookieCategory>(CookieCategory::class.java)
