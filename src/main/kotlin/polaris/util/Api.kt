package polaris.util

object WebhookRoutes {
    const val ROOT = "/webhooks"

    object Nylas {
        const val BASE = "$ROOT/nylas"

        const val OAUTH = "$BASE/oauth"
        const val ACCOUNT_CONNECTED = "$BASE/account.connected"
        const val ACCOUNT_ERROR = "$BASE/account.error"
        const val ACCOUNT_SYNC_ERROR = "$BASE/account.sync_error"
        const val THREAD_REPLIED = "$BASE/thread.replied"
        const val MESSAGE_CREATED = "$BASE/message.created"
    }

    object Cookiescan {
        const val BASE = "$ROOT/cookiescan"
        const val RESULT = "$BASE/result"
    }

    object Websiteaudit {
        const val BASE = "$ROOT/websiteaudit"
        const val RESULT = "$BASE/result"
    }

    object Bitbucket {
        const val BASE = "$ROOT/bitbucket"
        const val TEMPLATE_PUSH = "$BASE/templatepush"
    }

    object Twilio {
        const val BASE = "$ROOT/twilio"
        const val DATA_REQUEST = "$BASE/request"
    }

    object Oauth {
        const val BASE = "$ROOT/oauth"
        const val GOOGLE = "$BASE/google"
    }

    const val OAUTH = "${polaris.util.WebhookRoutes.Nylas.BASE}/oauth"
}

object ApiRoutes {
    const val ROOT = "/api"

    object Param {
        const val PAGE = "page"
        const val PER = "per"
        const val SORT = "sort"
        const val DESC = "desc"
    }

    object PrivacyCenter {
        object Param {
            const val ORGANIZATION_ID = "organizationId"
            const val PRIVACY_CENTER_ID = "privacyCenterId"
            const val POLICY_SECTION_ID = "policySectionId"
        }

        const val BASE = "$ROOT/organizations/{${Param.ORGANIZATION_ID}}/privacyCenters"
        const val SPECIFIC = "$BASE/{${Param.PRIVACY_CENTER_ID}}"
        const val BUILDER = "$SPECIFIC/builder"
        const val POLICY = "$SPECIFIC/policy"
        const val POLICY_SECTIONS = "$SPECIFIC/policy/sections"
        const val POLICY_SECTION_SPECIFIC = "$SPECIFIC/policy/sections/{${Param.POLICY_SECTION_ID}}"
    }

    object Public {
        object Param {
            const val ORGANIZATION_ID = "organizationId"
        }

        const val BASE = "$ROOT/public"
        const val ORG_NOTICES = "$BASE/org/{${Param.ORGANIZATION_ID}}/notices"
    }

    object RequestTemplate {
        const val BASE = "$ROOT/templates"
    }

    object RetentionReason {
        const val BASE = "$ROOT/retentionReasons"
    }

    object Login {
        const val BASE = "$ROOT/login"
        const val LOGOUT = "$ROOT/logout"
    }

    object User {
        object Param {
            const val USER = "user"
            const val UPDATE_PASSWORD = "updatePasswordId"
            const val INVITATION_ID = "invitationId"
            const val ORGANIZATION_ID = "organizationId"
        }

        private const val BASE = "$ROOT/users"
        const val SPECIFIC = "$BASE/{${Param.USER}}"
        const val SELF = "$BASE/self"
        const val SELF_ACTIVE_ORGANIZATION = "$SELF/activeOrganization"
        const val SESSION = "$BASE/self/session"
        const val FORGOT_PASSWORD = "$BASE/forgotPassword"
        const val GET_INVITATION = "$BASE/invitations/{${Param.INVITATION_ID}}"
        const val GET_UPDATE_PASSWORD = "$BASE/updatePassword/{${Param.UPDATE_PASSWORD}}"
        const val ACCEPT_INVITE = "$BASE/accept/{${Param.ORGANIZATION_ID}}"
        const val ACTIVATE = "$SPECIFIC/activate/{${Param.ORGANIZATION_ID}}"
    }

    object ConsumerGroups {
        const val BASE = "$ROOT/consumerGroups"
        const val TYPES = "$BASE/types"
    }

    object Regulation {
        object Param {
            const val REGULATION_SLUG = "regulationId"
        }

        const val BASE = "$ROOT/regulation"
        const val SPECIFIC = "$BASE/{${Param.REGULATION_SLUG}}"
    }

    object ProcessingActivity {
        private const val BASE = "$ROOT/processingActivities"
        const val ALL = BASE
    }

    object Organization {
        object Param {
            const val ORGANIZATION_ID = "organizationId"
            const val FORCE = "force"
            const val MAILBOX = "mailbox"
            const val REGULATION = "regulation"
            const val FILTER_VISIBLE = "filterVisible"
            const val GET_COMPLIANT_PROGRESS = "getCompliantProgress"
            const val GET_COMPLIANT_INVALIDATED_PROGRESS = "getCompliantInvalidatedProgress"
            const val EXCEPTIONS_MAPPING_PROGRESS = "exceptionsMappingProgress"
            const val COOKIES_MAPPING_PROGRESS = "cookiesMappingProgress"
            const val SURVEY_NAME = "surveyName"
            const val ADD_OR_REMOVE = "addOrRemove"
        }

        private const val BASE = "$ROOT/organizations"
        const val SPECIFIC = "$BASE/{${Param.ORGANIZATION_ID}}"

        const val GET_COMPLIANT_DONE = "$SPECIFIC/getCompliantDone"

        const val DATA_REQUEST_TYPES = "$SPECIFIC/dataRequestTypes"

        const val FEATURES = "$SPECIFIC/features"
        const val MAILBOXES = "$SPECIFIC/mailboxes"
        const val MESSAGE_SETTINGS = "$SPECIFIC/messageSettings"
        const val CUSTOM_FIELD = "$SPECIFIC/customField"
        const val FINANCIAL_INCENTIVES = "$SPECIFIC/financialIncentives"
        const val GA_WEB_PROPERTY = "$SPECIFIC/gaWebProperty"
        const val MESSAGE_SETTINGS_TEST = "$MESSAGE_SETTINGS/test"
        const val SPECIFIC_MAILBOX = "$SPECIFIC/mailboxes/{${Param.MAILBOX}}"
        const val VERIFICATION_INSTRUCTION = "$SPECIFIC/verificationInstruction"
        const val GET_COMPLIANT_PROGRESS = "$SPECIFIC/getCompliantProgress/{${Param.GET_COMPLIANT_PROGRESS}}"
        const val GET_COMPLIANT_INVALIDATED_PROGRESS = "$SPECIFIC/getCompliantInvalidatedProgress/{${Param.GET_COMPLIANT_INVALIDATED_PROGRESS}}/{${Param.ADD_OR_REMOVE}}"
        const val EXCEPTIONS_MAPPING_PROGRESS = "$SPECIFIC/exceptionsMappingProgress/{${Param.EXCEPTIONS_MAPPING_PROGRESS}}"
        const val COOKIES_MAPPING_PROGRESS = "$SPECIFIC/cookiesMappingProgress/{${Param.COOKIES_MAPPING_PROGRESS}}"
        const val PRIVACY_NOTICE_PROGRESS = "$SPECIFIC/privacyNoticeProgress"
        const val SURVEY = "$SPECIFIC/survey/{${Param.SURVEY_NAME}}"
        const val SURVEY_SEND = "$SPECIFIC/survey/{${Param.SURVEY_NAME}}/send"
        const val COMPLIANCE_CHECKLIST = "$SPECIFIC/complianceChecklist"

        object CookieConsent {
            const val BASE = "$SPECIFIC/cookieConsent"
            const val CSV_EXPORT = "$BASE.csv"
        }

        object Authorization {
            const val AUTHORIZATIONS = "$SPECIFIC/authorizations"
            const val GOOGLE = "$SPECIFIC/authorize/google"
        }

        object DataRetention {
            object Param {
                const val CATEGORY = "category"
            }
            const val BASE = "$SPECIFIC/dataRetention"
            const val SPECIFIC_CATEGORY = "$BASE/{${Param.CATEGORY}}"
        }

        object Progress {
            object Param {
                const val KEY = "key"
                const val SLUG = "slug"
                const val PROGRESS_KEYS = "keys"
            }
            const val BASE = "$SPECIFIC/progress"
            const val SPECIFIC_SURVEY_PROGRESS = "$BASE/survey/{${Param.SLUG}}"
            const val COMPLETE_SPECIFIC_SURVEY = "$BASE/survey/{${Param.SLUG}}/done"
            const val START_SPECIFIC_SURVEY = "$BASE/survey/{${Param.SLUG}}/start"
            const val SPECIFIC_PROGRESS = "$BASE/{${Param.KEY}}"
        }

        object ProcessingActivity {
            object Param {
                const val ID = "id"
                const val PIC_ID = "pic_id"
            }

            const val BASE = "${Organization.SPECIFIC}/processingActivities"
            const val SPECIFIC = "$BASE/{${Param.ID}}"
            const val SPECIFIC_ASSOCIATION_PIC = "$BASE/{${Param.ID}}/association/{${Param.PIC_ID}}"
            const val AUTOMATIC = "$BASE/automatic"
        }

        object DataSource {
            object Param {
                const val ID = "id"
            }

            const val BASE = "${Organization.SPECIFIC}/dataSources"
            const val SPECIFIC = "$BASE/{${Param.ID}}"
        }

        object Admin {
            const val BASE = "$SPECIFIC/admin"

            object User {
                object Param {
                    const val USER = "user"
                }

                const val BASE = "${Admin.BASE}/users"
                const val INVITE = "$BASE/invite"
                const val SPECIFIC = "$BASE/{${Param.USER}}"
                const val DEACTIVATE = "$SPECIFIC/deactivate"
                const val ACTIVATE = "$SPECIFIC/activate"
            }
        }

        object Templates {
            object Param {
                const val MESSAGE_TYPE = "messageType"
            }

            const val BASE = "${Organization.SPECIFIC}/templates"
            const val SPECIFIC = "$BASE/{${Param.MESSAGE_TYPE}}"
        }

        object CollectionGroup {
            object Param {
                const val DATA_SUBJECT_TYPE_ID = "dataSubjectTypeId"
                const val INFORMATION_TYPE = "informationType"
                const val MAPPING_PROGRESS = "mappingProgress"
                const val PRIVACY_NOTICE_PROGRESS = "privacyNoticeProgress"
                const val COLLECTION_GROUP_TYPES = "collectionGroupTypes"
                const val EXCLUDE_COLLECTION_GROUP_TYPES = "excludeCollectionGroupTypes"
            }

            const val BASE = "${Organization.SPECIFIC}/collectionGroups"
            const val DEFAULT = "$BASE/default"
            const val VENDORS = "$BASE/vendors"
            const val SPECIFIC = "$BASE/{${Param.DATA_SUBJECT_TYPE_ID}}"
            const val VERIFICATION = "$SPECIFIC/verification"
            const val MAPPING_PROGRESS = "$SPECIFIC/mappingProgress/{${Param.INFORMATION_TYPE}}/{${Param.MAPPING_PROGRESS}}"
            const val PRIVACY_NOTICE_PROGRESS = "$SPECIFIC/privacyNoticeProgress/{${Param.PRIVACY_NOTICE_PROGRESS}}"
            const val RETENTION = "$SPECIFIC/retention"

            object Vendor {
                object Param {
                    const val VENDOR_ID = "vendorId"
                }

                const val BASE = "${CollectionGroup.SPECIFIC}/vendors"
                const val SPECIFIC = "$BASE/{${Param.VENDOR_ID}}"
            }

            object DataRequestType {
                object Param {
                    const val DATA_REQUEST_TYPE_ID = "dataRequestTypeId"
                    const val MAPPING_PROGRESS = "mappingProgress"
                }

                const val BASE = "${CollectionGroup.SPECIFIC}/dataRequestTypes"
                const val SPECIFIC = "$BASE/{${Param.DATA_REQUEST_TYPE_ID}}"
            }
        }

        object Tasks {
            const val BASE = "${Organization.SPECIFIC}/tasks"

            object Param {
                const val TASK_ID = "taskId"
                const val FILTER = "filter"
            }

            const val COMPLETE_TASKS = "${Organization.SPECIFIC}/tasks/complete"
            const val COMPLETE_TASK = "${Organization.SPECIFIC}/tasks/{${Param.TASK_ID}}/complete"

            const val ADD_DPO = "$BASE/addDpo"
            const val AUTHORIZE_GOOGLE_ANALYTICS = "$BASE/authorizeGoogleAnalytics"
            const val COMPLETE_FINANCIAL_INCENTIVES = "$BASE/completeFinancialIncentives"
        }

        object CookieScanner {
            const val BASE = "${Organization.SPECIFIC}/cookieScanner"
            object Param {
                const val DOMAIN = "domain"
                const val NAME = "name"
            }
            const val RECOMMENDATIONS = "$BASE/recommendations"
            const val COOKIE = "$BASE/cookie/{${Param.DOMAIN}}/{${Param.NAME}}"
        }

        object WebsiteAudit {
            const val BASE = "${Organization.SPECIFIC}/websiteAudit"
            const val RESET = "${Organization.WebsiteAudit.BASE}/reset"
        }

        object Requests {
            const val BASE = "${Organization.SPECIFIC}/requests"

            object Param {
                const val REQUEST_ID = "requestId"
                const val FILTER = "filter"
                const val TYPE = "type"
                const val SUBJECT_EMAIL_ADDRESS = "subjectEmailAddress"
                const val DUE_IN = "dueIn"
                const val STATE = "state"
                const val REQUEST_DATE_START = "startDate"
                const val REQUEST_DATE_END = "endDate"
            }

            const val CSV_EXPORT = "$BASE.csv"
            const val FOR_CONSUMER = "$BASE/consumer"
            const val STATS = "$BASE/stats"
            const val EXPORT = "$BASE/export"

            const val SPECIFIC = "$BASE/{${Param.REQUEST_ID}}"

            // STAY COMPLIANT UPDATE
            const val STEP_FORWARD_REQUEST = "$SPECIFIC/step/forward"
            const val STEP_BACK_REQUEST = "$SPECIFIC/step/back"

            const val SET_SUBJECT_STATUS = "$SPECIFIC/subjectStatus"

            object Autodelete {
                const val GOOGLE_ANALYTICS = "$SPECIFIC/autodelete/googleAnalytics"
            }

            // LEGACY
            const val REPLY = "$SPECIFIC/reply"
            const val NOTE = "$SPECIFIC/note"
            const val VERIFY = "$SPECIFIC/verify"
            const val CONFIRM = "$SPECIFIC/confirm"
            const val STATE = "$SPECIFIC/state"
            const val EXTEND = "$SPECIFIC/extend"

            const val REOPEN = "$SPECIFIC/reopen"
            const val TEMPLATES = "$SPECIFIC/templates"
            const val CLOSED_TEMPLATE = "$SPECIFIC/templates/closed"
            const val CONTACT_VENDOR_TEMPLATE = "$SPECIFIC/templates/contactVendor"
            const val VENDOR = "$SPECIFIC/vendor"
            const val CONTACT_VENDORS = "$SPECIFIC/contactVendors"
            const val CONTACT_REQUESTOR = "$SPECIFIC/contactRequestor"
        }

        object Vendors {
            object Param {
                const val VENDOR_ID = "vendorId"
                const val MAPPING_PROGRESS = "mappingProgress"
                const val EXCLUDE_CATEGORIES = "excludeCategories"
                const val INCLUDE_DATA_STORAGE_LOCATIONS = "includeDataStorageLocations"
                const val EXCLUDE_INCOMPLETE = "excludeIncomplete"
            }

            const val BASE = "${Organization.SPECIFIC}/vendors"
            const val UNMAPPED = "${Organization.SPECIFIC}/vendors/unmapped/{${DataMap.Param.COLLECTION_GROUP_ID}}"
            const val REMOVE = "${Organization.SPECIFIC}/vendors/remove"
            const val THIRD_PARTY_RECIPIENTS = "$BASE/thirdPartyRecipients"
            const val CATALOG = "$BASE/catalog"
            const val NEW = "$BASE/new"
            const val SEARCH = "$BASE/search"
            const val DEFAULT = "$BASE/default"
            const val SPECIFIC = "${Organization.SPECIFIC}/vendors/{${Param.VENDOR_ID}}"
            const val EDIT = "$SPECIFIC/edit"
            const val ADD = "$SPECIFIC/new"
            const val MAPPING_PROGRESS = "$SPECIFIC/mappingProgress/{${Param.MAPPING_PROGRESS}}"
            const val AGREEMENT = "$SPECIFIC/agreement"
        }

        object RecipientPlatform {
            object Param {
                const val ID = "id"
                const val APP_ID = "appId"
            }
            const val BASE = "${Organization.SPECIFIC}/recipientPlatforms"
            const val SPECIFIC = "$BASE/{${Param.ID}}"

            const val SPECIFIC_APPS = "$SPECIFIC/apps"
            const val SPECIFIC_PLATFORMS = "$SPECIFIC/platforms"

            const val SPECIFIC_APP = "$SPECIFIC_APPS/{${Param.APP_ID}}"
        }

        object Processing {
            object Param {
                const val DATA_REQUEST_TYPE_ID = "dataRequestTypeId"
                const val DATA_SUBJECT_TYPE_ID = "dataSubjectTypeId"
            }

            const val LEGACY_INSTRUCTIONS = "$SPECIFIC/processingInstructions"
            const val BASE = "$SPECIFIC/processing"
            private const val BASE_PARAMS =
                "$BASE/dataRequestType/{${Param.DATA_REQUEST_TYPE_ID}}/dataSubjectType/{${Param.DATA_SUBJECT_TYPE_ID}}"
            const val INSTRUCTION = "${Processing.BASE_PARAMS}/instruction"
            const val VENDORS = "${Processing.BASE_PARAMS}/vendors"
        }

        // TODO: This deprecates and supercedes the "Processing" section above.
        object RequestHandlingInstructions {
            object Param {
                const val REQUEST_TYPE = "requestHandlingInstructionType"
                const val INSTRUCTION_ID = "requestHandlingInstructionId"
            }

            const val BASE = "$SPECIFIC/requestHandling"
            const val BY_TYPE = "$BASE/{${Param.REQUEST_TYPE}}"
            const val INSTRUCTION = "$BASE/{${Param.INSTRUCTION_ID}}"
            const val CONSUMER_GROUP = "$BASE/cgInstructions"
            const val OPT_OUT_INSTRUCTIONS = "$BASE/optOutInstructions"
            const val PROGRESS = "$BASE/mappingProgress"
        }

        object DataMap {
            object Param {
                const val PIC_ID = "picId"
                const val COLLECTION_GROUP_ID = "collectionGroupId"
                const val VENDOR_ID = "vendorId"
                const val PIC_GROUP_ID = "picGroupId"
            }
            const val BASE = "$SPECIFIC/dataMap"

            const val PIC_COLLECTION = "$BASE/collectionGroup/{${Param.COLLECTION_GROUP_ID}}/collection/{${Param.PIC_ID}}"
            const val PIC_ASSOCIATION = "$BASE/collectionGroup/{${Param.COLLECTION_GROUP_ID}}/association/{${Param.VENDOR_ID}}"
            const val PIC_SOURCE_ASSOCIATION = "$BASE/collectionGroup/{${Param.COLLECTION_GROUP_ID}}/sourceAssociation/{${Param.VENDOR_ID}}"
            const val PIC_DISCLOSURE = "$BASE/collectionGroup/{${Param.COLLECTION_GROUP_ID}}/disclosure/{${Param.VENDOR_ID}}/{${Param.PIC_GROUP_ID}}"
            const val PIC_RECEIVED = "$BASE/collectionGroup/{${Param.COLLECTION_GROUP_ID}}/received/{${Param.VENDOR_ID}}/{${Param.PIC_ID}}"
        }

        object DataInventory {
            const val BASE = "$SPECIFIC/dataInventory"
            const val SNAPSHOT = "$BASE/snapshot"

            object Collection {
                object Param {
                    const val DATA_SUBJECT_TYPE_ID = "dataSubjectTypeId"
                    const val CATEGORY_ID = "categoryId"
                    const val SOURCE_ID = "sourceId"
                    const val SOURCE_NAME = "sourceName"
                    const val PURPOSE_ID = "purposeId"
                }

                const val BASE = "${DataInventory.BASE}/collection"
                const val BATCH = "$BASE/{${Param.DATA_SUBJECT_TYPE_ID}}"
                const val CLEAR_INTERNALLY_STORED = "$BASE/{${Param.DATA_SUBJECT_TYPE_ID}}/category/internallyStored"
                const val SPECIFIC_CATEGORY = "$BASE/{${Param.DATA_SUBJECT_TYPE_ID}}/category/{${Param.CATEGORY_ID}}"
                const val SPECIFIC_SOURCE = "$BASE/{${Param.DATA_SUBJECT_TYPE_ID}}/source/{${Param.SOURCE_ID}}"
                const val CUSTOM_SOURCE = "$BASE/{${Param.DATA_SUBJECT_TYPE_ID}}/customSource/{${Param.SOURCE_NAME}}"
                const val SPECIFIC_PURPOSE = "$BASE/{${Param.DATA_SUBJECT_TYPE_ID}}/purpose/{${Param.PURPOSE_ID}}"
                const val DEFAULT = "$BASE/default"
            }

            object Exchange {
                object Param {
                    const val VENDOR_ID = "vendorId"
                    const val CONSUMER_GROUP_ID = "consumerGroupId"
                    const val CATEGORY_ID = "categoryId"
                }

                const val BASE = "${DataInventory.BASE}/exchange"
                const val SPECIFIC_CONSUMER_GROUP = "$BASE/consumerGroup/{${Param.CONSUMER_GROUP_ID}}"
                const val SPECIFIC_VENDOR = "$BASE/vendor/{${Param.VENDOR_ID}}"
            }
        }

        object PrivacyNotice {
            object Param {
                const val CONSUMER_GROUP_ID = "consumerGroupId"
            }

            const val BASE = "${Organization.SPECIFIC}/privacyNotices"
            const val CA_PRIVACY_NOTICE = "$BASE/ca"
            const val OPT_OUT_PRIVACY_NOTICE = "$BASE/optOut"

            const val CONSUMER_GROUP_NOTICE = "$BASE/consumerGroup/{${Param.CONSUMER_GROUP_ID}}"
            const val NOTICE_INTRO = "$BASE/consumerGroup/{${Param.CONSUMER_GROUP_ID}}/noticeIntro"
        }
    }

    object HostedForm {
        object Param {
            const val PRIVACY_CENTER_ID = "privacyCenterId"
        }

        const val BASE = "$ROOT/hosted"
        const val SPECIFIC = "$BASE/{${Param.PRIVACY_CENTER_ID}}"
        const val FORM_OPTIONS = "$SPECIFIC/formOptions"
        const val REQUESTS = "$SPECIFIC/requests"
        const val CONFIRM = "$SPECIFIC/requests/confirmEmail"
        const val CCPA_SPECIFIC = "$SPECIFIC/requests/ccpaSpecific"
        const val CCPA_OPT_OUT = "$SPECIFIC/requests/ccpaOptOut"
        const val CCPA_RIGHT_TO_LIMIT = "$SPECIFIC/requests/ccpaLimitSPI"
        const val VCDPA_OPT_OUT = "$SPECIFIC/requests/vcdpaOptOut"
        const val CPA_OPT_OUT = "$SPECIFIC/requests/cpaOptOut"
        const val CTDPA_OPT_OUT = "$SPECIFIC/requests/ctdpaOptOut"

        const val POLARIS_JS_CONFIG = "$SPECIFIC/polaris/config"

        const val GDPR_COOKIE_CONSENT = "$SPECIFIC/cookies/gdprConsent"
    }

    object BlockStorage {
        object Public {
            const val ATTACHMENT = "/attachments/{id}"
            const val IMAGES_WILDCARD = "/images/**"
        }

        object VendorAgreements {
            object Param {
                const val ORGANIZATION_ID = "organizationId"
                const val VENDOR_ID = "vendorId"
                const val AGREEMENT_KEY = "agreementKey"
            }
            const val BASE = "$ROOT/storage/vendoragreements"
            const val ORGANIZATION = "$BASE/organization/{${Param.ORGANIZATION_ID}}"
            const val VENDOR = "$ORGANIZATION/vendor/{${Param.VENDOR_ID}}"
            const val AGREEMENT = "$VENDOR/{${Param.AGREEMENT_KEY}}"
        }

        object Logos {
            object Param {
                const val ORGANIZATION_ID = "organizationId"
                const val PRIVACY_CENTER_ID = "privacyCenterId"
            }

            const val BASE = "$ROOT/storage/logos"
            const val ORGANIZATION = "$BASE/organization/{${Param.ORGANIZATION_ID}}"
            const val PRIVACY_CENTER = "$ORGANIZATION/privacyCenter/{${Param.ORGANIZATION_ID}}"
        }
    }
}

object Responses {
    const val EMPTY_SUCCESS = "{\"ok\": \"true\"}"
}
