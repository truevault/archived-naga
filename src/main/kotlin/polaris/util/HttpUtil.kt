package polaris.util

import org.springframework.web.util.UriComponentsBuilder
import java.net.URLEncoder
import java.net.http.HttpRequest
import java.nio.charset.StandardCharsets

class HttpUtil {
    companion object {
        fun buildUrl(base: String, params: Map<String, String>): String {
            val uriBuilder = UriComponentsBuilder
                .fromUriString(base)

            params.entries.forEach { e ->
                uriBuilder.queryParam(e.key, e.value)
            }

            return uriBuilder.build().toUriString()
        }

        fun urlFormEncodedBody(body: Map<String, String>): String {
            return body.entries
                .map { e -> e.key + "=" + URLEncoder.encode(e.value, StandardCharsets.UTF_8) }
                .joinToString("&")
        }

        fun urlFormEncodedBodyPublisher(body: Map<String, String>): HttpRequest.BodyPublisher {
            // val params = mapOf(
            //     "client_id" to clientId,
            //     "client_secret" to clientSecret,
            //     "refresh_token" to refresh,
            //     "grant_type" to "refresh_token"
            // )
            //
            // val form: String = body.entries
            //     .map { e -> e.key + "=" + URLEncoder.encode(e.value, StandardCharsets.UTF_8) }
            //     .joinToString("&")

            return HttpRequest.BodyPublishers.ofString(urlFormEncodedBody(body))
        }
    }
}
