package polaris.util.survey.interceptors

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import polaris.models.SurveySlugs.Companion.CUSTOM_AUDIENCE_QUESTION_SLUG
import polaris.models.SurveySlugs.Companion.DIRECT_SHARE_FOR_ADVERTISING_QUESTION_SLUG
import polaris.models.SurveySlugs.Companion.DISCLOSURE_FOR_SELLING_SLUG
import polaris.models.SurveySlugs.Companion.GOOGLE_ANALYTICS_DATA_SHARING_SLUG
import polaris.models.SurveySlugs.Companion.SELLING_UPLOAD_DATA_SLUG
import polaris.models.SurveySlugs.Companion.SELL_IN_EXCHANGE_FOR_MONEY_SLUG
import polaris.models.SurveySlugs.Companion.SHARE_DATA_FOR_ADVERTISING_QUESTION_SLUG
import polaris.models.SurveySlugs.Companion.SHOPIFY_AUDIENCES_QUESTION_SLUG
import polaris.models.SurveySlugs.Companion.VENDOR_INTENTIONAL_DISCLOSURE_SELECTION
import polaris.models.SurveySlugs.Companion.VENDOR_SELLING_QUESTION_SLUG
import polaris.models.SurveySlugs.Companion.VENDOR_SHARING_QUESTION_SLUG
import polaris.models.SurveySlugs.Companion.VENDOR_UPLOAD_DATA_SELECTION_SLUG
import polaris.models.dto.organization.RequestHandlingInstructionDto
import polaris.models.entity.organization.DataAccessibilityEnum
import polaris.models.entity.organization.DataDeletabilityEnum
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.models.entity.organization.SELLING_AND_SHARING_SURVEY_NAME
import polaris.models.entity.organization.SHARING_SELLING_SURVEY_NAME
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.models.entity.vendor.KnownVendorCategories
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.VendorClassificationRepository
import polaris.services.primary.OrganizationVendorSurveyService
import polaris.services.primary.RequestHandlingInstructionsService
import polaris.services.primary.ServiceProviderRecommendationService
import polaris.services.primary.SurveyService

class SellingAndSharingSurveyInterceptor(
    private val surveyService: SurveyService,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val requestHandlingInstructionsService: RequestHandlingInstructionsService,
    private val orgDataRecipientRepository: OrganizationDataRecipientRepository,
    private val organizationVendorSurveyService: OrganizationVendorSurveyService,
    private val vendorClassificationRepository: VendorClassificationRepository,
    private val serviceProviderRecommendationService: ServiceProviderRecommendationService
) : BaseSurveyInterceptor(organizationSurveyQuestionRepository, surveyService) {
    companion object {
        private val log = LoggerFactory.getLogger(SellingAndSharingSurveyInterceptor::class.java)
    }

    override fun intercepts(surveyName: String) = surveyName == SHARING_SELLING_SURVEY_NAME
    override fun slugs(surveyName: String) = listOf(
        SHARE_DATA_FOR_ADVERTISING_QUESTION_SLUG,
        CUSTOM_AUDIENCE_QUESTION_SLUG,
        VENDOR_SHARING_QUESTION_SLUG,
        DIRECT_SHARE_FOR_ADVERTISING_QUESTION_SLUG,
        // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
        // GOOGLE_RDP_QUESTION_SLUG,
        // FACEBOOK_LDU_QUESTION_SLUG,
        SHOPIFY_AUDIENCES_QUESTION_SLUG,
        VENDOR_SELLING_QUESTION_SLUG,
        GOOGLE_ANALYTICS_DATA_SHARING_SLUG,
        DISCLOSURE_FOR_SELLING_SLUG,
        SELLING_UPLOAD_DATA_SLUG,
        VENDOR_UPLOAD_DATA_SELECTION_SLUG,
        VENDOR_INTENTIONAL_DISCLOSURE_SELECTION,
        SELL_IN_EXCHANGE_FOR_MONEY_SLUG
    )

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        return defaultGetAnswer(org, surveyName, slug)
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        val oldAnswer = defaultGetAnswer(org, surveyName, slug)

        val newAnswer = OrganizationSurveyQuestion.sanitizeAnswerList(answer)

        defaultWrite(org, newAnswer, questionText, surveyName, slug)

        when (slug) {
            SHARE_DATA_FOR_ADVERTISING_QUESTION_SLUG -> sideEffectForShareData(org, newAnswer)
            CUSTOM_AUDIENCE_QUESTION_SLUG -> sideEffectForCustomAudience(org, newAnswer, oldAnswer)
            VENDOR_SHARING_QUESTION_SLUG -> sideEffectForVendorSharingQuestion(org, newAnswer)
            DIRECT_SHARE_FOR_ADVERTISING_QUESTION_SLUG -> autoSetAdNetworkClassifications(org)
            // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
            // GOOGLE_RDP_QUESTION_SLUG -> autoSetAdNetworkClassifications(org)
            // FACEBOOK_LDU_QUESTION_SLUG -> autoSetAdNetworkClassifications(org)
            SHOPIFY_AUDIENCES_QUESTION_SLUG -> sideEffectForShopifyAudiences(org)
            GOOGLE_ANALYTICS_DATA_SHARING_SLUG -> sideEffectForGoogleAnalyticsSharingSlug(org)
            DISCLOSURE_FOR_SELLING_SLUG -> updateDisclosureForSellingAnswer(org)
            SELLING_UPLOAD_DATA_SLUG -> updateSellingUploadDataAnswer(org)
            VENDOR_UPLOAD_DATA_SELECTION_SLUG -> sideEffectVendorUploadDataSelectionSlug(org, newAnswer)
            VENDOR_SELLING_QUESTION_SLUG -> sideEffectForVendorSelling(org, newAnswer)
            VENDOR_INTENTIONAL_DISCLOSURE_SELECTION -> sideEffectForIntentionalDisclosure(org, newAnswer, oldAnswer)
        }
    }

    private fun sideEffectForShareData(org: Organization, answer: String?) {
        // Answering anything but yes to this question is the functional  equivalent of
        // answering yes and selecting None on the Vendor Sharing question --- at least
        // from a side-effect perspective.
        if (answer != "true") {
            val allDataRecipients = org.organizationDataRecipients
            val adNetworkDataRecipients = allDataRecipients.filter { it.vendor?.isKnownCategory(KnownVendorCategories.AD_NETWORK) ?: false }.distinct()
            setGdprProcessorSetting(adNetworkDataRecipients, listOf("none"))
        }
    }

    private fun sideEffectForCustomAudience(org: Organization, answer: String?, oldAnswer: String?) {
        val stringArrAnswers = OrganizationSurveyQuestion.parseAnswerList(answer ?: "")
        val oldStringArrAnswers = OrganizationSurveyQuestion.parseAnswerList(oldAnswer ?: "")

        if (stringArrAnswers.isEmpty()) {
            return
        }

        val adNetworks = org.organizationDataRecipients.filter { it.vendor?.isKnownCategory(KnownVendorCategories.AD_NETWORK) ?: false }

        adNetworks.forEach {
            val isSelected = !stringArrAnswers.contains("none") && stringArrAnswers.contains(it.id.toString())
            val wasSelected = !oldStringArrAnswers.contains("none") && oldStringArrAnswers.contains(it.id.toString())

            if (oldAnswer != null && wasSelected == isSelected) {
                // we have the same value, so we need to distinguish between no selection being made, and "none"

                // if we *are* selected, it can't be that nothing was selected
                if (isSelected) { return@forEach }

                // continue if none wasn't just selected
                if (answer == oldAnswer || !stringArrAnswers.contains("none")) { return@forEach }
            }

            val deleteHandlingInstructionDto = RequestHandlingInstructionDto(
                organizationId = org.publicId,
                requestType = DataRequestInstructionType.DELETE,
                vendorId = it.vendor!!.id.toString(),
                processingMethod = if (isSelected) ProcessingMethod.DELETE else ProcessingMethod.INACCESSIBLE_OR_NOT_STORED,
                retentionReasons = emptyList(),
                hasRetentionExceptions = false
            )
            requestHandlingInstructionsService.createOrUpdate(org.publicId, deleteHandlingInstructionDto, false)

            val knowHandlingInstructionDto = RequestHandlingInstructionDto(
                organizationId = org.publicId,
                requestType = DataRequestInstructionType.KNOW,
                vendorId = it.vendor.id.toString(),
                processingMethod = if (isSelected) null else ProcessingMethod.INACCESSIBLE_OR_NOT_STORED,
                retentionReasons = emptyList(),
                hasRetentionExceptions = false
            )
            requestHandlingInstructionsService.createOrUpdate(org.publicId, knowHandlingInstructionDto, false, deleteProcessingMethodOnNull = true)

            it.dataAccessibility = if (isSelected) null else DataAccessibilityEnum.NOT_ACCESSIBLE
            it.dataDeletability = if (isSelected) null else DataDeletabilityEnum.NOT_DELETABLE
            orgDataRecipientRepository.save(it)
        }

        autoSetAdNetworkClassifications(org)
    }

    private fun sideEffectForVendorSharingQuestion(org: Organization, answer: String?) {
        val stringArrAnswers = OrganizationSurveyQuestion.parseAnswerList(answer ?: "")
        val allDataRecipients = org.organizationDataRecipients
        val adNetworkDataRecipients = allDataRecipients.filter { it.vendor?.isKnownCategory(KnownVendorCategories.AD_NETWORK) ?: false }.distinct()

        setGdprProcessorSetting(adNetworkDataRecipients, stringArrAnswers)
        autoSetAdNetworkClassifications(org)
    }

    private fun autoSetAdNetworkClassifications(org: Organization) {

        /*
            Vendors designated as sharing by the behavior advertising or as using the custom audience feature
            need to be handled together since they are all ad networks. As described in the tickets ( POLARIS-2548
            and POLARIS-2512 ), any ad network vender identified as matching either behavior advertising or custom
            audience should be auto classified as third party and all other should be marked as service provider.
        */

        val classificationOptions = vendorClassificationRepository.findAll()
        val thirdPartyClassification = classificationOptions.find { it.slug == VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY }
        val serviceProviderClassification = classificationOptions.find { it.slug == VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER }

        val questions = organizationSurveyQuestionRepository.findAllByOrganizationAndSurveyNameAndSlugIn(
            org, SELLING_AND_SHARING_SURVEY_NAME, listOf(CUSTOM_AUDIENCE_QUESTION_SLUG, VENDOR_SHARING_QUESTION_SLUG)
        )

        val sharingAnswersQuestion = questions.find { it.slug == VENDOR_SHARING_QUESTION_SLUG }
        val sharingStringAnswers = sharingAnswersQuestion?.parsedAnswerList() ?: emptyList()

        val customAudienceSharingQuestion = questions.find { it.slug == CUSTOM_AUDIENCE_QUESTION_SLUG }
        val caStringAnswers = customAudienceSharingQuestion?.parsedAnswerList() ?: emptyList()

        val allDataRecipients = org.organizationDataRecipients

        val adNetworkDataRecipients = allDataRecipients.filter { it.vendor?.isKnownCategory(KnownVendorCategories.AD_NETWORK) ?: false }.distinct()

        val sharingAdNetworks = if (sharingStringAnswers.contains("none")) emptyList() else adNetworkDataRecipients.filter { sharingStringAnswers.contains(it.id.toString()) }
        val customAudienceAdNetworks = if (caStringAnswers.contains("none")) emptyList() else adNetworkDataRecipients.filter { caStringAnswers.contains(it.id.toString()) }

        adNetworkDataRecipients.forEach { adNetwork ->

            val isThirdParty = sharingAdNetworks.contains(adNetwork) || customAudienceAdNetworks.contains(adNetwork)

            if (isThirdParty) {
                adNetwork.ccpaIsSharing = true
                adNetwork.automaticallyClassified = true
                adNetwork.vendorClassification = thirdPartyClassification
                adNetwork.usesCustomAudience = customAudienceAdNetworks.contains(adNetwork)
            } else {
                adNetwork.ccpaIsSharing = false
                adNetwork.automaticallyClassified = false
                adNetwork.vendorClassification = serviceProviderClassification
                adNetwork.usesCustomAudience = false
            }

            orgDataRecipientRepository.save(adNetwork)
        }
    }

    private fun setGdprProcessorSetting(adNetworkDataRecipients: List<OrganizationDataRecipient>, answers: List<String>) {
        if (answers.contains("none") || answers.isEmpty()) {
            // If NONE is selected, set all the ad networks to be Processors
            orgDataRecipientRepository.saveAll(
                adNetworkDataRecipients.map {
                    it.gdprProcessorLockedSetting = GDPRProcessorRecommendation.Processor
                    return@map it
                }
            )
        } else {
            // If NONE is not selected, then set all selected ad networks to Controller
            // and all non-selected ad networks to Processors
            orgDataRecipientRepository.saveAll(
                adNetworkDataRecipients.map {
                    val processorStatus =
                        if (answers.contains(it.id.toString())) GDPRProcessorRecommendation.Controller else GDPRProcessorRecommendation.Processor
                    it.gdprProcessorLockedSetting = processorStatus
                    return@map it
                }
            )
        }
    }

    private fun sideEffectForShopifyAudiences(org: Organization) {
        val shopifyAudiencesQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, SHOPIFY_AUDIENCES_QUESTION_SLUG)
        val shopifyAudiencesEnabled = shopifyAudiencesQuestion?.isTrue() ?: false
        val shopify = org.organizationDataRecipients.find { organizationVendorSurveyService.isShopify(it) } ?: return

        setShopifyGdprProcessorSetting(shopify, shopifyAudiencesEnabled)
        addShopifyToVendorSellingSelection(org, shopify, shopifyAudiencesEnabled)
        autoClassifyShopify(shopify, shopifyAudiencesEnabled)
        updateDisclosureForSellingAnswer(org)
        updateSellingUploadDataAnswer(org)
    }

    private fun setShopifyGdprProcessorSetting(shopify: OrganizationDataRecipient, shopifyAudiencesEnabled: Boolean) {
        shopify.gdprProcessorLockedSetting = if (shopifyAudiencesEnabled) GDPRProcessorRecommendation.Controller else GDPRProcessorRecommendation.Processor
        orgDataRecipientRepository.save(shopify)
    }

    private fun addShopifyToVendorSellingSelection(org: Organization, shopify: OrganizationDataRecipient, shopifyAudiencesEnabled: Boolean) {
        val vendorSellingSelectionQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, VENDOR_SELLING_QUESTION_SLUG)
        val selectedSellingVendorIds = vendorSellingSelectionQuestion?.parsedAnswerList() ?: emptyList()
        val mutableSellingVendorIds = selectedSellingVendorIds.toMutableSet()

        if (shopifyAudiencesEnabled) {
            mutableSellingVendorIds.add(shopify.id.toString())
        } else {
            mutableSellingVendorIds.remove(shopify.id.toString())
        }

        val newSelectedVendorIds = mutableSellingVendorIds.toList()
        val mapper = ObjectMapper()
        defaultWrite(org, mapper.writeValueAsString(newSelectedVendorIds), "", SELLING_AND_SHARING_SURVEY_NAME, VENDOR_SELLING_QUESTION_SLUG)
    }

    private fun autoClassifyShopify(shopify: OrganizationDataRecipient, shopifyAudiencesEnabled: Boolean) {
        if (shopifyAudiencesEnabled) {

            shopify.ccpaIsSelling = true
            shopify.automaticallyClassified = true
            shopify.vendorClassification = vendorClassificationRepository.findBySlug(VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY)
        } else {

            shopify.ccpaIsSelling = false
            shopify.automaticallyClassified = true
            shopify.vendorClassification = vendorClassificationRepository.findBySlug(VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER)
        }

        orgDataRecipientRepository.save(shopify)
    }

    private fun sideEffectForVendorSelling(org: Organization, answer: String?) {
        autoClassifySellingVendors(org, answer)
        updateSellingUploadDataAnswer(org)

        // Make sure we enforce the correct selection of Shopify
        val shopifyAudiencesQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, SHOPIFY_AUDIENCES_QUESTION_SLUG)
        val shopifyAudiencesEnabled = shopifyAudiencesQuestion?.isTrue() ?: false
        val shopify = org.organizationDataRecipients.find { organizationVendorSurveyService.isShopify(it) } ?: return

        addShopifyToVendorSellingSelection(org, shopify, shopifyAudiencesEnabled)

        // Make sure we enforce the correct selection of Google Analytics
        val googleAnalytics = org.organizationDataRecipients.find { organizationVendorSurveyService.isGoogleAnalytics(it) } ?: return
        val gaDataSharingQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, GOOGLE_ANALYTICS_DATA_SHARING_SLUG)
        val gaDataSharingOff = gaDataSharingQuestion?.isTrue() ?: false

        addGoogleAnalyticsToSellingVendors(org, googleAnalytics, gaDataSharingOff)
    }

    private fun autoClassifySellingVendors(org: Organization, answer: String?) {
        val potentialSellingVendors = organizationVendorSurveyService.getPotentialSellingVendors(org)
        val selectedSellingVendorIds = OrganizationSurveyQuestion.parseAnswerList(answer ?: "")

        // We have automatic classification specific to Shopify and Google Analytics, so defer to that
        val vendorsToClassify = potentialSellingVendors.filter { !organizationVendorSurveyService.isGoogleAnalytics(it) && !organizationVendorSurveyService.isShopify(it) }

        orgDataRecipientRepository.saveAll(
            vendorsToClassify.map { dr ->
                val isSelling = selectedSellingVendorIds.contains(dr.id.toString())

                if (isSelling) {
                    dr.ccpaIsSelling = true
                    dr.automaticallyClassified = true
                    dr.vendorClassification = vendorClassificationRepository.findBySlug(VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY)
                } else {
                    dr.ccpaIsSelling = false
                    dr.automaticallyClassified = false
                }

                return@map dr
            }
        )
    }

    private fun sideEffectForIntentionalDisclosure(org: Organization, answer: String?, oldAnswer: String?) {
        val potentialIntentionalInteractionVendors = org.organizationDataRecipients.filter { it.vendor?.isPotentialIntentionalInteractionVendor() ?: false }
        val selectedIntentionalInteractionVendorIds = OrganizationSurveyQuestion.parseAnswerList(answer ?: "")
        val oldSelectedIntentionalInteractionVendorIds = OrganizationSurveyQuestion.parseAnswerList(oldAnswer ?: "")

        val sellingVendorsQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, VENDOR_SELLING_QUESTION_SLUG)
        val selectedSellingVendorIds = sellingVendorsQuestion?.parsedAnswerList() ?: emptyList()

        // If this is a selling vendor, we defer to the classification from the sideEffect for Vendor Selling above
        val nonSellingPotentialVendors = potentialIntentionalInteractionVendors.filter { !selectedSellingVendorIds.contains(it.id.toString()) }
        val classificationOptions = vendorClassificationRepository.findAll()

        orgDataRecipientRepository.saveAll(
            nonSellingPotentialVendors.mapNotNull { dr ->
                val isIntentionalDisclosure = selectedIntentionalInteractionVendorIds.contains(dr.id.toString())
                val wasIntentionalDisclosure = oldSelectedIntentionalInteractionVendorIds.contains(dr.id.toString())

                // If the selection for _this_ data recipient didn't change, don't make any
                // changes to this data recipient
                if (isIntentionalDisclosure == wasIntentionalDisclosure) {
                    return@mapNotNull dr
                }

                if (isIntentionalDisclosure) {
                    dr.ccpaIsSelling = false
                    dr.automaticallyClassified = true
                    dr.vendorClassification = vendorClassificationRepository.findBySlug(VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY)
                } else {
                    val defaultClassification = serviceProviderRecommendationService.getDefaultClassification(dr, classificationOptions)

                    dr.automaticallyClassified = false
                    dr.vendorClassification = defaultClassification ?: dr.vendorClassification
                }

                return@mapNotNull dr
            }
        )
    }

    private fun sideEffectForGoogleAnalyticsSharingSlug(org: Organization) {
        val googleAnalytics = org.organizationDataRecipients.find { organizationVendorSurveyService.isGoogleAnalytics(it) } ?: return
        val gaDataSharingQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, GOOGLE_ANALYTICS_DATA_SHARING_SLUG)
        val gaDataSharingOff = gaDataSharingQuestion?.isTrue() ?: false

        automaticGoogleAnalyticsClassification(org, googleAnalytics, gaDataSharingOff)
        addGoogleAnalyticsToSellingVendors(org, googleAnalytics, gaDataSharingOff)
        updateDisclosureForSellingAnswer(org)
    }

    private fun addGoogleAnalyticsToSellingVendors(org: Organization, googleAnalytics: OrganizationDataRecipient, gaDataSharingOff: Boolean) {
        val vendorSellingSelectionQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, VENDOR_SELLING_QUESTION_SLUG)
        val selectedSellingVendorIds = vendorSellingSelectionQuestion?.parsedAnswerList() ?: emptyList()
        val mutableSellingVendorIds = selectedSellingVendorIds.toMutableSet()

        if (gaDataSharingOff) {
            mutableSellingVendorIds.remove(googleAnalytics.id.toString())
        } else {
            mutableSellingVendorIds.add(googleAnalytics.id.toString())
        }

        val newSelectedVendorIds = mutableSellingVendorIds.toList()
        val mapper = ObjectMapper()
        defaultWrite(org, mapper.writeValueAsString(newSelectedVendorIds), "", SELLING_AND_SHARING_SURVEY_NAME, VENDOR_SELLING_QUESTION_SLUG)
    }

    private fun updateDisclosureForSellingAnswer(org: Organization) {
        if (organizationVendorSurveyService.isGoogleAnalyticsWithDataSharing(org) || organizationVendorSurveyService.isShopifyWithAudiencesEnabled(org)) {
            defaultWrite(org, "true", "", SHARING_SELLING_SURVEY_NAME, DISCLOSURE_FOR_SELLING_SLUG)
        }
    }

    private fun updateSellingUploadDataAnswer(org: Organization) {
        if (organizationVendorSurveyService.isOnlyGoogleAnalyticsWithNoDataSharing(org)) {
            defaultWrite(org, "false", "", SHARING_SELLING_SURVEY_NAME, SELLING_UPLOAD_DATA_SLUG)
        } else if (organizationVendorSurveyService.isShopifyWithAudiencesEnabled(org)) {
            defaultWrite(org, "true", "", SHARING_SELLING_SURVEY_NAME, SELLING_UPLOAD_DATA_SLUG)
        }
    }

    private fun sideEffectVendorUploadDataSelectionSlug(org: Organization, answer: String?) {
        val stringArrAnswers = OrganizationSurveyQuestion.parseAnswerList(answer ?: "")

        org.organizationDataRecipients.forEach {
            val isSelected = !stringArrAnswers.contains("none") && stringArrAnswers.contains(it.id.toString())

            it.isUploadVendor = isSelected
            orgDataRecipientRepository.save(it)
        }
    }

    private fun automaticGoogleAnalyticsClassification(org: Organization, googleAnalytics: OrganizationDataRecipient, gaDataSharingOff: Boolean) {
        if (gaDataSharingOff) {
            googleAnalytics.ccpaIsSelling = false
            googleAnalytics.vendorClassification = vendorClassificationRepository.findBySlug(VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER)
            googleAnalytics.automaticallyClassified = true
        } else {
            googleAnalytics.ccpaIsSelling = true
            googleAnalytics.vendorClassification = vendorClassificationRepository.findBySlug(VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY)
            googleAnalytics.automaticallyClassified = true
        }

        orgDataRecipientRepository.save(googleAnalytics)
    }
}
