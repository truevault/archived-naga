package polaris.util.survey.interceptors

import polaris.models.entity.organization.CONSUMER_SOURCES_SURVEY_NAME
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.Organization
import polaris.models.entity.vendor.KnownVendorKey
import polaris.repositories.OrganizationDataSourceRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.OrganizationDataSourceService
import polaris.services.primary.SurveyService

class ConsumerSourcesSurveyInterceptor(
    private val surveyService: SurveyService,
    private val orgDataSourceService: OrganizationDataSourceService,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val orgDataSourceRepo: OrganizationDataSourceRepository
) : SourcesSurveyInterceptor(surveyService, orgDataSourceService, organizationSurveyQuestionRepository, orgDataSourceRepo) {
    companion object {
        private const val CONSUMER_DIRECTLY_SLUG = "consumer-directly"
        private const val OTHER_CONSUMERS_SLUG = "other-consumers"
        private const val AD_NETWORKS_SLUG = "ad-networks"
        private const val DATA_ANALYTICS_PROVIDERS_SLUG = "data-analytics-privders"
        private const val DATA_BROKERS_SLUG = "data-brokers"
    }

    override fun intercepts(surveyName: String) = surveyName == CONSUMER_SOURCES_SURVEY_NAME
    override fun slugs(surveyName: String) = listOf(
        CONSUMER_DIRECTLY_SLUG,
        OTHER_CONSUMERS_SLUG,
        AD_NETWORKS_SLUG,
        DATA_ANALYTICS_PROVIDERS_SLUG,
        DATA_BROKERS_SLUG
    )

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        val oldAnswer = defaultGetAnswer(org, surveyName, slug)
        defaultWrite(org, answer, questionText, surveyName, slug)

        when (slug) {
            CONSUMER_DIRECTLY_SLUG -> manageDataSource(org, KnownVendorKey.CONSUMER_DIRECTLY, CollectionContext.CONSUMER, answer, oldAnswer)
            OTHER_CONSUMERS_SLUG -> manageDataSource(org, KnownVendorKey.OTHER_INDIVIDUALS, CollectionContext.CONSUMER, answer, oldAnswer)
            AD_NETWORKS_SLUG -> clearLegacyDataSources(org)
            DATA_ANALYTICS_PROVIDERS_SLUG -> clearLegacyDataSources(org)
            DATA_BROKERS_SLUG -> clearLegacyDataSources(org)
        }
    }
}
