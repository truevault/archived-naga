package polaris.util.survey.interceptors

import polaris.models.dto.organization.PatchProcessingActivityRequest
import polaris.models.entity.organization.GDPR_LAWFUL_BASES_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.vendor.ProcessingRegion
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.ProcessingActivityService
import polaris.services.primary.SurveyService
import polaris.services.support.AutocreateProcessingActivityService

class LawfulBasesSurveyInterceptor(
    surveyService: SurveyService,
    organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val processingActivityService: ProcessingActivityService,
    private val autocreateService: AutocreateProcessingActivityService
) : BaseSurveyInterceptor(organizationSurveyQuestionRepository, surveyService) {
    companion object {
        private const val AUTOMATED_DECISION_MAKING_PROCESS = "automated-decision-making"
        private const val AUTOMATED_DECISION_MAKING_AUTO_CREATE_SLUG = "AUTOMATED_DECISION_MAKING"
        private const val OUTBOUND_SALES_SLUG = "outbound-sales-eea-uk"
        private const val COLD_SALES_COMMUNICATIONS_AUTO_CREATE_SLUG = "COLD_SALES_COMMUNICATIONS"
    }

    override fun intercepts(surveyName: String) = surveyName == GDPR_LAWFUL_BASES_SURVEY_NAME

    override fun slugs(surveyName: String) = listOf(AUTOMATED_DECISION_MAKING_PROCESS, OUTBOUND_SALES_SLUG)

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        return defaultGetAnswer(org, surveyName, slug)
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        defaultWrite(org, answer, questionText, surveyName, slug)

        when (slug) {
            AUTOMATED_DECISION_MAKING_PROCESS -> handleMandatoryConsentQuestion(org, answer, AUTOMATED_DECISION_MAKING_AUTO_CREATE_SLUG)
            OUTBOUND_SALES_SLUG -> handleMandatoryConsentQuestion(org, answer, COLD_SALES_COMMUNICATIONS_AUTO_CREATE_SLUG)
        }
    }

    private fun handleMandatoryConsentQuestion(org: Organization, answer: String?, autoCreationSlug: String) {

        autocreateService.manageAutomaticProcessingActivities(org.publicId)

        val activities = processingActivityService.processingActivitiesForOrg(org)
        val autoCreatedProcessingActivity = activities.find { it.autocreationSlug?.name == autoCreationSlug }
        if (answer == "true") {
            if (autoCreatedProcessingActivity != null) { // Shouldn't ever occur that this is null
                if (autoCreatedProcessingActivity.processingRegions.contains(ProcessingRegion.EEA_UK)) {
                    return
                }
                val regions = if (autoCreatedProcessingActivity.processingRegions.contains(ProcessingRegion.UNITED_STATES)) listOf(ProcessingRegion.UNITED_STATES, ProcessingRegion.EEA_UK) else listOf(ProcessingRegion.EEA_UK)
                processingActivityService.updateProcessingActivity(org, activityId = autoCreatedProcessingActivity.id, update = PatchProcessingActivityRequest(name = null, lawfulBases = null, regions = regions))
            }
        } else {
            if (autoCreatedProcessingActivity != null) {
                processingActivityService.deleteProcessingActivityRegion(org, activityId = autoCreatedProcessingActivity.id, regions = listOf(ProcessingRegion.EEA_UK))
            }
        }
    }
}
