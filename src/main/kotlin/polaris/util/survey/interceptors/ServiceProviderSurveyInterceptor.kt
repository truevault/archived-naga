package polaris.util.survey.interceptors

import polaris.models.entity.organization.BUSINESS_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.repositories.OrganizationRepository
import polaris.services.support.PrivacyCenterBuilderService

class ServiceProviderSurveyInterceptor(
    private val organizationRepository: OrganizationRepository,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService
) : SurveyInterceptor {
    override fun intercepts(surveyName: String) = surveyName == BUSINESS_SURVEY_NAME
    override fun slugs(surveyName: String) = listOf("business-service-provider")

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        if (org.isServiceProvider == null) {
            return null
        }

        if (org.isServiceProvider!!) {
            return "true"
        }

        return "false"
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        val old = org.isServiceProvider

        if (answer == null) {
            org.isServiceProvider = null
        } else org.isServiceProvider = answer == "true"

        organizationRepository.save(org)

        val changed = org.isServiceProvider != old
        if (changed) {
            privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(org, "is service provider updated")
        }
    }
}
