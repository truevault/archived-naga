package polaris.util.survey.interceptors

import polaris.controllers.api.datainventory.UpdateCollectionRequest
import polaris.controllers.util.LookupService
import polaris.models.entity.organization.CONSUMER_DATA_COLLECTION_SURVEY_PRFEIX
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.repositories.CollectionGroupCollectedPICRepo
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.support.DataMapService
import java.lang.IllegalArgumentException
import java.util.UUID

val INFERENCES_RESPONSE_SLUG = "inferences-responded"

class CollectionInferencesInterceptor(
    private val dataMapService: DataMapService,
    private val collectedPICRepo: CollectionGroupCollectedPICRepo,
    private val surveyQuestionRepo: OrganizationSurveyQuestionRepository,
    private val lookup: LookupService
) : SurveyInterceptor {
    override fun intercepts(surveyName: String): Boolean {
        if (!surveyName.startsWith(CONSUMER_DATA_COLLECTION_SURVEY_PRFEIX)) {
            return false
        }

        try {
            collectionGroupIdFromSurvey(surveyName)
        } catch (e: IllegalArgumentException) {
            return false
        }
        return true
    }

    override fun slugs(surveyName: String) = listOf("inferences")

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        val cgId = collectionGroupIdFromSurvey(surveyName)
        val pic = lookup.picByKeyOrThrow("inferences")
        val cg = lookup.collectionGroupOrThrow(org, cgId)

        val ans = collectedPICRepo.findByCollectionGroupAndPic(cg, pic)
        if (ans != null) {
            return "true"
        }

        val responded = surveyQuestionRepo.findByOrganizationAndSurveyNameAndSlug(org, surveyName, INFERENCES_RESPONSE_SLUG)
        return if (responded == null) null else "false"
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        val cgId = collectionGroupIdFromSurvey(surveyName)
        val pic = lookup.picByKeyOrThrow("inferences")
        val cg = lookup.collectionGroupOrThrow(org, cgId)

        val responded = surveyQuestionRepo.findByOrganizationAndSurveyNameAndSlug(org, surveyName, INFERENCES_RESPONSE_SLUG)

        dataMapService.updateCollection(org, pic, cg, UpdateCollectionRequest(collected = answer == "true"))

        if (responded == null) {
            surveyQuestionRepo.save(OrganizationSurveyQuestion(organization = org, surveyName = surveyName, slug = INFERENCES_RESPONSE_SLUG, answer = "true"))
        }
    }

    private fun collectionGroupIdFromSurvey(surveyName: String): UUID {
        return UUID.fromString(surveyName.removePrefix(CONSUMER_DATA_COLLECTION_SURVEY_PRFEIX))
    }
}
