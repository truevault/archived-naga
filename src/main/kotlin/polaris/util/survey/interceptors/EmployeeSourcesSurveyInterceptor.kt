package polaris.util.survey.interceptors

import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.EMPLOYEE_SOURCES_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.vendor.KnownVendorKey
import polaris.repositories.OrganizationDataSourceRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.OrganizationDataSourceService
import polaris.services.primary.SurveyService

class EmployeeSourcesSurveyInterceptor(
    private val surveyService: SurveyService,
    private val orgDataSourceService: OrganizationDataSourceService,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val orgDataSourceRepo: OrganizationDataSourceRepository
) : SourcesSurveyInterceptor(surveyService, orgDataSourceService, organizationSurveyQuestionRepository, orgDataSourceRepo) {
    companion object {
        private const val WORKER_DIRECTLY_SLUG = "worker-directly"
    }

    override fun intercepts(surveyName: String) = surveyName == EMPLOYEE_SOURCES_SURVEY_NAME
    override fun slugs(surveyName: String) = listOf(WORKER_DIRECTLY_SLUG)

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        val oldAnswer = defaultGetAnswer(org, surveyName, slug)
        defaultWrite(org, answer, questionText, surveyName, slug)

        when (slug) {
            WORKER_DIRECTLY_SLUG -> manageDataSource(org, KnownVendorKey.CONSUMER_DIRECTLY, CollectionContext.EMPLOYEE, answer, oldAnswer)
        }
    }
}
