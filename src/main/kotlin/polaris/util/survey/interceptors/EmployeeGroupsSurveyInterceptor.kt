package polaris.util.survey.interceptors

import polaris.models.entity.organization.HR_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.SurveyService
import polaris.services.support.RemoveAutocreateCollectionGroupService

class EmployeeGroupsSurveyInterceptor(
    surveyService: SurveyService,
    organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val removeAutocreateCollectionGroupService: RemoveAutocreateCollectionGroupService
) : BaseSurveyInterceptor(organizationSurveyQuestionRepository, surveyService) {
    companion object {
        private const val EMPLOYEES_CA = "business-employees-ca"
        private const val CONTRACTORS_CA = "business-contractors-ca"
        private const val APPLICANTS_CA = "business-job-applicants-ca"
        private const val EMPLOYEES_EEA = "employees-eea-uk"
    }

    override fun intercepts(surveyName: String) = surveyName == HR_SURVEY_NAME
    override fun slugs(surveyName: String) = listOf(EMPLOYEES_CA, CONTRACTORS_CA, APPLICANTS_CA, EMPLOYEES_EEA)
    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        return defaultGetAnswer(org, surveyName, slug)
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        val oldAnswer = defaultGetAnswer(org, surveyName, slug)
        defaultWrite(org, answer, questionText, surveyName, slug)

        if (oldAnswer == "true" && answer == "false") {
            // need to "undo" creation of the relevant group
            removeAutocreateCollectionGroupService.removeAutocreatedGroup(slug, org)
        }
    }
}
