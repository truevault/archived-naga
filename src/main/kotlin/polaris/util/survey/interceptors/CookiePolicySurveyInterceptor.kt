package polaris.util.survey.interceptors

import polaris.models.entity.organization.DATA_RETENTION_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.repositories.OrganizationRepository
import polaris.services.support.PrivacyCenterBuilderService

class CookiePolicySurveyInterceptor(
    private val organizationRepository: OrganizationRepository,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService
) : SurveyInterceptor {
    override fun intercepts(surveyName: String) = surveyName == DATA_RETENTION_SURVEY_NAME
    override fun slugs(surveyName: String) = listOf("cookie-policy")

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        return org.cookiePolicy
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        val old = org.cookiePolicy

        org.cookiePolicy = answer

        val changed = org.cookiePolicy != old
        if (changed) {
            organizationRepository.save(org)
            privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(org, "cookie policy updated")
        }
    }
}
