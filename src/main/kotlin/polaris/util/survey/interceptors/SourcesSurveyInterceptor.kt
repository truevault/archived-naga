package polaris.util.survey.interceptors

import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.Organization
import polaris.models.entity.vendor.KnownVendorKey
import polaris.repositories.OrganizationDataSourceRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.OrganizationDataSourceService
import polaris.services.primary.SurveyService

abstract class SourcesSurveyInterceptor(
    private val surveyService: SurveyService,
    private val orgDataSourceService: OrganizationDataSourceService,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val orgDataSourceRepo: OrganizationDataSourceRepository
) : BaseSurveyInterceptor(organizationSurveyQuestionRepository, surveyService) {
    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        return defaultGetAnswer(org, surveyName, slug)
    }

    protected fun manageDataSource(org: Organization, vendorKey: KnownVendorKey, context: CollectionContext, answer: String?, oldAnswer: String?) {
        if (answer == oldAnswer || answer == null) { return }

        // answer has been changed, and is a non-null value
        // get the appropriate source for the consumer survey
        val group = orgDataSourceRepo.findByOrganizationAndVendor_VendorKey(org, vendorKey.key)
        val hasGroup = group?.collectionContexts?.contains(context) == true

        if (answer == "true" && !hasGroup) {
            orgDataSourceService.addDataSource(org.publicId, vendorKey, listOf(context))
        }

        if (answer == "false" && hasGroup) {
            orgDataSourceService.removeDataSource(org.publicId, vendorKey, listOf(context))
        }
    }

    protected fun clearLegacyDataSources(org: Organization) {
        // Clears the legacy Ad Networks, Data Analytics Providers, and Data Brokers
        // sources from the organization's data map.
        orgDataSourceService.removeDataSource(org.publicId, KnownVendorKey.AD_NETWORKS, listOf())
        orgDataSourceService.removeDataSource(org.publicId, KnownVendorKey.DATA_ANALYTICS_PROVIDERS, listOf())
        orgDataSourceService.removeDataSource(org.publicId, KnownVendorKey.DATA_BROKERS, listOf())
    }
}
