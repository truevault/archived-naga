package polaris.util.survey.interceptors

import polaris.models.entity.organization.FINANCIAL_INCENTIVES_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.SurveyService

class FinancialIncentivesSurveyInterceptor(
    surveyService: SurveyService,
    organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository
) : BaseSurveyInterceptor(organizationSurveyQuestionRepository, surveyService) {
    companion object {
        private const val OFFER_CONSUMER_INCENTIVES_SLUG = "offer-consumer-incentives"
    }

    override fun intercepts(surveyName: String) = surveyName == FINANCIAL_INCENTIVES_SURVEY_NAME

    override fun slugs(surveyName: String) = listOf(OFFER_CONSUMER_INCENTIVES_SLUG)

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        return defaultGetAnswer(org, surveyName, slug)
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        defaultWrite(org, answer, questionText, surveyName, slug)

        when (slug) {
            OFFER_CONSUMER_INCENTIVES_SLUG -> handleFinancialIncentives(org, answer)
        }
    }

    private fun handleFinancialIncentives(org: Organization, answer: String?) {
        if (answer == "false") {
            org.financialIncentiveDetails = null
        }
    }
}
