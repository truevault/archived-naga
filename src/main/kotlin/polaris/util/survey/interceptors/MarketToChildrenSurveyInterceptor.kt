package polaris.util.survey.interceptors

import polaris.models.entity.organization.BUSINESS_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.SurveyService

class MarketToChildrenSurveyInterceptor(
    surveyService: SurveyService,
    organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository
) : BaseSurveyInterceptor(organizationSurveyQuestionRepository, surveyService) {
    private val surveyName = BUSINESS_SURVEY_NAME
    private val questionSlug = "business-products-aimed-at-kids"

    companion object {
        private const val AIMED_AT_KIDS_SLUG = "business-products-aimed-at-kids"
        private const val AIMED_AT_KIDS_UNDER_13_SLUG = "business-products-aimed-at-kids-under-13"
    }

    override fun intercepts(name: String) = name == surveyName
    override fun slugs(surveyName: String) = listOf(questionSlug)

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        return defaultGetAnswer(org, surveyName, slug)
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        if (answer == "false") {
            uncheckUnder13Question(org)
        }
        updateUnder16Question(org, answer, questionText)
    }

    private fun uncheckUnder13Question(org: Organization) {
        val existing = surveyRepo.findByOrganizationAndSurveyNameAndSlug(org, surveyName, AIMED_AT_KIDS_UNDER_13_SLUG)
        var question: OrganizationSurveyQuestion? = null
        if (existing == null) {
            question = OrganizationSurveyQuestion(
                organization = org,
                surveyName = surveyName,
                slug = AIMED_AT_KIDS_UNDER_13_SLUG,
                question = "",
                answer = null
            )
        } else {
            question = existing
            question.answer = null
        }
        surveyRepo.save(question)
    }

    private fun updateUnder16Question(org: Organization, answer: String?, questionText: String?) {
        val existing = surveyRepo.findByOrganizationAndSurveyNameAndSlug(org, surveyName, AIMED_AT_KIDS_SLUG)
        var question: OrganizationSurveyQuestion? = null
        if (existing == null) {
            question = OrganizationSurveyQuestion(
                organization = org,
                surveyName = surveyName,
                slug = AIMED_AT_KIDS_SLUG,
                question = questionText ?: "",
                answer = answer ?: "false"
            )
        } else {
            question = existing
            question.answer = answer ?: "false"
        }
        surveyRepo.save(question)
    }
}
