package polaris.util.survey.interceptors

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationSurveyQuestion

data class SurveyQuestionIdentifier(val surveyName: String, val questionSlug: String)

interface SurveyInterceptor {
    fun intercepts(surveyName: String): Boolean
    fun slugs(surveyName: String): List<String>
    fun get(org: Organization, surveyName: String, slug: String): OrganizationSurveyQuestion {
        val answer = getAnswer(org, surveyName, slug)

        return OrganizationSurveyQuestion(id = 0, organization = org, surveyName = surveyName, slug = slug, question = "", answer = answer)
    }

    fun getAnswer(org: Organization, surveyName: String, slug: String): String?
    fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String)
}
