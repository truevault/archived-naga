package polaris.util.survey.interceptors

import polaris.controllers.util.LookupService
import polaris.models.entity.compliance.PICSurveyClassification
import polaris.models.entity.organization.CONSUMER_DATA_COLLECTION_SURVEY_PRFEIX
import polaris.models.entity.organization.Organization
import polaris.repositories.CollectionGroupCollectedPICRepo
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.SurveyService
import polaris.services.support.DataMapService
import java.lang.IllegalArgumentException
import java.util.UUID

class UncommonCollectionSurveyInterceptor(
    surveyService: SurveyService,
    surveyRepo: OrganizationSurveyQuestionRepository,
    private val dataMapService: DataMapService,
    private val collectedPICRepo: CollectionGroupCollectedPICRepo,
    private val lookup: LookupService
) : BaseSurveyInterceptor(surveyRepo, surveyService) {
    override fun intercepts(surveyName: String): Boolean {
        if (!surveyName.startsWith(CONSUMER_DATA_COLLECTION_SURVEY_PRFEIX)) {
            return false
        }

        try {
            collectionGroupIdFromSurvey(surveyName)
        } catch (e: IllegalArgumentException) {
            return false
        }
        return true
    }

    override fun slugs(surveyName: String) = listOf("uncommon-collection")

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        val def = defaultGetAnswer(org, surveyName, slug)
        if (def == "true") {
            return def
        }

        val cgId = collectionGroupIdFromSurvey(surveyName)
        val cg = lookup.collectionGroupOrThrow(org, cgId)
        val allCollectedPic = dataMapService.getAllCollectedPic(listOf(cg))

        // check for uncommon collection
        if (allCollectedPic.any { PICSurveyClassification.UNCOMMON == it.pic?.getClassification() }) {
            return "true"
        }
        return def
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        return defaultWrite(org, answer, questionText, surveyName, slug)
    }

    private fun collectionGroupIdFromSurvey(surveyName: String): UUID {
        return UUID.fromString(surveyName.removePrefix(CONSUMER_DATA_COLLECTION_SURVEY_PRFEIX))
    }
}
