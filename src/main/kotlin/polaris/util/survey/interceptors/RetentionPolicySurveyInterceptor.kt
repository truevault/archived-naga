package polaris.util.survey.interceptors

import polaris.models.entity.organization.DATA_RETENTION_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.privacycenter.RETENTION_INTRO_SLUG
import polaris.repositories.PrivacyCenterRepository
import polaris.services.support.PrivacyCenterBuilderService

class RetentionPolicySurveyInterceptor(
    private val privacyCenterRepo: PrivacyCenterRepository,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService
) : SurveyInterceptor {
    override fun intercepts(surveyName: String) = surveyName == DATA_RETENTION_SURVEY_NAME
    override fun slugs(surveyName: String) = listOf("retention-policy")

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        val pc = org.privacyCenter()
        return pc?.retentionIntro()
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        val pc = org.privacyCenter()

        if (pc != null) {
            val old = pc.retentionIntro()

            if (answer == null) {
                pc.clearCustomText(RETENTION_INTRO_SLUG)
            } else {
                pc.setCustomText(RETENTION_INTRO_SLUG, answer)
            }

            val changed = pc.retentionIntro() != old
            if (changed) {
                privacyCenterRepo.save(pc)
                privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(org, "retention policy updated")
            }
        }
    }
}
