package polaris.util.survey.interceptors

import polaris.models.entity.organization.BUSINESS_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.repositories.OrganizationSurveyQuestionRepository

class B2BSurveyInterceptor(
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository
) : SurveyInterceptor {
    private val surveyName = BUSINESS_SURVEY_NAME
    private val questionSlug = "products-services-individuals-and-or-organizations"

    override fun intercepts(surveyName: String) = surveyName == surveyName
    override fun slugs(surveyName: String) = listOf(questionSlug)

    override fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        val existing = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, surveyName, questionSlug)
        if (existing?.answer == "individuals-and-organizations" || existing?.answer == "organizations") {
            return "true"
        } else if (existing?.answer == "individuals") {
            return "false"
        }
        return null
    }

    override fun write(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        var modifiedAnswer = answer
        modifiedAnswer = if (answer == "true") {
            "individuals-and-organizations"
        } else {
            "individuals"
        }

        val existing = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, surveyName, questionSlug)
        var question: OrganizationSurveyQuestion? = null
        if (existing == null) {
            question = OrganizationSurveyQuestion(
                organization = org,
                surveyName = surveyName,
                slug = questionSlug,
                question = questionText ?: "",
                answer = modifiedAnswer
            )
        } else {
            question = existing
            question.question = questionText ?: ""
            question.answer = modifiedAnswer
        }

        organizationSurveyQuestionRepository.save(question)
    }
}
