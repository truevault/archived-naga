package polaris.util.survey.interceptors

import polaris.models.entity.organization.Organization
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.SurveyService

abstract class BaseSurveyInterceptor(protected val surveyRepo: OrganizationSurveyQuestionRepository, private val surveyService: SurveyService) : SurveyInterceptor {
    protected fun defaultWrite(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        surveyService.writeSurveyAnswer(org, answer, questionText, surveyName, slug)
    }

    protected fun defaultGetAnswer(org: Organization, surveyName: String, slug: String): String? {
        return surveyService.getAnswer(org, surveyName, slug)
    }
}
