package polaris.util

import com.google.i18n.phonenumbers.PhoneNumberUtil

object PhoneNumberUtil {
    fun checkMatch(a: String, b: String): Boolean {
        val util = PhoneNumberUtil.getInstance()
        val matchType = util.isNumberMatch(a, b)

        return matchType == PhoneNumberUtil.MatchType.NSN_MATCH || matchType == PhoneNumberUtil.MatchType.EXACT_MATCH || matchType == PhoneNumberUtil.MatchType.SHORT_NSN_MATCH
    }

    fun formatPhoneNumber(phoneNumber: String, format: com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat = PhoneNumberUtil.PhoneNumberFormat.E164): String {
        val util = PhoneNumberUtil.getInstance()
        val parsedNumber = util.parse(phoneNumber, "US")
        return util.format(parsedNumber, format)
    }
}
