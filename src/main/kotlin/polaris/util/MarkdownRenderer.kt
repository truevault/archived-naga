package polaris.util

import org.commonmark.ext.autolink.AutolinkExtension
import org.commonmark.ext.gfm.strikethrough.StrikethroughExtension
import org.commonmark.ext.gfm.tables.TablesExtension
import org.commonmark.ext.task.list.items.TaskListItemsExtension
import org.commonmark.node.Node
import org.commonmark.parser.Parser
import org.commonmark.renderer.html.HtmlRenderer

object MarkdownRenderer {
    fun renderToHtml(markdown: String): String {
        val extensions = listOf(
            TablesExtension.create(),
            StrikethroughExtension.create(),
            AutolinkExtension.create(),
            TaskListItemsExtension.create()
        )
        val parser: Parser = Parser.builder()
            .extensions(extensions)
            .build()
        val renderer: HtmlRenderer = HtmlRenderer.builder()
            .extensions(extensions)
            .build()

        val document: Node = parser.parse(markdown)

        return renderer.render(document)
    }

    fun optRenderToHtml(markdown: String?): String? {
        if (markdown == null) { return null }
        return renderToHtml(markdown)
    }
}
