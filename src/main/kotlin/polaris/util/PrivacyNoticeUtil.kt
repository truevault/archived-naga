package polaris.util

import polaris.models.dto.compliance.DisclosureDto
import polaris.models.dto.compliance.FullDataMapDto
import polaris.models.dto.compliance.PersonalInformationSourceDto
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY
import polaris.services.primary.CollectionCategoriesMap
import polaris.services.primary.DisclosureCategory
import polaris.services.primary.DisclosureCategoryByVendor
import polaris.services.primary.SERVICE_PROVIDER_LABEL
import polaris.util.sorting.PICGroupDtoSort
import java.util.UUID

class PrivacyNoticeUtil {
    companion object {
        private fun addNoneIfEmpty(list: List<String>): List<String> {
            if (list.isEmpty()) {
                return listOf("None")
            }
            return list
        }

        fun sourcesListFromCollectionCategoriesMap(collectionCategories: CollectionCategoriesMap): List<String> {
            return collectionCategories.keys.map { s -> StringUtil.hardcodedPluralize(s) }
        }

        fun sourcesListFromDataMap(map: FullDataMapDto, collectionContext: CollectionContext): List<String> {
            return map.sources.values
                .asSequence()
                .filter { it.context.contains(collectionContext) }
                .sortedWith(compareBy({ it.displayOrder }, { it.category }))
                .map { s -> s.category }
                .map { s -> StringUtil.hardcodedPluralize(s) }
                .filter { s -> s != PersonalInformationSourceDto.YOU_CATEGORY && s != PersonalInformationSourceDto.YOU_NAME }
                .distinct()
                .toList()
        }

        fun getServiceAndThirdPartyDisclosureCategories(disclosures: List<DisclosureDto>, dataMap: FullDataMapDto, recipientsByVendor: Map<UUID, OrganizationDataRecipient>): List<DisclosureCategoryByVendor> {
            val disclosurePics = disclosures.flatMap { it.disclosed.map { id -> dataMap.categories[id]!! } }
            val disclosurePicsByGroup = disclosurePics
                .groupBy { it.picGroupId }
                .toSortedMap(PICGroupDtoSort.compareByGroupID(dataMap))

            val disclosureCategories = disclosurePicsByGroup.map { (groupId, categories) ->
                val group = dataMap.picGroups[groupId]!!
                val picIds = categories.map { it.id }.toSet()
                val recipientsByDisclosure = disclosures
                    .filter { d -> d.disclosed.any { id -> picIds.contains(id) } }
                    .map { recipientsByVendor[UUID.fromString(it.vendorId)] }

                val filteredServiceDataRecipients = recipientsByDisclosure.filter { r -> r?.vendorClassification?.slug == VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER }.map { getDisclosureCategoryLabel(it) }.distinct().sorted()
                val filteredThirdPartyDataRecipients = recipientsByDisclosure.filter { r -> r?.vendorClassification?.slug == VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY }.map { getDisclosureCategoryLabel(it) }.distinct().sorted()
                val serviceDataRecipients = addNoneIfEmpty(filteredServiceDataRecipients)
                val thirdPartyDataRecipients = addNoneIfEmpty(filteredThirdPartyDataRecipients)
                DisclosureCategoryByVendor(picGroup = group.name, serviceCategories = StringUtil.joinNatural(serviceDataRecipients), thirdPartyCategories = StringUtil.joinNatural(thirdPartyDataRecipients))
            }

            return disclosureCategories
        }

        fun getDisclosureCategories(disclosures: List<DisclosureDto>, dataMap: FullDataMapDto, recipientsByVendor: Map<UUID, OrganizationDataRecipient>): List<DisclosureCategory> {
            val disclosurePics = disclosures.flatMap { it.disclosed.map { id -> dataMap.categories[id]!! } }
            val disclosurePicsByGroup = disclosurePics
                .groupBy { it.picGroupId }
                .toSortedMap(PICGroupDtoSort.compareByGroupID(dataMap))

            val disclosureCategories = disclosurePicsByGroup.map { (groupId, categories) ->
                val group = dataMap.picGroups[groupId]!!
                val picIds = categories.map { it.id }.toSet()
                val dataRecipients = disclosures
                    .filter { d -> d.disclosed.any { id -> picIds.contains(id) } }
                    .map { recipientsByVendor[UUID.fromString(it.vendorId)] }
                    .map { getDisclosureCategoryLabel(it) }
                    .distinct()
                    .sorted()

                DisclosureCategory(picGroup = group.name, recipientCategories = StringUtil.joinNatural(dataRecipients))
            }

            return disclosureCategories
        }

        fun getDisclosureCategoryLabel(ov: OrganizationDataRecipient?): String {
            val category = ov?.vendor?.recipientCategory ?: ""
            return StringUtil.hardcodedPluralize(category)
        }

        fun getRecipientCategoryLabel(ov: OrganizationDataRecipient?): String {
            if (ov?.vendorClassification?.slug == VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER) {
                return SERVICE_PROVIDER_LABEL
            }

            val category = ov?.vendor?.recipientCategory ?: ""

            return StringUtil.hardcodedPluralize(category)
        }
    }
}
