package polaris.util

import java.lang.RuntimeException

open class PolarisException(message: String? = null) : RuntimeException(message)
