package polaris.util

import java.time.ZonedDateTime

fun ZonedDateTime.atStartOfDay() =
    this
        .toLocalDate()
        .atStartOfDay()
        .atZone(this.zone)
        .withEarlierOffsetAtOverlap()

fun ZonedDateTime.atEndOfDay() =
    this
        .toLocalDate()
        .plusDays(1)
        .atStartOfDay()
        .atZone(this.zone)
        .withEarlierOffsetAtOverlap()
        .minusSeconds(1)
