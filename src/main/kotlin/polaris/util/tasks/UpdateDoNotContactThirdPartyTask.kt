package polaris.util.tasks

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey

class UpdateDoNotContactThirdPartyTask : TaskManager {
    override fun manages() = TaskKey.UPDATE_DO_NOT_CONTACT_THIRD_PARTY
    override fun name() = "CPRA Regulation Changes: Review Third Party Settings"
    override fun description(org: Organization) = "Recent regulatory changes require you to review and update your contact settings for Third Parties for which you previously selected “Do Not Contact.” Visit <a href=\"/organization/requestHandling/requestToDelete\">Manage Request to Delete Settings</a> and click into the highlighted Data Recipients to review their settings."
    override fun completionType(): TaskCompletionType = TaskCompletionType.MANUAL

    // created by a migration and closed manually
}
