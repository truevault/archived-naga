package polaris.util.tasks

import polaris.models.SurveySlugs.Companion.WEBSITE_AUDIT_DEFER_SLUG
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.organization.WEBSITE_AUDIT_SURVEY_NAME
import polaris.repositories.OrganizationSurveyQuestionRepository

class WebsiteAuditDeferTask(
    private val orgSurveyQuestionRepo: OrganizationSurveyQuestionRepository
) : TaskManager {
    override fun manages() = TaskKey.WEBSITE_AUDIT_DEFER_TASK
    override fun name() = "Run an Audit on Your Website"
    override fun description(org: Organization) = "We’ll scan your site to ensure all required links are present. Visit the <a target='_blank' rel='noopener noreferrer' href='/surveys/midyear-2023/website-audit'>Website Audit</a> page and enter your URL to begin your scan."
    override fun completionType(): TaskCompletionType = TaskCompletionType.AUTOMATIC

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }
        val surveyQuestion = orgSurveyQuestionRepo.findByOrganizationAndSurveyNameAndSlug(org, WEBSITE_AUDIT_SURVEY_NAME, WEBSITE_AUDIT_DEFER_SLUG)
        return surveyQuestion?.answer == "CREATE_TASK"
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        val surveyQuestion = orgSurveyQuestionRepo.findByOrganizationAndSurveyNameAndSlug(org, WEBSITE_AUDIT_SURVEY_NAME, WEBSITE_AUDIT_DEFER_SLUG)
        return surveyQuestion?.answer != "CREATE_TASK"
    }
}
