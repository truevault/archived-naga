package polaris.util.tasks

import polaris.models.SurveySlugs.Companion.DATA_MINIMIZATION_SLUG
import polaris.models.entity.organization.CORE_PRIVACY_CONCEPTS_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey
import polaris.repositories.OrganizationSurveyQuestionRepository

class DataMinimizationTask(
    private val orgSurveyQuestionRepo: OrganizationSurveyQuestionRepository
) : TaskManager {
    override fun manages() = TaskKey.DATA_MINIMIZATION_TASK
    override fun name() = "Review Data Minimization Practices"
    override fun description(org: Organization) = "Your business indicated that you may need to take another look at whether all the data you collect is needed. <a target='_blank' rel='noopener noreferrer' href='https://www.truevault.com/resources/ccpa/ccpa-data-minimization-rules'>Here’s an article</a> on the data minimization requirement and what it requires of your business. When your business has implemented any necessary minimization steps, mark this task as completed. If you have any questions, contact our team at <a href='mailto:help@truevault.com'>help@truevault.com</a>."
    override fun completionType(): TaskCompletionType = TaskCompletionType.MANUAL

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }
        val surveyQuestion = orgSurveyQuestionRepo.findByOrganizationAndSurveyNameAndSlug(org, CORE_PRIVACY_CONCEPTS_SURVEY_NAME, DATA_MINIMIZATION_SLUG)
        val answer = surveyQuestion?.answer ?: return false
        return answer.contains("\"maybe-not\"")
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return false
    }
}
