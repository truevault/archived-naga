package polaris.util.tasks

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.vendor.DataRecipientType
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.services.dto.OrganizationVendorDtoService

class CompleteDataRecipientsTask(
    private val orgDataRecipientRepo: OrganizationDataRecipientRepository,
    private val organizationVendorDtoService: OrganizationVendorDtoService
) : TaskManager {

    override fun manages() = TaskKey.COMPLETE_DATA_RECIPIENTS
    override fun name() = "Complete Data Recipient Details"
    override fun description(org: Organization) = "You have incomplete data recipients. Go to your <a href='/data-map/?tab=data_recipients'>Data Map</a> and look for Data Recipients labeled \"Incomplete.\""
    override fun callToAction(): String = "Do this now"
    override fun callToActionUrl(): String = "/data-map/?tab=data_recipients"

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {

        if (existing.any { it.taskKey == manages() }) {
            return false
        }

        return !isDone(org)
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return isDone(org)
    }

    private fun isDone(org: Organization): Boolean {

        val dataRecipients = orgDataRecipientRepo.findAllByOrganization(org)

        val anyUnclassified = dataRecipients.any {
            val isVendor = it.vendor!!.dataRecipientType == DataRecipientType.vendor
            val vendorClassificationNull = it.vendorClassification == null
            val vendorClassificationNeedsClassification = it.vendorClassification?.slug == VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW

            isVendor && (vendorClassificationNull || vendorClassificationNeedsClassification)
        }

        if (anyUnclassified) {
            return false
        }

        val incompleteDataRecipients = dataRecipients.filter { !it.completed }
        val actuallyIncomplete = incompleteDataRecipients.filter { !organizationVendorDtoService.isComplete(it) }

        return actuallyIncomplete.isEmpty()
    }
}
