package polaris.util.tasks

import polaris.models.entity.organization.MIDYEAR_CATCHUP_SURVEY
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey
import polaris.repositories.OrganizationSurveyQuestionRepository

class UpdateGoogleAnalyticsIdTask(
    private val orgSurveyQuestionRepo: OrganizationSurveyQuestionRepository
) : TaskManager {
    override fun manages() = TaskKey.UPDATE_GOOGLE_ANALYTICS_ID
    override fun name() = "Update your GA ID to Google Analytics 4"
    override fun description(org: Organization) = "Starting July 1, 2023, standard Google’s Universal Analytics properties will stop processing new data. <a href=\"/organization/${org.publicId}/settings/integrations\">Update your GA ID in our system</a> to the new Google Analytics 4 ID."
    override fun completionType(): TaskCompletionType = TaskCompletionType.AUTOMATIC

    // created by a migration and closed manually
    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        val isWebPropertyMatchRegex = Regex("^([0-9]{9})$")
        val requestMessageMatch = isWebPropertyMatchRegex.find(org.gaWebProperty ?: "")?.destructured?.toList()
        return !requestMessageMatch.isNullOrEmpty()
    }

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }
        val deferGoogleAnalyticsTask = orgSurveyQuestionRepo.findByOrganizationAndSurveyNameAndSlug(org, MIDYEAR_CATCHUP_SURVEY, "tasks-intro-google-analytics")
        return deferGoogleAnalyticsTask?.answer == "deferred"
    }
}
