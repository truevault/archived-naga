package polaris.util.tasks

import polaris.models.dto.ApiMode
import polaris.models.entity.organization.FINANCIAL_INCENTIVES_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import polaris.services.primary.OrganizationSurveyService

class CompleteFinancialIncentivesTask(val surveyService: OrganizationSurveyService) : TaskManager {
    override fun manages() = TaskKey.COMPLETE_FINANCIAL_INCENTIVES
    override fun name() = "Complete Financial Incentives"
    override fun description(org: Organization) = "Go to <a href=\"/notices\">Notices</a> to complete your Financial Incentive disclosure."

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }

        return shouldBeOpen(org)
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return !shouldBeOpen(org)
    }

    private fun shouldBeOpen(org: Organization): Boolean {
        val incentivesSurvey = surveyService.getSurveyQuestions(org.publicId, ApiMode.Live, FINANCIAL_INCENTIVES_SURVEY_NAME)
        val anyFinancialIncentives = incentivesSurvey.any { it.slug == "offer-consumer-incentives" && it.answer == "true" }

        if (!anyFinancialIncentives) {
            return false
        }

        return org.financialIncentiveDetails.isNullOrBlank()
    }
}
