package polaris.util.tasks

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.REMINDER_SURVEY_NAME
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import polaris.repositories.OrganizationSurveyQuestionRepository

class UnsafeTransferReminderTask(
    private val orgSurveyQuestionRepo: OrganizationSurveyQuestionRepository
) : TaskManager {

    override fun manages() = TaskKey.UNSAFE_TRANSFERS_REMINDER
    override fun name() = "Unsafe Data Transfers"
    override fun description(org: Organization) = "Your organization must update SCC documentation for contacted processors and cease its use of providers with unsafe data transfers."

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }

        val unsafeTransfersQuestion = orgSurveyQuestionRepo.findByOrganizationAndSurveyNameAndSlug(org, REMINDER_SURVEY_NAME, "reminder-unsafe-transfers")

        return unsafeTransfersQuestion?.answer == "true"
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return isDone(org)
    }

    private fun isDone(org: Organization): Boolean {
        // Manual task completion is future ticket
        return false
    }
}
