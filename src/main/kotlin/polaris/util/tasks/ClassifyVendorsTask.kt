package polaris.util.tasks

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.vendor.DataRecipientType
import polaris.repositories.OrganizationDataRecipientRepository

@Deprecated("POLARIS-2355 - deprecated in favor of COMPLETE_DATA_RECIPIENTS. Added back temporarily due to POLARIS-2521")
class ClassifyVendorsTask(
    private val orgDataRecipientRepo: OrganizationDataRecipientRepository
) : TaskManager {
    override fun manages() = TaskKey.CLASSIFY_VENDORS
    override fun name() = "Classify Vendors"
    override fun description(org: Organization) = "Some of your vendors have not been classified as either a â€œservice providerâ€ or a â€œthird party.â€ Go to <a href='/data-map?tab=data_recipients'>Data Recipients</a> and look for vendors labeled â€œNeeds Classification.â€"

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        // POLARIS-2521 - should never create moving forward. added back temporarily due to presence of open CLASSIFY_VENDOR tasks in the DB
        return false
        // if (existing.any { it.taskKey == manages() }) {
        //     return false
        // }
        //
        // val dataRecipients = orgDataRecipientRepo.findAllByOrganization(org)
        // return dataRecipients.any {
        //     val isVendor = it.vendor!!.dataRecipientType == DataRecipientType.vendor
        //     val vendorClassificationNull = it.vendorClassification == null
        //     val vendorClassificationNeedsClassification = it.vendorClassification?.slug == VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW
        //
        //     isVendor && (vendorClassificationNull || vendorClassificationNeedsClassification)
        // }
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        val dataRecipients = orgDataRecipientRepo.findAllByOrganization(org)
        return dataRecipients.none { it.vendor!!.dataRecipientType == DataRecipientType.vendor && it.vendorClassification == null }
    }
}
