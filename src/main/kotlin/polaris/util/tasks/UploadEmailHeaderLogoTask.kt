package polaris.util.tasks

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey

class UploadEmailHeaderLogoTask() : TaskManager {
    override fun manages() = TaskKey.UPLOAD_EMAIL_HEADER_LOGO
    override fun name() = "Upload Email Header Logo"
    override fun description(org: Organization) = "Polaris does not have a logo to display in emails it sends on your business’s behalf. Go to <a href='/organization/${org.publicId}/settings/inbox'>Privacy Email</a> in Settings to upload a logo."

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }

        return org.headerImageUrl.isBlank()
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return org.headerImageUrl.isNotBlank()
    }
}
