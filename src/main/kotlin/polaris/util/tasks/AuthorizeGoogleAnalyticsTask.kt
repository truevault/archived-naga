package polaris.util.tasks

import org.slf4j.LoggerFactory
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationAuthTokenProvider
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.vendor.KnownVendorKey
import polaris.repositories.OrganizationAuthTokenRepository

class AuthorizeGoogleAnalyticsTask(val orgAuthTokenRepo: OrganizationAuthTokenRepository) : TaskManager {
    companion object {
        private val log = LoggerFactory.getLogger(AuthorizeGoogleAnalyticsTask::class.java)
    }

    override fun manages() = TaskKey.AUTHORIZE_GOOGLE_ANALYTICS
    override fun name() = "Authorize Google Analytics"
    override fun description(org: Organization) =
        "<a href='/organization/${org.publicId}/settings/integrations'>Connect your Google Analytics account</a> so your team can process consumer privacy requests for Google Analytics."

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }

        return isRequired(org) && !isDone(org)
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return isDone(org) || !isRequired(org)
    }

    private fun isDone(org: Organization): Boolean {
        return orgAuthTokenRepo.findByOrganizationAndProvider(org, OrganizationAuthTokenProvider.GOOGLE_OAUTH) != null
    }

    private fun isRequired(org: Organization): Boolean {
        return org.organizationDataRecipients.any {
            it.vendor?.isKnownVendor(KnownVendorKey.GOOGLE_ANALYTICS) ?: false
        }
    }
}
