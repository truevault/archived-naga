package polaris.util.tasks

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey
import polaris.repositories.OrganizationRepository
import polaris.repositories.TaskRepository
import polaris.services.support.OptOutService

class NoLongerSharingOrSellingTask(private val optOutService: OptOutService, private val organizationRepository: OrganizationRepository, private val taskRepository: TaskRepository) : TaskManager {
    override fun manages() = TaskKey.NO_LONGER_SELLING_OR_SHARING
    override fun name() = "Remove Your Opt-Out Link"
    override fun description(org: Organization) = "Changes to your Data Map indicate that you no longer sell or share personal data. An opt-out link is no longer required on your site and you can remove the \"Do Not Sell/Share\" link previously added to your website footer."

    override fun completionType(): TaskCompletionType = TaskCompletionType.MANUAL

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        // If you currently have this task or ever had this task, then do not re-create
        if (existing.any { it.taskKey == manages() }) {
            return false
        }

        val optOutAttrs = optOutService.getOptOutAttributes(org)

        if (org.isSharingLastState == null) {
            org.isSharingLastState = optOutAttrs.anySharing
        }
        if (org.isSellingLastState == null) {
            org.isSellingLastState = optOutAttrs.anySelling
        }
        val wasSellingOrSharing = org.isSharingLastState ?: optOutAttrs.anySharing || org.isSellingLastState ?: optOutAttrs.anySharing
        val isNowNotSellingOrSharing = !optOutAttrs.anySharing && !optOutAttrs.anySelling && !optOutAttrs.isSellingInExchangeForMoney
        return wasSellingOrSharing && isNowNotSellingOrSharing
    }

    override fun afterCreate(task: Task) {
        if (task.organization == null) {
            return
        }

        val optOutAttrs = optOutService.getOptOutAttributes(task.organization)
        task.organization.isSharingLastState = optOutAttrs.anySharing
        task.organization.isSellingLastState = optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney
        organizationRepository.save(task.organization)
    }

    override fun afterClose(task: Task) {
        if (task.organization == null) {
            return
        }

        val optOutAttrs = optOutService.getOptOutAttributes(task.organization)
        if (optOutAttrs.anySharing || optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney) {
            // We auto-closed due to adding back the sharing/selling state
            task.organization.isSharingLastState = optOutAttrs.anySharing
            task.organization.isSellingLastState = optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney
            organizationRepository.save(task.organization)
            taskRepository.delete(task)
        }
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        val optOutAttrs = optOutService.getOptOutAttributes(org)

        // Now selling or sharing (Manually Closed if actually completed)
        return optOutAttrs.anySharing || optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney
    }
}
