package polaris.util.tasks

import polaris.models.SurveySlugs.Companion.WEBSITE_AUDIT_OPT_OUT_LINK_UPGRADE_SLUG
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.organization.WEBSITE_AUDIT_SURVEY_NAME
import polaris.repositories.OrganizationSurveyQuestionRepository

class WebsiteAuditOptOutLinkUpgradeTask(
    private val orgSurveyQuestionRepo: OrganizationSurveyQuestionRepository
) : TaskManager {
    override fun manages() = TaskKey.WEBSITE_AUDIT_OPT_OUT_LINK_UPGRADE_TASK
    override fun name() = "Update Opt-Out Link to “Your Privacy Choices” Icon"
    override fun description(org: Organization) = "California now allows you to replace your “Do Not Sell/Share My Personal Information” link with a general opt-out link that can be used for California and other states with data privacy laws, including Virginia, Colorado, and Connecticut. View your <a target='_blank' rel='noopener noreferrer' href='/instructions/${org.devInstructionsUuid}'>developer instructions</a> to update your site."
    override fun completionType(): TaskCompletionType = TaskCompletionType.AUTOMATIC

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }
        val surveyQuestion = orgSurveyQuestionRepo.findByOrganizationAndSurveyNameAndSlug(org, WEBSITE_AUDIT_SURVEY_NAME, WEBSITE_AUDIT_OPT_OUT_LINK_UPGRADE_SLUG)
        return surveyQuestion?.answer == "CREATE_TASK"
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return false
    }
}
