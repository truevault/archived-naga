package polaris.util.tasks

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey
import java.time.ZonedDateTime
import java.util.*

interface TaskManager {
    fun manages(): TaskKey
    fun name(): String
    fun description(org: Organization): String

    fun callToAction(): String? = null
    fun callToActionUrl(): String? = null

    fun allowDuplicates(): Boolean = false

    fun dueAt(): ZonedDateTime? = null

    fun completionType(): TaskCompletionType = TaskCompletionType.AUTOMATIC

    fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task> = emptyList()): Boolean {
        return false
    }

    fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return false
    }

    fun afterCreate(task: Task) {}
    fun afterClose(task: Task) {}

    fun createFor(org: Organization, name: String? = null): Task {
        return Task(
            id = UUID.randomUUID(),
            taskKey = manages(),
            name = name ?: name(),
            description = description(org),
            createdAt = ZonedDateTime.now(),
            callToAction = callToAction(),
            callToActionUrl = callToActionUrl(),
            completionType = completionType(),
            dueAt = dueAt(),
            organization = org
        )
    }
}
