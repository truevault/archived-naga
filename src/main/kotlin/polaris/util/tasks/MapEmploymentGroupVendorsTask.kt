package polaris.util.tasks

import org.slf4j.LoggerFactory
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import polaris.services.support.DataMapService
import java.util.UUID

class MapEmploymentGroupVendorsTask(val dataMapService: DataMapService) : TaskManager {
    companion object {
        private val log = LoggerFactory.getLogger(MapEmploymentGroupVendorsTask::class.java)
    }

    override fun manages() = TaskKey.MAP_EMPLOYMENT_GROUP
    override fun name() = "Map your employment group"
    override fun description(org: Organization) = "Starting in January 2023, the CCPA exemption for mapping collected information about your employees is expiring. In order to comply with the new regulations, you will need to add any data recipients to your data map which may have been missed during your initial onboarding."

    override fun allowDuplicates(): Boolean = true

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        // this is a manual-only creation task
        return false
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        val collectionGroupId = t.taskMeta?.get("collectionGroupId") ?: return true
        val collectionGroup = t.organization?.collectionGroups?.find { it.id == UUID.fromString(collectionGroupId) } ?: return true

        if (dataMapService.collectionGroupCollectsPic(collectionGroup) && dataMapService.collectionGroupDisclosesPic(collectionGroup) && dataMapService.collectionGroupReceivesPic(collectionGroup)) {
            return true
        }

        return false
    }
}
