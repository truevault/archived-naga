package polaris.util.tasks

import org.slf4j.LoggerFactory
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import polaris.services.support.DpoService

class AddDpoTask(val dpoService: DpoService) : TaskManager {
    companion object {
        private val log = LoggerFactory.getLogger(AddDpoTask::class.java)
    }

    override fun manages() = TaskKey.ADD_DPO
    override fun name() = "Add Data Protection Officer"
    override fun description(org: Organization) = "Your organization requires a Data Protection Officer. Go to your privacy center settings and add the details for your Data Protection Officer."

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }
        val hasDpoFields = isDone(org)
        return !hasDpoFields && dpoService.isDpoRequired(org)
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return isDone(org) || !dpoService.isDpoRequired(org)
    }

    private fun isDone(org: Organization): Boolean {
        val pc = org.privacyCenter()
        log.info("CHECKING IF ADD DPO TASK IS DONE: ${pc?.dpoOfficerEmail != null && pc.dpoOfficerName != null}")
        return pc?.dpoOfficerEmail != null && pc.dpoOfficerName != null
    }
}
