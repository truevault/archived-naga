package polaris.util.tasks

import polaris.models.SurveySlugs.Companion.WEBSITE_AUDIT_PRIVACY_POLICY_LINK_SLUG
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.organization.WEBSITE_AUDIT_SURVEY_NAME
import polaris.repositories.OrganizationSurveyQuestionRepository

class WebsiteAuditPrivacyLinkTask(
    private val orgSurveyQuestionRepo: OrganizationSurveyQuestionRepository
) : TaskManager {
    override fun manages() = TaskKey.WEBSITE_AUDIT_PRIVACY_LINK_TASK
    override fun name() = "Add Missing Privacy Policy Link to Website Footer"
    override fun description(org: Organization) = "Your Privacy Policy link is missing from your website footer or does not link to TrueVault-hosted Privacy Center. View your <a target='_blank' rel='noopener noreferrer' href='/instructions/${org.devInstructionsUuid}'>developer instructions</a> to update your site."
    override fun completionType(): TaskCompletionType = TaskCompletionType.AUTOMATIC

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        if (existing.any { it.taskKey == manages() }) {
            return false
        }
        val surveyQuestion = orgSurveyQuestionRepo.findByOrganizationAndSurveyNameAndSlug(org, WEBSITE_AUDIT_SURVEY_NAME, WEBSITE_AUDIT_PRIVACY_POLICY_LINK_SLUG)
        return surveyQuestion?.answer == "CREATE_TASK"
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        return false
    }
}
