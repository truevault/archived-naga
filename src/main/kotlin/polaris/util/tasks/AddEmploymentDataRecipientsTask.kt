package polaris.util.tasks

import org.slf4j.LoggerFactory
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.vendor.ProcessingRegion
import polaris.services.primary.TaskService
import java.time.ZonedDateTime

class AddEmploymentDataRecipientsTask(val taskService: TaskService) : TaskManager {
    companion object {
        private val log = LoggerFactory.getLogger(AddEmploymentDataRecipientsTask::class.java)
    }

    override fun manages() = TaskKey.ADD_EMPLOYMENT_DATA_RECIPIENTS
    override fun name() = "Add Your Employment-Specific Data Recipients"
    override fun description(org: Organization) = "Starting in January 2023, the CCPA exemption for mapping collected information about your employees is expiring. In order to comply with the new regulations, you will need to add any data recipients to your data map which may have been missed during your initial onboarding."

    override fun callToAction(): String = "Do this now"
    override fun callToActionUrl(): String = "/data-map/employee-data-recipients"

    override fun completionType(): TaskCompletionType = TaskCompletionType.MANUAL

    override fun dueAt(): ZonedDateTime? {
        return ZonedDateTime.of(2023, 1, 15, 0, 0, 0, 0, POLARIS_TIME_ZONE)
    }

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {
        // If you currently have this task or ever had this task, then do not re-create
        if (existing.any { it.taskKey == manages() } || closed.any { it.taskKey == manages() }) {
            return false
        }

        // Don't create if the org has any data recipients in the employment context;
        // this will prevent orgs who have already done this in get compliant from
        // seeing this task
        if (org.organizationDataRecipients.any { it.collectionContexts.contains(CollectionContext.EMPLOYEE) }) {
            return false
        }

        return org.collectionGroups.any { it.collectionGroupType == CollectionGroupType.EMPLOYMENT }
    }

    override fun afterClose(task: Task) {
        if (task.organization == null) {
            return
        }

        val collectionGroups = task.organization.collectionGroups.filter { it.collectionGroupType == CollectionGroupType.EMPLOYMENT && it.processingRegions.contains(ProcessingRegion.UNITED_STATES) }
        val manager = taskService.managerFor(TaskKey.MAP_EMPLOYMENT_GROUP) ?: return

        collectionGroups.forEach { group ->
            val newTask = manager.createFor(task.organization, "Map your ${group.name}")

            newTask.callToAction = "Do this Now"
            newTask.callToActionUrl = "/data-map/collection/new/${group.id}/collection"
            newTask.dueAt = ZonedDateTime.now().plusDays(14)
            newTask.taskMeta = mapOf("collectionGroupId" to group.id.toString())

            // only create if we don't already meet the requirements for closing
            if (!manager.shouldClose(newTask, task.organization, emptyList())) {
                taskService.createTask(newTask)
            }
        }
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        // This is a manual-only task
        return false
    }
}
