package polaris.util.tasks

import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey
import polaris.repositories.OrganizationRepository
import polaris.repositories.TaskRepository
import polaris.services.support.OptOutService

class PublishSellingOrSharingTask(private val optOutService: OptOutService, private val organizationRepository: OrganizationRepository, private val taskRepository: TaskRepository) : TaskManager {
    override fun manages() = TaskKey.PUBLISH_SELLING_OR_SHARING
    override fun name() = "Publish Your Opt-Out Link"
    override fun description(org: Organization) = "Changes to your Data Map require your business to post a link for consumers to exercise their right to opt-out of the sale and/or sharing of their personal information. Visit your <a target='_blank' rel='noopener noreferrer' href='/instructions/${org.devInstructionsUuid}'>developer instructions</a> and follow the steps under \"Add Opt-Out Notice to website footer.\""

    override fun completionType(): TaskCompletionType = TaskCompletionType.MANUAL

    override fun shouldCreate(org: Organization, existing: List<Task>, closed: List<Task>): Boolean {

        // If you currently have this task then do not re-create
        if (existing.any { it.taskKey == manages() }) {
            return false
        }

        val optOutAttrs = optOutService.getOptOutAttributes(org)
        val wasNotSellingOrSharing = !(org.isSharingLastState || org.isSellingLastState)
        val isNowSellingOrSharing = optOutAttrs.anySharing || optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney
        return wasNotSellingOrSharing && isNowSellingOrSharing
    }

    override fun afterCreate(task: Task) {
        if (task.organization == null) {
            return
        }

        val optOutAttrs = optOutService.getOptOutAttributes(task.organization)
        task.organization.isSharingLastState = optOutAttrs.anySharing
        task.organization.isSellingLastState = optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney
        organizationRepository.save(task.organization)
    }

    override fun afterClose(task: Task) {
        if (task.organization == null) {
            return
        }

        val optOutAttrs = optOutService.getOptOutAttributes(task.organization)
        organizationRepository.save(task.organization)
        if (!optOutAttrs.anySharing && !optOutAttrs.anySelling && !optOutAttrs.isSellingInExchangeForMoney) {
            // We auto-closed due to no longer being selling or sharing, delete the task so that it doesn't show up as completed
            task.organization.isSharingLastState = false
            task.organization.isSellingLastState = false
            taskRepository.delete(task)
        }
    }

    override fun shouldClose(t: Task, org: Organization, existing: List<Task>): Boolean {
        val optOutAttrs = optOutService.getOptOutAttributes(org)

        // Not selling or sharing (Manual otherwise)
        return !optOutAttrs.anySharing && !optOutAttrs.anySelling && !optOutAttrs.isSellingInExchangeForMoney
    }
}
