package polaris.util

import java.security.SecureRandom

object IdGenerator {
    const val SHORT_ID_LENGTH = 9

    private val random = SecureRandom()

    // RFC4648 - better presented in the UI as uppercase characters
    // https://en.wikipedia.org/wiki/Base32#RFC_4648_Base32_alphabet
    private const val symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"

    // z-base-32 - Better presented in the UI as lowercase characters
    // val symbols = "abcdefghijkmnopqrstuwxyz13456789"

    fun generate(length: Int = SHORT_ID_LENGTH): String {
        val buffer = CharArray(length)
        for (idx in 0 until length) {
            buffer[idx] = symbols[random.nextInt(symbols.length)]
        }

        return String(buffer)
    }
}
