package polaris.util

import java.text.Normalizer

object StringUtil {

    fun slugify(word: String, replacement: String = "-") = Normalizer
        .normalize(word, Normalizer.Form.NFD)
        .replace("[^\\p{ASCII}]".toRegex(), "")
        .replace("[^a-zA-Z0-9\\s]+".toRegex(), "").trim()
        .replace("\\s+".toRegex(), replacement)
        .toLowerCase()

    fun joinNatural(strings: List<String>): String {
        if (strings.isEmpty()) return ""
        if (strings.size == 1) return strings[0]
        if (strings.size == 2) {
            return "${strings[0]} and ${strings[1]}"
        }
        return "${strings.slice(IntRange(0, strings.lastIndex - 1)).joinToString(", ")}, and ${strings.last()}"
    }

    fun hardcodedPluralize(str: String): String {
        return HARDCODED_PLURALIZATIONS.getOrDefault(str, str)
    }

    private val HARDCODED_PLURALIZATIONS = mapOf(
        "Ad Network" to "Ad Networks",
        "Affiliate" to "Affiliates",
        "Business Operation Tool" to "Business Operation Tools",
        "Business Partner" to "Business Partners",
        "Business Service" to "Business Services",
        "Cloud Computing & Storage Provider" to "Cloud Computing & Storage Providers",
        "Collaboration & Productivity Tool" to "Collaboration & Productivity Tools",
        "Commerce Software Tool" to "Commerce Software Tools",
        "Communications Tool" to "Communications Tools",
        "Content Management System" to "Content Management Systems",
        "Contractor" to "Contractors",
        "Customer Support Tool" to "Customer Support Tools",
        "Cybersecurity Provider" to "Cybersecurity Providers",
        "Data Analytics Provider" to "Data Analytics Providers",
        "Data Broker" to "Data Brokers",
        "Data Buyer" to "Data Buyers",
        "Fraud Prevention Tool" to "Fraud Prevention Tools",
        "Government Entity" to "Government Entities",
        "Healthcare Provider" to "Healthcare Providers",
        "IT Infrastructure Service" to "IT Infrastructure Services",
        "Payment Processor" to "Payment Processors",
        "Sales & Marketing Tool" to "Sales & Marketing Tools",
        "Shipping Service" to "Shipping Services",
        "Social Network" to "Social Networks",
        "Web Hosting Service" to "Web Hosting Services",
        "Retail Partner" to "Retail Partners"
    )
}
