package polaris.util.states

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.persistence.AttributeConverter

abstract class StateMachineHistoryConverter<State : Enum<State>> : AttributeConverter<List<State>, String> {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(StateMachineHistoryConverter::class.java)
    }

    override fun convertToDatabaseColumn(attribute: List<State>?): String {
        log.info("SMHC convert to database column: $attribute")

        return try {
            jacksonObjectMapper().writeValueAsString(attribute)
        } catch (e: JsonProcessingException) {
            "[]"
        }
    }

    override fun convertToEntityAttribute(dbData: String?): List<State> {
        if (dbData == null) { return emptyList() }

        return try {
            jacksonObjectMapper().readValue(dbData)
        } catch (e: JsonProcessingException) {
            log.warn("Had a json processing exception: ", e)
            emptyList()
        }
    }
}
