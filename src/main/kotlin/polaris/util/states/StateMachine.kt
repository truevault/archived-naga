package polaris.util.states

import polaris.util.PolarisException

abstract class StateMachineException : PolarisException()

class NoInitialStateException : StateMachineException() {
    override val message: String get() = "No initial state was provided for the state machine."
}

class NoTransitionException(val from: String, val to: String? = null) : StateMachineException() {
    override val message: String get() =
        if (to == null) "There is no available transition from $from."
        else "There is no available transition from $from to $to."
}

class TransitionGuardFailedException(override val message: String) : StateMachineException()

data class StateMachineState<State : Enum<State>>(val current: State, val history: List<State>) {
    fun transition(next: State) = StateMachineState(next, history + listOf(current))
}

data class GuardResult(val allow: Boolean, val message: String)

data class StateTransition<Context, State : Enum<State>>(
    val from: State?, // null is FROM-ANY
    val to: State?, // null is TO-ANY

    val beforeListeners: List<(Context, State, State) -> Unit>,
    val guards: List<(Context, State, StateMachineState<State>) -> GuardResult>,
    val afterListeners: List<(Context, State, State) -> Unit>,

    val allowDefault: Boolean
)

class StateTransitionBuilder<Context, State : Enum<State>>(
    val from: State?,
    val to: State?,
    private val beforeListeners: MutableList<(Context, State, State) -> Unit> = mutableListOf(),
    private val guards: MutableList<(Context, State, StateMachineState<State>) -> GuardResult> = mutableListOf(),
    private val afterListeners: MutableList<(Context, State, State) -> Unit> = mutableListOf(),
    private var allowDefault: Boolean = true
) {

    fun before(l: (Context, State, State) -> Unit) = beforeListeners.add(l)

    fun after(l: (Context, State, State) -> Unit) = afterListeners.add(l)

    fun guard(g: (Context) -> GuardResult) {
        guards.add { c: Context, _: State, _: StateMachineState<State> -> g(c) }
    }

    fun allowDefault() = apply { allowDefault = true }
    fun disallowDefault() = apply { allowDefault = false }

    fun build() = StateTransition(from, to, beforeListeners, guards, afterListeners, allowDefault)
}

class StateMachine<Context, State : Enum<State>>(
    val context: Context,
    state: StateMachineState<State>? = null,
    private val definition: StateMachineDefinition<Context, State>
) {
    var state: StateMachineState<State> = state ?: initialState()

    val current: State get() = state.current

    fun transition(next: State? = null) {
        val transition = findTransition(next, true)

        if (transition == null) {
            val possibleTransitions = findTransitions(next, false)
            val guardFailures = collectGuardFailures(possibleTransitions)

            if (guardFailures.isEmpty()) {
                throw NoTransitionException(current.name, next?.name)
            } else {
                throw TransitionGuardFailedException(guardFailures.joinToString(", "))
            }
        }

        val allTransitions = findTransitions(next, false)

        val starting = state.current
        val destination = transition.to ?: next!!

        // before listeners

        definition.beforeListeners.forEach { it(context, starting, destination) }
        allTransitions.forEach { l ->
            l.beforeListeners.forEach { it(context, starting, destination) }
        }

        // Transition our internal state
        state = state.transition(destination)

        // after listeners
        definition.afterListeners.forEach { it(context, starting, destination) }
        allTransitions.forEach { l ->
            l.afterListeners.forEach { it(context, starting, destination) }
        }
    }

    fun canTransition(next: State? = null) = findTransition(next, true) != null

    private fun findTransition(next: State? = null, respectGuards: Boolean) = findTransitions(next, respectGuards).firstOrNull()
    private fun findTransitions(next: State? = null, respectGuards: Boolean): List<StateTransition<Context, State>> {
        val nextState = next ?: findNext() ?: return emptyList()

        val potentialTransitions = listOfNotNull(
            definition.find(current, nextState), // specific transitions
            definition.find(null, nextState), // from-any transitions
            definition.find(current, null) // to-any transitions
        )

        if (!respectGuards) { return potentialTransitions }

        return potentialTransitions.filter {
            it.guards.all { g -> g(context, current, state).allow }
        }
    }

    private fun collectGuardFailures(transitions: List<StateTransition<Context, State>>) =
        transitions.flatMap {
            it.guards.mapNotNull { g ->
                val result = g(context, current, state)
                if (result.allow) null else result.message
            }
        }

    private fun findNext(): State? {
        return definition.transitions.find { it.from == current && it.guards.all { g -> g(context, current, state).allow } }?.to
    }

    private fun initialState(): StateMachineState<State> = StateMachineState(definition.initial, emptyList())

    companion object {
        fun <Context, State : Enum<State>> define(def: StateMachineDefinitionBuilder<Context, State>.() -> Unit): StateMachineDefinition<Context, State> {
            val builder = StateMachineDefinitionBuilder<Context, State>()
            builder.def()

            return builder.build()
        }
    }
}

data class StateMachineDefinition<Context, State : Enum<State>>(
    val initial: State,
    val transitions: List<StateTransition<Context, State>>,
    val beforeListeners: List<(Context, State, State) -> Unit>,
    val afterListeners: List<(Context, State, State) -> Unit>
) {
    fun find(from: State?, to: State?) = transitions.find { it.from == from && it.to == to }

    fun extend(def: StateMachineDefinitionBuilder<Context, State>.() -> Unit): StateMachineDefinition<Context, State> {
        val builder = StateMachineDefinitionBuilder.from(this)
        builder.def()

        return builder.build()
    }
}

class StateMachineDefinitionBuilder<Context, State : Enum<State>> {
    constructor()

    private constructor(initial: State, transitions: List<StateTransition<Context, State>>) {
        this.initial = initial
        this.transitions.addAll(transitions.map { StateTransitionBuilder(it.from, it.to, it.beforeListeners.toMutableList(), it.guards.toMutableList(), it.afterListeners.toMutableList(), it.allowDefault) })
    }

    private var initial: State? = null
    private val transitions: MutableList<StateTransitionBuilder<Context, State>> = mutableListOf()
    private val beforeListeners: MutableList<(Context, State, State) -> Unit> = mutableListOf()
    private val afterListeners: MutableList<(Context, State, State) -> Unit> = mutableListOf()

    fun initial(state: State) = apply { this.initial = state }

    fun transition(from: State, to: State) {
        if (find(from, to) != null) {
            throw IllegalStateException("Cannot add a transition because it already exists")
        }

        transitions.add(StateTransitionBuilder(from, to))
    }

    fun fromAny(to: State) {
        if (find(null, to) != null) {
            throw IllegalStateException("Cannot add a transition because it already exists")
        }

        transitions.add(StateTransitionBuilder(null, to, mutableListOf(), mutableListOf(), mutableListOf(), false))
    }

    fun toAny(from: State) {
        if (find(from, null) != null) {
            throw IllegalStateException("Cannot add a transition because it already exists")
        }

        transitions.add(StateTransitionBuilder(from, null, mutableListOf(), mutableListOf(), mutableListOf(), false))
    }

    fun before(listener: (Context, State, State) -> Unit) {
        beforeListeners.add(listener)
    }

    fun after(listener: (Context, State, State) -> Unit) {
        afterListeners.add(listener)
    }

    fun before(from: State, to: State, listener: (Context, State, State) -> Unit) {
        val tx = find(from, to) ?: throw NoTransitionException(from.name, to.name)
        tx.before(listener)
    }

    fun after(from: State, to: State, listener: (Context, State, State) -> Unit) {
        val tx = find(from, to) ?: throw NoTransitionException(from.name, to.name)
        tx.after(listener)
    }

    fun afterFromAny(to: State, listener: (Context, State, State) -> Unit) {
        val tx = find(null, to) ?: throw NoTransitionException("any", to.name)
        tx.after(listener)
    }

    fun guard(from: State, to: State, guard: (Context) -> GuardResult) {
        val tx = find(from, to) ?: throw NoTransitionException(from.name, to.name)
        tx.guard(guard)
    }

    fun guardFromAny(to: State, guard: (Context) -> GuardResult) {
        val tx = find(null, to) ?: throw NoTransitionException("any", to.name)
        tx.guard(guard)
    }

    private fun find(from: State?, to: State?) = transitions.find { it.from == from && it.to == to }

    fun build() = StateMachineDefinition(
        initial ?: throw NoInitialStateException(),
        transitions.map { it.build() },
        beforeListeners,
        afterListeners
    )

    companion object {
        fun <Context, State : Enum<State>> from(definition: StateMachineDefinition<Context, State>) = StateMachineDefinitionBuilder(definition.initial, definition.transitions)
    }
}
