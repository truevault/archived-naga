package polaris.specs

import org.springframework.data.jpa.domain.Specification
import polaris.models.entity.GdprCookieConsentTracking
import polaris.models.entity.organization.Organization

class GdprCookieConsentTrackingSpecs {
    companion object {
        fun forOrganization(org: Organization): Specification<GdprCookieConsentTracking> {
            return Specification { root, _, builder ->
                builder.equal(root.get<Organization>("organization"), org)
            }
        }
    }
}
