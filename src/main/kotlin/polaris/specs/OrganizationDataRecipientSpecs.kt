package polaris.specs

import org.springframework.data.jpa.domain.Specification
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import java.time.ZonedDateTime

class OrganizationDataRecipientSpecs {
    companion object {
        fun forGdprProcessorSetting(setting: GDPRProcessorRecommendation?): Specification<OrganizationDataRecipient> {
            return Specification { root, _, builder ->
                builder.equal(root.get<GDPRProcessorRecommendation>("gdprProcessorSetting"), setting)
            }
        }

        fun hasGdprProcessorSettingSetAt(): Specification<OrganizationDataRecipient> {
            return Specification { root, _, builder ->
                builder.isNotNull(root.get<ZonedDateTime>("gdprProcessorSettingSetAt"))
            }
        }

        fun gdprProcessorSettingSetAtBefore(date: ZonedDateTime): Specification<OrganizationDataRecipient> {
            return Specification { root, _, builder ->
                builder.lessThanOrEqualTo(root.get("gdprProcessorSettingSetAt"), date)
            }
        }
    }
}
