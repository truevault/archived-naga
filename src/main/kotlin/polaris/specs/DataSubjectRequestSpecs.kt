package polaris.specs

import org.springframework.data.jpa.domain.Specification
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.Organization
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubmissionSourceEnum
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import java.time.ZonedDateTime
import java.util.UUID

class DataSubjectRequestSpecs {
    companion object {
        fun withId(id: UUID): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.equal(root.get<UUID?>("id"), id)
            }
        }

        fun notId(id: UUID): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.notEqual(root.get<UUID?>("id"), id)
            }
        }

        fun open(): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.isNull(root.get<ZonedDateTime?>("closedAt"))
            }
        }

        fun searchSubjectEmail(email: String): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                if (email.isNotBlank()) {
                    builder.like(builder.lower(root.get("subjectEmailAddress")), "%${email.trim().toLowerCase()}%")
                } else {
                    null
                }
            }
        }

        fun withEmailAddress(emailAddress: String): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.equal(builder.lower(root.get("subjectEmailAddress")), emailAddress.trim().toLowerCase())
            }
        }

        fun searchRequestState(state: String): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                if (state.isNotBlank()) {
                    builder.like(builder.lower(root.get("state")), "%${state.toLowerCase()}%")
                } else {
                    null
                }
            }
        }

        fun withDataRequestType(type: DataRequestType): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.equal(root.get<DataRequestType?>("requestType"), type)
            }
        }

        fun withDataRequestContext(context: CollectionContext): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.equal(root.get<DataRequestType?>("context"), context)
            }
        }

        fun forDataRequestTypeIn(types: List<DataRequestType>): Specification<DataSubjectRequest> {
            return Specification { root, _, _ ->
                root.get<DataRequestType?>("requestType").`in`(types)
            }
        }

        fun forDataRequestStateIn(states: List<DataRequestState>?): Specification<DataSubjectRequest> {
            return Specification { root, _, _ ->
                if (states != null) {
                    root.get<DataRequestState?>("state").`in`(states)
                } else {
                    null
                }
            }
        }

        fun forOrganization(org: Organization): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.equal(root.get<Organization>("organization"), org)
            }
        }

        fun inStates(states: List<DataRequestState>): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.and(root.get<DataRequestState>("state").`in`(states))
            }
        }

        fun isDueInRange(startDate: ZonedDateTime, endDate: ZonedDateTime): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.between(root.get("requestDueDate"), startDate, endDate)
            }
        }

        fun createdInRange(startDate: ZonedDateTime?, endDate: ZonedDateTime?): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                if (startDate != null && endDate != null) {
                    builder.between(root.get("createdAt"), startDate, endDate)
                } else if (startDate != null && endDate == null) {
                    builder.greaterThanOrEqualTo(root.get("createdAt"), startDate)
                } else if (startDate == null && endDate != null) {
                    builder.lessThanOrEqualTo(root.get("createdAt"), endDate)
                } else {
                    null
                }
            }
        }

        fun requestDateInRange(startDate: ZonedDateTime?, endDate: ZonedDateTime?): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                if (startDate != null && endDate != null) {
                    builder.between(root.get("requestDate"), startDate, endDate)
                } else if (startDate != null && endDate == null) {
                    builder.greaterThanOrEqualTo(root.get("requestDate"), startDate)
                } else if (startDate == null && endDate != null) {
                    builder.lessThanOrEqualTo(root.get("requestDate"), endDate)
                } else {
                    null
                }
            }
        }

        fun withSubmissionSource(submissionSource: DataRequestSubmissionSourceEnum): Specification<DataSubjectRequest> {
            return Specification { root, _, builder ->
                builder.equal(root.get<DataRequestSubmissionSourceEnum>("submissionSource"), submissionSource)
            }
        }
    }
}
