package polaris.specs

import org.springframework.data.jpa.domain.Specification
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.PrivacyEmail
import java.time.ZonedDateTime

class PrivacyEmailSpecs {
    companion object {
        fun forOrganization(org: Organization): Specification<PrivacyEmail> {
            return Specification { root, _, builder ->
                builder.equal(root.get<Organization>("organization"), org)
            }
        }

        fun receivedInRange(startDate: ZonedDateTime, endDate: ZonedDateTime): Specification<PrivacyEmail> {
            return Specification { root, _, builder ->
                builder.between(root.get("receivedAt"), startDate, endDate)
            }
        }
    }
}
