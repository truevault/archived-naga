package polaris.filters

import org.slf4j.LoggerFactory
import java.io.IOException
import java.time.Duration
import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletRequestWrapper
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession

class SessionInvalidationFilter(val sessionTimeout: Duration) : Filter {
    @Throws(ServletException::class)
    override fun init(arg: FilterConfig?) {}

    override fun destroy() {}

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) { // Cast to HTTP request and response
        val sessionTimeoutMs = sessionTimeout.toMillis()
        val httpRequest = request as HttpServletRequest
        val httpResponse = response as HttpServletResponse

        val session = httpRequest.getSession(false)
        if (session == null) {
            chain.doFilter(request, response)
            return
        }

        // Check if we are handling standard request
        if (IGNORE_ACCESS_URI != httpRequest.requestURI) {
            chain.doFilter(SessionAccessAwareRequest(httpRequest), response)
            return
        }

        // Now we can handle the special case of non-tracked request
        val lastAccessTime = session.getAttribute(LAST_ACCESS_SESSION_ATTR) as Long?
        val expired = if (lastAccessTime == null || lastAccessTime + sessionTimeoutMs < System.currentTimeMillis()) {
            session.invalidate() // Invalidate manually
            true
        } else {
            false
        }

        // Handle error or process normally
        if (expired) {
            httpResponse.status = 401
        } else {
            chain.doFilter(request, response)
        }
    }

    private class SessionAccessAwareRequest(request: HttpServletRequest?) : HttpServletRequestWrapper(request) {
        override fun getSession(): HttpSession {
            return getSession(true)
        }

        override fun getSession(create: Boolean): HttpSession {
            val session = super.getSession(create)
            session?.setAttribute(LAST_ACCESS_SESSION_ATTR, System.currentTimeMillis())
            return session
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(SessionInvalidationFilter::class.java)
        const val LAST_ACCESS_SESSION_ATTR = "lastAccessTime"
        private const val IGNORE_ACCESS_URI = "/api/users/self/session"
    }
}
