package polaris.models

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

interface ToJson {
    fun toJson(): String = jacksonObjectMapper().writeValueAsString(this)
}
