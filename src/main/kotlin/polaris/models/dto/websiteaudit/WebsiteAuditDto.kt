package polaris.models.dto.websiteaudit

import polaris.models.OrganizationPublicId
import polaris.models.entity.organization.WebsiteAuditProgress
import java.util.UUID

data class WebsiteAuditDto(
    val id: UUID,
    val organizationId: OrganizationPublicId,
    val status: WebsiteAuditProgress?,
    val scanUrl: String? = null,
    val error: String? = null,

    val privacyPolicyLinkTextValid: Boolean? = null,
    val privacyPolicyLinkUrlValid: Boolean? = null,

    val caPrivacyNoticeLinkTextValid: Boolean? = null,
    val caPrivacyNoticeLinkUrlValid: Boolean? = null,

    val optOutLinkTextValid: Boolean? = null,
    val optOutLinkUrlValid: Boolean? = null,
    val optOutLinkYourPrivacyChoicesIcon: Boolean? = null,

    val polarisJsSetup: Boolean? = null,
    val polarisJsBeforeGoogleAnalytics: Boolean? = null,
    val polarisJsNoDefer: Boolean? = null
)
