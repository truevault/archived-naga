package polaris.models.dto.websiteaudit

import polaris.models.ToJson
import java.util.UUID

data class SQSMessageStartWebsiteAudit(
    val organizationId: String,
    val scanId: UUID,
    val websiteUrl: String,
    val privacyCenterUrl: String,
    val auditPrivacyLink: Boolean,
    val auditCaPrivacyNoticeLink: Boolean,
    val auditOptOutLink: Boolean,
    val auditPolarisJs: Boolean
) : ToJson {
    val type = "build"
}
