package polaris.models.dto.websiteaudit

data class StartWebsiteAuditRequest(
    val websiteUrl: String
)
