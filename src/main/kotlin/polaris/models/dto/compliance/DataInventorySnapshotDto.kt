package polaris.models.dto.compliance

import polaris.models.UUIDString

data class OrgPersonalInformationSnapshotDto(
    val sources: List<PersonalInformationSourceDto>,
    val dataInventory: List<DataInventorySnapshotDto>
)

data class DataInventorySnapshotDto(
    val dataSubjectTypeId: UUIDString,
    val dataSubjectTypeName: String,
    val dataSubjectTypeDescriptor: String,
    val personalInformationCategories: List<SnapshotCategoryDto>,
    val businessPurposes: List<BusinessPurposeDto>,
    val consumerGroupTypeName: String?,
    val consumerGroupTypeDisplayOrder: Int?,
    val isConsumerType: Boolean
)

data class SnapshotCategoryDto(
    val id: UUIDString,
    val groupName: String,
    val groupOrder: Int,
    val groupDisplayOrder: Int,
    val name: String,
    val alwaysPreserveCasing: Boolean = false,
    val description: String?,
    val collected: Boolean? = false,
    val exchanged: Boolean?,
    val vendorsExchangedWith: List<UUIDString>,
    val isShared: Boolean? = false,
    val vendorsSharedWith: List<UUIDString>,
    val isSold: Boolean? = false,
    val vendorsSoldTo: List<UUIDString>
)
