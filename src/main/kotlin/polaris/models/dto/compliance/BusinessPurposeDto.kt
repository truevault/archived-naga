package polaris.models.dto.compliance

import polaris.models.UUIDString

data class BusinessPurposeDto(
    val id: UUIDString,
    val name: String,
    val description: String?,
    val enabled: Boolean? = false,
    val displayOrder: Int? = 0
)
