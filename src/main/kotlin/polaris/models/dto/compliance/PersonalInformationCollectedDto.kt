package polaris.models.dto.compliance

import java.util.*

data class PersonalInformationCollectedDto(
    val personalInformationCategories: List<PersonalInformationCategoryDto>,
    val businessPurposes: List<BusinessPurposeDto>,
    val exchanged: List<PersonalInformationExchangedDto>
)

data class UpdatePersonalInformationCollectedDto(
    val personalInformationCategories: List<UUID>,
    val customPersonalInformationSources: List<String>,
    val businessPurposes: List<UUID>
)
