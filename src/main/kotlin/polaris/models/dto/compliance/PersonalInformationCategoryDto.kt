package polaris.models.dto.compliance

import polaris.models.UUIDString
import polaris.models.entity.compliance.PICSurveyClassification

data class PersonalInformationCategoryDto(
    // pic info
    val id: UUIDString,
    val key: String?,
    val order: Int,

    val name: String,
    val description: String?,

    val isSensitive: Boolean = false,
    val alwaysPreserveCasing: Boolean = false,

    val surveyClassification: PICSurveyClassification,

    // group info
    val groupId: UUIDString,
    val groupName: String,
    val groupOrder: Int,
    val groupEmployeeOrder: Int,

    // org settings
    val collected: Boolean = false, // this indicates the org collects the value
    val exchanged: Boolean = false // this indicates that we send the value to at least one data recipient
)
