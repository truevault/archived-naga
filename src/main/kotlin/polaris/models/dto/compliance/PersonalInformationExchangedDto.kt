package polaris.models.dto.compliance

import polaris.models.UUIDString
import java.util.*

data class PersonalInformationExchangedDto(
    val dataSubjectTypeId: UUIDString,
    val vendorId: UUIDString,
    val personalInformationCategories: List<PersonalInformationCategoryDto>,
    val noDataMapping: Boolean?
)

data class UpdatePersonalInformationExchangedDto(
    val dataSubjectTypeId: UUID,
    val personalInformationCategories: List<UUID>,
    val noDataMapping: Boolean
)
