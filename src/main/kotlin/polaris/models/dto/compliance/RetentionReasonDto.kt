package polaris.models.dto.compliance

import polaris.models.UUIDString

data class RetentionReasonDto(
    val id: UUIDString,
    val reason: String,
    val slug: String,
    val description: String,
    val displayOrder: Int = 0
)
