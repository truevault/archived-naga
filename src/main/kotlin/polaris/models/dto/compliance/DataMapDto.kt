package polaris.models.dto.compliance

import polaris.controllers.InvalidDataMapOperation
import polaris.models.UUIDString
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.compliance.PICSurveyClassification
import polaris.models.entity.organization.CollectionContext

data class V2DataMapDto(
    val categories: Map<UUIDString, CategoryDto>,
    val collectionGroups: Map<UUIDString, DMCollectionGroupDto>,
    val recipients: Map<UUIDString, DMDataRecipientDto>,
    val sources: Map<UUIDString, DMDataSourceDto>,
    val collection: List<V2CollectionDto>,
    val disclosure: List<V2DisclosureDto>,
    val received: List<V2ReceivedDto>,
    val recipientAssociations: List<V2RecipientAssociationDto>,
    val sourceAssociations: List<V2SourceAssociationDto>
)

data class V2CollectionDto(val groupId: UUIDString, val picId: UUIDString)
data class V2DisclosureDto(val groupId: UUIDString, val vendorId: UUIDString, val picId: UUIDString)
data class V2ReceivedDto(val groupId: UUIDString, val vendorId: UUIDString, val picId: UUIDString)

data class V2RecipientAssociationDto(val groupId: UUIDString, val vendorId: UUIDString)
data class V2SourceAssociationDto(val groupId: UUIDString, val vendorId: UUIDString)

enum class DataMapTarget { COLLECTION, DISCLOSURE, RECEIVED, RECIPIENT_ASSOCIATION, SOURCE_ASSOCIATION }
enum class DataMapUpdate { ADD, REMOVE }
data class DataMapOperation(val target: DataMapTarget, val groupId: UUIDString, val picId: UUIDString?, val picGroupId: UUIDString?, val vendorId: UUIDString?, val update: DataMapUpdate) {
    fun validate() {
        when (target) {
            DataMapTarget.COLLECTION -> picId ?: throw InvalidDataMapOperation("The PIC ID must be present for a collection update")
            DataMapTarget.DISCLOSURE -> {
                picGroupId ?: throw InvalidDataMapOperation("The PIC Group ID must be present for a disclosure update")
                vendorId ?: throw InvalidDataMapOperation("The Vendor ID must be present for a disclosure update")
            }
            DataMapTarget.RECEIVED -> {
                picId ?: throw InvalidDataMapOperation("The PIC ID must be present for a received update")
                vendorId ?: throw InvalidDataMapOperation("The Vendor ID must be present for a received update")
            }
            DataMapTarget.RECIPIENT_ASSOCIATION -> vendorId ?: throw InvalidDataMapOperation("The Vendor ID must be present for a recipient association update")
            DataMapTarget.SOURCE_ASSOCIATION -> vendorId ?: throw InvalidDataMapOperation("The Vendor ID must be present for a source association update")
        }
    }
}

data class UpdateDataMapRequest(val operations: List<DataMapOperation>)

data class CategoryDto(
    val id: UUIDString,
    val key: String?,
    val order: Int,

    val name: String,
    val description: String?,

    val isSensitive: Boolean = false,
    val alwaysPreserveCasing: Boolean = false,

    val surveyClassification: PICSurveyClassification,

    // group info
    val picGroupId: UUIDString,
    val picGroupName: String,
    val picGroupOrder: Int,
    val picGroupEmployeeOrder: Int,
    val picGroupKey: String?
)

data class DisclosureDto(
    val collectionGroupId: UUIDString,
    val collectionGroupName: String,
    val collectionGroupDescription: String?,
    val collectionGroupType: CollectionGroupType,
    val vendorId: UUIDString,
    val vendorName: String,
    val vendorCategory: String,
    val disclosed: List<UUIDString>
)

data class SourceDto(
    val collectionGroupId: UUIDString,
    val collectionGroupName: String,
    val collectionGroupDescription: String?,
    val vendorId: UUIDString,
    val vendorName: String,
    val vendorCategory: String,
    val received: List<UUIDString>,
    val displayOrder: Int
)

data class CollectionDto(
    val collectionGroupId: UUIDString,
    val collectionGroupName: String,
    val collectionGroupDescription: String?,
    val collectionGroupType: CollectionGroupType,
    val collected: List<UUIDString>
)

data class DataMapDto(
    val categories: List<CategoryDto>,
    val collection: List<CollectionDto>,
    val disclosure: List<DisclosureDto>,
    val received: List<SourceDto>
)

data class PICGroupDto(
    val id: UUIDString,
    val name: String,
    val order: Int,
    val employeeOrder: Int,
    val key: String?
)

data class DMCollectionGroupDto(
    val id: UUIDString,
    val name: String,
    val description: String?,
    val type: CollectionGroupType
)

data class DMDataRecipientDto(
    val vendorId: UUIDString,
    val name: String,
    val category: String
)

data class DMDataSourceDto(
    val vendorId: UUIDString,
    val name: String,
    val category: String,
    val context: List<CollectionContext>,
    val displayOrder: Int
)

data class FullDataMapDto(
    val categories: Map<UUIDString, CategoryDto>,
    val picGroups: Map<UUIDString, PICGroupDto>,
    val collectionGroups: Map<UUIDString, DMCollectionGroupDto>,
    val recipients: Map<UUIDString, DMDataRecipientDto>,
    val sources: Map<UUIDString, DMDataSourceDto>,
    val collection: List<CollectionDto>,
    val disclosure: List<DisclosureDto>,
    val received: List<SourceDto>
)
