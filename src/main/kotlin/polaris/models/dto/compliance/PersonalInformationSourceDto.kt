package polaris.models.dto.compliance

import polaris.models.UUIDString

data class PersonalInformationSourceDto(
    val id: UUIDString? = null,
    val name: String,
    val disclosureName: String? = null,
    val description: String? = null,
    val enabled: Boolean? = false,
    val displayOrder: Int? = 0,
    val isCustom: Boolean? = false,
    val deprecated: Boolean = false,
    val category: String? = null
) {
    companion object {
        const val YOU_CATEGORY: String = "You (the consumer)"
        const val YOU_NAME: String = "Consumer"
    }
}
