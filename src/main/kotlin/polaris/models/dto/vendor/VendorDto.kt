package polaris.models.dto.vendor

import polaris.models.OrganizationPublicId
import polaris.models.ToJson
import polaris.models.UUIDString
import polaris.models.entity.vendor.DataRecipientType
import polaris.models.entity.vendor.ServiceProviderRecommendation

data class VendorDto(
    val id: UUIDString,
    val name: String,
    val vendorKey: String?,
    val aliases: List<String>?,
    val category: String?,
    val sourceCategory: String?,
    val url: String?,
    val email: String?,
    val ccpaRequestToDeleteLink: String? = null,
    val deletionInstructions: String?,
    val optOutInstructions: String?,
    val generalGuidance: String?,
    val specialCircumstancesGuidance: String?,
    val ccpaRequestToKnowLink: String? = null,
    val serviceProviderRecommendation: ServiceProviderRecommendation? = null,
    val serviceProviderLanguageUrl: String? = null,
    val dpaApplicable: Boolean = false,
    val dpaUrl: String? = null,
    val dataRecipientType: DataRecipientType,
    val logoUrl: String,
    val defaultVendorClassification: VendorClassificationDto?,
    val organizationId: OrganizationPublicId? = null,
    val recommendedPersonalInformationCategories: List<String> = mutableListOf(),
    val isCustom: Boolean,
    val isDataRecipient: Boolean,
    val isDataSource: Boolean,
    val isStandalone: Boolean?,
    val isPlatform: Boolean?,
    var respectsPlatformRequests: Boolean = false,
    val defaultRetentionReasons: List<String>,
    val isPotentialIntentionalInteractionVendor: Boolean = false,
    val isAutomaticIntentionalInteractionVendor: Boolean = false
) : ToJson

data class VendorClassificationDto(
    val id: UUIDString,
    val name: String,
    val slug: String,
    val displayOrder: Int,
    val tooltip: String?
)

data class VendorContractDto(
    val id: UUIDString,
    val name: String,
    val slug: String,
    val displayOrder: Int,
    val tooltip: String?,
    val contractUrl: String?
)

data class VendorContractReviewedDto(
    val id: UUIDString,
    val name: String,
    val slug: String,
    val displayOrder: Int,
    val tooltip: String?
)
