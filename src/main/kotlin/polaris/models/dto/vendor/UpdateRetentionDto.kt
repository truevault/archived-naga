package polaris.models.dto.vendor

import java.util.*

data class UpdateRetentionDto(
    val isAccessible: Boolean? = true,
    val isDelete: Boolean? = false,
    val retentionReasons: List<UUID>
)
