package polaris.models.dto.vendor

interface LabeledEnumDtoIface<T> {
    val value: T
    val label: String
}

data class LabeledEnumDto<T>(
    override val value: T,
    override val label: String
) : LabeledEnumDtoIface<T>
