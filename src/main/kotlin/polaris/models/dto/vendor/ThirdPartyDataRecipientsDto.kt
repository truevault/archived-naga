package polaris.models.dto.vendor

import polaris.models.UUIDString

data class ThirdPartyDataRecipientsDto(
    val recipients: List<ThirdPartyDataRecipientDto>
)

data class ThirdPartyDataRecipientDto(
    val id: UUIDString? = null,
    val name: String,
    val category: String
)
