package polaris.models.dto.vendor

import polaris.models.UUIDString
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.DataAccessibilityEnum
import polaris.models.entity.organization.DataDeletabilityEnum
import polaris.models.entity.organization.GDPRContactProcessorStatus
import polaris.models.entity.organization.GDPRContactUnsafeTransfer
import polaris.models.entity.organization.GDPRSccInPlace
import polaris.models.entity.vendor.DataRecipientType
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.models.entity.vendor.ProcessingRegion

// Mirrors OrganizationVendorSettingsDto in src/js/common/service/server/dto/OrganizationDataRecipientDto.ts
data class AddOrUpdateCustomVendorDto(
    val name: String? = null,
    val category: String? = null,
    val dataRecipientType: DataRecipientType? = null,
    val url: String? = null,
    val email: String? = null,
    val contacted: Boolean? = null,

    val classificationId: UUIDString? = null,
    val automaticallyClassified: Boolean? = null,

    val vendorContractId: UUIDString? = null,
    val vendorContractReviewedId: UUIDString? = null,
    val publicTosUrl: String? = null,
    val ccpaIsSelling: Boolean? = null,
    val ccpaIsSharing: Boolean? = null,
    val usesCustomAudience: Boolean? = null,
    val isUploadVendor: Boolean? = null,
    val ccpaIsCookieSharing: Boolean? = null,

    val isDataStorage: Boolean? = null,
    val tosFileName: String? = null,
    val tosFileKey: String? = null,
    val processingRegions: List<ProcessingRegion>? = null,
    val collectionContext: List<CollectionContext>? = null,
    val dataAccessibility: DataAccessibilityEnum? = null,
    val dataDeletability: DataDeletabilityEnum? = null,

    var gdprProcessorSetting: GDPRProcessorRecommendation?,
    var gdprProcessorGuaranteeUrl: String?,
    var gdprProcessorGuaranteeFileName: String?,
    var gdprProcessorGuaranteeFileKey: String?,
    var gdprControllerGuaranteeUrl: String?,
    var gdprControllerGuaranteeFileName: String?,
    var gdprControllerGuaranteeFileKey: String?,
    val gdprContactUnsafeTransfer: GDPRContactUnsafeTransfer? = null,
    val gdprEnsureSccInPlace: GDPRSccInPlace? = null,
    val gdprContactProcessorStatus: GDPRContactProcessorStatus? = null,

    var gdprHasSccSetting: Boolean,
    var gdprSccUrl: String?,
    var gdprSccFileName: String?,
    var gdprSccFileKey: String?,

    var removedFromExceptionsToScc: Boolean?,
    var exceptedFromSccs: Boolean? = false,

    var completed: Boolean? = null
)
