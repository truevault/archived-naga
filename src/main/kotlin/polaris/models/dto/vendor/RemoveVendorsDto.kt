package polaris.models.dto.vendor

import polaris.models.UUIDString

data class RemoveVendorsDto(
    val vendors: List<UUIDString>
)
