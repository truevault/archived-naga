package polaris.models.dto

import polaris.models.entity.organization.GetCompliantProgress
import polaris.models.entity.organization.OrgComplianceVersion

enum class SurveySlug(val defaultProgress: String) {
    onboarding(GetCompliantProgress.STARTING_PROGRESS),
    s2023_1("BusinessSurvey"),
    s2023_3("SellingSharing");

    // This is implemented as a helper function instead of a value on SurveySlug, because
    // having circular references between SurveySlug and OrgComplianceVersion seems to be
    // very annoying to Kotlin, which just silently ignores some of the values.
    fun nextComplianceVersion(): OrgComplianceVersion {
        return when (this) {
            onboarding -> OrgComplianceVersion.c2023_1
            s2023_1 -> OrgComplianceVersion.c2023_1
            s2023_3 -> OrgComplianceVersion.c2023_3
        }
    }
}
