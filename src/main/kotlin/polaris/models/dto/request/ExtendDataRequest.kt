package polaris.models.dto.request

data class ExtendDataRequest(
    val notice: String
)
