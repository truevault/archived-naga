package polaris.models.dto.request

import java.util.*

data class ConfirmIdentityDataRequest(
    val confirmationToken: UUID,
    val organizationCustomInput: String? = null,
    val declarationConfirmation: Boolean? = null
)
