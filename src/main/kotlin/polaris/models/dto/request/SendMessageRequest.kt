package polaris.models.dto.request

import org.springframework.web.multipart.MultipartFile

data class SendMessageRequest(
    val message: String,
    val attachments: List<MultipartFile>?
)
