package polaris.models.dto.request

import polaris.models.entity.organization.Organization
import polaris.models.entity.request.DataRequestType
import java.time.ZonedDateTime

data class PhoneDataRequestDto(
    val requestDate: ZonedDateTime,
    val requestType: String,
    val org: Organization,
    val consumerPhoneNumber: String,
    val consumerNameRecording: String,
    val consumerEmailRecording: String
) {
    fun getRequestType(): DataRequestType? {
        return try {
            DataRequestType.valueOf(requestType)
        } catch (e: IllegalArgumentException) {
            null
        }
    }
}
