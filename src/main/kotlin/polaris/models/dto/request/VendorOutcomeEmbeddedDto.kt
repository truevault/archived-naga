package polaris.models.dto.request

import polaris.models.ToJson
import polaris.models.dto.vendor.VendorDto

data class VendorOutcomeEmbeddedDto(
    val vendor: VendorDto?,
    val processingInstructions: String?,
    val processed: Boolean,
    val outcome: VendorOutcomeValue?
) : ToJson
