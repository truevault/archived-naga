package polaris.models.dto.request

import org.springframework.web.multipart.MultipartFile
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubstate

data class TransitionDataRequest(
    val state: DataRequestState,
    val substate: DataRequestSubstate? = null,
    val notes: String? = null,
    val attachment: MultipartFile? = null
)
