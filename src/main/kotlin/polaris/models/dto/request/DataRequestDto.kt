package polaris.models.dto.request

import polaris.models.DataRequestPublicId
import polaris.models.Iso8601Date
import polaris.models.OrganizationPublicId
import polaris.models.ToJson
import polaris.models.dto.PaginatedDto
import polaris.models.dto.organization.CollectionGroupDto
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubmissionMethodEnum
import polaris.models.entity.request.DataRequestSubmissionSourceEnum
import polaris.models.entity.request.DataRequestSubstate

typealias PaginatedSubjectRequestDto = PaginatedDto<DataSubjectRequestSummaryDto>

data class DataSubjectRequestSummaryDto(
    val id: DataRequestPublicId,
    val dataRequestType: DataRequestTypeDto,

    val context: CollectionContext,

    val state: DataRequestState,
    val substate: DataRequestSubstate?,

    val consumerGroups: List<CollectionGroupDto>,

    val subjectFirstName: String?,
    val subjectLastName: String?,
    val subjectEmailAddress: String?,
    val organizationCustomInput: String?,

    val requestDate: Iso8601Date,
    val dueDate: Iso8601Date?,
    val originalDueDate: Iso8601Date?,
    val closedAt: Iso8601Date?
) : ToJson

data class DataSubjectRequestDetailDto(
    val id: DataRequestPublicId,
    val organizationId: OrganizationPublicId,
    val dataRequestType: DataRequestTypeDto,

    val state: DataRequestState,
    val stateHistory: List<DataRequestState>,
    val substate: DataRequestSubstate?,

    val consumerGroups: List<CollectionGroupDto>,

    val subjectFirstName: String?,
    val subjectLastName: String?,
    val subjectEmailAddress: String?,
    val subjectPhoneNumber: String?,
    val subjectMailingAddress: String?,
    val subjectIpAddress: String?,
    val organizationCustomInput: String?,

    val submissionMethod: DataRequestSubmissionMethodEnum?,
    val submissionSource: DataRequestSubmissionSourceEnum,

    val requestDate: Iso8601Date,
    val emailVerifiedDate: Iso8601Date?,
    val dueDate: Iso8601Date?,
    val originalDueDate: Iso8601Date?,
    val closedAt: Iso8601Date?,

    val events: List<DataSubjectRequestEventEmbeddedDto>,
    val vendorOutcomes: List<VendorOutcomeEmbeddedDto>,

    val context: CollectionContext,

    val selfDeclarationProvided: Boolean,

    val consumerGroupProcessingInstructions: String?,
    val optOutProcessingInstructions: String?,

    val consumerGroupResult: SubjectStatus?,

    val helpArticleId: String?,

    val vendorContacted: Boolean,
    var contactVendorsEverHidden: Boolean,
    var contactVendorsEverShown: Boolean,
    var contactVendorsHidden: Boolean,

    var hasGaUserId: Boolean,

    val emailVerified: Boolean = false,
    val emailVerifiedAutomatically: Boolean = false,
    val emailVerifiedManually: Boolean = false,

    val correctionRequest: String? = null,
    val correctionFinished: Boolean? = null,
    val limitationFinished: Boolean? = null,
    val objectionRequest: String? = null,
    var consentWithdrawnRequest: MutableList<String>? = null,
    var consentWithdrawnFinished: Boolean? = null,

    val requestorContacted: Boolean,
    val isAutocreated: Boolean,
    val isGpcOptOut: Boolean,
    val isDuplicate: Boolean = false,
    val duplicatesRequestId: String? = null
) : ToJson
