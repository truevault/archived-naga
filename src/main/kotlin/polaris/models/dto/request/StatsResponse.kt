package polaris.models.dto.request

data class StatsResponse(
    val active: ActiveStats,
    val inactive: InactiveStats
)

data class ActiveStats(
    val count: Int,
    val dueThisWeek: Int,
    val overdue: Int
)

data class InactiveStats(
    val count: Int,
    val avgProcessingDays: Float,
    val pctOnTime: Float
)
