package polaris.models.dto.request

import java.util.*

enum class SubjectStatus {
    FOUND,
    PROCESSED_AS_SERVICE_PROVIDER,
    NOT_FOUND,
}

data class RequestSubjectStatusUpdate(
    val subjectStatus: SubjectStatus
)
