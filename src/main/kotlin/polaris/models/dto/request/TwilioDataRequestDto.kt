package polaris.models.dto.request

data class TwilioDataRequestDto(
    val phoneNumberCalled: String,
    val requestType: String,
    val consumerPhoneNumber: String,
    val consumerNameRecording: String,
    val consumerEmailRecording: String
)
