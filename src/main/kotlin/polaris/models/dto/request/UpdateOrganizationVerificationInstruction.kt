package polaris.models.dto.request

data class UpdateOrganizationVerificationInstruction(
    val regulationSlug: String,
    val instruction: String
)
