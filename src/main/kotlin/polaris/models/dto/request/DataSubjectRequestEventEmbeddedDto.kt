package polaris.models.dto.request

import polaris.models.Iso8601Date
import polaris.models.ToJson
import polaris.models.dto.attachment.AttachmentDto
import polaris.models.dto.user.UserDto
import polaris.models.entity.request.DataRequestEventType

data class DataSubjectRequestEventEmbeddedDto(
    val eventId: Int,
    val actor: UserDto?,
    val eventType: DataRequestEventType,
    val eventTypeTitle: String,
    val eventTypeDescription: String,
    val isUserFacingEvent: Boolean,
    val isRequesterAction: Boolean,
    val message: String?,
    val attachments: List<AttachmentDto>,
    val createdAt: Iso8601Date,
    val eventDate: Iso8601Date
) : ToJson
