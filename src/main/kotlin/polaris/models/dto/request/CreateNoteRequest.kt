package polaris.models.dto.request

import org.springframework.web.multipart.MultipartFile
import java.io.Serializable

data class CreateNoteRequest(
    val message: String,
    val attachments: List<MultipartFile>?
) : Serializable
