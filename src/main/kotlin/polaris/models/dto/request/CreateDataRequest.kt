package polaris.models.dto.request

import polaris.models.Iso8601Date
import polaris.models.UUIDString
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.request.DataRequestSubmissionSourceEnum
import polaris.models.entity.request.DataRequestType

data class CreateDataRequest(
    val dataRequestType: DataRequestType,
    val requestDate: Iso8601Date,
    val isDataSubject: Boolean = false,

    val gpcOptOut: Boolean = false,
    val context: CollectionContext = CollectionContext.CONSUMER,
    val subjectFirstName: String? = null,
    val subjectLastName: String? = null,
    val dataSubjectTypeId: UUIDString? = null,
    val subjectEmailAddress: String? = null,
    val subjectPhoneNumber: String? = null,
    val organizationCustomInput: String? = null,
    val submissionMethod: String? = null,
    val subjectMailingAddress: String? = null,
    val subjectIpAddress: String? = null,
    val notes: String? = null,
    val selfDeclarationProvided: Boolean = false,
    val correctionRequest: String? = null,
    var objectionRequest: String? = null,
    var appealId: String? = null,
    var consentWithdrawnRequest: List<String>? = null,
    val submissionSource: DataRequestSubmissionSourceEnum = DataRequestSubmissionSourceEnum.POLARIS,
    val gaUserId: String? = null,
    val skipEmailVerification: Boolean? = null
)
