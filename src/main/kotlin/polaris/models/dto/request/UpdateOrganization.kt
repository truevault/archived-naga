package polaris.models.dto.organization

import polaris.models.OrganizationPublicId
import polaris.models.UUIDString

data class UpdateOrganization(
    val name: String,
    val logoUrl: String?,
    val faviconUrl: String?,
    val regulations: List<UpdateOrganizationRegulationDto>,
    val isServiceProvider: Boolean?
)

data class UpdateOrganizationRegulationDto(
    val id: UUIDString,
    val enabled: Boolean
)

data class UpdateOrganizationCGInstructionsDto(
    val organizationId: OrganizationPublicId,
    val cgRequestHandlingInstructions: String
)

data class UpdateOrganizationCustomFieldDto(
    val customFieldEnabled: Boolean,
    val customFieldHelp: String? = null,
    val customFieldLabel: String? = null
)

data class UpdateOrganizationFinancialIncentivesRequest(
    val financialIncentiveDetails: String
)

data class UpdateOrganizationGoogleAnalyticsWebPropertyDto(
    val webProperty: String? = null
)
