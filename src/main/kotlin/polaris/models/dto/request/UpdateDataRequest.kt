package polaris.models.dto.request

import polaris.models.Iso8601Date
import polaris.models.entity.request.DataRequestType

data class UpdateDataRequest(
    val dueDate: Iso8601Date?,
    val requestDate: Iso8601Date?,
    val subjectEmailAddress: String?,
    val subjectMailingAddress: String?,
    val subjectPhoneNumber: String?,
    val subjectFirstName: String?,
    val subjectLastName: String?,
    val submissionMethod: String?,
    val dataRequestType: DataRequestType?,
    val contactVendorsHidden: Boolean?,
    val correctionFinished: Boolean?,
    val consentWithdrawnFinished: Boolean?,
    val limitationFinished: Boolean?
)
