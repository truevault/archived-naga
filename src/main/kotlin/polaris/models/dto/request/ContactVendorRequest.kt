package polaris.models.dto.request

import polaris.models.UUIDString

data class ContactVendorRequest(
    val vendors: List<UUIDString>
)
