package polaris.models.dto.request

import polaris.models.UUIDString

enum class VendorOutcomeValue {
    NO_DATA,
    DATA_DELETED,
    DATA_RETAINED,
    DATA_RETRIEVED,
    CONSUMER_REMOVED
}

data class UpdateVendorRequest(
    val vendor: UUIDString,
    val outcome: VendorOutcomeValue
)
