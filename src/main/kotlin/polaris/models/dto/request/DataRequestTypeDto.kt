package polaris.models.dto.request

import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.RequestWorkflow

data class DataRequestTypeDto(
    val type: DataRequestType,
    val shortName: String,
    val name: String,
    val longName: String,
    val description: String,
    val regulationName: String,
    val workflow: RequestWorkflow,
    val requireVerification: Boolean,
    val canExtend: Boolean,
    val declarationText: String?
)
