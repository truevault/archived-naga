package polaris.models.dto.request

import java.util.*

data class VerifyDataRequest(
    val notes: String,
    val consumerGroupId: UUID
)
