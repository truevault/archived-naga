package polaris.models.dto.organization

data class UpdateOrganizationMessageSettings(
    val headerImageUrl: String,
    val physicalAddress: String,
    val messageSignature: String = ""
)
