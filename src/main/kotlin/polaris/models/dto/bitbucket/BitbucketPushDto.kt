package polaris.models.dto.bitbucket

data class BitbucketPushWebhookDto(
    val push: BitbucketPushDto
)

data class BitbucketPushDto(
    val changes: List<BitbucketChangeDto>
)

data class BitbucketChangeDto(
    val new: BitbucketNewDto?
)

data class BitbucketNewDto(
    val type: String,
    val name: String
)
