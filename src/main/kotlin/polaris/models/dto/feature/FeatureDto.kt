package polaris.models.dto.feature

import polaris.models.Iso8601Date
import polaris.models.UUIDString

data class FeatureDto(
    val id: UUIDString? = null,
    val name: String? = null,
    val description: String? = null,
    val startTs: Iso8601Date? = null,
    val endTs: Iso8601Date? = null
)
