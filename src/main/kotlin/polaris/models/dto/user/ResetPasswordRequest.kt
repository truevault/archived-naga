package polaris.models.dto.user

data class ResetPasswordRequest(val password: String, val resetToken: String)
