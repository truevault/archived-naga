package polaris.models.dto.user

import polaris.models.ToJson

data class CreateUserRequest(val email: String, val firstName: String, val lastName: String) : ToJson
