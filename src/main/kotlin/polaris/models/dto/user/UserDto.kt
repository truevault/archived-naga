package polaris.models.dto.user

import polaris.models.Iso8601Date
import polaris.models.OrganizationPublicId
import polaris.models.ToJson
import polaris.models.UUIDString
import polaris.models.entity.organization.Phase
import polaris.models.entity.user.GlobalRole
import polaris.models.entity.user.UserRole
import polaris.models.entity.user.UserStatus

data class UserDto(
    val email: String,
    val lastLogin: Iso8601Date?,
    val firstName: String? = null,
    val lastName: String? = null,
    val globalRole: GlobalRole? = null
) : ToJson

data class UserDetailsDto(
    val id: UUIDString,
    val email: String,
    val lastLogin: Iso8601Date?,
    val firstName: String? = null,
    val lastName: String? = null,
    val organizations: List<OrganizationUserDetailsDto>,
    val invitedOrganizations: List<OrganizationUserDetailsDto>,
    val beaconSignature: String? = null,
    val productBoardJwt: String? = null,
    val globalRole: GlobalRole? = null,
    val adminSite: String? = null,
    val activeOrganization: OrganizationPublicId? = null,
    val activeOrganizationPhase: Phase? = null
) : ToJson

data class SessionDto(
    val secondsRemaining: Int
)

// this dto represents a user for a specific organization
data class OrganizationUserDto(
    val firstName: String?,
    val lastName: String?,
    val email: String,
    val lastLogin: Iso8601Date?,
    val role: UserRole,
    val status: UserStatus,
    val organization: OrganizationPublicId,
    val organizationName: String
) : ToJson

data class OrganizationUserDetailsDto(
    val role: UserRole,
    val status: UserStatus,
    val organization: OrganizationPublicId,
    val organizationName: String,
    val faviconUrl: String?
) : ToJson
