package polaris.models.dto.user

import polaris.models.OrganizationPublicId

data class UpdateActiveOrganization(val organizationId: OrganizationPublicId)
