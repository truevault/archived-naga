package polaris.models.dto

enum class LogoType(val label: String) {
    LOGO("logo"),
    FAVICON("favicon"),
    MESSAGE_HEADER("message-header")
}
