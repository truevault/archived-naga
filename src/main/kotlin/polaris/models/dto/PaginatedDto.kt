package polaris.models.dto

data class PaginatedMetaDto(
    val page: Int,
    val per: Int,
    val count: Int,
    val totalPages: Int,
    val totalCount: Int
)
data class PaginatedDto<T>(val data: List<T>, val meta: PaginatedMetaDto)
