package polaris.models.dto.cookiescan

import polaris.models.entity.organization.CookieCategory
import java.util.UUID

data class CookieRecommendationDto(
    val id: UUID,
    val name: String?,
    val domain: String?,
    val categories: List<CookieCategory> = emptyList(),
    val vendorId: UUID?,
    val notes: String?
)

data class CookieRecommendationAdminDto(
    val id: UUID?,
    val name: String?,
    val domain: String?,
    val categories: List<CookieCategory> = emptyList(),
    val vendorId: UUID?,
    val vendorName: String?,
    val notes: String?
)
