package polaris.models.dto.cookiescan

import polaris.models.UUIDString
import polaris.models.entity.organization.CookieCategory

data class UpdateCookieRequest(
    val categories: List<CookieCategory>,
    val dataRecipients: List<UUIDString>,
    val hidden: Boolean,
    val firstParty: Boolean
)
