package polaris.models.dto.cookiescan

import polaris.models.ToJson

data class SQSMessageStartCookieScan(
    val organizationId: String,
    val scanId: String,
    val url: String
) : ToJson {
    val type = "build"
}
