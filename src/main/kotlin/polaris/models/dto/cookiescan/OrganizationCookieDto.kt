package polaris.models.dto.cookiescan

import polaris.models.UUIDString
import polaris.models.entity.organization.CookieCategory

data class OrganizationCookieDto(
    val name: String?,
    val domain: String?,
    val value: String?,
    val expires: String?,
    val httpOnly: String?,
    val secure: String?,
    val lastFetched: String?,

    val categories: List<CookieCategory>,
    val dataRecipients: List<UUIDString>,
    val hidden: Boolean,
    val firstParty: Boolean
)
