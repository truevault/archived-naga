package polaris.models.dto.cookiescan

import polaris.models.entity.organization.CookieScanProgress

data class CookieScanDto(
    val organizationId: String,
    val status: CookieScanProgress,
    val scanUrl: String?,
    val error: String?,
    val cookies: List<OrganizationCookieDto>
)
