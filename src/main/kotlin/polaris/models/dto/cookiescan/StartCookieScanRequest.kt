package polaris.models.dto.cookiescan

data class StartCookieScanRequest(
    val url: String
)
