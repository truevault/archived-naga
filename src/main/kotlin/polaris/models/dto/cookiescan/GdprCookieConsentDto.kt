package polaris.models.dto.cookiescan

import polaris.models.Iso8601Date
import polaris.models.dto.PaginatedDto

data class GdprCookieConsentDto(
    val visitorId: String,
    val consentGranted: List<GdprCookieConsentCategory>,
    val date: Iso8601Date
)

typealias PaginatedGdprCookieConsentDto = PaginatedDto<GdprCookieConsentDto>

enum class GdprCookieConsentCategory {
    ANALYTICS,
    PERSONALIZATION,
    ADS,
    ESSENTIAL
}
