package polaris.models.dto.cookiescan

import polaris.models.OrganizationPublicId

data class CookieScanResultDto(
    val organizationId: OrganizationPublicId,
    val url: String,
    val cookies: List<CookiescanCookie>,
    val error: String? = null
)

data class CookiescanCookie(
    val name: String,
    val value: String,
    val domain: String,
    val path: String?,
    val expires: String?,
    val httpOnly: Boolean?,
    val secure: Boolean?,
    val session: Boolean?,
    val sameSite: String?,
    val priority: String?,
    val sameParty: Boolean?
)
