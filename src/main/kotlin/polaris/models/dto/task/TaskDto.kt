package polaris.models.dto.task

import polaris.models.Iso8601Date
import polaris.models.ToJson
import polaris.models.UUIDString
import polaris.models.dto.user.UserDto
import polaris.models.entity.organization.TaskCompletionType
import polaris.models.entity.organization.TaskKey

class TaskDto(
    val id: UUIDString,
    val taskKey: TaskKey,
    val name: String,
    val description: String,
    val createdAt: Iso8601Date,
    val dueAt: Iso8601Date?,
    val completedAt: Iso8601Date?,
    val completedBy: UserDto?,
    val completionType: TaskCompletionType? = TaskCompletionType.AUTOMATIC,
    val callToAction: String?,
    val callToActionUrl: String?
) : ToJson
