package polaris.models.dto

import polaris.models.Iso8601Date
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

fun ZonedDateTime.toIso(): Iso8601Date = DateTimeFormatter.ISO_INSTANT.format(this)

fun LocalDate.toIso(): Iso8601Date = DateTimeFormatter.ISO_DATE.format(this)
fun LocalDate.atEndOfDay(): ZonedDateTime = this.plusDays(1).atStartOfDay(ZoneId.systemDefault())
