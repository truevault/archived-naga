package polaris.models.dto

data class InboundEmail(
    val to: String?,
    val from: String?,
    val subject: String?,
    val body: String?,
    val message: String?,
    val unsanitizedBody: String?,
    val senderIp: String?
)

// log.info("to / from /subject: $to / $from / $subject")
// log.info("sender ip: $senderIp")
// log.info("attachments: $attachments")
// log.info("attachmentInfo: $attachmentInfo")
// log.info("\nbody: $html")
