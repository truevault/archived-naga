package polaris.models.dto.privacycenter

import polaris.models.Iso8601Date
import polaris.models.UUIDString

data class UpdatePrivacyCenterPolicy(
    val policyLastUpdated: Iso8601Date?,
    val policySectionsOrder: Map<UUIDString, Int>
)
