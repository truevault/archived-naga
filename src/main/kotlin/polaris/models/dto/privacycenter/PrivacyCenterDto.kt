package polaris.models.dto.privacycenter

import polaris.models.Iso8601Date
import polaris.models.OrganizationPublicId
import polaris.models.PrivacyCenterPublicId
import polaris.models.UUIDString
import polaris.models.entity.privacycenter.CustomUrlStatus

data class PrivacyCenterDetailDto(
    val id: PrivacyCenterPublicId,
    val organizationId: OrganizationPublicId,
    val name: String,

    val faviconUrl: String,
    val logoUrl: String?,
    val logoLinkUrl: String?,
    val defaultSubdomain: String,
    val customUrl: String?,
    val customUrlStatus: CustomUrlStatus?,
    val customUrlCnameReady: Boolean,
    val url: String,
    val hasMobileApplication: Boolean?,
    var dpoOfficerName: String? = null,
    var dpoOfficerEmail: String? = null,
    var dpoOfficerPhone: String? = null,
    var dpoOfficerAddress: String? = null,
    var dpoOfficerDoesNotApply: Boolean = false,

    val ccpaPrivacyNoticeIntroText: String,
    val gdprPrivacyNoticeIntroText: String,
    val vcdpaPrivacyNoticeIntroText: String,
    val cpaPrivacyNoticeIntroText: String,
    val ctdpaPrivacyNoticeIntroText: String,
    val optOutNoticeIntroText: String,
    val cookiePolicyIntroText: String,
    val retentionIntroText: String,

    val privacyPolicyUrl: String?,
    val requestTollFreePhoneNumber: String?,
    val requestFormUrl: String?,
    val requestEmail: String?,

    val doNotSellMarkdown: String?,
    val employeePrivacyNoticeMarkdown: String?,
    val jobApplicantPrivacyNoticeMarkdown: String?,

    val requiresDpo: Boolean,
    val hasDpoTask: Boolean,

    val disableCaResidencyConfirmation: Boolean
)

data class SellingAndSharingDto(
    val doNotSellHtml: String,
    val enableIbaBrowserOptOut: Boolean?,
    val enableIbaDirectOptOut: Boolean?,
    val selling: Boolean?,
    val sharing: Boolean?
)

data class PrivacyCenterBuilderDto(
    val id: UUIDString,
    val publicId: PrivacyCenterPublicId,
    val organizationId: OrganizationPublicId,
    val name: String,
    val isLegacy: Boolean = false,

    val faviconUrl: String,
    val logoUrl: String?,
    val logoLinkUrl: String?,
    val url: String,

    val organizationName: String,

    val privacyPolicyName: String,
    val sellingAndSharing: SellingAndSharingDto,
    val employeePrivacyNoticeHtml: String,
    val contractorPrivacyNoticeHtml: String,
    val jobApplicantPrivacyNoticeHtml: String,
    val eeaUkEmployeePrivacyNoticeHtml: String,
    val customField: CustomFieldDto?,

    val privacyPolicySections: List<PrivacyCenterPolicySectionDto>? = emptyList(),
    val privacyPolicyLastUpdated: Iso8601Date? = null,
    val includeRightToLimitForm: Boolean = false,
    val rightToLimitHtml: String,

    val hostedUrlBase: String,
    val requestTollFreePhoneNumber: String?,
    val requestFormUrl: String?,
    val requestEmail: String?,

    val dpoEmail: String?,
    val dpoPhone: String?,

    val featureMultistate: Boolean?,

    val bucket: String,

    val caResidencyConfirmation: Boolean,
    val hideCcpaSection: Boolean
)

data class CustomFieldDto(
    val label: String,
    val help: String?
)

enum class BuiltinPrivacySectionSlug(val displayOrder: Int) {
    collect(10000),
    disclose(20000),
    ccpa(30000),
    vcdpa(40000),
    cpa(50000),
    ctdpa(60000),
    financial(70000),
    gdpr(80000)
}

data class PrivacyCenterPolicySectionDto(
    val displayOrder: Int,
    val showHeader: Boolean,
    val custom: Boolean,
    val builtinSlug: BuiltinPrivacySectionSlug?,
    val id: UUIDString? = null,
    val name: String? = "",

    val anchor: String? = "",
    val addToMenu: Boolean = true,
    val noPrivacyPolicy: Boolean? = null,
    val body: String? = ""
)

data class PrivacyCenterPolicyDto(
    val privacyCenterId: PrivacyCenterPublicId,
    val policyLastUpdated: Iso8601Date? = null,
    val policySections: List<PrivacyCenterPolicySectionDto>
)
