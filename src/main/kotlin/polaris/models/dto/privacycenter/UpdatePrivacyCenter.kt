package polaris.models.dto.privacycenter

data class UpdatePrivacyCenter(
    val name: String,
    val defaultSubdomain: String,
    val faviconUrl: String = "",
    val logoUrl: String? = null,
    val logoLinkUrl: String? = null,
    val customUrl: String? = null,
    val customUrlCnameReady: Boolean? = null,
    val hasMobileApplication: Boolean? = null,
    val doNotSellMarkdown: String? = null,
    val employeePrivacyNoticeMarkdown: String? = null,
    val jobApplicantPrivacyNoticeMarkdown: String? = null,
    val customFieldEnabled: Boolean?,
    val customFieldLabel: String? = null,
    val customFieldHelp: String? = null,
    val disableCaResidencyConfirmation: Boolean? = null,

    val dpoOfficerName: String? = null,
    val dpoOfficerEmail: String? = null,
    var dpoOfficerPhone: String? = null,
    var dpoOfficerAddress: String? = null,
    val dpoOfficerDoesNotApply: Boolean = false,

    var ccpaPrivacyNoticeIntroText: String,
    var gdprPrivacyNoticeIntroText: String,
    var vcdpaPrivacyNoticeIntroText: String,
    var cpaPrivacyNoticeIntroText: String,
    var ctdpaPrivacyNoticeIntroText: String,
    var cookiePolicyIntroText: String,
    var optOutNoticeIntroText: String,
    var retentionIntroText: String
)
