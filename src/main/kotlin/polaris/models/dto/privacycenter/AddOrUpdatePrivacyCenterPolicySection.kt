package polaris.models.dto.privacycenter

data class AddOrUpdatePrivacyCenterPolicySection(
    val name: String,
    val anchor: String,
    val addToMenu: Boolean,
    val body: String,
    val noPrivacyPolicy: Boolean,
    val publish: Boolean = true,
    val showHeader: Boolean,
    val before: BuiltinPrivacySectionSlug?,
    val after: BuiltinPrivacySectionSlug?
)
