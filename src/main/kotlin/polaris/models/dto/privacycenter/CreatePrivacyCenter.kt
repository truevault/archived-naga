package polaris.models.dto.privacycenter

data class CreatePrivacyCenter(
    val name: String,
    val defaultSubdomain: String,
    val faviconUrl: String,
    val customUrl: String? = null,

    val privacyPolicyUrl: String? = null,
    val requestTollFreePhoneNumber: String? = null,
    val requestFormUrl: String? = null,
    val requestEmail: String? = null,
    val logoUrl: String? = null
)
