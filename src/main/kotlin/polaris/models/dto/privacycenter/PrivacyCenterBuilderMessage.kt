package polaris.models.dto.privacycenter

import polaris.models.ToJson

data class PrivacyCenterBuilderMessage(
    val organizationId: String,
    val internalOrganizationId: String,
    val privacyCenterId: String,
    val sitesBucket: String,
    val cloudfrontDistribution: String?
) : ToJson {
    val type = "build"
}
