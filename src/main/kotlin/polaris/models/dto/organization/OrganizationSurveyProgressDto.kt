package polaris.models.dto.organization

import polaris.models.dto.SurveySlug

data class OrganizationSurveyProgressDto(
    val survey: SurveySlug,
    val progress: String
)

data class UpdateOrganizationSurveyProgressDto(
    val progress: String
)
