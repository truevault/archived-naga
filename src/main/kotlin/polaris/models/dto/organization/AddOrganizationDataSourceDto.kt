package polaris.models.dto.organization

import polaris.models.entity.organization.CollectionContext
import java.util.*

data class AddOrganizationDataSourceDto(
    val vendorId: UUID?,
    val custom: AddCustomOrganizationDataSourceDto?,
    val collectionContext: List<CollectionContext>? = null
)

data class AddCustomOrganizationDataSourceDto(
    val name: String,
    val category: String,
    val contactEmail: String?,
    val url: String?
)
