package polaris.models.dto.organization

import polaris.models.Iso8601Date
import polaris.models.OrganizationPublicId
import polaris.models.ToJson
import polaris.models.UUIDString
import polaris.models.dto.SurveySlug
import polaris.models.dto.vendor.LabeledEnumDto
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.organization.OptOutPageType
import polaris.models.entity.organization.Phase
import polaris.models.entity.organization.PrivacyNoticeProgress

data class OrganizationDto(
    val id: OrganizationPublicId,
    val name: String,
    val createdAt: Iso8601Date,
    val logoUrl: String?,
    val faviconUrl: String?,
    val identityVerificationMessage: String,
    val mailbox: OrganizationMailboxStatusDto,
    val messageSettings: OrganizationMessageSettingsDto,
    val regulations: List<OrganizationRegulationDto>,
    val phase: Phase,
    val getCompliantProgress: String,
    val getCompliantInvalidatedProgress: List<String>,
    val exceptionsMappingProgress: LabeledEnumDto<MappingProgressEnum>,
    val cookiesMappingProgress: LabeledEnumDto<MappingProgressEnum>,
    val caPrivacyNoticeProgress: LabeledEnumDto<PrivacyNoticeProgress>,
    val optOutPrivacyNoticeProgress: LabeledEnumDto<PrivacyNoticeProgress>,
    val optOutPageType: OptOutPageType,
    val requiresNoticeOfRightToLimit: Boolean = false,
    val requiresAssociations: Boolean = false,
    val requiresEmployeeMapping: Boolean = false,
    val isServiceProvider: Boolean?,
    val activeRequestCount: Int?,
    val activeTaskCount: Int?,
    val trainingComplete: Boolean,
    val walkthroughEmailSent: Boolean,
    val customFieldEnabled: Boolean,
    val customFieldLabel: String?,
    val customFieldHelp: String?,
    val doesOrgProcessOptOuts: Boolean,

    val isSelling: Boolean? = false,
    val isSharing: Boolean? = false,

    var financialIncentiveDetails: String?,

    val cookiePolicy: String?,

    var gaServiceAccount: String? = null,
    var gaWebProperty: String? = null,
    val hasGaServiceAccountToken: Boolean = false,

    val devInstructionsId: UUIDString,
    val hrInstructionsId: UUIDString,

    val featureGdpr: Boolean,
    val featureMultistate: Boolean,

    val nextSurvey: SurveySlug? = null,
    val currentSurvey: SurveySlug? = null
) : ToJson

enum class OrganizationMailboxStatus {
    OK,
    NOT_CONNECTED
}

data class OrganizationRegulationDto(
    val id: UUIDString,
    val slug: String,
    val name: String,
    val longName: String,
    val description: String,
    val enabled: Boolean,
    val identityVerificationInstruction: String? = ""
)

data class OrganizationMessageSettingsDto(
    val headerImageUrl: String,
    val physicalAddress: String,
    val messageSignature: String,
    val defaultMessageSignature: String
)

data class OrganizationMailboxStatusDto(
    val viewSettings: Boolean,
    val mailboxStatus: OrganizationMailboxStatus,
    val mailboxOauthUrl: String?
)
