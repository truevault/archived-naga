package polaris.models.dto.organization

import polaris.models.UUIDString
import polaris.models.entity.organization.DataRetentionPolicyDetailCategory

data class DataRetentionPolicyDto(val detail: List<DataRetentionPolicyCategoryDto>)

data class DataRetentionPolicyCategoryDto(val category: DataRetentionPolicyDetailCategory, val description: String, val pic: List<UUIDString>)
