package polaris.models.dto.organization

import java.util.*

data class CustomMessageTemplateTextDto(
    val customText: String
)
