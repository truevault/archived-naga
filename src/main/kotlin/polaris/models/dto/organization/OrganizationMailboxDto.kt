package polaris.models.dto.organization

import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.MailboxType

data class OrganizationMailboxDto(
    val status: MailboxStatus,
    val type: MailboxType,
    val mailbox: String,
    val headerImageUrl: String,
    val physicalAddress: String,
    val messageSignature: String
)
