package polaris.models.dto.organization

import polaris.models.Iso8601Date
import polaris.models.OrganizationPublicId
import polaris.models.ToJson
import polaris.models.UUIDString
import polaris.models.dto.vendor.LabeledEnumDto
import polaris.models.dto.vendor.VendorClassificationDto
import polaris.models.dto.vendor.VendorContractDto
import polaris.models.dto.vendor.VendorContractReviewedDto
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.DataAccessibilityEnum
import polaris.models.entity.organization.DataDeletabilityEnum
import polaris.models.entity.organization.GDPRContactProcessorStatus
import polaris.models.entity.organization.GDPRContactUnsafeTransfer
import polaris.models.entity.organization.GDPRSccInPlace
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.vendor.DataRecipientType
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.models.entity.vendor.ProcessingRegion
import polaris.models.entity.vendor.ServiceProviderRecommendation
import java.time.ZonedDateTime

data class OrganizationDataRecipientDto(
    val id: String,
    val vendorId: UUIDString,
    val name: String?,
    val vendorKey: String?,
    val aliases: List<String>?,
    val category: String?,
    val url: String?,
    val ccpaRequestToDeleteLink: String? = null,
    val deletionInstructions: String?,
    val accessInstructions: String?,
    val optOutInstructions: String?,
    val generalGuidance: String?,
    val specialCircumstancesGuidance: String?,
    val ccpaRequestToKnowLink: String? = null,
    val email: String?,
    val defaultEmail: String?,
    val serviceProviderRecommendation: ServiceProviderRecommendation? = null,
    val serviceProviderLanguageUrl: String? = null,
    val gdprProcessorRecommendation: GDPRProcessorRecommendation? = null,
    val gdprProcessorLanguageUrl: String? = null,
    val gdprInternationalDataTransferRulesApply: Boolean? = false,
    val gdprHasSccRecommendation: Boolean = false,
    var gdprSccDocumentationUrl: String? = null,
    val dpaApplicable: Boolean = false,
    val dpaUrl: String? = null,
    val logoUrl: String?,
    val dateAdded: Iso8601Date?,
    val defaultClassification: VendorClassificationDto?,
    val dataRecipientType: DataRecipientType,
    val classification: VendorClassificationDto?,
    val classificationOptions: List<VendorClassificationDto>,
    val vendorContract: VendorContractDto?,
    val vendorContractOptions: List<VendorContractDto>,
    val vendorContractReviewed: VendorContractReviewedDto?,
    val vendorContractReviewedOptions: List<VendorContractReviewedDto>,
    val organization: OrganizationPublicId,
    val organizationName: String,
    val status: String,
    val publicTosUrl: String? = null,
    val ccpaIsSelling: Boolean,
    val ccpaIsSharing: Boolean,
    val usesCustomAudience: Boolean,
    val tosFileName: String?,
    val tosFileKey: String?,
    val isCustom: Boolean,
    val contacted: Boolean?,
    val isDataStorage: Boolean,
    val mappingProgress: LabeledEnumDto<MappingProgressEnum>,
    val dataAccessibility: DataAccessibilityEnum? = null,
    val dataDeletability: DataDeletabilityEnum? = null,
    val helptextAdnetworkSharing: String? = null,
    val helptextIntentionalInteractionDisclosure: String? = null,
    val automaticallyClassified: Boolean? = null,
    val dataMapped: Boolean? = null,
    val instructions: List<RequestHandlingInstructionEmbeddedDto>,
    val recommendedPersonalInformationCategories: List<String> = mutableListOf(),
    val isPlatform: Boolean,
    val isStandalone: Boolean,
    val isInstalled: Boolean,
    val respectsPlatformRequests: Boolean,
    val isPotentialIntentionalInteractionVendor: Boolean = false,
    val isAutomaticIntentionalInteractionVendor: Boolean? = false,
    val isPotentialSellingVendor: Boolean? = false,
    val isAutomaticSellingVendor: Boolean? = false,

    val isPotentialUploadVendor: Boolean? = false,
    val isAutomaticUploadVendor: Boolean? = false,
    val isUploadVendor: Boolean? = false,

    val disclosedToByEeaUkGroup: Boolean = false,
    val disclosedToByUsGroup: Boolean = false,

    var gdprProcessorSetting: GDPRProcessorRecommendation? = null,
    var isGdprProcessorSettingLocked: Boolean,
    var gdprProcessorGuaranteeUrl: String? = null,
    var gdprProcessorGuaranteeFileName: String? = null,
    var gdprProcessorGuaranteeFileKey: String? = null,
    var gdprControllerGuaranteeUrl: String? = null,
    var gdprControllerGuaranteeFileName: String? = null,
    var gdprControllerGuaranteeFileKey: String? = null,
    var gdprContactUnsafeTransfer: GDPRContactUnsafeTransfer? = null,
    var gdprEnsureSccInPlace: GDPRSccInPlace? = null,
    var gdprContactProcessorStatus: GDPRContactProcessorStatus? = null,

    var gdprHasSccSetting: Boolean? = null,
    var gdprSccUrl: String? = null,
    var gdprSccFileName: String? = null,
    var gdprSccFileKey: String? = null,
    val gdprHasSccs: Boolean = false,
    val gdprNeedsSccs: Boolean = false,
    val gdprHasUserProvidedSccs: Boolean = false,

    val removedFromExceptionsToScc: Boolean = false,
    val isExceptedFromSccs: Boolean = false,
    val isPotentiallyExceptedFromSccs: Boolean = false,

    var completed: Boolean = true,

    val defaultRetentionReasons: List<String>,

    var processingRegions: List<ProcessingRegion>,
    var collectionContext: List<CollectionContext>
) : ToJson

data class OrganizationVendorTypesDto(
    val dataSubjectTypeId: UUIDString,
    val dataRequestTypeId: UUIDString
)

data class OrganizationVendorSettingsDto(
    val contacted: Boolean? = null,
    val email: String? = null,
    val classificationId: UUIDString? = null,
    val vendorContractId: UUIDString? = null,
    val vendorContractReviewedId: UUIDString? = null,
    val ccpaIsSelling: Boolean? = null,
    val ccpaIsSharing: Boolean? = null,
    val usesCustomAudience: Boolean? = null,
    val isUploadVendor: Boolean? = null,
    val automaticallyClassified: Boolean? = null,
    val isDataStorage: Boolean? = null,
    val publicTosUrl: String? = null,
    val tosFileName: String? = null,
    val tosFileKey: String? = null,
    val gdprProcessorSetting: GDPRProcessorRecommendation? = null,
    val gdprProcessorSettingSetAt: ZonedDateTime? = null,
    val gdprProcessorGuaranteeUrl: String? = null,
    val gdprProcessorGuaranteeFileName: String? = null,
    val gdprProcessorGuaranteeFileKey: String? = null,
    val gdprControllerGuaranteeUrl: String? = null,
    val gdprControllerGuaranteeFileName: String? = null,
    val gdprControllerGuaranteeFileKey: String? = null,
    val gdprContactUnsafeTransfer: GDPRContactUnsafeTransfer? = null,
    val gdprEnsureSccInPlace: GDPRSccInPlace? = null,
    val gdprContactProcessorStatus: GDPRContactProcessorStatus? = null,
    val gdprHasSccSetting: Boolean? = null,
    val gdprSccUrl: String? = null,
    val gdprSccFileName: String? = null,
    val gdprSccFileKey: String? = null,
    val removedFromExceptionsToScc: Boolean? = null,
    var processingRegions: List<ProcessingRegion>? = null,
    var collectionContext: List<CollectionContext>? = null,
    val dataAccessibility: DataAccessibilityEnum? = null,
    val dataDeletability: DataDeletabilityEnum? = null,
    var completed: Boolean? = null
)

data class OrganizationVendorNewAndEditDto(
    val classificationOptions: List<VendorClassificationDto>,
    val vendorContractOptions: List<VendorContractDto>,
    val vendorContractReviewedOptions: List<VendorContractReviewedDto>,
    val existingCategories: List<String> = emptyList(),
    val vendor: OrganizationDataRecipientDto? = null
)
