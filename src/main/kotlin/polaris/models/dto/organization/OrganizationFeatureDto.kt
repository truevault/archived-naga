package polaris.models.dto.organization

data class OrganizationFeatureDto(
    val name: String,
    val description: String,
    val enabled: Boolean
)
