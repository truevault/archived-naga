package polaris.models.dto.organization

import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.vendor.ProcessingRegion
import java.util.*

data class UpdateCollectionGroupDto(
    val name: String?,
    val enabled: Boolean?,
    val description: String? = null,
    val privacyCenterEnabled: Boolean?,
    val collectionGroupType: CollectionGroupType?,
    val gdprEeaUkDataCollection: Boolean? = null,
    val processingRegions: List<ProcessingRegion>? = null,
    val privacyNoticeIntroText: String?
)
