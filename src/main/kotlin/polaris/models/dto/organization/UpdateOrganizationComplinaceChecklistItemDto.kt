package polaris.models.dto.organization

data class UpdateOrganizationComplinaceChecklistItemDto(
    val slug: String,
    val description: String = "",
    val done: Boolean = false
)
