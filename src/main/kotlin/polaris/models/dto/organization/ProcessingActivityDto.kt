package polaris.models.dto.organization

import polaris.models.UUIDString
import polaris.models.entity.compliance.AutocreatedProcessingActivitySlug
import polaris.models.entity.organization.LawfulBasis
import polaris.models.entity.vendor.ProcessingRegion
import java.util.UUID

data class DefProcessingActivityDto(
    val id: UUIDString,
    val name: String,
    val slug: String?,
    val materialIconKey: String?,
    val displayOrder: Int?,
    val deprecated: Boolean,
    val helpText: String?,
    val mandatoryLawfulBases: List<LawfulBasis>,
    val optionalLawfulBases: List<LawfulBasis>
)

data class ProcessingActivityDto(
    val id: UUIDString,
    val name: String = "",
    val lawfulBases: List<LawfulBasis>,

    val regions: List<ProcessingRegion>,
    val autocreationSlug: AutocreatedProcessingActivitySlug?,

    val isDefault: Boolean,

    val defaultId: UUIDString?,
    val defaultSlug: String?,
    val defaultMandatoryLawfulBases: List<LawfulBasis>,
    val defaultOptionalLawfulBases: List<LawfulBasis>,

    val processedPic: List<UUIDString>
)

data class CreateProcessingActivityRequest(
    val name: String?,
    val defaultId: UUID?,
    val lawfulBases: List<LawfulBasis>?,
    val regions: List<ProcessingRegion>?
)

data class PatchProcessingActivityRequest(
    val name: String?,
    val lawfulBases: List<LawfulBasis>?,
    val regions: List<ProcessingRegion>?
)
