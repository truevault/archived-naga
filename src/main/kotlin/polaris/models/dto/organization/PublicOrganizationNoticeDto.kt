package polaris.models.dto.organization

import polaris.models.ToJson
import polaris.models.dto.cookiescan.OrganizationCookieDto

data class PublicOrganizationNoticeDto(
    val type: String, // dev || hr
    val privacyCenter: PublicPrivacyCenterDto,
    val isGoogleAnalytics: Boolean,
    val isGdpr: Boolean,
    val isSharing: Boolean,
    val isSelling: Boolean,
    val showFinancialIncentive: Boolean,
    val showLimitNotice: Boolean,
    val hasJobApplicants: Boolean,

    val hasPhysicalLocation: Boolean,
    val hasMobileApplication: Boolean,

    val employmentGroupNotices: List<EmploymentGroupNotices>,
    val cookies: List<OrganizationCookieDto>,
    val cookieScanUrl: String?
) : ToJson

data class EmploymentGroupNotices(
    val name: String,
    val url: String
)

data class PublicPrivacyCenterDto(
    val url: String,
    val id: String
)
