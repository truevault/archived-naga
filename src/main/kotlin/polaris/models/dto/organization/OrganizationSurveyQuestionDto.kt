package polaris.models.dto.organization

import polaris.models.OrganizationPublicId

data class OrganizationSurveyQuestionDto(
    val organizationId: OrganizationPublicId,
    val surveyName: String,
    val question: String,
    val slug: String,
    val answer: String?
)
