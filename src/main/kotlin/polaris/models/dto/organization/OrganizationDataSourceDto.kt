package polaris.models.dto.organization

import polaris.models.ToJson
import polaris.models.UUIDString
import polaris.models.entity.organization.CollectionContext

data class OrganizationDataSourceDto(
    val vendorId: UUIDString,
    val name: String,
    val vendorKey: String?,
    val aliases: List<String>?,
    val category: String?,
    val logoUrl: String?,
    val url: String?,
    val email: String?,
    val collectionContext: List<CollectionContext>,
    val isCustom: Boolean
) : ToJson
