package polaris.models.dto.organization

import polaris.models.dto.vendor.LabeledEnumDto
import polaris.models.entity.organization.MappingProgressEnum

data class OrganizationProgressDto(
    val key: String,
    val progress: LabeledEnumDto<MappingProgressEnum>
)
