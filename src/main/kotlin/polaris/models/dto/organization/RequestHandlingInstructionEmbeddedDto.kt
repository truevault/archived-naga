package polaris.models.dto.organization

import polaris.models.entity.organization.DoNotContactReason
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod

data class RequestHandlingInstructionEmbeddedDto(
    val requestType: DataRequestInstructionType,
    val processingMethod: ProcessingMethod?,
    val processingInstructions: String?,
    val deleteDoNotContact: Boolean?,
    val hasRetentionExceptions: Boolean?,
    var doNotContactReason: DoNotContactReason? = null,
    var doNotContactExplanation: String? = null
)
