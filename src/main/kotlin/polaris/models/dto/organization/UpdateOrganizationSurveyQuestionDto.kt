package polaris.models.dto.organization

data class UpdateOrganizationSurveyQuestionDto(
    val slug: String,
    val question: String?,
    val answer: String?
)
