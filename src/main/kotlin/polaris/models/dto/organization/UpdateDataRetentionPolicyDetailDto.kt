package polaris.models.dto.organization

import polaris.models.UUIDString

data class UpdateDataRetentionPolicyDetailDto(val pic: List<UUIDString>, val description: String)
