package polaris.models.dto.organization

import polaris.models.ToJson
import polaris.models.UUIDString
import polaris.models.dto.vendor.*
import java.util.*

data class OrgDataRecipientPlatformDto(
    val id: String,
    val logoUrl: String?,
    val vendorId: UUIDString,
    val name: String?,
    val vendorKey: String?,
    val available: List<OrganizationDataRecipientDto>,
    val installed: List<UUID>
) : ToJson
