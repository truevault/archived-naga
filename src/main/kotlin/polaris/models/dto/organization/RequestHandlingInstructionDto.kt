package polaris.models.dto.organization

import polaris.models.OrganizationPublicId
import polaris.models.UUIDString
import polaris.models.VendorId
import polaris.models.entity.organization.DoNotContactReason
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod

data class RequestHandlingInstructionDto(
    val id: UUIDString? = null,
    val organizationId: OrganizationPublicId,
    val requestType: DataRequestInstructionType,
    val vendorId: VendorId? = null,
    val processingMethod: ProcessingMethod? = null,
    var retentionReasons: List<String>? = null,
    val processingInstructions: String? = null,
    val deleteDoNotContact: Boolean? = null,
    var doNotContactReason: DoNotContactReason? = null,
    var doNotContactExplanation: String? = null,
    val exceptionsAffirmed: Boolean = false,
    val hasRetentionExceptions: Boolean? = null,
    val deletedPIC: List<String>? = emptyList()
)
