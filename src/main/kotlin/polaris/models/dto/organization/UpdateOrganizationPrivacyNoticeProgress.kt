package polaris.models.dto.organization

import polaris.models.entity.organization.PrivacyNoticeProgress

data class UpdateOrganizationPrivacyNoticeProgress(
    val caPrivacyNoticeProgress: PrivacyNoticeProgress,
    val optOutPrivacyNoticeProgress: PrivacyNoticeProgress
)
