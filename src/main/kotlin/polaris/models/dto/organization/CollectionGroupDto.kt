package polaris.models.dto.organization

import polaris.models.UUIDString
import polaris.models.dto.vendor.LabeledEnumDto
import polaris.models.dto.vendor.LabeledEnumDtoIface
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.compliance.BusinessPurpose
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.compliance.PersonalInformationType
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.organization.PrivacyNoticeProgress
import polaris.models.entity.vendor.ProcessingRegion

// ConsumerGroupDto
typealias CollectionGroupDto = String

// ConsumerGroupMappingProgressDto
data class CollectionGroupMappingProgressDto(
    override val value: MappingProgressEnum,
    override val label: String,
    val personalInformationType: PersonalInformationType
) : LabeledEnumDtoIface<MappingProgressEnum>

// ConsumerGroupOrganizationVendorDto
data class CollectionGroupDataRecipientDto(
    val consumerGroupId: UUIDString,
    val vendorId: UUIDString
)

// CollectionGroupDetailsDto
data class CollectionGroupDetailsDto(
    val id: UUIDString,
    val name: String,
    val description: String?,
    val inUse: Boolean = true,
    val enabled: Boolean,
    val collectionGroupType: CollectionGroupType,
    val autocreationSlug: AutocreatedCollectionGroupSlug?,
    val mappingProgress: List<CollectionGroupMappingProgressDto>,
    val privacyNoticeProgress: LabeledEnumDto<PrivacyNoticeProgress>,
    val processingRegions: List<ProcessingRegion>,
    val createdAsDataSubject: Boolean,
    val gdprEeaUkDataCollection: Boolean? = null,
    val businessPurposes: List<BusinessPurpose> = emptyList(),
    val privacyNoticeIntroText: String
)
