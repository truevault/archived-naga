package polaris.models.dto.organization

import polaris.models.OrganizationPublicId
import polaris.models.ToJson

data class OrganizationAuthorizationsDto(
    val id: OrganizationPublicId,
    val hasGoogle: Boolean
) : ToJson
