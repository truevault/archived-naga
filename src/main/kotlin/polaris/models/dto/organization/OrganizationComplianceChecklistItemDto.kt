package polaris.models.dto.organization

import polaris.models.OrganizationPublicId

data class OrganizationComplianceChecklistItemDto(
    val organizationId: OrganizationPublicId,
    val slug: String,
    val description: String = "",
    val done: Boolean = false
)
