package polaris.models.dto.organization

import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.vendor.ProcessingRegion
import java.util.*

data class CreateCollectionGroupDto(
    val name: String,
    val description: String,
    val enabled: Boolean = true,
    val privacyCenterEnabled: Boolean = true,
    val collectionGroupType: CollectionGroupType,
    val gdprEeaUkDataCollection: Boolean = false,
    val gdprDataSubject: Boolean = false,
    val processingRegions: List<ProcessingRegion>? = null
)
