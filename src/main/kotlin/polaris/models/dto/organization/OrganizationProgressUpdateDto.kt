package polaris.models.dto.organization

import polaris.models.entity.organization.MappingProgressEnum

data class OrganizationProgressUpdateDto(
    val key: String,
    val progress: MappingProgressEnum
)
