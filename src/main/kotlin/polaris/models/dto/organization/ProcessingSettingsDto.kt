package polaris.models.dto.organization

import polaris.models.OrganizationPublicId
import polaris.models.UUIDString
import polaris.models.VendorId
import polaris.models.entity.request.DataRequestType

data class ProcessingSettingsDto(
    val processingInstructions: List<ProcessingInstructionDto>,
    val organizationVendors: List<OrganizationDataRecipientDto>
)

data class ProcessingInstructionDto(
    val organizationId: OrganizationPublicId,
    val dataSubjectTypeId: UUIDString,
    val dataRequestType: DataRequestType?,
    val instruction: String
)

data class UpdateProcessingInstruction(
    val instruction: String
)

data class UpdateProcessingServices(
    val vendorIds: List<VendorId>
)
