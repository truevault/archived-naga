package polaris.models.dto.attachment

data class AttachmentDto(
    val name: String,
    val mimeType: String,
    val size: Long,
    val url: String
)
