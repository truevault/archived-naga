package polaris.models.dto

enum class ApiMode {
    Draft,
    Live
}
