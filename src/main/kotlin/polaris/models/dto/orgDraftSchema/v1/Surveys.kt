package polaris.models.dto.orgDraftSchema.v1

typealias SurveyAcknowledgement = Boolean
typealias VendorID = String

data class Surveys(
    val `business-survey`: BusinessSurvey = BusinessSurvey(),
    val `eea-uk-survey`: EeaUkSurvey = EeaUkSurvey(),
    val `reminder-survey`: ReminderSurvey = ReminderSurvey(),
    val `sharing-survey`: SharingSurvey = SharingSurvey(),
    val `selling-survey`: SellingSurvey = SellingSurvey()
)

data class BusinessSurvey(
    var `business-physical-location`: Boolean? = null,
    var `business-products-aimed-at-kids`: Boolean? = null,
    var `business-products-aimed-at-kids-under-13`: Boolean? = null,
    var `business-has-mobile-apps`: Boolean? = null,
    var `business-california-nonenglish`: Boolean? = null,
    var `business-survice-provider`: Boolean? = null,
    var `business-california-10million`: Boolean? = null,
    var `business-service-provider`: Boolean? = null,
    var `business-hipaa-compliant`: Boolean? = null,
    var `business-glba-compliant`: Boolean? = null,
    var `business-fcra-compliant`: Boolean? = null,
    var `business-employees-ca`: Boolean? = null,
    var `business-contractors-ca`: Boolean? = null,
    var `business-job-applicants-ca`: Boolean? = null
)

data class EeaUkSurvey(
    var `personal-data-large-scale`: Boolean? = null,
    var `employees-eea-uk`: Boolean? = null,
    var `established-in-eea-uk`: Boolean? = null,
    var `non-english-website`: Boolean? = null
)

data class ReminderSurvey(
    var `reminder-unsafe-transfers`: Boolean? = null
)

data class SharingSurvey(
    var `sharing-definition-acknowledgement`: SurveyAcknowledgement? = null,
    var `share-data-for-advertising`: Boolean? = null,
    var `vendor-sharing-categories`: List<VendorID>? = null,
    var `custom-audience-feature`: List<VendorID>? = null,
    var `facebook-ldu`: Boolean? = null,
    var `google-rdp`: Boolean? = null
)

data class SellingSurvey(
    var `selling-definition-acknowledgement`: SurveyAcknowledgement? = null,
    var `disclosure-for-intentional-interaction`: Boolean? = null,
    var `vendor-intentional-disclosure-selection`: List<VendorID>? = null,
    var `google-analytics-data-sharing`: Boolean? = null,
    var `shopify-audiences`: Boolean? = null,
    var `create-profiles-for-consumers`: Boolean? = null,
    var `sell-in-exchange-for-money`: Boolean? = null,
    var `disclosure-for-selling`: Boolean? = null,
    var `vendor-selling-selection`: List<VendorID>? = null,
    var `selling-upload-data`: Boolean? = null,
    var `vendor-upload-data-selection`: List<VendorID>? = null
)
