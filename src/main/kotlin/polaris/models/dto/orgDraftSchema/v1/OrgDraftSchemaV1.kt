package polaris.models.dto.orgDraftSchema.v1

data class OrgDraftSchemaV1(
    val surveys: Surveys = Surveys()
)
