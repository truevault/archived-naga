package polaris.models.dto.springevents

import polaris.models.entity.organization.Event

data class PublishPolarisEvents(val events: List<Event>)
