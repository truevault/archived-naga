package polaris.models.dto.springevents

import polaris.models.OrganizationPublicId
import java.util.UUID

data class UpdateTasksSpringEvent(val orgPublicId: OrganizationPublicId, val autoCreate: Boolean, val autoClose: Boolean, val actorId: UUID? = null)
