package polaris.models.dto.nylas

data class NylasWebhookDto(
    val deltas: List<NylasWebhookDelta>
)

data class NylasWebhookDelta(
    val date: Long?,
    val type: String?,
    val object_data: NylasWebhookObjectData?
)

data class NylasWebhookObjectData(
    val account_id: String?,
    val id: String?,
    val metadata: NylasWebhookMetadata?
)

data class NylasWebhookMetadata(
    val sender_app_id: String?,
    val payload: String?,
    val message_id: String?,
    val reply_to_message_id: String?,
    val thread_id: String?,
    val from_self: Boolean?,
    val timestamp: Long?
)

// metadata": {
// "sender_app_id": "SENDER_APP_ID",
// "payload": "{myCustom: 'data'}",
// "message_id": "NYLAS_MESSAGE_ID",
// "reply_to_message_id": "NYLAS_REPLY_TO_MESSAGE_ID",
// "thread_id": "NYLAS_THREAD_ID",
// "from_self": true,
// "timestamp": 1471630848,
// },
