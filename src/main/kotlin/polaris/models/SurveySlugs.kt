package polaris.models

class SurveySlugs {
    companion object {
        const val SHARE_DATA_FOR_ADVERTISING_QUESTION_SLUG = "share-data-for-advertising"
        const val CUSTOM_AUDIENCE_QUESTION_SLUG = "custom-audience-feature"
        const val VENDOR_SHARING_QUESTION_SLUG = "vendor-sharing-categories"
        const val DIRECT_SHARE_FOR_ADVERTISING_QUESTION_SLUG = "direct-share-for-advertising"
        const val SHOPIFY_AUDIENCES_QUESTION_SLUG = "shopify-audiences"
        // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
        // const val GOOGLE_RDP_QUESTION_SLUG = "google-rdp"
        // const val FACEBOOK_LDU_QUESTION_SLUG = "facebook-ldu"
        const val GOOGLE_ANALYTICS_DATA_SHARING_SLUG = "google-analytics-data-sharing"
        const val DISCLOSURE_FOR_SELLING_SLUG = "disclosure-for-selling"
        const val SELLING_UPLOAD_DATA_SLUG = "selling-upload-data"
        const val VENDOR_UPLOAD_DATA_SELECTION_SLUG = "vendor-upload-data-selection"
        const val VENDOR_SELLING_QUESTION_SLUG = "vendor-selling-selection"
        const val VENDOR_INTENTIONAL_DISCLOSURE_SELECTION = "vendor-intentional-disclosure-selection"
        const val SELL_IN_EXCHANGE_FOR_MONEY_SLUG = "sell-in-exchange-for-money"

        const val WEBSITE_AUDIT_CA_PRIVACY_NOTICE_SLUG = "ca-privacy-notice"
        const val WEBSITE_AUDIT_OPT_OUT_LINK_SLUG = "opt-out-link"
        const val WEBSITE_AUDIT_OPT_OUT_LINK_UPGRADE_SLUG = "opt-out-link-upgrade"
        const val WEBSITE_AUDIT_POLARIS_JS_SLUG = "polaris-js"
        const val WEBSITE_AUDIT_PRIVACY_POLICY_LINK_SLUG = "privacy-policy-link"
        const val WEBSITE_AUDIT_DEFER_SLUG = "defer-website-audit"

        const val DATA_MINIMIZATION_SLUG = "cpc-data-minimization"
    }
}
