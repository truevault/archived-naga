package polaris.models

import java.time.LocalDate
import java.time.ZonedDateTime

// ID Types
typealias OrganizationPublicId = String
typealias DataRequestPublicId = String
typealias PrivacyCenterPublicId = String
typealias VendorId = UUIDString

// DTO Types
typealias Iso8601Date = String

typealias UUIDString = String

fun Iso8601Date.toDateTime() = ZonedDateTime.parse(this)
fun Iso8601Date.toDate() = LocalDate.parse(this)
