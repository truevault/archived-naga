package polaris.models.entity.request

import polaris.models.dto.request.DataSubjectRequestEventEmbeddedDto
import polaris.models.dto.toIso
import polaris.models.dto.user.UserDto
import polaris.models.entity.attachment.DataSubjectRequestEventAttachment
import polaris.models.entity.user.User
import polaris.services.dto.UserDtoService
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import javax.persistence.*

@Entity
data class DataSubjectRequestEvent(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false)
    val actor: User? = null,

    val isDataSubject: Boolean = false,

    @ManyToOne
    @JoinColumn(name = "data_subject_request_id", updatable = false)
    val dataSubjectRequest: DataSubjectRequest? = null,

    @OneToMany(mappedBy = "dataSubjectRequestEvent", cascade = [CascadeType.ALL])
    val attachments: List<DataSubjectRequestEventAttachment> = emptyList(),

    @Enumerated(EnumType.STRING)
    val eventType: DataRequestEventType = DataRequestEventType.CREATED,
    val message: String? = null,

    val createdAt: ZonedDateTime = ZonedDateTime.now()
) {
    fun toEmbeddedDto(userDtoService: UserDtoService): DataSubjectRequestEventEmbeddedDto {
        val actor = if (isDataSubject) {
            val hasAnyNames = dataSubjectRequest?.subjectFirstName != null || dataSubjectRequest?.subjectLastName != null

            UserDto(
                email = dataSubjectRequest?.subjectEmailAddress ?: "",
                lastLogin = null,
                firstName = if (hasAnyNames) { dataSubjectRequest?.subjectFirstName } else { "Data" },
                lastName = if (hasAnyNames) { dataSubjectRequest?.subjectLastName } else { "Subject" }
            )
        } else {
            userDtoService.toDtoOrSystem(actor)
        }

        return DataSubjectRequestEventEmbeddedDto(
            eventId = id,
            actor = actor,
            eventType = eventType,
            eventTypeTitle = eventTypeTitle(),
            eventTypeDescription = eventTypeDescription(),
            eventDate = eventDate().toIso(),
            isUserFacingEvent = isUserFacingEvent(),
            isRequesterAction = isRequesterAction(),
            message = message,
            createdAt = createdAt.toIso(),
            attachments = attachments.mapNotNull { it.attachment?.toDto() }
        )
    }

    private fun isUserFacingEvent(): Boolean {
        return !listOf(
            DataRequestEventType.REQUEST_UPDATED,
            DataRequestEventType.NOTE_ADDED,
            DataRequestEventType.MESSAGE_SENT,
            DataRequestEventType.MESSAGE_RECEIVED,
            DataRequestEventType.REOPENED,
            DataRequestEventType.REQUEST_DATE_UPDATED
        ).contains(eventType)
    }

    private fun isRequesterAction(): Boolean {
        return listOf(DataRequestEventType.CREATED, DataRequestEventType.IDENTITY_VERIFIED).contains(eventType)
    }

    private fun eventTypeDescription(): String {
        if (dataSubjectRequest == null) {
            return "Unknown request"
        }

        var source = when (dataSubjectRequest.submissionMethod) {
            DataRequestSubmissionMethodEnum.TELEPHONE -> "Telephone"
            DataRequestSubmissionMethodEnum.EMAIL -> "Email"
            else -> {
                if (dataSubjectRequest.submissionSource == DataRequestSubmissionSourceEnum.POLARIS) {
                    "Polaris"
                } else {
                    "Privacy Center"
                }
            }
        }

        val dateFormatter = DateTimeFormatter.ofPattern("MMM d, YYYY")
        val oldDate = if (dataSubjectRequest.requestOriginalDueDate == null) "" else dateFormatter.format(dataSubjectRequest.requestOriginalDueDate)
        val newDate = if (dataSubjectRequest.requestDueDate == null) "" else dateFormatter.format(dataSubjectRequest.requestDueDate)
        val duplicatedRequest = dataSubjectRequest.duplicatesRequest

        return when (eventType) {
            DataRequestEventType.CREATED -> "${dataSubjectRequest.requestType.label} submitted via $source"
            DataRequestEventType.CLOSED -> "Request processed"
            DataRequestEventType.CLOSED_NOT_VERIFIED -> "Unable to verify requester"
            DataRequestEventType.CLOSED_NOT_FOUND -> "No record of requester"
            DataRequestEventType.CLOSED_SERVICE_PROVIDER -> "Business is a service provider"
            DataRequestEventType.REOPENED -> "Request re-opened"
            DataRequestEventType.IDENTITY_VERIFIED -> "Requester completed email verification"
            DataRequestEventType.MESSAGE_RECEIVED -> "Message received"
            DataRequestEventType.MESSAGE_SENT -> "Message sent"
            DataRequestEventType.NOTE_ADDED -> "Note added"
            DataRequestEventType.REQUEST_DATE_UPDATED -> "Submission date changed"
            DataRequestEventType.REQUEST_DEADLINE_UPDATED -> "Deadline extended from $oldDate to $newDate"
            DataRequestEventType.REQUEST_UPDATED -> "Request updated"
            DataRequestEventType.REQUESTOR_CONTACTED -> "Requester contacted"
            DataRequestEventType.IDENTITY_VERIFICATION_SENT -> "Identity verification sent"
            DataRequestEventType.CLOSED_AS_DUPLICATE -> "This request was identified as a duplicate submission for ${duplicatedRequest?.publicId?.let { "request $it" } ?: "another request"} and was automatically closed."
        }
    }

    private fun eventTypeTitle(): String {
        return when (eventType) {
            DataRequestEventType.CREATED -> "Request submitted"
            DataRequestEventType.CLOSED -> "Request closed"
            DataRequestEventType.CLOSED_NOT_VERIFIED -> "Request closed"
            DataRequestEventType.CLOSED_NOT_FOUND -> "Request closed"
            DataRequestEventType.CLOSED_SERVICE_PROVIDER -> "Request closed"
            DataRequestEventType.REOPENED -> "Request re-opened"
            DataRequestEventType.IDENTITY_VERIFIED -> "Request verified"
            DataRequestEventType.MESSAGE_RECEIVED -> "Message recieved"
            DataRequestEventType.MESSAGE_SENT -> "Message sent"
            DataRequestEventType.NOTE_ADDED -> "Note added"
            DataRequestEventType.REQUEST_DATE_UPDATED -> "Change to the submitted date of a manual request"
            DataRequestEventType.REQUEST_DEADLINE_UPDATED -> "Deadline extended"
            DataRequestEventType.REQUEST_UPDATED -> "Request updated"
            DataRequestEventType.REQUESTOR_CONTACTED -> "Requester contacted"
            DataRequestEventType.IDENTITY_VERIFICATION_SENT -> "Verification sent"
            DataRequestEventType.CLOSED_AS_DUPLICATE -> "Request closed as duplicate"
        }
    }

    fun eventDate(): ZonedDateTime {
        if (eventType == DataRequestEventType.CREATED && dataSubjectRequest?.submissionSource == DataRequestSubmissionSourceEnum.POLARIS) {
            return dataSubjectRequest.requestDate
        }

        return createdAt
    }

    override fun toString() = "DataSubjectRequestEvent(id=$id, eventType=$eventType, dataSubjectRequest=${dataSubjectRequest?.publicId}, actor=${actor?.id}, isDataSubject=$isDataSubject)"
}
