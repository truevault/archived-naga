package polaris.models.entity.request

enum class DataRequestTypeEnum(val label: String) {
    ERASURE("Erasure"),
    ACCESS("Access"),
    RECTIFICATION("Rectification"),
    OBJECTION_TO_PROCESSING("Objection to Processing");
}
