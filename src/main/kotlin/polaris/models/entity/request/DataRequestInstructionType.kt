package polaris.models.entity.request

enum class DataRequestInstructionType(val label: String) {
    KNOW("KNOW"),
    DELETE("DELETE"),
    OPT_OUT("OPT_OUT"),
}
