package polaris.models.entity.request

enum class RequestWorkflow(val handler: WorkflowHandler, val initialState: DataRequestState = DataRequestState.VERIFY_CONSUMER) {
    KNOW(KnowWorkflowHandler()),
    DELETE(DeleteWorkflowHandler()),
    OPT_OUT(OptOutWorkflowHandler(), DataRequestState.REMOVE_CONSUMER),
    PORTABILITY(KnowWorkflowHandler()),
    RECTIFICATION(RectificationWorkflowHandler()),
    OBJECTION(ObjectionWorkflowHandler()),
    WITHDRAW_CONSENT(WithdrawConsentWorkflowHandler()),
    LIMIT(LimitWorkflowHandler()),
    NONE(NullWorkflowHandler());
}
