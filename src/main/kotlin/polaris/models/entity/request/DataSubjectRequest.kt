package polaris.models.entity.request

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import polaris.models.DataRequestPublicId
import polaris.models.dto.request.SubjectStatus
import polaris.models.entity.attachment.Attachment
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.Organization
import polaris.models.entity.user.User
import polaris.models.listeners.DataSubjectRequestEntityListener
import polaris.util.IdGenerator
import polaris.util.converters.DataRequestStateHistoryCon
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.AttributeConverter
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OrderBy

@EntityListeners(DataSubjectRequestEntityListener::class)
@Entity
data class DataSubjectRequest(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    val publicId: DataRequestPublicId = IdGenerator.generate(),

    val replyKey: UUID = UUID.randomUUID(),

    val confirmationToken: UUID = UUID.randomUUID(),
    var emailVerifiedAutomatically: Boolean = false,
    var emailVerifiedManually: Boolean = false,
    var emailVerifiedDate: ZonedDateTime? = null,

    @Enumerated(EnumType.STRING)
    var context: CollectionContext = CollectionContext.CONSUMER,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "email_verified_manually_by", updatable = false)
    val emailVerifiedManuallyBy: User? = null,

    @Enumerated(EnumType.STRING)
    var state: DataRequestState = DataRequestState.PENDING_EMAIL_VERIFICATION,

    @Convert(converter = DataRequestStateHistoryCon::class)
    var stateHistory: List<DataRequestState> = emptyList(),

    @Enumerated(EnumType.STRING)
    var substate: DataRequestSubstate? = null,

    var subjectFirstName: String? = "",
    var subjectLastName: String? = "",
    var subjectEmailAddress: String? = null,
    var subjectPhoneNumber: String? = null,
    var subjectMailingAddress: String? = null,
    val subjectIpAddress: String? = null,
    var selfDeclarationProvided: Boolean = false,
    var organizationCustomInput: String? = null,

    @ManyToOne
    @JoinColumn(name = "duplicates_request_id", updatable = true)
    var duplicatesRequest: DataSubjectRequest? = null,

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JoinTable(
        name = "data_subject_request_collection_groups",
        joinColumns = [JoinColumn(name = "data_subject_request_id")],
        inverseJoinColumns = [JoinColumn(name = "collection_group_id")]
    )
    var collectionGroups: MutableList<CollectionGroup> = mutableListOf(),

    @Enumerated(EnumType.STRING)
    var collectionGroupResult: SubjectStatus? = null,

    @Enumerated(EnumType.STRING)
    var submissionMethod: DataRequestSubmissionMethodEnum? = null,

    @Enumerated(EnumType.STRING)
    val submissionSource: DataRequestSubmissionSourceEnum = DataRequestSubmissionSourceEnum.POLARIS,

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @Enumerated(EnumType.STRING)
    var requestType: DataRequestType,

    @ManyToOne()
    @JoinColumn(name = "attachment_id", updatable = true)
    var attachment: Attachment? = null,

    @OneToMany(mappedBy = "dataSubjectRequest", cascade = [CascadeType.ALL])
    @OrderBy("createdAt ASC")
    val dataSubjectRequestEvents: MutableList<DataSubjectRequestEvent> = mutableListOf(),

    @OneToMany(mappedBy = "dataSubjectRequest", cascade = [CascadeType.ALL])
    var vendorOutcomes: MutableList<VendorOutcome> = mutableListOf(),

    var requestDate: ZonedDateTime = ZonedDateTime.now(),
    var requestDueDate: ZonedDateTime? = null,
    var requestOriginalDueDate: ZonedDateTime? = null,
    var contactVendorsEverHidden: Boolean? = null,
    var contactVendorsEverShown: Boolean? = null,

    var closedAt: ZonedDateTime? = null,
    var createdAt: ZonedDateTime = ZonedDateTime.now(),

    var requestorContacted: Boolean = false,
    var vendorContacted: Boolean = false,
    var isAutocreated: Boolean = false,
    var isAutoclosed: Boolean = false,

    var gpcOptOut: Boolean = false,

    var correctionRequest: String? = null,
    var correctionFinished: Boolean? = null,
    var limitationFinished: Boolean? = null,

    var objectionRequest: String? = null,

    var appealDecisionId: String? = null,

    @Convert(converter = ProcessingActivitesConverter::class)
    var consentWithdrawnRequest: MutableList<String>? = null,
    var consentWithdrawnFinished: Boolean? = null,

    val gaUserId: String? = null
) {
    fun isKnow() = requestType.isKnow()
    fun isDelete() = requestType.isDelete()
    fun isGdpr() = requestType.isGdpr()

    fun isDuplicate() = duplicatesRequest != null

    fun requiresDeclaration() = requestType == DataRequestType.CCPA_RIGHT_TO_KNOW
    fun requiresCorrectionRequest() = requestType == DataRequestType.GDPR_RECTIFICATION

    fun emailVerified(): Boolean = emailVerifiedAutomatically || emailVerifiedManually
}

class ProcessingActivitesConverter : AttributeConverter<List<String>, String> {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(ProcessingActivitesConverter::class.java)
    }

    override fun convertToDatabaseColumn(attribute: List<String>?): String {
        val result = try {
            jacksonObjectMapper().writeValueAsString(attribute)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (todb): ", e)
            "{}"
        }
        return result
    }

    override fun convertToEntityAttribute(dbData: String?): List<String> {
        if (dbData == null) {
            return emptyList()
        }
        val result: List<String> = try {
            jacksonObjectMapper().readValue(dbData)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (toentity): ", e)
            emptyList()
        }
        return result
    }
}
