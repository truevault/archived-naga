package polaris.models.entity.request

enum class DataRequestEventType(val public: Boolean) {
    CREATED(true),
    IDENTITY_VERIFIED(true),
    REOPENED(true),
    CLOSED(true),
    CLOSED_NOT_VERIFIED(true),
    CLOSED_NOT_FOUND(true),
    CLOSED_SERVICE_PROVIDER(true),
    CLOSED_AS_DUPLICATE(true),
    MESSAGE_SENT(true),
    REQUESTOR_CONTACTED(true),
    MESSAGE_RECEIVED(true),
    NOTE_ADDED(true),
    REQUEST_DATE_UPDATED(true),
    REQUEST_DEADLINE_UPDATED(true),
    REQUEST_UPDATED(true),
    IDENTITY_VERIFICATION_SENT(false)
}
