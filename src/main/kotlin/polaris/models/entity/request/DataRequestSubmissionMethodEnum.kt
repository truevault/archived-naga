package polaris.models.entity.request

enum class DataRequestSubmissionMethodEnum(val label: String) {
    EMAIL("Email"),
    TELEPHONE("Telephone");
}
