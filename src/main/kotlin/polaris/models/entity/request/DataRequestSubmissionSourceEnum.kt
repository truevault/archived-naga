package polaris.models.entity.request

enum class DataRequestSubmissionSourceEnum(val label: String) {
    POLARIS("Polaris"),
    PRIVACY_CENTER("Privacy_Center"),
    MINE_EMAIL("Mine");
}
