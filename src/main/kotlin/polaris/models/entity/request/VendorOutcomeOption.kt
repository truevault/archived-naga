package polaris.models.entity.request

enum class VendorOutcomeOption(val types: List<String>) {
    REVIEWED(listOf("RIGHT-TO-KNOW")),
    NO_DATA(listOf("ERASURE", "RIGHT-TO-DELETION")),
    ERASED(listOf("ERASURE", "RIGHT-TO-DELETION")),
    RETAINED(listOf("ERASURE", "RIGHT-TO-DELETION")),
    NO_DATA_FOUND(listOf("ACCESS", "RIGHT-TO-KNOW-SPECIFIC")),
    DATA_ATTACHED(listOf("ACCESS", "RIGHT-TO-KNOW-SPECIFIC")),
    DATA_DUPLICATED(listOf("ACCESS", "RIGHT-TO-KNOW-SPECIFIC")),
    COMPLETED(listOf("CCPA-OPT-OUT")),
    NO_CORRECTION(listOf("RECTIFICATION")),
    CORRECTED(listOf("RECTIFICATION")),
    NO_CHANGE(listOf("OBJECTION")),
    UPDATED(listOf("OBJECTION"));

    companion object {
        fun allowedCombinations(): List<List<VendorOutcomeOption>> = listOf(
            listOf(ERASED, RETAINED),
            listOf(DATA_ATTACHED, DATA_DUPLICATED)
        )
    }
}

fun VendorOutcomeOption.allowedCombinations(): List<VendorOutcomeOption> {
    val allowedCombinations = VendorOutcomeOption.allowedCombinations()

    val comboList = allowedCombinations.find { comboList -> comboList.contains(this) } ?: return emptyList()

    return comboList.filter { it != this }
}

fun VendorOutcomeOption.allowedWith(other: VendorOutcomeOption): Boolean =
    this.allowedCombinations().contains(other)
