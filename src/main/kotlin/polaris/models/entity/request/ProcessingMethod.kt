package polaris.models.entity.request

enum class ProcessingMethod(val label: String) {
    DELETE("DELETE"),
    RETAIN("RETAIN"),
    INACCESSIBLE_OR_NOT_STORED("INACCESSIBLE_OR_NOT_STORED");
}
