package polaris.models.entity.request

import javax.persistence.*

@Entity
class DataRequestTypeRequestStateHelpArticle(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @Enumerated(EnumType.STRING)
    var requestState: DataRequestState? = null,

    @Enumerated(EnumType.STRING)
    var requestType: DataRequestType? = null,

    val helpArticleId: String? = null
)
