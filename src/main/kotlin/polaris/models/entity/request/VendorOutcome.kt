package polaris.models.entity.request

import polaris.models.dto.request.VendorOutcomeValue
import polaris.models.entity.vendor.Vendor
import java.io.Serializable
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "data_subject_request_vendors")
@IdClass(VendorOutcomeId::class)
data class VendorOutcome(
    @Id
    @Column(name = "data_subject_request_id", updatable = false)
    val dataSubjectRequestId: UUID? = null,

    @Id
    @Column(name = "vendor_id", updatable = false)
    val vendorId: UUID? = null,

    @ManyToOne
    @JoinColumn(name = "data_subject_request_id", insertable = false, updatable = false)
    val dataSubjectRequest: DataSubjectRequest? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_id", insertable = false, updatable = false)
    val vendor: Vendor? = null,

    var processed: Boolean = false,
    var processedAt: ZonedDateTime? = null,

    @Enumerated(EnumType.STRING)
    var outcome: VendorOutcomeValue? = null

) {
    override fun toString() = "VendorOutcome(subjectRequest=${dataSubjectRequest?.publicId}, vendor=${vendor?.name} (${vendor?.id}), processed=$processed"
}

@Embeddable
data class VendorOutcomeId(
    val dataSubjectRequestId: UUID? = null,
    val vendorId: UUID? = null
) : Serializable
