package polaris.models.entity.request

import polaris.models.dto.request.SubjectStatus
import polaris.models.dto.request.TransitionDataRequest
import polaris.models.entity.organization.Organization
import polaris.models.entity.user.User
import polaris.services.primary.DataSubjectRequestService

typealias TransitionFn = (actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService) -> TransitionDataRequest?

abstract class WorkflowHandler {
    private val forwardHandlers: MutableMap<DataRequestState, TransitionFn> = mutableMapOf()
    private val backwardHandlers: MutableMap<DataRequestState, TransitionFn> = mutableMapOf()

    protected fun registerForward(from: DataRequestState, transition: TransitionFn) {
        forwardHandlers.put(from, transition)
    }
    protected fun registerBackward(from: DataRequestState, transition: TransitionFn) {
        backwardHandlers.put(from, transition)
    }

    fun forward(state: DataRequestState, actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        val transition = forwardHandlers.get(state)

        if (transition == null) {
            return null
        } else {
            return transition(actor, organization, request, service)
        }
    }

    fun backward(state: DataRequestState, actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        val transition = backwardHandlers.get(state)

        if (transition == null) {
            return null
        } else {
            return transition(actor, organization, request, service)
        }
    }
}

class KnowWorkflowHandler : WorkflowHandler() {
    init {
        registerForward(DataRequestState.VERIFY_CONSUMER, ::verifyConsumerForward)
        registerForward(DataRequestState.RETRIEVE_DATA, ::retrieveConsumerForward)
        registerForward(DataRequestState.SEND_DATA, ::sendOrNotifyForward)
        registerForward(DataRequestState.NOTIFY_CONSUMER, ::sendOrNotifyForward)

        registerBackward(DataRequestState.SEND_DATA, ::sendDataBackward)
        registerBackward(DataRequestState.RETRIEVE_DATA, ::retrieveBackward)
    }

    private fun verifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.RETRIEVE_DATA)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.SEND_DATA)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.SEND_DATA)
            else -> null
        }
    }

    private fun retrieveConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.SEND_DATA)
    }

    private fun sendOrNotifyForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        val substate = when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> DataRequestSubstate.COMPLETED
            SubjectStatus.NOT_FOUND -> DataRequestSubstate.SUBJECT_NOT_FOUND
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> DataRequestSubstate.PROCESSED_AS_SERVICE_PROVIDER
            else -> DataRequestSubstate.COMPLETED
        }

        return TransitionDataRequest(state = DataRequestState.CLOSED, substate = substate)
    }

    private fun sendDataBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        // Can go to DELETE_INFO, CONTACT_VENDORS, VERIFY_CONSUMER based on logic TBD
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.RETRIEVE_DATA)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            else -> null
        }
    }

    private fun retrieveBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
    }
}

class RectificationWorkflowHandler : WorkflowHandler() {
    init {
        registerForward(DataRequestState.VERIFY_CONSUMER, ::verifyConsumerForward)
        registerForward(DataRequestState.CORRECT_DATA, ::correctDataForward)
        registerForward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerForward)

        registerBackward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerBackward)
        registerBackward(DataRequestState.CORRECT_DATA, ::correctDataBackward)
    }

    private fun verifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.CORRECT_DATA)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            else -> null
        }
    }

    private fun correctDataForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
    }

    private fun notifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        val substate = when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> DataRequestSubstate.COMPLETED
            SubjectStatus.NOT_FOUND -> DataRequestSubstate.SUBJECT_NOT_FOUND
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> DataRequestSubstate.PROCESSED_AS_SERVICE_PROVIDER
            else -> DataRequestSubstate.COMPLETED
        }

        return TransitionDataRequest(state = DataRequestState.CLOSED, substate = substate)
    }

    private fun notifyConsumerBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        // Can go to DELETE_INFO, CONTACT_VENDORS, VERIFY_CONSUMER based on logic TBD
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.CORRECT_DATA)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            else -> null
        }
    }

    private fun correctDataBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
    }
}

class DeleteWorkflowHandler : WorkflowHandler() {
    init {
        registerForward(DataRequestState.VERIFY_CONSUMER, ::verifyConsumerForward)
        registerForward(DataRequestState.DELETE_INFO, ::deleteInfoForward)
        registerForward(DataRequestState.CONTACT_VENDORS, ::contactVendorsForward)
        registerForward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerForward)

        registerBackward(DataRequestState.DELETE_INFO, ::deleteInfoBackward)
        registerBackward(DataRequestState.CONTACT_VENDORS, ::contactVendorsBackward)
        registerBackward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerBackward)
    }

    private fun verifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.DELETE_INFO)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            else -> null
        }
    }

    private fun deleteInfoForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        // If we require a contact vendor step, then the next
        // step is to contact vendors, otherwise we go directly
        // to the notify consumer step.
        return if (!service.isContactVendorStepHidden(request)) {
            TransitionDataRequest(state = DataRequestState.CONTACT_VENDORS)
        } else {
            TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
        }
    }

    private fun contactVendorsForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
    }

    private fun notifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        val substate = when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> DataRequestSubstate.COMPLETED
            SubjectStatus.NOT_FOUND -> DataRequestSubstate.SUBJECT_NOT_FOUND
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> DataRequestSubstate.PROCESSED_AS_SERVICE_PROVIDER
            else -> null
        }

        return TransitionDataRequest(state = DataRequestState.CLOSED, substate = substate)
    }

    private fun deleteInfoBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
    }

    private fun contactVendorsBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.DELETE_INFO)
    }

    private fun notifyConsumerBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> {
                if (!service.isContactVendorStepHidden(request)) {
                    TransitionDataRequest(state = DataRequestState.CONTACT_VENDORS)
                } else {
                    TransitionDataRequest(state = DataRequestState.DELETE_INFO)
                }
            }
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            else -> null
        }
    }
}

class OptOutWorkflowHandler : WorkflowHandler() {
    init {
        registerForward(DataRequestState.REMOVE_CONSUMER, ::removeConsumerForward)
        registerForward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerForward)

        registerBackward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerBackward)
    }

    private fun removeConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
    }

    private fun notifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        val substate = when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> DataRequestSubstate.COMPLETED
            SubjectStatus.NOT_FOUND -> DataRequestSubstate.SUBJECT_NOT_FOUND
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> DataRequestSubstate.PROCESSED_AS_SERVICE_PROVIDER
            else -> DataRequestSubstate.COMPLETED
        }

        return TransitionDataRequest(state = DataRequestState.CLOSED, substate = substate)
    }

    private fun notifyConsumerBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.REMOVE_CONSUMER)
    }
}

class ObjectionWorkflowHandler : WorkflowHandler() {
    init {
        registerForward(DataRequestState.VERIFY_CONSUMER, ::verifyConsumerForward)
        registerForward(DataRequestState.EVALUATE_OBJECTION, ::evaluateObjectionForward)
        registerForward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerForward)

        registerBackward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerBackward)
        registerBackward(DataRequestState.EVALUATE_OBJECTION, ::evaluateObjectionBackward)
    }

    private fun verifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.EVALUATE_OBJECTION)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            else -> null
        }
    }

    private fun evaluateObjectionForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
    }

    private fun notifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        val substate = when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> DataRequestSubstate.COMPLETED
            SubjectStatus.NOT_FOUND -> DataRequestSubstate.SUBJECT_NOT_FOUND
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> DataRequestSubstate.PROCESSED_AS_SERVICE_PROVIDER
            else -> DataRequestSubstate.COMPLETED
        }

        return TransitionDataRequest(state = DataRequestState.CLOSED, substate = substate)
    }

    private fun notifyConsumerBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        // Can go to DELETE_INFO, CONTACT_VENDORS, VERIFY_CONSUMER based on logic TBD
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.EVALUATE_OBJECTION)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            else -> null
        }
    }

    private fun evaluateObjectionBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
    }
}

class WithdrawConsentWorkflowHandler : WorkflowHandler() {
    init {
        registerForward(DataRequestState.VERIFY_CONSUMER, ::verifyConsumerForward)
        registerForward(DataRequestState.REMOVE_CONSENT, ::removeConsentForward)
        registerForward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerForward)

        registerBackward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerBackward)
        registerBackward(DataRequestState.REMOVE_CONSENT, ::removeConsentBackward)
    }

    private fun verifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.REMOVE_CONSENT)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            else -> null
        }
    }

    private fun removeConsentForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
    }

    private fun notifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        val substate = when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> DataRequestSubstate.COMPLETED
            SubjectStatus.NOT_FOUND -> DataRequestSubstate.SUBJECT_NOT_FOUND
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> DataRequestSubstate.PROCESSED_AS_SERVICE_PROVIDER
            else -> DataRequestSubstate.COMPLETED
        }

        return TransitionDataRequest(state = DataRequestState.CLOSED, substate = substate)
    }

    private fun notifyConsumerBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        // Can go to DELETE_INFO, CONTACT_VENDORS, VERIFY_CONSUMER based on logic TBD
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.REMOVE_CONSENT)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            else -> null
        }
    }

    private fun contactVendorsBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.DELETE_INFO)
    }

    private fun removeConsentBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
    }
}

class LimitWorkflowHandler : WorkflowHandler() {
    init {
        registerForward(DataRequestState.VERIFY_CONSUMER, ::verifyConsumerForward)
        registerForward(DataRequestState.LIMIT_DATA, ::limitDataForward)
        registerForward(DataRequestState.CONTACT_VENDORS, ::contactVendorsForward)
        registerForward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerForward)

        registerBackward(DataRequestState.NOTIFY_CONSUMER, ::notifyConsumerBackward)
        registerBackward(DataRequestState.CONTACT_VENDORS, ::contactVendorsBackward)
        registerBackward(DataRequestState.LIMIT_DATA, ::limitDataBackward)
    }

    private fun verifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.LIMIT_DATA)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
            else -> null
        }
    }

    private fun limitDataForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.CONTACT_VENDORS)
    }

    private fun contactVendorsForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.NOTIFY_CONSUMER)
    }

    private fun notifyConsumerForward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        val substate = when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> DataRequestSubstate.COMPLETED
            SubjectStatus.NOT_FOUND -> DataRequestSubstate.SUBJECT_NOT_FOUND
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> DataRequestSubstate.PROCESSED_AS_SERVICE_PROVIDER
            else -> null
        }

        return TransitionDataRequest(state = DataRequestState.CLOSED, substate = substate)
    }

    private fun notifyConsumerBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return when (request.collectionGroupResult) {
            SubjectStatus.FOUND -> TransitionDataRequest(state = DataRequestState.CONTACT_VENDORS)
            SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            SubjectStatus.NOT_FOUND -> TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
            else -> null
        }
    }

    private fun contactVendorsBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.LIMIT_DATA)
    }

    private fun limitDataBackward(actor: User, organization: Organization, request: DataSubjectRequest, service: DataSubjectRequestService): TransitionDataRequest? {
        return TransitionDataRequest(state = DataRequestState.VERIFY_CONSUMER)
    }
}

class NullWorkflowHandler : WorkflowHandler()
