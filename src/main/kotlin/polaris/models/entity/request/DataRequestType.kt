package polaris.models.entity.request

import polaris.models.dto.request.DataRequestTypeDto

enum class Regulation(val isDueFromVerificationDate: Boolean, val initialDueDateDays: Long, val extendDueDateDays: Long) {
    GDPR(true, 30, 60),
    CCPA(false, 45, 45),
    VOLUNTARY(false, 45, 45),
    VCDPA(false, 45, 45),
    CPA(false, 45, 45),
    CTDPA(false, 45, 45),
}

enum class DataRequestType(
    val regulation: Regulation,
    val workflow: RequestWorkflow,
    val label: String,
    val shortLabel: String,

    val description: String,
    val declarationText: String? = null,
    val canSubjectSubmit: Boolean = true,
    val canPolarisSubmit: Boolean = true,
    val longLabel: String = label,
    val isDueFromVerificationDate: Boolean = regulation.isDueFromVerificationDate,
    val initialDueDateDays: Long = regulation.initialDueDateDays,
    val extendDueDateDays: Long = regulation.extendDueDateDays,
    val sendClosedNotVerifiedNotification: Boolean = false,
    val requireVerification: Boolean = true
) {
    GDPR_ACCESS(Regulation.GDPR, RequestWorkflow.KNOW, "Access Request", "Access", "Provide information stored on the subject", sendClosedNotVerifiedNotification = true),
    GDPR_ERASURE(Regulation.GDPR, RequestWorkflow.DELETE, "Erasure Request", "Erasure", "Erase information stored on the subject", sendClosedNotVerifiedNotification = true),
    GDPR_OBJECTION(Regulation.GDPR, RequestWorkflow.OBJECTION, "Objection to Processing", "Objection", "Object to processing of information about the subject", sendClosedNotVerifiedNotification = true, canPolarisSubmit = false),
    GDPR_RECTIFICATION(Regulation.GDPR, RequestWorkflow.RECTIFICATION, "Rectification Request", "Rectification", "Rectify information stored on the subject", sendClosedNotVerifiedNotification = true, canPolarisSubmit = false),
    GDPR_WITHDRAWAL(Regulation.GDPR, RequestWorkflow.WITHDRAW_CONSENT, "Withdrawal of Consent", "Withdrawal", "Withdrawl of consent for the subject", sendClosedNotVerifiedNotification = true, canPolarisSubmit = false),
    GDPR_PORTABILITY(Regulation.GDPR, RequestWorkflow.PORTABILITY, "Portability Request", "Portability", "Right to portability of data", sendClosedNotVerifiedNotification = true),

    CCPA_OPT_OUT(Regulation.CCPA, RequestWorkflow.OPT_OUT, "Request to Opt-Out", "Opt-Out", "Request to Opt Out of the sale of your information to third parties", canSubjectSubmit = false, initialDueDateDays = 15, extendDueDateDays = 0, requireVerification = false),
    CCPA_RIGHT_TO_KNOW_CATEGORIES(Regulation.CCPA, RequestWorkflow.KNOW, "Request to Know (Categories)", "Know", "Provide information stored on the subject at a categorical level, both for the types of data and providers", canSubjectSubmit = false, canPolarisSubmit = false),
    CCPA_RIGHT_TO_DELETE(Regulation.CCPA, RequestWorkflow.DELETE, "Request to Delete", "Delete", "Delete all non-essential information stored on the subject", declarationText = "By submitting a Request to Delete, you are asking us to permanently erase any personal information we have about you, including any information you have provided to us or that we have collected in order to provide a product or service to you. Please confirm you would like us to delete your personal information.", sendClosedNotVerifiedNotification = true),
    CCPA_RIGHT_TO_KNOW(Regulation.CCPA, RequestWorkflow.KNOW, "Request to Know", "Know", "Provide information stored on the subject at a specific level, both for the types of data and providers", declarationText = "I declare under penalty of perjury that the above information is true and correct and that I am the person whose personal information is the subject of this request.", sendClosedNotVerifiedNotification = true),
    CCPA_RIGHT_TO_LIMIT(Regulation.CCPA, RequestWorkflow.LIMIT, "Request to Limit", "Limit", "Desc", extendDueDateDays = 0, sendClosedNotVerifiedNotification = true, canSubjectSubmit = false, initialDueDateDays = 15, longLabel = "Request to Limit the Use of Sensitive Personal Information", requireVerification = false),
    CCPA_CORRECTION(Regulation.CCPA, RequestWorkflow.RECTIFICATION, "Request to Correct", "Correct", "Rectify information stored on the subject", sendClosedNotVerifiedNotification = true),

    VOLUNTARY_RIGHT_TO_KNOW(Regulation.VOLUNTARY, RequestWorkflow.KNOW, "Access Request", "Access", "Provide information stored on the subject at a specific level, both for the types of data and providers", declarationText = "I declare under penalty of perjury that the above information is true and correct and that I am the person whose personal information is the subject of this request.", sendClosedNotVerifiedNotification = true),
    VOLUNTARY_RIGHT_TO_DELETE(Regulation.VOLUNTARY, RequestWorkflow.DELETE, "Deletion Request", "Deletion", "Delete all non-essential information stored on the subject", declarationText = "By submitting a Request to Delete, you are asking us to permanently erase any personal information we have about you, including any information you have provided to us or that we have collected in order to provide a product or service to you. Please confirm you would like us to delete your personal information.", sendClosedNotVerifiedNotification = true),

    VCDPA_ACCESS(Regulation.VCDPA, RequestWorkflow.KNOW, "Access Request", "Access", "Provide information stored on the subject at a specific level, both for the types of data and providers", declarationText = "I declare under penalty of perjury that the above information is true and correct and that I am the person whose personal information is the subject of this request.", sendClosedNotVerifiedNotification = true),
    VCDPA_DELETION(Regulation.VCDPA, RequestWorkflow.DELETE, "Deletion Request", "Deletion", "Delete all non-essential information stored on the subject", declarationText = "By submitting a Request to Delete, you are asking us to permanently erase any personal information we have about you, including any information you have provided to us or that we have collected in order to provide a product or service to you. Please confirm you would like us to delete your personal information.", sendClosedNotVerifiedNotification = true),
    VCDPA_CORRECTION(Regulation.VCDPA, RequestWorkflow.RECTIFICATION, "Request to Correct", "Correct", "Rectify information stored on the subject", sendClosedNotVerifiedNotification = true),
    VCDPA_APPEAL(Regulation.VCDPA, RequestWorkflow.NONE, "Appeal a Decision", "Appeal", "Appeal a previous decision", sendClosedNotVerifiedNotification = true),
    VCDPA_OPT_OUT(Regulation.VCDPA, RequestWorkflow.OPT_OUT, "Request to Opt-Out", "Opt-Out", "Request to Opt Out of the sale of your information to third parties", canSubjectSubmit = false, requireVerification = false),

    CPA_ACCESS(Regulation.CPA, RequestWorkflow.KNOW, "Access Request", "Access", "Provide information stored on the subject at a specific level, both for the types of data and providers", declarationText = "I declare under penalty of perjury that the above information is true and correct and that I am the person whose personal information is the subject of this request.", sendClosedNotVerifiedNotification = true),
    CPA_DELETION(Regulation.CPA, RequestWorkflow.DELETE, "Deletion Request", "Deletion", "Delete all non-essential information stored on the subject", declarationText = "By submitting a Request to Delete, you are asking us to permanently erase any personal information we have about you, including any information you have provided to us or that we have collected in order to provide a product or service to you. Please confirm you would like us to delete your personal information.", sendClosedNotVerifiedNotification = true),
    CPA_CORRECTION(Regulation.CPA, RequestWorkflow.RECTIFICATION, "Request to Correct", "Correct", "Rectify information stored on the subject", sendClosedNotVerifiedNotification = true),
    CPA_APPEAL(Regulation.CPA, RequestWorkflow.NONE, "Appeal a Decision", "Appeal", "Appeal a previous decision", sendClosedNotVerifiedNotification = true),
    CPA_OPT_OUT(Regulation.CPA, RequestWorkflow.OPT_OUT, "Request to Opt-Out", "Opt-Out", "Request to Opt Out of the sale of your information to third parties", canSubjectSubmit = false, requireVerification = false),

    CTDPA_ACCESS(Regulation.CTDPA, RequestWorkflow.KNOW, "Access Request", "Access", "Provide information stored on the subject at a specific level, both for the types of data and providers", declarationText = "I declare under penalty of perjury that the above information is true and correct and that I am the person whose personal information is the subject of this request.", sendClosedNotVerifiedNotification = true),
    CTDPA_DELETION(Regulation.CTDPA, RequestWorkflow.DELETE, "Deletion Request", "Deletion", "Delete all non-essential information stored on the subject", declarationText = "By submitting a Request to Delete, you are asking us to permanently erase any personal information we have about you, including any information you have provided to us or that we have collected in order to provide a product or service to you. Please confirm you would like us to delete your personal information.", sendClosedNotVerifiedNotification = true),
    CTDPA_CORRECTION(Regulation.CTDPA, RequestWorkflow.RECTIFICATION, "Request to Correct", "Correct", "Rectify information stored on the subject", sendClosedNotVerifiedNotification = true),
    CTDPA_APPEAL(Regulation.CTDPA, RequestWorkflow.NONE, "Appeal a Decision", "Appeal", "Appeal a previous decision", sendClosedNotVerifiedNotification = true),
    CTDPA_OPT_OUT(Regulation.CTDPA, RequestWorkflow.OPT_OUT, "Request to Opt-Out", "Opt-Out", "Request to Opt Out of the sale of your information to third parties", canSubjectSubmit = false, requireVerification = false),

    ;

    fun isDelete(): Boolean = workflow == RequestWorkflow.DELETE
    fun isKnow(): Boolean = workflow == RequestWorkflow.KNOW
    fun isGdpr(): Boolean = regulation == Regulation.GDPR

    fun regulationSlug() = regulation.toString()
    fun regulationName() = regulation.toString()

    fun toDto(): DataRequestTypeDto {
        return DataRequestTypeDto(
            type = this,
            shortName = this.shortLabel,
            name = this.label,
            longName = this.longLabel,
            description = this.description,
            regulationName = this.regulationName(),
            workflow = this.workflow,
            requireVerification = this.requireVerification,
            canExtend = this.extendDueDateDays > 0,
            declarationText = this.declarationText
        )
    }

    fun requestInstructionType(): DataRequestInstructionType? {
        return when (workflow) {
            RequestWorkflow.KNOW -> DataRequestInstructionType.KNOW
            RequestWorkflow.DELETE -> DataRequestInstructionType.DELETE
            RequestWorkflow.OPT_OUT -> DataRequestInstructionType.OPT_OUT
            else -> null
        }
    }
}
