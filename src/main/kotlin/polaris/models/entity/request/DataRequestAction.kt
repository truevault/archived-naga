package polaris.models.entity.request

enum class DataRequestAction {
    PROCESSING_NEEDED,
    PROCESSING_NOT_NEEDED
}
