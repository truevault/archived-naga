package polaris.models.entity.organization

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import polaris.models.OrganizationPublicId
import polaris.models.dto.SurveySlug
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.feature.Features
import polaris.models.entity.feature.OrganizationFeature
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.regulation.Regulation
import polaris.models.entity.user.UserStatus
import polaris.services.support.ArrayConverter
import polaris.util.IdGenerator
import polaris.util.converters.AutocreatedCollectionGroupSlugListCon
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.AttributeConverter
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.OneToMany
import javax.persistence.OneToOne

@Entity
@Where(clause = "deleted_at is null")
@SQLDelete(sql = "UPDATE organization SET deleted_at = NOW() WHERE id=?")
data class Organization(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    val publicId: OrganizationPublicId = IdGenerator.generate(),
    var name: String = "",
    var logoUrl: String? = "",
    var faviconUrl: String? = "",
    var identityVerificationMessage: String = "",

    @OneToMany(mappedBy = "organization")
    val organizationUsers: List<OrganizationUser> = emptyList(),

    @OneToMany(mappedBy = "organization")
    val organizationDataRecipients: List<OrganizationDataRecipient> = emptyList(),

    @OneToMany(mappedBy = "organization")
    val collectionGroups: List<CollectionGroup> = emptyList(),

    @OneToMany(mappedBy = "organization")
    val privacyCenters: List<PrivacyCenter> = emptyList(),

    @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.MERGE])
    @JoinColumn(name = "current_draft_id")
    var currentDraft: OrganizationDraft? = null,

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JoinTable(
        name = "organization_regulation",
        joinColumns = [JoinColumn(name = "organization_id")],
        inverseJoinColumns = [JoinColumn(name = "regulation_id")]
    )
    var regulations: List<Regulation> = emptyList(),

    @OneToMany(mappedBy = "organization")
    val regulationsSettings: List<OrganizationRegulation> = emptyList(),

    @Convert(converter = AutocreatedCollectionGroupSlugListCon::class)
    var autocreatedCollectionGroupSlugs: List<AutocreatedCollectionGroupSlug> = emptyList(),

    @OneToMany(mappedBy = "organization")
    var organizationFeatures: List<OrganizationFeature> = emptyList(),

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JoinTable(
        name = "organization_vendor_client",
        joinColumns = [JoinColumn(name = "vendor_id")],
        inverseJoinColumns = [JoinColumn(name = "client_id")]
    )
    @Where(clause = "deleted_at is null")
    var clientOrganizations: List<Organization> = emptyList(),

    @ManyToMany(mappedBy = "clientOrganizations")
    @Where(clause = "deleted_at is null")
    val vendorOrganizations: List<Organization> = emptyList(),

    @Enumerated(EnumType.STRING)
    var sendMethod: SendMethod = SendMethod.NYLAS,

    @Enumerated(EnumType.STRING)
    var complianceVersion: OrgComplianceVersion? = null,

    var privacyPolicyUrl: String? = null,
    var requestTollFreePhoneNumber: String? = null,
    var requestFormUrl: String? = null,

    var headerImageUrl: String = "",
    var physicalAddress: String = "",
    var messageSignature: String = "",

    var employeePrivacyNoticeCustom: String? = null,
    var employeePrivacyNoticeRetentionPeriod: String? = null,

    var requestProcessingInstructionsCollectionGroup: String? = null,
    var requestProcessingInstructionsOptOut: String? = null,

    @Enumerated(EnumType.STRING)
    var caPrivacyNoticeProgress: PrivacyNoticeProgress = PrivacyNoticeProgress.NEEDS_REVIEW,
    @Enumerated(EnumType.STRING)
    var optOutPrivacyNoticeProgress: PrivacyNoticeProgress = PrivacyNoticeProgress.NEEDS_REVIEW,

    @Convert(converter = GetCompliantConverter::class)
    var getCompliantInvalidatedProgressSteps: List<String> = emptyList(),

    @Enumerated(EnumType.STRING)
    var exceptionsMappingProgress: MappingProgressEnum = MappingProgressEnum.NOT_STARTED,

    @Enumerated(EnumType.STRING)
    var cookiesMappingProgress: MappingProgressEnum = MappingProgressEnum.NOT_STARTED,

    @OneToMany(mappedBy = "organization")
    val organizationMailboxes: List<OrganizationMailbox> = emptyList(),

    val createdAt: ZonedDateTime = ZonedDateTime.now(),

    var isServiceProvider: Boolean? = null,

    var trainingComplete: Boolean? = false,
    var walkthroughEmailSent: Boolean? = false,

    var cookiePolicy: String? = "We use cookies to create a better experience for you on our site. For example, cookies prevent you from having to login repeatedly, and they help us remember items you've added to your cart. We also use third-party cookies, which are cookies placed by third parties for advertising and analytics purposes. You can control these cookies through your browser settings.",

    var customFieldEnabled: Boolean = false,
    var customFieldLabel: String? = "",
    var customFieldHelp: String? = "",

    var financialIncentiveDetails: String? = null,

    var gaWebProperty: String? = null,

    val devInstructionsUuid: UUID = UUID.randomUUID(),
    val hrInstructionsUuid: UUID = UUID.randomUUID(),

    // These are needed to track when a task update is needed for the SellingOrSellingOrgChangeTask
    // When isSelling or isSharing changes from false to true, it will create a SellingOrSellingOrgChangeTask (if not already existing)
    // When isSelling or isSharing changes from true to false, it will create a SellingOrSellingOrgChangeTask unless
    // both are now false in which case it will create a NoLongerSharingOrSellingTask
    var isSellingLastState: Boolean = false,
    var isSharingLastState: Boolean = false,

    @Enumerated(EnumType.STRING)
    var currentSurvey: SurveySlug? = null,

    var deletedAt: ZonedDateTime? = null
) {
    override fun toString() = "Organization(id=$id, publicId=$publicId)"
    fun getFullHeaderImageUrl() =
        if (headerImageUrl.isNotBlank() && !headerImageUrl.startsWith("http")) "https://$headerImageUrl" else headerImageUrl

    fun privacyEmailAddress() = organizationMailboxes.firstOrNull { it.status == MailboxStatus.CONNECTED }?.mailbox
    fun anyPrivacyEmailAddress() = organizationMailboxes.firstOrNull()?.mailbox

    fun activeUsers() = organizationUsers.filter { u -> u.status == UserStatus.ACTIVE }

    fun privacyCenter() = privacyCenters.firstOrNull()

    fun isSetupPhase() = organizationFeatures.firstOrNull { it.feature!!.name == Features.SETUP_PHASE }?.enabled == true
    fun isGetCompliantPhase() = organizationFeatures.firstOrNull { it.feature!!.name == Features.GET_COMPLIANT_PHASE }?.enabled == true
    fun isGdpr() = organizationFeatures.firstOrNull { it.feature!!.name == Features.GDPR }?.enabled == true
    fun isMultistate() = organizationFeatures.firstOrNull { it.feature!!.name == Features.MULTISTATE }?.enabled == true

    fun getPhase(): Phase {
        if (isSetupPhase()) return Phase.SETUP
        if (isGetCompliantPhase()) return Phase.GET_COMPLIANT
        return Phase.STAY_COMPLIANT
    }

    fun getDefaultSignature(): String = "$name Privacy Team\n${privacyCenter()?.getBaseUrl() ?: ""}"

    fun getSignature(): String {
        if (messageSignature != "") {
            return messageSignature
        }

        return getDefaultSignature()
    }
}

class GetCompliantConverter : AttributeConverter<List<String>, String> {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(ArrayConverter::class.java)
    }

    override fun convertToDatabaseColumn(attribute: List<String>?): String {
        val result = try {
            jacksonObjectMapper().writeValueAsString(attribute)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (todb): ", e)
            "{}"
        }
        return result
    }

    override fun convertToEntityAttribute(dbData: String?): List<String> {
        if (dbData == null) { return emptyList() }
        val result: List<String> = try {
            jacksonObjectMapper().readValue(dbData)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (toentity): ", e)
            emptyList()
        }
        return result
    }
}

enum class OrgComplianceVersion(val nextSurvey: SurveySlug?) {
    c2022_1(SurveySlug.s2023_1),
    c2023_1(SurveySlug.s2023_3),
    c2023_3(null)
}
