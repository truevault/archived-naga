package polaris.models.entity.organization

import org.hibernate.annotations.Where
import polaris.models.entity.compliance.PersonalInformationCategory
import polaris.models.listeners.CollectionGroupCollectedPICEntityListener
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@EntityListeners(CollectionGroupCollectedPICEntityListener::class)
@Where(clause = "deleted_at is null")
@Table(name = "collection_group_collected_pic")
data class CollectionGroupCollectedPIC(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "collection_group_id", updatable = false)
    val collectionGroup: CollectionGroup? = null,

    @ManyToOne
    @JoinColumn(name = "pic_id", updatable = false)
    val pic: PersonalInformationCategory? = null,

    @OneToMany(mappedBy = "collectedPIC")
    val disclosedToVendors: List<DataRecipientDisclosedPIC> = emptyList(),

    @OneToMany(mappedBy = "collectedPIC")
    val receivedFromSources: List<DataSourceReceivedPIC> = emptyList(),

    var deletedAt: ZonedDateTime? = null
) {
    override fun toString(): String {
        return "OrganizationDataSubjectTypePersonalInformationCategory" +
            "(dataSubjectType=$collectionGroup, personalInformationCategory=$pic)"
    }
}
