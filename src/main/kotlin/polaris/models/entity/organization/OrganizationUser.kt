package polaris.models.entity.organization

import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import polaris.models.dto.toIso
import polaris.models.dto.user.OrganizationUserDetailsDto
import polaris.models.dto.user.OrganizationUserDto
import polaris.models.entity.user.User
import polaris.models.entity.user.UserRole
import polaris.models.entity.user.UserStatus
import javax.persistence.*

@Entity
@Where(clause = "deleted_at is null")
@SQLDelete(sql = "UPDATE organization_user SET deleted_at = NOW() WHERE id=?")
data class OrganizationUser(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false)
    val user: User? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    // Note: When new roles are added in the future, this
    // should change to a less permissive default role.
    @Enumerated(EnumType.STRING)
    val role: UserRole = UserRole.ADMIN,

    @Enumerated(EnumType.STRING)
    var status: UserStatus = UserStatus.INACTIVE
) {
    fun toDto() = OrganizationUserDto(
        firstName = user!!.firstName,
        lastName = user.lastName,
        email = user.email,
        lastLogin = user.lastLogin?.toIso(),
        role = role,
        status = status,
        organization = organization!!.publicId,
        organizationName = organization.name
    )

    fun toDetailsDto() = OrganizationUserDetailsDto(
        role = role,
        status = status,
        organization = organization!!.publicId,
        organizationName = organization.name,
        faviconUrl = organization.faviconUrl
    )

    override fun toString(): String {
        return "OrganizationUser(id=$id, role=$role, status=$status)"
    }
}
