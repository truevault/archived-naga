package polaris.models.entity.organization

import org.hibernate.Hibernate
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@IdClass(OrganizationProgressId::class)
data class OrganizationProgress(
    @Id
    val organizationId: UUID? = null,

    @Id
    val progressKey: String? = "",

    @Enumerated(EnumType.STRING)
    var progressValue: MappingProgressEnum = MappingProgressEnum.NOT_STARTED
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as OrganizationProgress

        return organizationId == other.organizationId &&
            progressKey == other.progressKey &&
            progressValue == other.progressValue
    }

    override fun hashCode(): Int = Objects.hash(organizationId, progressKey, progressValue)

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(organizationId = $organizationId , progressKey = $progressKey , progressValue = $progressValue )"
    }
}

data class OrganizationProgressId(
    val organizationId: UUID? = null,
    val progressKey: String? = null
) : Serializable
