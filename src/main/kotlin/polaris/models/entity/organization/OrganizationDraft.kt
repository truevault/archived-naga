package polaris.models.entity.organization

import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.Where
import polaris.models.dto.orgDraftSchema.v1.OrgDraftSchemaV1
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
@Where(clause = "deleted_at is null")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType::class)
class OrganizationDraft(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization,

    @Enumerated(EnumType.STRING)
    var schemaVersion: OrgDraftSchemaVersion,

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    var data: OrgDraftSchemaV1,

    val createdAt: ZonedDateTime = ZonedDateTime.now(),
    val lastUpdatedAt: ZonedDateTime? = null,
    var deletedAt: ZonedDateTime? = null
)

enum class OrgDraftSchemaVersion {
    V1,
    UNKNOWN
}

enum class OrgDraftComplianceVersion {
    OTHER
}
