package polaris.models.entity.organization

enum class OrganizationVendorStatus(val label: String) {
    IN_PROGRESS("In-Progress"),
    COMPLETED("Completed")
}
