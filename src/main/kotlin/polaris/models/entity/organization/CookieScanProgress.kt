package polaris.models.entity.organization

enum class CookieScanProgress {
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETE,
    RESET,
    ERROR,
}
