package polaris.models.entity.organization

import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "privacy_email")
data class PrivacyEmail(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    var organization: Organization,

    val subject: String,
    val createdPrivacyRequest: Boolean = false,

    var receivedAt: ZonedDateTime
)
