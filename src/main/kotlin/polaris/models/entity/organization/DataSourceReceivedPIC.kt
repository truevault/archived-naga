package polaris.models.entity.organization

import polaris.models.entity.vendor.Vendor
import javax.persistence.*

@Entity
@Table(name = "data_source_received_pic")
data class DataSourceReceivedPIC(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "collected_pic_id", updatable = false)
    val collectedPIC: CollectionGroupCollectedPIC? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_id", updatable = false)
    val vendor: Vendor? = null
) {
    override fun toString(): String {
        return "DataSourceReceivedPIC(id=$id)"
    }
}
