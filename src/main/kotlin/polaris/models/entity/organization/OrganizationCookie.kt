package polaris.models.entity.organization

import polaris.util.converters.CookieCategoryListCon
import java.io.Serializable
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Embeddable
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.IdClass
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne

@Entity
@IdClass(OrganizationCookieId::class)
class OrganizationCookie(
    @Id
    @Column(name = "organization_id", updatable = false)
    val organizationId: UUID? = null,

    @Id
    val name: String? = null,

    @Id
    val domain: String? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", insertable = false, updatable = false)
    val organization: Organization? = null,

    val firstScannedAt: ZonedDateTime? = null,
    var lastScannedAt: ZonedDateTime? = null,

    var lastScannedUrl: String? = null,
    var lastScannedValue: String? = null,
    var lastScannedPath: String? = null,
    var lastScannedExpires: String? = null,
    var lastScannedHttpOnly: Boolean? = null,
    var lastScannedSecure: Boolean? = null,
    var lastScannedSession: Boolean? = null,
    var lastScannedSameSite: String? = null,
    var lastScannedPriority: String? = null,
    var lastScannedSameParty: Boolean? = null,

    @Convert(converter = CookieCategoryListCon::class)
    var categories: List<CookieCategory> = emptyList(),

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JoinTable(
        name = "organization_cookie_data_recipients",
        joinColumns = [JoinColumn(name = "name", referencedColumnName = "name"), JoinColumn(name = "domain", referencedColumnName = "domain"), JoinColumn(name = "organizationId", columnDefinition = "UUID", referencedColumnName = "organization_id")],
        inverseJoinColumns = [JoinColumn(name = "data_recipient_id")]
    )
    var dataRecipients: MutableSet<OrganizationDataRecipient> = mutableSetOf(),

    val isSnoozed: Boolean = false,
    var isHidden: Boolean = false,
    var isFirstParty: Boolean = false
)

enum class CookieCategory {
    ANALYTICS,
    FUNCTIONALITY,
    ADVERTISING,
    PERSONALIZATION,
    SECURITY
}

@Embeddable
data class OrganizationCookieId(
    val organizationId: UUID? = null,
    val name: String? = null,
    val domain: String? = null
) : Serializable
