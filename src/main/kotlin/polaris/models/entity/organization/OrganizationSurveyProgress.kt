package polaris.models.entity.organization

import polaris.models.dto.SurveySlug
import java.io.Serializable
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.IdClass
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
@IdClass(OrganizationSurveyProgressId::class)
data class OrganizationSurveyProgress(
    @Id
    @Column(name = "organization_id", updatable = false)
    val organizationId: UUID? = null,

    @Id
    @Enumerated(EnumType.STRING)
    val surveySlug: SurveySlug? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", insertable = false, updatable = false)
    val organization: Organization? = null,

    var currentStep: String? = null
)

@Embeddable
data class OrganizationSurveyProgressId(
    val organizationId: UUID? = null,
    val surveySlug: SurveySlug? = null
) : Serializable
