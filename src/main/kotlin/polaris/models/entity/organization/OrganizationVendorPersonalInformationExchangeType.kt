package polaris.models.entity.organization

enum class OrganizationVendorPersonalInformationExchangeType {
    DISCLOSURE,
    SALE
}
