package polaris.models.entity.organization

import org.hibernate.annotations.Where
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.vendor.ProcessingRegion
import polaris.models.listeners.CollectionGroupEntityListener
import polaris.util.converters.ProcessingRegionListCon
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

@Entity
@Where(clause = "deleted_at is null")
@EntityListeners(CollectionGroupEntityListener::class)
data class CollectionGroup(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    var name: String = "",
    var description: String? = "",
    var enabled: Boolean = true,

    var gdprEeaUkDataCollection: Boolean? = null,

    @Enumerated(EnumType.STRING)
    var collectionGroupType: CollectionGroupType = CollectionGroupType.BUSINESS_TO_CONSUMER,

    @Enumerated(EnumType.STRING)
    var autocreationSlug: AutocreatedCollectionGroupSlug? = null,

    @OneToMany(mappedBy = "collectionGroup")
    val mappingProgress: List<CollectionGroupMappingProgress> = emptyList(),

    @OneToMany(mappedBy = "collectionGroup")
    val collectedPIC: List<CollectionGroupCollectedPIC> = emptyList(),

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JoinTable(
        name = "data_subject_request_collection_groups",
        joinColumns = [JoinColumn(name = "collection_group_id")],
        inverseJoinColumns = [JoinColumn(name = "data_subject_request_id")]
    )
    var dataSubjectRequests: List<DataSubjectRequest> = emptyList(),

    @Enumerated(EnumType.STRING)
    var privacyNoticeProgress: PrivacyNoticeProgress = PrivacyNoticeProgress.NEEDS_REVIEW,

    @Convert(converter = ProcessingRegionListCon::class)
    var processingRegions: List<ProcessingRegion> = listOf(ProcessingRegion.UNITED_STATES),

    var privacyNoticeIntroText: String? = null,

    val createdAsDataSubject: Boolean = false,

    var deletedAt: ZonedDateTime? = null
) {
    fun toDto() = name

    override fun toString(): String {
        return "CollectionGroup(id=$id, type=$name, description=$description)"
    }
}
