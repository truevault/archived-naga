package polaris.models.entity.organization

import org.hibernate.annotations.Where
import polaris.models.entity.compliance.PersonalInformationType
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Where(clause = "deleted_at is null")
data class CollectionGroupMappingProgress(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "collection_group_id", updatable = false)
    val collectionGroup: CollectionGroup? = null,

    @Enumerated(EnumType.STRING)
    val personalInformationType: PersonalInformationType? = null,

    @Enumerated(EnumType.STRING)
    var mappingProgress: MappingProgressEnum = MappingProgressEnum.NOT_STARTED,

    var deletedAt: ZonedDateTime? = null
)
