package polaris.models.entity.organization

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import polaris.models.dto.organization.OrganizationSurveyQuestionDto
import javax.persistence.*

const val BUSINESS_SURVEY_NAME = "business-survey"
const val SELLING_AND_SHARING_SURVEY_NAME = "selling-and-sharing-survey"
const val HR_SURVEY_NAME = "hr-survey"
const val EEA_UK_SURVEY_NAME = "eea-uk-survey"
const val DATA_RETENTION_SURVEY_NAME = "data-retention-survey"
const val GDPR_LAWFUL_BASES_SURVEY_NAME = "gdpr-lawful-bases-survey"
const val SHARING_SELLING_SURVEY_NAME = "selling-and-sharing-survey"
const val CONSUMER_SOURCES_SURVEY_NAME = "consumer-sources-survey"
const val EMPLOYEE_SOURCES_SURVEY_NAME = "employee-sources-survey"
const val WEBSITE_AUDIT_SURVEY_NAME = "website-audit-survey"
const val CORE_PRIVACY_CONCEPTS_SURVEY_NAME = "core-privacy-concepts-survey"

const val REMINDER_SURVEY_NAME = "reminder-survey"

const val FINANCIAL_INCENTIVES_SURVEY_NAME = "consumer-collection-incentives-survey"

const val CONSUMER_DATA_COLLECTION_SURVEY_PRFEIX = "consumer-data-collection-"

const val EMPLOYEE_COLLECTION_PURPOSES_SURVEY = "emp-collection-purposes"

const val MIDYEAR_CATCHUP_SURVEY = "2023q3-survey"

@Entity
data class OrganizationSurveyQuestion(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    val surveyName: String = "",
    val slug: String = "",
    var question: String = "",
    var answer: String? = null
) {
    companion object {
        fun parseAnswerList(answer: String): List<String> {
            if (answer.isBlank()) {
                return emptyList()
            }

            return jacksonObjectMapper().readValue(answer, object : TypeReference<List<String>>() {})
        }

        /**
         * If answer is a valid JSON array of strings that contains "none" as an element,
         *   then ensures the array either only contains "none" OR removes "none" if there are other non-"none" elements.
         * Otherwise it returns the answer unchanged (even if it the JSON may be invalid)
         */
        fun sanitizeAnswerList(answer: String?): String? {
            if (answer?.startsWith("[") != true || !answer?.contains("\"none\"") || answer == "[\"none\"]")
                return answer

            val entries = parseAnswerList(answer).toSet().toList()
            val newAnswer = if (entries.size == 1) entries else entries.filter { a -> a != "none" }

            return jacksonObjectMapper().writeValueAsString(newAnswer)
        }
    }

    fun toDto() = OrganizationSurveyQuestionDto(
        organizationId = organization!!.publicId,
        surveyName = surveyName,
        question = question,
        slug = slug,
        answer = trimmedAnswer()
    )

    fun isTrue() = trimmedAnswer() == "true"

    fun getCompositeKey() = "$surveyName/$slug"

    fun parsedAnswerList(): List<String> = parseAnswerList(trimmedAnswer())

    private fun trimmedAnswer() = answer?.trim() ?: ""
}

fun List<OrganizationSurveyQuestion>.get(slug: String): OrganizationSurveyQuestion? {
    return this.find { it.slug == slug }
}

fun List<OrganizationSurveyQuestion>.hasAnswer(slug: String): Boolean {
    return this.get(slug) != null
}

fun List<OrganizationSurveyQuestion>.hasAnswer(slug: String, answer: String): Boolean {
    return this.get(slug)?.answer == answer
}

fun List<OrganizationSurveyQuestion>.hasTrueAnswer(slug: String): Boolean {
    return this.hasAnswer(slug, "true")
}

fun List<OrganizationSurveyQuestion>.hasFalseAnswer(slug: String): Boolean {
    return this.hasAnswer(slug, "false")
}
