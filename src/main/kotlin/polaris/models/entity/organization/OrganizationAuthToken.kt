package polaris.models.entity.organization

import java.io.Serializable
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.IdClass
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
@IdClass(OrganizationAuthTokenId::class)
class OrganizationAuthToken(
    @Id
    @Column(name = "organization_id", updatable = false)
    val organizationId: UUID? = null,

    @Id
    @Enumerated(EnumType.STRING)
    val provider: OrganizationAuthTokenProvider? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", insertable = false, updatable = false)
    val organization: Organization? = null,

    var auth: String? = null,
    var refresh: String? = null,

    var createdAt: ZonedDateTime = ZonedDateTime.now(),
    var updatedAt: ZonedDateTime = ZonedDateTime.now(),
    var expiresAt: ZonedDateTime? = null
)

enum class OrganizationAuthTokenProvider {
    GOOGLE_OAUTH,
}

@Embeddable
data class OrganizationAuthTokenId(
    val organizationId: UUID? = null,
    val provider: OrganizationAuthTokenProvider? = null
) : Serializable
