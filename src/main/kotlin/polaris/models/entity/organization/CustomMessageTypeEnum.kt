package polaris.models.entity.organization
import polaris.models.entity.CustomMessageTypeNotFound
import java.util.*

const val CLOSED_NOT_FOUND_DESCRIPTION = "You can't match the consumer's data to anyone in your records"
const val CLOSED_NOT_FOUND_TITLE = "No Record of Consumer"
const val CLOSED_NOT_FOUND_INSTRUCTIONS = "The message below is sent to the consumer when you can’t find their data in your records. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_SERVICE_PROVIDER_DESCRIPTION = "You can't process the request because you act as a service provider"
const val CLOSED_SERVICE_PROVIDER_TITLE = "Processed as a Service Provider"
const val CLOSED_SERVICE_PROVIDER_INSTRUCTIONS = "The message below is sent to the user when they are found but you are acting in your capacity as a service provider."

const val CLOSED_DELETE_NOT_VERIFIED_DESCRIPTION = "The consumer's Request to Delete was closed because they never clicked the email verification link"
const val CLOSED_DELETE_NOT_VERIFIED_TITLE = "Request to Delete Closed as Unverified"
const val CLOSED_DELETE_NOT_VERIFIED_INSTRUCTIONS = "The message below is sent to the consumer when their Request to Delete was closed because they did not click the email verification link. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_KNOW_NOT_VERIFIED_DESCRIPTION = "The consumer’s Request to Know was closed because they never clicked the email verification link"
const val CLOSED_KNOW_NOT_VERIFIED_TITLE = "Request to Know Closed as Unverified"
const val CLOSED_KNOW_NOT_VERIFIED_INSTRUCTIONS = "The message below is sent to the consumer when their Request to Know was closed because they did not click the email verification link. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_DELETE_DESCRIPTION = "You’ve processed the consumer’s Request to Delete"
const val CLOSED_DELETE_TITLE = "Request to Delete Processed"
const val CLOSED_DELETE_INSTRUCTIONS = "The message below is sent to the consumer when you’ve deleted their personal data. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_KNOW_DESCRIPTION = "You’ve processed the consumer’s Request to Know"
const val CLOSED_KNOW_TITLE = "Request to Know Processed"
const val CLOSED_KNOW_INSTRUCTIONS = "The message below is sent to the consumer when you’ve retreived their personal data. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_PORTABILITY_DESCRIPTION = "You’ve processed the consumer’s Portability Request"
const val CLOSED_PORTABILITY_TITLE = "Portability Request Processed"
const val CLOSED_PORTABILITY_INSTRUCTIONS = "The message below is sent to the consumer when you’ve processed their portability request. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_LIMIT_DESCRIPTION = "You’ve processed the consumer’s Request to Limit Sensitive Personal Information"
const val CLOSED_LIMIT_TITLE = "Request to Limit Sensitive Personal Information Processed"
const val CLOSED_LIMIT_INSTRUCTIONS = "The message below is sent to the consumer when you’ve processed their request to limit sensitive personal information. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_RECTIFICATION_DESCRIPTION = "You’ve processed the consumer’s Rectification Request"
const val CLOSED_RECTIFICATION_TITLE = "Rectification Request Processed"
const val CLOSED_RECTIFICATION_INSTRUCTIONS = "The message below is sent to the consumer when you’ve processed their rectification request. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_WITHDRAWN_CONSENT_DESCRIPTION = "You’ve processed the consumer’s Request to Withdraw Consent"
const val CLOSED_WITHDRAWN_CONSENT_TITLE = "Request to Withdraw Consent Processed"
const val CLOSED_WITHDRAWN_CONSENT_INSTRUCTIONS = "The message below is sent to the consumer when you’ve processed their request to withdraw consent. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_OBJECTION_DESCRIPTION = "You’ve processed the consumer’s Objection to Processing"
const val CLOSED_OBJECTION_TITLE = "Objection to Processing Processed"
const val CLOSED_OBJECTION_INSTRUCTIONS = "The message below is sent to the consumer when you’ve processed their objection to processing. If you’d like to add your own language to the message, you may do so below."

const val CLOSED_OPT_OUT_DESCRIPTION = "You’ve processed the consumer’s Request to Opt-Out"
const val CLOSED_OPT_OUT_TITLE = "Request to Opt-Out Processed"
const val CLOSED_OPT_OUT_INSTRUCTIONS = "The message below is sent to the consumer when you’ve opted them out of “selling.” If you’d like to add your own language to the message, you may do so below."

val sharedParams = mapOf(
    "firstName" to "{first}",
    "lastName" to "{last}",
    "fullName" to "{full name}",
    "subjectEmailAddress" to "consumer@example.com",
    // "type" to EACH NEEDS MERGING,
    "signature" to "Acme Co. Data Privacy Team<br/><br/>https://privacy.acmeco.com/privacy-policy",
    "requestFormUrl" to "<Link to Privacy Center Request Form>",
    "requestEmail" to "<privacy@example.com>",
    "isGdpr" to false,
    "optOutLabel" to "Do Not Share",
    "optOutUrl" to "https://xxx-stay-compliant-test-org.truevaultprivacycenter.com/do-not-share",
    "customText" to "[[[CUSTOM_TEXT]]]",
    "regulationName" to "CCPA",
    "dueDateDays" to 45,
    "extensionDays" to 45
)

enum class CustomMessageTypeEnum(
    val slug: String,
    val title: String,
    val description: String,
    val instructions: String,
    val sortKey: Int,
    val template: String,
    val previewParams: Map<String, Any>
) {
    ClosedNotFound(
        "closed-not-found",
        CLOSED_NOT_FOUND_TITLE,
        CLOSED_NOT_FOUND_DESCRIPTION,
        CLOSED_NOT_FOUND_INSTRUCTIONS,
        1,
        "email/subject-request/closed-not-found.ftlh",
        sharedParams.plus("type" to "Privacy Request")
    ),

    ClosedServiceProvider(
        "closed-service-provider",
        CLOSED_SERVICE_PROVIDER_TITLE,
        CLOSED_SERVICE_PROVIDER_DESCRIPTION,
        CLOSED_SERVICE_PROVIDER_INSTRUCTIONS,
        2,
        "email/subject-request/closed-service-provider.ftl",
        sharedParams.plus("type" to "Privacy Request")
    ),

    ClosedDeleteNotVerified(
        "closed-delete-not-verified",
        CLOSED_DELETE_NOT_VERIFIED_TITLE,
        CLOSED_DELETE_NOT_VERIFIED_DESCRIPTION,
        CLOSED_DELETE_NOT_VERIFIED_INSTRUCTIONS,
        3,
        "email/subject-request/closed-delete-not-verified.ftlh",
        sharedParams.plus("type" to "Request to Delete")
    ),
    ClosedKnowNotVerified(
        "closed-know-not-verified",
        CLOSED_KNOW_NOT_VERIFIED_TITLE,
        CLOSED_KNOW_NOT_VERIFIED_DESCRIPTION,
        CLOSED_KNOW_NOT_VERIFIED_INSTRUCTIONS,
        4,
        "email/subject-request/closed-know-not-verified.ftlh",
        sharedParams.plus("type" to "Request to Know")
    ),
    ClosedDelete(
        "closed-delete",
        CLOSED_DELETE_TITLE,
        CLOSED_DELETE_DESCRIPTION,
        CLOSED_DELETE_INSTRUCTIONS,
        5,
        "email/subject-request/closed-delete.ftlh",
        sharedParams.plus("type" to "Request to Delete")
    ),
    ClosedKnow(
        "closed-know",
        CLOSED_KNOW_TITLE,
        CLOSED_KNOW_DESCRIPTION,
        CLOSED_KNOW_INSTRUCTIONS,
        6,
        "email/subject-request/closed-know.ftlh",
        sharedParams.plus("type" to "Request to Know")
    ),
    ClosedPortability(
        "closed-portability",
        CLOSED_PORTABILITY_TITLE,
        CLOSED_PORTABILITY_DESCRIPTION,
        CLOSED_PORTABILITY_INSTRUCTIONS,
        9,
        "email/subject-request/closed-portability.ftlh",
        sharedParams.plus("type" to "Portability Request")
    ),
    ClosedLimit(
        "closed-limit",
        CLOSED_LIMIT_TITLE,
        CLOSED_LIMIT_DESCRIPTION,
        CLOSED_LIMIT_INSTRUCTIONS,
        10,
        "email/subject-request/closed-limit.ftlh",
        sharedParams.plus("type" to "Portability Request")
    ),
    ClosedRectification(
        "closed-portability",
        CLOSED_RECTIFICATION_TITLE,
        CLOSED_RECTIFICATION_DESCRIPTION,
        CLOSED_RECTIFICATION_INSTRUCTIONS,
        11,
        "email/subject-request/closed-rectification.ftlh",
        sharedParams.plus("type" to "Rectification Request")
    ),
    ClosedWithdrawnConsent(
        "closed-withdrawn-consent",
        CLOSED_WITHDRAWN_CONSENT_TITLE,
        CLOSED_WITHDRAWN_CONSENT_DESCRIPTION,
        CLOSED_WITHDRAWN_CONSENT_INSTRUCTIONS,
        11,
        "email/subject-request/closed-withdrawn-request.ftlh",
        sharedParams.plus("type" to "Rectification Request")
    ),
    ClosedObjection(
        "closed-objection",
        CLOSED_OBJECTION_TITLE,
        CLOSED_OBJECTION_DESCRIPTION,
        CLOSED_OBJECTION_INSTRUCTIONS,
        11,
        "email/subject-request/closed-objection.ftlh",
        sharedParams.plus("type" to "Rectification Request")
    ),
    ClosedOptOut(
        "closed-opt-out",
        CLOSED_OPT_OUT_TITLE,
        CLOSED_OPT_OUT_DESCRIPTION,
        CLOSED_OPT_OUT_INSTRUCTIONS,
        7,
        "email/subject-request/closed-opt-out.ftlh",
        sharedParams.plus("type" to "Request to Opt-Out")
    );

    fun toDto() = mapOf<String, Any>(
        "id" to name,
        "slug" to slug,
        "title" to title,
        "description" to description,
        "instructions" to instructions,
        "sortKey" to sortKey
    )

    companion object {
        val valuesBySlug = values().associateBy { it.slug }

        fun bySlug(slug: String): CustomMessageTypeEnum {
            return valuesBySlug[slug] ?: throw CustomMessageTypeNotFound(slug)
        }
    }
}
