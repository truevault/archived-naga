package polaris.models.entity.organization

enum class WebsiteAuditProgress {
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETE,
    ARCHIVED,
    RESET,
    ERROR,
}
