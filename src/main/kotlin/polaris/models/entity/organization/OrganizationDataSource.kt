package polaris.models.entity.organization

import polaris.models.entity.vendor.Vendor
import polaris.util.converters.CollectionContextListCon
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "organization_data_source")
@IdClass(OrganizationDataSourceId::class)
data class OrganizationDataSource(
    @Id
    @Column(name = "organization_id", updatable = false)
    val organizationId: UUID? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", insertable = false, updatable = false)
    val organization: Organization? = null,

    @Id
    @Column(name = "vendor_id", updatable = false)
    val vendorId: UUID? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_id", insertable = false, updatable = false)
    val vendor: Vendor? = null,

    @Convert(converter = CollectionContextListCon::class)
    @Column(name = "consumer_context")
    var collectionContexts: List<CollectionContext> = listOf(CollectionContext.CONSUMER)
)

data class OrganizationDataSourceId(
    val organizationId: UUID? = null,
    val vendorId: UUID? = null
) : Serializable
