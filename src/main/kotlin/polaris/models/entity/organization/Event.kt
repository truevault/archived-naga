package polaris.models.entity.organization

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubstate
import polaris.models.entity.user.User
import java.util.UUID
import javax.persistence.*

@Entity
data class Event(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false)
    val user: User? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    // Note: When new roles are added in the future, this
    // should change to a less permissive default role.
    @Enumerated(EnumType.STRING)
    val eventName: EventName = EventName.SurveyResponse,

    @Convert(converter = EventDataConverter::class)
    val eventData: EventData? = null
) {
    override fun toString(): String {
        return "Event(event=$eventName, id=$id, org=${organization?.name}, user:${user?.email}, data: $eventData)"
    }
}

enum class EventName {
    SurveyResponse,
    DataSubjectRequestTransition,
    GenericProgressUpdate,
    GetCompliantProgressUpdate,
}

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type"
)
@JsonSubTypes(
    JsonSubTypes.Type(value = EventData.SurveyResponse::class, name = "SurveyResponse"),
    JsonSubTypes.Type(value = EventData.DataSubjectRequestTransition::class, name = "DataSubjectRequestTransition"),
    JsonSubTypes.Type(value = EventData.GenericProgressUpdate::class, name = "GenericProgressUpdate"),
    JsonSubTypes.Type(value = EventData.GetCompliantProgressUpdate::class, name = "GetCompliantProgressUpdate")
)
sealed class EventData(@JsonIgnore val eventName: EventName) {
    data class SurveyResponse(val surveyName: String, val surveySlug: String, val response: String?, val oldResponse: String?) : EventData(EventName.SurveyResponse)
    data class DataSubjectRequestTransition(val requestId: String, val from: DataRequestState?, val fromSubstate: DataRequestSubstate?, val to: DataRequestState, val toSubstate: DataRequestSubstate?) : EventData(EventName.DataSubjectRequestTransition)
    data class GenericProgressUpdate(val progressKey: String, val from: MappingProgressEnum?, val to: MappingProgressEnum) : EventData(EventName.GenericProgressUpdate)
    data class GetCompliantProgressUpdate(val from: String, val to: String) : EventData(EventName.GetCompliantProgressUpdate)
}

@Converter
class EventDataConverter : AttributeConverter<EventData, String> {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(EventDataConverter::class.java)
    }

    override fun convertToDatabaseColumn(attribute: EventData?): String? {
        if (attribute == null) {
            return null
        }

        val result = try {
            jacksonObjectMapper().writeValueAsString(attribute)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (todb): ", e)
            "{}"
        }
        return result
    }

    override fun convertToEntityAttribute(dbData: String?): EventData? {
        if (dbData == null) { return null }
        val result: EventData? = try {
            jacksonObjectMapper().readValue(dbData)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (toentity): ", e)
            null
        }
        return result
    }
}
