package polaris.models.entity.organization

import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
data class OrganizationCookieScan(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @Enumerated(EnumType.STRING)
    var status: CookieScanProgress,

    val scanUrl: String,

    val scanStartedAt: ZonedDateTime = ZonedDateTime.now(),
    var scanEndedAt: ZonedDateTime? = null,

    var scanError: String? = null
) {
    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , organization = $organization , status = $status , scan_url = $scanUrl , scan_started_at = $scanStartedAt, scan_ended_at = $scanEndedAt )"
    }

    fun hasScanTimedOut(): Boolean {
        val now = ZonedDateTime.now()

        if (status == CookieScanProgress.IN_PROGRESS && scanStartedAt.plusMinutes(5).toInstant() < now.toInstant()) {
            return true
        }

        return false
    }
}
