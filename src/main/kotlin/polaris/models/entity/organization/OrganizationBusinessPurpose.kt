package polaris.models.entity.organization

import org.hibernate.Hibernate
import polaris.models.entity.compliance.BusinessPurpose
import java.io.Serializable
import java.util.Objects
import java.util.UUID
import javax.persistence.*

@Entity
@IdClass(OrganizationBusinessPurposeId::class)
data class OrganizationBusinessPurpose(
    @Id
    @Column(name = "organization_id", updatable = false)
    val organizationId: UUID? = null,

    @Id
    @Column(name = "business_purpose_id", updatable = false)
    val businessPurposeId: UUID? = null,

    @Id
    @Enumerated(EnumType.STRING)
    var businessPurposeType: BusinessPurposeType? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", insertable = false, updatable = false)
    val organization: Organization? = null,

    @ManyToOne
    @JoinColumn(name = "business_purpose_id", insertable = false, updatable = false)
    val businessPurpose: BusinessPurpose? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as OrganizationBusinessPurpose

        return organizationId == other.organizationId &&
            businessPurposeId == other.businessPurposeId &&
            businessPurposeType == other.businessPurposeType
    }

    override fun hashCode(): Int = Objects.hash(organizationId, businessPurposeId, businessPurposeType)

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(organizationId = $organizationId , businessPurposeId = $businessPurposeId , businessPurposeType = $businessPurposeType )"
    }
}

enum class BusinessPurposeType {
    CONSUMER,
    EMPLOYMENT
}

@Embeddable
data class OrganizationBusinessPurposeId(
    val organizationId: UUID? = null,
    val businessPurposeId: UUID? = null,
    val businessPurposeType: BusinessPurposeType? = null
) : Serializable
