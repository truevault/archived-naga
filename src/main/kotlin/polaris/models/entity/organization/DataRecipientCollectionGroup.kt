package polaris.models.entity.organization

import polaris.models.entity.vendor.Vendor
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "data_recipient_collection_group")
@IdClass(DataRecipientCollectionGroupId::class)
data class DataRecipientCollectionGroup(
    @Id
    @Column(name = "vendor_id", updatable = false)
    val vendorId: UUID? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_id", insertable = false, updatable = false)
    val vendor: Vendor? = null,

    @Id
    @Column(name = "collection_group_id", updatable = false)
    val collectionGroupId: UUID? = null,
    @ManyToOne
    @JoinColumn(name = "collection_group_id", insertable = false, updatable = false)
    val collectionGroup: CollectionGroup? = null,

    var noDataDisclosure: Boolean? = null
) {
    override fun toString(): String {
        return "OrganizationVendorConsumerGroup(vendor=${vendor?.name} [${vendor?.id}], consumer group=${collectionGroup?.name} [${collectionGroup?.id}], no data mapping=$noDataDisclosure)"
    }
}

data class DataRecipientCollectionGroupId(
    val vendorId: UUID? = null,
    val collectionGroupId: UUID? = null
) : Serializable
