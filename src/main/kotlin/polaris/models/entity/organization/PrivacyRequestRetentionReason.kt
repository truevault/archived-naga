package polaris.models.entity.organization

import polaris.models.entity.compliance.RetentionReason
import javax.persistence.*

@Entity
data class PrivacyRequestRetentionReason(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "collection_group_id")
    val collectionGroup: CollectionGroup? = null,

    @ManyToOne
    @JoinColumn(name = "organization_service_id")
    val organizationDataRecipient: OrganizationDataRecipient? = null,

    @ManyToOne
    @JoinColumn(name = "retention_reason_id")
    val retentionReason: RetentionReason? = null
)
