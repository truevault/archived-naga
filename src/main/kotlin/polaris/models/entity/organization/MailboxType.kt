package polaris.models.entity.organization

enum class MailboxType {
    GMAIL,
    EXCHANGE,
    UNKNOWN
}
