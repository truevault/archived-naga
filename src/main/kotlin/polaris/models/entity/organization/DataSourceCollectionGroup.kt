package polaris.models.entity.organization

import polaris.models.entity.vendor.Vendor
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "data_source_collection_group")
@IdClass(DataSourceCollectionGroupId::class)
data class DataSourceCollectionGroup(
    @Id
    @Column(name = "vendor_id", updatable = false)
    val vendorId: UUID? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_id", insertable = false, updatable = false)
    val vendor: Vendor? = null,

    @Id
    @Column(name = "collection_group_id", updatable = false)
    val collectionGroupId: UUID? = null,
    @ManyToOne
    @JoinColumn(name = "collection_group_id", insertable = false, updatable = false)
    val collectionGroup: CollectionGroup? = null
) {
    override fun toString(): String {
        return "DataSourceConsumerGroup(vendor=${vendor?.name} [${vendor?.id}], consumer group=${collectionGroup?.name} [${collectionGroup?.id}])"
    }
}

data class DataSourceCollectionGroupId(
    val vendorId: UUID? = null,
    val collectionGroupId: UUID? = null
) : Serializable
