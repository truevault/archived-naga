package polaris.models.entity.organization

import polaris.models.dto.vendor.LabeledEnumDto

enum class MappingProgressEnum(val label: String) {
    NOT_STARTED("not started"),
    IN_PROGRESS("in progress"),
    DONE("done")
}

fun mappingProgressDto(e: MappingProgressEnum) = LabeledEnumDto(e, e.label)
