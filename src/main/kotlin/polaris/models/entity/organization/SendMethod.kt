package polaris.models.entity.organization

enum class SendMethod {
    SES,
    SENDGRID,
    NYLAS
}
