package polaris.models.entity.organization

import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import polaris.util.converters.LawfulBasisLisCon
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.Id

@Entity
@Where(clause = "deleted_at is null")
@SQLDelete(sql = "UPDATE organization SET deleted_at = NOW() WHERE id=?")
data class DefaultProcessingActivity(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    var name: String = "",
    var slug: String? = null,

    var materialIconKey: String? = null,

    @Convert(converter = LawfulBasisLisCon::class)
    var gdprMandatoryLawfulBases: List<LawfulBasis> = emptyList(),

    @Convert(converter = LawfulBasisLisCon::class)
    var gdprOptionalLawfulBases: List<LawfulBasis> = emptyList(),

    var displayOrder: Int? = null,
    var deprecated: Boolean = false,
    var helpText: String? = null,

    var deletedAt: ZonedDateTime? = null
)

enum class LawfulBasis {
    COMPLY_WITH_LEGAL_OBLIGATIONS,
    FULFILL_CONTRACTS,
    LEGITIMATE_INTERESTS,
    CONSENT,
    PUBLIC_INTEREST,
    VITAL_INTEREST_OF_INDIVIDUAL
}
