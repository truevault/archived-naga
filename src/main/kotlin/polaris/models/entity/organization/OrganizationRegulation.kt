package polaris.models.entity.organization

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import polaris.models.entity.regulation.Regulation
import javax.persistence.AttributeConverter
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
data class OrganizationRegulation(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "regulation_id", updatable = false)
    val regulation: Regulation? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @Convert(converter = OrganizationRegulationSettingsCon::class)
    var settings: Map<String, Any>? = emptyMap(),

    var identityVerificationInstruction: String = ""
)

class OrganizationRegulationSettingsCon : AttributeConverter<Map<String, Any>, String> {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(OrganizationRegulationSettingsCon::class.java)
    }

    override fun convertToDatabaseColumn(attribute: Map<String, Any>?): String {
        val result = try {
            jacksonObjectMapper().writeValueAsString(attribute)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (todb): ", e)
            "{}"
        }
        return result
    }

    override fun convertToEntityAttribute(dbData: String?): Map<String, Any> {
        if (dbData == null) { return emptyMap() }
        val result: Map<String, Any> = try {
            jacksonObjectMapper().readValue(dbData)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (toentity): ", e)
            emptyMap()
        }
        return result
    }
}
