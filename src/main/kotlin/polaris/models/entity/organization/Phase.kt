package polaris.models.entity.organization

enum class Phase {
    SETUP,
    GET_COMPLIANT,
    STAY_COMPLIANT
}
