package polaris.models.entity.organization

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.hibernate.annotations.Where
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import polaris.models.dto.organization.RequestHandlingInstructionDto
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "request_handling_instructions")
@Where(clause = "deleted_at is null")
data class RequestHandlingInstruction(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @ManyToOne
    @JoinColumn(name = "organization_service_id", updatable = false)
    val organizationDataRecipient: OrganizationDataRecipient? = null,

    @Enumerated(EnumType.STRING)
    val requestType: DataRequestInstructionType? = null,

    @Enumerated(EnumType.STRING)
    var processingMethod: ProcessingMethod? = null,

    @Convert(converter = JSONListConverter::class)
    var retentionReasons: MutableList<String> = mutableListOf(),

    var processingInstructions: String? = "",

    var deleteDoNotContact: Boolean? = null,

    @Enumerated(EnumType.STRING)
    var doNotContactReason: DoNotContactReason? = null,
    var doNotContactExplanation: String? = null,

    var exceptionsAffirmed: Boolean = false,

    var hasRetentionExceptions: Boolean? = null,

    val createdAt: ZonedDateTime = ZonedDateTime.now(),

    @Convert(converter = JSONListConverter::class)
    var deletedPicIds: MutableList<String> = mutableListOf(),

    var deletedAt: ZonedDateTime? = null
) {
    fun toDto() = RequestHandlingInstructionDto(
        id = id.toString(),
        organizationId = organization!!.publicId,
        vendorId = vendorIdOrNull(),

        requestType = requestType!!,
        processingMethod = processingMethod,
        retentionReasons = retentionReasons,
        processingInstructions = processingInstructions,
        deleteDoNotContact = deleteDoNotContact,
        doNotContactReason = doNotContactReason,
        doNotContactExplanation = doNotContactExplanation,
        hasRetentionExceptions = hasRetentionExceptions,
        exceptionsAffirmed = exceptionsAffirmed,
        deletedPIC = deletedPicIds
    )

    fun vendorIdOrNull(): String? {
        return organizationDataRecipient?.vendor?.let { it.id.toString() }
    }
}

enum class DoNotContactReason {
    CONTACT_VIA_OTHER_MEANS,
    IMPOSSIBLE_OR_DIFFICULT,
    SELF_SERVICE_DELETION
}

class JSONListConverter : AttributeConverter<List<String>, String> {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(JSONListConverter::class.java)
    }

    override fun convertToDatabaseColumn(attribute: List<String>?): String {
        val result = try {
            jacksonObjectMapper().writeValueAsString(attribute)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (todb): ", e)
            "{}"
        }
        return result
    }

    override fun convertToEntityAttribute(dbData: String?): List<String> {
        if (dbData == null) { return emptyList() }
        val result: List<String> = try {
            jacksonObjectMapper().readValue(dbData)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (toentity): ", e)
            emptyList()
        }
        return result
    }
}
