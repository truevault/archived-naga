package polaris.models.entity.organization

import polaris.models.dto.organization.OrganizationMailboxDto
import java.util.*
import javax.persistence.*

@Entity
data class OrganizationMailbox(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @Enumerated(EnumType.STRING)
    var status: MailboxStatus = MailboxStatus.DISCONNECTED,

    @Enumerated(EnumType.STRING)
    val type: MailboxType = MailboxType.UNKNOWN,

    val mailbox: String = "",

    val nylasAccessToken: String = "",
    val nylasAccountId: String = ""

) {
    fun toDto() = OrganizationMailboxDto(
        status = status,
        type = type,
        mailbox = mailbox,
        headerImageUrl = organization!!.getFullHeaderImageUrl(),
        physicalAddress = organization.physicalAddress,
        messageSignature = organization.messageSignature
    )
}
