package polaris.models.entity.organization

import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.ProcessingInstructionDto
import polaris.models.entity.request.DataRequestType
import polaris.util.MarkdownRenderer
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "processing_instructions")
data class ProcessingInstruction(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @ManyToOne
    @JoinColumn(name = "collection_group_id", updatable = false)
    val collectionGroup: CollectionGroup? = null,

    @Enumerated(EnumType.STRING)
    var requestType: DataRequestType? = null,

    var instruction: String = "",

    val createdAt: ZonedDateTime = ZonedDateTime.now()
) {
    fun toDto() = ProcessingInstructionDto(
        organizationId = organization?.publicId as OrganizationPublicId,
        dataSubjectTypeId = collectionGroup?.id.toString(),
        dataRequestType = requestType,
        // TODO: Remove markdown renderer when deprecated or migrated to only HTML for the WYSIWYG editor
        instruction = MarkdownRenderer.renderToHtml(instruction)
    )
}
