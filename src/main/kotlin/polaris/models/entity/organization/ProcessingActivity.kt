package polaris.models.entity.organization

import polaris.models.entity.compliance.AutocreatedProcessingActivitySlug
import polaris.models.entity.compliance.PersonalInformationCategory
import polaris.models.entity.vendor.ProcessingRegion
import polaris.util.converters.LawfulBasisLisCon
import polaris.util.converters.ProcessingRegionListCon
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne

@Entity
data class ProcessingActivity(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @ManyToOne
    @JoinColumn(name = "default_processing_activity_id", updatable = false)
    val defaultProcessingActivity: DefaultProcessingActivity? = null,

    var name: String = "",

    @Enumerated(EnumType.STRING)
    var autocreationSlug: AutocreatedProcessingActivitySlug? = null,

    @Convert(converter = LawfulBasisLisCon::class)
    var gdprLawfulBases: List<LawfulBasis> = emptyList(),

    @Convert(converter = ProcessingRegionListCon::class)
    var processingRegions: List<ProcessingRegion> = listOf(ProcessingRegion.UNITED_STATES),

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JoinTable(
        name = "processing_activity_pic",
        joinColumns = [JoinColumn(name = "processing_activity_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "pic_id", referencedColumnName = "id")]
    )
    var processedPic: MutableSet<PersonalInformationCategory> = mutableSetOf()
)
