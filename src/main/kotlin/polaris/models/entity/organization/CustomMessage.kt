package polaris.models.entity.organization

import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
data class CustomMessage(
    @Id
    @Column(updatable = false)
    @GeneratedValue
    val id: UUID? = null,
    @Enumerated(EnumType.STRING)
    val messageType: CustomMessageTypeEnum = CustomMessageTypeEnum.ClosedNotFound,
    var customText: String = "",
    val createdAt: ZonedDateTime = ZonedDateTime.now(),
    val lastUpdatedAt: ZonedDateTime = ZonedDateTime.now(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null
)
