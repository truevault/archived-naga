package polaris.models.entity.organization

import polaris.models.dto.organization.OrganizationComplianceChecklistItemDto
import javax.persistence.*

@Entity
data class OrganizationComplianceChecklistItem(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    val slug: String = "",
    var description: String = "",
    var done: Boolean = false
) {
    fun toDto() = OrganizationComplianceChecklistItemDto(
        organizationId = organization!!.publicId,
        slug = slug,
        description = description,
        done = done
    )
}
