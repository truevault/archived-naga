package polaris.models.entity.organization

import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import org.hibernate.Hibernate
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import polaris.models.entity.user.User
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "task")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType::class)
data class Task(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @Enumerated(EnumType.STRING)
    var taskKey: TaskKey? = null,

    val name: String = "",
    val description: String = "",

    val createdAt: ZonedDateTime = ZonedDateTime.now(),
    var dueAt: ZonedDateTime? = null,
    var completedAt: ZonedDateTime? = null,

    var callToAction: String? = null,
    var callToActionUrl: String? = null,

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    var taskMeta: Map<String, String>? = emptyMap(),

    @Enumerated(EnumType.STRING)
    val completionType: TaskCompletionType? = TaskCompletionType.AUTOMATIC,

    @ManyToOne
    @JoinColumn(name = "completed_by")
    var completedBy: User? = null
) {
    fun isComplete(): Boolean = completedAt != null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Task

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , organization = $organization , taskKey = $taskKey , name = $name , description = $description , createdAt = $createdAt , dueAt = $dueAt , completedAt = $completedAt , completedBy = $completedBy )"
    }
}
