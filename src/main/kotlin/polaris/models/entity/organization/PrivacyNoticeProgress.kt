package polaris.models.entity.organization

import polaris.models.dto.vendor.LabeledEnumDto

enum class PrivacyNoticeProgress(val label: String) {
    NEEDS_REVIEW("needs review"),
    APPROVED("approved")
}

fun privacyNoticeProgressDto(e: PrivacyNoticeProgress) = LabeledEnumDto(e, e.label)
