package polaris.models.entity.organization

import polaris.models.dto.vendor.LabeledEnumDto

enum class OptOutPageType(val label: String) {
    NONE("NONE"),
    SELLING("SELLING"),
    SHARING("SHARING"),
    SELLING_AND_SHARING("SELLING_AND_SHARING")
}

fun optOutPageTypeDto(e: OptOutPageType) = LabeledEnumDto(e, e.label)
