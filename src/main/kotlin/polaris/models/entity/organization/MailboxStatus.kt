package polaris.models.entity.organization

enum class MailboxStatus {
    CONNECTED,
    DISCONNECTED,
    INACTIVE
}
