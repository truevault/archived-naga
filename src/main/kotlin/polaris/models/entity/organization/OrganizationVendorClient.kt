package polaris.models.entity.organization

import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import javax.persistence.*

@Entity
data class OrganizationVendorClient(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "vendor_id", updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    val vendorOrganization: Organization? = null,

    @ManyToOne
    @JoinColumn(name = "client_id", updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    val clientOrganization: Organization? = null
)
