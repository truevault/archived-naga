package polaris.models.entity.organization

import org.hibernate.annotations.Where
import java.io.Serializable
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "platform_app_installation")
@IdClass(PlatformAppInstallId::class)
@Where(clause = "deleted_at is null")
data class PlatformAppInstall(
    @Id
    @Column(name = "platform_id", updatable = false)
    val platformId: Int? = null,

    @ManyToOne
    @JoinColumn(name = "platform_id", insertable = false, updatable = false)
    val platform: OrganizationDataRecipient? = null,

    @Id
    @Column(name = "app_id", updatable = false)
    val appId: Int? = null,

    @ManyToOne
    @JoinColumn(name = "app_id", insertable = false, updatable = false)
    val app: OrganizationDataRecipient? = null,

    var deletedAt: ZonedDateTime? = null
) {
    override fun toString(): String {
        return "PlatformAppInstall(platform=${platform?.vendor?.name} app=${app?.vendor?.name})"
    }
}

data class PlatformAppInstallId(
    val platformId: Int? = null,
    val appId: Int? = null
) : Serializable
