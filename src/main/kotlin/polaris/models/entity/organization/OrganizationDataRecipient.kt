package polaris.models.entity.organization

import org.hibernate.annotations.Where
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.models.entity.vendor.ProcessingRegion
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_CONTRACTOR
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY
import polaris.models.entity.vendor.Vendor
import polaris.models.entity.vendor.VendorClassification
import polaris.models.entity.vendor.VendorContract
import polaris.models.entity.vendor.VendorContractReviewed
import polaris.models.listeners.OrganizationDataRecipientEntityListener
import polaris.util.converters.CollectionContextListCon
import polaris.util.converters.ProcessingRegionListCon
import java.time.ZonedDateTime
import javax.persistence.AttributeConverter
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Converter
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@EntityListeners(OrganizationDataRecipientEntityListener::class)
@Entity
@Table(name = "organization_data_recipient")
@Where(clause = "deleted_at is null")
data class OrganizationDataRecipient(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_id", updatable = false)
    val vendor: Vendor? = null,

    @OneToMany(mappedBy = "organizationDataRecipient")
    val requestHandlingInstructions: List<RequestHandlingInstruction> = emptyList(),

    @ManyToOne
    @JoinColumn(name = "service_type_id")
    var vendorClassification: VendorClassification? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_contract_id")
    var vendorContract: VendorContract? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_contract_reviewed_id")
    var vendorContractReviewed: VendorContractReviewed? = null,

    @Enumerated(EnumType.STRING)
    var status: OrganizationVendorStatus? = OrganizationVendorStatus.IN_PROGRESS,

    var publicTosUrl: String? = null,
    var ccpaIsSelling: Boolean = false,
    var ccpaIsSharing: Boolean = false,
    var usesCustomAudience: Boolean = false,
    var isUploadVendor: Boolean? = null,
    var automaticallyClassified: Boolean = false,
    var email: String? = null,
    var isDataStorage: Boolean = false,
    var contacted: Boolean? = null,

    var tosFileName: String? = null,
    var tosFileKey: String? = null,

    @Enumerated(EnumType.STRING)
    var mappingProgress: MappingProgressEnum = MappingProgressEnum.NOT_STARTED,

    @Enumerated(EnumType.STRING)
    var dataAccessibility: DataAccessibilityEnum? = null,

    @Enumerated(EnumType.STRING)
    var dataDeletability: DataDeletabilityEnum? = null,

    @Convert(converter = ProcessingRegionListCon::class)
    var processingRegions: List<ProcessingRegion> = listOf(ProcessingRegion.UNITED_STATES, ProcessingRegion.EEA_UK),

    @Convert(converter = CollectionContextListCon::class)
    @Column(name = "consumer_context")
    var collectionContexts: List<CollectionContext> = emptyList(),

    @Enumerated(EnumType.STRING)
    var gdprProcessorSetting: GDPRProcessorRecommendation? = null,
    @Enumerated(EnumType.STRING)
    var gdprProcessorLockedSetting: GDPRProcessorRecommendation? = null,
    var gdprProcessorSettingSetAt: ZonedDateTime? = null,

    var gdprProcessorGuaranteeUrl: String? = null,
    var gdprProcessorGuaranteeFileName: String? = null,
    var gdprProcessorGuaranteeFileKey: String? = null,
    var gdprControllerGuaranteeUrl: String? = null,
    var gdprControllerGuaranteeFileName: String? = null,
    var gdprControllerGuaranteeFileKey: String? = null,

    var gdprContactUnsafeTransfer: GDPRContactUnsafeTransfer? = null,
    var gdprEnsureSccInPlace: GDPRSccInPlace? = null,

    @Enumerated(EnumType.STRING)
    var gdprContactProcessorStatus: GDPRContactProcessorStatus? = null,

    var gdprHasSccSetting: Boolean? = null,
    var gdprSccUrl: String? = null,
    var gdprSccFileName: String? = null,
    var gdprSccFileKey: String? = null,

    var removedFromExceptionsToScc: Boolean = false,

    var completed: Boolean = true,

    var createdAt: ZonedDateTime = ZonedDateTime.now(),

    var deletedAt: ZonedDateTime? = null
) {

    fun activeGdprProcessorSetting(): GDPRProcessorRecommendation? = gdprProcessorLockedSetting ?: gdprProcessorSetting

    override fun toString(): String {
        return "OrganizationDataRecipient(id=$id organization=${organization?.name} vendor=${vendor?.name} ccpaIsSelling=$ccpaIsSelling ccpaIsSharing=$ccpaIsSharing vendorClassification=$vendorClassification)"
    }

    fun isExceptedFromSccs(): Boolean =
        !removedFromExceptionsToScc && (vendor?.isPotentiallyExceptedFromSccs() ?: false)

    fun isPotentiallyExceptedFromSccs(): Boolean = vendor?.isPotentiallyExceptedFromSccs() ?: false

    fun isPotentialIntentionalInteractionVendor(): Boolean =
        vendor?.isPotentialIntentionalInteractionVendor() ?: false && !isContractor()

    fun isAutomaticIntentionalInteractionVendor(): Boolean =
        isPotentialIntentionalInteractionVendor() && (vendor?.isAutomaticIntentionalInteractionVendor() ?: false)

    fun isContractor(): Boolean =
        vendor?.isContractor() ?: false || vendorClassification?.slug == VENDOR_CLASSIFICATION_SLUG_CONTRACTOR

    fun isThirdParty(): Boolean =
        vendor?.isThirdParty() ?: false || vendorClassification?.slug == VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY
}

enum class DataAccessibilityEnum {
    ACCESSIBLE,
    NOT_ACCESSIBLE,
    UNKNOWN,
}

enum class DataDeletabilityEnum {
    DELETABLE,
    NOT_DELETABLE,
    UNKNOWN,
}

enum class CollectionContext(val collectionGroupTypes: List<CollectionGroupType>) {
    CONSUMER(listOf(CollectionGroupType.BUSINESS_TO_CONSUMER, CollectionGroupType.BUSINESS_TO_BUSINESS)),
    EMPLOYEE(listOf(CollectionGroupType.EMPLOYMENT))
    ;
}

@Converter(autoApply = true)
class GDPRContactUnsafeTransferConverter : AttributeConverter<GDPRContactUnsafeTransfer, String> {
    override fun convertToDatabaseColumn(attribute: GDPRContactUnsafeTransfer?): String? = attribute?.toString()
    override fun convertToEntityAttribute(dbData: String?): GDPRContactUnsafeTransfer? {
        if (dbData == null) {
            return null
        }

        return GDPRContactUnsafeTransfer.fromStatus(dbData)
    }
}

enum class GDPRContactUnsafeTransfer(val status: String) {
    CONTACTED("contacted"),
    UNSAFE("unsafe");

    companion object {
        fun fromStatus(status: String): GDPRContactUnsafeTransfer? {
            return when (status) {
                "contacted" -> CONTACTED
                "unsafe" -> UNSAFE
                else -> null
            }
        }
    }

    override fun toString(): String {
        return status
    }
}

@Converter(autoApply = true)
class GDPRSccInPlaceConverter : AttributeConverter<GDPRSccInPlace?, String> {
    override fun convertToDatabaseColumn(attribute: GDPRSccInPlace?): String? = attribute?.toString()
    override fun convertToEntityAttribute(dbData: String?): GDPRSccInPlace? {
        if (dbData == null) {
            return null
        }

        return GDPRSccInPlace.fromStatus(dbData)
    }
}

enum class GDPRSccInPlace(val setting: String) {
    IN_PLACE("in-place"),
    STOP_USING("stop-using");

    companion object {
        fun fromStatus(status: String): GDPRSccInPlace? {
            return when (status) {
                "in-place" -> IN_PLACE
                "stop-using" -> STOP_USING
                else -> null
            }
        }
    }

    override fun toString(): String {
        return setting
    }
}

enum class GDPRContactProcessorStatus(val status: String) {
    CONTACTED("contacted"),
    CONTROLLER("controller");

    override fun toString(): String {
        return status
    }
}
