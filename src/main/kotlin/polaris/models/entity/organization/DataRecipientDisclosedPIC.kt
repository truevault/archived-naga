package polaris.models.entity.organization

import polaris.models.entity.vendor.Vendor
import javax.persistence.*

@Entity
@Table(name = "data_recipient_disclosed_pic")
data class DataRecipientDisclosedPIC(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "collected_pic_id", updatable = false)
    val collectedPIC: CollectionGroupCollectedPIC? = null,

    @ManyToOne
    @JoinColumn(name = "service_id", updatable = false)
    val vendor: Vendor? = null
) {
    override fun toString(): String {
        return "DataRecipientDisclosedPIC(id=$id)"
    }
}
