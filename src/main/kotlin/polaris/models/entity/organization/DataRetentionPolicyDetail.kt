package polaris.models.entity.organization

import polaris.models.entity.compliance.PersonalInformationCategory
import java.io.Serializable
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.IdClass
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "data_retention_policy_detail")
@IdClass(DataRetentionPolicyDetailId::class)
class DataRetentionPolicyDetail(
    @Id
    @Column(name = "organization_id", updatable = false)
    val organizationId: UUID? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", insertable = false, updatable = false)
    val organization: Organization? = null,

    @Id
    @Column(name = "type", updatable = false)
    @Enumerated(EnumType.STRING)
    val type: DataRetentionPolicyDetailCategory? = null,

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JoinTable(
        name = "data_retention_policy_detail_pic",
        joinColumns = [JoinColumn(name = "organizationId", columnDefinition = "UUID", referencedColumnName = "organization_id"), JoinColumn(name = "type", referencedColumnName = "type")],
        inverseJoinColumns = [JoinColumn(name = "pic_id")]

    )
    var pic: MutableList<PersonalInformationCategory> = mutableListOf(),

    var description: String? = null
)

data class DataRetentionPolicyDetailId(
    val organizationId: UUID? = null,
    @Enumerated(EnumType.STRING)
    val type: DataRetentionPolicyDetailCategory? = null
) : Serializable

enum class DataRetentionPolicyDetailCategory(val label: String) {
    ONLINE_DATA_FROM_WEBSITE("Cookies and online data we collect while you use our website"),
    PROCESS_AND_SHIP_ORDERS("Data we collect in order to process and ship orders you place with us"),
    CONTACT_CUSTOMER_SUPPORT("Data we collect when you contact us for customer support and other inquiries"),
    SIGN_UP_FOR_PROMOTIONAL_AND_MARKETING("Data we collect when you sign up for promotional and marketing communications"),
    REVIEW_PRODUCTS_ANSWER_SURVEYS_SEND_FEEDBACK("Data we collect when you review our products, answer surveys, or send feedback"),
    PRIVACY_REQUESTS("Data we collect in connection with privacy requests"),
    SECURITY_PURPOSES("Data we collect for security purposes")
}
