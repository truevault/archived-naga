package polaris.models.entity.user

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "login_activity")
data class LoginActivity(

    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    val email: String,
    val success: Boolean,
    val userId: UUID? = null,
    val organizationId: UUID? = null

)
