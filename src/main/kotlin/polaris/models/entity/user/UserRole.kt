package polaris.models.entity.user

enum class UserRole {
    ADMIN,

    // Note: When adding a new least-permissive role, update the default
    // assignment to role in OrganizationUser.kt
}
