package polaris.models.entity.user

import polaris.models.entity.organization.Organization
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

enum class InvitationStatus {
    OPEN,
    ACCEPTED
}

@Entity
@Table(name = "invitations")
data class Invitation(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @Enumerated(EnumType.STRING)
    var status: InvitationStatus? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    var organization: Organization,

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false)
    var user: User,

    var acceptedAt: ZonedDateTime? = null,
    var createdAt: ZonedDateTime = ZonedDateTime.now()
)
