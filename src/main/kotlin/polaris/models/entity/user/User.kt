package polaris.models.entity.user

import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationUser
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "polaris_user")
@Where(clause = "deleted_at is null")
@SQLDelete(sql = "UPDATE polaris_user SET deleted_at = NOW() WHERE id=?")
data class User(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    var email: String = "",
    var password: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var lastLogin: ZonedDateTime? = null,
    var updatePasswordId: UUID? = null,

    @Enumerated(EnumType.STRING)
    var globalRole: GlobalRole? = null,

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = [CascadeType.MERGE])
    val organization_users: List<OrganizationUser> = mutableListOf(),

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
    val invitations: List<Invitation> = mutableListOf(),

    @ManyToOne
    @JoinColumn(name = "active_organization_id")
    var activeOrganization: Organization? = null,

    var deletedAt: ZonedDateTime? = null
)
