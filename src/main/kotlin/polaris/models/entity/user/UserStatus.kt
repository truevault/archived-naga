package polaris.models.entity.user

enum class UserStatus {
    ACTIVE,
    INACTIVE,
    INVITED
}

fun UserStatus?.isActive() = this.isStatus(UserStatus.ACTIVE)
fun UserStatus?.isInactive() = this.isStatus(UserStatus.INACTIVE)
fun UserStatus?.isInvited() = this.isStatus(UserStatus.INVITED)
fun UserStatus?.isStatus(status: UserStatus): Boolean {
    if (this == null) return false
    return this == status
}
