package polaris.models.entity.privacycenter

import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
data class PrivacyCenterPolicySection(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "privacy_center_id", updatable = false)
    val privacyCenter: PrivacyCenter? = null,

    var displayOrder: Int = 0,
    var name: String = "",
    var anchor: String = "",
    var addToMenu: Boolean = true,
    var noPrivacyPolicy: Boolean = false,
    var body: String = "",
    var showHeader: Boolean = true,

    val createdAt: ZonedDateTime = ZonedDateTime.now()
)
