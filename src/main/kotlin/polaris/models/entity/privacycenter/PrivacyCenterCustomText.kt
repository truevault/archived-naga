package polaris.models.entity.privacycenter

import java.io.Serializable
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.IdClass
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

const val CCPA_PRIVACY_NOTICE_INTRO_TEXT = "<p>This section provides additional information for California residents under the California Consumer Privacy Act (CCPA). The terms used in this section have the same meaning as in the CCPA. This section does not apply to information that is not considered \"personal information,\" such as anonymous, deidentified, or aggregated information, nor does it apply to publicly available information as defined in the CCPA. </p><p>To the extent we process deidentified personal information, we will make no attempt to reidentify such data.</p>"
const val GDPR_PRIVACY_NOTICE_INTRO_TEXT = "<p>This section provides additional information for people in the European Economic Area (EEA) or United Kingdom (UK). The terms used in this section have the same meaning as in the General Data Protection Regulation and the UK Data Protection Act (GDPR). The term “personal information”  as used in this notice has the same meaning as “personal data” in the GDPR. </p>"
const val VCDPA_PRIVACY_NOTICE_INTRO_TEXT = "<p>This section provides additional information for Virginia residents under the Virginia Consumer Data Protection Act (VCDPA). The terms used in this section have the same meaning as in the VCDPA. This section does not apply to information that is not considered \"personal data,\" such as deidentified or publicly available information as defined in the VCDPA.</p>"
const val CPA_PRIVACY_NOTICE_INTRO_TEXT = "<p>This section provides additional information for Colorado residents under the Colorado Privacy Act (CPA). The terms used in this section have the same meaning as in the CPA. This section does not apply to information that is not considered \"personal data,\" such as deidentified or publicly available information as defined in the CPA.</p>"
const val CTDPA_PRIVACY_NOTICE_INTRO_TEXT = "<p>This section provides additional information for Connecticut residents under the Connecticut Data Privacy Act (CTDPA). The terms used in this section have the same meaning as in the CTDPA. This section does not apply to information that is not considered \"personal data,\" such as deidentified or publicly available information as defined in the CTDPA.</p><p>If you have any questions or wish to contact us regarding this privacy notice, please send an email to <a href=\"mailto:%s\">%s</a>.</p>"
const val RETENTION_INTRO_TEXT = "<p>We do not retain data for any longer than is necessary for the purposes described in this Policy.</p>"

const val CCPA_INTRO_SLUG = "ccpa-intro"
const val VCDPA_INTRO_SLUG = "vcdpa-intro"
const val CPA_INTRO_SLUG = "cpa-intro"
const val CTDPA_INTRO_SLUG = "ctdpa-intro"
const val GDPR_INTRO_SLUG = "gdpr-intro"
const val RETENTION_INTRO_SLUG = "retention-intro"
const val OPT_OUT_INTRO_SLUG = "optout-intro"
const val COOKIE_POLICY_INTRO_SLUG = "cookie-policy-intro"

@Entity
@IdClass(PrivacyCenterCustomTextId::class)
class PrivacyCenterCustomText(
    @Id
    @Column(name = "privacy_center_id", updatable = false)
    val privacyCenterId: UUID? = null,

    @Id
    val slug: String? = null,

    var value: String? = null,

    @ManyToOne
    @JoinColumn(name = "privacy_center_id", insertable = false, updatable = false)
    val privacyCenter: PrivacyCenter? = null
)

@Embeddable
data class PrivacyCenterCustomTextId(
    val privacyCenterId: UUID? = null,
    val slug: String? = null
) : Serializable
