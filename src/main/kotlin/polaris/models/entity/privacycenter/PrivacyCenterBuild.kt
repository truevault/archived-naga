package polaris.models.entity.privacycenter

import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
data class PrivacyCenterBuild(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "privacy_center_id", updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    val privacyCenter: PrivacyCenter?,

    var requestedAt: ZonedDateTime = ZonedDateTime.now(),
    var sentBuildMessageAt: ZonedDateTime? = null,
    var source: String? = null
)
