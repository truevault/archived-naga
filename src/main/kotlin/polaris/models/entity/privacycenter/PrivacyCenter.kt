package polaris.models.entity.privacycenter

import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import polaris.models.PrivacyCenterPublicId
import polaris.models.entity.organization.Organization
import polaris.util.IdGenerator
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OrderBy

enum class CustomUrlStatus {
    VERIFYING,
    ACTIVE
}

@Entity
@Where(clause = "deleted_at is null")
@SQLDelete(sql = "UPDATE privacy_center SET deleted_at = NOW() WHERE id=?")
data class PrivacyCenter(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    var publicId: PrivacyCenterPublicId = IdGenerator.generate(),
    var name: String = "",

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    var defaultSubdomain: String = "",
    var faviconUrl: String = "",
    var doNotSellMarkdown: String? = null,
    var employeePrivacyNoticeMarkdown: String? = null,
    var jobApplicantPrivacyNoticeMarkdown: String? = null,

    @OneToMany(mappedBy = "privacyCenter", cascade = [CascadeType.ALL])
    @OrderBy("displayOrder ASC")
    var policySections: List<PrivacyCenterPolicySection> = mutableListOf(),

    var policyLastUpdated: ZonedDateTime? = ZonedDateTime.now(),

    @Enumerated(EnumType.STRING)
    var customUrlStatus: CustomUrlStatus? = null,
    var customUrl: String? = null,
    var customUrlCnameReady: Boolean = false,
    var hasMobileApplication: Boolean = false,

    var privacyPolicyUrl: String? = null,
    var requestTollFreePhoneNumber: String? = null,
    var requestFormUrl: String? = null,
    var requestEmail: String? = null,

    @OneToMany(mappedBy = "privacyCenter", cascade = [CascadeType.ALL])
    var customText: MutableList<PrivacyCenterCustomText> = mutableListOf(),

    var logoUrl: String? = null,
    var logoLinkUrl: String? = null,

    var dpoOfficerDoesNotApply: Boolean = false,
    var dpoOfficerName: String? = null,
    var dpoOfficerEmail: String? = null,
    var dpoOfficerPhone: String? = null,
    var dpoOfficerAddress: String? = null,

    var cloudfrontDistribution: String? = null,

    var deletedAt: ZonedDateTime? = null,
    var caResidencyConfirmation: Boolean = true,

    var hideCcpaSection: Boolean = false
) {
    fun getBaseUrl(): String {
        if (!customUrl.isNullOrEmpty() && customUrlStatus == CustomUrlStatus.ACTIVE) {
            val url = customUrl as String
            return ensureHttpScheme(url)
        }

        return "https://$defaultSubdomain.truevaultprivacycenter.com"
    }

    fun getLogoLinkUrlWithScheme(): String? {
        if (logoLinkUrl == null) {
            return null
        }

        return ensureHttpScheme(logoLinkUrl!!)
    }

    private fun ensureHttpScheme(url: String): String {
        if (url.startsWith("http")) {
            return url
        } else {
            return "https://$url"
        }
    }

    fun getPrivacyRequestUrl() = getUrl("/privacy-request")
    fun getSpecificDeclarationUrl() = getUrl("/privacy-request-ccpa-specific")
    fun getContractorNoticeUrl() = getUrl("/contractor-privacy-notice")
    fun getEmployeeNoticeUrl() = getUrl("/employee-privacy-notice")
    fun getEeaUkEmployeeNoticeUrl() = getUrl("/eea-uk-employee-privacy-notice")

    fun getSPILimitUrl() = getUrl("/limit")

    fun getDoNotSellUrl() = getUrl("/opt-out")

    fun getEmailConfirmationUrl(token: UUID) = getUrl("/confirm-request?token=$token")

    fun getUrl(path: String): String {
        var newPath = path
        var base = getBaseUrl()

        // ensure base ends with a /
        if (!base.endsWith("/")) {
            base = base + "/"
        }

        if (newPath.startsWith("/")) {
            newPath = path.substring(1)
        }

        return "${base}$newPath"
    }

    fun emailWithFallback(): String {
        return requestEmail ?: "[your email address will be added here]"
    }

    fun ccpaIntro() = customTextOrDefault(CCPA_INTRO_SLUG, CCPA_PRIVACY_NOTICE_INTRO_TEXT)
    fun vcdpaIntro() = customTextOrDefault(VCDPA_INTRO_SLUG, VCDPA_PRIVACY_NOTICE_INTRO_TEXT)
    fun cpaIntro() = customTextOrDefault(CPA_INTRO_SLUG, CPA_PRIVACY_NOTICE_INTRO_TEXT)
    fun ctdpaIntro() = customTextOrDefault(CTDPA_INTRO_SLUG, CTDPA_PRIVACY_NOTICE_INTRO_TEXT.format(emailWithFallback(), emailWithFallback()))
    fun gdprIntro() = customTextOrDefault(GDPR_INTRO_SLUG, GDPR_PRIVACY_NOTICE_INTRO_TEXT)
    fun optOutIntro(defaultText: () -> String?) = customTextOrDefault(OPT_OUT_INTRO_SLUG, "<p>${defaultText()}</p>" ?: "")
    fun retentionIntro() = customTextOrDefault(RETENTION_INTRO_SLUG, RETENTION_INTRO_TEXT)
    fun cookiePolicyIntro() = organization!!.cookiePolicy ?: ""

    private fun customTextOrDefault(slug: String, def: String): String {
        return getCustomText(slug) ?: def
    }

    fun getCustomText(slug: String): String? {
        return customText.find { it.slug == slug }?.value
    }

    fun setCustomText(slug: String, value: String) {
        val existing = customText.find { it.slug == slug }
        if (existing != null) {
            existing.value = value
        } else {
            customText.add(
                PrivacyCenterCustomText(
                    privacyCenterId = id,
                    privacyCenter = this,
                    slug = slug,
                    value = value
                )
            )
        }
    }

    fun clearCustomText(slug: String) {
        customText.removeAll { it.slug == slug }
    }
}
