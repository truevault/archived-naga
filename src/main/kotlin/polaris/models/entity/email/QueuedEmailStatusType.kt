package polaris.models.entity.email

enum class QueuedEmailStatusType {
    NEW,
    PENDING,
    SENT,
    FAILURE
}
