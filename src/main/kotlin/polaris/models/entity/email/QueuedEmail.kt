package polaris.models.entity.email
import polaris.models.entity.request.DataSubjectRequest
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
data class QueuedEmail(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "data_subject_request_id", updatable = false)
    val dataSubjectRequest: DataSubjectRequest? = null,

    val recipientEmailAddress: String? = null,
    val senderEmailAddress: String? = null,

    val subject: String? = null,
    val message: String? = null,

    @Enumerated(EnumType.STRING)
    var status: QueuedEmailStatusType = QueuedEmailStatusType.NEW,

    var lastUpdatedAt: ZonedDateTime? = ZonedDateTime.now(),
    val createdAt: ZonedDateTime = ZonedDateTime.now()

)
