package polaris.models.entity.vendor

enum class ServiceProviderRecommendation {
    SERVICE_PROVIDER,
    CONTRACTOR,
    THIRD_PARTY,
    NONE
}
