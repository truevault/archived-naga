package polaris.models.entity.vendor

import org.hibernate.annotations.Where
import polaris.models.entity.compliance.RetentionReason
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.DataRecipientDisclosedPIC
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.services.support.ArrayConverter
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

enum class KnownVendorCategories(val category: String) {
    AD_NETWORK("Ad Network"),
    PAYMENT_PROCESSOR("Payment Processor"),
    SHIPPING_SERVICE("Shipping Service")
}

enum class KnownVendorKey(val key: String, val vendorName: String) {
    GOOGLE_ANALYTICS("google-analytics", "Google Analytics"),
    GOOGLE_ADS("google-ads", "Google Ads"),
    TRUEVAULT_POLARIS("truevault-polaris", "TrueVault Polaris"),
    FACEBOOK_ADS("facebook-ads", "Facebook Ads"),
    PAYPAL("paypal", "PayPal"),
    SHOPIFY("shopify", "Shopify"),
    APPLE_PAY("apple-pay", "Apple Pay"),
    AMAZON_PAY("amazon-pay", "Amazon Pay"),
    FACEBOOK_PAY("facebook-pay", "Facebook Pay"),
    SHOP_PAY("shop-pay", "Shop Pay"),
    KLARNA("klarna", "Klarna"),
    AFFIRM("affirm", "Affirm"),

    CONSUMER_DIRECTLY("source-consumer-directly", "Consumer"),
    OTHER_INDIVIDUALS("source-other-individuals", "Other Individuals"),

    AD_NETWORKS("source-ad-networks", "Ad Networks"),
    DATA_ANALYTICS_PROVIDERS("source-data-analytics-provider", "Data Analytics Providers"),
    DATA_BROKERS("source-data-brokers", "Data Brokers")
}

@Entity
@Table(name = "vendor")
@Where(clause = "deleted_at is null")
data class Vendor(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    var vendorKey: String? = null,
    @Convert(converter = ArrayConverter::class)
    var aliases: List<String>? = null,
    var name: String,
    var recipientCategory: String? = null,
    var disclosureSourceCategory: String? = null,
    var url: String? = null,
    var ccpaRequestToDeleteLink: String? = null,
    var deletionInstructions: String? = null,
    var optOutInstructions: String? = null,
    var generalGuidance: String? = null,
    var specialCircumstancesGuidance: String? = null,
    var ccpaRequestToKnowLink: String? = null,
    var logoFilename: String? = null,
    var email: String? = null,

    var isDataSource: Boolean = false,
    var isDataRecipient: Boolean = false,
    var isMigrationOnly: Boolean = false,

    @Enumerated(EnumType.STRING)
    var serviceProviderRecommendation: ServiceProviderRecommendation? = ServiceProviderRecommendation.NONE,

    @Convert(converter = ArrayConverter::class)
    @Column(name = "recommended_pic_ids")
    var recommendedPICIds: List<String> = mutableListOf(),

    var serviceProviderLanguageUrl: String? = null,
    var dpaApplicable: Boolean = false,
    var dpaUrl: String? = null,

    var helptextAdnetworkSharing: String? = null,
    var helptextIntentionalInteractionDisclosure: String? = null,

    @Enumerated(EnumType.STRING)
    var dataRecipientType: DataRecipientType = DataRecipientType.vendor,

    var lastReviewed: ZonedDateTime? = ZonedDateTime.now(),

    @Enumerated(EnumType.STRING)
    var gdprProcessorRecommendation: GDPRProcessorRecommendation? = null,
    var gdprProcessorLanguageUrl: String? = null,
    var gdprDpaApplicable: Boolean? = null,
    var gdprDpaUrl: String? = null,
    var gdprInternationalDataTransferRulesApply: Boolean? = null,
    var gdprProcessingPurpose: String? = null,
    var gdprLastReviewed: ZonedDateTime? = null,
    var gdprAccessInstructions: String? = null,
    var gdprHasSccRecommendation: Boolean = false,
    var gdprSccDocumentationUrl: String? = null,

    var gdprMultiPurposeVendor: Boolean = false,
    var gdprLawfulBasis: String? = null,

    @ManyToOne
    @JoinColumn(name = "organization_id")
    val organization: Organization? = null,

    @ManyToOne
    @JoinColumn(name = "service_type_id")
    var vendorClassification: VendorClassification? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_contract_id")
    var vendorContract: VendorContract? = null,

    @ManyToOne
    @JoinColumn(name = "vendor_contract_reviewed_id")
    var vendorContractReviewed: VendorContractReviewed? = null,

    @OneToMany(mappedBy = "vendor")
    var organizationDataRecipients: List<OrganizationDataRecipient> = emptyList(),

    var isPlatform: Boolean? = null,
    var isStandalone: Boolean? = null,

    var respectsPlatformRequests: Boolean = false,

    var isWalletService: Boolean = false,

    @ManyToMany
    @JoinTable(
        name = "vendor_default_retention_reasons",
        joinColumns = [JoinColumn(name = "vendor_id")],
        inverseJoinColumns = [JoinColumn(name = "retention_reason_id")]
    )
    var defaultRetentionReasons: MutableSet<RetentionReason> = mutableSetOf(),

    var deletedAt: ZonedDateTime? = null
) {
    fun disclosureSourceCategory(): String {
        return this.disclosureSourceCategory ?: this.recipientCategory ?: "Uncategorized"
    }

    fun disclosureRecipientCategory(): String {
        return this.recipientCategory ?: ""
    }

    fun disclosuresForCollectionGroup(collectionGroup: CollectionGroup): List<DataRecipientDisclosedPIC> {
        return collectionGroup.collectedPIC.flatMap { it.disclosedToVendors }.filter { it.vendor == this }
    }

    fun isKnownVendor(kvk: KnownVendorKey): Boolean {
        return vendorKey == kvk.key || name == kvk.vendorName
    }

    fun isKnownVendorByName(kvk: KnownVendorKey): Boolean {
        return name == kvk.vendorName
    }

    fun isPotentialIntentionalInteractionVendor(): Boolean {
        val isAdNetwork = isKnownCategory(KnownVendorCategories.AD_NETWORK)
        val hasServiceProviderRecommendation = serviceProviderRecommendation == ServiceProviderRecommendation.SERVICE_PROVIDER

        val isGoogleAnalytics = isKnownVendor(KnownVendorKey.GOOGLE_ANALYTICS)
        val isShopify = isKnownVendor(KnownVendorKey.SHOPIFY)

        val matchesRules = !isAdNetwork && !hasServiceProviderRecommendation && !isGoogleAnalytics && !isShopify

        return matchesRules || isKnownWalletService()
    }

    fun isAutomaticIntentionalInteractionVendor(): Boolean {
        return isPotentialIntentionalInteractionVendor() && isKnownWalletService()
    }

    fun isKnownWalletService() = this.isWalletService

    fun isPotentiallyExceptedFromSccs(): Boolean {
        return isKnownCategory(KnownVendorCategories.PAYMENT_PROCESSOR) || isKnownCategory(KnownVendorCategories.SHIPPING_SERVICE)
    }

    fun isKnownCategory(kvc: KnownVendorCategories): Boolean {
        return recipientCategory == kvc.category
    }

    fun isCustomVendor(): Boolean {
        return organization != null
    }

    fun isThirdParty(): Boolean = dataRecipientType == DataRecipientType.third_party_recipient && isCustomVendor()
    fun isContractor(): Boolean = dataRecipientType == DataRecipientType.contractor && isCustomVendor()
}

enum class DataRecipientType {
    third_party_recipient,
    contractor,
    storage_location,
    vendor
}

enum class GDPRProcessorRecommendation {
    Processor,
    Controller,
    Unknown
}
