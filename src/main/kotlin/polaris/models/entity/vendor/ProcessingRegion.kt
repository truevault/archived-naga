package polaris.models.entity.vendor

enum class ProcessingRegion {
    UNITED_STATES,
    EEA_UK
}
