package polaris.models.entity.vendor

enum class GdprProviderRecommendation {
    PROCESSOR,
    CONTROLLER,
    NONE
}
