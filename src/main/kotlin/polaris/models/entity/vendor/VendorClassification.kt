package polaris.models.entity.vendor

import polaris.models.dto.vendor.VendorClassificationDto
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

const val VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW = "needs-review"
const val VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER = "service-provider"
const val VENDOR_CLASSIFICATION_SLUG_CONTRACTOR = "contractor"
const val VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY = "third-party"

@Entity
@Table(name = "service_type")
data class VendorClassification(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    val slug: String? = null,
    val name: String? = null,
    val tooltipText: String? = null,
    val displayOrder: Int = 0
) {
    fun toDto() = VendorClassificationDto(
        id = id.toString(),
        name = name!!,
        slug = slug!!,
        tooltip = tooltipText,
        displayOrder = displayOrder
    )
}
