package polaris.models.entity.vendor

import polaris.models.dto.vendor.VendorContractReviewedDto
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "vendor_contract_reviewed")
data class VendorContractReviewed(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    val slug: String? = null,
    val name: String? = null,
    val tooltipText: String? = null,
    val displayOrder: Int = 0
) {
    fun toDto() = VendorContractReviewedDto(
        id = id.toString(),
        name = name!!,
        slug = slug!!,
        tooltip = tooltipText,
        displayOrder = displayOrder
    )
}
