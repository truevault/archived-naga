package polaris.models.entity.attachment

import polaris.models.entity.request.DataSubjectRequestEvent
import javax.persistence.*

@Entity
data class DataSubjectRequestEventAttachment(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "data_subject_request_event_id", updatable = false)
    val dataSubjectRequestEvent: DataSubjectRequestEvent? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "attachment_id", updatable = false)
    val attachment: Attachment? = null
)
