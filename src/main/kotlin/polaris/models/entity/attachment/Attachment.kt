package polaris.models.entity.attachment

import polaris.models.dto.attachment.AttachmentDto
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Attachment(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    val name: String = "",
    val mimeType: String = "",
    val size: Long = 0
) {
    fun toDto() = AttachmentDto(
        name = name,
        mimeType = mimeType,
        size = size,
        url = "/attachments/$id"
    )
}
