package polaris.models.entity

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import polaris.models.DataRequestPublicId
import polaris.models.OrganizationPublicId
import polaris.models.PrivacyCenterPublicId
import polaris.models.VendorId
import polaris.models.entity.organization.OrganizationAuthTokenProvider
import polaris.models.entity.request.DataSubjectRequest
import polaris.util.PolarisException
import java.util.*

@ResponseStatus(HttpStatus.NOT_FOUND)
class OrganizationNotFound(organizationId: OrganizationPublicId) :
    PolarisException("Could not find an organization '$organizationId'.") {
    constructor() : this("")
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class AuthorizationNotFound(organizationId: OrganizationPublicId, authorization: OrganizationAuthTokenProvider?) : PolarisException("Could not find authorization for $authorization on org $organizationId") {
    constructor() : this("", null)
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class OrganizationPhoneNumberNotFound(phoneNumber: String) :
    PolarisException("Could not find an organization '$phoneNumber'.") {
    constructor() : this("")
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class FeatureNotFound(name: String) :
    PolarisException("Could not find an feature '$name'.") {
    constructor() : this("")
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class OrganizationUserNotFound(email: String, organizationId: OrganizationPublicId) :
    PolarisException("Could not find a user '$email' for organization '$organizationId'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class OrganizationMailboxNotFound(mailbox: String, organizationId: OrganizationPublicId) :
    PolarisException("Could not find a mailbox '$mailbox' for organization '$organizationId'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class VendorNotFound(vendorId: VendorId) : PolarisException("Could not find vendor with id $vendorId.")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class PolarisCannotBeRemoved() :
    PolarisException("You aren't allowed the remove the TrueVault Polaris vendor.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class CollectionGroupNotFound(type: String) : PolarisException("Could not find the consumer group '$type'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class CookieNotFound(domain: String, name: String) : PolarisException("Could not find the cookie '$domain; $name'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class ProcessingActivityNotFound(id: String) : PolarisException("Could not find the processing activity '$id'.")

@ResponseStatus(HttpStatus.CONFLICT)
class ProcessingActivityExists(defaultId: String) : PolarisException("A processing activity already exists for default '$defaultId'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class UserNotFound : PolarisException("The user could not be found")

@ResponseStatus(HttpStatus.NOT_FOUND)
class DataRequestNotFound(dataRequestId: DataRequestPublicId, organizationId: OrganizationPublicId) :
    PolarisException("Could not find a data request '$dataRequestId' for organization '$organizationId'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class RequestHandlingInstructionNotFound(instructionId: UUID, organizationId: OrganizationPublicId) :
    PolarisException("Could not find a request instruction '$instructionId' for organization '$organizationId'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class CouldNotConfirm() :
    PolarisException("The request email could not be confirmed.")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestNoCorrectionRequest() :
    PolarisException("No correction request was provided.")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestNoObjectionRequest() :
    PolarisException("No objection was provided.")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestNoConsentProcessingActivities() :
    PolarisException("No processing activities were provided.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class ConfirmationTokenDoesNotMatch(token: UUID, dataRequestId: DataRequestPublicId) :
    PolarisException("Confirmation token '$token' does not match for request '$dataRequestId'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class MissingDeclarationOrCustomField(
    val dataRequest: DataSubjectRequest,
    val missingDeclaration: Boolean,
    val missingCustomField: Boolean,
    val customFieldLabel: String?,
    val customFieldHelp: String?
) :
    PolarisException("Missing data for Declaration ($missingDeclaration) or custom field ($missingCustomField) - ('$customFieldLabel') request '$dataRequest.publicId'.")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class MultipleVendorsNotAllowed() :
    PolarisException("Cannot send both a built-in vendor and a custom vendor")

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class ConsumerGroupsNotPresent(dataRequestId: DataRequestPublicId) :
    PolarisException("Consumer groups must be selected for request '$dataRequestId'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class VendorOutcomeNotFound(name: String, dataRequestId: DataRequestPublicId) :
    PolarisException("Could not find a vendor '$name' for request '$dataRequestId'.")

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class OrganizationUserNotDeletable(reason: String) : PolarisException(reason) {
    constructor(
        email: String,
        organizationId: OrganizationPublicId
    ) : this("The user '$email' could not be removed from the organization '$organizationId'.")

    constructor(
        email: String,
        organizationId: OrganizationPublicId,
        reason: String
    ) : this("The user '$email' could not be removed from the organization '$organizationId' because $reason.")
}

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class OrganizationUserAlreadyExists(reason: String) : PolarisException(reason) {
    constructor(
        email: String,
        organizationId: OrganizationPublicId
    ) : this("The user '$email' already exists on the organization '$organizationId'.")
}

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class VendorOutcomeOptionNotAllowed(reason: String) : PolarisException(reason)

@ResponseStatus(HttpStatus.NOT_FOUND)
class PrivacyCenterNotFound(privacyCenterId: PrivacyCenterPublicId) :
    PolarisException("Could not find privacy center '$privacyCenterId'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class CustomMessageTypeNotFound(slug: String) :
    PolarisException("Could not find custom message type '$slug'.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class TaskNotFound() : PolarisException("The task could not be found")
