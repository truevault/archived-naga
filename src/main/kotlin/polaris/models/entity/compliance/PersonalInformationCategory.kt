package polaris.models.entity.compliance

import polaris.models.dto.compliance.CategoryDto
import polaris.models.dto.compliance.PersonalInformationCategoryDto
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
data class PersonalInformationCategory(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    val name: String? = null,
    val description: String? = null,
    val picKey: String? = null,

    val deprecated: Boolean = false,
    val groupDisplayOrder: Int = 0,

    val isSensitive: Boolean = false,
    val alwaysPreserveCasing: Boolean = false,

    val isCcpa: Boolean = true,
    val isGdpr: Boolean = false,

    @ManyToOne
    @JoinColumn(name = "group_id", updatable = false)
    val group: PICGroup? = null
) {

    companion object {
        val COMMON_KEYS = listOf("name", "email-address", "postal-address", "phone-number", "credit-debit-card-number", "purchases")
        val SENSITIVE_KEYS = listOf(
            "ssn",
            "passport-number",
            "driver-license-number",
            "california-id-number",
            "military-id-number",
            "tax-id-number",
            "fingerprints",
            "face-hand-palm-vein-patterns",
            "voice-recordings",
            "dna",
            "iris-retina-imagery",
            "keystroke-patterns",
            "health-data",
            "financial-account-creds",
            "financial-account-number",
            "health-insurance-id",
            "account-login",
            "geolocation-data",
            "citizenship-status",
            "data-from-child-under-13"
        )
        val INDIRECT_CATEGORY_KEYS = listOf("online-identifiers", "geolocation", "internet-activity")

        val SENSITIVE_GROUP_KEYS = listOf("other-sensitive-data")

        val GDPR_KEYS = listOf("gdpr-genetics", "gdpr-data-revealing-beliefs", "gdpr-health-or-sex-data", "gdpr-criminal-offenses")
        val SUPPRESSED_GROUP_KEYS = listOf(
            "online-identifiers",
            "internet-activity",
            "geolocation",
            "inferences",
            "personal-identifiers"
        )
    }

    fun getClassification(): PICSurveyClassification {
        if (COMMON_KEYS.contains(picKey)) {
            return PICSurveyClassification.COMMON
        }

        if (SENSITIVE_KEYS.contains(picKey) || SENSITIVE_GROUP_KEYS.contains(group?.groupKey)) {
            return PICSurveyClassification.SENSITIVE
        }

        if (GDPR_KEYS.contains(picKey)) {
            return PICSurveyClassification.GDPR_SENSITIVE
        }

        if (INDIRECT_CATEGORY_KEYS.contains(group?.groupKey)) {
            return PICSurveyClassification.INDIRECT
        }

        if (SUPPRESSED_GROUP_KEYS.contains(group?.groupKey)) {
            return PICSurveyClassification.SUPPRESSED
        }

        return PICSurveyClassification.UNCOMMON
    }

    fun toCategoryDto(): CategoryDto {
        return CategoryDto(
            id = id.toString(),
            key = picKey,
            name = name!!,
            description = description,
            order = groupDisplayOrder,
            isSensitive = isSensitive,
            alwaysPreserveCasing = alwaysPreserveCasing,
            surveyClassification = getClassification(),

            picGroupId = group!!.id.toString(),
            picGroupName = group.name!!,
            picGroupOrder = group.displayOrder,
            picGroupEmployeeOrder = group.employeeDisplayOrder,
            picGroupKey = group.groupKey
        )
    }

    fun toDto(
        collected: Boolean?,
        exchanged: Boolean?
    ) =
        PersonalInformationCategoryDto(
            id = id.toString(),
            key = picKey,
            name = name!!,
            description = description,
            order = groupDisplayOrder,
            isSensitive = isSensitive,
            alwaysPreserveCasing = alwaysPreserveCasing,
            surveyClassification = getClassification(),

            groupId = group!!.id.toString(),
            groupName = group.name!!,
            groupOrder = group.displayOrder,
            groupEmployeeOrder = group.employeeDisplayOrder,

            collected = collected ?: false,
            exchanged = exchanged ?: false
        )

    fun toDto() = toDto(false, false)
}

enum class PICSurveyClassification {
    COMMON,
    SENSITIVE,
    INDIRECT,
    GDPR_SENSITIVE,
    UNCOMMON,
    SUPPRESSED
}
