package polaris.models.entity.compliance

import polaris.models.entity.organization.BusinessPurposeType
import polaris.models.entity.organization.CollectionContext

enum class CollectionGroupType(val label: String, val description: String, val businessPurposeType: BusinessPurposeType) {
    BUSINESS_TO_CONSUMER("Business-to-Consumer", "Business to Consumer", BusinessPurposeType.CONSUMER),
    BUSINESS_TO_BUSINESS("Business-to-Business", "Business to Business", BusinessPurposeType.CONSUMER),
    EMPLOYMENT("Employment", "Employment", BusinessPurposeType.EMPLOYMENT);

    fun isNotEmployment(): Boolean = !isEmployment()
    fun isEmployment(): Boolean = this == EMPLOYMENT

    fun context(): CollectionContext = if (isEmployment()) CollectionContext.EMPLOYEE else CollectionContext.CONSUMER
}
