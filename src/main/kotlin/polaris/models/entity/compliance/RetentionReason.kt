package polaris.models.entity.compliance

import polaris.models.dto.compliance.RetentionReasonDto
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class RetentionReason(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    val reason: String = "",
    val slug: String = "",
    val description: String = "",
    val displayOrder: Int = 0
) {
    fun toDto() = RetentionReasonDto(
        id = id.toString(),
        reason = reason,
        slug = slug,
        description = description,
        displayOrder = displayOrder
    )
}
