package polaris.models.entity.compliance

enum class PersonalInformationType {
    CATEGORY,
    SOURCE,
    PURPOSE
}
