package polaris.models.entity.compliance

import polaris.models.dto.compliance.PICGroupDto
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "pic_group")
data class PICGroup(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    val name: String? = null,
    val groupKey: String? = null,
    val displayOrder: Int = 0,
    val employeeDisplayOrder: Int = 0,
    val useForNoticeDisclosure: Boolean = true
) {
    fun toPicGroupDto(): PICGroupDto {
        return PICGroupDto(
            id = id.toString(),
            name = name!!,
            key = groupKey,
            order = displayOrder,
            employeeOrder = employeeDisplayOrder
        )
    }
}
