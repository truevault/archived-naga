package polaris.models.entity.compliance

import polaris.models.dto.compliance.BusinessPurposeDto
import java.util.*
import javax.persistence.*

@Entity
data class BusinessPurpose(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    val name: String? = null,
    val description: String? = null,
    val displayOrder: Int = 0
) {
    fun toDto(enabled: Boolean?) = BusinessPurposeDto(
        id = id.toString(),
        name = name!!,
        description = description,
        enabled = enabled ?: false,
        displayOrder = displayOrder
    )

    fun toDto() = toDto(false)
}
