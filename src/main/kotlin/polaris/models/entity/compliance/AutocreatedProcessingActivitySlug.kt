package polaris.models.entity.compliance

enum class AutocreatedProcessingActivitySlug(val defaultLabel: String) {
    AUTOMATED_DECISION_MAKING("Automated Decision-Making"),
    COLD_SALES_COMMUNICATIONS("Cold Sales Communications")
}
