package polaris.models.entity.compliance

import polaris.models.entity.vendor.ProcessingRegion

enum class AutocreatedCollectionGroupSlug(val defaultLabel: String, val description: String, val type: CollectionGroupType, val processingRegions: List<ProcessingRegion>) {
    ONLINE_SHOPPERS("Consumers", "purchase our products or visit our website", CollectionGroupType.BUSINESS_TO_CONSUMER, listOf(ProcessingRegion.UNITED_STATES, ProcessingRegion.EEA_UK)),
    B2B_PROSPECTS("B2B Prospects", "appear on contact lists we purchase from third parties", CollectionGroupType.BUSINESS_TO_BUSINESS, listOf(ProcessingRegion.UNITED_STATES)),
    CA_EMPLOYEES("California Employees", "", CollectionGroupType.EMPLOYMENT, listOf(ProcessingRegion.UNITED_STATES)),
    CA_CONTRACTORS("California Contractors", "", CollectionGroupType.EMPLOYMENT, listOf(ProcessingRegion.UNITED_STATES)),
    CA_JOB_APPLICANTS("California Job Applicants", "", CollectionGroupType.EMPLOYMENT, listOf(ProcessingRegion.UNITED_STATES)),
    EEA_UK_EMPLOYEE("EEA/UK Employees", "", CollectionGroupType.EMPLOYMENT, listOf(ProcessingRegion.EEA_UK)),
    BUSINESS_CONTACTS("Business Contacts", "use and communicate with us about our services", CollectionGroupType.BUSINESS_TO_BUSINESS, listOf(ProcessingRegion.EEA_UK))
}
