package polaris.models.entity.cookieRecommendation

import org.hibernate.annotations.SQLDelete
import org.hibernate.annotations.Where
import polaris.models.dto.cookiescan.CookieRecommendationDto
import polaris.models.entity.organization.CookieCategory
import polaris.models.entity.vendor.Vendor
import polaris.util.converters.EnumListConverter
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Converter
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
@Where(clause = "deleted_at is null")
@SQLDelete(sql = "UPDATE cookie_recommendation SET deleted_at = NOW() WHERE id=?")
data class CookieRecommendation(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    var name: String = "",
    var domain: String = "",

    @Convert(converter = CookieCategoryConverter::class)
    var categories: List<CookieCategory> = emptyList(),

    @ManyToOne
    @JoinColumn(name = "vendor_id")
    var vendor: Vendor? = null,

    var notes: String = ""
) {
    fun toDto(): CookieRecommendationDto {
        return CookieRecommendationDto(
            id = id,
            name = name,
            domain = domain,
            categories = categories,
            vendorId = vendor?.id,
            notes = notes
        )
    }
}

@Converter
class CookieCategoryConverter :
    EnumListConverter<CookieCategory>(CookieCategory::class.java)
