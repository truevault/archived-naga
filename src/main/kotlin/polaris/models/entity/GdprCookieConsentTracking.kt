package polaris.models.entity

import polaris.models.entity.organization.Organization
import java.time.ZonedDateTime
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
data class GdprCookieConsentTracking(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @Column(name = "ip_v4")
    val ipV4: String? = null,
    @Column(name = "ip_v6")
    val ipV6: String? = null,
    val sourceUrl: String,
    val userAgent: String? = null,
    val key: String,
    val consentState: String,

    val createdAt: ZonedDateTime = ZonedDateTime.now()
)

data class GdprCookieConsentState(
    val analyticsPermitted: Boolean,
    val personalizationPermitted: Boolean,
    val adsPermitted: Boolean,
    val essentialPermitted: Boolean
)
