package polaris.models.entity.regulation

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Regulation(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),
    val slug: String = "",
    val name: String = "",
    val longName: String = "",
    val description: String = ""
)
