package polaris.models.entity.feature

import java.time.ZonedDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Feature(
    @Id
    @Column(updatable = false)
    val id: UUID = UUID.randomUUID(),

    var name: String? = "",
    var description: String? = "",
    var enabledByDefault: Boolean? = false,
    var startTs: ZonedDateTime? = ZonedDateTime.now(),
    var endTs: ZonedDateTime? = null
)

object Features {
    const val LEGACY_ORG = "IsLegacyOrg"
    const val LEGACY_TRANSITION = "LegacyCustomerTransition"
    const val SETUP_PHASE = "SetupPhase"
    const val GET_COMPLIANT_PHASE = "GetCompliantPhase"
    const val GDPR = "GDPR"
    const val MULTISTATE = "Multistate"
}
