package polaris.models.entity.feature

import polaris.models.entity.organization.Organization
import javax.persistence.*

@Entity
data class OrganizationFeature(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    val id: Int = 0,

    @ManyToOne
    @JoinColumn(name = "organization_id", updatable = false)
    val organization: Organization? = null,

    @ManyToOne
    @JoinColumn(name = "feature_id", updatable = false)
    val feature: Feature? = null,

    var enabled: Boolean = false
)
