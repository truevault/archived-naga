package polaris.models.listeners

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.support.TransactionSynchronization
import org.springframework.transaction.support.TransactionSynchronizationAdapter
import org.springframework.transaction.support.TransactionSynchronizationManager
import polaris.models.entity.organization.CollectionGroupCollectedPIC
import polaris.services.Hook
import polaris.services.HookArgument
import polaris.services.primary.hooks.RebuildPrivacyCenterHook
import polaris.services.primary.hooks.SetUncommonCollectionAnswerOnCollectionChange
import javax.persistence.PostPersist
import javax.persistence.PostRemove

@Component
class CollectionGroupCollectedPICEntityListener {
    companion object {
        private var setUncommonCollectionAnswerHook: SetUncommonCollectionAnswerOnCollectionChange? = null
        private var rebuildPrivacyCenterHook: RebuildPrivacyCenterHook? = null
    }

    @Autowired
    fun setDependencies(
        setUncommonCollectionAnswerHook: SetUncommonCollectionAnswerOnCollectionChange,
        rebuildPrivacyCenterHook: RebuildPrivacyCenterHook
    ) {
        CollectionGroupCollectedPICEntityListener.setUncommonCollectionAnswerHook = setUncommonCollectionAnswerHook
        CollectionGroupCollectedPICEntityListener.rebuildPrivacyCenterHook = rebuildPrivacyCenterHook
    }

    @PostPersist
    fun postPersist(collectedPic: CollectionGroupCollectedPIC) {
        runSetUncommonCollectionAnswerHook(collectedPic)
        runRebuildPrivacyCenterHook(Hook.COLLECTED_PIC_CREATED, collectedPic)
    }

    @PostRemove
    fun postRemove(collectedPic: CollectionGroupCollectedPIC) {
        runRebuildPrivacyCenterHook(Hook.COLLECTED_PIC_DELETED, collectedPic)
    }

    private fun runSetUncommonCollectionAnswerHook(collectedPIC: CollectionGroupCollectedPIC) {
        TransactionSynchronizationManager.registerSynchronization(object : TransactionSynchronizationAdapter() {
            override fun afterCommit() {
                if (collectedPIC.collectionGroup?.organization == null || collectedPIC.pic == null) {
                    return
                }
                setUncommonCollectionAnswerHook?.run(
                    HookArgument.CollectedPicCreated(
                        collectedPIC.collectionGroup.organization,
                        collectedPIC.pic,
                        collectedPIC.collectionGroup
                    )
                )
            }
        })
    }

    private fun runRebuildPrivacyCenterHook(hook: Hook, collectedPic: CollectionGroupCollectedPIC) {
        TransactionSynchronizationManager.registerSynchronization(object : TransactionSynchronization {
            override fun afterCommit() {
                rebuildPrivacyCenterHook?.run(hook, collectedPic)
            }
        })
    }
}
