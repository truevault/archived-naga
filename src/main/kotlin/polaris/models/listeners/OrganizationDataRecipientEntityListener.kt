package polaris.models.listeners

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.support.TransactionSynchronizationAdapter
import org.springframework.transaction.support.TransactionSynchronizationManager
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.services.HookArgument
import polaris.services.primary.hooks.SetupInitialRequestHandlingInstructionsForOrgDataRecipient
import javax.persistence.PostPersist

@Component
class OrganizationDataRecipientEntityListener {
    companion object {
        private var requestHandlingInstructionHook: SetupInitialRequestHandlingInstructionsForOrgDataRecipient? = null
    }

    @Autowired
    fun setDependencies(requestHandlingInstructionHook: SetupInitialRequestHandlingInstructionsForOrgDataRecipient) {
        OrganizationDataRecipientEntityListener.requestHandlingInstructionHook = requestHandlingInstructionHook
    }

    @PostPersist
    fun postPersist(dataRecipient: OrganizationDataRecipient) {
        runRequestHandlingInstructionsHook(dataRecipient)
    }

    private fun runRequestHandlingInstructionsHook(dataRecipient: OrganizationDataRecipient) {
        TransactionSynchronizationManager.registerSynchronization(object : TransactionSynchronizationAdapter() {
            override fun afterCommit() {
                if (dataRecipient.organization == null) {
                    return
                }

                requestHandlingInstructionHook?.run(
                    HookArgument.OrganizationDataRecipientCreated(
                        dataRecipient.organization,
                        dataRecipient
                    )
                )
            }
        })
    }
}
