package polaris.models.listeners

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.support.TransactionSynchronizationAdapter
import org.springframework.transaction.support.TransactionSynchronizationManager
import polaris.models.entity.organization.CollectionGroup
import polaris.services.Hook
import polaris.services.primary.hooks.RebuildPrivacyCenterHook
import javax.persistence.PostRemove
import javax.persistence.PostUpdate

@Component
class CollectionGroupEntityListener {
    companion object {
        private var rebuildPrivacyCenterHook: RebuildPrivacyCenterHook? = null
    }

    @Autowired
    fun setDependencies(rebuildPrivacyCenterHook: RebuildPrivacyCenterHook) {
        CollectionGroupEntityListener.rebuildPrivacyCenterHook = rebuildPrivacyCenterHook
    }

    @PostUpdate
    fun postUpdate(collectionGroup: CollectionGroup) {
        runRebuildPrivacyCenterHook(Hook.COLLECTION_GROUP_UPDATED, collectionGroup)
    }

    @PostRemove
    fun postRemove(collectionGroup: CollectionGroup) {
        runRebuildPrivacyCenterHook(Hook.COLLECTION_GROUP_DELETED, collectionGroup)
    }

    private fun runRebuildPrivacyCenterHook(hook: Hook, collectionGroup: CollectionGroup) {
        if (rebuildPrivacyCenterHook == null) return

        TransactionSynchronizationManager.registerSynchronization(object : TransactionSynchronizationAdapter() {
            override fun afterCommit() {
                rebuildPrivacyCenterHook!!.run(hook, collectionGroup)
            }
        })
    }
}
