package polaris.models.listeners

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.support.TransactionSynchronizationAdapter
import org.springframework.transaction.support.TransactionSynchronizationManager
import polaris.models.entity.request.DataSubjectRequest
import polaris.services.HookArgument
import polaris.services.primary.hooks.AutoCloseDuplicateDataSubjectRequests
import javax.persistence.PostPersist

@Component
class DataSubjectRequestEntityListener {
    companion object {
        private var autoCloseDuplicatesHook: AutoCloseDuplicateDataSubjectRequests? = null
    }

    @Autowired
    fun setDependencies(autoCloseDuplicatesHook: AutoCloseDuplicateDataSubjectRequests) {
        DataSubjectRequestEntityListener.autoCloseDuplicatesHook = autoCloseDuplicatesHook
    }

    @PostPersist
    fun postPersist(request: DataSubjectRequest) {
        runAutoCloseDuplicatesHook(request)
    }

    private fun runAutoCloseDuplicatesHook(request: DataSubjectRequest) {
        TransactionSynchronizationManager.registerSynchronization(object : TransactionSynchronizationAdapter() {
            override fun beforeCommit(readOnly: Boolean) {
                autoCloseDuplicatesHook?.run(HookArgument.DataSubjectRequestCreated(request))
            }
        })
    }
}
