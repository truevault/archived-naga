package polaris.scheduled

import net.javacrumbs.shedlock.core.LockAssert
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE_STR
import polaris.services.primary.DataSubjectRequestService
import polaris.services.primary.DigestService
import polaris.services.primary.QueuedEmailService
import polaris.services.support.EmailService

@Component
class EmailScheduledTasks(
    private val requestsService: DataSubjectRequestService,
    private val queuedEmailService: QueuedEmailService,
    private val digestService: DigestService,
    private val emailService: EmailService
) {

    companion object {
        private val log = LoggerFactory.getLogger(RequestScheduledTasks::class.java)
    }
    //   s  m  h d w m
    @Scheduled(cron = "0 0/5 * * * *", zone = POLARIS_TIME_ZONE_STR)
    @SchedulerLock(name = "sendEmails", lockAtLeastFor = "3m")
    fun sendEmails() {
        LockAssert.assertLocked()
        log.info("[scheduled] send email]")

        queuedEmailService.findAndProcessEmails()
    }

    // Weekly at 7am PTZ on Monday Morning
    @Scheduled(cron = "0 0 7 * * 1", zone = POLARIS_TIME_ZONE_STR)
    @SchedulerLock(name = "sendWeeklyDigests", lockAtLeastFor = "3m")
    fun sendWeeklyDigests() {
        LockAssert.assertLocked()
        log.info("[scheduled] send weekly snapshot")

        digestService.sendAllWeeklyDigestEmails()
    }
}
