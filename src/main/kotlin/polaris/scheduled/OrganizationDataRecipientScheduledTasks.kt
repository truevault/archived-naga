package polaris.scheduled

import net.javacrumbs.shedlock.core.LockAssert
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE_STR
import polaris.services.primary.OrganizationVendorService

@Component
class OrganizationDataRecipientScheduledTasks(
    private val organizationVendorService: OrganizationVendorService
) {

    companion object {
        private val log = LoggerFactory.getLogger(OrganizationDataRecipientScheduledTasks::class.java)
    }

    // Every day at 3 am
    @Scheduled(cron = "0 3 * * * *", zone = POLARIS_TIME_ZONE_STR)
    @SchedulerLock(name = "moveOutdatedUnknownOrgDataRecipientsToController", lockAtLeastFor = "3m")
    fun moveOutdatedUnknownOrgDataRecipientsToController() {
        LockAssert.assertLocked()

        log.info("[scheduled] moving gdpr unknown processor org data recipients to controller]")
        organizationVendorService.moveOutdatedUnknownOrgDataRecipientsToController()
    }
}
