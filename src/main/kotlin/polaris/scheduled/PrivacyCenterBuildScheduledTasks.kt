package polaris.scheduled

import net.javacrumbs.shedlock.core.LockAssert
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE_STR
import polaris.services.primary.CmpConfigService
import polaris.services.support.PrivacyCenterBuilderService

@Component
class PrivacyCenterBuildScheduledTasks(
    private val privacyCenterBuilderService: PrivacyCenterBuilderService,
    private val cmpConfigService: CmpConfigService
) {

    companion object {
        private val log = LoggerFactory.getLogger(PrivacyCenterBuildScheduledTasks::class.java)
    }
    //   s  m  h d w m
    @Scheduled(cron = "30 * * * * *", zone = POLARIS_TIME_ZONE_STR)
    @SchedulerLock(name = "sendPrivacyCenterBuilds", lockAtLeastFor = "10s")
    fun sendPrivacyCenterBuilds() {
        LockAssert.assertLocked()

        log.info("[scheduled] send privacy center builds")
        privacyCenterBuilderService.sendBuilds()
    }

    @Scheduled(cron = "0 3 * * * *", zone = POLARIS_TIME_ZONE_STR)
    @SchedulerLock(name = "updatePolarisCmpConfigs", lockAtLeastFor = "1m")
    fun updatePolarisCmpConfigs() {
        LockAssert.assertLocked()

        log.info("[scheduled] update all polaris CMP configs")
        cmpConfigService.publishForAllOrgs()
    }
}
