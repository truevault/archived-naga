package polaris.scheduled

import net.javacrumbs.shedlock.core.LockAssert
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE_STR
import polaris.services.support.email.MailboxService

@Component
class NylasScheduledTasks(
    private val mailboxService: MailboxService
) {

    companion object {
        private val log = LoggerFactory.getLogger(NylasScheduledTasks::class.java)
    }

    //   s m h d w m
    @Scheduled(cron = "0 0 */1 * * *", zone = POLARIS_TIME_ZONE_STR)
    @SchedulerLock(name = "pollNylasMailboxes", lockAtLeastFor = "3m")
    fun pollNylasMailboxes() {
        LockAssert.assertLocked()

        log.info("[scheduled] polling nylas mailboxes...")

        mailboxService.pollNylasMailboxes()
    }
}
