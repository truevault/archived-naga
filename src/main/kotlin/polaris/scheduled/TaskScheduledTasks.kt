package polaris.scheduled

import net.javacrumbs.shedlock.core.LockAssert
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE_STR
import polaris.services.primary.TaskService

@Component
class TaskScheduledTasks(
    private val tasksService: TaskService
) {
    companion object {
        private val log = LoggerFactory.getLogger(TaskScheduledTasks::class.java)
    }

    //   s m h d w m
    @Scheduled(cron = "0 0 1 * * *", zone = POLARIS_TIME_ZONE_STR)
    @SchedulerLock(name = "autoCreateTasks", lockAtLeastFor = "3m")
    fun autoCreateTasks() {
        LockAssert.assertLocked()
        log.info("[scheduled] auto close/create tasks")

        tasksService.autoCloseTasks()
        tasksService.autoCreateTasks()
    }
}
