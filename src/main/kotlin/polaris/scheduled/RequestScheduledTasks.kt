package polaris.scheduled

import net.javacrumbs.shedlock.core.LockAssert
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE_STR
import polaris.services.primary.DataSubjectRequestAutoCloseService

@Component
class RequestScheduledTasks(
    private val requestAutoCloseService: DataSubjectRequestAutoCloseService
) {

    companion object {
        private val log = LoggerFactory.getLogger(RequestScheduledTasks::class.java)
    }

    //   s m  h d w m
    @Scheduled(cron = "0 15 * * * *", zone = POLARIS_TIME_ZONE_STR)
    @SchedulerLock(name = "autoCloseRequests", lockAtLeastFor = "3m")
    fun autoCloseRequests() {
        LockAssert.assertLocked()
        log.info("[scheduled] auto closing requests...")
        requestAutoCloseService.autoCloseRequests()
    }
}
