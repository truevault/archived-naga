package polaris.controllers.admin

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import polaris.models.dto.cookiescan.CookieRecommendationAdminDto
import polaris.models.entity.cookieRecommendation.CookieRecommendation
import polaris.repositories.CookieRecommendationRepository
import polaris.services.dto.CookieRecommendationDtoService
import java.util.*

@RestController
@RequestMapping("/admin/cookies")
class CookieRecommendationAdminController(
    repo: CookieRecommendationRepository,
    val dtoService: CookieRecommendationDtoService
) : BaseAdminController<CookieRecommendation, UUID, CookieRecommendationAdminDto, CookieRecommendationAdminDto, CookieRecommendationRepository>(repo) {
    override fun toDto(resource: CookieRecommendation) = dtoService.toAdminDto(resource)
    override fun apply(dto: CookieRecommendationAdminDto, resource: CookieRecommendation) = dtoService.applyAdminDto(dto, resource)
    override fun construct() = CookieRecommendation()
    override fun label(resource: CookieRecommendation) = resource.name
}
