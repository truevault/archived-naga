package polaris.controllers.admin

import org.slf4j.LoggerFactory
import org.springframework.data.repository.CrudRepository
import org.springframework.web.bind.annotation.*
import polaris.controllers.ResourceNotFoundException
import polaris.controllers.util.SessionAwareController

abstract class BaseAdminController<T, TID, OutDT, InDT, RT>(
    private val repository: RT
) : SessionAwareController() where RT : CrudRepository<T, TID> {
    companion object {
        private val log = LoggerFactory.getLogger(BaseAdminController::class.java)
    }

    @GetMapping("/")
    protected fun index(): List<OutDT> {
        requireGlobalAdmin()

        return repository.findAll()
            .sortedBy { label(it) }
            .map { toDto(it) }
    }

    @GetMapping("/{id}")
    protected fun read(@PathVariable id: TID): OutDT {
        requireGlobalAdmin()
        return toDto(get(id))
    }

    @PostMapping("/")
    protected fun create(@RequestBody dto: InDT): OutDT {
        requireGlobalAdmin()
        val resource = construct(dto)
        val created = repository.save(resource)
        afterCreate(created)
        return toDto(created)
    }

    @PutMapping("/{id}")
    protected fun update(@PathVariable id: TID, @RequestBody dto: InDT): OutDT {
        requireGlobalAdmin()
        val resource = apply(dto, get(id))
        val updated = repository.save(resource)
        afterUpdate(updated)
        return toDto(updated)
    }

    @DeleteMapping("/{id}")
    protected fun destroy(@PathVariable id: TID) {
        requireGlobalAdmin()
        val resource = get(id)
        if (allowDelete(resource)) {
            beforeDelete(resource)
            repository.deleteById(id)
        }
    }

    protected fun get(id: TID): T {
        val optResource = repository.findById(id)
        if (optResource.isEmpty) {
            throw ResourceNotFoundException(id.toString())
        }

        return optResource.get()
    }

    protected abstract fun toDto(resource: T): OutDT
    protected abstract fun apply(dto: InDT, resource: T): T
    protected fun construct(dto: InDT): T {
        return apply(dto, construct())
    }
    protected abstract fun construct(): T

    protected fun afterCreate(resource: T) {}
    protected fun afterUpdate(resource: T) {}
    protected fun beforeDelete(resource: T) {}
    protected fun allowDelete(resource: T) = false

    protected fun label(resource: T): String? = null
}

data class AdminRelationshipDto(
    val id: String,
    val label: String
)
