package polaris.controllers.admin

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import polaris.models.UUIDString
import polaris.models.entity.privacycenter.CustomUrlStatus
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.repositories.PrivacyCenterRepository
import polaris.services.dto.PrivacyCenterDtoService
import polaris.services.support.PrivacyCenterBuilderService
import java.util.*
import javax.persistence.EnumType
import javax.persistence.Enumerated

@RestController @RequestMapping("/admin/privacyCenters")
class PrivacyCenterAdminController(
    repo: PrivacyCenterRepository,
    val dtoService: PrivacyCenterDtoService,
    val privacyCenterBuilderService: PrivacyCenterBuilderService
) : BaseAdminController<PrivacyCenter, UUID, PrivacyCenterAdminDto, PrivacyCenterAdminDto, PrivacyCenterRepository>(repo) {
    override fun toDto(resource: PrivacyCenter) = dtoService.toAdminDto(resource)
    override fun apply(dto: PrivacyCenterAdminDto, resource: PrivacyCenter) = dtoService.applyAdminDto(dto, resource)
    override fun construct() = PrivacyCenter()
    override fun afterCreate(resource: PrivacyCenter) = privacyCenterBuilderService.insertPrivacyCenterBuildRequest(resource, "privacy center admin create")
    override fun afterUpdate(resource: PrivacyCenter) = privacyCenterBuilderService.insertPrivacyCenterBuildRequest(resource, "privacy center admin update")

    override fun label(resource: PrivacyCenter) = resource.organization?.name
}

data class PrivacyCenterAdminDto(
    val id: UUIDString? = null,
    val publicId: String? = null,
    val name: String = "",

    val organization: AdminRelationshipDto? = null,

    val defaultSubdomain: String = "",
    val faviconUrl: String = "",

    @Enumerated(EnumType.STRING)
    val customUrlStatus: CustomUrlStatus? = null,
    val customUrl: String? = null,

    val cloudfrontDistribution: String? = null,

    val logoUrl: String? = null,
    val privacyPolicyUrl: String? = null,
    val requestTollFreePhoneNumber: String? = null,
    val requestFormUrl: String? = null,
    val requestEmail: String? = null
)
