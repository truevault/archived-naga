package polaris.controllers.admin

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import polaris.models.UUIDString
import polaris.models.entity.organization.DefaultProcessingActivity
import polaris.models.entity.organization.LawfulBasis
import polaris.repositories.DefaultProcessingActivityRepo
import polaris.services.dto.ProcessingActivityDtoService
import java.util.*

@RestController
@RequestMapping("/admin/defaultProcessingActivities")
class DefaultProcessingActivityController(
    val repo: DefaultProcessingActivityRepo,
    val dtoService: ProcessingActivityDtoService
) : BaseAdminController<DefaultProcessingActivity, UUID, DefProcessingActivityAdminDto, DefProcessingActivityAdminDto, DefaultProcessingActivityRepo>(repo) {
    override fun toDto(resource: DefaultProcessingActivity) = dtoService.defaultToAdminDto(resource)
    override fun apply(dto: DefProcessingActivityAdminDto, resource: DefaultProcessingActivity) = dtoService.applyAdminDto(dto, resource)
    override fun construct() = DefaultProcessingActivity()

    override fun label(resource: DefaultProcessingActivity) = resource.name

    @GetMapping("/")
    override fun index(): List<DefProcessingActivityAdminDto> {
        requireGlobalAdmin()

        val comparator = compareBy<DefaultProcessingActivity, Int?>(nullsLast()) { it.displayOrder }.thenBy { it.name }

        return repo.findAll()
            .sortedWith(comparator)
            .map { toDto(it) }
    }
}

data class DefProcessingActivityAdminDto(
    val id: UUIDString? = null,
    val name: String = "",
    val slug: String? = null,
    var materialIconKey: String? = null,
    val displayOrder: Int? = null,
    val deprecated: Boolean = false,
    val helpText: String? = null,
    val mandatoryLawfulBases: List<LawfulBasis> = emptyList(),
    val optionalLawfulBases: List<LawfulBasis> = emptyList()
)
