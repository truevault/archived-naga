package polaris.controllers.admin

import com.amazonaws.AmazonServiceException
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import polaris.models.UUIDString
import polaris.models.dto.toIso
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.models.entity.vendor.ServiceProviderRecommendation
import polaris.models.entity.vendor.Vendor
import polaris.repositories.VendorRepository
import polaris.services.dto.VendorDtoService
import polaris.services.support.BlockStorageService
import java.util.UUID

@RestController
@RequestMapping("/admin/vendors")
class VendorAdminController(
    val repo: VendorRepository,
    val dtoService: VendorDtoService,
    val blockStorageService: BlockStorageService
) : BaseAdminController<Vendor, UUID, VendorAdminDto, VendorAdminInboundDto, VendorRepository>(repo) {

    companion object {
        private val log = LoggerFactory.getLogger(VendorAdminController::class.java)
    }

    override fun toDto(resource: Vendor) = dtoService.toAdminDto(resource)
    override fun apply(dto: VendorAdminInboundDto, resource: Vendor) = dtoService.applyAdminDto(dto, resource)
    override fun construct() = Vendor(name = "temp")

    override fun label(resource: Vendor) = resource.name

    @GetMapping("/")
    override fun index(): List<VendorAdminDto> {
        requireGlobalAdmin()

        return repo.findAllByOrganizationIsNull()
            .sortedBy { label(it) }
            .map { toAdminIndexDto(it) }
    }

    @GetMapping("/new")
    fun new(): VendorAdminNewDto {
        return VendorAdminNewDto(
            categories = repo.findDistinctRecipientCategories(null),
            existingNames = repo.findDistinctNames(null)
        )
    }

    @PutMapping("/{id}")
    override fun update(@PathVariable id: UUID, @RequestBody dto: VendorAdminInboundDto): VendorAdminDto {
        requireGlobalAdmin()
        val service = super.get(id)
        if (service.logoFilename != null && dto.logoFilename != service.logoFilename) {
            try {
                blockStorageService.deleteImage(service.logoFilename!!)
            } catch (e: AmazonServiceException) {
                log.warn("Failed to delete logo with filename ${service.logoFilename}")
            }
        }
        return super.update(id, dto)
    }

    @PostMapping("/logo")
    fun postLogo(
        @RequestParam("logo") logo: MultipartFile
    ): Map<String, String> {
        requireGlobalAdmin()

        val logoFilename = generateFilename(blockStorageService.getFileExtension(logo.originalFilename ?: ""))
        try {
            blockStorageService.putImage(logoFilename, logo)
        } catch (e: AmazonServiceException) {
            log.warn("Failed to put image $logoFilename in block storage: $e")
            return mapOf("error" to e.toString())
        }
        return mapOf("logoFilename" to logoFilename, "logoUrl" to dtoService.logoUrl(logoFilename)!!)
    }

    private fun generateFilename(extension: String?): String {
        val filename = "${UUID.randomUUID()}" + if (!extension.isNullOrBlank()) ".$extension" else ""
        return getFullPath(filename)
    }

    private fun getFullPath(sourceFilename: String): String {
        return if (sourceFilename.startsWith("logos/")) sourceFilename else "logos/services/$sourceFilename"
    }

    private fun toAdminIndexDto(vendor: Vendor) = VendorAdminDto(
        id = vendor.id.toString(),
        name = vendor.name,
        category = vendor.recipientCategory,
        disclosureSourceCategory = vendor.disclosureSourceCategory,
        url = vendor.url,
        email = vendor.email,
        serviceProviderRecommendation = vendor.serviceProviderRecommendation,
        serviceProviderLanguageUrl = vendor.serviceProviderLanguageUrl,
        deletionInstructions = vendor.deletionInstructions,
        optOutInstructions = vendor.optOutInstructions,
        generalGuidance = vendor.generalGuidance,
        specialCircumstancesGuidance = vendor.specialCircumstancesGuidance,
        dpaApplicable = vendor.dpaApplicable,
        dpaUrl = vendor.dpaUrl,
        logoFilename = vendor.logoFilename,
        logoUrl = dtoService.logoUrl(vendor.logoFilename),
        lastReviewed = vendor.lastReviewed?.toIso(),
        isDataRecipient = vendor.isDataRecipient,
        isDataSource = vendor.isDataSource,
        isPlatform = vendor.isPlatform ?: false,
        isStandalone = vendor.isStandalone ?: false,
        respectsPlatformRequests = vendor.respectsPlatformRequests,
        isMigrationOnly = vendor.isMigrationOnly,
        gdprDpaApplicable = vendor.gdprDpaApplicable,
        gdprDpaUrl = vendor.gdprDpaUrl,
        gdprInternationalDataTransferRulesApply = vendor.gdprInternationalDataTransferRulesApply,
        gdprProcessorLanguageUrl = vendor.gdprProcessorLanguageUrl,
        gdprProcessingPurpose = vendor.gdprProcessingPurpose,
        gdprProcessorRecommendation = vendor.gdprProcessorRecommendation,
        gdprLastReviewed = vendor.gdprLastReviewed?.toIso(),
        gdprAccessInstructions = vendor.gdprAccessInstructions,
        gdprMultiPurposeVendor = vendor.gdprMultiPurposeVendor,
        gdprLawfulBasis = vendor.gdprLawfulBasis,
        gdprHasSccRecommendation = vendor.gdprHasSccRecommendation,
        gdprSccDocumentationUrl = vendor.gdprSccDocumentationUrl,
        isWalletService = vendor.isWalletService
    )
}

data class VendorAdminDto(
    val id: UUIDString? = null,
    val vendorKey: String? = null,
    val aliases: String? = null,
    val name: String? = null,
    val category: String? = null,
    val disclosureSourceCategory: String? = null,
    val url: String? = null,
    val ccpaRequestToDeleteLink: String? = null,
    val deletionInstructions: String? = null,
    val optOutInstructions: String? = null,
    val generalGuidance: String? = null,
    val specialCircumstancesGuidance: String? = null,
    val ccpaRequestToKnowLink: String? = null,
    val email: String? = null,
    val serviceProviderRecommendation: ServiceProviderRecommendation? = null,
    val serviceProviderLanguageUrl: String? = null,
    val dpaApplicable: Boolean = false,
    val dpaUrl: String? = null,
    val logoFilename: String? = null,
    val logoUrl: String? = null,
    val lastReviewed: String?,
    val recommendedPersonalInformationCategories: List<String> = mutableListOf(),
    val isDataRecipient: Boolean,
    val isDataSource: Boolean,
    val isPlatform: Boolean,
    val isStandalone: Boolean,
    var respectsPlatformRequests: Boolean = false,
    val isMigrationOnly: Boolean,

    var gdprProcessorRecommendation: GDPRProcessorRecommendation? = null,
    var gdprProcessorLanguageUrl: String? = null,
    var gdprDpaApplicable: Boolean? = null,
    var gdprDpaUrl: String? = null,
    var gdprInternationalDataTransferRulesApply: Boolean? = null,
    var gdprProcessingPurpose: String? = null,
    var gdprLastReviewed: String? = null,
    var gdprAccessInstructions: String? = null,
    var gdprMultiPurposeVendor: Boolean = false,
    var gdprLawfulBasis: String? = null,
    var gdprHasSccRecommendation: Boolean = false,
    var gdprSccDocumentationUrl: String? = null,

    var isWalletService: Boolean = false,

    val vendorClassification: AdminRelationshipDto? = null,

    val defaultRetentionReasons: List<UUIDString> = mutableListOf(),

    val existingNames: List<String> = mutableListOf(),
    val categories: List<String> = mutableListOf()
)

data class VendorAdminInboundDto(
    val id: UUIDString? = null,
    val vendorKey: String? = null,
    val aliases: String? = null,
    val name: String? = null,
    val category: String? = null,
    val sourceCategory: String? = null,
    val disclosureSourceCategory: String? = null,
    val url: String? = null,
    val ccpaRequestToDeleteLink: String? = null,
    val deletionInstructions: String? = null,
    val optOutInstructions: String? = null,
    val generalGuidance: String? = null,
    val specialCircumstancesGuidance: String? = null,
    val ccpaRequestToKnowLink: String? = null,
    val email: String? = null,
    val recommendedPersonalInformationCategories: List<String> = mutableListOf(),
    val serviceProviderRecommendation: ServiceProviderRecommendation? = null,
    val serviceProviderLanguageUrl: String? = null,
    val dpaApplicable: Boolean = false,
    val dpaUrl: String? = null,
    val logoFilename: String? = null,
    val lastReviewed: String? = null,
    val isDataRecipient: Boolean? = null,
    val isDataSource: Boolean? = null,
    val isPlatform: Boolean? = null,
    val isStandalone: Boolean? = null,
    var respectsPlatformRequests: Boolean = false,
    val isMigrationOnly: Boolean = false,

    var isWalletService: Boolean = false,

    val defaultRetentionReasons: List<UUIDString> = emptyList(),

    var gdprProcessorRecommendation: GDPRProcessorRecommendation? = null,
    var gdprProcessorLanguageUrl: String? = null,
    var gdprDpaApplicable: Boolean? = null,
    var gdprDpaUrl: String? = null,
    var gdprInternationalDataTransferRulesApply: Boolean? = null,
    var gdprProcessingPurpose: String? = null,
    var gdprLastReviewed: String? = null,
    var gdprAccessInstructions: String? = null,
    var gdprMultiPurposeVendor: Boolean = false,
    var gdprLawfulBasis: String? = null,
    var gdprHasSccRecommendation: Boolean = false,
    var gdprSccDocumentationUrl: String? = null
)

data class VendorAdminNewDto(
    val categories: List<String> = mutableListOf(),
    val existingNames: List<String> = mutableListOf()
)
