package polaris.controllers.admin

import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import polaris.models.UUIDString
import polaris.models.dto.privacycenter.CreatePrivacyCenter
import polaris.models.entity.feature.Features
import polaris.models.entity.organization.OrgComplianceVersion
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationRegulation
import polaris.models.entity.organization.Phase
import polaris.models.entity.organization.SendMethod
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.OrganizationRegulationRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationUserRepository
import polaris.repositories.PrivacyCenterRepository
import polaris.repositories.RegulationRepository
import polaris.repositories.UserRepository
import polaris.services.dto.OrganizationDtoService
import polaris.services.primary.CmpConfigService
import polaris.services.primary.OrganizationService
import polaris.services.primary.PrivacyCenterService
import polaris.services.support.PrivacyCenterBuilderService
import polaris.util.StringUtil
import java.util.UUID

@RestController
@RequestMapping("/admin/organizations")
class OrganizationAdminController(
    repo: OrganizationRepository,
    val dtoService: OrganizationDtoService,
    val orgService: OrganizationService,
    val privacyCenterBuilderService: PrivacyCenterBuilderService,
    val organizationRegulationRepository: OrganizationRegulationRepository,
    val privacyCenterService: PrivacyCenterService,
    val regulationRepository: RegulationRepository,
    val collectionGroupRepository: CollectionGroupRepository,
    val privacyCenterRepository: PrivacyCenterRepository,
    val organizationUserRepository: OrganizationUserRepository,
    val cmpConfigService: CmpConfigService,
    val userRepository: UserRepository
) : BaseAdminController<Organization, UUID, OrganizationAdminDto, OrganizationAdminDto, OrganizationRepository>(repo) {
    override fun toDto(resource: Organization) = dtoService.toAdminDto(resource)
    override fun apply(dto: OrganizationAdminDto, resource: Organization) = dtoService.applyAdminDto(dto, resource)
    override fun construct() = Organization()

    override fun afterUpdate(resource: Organization) =
        privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(resource, "organization admin update")

    override fun afterCreate(resource: Organization) {
        // Enable Get Compliant phase by default
        dtoService.enableOrgFeature(resource, Features.GET_COMPLIANT_PHASE)

        // Add CCPA regulation by default
        regulationRepository.findBySlug("CCPA")?.let {
            organizationRegulationRepository.save(
                OrganizationRegulation(
                    regulation = it,
                    organization = resource
                )
            )
        }
        privacyCenterService.create(
            resource.publicId,
            CreatePrivacyCenter(
                name = "Privacy Center",
                defaultSubdomain = StringUtil.slugify(resource.name, "-"),
                faviconUrl = ""
            )
        )
    }

    override fun label(resource: Organization) = resource.name

    override fun allowDelete(resource: Organization) = true
    override fun beforeDelete(resource: Organization) {
        val delUsers = resource.organizationUsers.mapNotNull { ou -> ou.user }.filter { u -> u.organization_users.count() <= 1 }
        val survivingUsers = userRepository.findAllByActiveOrganization(resource).filter { u -> !delUsers.contains(u) }

        organizationUserRepository.deleteAll(resource.organizationUsers)
        userRepository.deleteAll(delUsers)

        survivingUsers.forEach { u ->
            if (u.activeOrganization == resource) {
                u.activeOrganization = null
                userRepository.save(u)
            }
        }

        // delete all privacy centers
        privacyCenterRepository.deleteAll(resource.privacyCenters)
    }

    // custom actions

    @PostMapping("/{id}/toGetCompliant")
    protected fun update(@PathVariable id: UUID) {
        requireGlobalAdmin()
        val org = get(id)
        orgService.returnToGetCompliant(org.publicId)
    }

    @PostMapping("/{id}/publishCmpConfigs")
    protected fun publishCmpConfigs(@PathVariable id: UUID) {
        requireGlobalAdmin()
        val org = get(id)
        cmpConfigService.publishForOrg(org)
    }
}

data class OrganizationAdminDto(
    val id: UUIDString? = null,
    val publicId: String? = null,
    val name: String = "",
    val logoUrl: String? = null,
    val faviconUrl: String? = null,
    val sendMethod: SendMethod = SendMethod.NYLAS,
    val phase: Phase? = null,
    val featureGdpr: Boolean? = null,
    val featureMultistate: Boolean? = null,
    val trainingComplete: Boolean = false,
    val complianceVersion: OrgComplianceVersion? = null,

    var privacyPolicyUrl: String? = null,
    var requestTollFreePhoneNumber: String? = null,
    var requestFormUrl: String? = null,

    var clientOrganizations: List<ClientOrganizationAdminDto> = emptyList()
)

data class ClientOrganizationAdminDto(
    val id: UUIDString,
    val name: String? = ""
)
