package polaris.controllers

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import polaris.util.PolarisException

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class DataRequestFilterNotRecognized(filter: String?) : PolarisException("The data request filter `$filter` was not recognized.")

@ResponseStatus(HttpStatus.UNAUTHORIZED)
class CurrentUserNotFound() : PolarisException("You need to be logged in to perform this action.")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class PrivacyInboxInUseException(message: String) : PolarisException(message)

@ResponseStatus(HttpStatus.UNAUTHORIZED)
class WebhookNotValidated() : PolarisException("")

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class InvalidNote(message: String) : PolarisException(message)

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class MailboxNotConnectedException(message: String) : PolarisException(message)

@ResponseStatus(HttpStatus.NOT_FOUND)
class InvalidResetTokenException(message: String) : PolarisException(message)

@ResponseStatus(HttpStatus.FORBIDDEN)
class OrganizationNotAllowedException() : PolarisException("That action is not allowed.")

@ResponseStatus(HttpStatus.UNAUTHORIZED)
class AuthorizationNotSentException() : PolarisException("No authorization header was included with this request.")

@ResponseStatus(HttpStatus.FORBIDDEN)
class AuthorizationNotValidException() : PolarisException("The authorization header was not valid.")

@ResponseStatus(HttpStatus.NOT_FOUND)
class ResourceNotFoundException(id: String) : PolarisException("The resource $id was not found.")

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class InvalidDataMapOperation(message: String) : PolarisException("The provided data map operation was not valid: $message")

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class RequestAttachmentNotAllowed() : PolarisException("Attachments are not allowed for this request.")

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class GoogleAnalyticsOAuthException(val error: String, message: String = "There was an issue while processing Google Analytics OAuth") : PolarisException(message)

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class GoogleAnalyticsCannotAutodeleteException(message: String = "There is not enough data available to autodelete records from Google Analytics") : PolarisException(message)

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
class GoogleAnalyticsAutodeleteFailedException(message: String = "Something went wrong when attempting to delete the service from Google Analytics") : PolarisException(message)

@ResponseStatus(HttpStatus.BAD_REQUEST)
class UnrecognizedSurveyException(surveyName: String) : PolarisException("The survey name $surveyName was not recognized.")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class UnrecognizedSurveySlugException(surveyName: String, surveySlug: String) : PolarisException("The survey question $surveySlug for the survey $surveyName was not recognized.")
