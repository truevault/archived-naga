package polaris.controllers.webhook

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import polaris.models.dto.bitbucket.BitbucketPushWebhookDto
import polaris.services.support.PrivacyCenterBuilderService
import polaris.util.WebhookRoutes

@RestController
class BitbucketWebhookController(
    private val privacyCenterBuilderService: PrivacyCenterBuilderService,

    @Value("\${polaris.bitbucket.privacyCenterTemplateBranch}")
    private val privacyCenterTemplateBranch: String
) {

    companion object {
        private val log = LoggerFactory.getLogger(BitbucketWebhookController::class.java)
    }

    @PostMapping(WebhookRoutes.Bitbucket.TEMPLATE_PUSH)
    fun templatePush(@RequestBody pushDto: BitbucketPushWebhookDto) {
        log.info("received webhook request from bitbucket: $pushDto")
        pushDto.push.changes.forEach {
            if (it.new?.type == "branch" && it.new.name == privacyCenterTemplateBranch) {
                log.info("branch matches, triggering all rebuilds...")
                privacyCenterBuilderService.requestBuildAllPrivacyCenters("privacy center source update")
                return
            }
        }

        log.info("no changes match, not triggering rebuilds")
    }
}
