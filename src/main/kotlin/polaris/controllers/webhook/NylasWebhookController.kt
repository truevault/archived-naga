package polaris.controllers.webhook

import com.nylas.Message
import io.sentry.Sentry
import org.owasp.html.PolicyFactory
import org.owasp.html.Sanitizers
import org.slf4j.LoggerFactory
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import org.springframework.web.servlet.view.RedirectView
import polaris.models.dto.InboundEmail
import polaris.models.dto.nylas.NylasWebhookDto
import polaris.models.dto.request.CreateDataRequest
import polaris.models.dto.toIso
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.request.DataRequestSubmissionMethodEnum
import polaris.models.entity.request.DataRequestSubmissionSourceEnum
import polaris.models.entity.request.DataRequestType
import polaris.repositories.OrganizationRepository
import polaris.services.MineEmailContentException
import polaris.services.primary.DataSubjectRequestService
import polaris.services.primary.PrivacyEmailService
import polaris.services.support.EmailService
import polaris.services.support.email.MailboxService
import polaris.services.support.email.NylasClientFactory
import polaris.services.support.email.NylasService
import polaris.services.support.email.PoweredByMineService
import polaris.util.EmailParser
import polaris.util.Responses
import polaris.util.WebhookRoutes
import java.time.ZonedDateTime
import java.util.UUID

@RestController
class NylasWebhookController(
    private val dataSubjectRequestService: DataSubjectRequestService,
    private val emailService: EmailService,
    private val nylasService: NylasService,
    private val nylasClientFactory: NylasClientFactory,
    private val mailboxService: MailboxService,
    private val organizationRepository: OrganizationRepository,
    private val mineService: PoweredByMineService,
    private val privacyEmailService: PrivacyEmailService
) {
    companion object {
        private val log = LoggerFactory.getLogger(NylasWebhookController::class.java)
    }

    @GetMapping(WebhookRoutes.Nylas.OAUTH)
    fun oauthResult(
        @RequestParam("state") state: String,
        @RequestParam("code") code: String,
        attributes: RedirectAttributes
    ): RedirectView {

        try {
            log.info("[nylas] Callback from nylas with state, code: $state, [CODE FILTERED]")
            val orgId = UUID.fromString(state)

            log.info("[nylas] Decoded org id: $orgId")

            val mailboxDetails = nylasService.oauthGetMailboxDetails(code)

            log.info("[nylas] obtained mailbox details: $mailboxDetails")

            mailboxService.createMailbox(orgId, mailboxDetails)

            log.info("[nylas] done")

            attributes.addFlashAttribute("result", "success")

            val org = lookupOrganizationOrThrow(orgId)

            val isGetCompliant = org.isGetCompliantPhase()
            val redirectUri =
                if (isGetCompliant) "/get-compliant/privacy-requests/privacy-center" else "/organization/${org.publicId}/settings/inbox/?auth_redirect=1"

            return RedirectView(redirectUri)
        } catch (e: Exception) {
            log.error("Nylas Oauth Callback exception: ", e)
            return RedirectView("/")
        }
    }

    @GetMapping(
        WebhookRoutes.Nylas.ACCOUNT_CONNECTED,
        WebhookRoutes.Nylas.ACCOUNT_ERROR,
        WebhookRoutes.Nylas.ACCOUNT_SYNC_ERROR,
        WebhookRoutes.Nylas.THREAD_REPLIED,
        WebhookRoutes.Nylas.MESSAGE_CREATED
    )
    fun webhookChallenge(@RequestParam("challenge") challenge: String): String {
        log.info("[nylas] webhook setup challenge: $challenge")
        return challenge
    }

    @PostMapping(WebhookRoutes.Nylas.ACCOUNT_CONNECTED, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun accountCreated(@RequestBody nylas: NylasWebhookDto): String {
        try {
            log.info("[nylas] account.created callback: ${nylas.deltas.joinToString("; ")}")
            nylas.deltas.forEach {
                val accountId = it.object_data?.account_id
                accountId?.let { id -> nylasService.setMailboxSuccess(id) }
            }
        } catch (e: Exception) {
            Sentry.captureException(e)
        }

        return Responses.EMPTY_SUCCESS
    }

    @PostMapping(WebhookRoutes.Nylas.ACCOUNT_ERROR, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun accountError(@RequestBody nylas: NylasWebhookDto): String {
        try {
            log.info("[nylas] account.error callback: ${nylas.deltas.joinToString("; ")}")
            nylas.deltas.forEach {
                val accountId = it.object_data?.account_id
                accountId?.let { id -> nylasService.setMailboxError(id, true) }
            }
        } catch (e: Exception) {
            Sentry.captureException(e)
        }

        return Responses.EMPTY_SUCCESS
    }

    @PostMapping(WebhookRoutes.Nylas.ACCOUNT_SYNC_ERROR, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun accountSyncError(@RequestBody nylas: NylasWebhookDto): String {
        try {
            log.info("[nylas] account.sync_error callback: ${nylas.deltas.joinToString("; ")}")
            nylas.deltas.forEach {
                val accountId = it.object_data?.account_id
                accountId?.let { id -> nylasService.setMailboxError(id, true) }
            }
        } catch (e: Exception) {
            Sentry.captureException(e)
        }

        return Responses.EMPTY_SUCCESS
    }

    /**
     * If a new message is sent to the privacy inbox and not a part of a request thread then notify the admins
     */
    @PostMapping(WebhookRoutes.Nylas.MESSAGE_CREATED)
    fun messageCreated(@RequestBody nylas: NylasWebhookDto): String {
        try {
            log.info("[nylas] message.created callback: ${nylas.deltas.joinToString("; ")}")
            var sentNotificationByMailboxId: Map<UUID, Boolean> = mapOf()
            val hiddenPrefix = emailService.getRequestMessageHiddenPrefix()
            val isRequestMessageRegex = Regex("$hiddenPrefix([A-Z0-9]{9})")

            nylas.deltas.filter { it.type == "message.created" }.forEach {
                log.info("[nylas] handling message: $it")
                val accountId = it.object_data?.account_id
                val messageId = it.object_data?.id
                val mailbox = accountId?.let { nylasService.getMailboxByAccountId(accountId) }
                log.info("[nylas][$messageId] found mailbox: $mailbox")
                if (mailbox != null && messageId != null && sentNotificationByMailboxId[mailbox.id] != true) {
                    val message = nylasService.getMessageDetailsById(mailbox.nylasAccessToken, messageId)
                    val wasSentToPrivacyMailbox =
                        (message.to.map { it.email } + message.cc.map { it.email }).contains(mailbox.mailbox)

                    if (wasSentToPrivacyMailbox) {
                        val isMineEmail = mineService.isMineEmail(message.body)
                        val requestMessageMatch = isRequestMessageRegex.find(message.body)?.destructured?.toList()

                        log.info(
                            "[nylas][$messageId] New inbound privacy message received:\n" +
                                "\tsubject=${message.subject}\n" +
                                "\tfrom=${message.from}\n" +
                                "\tto=${message.to}\n" +
                                "\tcc=${message.cc}\n" +
                                "\tisMineEmail=${isMineEmail}\n" +
                                "\tbody=\n${message.body}"
                        )

                        if (isMineEmail) {
                            log.info("[nylas][$messageId] Mine message detected. Processing...")
                            processMineEmail(messageId, message, mailbox)
                            privacyEmailService.logPrivacyEmail(message, mailbox, true)
                        } else if (requestMessageMatch.isNullOrEmpty()) {
                            log.info("[nylas][$messageId] Non-Mine message detected. Processing...")
                            sentNotificationByMailboxId = sentNotificationByMailboxId + mapOf(mailbox.id to true)
                            privacyEmailService.logPrivacyEmail(message, mailbox)
                        } else {
                            log.info("[nylas][$messageId] Threaded message detected. Nothing to do here.")
                        }
                    } else {
                        log.info(
                            "[nylas][$messageId] Non-privacy message received. Skipping... \n" +
                                "\tsubject=${message.subject}\n" +
                                "\tfrom=${message.from}\n" +
                                "\tto=${message.to}\n" +
                                "\tcc=${message.cc}"
                        )
                    }
                }
            }
        } catch (e: Exception) {
            log.error("[nylas] Exception processing message.created webhook: {}", e.message, e)
            Sentry.captureException(e)
        }
        return Responses.EMPTY_SUCCESS
    }

    @PostMapping(WebhookRoutes.Nylas.THREAD_REPLIED, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun threadReplied(@RequestBody nylas: NylasWebhookDto): String {
        try {
            log.info("[nylas] thread.replied callback: ${nylas.deltas.joinToString("; ")}")
            nylas.deltas.filter { it.type == "thread.replied" }.forEach {
                val messageId = it.object_data?.metadata?.message_id
                val payload = it.object_data?.metadata?.payload

                if (messageId == null || payload == null) {
                    return@forEach
                }

                receiveInboundEmail(messageId, payload)
            }
        } catch (e: Exception) {
            Sentry.captureException(e)
        }
        return Responses.EMPTY_SUCCESS
    }

    private fun processMineEmail(messageId: String, message: Message, mailbox: OrganizationMailbox) {
        val from = if ((message.from?.size ?: 0) > 0) message.from[0] else null
        val subject = message.subject
        val body = message.body
        val org = mailbox.organization!!

        // 1 - Extract email and name
        val contents = mineService.extractMineEmailContents(body, from?.name, from?.email)

        if (contents.isValid) {
            // 2 - Create request to delete
            val createDataRequest = CreateDataRequest(
                subjectFirstName = contents.firstName,
                subjectLastName = contents.lastName,
                dataRequestType = DataRequestType.CCPA_RIGHT_TO_DELETE,
                requestDate = ZonedDateTime.now().toIso(),
                subjectEmailAddress = contents.email,
                isDataSubject = true,
                submissionSource = DataRequestSubmissionSourceEnum.MINE_EMAIL,
                submissionMethod = DataRequestSubmissionMethodEnum.EMAIL.name
            )
            log.info("[nylas][$messageId] creating data subject request: $createDataRequest")
            val dataSubjectRequest =
                dataSubjectRequestService.create(null, org.publicId, createDataRequest, isAutocreated = true)
            log.info("[nylas][$messageId] created data subject request: ${dataSubjectRequest.publicId}")

            // 3 - send mine notification(s)
            val users = org.activeUsers()
            val afterContent = "<div class='bg-none padding-sm'><p>Original Email:</p><div>$body</div></div>"
            users.forEach { orgUser ->
                emailService.sendInternalNewSubjectRequest(orgUser.organization!!, orgUser.user!!, after = afterContent)
            }
        } else {
            emailService.sendNewPrivacyInboxMessageNotification(
                mailbox,
                message.from.joinToString { f -> f.email },
                message.subject,
                true
            )
            throw MineEmailContentException("Could not extract valid content from Mine email sent to ${org.name} (${org.publicId}), messageId=$messageId, subject=$subject")
        }
    }

    private fun receiveInboundEmail(messageId: String, payload: String) {
        log.info("[nylas] received inbound email with message id: $messageId and payload $payload")

        val routing = createRouting(payload)

        // need to determine the nylas access token
        val accessToken = when (routing.type) {
            InboundType.DATA_REQUEST -> dataRequestAccessToken(routing)
            InboundType.UNKNOWN -> null
        }

        if (accessToken == null) {
            log.warn("[nylas] Unable to find an active nylas access token for inbound email $payload")
            return
        }

        val account = nylasClientFactory.nylasAccount(accessToken)
        val messages = account.messages()

        // Replace '{id}' with the appropriate value
        // Replace '{id}' with the appropriate value
        val message = messages[messageId]

        // The following attributes are available for the message object
        // The following attributes are available for the message object
        val html = message.body

        val policy: PolicyFactory = Sanitizers.FORMATTING.and(Sanitizers.LINKS).and(Sanitizers.BLOCKS)
        val sanitizedHtml: String = policy.sanitize(html)
        val msg = EmailParser.parseMessage(body = sanitizedHtml)

        val inboundEmail = createInboundDto(
            to = message.to.toString(),
            from = message.from.toString(),
            subject = message.subject,
            body = sanitizedHtml,
            unsanitizedBody = html,
            message = msg,
            senderIp = null
        )

        when (routing.type) {
            InboundType.DATA_REQUEST -> processDataRequestInbound(routing, inboundEmail)
            InboundType.UNKNOWN -> log.info("could not determine a routing type for incoming payload: $payload")
        }
    }

    private fun dataRequestAccessToken(routing: RequestRouting): String? {
        log.info("attempting to get nylas access token for: ${routing.id}")
        val replyKey = UUID.fromString(routing.id!!)
        return dataSubjectRequestService.getNylasAccessToken(replyKey)
    }

    private fun processDataRequestInbound(routing: RequestRouting, inboundEmail: InboundEmail) {
        log.info("attempting to process data request with reply key: ${routing.id}")
        val replyKey = UUID.fromString(routing.id!!)
        dataSubjectRequestService.handleIncomingEmail(replyKey, inboundEmail)
    }

    private fun createInboundDto(
        to: String?,
        from: String?,
        subject: String?,
        body: String?,
        unsanitizedBody: String?,
        message: String?,
        senderIp: String?
    ) = InboundEmail(
        to = to,
        from = from,
        subject = subject,
        body = body,
        unsanitizedBody = unsanitizedBody,
        message = message,
        senderIp = senderIp
    )

    private fun createRouting(r: String): RequestRouting {
        return when (r.substring(0, 1)) {
            "r" -> RequestRouting.dataRequest(r)
            else -> RequestRouting.unknown()
        }
    }

    private fun lookupOrganizationOrThrow(organizationId: UUID) =
        organizationRepository.findByIdOrNull(organizationId) ?: throw OrganizationNotFound()
}
