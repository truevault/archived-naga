package polaris.controllers.webhook

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.AuthorizationNotSentException
import polaris.controllers.AuthorizationNotValidException
import polaris.models.dto.websiteaudit.WebsiteAuditDto
import polaris.services.support.WebsiteAuditService
import polaris.util.WebhookRoutes

@RestController
class WebsiteAuditWebhookController(
    private val websiteAuditService: WebsiteAuditService,
    @Value("\${polaris.websiteaudit.inbound}")
    private val websiteAuditInboundKey: String
) {
    companion object {
        private val log = LoggerFactory.getLogger(WebsiteAuditWebhookController::class.java)
    }

    @PostMapping(WebhookRoutes.Websiteaudit.RESULT)
    fun addWebsiteAuditResult(@RequestBody result: WebsiteAuditDto, @RequestHeader(name = "authorization") authorization: String?) {
        if (authorization == null) {
            log.info("Authorization Header was not present")
            throw AuthorizationNotSentException()
        }

        if (authorization != "Bearer $websiteAuditInboundKey") {
            log.info("Authorization Header was not valid: \"$authorization\" was not equal to \"Bearer $websiteAuditInboundKey\"")
            throw AuthorizationNotValidException()
        }

        websiteAuditService.addWebsiteAuditResult(result)
    }
}
