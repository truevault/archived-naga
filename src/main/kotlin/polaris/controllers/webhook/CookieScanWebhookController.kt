package polaris.controllers.webhook

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.AuthorizationNotSentException
import polaris.controllers.AuthorizationNotValidException
import polaris.models.dto.cookiescan.CookieScanResultDto
import polaris.services.support.CookieScannerService
import polaris.util.WebhookRoutes

@RestController
class CookieScanWebhookController(
    private val cookieScanService: CookieScannerService,
    @Value("\${polaris.cookiescan.inbound}")
    private val cookieScanInboundKey: String
) {
    companion object {
        private val log = LoggerFactory.getLogger(CookieScanWebhookController::class.java)
    }

    @PostMapping(WebhookRoutes.Cookiescan.RESULT)
    fun addCookieScanResult(@RequestBody result: CookieScanResultDto, @RequestHeader(name = "authorization") authorization: String?) {
        if (authorization == null) {
            log.info("Authorization Header was not present")
            throw AuthorizationNotSentException()
        }

        if (authorization != "Bearer $cookieScanInboundKey") {
            log.info("Authorization Header was not valid: \"$authorization\" was not equal to \"Bearer $cookieScanInboundKey\"")
            throw AuthorizationNotValidException()
        }

        cookieScanService.addCookieScanResult(result)
    }
}
