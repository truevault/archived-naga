package polaris.controllers.webhook

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import org.springframework.web.servlet.view.RedirectView
import polaris.controllers.GoogleAnalyticsOAuthException
import polaris.controllers.util.LookupService
import polaris.services.support.oauth.GoogleAuthTokenService
import polaris.util.WebhookRoutes

@RestController
class OauthWebhookController(
    private val googleAuthTokenService: GoogleAuthTokenService,
    private val lookup: LookupService
) {
    companion object {
        private val log = LoggerFactory.getLogger(OauthWebhookController::class.java)
    }

    @GetMapping(WebhookRoutes.Oauth.GOOGLE)
    fun googleOauthResult(
        @RequestParam("state") state: String,
        @RequestParam("error", required = false) error: String?,
        @RequestParam("code", required = false) code: String?,
        @RequestParam("scope", required = false) scope: String?,
        attributes: RedirectAttributes
    ): RedirectView {

        log.info("Google OAuth Callback received for org $state: error=$error, scope=$scope, code=${if (code == null) "null" else "****"}")

        val org = try {
            lookup.orgOrThrow(state)
        } catch (e: Exception) {
            log.error("Google Oauth Callback exception occurred for org $state: ${e.message}", e)
            return RedirectView("/")
        }

        val isGetCompliant = org.isGetCompliantPhase()
        var redirectUri =
            if (isGetCompliant) "/get-compliant/data-retention/integrations" else "/organization/${org.publicId}/settings/integrations"

        try {

            if (error != null) {
                redirectUri += "?error=$error"
                log.error("Google OAuth Callback received with error '$error' for org $state (${org.name}). Redirecting user to $redirectUri")
                return RedirectView(redirectUri)
            }

            if (code == null) {
                redirectUri += "?error_message=Google OAuth code was missing in the response"
                log.error("Google OAuth Callback received with no code for org $state (${org.name}). Redirecting user to $redirectUri")
                return RedirectView(redirectUri)
            }

            googleAuthTokenService.fetchAndPersistGoogleAnalyticsAuthToken(org, code)

            log.info("Refresh token successfully requested and persisted for org $state (${org.name}). Redirecting user to $redirectUri")

            return RedirectView(redirectUri)
        } catch (e: GoogleAnalyticsOAuthException) {
            redirectUri += "?error=${e.error}&error_message=${e.message}"
            log.error("Google Oauth Callback exception occurred for org $state: ${e.message}", e)
            return RedirectView(redirectUri)
        } catch (e: Exception) {
            redirectUri += "?error_message=${e.message}"
            log.error("Google Oauth Callback exception occurred for org $state: ${e.message}", e)
            return RedirectView(redirectUri)
        }
    }
}
