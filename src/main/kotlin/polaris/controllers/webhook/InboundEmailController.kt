package polaris.controllers.webhook

import org.owasp.html.PolicyFactory
import org.owasp.html.Sanitizers
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.WebhookNotValidated
import polaris.models.dto.InboundEmail
import polaris.services.primary.DataSubjectRequestService
import polaris.util.EmailParser
import java.util.*
import javax.servlet.http.HttpServletRequest

@RestController
class InboundEmailController(
    @Value("\${polaris.mailer.sendgrid.inbound}")
    private val sendgridInboundKey: String,
    private val dataSubjectRequestService: DataSubjectRequestService
) {
    companion object {
        private val log = LoggerFactory.getLogger(InboundEmailController::class.java)
    }

    @PostMapping("/webhooks/email")
    fun inboundEmail(
        @RequestParam("key") key: String,
        @RequestParam("to") to: String?,
        @RequestParam("from") from: String?,
        @RequestParam("subject") subject: String?,
        @RequestParam("html") html: String?,
        @RequestParam("sender_ip") senderIp: String?,
        @RequestParam("attachments") attachments: String?,
        @RequestParam("attachment-info") attachmentInfo: String?,
        request: HttpServletRequest
    ) {
        log.info("inbound key should be: $sendgridInboundKey")
        log.info("received an inbound email with key: $key")

        if (key != sendgridInboundKey) {
            log.info("keys didn't match. aborting request")
            throw WebhookNotValidated()
        }

        log.info("request was valid")

        val toComponents = to?.split("-")

        val policy: PolicyFactory = Sanitizers.FORMATTING.and(Sanitizers.LINKS).and(Sanitizers.BLOCKS)
        val sanitizedHtml: String = policy.sanitize(html)
        val message = EmailParser.parseMessage(body = sanitizedHtml)

        // privacy.ruuid@polaris-local.truevaultatlas.com
        toComponents?.let {
            if (it.size >= 2) {
                val inboundType = it.get(it.size - 2)
                val inboundId = it.get(it.size - 1)

                log.info("obtained inbound type / id: $inboundType / $inboundId")
            } else {
                log.info("to component did not have enough components for a type")
            }
        }

        log.info("to / from /subject: $to / $from / $subject")
        log.info("sender ip: $senderIp")
        log.info("attachments: $attachments")
        log.info("attachmentInfo: $attachmentInfo")
        log.info("\nsanitized body: $sanitizedHtml")
        log.info("\nparsed message: $message")

        request.parts.forEach {
            log.info("part: ${it.name} / ${it.submittedFileName} / ${it.contentType} / ${it.size}")
        }

        val routing = determineRouting(to)
        val inboundEmail = createInboundDto(
            to = to,
            from = from,
            subject = subject,
            body = sanitizedHtml,
            unsanitizedBody = html,
            message = message,
            senderIp = senderIp
        )

        when (routing.type) {
            InboundType.DATA_REQUEST -> processDataRequestInbound(routing, inboundEmail)
            InboundType.UNKNOWN -> log.info("could not determine a routing type for incoming address: $to")
        }
    }

    private fun processDataRequestInbound(routing: RequestRouting, inboundEmail: InboundEmail) {
        log.info("attempting to process data request with reply key: ${routing.id}")
        val id = UUID.fromString(routing.id!!)
        dataSubjectRequestService.handleIncomingEmail(id, inboundEmail)
    }

    private fun determineRouting(to: String?): RequestRouting {
        to ?: return RequestRouting.unknown()

        val emailUser = to.split("@").first()
        val routing = emailUser.split(".").last()

        log.info("Found routing: $routing")

        val r = routing.take(1)

        return when (r) {
            "r" -> RequestRouting.dataRequest(routing)
            else -> RequestRouting.unknown()
        }
    }

    private fun createInboundDto(
        to: String?,
        from: String?,
        subject: String?,
        body: String?,
        unsanitizedBody: String?,
        message: String?,
        senderIp: String?
    ) = InboundEmail(
        to = to,
        from = from,
        subject = subject,
        body = body,
        unsanitizedBody = unsanitizedBody,
        message = message,
        senderIp = senderIp
    )
}

enum class InboundType {
    DATA_REQUEST,
    UNKNOWN
}

data class RequestRouting(
    val type: InboundType,
    val id: String? = null
) {
    companion object {
        private val log = LoggerFactory.getLogger(RequestRouting::class.java)

        fun unknown() = RequestRouting(InboundType.UNKNOWN)

        fun dataRequest(routing: String): RequestRouting {
            val id = routing.substring(1)

            return try {
                // fromString -> toString() to validate the id as a uuid
                RequestRouting(InboundType.DATA_REQUEST, UUID.fromString(id).toString())
            } catch (e: IllegalArgumentException) {
                log.info("Could not parse a routing as a data request routing: $routing")
                unknown()
            }
        }
    }
}
