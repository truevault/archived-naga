package polaris.controllers.webhook

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.LookupService
import polaris.models.dto.request.PhoneDataRequestDto
import polaris.models.dto.request.TwilioDataRequestDto
import polaris.services.support.EmailService
import polaris.util.WebhookRoutes
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

@Lazy
@RestController
class TwilioController(
    private val emailService: EmailService,
    private val lookupService: LookupService
) {
    companion object {
        private val log = LoggerFactory.getLogger(TwilioController::class.java)
    }

    @PostMapping(WebhookRoutes.Twilio.DATA_REQUEST)
    fun receiveTwilioDataRequest(@RequestBody requestDto: TwilioDataRequestDto) {
        log.info("[twilio] Callback from Twilio with Phone Number: ${requestDto.phoneNumberCalled}")
        val organization = lookupService.organizationByPhoneNumberOrThrow(requestDto.phoneNumberCalled)

        log.info("[twilio] Decoded org id: ${organization.id}")
        val requestDto = PhoneDataRequestDto(
            requestDate = ZonedDateTime.now().truncatedTo(ChronoUnit.SECONDS),
            requestType = requestDto.requestType,
            org = organization,
            consumerPhoneNumber = requestDto.consumerPhoneNumber,
            consumerNameRecording = requestDto.consumerNameRecording,
            consumerEmailRecording = requestDto.consumerEmailRecording
        )

        log.info("[twilio] Sending new request by phone notification")
        emailService.sendNewRequestByPhoneNotification(requestDto)
    }
}
