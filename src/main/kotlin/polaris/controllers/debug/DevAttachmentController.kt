package polaris.controllers.debug

import org.springframework.context.annotation.Profile
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import polaris.services.support.AttachmentService
import polaris.services.support.ResourceAttachment
import polaris.util.Responses
import java.util.*

@RestController
@Profile("dev")
class DevAttachmentController(
    private val attachmentService: AttachmentService
) {
    @PostMapping("/debug/attachment")
    fun handleFileUpload(@RequestParam("file") file: MultipartFile): String? {
        attachmentService.createAttachment(file)
        return Responses.EMPTY_SUCCESS
    }

    @GetMapping("/debug/attachment/{id}")
    @ResponseBody
    fun getAttachment(@PathVariable id: UUID): ResponseEntity<Resource> {
        val f: ResourceAttachment = attachmentService.getAttachment(id)
        return ResponseEntity.ok().header(
            HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"${f.attachment.name}\""
        ).body(f.resource)
    }
}
