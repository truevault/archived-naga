package polaris.controllers.debug

import org.springframework.context.annotation.Profile
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
@Profile("dev")
class ErrorsController {

    @RequestMapping("/debug/errors")
    fun errors(@RequestParam("code") code: Int?) {
        throw ResponseStatusException(HttpStatus.resolve(code ?: 500) ?: HttpStatus.INTERNAL_SERVER_ERROR, "Debug Endpoint")
    }
}
