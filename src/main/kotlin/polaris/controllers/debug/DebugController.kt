package polaris.controllers.debug

import com.google.common.net.HttpHeaders
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.admin.OrganizationAdminDto
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.entity.organization.GetCompliantProgress
import polaris.repositories.PrivacyCenterRepository
import polaris.services.debug.SeedService
import polaris.services.dto.OrganizationDtoService
import polaris.services.dto.PrivacyCenterDtoService
import polaris.services.primary.CmpConfigService
import polaris.services.primary.DataSubjectRequestAutoCloseService
import polaris.services.primary.DataSubjectRequestService
import polaris.services.primary.DigestService
import polaris.services.primary.OrganizationService
import polaris.services.primary.QueuedEmailService
import polaris.services.primary.TaskService
import polaris.services.primary.TaskUpdateService
import polaris.services.support.EmailService
import polaris.services.support.PdfService
import polaris.services.support.PrivacyCenterBuilderService
import polaris.services.support.email.MailboxService
import polaris.util.Responses
import java.time.ZonedDateTime

@RestController
@Profile("dev")
class DebugController(
    private val seedService: SeedService,
    private val privacyCenterRepo: PrivacyCenterRepository,
    private val privacyCenterDtoService: PrivacyCenterDtoService,
    private val taskService: TaskService,
    private val taskUpdateService: TaskUpdateService,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService,
    private val organizationService: OrganizationService,
    private val organizationDtoService: OrganizationDtoService,
    private val requestsService: DataSubjectRequestService,
    private val requestAutoCloseService: DataSubjectRequestAutoCloseService,
    private val queuedEmailService: QueuedEmailService,
    private val emailService: EmailService,
    private val digestService: DigestService,
    private val lookupService: LookupService,
    private val pdfService: PdfService,
    private val mailboxService: MailboxService,
    private val cmpConfigService: CmpConfigService
) {

    companion object {
        private val log = LoggerFactory.getLogger(DebugController::class.java)
    }

    @RequestMapping("/debug/seed")
    fun seed(): String {
        seedService.seed()
        return Responses.EMPTY_SUCCESS
    }

    data class SeedOrgDto(
        val name: String,
        val username: String,
        val domain: String,
        val features: List<String> = emptyList()
    )

    @PostMapping("/debug/seed/org")
    fun seedOrg(@RequestBody seedOrgDto: SeedOrgDto): OrganizationAdminDto {
        val org = seedService.seedOrg(seedOrgDto.name, seedOrgDto.username, seedOrgDto.domain, seedOrgDto.features)
        return organizationDtoService.toAdminDto(org)
    }

    @PostMapping("/debug/seed/org/{orgId}/data-map")
    fun seedOrg(@PathVariable orgId: OrganizationPublicId): OrganizationAdminDto {
        val org = seedService.seedDataMap(orgId)
        return organizationDtoService.toAdminDto(org)
    }

    @RequestMapping("/debug/rebuild/{orgPublicId}")
    fun rebuild(@PathVariable(required = false) orgPublicId: OrganizationPublicId?): String {
        if (orgPublicId != null) {
            val organization = organizationService.findByPublicId(orgPublicId)
            privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(organization, "debug rebuild")
        } else {

            privacyCenterBuilderService.requestBuildAllPrivacyCenters("debug rebuild")
        }

        return Responses.EMPTY_SUCCESS
    }

    @RequestMapping("/debug/pdf")
    @ResponseBody
    fun pdf(): ResponseEntity<ByteArray> {
        val file = pdfService.pdfFromTemplate("pdfs/debug.ftl", emptyMap())
        val bytes = file.readBytes()
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
            .header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=test.pdf").body(bytes)
    }

    @RequestMapping("/debug/org/{orgId}/privacy-builders", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun privacyBuilders(@PathVariable orgId: String) = privacyCenterDtoService.toBuilderDto(
        privacyCenterRepo.findOneByOrganization(
            organizationService.findByPublicId(orgId)
        )
    )

    @RequestMapping("/debug/publish-cmp-configs")
    fun publishPrivacyConfig(@RequestParam(required = false) orgId: String?): String {
        if (orgId == null) {
            cmpConfigService.publishForAllOrgs()
        } else {
            val org = lookupService.orgOrThrow(orgId)
            cmpConfigService.publishForOrg(org)
        }

        return Responses.EMPTY_SUCCESS
    }

    @RequestMapping("/debug/org/{orgId}/createTasks", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createTasks(@PathVariable orgId: String) = taskUpdateService.updateTasks(orgId, create = true, close = false)

    @RequestMapping("/debug/org/{orgId}/closeTasks", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun closeTasks(@PathVariable orgId: String) = taskUpdateService.updateTasks(orgId, create = false, close = true)

    @RequestMapping("/debug/autoCloseRequests")
    fun autoCloseRequests(@RequestParam("at") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) at: ZonedDateTime?): String {
        log.info("received request to auto-close data subject requests at: {}", at)
        requestAutoCloseService.autoCloseRequests(at)

        return Responses.EMPTY_SUCCESS
    }

    @RequestMapping("/debug/autoCreateTasks", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun autoCreateTasks() = taskService.autoCreateTasks()

    @RequestMapping("/debug/autoCloseTasks", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun autoCloseTasks() = taskService.autoCloseTasks()

    @RequestMapping("/debug/resetSurvey")
    fun resetSurvey(): String {
        organizationService.updateGetCompliantProgress(
            organizationId = "HKCX52BKC",
            getCompliantProgress = GetCompliantProgress.STARTING_PROGRESS,
            events = mutableListOf()
        )
        return Responses.EMPTY_SUCCESS
    }

    @RequestMapping("/debug/sendEmails")
    fun sendEmails(): String {
        queuedEmailService.findAndProcessEmails()
        return Responses.EMPTY_SUCCESS
    }

    @RequestMapping("/debug/sendWeeklyDigest")
    fun sendWeeklyDigests(@RequestParam(required = false) orgId: String?): String {
        if (orgId == null) {
            digestService.sendAllWeeklyDigestEmails()
        } else {
            val org = lookupService.orgOrThrow(orgId)
            digestService.sendWeeklyDigestEmail(org)
        }

        return Responses.EMPTY_SUCCESS
    }

    @RequestMapping("/debug/org/{orgId}/sendReconnect")
    fun sendReconnect(@PathVariable orgId: String): String {
        val org = lookupService.orgOrThrow(orgId)
        emailService.sendReconnectEmailToOrg(org)
        return Responses.EMPTY_SUCCESS
    }

    @RequestMapping("/debug/sendEmailAddressVerification")
    fun sendEmailAddressVerification(): String {
        // Seeded data subject request on Truevault org
        val request = requestsService.findByPublicId("UXWSDP2MQ", "W7VH6H6AI")
        emailService.sendEmailAddressVerification(request)

        return Responses.EMPTY_SUCCESS
    }

    @RequestMapping("/debug/sendMineEmails")
    fun sendMineEmails() {
        val tv = organizationService.findByPublicId("UXWSDP2MQ")
        val users = tv.activeUsers()
        users.forEach {
            emailService.sendInternalNewSubjectRequest(
                tv,
                it.user!!,
                after = "<div class='bg-none padding-sm'><p>Original Email:</p><div><h1>THIS IS WHERE WE WOULD SEE THE NYLAS HTML</h1></div></div>"
            )
        }
    }

    @PostMapping("/debug/mailboxes/pollNylasMailboxes")
    fun pollNylasMailboxes() {
        mailboxService.pollNylasMailboxes()
    }

    // TODO: Update the resetSurvey to allow optional org & survey params.
    // Hit this endpoint to clear an entire survey (or all surveys) for an org.
    // e.g. DELETE FROM organization_survey_question
    //        WHERE organization_id = '610d5deb-4e71-4ea4-8e46-58311b88e2f7'
    //        AND survey_name = 'selling-and-sharing-survey';
    // Also add a /debug/surveyList endpoint to get all survey_name entries.
}
