package polaris.controllers.util

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Controller
import polaris.controllers.CurrentUserNotFound
import polaris.controllers.OrganizationNotAllowedException
import polaris.filters.SessionInvalidationFilter
import polaris.models.OrganizationPublicId
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.organization.EventData
import polaris.models.entity.user.User
import polaris.models.entity.user.UserRole
import polaris.models.entity.user.UserStatus
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationUserRepository
import polaris.services.primary.UserService
import polaris.services.support.AuthenticationService
import polaris.services.support.events.EventTrackingService
import java.time.Duration
import java.util.*
import javax.servlet.http.HttpSession

@Controller
@Lazy
abstract class SessionAwareController {
    @Autowired
    private lateinit var session: HttpSession

    @Autowired
    private lateinit var userDetailsService: UserDetailsService

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var authenticationService: AuthenticationService

    @Autowired
    private lateinit var eventTrackingService: EventTrackingService

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var organizationUserRepository: OrganizationUserRepository

    @Value("#{T(java.time.Duration).parse('\${polaris.session.timeout:PT4H}')}")
    private lateinit var sessionTimeout: Duration

    protected fun getCurrentUser() = authenticationService.getCurrentUser(getContext())

    protected fun requireCurrentUser() = getCurrentUser() ?: throw CurrentUserNotFound()

    protected fun requireGlobalAdmin(): User {
        val cu = requireCurrentUser()
        if (cu.globalRole == null) {
            throw CurrentUserNotFound()
        }

        return cu
    }

    protected fun isCurrentUser(id: UUID) = authenticationService.isCurrentUser(getContext(), id)
    protected fun logoutIfSameUser(id: UUID) {
        if (isCurrentUser(id)) {
            // Consider moving everything inside this block to AuthenticationService.logout() if needed for re-use.
            SecurityContextHolder.clearContext()
            session.invalidate()
        }
    }

    protected fun<T> authorizeOrg(organizationId: OrganizationPublicId, action: (events: MutableList<EventData>) -> T): T {
        val eventData = mutableListOf<EventData>()

        val cu = getCurrentUser() ?: throw OrganizationNotFound(organizationId)
        val o = organizationRepository.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)
        val ou = organizationUserRepository.findByUserAndOrganization(cu, o)

        if (ou != null) {
            if (ou.status != UserStatus.ACTIVE || ou.role != UserRole.ADMIN) {
                throw OrganizationNotAllowedException()
            }
        } else {
            // No OrganizationUser exists, but we might still be granted access through an active vendor org
            cu.organization_users.filter {
                it.status == UserStatus.ACTIVE && it.role == UserRole.ADMIN
            }.flatMap {
                    orgUser ->
                orgUser.organization?.clientOrganizations ?: emptyList()
            }.firstOrNull {
                it.publicId == organizationId
            } ?: throw OrganizationNotFound(organizationId)
        }

        val res = action(eventData)

        eventTrackingService.publish(eventData, o, cu)

        return res
    }

    protected fun sessionSecondsRemaining(): Int {
        val timeoutMs = sessionTimeout.toMillis()

        val timeMs = System.currentTimeMillis()
        val lastAccessTimeMs = session.getAttribute(SessionInvalidationFilter.LAST_ACCESS_SESSION_ATTR) as Long?
            ?: return (timeoutMs / 1000).toInt()

        val timeSinceActivityMs = (timeMs - lastAccessTimeMs).coerceAtLeast(0)
        val timeRemainingMs = (timeoutMs - timeSinceActivityMs).coerceAtLeast(0)

        return (timeRemainingMs / 1000).toInt()
    }

    protected fun autoLoginUser(email: String) {
        // This is how to login a user programmatically. Could be pulled out into a common spot but only need it in
        // this one spot for now. If this is moved, consider moving it to the AuthenticationService
        val userDetails = userDetailsService.loadUserByUsername(email)

        SecurityContextHolder.getContext().authentication =
            UsernamePasswordAuthenticationToken(userDetails, userDetails.password, userDetails.authorities)
    }

    protected fun getContext(): SecurityContext? = SecurityContextHolder.getContext()
}
