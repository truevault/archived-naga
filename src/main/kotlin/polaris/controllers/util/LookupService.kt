package polaris.controllers.util

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import polaris.controllers.ResourceNotFoundException
import polaris.models.OrganizationPublicId
import polaris.models.entity.CollectionGroupNotFound
import polaris.models.entity.CookieNotFound
import polaris.models.entity.FeatureNotFound
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.UserNotFound
import polaris.models.entity.VendorNotFound
import polaris.models.entity.feature.Feature
import polaris.models.entity.feature.OrganizationFeature
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.user.User
import polaris.models.entity.vendor.KnownVendorKey
import polaris.models.entity.vendor.Vendor
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.FeatureRepository
import polaris.repositories.OrgCookieRepo
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationFeatureRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.PICGroupRepo
import polaris.repositories.PersonalInformationCategoryRepository
import polaris.repositories.UserRepository
import polaris.repositories.VendorRepository
import polaris.util.PhoneNumberUtil
import polaris.util.PolarisException
import java.util.*

@Service
class LookupService(
    private val organizationRepo: OrganizationRepository,
    private val userRepository: UserRepository,
    private val collectionGroupRepo: CollectionGroupRepository,
    private val vendorRepo: VendorRepository,
    private val featureRepository: FeatureRepository,
    private val organizationFeatureRepo: OrganizationFeatureRepository,
    private val organizationDataRecipientRepo: OrganizationDataRecipientRepository,
    private val picRepo: PersonalInformationCategoryRepository,
    private val picGroupRepo: PICGroupRepo,
    private val orgCookieRepo: OrgCookieRepo
) {
    fun orgOrThrow(organizationId: OrganizationPublicId): Organization =
        organizationRepo.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)

    fun orgOrThrow(organizationId: UUID): Organization =
        organizationRepo.findByIdOrNull(organizationId) ?: throw OrganizationNotFound()

    fun userOrThrow(userId: UUID): User = userRepository.findByIdOrNull(userId) ?: throw UserNotFound()

    fun orgByDevInstructionsId(instructionId: UUID): Organization? = organizationRepo.findByDevInstructionsUuid(instructionId)
    fun orgByHrInstructionsId(instructionId: UUID): Organization? = organizationRepo.findByHrInstructionsUuid(instructionId)

    fun organizationByPhoneNumberOrThrow(phoneNumber: String): Organization {
        val matchingOrg = organizationRepo
            .findAll()
            .filter { !it.requestTollFreePhoneNumber.isNullOrBlank() }
            .find { PhoneNumberUtil.checkMatch(it.requestTollFreePhoneNumber!!, phoneNumber) }

        return matchingOrg ?: throw OrganizationNotFound(phoneNumber)
    }

    fun vendorOrThrow(vendorId: UUID): Vendor =
        vendorRepo.findByIdOrNull(vendorId) ?: throw VendorNotFound(vendorId.toString())

    fun globalVendorByKeyOrThrow(key: KnownVendorKey): Vendor =
        vendorRepo.findByVendorKeyAndOrganizationIsNull(key.key) ?: throw VendorNotFound(key.key)

    fun vendor(vendorId: UUID): Vendor? =
        vendorRepo.findByIdOrNull(vendorId)

    fun vendorForOrgOrThrow(organizationId: OrganizationPublicId, vendorId: UUID): Vendor {
        val vendor = vendorRepo.findByIdOrNull(vendorId) ?: throw VendorNotFound(vendorId.toString())
        if (vendor.organization != null && vendor.organization.publicId != organizationId) throw VendorNotFound(vendorId.toString())
        return vendor
    }

    fun featureOrThrow(feature: String): Feature =
        featureRepository.findByName(feature) ?: throw FeatureNotFound(feature)

    fun orgFeatureOrDefault(org: Organization, feature: String): OrganizationFeature {
        val featureEntity = featureOrThrow(feature)
        return organizationFeatureRepo.findByOrganizationAndFeature(org, featureEntity)
            ?: return OrganizationFeature(
                organization = org,
                feature = featureEntity,
                enabled = featureEntity.enabledByDefault ?: false
            )
    }

    fun picOrThrow(categoryId: UUID) = picRepo.findByIdOrNull(categoryId) ?: throw ResourceNotFoundException("PersonalInformationCategory: $categoryId")
    fun picByKeyOrThrow(key: String) = picRepo.findByPicKey(key) ?: throw ResourceNotFoundException("PersonalInformationCategory: key $key")
    fun picGroupOrThrow(groupId: UUID) = picGroupRepo.findById(groupId).orElseThrow { ResourceNotFoundException("PIC Group: $groupId") }
    fun orgDataRecipientOrThrow(org: Organization, vendor: Vendor): OrganizationDataRecipient =
        organizationDataRecipientRepo.findByOrganizationAndVendor(org, vendor) ?: throw PolarisException("Organization ${org.publicId} has no data recipient ${vendor.id}")

    fun collectionGroupOrThrow(organizationId: OrganizationPublicId, collectionGroupId: UUID) = collectionGroupOrThrow(orgOrThrow(organizationId), collectionGroupId)
    fun collectionGroupOrThrow(org: Organization, collectionGroupId: UUID) = collectionGroupRepo.findByIdOrNull(collectionGroupId)
        ?.let { if (it.organization!!.publicId == org.publicId) it else null }
        ?: throw CollectionGroupNotFound(collectionGroupId.toString())

    fun cookieOrThrow(org: OrganizationPublicId, domain: String, name: String) = cookieOrThrow(orgOrThrow(org), domain, name)
    fun cookieOrThrow(org: Organization, domain: String, name: String) = orgCookieRepo.findByOrganizationAndNameAndDomain(org, name, domain)
        ?: throw CookieNotFound(domain, name)
}
