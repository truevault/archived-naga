package polaris.controllers.api

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import polaris.repositories.RetentionReasonRepository
import polaris.util.ApiRoutes

@Lazy
@RestController
class PolarisDataController(
    private val retentionReasonRepository: RetentionReasonRepository
) {

    @GetMapping(ApiRoutes.RetentionReason.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRetentionReasons() =
        retentionReasonRepository.findAll().map { it.toDto() }
}
