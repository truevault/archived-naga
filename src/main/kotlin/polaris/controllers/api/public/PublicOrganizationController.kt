package polaris.controllers.api.public

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import polaris.models.dto.organization.PublicOrganizationNoticeDto
import polaris.services.primary.OrganizationService
import polaris.util.ApiRoutes
import java.util.UUID

@RestController
@Lazy
class PublicOrganizationController(private val orgService: OrganizationService) {
    @GetMapping(ApiRoutes.Public.ORG_NOTICES, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrganizationNotices(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: UUID): PublicOrganizationNoticeDto {
        return orgService.getPublicNotices(organizationId)
    }
}
