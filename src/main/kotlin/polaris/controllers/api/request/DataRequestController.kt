package polaris.controllers.api.request

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.DataRequestPublicId
import polaris.models.OrganizationPublicId
import polaris.models.dto.PaginatedMetaDto
import polaris.models.dto.request.*
import polaris.models.dto.toIso
import polaris.models.entity.organization.OrganizationAuthTokenProvider
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubstate
import polaris.models.entity.request.RequestWorkflow
import polaris.repositories.OrganizationAuthTokenRepository
import polaris.repositories.PrivacyCenterRepository
import polaris.services.dto.DataSubjectRequestDtoService
import polaris.services.primary.*
import polaris.services.support.EmailService
import polaris.services.support.PdfService
import polaris.services.support.autodelete.GoogleAnalyticsAutodeleteService
import polaris.util.ApiRoutes
import java.io.IOException
import java.net.URLEncoder
import java.time.ZonedDateTime
import javax.servlet.http.HttpServletResponse

@RestController
@Lazy
class DataRequestController(
    private val dataSubjectRequestService: DataSubjectRequestService,
    private val requestStatsService: DataRequestStatsService,
    private val dto: DataSubjectRequestDtoService,
    private val privacyCenterRepository: PrivacyCenterRepository,
    private val gaAutodelete: GoogleAnalyticsAutodeleteService,
    private val emailService: EmailService,
    private val lookup: LookupService,
    private val orgAuthTokenRepo: OrganizationAuthTokenRepository,
    private val pdfService: PdfService
) : SessionAwareController() {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(DataRequestController::class.java)
    }

    @GetMapping(ApiRoutes.Organization.Requests.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRequests(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.Requests.Param.FILTER) filter: DataRequestFilter?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.TYPE) type: RequestTypeFilter?,
        @RequestParam(ApiRoutes.Param.PAGE) page: Int?,
        @RequestParam(ApiRoutes.Param.PER) per: Int?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.SUBJECT_EMAIL_ADDRESS) email: String?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.DUE_IN) dueIn: Int?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.STATE) state: String?,
        @RequestParam(ApiRoutes.Param.SORT) sort: String?,
        @RequestParam(ApiRoutes.Param.DESC) desc: Boolean = true,
        @RequestParam(ApiRoutes.Organization.Requests.Param.REQUEST_DATE_START) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) requestDateStart: ZonedDateTime?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.REQUEST_DATE_END) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) requestDateEnd: ZonedDateTime?
    ) = authorizeOrg(organizationId) {
        // paginated
        val res = dataSubjectRequestService.paginatedFindForOrganization(
            organizationId = organizationId,
            filter = filter ?: DataRequestFilter.all,
            search = listOfNotNull(
                email?.let { DataRequestSearchParam.SubjectEmail(it) },
                dueIn?.let { DataRequestSearchParam.DueDate(it) },
                state?.let { DataRequestSearchParam.RequestState(it) },

                // if both are null, this filter is a no-op
                DataRequestSearchParam.RequestDateRange(requestDateStart, requestDateEnd)
            ),

            sort = DataRequestSort(id = DataRequestSortId.valueOf(sort ?: DataRequestSortId.dueIn.toString()), desc = desc),
            type = type ?: RequestTypeFilter.all, page = page ?: 0, per = per ?: 15
        )
        val data = res.content.map { dto.toSummaryDto(it) }
        val meta = PaginatedMetaDto(page = res.number, per = res.pageable.pageSize, count = res.content.size, totalPages = res.totalPages, totalCount = res.totalElements.toInt())

        PaginatedSubjectRequestDto(data = data, meta = meta)
    }

    @GetMapping(ApiRoutes.Organization.Requests.CSV_EXPORT, produces = [MediaType.TEXT_PLAIN_VALUE])
    fun getRequestsCsv(
        servletResponse: HttpServletResponse,
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.Requests.Param.FILTER) filter: DataRequestFilter?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.TYPE) type: RequestTypeFilter?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.SUBJECT_EMAIL_ADDRESS) email: String?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.DUE_IN) dueIn: Int?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.STATE) state: String?,
        @RequestParam(ApiRoutes.Param.SORT) sort: String?,
        @RequestParam(ApiRoutes.Param.DESC) desc: Boolean = true,
        @RequestParam(ApiRoutes.Organization.Requests.Param.REQUEST_DATE_START) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) requestDateStart: ZonedDateTime?,
        @RequestParam(ApiRoutes.Organization.Requests.Param.REQUEST_DATE_END) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) requestDateEnd: ZonedDateTime?
    ) = authorizeOrg(organizationId) {
        val res = dataSubjectRequestService.unpaginatedFindForOrganization(
            organizationId = organizationId,
            filter = filter ?: DataRequestFilter.all,
            search = listOfNotNull(
                email?.let { DataRequestSearchParam.SubjectEmail(it) },
                dueIn?.let { DataRequestSearchParam.DueDate(it) },
                state?.let { DataRequestSearchParam.RequestState(it) },

                // if both are null, this filter is a no-op
                DataRequestSearchParam.RequestDateRange(requestDateStart, requestDateEnd)
            ),

            sort = DataRequestSort(id = DataRequestSortId.valueOf(sort ?: DataRequestSortId.dueIn.toString()), desc = desc),
            type = type ?: RequestTypeFilter.all
        )
        val data = res.content.map { dto.toSummaryDto(it) }

        val now = ZonedDateTime.now().toIso()
        val filename = URLEncoder.encode("$now-request-export", "utf-8")

        servletResponse.contentType = MediaType.TEXT_PLAIN_VALUE
        servletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=$filename.csv")

        val printer = CSVPrinter(servletResponse.getWriter(), CSVFormat.DEFAULT)
        try {
            printer.printRecord("ID", "Requester", "First Name", "Last Name", "Status", "Request Type", "Request Date", "Due Date", "Closed At")
            data.forEach { req ->
                printer.printRecord(req.id, req.subjectEmailAddress, req.subjectFirstName, req.subjectLastName, req.state, req.dataRequestType.name, req.requestDate, req.dueDate, req.closedAt)
            }
        } catch (e: IOException) {
            log.error("Error while writing Request CSV", e)
        }
    }

    @GetMapping(ApiRoutes.Organization.Requests.STATS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getStats(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) = authorizeOrg(organizationId) {
        requestStatsService.getStats(organizationId = organizationId)
    }

    @GetMapping(ApiRoutes.Organization.Requests.FOR_CONSUMER, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRequestsForConsumer(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.Requests.Param.SUBJECT_EMAIL_ADDRESS) email: String
    ) = authorizeOrg(organizationId) {
        val requests = dataSubjectRequestService.findAllBySubjectEmail(organizationId, email)
        val requestDtos = requests.map { dto.toDetailDto(it) }.sortedBy { it.requestDate }

        requestDtos
    }

    @GetMapping(ApiRoutes.Organization.Requests.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRequest(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId
    ) = authorizeOrg(organizationId) {
        dto.toDetailDto(dataSubjectRequestService.findByPublicId(organizationId = organizationId, requestId = requestId))
    }

    @GetMapping(ApiRoutes.Organization.Requests.EXPORT, produces = [MediaType.APPLICATION_PDF_VALUE])
    fun exportForSubject(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.Requests.Param.SUBJECT_EMAIL_ADDRESS) email: String
    ) = authorizeOrg(organizationId) {
        val requests = dataSubjectRequestService.findAllBySubjectEmail(organizationId, email)
        val requestDtos = requests.map { dto.toDetailDto(it) }.sortedBy { it.requestDate }

        val file = pdfService.pdfFromTemplate("pdfs/consumer_report.ftl", mapOf("consumerEmail" to email, "requests" to requestDtos))
        val bytes = file.readBytes()
        val now = ZonedDateTime.now().toIso()
        val filename = URLEncoder.encode("$now-consumer-report-$email", "utf-8")

        ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF).header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=$filename.pdf").body(bytes)
    }

    @GetMapping(ApiRoutes.Organization.Requests.CLOSED_TEMPLATE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getClosedTemplate(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId
    ) = authorizeOrg(organizationId) {
        val dataRequest = dataSubjectRequestService.findByPublicId(organizationId, requestId)
        val actor = requireCurrentUser()
        val workflow = dataRequest.requestType.workflow
        if (dataRequest.collectionGroupResult == SubjectStatus.NOT_FOUND) {
            emailService.getSubjectRequestNotFoundBody(actor, dataRequest)
        } else if (dataRequest.collectionGroupResult == SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER) {
            emailService.getSubjectRequestServiceProviderBody(actor, dataRequest)
        } else {
            when (workflow) {
                RequestWorkflow.OPT_OUT -> emailService.getSubjectRequestClosedOptOutBody(actor, dataRequest)
                RequestWorkflow.KNOW -> emailService.getSubjectRequestClosedKnowBody(actor, dataRequest)
                RequestWorkflow.DELETE -> emailService.getSubjectRequestClosedDeleteBody(actor, dataRequest)
                RequestWorkflow.PORTABILITY -> emailService.getSubjectRequestClosedPortabilityBody(actor, dataRequest)
                RequestWorkflow.RECTIFICATION -> emailService.getSubjectRequestClosedRectificationBody(actor, dataRequest)
                RequestWorkflow.WITHDRAW_CONSENT -> emailService.getSubjectRequestClosedWithdrawnConsentBody(actor, dataRequest)
                RequestWorkflow.OBJECTION -> emailService.getSubjectRequestClosedObjectionBody(actor, dataRequest)
                RequestWorkflow.NONE -> emailService.getSubjectRequestClosedKnowBody(actor, dataRequest)
                RequestWorkflow.LIMIT -> emailService.getSubjectRequestClosedLimitBody(actor, dataRequest)
            }
        }
    }

    @GetMapping(ApiRoutes.Organization.Requests.CONTACT_VENDOR_TEMPLATE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getContactVendorTemplate(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId
    ) = authorizeOrg(organizationId) {
        val dataRequest = dataSubjectRequestService.findByPublicId(organizationId, requestId)
        val actor = requireCurrentUser()
        val workflow = dataRequest.requestType.workflow
        when (workflow) {
            RequestWorkflow.LIMIT -> emailService.getServiceProviderRequestToLimitBody(actor, dataRequest)
            else -> emailService.getServiceProviderRequestToDeleteBody(actor, dataRequest)
        }
    }

    @DeleteMapping(ApiRoutes.Organization.Requests.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun deleteRequest(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId
    ) = authorizeOrg(organizationId) {
        dataSubjectRequestService.deleteByPublicId(requireCurrentUser(), organizationId, requestId)
        mapOf("ok" to true)
    }

    @PutMapping(ApiRoutes.Organization.Requests.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateRequest(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestBody updatedRequest: UpdateDataRequest
    ) = authorizeOrg(organizationId) {
        dto.toDetailDto(dataSubjectRequestService.updateRequest(actor = requireCurrentUser(), organizationId = organizationId, requestId = requestId, dto = updatedRequest))
    }

    @PostMapping(ApiRoutes.Organization.Requests.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    fun createRequest(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody createDataRequest: CreateDataRequest
    ) = authorizeOrg(organizationId) {
        dto.toDetailDto(dataSubjectRequestService.create(requireCurrentUser(), organizationId, createDataRequest))
    }

    @PutMapping(ApiRoutes.Organization.Requests.STATE, produces = [MediaType.APPLICATION_JSON_VALUE], consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun transitionRequest(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestParam("state") state: DataRequestState,
        @RequestParam("substate") substate: DataRequestSubstate?,
        @RequestParam("notes") notes: String?,
        @RequestParam("attachment") attachment: MultipartFile?
    ) = authorizeOrg(organizationId) { events ->
        val transitionDataRequest = TransitionDataRequest(
            state = state,
            substate = substate,
            notes = notes,
            attachment = attachment
        )
        dto.toDetailDto(dataSubjectRequestService.transition(requireCurrentUser(), organizationId, requestId, transitionDataRequest, events))
    }

    @PostMapping(ApiRoutes.Organization.Requests.VERIFY, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    fun verifyRequest(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestBody verifyDataRequest: VerifyDataRequest
    ) = authorizeOrg(organizationId) { events ->
        dto.toDetailDto(dataSubjectRequestService.verify(requireCurrentUser(), organizationId, requestId, verifyDataRequest, events))
    }

    @PostMapping(ApiRoutes.Organization.Requests.CONFIRM, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    fun confirmRequestIdentity(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestBody confirmIdentityRequest: ConfirmIdentityDataRequest
    ) = authorizeOrg(organizationId) { events ->
        val privacyCenter = privacyCenterRepository.findOneByOrganizationId(organizationId)
        dto.toDetailDto(
            dataSubjectRequestService.confirmEmail(
                requireCurrentUser(), organizationId, requestId, confirmIdentityRequest, privacyCenter,
                ZonedDateTime.now(
                    POLARIS_TIME_ZONE
                ),
                events
            )
        )
    }

    @PostMapping(ApiRoutes.Organization.Requests.NOTE, produces = [MediaType.APPLICATION_JSON_VALUE], consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    fun createNote(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestParam("message") message: String,
        @RequestParam("attachments[]") attachments: List<MultipartFile>?
    ) = authorizeOrg(organizationId) {
        dto.toDetailDto(dataSubjectRequestService.createNote(requireCurrentUser(), organizationId, requestId, CreateNoteRequest(message, attachments)))
    }

    @PutMapping(ApiRoutes.Organization.Requests.VENDOR, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateVendor(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestBody updateVendorRequest: UpdateVendorRequest
    ) = authorizeOrg(organizationId) {
        dto.toDetailDto(dataSubjectRequestService.updateVendorOutcome(requireCurrentUser(), organizationId, requestId, updateVendorRequest))
    }

    @PostMapping(ApiRoutes.Organization.Requests.Autodelete.GOOGLE_ANALYTICS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun autodeleteGoogleAnalytics(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val auth = orgAuthTokenRepo.findByOrganizationAndProvider(org, OrganizationAuthTokenProvider.GOOGLE_OAUTH)

        val request = dataSubjectRequestService.findByPublicId(organizationId = organizationId, requestId = requestId)
        gaAutodelete.autodelete(org, auth, request)
    }

    @PutMapping(ApiRoutes.Organization.Requests.CONTACT_VENDORS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun contactVendors(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestBody contactVendorRequest: ContactVendorRequest
    ) = authorizeOrg(organizationId) {
        dto.toDetailDto(dataSubjectRequestService.contactVendors(requireCurrentUser(), organizationId, requestId, contactVendorRequest))
    }

    @PutMapping(ApiRoutes.Organization.Requests.CONTACT_REQUESTOR, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun contactRequestor(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestPart("attachment") attachment: List<MultipartFile>?
    ) = authorizeOrg(organizationId) {
        dto.toDetailDto(dataSubjectRequestService.contactRequestor(requireCurrentUser(), organizationId, requestId, attachment ?: emptyList()))
    }

    @PutMapping(ApiRoutes.Organization.Requests.REOPEN, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun reopen(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId
    ) = authorizeOrg(organizationId) { events ->
        dto.toDetailDto(dataSubjectRequestService.reopen(requireCurrentUser(), organizationId, requestId, events))
    }

    @GetMapping(ApiRoutes.Organization.Requests.STEP_FORWARD_REQUEST, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun stepRequestForward(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId
    ) = authorizeOrg(organizationId) { events ->
        dto.toDetailDto(dataSubjectRequestService.stepForwardRequest(requireCurrentUser(), organizationId, requestId, events))
    }
    @GetMapping(ApiRoutes.Organization.Requests.STEP_BACK_REQUEST, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun stepRequestBack(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId
    ) = authorizeOrg(organizationId) { events ->
        dto.toDetailDto(dataSubjectRequestService.stepBackRequest(requireCurrentUser(), organizationId, requestId, events))
    }

    @PutMapping(ApiRoutes.Organization.Requests.SET_SUBJECT_STATUS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun setRequestSubjectStatus(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestBody statusUpdate: RequestSubjectStatusUpdate
    ) = authorizeOrg(organizationId) {

        dto.toDetailDto(dataSubjectRequestService.setRequestSubjectStatus(requireCurrentUser(), organizationId, requestId, statusUpdate))
    }

    @PutMapping(ApiRoutes.Organization.Requests.EXTEND, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun extend(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Requests.Param.REQUEST_ID) requestId: DataRequestPublicId,
        @RequestBody extendDataRequest: ExtendDataRequest
    ) = authorizeOrg(organizationId) {
        dto.toDetailDto(dataSubjectRequestService.extend(actor = requireCurrentUser(), organizationId = organizationId, requestId = requestId, dto = extendDataRequest))
    }
}
