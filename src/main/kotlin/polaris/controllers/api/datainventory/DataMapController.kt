package polaris.controllers.api.datainventory

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.compliance.UpdateDataMapRequest
import polaris.models.dto.compliance.V2DataMapDto
import polaris.repositories.CollectionGroupCollectedPICRepo
import polaris.services.support.DataMapService
import polaris.util.ApiRoutes
import java.util.UUID

data class UpdateCollectionRequest(val collected: Boolean)
data class UpdateRecipientAssocationRequest(val associated: Boolean, val updateDisclosure: Boolean?)
data class UpdateSourceAssocationRequest(val associated: Boolean, val updateReceived: Boolean?)
data class UpdateDisclosureRequest(val disclosed: Boolean, val updateCollection: Boolean)
data class UpdateReceivedRequest(val received: Boolean, val updateCollection: Boolean)

@RestController
@Lazy
class DataMapController(val dataMapService: DataMapService, val lookup: LookupService, val collectedPICRepo: CollectionGroupCollectedPICRepo) : SessionAwareController() {

    @GetMapping(ApiRoutes.Organization.DataMap.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getDataMap(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): V2DataMapDto = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        dataMapService.getDataMap(org)
    }

    @PatchMapping(ApiRoutes.Organization.DataMap.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun patchDataMap(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody update: UpdateDataMapRequest
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        dataMapService.updateDataMap(org, update.operations)
    }

    @PostMapping(ApiRoutes.Organization.DataMap.PIC_COLLECTION, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateCollection(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.COLLECTION_GROUP_ID) collectionGroupId: UUID,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.PIC_ID) picId: UUID,
        @RequestBody update: UpdateCollectionRequest
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val cg = lookup.collectionGroupOrThrow(org, collectionGroupId)
        val pic = lookup.picOrThrow(picId)
        dataMapService.updateCollection(org, pic, cg, update)
    }

    @PostMapping(ApiRoutes.Organization.DataMap.PIC_ASSOCIATION, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateRecipientAssociation(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.COLLECTION_GROUP_ID) collectionGroupId: UUID,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.VENDOR_ID) vendorId: UUID,
        @RequestBody update: UpdateRecipientAssocationRequest
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val cg = lookup.collectionGroupOrThrow(org, collectionGroupId)
        val vendor = lookup.vendorOrThrow(vendorId)
        dataMapService.updateRecipientAssociation(org, vendor, cg, update)
    }

    @PostMapping(ApiRoutes.Organization.DataMap.PIC_SOURCE_ASSOCIATION, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateSourceAssociation(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.COLLECTION_GROUP_ID) collectionGroupId: UUID,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.VENDOR_ID) vendorId: UUID,
        @RequestBody update: UpdateSourceAssocationRequest
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val cg = lookup.collectionGroupOrThrow(org, collectionGroupId)
        val vendor = lookup.vendorOrThrow(vendorId)
        dataMapService.updateSourceAssociation(org, vendor, cg, update)
    }

    @PostMapping(ApiRoutes.Organization.DataMap.PIC_DISCLOSURE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateDisclosure(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.COLLECTION_GROUP_ID) collectionGroupId: UUID,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.VENDOR_ID) vendorId: UUID,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.PIC_GROUP_ID) picGroupId: UUID,
        @RequestBody update: UpdateDisclosureRequest
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val cg = lookup.collectionGroupOrThrow(org, collectionGroupId)
        val vendor = lookup.vendorOrThrow(vendorId)
        val picGroup = lookup.picGroupOrThrow(picGroupId)
        dataMapService.updateDisclosure(org, vendor, cg, picGroup, update)
    }

    @PostMapping(ApiRoutes.Organization.DataMap.PIC_RECEIVED, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateReceived(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.COLLECTION_GROUP_ID) collectionGroupId: UUID,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.VENDOR_ID) vendorId: UUID,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.PIC_ID) picId: UUID,
        @RequestBody update: UpdateReceivedRequest
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val cg = lookup.collectionGroupOrThrow(org, collectionGroupId)
        val vendor = lookup.vendorOrThrow(vendorId)
        val pic = lookup.picOrThrow(picId)
        dataMapService.updateReceived(org, vendor, cg, pic, update)
    }
}
