package polaris.controllers.api.datainventory

import org.hibernate.StaleStateException
import org.postgresql.util.PSQLException
import org.springframework.context.annotation.Lazy
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.compliance.*
import polaris.services.primary.PersonalInformationService
import polaris.util.ApiRoutes
import java.util.*

@RestController
@Lazy
/**
 * LEGACY - Note that this controller is now a legacy controller. It still contains valid business logic, but new functionality
 * should not be added here.
 *
 * New functionality should live in the DataMapController. As much as possible, functions here should be eliminated from use
 * and removed.
 */
class DataInventoryController(
    private val personalInformationService: PersonalInformationService
) : SessionAwareController() {
    @GetMapping(ApiRoutes.Organization.DataInventory.Collection.DEFAULT, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getDefaultPersonalInformationCollected(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): PersonalInformationCollectedDto = authorizeOrg(organizationId) {
        personalInformationService.getDefaultPersonalInformationCollected(organizationId)
    }

    @GetMapping(ApiRoutes.Organization.DataInventory.SNAPSHOT, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getSnapshot(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): OrgPersonalInformationSnapshotDto = authorizeOrg(organizationId) {
        personalInformationService.getOrganizationSnapshot(organizationId)
    }

    @GetMapping(ApiRoutes.Organization.DataInventory.Collection.BATCH, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrganizationDataSubjectTypePersonalInformationOptions(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID
    ) = authorizeOrg(organizationId) {
        PersonalInformationCollectedDto(
            personalInformationCategories = personalInformationService.getCollectionGroupPIC(
                organizationId,
                dataSubjectTypeId
            ),
            businessPurposes = personalInformationService.getOrganizationDataSubjectTypeBusinessPurposesDto(
                organizationId,
                dataSubjectTypeId
            ),
            exchanged = personalInformationService.getOrgConsumerGroupExchangedCategoriesDto(organizationId, dataSubjectTypeId)
        )
    }

    @PutMapping(ApiRoutes.Organization.DataInventory.Collection.BATCH, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun setOrganizationDataSubjectTypePersonalInformationOptions(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID,
        @RequestBody updateCollected: UpdatePersonalInformationCollectedDto
    ) = authorizeOrg(organizationId) {
        personalInformationService.setOrganizationDataSubjectTypePersonalInformationCategories(
            organizationId, dataSubjectTypeId, updateCollected.personalInformationCategories
        )
        personalInformationService.setOrganizationDataSubjectTypeBusinessPurposes(
            organizationId, dataSubjectTypeId, updateCollected.businessPurposes
        )
        mapOf("ok" to true)
    }

    @PutMapping(
        ApiRoutes.Organization.DataInventory.Collection.SPECIFIC_CATEGORY,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun putOrganizationDataSubjectTypePersonalInformationCategory(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.CATEGORY_ID) categoryId: UUID
    ) = authorizeOrg(organizationId) {
        personalInformationService.addOrganizationDataSubjectTypePersonalInformationCategory(
            organizationId,
            dataSubjectTypeId,
            categoryId
        )
        mapOf("ok" to true)
    }

    @DeleteMapping(
        ApiRoutes.Organization.DataInventory.Collection.SPECIFIC_CATEGORY,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun deleteOrganizationDataSubjectTypePersonalInformationCategory(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.CATEGORY_ID) categoryId: UUID
    ) = authorizeOrg(organizationId) {
        personalInformationService.removeOrganizationDataSubjectTypePersonalInformationCategory(
            organizationId,
            dataSubjectTypeId,
            categoryId
        )
        mapOf("ok" to true)
    }

    @DeleteMapping(
        ApiRoutes.Organization.DataInventory.Collection.CLEAR_INTERNALLY_STORED,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun deleteOrganizationDataSubjectTypePersonalInformationCategoryBatch(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID,
        @RequestBody categoryIds: List<UUID>
    ) = authorizeOrg(organizationId) {
        for (categoryId in categoryIds) {
            personalInformationService.removeOrganizationDataSubjectTypePersonalInformationCategory(
                organizationId,
                dataSubjectTypeId,
                categoryId
            )
        }

        mapOf("ok" to true)
    }

    @PutMapping(
        ApiRoutes.Organization.DataInventory.Collection.SPECIFIC_PURPOSE,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun putOrganizationDataSubjectTypePersonalInformationPurpose(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.PURPOSE_ID) purposeId: UUID
    ) = authorizeOrg(organizationId) {
        personalInformationService.addOrganizationDataSubjectTypePersonalInformationPurpose(
            organizationId,
            dataSubjectTypeId,
            purposeId
        )
        mapOf("ok" to true)
    }

    @DeleteMapping(
        ApiRoutes.Organization.DataInventory.Collection.SPECIFIC_PURPOSE,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun deleteOrganizationDataSubjectTypePersonalInformationPurpose(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID,
        @PathVariable(ApiRoutes.Organization.DataInventory.Collection.Param.PURPOSE_ID) purposeId: UUID
    ) = authorizeOrg(organizationId) {
        personalInformationService.removeOrganizationDataSubjectTypePersonalInformationPurpose(
            organizationId,
            dataSubjectTypeId,
            purposeId
        )
        mapOf("ok" to true)
    }

    @GetMapping(
        ApiRoutes.Organization.DataInventory.Exchange.BASE,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun getOrganizationPersonalInformationExchanged(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        personalInformationService.getOrganizationPersonalInformationExchangedDto(organizationId)
    }

    @GetMapping(
        ApiRoutes.Organization.DataInventory.Exchange.SPECIFIC_CONSUMER_GROUP,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun getOrganizationConsumerGroupPersonalInformationExchanged(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataInventory.Exchange.Param.CONSUMER_GROUP_ID) consumerGroupId: UUID
    ) = authorizeOrg(organizationId) {
        personalInformationService.getOrganizationConsumerGroupPersonalInformationCategoriesDto(
            organizationId,
            consumerGroupId
        )
    }

    @GetMapping(
        ApiRoutes.Organization.DataInventory.Exchange.SPECIFIC_VENDOR,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun getOrganizationVendorPersonalInformation(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.VENDOR_ID) vendorId: UUID
    ) = authorizeOrg(organizationId) {
        personalInformationService.getDataRecipientPIC(organizationId, vendorId)
    }

    @PutMapping(ApiRoutes.Organization.DataInventory.Exchange.SPECIFIC_VENDOR, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun setOrganizationVendorPersonalInformationExchanged(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.VENDOR_ID) vendorId: UUID,
        @RequestBody updateExchanged: List<UpdatePersonalInformationExchangedDto>
    ) = authorizeOrg(organizationId) {
        try {
            personalInformationService.setOrganizationVendorPersonalInformationCategoriesExchangedDto(
                organizationId, vendorId, updateExchanged
            )
        } catch (e: StaleStateException) {
        } catch (e: DataIntegrityViolationException) {
        } catch (e: PSQLException) {}

        mapOf("ok" to true)
    }
}
