package polaris.controllers.api.organization

import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.user.CreateUserRequest
import polaris.models.entity.organization.OrganizationUser
import polaris.models.entity.user.UserStatus
import polaris.services.primary.UserService
import polaris.util.ApiRoutes
import polaris.util.Responses

@RestController
@Lazy
class OrganizationUserController(
    private val userService: UserService
) : SessionAwareController() {

    @GetMapping(ApiRoutes.Organization.Admin.User.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getUsers(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) = authorizeOrg(organizationId) {
        userService.allUsersForOrganization(organizationId = organizationId).map(OrganizationUser::toDto)
    }

    @PostMapping(ApiRoutes.Organization.Admin.User.DEACTIVATE, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.OK)
    fun deactivateUser(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Admin.User.Param.USER) email: String
    ) = authorizeOrg(organizationId) {
        userService.updateUserStatus(email, organizationId, UserStatus.INACTIVE).toDto()
    }

    @PostMapping(ApiRoutes.Organization.Admin.User.ACTIVATE, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.OK)
    fun activateUser(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Admin.User.Param.USER) email: String
    ) = authorizeOrg(organizationId) {
        userService.updateUserStatus(email, organizationId, UserStatus.ACTIVE).toDto()
    }

    @DeleteMapping(ApiRoutes.Organization.Admin.User.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.OK)
    fun deleteUser(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Admin.User.Param.USER) email: String
    ): String = authorizeOrg(organizationId) {
        userService.delete(email, organizationId)
        Responses.EMPTY_SUCCESS
    }

    @PostMapping(ApiRoutes.Organization.Admin.User.INVITE, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    fun inviteUser(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody user: CreateUserRequest
    ) = authorizeOrg(organizationId) {
        userService.inviteUser(user.email, user.firstName, user.lastName, organizationId).toDto()
    }
}
