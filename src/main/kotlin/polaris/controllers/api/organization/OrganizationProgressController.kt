package polaris.controllers.api.organization

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.SurveySlug
import polaris.models.dto.organization.OrganizationProgressDto
import polaris.models.dto.organization.OrganizationProgressUpdateDto
import polaris.models.dto.organization.OrganizationSurveyProgressDto
import polaris.models.dto.organization.UpdateOrganizationSurveyProgressDto
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.organization.OrganizationProgress
import polaris.models.entity.organization.mappingProgressDto
import polaris.repositories.OrganizationProgressRepository
import polaris.services.primary.OrganizationProgressService
import polaris.services.primary.OrganizationService
import polaris.services.support.SurveyProgressService
import polaris.util.ApiRoutes

@RestController
@Lazy
class OrganizationProgressController(
    private val organizationProgressService: OrganizationProgressService,
    private val surveyProgressService: SurveyProgressService,
    private val organizationService: OrganizationService,
    private val organizationProgressRepo: OrganizationProgressRepository,
    private val lookup: LookupService
) : SessionAwareController() {

    companion object {
        private val log = LoggerFactory.getLogger(OrganizationProgressController::class.java)
    }

    @GetMapping(ApiRoutes.Organization.Progress.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrganizationProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.Progress.Param.PROGRESS_KEYS, required = false, defaultValue = "") progressKeys: List<String> = emptyList()
    ) =
        authorizeOrg(organizationId) {
            val o = organizationService.findByPublicId(organizationId = organizationId)

            val progressDtos = if (progressKeys.isEmpty()) {
                val progressValues = organizationProgressRepo.findByOrganizationId(o.id)
                progressValues.map { dto(it) }
            } else {
                val existingValues = organizationProgressRepo.findByOrganizationIdAndProgressKeyIn(o.id, progressKeys)
                val existingDtos = existingValues.map { dto(it) }
                val missingKeys = progressKeys.filter { key -> existingDtos.none { dto -> dto.key == key } }
                val missingDtos = missingKeys.map { defaultDto(it) }

                existingDtos + missingDtos
            }

            progressDtos
        }

    @PutMapping(ApiRoutes.Organization.Progress.SPECIFIC_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun putOrganizationProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Progress.Param.KEY) key: String,
        @RequestBody progress: OrganizationProgressUpdateDto
    ) = authorizeOrg(organizationId) { events ->
        try {
            dto(organizationProgressService.addOrganizationProgress(organizationId, key, progress, events))
        } catch (e: DataIntegrityViolationException) {
            // retry once
            dto(organizationProgressService.addOrganizationProgress(organizationId, key, progress, events))
        }
    }

    @GetMapping(ApiRoutes.Organization.Progress.SPECIFIC_SURVEY_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrganizationSurveyProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Progress.Param.SLUG, required = true) slug: SurveySlug
    ) =
        authorizeOrg(organizationId) {
            val org = lookup.orgOrThrow(organizationId)

            OrganizationSurveyProgressDto(survey = slug, progress = surveyProgressService.getSurveyProgress(org, slug))
        }

    @PutMapping(ApiRoutes.Organization.Progress.SPECIFIC_SURVEY_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationSurveyProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Progress.Param.SLUG) slug: SurveySlug,
        @RequestBody dto: UpdateOrganizationSurveyProgressDto
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        // TODO: POLARIS-1952 - wire up event tracking
        surveyProgressService.updateSurveyProgress(org, slug, dto.progress)
        OrganizationSurveyProgressDto(survey = slug, progress = dto.progress)
    }

    @PostMapping(ApiRoutes.Organization.Progress.START_SPECIFIC_SURVEY, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun startOrganizationSurvey(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Progress.Param.SLUG) slug: SurveySlug
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        surveyProgressService.startSurvey(org, slug)
    }

    @PostMapping(ApiRoutes.Organization.Progress.COMPLETE_SPECIFIC_SURVEY, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun completeOrganizationSurvey(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Progress.Param.SLUG) slug: SurveySlug
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        surveyProgressService.completeSurvey(org, slug)
    }

    private fun dto(progress: OrganizationProgress): OrganizationProgressDto {
        return OrganizationProgressDto(
            key = progress.progressKey!!,
            progress = mappingProgressDto(progress.progressValue)
        )
    }

    private fun defaultDto(key: String): OrganizationProgressDto {
        return OrganizationProgressDto(
            key = key,
            progress = mappingProgressDto(MappingProgressEnum.NOT_STARTED)
        )
    }
}
