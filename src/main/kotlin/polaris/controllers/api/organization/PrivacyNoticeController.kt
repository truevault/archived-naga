package polaris.controllers.api.organization

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.entity.vendor.ProcessingRegion
import polaris.services.primary.PrivacyNoticeService
import polaris.services.primary.PrivacyPolicyService
import polaris.services.support.OptOutService
import polaris.util.ApiRoutes
import java.util.*

@RestController
@Lazy
class PrivacyNoticeController(
    private val privacyNoticeService: PrivacyNoticeService,
    private val privacyPolicyService: PrivacyPolicyService,
    private val optOutService: OptOutService,
    private val lookup: LookupService
) : SessionAwareController() {

    @GetMapping(ApiRoutes.Organization.PrivacyNotice.NOTICE_INTRO, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCollectionGroupPrivacyNotice(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.PrivacyNotice.Param.CONSUMER_GROUP_ID) consumerGroupId: UUID
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val cg = lookup.collectionGroupOrThrow(org, consumerGroupId)
        privacyNoticeService.getEmployeeCollectionGroupNoticeIntro(org, cg)
    }

    @GetMapping(ApiRoutes.Organization.PrivacyNotice.CA_PRIVACY_NOTICE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCaPrivacyNotice(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        mapOf("notice" to privacyPolicyService.getPrivacyPolicyHtml(org, false))
    }

    @GetMapping(
        ApiRoutes.Organization.PrivacyNotice.OPT_OUT_PRIVACY_NOTICE,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun getOptOutPrivacyNotice(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        mapOf(
            "notice" to privacyNoticeService.getOptOutPrivacyNoticeHtml(organizationId),
            "label" to optOutService.getOptOutPageLabel(org.privacyCenter()),
            "url" to optOutService.getOptOutUrl(org.privacyCenter())
        )
    }

    @GetMapping(ApiRoutes.Organization.PrivacyNotice.CONSUMER_GROUP_NOTICE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getEmployeeCollectionGroupPrivacyNotice(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.PrivacyNotice.Param.CONSUMER_GROUP_ID) consumerGroupId: UUID
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val cg = lookup.collectionGroupOrThrow(org, consumerGroupId)
        if (cg.processingRegions.contains(ProcessingRegion.EEA_UK) && !cg.processingRegions.contains(ProcessingRegion.UNITED_STATES)) {
            mapOf("notice" to privacyNoticeService.getEeaUkEmployeeNoticeHtml(org, cg))
        } else {
            mapOf("notice" to privacyNoticeService.getEmployeeCollectionGroupNoticeHtml(organizationId, consumerGroupId))
        }
    }
}
