package polaris.controllers.api.organization

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.AddOrganizationDataSourceDto
import polaris.models.dto.organization.OrganizationDataSourceDto
import polaris.models.entity.MultipleVendorsNotAllowed
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.OrganizationDataSource
import polaris.repositories.OrganizationDataSourceRepository
import polaris.services.primary.OrganizationDataSourceService
import polaris.util.ApiRoutes
import java.util.UUID

@RestController
@Lazy
class OrganizationDataSourceController(
    private val lookup: LookupService,
    private val orgDataSourceRepo: OrganizationDataSourceRepository,
    private val orgDataSourceService: OrganizationDataSourceService,
    @Value("\${polaris.images.baseUrl}")
    private val imagesBaseUrl: String?
) :
    SessionAwareController() {

    @GetMapping(ApiRoutes.Organization.DataSource.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrgDataSources(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        val o = lookup.orgOrThrow(organizationId)
        val orgDataSources = orgDataSourceRepo.findAllByOrganization(o)

        orgDataSources.map { toDto(it) }
    }

    @PostMapping(ApiRoutes.Organization.DataSource.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun addOrgDataSource(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody addSource: AddOrganizationDataSourceDto
    ) = authorizeOrg(organizationId) {
        if (addSource.custom != null && addSource.vendorId != null) {
            throw MultipleVendorsNotAllowed()
        }

        if (addSource.custom == null && addSource.vendorId == null) {
            throw MultipleVendorsNotAllowed()
        }

        val source = if (addSource.vendorId != null) {
            orgDataSourceService.addDataSource(organizationId, addSource.vendorId, addSource.collectionContext ?: listOf(CollectionContext.CONSUMER))
        } else {
            orgDataSourceService.addDataSource(organizationId, addSource.custom!!, addSource.collectionContext ?: listOf(CollectionContext.CONSUMER))
        }

        toDto(source)
    }

    @DeleteMapping(ApiRoutes.Organization.DataSource.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun removeOrgDataSource(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataSource.Param.ID) vendorId: UUID,
        @RequestParam("collectionContext", required = false, defaultValue = "") collectionContext: List<CollectionContext> = emptyList()
    ) = authorizeOrg(organizationId) {
        orgDataSourceService.removeDataSource(organizationId, vendorId, collectionContext)
    }

    private fun toDto(source: OrganizationDataSource): OrganizationDataSourceDto {
        return OrganizationDataSourceDto(
            vendorId = source.vendor!!.id.toString(),
            name = source.vendor.name,
            vendorKey = source.vendor.vendorKey,
            aliases = source.vendor.aliases,
            category = source.vendor.recipientCategory,
            url = source.vendor.url,
            email = source.vendor.email,
            collectionContext = source.collectionContexts,
            isCustom = source.vendor.organization != null,
            logoUrl = logoUrl(source.vendor.logoFilename)
        )
    }

    private fun logoUrl(logoFilename: String?): String? {
        return logoFilename?.let { "$imagesBaseUrl/$logoFilename" }
    }
}
