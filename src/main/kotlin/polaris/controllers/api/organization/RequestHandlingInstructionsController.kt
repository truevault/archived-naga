package polaris.controllers.api.organization

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.UUIDString
import polaris.models.dto.organization.*
import polaris.models.entity.request.DataRequestInstructionType
import polaris.services.primary.RequestHandlingInstructionsService
import polaris.util.ApiRoutes

@RestController
@Lazy
class RequestHandlingInstructionsController(
    private val requestHandlingInstructionsService: RequestHandlingInstructionsService
) : SessionAwareController() {
    @GetMapping(ApiRoutes.Organization.RequestHandlingInstructions.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getInstructions(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): List<RequestHandlingInstructionDto> = authorizeOrg(organizationId) {
        requestHandlingInstructionsService.autoSelectInaccessibleForAdNetworks(organizationId)
        val processingInstructions = requestHandlingInstructionsService.findAllByOrganization(organizationId)
        processingInstructions.map { it.toDto() }
    }

    @GetMapping(ApiRoutes.Organization.RequestHandlingInstructions.BY_TYPE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getInstructionsByType(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.RequestHandlingInstructions.Param.REQUEST_TYPE) requestType: DataRequestInstructionType
    ): List<RequestHandlingInstructionDto> = authorizeOrg(organizationId) {
        requestHandlingInstructionsService.autoSelectInaccessibleForAdNetworks(organizationId)
        val processingInstructions = requestHandlingInstructionsService.findAllByOrganizationAndType(organizationId, requestType)
        processingInstructions.map { it.toDto() }
    }

    @PutMapping(ApiRoutes.Organization.RequestHandlingInstructions.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun upsertInstruction(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody dto: RequestHandlingInstructionDto
    ): RequestHandlingInstructionDto = authorizeOrg(organizationId) {
        requestHandlingInstructionsService.createOrUpdate(organizationId, dto, deleteOnNull = true).toDto()
    }

    @DeleteMapping(ApiRoutes.Organization.RequestHandlingInstructions.INSTRUCTION, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun deleteInstruction(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.RequestHandlingInstructions.Param.INSTRUCTION_ID) instructionId: UUIDString
    ): RequestHandlingInstructionDto? = authorizeOrg(organizationId) {
        requestHandlingInstructionsService.delete(organizationId, instructionId)?.toDto()
    }

    @GetMapping(ApiRoutes.Organization.RequestHandlingInstructions.CONSUMER_GROUP, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCGInstructions(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): String = authorizeOrg(organizationId) {
        requestHandlingInstructionsService.getConsumerGroupHandlingInstructions(organizationId)
    }

    @PutMapping(ApiRoutes.Organization.RequestHandlingInstructions.CONSUMER_GROUP, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateConsumerGroupRequestHandlingInstructions(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody dto: UpdateOrganizationCGInstructionsDto
    ): UpdateOrganizationCGInstructionsDto = authorizeOrg(organizationId) {
        requestHandlingInstructionsService.updateConsumerGroupRequestHandlingInstructions(dto)
    }

    @GetMapping(ApiRoutes.Organization.RequestHandlingInstructions.OPT_OUT_INSTRUCTIONS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOptOutInstructions(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): String = authorizeOrg(organizationId) {
        requestHandlingInstructionsService.getOptOutInstructions(organizationId)
    }

    @PutMapping(ApiRoutes.Organization.RequestHandlingInstructions.OPT_OUT_INSTRUCTIONS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOptOutInstructions(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody dto: UpdateOrganizationCGInstructionsDto
    ): UpdateOrganizationCGInstructionsDto = authorizeOrg(organizationId) {
        requestHandlingInstructionsService.updateOptOutInstructions(dto)
    }
}
