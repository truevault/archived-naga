package polaris.controllers.api.organization

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.view.RedirectView
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.services.support.oauth.AuthTokenService
import polaris.services.support.oauth.GoogleAuthTokenService
import polaris.util.ApiRoutes

@RestController
@Lazy
class OrganizationAuthController(
    private val authTokenService: AuthTokenService,
    private val googleAuthTokenService: GoogleAuthTokenService
) : SessionAwareController() {
    @GetMapping(ApiRoutes.Organization.Authorization.AUTHORIZATIONS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAuthorizations(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) =
        authorizeOrg(organizationId) {
            authTokenService.getAuthorizations(organizationId)
        }

    @GetMapping(ApiRoutes.Organization.Authorization.GOOGLE)
    fun authorizeGoogleAnalytics(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) =
        authorizeOrg(organizationId) {
            RedirectView(googleAuthTokenService.generateGoogleAnalyticsOAuthUrl(organizationId))
        }

    @DeleteMapping(ApiRoutes.Organization.Authorization.GOOGLE)
    fun disconnectGoogleAnalytics(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) =
        authorizeOrg(organizationId) {
            googleAuthTokenService.deleteGoogleAnalyticsAuthToken(organizationId)
            mapOf("ok" to true)
        }
}
