package polaris.controllers.api.organization

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.entity.VendorNotFound
import polaris.models.entity.organization.PlatformAppInstall
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.PlatformAppInstallRepository
import polaris.services.dto.OrganizationVendorDtoService
import polaris.util.ApiRoutes
import polaris.util.Responses
import java.util.*

@RestController
@Lazy
class PlatformAppInstallController(
    private val organizationDataRecipientRepo: OrganizationDataRecipientRepository,
    private val appInstallRepo: PlatformAppInstallRepository,
    private val dtoService: OrganizationVendorDtoService,
    private val lookup: LookupService
) : SessionAwareController() {

    companion object {
        private val log = LoggerFactory.getLogger(PlatformAppInstallController::class.java)
    }

    @GetMapping(ApiRoutes.Organization.RecipientPlatform.SPECIFIC_APPS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getApps(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.RecipientPlatform.Param.ID) platformId: UUID
    ) =
        authorizeOrg(organizationId) {
            val org = lookup.orgOrThrow(organizationId)
            val platformVndor = lookup.vendorOrThrow(platformId)
            val platformRecipient = organizationDataRecipientRepo.findByOrganizationAndVendor(org, platformVndor) ?: throw VendorNotFound(
                platformId.toString()
            )

            dtoService.toPlatformDto(platformRecipient)
        }

    @GetMapping(ApiRoutes.Organization.RecipientPlatform.SPECIFIC_PLATFORMS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getPlatforms(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.RecipientPlatform.Param.ID) appId: UUID
    ) =
        authorizeOrg(organizationId) {
            val org = lookup.orgOrThrow(organizationId)
            val appVndor = lookup.vendorOrThrow(appId)
            val appRecipient = organizationDataRecipientRepo.findByOrganizationAndVendor(org, appVndor) ?: throw VendorNotFound(
                appId.toString()
            )

            dtoService.toAppDto(appRecipient)
        }

    @PostMapping(ApiRoutes.Organization.RecipientPlatform.SPECIFIC_APP, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun addInstalledApp(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.RecipientPlatform.Param.ID) platformId: UUID,
        @PathVariable(ApiRoutes.Organization.RecipientPlatform.Param.APP_ID) appId: UUID
    ) =
        authorizeOrg(organizationId) {
            val org = lookup.orgOrThrow(organizationId)
            val platformVendor = lookup.vendorOrThrow(platformId)
            val appVendor = lookup.vendorOrThrow(appId)

            val platformRecipient = organizationDataRecipientRepo.findByOrganizationAndVendor(org, platformVendor) ?: throw VendorNotFound(
                platformId.toString()
            )
            val appRecipient = organizationDataRecipientRepo.findByOrganizationAndVendor(org, appVendor) ?: throw VendorNotFound(
                platformId.toString()
            )

            dtoService.toPlatformDto(platformRecipient)

            val app = PlatformAppInstall(platformId = platformRecipient.id, appId = appRecipient.id)

            appInstallRepo.save(app)

            Responses.EMPTY_SUCCESS
        }

    @DeleteMapping(ApiRoutes.Organization.RecipientPlatform.SPECIFIC_APP, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun removeInstalledApp(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.RecipientPlatform.Param.ID) platformId: UUID,
        @PathVariable(ApiRoutes.Organization.RecipientPlatform.Param.APP_ID) appId: UUID
    ) =
        authorizeOrg(organizationId) {
            val org = lookup.orgOrThrow(organizationId)
            val platformVendor = lookup.vendorOrThrow(platformId)
            val appVendor = lookup.vendorOrThrow(appId)

            val platformRecipient = organizationDataRecipientRepo.findByOrganizationAndVendor(org, platformVendor) ?: throw VendorNotFound(
                platformId.toString()
            )
            val appRecipient = organizationDataRecipientRepo.findByOrganizationAndVendor(org, appVendor) ?: throw VendorNotFound(
                platformId.toString()
            )

            dtoService.toPlatformDto(platformRecipient)

            val app = PlatformAppInstall(platformId = platformRecipient.id, appId = appRecipient.id)

            appInstallRepo.delete(app)

            Responses.EMPTY_SUCCESS
        }
}
