package polaris.controllers.api.organization

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.websiteaudit.StartWebsiteAuditRequest
import polaris.services.support.WebsiteAuditService
import polaris.util.ApiRoutes

@RestController
@Lazy
class WebsiteAuditController(
    private val websiteAuditService: WebsiteAuditService
) : SessionAwareController() {

    @PostMapping(ApiRoutes.Organization.WebsiteAudit.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun startWebsiteAudit(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody websiteAuditRequest: StartWebsiteAuditRequest
    ) = authorizeOrg(organizationId) {
        val url = if (websiteAuditRequest.websiteUrl.startsWith("https://")) websiteAuditRequest.websiteUrl else "https://${websiteAuditRequest.websiteUrl}"
        websiteAuditService.startWebsiteAuditRequest(organizationId, url)
    }

    @GetMapping(ApiRoutes.Organization.WebsiteAudit.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getWebsiteAuditResult(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) = authorizeOrg(organizationId) {
        websiteAuditService.getWebsiteAuditResult(organizationId)
    }

    @PostMapping(ApiRoutes.Organization.WebsiteAudit.RESET, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun resetWebsiteAudit(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) = authorizeOrg(organizationId) {
        websiteAuditService.resetWebsiteAudit(organizationId)
    }
}
