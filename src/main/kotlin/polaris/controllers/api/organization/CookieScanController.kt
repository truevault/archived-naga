package polaris.controllers.api.organization

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.cookiescan.StartCookieScanRequest
import polaris.models.dto.cookiescan.UpdateCookieRequest
import polaris.services.support.CookieScannerService
import polaris.util.ApiRoutes

@RestController
@Lazy
class CookieScanController(
    private val cookieScannerService: CookieScannerService
) : SessionAwareController() {

    @PostMapping(ApiRoutes.Organization.CookieScanner.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun startCookieScan(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody cookieScanRequest: StartCookieScanRequest
    ) = authorizeOrg(organizationId) {
        val url = if (cookieScanRequest.url.startsWith("http")) cookieScanRequest.url else "https://${cookieScanRequest.url}"
        cookieScannerService.startCookieScanRequest(organizationId, url)
    }

    @GetMapping(ApiRoutes.Organization.CookieScanner.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCookieScanResult(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) = authorizeOrg(organizationId) {
        cookieScannerService.getCookieScanResult(organizationId)
    }

    @DeleteMapping(ApiRoutes.Organization.CookieScanner.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun resetCookieScan(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        cookieScannerService.resetCookieScan(organizationId)
    }

    @GetMapping(ApiRoutes.Organization.CookieScanner.RECOMMENDATIONS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCookieRecommendations(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        cookieScannerService.getRecommendations(organizationId)
    }

    @PutMapping(
        ApiRoutes.Organization.CookieScanner.COOKIE, produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun updateCookie(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.CookieScanner.Param.DOMAIN) domain: String,
        @PathVariable(ApiRoutes.Organization.CookieScanner.Param.NAME) name: String,
        @RequestBody updateCookieRequest: UpdateCookieRequest
    ) = authorizeOrg(organizationId) {
        cookieScannerService.updateCookie(organizationId, domain, name, updateCookieRequest)
    }
}
