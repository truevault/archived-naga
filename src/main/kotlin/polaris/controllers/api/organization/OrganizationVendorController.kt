package polaris.controllers.api.organization

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.OrganizationVendorSettingsDto
import polaris.models.dto.vendor.AddOrUpdateCustomVendorDto
import polaris.models.dto.vendor.RemoveVendorsDto
import polaris.models.entity.PolarisCannotBeRemoved
import polaris.models.entity.VendorNotFound
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.vendor.DataRecipientType
import polaris.models.entity.vendor.KnownVendorKey
import polaris.services.dto.OrganizationVendorDtoService
import polaris.services.dto.VendorDtoService
import polaris.services.primary.OrganizationVendorService
import polaris.services.primary.PersonalInformationService
import polaris.services.primary.TaskUpdateService
import polaris.services.primary.VendorService
import polaris.services.support.DataMapService
import polaris.util.ApiRoutes
import java.util.UUID

@RestController
@Lazy
class OrganizationVendorController(
    private val organizationVendorService: OrganizationVendorService,
    private val organizationVendorDtoService: OrganizationVendorDtoService,
    private val vendorService: VendorService,
    private val vendorDtoService: VendorDtoService,
    private val personalInformationService: PersonalInformationService,
    private val taskUpdateService: TaskUpdateService,
    private val dataMapService: DataMapService,
    private val lookup: LookupService
) : SessionAwareController() {

    companion object {
        private val log = LoggerFactory.getLogger(OrganizationVendorController::class.java)
    }

    @GetMapping(ApiRoutes.Organization.Vendors.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getVendors(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.Vendors.Param.EXCLUDE_CATEGORIES, required = false, defaultValue = "") excludeCategories: List<String> = emptyList(),
        @RequestParam(ApiRoutes.Organization.Vendors.Param.INCLUDE_DATA_STORAGE_LOCATIONS, required = false) includeDataStorageLocations: Boolean = false,
        @RequestParam(ApiRoutes.Organization.Vendors.Param.EXCLUDE_INCOMPLETE, required = false, defaultValue = "false") excludeIncomplete: Boolean = false
    ) =
        authorizeOrg(organizationId) {
            val allVendors = organizationVendorService.allVendorsForOrganization(organizationId)

            val nonStorageLocations = if (includeDataStorageLocations) allVendors else allVendors.filter { it.vendor?.dataRecipientType != DataRecipientType.storage_location }
            var filteredVendors = if (excludeCategories.isNotEmpty()) nonStorageLocations.filter { !excludeCategories.contains(it.vendor?.recipientCategory) } else nonStorageLocations
            filteredVendors = if (excludeIncomplete) filteredVendors.filter { it.completed } else filteredVendors

            val res = filteredVendors.map {
                val mappedCategories = personalInformationService.getExchangedPIC(organizationId, it.vendor!!.id)
                organizationVendorDtoService.toDto(it, mappedCategories.size)
            }

            res
        }

    @GetMapping(ApiRoutes.Organization.Vendors.UNMAPPED, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getUnmappedVendors(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataMap.Param.COLLECTION_GROUP_ID) collectionGroupId: UUID
    ) =
        authorizeOrg(organizationId) {
            val org = lookup.orgOrThrow(organizationId)
            val cg = lookup.collectionGroupOrThrow(organizationId, collectionGroupId)
            val unmappedVendors = dataMapService.getUnmappedDataRecipients(org, cg)

            val res = unmappedVendors.map {
                val mappedCategories = personalInformationService.getExchangedPIC(organizationId, it.vendor!!.id)
                organizationVendorDtoService.toDto(it, mappedCategories.size)
            }

            res
        }

    @GetMapping(ApiRoutes.Organization.Vendors.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getVendorDetails(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.VENDOR_ID) vendorId: UUID
    ) = authorizeOrg(organizationId) {
        val orgVendor = organizationVendorService.getOrganizationVendor(organizationId, vendorId) ?: throw VendorNotFound(
            vendorId.toString()
        )
        organizationVendorDtoService.toDto(orgVendor)
    }

    @GetMapping(ApiRoutes.Organization.Vendors.CATALOG, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getVendorCatalog(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        vendorService.getVendorCatalog(organizationId).map { vendorDtoService.toDto(it) }
    }

    @GetMapping(ApiRoutes.Organization.Vendors.EDIT, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun editVendor(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.VENDOR_ID) vendorId: UUID
    ) = authorizeOrg(organizationId) {
        organizationVendorService.getEditData(organizationId, vendorId)
    }

    @GetMapping(ApiRoutes.Organization.Vendors.NEW, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getNewData(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        organizationVendorService.getNewData(organizationId)
    }

    @GetMapping(ApiRoutes.Organization.Vendors.ADD, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getNewVendorData(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.VENDOR_ID) vendorId: UUID
    ) = authorizeOrg(organizationId) {
        organizationVendorService.getAddData(organizationId, vendorId)
    }

    @GetMapping(ApiRoutes.Organization.Vendors.SEARCH, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun searchVendors(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam("name") name: String
    ) = authorizeOrg(organizationId) {
        vendorService.findVendorsByName(organizationId, name).map { vendorDtoService.toDto(it) }
    }

    @PostMapping(ApiRoutes.Organization.Vendors.DEFAULT, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun addDefaultDataRecipients(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        organizationVendorService.addDefaultVendors(organizationId)
        mapOf("ok" to true)
    }

    @PutMapping(ApiRoutes.Organization.Vendors.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun addOrUpdateOrganizationVendor(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.VENDOR_ID) vendorId: UUID,
        @RequestBody vendorDto: AddOrUpdateCustomVendorDto?
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val vendor = vendorService.getVendor(organizationId, vendorId)

        if (vendor.organization?.publicId == organizationId && vendorDto?.name != null) {
            vendorService.updateCustomVendor(organizationId, vendorId, vendorDto)
        }

        val settings = OrganizationVendorSettingsDto(
            contacted = vendorDto?.contacted,
            email = vendorDto?.email,
            classificationId = vendorDto?.classificationId,
            vendorContractId = vendorDto?.vendorContractId,
            vendorContractReviewedId = vendorDto?.vendorContractReviewedId,
            publicTosUrl = vendorDto?.publicTosUrl,
            ccpaIsSelling = vendorDto?.ccpaIsSelling,
            ccpaIsSharing = vendorDto?.ccpaIsSharing,
            usesCustomAudience = vendorDto?.usesCustomAudience,
            isUploadVendor = vendorDto?.isUploadVendor,
            automaticallyClassified = vendorDto?.automaticallyClassified,
            isDataStorage = vendorDto?.isDataStorage,
            dataAccessibility = vendorDto?.dataAccessibility,
            dataDeletability = vendorDto?.dataDeletability,
            tosFileName = vendorDto?.tosFileName,
            tosFileKey = vendorDto?.tosFileKey,
            processingRegions = vendorDto?.processingRegions,
            collectionContext = vendorDto?.collectionContext,
            gdprProcessorSetting = vendorDto?.gdprProcessorSetting,
            gdprProcessorGuaranteeUrl = vendorDto?.gdprProcessorGuaranteeUrl,
            gdprProcessorGuaranteeFileName = vendorDto?.gdprProcessorGuaranteeFileName,
            gdprProcessorGuaranteeFileKey = vendorDto?.gdprProcessorGuaranteeFileKey,
            gdprControllerGuaranteeUrl = vendorDto?.gdprControllerGuaranteeUrl,
            gdprControllerGuaranteeFileName = vendorDto?.gdprControllerGuaranteeFileName,
            gdprControllerGuaranteeFileKey = vendorDto?.gdprControllerGuaranteeFileKey,
            gdprContactUnsafeTransfer = vendorDto?.gdprContactUnsafeTransfer,
            gdprEnsureSccInPlace = vendorDto?.gdprEnsureSccInPlace,
            gdprContactProcessorStatus = vendorDto?.gdprContactProcessorStatus,
            gdprHasSccSetting = vendorDto?.gdprHasSccSetting,
            gdprSccUrl = vendorDto?.gdprSccUrl,
            gdprSccFileName = vendorDto?.gdprSccFileName,
            gdprSccFileKey = vendorDto?.gdprSccFileKey,
            removedFromExceptionsToScc = vendorDto?.removedFromExceptionsToScc,
            completed = vendorDto?.completed ?: false
        )

        val dto = organizationVendorDtoService.toDto(
            organizationVendorService.addOrUpdateOrganizationVendor(organizationId, vendorId, settings)
        )

        taskUpdateService.updateTasks(org, close = true, create = true, actor = requireCurrentUser())

        dto
    }

    @PostMapping(ApiRoutes.Organization.Vendors.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun addCustomVendor(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody vendorDto: AddOrUpdateCustomVendorDto
    ) = authorizeOrg(organizationId) {
        val vendor = vendorService.addCustomVendor(organizationId, vendorDto)
        val settings = OrganizationVendorSettingsDto(
            contacted = vendorDto.contacted,
            email = vendorDto.email,
            classificationId = vendorDto.classificationId,
            vendorContractId = vendorDto.vendorContractId,
            vendorContractReviewedId = vendorDto.vendorContractReviewedId,
            ccpaIsSelling = vendorDto.ccpaIsSelling,
            ccpaIsSharing = vendorDto.ccpaIsSharing,
            usesCustomAudience = vendorDto.usesCustomAudience,
            isUploadVendor = vendorDto.isUploadVendor,
            automaticallyClassified = vendorDto.automaticallyClassified,
            isDataStorage = vendorDto.isDataStorage,
            publicTosUrl = vendorDto.publicTosUrl,
            tosFileName = null,
            tosFileKey = null,
            processingRegions = null,
            collectionContext = vendorDto.collectionContext,
            gdprProcessorSetting = vendorDto.gdprProcessorSetting,
            gdprProcessorGuaranteeUrl = vendorDto.gdprProcessorGuaranteeUrl,
            gdprProcessorGuaranteeFileName = vendorDto.gdprProcessorGuaranteeFileName,
            gdprProcessorGuaranteeFileKey = vendorDto.gdprProcessorGuaranteeFileKey,
            gdprControllerGuaranteeUrl = vendorDto.gdprControllerGuaranteeUrl,
            gdprControllerGuaranteeFileName = vendorDto.gdprControllerGuaranteeFileName,
            gdprControllerGuaranteeFileKey = vendorDto.gdprControllerGuaranteeFileKey,
            gdprContactUnsafeTransfer = null,
            gdprEnsureSccInPlace = null,
            gdprContactProcessorStatus = null,
            gdprHasSccSetting = vendorDto.gdprHasSccSetting,
            gdprSccUrl = vendorDto.gdprSccUrl,
            gdprSccFileName = null,
            gdprSccFileKey = null,
            dataAccessibility = vendorDto.dataAccessibility,
            dataDeletability = vendorDto.dataDeletability,
            removedFromExceptionsToScc = null,
            completed = vendorDto.completed ?: false
        )

        organizationVendorDtoService.toDto(
            organizationVendorService.addOrUpdateOrganizationVendor(organizationId, vendor.id, settings)
        )
    }

    @GetMapping(ApiRoutes.Organization.Vendors.THIRD_PARTY_RECIPIENTS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getThirdPartyDataRecipients(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        val result = vendorService.getThirdPartyDataRecipients(organizationId)
        result.map { v -> vendorDtoService.toDto(v) }
    }

    @DeleteMapping(ApiRoutes.Organization.Vendors.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun removeVendor(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.VENDOR_ID) vendorId: UUID,
        @RequestParam("collectionContext", required = false, defaultValue = "") collectionContext: List<CollectionContext> = emptyList()
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val vendor = lookup.vendor(vendorId)

        if (vendor?.vendorKey == KnownVendorKey.TRUEVAULT_POLARIS.key) {
            throw PolarisCannotBeRemoved()
        }

        if (collectionContext.isNullOrEmpty()) {
            organizationVendorService.removeVendor(organizationId, vendorId)
        } else {
            organizationVendorService.removeCollectionContext(organizationId, vendorId, collectionContext, true)
        }

        taskUpdateService.updateTasks(org, close = true, create = false, actor = requireCurrentUser())
        mapOf("ok" to true)
    }

    @PostMapping(ApiRoutes.Organization.Vendors.REMOVE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun removeVendors(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody vendorsDto: RemoveVendorsDto
    ) = authorizeOrg(organizationId) {
        vendorsDto.vendors.map { v -> organizationVendorService.removeVendor(organizationId, UUID.fromString(v)) }
        mapOf("ok" to true)
    }

    @PutMapping(ApiRoutes.Organization.Vendors.MAPPING_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateMappingProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.VENDOR_ID) vendorId: UUID,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.MAPPING_PROGRESS) mappingProgress: MappingProgressEnum
    ) = authorizeOrg(organizationId) {
        organizationVendorService.updateMappingProgress(organizationId, vendorId, mappingProgress)
        mapOf("ok" to true)
    }

    @DeleteMapping(ApiRoutes.Organization.Vendors.AGREEMENT, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun removeVendorAgreementFile(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Vendors.Param.VENDOR_ID) vendorId: UUID,
        @RequestBody vendorDto: AddOrUpdateCustomVendorDto?
    ) = authorizeOrg(organizationId) {

        organizationVendorDtoService.toDto(
            organizationVendorService.removeVendorAgreementFile(organizationId, vendorId)
        )
    }
}
