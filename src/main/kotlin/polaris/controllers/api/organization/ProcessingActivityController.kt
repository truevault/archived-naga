package polaris.controllers.api.organization

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.CreateProcessingActivityRequest
import polaris.models.dto.organization.DefProcessingActivityDto
import polaris.models.dto.organization.PatchProcessingActivityRequest
import polaris.models.dto.organization.ProcessingActivityDto
import polaris.models.entity.organization.DefaultProcessingActivity
import polaris.models.entity.vendor.ProcessingRegion
import polaris.services.dto.ProcessingActivityDtoService
import polaris.services.primary.ProcessingActivityService
import polaris.services.support.AutocreateProcessingActivityService
import polaris.util.ApiRoutes
import java.util.UUID

@RestController
@Lazy
class ProcessingActivityController(
    private val service: ProcessingActivityService,
    private val autocreateService: AutocreateProcessingActivityService,
    private val dtoService: ProcessingActivityDtoService,
    private val lookupService: LookupService
) : SessionAwareController() {

    companion object {
        private val log = LoggerFactory.getLogger(ProcessingActivityController::class.java)
    }

    @GetMapping(ApiRoutes.ProcessingActivity.ALL, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getDefaultProcessingActivities(): List<DefProcessingActivityDto> {
        val comparator = compareBy<DefaultProcessingActivity, Int?>(nullsLast()) { it.displayOrder }.thenBy { it.name }
        return service.allDefaultProcessingActivities().sortedWith(comparator).map { dtoService.defaultToDto(it) }
    }

    @GetMapping(ApiRoutes.Organization.ProcessingActivity.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getProcessingActivities(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): List<ProcessingActivityDto> = authorizeOrg(organizationId) {
        val org = lookupService.orgOrThrow(organizationId)
        val all = service.processingActivitiesForOrg(org)

        all.map { dtoService.toDto(it) }.sortedBy { it.name }
    }

    @PostMapping(ApiRoutes.Organization.ProcessingActivity.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createProcessingActivity(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody activity: CreateProcessingActivityRequest
    ): ProcessingActivityDto = authorizeOrg(organizationId) {
        val org = lookupService.orgOrThrow(organizationId)
        val pa = service.createProcessingActivity(org, activity)
        dtoService.toDto(pa)
    }

    @PostMapping(ApiRoutes.Organization.ProcessingActivity.AUTOMATIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun manageAutomaticProcessingActivities(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        autocreateService.manageAutomaticProcessingActivities(organizationId)
    }

    @PatchMapping(ApiRoutes.Organization.ProcessingActivity.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateProcessingActivity(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.ProcessingActivity.Param.ID) activityId: UUID,
        @RequestBody activity: PatchProcessingActivityRequest
    ): ProcessingActivityDto = authorizeOrg(organizationId) {
        val org = lookupService.orgOrThrow(organizationId)
        val pa = service.updateProcessingActivity(org, activityId, activity)
        dtoService.toDto(pa)
    }

    @DeleteMapping(ApiRoutes.Organization.ProcessingActivity.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun removeProcessingActivityRegion(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.ProcessingActivity.Param.ID) activityId: UUID,
        @RequestParam("regions", required = true) regions: String
    ) = authorizeOrg(organizationId) {
        val org = lookupService.orgOrThrow(organizationId)
        val enumRegions = regions.split(",").map { ProcessingRegion.valueOf(it) }
        service.deleteProcessingActivityRegion(org, activityId, enumRegions)
    }

    @PostMapping(ApiRoutes.Organization.ProcessingActivity.SPECIFIC_ASSOCIATION_PIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun associateProcessingActivityAndPic(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.ProcessingActivity.Param.ID) activityId: UUID,
        @PathVariable(ApiRoutes.Organization.ProcessingActivity.Param.PIC_ID) picId: UUID
    ): ProcessingActivityDto = authorizeOrg(organizationId) {
        val org = lookupService.orgOrThrow(organizationId)
        val pa = service.associateProcessingActivityAndPic(org, activityId, picId)
        dtoService.toDto(pa)
    }

    @DeleteMapping(ApiRoutes.Organization.ProcessingActivity.SPECIFIC_ASSOCIATION_PIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun unassociateProcessingActivityAndPic(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.ProcessingActivity.Param.ID) activityId: UUID,
        @PathVariable(ApiRoutes.Organization.ProcessingActivity.Param.PIC_ID) picId: UUID
    ): ProcessingActivityDto = authorizeOrg(organizationId) {
        val org = lookupService.orgOrThrow(organizationId)
        val pa = service.unassociateProcessingActivityAndPic(org, activityId, picId)
        dtoService.toDto(pa)
    }
}
