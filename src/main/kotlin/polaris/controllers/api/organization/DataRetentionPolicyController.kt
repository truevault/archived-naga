package polaris.controllers.api.organization

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.UpdateDataRetentionPolicyDetailDto
import polaris.models.entity.organization.DataRetentionPolicyDetailCategory
import polaris.services.dto.DataRetentionDtoService
import polaris.services.dto.OrganizationDtoService
import polaris.services.primary.OrganizationService
import polaris.services.support.DataRetentionService
import polaris.util.ApiRoutes
import polaris.util.Responses

@RestController
@Lazy
class DataRetentionPolicyController(
    private val organizationService: OrganizationService,
    private val organizationDtoService: OrganizationDtoService,
    private val dataRetentionService: DataRetentionService,
    private val dataRetentionDtoService: DataRetentionDtoService,
    private val lookup: LookupService
) : SessionAwareController() {

    companion object {
        private val log = LoggerFactory.getLogger(DataRetentionPolicyController::class.java)
    }

    @GetMapping(ApiRoutes.Organization.DataRetention.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getDataRetention(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) =
        authorizeOrg(organizationId) {
            val o = lookup.orgOrThrow(organizationId)
            val details = dataRetentionService.getDataRetentionDetails(o)
            dataRetentionDtoService.toDto(o, details)
        }

    @PutMapping(ApiRoutes.Organization.DataRetention.SPECIFIC_CATEGORY, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateDataRetentionCategory(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.DataRetention.Param.CATEGORY) category: DataRetentionPolicyDetailCategory,
        @RequestBody dto: UpdateDataRetentionPolicyDetailDto
    ) =
        authorizeOrg(organizationId) {
            val o = organizationService.findByPublicId(organizationId = organizationId)
            dataRetentionService.updateDataRetentionCategory(o, category, dto)
            Responses.EMPTY_SUCCESS
        }
}
