package polaris.controllers.api.organization

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.CreateCollectionGroupDto
import polaris.models.dto.organization.UpdateCollectionGroupDto
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.compliance.PersonalInformationType
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.organization.PrivacyNoticeProgress
import polaris.services.primary.CollectionGroupService
import polaris.services.support.AutocreateCollectionGroupService
import polaris.util.ApiRoutes
import java.util.*

data class VendorMappingProgressDto(
    val mappingProgress: MappingProgressEnum,
    val vendorIds: List<UUID>
)

@RestController
@Lazy
class CollectionGroupController(
    private val collectionGroupService: CollectionGroupService,
    private val autocreateCollectionGroupService: AutocreateCollectionGroupService
) : SessionAwareController() {

    @PostMapping(ApiRoutes.Organization.CollectionGroup.DEFAULT, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createDefaultCollectionGroups(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) =
        authorizeOrg(organizationId) {
            autocreateCollectionGroupService.createDefaultCollectionGroups(organizationId)
        }

    @GetMapping(ApiRoutes.Organization.CollectionGroup.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCollectionGroups(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.CollectionGroup.Param.COLLECTION_GROUP_TYPES, required = false, defaultValue = "") collectionGroupTypes: List<CollectionGroupType> = emptyList(),
        @RequestParam(ApiRoutes.Organization.CollectionGroup.Param.EXCLUDE_COLLECTION_GROUP_TYPES, required = false, defaultValue = "") excludeCollectionGroupTypes: List<CollectionGroupType> = emptyList()
    ) =
        authorizeOrg(organizationId) {
            val allCollectionGroups = if (collectionGroupTypes.isNotEmpty()) {
                collectionGroupService.findAllByOrganizationAndType(organizationId, collectionGroupTypes)
            } else {
                collectionGroupService.findAllByOrganization(organizationId)
            }

            val filteredCollectionGroups = if (excludeCollectionGroupTypes.isEmpty()) allCollectionGroups else allCollectionGroups.filter { !excludeCollectionGroupTypes.contains(it.collectionGroupType) }

            filteredCollectionGroups.map { collectionGroupService.toDetailsDto(organizationId, it) }
        }

    @PostMapping(ApiRoutes.Organization.CollectionGroup.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun addOrganizationDataSubjectType(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody createDto: CreateCollectionGroupDto
    ) = authorizeOrg(organizationId) {
        val dataSubjectType = collectionGroupService.create(createDto.name, organizationId, createDto.collectionGroupType, createDto.gdprDataSubject)
        collectionGroupService.update(
            organizationId, dataSubjectType.id,
            UpdateCollectionGroupDto(
                name = dataSubjectType.name,
                description = createDto.description,
                enabled = createDto.enabled,
                privacyCenterEnabled = createDto.privacyCenterEnabled,
                collectionGroupType = createDto.collectionGroupType,
                gdprEeaUkDataCollection = createDto.gdprEeaUkDataCollection,
                processingRegions = createDto.processingRegions,
                privacyNoticeIntroText = null // Revisit
            )
        )
        collectionGroupService.toDetailsDto(organizationId, dataSubjectType)
    }

    @PutMapping(ApiRoutes.Organization.CollectionGroup.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationDataSubjectType(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.CollectionGroup.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID,
        @RequestBody updateDto: UpdateCollectionGroupDto
    ) = authorizeOrg(organizationId) {
        collectionGroupService.toDetailsDto(organizationId, collectionGroupService.update(organizationId, dataSubjectTypeId, updateDto))
    }

    @PutMapping(ApiRoutes.Organization.CollectionGroup.MAPPING_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateConsumerGroupMappingProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.CollectionGroup.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID,
        @PathVariable(ApiRoutes.Organization.CollectionGroup.Param.INFORMATION_TYPE) informationType: PersonalInformationType,
        @PathVariable(ApiRoutes.Organization.CollectionGroup.Param.MAPPING_PROGRESS) mappingProgress: MappingProgressEnum
    ) = authorizeOrg(organizationId) {
        collectionGroupService.toDetailsDto(
            organizationId,
            collectionGroupService.updateMappingProgress(organizationId, dataSubjectTypeId, informationType, mappingProgress)
        )
    }

    @PutMapping(
        ApiRoutes.Organization.CollectionGroup.PRIVACY_NOTICE_PROGRESS,
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun updatePrivacyNoticeProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.CollectionGroup.Param.DATA_SUBJECT_TYPE_ID) consumerGroupId: UUID,
        @PathVariable(ApiRoutes.Organization.CollectionGroup.Param.PRIVACY_NOTICE_PROGRESS) privacyNoticeProgress: PrivacyNoticeProgress
    ) = authorizeOrg(organizationId) {
        collectionGroupService.toDetailsDto(
            organizationId,
            collectionGroupService.updatePrivacyNoticeProgress(
                organizationId,
                consumerGroupId,
                privacyNoticeProgress
            )
        )
    }

    @DeleteMapping(ApiRoutes.Organization.CollectionGroup.SPECIFIC)
    fun deleteConsumerGroup(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.CollectionGroup.Param.DATA_SUBJECT_TYPE_ID) dataSubjectTypeId: UUID
    ) = authorizeOrg(organizationId) {
        collectionGroupService.delete(organizationId, dataSubjectTypeId)
    }
}
