package polaris.controllers.api.organization

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.CustomMessageTemplateTextDto
import polaris.models.entity.organization.CustomMessageTypeEnum
import polaris.services.primary.MessageTemplateService
import polaris.util.ApiRoutes
import java.util.*

@RestController
@Lazy
class MessageTemplatesController(
    private val messageTemplateService: MessageTemplateService
) : SessionAwareController() {
    @GetMapping(ApiRoutes.Organization.Templates.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun listTemplates(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        CustomMessageTypeEnum.values().toList().map { it.toDto() }
    }

    @GetMapping(ApiRoutes.Organization.Templates.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getTemplate(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Templates.Param.MESSAGE_TYPE) messageType: String
    ) = authorizeOrg(organizationId) {
        val cmt = CustomMessageTypeEnum.bySlug(messageType)
        val actor = requireCurrentUser()
        messageTemplateService.previewTemplate(organizationId, actor, cmt)
    }

    @PutMapping(ApiRoutes.Organization.Templates.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateTemplate(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Templates.Param.MESSAGE_TYPE) messageType: String,
        @RequestBody dto: CustomMessageTemplateTextDto
    ) = authorizeOrg(organizationId) {
        val cmt = CustomMessageTypeEnum.bySlug(messageType)
        val actor = requireCurrentUser()
        messageTemplateService.customizeTemplate(organizationId, actor, cmt, dto.customText)
        mapOf("ok" to true)
    }
}
