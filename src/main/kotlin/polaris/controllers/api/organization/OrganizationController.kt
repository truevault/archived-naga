package polaris.controllers.api.organization

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.ApiMode
import polaris.models.dto.organization.UpdateOrganization
import polaris.models.dto.organization.UpdateOrganizationComplinaceChecklistItemDto
import polaris.models.dto.organization.UpdateOrganizationCustomFieldDto
import polaris.models.dto.organization.UpdateOrganizationFinancialIncentivesRequest
import polaris.models.dto.organization.UpdateOrganizationGoogleAnalyticsWebPropertyDto
import polaris.models.dto.organization.UpdateOrganizationMessageSettings
import polaris.models.dto.organization.UpdateOrganizationPrivacyNoticeProgress
import polaris.models.dto.organization.UpdateOrganizationSurveyQuestionDto
import polaris.models.dto.request.UpdateOrganizationVerificationInstruction
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.request.Regulation
import polaris.services.dto.OrganizationDtoService
import polaris.services.primary.OrganizationService
import polaris.services.primary.RequestTypeVisbility
import polaris.services.primary.TaskUpdateService
import polaris.util.ApiRoutes

@RestController
@Lazy
class OrganizationController(
    private val organizationService: OrganizationService,
    private val organizationDtoService: OrganizationDtoService,
    private val taskUpdateService: TaskUpdateService
) : SessionAwareController() {

    companion object {
        private val log = LoggerFactory.getLogger(OrganizationController::class.java)
    }

    @GetMapping(ApiRoutes.Organization.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrganization(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) =
        authorizeOrg(organizationId) {
            val o = organizationService.findByPublicId(organizationId = organizationId)

            organizationDtoService.toDto(o)
        }

    @GetMapping(ApiRoutes.Organization.DATA_REQUEST_TYPES, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getDataRequestTypes(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.Param.REGULATION) regulations: String?,
        @RequestParam(ApiRoutes.Organization.Param.FILTER_VISIBLE) filterVisible: Boolean?
    ) = authorizeOrg(organizationId) {
        val filter: RequestTypeVisbility? = if (filterVisible == true) RequestTypeVisbility.POLARIS else null
        val regs = regulations?.let { it.split(",").mapNotNull { Regulation.values().firstOrNull { reg -> reg.name.equals(it, true) } } }
        organizationService.getDataRequestTypes(organizationId, filter, regs).map { it.toDto() }
    }

    @PutMapping(ApiRoutes.Organization.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganization(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody updateOrganization: UpdateOrganization
    ) = authorizeOrg(organizationId) {
        organizationDtoService.toDto(
            organizationService.updateOrganization(organizationId = organizationId, dto = updateOrganization)
        )
    }

    @PutMapping(ApiRoutes.Organization.CUSTOM_FIELD, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationCustomField(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody updateCustomFieldDto: UpdateOrganizationCustomFieldDto
    ) = authorizeOrg(organizationId) {
        organizationDtoService.toDto(
            organizationService.updateCustomField(organizationId = organizationId, dto = updateCustomFieldDto)
        )
    }

    @PutMapping(ApiRoutes.Organization.FINANCIAL_INCENTIVES, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationFinancialIncentives(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody request: UpdateOrganizationFinancialIncentivesRequest
    ) = authorizeOrg(organizationId) {
        organizationDtoService.toDto(
            organizationService.updateFinancialIncentiveDetails(organizationId = organizationId, dto = request, user = requireCurrentUser())
        )
    }

    @PutMapping(ApiRoutes.Organization.GA_WEB_PROPERTY, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationWebProperty(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody updateDto: UpdateOrganizationGoogleAnalyticsWebPropertyDto
    ) = authorizeOrg(organizationId) {
        organizationDtoService.toDto(
            organizationService.updateGoogleAnalyticsWebProperty(organizationId = organizationId, dto = updateDto)
        )
    }

    @PutMapping(ApiRoutes.Organization.MESSAGE_SETTINGS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationMailbox(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody updateMessageSettings: UpdateOrganizationMessageSettings
    ) = authorizeOrg(organizationId) {
        organizationDtoService.toDto(
            organizationService.updateMessageSettings(
                requireCurrentUser(),
                organizationId = organizationId,
                dto = updateMessageSettings
            )
        )
    }

    @GetMapping(ApiRoutes.Organization.MAILBOXES, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrganizationMailboxes(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) =
        authorizeOrg(organizationId) {
            organizationService.mailboxesForOrganization(organizationId).map { it.toDto() }
        }

    @PostMapping(ApiRoutes.Organization.GET_COMPLIANT_DONE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun setGetCompliantDone(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.Param.FORCE) force: Boolean
    ) =
        authorizeOrg(organizationId) {
            organizationService.automatedReturnToStayCompliant(organizationId, force)
        }

    @GetMapping(ApiRoutes.Organization.FEATURES, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrganizationFeatures(@PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId) =
        authorizeOrg(organizationId) {
            organizationService.featuresForOrganization(organizationId)
                .map { organizationDtoService.organizationFeatureDto(it) }
        }

    @GetMapping(ApiRoutes.Organization.SURVEY, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrganizationSurveyByName(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.SURVEY_NAME) surveyName: String
    ) = authorizeOrg(organizationId) {
        organizationService.getSurveyQuestions(organizationId, surveyName, ApiMode.Live).map { it.toDto() }
    }

    @GetMapping(ApiRoutes.Organization.SURVEY, headers = ["X-Api-Mode=Draft"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun GCgetOrganizationSurveyByName(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.SURVEY_NAME) surveyName: String
    ) = authorizeOrg(organizationId) {
        organizationService.getSurveyQuestions(organizationId, surveyName, ApiMode.Draft).map { it.toDto() }
    }

    @PostMapping(ApiRoutes.Organization.SURVEY, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun putOrganizationSurveyQuestionsByName(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.SURVEY_NAME) surveyName: String,
        @RequestBody updateQuestionsDto: List<UpdateOrganizationSurveyQuestionDto>
    ) = authorizeOrg(organizationId) { events ->
        try {
            organizationService.updateSurveyQuestions(organizationId, surveyName, updateQuestionsDto, events)
        } catch (e: DataIntegrityViolationException) {
            organizationService.updateSurveyQuestions(organizationId, surveyName, updateQuestionsDto, events)
        }
        mapOf("ok" to true)
    }

    @PostMapping(ApiRoutes.Organization.SURVEY, headers = ["X-Api-Mode=Draft"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun GCputOrganizationSurveyQuestionsByName(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.SURVEY_NAME) surveyName: String,
        @RequestBody updateQuestionsDto: List<UpdateOrganizationSurveyQuestionDto>
    ) = authorizeOrg(organizationId) { events ->
        {
            try {
                organizationService.updateSurveyQuestions(organizationId, surveyName, updateQuestionsDto, events, ApiMode.Draft)
            } catch (e: DataIntegrityViolationException) {
                organizationService.updateSurveyQuestions(organizationId, surveyName, updateQuestionsDto, events, ApiMode.Draft)
            }
            taskUpdateService.updateTasks(organizationId, create = false, actor = requireCurrentUser())
            mapOf("ok" to true)
        }
    }

    @PostMapping(ApiRoutes.Organization.SURVEY_SEND, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun sendOrganizationSurveyEmail(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.SURVEY_NAME) surveyName: String
    ) = authorizeOrg(organizationId) {
        organizationService.sendSurveyEmail(organizationId, surveyName)
        mapOf("ok" to true)
    }

    @GetMapping(ApiRoutes.Organization.COMPLIANCE_CHECKLIST, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrganizationComplianceChecklistItems(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        organizationService.getComplianceChecklistItems(organizationId).map { it.toDto() }
    }

    @PostMapping(ApiRoutes.Organization.COMPLIANCE_CHECKLIST, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun postOrganizationComplianceChecklistItems(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody updateChecklistItems: List<UpdateOrganizationComplinaceChecklistItemDto>
    ) = authorizeOrg(organizationId) {
        organizationService.updateComplianceChecklistItems(organizationId, updateChecklistItems)
        mapOf("ok" to true)
    }

    @PutMapping(ApiRoutes.Organization.VERIFICATION_INSTRUCTION, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationVerificationInstruction(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody updateVerificationInstruction: UpdateOrganizationVerificationInstruction
    ) = authorizeOrg(organizationId) {
        organizationDtoService.toDto(
            organizationService.updateVerificationInstruction(
                organizationId = organizationId,
                dto = updateVerificationInstruction
            )
        )
    }

    @PutMapping(ApiRoutes.Organization.GET_COMPLIANT_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationGetCompliantProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.GET_COMPLIANT_PROGRESS) getCompliantProgress: String
    ) = authorizeOrg(organizationId) { events ->
        organizationDtoService.toDto(
            organizationService.updateGetCompliantProgress(
                organizationId = organizationId,
                getCompliantProgress = getCompliantProgress,
                events = events
            )
        )
    }

    @PatchMapping(ApiRoutes.Organization.GET_COMPLIANT_INVALIDATED_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationGetCompliantInvalidatedProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.GET_COMPLIANT_INVALIDATED_PROGRESS) getCompliantInvalidatedProgress: String,
        @PathVariable(ApiRoutes.Organization.Param.ADD_OR_REMOVE) addOrRemove: String
    ) = authorizeOrg(organizationId) {

        if (addOrRemove.toLowerCase() == "add") {
            organizationDtoService.toDto(
                organizationService.addGetCompliantInvalidatedProgress(
                    organizationId = organizationId,
                    getCompliantInvalidatedProgress = getCompliantInvalidatedProgress
                )
            )
        } else if (addOrRemove.toLowerCase() == "remove") {
            organizationDtoService.toDto(
                organizationService.removeGetCompliantInvalidatedProgress(
                    organizationId = organizationId,
                    getCompliantInvalidatedProgress = getCompliantInvalidatedProgress
                )
            )
        } else {
            // unsupported 400
        }
    }

    @PutMapping(ApiRoutes.Organization.PRIVACY_NOTICE_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationPrivacyNoticeProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody updatePrivacyNoticeProgress: UpdateOrganizationPrivacyNoticeProgress
    ) = authorizeOrg(organizationId) {
        organizationDtoService.toDto(
            organizationService.updatePrivacyNoticeProgress(
                organizationId = organizationId,
                updatePrivacyNoticeProgress = updatePrivacyNoticeProgress
            )
        )
    }

    @PutMapping(ApiRoutes.Organization.EXCEPTIONS_MAPPING_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationExceptionsMappingProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.EXCEPTIONS_MAPPING_PROGRESS) mappingProgress: MappingProgressEnum
    ) = authorizeOrg(organizationId) {
        organizationDtoService.toDto(
            organizationService.updateExceptionsMappingProgress(
                organizationId = organizationId,
                mappingProgress = mappingProgress
            )
        )
    }

    @PutMapping(ApiRoutes.Organization.COOKIES_MAPPING_PROGRESS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateOrganizationCookiesMappingProgress(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.COOKIES_MAPPING_PROGRESS) mappingProgress: MappingProgressEnum
    ) = authorizeOrg(organizationId) {
        organizationDtoService.toDto(
            organizationService.updateCookiesMappingProgress(
                organizationId = organizationId,
                mappingProgress = mappingProgress
            )
        )
    }

    @DeleteMapping(ApiRoutes.Organization.SPECIFIC_MAILBOX, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun deleteOrganizationMailbox(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Param.MAILBOX) mailbox: String
    ) = authorizeOrg(organizationId) {
        organizationService.deleteMailbox(organizationId, mailbox)
    }

    @PostMapping(ApiRoutes.Organization.MESSAGE_SETTINGS_TEST, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun testOrganizationMessageSettings(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        organizationService.messageSettingsTestEmail(requireCurrentUser(), organizationId)
    }
}
