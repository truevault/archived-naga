package polaris.controllers.api.organization

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.PaginatedMetaDto
import polaris.models.dto.cookiescan.PaginatedGdprCookieConsentDto
import polaris.models.dto.toIso
import polaris.services.dto.GdprCookieConsentDtoService
import polaris.services.support.CookieConsentSort
import polaris.services.support.CookieConsentSortColumn
import polaris.services.support.GdprCookieConsentService
import polaris.util.ApiRoutes
import java.io.IOException
import java.net.URLEncoder
import java.time.ZonedDateTime
import javax.servlet.http.HttpServletResponse

@RestController
@Lazy
class GdprCookieConsentController(
    private val cookieConsentService: GdprCookieConsentService,
    private val dto: GdprCookieConsentDtoService
) : SessionAwareController() {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(GdprCookieConsentController::class.java)
    }

    @GetMapping(ApiRoutes.Organization.CookieConsent.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCookieConsent(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Param.PAGE) page: Int?,
        @RequestParam(ApiRoutes.Param.PER) per: Int?,
        @RequestParam(ApiRoutes.Param.SORT) sort: CookieConsentSortColumn?,
        @RequestParam(ApiRoutes.Param.DESC) desc: Boolean? = true
    ): PaginatedGdprCookieConsentDto = authorizeOrg(organizationId) {
        // paginated
        val res = cookieConsentService.paginatedFindForOrganization(
            organizationId = organizationId,
            sort = CookieConsentSort(col = sort ?: CookieConsentSortColumn.date, desc = desc),
            page = page ?: 0,
            per = per ?: 15
        )

        val data = res.content.map { dto.toDto(it) }
        val meta = PaginatedMetaDto(page = res.number, per = res.pageable.pageSize, count = res.content.size, totalPages = res.totalPages, totalCount = res.totalElements.toInt())

        PaginatedGdprCookieConsentDto(data = data, meta = meta)
    }

    @GetMapping(ApiRoutes.Organization.CookieConsent.CSV_EXPORT, produces = [MediaType.TEXT_PLAIN_VALUE])
    fun getCookieConsentCsv(
        servletResponse: HttpServletResponse,
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Param.PAGE) page: Int?,
        @RequestParam(ApiRoutes.Param.PER) per: Int?,
        @RequestParam(ApiRoutes.Param.SORT) sort: CookieConsentSortColumn?,
        @RequestParam(ApiRoutes.Param.DESC) desc: Boolean? = true
    ) = authorizeOrg(organizationId) {
        val res = cookieConsentService.paginatedFindForOrganization(
            organizationId = organizationId,
            sort = CookieConsentSort(col = sort ?: CookieConsentSortColumn.date, desc = desc),
            page = page ?: 0,
            per = per ?: 15
        )
        val data = res.content.map { dto.toDto(it) }

        val now = ZonedDateTime.now().toIso()
        val filename = URLEncoder.encode("$now-cookie-consent-export", "utf-8")

        servletResponse.contentType = MediaType.TEXT_PLAIN_VALUE
        servletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=$filename.csv")

        val printer = CSVPrinter(servletResponse.getWriter(), CSVFormat.DEFAULT)
        try {
            printer.printRecord("Visitor ID", "Consent Granted", "Date", "Request Type", "Request Date", "Due Date", "Closed At")
            data.forEach { req ->
                printer.printRecord(req.visitorId, req.consentGranted.joinToString("|"), req.date)
            }
        } catch (e: IOException) {
            log.error("Error while writing Cookie Consent CSV", e)
        }
    }
}
