package polaris.controllers.api

import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.session.FindByIndexNameSessionRepository
import org.springframework.session.Session
import org.springframework.web.bind.annotation.*
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.dto.user.ResetPasswordRequest
import polaris.models.dto.user.SessionDto
import polaris.models.dto.user.UpdateActiveOrganization
import polaris.models.dto.user.UserDetailsDto
import polaris.models.entity.user.User
import polaris.services.dto.UserDtoService
import polaris.services.primary.UserService
import polaris.services.support.SSOService
import polaris.util.ApiRoutes
import polaris.util.Responses
import java.util.*
import javax.servlet.http.HttpServletRequest

@RestController
@Lazy
@CrossOrigin(origins = [])
class UserController(
    private val userService: UserService,
    private val userDtoService: UserDtoService,
    private val ssoService: SSOService,
//   Suppressing the error for this, it's autoconfigurated and provided by spring-session-jdbc to spring-boot, but Intellij can't find it.
    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    private val sessions: FindByIndexNameSessionRepository<out Session>
) : SessionAwareController() {
    // Return null rather than throw a 401 error if the user isn't logged in and let the UI handle what that means.
    @GetMapping(ApiRoutes.User.SELF, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getSelf(): UserDetailsDto? {
        val user = getCurrentUser()
        return user?.let { userDtoService.toDetailsDto(it) }
    }

    @GetMapping(ApiRoutes.User.SESSION, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getSelfSession() = SessionDto(
        secondsRemaining = sessionSecondsRemaining()
    )

    @PatchMapping(ApiRoutes.User.SELF_ACTIVE_ORGANIZATION, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateUserActiveOrganization(
        @RequestBody updateActiveOrganization: UpdateActiveOrganization
    ): UserDetailsDto? {
        var user = getCurrentUser()
        user = user?.let { userService.updateUserActiveOrganization(it, updateActiveOrganization.organizationId) }
        return user?.let { userDtoService.toDetailsDto(user) }
    }

    @GetMapping(ApiRoutes.User.GET_INVITATION, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getUserByInvitationId(@PathVariable(ApiRoutes.User.Param.INVITATION_ID) invitationId: UUID): UserDetailsDto {
        val invitation = userService.findInvitation(invitationId)
        return userDtoService.toDetailsDto(invitation.user)
    }

    @GetMapping(ApiRoutes.User.GET_UPDATE_PASSWORD, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getUserByPasswordUpdateId(@PathVariable(ApiRoutes.User.Param.UPDATE_PASSWORD) updatePasswordId: UUID): UserDetailsDto {
        val user = userService.findByPasswordResetId(updatePasswordId)
        return userDtoService.toDetailsDto(user)
    }

    @PostMapping(ApiRoutes.User.ACTIVATE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun activateUser(
        @PathVariable(ApiRoutes.User.Param.USER) email: String,
        @PathVariable(ApiRoutes.User.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody resetPasswordRequest: ResetPasswordRequest
    ): UserDetailsDto {
        val user = updatePassword(email, resetPasswordRequest)
        return userDtoService.toDetailsDto(userService.acceptInvitation(user, organizationId))
    }

    @PatchMapping(ApiRoutes.User.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateUserPassword(
        @PathVariable(ApiRoutes.User.Param.USER) email: String,
        @RequestBody resetPasswordRequest: ResetPasswordRequest
    ): UserDetailsDto {
        return userDtoService.toDetailsDto(updatePassword(email, resetPasswordRequest))
    }

    @PostMapping(ApiRoutes.User.ACCEPT_INVITE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun acceptInvitation(@PathVariable(ApiRoutes.User.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId): UserDetailsDto {
        return userDtoService.toDetailsDto(userService.acceptInvitation(requireCurrentUser(), organizationId))
    }

    @PostMapping(ApiRoutes.User.FORGOT_PASSWORD, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.OK)
    fun forgotPasswordForUser(@RequestBody email: String, request: HttpServletRequest): String {
        userService.resetUserPassword(email, request.getHeader("X-Forwarded-For") ?: request.remoteAddr)
        return Responses.EMPTY_SUCCESS
    }

    private fun updatePassword(email: String, resetPasswordRequest: ResetPasswordRequest): User {
        val user = userService.updateUserPassword(email, resetPasswordRequest.password, resetPasswordRequest.resetToken)

        expireAllSessions(user.email)
        autoLoginUser(user.email)

        return user
    }

    private fun expireAllSessions(email: String) {
        this.sessions.findByPrincipalName(email).forEach {
            sessions.deleteById(it.value.id)
        }
    }
}
