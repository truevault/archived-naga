package polaris.controllers.api.privacycenter

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import polaris.controllers.AuthorizationNotSentException
import polaris.controllers.AuthorizationNotValidException
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.PrivacyCenterPublicId
import polaris.models.UUIDString
import polaris.models.dto.privacycenter.*
import polaris.services.dto.PrivacyCenterDtoService
import polaris.services.primary.OrganizationService
import polaris.services.primary.PrivacyCenterService
import polaris.util.ApiRoutes

@RestController
@Lazy
class PrivacyCenterController(
    private val organizationService: OrganizationService,
    private val privacyCenterService: PrivacyCenterService,
    private val privacyCenterDtoService: PrivacyCenterDtoService,
    private val lookup: LookupService,
    @Value("\${polaris.privacyCenter.inbound}")
    private val privacyCenterInboundKey: String
) : SessionAwareController() {
    companion object {
        private val log = LoggerFactory.getLogger(PrivacyCenterController::class.java)
    }

    @GetMapping(ApiRoutes.PrivacyCenter.SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getPrivacyCenter(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId
    ) = authorizeOrg(organizationId) {
        val privacyCenter = privacyCenterService.findByOrganizationAndPublicId(organizationId, privacyCenterId)
        privacyCenterDtoService.toDetailDto(privacyCenter)
    }

    @PostMapping(ApiRoutes.PrivacyCenter.BASE)
    fun createPrivacyCenter(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestBody createPrivacyCenter: CreatePrivacyCenter
    ) = authorizeOrg(organizationId) {
        privacyCenterDtoService.toDetailDto(privacyCenterService.create(organizationId, createPrivacyCenter))
    }

    @PutMapping(ApiRoutes.PrivacyCenter.SPECIFIC)
    fun updatePrivacyCenter(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody updatePrivacyCenter: UpdatePrivacyCenter
    ) = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        privacyCenterDtoService.toDetailDto(privacyCenterService.update(privacyCenterId, org, updatePrivacyCenter, requireCurrentUser()))
    }

    @GetMapping(ApiRoutes.PrivacyCenter.BASE)
    fun getOrganizationPrivacyCenters(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestHeader(name = "authorization") authorization: String?
    ) = authorizeOrg(organizationId) {
        val privacyCenters = privacyCenterService.findAllByOrganization(organizationId)
        privacyCenters.map { privacyCenterDtoService.toDetailDto(it) }
    }

    @GetMapping(ApiRoutes.PrivacyCenter.POLICY, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getPrivacyCenterPolicy(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId
    ) = authorizeOrg(organizationId) {
        val privacyCenter = privacyCenterService.findByOrganizationAndPublicId(organizationId, privacyCenterId)
        privacyCenterDtoService.toPolicyDto(privacyCenter)
    }

    @PutMapping(ApiRoutes.PrivacyCenter.POLICY, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updatePrivacyCenterPolicy(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody updatePolicy: UpdatePrivacyCenterPolicy
    ) = authorizeOrg(organizationId) {
        privacyCenterDtoService.toPolicyDto(
            privacyCenterService.updatePolicy(organizationId, privacyCenterId, updatePolicy)
        )
    }

    @PostMapping(ApiRoutes.PrivacyCenter.POLICY_SECTIONS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun addPrivacyCenterPolicySection(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody addOrUpdatePolicySection: AddOrUpdatePrivacyCenterPolicySection
    ) = authorizeOrg(organizationId) {
        privacyCenterDtoService.toPolicySectionDto(
            privacyCenterService.addPolicySection(organizationId, privacyCenterId, addOrUpdatePolicySection)
        )
    }

    @PutMapping(ApiRoutes.PrivacyCenter.POLICY_SECTION_SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updatePrivacyCenterPolicySection(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.POLICY_SECTION_ID) policySectionId: UUIDString,
        @RequestBody updatePolicySection: AddOrUpdatePrivacyCenterPolicySection
    ) = authorizeOrg(organizationId) {
        privacyCenterDtoService.toPolicySectionDto(
            privacyCenterService.updatePolicySection(organizationId, privacyCenterId, policySectionId, updatePolicySection)
        )
    }

    @DeleteMapping(ApiRoutes.PrivacyCenter.POLICY_SECTION_SPECIFIC)
    fun deletePrivacyCenterPolicySection(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.POLICY_SECTION_ID) policySectionId: UUIDString
    ) = authorizeOrg(organizationId) {
        privacyCenterService.deletePolicySection(organizationId, privacyCenterId, policySectionId)
    }

    @GetMapping(ApiRoutes.PrivacyCenter.BUILDER)
    fun getPrivacyCenterBuilder(
        @PathVariable(ApiRoutes.PrivacyCenter.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.PrivacyCenter.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestHeader(name = "authorization") authorization: String?
    ): PrivacyCenterBuilderDto {
        if (authorization == null) {
            log.info("Authorization Header was not present")
            throw AuthorizationNotSentException()
        }

        if (authorization != "Bearer $privacyCenterInboundKey") {
            log.info("Authorization Header was not valid: \"$authorization\" was not equal to \"Bearer $privacyCenterInboundKey\"")
            throw AuthorizationNotValidException()
        }
        val privacyCenter = privacyCenterService.findByOrganizationAndPublicId(organizationId, privacyCenterId)
        return privacyCenterDtoService.toBuilderDto(privacyCenter)
    }
}
