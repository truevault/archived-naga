package polaris.controllers.api.task

import org.springframework.context.annotation.Lazy
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.LookupService
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.UUIDString
import polaris.models.dto.task.TaskDto
import polaris.models.entity.TaskNotFound
import polaris.models.entity.organization.TaskCompletionType
import polaris.services.dto.TaskDtoService
import polaris.services.primary.TaskFilter
import polaris.services.primary.TaskService
import polaris.util.ApiRoutes

@RestController
@Lazy
class TaskController(
    private val taskService: TaskService,
    private val taskDtoService: TaskDtoService,
    private val lookup: LookupService
) : SessionAwareController() {
    @GetMapping(ApiRoutes.Organization.Tasks.BASE, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getTasks(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam(ApiRoutes.Organization.Requests.Param.FILTER) filter: TaskFilter = TaskFilter.active
    ): List<TaskDto> = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val tasks = taskService.forOrg(org, filter)
        tasks.map { taskDtoService.toDto(it) }
    }

    @PostMapping(ApiRoutes.Organization.Tasks.COMPLETE_TASKS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun completeTasks(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ) = authorizeOrg(organizationId) {
        val cu = getCurrentUser()
        val org = lookup.orgOrThrow(organizationId)
        taskService.closeTasksForOrg(org, cu)
    }

    @PostMapping(ApiRoutes.Organization.Tasks.COMPLETE_TASK, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun completeTask(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.Organization.Tasks.Param.TASK_ID) taskId: UUIDString
    ): TaskDto? = authorizeOrg(organizationId) {
        val cu = getCurrentUser()
        val org = lookup.orgOrThrow(organizationId)
        val task = taskService.findById(org, taskId) ?: throw TaskNotFound()

        if (task.completionType == TaskCompletionType.MANUAL && !task.isComplete()) {
            taskService.closeTask(task, cu)
            taskDtoService.toDto(task)
        } else {
            throw TaskNotFound()
        }
    }

    @PostMapping(ApiRoutes.Organization.Tasks.ADD_DPO, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createAddDpoTask(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): TaskDto? = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val task = taskService.createDpoTask(org)
        task?.let { taskDtoService.toDto(it) }
    }

    @PostMapping(ApiRoutes.Organization.Tasks.AUTHORIZE_GOOGLE_ANALYTICS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createAuthorizeGoogleAnalyticsTask(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): TaskDto? = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val task = taskService.createAuthorizeGoogleAnalyticsTask(org)

        task?.let { taskDtoService.toDto(it) }
    }

    @PostMapping(ApiRoutes.Organization.Tasks.COMPLETE_FINANCIAL_INCENTIVES, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createCompleteFinancialIncentives(
        @PathVariable(ApiRoutes.Organization.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId
    ): TaskDto? = authorizeOrg(organizationId) {
        val org = lookup.orgOrThrow(organizationId)
        val task = taskService.createFinancialIncentivesTask(org)
        task?.let { taskDtoService.toDto(it) }
    }
}
