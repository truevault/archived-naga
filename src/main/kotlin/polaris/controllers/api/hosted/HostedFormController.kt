package polaris.controllers.api.hosted

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.PrivacyCenterPublicId
import polaris.models.dto.request.ConfirmIdentityDataRequest
import polaris.models.dto.request.CreateDataRequest
import polaris.models.dto.request.DataRequestTypeDto
import polaris.models.dto.request.DataSubjectRequestSummaryDto
import polaris.models.dto.toIso
import polaris.models.entity.BadRequestNoConsentProcessingActivities
import polaris.models.entity.BadRequestNoCorrectionRequest
import polaris.models.entity.BadRequestNoObjectionRequest
import polaris.models.entity.CouldNotConfirm
import polaris.models.entity.GdprCookieConsentState
import polaris.models.entity.MissingDeclarationOrCustomField
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.PrivacyCenterNotFound
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.EventData
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubmissionSourceEnum
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.Regulation
import polaris.models.entity.request.RequestWorkflow
import polaris.repositories.DataRequestRepository
import polaris.repositories.PrivacyCenterRepository
import polaris.services.dto.DataSubjectRequestDtoService
import polaris.services.primary.CMPOptionsDto
import polaris.services.primary.ConsentConfigurationService
import polaris.services.primary.DataSubjectRequestService
import polaris.services.primary.OrganizationService
import polaris.services.primary.ProcessingActivityService
import polaris.services.primary.RequestTypeVisbility
import polaris.services.support.EmailService
import polaris.services.support.GdprCookieConsentService
import polaris.services.support.events.EventTrackingService
import polaris.util.ApiRoutes
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import javax.servlet.http.HttpServletRequest
import kotlin.math.abs

data class HostedFormOptionsDto(
    val dataRequestTypeOptions: List<HostedFormSelectOptions>,
    val consentProcessingActivites: List<String>
)

data class HostedFormSelectOptions(
    val value: DataRequestType,
    val regulation: Regulation,
    val label: String,
    val declaration: String?,

    val correctionInput: Boolean?,
    val objectionInput: Boolean?,
    val withdrawalInput: Boolean?,
    val appealInput: Boolean?
)

data class CreateHostedDataRequest(
    val firstName: String,
    val lastName: String,
    val email: String,
    val context: CollectionContext? = null,
    val dataRequestType: DataRequestType,
    val phoneNumber: String? = null,
    val mailingAddress: String? = null,
    val organizationCustomInput: String? = null,
    val confirmation: Boolean? = false,
    val correctionRequest: String? = null,
    val objectionRequest: String? = null,
    val appealId: String? = null,
    val withdrawnConsent: List<String>? = null,
    val notes: String? = null,
    val gaUserId: String? = null
)

data class ConfirmEmailResponse(
    val id: String,
    val type: String?,
    val days: Long
)

data class MissingDeclarationOrCustomFieldResponse(
    val code: String,
    val missingCustomField: Boolean? = false,
    val missingDeclaration: Boolean? = false,
    val firstName: String?,
    val lastName: String?,
    val email: String?,
    val dataRequestType: DataRequestTypeDto
)

data class CCPASpecificRequest(
    val firstName: String?,
    val lastName: String?,
    val email: String?,
    val selfDeclarationProvided: Boolean
)

data class OptOutRequest(
    val browserOptOut: Boolean,
    val directOptOut: Boolean,
    val gpcOptOut: Boolean?,
    val firstName: String?,
    val lastName: String?,
    val email: String?
)

data class GDPRCookieConsentRequest(
    val sourceUrl: String,
    val key: String,
    val consentState: GdprCookieConsentState
)

// per POLARIS-838 (https://truevault.atlassian.net/browse/POLARIS-838) we should do all of our
// time calculation in PST
const val POLARIS_TIME_ZONE_STR = "America/Los_Angeles"
val POLARIS_TIME_ZONE: ZoneId = ZoneId.of(POLARIS_TIME_ZONE_STR)

@RestController
@Lazy
@CrossOrigin(origins = ["*"])
class HostedFormController(
    private val dataSubjectRequestService: DataSubjectRequestService,
    private val emailService: EmailService,
    private val organizationService: OrganizationService,
    private val privacyCenterRepository: PrivacyCenterRepository,
    private val dataSubjectRequestDto: DataSubjectRequestDtoService,
    private val dataRequestRepository: DataRequestRepository,
    private val gdprCookieConsentService: GdprCookieConsentService,
    private val cmpConfigurationService: ConsentConfigurationService,
    private val processingActivityService: ProcessingActivityService,
    private val eventTrackingService: EventTrackingService,
    private val lookup: LookupService
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(DataSubjectRequestService::class.java)
    }

    @GetMapping(ApiRoutes.HostedForm.FORM_OPTIONS, produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getHostedFormOptions(@PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId): HostedFormOptionsDto {
        val privacyCenter: PrivacyCenter =
            privacyCenterRepository.findByPublicId(privacyCenterId) ?: throw PrivacyCenterNotFound(privacyCenterId)
        val organizationId: OrganizationPublicId = privacyCenter.organization?.publicId as OrganizationPublicId
        val org = lookup.orgOrThrow(organizationId)

        val enabledDataRequestTypes = organizationService.getDataRequestTypes(organizationId, RequestTypeVisbility.CONSUMER_FORM).map {
            HostedFormSelectOptions(
                value = it, label = it.label, regulation = it.regulation, declaration = it.declarationText,
                correctionInput = it.workflow == RequestWorkflow.RECTIFICATION,
                objectionInput = it.workflow == RequestWorkflow.OBJECTION,
                withdrawalInput = it.workflow == RequestWorkflow.WITHDRAW_CONSENT,
                appealInput = it == DataRequestType.VCDPA_APPEAL
            )
        }.sortedBy {
            it.label
        }

        return HostedFormOptionsDto(
            dataRequestTypeOptions = enabledDataRequestTypes,
            consentProcessingActivites = processingActivityService.getConsentProcessingActivitiesForOrg(org).map { it.name }
        )
    }

    @PostMapping(ApiRoutes.HostedForm.CONFIRM, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun confirmEmail(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody confirmDto: ConfirmIdentityDataRequest
    ): ResponseEntity<Any> {
        log.info("Starting confirm email controller")
        val privacyCenter: PrivacyCenter =
            privacyCenterRepository.findByPublicId(privacyCenterId) ?: throw PrivacyCenterNotFound(privacyCenterId)
        log.info("Found privacy center")
        val org = privacyCenter.organization!!
        log.info("Found organization")

        log.info("Looking up request with confirmation token: ${confirmDto.confirmationToken}")
        val request =
            dataRequestRepository.findByConfirmationToken(confirmDto.confirmationToken) ?: throw CouldNotConfirm()
        log.info("Found request with token")

        if (request.organization != org) {
            log.info("Failed to confirm due to organizations not matching")
            throw CouldNotConfirm()
        }

        log.info("Starting email verification")
        val updatedRequest =
            if (!request.emailVerified() && request.state == DataRequestState.PENDING_EMAIL_VERIFICATION) {
                try {
                    log.info("Calling confirm email")
                    val events = mutableListOf<EventData>()
                    val res = dataSubjectRequestService.confirmEmail(
                        null,
                        org.publicId,
                        request.publicId,
                        confirmDto,
                        privacyCenter,
                        ZonedDateTime.now(POLARIS_TIME_ZONE),
                        events
                    )
                    eventTrackingService.publish(events, org, null)
                    res
                } catch (e: MissingDeclarationOrCustomField) {
                    if (e.missingCustomField) log.warn("HostedFormController.confirmEmail failed due to a missing custom field")
                    if (e.missingDeclaration) log.warn("HostedFormController.confirmEmail failed due to a missing declaration")

                    return ResponseEntity(
                        MissingDeclarationOrCustomFieldResponse(
                            code = "missing-additional-data",
                            missingCustomField = e.missingCustomField,
                            missingDeclaration = e.missingDeclaration,
                            firstName = e.dataRequest.subjectFirstName,
                            lastName = e.dataRequest.subjectLastName,
                            email = e.dataRequest.subjectEmailAddress,
                            dataRequestType = e.dataRequest.requestType.toDto()
                        ),
                        HttpStatus.BAD_REQUEST
                    )
                }
            } else {
                request
            }

        val now = ZonedDateTime.now(POLARIS_TIME_ZONE)
        val days = ChronoUnit.DAYS.between(now, updatedRequest.requestDueDate)

        val body = ConfirmEmailResponse(updatedRequest.publicId, updatedRequest.requestType.label, abs(days))

        return ResponseEntity(body, HttpStatus.OK)
    }

    @PostMapping(ApiRoutes.HostedForm.REQUESTS, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    fun createRequest(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody createDto: CreateHostedDataRequest,
        request: HttpServletRequest
    ): DataSubjectRequestSummaryDto {
        val privacyCenter: PrivacyCenter =
            privacyCenterRepository.findByPublicId(privacyCenterId) ?: throw PrivacyCenterNotFound(privacyCenterId)
        val organizationId: OrganizationPublicId = privacyCenter.organization?.publicId as OrganizationPublicId
        val ipAddress = request.getHeader("X-Forwarded-For") ?: request.remoteAddr

        if (createDto.dataRequestType == DataRequestType.GDPR_RECTIFICATION && createDto.correctionRequest.isNullOrBlank()) {
            throw BadRequestNoCorrectionRequest()
        }

        if (createDto.dataRequestType == DataRequestType.GDPR_OBJECTION && createDto.objectionRequest.isNullOrBlank()) {
            throw BadRequestNoObjectionRequest()
        }

        if (createDto.dataRequestType == DataRequestType.GDPR_WITHDRAWAL && createDto.withdrawnConsent.isNullOrEmpty()) {
            throw BadRequestNoConsentProcessingActivities()
        }

        var notes = createDto.notes ?: ""
        if (notes.isNotEmpty()) {
            notes += "\n\n"
        }
        notes += "Request Source: ${privacyCenter.name}"

        val createDataRequest = CreateDataRequest(
            subjectFirstName = createDto.firstName,
            subjectLastName = createDto.lastName,
            dataRequestType = createDto.dataRequestType,
            requestDate = ZonedDateTime.now(POLARIS_TIME_ZONE).toIso(),
            subjectEmailAddress = createDto.email,
            subjectPhoneNumber = createDto.phoneNumber,
            subjectMailingAddress = createDto.mailingAddress,
            organizationCustomInput = createDto.organizationCustomInput,
            selfDeclarationProvided = createDto.confirmation ?: false,
            subjectIpAddress = ipAddress,
            isDataSubject = true,
            notes = notes,
            correctionRequest = createDto.correctionRequest,
            appealId = createDto.appealId,
            objectionRequest = createDto.objectionRequest,
            consentWithdrawnRequest = createDto.withdrawnConsent,
            submissionSource = DataRequestSubmissionSourceEnum.PRIVACY_CENTER,
            gaUserId = createDto.gaUserId,
            context = createDto.context ?: CollectionContext.CONSUMER
        )
        val privacyRequest = dataSubjectRequestService.create(null, organizationId, createDataRequest)
        emailService.sendNewDataSubjectRequestNotification(privacyRequest, privacyCenter.name)
        return dataSubjectRequestDto.toSummaryDto(privacyRequest)
    }

    @PostMapping(ApiRoutes.HostedForm.CCPA_SPECIFIC, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    fun ccpaSpecific(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody specificDto: CCPASpecificRequest,
        request: HttpServletRequest
    ): DataSubjectRequestSummaryDto {
        val privacyCenter: PrivacyCenter =
            privacyCenterRepository.findByPublicId(privacyCenterId) ?: throw PrivacyCenterNotFound(privacyCenterId)
        val organizationId: OrganizationPublicId = privacyCenter.organization?.publicId as OrganizationPublicId
        val notes = "Request Source: ${privacyCenter.name}"
        val ipAddress = request.getHeader("X-Forwarded-For") ?: request.remoteAddr

        val createDataRequest = CreateDataRequest(
            subjectFirstName = specificDto.firstName,
            subjectLastName = specificDto.lastName,
            dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW_CATEGORIES,
            requestDate = ZonedDateTime.now().toIso(),
            subjectEmailAddress = specificDto.email,
            subjectIpAddress = ipAddress,
            isDataSubject = true,
            notes = notes,
            submissionSource = DataRequestSubmissionSourceEnum.PRIVACY_CENTER
        )
        val privacyRequest = dataSubjectRequestService.create(null, organizationId, createDataRequest)
        emailService.sendNewDataSubjectRequestNotification(privacyRequest, privacyCenter.name)
        return dataSubjectRequestDto.toSummaryDto(privacyRequest)
    }

    @PostMapping(ApiRoutes.HostedForm.CCPA_OPT_OUT, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    fun ccpaOptOut(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody optOutDto: OptOutRequest,
        request: HttpServletRequest
    ): DataSubjectRequestSummaryDto {
        return internalOptOut(privacyCenterId, optOutDto, request, DataRequestType.CCPA_OPT_OUT)
    }

    @PostMapping(ApiRoutes.HostedForm.CCPA_RIGHT_TO_LIMIT, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    fun ccpalimitSpi(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody optOutDto: OptOutRequest,
        request: HttpServletRequest
    ): DataSubjectRequestSummaryDto {
        return internalOptOut(privacyCenterId, optOutDto, request, DataRequestType.CCPA_RIGHT_TO_LIMIT, canAutoclose = false)
    }

    @PostMapping(ApiRoutes.HostedForm.VCDPA_OPT_OUT, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    fun vcdpaOptOut(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody optOutDto: OptOutRequest,
        request: HttpServletRequest
    ): DataSubjectRequestSummaryDto {
        return internalOptOut(privacyCenterId, optOutDto, request, DataRequestType.VCDPA_OPT_OUT)
    }

    @PostMapping(ApiRoutes.HostedForm.CPA_OPT_OUT, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    fun cpaOptOut(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody optOutDto: OptOutRequest,
        request: HttpServletRequest
    ): DataSubjectRequestSummaryDto {
        return internalOptOut(privacyCenterId, optOutDto, request, DataRequestType.CPA_OPT_OUT)
    }

    @PostMapping(ApiRoutes.HostedForm.CTDPA_OPT_OUT, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    fun ctdpaOptOut(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody optOutDto: OptOutRequest,
        request: HttpServletRequest
    ): DataSubjectRequestSummaryDto {
        return internalOptOut(privacyCenterId, optOutDto, request, DataRequestType.CTDPA_OPT_OUT)
    }

    private fun internalOptOut(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody optOutDto: OptOutRequest,
        request: HttpServletRequest,
        requestType: DataRequestType,
        canAutoclose: Boolean = true
    ): DataSubjectRequestSummaryDto {
        val privacyCenter: PrivacyCenter =
            privacyCenterRepository.findByPublicId(privacyCenterId) ?: throw PrivacyCenterNotFound(privacyCenterId)
        val organizationId: OrganizationPublicId = privacyCenter.organization?.publicId as OrganizationPublicId
        val notes = "Opt Out Source: ${privacyCenter.name}"
        val ipAddress = request.getHeader("X-Forwarded-For") ?: request.remoteAddr

        val createDataRequest = CreateDataRequest(
            subjectFirstName = optOutDto.firstName,
            subjectLastName = optOutDto.lastName,
            dataRequestType = requestType,
            requestDate = ZonedDateTime.now().toIso(),
            subjectEmailAddress = optOutDto.email,
            subjectIpAddress = ipAddress,
            isDataSubject = true,
            notes = notes,
            submissionSource = DataRequestSubmissionSourceEnum.PRIVACY_CENTER,
            gpcOptOut = optOutDto.gpcOptOut ?: false
        )
        val privacyRequest = dataSubjectRequestService.createOptOutRequest(
            null,
            organizationId,
            createDataRequest,
            autoClose = canAutoclose && !optOutDto.directOptOut
        )

        if (optOutDto.directOptOut) {
            emailService.sendNewDataSubjectRequestNotification(privacyRequest, privacyCenter.name)
        }
        return dataSubjectRequestDto.toSummaryDto(privacyRequest)
    }

    @PostMapping(ApiRoutes.HostedForm.GDPR_COOKIE_CONSENT, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    fun gdprCookieConsent(
        @PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestBody dto: GDPRCookieConsentRequest,
        request: HttpServletRequest
    ) {
        val privacyCenter: PrivacyCenter =
            privacyCenterRepository.findByPublicId(privacyCenterId) ?: throw PrivacyCenterNotFound(privacyCenterId)

        val ipAddress = request.getHeader("X-Forwarded-For") ?: request.remoteAddr
        val userAgent = request.getHeader(HttpHeaders.USER_AGENT)

        gdprCookieConsentService.addCookieConsent(
            privacyCenter,
            key = dto.key,
            sourceUrl = dto.sourceUrl,
            consentState = dto.consentState,
            ip = ipAddress,
            userAgent = userAgent
        )
    }

    @GetMapping(ApiRoutes.HostedForm.POLARIS_JS_CONFIG, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun polarisJsConfig(@PathVariable(ApiRoutes.HostedForm.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId): CMPOptionsDto {
        val privacyCenter: PrivacyCenter = privacyCenterRepository.findByPublicId(privacyCenterId) ?: throw PrivacyCenterNotFound(privacyCenterId)
        val organization = privacyCenter.organization ?: throw OrganizationNotFound()

        return cmpConfigurationService.buildConsentConfiguration(organization, privacyCenter)
    }
}
