package polaris.controllers.api

import com.amazonaws.services.s3.model.AmazonS3Exception
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.InputStreamResource
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import polaris.controllers.util.SessionAwareController
import polaris.models.OrganizationPublicId
import polaris.models.PrivacyCenterPublicId
import polaris.models.dto.LogoType
import polaris.models.entity.VendorNotFound
import polaris.services.primary.OrganizationVendorService
import polaris.services.support.AttachmentService
import polaris.services.support.BlockStorageService
import polaris.services.support.ResourceAttachment
import polaris.util.ApiRoutes
import java.security.SecureRandom
import java.util.UUID
import javax.servlet.http.HttpServletRequest

@RestController
class BlockStorageController(
    @Value("\${polaris.images.baseUrl}")
    private val imagesBaseUrl: String?,
    @Value("\${polaris.vendor.agreements.bucket}")
    private val vendorAgreementsBucket: String?,
    @Value("\${polaris.vendor.agreements.prefix}")
    private val vendorAgreementsPrefix: String,
    private val attachmentService: AttachmentService,
    private val blockStorageService: BlockStorageService,
    private val organizationVendorService: OrganizationVendorService

) : SessionAwareController() {
    companion object {
        private val log = LoggerFactory.getLogger(BlockStorageController::class.java)
    }

    @GetMapping(ApiRoutes.BlockStorage.Public.ATTACHMENT)
    @ResponseBody
    fun getAttachment(@PathVariable id: UUID): ResponseEntity<Resource> {
        val f: ResourceAttachment = attachmentService.getAttachment(id)
        return ResponseEntity.ok().header(
            HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"${f.attachment.name}\""
        ).body(f.resource)
    }

    @GetMapping(ApiRoutes.BlockStorage.Public.IMAGES_WILDCARD)
    @ResponseBody
    fun getImage(request: HttpServletRequest): ResponseEntity<Resource> {
        val filename = request.requestURL.split("/images/")[1]
        return try {
            val resource = InputStreamResource(blockStorageService.getImage(filename))
            val headers = HttpHeaders()
            headers.contentType = blockStorageService.getMediaTypeFromFilename(filename)
            ResponseEntity(resource, headers, HttpStatus.OK)
        } catch (e: AmazonS3Exception) {
            log.info("Failed to fetch resource [$filename] from block storage: $e")
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping(ApiRoutes.BlockStorage.Logos.ORGANIZATION, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun postOrganizationLogo(
        @PathVariable(ApiRoutes.BlockStorage.Logos.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @RequestParam("logoType") logoType: LogoType,
        @RequestParam("logo") logo: MultipartFile
    ) = authorizeOrg(organizationId) {
        val logoFilename = generateLogoFilename(
            organizationId, logoType.label, blockStorageService.getFileExtension(logo.originalFilename ?: "")
        )
        val fullLogoPath = getFullPath("organizations", logoFilename)
        uploadLogo(fullLogoPath, logo)
    }

    @PostMapping(ApiRoutes.BlockStorage.Logos.PRIVACY_CENTER, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun postPrivacyCenterLogo(
        @PathVariable(ApiRoutes.BlockStorage.Logos.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.BlockStorage.Logos.Param.PRIVACY_CENTER_ID) privacyCenterId: PrivacyCenterPublicId,
        @RequestParam("logoType") logoType: LogoType,
        @RequestParam("logo") logo: MultipartFile
    ) = authorizeOrg(organizationId) {
        val logoFilename = generateLogoFilename(
            privacyCenterId, logoType.label, blockStorageService.getFileExtension(logo.originalFilename ?: "")
        )
        val fullLogoPath = getFullPath("privacy-centers", logoFilename)
        uploadLogo(fullLogoPath, logo)
    }

    @PostMapping(ApiRoutes.BlockStorage.VendorAgreements.VENDOR, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun postOrganizationVendorAgreement(
        @PathVariable(ApiRoutes.BlockStorage.VendorAgreements.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.BlockStorage.VendorAgreements.Param.VENDOR_ID) vendorId: PrivacyCenterPublicId,
        @RequestParam("filename") filename: String,
        @RequestParam("filedata") filedata: MultipartFile
    ) = authorizeOrg(organizationId) {
        uploadVendorAgreement(organizationId, vendorId, filename, filedata)
    }
    @DeleteMapping(ApiRoutes.BlockStorage.VendorAgreements.AGREEMENT, produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun deleteOrganizationVendorAgreement(
        @PathVariable(ApiRoutes.BlockStorage.VendorAgreements.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.BlockStorage.VendorAgreements.Param.VENDOR_ID) vendorId: String,
        @PathVariable(ApiRoutes.BlockStorage.VendorAgreements.Param.AGREEMENT_KEY) fileKey: String
    ) = authorizeOrg(organizationId) {

        deleteVendorAgreement(vendorId, fileKey)
    }

    @GetMapping(ApiRoutes.BlockStorage.VendorAgreements.VENDOR)
    @ResponseBody
    fun getOrganizationVendorAgreement(
        @PathVariable(ApiRoutes.BlockStorage.VendorAgreements.Param.ORGANIZATION_ID) organizationId: OrganizationPublicId,
        @PathVariable(ApiRoutes.BlockStorage.VendorAgreements.Param.VENDOR_ID) vendorId: UUID
    ): ResponseEntity<Resource> {

        return try {

            val orgVendor = organizationVendorService.getOrganizationVendor(organizationId, vendorId) ?: throw VendorNotFound(
                vendorId.toString()
            )
            val fileKey = orgVendor.tosFileKey!!
            val resource = InputStreamResource(blockStorageService.get(vendorAgreementsBucket!!, "$vendorAgreementsPrefix/$fileKey"))
            val headers = HttpHeaders()
            headers.contentType = blockStorageService.getMediaTypeFromFilename(orgVendor.tosFileName!!)
            ResponseEntity(resource, headers, HttpStatus.OK)
        } catch (e: AmazonS3Exception) {
            log.info("Failed to fetch resource from block storage: $e")
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    private fun uploadVendorAgreement(organizationId: String, vendorId: String, filename: String, filedata: MultipartFile): Map<String, String> {
        val random = SecureRandom()
        val uniq = String.format("%06d", random.nextInt(999999))
        val fileKey = "${organizationId}_${vendorId}_${uniq}_$filename"

        return try {
            blockStorageService.putFile(vendorAgreementsBucket!!, "$vendorAgreementsPrefix/$fileKey", filedata)
            return mapOf("fileKey" to fileKey, "fileName" to filename, "vendorId" to vendorId)
        } catch (e: RuntimeException) {
            log.warn("Failed to put file $fileKey in block storage: $e")
            return mapOf("error" to e.toString())
        }
    }

    private fun deleteVendorAgreement(vendorId: String, fileKey: String): Map<String, String> {
        return try {
            blockStorageService.delete(vendorAgreementsBucket!!, "$vendorAgreementsPrefix/$fileKey")
            return mapOf("success" to "true", "vendorId" to vendorId)
        } catch (e: RuntimeException) {
            log.warn("Failed to delete file $fileKey in block storage: $e")
            return mapOf("error" to e.toString())
        }
    }

    private fun uploadLogo(fullLogoPath: String, logo: MultipartFile): Map<String, String> {
        return try {
            blockStorageService.putImage(fullLogoPath, logo)
            mapOf("logoPath" to fullLogoPath, "logoUrl" to getLogoUrl(fullLogoPath))
        } catch (e: RuntimeException) {
            log.warn("Failed to put image $fullLogoPath in block storage: $e")
            mapOf("error" to e.toString())
        }
    }

    private fun generateLogoFilename(sourceId: String, logoType: String, extension: String?): String {
        val random = SecureRandom()
        val suffix = String.format("%06d", random.nextInt(999999))
        val ext = if (!extension.isNullOrBlank()) ".$extension" else ""
        return "$sourceId-$logoType-$suffix$ext"
    }

    private fun getFullPath(sourceType: String, sourceFilename: String) = "logos/$sourceType/$sourceFilename"

    private fun getLogoUrl(fullLogoPath: String) = "$imagesBaseUrl/$fullLogoPath"
}
