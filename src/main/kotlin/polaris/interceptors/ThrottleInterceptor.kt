package polaris.interceptors

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import polaris.services.ThrottleService
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class ThrottleInterceptor : HandlerInterceptorAdapter() {
    @Autowired
    private val throttleService: ThrottleService = ThrottleService()

    @Override
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {

        val ipAddress = request.remoteAddr
        val bucket = throttleService.resolveBucket(ipAddress)
        val probe = bucket.tryConsumeAndReturnRemaining(1)

        return if (probe.isConsumed) {
            response.addHeader("RateLimit-Remaining", probe.remainingTokens.toString())
            true
        } else {
            val waitForRefill = probe.nanosToWaitForRefill / 1_000_000_000
            response.addHeader("RateLimit-Reset", waitForRefill.toString())
            response.addHeader("RateLimit-Remaining", probe.remainingTokens.toString())
            response.status = HttpStatus.TOO_MANY_REQUESTS.value()
            false
        }
    }
}
