package polaris.services

import io.github.bucket4j.Bandwidth
import io.github.bucket4j.Bucket
import io.github.bucket4j.Refill
import java.time.Duration
import java.util.concurrent.ConcurrentHashMap

class ThrottleService {
    private val bucketCache: ConcurrentHashMap<String, Bucket> = ConcurrentHashMap()

    fun resolveBucket(ipAddress: String): Bucket {
        return bucketCache.computeIfAbsent(ipAddress, this::newBucket)
    }

    private fun newBucket(key: String): Bucket {
        val refill = Refill.greedy(400, Duration.ofMinutes(1))
        val limit = Bandwidth.classic(400, refill)

        return Bucket.builder().addLimit(limit).build()
    }
}
