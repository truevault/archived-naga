package polaris.services.support.autodelete

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.controllers.GoogleAnalyticsAutodeleteFailedException
import polaris.controllers.GoogleAnalyticsCannotAutodeleteException
import polaris.models.Iso8601Date
import polaris.models.dto.toIso
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationAuthToken
import polaris.models.entity.organization.OrganizationAuthTokenProvider
import polaris.models.entity.request.DataSubjectRequest
import polaris.services.support.oauth.GoogleOauthSupport
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.ZonedDateTime

/**
 * See https://developers.google.com/analytics/devguides/config/userdeletion/v3
 */

data class GaUserDeletionRequestId(
    val type: String,
    val userId: String
)

data class GaUserDeletionRequest(
    val kind: String,
    val id: GaUserDeletionRequestId,
    val deletionRequestTime: Iso8601Date,
    val propertyId: String? = null,
    val webPropertyId: String? = null
)

@Service
class GoogleAnalyticsAutodeleteService(
    private val googleOauth: GoogleOauthSupport
) {

    companion object {
        private val log = LoggerFactory.getLogger(GoogleAnalyticsAutodeleteService::class.java)
    }

    fun assertCanAutodelete(org: Organization, authorization: OrganizationAuthToken?, request: DataSubjectRequest) {
        val hasWebProperty = !org.gaWebProperty.isNullOrBlank()
        val hasGaUserId = !request.gaUserId.isNullOrBlank()
        val hasAuthorization = authorization != null &&
            authorization.organization == org &&
            authorization.provider == OrganizationAuthTokenProvider.GOOGLE_OAUTH &&
            authorization.refresh != null

        val result = hasAuthorization && hasGaUserId && hasWebProperty

        if (!result) {
            log.warn("Request ${request.publicId} for org ${org.publicId} (${org.name}) not eligible for GA auto-deletion: hasWebProperty=$hasWebProperty, hasAuthorization=$hasAuthorization, hasGaUserId=$hasGaUserId")
        }

        if (!hasWebProperty) {
            throw GoogleAnalyticsCannotAutodeleteException("GA Property ID not configured")
        }
        if (!hasAuthorization) {
            throw GoogleAnalyticsCannotAutodeleteException("GA OAuth not configured")
        }
        if (!hasGaUserId) {
            throw GoogleAnalyticsCannotAutodeleteException("Request ${request.publicId} does not have GA user/client information to delete")
        }
    }

    fun autodelete(org: Organization, authorization: OrganizationAuthToken?, request: DataSubjectRequest) {
        log.info("Processing auto-delete of GA user data for request ${request.publicId} for org ${org.publicId} (${org.name})...")

        assertCanAutodelete(org, authorization, request)

        try {
            log.info("Requesting access token from refresh token...")
            val response = googleOauth.requestAccessTokenFromRefreshToken(authorization!!.refresh!!)

            deleteGoogleAnalyticsUser(response.access_token!!, org.gaWebProperty!!, request.gaUserId!!)
            log.info("Successfully auto-deleted GA user data for org ${org.publicId} (${org.name}), request ${request.publicId}")
        } catch (ex: Exception) {
            log.error("Exception occurred while processing GA auto-delete request: ${ex.message}", ex)
            throw ex
        }
    }

    fun deleteGoogleAnalyticsUser(accessToken: String, propertyId: String, clientId: String) {
        val bodyObj = createGaUserDeletionRequest(propertyId, clientId)

        val body = jacksonObjectMapper().writeValueAsString(bodyObj)
        val client = HttpClient.newBuilder().build()

        val request = HttpRequest.newBuilder()
            .uri(URI.create("https://www.googleapis.com/analytics/v3/userDeletion/userDeletionRequests:upsert?access_token=$accessToken"))
            .headers("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(body))
            .build()

        log.info("Submitting GA userDeletionRequest: $bodyObj")
        val response: HttpResponse<*> = client.send(request, HttpResponse.BodyHandlers.ofString())

        if (response.statusCode() < 200 || response.statusCode() >= 300) {
            log.error("GA userDeletionRequest failed for GA property $propertyId with error: ${response.body()}")
            throw GoogleAnalyticsAutodeleteFailedException()
        }
    }

    fun createGaUserDeletionRequest(propertyId: String, clientId: String): GaUserDeletionRequest {
        val isUniversalAnalytics = propertyId.startsWith("UA-")
        return if (isUniversalAnalytics)
            GaUserDeletionRequest(
                kind = "analytics#userDeletionRequest",
                id = GaUserDeletionRequestId(type = "CLIENT_ID", userId = clientId),
                deletionRequestTime = ZonedDateTime.now().toIso(),
                webPropertyId = propertyId
            )
        else
            GaUserDeletionRequest(
                kind = "analytics#userDeletionRequest",
                id = GaUserDeletionRequestId(type = "CLIENT_ID", userId = clientId),
                deletionRequestTime = ZonedDateTime.now().toIso(),
                propertyId = propertyId
            )
    }
}
