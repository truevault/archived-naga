package polaris.services.support

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.util.LookupService
import polaris.models.dto.privacycenter.PrivacyCenterBuilderMessage
import polaris.models.dto.toIso
import polaris.models.entity.organization.Organization
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.privacycenter.PrivacyCenterBuild
import polaris.repositories.PrivacyCenterBuildRepository
import polaris.repositories.PrivacyCenterRepository
import java.time.ZonedDateTime
import java.util.*

const val BUILD_DEBOUNCE_MINUTES = 1

@Service
class PrivacyCenterBuilderService(
    private val privacyCenterRepository: PrivacyCenterRepository,
    private val awsClientService: AwsClientService,
    private val lookupService: LookupService,

    @Value("\${polaris.aws.privacyCenterBuildQueueUrl}")
    private val privacyCenterBuildQueueUrl: String,
    @Value("\${polaris.privacyCenter.bucket}")
    private val privacyCenterBucket: String,
    @Value("\${polaris.privacyCenter.disableBuild}")
    private val privacyCenterDisableBuilds: Boolean,

    private val privacyCenterBuildRepository: PrivacyCenterBuildRepository
) {
    companion object {
        private val log = LoggerFactory.getLogger(PrivacyCenterBuilderService::class.java)
    }

    private val sqs = awsClientService.sqsClient()

    fun sendBuildPrivacyCenterMessage(privacyCenter: PrivacyCenter) {
        log.info("requesting privacy center build for privacy center: ${privacyCenter.publicId}")
        log.info("inserting message into queue: $privacyCenterBuildQueueUrl")

        val o = privacyCenter.organization!!

        val message = PrivacyCenterBuilderMessage(
            organizationId = o.publicId, internalOrganizationId = o.id.toString(),
            privacyCenterId = privacyCenter.publicId,
            sitesBucket = privacyCenterBucket,
            cloudfrontDistribution = privacyCenter.cloudfrontDistribution
        )

        log.info("inserting message json: ${message.toJson()}")

        if (!privacyCenterDisableBuilds) {
            sqs.sendMessage(privacyCenterBuildQueueUrl, message.toJson())
            log.info("inserted message")
        } else {
            log.info("not sending, due to disabled builds")
        }
    }

    fun sendBuilds() {
        log.info("Sending builds, retrieving all open privacy center build requests at: ${ZonedDateTime.now().toIso()}")
        val allUnbuilt = privacyCenterBuildRepository.findAllBySentBuildMessageAtIsNull()

        val byPrivacyCenter = allUnbuilt.filter { it.privacyCenter != null }.groupBy { it.privacyCenter }

        val cutoff = ZonedDateTime.now().minusMinutes(BUILD_DEBOUNCE_MINUTES.toLong())
        byPrivacyCenter.forEach { (pc, unbuilt) ->
            log.info("Some privacy centers have open build requests ${pc!!.publicId}: ${unbuilt.size}. Comparing to the current cutoff of $cutoff")
            val needsBuild = unbuilt.all { it.requestedAt.isBefore(cutoff) }
            if (needsBuild) {
                log.info("All open privacy center build requests for ${pc.publicId} are before the cutoff. Sending a message, and marking these as done.")
                sendBuildPrivacyCenterMessage(pc)
                unbuilt.forEach { it.sentBuildMessageAt = ZonedDateTime.now() }
                privacyCenterBuildRepository.saveAll((unbuilt))
            }
        }
        log.info("Sending builds, finished at: ${ZonedDateTime.now().toIso()}")
    }

    fun insertPrivacyCenterBuildRequest(
        privacyCenter: PrivacyCenter,
        source: String,
        throttle: Boolean = false
    ) {
        val exists = {
            privacyCenterBuildRepository.existsByPrivacyCenterAndSourceAndSentBuildMessageAtIsNull(
                privacyCenter,
                source
            )
        }
        if (!throttle || !exists()) {
            privacyCenterBuildRepository.save(
                PrivacyCenterBuild(
                    privacyCenter = privacyCenter,
                    requestedAt = ZonedDateTime.now(),
                    sentBuildMessageAt = null,
                    source = source
                )
            )
        }
    }

    @Transactional
    fun requestBuildPrivacyCentersForOrg(organization: Organization, source: String, throttle: Boolean = false) {
        log.info("Queuing Privacy Center rebuilds for: ${organization.name} (${organization.privacyCenters.size} privacy centers)")
        organization.privacyCenters.forEach { insertPrivacyCenterBuildRequest(it, source, throttle) }
    }

    @Async
    @Transactional
    fun requestBuildPrivacyCentersForOrgAsync(organizationId: UUID, source: String) {
        val organization = lookupService.orgOrThrow(organizationId)
        requestBuildPrivacyCentersForOrg(organization, source, throttle = true)
    }

    fun requestBuildAllPrivacyCenters(source: String) {
        val allPrivacyCenters = privacyCenterRepository.findAll()
        allPrivacyCenters.forEach { insertPrivacyCenterBuildRequest(it, source) }
    }
}
