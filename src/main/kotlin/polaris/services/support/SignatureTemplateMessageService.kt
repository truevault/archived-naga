package polaris.services.support

import org.springframework.stereotype.Service
import polaris.models.entity.organization.Organization
import polaris.models.entity.user.User

@Service
class SignatureTemplateMessageService(optOutService: OptOutService) : TemplateMessageService(optOutService) {
    fun organizationSignatureMessageTemplate(organization: Organization, actor: User? = null): String {

        return renderTemplate(
            organization.getSignature(),
            OrganizationSignatureTemplateData(
                organization = organizationToTemplateData(organization),
                user = if (actor == null) organizationToUserTemplateData(organization) else userToTemplateData(actor)
            )
        )
    }
}
