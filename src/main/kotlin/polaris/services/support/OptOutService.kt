package polaris.services.support

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.SurveySlugs.Companion.SELL_IN_EXCHANGE_FOR_MONEY_SLUG
import polaris.models.entity.organization.OptOutPageType
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.models.entity.organization.SELLING_AND_SHARING_SURVEY_NAME
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.SurveyService

data class OptOutAttributes(
    val anySelling: Boolean,
    val anySharing: Boolean,
    val anyDirectShare: Boolean,
    val anyCustomAudience: Boolean,
    val isSellingInExchangeForMoney: Boolean
)

@Service
class OptOutService(
    private val surveyService: SurveyService,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(OptOutService::class.java)
    }

    fun getOptOutPageType(org: Organization): OptOutPageType {
        val optOutAttributes = getOptOutAttributes(org)

        if (!optOutAttributes.anySelling && !optOutAttributes.anySharing && !optOutAttributes.isSellingInExchangeForMoney && !optOutAttributes.anyCustomAudience) {
            return OptOutPageType.NONE
        }

        if (!optOutAttributes.anySelling && !optOutAttributes.isSellingInExchangeForMoney && (optOutAttributes.anySharing || optOutAttributes.anyCustomAudience)) {
            return OptOutPageType.SHARING
        }

        if ((optOutAttributes.anySelling || optOutAttributes.isSellingInExchangeForMoney) && !(optOutAttributes.anySharing || optOutAttributes.anyCustomAudience)) {
            return OptOutPageType.SELLING
        }

        return OptOutPageType.SELLING_AND_SHARING
    }

    fun getOptOutPageLabel(privacyCenter: PrivacyCenter?): String? {
        if (privacyCenter == null) {
            return null
        }
        val type = getOptOutPageType(privacyCenter.organization!!)
        return getOptOutPageLabel(type)
    }

    fun getOptOutPageLabel(pageType: OptOutPageType): String? {
        return when (pageType) {
            OptOutPageType.NONE -> null
            OptOutPageType.SELLING -> "Do Not Sell"
            OptOutPageType.SHARING -> "Do Not Share"
            OptOutPageType.SELLING_AND_SHARING -> "Do Not Sell Or Share"
        }
    }

    fun getOptOutUrl(privacyCenter: PrivacyCenter?): String? {
        if (privacyCenter == null) {
            return null
        }
        val type = getOptOutPageType(privacyCenter.organization!!)
        val path = getOptOutPath(type)
        return path?.let { privacyCenter.getUrl(it) }
    }

    fun getOptOutPath(pageType: OptOutPageType): String? {
        return when (pageType) {
            OptOutPageType.NONE -> null
            OptOutPageType.SELLING -> "/opt-out"
            OptOutPageType.SHARING -> "/opt-out"
            OptOutPageType.SELLING_AND_SHARING -> "/opt-out"
        }
    }

    fun getOptOutAttributes(org: Organization) = getOptOutAttributes(org, org.organizationDataRecipients)

    fun getOptOutAttributes(org: Organization, orgVendors: List<OrganizationDataRecipient>): OptOutAttributes {
        // I *think* that we want to update anySelling to have isSellingInExchangeForMoney included
        // And then add an only basic selling option. It seems like most places want both anySelling and inExchangeFormMoney
        // and anySelling is a bit of a misleading name
        val anySelling =
            orgVendors.any { it.ccpaIsSelling && it.vendorClassification?.slug != VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER }
        val anySharing = orgVendors.any { it.ccpaIsSharing || it.usesCustomAudience }
        val anyDirectSharing = orgVendors.any { it.ccpaIsSharing } // TODO Maybe this is upload?
        val anyCustomAudience = orgVendors.any { it.usesCustomAudience }
        val isSellingInExchangeForMoney = surveyService.getAnswer(
            org,
            SELLING_AND_SHARING_SURVEY_NAME,
            SELL_IN_EXCHANGE_FOR_MONEY_SLUG
        ) == "true"

        return OptOutAttributes(
            anySelling = anySelling,
            anySharing = anySharing,
            anyDirectShare = anyDirectSharing,
            anyCustomAudience = anyCustomAudience,
            isSellingInExchangeForMoney = isSellingInExchangeForMoney
        )
    }

    fun processesOptOut(org: Organization): Boolean {
        return hasDirectTransmissionSellingDataRecipients(org) || hasCustomAudienceSharingDataRecipients(org)
    }

    fun hasDirectTransmissionSellingDataRecipients(org: Organization): Boolean {
        val dataRecipients = org.organizationDataRecipients

        val directSellingQuestion = getDirectSellingQuestion(org)
        val directSellingVendorIds = directSellingQuestion?.answer?.let { parseDirectSellingAnswer(it) } ?: emptyList()
        val directSellingRecipients =
            dataRecipients.filter { it.ccpaIsSelling && directSellingVendorIds.contains(it.id.toString()) }
        return directSellingRecipients.isNotEmpty()
    }

    fun hasCustomAudienceSharingDataRecipients(org: Organization): Boolean {
        val dataRecipients = org.organizationDataRecipients
        return dataRecipients.any { it.usesCustomAudience }
    }

    private fun parseDirectSellingAnswer(answer: String): List<String> {
        return try {
            jacksonObjectMapper().readValue(answer)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (parseDirectSellingAnswer): ", e)
            emptyList()
        }
    }

    private fun getDirectSellingQuestion(org: Organization): OrganizationSurveyQuestion? {
        return organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(
            org,
            "selling-and-sharing-survey",
            "vendor-upload-data-selection"
        )
    }

    private fun getCustomAudiencesQuestion(org: Organization): OrganizationSurveyQuestion? {
        return organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(
            org,
            "selling-and-sharing-survey",
            "custom-audience-feature"
        )
    }
}
