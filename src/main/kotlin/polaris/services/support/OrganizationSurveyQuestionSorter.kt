package polaris.services.support

import polaris.models.entity.organization.OrganizationSurveyQuestion

object OrganizationSurveyQuestionSorter {
    private val SLUG_ORDER = mapOf(
        "intro-survey" to listOf(
            "provide-products-and-or-services",
            "individual-consumers-and-or-organizations",
            "products-individual-consumers-and-or-organizations",
            "services-individual-consumers-and-or-organizations",
            "individuals-buy-both",
            "organizations-buy-both",
            "consumer-group-individual-products",
            "consumer-group-organization-products",
            "consumer-group-individual-services",
            "consumer-group-organization-services",
            "consumer-group-individual-products-and-services",
            "consumer-group-organization-products-and-services",
            "business-website-personal-information",
            "business-website-privacy-policy",
            "business-web-application",
            "business-physical-location",
            "business-employees-ca",
            "business-contractors-ca",
            "business-job-applicants-ca",
            "business-hire-or-plan-to-ca",
            "business-employee-info-hiring-only",
            "business-promotional-newsletters",
            "business-collect-info-from-about-prospects",
            "business-buy-from-data-brokers",
            "business-share-info-under-16",
            "business-share-info-under-13",
            "business-provide-info-non-english",
            "business-financial-incentive-collection",
            "offer-consumer-incentives",
            "business-collect-sell-share-10-million",
            "business-collect-mobile-not-expected",
            "business-service-provider"
        )
    )

    fun sortedSurveyQuestions(questions: List<OrganizationSurveyQuestion>) = questions.sortedBy {
        SLUG_ORDER.getOrDefault(it.surveyName, emptyList()).indexOf(it.slug)
    }
}
