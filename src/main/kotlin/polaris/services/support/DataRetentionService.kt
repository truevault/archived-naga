package polaris.services.support

import org.springframework.stereotype.Service
import polaris.models.dto.organization.UpdateDataRetentionPolicyDetailDto
import polaris.models.entity.organization.DataRetentionPolicyDetail
import polaris.models.entity.organization.DataRetentionPolicyDetailCategory
import polaris.models.entity.organization.Organization
import polaris.repositories.DataRetentionPolicyDetailRepository
import polaris.repositories.PersonalInformationCategoryRepository
import java.util.UUID

@Service
class DataRetentionService(
    private val dataRetentionPolicyDetailRepo: DataRetentionPolicyDetailRepository,
    private val picRepo: PersonalInformationCategoryRepository
) {
    fun getDataRetentionDetails(org: Organization): List<DataRetentionPolicyDetail> {
        return dataRetentionPolicyDetailRepo.findAllByOrganization(org)
    }

    fun updateDataRetentionCategory(org: Organization, category: DataRetentionPolicyDetailCategory, dto: UpdateDataRetentionPolicyDetailDto) {
        val existing = dataRetentionPolicyDetailRepo.findByOrganizationAndType(org, category) ?: DataRetentionPolicyDetail(organizationId = org.id, type = category, pic = mutableListOf(), description = null)

        existing.pic = picRepo.findAllById(dto.pic.map { UUID.fromString(it) })
        existing.description = dto.description

        dataRetentionPolicyDetailRepo.save(existing)
    }
}
