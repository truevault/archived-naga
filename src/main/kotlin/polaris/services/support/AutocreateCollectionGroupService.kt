package polaris.services.support

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.feature.Features
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.HR_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.hasTrueAnswer
import polaris.models.entity.vendor.ProcessingRegion
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.OrganizationRepository
import polaris.services.primary.OrganizationService

@Service
@Transactional
class AutocreateCollectionGroupService(
    private val cgRepo: CollectionGroupRepository,
    private val orgRepo: OrganizationRepository,
    private val organizationService: OrganizationService,
    private val lookup: LookupService
) {
    fun createDefaultCollectionGroups(organizationId: OrganizationPublicId) {
        val org = lookup.orgOrThrow(organizationId)

        // Determine the default groups to create
        val all = getAutocreateGroupsForOrg(org)

        // Check which groups already exist
        val existing = org.collectionGroups.mapNotNull { it.autocreationSlug }.toSet()

        // Check which groups we have already auto-created
        val alreadyCreated = org.autocreatedCollectionGroupSlugs.toSet()

        // Determine the groups to create
        val toCreate = ((all - existing) - alreadyCreated)

        // Create the groups
        val groups = toCreate.map { constructAutocreateGroup(it, org) }
        cgRepo.saveAll(groups)

        // Update the org's autocreated slugs
        org.autocreatedCollectionGroupSlugs = (org.autocreatedCollectionGroupSlugs + toCreate).distinct()
        orgRepo.save(org)
    }

    fun getAutocreateGroupsForOrg(org: Organization): Set<AutocreatedCollectionGroupSlug> {
        // now do the front-end ui thing:
        val groups = mutableSetOf<AutocreatedCollectionGroupSlug>()

        // everyone gets online shoppers
        groups.add(AutocreatedCollectionGroupSlug.ONLINE_SHOPPERS)

        val hrSurvey = organizationService.getSurveyQuestions(org.publicId, HR_SURVEY_NAME)

        // CA employment groups based on survey responses
        if (hrSurvey.hasTrueAnswer("business-employees-ca")) {
            groups.add(AutocreatedCollectionGroupSlug.CA_EMPLOYEES)
        }
        if (hrSurvey.hasTrueAnswer("business-contractors-ca")) {
            groups.add(AutocreatedCollectionGroupSlug.CA_CONTRACTORS)
        }
        if (hrSurvey.hasTrueAnswer("business-job-applicants-ca")) {
            groups.add(AutocreatedCollectionGroupSlug.CA_JOB_APPLICANTS)
        }

        if (lookup.orgFeatureOrDefault(org, Features.GDPR).enabled) {
            // EEA UK employee groups based on feature gdpr & has employees
            if (hrSurvey.hasTrueAnswer("employees-eea-uk")) {
                groups.add(AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE)
            }
        }

        return groups
    }

    fun constructAutocreateGroup(slug: AutocreatedCollectionGroupSlug, org: Organization): CollectionGroup {
        return CollectionGroup(name = slug.defaultLabel, description = slug.description, organization = org, autocreationSlug = slug, collectionGroupType = slug.type, processingRegions = slug.processingRegions, createdAsDataSubject = slug.processingRegions.size == 1 && slug.processingRegions.contains(ProcessingRegion.EEA_UK))
    }
}
