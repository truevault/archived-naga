package polaris.services.support

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.controllers.api.datainventory.UpdateCollectionRequest
import polaris.controllers.api.datainventory.UpdateDisclosureRequest
import polaris.controllers.api.datainventory.UpdateReceivedRequest
import polaris.controllers.api.datainventory.UpdateRecipientAssocationRequest
import polaris.controllers.api.datainventory.UpdateSourceAssocationRequest
import polaris.controllers.util.LookupService
import polaris.models.dto.compliance.CollectionDto
import polaris.models.dto.compliance.DMCollectionGroupDto
import polaris.models.dto.compliance.DMDataRecipientDto
import polaris.models.dto.compliance.DMDataSourceDto
import polaris.models.dto.compliance.DataMapOperation
import polaris.models.dto.compliance.DataMapTarget
import polaris.models.dto.compliance.DataMapUpdate
import polaris.models.dto.compliance.DisclosureDto
import polaris.models.dto.compliance.FullDataMapDto
import polaris.models.dto.compliance.PersonalInformationCategoryDto
import polaris.models.dto.compliance.SourceDto
import polaris.models.dto.compliance.V2CollectionDto
import polaris.models.dto.compliance.V2DataMapDto
import polaris.models.dto.compliance.V2DisclosureDto
import polaris.models.dto.compliance.V2ReceivedDto
import polaris.models.dto.compliance.V2RecipientAssociationDto
import polaris.models.dto.compliance.V2SourceAssociationDto
import polaris.models.entity.CollectionGroupNotFound
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.compliance.PICGroup
import polaris.models.entity.compliance.PersonalInformationCategory
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.CollectionGroupCollectedPIC
import polaris.models.entity.organization.DataRecipientCollectionGroup
import polaris.models.entity.organization.DataRecipientDisclosedPIC
import polaris.models.entity.organization.DataSourceCollectionGroup
import polaris.models.entity.organization.DataSourceReceivedPIC
import polaris.models.entity.organization.DoNotContactReason
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.SELLING_AND_SHARING_SURVEY_NAME
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod
import polaris.models.entity.vendor.Vendor
import polaris.repositories.*
import polaris.services.primary.SurveyService
import polaris.util.sorting.PICSort
import java.util.UUID

@Service
class DataMapService(
    private val requestHandlingInstructionsRepository: RequestHandlingInstructionsRepository,
    private val retentionReasonRepository: RetentionReasonRepository,
    private val collectionGroupRepo: CollectionGroupRepository,

    private val dataRecipientCollectionGroupRepo: DataRecipientCollectionGroupRepo,
    private val dataSourceAssociationRepo: DataSourceCollectionGroupRepo,
    private val organizationDataRecipientRepository: OrganizationDataRecipientRepository,

    private val disclosedPICRepo: DataRecipientDisclosedPICRepo,
    private val receivedPICRepo: DataSourceReceivedPICRepo,
    private val collectedPICRepo: CollectionGroupCollectedPICRepo,
    private val picRepo: PersonalInformationCategoryRepository,
    private val picGroupRepo: PICGroupRepo,
    private val orgDataSourceRepo: OrganizationDataSourceRepository,
    private val vendorRepository: VendorRepository,
    private val lookup: LookupService,

    private val surveyService: SurveyService
) {

    companion object {
        private val logger = LoggerFactory.getLogger(DataMapService::class.java)
    }

    fun getDataMap(organization: Organization): V2DataMapDto {
        val fullMap = getFullDataMap(organization)
        logger.info("FULL MAP RECEIVED: ${fullMap.received}")
        return V2DataMapDto(
            categories = fullMap.categories,
            collectionGroups = fullMap.collectionGroups,
            recipients = fullMap.recipients,
            sources = fullMap.sources,
            collection = fullMap.collection.flatMap { c -> c.collected.map { pic -> V2CollectionDto(c.collectionGroupId, pic) } },
            disclosure = fullMap.disclosure.flatMap { d -> d.disclosed.map { pic -> V2DisclosureDto(d.collectionGroupId, d.vendorId, pic) } },
            received = fullMap.received.flatMap { r -> r.received.map { pic -> V2ReceivedDto(r.collectionGroupId, r.vendorId, pic) } },
            recipientAssociations = fullMap.disclosure.map { V2RecipientAssociationDto(it.collectionGroupId, it.vendorId) },
            sourceAssociations = fullMap.received.map { V2SourceAssociationDto(it.collectionGroupId, it.vendorId) }
        )
    }

    fun getAllCollectedPic(org: Organization): List<CollectionGroupCollectedPIC> {
        val allCollectionGroups = collectionGroupRepo.findAllByOrganization(org)
        return getAllCollectedPic(allCollectionGroups)
    }

    fun getAllCollectedPic(collectionGroups: List<CollectionGroup>): List<CollectionGroupCollectedPIC> {
        val allPic = picRepo.findAllByDeprecated(false)
        val categories = allPic.map { it.toCategoryDto() }.associateBy { it.id }

        return collectedPICRepo.findAllByCollectionGroupIn(collectionGroups)
            .filter { categories.containsKey(it.pic?.id?.toString()) }
    }

    fun getUnmappedDataRecipients(
        organization: Organization,
        collectionGroup: CollectionGroup
    ): List<OrganizationDataRecipient> {
        val allCollectedPIC = getAllCollectedPic(listOf(collectionGroup))
        val allDisclosedPIC = allCollectedPIC.flatMap { it.disclosedToVendors }

        val recipientAssociations = dataRecipientCollectionGroupRepo.findAllByCollectionGroupIn(listOf(collectionGroup))
        val allAssociatedVendors =
            recipientAssociations.filter { it.noDataDisclosure != true }.mapNotNull { it.vendor }.distinct()
        val allDisclosedVendors = allDisclosedPIC.map { it.vendor }.distinct()

        val unmappedVendors = allAssociatedVendors.filter { !allDisclosedVendors.contains(it) }

        return organizationDataRecipientRepository.findAllByOrganizationAndVendorIn(organization, unmappedVendors)
    }

    fun getFullDataMap(organization: Organization): FullDataMapDto {
        val allPic = picRepo.findAllByDeprecated(false).sortedWith(PICSort)
        val allPicGroups = allPic.map { it.group!! }
        val allCollectionGroups = collectionGroupRepo.findAllByOrganization(organization)

        val categories = allPic.map { it.toCategoryDto() }.associateBy { it.id }

        val allCollectedPIC = getAllCollectedPic(allCollectionGroups)
        val allDisclosedPIC = allCollectedPIC.flatMap { it.disclosedToVendors }
        val allReceivedPIC = allCollectedPIC.flatMap { it.receivedFromSources }

        val recipientAssociations = dataRecipientCollectionGroupRepo.findAllByCollectionGroupIn(allCollectionGroups)
        val allAssociatedRecipients = recipientAssociations.mapNotNull { it.vendor }.distinct()
        val allDisclosedRecipients = allDisclosedPIC.map { it.vendor }.distinct()
        val allRecipients = allAssociatedRecipients.union(allDisclosedRecipients).filterNotNull().distinct()

        val sourceAssocations = dataSourceAssociationRepo.findAllByCollectionGroupIn(allCollectionGroups)
        val allAssociatedSources = sourceAssocations.mapNotNull { it.vendor }.distinct()
        val allDisclosingSources = allReceivedPIC.map { it.vendor }.distinct()
        val allSources = allAssociatedSources.union(allDisclosingSources).filterNotNull().distinct()

        logger.info("all cg: $allCollectionGroups")
        logger.info("source associations: $allAssociatedSources")
        logger.info("all disclosing: $allDisclosingSources")
        logger.info("all **associated** sources: $allSources")

        val orgSources = orgDataSourceRepo.findAllByOrganization(organization)

        val collectedByGroup = allCollectedPIC.groupBy { it.collectionGroup!! }
        val collection = collectedByGroup.map { (cg, c) ->
            CollectionDto(
                collectionGroupId = cg.id.toString(),
                collectionGroupName = cg.name,
                collectionGroupDescription = cg.description,
                collected = c.map { it.pic!!.id.toString() },
                collectionGroupType = cg.collectionGroupType
            )
        }

        val disclosureByVendor = allDisclosedPIC.groupBy { it.vendor }
        val associatedGroupsByRecipient = recipientAssociations.groupBy { it.vendor!! }
            .mapValues { it.value.map { l -> l.collectionGroup!! }.toSet() }

        val receivedByVendor = allReceivedPIC.groupBy { it.vendor }
        val associatedGroupsBySource =
            sourceAssocations.groupBy { it.vendor!! }.mapValues { it.value.map { l -> l.collectionGroup!! }.toSet() }

        val disclosure = allRecipients.flatMap { recipient ->
            val disclosedForVendor = disclosureByVendor.getOrDefault(recipient, emptyList())
            val disclosedForVendorByCollectionGroup = disclosedForVendor.groupBy { it.collectedPIC!!.collectionGroup!! }

            val disclosedGroups = disclosedForVendor.map { it.collectedPIC!!.collectionGroup!! }.toSet()
            val associatedGroups = associatedGroupsByRecipient.getOrDefault(recipient, emptySet())

            val relatedGroups = disclosedGroups.union(associatedGroups)

            relatedGroups.map { cg ->

                val disc = disclosedForVendorByCollectionGroup.getOrDefault(cg, emptyList())
                    .map { it.collectedPIC!!.pic!!.id.toString() }

                DisclosureDto(
                    collectionGroupId = cg.id.toString(),
                    collectionGroupName = cg.name,
                    collectionGroupDescription = cg.description,
                    collectionGroupType = cg.collectionGroupType,
                    vendorId = recipient.id.toString(),
                    vendorName = recipient.name!!,
                    vendorCategory = recipient.disclosureRecipientCategory(),
                    disclosed = disc
                )
            }
        }

        val received = allSources.flatMap { source ->
            val receivedFromVendor = receivedByVendor.getOrDefault(source, emptyList())
            val receivedForVendorByCollectionGroup = receivedFromVendor.groupBy { it.collectedPIC!!.collectionGroup!! }

            val receivedGroups = receivedFromVendor.map { it.collectedPIC!!.collectionGroup!! }.toSet()
            val associatedGroups = associatedGroupsBySource.getOrDefault(source, emptySet())

            val relatedGroups = receivedGroups.union(associatedGroups)

            relatedGroups.map { cg ->
                val r = receivedForVendorByCollectionGroup.getOrDefault(cg, emptyList())
                    .map { it.collectedPIC!!.pic!!.id.toString() }

                SourceDto(
                    collectionGroupId = cg.id.toString(),
                    collectionGroupName = cg.name,
                    collectionGroupDescription = cg.description,
                    vendorId = source.id.toString(),
                    vendorName = source.name!!,
                    vendorCategory = source.disclosureSourceCategory(),
                    received = r,
                    displayOrder = if (source.organization != null) 99 else 0
                )
            }
        }

        logger.info("Received (post-map): $received")

        return FullDataMapDto(
            categories = categories,
            picGroups = allPicGroups.map { it.toPicGroupDto() }.associateBy { it.id },
            collectionGroups = allCollectionGroups.map {
                DMCollectionGroupDto(
                    id = it.id.toString(),
                    name = it.name,
                    description = it.description,
                    type = it.collectionGroupType
                )
            }.associateBy { it.id },
            recipients = allRecipients.filter { it.name != null && it.recipientCategory != null }.map {
                DMDataRecipientDto(
                    vendorId = it.id.toString(),
                    name = it.name!!,
                    category = it.recipientCategory!!
                )
            }.associateBy { it.vendorId },
            sources = orgSources.map {
                DMDataSourceDto(
                    vendorId = it.vendor!!.id.toString(),
                    name = it.vendor.name!!,
                    category = it.vendor.disclosureSourceCategory(),
                    context = it.collectionContexts,
                    displayOrder = if (it.vendor.organization != null) 99 else 0
                )
            }.associateBy { it.vendorId },
            collection = collection,
            disclosure = disclosure,
            received = received
        )
    }

    fun disclosureForDataRecipient(dataRecipient: OrganizationDataRecipient): List<DataRecipientDisclosedPIC> {
        val organization = dataRecipient.organization!!

        val allCollectionGroups = collectionGroupRepo.findAllByOrganization(organization)

        val allCollectedPIC = getAllCollectedPic(allCollectionGroups)
        val allDisclosedPIC = allCollectedPIC.flatMap { it.disclosedToVendors }

        return allDisclosedPIC.filter { it.vendor == dataRecipient.vendor }
    }

    fun collectionGroupCollectsPic(collectionGroup: CollectionGroup): Boolean {
        return collectionGroup.collectedPIC.isNotEmpty()
    }

    fun collectionGroupDisclosesPic(collectionGroup: CollectionGroup): Boolean {
        return collectionGroup.collectedPIC.any { it.disclosedToVendors.isNotEmpty() }
    }

    fun collectionGroupReceivesPic(collectionGroup: CollectionGroup): Boolean {
        return collectionGroup.collectedPIC.any { it.receivedFromSources.isNotEmpty() }
    }

    fun getPersonalInfoCategories(organization: Organization): List<PersonalInformationCategoryDto> {
        val all = picRepo.findAll()
        val collected = collectedPICRepo.findAllByCollectionGroup_Organization(organization)
        val collectedIds = collected.map { it.pic!!.id }
        val exchangedIds = collected.filter { it.disclosedToVendors.isNotEmpty() }.map { it.pic!!.id }

        return all.filter { !it.deprecated }.map {
            it.toDto(
                collected = collectedIds.contains(it.id),
                exchanged = exchangedIds.contains(it.id)
            )
        }
    }

    fun updateDataMap(org: Organization, operations: List<DataMapOperation>) {
        // 1 - validate all operations have necessary data
        operations.forEach { it.validate() }

        // 2 - fetch all relevant objects for the given updates
        val picIds = operations.mapNotNull { it.picId }.map { UUID.fromString(it) }
        val pic = picRepo.findAllByIdIn(picIds)

        val picGroupIds = operations.mapNotNull { it.picGroupId }.map { UUID.fromString(it) }
        val picGroups = picGroupRepo.findAllByIdIn(picGroupIds)

        val groups = collectionGroupRepo.findAllByOrganization(org)

        val vendorIds = operations.mapNotNull { it.vendorId }.map { UUID.fromString(it) }
        val vendors = vendorRepository.findAllByIdIn(vendorIds)

        // 3 - iterate through the updates applying them
        operations.forEach { op ->
            when (op.target) {
                DataMapTarget.COLLECTION -> updateCollection(
                    org,
                    pic.find { it.id == UUID.fromString(op.picId) }!!,
                    groups.find { it.id == UUID.fromString(op.groupId) }!!,
                    UpdateCollectionRequest(collected = op.update == DataMapUpdate.ADD)
                )
                DataMapTarget.RECIPIENT_ASSOCIATION -> updateRecipientAssociation(
                    org,
                    vendors.find { it.id == UUID.fromString(op.vendorId) }!!,
                    groups.find { it.id == UUID.fromString(op.groupId) }!!,
                    UpdateRecipientAssocationRequest(associated = op.update == DataMapUpdate.ADD, updateDisclosure = true)
                )
                DataMapTarget.SOURCE_ASSOCIATION -> updateSourceAssociation(
                    org,
                    vendors.find { it.id == UUID.fromString(op.vendorId) }!!,
                    groups.find { it.id == UUID.fromString(op.groupId) }!!,
                    UpdateSourceAssocationRequest(associated = op.update == DataMapUpdate.ADD, updateReceived = true)
                )
                DataMapTarget.RECEIVED -> updateReceived(
                    org,
                    vendors.find { it.id == UUID.fromString(op.vendorId) }!!,
                    groups.find { it.id == UUID.fromString(op.groupId) }!!,
                    pic.find { it.id == UUID.fromString(op.picId) }!!,
                    UpdateReceivedRequest(received = op.update == DataMapUpdate.ADD, updateCollection = true)
                )
                DataMapTarget.DISCLOSURE -> updateDisclosure(
                    org,
                    vendors.find { it.id == UUID.fromString(op.vendorId) }!!,
                    groups.find { it.id == UUID.fromString(op.groupId) }!!,
                    picGroups.find { it.id == UUID.fromString(op.picId) }!!,
                    UpdateDisclosureRequest(disclosed = op.update == DataMapUpdate.ADD, updateCollection = true)
                )
            }
        }
    }

    fun updateCollection(
        organization: Organization,
        pic: PersonalInformationCategory,
        cg: CollectionGroup,
        update: UpdateCollectionRequest
    ) {
        if (cg.organization != organization) {
            throw CollectionGroupNotFound(cg.id.toString())
        }

        val existing = collectedPICRepo.findByCollectionGroupAndPic(cg, pic)

        if (update.collected && existing == null) {
            collectedPICRepo.save(CollectionGroupCollectedPIC(collectionGroup = cg, pic = pic))
        }

        if (!update.collected && existing != null) {
            disclosedPICRepo.deleteAll(existing.disclosedToVendors)
            receivedPICRepo.deleteAll(existing.receivedFromSources)
            collectedPICRepo.delete(existing)
        }
    }

    fun updateRecipientAssociation(
        organization: Organization,
        vendor: Vendor,
        cg: CollectionGroup,
        update: UpdateRecipientAssocationRequest
    ) {
        if (cg.organization != organization) {
            throw CollectionGroupNotFound(cg.id.toString())
        }

        val existing = dataRecipientCollectionGroupRepo.findByVendorIdAndCollectionGroupId(vendor.id, cg.id)

        if (update.associated && existing == null) {
            dataRecipientCollectionGroupRepo.save(
                DataRecipientCollectionGroup(
                    collectionGroupId = cg.id,
                    vendorId = vendor.id
                )
            )
        }

        if (!update.associated) {
            if (update.updateDisclosure != null && update.updateDisclosure) {
                val all = disclosedPICRepo.findAllByCollectedPIC_CollectionGroupAndVendor(cg, vendor)
                disclosedPICRepo.deleteAll(all)
            }

            if (existing != null) {
                dataRecipientCollectionGroupRepo.delete(existing)
            }
        }
    }

    fun updateSourceAssociation(
        organization: Organization,
        vendor: Vendor,
        cg: CollectionGroup,
        update: UpdateSourceAssocationRequest
    ) {
        if (cg.organization != organization) {
            throw CollectionGroupNotFound(cg.id.toString())
        }

        val existing = dataSourceAssociationRepo.findByVendorIdAndCollectionGroupId(vendor.id, cg.id)

        if (update.associated && existing == null) {
            dataSourceAssociationRepo.save(DataSourceCollectionGroup(collectionGroupId = cg.id, vendorId = vendor.id))
        }

        if (!update.associated) {
            if (update.updateReceived != null && update.updateReceived) {
                val all = receivedPICRepo.findAllByCollectedPIC_CollectionGroupAndVendor(cg, vendor)
                receivedPICRepo.deleteAll(all)
            }

            if (existing != null) {
                dataSourceAssociationRepo.delete(existing)
            }
        }
    }

    fun updateDisclosure(
        organization: Organization,
        vendor: Vendor,
        cg: CollectionGroup,
        picGroup: PICGroup,
        update: UpdateDisclosureRequest
    ) {
        if (cg.organization != organization) {
            logger.info("organization's did not match")
            throw CollectionGroupNotFound(cg.id.toString())
        }

        // update the vendor to include the context as the collection group
        if (update.disclosed) {
            val context = cg.collectionGroupType.context()
            val dataRecipient = organization.organizationDataRecipients.find { it.vendor?.id == vendor.id }
            if (dataRecipient != null && !dataRecipient.collectionContexts.contains(context)) {
                val existingContexts = dataRecipient.collectionContexts.toMutableList()
                existingContexts.add(context)
                dataRecipient.collectionContexts = existingContexts
                organizationDataRecipientRepository.save(dataRecipient)
            }
        }

        // get collection for the collection group
        var collected = collectedPICRepo.findAllByCollectionGroup(cg)
        if (update.disclosed && update.updateCollection) {
            val all = picRepo.findAllByGroup(picGroup)
            val toCollect = all - collected.map { it.pic }.toSet()
            val newCollections = toCollect.map { CollectionGroupCollectedPIC(collectionGroup = cg, pic = it) }
            val saved = collectedPICRepo.saveAll(newCollections)
            collected = collected + saved
        }

        val collectedForGroup = collected.filter { it.pic!!.group == picGroup }.toSet()

        if (update.disclosed) {
            // ensure disclosures exist for each PIC
            val disclosedCollections =
                disclosedPICRepo.findAllByCollectedPIC_CollectionGroupAndVendor(cg, vendor).map { it.collectedPIC!! }
                    .toSet()
            val toAdd = collectedForGroup - disclosedCollections
            val disclosures = toAdd.map { DataRecipientDisclosedPIC(collectedPIC = it, vendor = vendor) }
            disclosedPICRepo.saveAll(disclosures)
        } else {
            // remove any existing disclosures for each PIC
            val disclosedCollections = disclosedPICRepo.findAllByCollectedPIC_CollectionGroupAndVendor(cg, vendor)
                .filter { collectedForGroup.contains(it.collectedPIC!!) }
            disclosedPICRepo.deleteAll(disclosedCollections)
        }
    }

    fun updateReceived(
        organization: Organization,
        vendor: Vendor,
        cg: CollectionGroup,
        pic: PersonalInformationCategory,
        update: UpdateReceivedRequest
    ) {
        if (cg.organization != organization) {
            logger.info("organization's did not match")
            throw CollectionGroupNotFound(cg.id.toString())
        }

        // update the vendor to include the context as the collection group
        if (update.received) {
            val context = cg.collectionGroupType.context()
            val dataSource = orgDataSourceRepo.findByOrganizationAndVendor(organization, vendor)
            if (dataSource != null && !dataSource.collectionContexts.contains(context)) {
                val existingContexts = dataSource.collectionContexts.toMutableList()
                existingContexts.add(context)
                dataSource.collectionContexts = existingContexts
                orgDataSourceRepo.save(dataSource)
            }
        }

        // get collection for the collection group
        var collected = collectedPICRepo.findAllByCollectionGroup(cg)
        if (update.received && update.updateCollection) {
            val all = setOf(pic)
            val toCollect = all - collected.map { it.pic }.toSet()
            val newCollections = toCollect.map { CollectionGroupCollectedPIC(collectionGroup = cg, pic = it) }
            val saved = collectedPICRepo.saveAll(newCollections)
            collected = collected + saved
        }

        val collectedForGroup = collected.filter { it.pic!! == pic }.toSet()

        if (update.received) {
            // ensure disclosures exist for each PIC
            val receivedCollections =
                receivedPICRepo.findAllByCollectedPIC_CollectionGroupAndVendor(cg, vendor).map { it.collectedPIC!! }
                    .toSet()
            val toAdd = collectedForGroup - receivedCollections
            val received = toAdd.map { DataSourceReceivedPIC(collectedPIC = it, vendor = vendor) }
            receivedPICRepo.saveAll(received)
        } else {
            // remove any existing disclosures for each PIC
            val receivedCollections = receivedPICRepo.findAllByCollectedPIC_CollectionGroupAndVendor(cg, vendor)
                .filter { collectedForGroup.contains(it.collectedPIC!!) }
            receivedPICRepo.deleteAll(receivedCollections)
        }
    }

    fun collectedPIC(
        organization: Organization,
        removeEmploymentGroups: Boolean = false
    ): List<PersonalInformationCategory> {
        val collected = collectedPICRepo.findAllByCollectionGroup_Organization(organization)
        return collected.filter {
            !removeEmploymentGroups || it.collectionGroup?.collectionGroupType != CollectionGroupType.EMPLOYMENT
        }.mapNotNull { it.pic }.distinct()
    }

    fun orgVendorsForConsumerGroup(
        org: Organization,
        consumerGroup: CollectionGroup,
        includeExplicitNoMapping: Boolean
    ): List<OrganizationDataRecipient> {
        val relevantospic = disclosedPICRepo.findForOrganizationAndConsumerGroups(org, listOf(consumerGroup))
        val explicitNoMappingVendors =
            if (includeExplicitNoMapping) dataRecipientCollectionGroupRepo.findAllByCollectionGroupIdAndNoDataDisclosure(
                consumerGroup.id,
                true
            ) else emptyList()
        val relevantVendors =
            (relevantospic.mapNotNull { it.vendor } + explicitNoMappingVendors.mapNotNull { it.vendor }).distinct()
        val orgVendors = organizationDataRecipientRepository.findAllByOrganization(org).toMutableList()

        orgVendors.retainAll { relevantVendors.contains(it.vendor) }
        return orgVendors
    }

    fun retentionReasons(organization: Organization?, instructionType: DataRequestInstructionType?): List<String> {
        logger.info("Getting retention reasons for org: ${organization?.name} and instructionType: $instructionType}")

        if (organization != null && instructionType != null) {
            val instructions = requestHandlingInstructionsRepository.findAllByOrganizationAndRequestType(
                organization, instructionType
            )

            val reasonSlugs = instructions
                .filter { it.processingMethod == ProcessingMethod.RETAIN }
                .flatMap { it.retentionReasons }
                .distinct()

            val retentionReasons = retentionReasonRepository.findAllBySlugIn(reasonSlugs)

            return retentionReasons.map { it.reason }
        }

        return emptyList()
    }

    fun noContactReasons(organization: Organization?, instructionType: DataRequestInstructionType?): List<String> {
        if (organization != null && instructionType != null) {
            val instructions = requestHandlingInstructionsRepository.findAllByOrganizationAndRequestType(
                organization, instructionType
            )

            return instructions
                .filter { it.deleteDoNotContact == true }
                .filter { it.doNotContactReason == DoNotContactReason.IMPOSSIBLE_OR_DIFFICULT }
                .mapNotNull { it.doNotContactExplanation }
        }

        return emptyList()
    }

    fun intentionalInteractionVendors(organization: Organization?): List<String> {
        if (organization != null) {
            val orgVendors = organizationDataRecipientRepository.findAllByOrganization(organization).toMutableList()
            val answer = surveyService.getAnswer(
                organization,
                SELLING_AND_SHARING_SURVEY_NAME,
                "vendor-intentional-disclosure-selection"
            ) ?: return emptyList()
            val intentionalRecipientIds: List<String> = try {
                jacksonObjectMapper().readValue(answer)
            } catch (e: JsonProcessingException) {
                logger.info("Had a json processing exception (intentionalInteractionVendors): ", e)
                emptyList()
            }
            val intentionalRecipients = orgVendors.filter {
                it.vendor != null && intentionalRecipientIds.contains(it.id.toString())
            }
            return intentionalRecipients.map { it.vendor!!.name }
        }

        return emptyList()
    }
}
