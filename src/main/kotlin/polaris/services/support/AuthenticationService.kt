package polaris.services.support

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.context.WebApplicationContext
import polaris.models.entity.user.User
import polaris.services.primary.UserService
import java.util.*

@Service
@Transactional
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
class AuthenticationService {

    @Autowired
    private lateinit var userDetailsService: UserDetailsService

    @Autowired
    private lateinit var userService: UserService

    private var cachedCurrentUserDetails: UserDetails? = null
    private var cachedCurrentUser: User? = null

    fun getCurrentUserDetails(context: SecurityContext?): UserDetails? {
        if (context == null) { return null }
        if (cachedCurrentUserDetails != null) { return cachedCurrentUserDetails }

        val authentication = context.authentication
        if (authentication is UsernamePasswordAuthenticationToken) {
            val authUserDetails = authentication.principal as UserDetails
            cachedCurrentUserDetails = userDetailsService.loadUserByUsername(authUserDetails.username)
            return cachedCurrentUserDetails
        }
        return null
    }

    fun getCurrentUser(context: SecurityContext?): User? {
        if (context == null) { return null }
        if (cachedCurrentUser != null) { return cachedCurrentUser }

        val userDetails = getCurrentUserDetails(context)

        if (userDetails != null) {
            cachedCurrentUser = userService.findByEmail(userDetails.username)
            return cachedCurrentUser
        }

        return null
    }

    fun isCurrentUser(context: SecurityContext?, id: UUID): Boolean {
        val user = getCurrentUser(context)
        return user != null && user.id == id
    }
}
