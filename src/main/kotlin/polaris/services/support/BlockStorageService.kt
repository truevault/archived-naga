package polaris.services.support

import com.amazonaws.services.s3.model.ObjectMetadata
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.http.MediaTypeFactory
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.InputStream

@Service
class BlockStorageService(
    private val awsClientService: AwsClientService,
    @Value("\${polaris.images.baseUrl}")
    private val imagesBaseUrl: String
) {
    @Value("\${polaris.images.bucket}")
    private lateinit var imagesBucket: String
    @Value("\${polaris.images.prefix}")
    private lateinit var imagesPrefix: String

    private val s3 = awsClientService.s3Client()

    companion object {
        private val log = LoggerFactory.getLogger(BlockStorageService::class.java)
    }

    fun put(bucket: String, key: String, data: InputStream, metadata: ObjectMetadata) {
        s3.putObject(bucket, key, data, metadata)
    }

    fun get(bucket: String, key: String): InputStream {
        return s3.getObject(bucket, key).objectContent
    }

    fun delete(bucket: String, key: String) {
        s3.deleteObject(bucket, key)
    }

    fun exists(bucket: String, key: String): Boolean {
        return s3.doesObjectExist(bucket, key)
    }

    fun putFile(bucket: String, key: String, file: MultipartFile) {
        val metadata = ObjectMetadata()
        metadata.contentLength = file.size
        metadata.contentType = getMediaTypeFromFilename(key).toString()
        put(bucket, key, file.inputStream, metadata)
    }

    fun putImage(filename: String, file: MultipartFile) {
        return putFile(imagesBucket, getKey(imagesPrefix, filename), file)
    }

    fun getImage(filename: String): InputStream {
        return get(imagesBucket, getKey(imagesPrefix, filename))
    }

    fun deleteImage(filename: String) {
        val key = getKey(imagesPrefix, filename)
        if (exists(imagesBucket, key)) {
            delete(imagesBucket, key)
        }
    }

    fun deleteImageByUrl(url: String) {
        val key = getKeyFromUrl(url)
        log.info("Delete Image Key: $key")
        if (exists(imagesBucket, key)) {
            log.info("Image exists, deleting $key from $imagesBucket")
            delete(imagesBucket, key)
        }
    }

    fun getMediaTypeFromFilename(filename: String): MediaType {
        return MediaTypeFactory.getMediaType(filename).orElseGet { MediaType.APPLICATION_OCTET_STREAM }
    }

    fun getFileExtension(filename: String): String? {
        val parts = filename.split(".")
        return if (parts.size > 1) parts.last() else null
    }

    private fun getKeyFromUrl(url: String): String {
        val path = url.replace(imagesBaseUrl, "").substring(1)
        return getKey(imagesPrefix, path)
    }

    private fun getKey(prefix: String, filename: String): String = "$prefix/$filename"
}
