package polaris.services.support

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.persistence.AttributeConverter

class ArrayConverter : AttributeConverter<List<String>, String> {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(ArrayConverter::class.java)
    }

    override fun convertToDatabaseColumn(attribute: List<String>?): String {
        val result = try {
            jacksonObjectMapper().writeValueAsString(attribute)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (todb): ", e)
            "{}"
        }
        return result
    }

    override fun convertToEntityAttribute(dbData: String?): List<String> {
        if (dbData == null) { return emptyList() }
        val result: List<String> = try {
            jacksonObjectMapper().readValue(dbData)
        } catch (e: JsonProcessingException) {
            log.info("Had a json processing exception (toentity): ", e)
            emptyList()
        }
        return result
    }
}
