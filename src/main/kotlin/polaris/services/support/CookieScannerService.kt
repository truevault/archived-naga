package polaris.services.support

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.dto.cookiescan.CookieRecommendationDto
import polaris.models.dto.cookiescan.CookieScanDto
import polaris.models.dto.cookiescan.CookieScanResultDto
import polaris.models.dto.cookiescan.SQSMessageStartCookieScan
import polaris.models.dto.cookiescan.UpdateCookieRequest
import polaris.models.entity.cookieRecommendation.CookieRecommendation
import polaris.models.entity.organization.CookieScanProgress
import polaris.models.entity.organization.OrganizationCookie
import polaris.models.entity.organization.OrganizationCookieScan
import polaris.repositories.CookieRecommendationRepository
import polaris.repositories.OrgCookieRepo
import polaris.repositories.OrganizationCookieScanRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.services.dto.OrganizationCookieDtoService
import java.time.ZonedDateTime
import java.util.UUID

@Service
class CookieScannerService(
    private val awsClientService: AwsClientService,
    private val orgCookieRepo: OrgCookieRepo,
    private val orgCookieScanRepo: OrganizationCookieScanRepository,
    private val cookieRecommendationRepository: CookieRecommendationRepository,
    private val organizationCookieDtoService: OrganizationCookieDtoService,
    private val dataRecipientRepo: OrganizationDataRecipientRepository,
    private val lookup: LookupService,

    @Value("\${polaris.aws.cookieScannerQueueUrl}")
    private val cookieScannerQueueUrl: String
) {

    companion object {
        private val log = LoggerFactory.getLogger(PrivacyCenterBuilderService::class.java)
    }

    fun addCookieScanResult(result: CookieScanResultDto) {
        log.info("Received scan result: $result")
        val org = lookup.orgOrThrow(result.organizationId)

        val existing = orgCookieRepo.findAllByOrganization(org)
        val dataRecipients = dataRecipientRepo.findAllByOrganization(org)

        val cookies = result.cookies.map { cookie ->
            val obj = existing.find { it.name == cookie.name && it.domain == cookie.domain } ?: OrganizationCookie(
                organization = org,
                organizationId = org.id,
                name = cookie.name,
                domain = cookie.domain,
                isHidden = false,
                isSnoozed = false,
                firstScannedAt = ZonedDateTime.now()
            )

            obj.lastScannedAt = ZonedDateTime.now()
            obj.lastScannedUrl = result.url

            obj.lastScannedValue = cookie.value
            obj.lastScannedPath = cookie.path
            obj.lastScannedExpires = cookie.expires
            obj.lastScannedHttpOnly = cookie.httpOnly
            obj.lastScannedSecure = cookie.secure
            obj.lastScannedSession = cookie.session
            obj.lastScannedSameSite = cookie.sameSite
            obj.lastScannedPriority = cookie.priority
            obj.lastScannedSameParty = cookie.sameParty

            val recommendation = recommendationForCookie(obj)

            if (recommendation != null) {
                obj.categories = recommendation.categories.union(obj.categories).toList()

                if (obj.dataRecipients.isEmpty()) {
                    obj.dataRecipients = dataRecipients.filter { recommendation.vendor != null && it.vendor?.id == recommendation.vendor?.id }.toMutableSet()
                }
            }

            obj
        }

        orgCookieRepo.saveAll(cookies)

        val scans = orgCookieScanRepo.findAllByOrganizationAndScanUrl(org, result.url)
        scans.forEach {
            it.status = if (result.error != null) CookieScanProgress.ERROR else CookieScanProgress.COMPLETE
            it.scanEndedAt = ZonedDateTime.now()

            it.scanError = result.error
        }
        orgCookieScanRepo.saveAll(scans)
    }

    fun recommendationForCookie(cookie: OrganizationCookie): CookieRecommendation? {
        if (cookie.name == null || cookie.domain == null) {
            return null
        }

        val recsForName = cookieRecommendationRepository.findAllByName(cookie.name)
        return recsForName.find { matchesRecommendation(cookie, it) }
    }

    fun matchesRecommendation(cookie: OrganizationCookie, recommendation: CookieRecommendation): Boolean {
        val recDomain = recommendation.domain.trim()
        val nameMatches = cookie.name == recommendation.name
        val domainMatches = recDomain == "*" || (cookie.domain?.trim()?.endsWith(recDomain) ?: false)

        return nameMatches && domainMatches
    }

    fun getCookieScanResult(organizationPublicId: OrganizationPublicId): CookieScanDto {
        val org = lookup.orgOrThrow(organizationPublicId)

        val scan = orgCookieScanRepo.findAllByOrganization(org).firstOrNull()
            ?: return CookieScanDto(
                organizationId = org.publicId,
                status = CookieScanProgress.NOT_STARTED,
                scanUrl = null,
                error = null,
                cookies = emptyList()
            )

        if (scan.hasScanTimedOut()) {
            scan.status = CookieScanProgress.ERROR
            orgCookieScanRepo.save(scan)

            return CookieScanDto(
                organizationId = org.publicId,
                status = CookieScanProgress.ERROR,
                scanUrl = scan.scanUrl,
                error = "Cookie Scan has Timed Out",
                cookies = emptyList()
            )
        }

        val cookies = if (scan.status == CookieScanProgress.COMPLETE) {
            val cookies = orgCookieRepo.findAllByOrganization(org)
            cookies.map { organizationCookieDtoService.toDto(it) }
        } else {
            emptyList()
        }

        return CookieScanDto(
            organizationId = org.publicId,
            status = scan.status,
            scanUrl = scan.scanUrl,
            error = scan.scanError,
            cookies = cookies
        )
    }

    fun startCookieScanRequest(organizationPublicId: OrganizationPublicId, url: String) {
        val org = lookup.orgOrThrow(organizationPublicId)
        val scanId = UUID.randomUUID()
        val sqs = awsClientService.sqsClient()

        log.info("starting cookie scan $scanId")

        val message = SQSMessageStartCookieScan(
            organizationId = organizationPublicId,
            scanId = scanId.toString(),
            url = url
        )

        log.info("inserting message into SQS queue: $cookieScannerQueueUrl; json: ${message.toJson()}")
        sqs.sendMessage(cookieScannerQueueUrl, message.toJson())
        log.info("inserted message")

        // we're going to purge any other in progress scans
        val existing = orgCookieScanRepo.findAllByOrganization(org)
        orgCookieScanRepo.deleteAll(existing)
        orgCookieScanRepo.save(
            OrganizationCookieScan(
                id = scanId,
                organization = org,
                scanUrl = url,
                status = CookieScanProgress.IN_PROGRESS,
                scanStartedAt = ZonedDateTime.now()
            )
        )
    }

    fun resetCookieScan(organizationPublicId: OrganizationPublicId) {
        val org = lookup.orgOrThrow(organizationPublicId)
        val scans = orgCookieScanRepo.findAllByOrganization(org)
        orgCookieScanRepo.deleteAll(scans)

        val cookies = orgCookieRepo.findAllByOrganization(org)
        orgCookieRepo.deleteAll(cookies)
    }

    fun updateCookie(
        organizationPublicId: OrganizationPublicId,
        domain: String,
        name: String,
        update: UpdateCookieRequest
    ) {
        val org = lookup.orgOrThrow(organizationPublicId)
        val cookie = lookup.cookieOrThrow(org, domain, name)

        val dataRecipients = dataRecipientRepo.findAllByOrganization(org)

        cookie.categories = update.categories
        cookie.isHidden = update.hidden
        cookie.isFirstParty = update.firstParty
        cookie.dataRecipients =
            dataRecipients.filter { update.dataRecipients.contains(it.vendor!!.id.toString()) }.toMutableSet()

        orgCookieRepo.save(cookie)
    }

    fun getRecommendations(organizationPublicId: OrganizationPublicId): List<CookieRecommendationDto> {
        val org = lookup.orgOrThrow(organizationPublicId)
        val recommendations = cookieRecommendationRepository.findAll()
        val cookies = orgCookieRepo.findAllByOrganization(org)

        // filter to relevant recommendations
        val relevantRecs = recommendations.filter { r -> cookies.any { c -> matchesRecommendation(c, r) } }

        return relevantRecs.map { it.toDto() }
    }
}
