package polaris.services.support

import io.jsonwebtoken.Jwts
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import javax.xml.bind.DatatypeConverter

@Service
class SSOService(
    @Value("\${polaris.integration.beaconSecret}")
    private val beaconSecret: String,

    @Value("\${polaris.integration.productBoardSecret}")
    private val productBoardSecret: String
) {
    fun createBeaconSSOToken(email: String): String {
        val hasher = Mac.getInstance("HmacSHA256")
        hasher.init(SecretKeySpec(beaconSecret.toByteArray(), "HmacSHA256"))
        val hash = hasher.doFinal(email.toByteArray())
        return DatatypeConverter.printHexBinary(hash).toLowerCase()
    }

    fun createProductBoardJWT(id: String, email: String, name: String, companyName: String): String {
        val key = SecretKeySpec(productBoardSecret.toByteArray(), "HmacSHA256")
        return Jwts.builder()
            .setClaims(
                mapOf(
                    "id" to id,
                    "email" to email,
                    "name" to name,
                    "company_name" to companyName
                )
            )
            .setIssuedAt(Date())
            .signWith(key)
            .compact()
    }
}
