package polaris.services.support

import org.springframework.stereotype.Service
import polaris.models.entity.organization.Organization
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.user.User

@Service
class RequestTemplateMessageService(optOutService: OptOutService) : TemplateMessageService(optOutService) {
    fun dataRequestMessageTemplate(
        actor: User,
        organization: Organization,
        dataSubjectRequest: DataSubjectRequest,
        template: String
    ): String {
        val root = RequestMessageTemplateData(
            organization = organizationToTemplateData(organization),
            request = dataSubjectRequestToTemplateData(dataSubjectRequest),
            user = userToTemplateData(actor)
        )
        return renderTemplate(template, root)
    }
}
