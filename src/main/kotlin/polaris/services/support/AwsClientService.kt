package polaris.services.support

import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.regions.Regions
import com.amazonaws.services.cloudfront.AmazonCloudFront
import com.amazonaws.services.cloudfront.AmazonCloudFrontClientBuilder
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class AwsClientService(
    @Value("\${polaris.aws.sqsEndpoint:null}")
    private val sqsEndpoint: String?,
    @Value("\${polaris.aws.s3Endpoint:null}")
    private val s3Endpoint: String?,
    @Value("\${polaris.aws.sesEndpoint:null}")
    private val sesEndpoint: String?
) {
    fun s3Client(): AmazonS3 {
        var builder = AmazonS3ClientBuilder.standard()

        builder = if (!s3Endpoint.isNullOrBlank() && s3Endpoint != "null") {
            builder
                .withEndpointConfiguration(AwsClientBuilder.EndpointConfiguration(s3Endpoint, "us-west-2"))
                .withPathStyleAccessEnabled(true)
        } else {
            builder.withRegion(Regions.US_WEST_2)
        }

        return builder.build()!!
    }

    fun cfClient(): AmazonCloudFront {
        val builder = AmazonCloudFrontClientBuilder.standard()

        return builder.withRegion(Regions.US_EAST_1).build()
    }

    fun sqsClient(): AmazonSQS {
        var builder = AmazonSQSClientBuilder.standard()

        builder = if (!sqsEndpoint.isNullOrBlank() && sqsEndpoint != "null") {
            builder.withEndpointConfiguration(AwsClientBuilder.EndpointConfiguration(sqsEndpoint, "us-west-2"))
        } else {
            builder.withRegion(Regions.US_WEST_2)
        }

        return builder.build()!!
    }

    fun sesClient(): AmazonSimpleEmailService {
        var builder = AmazonSimpleEmailServiceClientBuilder.standard()

        builder = if (!sesEndpoint.isNullOrBlank() && sesEndpoint != "null") {
            builder.withEndpointConfiguration(AwsClientBuilder.EndpointConfiguration(sesEndpoint, "us-west-2"))
        } else {
            builder.withRegion(Regions.US_WEST_2)
        }

        return builder.build()!!
    }
}
