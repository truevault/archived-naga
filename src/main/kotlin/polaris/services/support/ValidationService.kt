package polaris.services.support

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.ResponseStatus
import polaris.util.PolarisException

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class UnacceptablePasswordException(message: String) : PolarisException(message)

@Service
class ValidationService {
    fun validatePassword(password: String) {
        if (password.length < 8) { throw UnacceptablePasswordException("The password must be at least 8 characters") }
        if (password.length > 64) { throw UnacceptablePasswordException("The password must be fewer than 64 characters") }
    }
}
