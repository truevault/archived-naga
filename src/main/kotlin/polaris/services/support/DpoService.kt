package polaris.services.support

import org.springframework.stereotype.Service
import polaris.models.dto.ApiMode
import polaris.models.entity.organization.EEA_UK_SURVEY_NAME
import polaris.models.entity.organization.GDPR_LAWFUL_BASES_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.repositories.CollectionGroupCollectedPICRepo
import polaris.services.primary.OrganizationSurveyService

@Service
class DpoService(
    private val surveyService: OrganizationSurveyService,
    private val collectedPICRepo: CollectionGroupCollectedPICRepo
) {
    fun isDpoRequired(org: Organization): Boolean {
        var dpoRequired = false

        if (!dpoRequired) {
            val lawfulBasesSurvey = surveyService.getSurveyQuestions(org.publicId, ApiMode.Live, GDPR_LAWFUL_BASES_SURVEY_NAME)
            dpoRequired = lawfulBasesSurvey.any { it.slug == "automated-decision-making" && it.answer == "true" }
        }

        if (!dpoRequired) {
            val eeaUkSurvey = surveyService.getSurveyQuestions(org.publicId, ApiMode.Live, EEA_UK_SURVEY_NAME)
            dpoRequired = eeaUkSurvey.any { it.slug == "personal-data-large-scale" && it.answer == "true" }
        }

        if (!dpoRequired) {
            val sensitiveCollection = collectedPICRepo.findAllByCollectionGroup_OrganizationAndPic_PicKeyIn(org, listOf("gdpr-genetics", "gdpr-data-revealing-beliefs", "gdpr-health-or-sex-data", "gdpr-criminal-offenses"))
            dpoRequired = sensitiveCollection.isNotEmpty()
        }

        return dpoRequired
    }
}
