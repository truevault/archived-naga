package polaris.services.support

import org.springframework.stereotype.Service
import polaris.models.entity.organization.Organization
import polaris.models.entity.user.User

@Service
class TestTemplateMessageService(optOutService: OptOutService) : TemplateMessageService(optOutService) {

    val templateString = """
    Your privacy email address is successfully connected to Polaris.
    """.trimIndent().replace("\n", " ")

    fun testMessageTemplate(user: User, organization: Organization): String {
        val templateData = OrganizationSignatureTemplateData(
            organization = organizationToTemplateData(organization),
            user = userToTemplateData(user)
        )
        return renderTemplate(templateString + "\n\n${organization.messageSignature}", templateData)
    }
}
