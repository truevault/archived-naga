package polaris.services.support

import com.amazonaws.AmazonServiceException
import com.amazonaws.services.s3.model.ObjectMetadata
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.InputStreamResource
import org.springframework.core.io.Resource
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import polaris.models.entity.attachment.Attachment
import polaris.repositories.AttachmentRepository
import polaris.util.PolarisException
import java.lang.Exception
import java.util.*
import javax.transaction.Transactional

data class ResourceAttachment(
    val resource: Resource,
    val attachment: Attachment
)

@Service
@Transactional
class AttachmentService(
    private val attachmentRepository: AttachmentRepository,
    private val awsClientService: AwsClientService
) {

    @Value("\${polaris.attachments.bucket}")
    private lateinit var bucket: String

    @Value("\${polaris.attachments.prefix}")
    private lateinit var prefix: String

    private val s3 = awsClientService.s3Client()

    companion object {
        private val log = LoggerFactory.getLogger(AttachmentService::class.java)
    }

    fun getAttachment(attachmentId: UUID): ResourceAttachment {
        val attachment = attachmentRepository.findByIdOrNull(attachmentId)
            ?: throw PolarisException("Unable to find attachment $attachmentId")
        val k = key(attachmentId)

        val obj = s3.getObject(bucket, k)

        val resource = InputStreamResource(obj.objectContent)

        return ResourceAttachment(resource, attachment)
    }

    fun createAttachment(file: MultipartFile): Attachment {
        val attachmentId = UUID.randomUUID()
        val k = key(attachmentId)

        log.info("[$attachmentId] creating attachment for: ${file.name} / ${file.originalFilename} / ${file.contentType} / ${file.size}")

        val attachment = attachmentRepository.save(
            Attachment(
                id = attachmentId,
                name = file.originalFilename!!,
                mimeType = file.contentType!!,
                size = file.size
            )
        )

        log.info("[$attachmentId] saved attachment.")
        log.info("[$attachmentId] uploading to: s3://$bucket/$k")

        try {
            val metadata = ObjectMetadata()
            metadata.contentLength = file.size
            s3.putObject(bucket, k, file.inputStream, metadata)
        } catch (e: AmazonServiceException) {
            log.error("[$attachmentId] failed to upload attachment to S3: ${e.errorMessage}.")
        }

        log.info("[$attachmentId] wrote file successfully.")

        return attachment
    }

    fun deleteAttachment(attachmentId: UUID) {
        attachmentRepository.findByIdOrNull(attachmentId)
            ?: throw PolarisException("Unable to find attachment $attachmentId")
        val k = key(attachmentId)
        if (s3.doesObjectExist(bucket, k)) {
            try {
                s3.deleteObject(bucket, k)
            } catch (e: Exception) {
                log.error("Failed to delete attachment $attachmentId: $e")
            }
        }
    }

    private fun key(attachmentId: UUID) = "$prefix/$attachmentId"
}
