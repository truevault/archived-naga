package polaris.services.support.oauth

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import polaris.controllers.GoogleAnalyticsOAuthException
import polaris.models.entity.organization.Organization
import polaris.util.HttpUtil
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

@JsonIgnoreProperties(ignoreUnknown = true)
data class GaAccessTokenResponse(
    val access_token: String?,
    val expires_in: Long?,
    val token_type: String?,
    val refresh_token: String?,
    val scope: String?,
    val error: String?,
    val error_description: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class GaRevokeAccessTokenResponse(
    val error: String?,
    val error_description: String?
)

@Service
class GoogleOauthSupport(
    @Value("\${polaris.oauth.google.clientId}")
    private val clientId: String,
    @Value("\${polaris.oauth.google.clientSecret}")
    private val clientSecret: String,
    @Value("\${polaris.domain}")
    private val polarisDomain: String
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(GoogleOauthSupport::class.java)
    }

    fun generateOauthUrl(org: Organization): String {
        return HttpUtil.buildUrl(
            "https://accounts.google.com/o/oauth2/v2/auth",
            mapOf(
                "scope" to "https://www.googleapis.com/auth/analytics.user.deletion",
                "state" to org.publicId,
                "access_type" to "offline",
                "include_granted_scopes" to "true",
                "response_type" to "code",
                "redirect_uri" to "$polarisDomain/webhooks/oauth/google",
                "client_id" to clientId
            )
        )
    }

    fun requestAccessTokenFromRefreshToken(refresh: String): GaAccessTokenResponse {
        val client = HttpClient.newBuilder().build()

        val request = HttpRequest.newBuilder()
            .uri(URI.create("https://oauth2.googleapis.com/token"))
            .headers("Content-Type", "application/x-www-form-urlencoded")
            .POST(
                HttpUtil.urlFormEncodedBodyPublisher(
                    mapOf(
                        "client_id" to clientId,
                        "client_secret" to clientSecret,
                        "refresh_token" to refresh,
                        "grant_type" to "refresh_token"
                    )
                )
            )
            .build()

        val response: HttpResponse<*> = client.send(request, HttpResponse.BodyHandlers.ofString())

        val body = response.body().toString()

        val result = jacksonObjectMapper().readValue<GaAccessTokenResponse>(body)

        if (result.error != null) {
            log.error("Received an error response from Google API: body=$body")
            throw GoogleAnalyticsOAuthException(result.error, result.error_description!!)
        }

        return result
    }

    fun requestRefreshToken(org: Organization, code: String): GaAccessTokenResponse {
        val client = HttpClient.newBuilder().build()

        val request = HttpRequest.newBuilder()
            .uri(URI.create("https://oauth2.googleapis.com/token"))
            .headers("Content-Type", "application/x-www-form-urlencoded")
            .POST(
                HttpUtil.urlFormEncodedBodyPublisher(
                    mapOf(
                        "code" to code,
                        "client_id" to clientId,
                        "client_secret" to clientSecret,
                        "redirect_uri" to "$polarisDomain/webhooks/oauth/google",
                        "grant_type" to "authorization_code"
                    )
                )
            )
            .build()

        val response: HttpResponse<*> = client.send(request, HttpResponse.BodyHandlers.ofString())
        return jacksonObjectMapper().readValue(response.body().toString())
    }

    fun assertRefreshTokenIsValid(tokens: GaAccessTokenResponse) {
        if (tokens.access_token == null) {
            throw GoogleAnalyticsOAuthException("access_token_null", "Access token was null")
        }
        if (tokens.refresh_token == null) {
            throw GoogleAnalyticsOAuthException("refresh_token_null", "Refresh token was null")
        }
    }

    fun revokeAuthToken(accessToken: String): Boolean {
        val client = HttpClient.newBuilder().build()
        val request = HttpRequest.newBuilder()
            .uri(URI.create("https://oauth2.googleapis.com/revoke?token=$accessToken"))
            .headers("Content-Type", "application/x-www-form-urlencoded")
            .POST(HttpUtil.urlFormEncodedBodyPublisher(emptyMap()))
            .build()

        val response: HttpResponse<*> = client.send(request, HttpResponse.BodyHandlers.ofString())
        val body = response.body().toString()
        val result = jacksonObjectMapper().readValue<GaRevokeAccessTokenResponse>(body)

        if (result.error != null) {
            log.error("Received an error response from Google API: body=$body")
            throw GoogleAnalyticsOAuthException(
                result.error,
                result.error_description ?: "There was an issue while revoking Google Analytics OAuth token"
            )
        }

        return true
    }
}
