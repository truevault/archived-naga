package polaris.services.support.oauth

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.GoogleAnalyticsOAuthException
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.entity.AuthorizationNotFound
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationAuthToken
import polaris.models.entity.organization.OrganizationAuthTokenProvider
import polaris.repositories.OrganizationAuthTokenRepository
import polaris.services.primary.TaskUpdateService
import java.time.ZonedDateTime

@Service
class GoogleAuthTokenService(
    private val authTokenRepo: OrganizationAuthTokenRepository,
    private val googleOauth: GoogleOauthSupport,
    private val lookup: LookupService,
    private val taskUpdateService: TaskUpdateService

) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(GoogleAuthTokenService::class.java)
    }

    fun generateGoogleAnalyticsOAuthUrl(organizationId: OrganizationPublicId): String {
        val org = lookup.orgOrThrow(organizationId)
        return googleOauth.generateOauthUrl(org)
    }

    /**
     * Requests Google Analytics auth tokens using the code returned from
     * the OAuth webhook. If the tokens are valid, then persists the tokens
     * to the DB.
     */
    @Transactional
    fun fetchAndPersistGoogleAnalyticsAuthToken(org: Organization, code: String, allowRetryOnce: Boolean = true) {
        log.info("Requesting refresh token for org ${org.name}...")
        val tokens = googleOauth.requestRefreshToken(org, code)

        try {
            googleOauth.assertRefreshTokenIsValid(tokens)
        } catch (ex: GoogleAnalyticsOAuthException) {
            if (ex.error == "refresh_token_null") {
                log.warn("access_token was not null while refresh_token was null. Revoking the access_token so that the user can re-auth.")
                googleOauth.revokeAuthToken(tokens.access_token!!)
                throw GoogleAnalyticsOAuthException("invalid_oauth_state", "We encountered an error while authorizing your account. Please retry authorization.")
            }
            throw ex
        }

        log.info("Persisting refresh token for org ${org.name}...")
        upsertGoogleAnalyticsAuthToken(org, tokens)

        log.info("Closing tasks for org ${org.name}")
        taskUpdateService.updateTasks(org, create = false, close = true)
    }

    /**
     * Inserts or updates the Google OAuth token into the DB
     */
    @Transactional
    fun upsertGoogleAnalyticsAuthToken(org: Organization, tokens: GaAccessTokenResponse) {
        val authToken = authTokenRepo.findByOrganizationAndProvider(org, OrganizationAuthTokenProvider.GOOGLE_OAUTH)
            ?: OrganizationAuthToken(organizationId = org.id, provider = OrganizationAuthTokenProvider.GOOGLE_OAUTH, createdAt = ZonedDateTime.now())

        authToken.updatedAt = ZonedDateTime.now()
        authToken.expiresAt = ZonedDateTime.now().plusSeconds(tokens.expires_in!!)
        authToken.auth = tokens.access_token
        authToken.refresh = tokens.refresh_token

        authTokenRepo.save(authToken)
    }

    /**
     * Revokes the Google OAuth token via Google API and deletes the auth token from the DB.
     * Returns the error code from Google API if it fails.
     */
    @Transactional
    fun deleteGoogleAnalyticsAuthToken(organizationId: OrganizationPublicId): String? {
        val org = lookup.orgOrThrow(organizationId)
        val authToken = authTokenRepo.findByOrganizationAndProvider(org, OrganizationAuthTokenProvider.GOOGLE_OAUTH)
            ?: throw AuthorizationNotFound(org.publicId, OrganizationAuthTokenProvider.GOOGLE_OAUTH)

        val errorCode = try {
            revokeGoogleAnalyticsAuthToken(org)
        } catch (ex: GoogleAnalyticsOAuthException) {
            ex.error
        } finally {
            // NOTE: We always delete the auth token from the DB regardless of the outcome
            // of revoking the existing token via the Google API. A common cause for the Google API request
            // failing with an invalid token is when the existing token belonged to somebody who no
            // longer works for the company and whose Google account is no longer valid.
            log.info("deleting auth token from DB... [org=${org.name}, provider=${authToken.provider}]")
            authTokenRepo.delete(authToken)
        }

        return errorCode
    }

    fun revokeGoogleAnalyticsAuthToken(organization: Organization): String? {
        val authToken = authTokenRepo.findByOrganizationAndProvider(organization, OrganizationAuthTokenProvider.GOOGLE_OAUTH)
            ?: throw AuthorizationNotFound(organization.publicId, OrganizationAuthTokenProvider.GOOGLE_OAUTH)

        return try {
            log.info("revoking auth token from Google... [org=${organization.name}, provider=${authToken.provider}]")
            if (authToken.auth != null) googleOauth.revokeAuthToken(authToken.auth!!)
            null
        } catch (ex: GoogleAnalyticsOAuthException) {
            log.warn("An error occurred while revoking token from Google Analytics API for Organization ${organization.name}: ${ex.error}")
            ex.error
        }
    }
}
