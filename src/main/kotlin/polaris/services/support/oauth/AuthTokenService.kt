package polaris.services.support.oauth

import org.springframework.stereotype.Service
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.OrganizationAuthorizationsDto
import polaris.models.entity.organization.OrganizationAuthTokenProvider
import polaris.repositories.OrganizationAuthTokenRepository

@Service
class AuthTokenService(
    private val authTokenRepo: OrganizationAuthTokenRepository,
    private val lookup: LookupService
) {
    fun getAuthorizations(organizationId: OrganizationPublicId): OrganizationAuthorizationsDto {
        val org = lookup.orgOrThrow(organizationId)
        val googleOauth = authTokenRepo.findByOrganizationAndProvider(org, OrganizationAuthTokenProvider.GOOGLE_OAUTH)

        return OrganizationAuthorizationsDto(
            id = org.publicId,
            hasGoogle = googleOauth?.refresh != null
        )
    }
}
