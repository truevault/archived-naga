package polaris.services.support

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.stereotype.Service
import polaris.models.dto.ApiMode
import polaris.models.dto.SurveySlug
import polaris.models.entity.organization.BUSINESS_SURVEY_NAME
import polaris.models.entity.organization.EEA_UK_SURVEY_NAME
import polaris.models.entity.organization.FINANCIAL_INCENTIVES_SURVEY_NAME
import polaris.models.entity.organization.HR_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationSurveyProgress
import polaris.models.entity.organization.SELLING_AND_SHARING_SURVEY_NAME
import polaris.repositories.OrgSurveyProgressRepo
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationRepository
import polaris.services.primary.OrganizationSurveyService
import polaris.services.primary.SurveyService

@Service
class SurveyProgressService(
    val surveyProgressRepo: OrgSurveyProgressRepo,
    val orgRepo: OrganizationRepository,
    val surveyService: SurveyService,
    val orgSurveyService: OrganizationSurveyService,
    val dataMapService: DataMapService,
    val organizationDataRecipientRepository: OrganizationDataRecipientRepository
) {
    fun getSurveyProgress(org: Organization, surveySlug: SurveySlug): String {
        val progress = surveyProgressRepo.findByOrganizationAndSurveySlug(organization = org, surveySlug = surveySlug)

        return progress?.currentStep ?: surveySlug.defaultProgress
    }

    fun updateSurveyProgress(org: Organization, surveySlug: SurveySlug, progress: String) {
        val row = surveyProgressRepo.findByOrganizationAndSurveySlug(organization = org, surveySlug = surveySlug)
            ?: OrganizationSurveyProgress(organizationId = org.id, organization = org, surveySlug = surveySlug, currentStep = progress)

        row.currentStep = progress

        surveyProgressRepo.save(row)
    }

    fun startSurvey(org: Organization, surveySlug: SurveySlug) {
        val currentSurvey = org.currentSurvey
        if (currentSurvey == null) {
            org.currentSurvey = surveySlug

            if (surveySlug == SurveySlug.s2023_1) {
                // do the new survey question update
                captureUnansweredSurveyQuestions(org, "2023q1-survey")
            }
            if (surveySlug == SurveySlug.s2023_3) {
                // do the new survey question update
                captureUnansweredSurveyQuestions(org, "2023q3-survey")
                captureCollectedDataMap(org, "2023q3-survey")
                captureDataRecipients(org, "2023q3-survey")
            }
            orgRepo.save(org)
        }
    }

    fun completeSurvey(org: Organization, surveySlug: SurveySlug) {
        val currentVersion = org.complianceVersion
        val newVersion = surveySlug.nextComplianceVersion()

        if (currentVersion == null || currentVersion.ordinal < newVersion.ordinal) {
            org.complianceVersion = surveySlug.nextComplianceVersion()
            org.currentSurvey = null
            orgRepo.save(org)
        }
    }

    fun captureUnansweredSurveyQuestions(org: Organization, surveyName: String) {
        // this is bad, go through the survey service
        // val allAnsweredQuestions = surveyRepo.findAllByOrganization(org)
        val questions = orgSurveyService.getSurveyQuestions(org.publicId, ApiMode.Live, BUSINESS_SURVEY_NAME, EEA_UK_SURVEY_NAME, SELLING_AND_SHARING_SURVEY_NAME, FINANCIAL_INCENTIVES_SURVEY_NAME)
        val answeredQuestions = questions.filter { it.answer != null }

        val answeredQuestionKeys = answeredQuestions.map { it.getCompositeKey() }.toSet()

        val unansweredQuestions = CATCH_UP_SURVEY_COMPOSITE_KEY_MAP[surveyName]?.filter { !answeredQuestionKeys.contains(it) }

        val json = jacksonObjectMapper().writeValueAsString(unansweredQuestions)

        val questionSlug = "unanswered-survey-questions"
        val answer = surveyService.getAnswer(org, surveyName, questionSlug)
        if (answer == null) {
            surveyService.writeSurveyAnswer(org, json, null, surveyName, questionSlug)
        }
    }

    fun captureCollectedDataMap(org: Organization, surveyName: String) {
        val allCollectedPIC = dataMapService.getAllCollectedPic(org)
        val collectedByGroup = allCollectedPIC.groupBy { it.collectionGroup!! }
        val collection = collectedByGroup.map { (cg, c) ->
            cg.id.toString() to c.map { it.pic!!.id.toString() }
        }.toMap()
        val json = jacksonObjectMapper().writeValueAsString(collection)
        surveyService.writeSurveyAnswer(org, json, null, surveyName, "initial-collected-pic")
    }

    fun captureDataRecipients(org: Organization, surveyName: String) {
        val dataRecipients = organizationDataRecipientRepository.findAllByOrganization(org)
        val dataRecipientIds = dataRecipients.map { it.id }
        val json = jacksonObjectMapper().writeValueAsString(dataRecipientIds)
        surveyService.writeSurveyAnswer(org, json, null, surveyName, "initial-data-recipient-ids")
    }
}

data class CatchUpBusinessSurveyQuestion(val survey: String, val slug: String) {
    fun getCompositeKey(): String { return "$survey/$slug" }
}

val CATCH_UP_BUSINESS_SURVEY_QUESTIONS = listOf(
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-physical-location"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-products-aimed-at-kids"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-has-mobile-apps"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-california-nonenglish"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-california-10million"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "consumer-collection-for-other-businesses"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-service-provider"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "purchases-consumer-data"),

    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-hipaa-compliant"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-glba-compliant"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-fcra-compliant"),
    CatchUpBusinessSurveyQuestion(HR_SURVEY_NAME, "business-employees-ca"),
    CatchUpBusinessSurveyQuestion(HR_SURVEY_NAME, "business-contractors-ca"),
    CatchUpBusinessSurveyQuestion(HR_SURVEY_NAME, "business-job-applicants-ca"),

    CatchUpBusinessSurveyQuestion(EEA_UK_SURVEY_NAME, "personal-data-large-scale"),
    CatchUpBusinessSurveyQuestion(HR_SURVEY_NAME, "employees-eea-uk"),
    CatchUpBusinessSurveyQuestion(EEA_UK_SURVEY_NAME, "established-in-eea-uk"),
    CatchUpBusinessSurveyQuestion(EEA_UK_SURVEY_NAME, "non-english-website"),

    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "share-data-for-advertising"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "vendor-sharing-categories"),
    // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
    // CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "facebook-ldu"),
    // CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "google-rdp"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "custom-audience-feature"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "disclosure-for-intentional-interaction"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "vendor-intentional-disclosure-selection"),

    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "google-analytics-data-sharing"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "shopify-audiences"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "create-profiles-about-consumers"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "sell-in-exchange-for-money"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "disclosure-for-selling"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "vendor-selling-selection"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "selling-upload-data"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "vendor-upload-data-selection"),

    CatchUpBusinessSurveyQuestion(FINANCIAL_INCENTIVES_SURVEY_NAME, "offer-consumer-incentives"),
    CatchUpBusinessSurveyQuestion(FINANCIAL_INCENTIVES_SURVEY_NAME, "summary-of-financial-incentive"),
    CatchUpBusinessSurveyQuestion(FINANCIAL_INCENTIVES_SURVEY_NAME, "description-of-financial-incentive"),
    CatchUpBusinessSurveyQuestion(FINANCIAL_INCENTIVES_SURVEY_NAME, "description-of-optin"),
    CatchUpBusinessSurveyQuestion(FINANCIAL_INCENTIVES_SURVEY_NAME, "right-to-withdraw"),
    CatchUpBusinessSurveyQuestion(FINANCIAL_INCENTIVES_SURVEY_NAME, "value-of-consumer-data")
)

val CATCH_UP_BUSINSES_SURVEY_COMPOSITE_KEYS = CATCH_UP_BUSINESS_SURVEY_QUESTIONS.map { it.getCompositeKey() }

val CATCH_UP_SURVEY_20230701_QUESTIONS = listOf(
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "disclosure-for-intentional-interaction"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "vendor-intentional-disclosure-selection"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "google-analytics-data-sharing"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "disclosure-for-selling"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-products-aimed-at-kids"),
    CatchUpBusinessSurveyQuestion(BUSINESS_SURVEY_NAME, "business-products-aimed-at-kids-under-13"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "custom-audience-feature"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "sell-in-exchange-for-money"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "vendor-selling-selection"),
    CatchUpBusinessSurveyQuestion(SELLING_AND_SHARING_SURVEY_NAME, "selling-upload-data")
)

val CATCH_UP_SURVEY_20230701_COMPOSITE_KEYS = CATCH_UP_SURVEY_20230701_QUESTIONS.map { it.getCompositeKey() }

val CATCH_UP_SURVEY_COMPOSITE_KEY_MAP = mapOf(
    "2023q1-survey" to CATCH_UP_BUSINSES_SURVEY_COMPOSITE_KEYS,
    "2023q3-survey" to CATCH_UP_SURVEY_20230701_COMPOSITE_KEYS
)
