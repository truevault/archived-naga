package polaris.services.support

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.entity.compliance.AutocreatedProcessingActivitySlug
import polaris.models.entity.organization.GDPR_LAWFUL_BASES_SURVEY_NAME
import polaris.models.entity.organization.LawfulBasis
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.ProcessingActivity
import polaris.models.entity.vendor.ProcessingRegion
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.ProcessingActivityRepo

@Service
@Transactional
class AutocreateProcessingActivityService(
    private val paRepo: ProcessingActivityRepo,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val lookup: LookupService
) {
    fun manageAutomaticProcessingActivities(organizationId: OrganizationPublicId) {
        val org = lookup.orgOrThrow(organizationId)

        // Will need to add US methods if ever necessary

        // Determine the activities to create
        val allAutomaticGdpr = getAutocreateGdprActivitiesForOrg(org) // TODO region based

        // Check which groups already exist
        val existing = paRepo.findAllByOrganization(org)
        val existingSlugs = existing.mapNotNull { it.autocreationSlug }.toSet()

        // Determine the groups to create + destroy
        val toCreate = allAutomaticGdpr - existingSlugs
        val toDestroy = existingSlugs - allAutomaticGdpr

        // Create + destroy the activities
        val createGdprActivities = toCreate.map { constructAutocreateActivities(it, org, listOf(ProcessingRegion.EEA_UK)) }
        paRepo.saveAll(createGdprActivities)

        val destroyGdprActivities = toDestroy.mapNotNull { slug -> existing.find { it.autocreationSlug == slug && it.processingRegions.contains(ProcessingRegion.EEA_UK) } }
        paRepo.deleteAll(destroyGdprActivities)
    }

    fun getAutocreateGdprActivitiesForOrg(org: Organization): Set<AutocreatedProcessingActivitySlug> {
        // now do the front-end ui thing:
        val activities = mutableSetOf<AutocreatedProcessingActivitySlug>()

        val survey = organizationSurveyQuestionRepository.findAllByOrganizationAndSurveyName(org, GDPR_LAWFUL_BASES_SURVEY_NAME)

        if (survey.find { it.slug == "automated-decision-making" }?.answer == "true") {
            activities.add(AutocreatedProcessingActivitySlug.AUTOMATED_DECISION_MAKING)
        }

        if (survey.find { it.slug == "outbound-sales-eea-uk" }?.answer == "true") {
            activities.add(AutocreatedProcessingActivitySlug.COLD_SALES_COMMUNICATIONS)
        }

        return activities
    }

    fun constructAutocreateActivities(slug: AutocreatedProcessingActivitySlug, org: Organization, regions: List<ProcessingRegion>): ProcessingActivity {
        return ProcessingActivity(name = slug.defaultLabel, organization = org, autocreationSlug = slug, gdprLawfulBases = listOf(LawfulBasis.CONSENT), processingRegions = regions)
    }
}
