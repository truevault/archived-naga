package polaris.services.support

import freemarker.template.Configuration
import freemarker.template.Template
import org.springframework.stereotype.Service
import polaris.models.entity.organization.Organization
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.user.User
import java.io.ByteArrayOutputStream
import java.io.OutputStreamWriter
import java.io.StringReader
import java.io.Writer
import java.nio.charset.StandardCharsets
import java.time.format.DateTimeFormatter

@Service
abstract class TemplateMessageService(private val optOutService: OptOutService) {
    fun renderTemplate(template: String, templateData: Any): String {
        val compiledTemplate = Template("inline", StringReader(template), Configuration(Configuration.VERSION_2_3_29))
        val outputStream = ByteArrayOutputStream()
        val out: Writer = OutputStreamWriter(outputStream)
        compiledTemplate.process(templateData, out)
        return outputStream.toString(StandardCharsets.UTF_8)
    }

    fun organizationToTemplateData(organization: Organization): OrganizationTemplateData {
        val privacyCenter = organization.privacyCenters.firstOrNull()
        return OrganizationTemplateData(
            name = organization.name,
            privacy_policy_url = privacyCenter?.privacyPolicyUrl ?: "",
            request_form_url = privacyCenter?.requestFormUrl ?: "",
            request_toll_free_phone_number = privacyCenter?.requestTollFreePhoneNumber ?: "",
            request_to_know_declaration_url = privacyCenter?.getSpecificDeclarationUrl() ?: "",
            do_not_sell_url = optOutService.getOptOutUrl(privacyCenter) ?: ""
        )
    }

    fun consumerTemplateData(dataSubjectRequest: DataSubjectRequest) = ConsumerTemplateData(
        first_name = dataSubjectRequest.subjectFirstName ?: "",
        last_name = dataSubjectRequest.subjectLastName ?: "",
        type = "",
        email = dataSubjectRequest.subjectEmailAddress ?: ""
    )

    fun dataSubjectRequestToTemplateData(dataSubjectRequest: DataSubjectRequest): RequestTemplateData {
        return RequestTemplateData(
            id = dataSubjectRequest.publicId,
            type = dataSubjectRequest.requestType!!.label,
            request_date = dataSubjectRequest.requestDate.format(DateTimeFormatter.ISO_LOCAL_DATE),
            deadline = dataSubjectRequest.requestDueDate?.format(DateTimeFormatter.ISO_LOCAL_DATE) ?: "${dataSubjectRequest.requestType.initialDueDateDays} days from verification",
            consumer = consumerTemplateData(dataSubjectRequest),

            // Legacy keys/names; deprecated
            request_type = dataSubjectRequest.requestType!!.label,
            data_subject = consumerTemplateData(dataSubjectRequest)
        )
    }

    fun userToTemplateData(user: User): UserTemplateData {
        return UserTemplateData(
            last_name = user.lastName ?: "",
            first_name = user.firstName ?: "",
            full_name = "${user.firstName} ${user.lastName}"
        )
    }

    fun organizationToUserTemplateData(org: Organization): UserTemplateData {
        return UserTemplateData(
            last_name = org.name,
            first_name = "",
            full_name = org.name
        )
    }
}

data class RequestMessageTemplateData(
    val organization: OrganizationTemplateData,
    val request: RequestTemplateData,
    val user: UserTemplateData
)

data class OrganizationSignatureTemplateData(
    val organization: OrganizationTemplateData,
    val user: UserTemplateData
)

data class OrganizationTemplateData(
    val name: String = "",
    val privacy_policy_url: String = "",
    val request_toll_free_phone_number: String = "",
    val request_form_url: String = "",
    val request_to_know_declaration_url: String = "",
    val do_not_sell_url: String = ""
)

data class RequestTemplateData(
    val id: String = "",
    val type: String = "",
    val request_date: String = "",
    val deadline: String = "",
    val consumer: ConsumerTemplateData,

    // Legacy keys/names; deprecated
    val request_type: String = "",
    val data_subject: ConsumerTemplateData
)

data class UserTemplateData(
    val first_name: String = "",
    val last_name: String = "",
    val full_name: String = ""
)

data class ConsumerTemplateData(
    val first_name: String = "",
    val last_name: String = "",
    val type: String = "",
    val email: String = ""
)
