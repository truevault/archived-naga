package polaris.services.support

import freemarker.template.Configuration
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils
import org.springframework.web.multipart.MultipartFile
import polaris.controllers.webhook.InboundType
import polaris.controllers.webhook.RequestRouting
import polaris.models.dto.request.PhoneDataRequestDto
import polaris.models.entity.organization.CustomMessageTypeEnum
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.models.entity.organization.OrganizationUser
import polaris.models.entity.organization.PrivacyEmail
import polaris.models.entity.organization.Task
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.request.Regulation
import polaris.models.entity.user.Invitation
import polaris.models.entity.user.User
import polaris.models.entity.user.UserRole
import polaris.models.entity.user.isActive
import polaris.repositories.CustomMessageRepository
import polaris.services.support.OrganizationSurveyQuestionSorter.sortedSurveyQuestions
import polaris.services.support.email.EmailContext
import polaris.services.support.email.EmailResponse
import polaris.services.support.email.EmailSender
import polaris.util.PhoneNumberUtil
import java.io.File
import java.lang.ProcessBuilder.Redirect
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalAdjusters
import java.util.Date
import java.util.concurrent.TimeUnit

enum class EmailType {
    CREATE_USER_INVITE,
    RECONNECT_EMAIL,
    RESET_PASSWORD,
    DATA_REQUEST_MESSAGE,
    EMAIL_VERIFICATION,
    WALKTHROUGH_INVITE,
    NEW_REQUEST_NOTIFICATION,
    NEW_REQUEST_MESSAGE_NOTIFICATION,
    NEW_INBOX_MESSAGE_NOTIFICATION,
    TEST_MESSAGE,
    INTRO_SURVEY_MESSAGE_TO_STAFF,
    REQUEST_PROCESSING_DUE_DATE_EXTENDED,
    REQUEST_PROCESSING_CLOSED_NOT_FOUND,
    REQUEST_PROCESSING_CLOSED_SERVICE_PROVIDER,
    REQUEST_PROCESSING_CLOSED_OPT_OUT,
    REQUEST_PROCESSING_CLOSED_DELETE,
    REQUEST_PROCESSING_CLOSED_KNOW,
    REQUEST_PROCESSING_CLOSED_LIMIT,
    REQUEST_PROCESSING_CLOSED_PORTABILITY,
    REQUEST_PROCESSING_CLOSED_RECTIFICATION,
    REQUEST_PROCESSING_CLOSED_WITHDRAWN_CONSENT,
    REQUEST_PROCESSING_CLOSED_OBJECTION,
    REQUEST_PROCESSING_CLOSED_DELETE_NOT_VERIFIED,
    REQUEST_PROCESSING_CLOSED_KNOW_NOT_VERIFIED,
    WEEKLY_DIGEST,
    INTERNAL_NEW_SUBJECT_REQUEST
}

@Service
class EmailService(
    @Value("\${polaris.mailer.from.user: privacy}")
    private val fromUser: String,
    @Value("\${polaris.mailer.from.domain}")
    private val fromDomain: String,
    @Value("\${polaris.mailer.from.name}")
    private val fromName: String,
    @Value("\${polaris.mailer.address.product}")
    private val productEmail: String,

    @Value("\${polaris.domain}")
    private val polarisDomain: String,

    private val freemarkerConfig: Configuration,
    private val emailSender: EmailSender,
    private val signatureTemplateMessageService: SignatureTemplateMessageService,
    private val optOutService: OptOutService,
    private val subjectRequestRelationService: DataMapService,
    private val customMessageRepo: CustomMessageRepository
) {

    companion object {
        private val logger = LoggerFactory.getLogger(EmailService::class.java)
    }

    fun sendUserInvite(user: User, organizationUser: OrganizationUser, invitation: Invitation) {
        val org = organizationUser.organization!!

        val resetParam = "?reset=${invitation.id}"
        val inviteURL = "$polarisDomain/invite/${org.publicId}$resetParam"

        val model = mapOf("inviteURL" to inviteURL)
        val body = templatedBody("email/user-create-invite.ftl", model)

        sendMessage(
            EmailType.CREATE_USER_INVITE,
            RequestRouting.unknown(),
            user.email,
            "You've been invited to TrueVault Polaris!",
            body,
            null
        )
    }

    fun sendForgotPassword(user: User, requestIp: String? = null) {
        val resetURL = "$polarisDomain/reset/${user.updatePasswordId}"
        val model = mapOf("resetURL" to resetURL, "ipAddress" to (requestIp ?: "Unknown"))
        val body = templatedBody("email/reset-password.ftl", model)

        sendMessage(
            EmailType.RESET_PASSWORD,
            RequestRouting.unknown(),
            user.email,
            "Instructions on Resetting Your TrueVault Polaris Password",
            body,
            null
        )
    }

    fun sendWeeklyDigestEmail(
        overdueRequests: List<DataSubjectRequest>,
        openRequests: List<DataSubjectRequest>,
        pendingRequests: List<DataSubjectRequest>,
        autoclosedRequests: List<DataSubjectRequest>,
        receivedEmails: List<PrivacyEmail>,
        tasks: List<Task>,
        org: Organization,
        user: User
    ) {
        val to = user.email
        logger.info("Sending weekly snapshot to $to")

        val data = mapOf(
            "overdueCount" to overdueRequests.size,
            "overdueLink" to "$polarisDomain/requests/active",
            "openCount" to openRequests.size,
            "openLink" to "$polarisDomain/requests/active",
            "pendingCount" to pendingRequests.size,
            "pendingLink" to "$polarisDomain/requests/pending",
            "autoclosedCount" to autoclosedRequests.size,
            "closedLink" to "$polarisDomain/requests/closed",
            "receivedEmails" to receivedEmails.map { e -> serializePrivacyEmailForDigest(e) },
            "recipientName" to user.firstName,
            "organizationName" to org.name,
            "privacyEmailAddress" to (org.privacyEmailAddress() ?: org.anyPrivacyEmailAddress()),
            "tasks" to tasks.map { t -> serializeTaskForDigest(t) },
            "polarisDomain" to polarisDomain
        )

        val body = templatedBody("email/weekly-digest.ftl", data)
        val digestDateRange = getDigestDateRange()
        sendMessage(
            EmailType.WEEKLY_DIGEST,
            RequestRouting.unknown(),
            to,
            "Weekly Snapshot for ${org.name} – $digestDateRange",
            body,
            onBehalf = null,
            useEmpty = true,
            date = digestDateRange
        )
    }

    fun sendReconnectEmailToOrg(org: Organization) {
        val users = org.activeUsers()
        logger.info("Sending reconnect email to ${users.size} active users for ${org.name}...")
        users.forEach { orgUser ->
            sendReconnectEmail(org, orgUser.user!!)
        }
    }

    fun sendReconnectEmail(org: Organization, user: User) {
        val to = user.email
        logger.info("Sending reconnect email to $to")

        try {
            val privacyEmail = org.anyPrivacyEmailAddress()
            val reconnectUrl = "$polarisDomain/organization/${org.publicId}/settings/inbox"

            val model = mapOf("privacyEmail" to privacyEmail, "reconnectUrl" to reconnectUrl)
            val body = templatedBody("email/reconnect-email.ftl", model)

            sendMessage(
                EmailType.RECONNECT_EMAIL,
                RequestRouting.unknown(),
                to,
                "[IMPORTANT] Privacy Email Disconnected",
                body,
                null
            )
        } catch (e: Exception) {
            logger.warn("Exception occurred while sending reconnect email to $to: ${e.message}", e)
        }
    }

    private fun getDigestDateRange(): String {
        val firstDayOfWeek = DayOfWeek.MONDAY

        val now = ZonedDateTime.now()
        val start = now.with(TemporalAdjusters.previousOrSame(firstDayOfWeek))

        val startDateFormatter = DateTimeFormatter.ofPattern("MMM dd, yyyy")
        return startDateFormatter.format(start)
    }

    private fun serializeTaskForDigest(task: Task): Map<String, Any> {
        return mapOf(
            "label" to task.name,
            "due" to "",
            "link" to "$polarisDomain/tasks/todo",
            "color" to getTaskDueDateColor(task)
        )
    }

    private fun serializePrivacyEmailForDigest(email: PrivacyEmail): Map<String, Any> {
        val dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")

        return mapOf(
            "subject" to email.subject,
            "receivedDate" to dateFormatter.format(email.receivedAt),
            "createdPrivacyRequest" to email.createdPrivacyRequest
        )
    }

    private fun getTaskDueAgo(task: Task): String {
        if (task.dueAt == null) {
            return "No due date"
        }

        val dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
        return dateFormatter.format(task.dueAt)
    }

    private fun getTaskDueDateColor(task: Task): String {
        if (task.dueAt == null) {
            return "gray"
        }

        val now = ZonedDateTime.now()
        if (task.dueAt!!.isBefore(now)) {
            return "red"
        } else if (ChronoUnit.DAYS.between(now, task.dueAt!!) < 7) {
            return "blue"
        }

        return "gray"
    }

    fun getInternalNewSubjectRequestBody(org: Organization, user: User): String {
        return templatedBody("email/subject-request/internal-new-subject-request.ftlh", mapOf("privacyEmail" to org.privacyEmailAddress()))
    }

    fun sendInternalNewSubjectRequest(org: Organization, user: User, after: String? = null) {
        val to = user.email
        val body = getInternalNewSubjectRequestBody(org, user)

        sendMessage(
            EmailType.INTERNAL_NEW_SUBJECT_REQUEST,
            RequestRouting.unknown(),
            to,
            "CCPA Email Request Added to Polaris",
            body,
            onBehalf = null,
            useEmpty = false,
            after = after
        )
    }

    fun getSubjectRequestNotFoundBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(actor, request, CustomMessageTypeEnum.ClosedNotFound)
        return templatedBody(CustomMessageTypeEnum.ClosedNotFound.template, params.model)
    }

    fun sendSubjectRequestNotFound(actor: User, request: DataSubjectRequest): EmailResponse {
        val params = subjectRequestEmailParams(actor, request, CustomMessageTypeEnum.ClosedNotFound)
        val body = getSubjectRequestNotFoundBody(actor, request)

        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_NOT_FOUND,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    fun getSubjectRequestServiceProviderBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(actor, request, CustomMessageTypeEnum.ClosedServiceProvider)
        return templatedBody("email/subject-request/closed-service-provider.ftl", params.model)
    }

    fun sendSubjectRequestServiceProvider(actor: User, request: DataSubjectRequest): EmailResponse {
        val params = subjectRequestEmailParams(actor, request, CustomMessageTypeEnum.ClosedServiceProvider)
        val body = getSubjectRequestServiceProviderBody(actor, request)
        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_SERVICE_PROVIDER,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    fun getSubjectRequestClosedOptOutBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(actor, request, CustomMessageTypeEnum.ClosedOptOut)
        return templatedBody(CustomMessageTypeEnum.ClosedOptOut.template, params.model)
    }

    fun sendSubjectRequestClosedOptOut(actor: User, request: DataSubjectRequest): EmailResponse {
        val params = subjectRequestEmailParams(actor, request, CustomMessageTypeEnum.ClosedOptOut)
        val body = getSubjectRequestClosedOptOutBody(actor, request)
        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_OPT_OUT,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    fun getSubjectRequestClosedDeleteBody(actor: User, request: DataSubjectRequest): String {
        val retentionReasons = calculateRetentionReasons(request.organization!!, DataRequestInstructionType.DELETE)
        val noContactReasons = calculateNoContactReasons(request.organization, DataRequestInstructionType.DELETE, request)
        val intentionalInteractionVendors = calculateIntentionalInteractionVendors(request.organization)
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedDelete,
            mapOf(
                "retentionReasons" to retentionReasons,
                "noContactReasons" to noContactReasons,
                "intentionalInteractionVendors" to intentionalInteractionVendors
            )
        )
        return templatedBody("email/subject-request/closed-delete.ftlh", params.model)
    }

    fun getSubjectRequestClosedNotVerifiedBody(request: DataSubjectRequest): String {
        val messageType =
            if (request.isDelete()) CustomMessageTypeEnum.ClosedDeleteNotVerified else CustomMessageTypeEnum.ClosedKnowNotVerified
        val params = subjectRequestEmailParams(null, request, messageType)
        return templatedBody(messageType.template, params.model)
    }

    fun sendSubjectRequestClosedNotVerifiedBody(request: DataSubjectRequest): EmailResponse {
        val emailType = if (request.isDelete()) {
            EmailType.REQUEST_PROCESSING_CLOSED_DELETE_NOT_VERIFIED
        } else {
            EmailType.REQUEST_PROCESSING_CLOSED_KNOW_NOT_VERIFIED
        }
        val messageType = if (request.isDelete()) {
            CustomMessageTypeEnum.ClosedDeleteNotVerified
        } else {
            CustomMessageTypeEnum.ClosedKnowNotVerified
        }
        val params = subjectRequestEmailParams(null, request, messageType)
        val body = getSubjectRequestClosedNotVerifiedBody(request)

        return sendMessage(
            emailType,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    fun getServiceProviderRequestToDeleteBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(actor, request, null)
        return templatedBody("email/subject-request/service-provider-request-to-delete.ftl", params.model)
    }

    fun getServiceProviderRequestToLimitBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(actor, request, null)
        return templatedBody("email/subject-request/service-provider-request-to-limit.ftl", params.model)
    }

    fun getServiceProviderRequestSubject(request: DataSubjectRequest): String {
        return "${request.requestType.regulationName()} ${request.requestType.longLabel}"
    }

    fun sendSubjectRequestClosedDelete(actor: User, request: DataSubjectRequest): EmailResponse {
        val retentionReasons = calculateRetentionReasons(request.organization!!, DataRequestInstructionType.DELETE)
        val noContactReasons = calculateNoContactReasons(request.organization, DataRequestInstructionType.DELETE, request)
        val intentionalInteractionVendors = calculateIntentionalInteractionVendors(request.organization)
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedDelete,
            mapOf(
                "retentionReasons" to retentionReasons,
                "noContactReasons" to noContactReasons,
                "intentionalInteractionVendors" to intentionalInteractionVendors
            )
        )
        val body = getSubjectRequestClosedDeleteBody(actor, request)
        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_DELETE,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    fun getSubjectRequestClosedKnowBody(actor: User, request: DataSubjectRequest): String {
        val sensitivePic = calculateConsumerSensitivePersonalInformation(request.organization!!)
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedKnow,
            mapOf("sensitivePic" to sensitivePic)
        )
        return templatedBody("email/subject-request/closed-know.ftlh", params.model)
    }

    fun getSubjectRequestClosedPortabilityBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedPortability
        )
        return templatedBody("email/subject-request/closed-portability.ftlh", params.model)
    }

    fun getSubjectRequestClosedRectificationBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedRectification
        )
        return templatedBody("email/subject-request/closed-rectification.ftlh", params.model)
    }

    fun getSubjectRequestClosedWithdrawnConsentBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedWithdrawnConsent
        )
        return templatedBody("email/subject-request/closed-withdrawn-request.ftlh", params.model)
    }

    fun getSubjectRequestClosedObjectionBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedObjection
        )
        return templatedBody("email/subject-request/closed-objection.ftlh", params.model)
    }

    fun getSubjectRequestClosedLimitBody(actor: User, request: DataSubjectRequest): String {
        val params = subjectRequestEmailParams(actor, request, CustomMessageTypeEnum.ClosedLimit)
        return templatedBody("email/subject-request/closed-limit.ftlh", params.model)
    }

    fun sendSubjectRequestClosedLimit(
        actor: User,
        request: DataSubjectRequest
    ): EmailResponse {
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedLimit
        )
        val body = getSubjectRequestClosedLimitBody(actor, request)
        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_LIMIT,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    fun sendSubjectRequestClosedKnow(
        actor: User,
        request: DataSubjectRequest,
        attachment: List<MultipartFile>
    ): EmailResponse {
        val sensitivePic = calculateConsumerSensitivePersonalInformation(request.organization!!)
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedKnow,
            mapOf("sensitivePic" to sensitivePic)
        )
        val body = getSubjectRequestClosedKnowBody(actor, request)
        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_KNOW,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            attachment,
            onBehalf = request.organization
        )
    }

    fun sendSubjectRequestClosedPortability(
        actor: User,
        request: DataSubjectRequest,
        attachment: List<MultipartFile>
    ): EmailResponse {
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedPortability
        )
        val body = getSubjectRequestClosedPortabilityBody(actor, request)
        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_PORTABILITY,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            attachment,
            onBehalf = request.organization
        )
    }

    fun sendSubjectRequestClosedRectification(
        actor: User,
        request: DataSubjectRequest
    ): EmailResponse {
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedRectification
        )
        val body = getSubjectRequestClosedRectificationBody(actor, request)
        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_RECTIFICATION,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    fun sendSubjectRequestClosedWithdrawnConsent(
        actor: User,
        request: DataSubjectRequest
    ): EmailResponse {
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedWithdrawnConsent
        )
        val body = getSubjectRequestClosedWithdrawnConsentBody(actor, request)
        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_WITHDRAWN_CONSENT,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    fun sendSubjectRequestClosedObjection(
        actor: User,
        request: DataSubjectRequest
    ): EmailResponse {
        val params = subjectRequestEmailParams(
            actor,
            request,
            CustomMessageTypeEnum.ClosedObjection
        )
        val body = getSubjectRequestClosedObjectionBody(actor, request)
        return sendMessage(
            EmailType.REQUEST_PROCESSING_CLOSED_OBJECTION,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    private fun calculateConsumerSensitivePersonalInformation(org: Organization): String? {
        val pics = subjectRequestRelationService.collectedPIC(org, true)
        val joinedPic = pics.filter { it.isSensitive }.mapNotNull { it.name }.joinToString("; ")

        return joinedPic.ifEmpty { null }
    }

    private fun calculateRetentionReasons(
        organization: Organization,
        instructionType: DataRequestInstructionType
    ): String? {
        val retentionReasons = subjectRequestRelationService.retentionReasons(organization, instructionType)
        logger.info("For org: ${organization.name}, calculated $instructionType retention reasons: $retentionReasons")
        var retentionReasonJoin: String? = null

        if (retentionReasons.isNotEmpty()) {
            retentionReasonJoin = retentionReasons.distinct().joinToString(separator = "; ")
        }

        return retentionReasonJoin
    }

    private fun calculateNoContactReasons(
        organization: Organization,
        instructionType: DataRequestInstructionType,
        request: DataSubjectRequest?
    ): String? {
        if (request != null && request.requestType.regulation != Regulation.CCPA) {
            return null
        }

        val noContact = subjectRequestRelationService.noContactReasons(organization, instructionType)
        var joined: String? = null

        if (noContact.isNotEmpty()) {
            joined = noContact.distinct().joinToString(separator = "; ")
        }

        return joined
    }

    private fun calculateIntentionalInteractionVendors(
        organization: Organization
    ): String? {
        val intentionalInteractionVendors = subjectRequestRelationService.intentionalInteractionVendors(organization)
        logger.info("For org: ${organization.name}, calculated intentional interaction vendors: $intentionalInteractionVendors")
        var joined: String? = null

        if (intentionalInteractionVendors.isNotEmpty()) {
            joined = intentionalInteractionVendors.distinct().joinToString(separator = ", ")
        }

        return joined
    }

    private fun subjectRequestEmailParams(
        actor: User?,
        request: DataSubjectRequest,
        customMessageType: CustomMessageTypeEnum?,
        mapExtras: Map<String, Any?> = emptyMap()
    ): SubjectRequestEmail {
        val to: String = request.subjectEmailAddress!!
        val requestId: String = request.publicId

        val org = request.organization!!
        val privacyCenter = org.privacyCenters.firstOrNull()
        val optOutPageType = optOutService.getOptOutPageType(org)

        val customMessage = if (customMessageType != null) customMessageRepo.findByOrganizationAndMessageType(
            org,
            customMessageType
        )?.customText else null

        val firstName = request.subjectFirstName ?: ""
        val lastName = request.subjectLastName ?: ""
        val fullName = "$firstName $lastName"
        val type = request.requestType.label
        val longType = request.requestType.longLabel
        val isGdpr = request.requestType.isGdpr()
        val regulationName = request.requestType.regulationName()
        val requestFormUrl = privacyCenter?.requestFormUrl
        val requestEmail = privacyCenter?.requestEmail
        val requestDoNotSellUrl = privacyCenter?.getDoNotSellUrl()
        val optOutLabel = optOutService.getOptOutPageLabel(optOutPageType) ?: ""
        val optOutUrl = privacyCenter?.getUrl(optOutService.getOptOutPath(optOutPageType) ?: "")
        val signature =
            signatureTemplateMessageService.organizationSignatureMessageTemplate(org, actor).replace("\n", "<br />")
        val dueDateDays = request.requestType.initialDueDateDays
        val extensionDays = request.requestType.extendDueDateDays

        val model = mapOf(
            "firstName" to firstName,
            "lastName" to lastName,
            "fullName" to fullName,
            "subjectEmailAddress" to request.subjectEmailAddress,
            "processingActivites" to (request.consentWithdrawnRequest ?: emptyList<String>()),
            "type" to type,
            "longType" to longType,
            "signature" to signature,
            "customText" to customMessage,
            "requestFormUrl" to requestFormUrl,
            "requestEmail" to requestEmail,
            "requestDoNotSellUrl" to requestDoNotSellUrl,
            "optOutLabel" to optOutLabel,
            "optOutUrl" to optOutUrl,
            "isGdpr" to isGdpr,
            "regulationName" to regulationName,
            "dueDateDays" to dueDateDays,
            "extensionDays" to extensionDays

        )
        val subject = "A message about your request [$requestId]"

        return SubjectRequestEmail(to = to, subject = subject, model = model + mapExtras)
    }

    data class SubjectRequestEmail(
        val to: String,
        val subject: String,
        val model: Map<String, Any?>
    )

    fun getExtendDueDateBody(actor: User, request: DataSubjectRequest, notice: String): String {
        val params = subjectRequestEmailParams(actor, request, null, mapOf("noticeMsg" to notice))
        return templatedBody("email/subject-request/extend-due-date.ftl", params.model)
    }

    fun sendExtendDueDate(actor: User, request: DataSubjectRequest, notice: String): EmailResponse {
        val params = subjectRequestEmailParams(actor, request, null, mapOf("noticeMsg" to notice))
        val body = getExtendDueDateBody(actor, request, notice)

        return sendMessage(
            EmailType.REQUEST_PROCESSING_DUE_DATE_EXTENDED,
            RequestRouting.unknown(),
            params.to,
            params.subject,
            body,
            null,
            onBehalf = request.organization
        )
    }

    fun sendEmailAddressVerification(request: DataSubjectRequest) {
        val org = request.organization
        val emailAddress = request.subjectEmailAddress

        if (org != null && emailAddress != null) {
            val privacyCenter = org.privacyCenters.firstOrNull()

            val data = mapOf(
                "orgName" to org.name,
                "confirmationUrl" to privacyCenter!!.getEmailConfirmationUrl(request.confirmationToken),
                "type" to request.requestType.label,
                "dueDateDays" to request.requestType.initialDueDateDays
            )

            val subject = "Verify Email Address for ${org.name} ${request.requestType!!.label}"
            val body = templatedBody("email/email-verification.ftl", data)

            sendMessages(
                EmailType.EMAIL_VERIFICATION,
                RequestRouting.unknown(),
                listOf(emailAddress),
                subject,
                body,
                onBehalf = request.organization
            )
        }
    }

    fun sendNewRequestByPhoneNotification(request: PhoneDataRequestDto) {
        val recipients = getOrganizationRecipients(request.org)
        val dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
        if (recipients.isNotEmpty()) {
            val model = mapOf(
                "requestType" to (request.getRequestType()?.label ?: "Unknown Request Type"),
                "dateSubmitted" to dateFormatter.format(request.requestDate),
                "organization" to request.org.name,
                "consumerPhoneNumber" to PhoneNumberUtil.formatPhoneNumber(request.consumerPhoneNumber, com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL),
                "consumerNameRecording" to request.consumerNameRecording,
                "consumerEmailRecording" to request.consumerEmailRecording,
                "polarisDomain" to polarisDomain
            )

            val body = templatedBody("email/phone-request-notification.ftl", model)

            sendMessages(
                EmailType.NEW_REQUEST_NOTIFICATION,
                RequestRouting.unknown(),
                recipients,
                "New Voice Privacy Request",
                body
            )
        }
    }

    fun sendOnboardingEmail(
        request: DataSubjectRequest
    ) {

        val org = request.organization
        if (org != null) {
            val recipients = getOrganizationRecipients(org)
            if (recipients.isNotEmpty()) {
                val data = mapOf(
                    "org_name" to org.name
                )
                val message = templatedBody(
                    "email/walkthrough-invite.ftl",
                    data
                )

                sendMessages(
                    EmailType.WALKTHROUGH_INVITE,
                    RequestRouting.unknown(),
                    recipients,
                    "Privacy Request Walkthrough",
                    message
                )
            }
        }
    }

    fun sendNewDataSubjectRequestNotification(request: DataSubjectRequest, source: String) {
        if (!shouldNewSendDSRNotification(request)) {
            return
        }

        val org = request.organization
        if (org != null) {
            val recipients = getOrganizationRecipients(org)
            if (recipients.isNotEmpty()) {
                val dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
                val requestURL = "$polarisDomain/requests/${request.publicId}"
                val model = mapOf(
                    "messageText" to "You have a new privacy request to process.",
                    "requestID" to request.publicId,
                    "requestURL" to requestURL,
                    "requestType" to request.requestType!!.label,
                    "requestDate" to dateFormatter.format(request.requestDate),
                    "dueDate" to dateFormatter.format(request.requestDueDate),
                    "source" to source,
                    "organization" to org.name
                )
                val body = templatedBody("email/request-notification.ftl", model)

                sendMessages(
                    EmailType.NEW_REQUEST_NOTIFICATION,
                    RequestRouting.unknown(),
                    recipients,
                    "[TrueVault Polaris] New Privacy Request",
                    body
                )
            }
        }
    }

    fun shouldNewSendDSRNotification(request: DataSubjectRequest): Boolean {
        val typeQualifies = request.requestType == DataRequestType.CCPA_OPT_OUT
        val stateQualifies = request.state != DataRequestState.PENDING_EMAIL_VERIFICATION && request.state != DataRequestState.CLOSED

        if (!typeQualifies && !stateQualifies) {
            return false
        }

        if (shouldSuppressDSRNotificationForOrg(request.organization)) {
            logger.info("Notification Email suppressed for ${request.organization?.name}: id=${request.publicId} type=${request.requestType}, state=${request.state}")
            return false
        }

        return true
    }

    private fun shouldSuppressDSRNotificationForOrg(organization: Organization?): Boolean {
        // HACK: POLARIS-2492 this is a short-term fix for Clare V
        // TODO: replace with actual user preferences POLARIS-1211
        val notificationSuppressionList = listOf(
            "53faa226-6996-4310-b4f4-84cfaa6900cc" // Clare V. LLC
        )

        return notificationSuppressionList.contains(organization?.id.toString())
    }

    fun sendNewDataSubjectRequestMessageNotification(request: DataSubjectRequest) {
        val org = request.organization
        if (org != null) {
            val recipients = getOrganizationRecipients(org)
            if (recipients.isNotEmpty()) {
                val dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
                val requestURL = "$polarisDomain/requests/${request.publicId}"
                val model = mapOf(
                    "messageText" to "A new message was received on a Privacy Request.",
                    "requestID" to request.publicId,
                    "requestURL" to requestURL,
                    "requestType" to request.requestType!!.label,
                    "requestDate" to dateFormatter.format(request.requestDate),
                    "dueDate" to dateFormatter.format(request.requestDueDate),
                    "organization" to org.name
                )
                val body = templatedBody("email/request-notification.ftl", model)

                sendMessages(
                    EmailType.NEW_REQUEST_MESSAGE_NOTIFICATION,
                    RequestRouting.unknown(),
                    recipients,
                    "[TrueVault Polaris] New Message on a Privacy Request",
                    body
                )
            }
        }
    }

    fun sendNewPrivacyInboxMessageNotification(mailbox: OrganizationMailbox, from: String, subject: String, mineParseError: Boolean? = false) {
        val org = mailbox.organization!!
        val recipients = getOrganizationRecipients(org)
        if (recipients.isNotEmpty()) {
            val model = mapOf(
                "email" to mailbox.mailbox,
                "from" to from,
                "subject" to subject,
                "mineParseError" to mineParseError
            )
            val body = templatedBody("email/new-privacy-inbox-message-notification.ftl", model)

            sendMessages(
                EmailType.NEW_INBOX_MESSAGE_NOTIFICATION,
                RequestRouting.unknown(),
                recipients,
                "[TrueVault Polaris] New Message In Your Privacy Inbox from $from",
                body
            )
        }
    }

    fun getRequestMessageHiddenPrefix() = "message-requestId:"

    fun sendTestEmail(user: User, organization: Organization, body: String): EmailResponse {
        val contentHtml = body.replace("\n", "<br>")
        val headerImageUrl = organization.getFullHeaderImageUrl()
        val model = mapOf("content" to contentHtml, "headerImageUrl" to headerImageUrl)
        val bodyHtml = templatedBody("email/test-email.ftl", model)
        return sendMessage(
            EmailType.TEST_MESSAGE,
            RequestRouting.unknown(),
            user.email,
            "[TrueVault Polaris Test Email]",
            bodyHtml,
            null,
            null,
            organization
        )
    }

    fun sendIntroSurveyEmailToStaff(
        organization: Organization,
        surveyQuestions: List<OrganizationSurveyQuestion>
    ): EmailResponse {
        val body =
            templatedBody(
                "email/intro-survey-to-staff.ftl",
                mapOf("questions" to sortedSurveyQuestions(surveyQuestions))
            )
        return sendMessage(
            EmailType.INTRO_SURVEY_MESSAGE_TO_STAFF,
            RequestRouting.unknown(),
            productEmail,
            "Intro Survey Responses for ${organization.name}: ${organization.publicId}",
            body
        )
    }

    fun sendRequestGeneralEmail(
        request: DataSubjectRequest,
        to: String,
        from: String? = null,
        subject: String,
        message: String
    ): EmailResponse {
        val fromAddress = from ?: "$fromUser@$fromDomain"
        val onBehalf = request.organization
        val templateHtml = if (onBehalf != null) {
            baseTemplateOnBehalf(message, onBehalf.physicalAddress, onBehalf.getFullHeaderImageUrl())
        } else {
            baseTemplate(message)
        }

        logger.info("Sending general to: $to, from $fromAddress")

        val emailContext = EmailContext(
            to,
            constructRouting(RequestRouting.unknown()),
            fromAddress,
            fromName,
            subject,
            templateHtml,
            null,
            null,
            request.organization
        )
        return emailSender.send(emailContext)
    }

    fun previewEmail(org: Organization, actor: User, messageType: CustomMessageTypeEnum): String {
        val privacyCenter = org.privacyCenters.firstOrNull()

        val params = messageType.previewParams.toMutableMap()

        if (params.containsKey("requestFormUrl") && privacyCenter != null) {
            params["requestFormUrl"] = privacyCenter.getPrivacyRequestUrl()
        }

        if (params.containsKey("requestEmail") && privacyCenter?.requestEmail != null) {
            params["requestEmail"] = privacyCenter.requestEmail!!
        }

        if (params.containsKey("optOutLabel") && privacyCenter != null) {
            params["optOutLabel"] = optOutService.getOptOutPageLabel(privacyCenter) ?: ""
        }

        if (params.containsKey("optOutUrl") && privacyCenter != null) {
            params["optOutUrl"] = optOutService.getOptOutUrl(privacyCenter) ?: ""
        }

        if (params.containsKey("signature")) {
            val signature =
                signatureTemplateMessageService.organizationSignatureMessageTemplate(org, actor).replace("\n", "<br />")
            params["signature"] = signature
        }

        if (params.containsKey("isGdpr")) {
            params["isGdpr"] = params["regulationName"] == "GDPR"
        }

        if (messageType == CustomMessageTypeEnum.ClosedDelete) {
            val retentionReasons = calculateRetentionReasons(org, DataRequestInstructionType.DELETE)
            val noContactReasons = calculateNoContactReasons(org, DataRequestInstructionType.DELETE, null)
            val intentionalInteractionVendors = calculateIntentionalInteractionVendors(org)
            if (retentionReasons != null) {
                params["retentionReasons"] = retentionReasons
            }
            if (noContactReasons != null) {
                params["noContactReasons"] = noContactReasons
            }
            if (intentionalInteractionVendors != null) {
                params["intentionalInteractionVendors"] = intentionalInteractionVendors
            }
        }

        if (messageType == CustomMessageTypeEnum.ClosedKnow) {
            val sensitivePic = calculateConsumerSensitivePersonalInformation(org)
            if (sensitivePic != null) {
                params["sensitivePic"] = sensitivePic
            }
        }

        return templatedBody(messageType.template, params)
    }

    // I'm not sure if it'll be nicer longterm to do this, or to implement multiple addresses for each sender. @NOAH: How do our live senders behave here?
    private fun sendMessages(
        type: EmailType,
        requestRouting: RequestRouting,
        toList: List<String>,
        subject: String,
        html: String,
        plaintext: String? = null,
        attachments: List<MultipartFile>? = null,
        onBehalf: Organization? = null
    ): List<EmailResponse> {
        return toList.map { to ->
            sendMessage(type, requestRouting, to, subject, html, plaintext, attachments, onBehalf)
        }
    }

    private fun sendMessage(
        type: EmailType,
        requestRouting: RequestRouting,
        to: String,
        subject: String,
        html: String,
        plaintext: String? = null,
        attachments: List<MultipartFile>? = null,
        onBehalf: Organization? = null,
        useEmpty: Boolean = false,
        date: String? = null,
        after: String? = null
    ): EmailResponse {
        val fromAddress = "$fromUser@$fromDomain"

        val templateHtml = if (onBehalf != null && !useEmpty) {
            baseTemplateOnBehalf(html, onBehalf.physicalAddress, onBehalf.getFullHeaderImageUrl())
        } else if (!useEmpty) {
            baseTemplate(html, date, after)
        } else {
            baseTemplateEmpty(html, date, after)
        }

        logger.info("Sending email '$type' to: $to, from $fromAddress")

        val emailContext = EmailContext(
            to,
            constructRouting(requestRouting),
            fromAddress,
            fromName,
            subject,
            templateHtml,
            plaintext,
            attachments,
            onBehalf
        )

        return emailSender.send(emailContext)
    }

    private fun getOrganizationRecipients(organization: Organization) =
        organization.organizationUsers.filter {
            it.role == UserRole.ADMIN && it.status.isActive() &&
                it.user?.email != null
        }.mapNotNull { it.user?.email }

    private fun constructRouting(requestRouting: RequestRouting): String? {
        return when (requestRouting.type) {
            InboundType.DATA_REQUEST -> "r${requestRouting.id}"
            InboundType.UNKNOWN -> null
        }
    }

    private fun baseTemplate(body: String, date: String? = null, after: String? = null): String {
        val model = mapOf(
            "body" to body,
            "year" to getYear(),
            "date" to date,
            "polarisDomain" to polarisDomain,
            "after" to after
        )
        return templatedBody("email/base.ftl", model)
    }

    private fun getYear(): String {
        return SimpleDateFormat("yyyy").format(Date())
    }

    private fun baseTemplateOnBehalf(body: String, physicalAddress: String, headerImageUrl: String): String {
        val model = mapOf("body" to body, "physicalAddress" to physicalAddress, "headerImageUrl" to headerImageUrl, "polarisDomain" to polarisDomain)
        return templatedBody("email/base-on-behalf.ftl", model)
    }

    private fun baseTemplateEmpty(body: String, date: String? = null, after: String? = null): String {
        val model = mapOf("body" to body, "year" to getYear(), "date" to date, "polarisDomain" to polarisDomain, "after" to after)
        return templatedBody("email/base-empty.ftl", model)
    }

    private fun templatedBody(templateName: String, model: Map<String, Any?>): String {
        val tpl = FreeMarkerTemplateUtils.processTemplateIntoString(
            freemarkerConfig.getTemplate(templateName), model
        )

        return try {
            val tmpOut = Files.createTempFile("email-out", ".html")
            val tmpIn = Files.createTempFile("email-in", ".html")
            Files.write(tmpIn, tpl.toByteArray(StandardCharsets.UTF_8))
            runCommand("juice --apply-attributes-table-elements false --web-resources-images false $tmpIn $tmpOut")

            val out = Files.readString(tmpOut)
            Files.delete(tmpIn)
            Files.delete(tmpOut)
            out
        } catch (e: java.io.IOException) {
            tpl
        }
    }

    // Taken from https://gist.github.com/seanf/58b76e278f4b7ec0a2920d8e5870eed6
    private fun runCommand(cmd: String, workingDir: File? = null) {
        val process = ProcessBuilder(*cmd.split(" ").toTypedArray())
            .directory(workingDir)
            .redirectOutput(Redirect.INHERIT)
            .redirectError(Redirect.INHERIT)
            .start()

        if (!process.waitFor(10, TimeUnit.SECONDS)) {
            process.destroy()
            throw RuntimeException("execution timed out: $this")
        }
        if (process.exitValue() != 0) {
            throw RuntimeException("execution failed with code ${process.exitValue()}: $this")
        }
    }
}
