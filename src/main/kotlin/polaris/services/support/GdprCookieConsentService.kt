package polaris.services.support

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import inet.ipaddr.AddressStringException
import inet.ipaddr.IPAddressString
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.entity.GdprCookieConsentState
import polaris.models.entity.GdprCookieConsentTracking
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.repositories.GdprCookieConsentTrackingRepo
import polaris.specs.GdprCookieConsentTrackingSpecs
import java.util.UUID

enum class CookieConsentSortColumn {
    visitorId,
    date,
}

data class CookieConsentSort(
    val col: CookieConsentSortColumn,
    val desc: Boolean?
)

@Service
class GdprCookieConsentService(
    private val gdprCookieConsentTrackingRepo: GdprCookieConsentTrackingRepo,
    private val lookup: LookupService
) {

    fun addCookieConsent(privacyCenter: PrivacyCenter, key: String, consentState: GdprCookieConsentState, sourceUrl: String, ip: String, userAgent: String?) {
        val ipv4 = calculateMaskedIpV4Value(ip)
        val ipv6 = calculateMaskedIpV6Value(ip)

        if (ipv4 == null && ipv6 == null) {
            // abort
        }

        if (sourceUrl.isBlank()) {
            // abort
        }

        gdprCookieConsentTrackingRepo.save(
            GdprCookieConsentTracking(
                id = UUID.randomUUID(),
                organization = privacyCenter.organization,
                key = key,
                consentState = jacksonObjectMapper().writeValueAsString(consentState),
                sourceUrl = sourceUrl,
                userAgent = userAgent,
                ipV4 = ipv4,
                ipV6 = ipv6
            )
        )
    }

    fun paginatedFindForOrganization(
        organizationId: OrganizationPublicId,
        sort: CookieConsentSort? = null,
        page: Int = 0,
        per: Int = 15
    ): Page<GdprCookieConsentTracking> {
        val sortCol = columnForSortParam(sort?.col)
        val sortDesc = sort?.desc ?: true
        val querySort = if (sortDesc) Sort.by(sortCol).descending() else Sort.by(sortCol).ascending()

        val pageable: Pageable = PageRequest.of(page, per.coerceAtMost(100), querySort)
        val organization = lookup.orgOrThrow(organizationId)

        val spec = GdprCookieConsentTrackingSpecs.forOrganization(organization)

        return gdprCookieConsentTrackingRepo.findAll(spec, pageable)
    }

    private fun columnForSortParam(param: CookieConsentSortColumn?): String {
        return when (param) {
            CookieConsentSortColumn.date -> "createdAt"
            CookieConsentSortColumn.visitorId -> "key"
            else -> { "createdAt" }
        }
    }

    fun calculateMaskedIpV4Value(ip: String): String? {
        if (ip.isBlank()) {
            return null
        }
        try {
            val ipAddrStr = IPAddressString(ip)
            val ipAddr = ipAddrStr.toAddress()
            if (ipAddr.isIPv4) {
                val masked = ipAddr.mask(IPAddressString("255.255.0.0").toAddress())
                return masked.toString()
            }
        } catch (e: AddressStringException) {
            return null
        }
        return null
    }

    fun calculateMaskedIpV6Value(ip: String): String? {
        if (ip.isBlank()) {
            return null
        }
        try {
            val ipAddrStr = IPAddressString(ip)
            val ipAddr = ipAddrStr.toAddress()
            if (ipAddr.isIPv6) {
                val masked = ipAddr.mask(IPAddressString("ffff:ffff:ffff:ffff:ffff:ffff:ff00:0000").toAddress())
                return masked.toString()
            }
        } catch (e: AddressStringException) {
            return null
        }
        return null
    }
}
