package polaris.services.support

import org.springframework.stereotype.Service
import polaris.models.dto.orgDraftSchema.v1.OrgDraftSchemaV1
import polaris.models.entity.organization.OrgDraftSchemaVersion
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDraft
import polaris.repositories.OrganizationDraftRepository
import kotlin.reflect.KMutableProperty

@Service
class OrgDraftService(private val orgDraftRepo: OrganizationDraftRepository) {

    fun getDraft(org: Organization, expectedVersion: OrgDraftSchemaVersion): OrganizationDraft {
        // todo: current draft functionality of org doesn't appear to be working. When it's working, replace this
        return orgDraftRepo.findAllByOrganization(org).firstOrNull() ?: OrganizationDraft(
            organization = org,
            schemaVersion = expectedVersion,
            data = OrgDraftSchemaV1()
        )

        // todo: when we add V2 and future support schemas, we'll need to support either:
        //  - auto-migrating the old draft to the newest version
        //  - discarding the old draft, and automatically generating the newest version
        //  - throwing an exception
    }

    fun persistDraft(draft: OrganizationDraft) {
        orgDraftRepo.save(draft)
    }

    fun serializeSurveyAnswer(answer: String?, property: KMutableProperty<*>): Any? {
        if (answer == null) {
            return null
        }

        if (property.returnType.classifier == Boolean::class) {
            return serializeBooleanSurveyAnswer(answer)
        }

        return answer
    }

    private fun serializeBooleanSurveyAnswer(answer: String?): Boolean? {
        return when (answer) {
            "true" -> true
            "false" -> false
            null -> null
            else -> null
        }
    }
}
