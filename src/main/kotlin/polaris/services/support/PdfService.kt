package polaris.services.support

import com.github.jhonnymertz.wkhtmltopdf.wrapper.Pdf
import com.github.jhonnymertz.wkhtmltopdf.wrapper.configurations.WrapperConfig
import com.github.jhonnymertz.wkhtmltopdf.wrapper.params.Param
import freemarker.template.Configuration
import org.springframework.stereotype.Service
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

@Service
class PdfService(
    private val freemarkerConfig: Configuration
) {

    fun generatePdf(html: String): File {
        val tmpOut = Files.createTempFile("pdf-out", ".pdf")
        val pdf = Pdf(WrapperConfig("wkhtmltopdf"))
        pdf.addPageFromString(html)
        pdf.addParam(Param("--enable-local-file-access"))
        pdf.saveAs(tmpOut.toString())
        return tmpOut.toFile()
    }

    fun pdfFromTemplate(templateName: String, model: Map<String, Any?>): File {
        val html = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfig.getTemplate(templateName), model)
        val cssPath = Paths.get("/polaris/src/main/resources/static/assets/print.css")
        val logoPath = Paths.get("/polaris/src/main/resources/static/assets/images/truevault-logo-print.png")
        val wrappedHtml = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfig.getTemplate("pdfs/base.ftl"), mapOf("body" to html, "cssPath" to cssPath.toString(), "logoPath" to logoPath.toString()))
        return generatePdf(wrappedHtml)
    }
}
