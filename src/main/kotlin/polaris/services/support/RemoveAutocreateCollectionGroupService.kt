package polaris.services.support

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.organization.Organization
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.OrganizationRepository
import polaris.services.primary.CollectionGroupService

@Service
@Transactional
class RemoveAutocreateCollectionGroupService(
    private val cgRepo: CollectionGroupRepository,
    private val orgRepo: OrganizationRepository,
    private val collectionGroupService: CollectionGroupService
) {
    fun removeAutocreatedGroup(surveySlug: String, org: Organization) {
        val autocreateSlug = when (surveySlug) {
            "business-employees-ca" -> AutocreatedCollectionGroupSlug.CA_EMPLOYEES
            "business-contractors-ca" -> AutocreatedCollectionGroupSlug.CA_CONTRACTORS
            "business-job-applicants-ca" -> AutocreatedCollectionGroupSlug.CA_JOB_APPLICANTS
            "employees-eea-uk" -> AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE
            else -> null
        }

        if (autocreateSlug != null) {
            removeAutocreatedGroup(autocreateSlug, org)
        }
    }

    fun removeAutocreatedGroup(slug: AutocreatedCollectionGroupSlug, org: Organization) {
        val groups = cgRepo.findAllByOrganization(org).filter { g -> g.autocreationSlug == slug }
        for (group in groups) {
            collectionGroupService.delete(org.publicId, group.id)
        }
        org.autocreatedCollectionGroupSlugs = (org.autocreatedCollectionGroupSlugs - slug).distinct()
        orgRepo.save(org)
    }
}
