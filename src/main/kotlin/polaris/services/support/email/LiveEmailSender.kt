package polaris.services.support.email

import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import polaris.models.entity.organization.SendMethod

@Component
@Profile("live-email")
@Primary
class LiveEmailSender(
    private val sendGridEmailSender: SendGridEmailSender,
    private val nylasEmailSender: NylasEmailSender,
    private val awssesEmailSender: AWSSESEmailSender
) : EmailSender {
    override fun send(context: EmailContext): EmailResponse {
        return when (if (context.onBehalf == null) SendMethod.SES else context.onBehalf.sendMethod) {
            SendMethod.SENDGRID -> sendGridEmailSender.send(context)
            SendMethod.NYLAS -> nylasEmailSender.send(context)
            SendMethod.SES -> awssesEmailSender.send(context)
        }
    }
}
