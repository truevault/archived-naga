package polaris.services.support.email

data class EmailResponse(
    var statusCode: Int = 0,
    var responseBody: String = "",
    var responseHeaders: Map<String, String> = mutableMapOf()
)
