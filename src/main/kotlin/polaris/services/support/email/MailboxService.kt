package polaris.services.support.email

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.controllers.PrivacyInboxInUseException
import polaris.controllers.util.LookupService
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.MailboxType
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.organization.SendMethod
import polaris.repositories.OrganizationMailboxRepository
import polaris.services.primary.PrivacyCenterService
import polaris.services.primary.UserService
import polaris.services.support.EmailService
import java.util.UUID

@Service
class MailboxService(
    private val nylasService: NylasService,

    private val organizationMailboxRepository: OrganizationMailboxRepository,
    private val privacyCenterService: PrivacyCenterService,
    private val userService: UserService,
    private val emailService: EmailService,
    private val lookupService: LookupService

) {

    companion object {
        private val log = LoggerFactory.getLogger(MailboxService::class.java)
    }

    fun getHostedAuthUrl(organization: Organization): String? {
        return when (organization.sendMethod) {
            SendMethod.SENDGRID -> null
            SendMethod.SES -> null
            SendMethod.NYLAS -> nylasService.getHostedAuthUrl(organization)
        }
    }

    fun createMailbox(organizationId: UUID, mailboxDetails: MailboxDetails) {
        val organization = lookupService.orgOrThrow(organizationId)
        val privacyCenter = organization.privacyCenters.firstOrNull()

        assertMailboxNotInUse(mailboxDetails.mailbox, organization)

        // If there is an existing mailbox with the same Nylas Account ID then delete and re-create it
        val existing = organizationMailboxRepository.findByNylasAccountId(mailboxDetails.nylasAccountId)
        if (existing != null) {
            organizationMailboxRepository.delete(existing)
        }

        // immediately store the mailbox...
        val mailbox = OrganizationMailbox(
            organization = organization,
            status = MailboxStatus.CONNECTED,
            type = mailboxDetails.type,
            mailbox = mailboxDetails.mailbox,
            nylasAccessToken = mailboxDetails.nylasAccessToken,
            nylasAccountId = mailboxDetails.nylasAccountId
        )

        organizationMailboxRepository.save(mailbox)

        if (privacyCenter != null) {
            privacyCenterService.updatePrivacyCenterRequestEmail(privacyCenter, mailbox.mailbox)
        }
    }

    fun pollNylasMailboxes() {

        log.info("polling nylas mailboxes...")

        val activeMailboxes = organizationMailboxRepository.findAllActiveMailboxes()

        log.info("processing ${activeMailboxes.size} mailboxes...")

        activeMailboxes.forEach {
            try {
                val result = nylasService.updateMailboxStatus(it)
                if (result == NylasService.MailboxUpdateResult.UNPLANNED_DISCONNECT) {
                    log.info("unplanned disconnect detected for mailbox ${it.mailbox}")
                    emailService.sendReconnectEmailToOrg(it.organization!!)
                }
            } catch (ex: Exception) {
                log.error("Error while processing mailbox ${it.mailbox}: ${ex.message}", ex)
            }
        }

        log.info("finished processing ${activeMailboxes.size} mailboxes")
    }

    /**
     * Throws a PrivacyInboxInUseException if mailbox is currently
     * in use by a Privacy Center belonging to another Organization
     * or by a user in any Organization
     *
     * @see polaris.controllers.PrivacyInboxInUseException
     */
    private fun assertMailboxNotInUse(mailbox: String, organization: Organization) {
        val inUseByOtherPrivacyCenter = {
            val privacyCenter = privacyCenterService.findByEmailAddress(mailbox)
            privacyCenter != null && privacyCenter.organization?.id != organization.id
        }
        val inUseByExistingUser = { userService.findByEmail(mailbox) != null }

        if (inUseByOtherPrivacyCenter() || inUseByExistingUser()) {
            throw PrivacyInboxInUseException("email address is already in use by another privacy center or user")
        }
    }
}

data class MailboxDetails(
    val type: MailboxType,
    val mailbox: String,
    val nylasAccessToken: String,
    val nylasAccountId: String
)
