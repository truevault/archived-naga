package polaris.services.support.email

import com.amazonaws.services.simpleemail.model.RawMessage
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest
import io.sentry.Sentry
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import polaris.services.support.AwsClientService
import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer
import java.util.*
import javax.activation.DataHandler
import javax.mail.Message
import javax.mail.Part
import javax.mail.Session
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import javax.mail.util.ByteArrayDataSource

@Component
@Profile("live-email")
class AWSSESEmailSender(
    awsClientService: AwsClientService
) : EmailSender {
    companion object {
        private val logger = LoggerFactory.getLogger(AWSSESEmailSender::class.java)
    }

    private val ses = awsClientService.sesClient()

    override fun send(context: EmailContext): EmailResponse {
        // Setup session/message
        val session = Session.getDefaultInstance(Properties())
        val message = MimeMessage(session)

        // Set basic mail properties
        message.subject = context.subject
        message.setFrom(InternetAddress(context.fromAddress, context.fromName))
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(context.to))
        message.replyTo = InternetAddress.parse(context.fromAddress)

        // Setup message/body with mime/multipart containers
        val msg = MimeMultipart("mixed")
        val msgBody = MimeMultipart("alternative")
        val wrap = MimeBodyPart()

        // If plaintext set it up and add it
        if (context.plaintextBody != null) {
            msgBody.addBodyPart(getTextBodyPart(context.plaintextBody))
        }

        // We always have an html version
        msgBody.addBodyPart(getHtmlBodyPart(context.htmlBody))

        // Add message body to wrapping container
        wrap.setContent(msgBody)

        // Add the wrapped container to the message
        message.setContent(msg)
        msg.addBodyPart(wrap)

        // Add each attachment to the message
        context.attachments?.forEach {
            msg.addBodyPart(getAttachmentBodyPart(it))
        }

        // Send the message
        try {
            logger.info("Attempting to send email via AWS SES: ${context.fromAddress} (${context.fromName}) / ${context.routing} / ${context.to} / ${context.subject}")
            val rawEmailRequest = getRawEmailRequest(message)
                .withConfigurationSetName("polaris-transactional")
            ses.sendRawEmail(rawEmailRequest)
            logger.info("Email successfully sent!")
        } catch (ex: Exception) {
            logger.error("Failed to send email to ${context.to} via AWS SES; error: $ex")
            Sentry.captureException(ex)
            return EmailResponse(500, "error")
        }
        return EmailResponse(200, "ok")
    }

    private fun getHtmlBodyPart(htmlBody: String): MimeBodyPart {
        val html = MimeBodyPart()
        html.setContent(htmlBody, "text/html; charset=UTF-8")
        return html
    }
    private fun getTextBodyPart(textBody: String): MimeBodyPart {
        val text = MimeBodyPart()
        text.setContent(textBody, "text/plain; charset=UTF-8")
        return text
    }
    private fun getAttachmentBodyPart(attachment: MultipartFile): MimeBodyPart {
        val att = MimeBodyPart()
        val ds = ByteArrayDataSource(attachment.inputStream, attachment.contentType)
        att.dataHandler = DataHandler(ds)
        att.fileName = attachment.originalFilename
        att.disposition = Part.ATTACHMENT
        return att
    }
    private fun getRawEmailRequest(message: MimeMessage): SendRawEmailRequest {
        val outputStream = ByteArrayOutputStream()
        message.writeTo(outputStream)
        val rawMessage = RawMessage(ByteBuffer.wrap(outputStream.toByteArray()))
        return SendRawEmailRequest(rawMessage)
    }
}
