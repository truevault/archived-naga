package polaris.services.support.email

import org.springframework.web.multipart.MultipartFile
import polaris.models.entity.organization.Organization

interface EmailSender {
    fun send(context: EmailContext): EmailResponse
}

data class EmailContext(
    val to: String,
    val routing: String?,
    val fromAddress: String,
    val fromName: String,
    val subject: String,
    val htmlBody: String,
    val plaintextBody: String? = null,
    val attachments: List<MultipartFile>? = null,
    val onBehalf: Organization? = null,
    val after: String? = null
)
