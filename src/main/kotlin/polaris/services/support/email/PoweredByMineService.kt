package polaris.services.support.email

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class PoweredByMineService {
    companion object {
        private val log = LoggerFactory.getLogger(PoweredByMineService::class.java)

        // these extractors are sorted in priority order
        private val extractors = listOf(MineContentExtractorLi, MineContentExtractorBr, MineContentExtractorMeta)
    }

    fun isMineEmail(emailBody: String): Boolean {
        val doc: Document = Jsoup.parseBodyFragment(emailBody)
        val body: Element = doc.body()
        val plainText: String = body.text()

        log.info("checking mine email\nBODY:\n${emailBody}\n\nPLAINTEXT:\n$plainText")

        val hasPoweredByMine = plainText.contains("Powered by Mine", ignoreCase = true)
        val hasDeletion = plainText.contains("deletion", ignoreCase = true)

        log.info("is mine email: ${hasPoweredByMine && hasDeletion} / hasPoweredByMine=$hasPoweredByMine, hasDeletion=$hasDeletion")

        return hasPoweredByMine && hasDeletion
    }

    fun extractMineEmailContents(emailBody: String, name: String?, email: String?): MineEmailContents {
        val doc: Document = Jsoup.parseBodyFragment(emailBody)
        val body: Element = doc.body()

        var contents = MineEmailContents(null, null, null)

        for (extractor: MineContentExtractor in extractors) {
            contents = extractor.extract(name, email, body)
            if (contents.isValid) break
        }

        log.info("extracted contents: $contents")

        return contents
    }
}

data class MineEmailContents private constructor(
    val firstName: String?,
    val lastName: String?,
    val email: String?,
    val isValid: Boolean
) {
    constructor(firstName: String?, lastName: String?, email: String?) :
        this(
            firstName = if (!firstName.isNullOrBlank()) firstName else email,
            lastName = lastName,
            email = email,
            isValid = !email.isNullOrBlank()
        )
}

/**
 * Extracts mine content based on nullable name/email values from the message metadata
 */
object MineContentExtractorMeta : MineContentExtractor {
    private val log = LoggerFactory.getLogger(MineContentExtractorMeta::class.java)

    override fun extract(name: String?, email: String?, body: Element?): MineEmailContents {
        return extract(log, name, email)
    }
}

/**
 * Extracts mine content based on <li> elements in the email body
 */
object MineContentExtractorLi : MineContentExtractor {
    private val log = LoggerFactory.getLogger(MineContentExtractorLi::class.java)
    private val NAME_REGEX = Regex("Name: (.*)$")
    private val EMAIL_REGEX = Regex("Email: (.*)$")

    override fun extract(name: String?, email: String?, body: Element?): MineEmailContents {
        val liElements = body?.getElementsByTag("li")?.toList()
        val nameMatches = liElements?.find { NAME_REGEX.matches(it.text()) }?.text()?.let { NAME_REGEX.find(it) }
        val emailMatches = liElements?.find { EMAIL_REGEX.matches(it.text()) }?.text()?.let { EMAIL_REGEX.find(it) }

        return extract(log, nameMatches, emailMatches)
    }
}

/**
 * Extracts Mine content based on the <br> line separators in the email body
 */
object MineContentExtractorBr : MineContentExtractor {
    private val log = LoggerFactory.getLogger(MineContentExtractorBr::class.java)
    private val NAME_REGEX = Regex(".*Name\\W*([\\w\\s]+).*$")
    private val MAILTO_REGEX = Regex(".*Email\\W.*mailto:([\\w+.-]+@[\\w.-]+).*$")
    private val EMAIL_REGEX = Regex(".*Email\\W*([\\w+.-]+@[\\w.-]+).*$")

    override fun extract(name: String?, email: String?, body: Element?): MineEmailContents {
        val tokens = body?.html()?.split(Regex("<br>"))
        val nameMatches = tokens?.find { NAME_REGEX.matches(it) }?.let { NAME_REGEX.find(it) }
        val emailMatches = tokens?.find { MAILTO_REGEX.matches(it) }?.let { MAILTO_REGEX.find(it) }
            ?: tokens?.find { EMAIL_REGEX.matches(it) }?.let { EMAIL_REGEX.find(it) }

        return extract(log, nameMatches, emailMatches)
    }
}

interface MineContentExtractor {
    fun extract(name: String?, email: String?, body: Element?): MineEmailContents

    fun extract(log: Logger, nameMatches: MatchResult?, emailMatches: MatchResult?): MineEmailContents {
        val name = nameMatches?.groupValues?.elementAtOrNull(1)
        val email = emailMatches?.groupValues?.elementAtOrNull(1)

        return extract(log, name, email)
    }

    fun extract(log: Logger, name: String?, email: String?): MineEmailContents {
        val nameParts = name?.trim()?.split(" ", limit = 2)
        val firstName = nameParts?.elementAtOrNull(0)
        val lastName = nameParts?.elementAtOrNull(1)

        log.info("extracted email: '$email', name: '$name' => $nameParts")

        return MineEmailContents(firstName, lastName, email)
    }
}
