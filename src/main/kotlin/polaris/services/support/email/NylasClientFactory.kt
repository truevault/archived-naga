package polaris.services.support.email

import com.nylas.NylasAccount
import com.nylas.NylasApplication
import com.nylas.NylasClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class NylasClientFactory(
    @Value("\${polaris.mailer.nylas.clientId}")
    private val nylasClientId: String,

    @Value("\${polaris.mailer.nylas.clientSecret}")
    private val nylasClientSecret: String
) {
    fun nylasAccount(accessToken: String): NylasAccount {
        return NylasClient().account(accessToken)
    }

    fun nylasApplication(): NylasApplication {
        return NylasClient().application(nylasClientId, nylasClientSecret)
    }
}
