package polaris.services.support.email

import com.nylas.Draft
import com.nylas.File
import com.nylas.NameEmail
import com.nylas.RequestFailedException
import com.nylas.Tracking
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import polaris.controllers.MailboxNotConnectedException
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.SendMethod

@Component
@Profile("live-email")
class NylasEmailSender(
    @Lazy private val nylasService: NylasService,
    private val nylasClientFactory: NylasClientFactory
) : EmailSender {
    companion object {
        private val logger = LoggerFactory.getLogger(NylasEmailSender::class.java)
    }

    override fun send(context: EmailContext): EmailResponse {
        logger.info("[nylas] send: ${context.fromAddress} (${context.fromName}) / ${context.routing} / ${context.to} / ${context.subject}")

        if (context.onBehalf == null || context.onBehalf.sendMethod != SendMethod.NYLAS) {

            throw IllegalArgumentException("To send with Nylas an organization must be present and configured to send with Nylas.")
        }

        try {

            sendEmail(context)
            logger.info("Finished sending...")
        } catch (e: RequestFailedException) {
            logger.error("[nylas] Send request failed. ", e)
            return EmailResponse(400, "error")
        }

        return EmailResponse(200, "ok")
    }

    private fun sendEmail(context: EmailContext) {
        val organization = context.onBehalf!!
        val mailbox = organization.organizationMailboxes.find { it.status == MailboxStatus.CONNECTED }
            ?: throw MailboxNotConnectedException("Unable to send message - Privacy Mailbox not connected. Go to Settings -> Privacy Email to re-connect.")
        val account = nylasClientFactory.nylasAccount(mailbox.nylasAccessToken)

        logger.info("sending mail from mailbox: ${mailbox.mailbox} with account token: ${mailbox.nylasAccessToken}")

        // Create a new draft object
        val draft = Draft()

        // NOTE: We don't need to set the from/reply-to because Nylas will handle that for us
        draft.subject = context.subject
        draft.to = listOf(NameEmail(null, context.to))
        draft.body = context.htmlBody

        if (context.routing != null) {
            val tracking = Tracking()
            tracking.isThreadReplies = true
            tracking.payload = context.routing
            draft.tracking = tracking
        }

        // attach files
        val files = account.files()
        val attachmentIds =
            context.attachments?.map { files.upload(it.originalFilename, it.contentType, it.bytes) } ?: emptyList()
        attachmentIds.forEach {
            val file: File = account.files()[it.id]
            draft.attach(file)
        }

        // Send the draft without saving it to the third party provider
        try {
            account.drafts().send(draft)
        } catch (e: RequestFailedException) {
            logger.info("internally dc mailbox...")
            nylasService.disconnectMailbox(mailbox, true)
            throw e
        }
    }
}
