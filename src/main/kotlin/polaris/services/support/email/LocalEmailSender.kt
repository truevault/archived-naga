package polaris.services.support.email

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import org.springframework.util.FileCopyUtils
import java.awt.Desktop
import java.io.File
import java.nio.file.Paths
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

@Component
@Profile("!live-email")
class LocalEmailSender(
    @Value("\${polaris.mailer.local.output:logs/emails}")
    private val outputDirectory: String
) : EmailSender {
    init {
        File(outputDirectory).mkdirs()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(LocalEmailSender::class.java)
    }

    override fun send(context: EmailContext): EmailResponse {
        logger.info("local send: ${context.fromAddress} (${context.fromName}) / ${context.routing} / ${context.to} / ${context.subject}")
        if (context.onBehalf != null) {
            logger.info("sending on behalf of: ${context.onBehalf.name} (${context.onBehalf.sendMethod})")
        }

        // context.onBehalf?.let {
        //     context.onBehalf.organizationMailboxes.find { it.status == MailboxStatus.CONNECTED }
        //         ?: throw MailboxNotConnectedException("Unable to send message - Privacy Mailbox not connected. Go to Settings -> Privacy Email to re-connect.")
        // }

        val sendDatestamp = DateTimeFormatter
            .ofPattern("yyyy-MM-dd")
            .withZone(ZoneId.systemDefault())
            .format(Instant.now())

        val sendTimestamp = DateTimeFormatter
            .ofPattern("HHmmssSSS")
            .withZone(ZoneId.systemDefault())
            .format(Instant.now())

        val savedMessageBaseFileName = sanitizeFilename("$sendDatestamp-${context.subject}-$sendTimestamp")

        val outputHtmlFile = Paths.get(outputDirectory, "$savedMessageBaseFileName.html").toFile()
        outputHtmlFile.outputStream().writer(Charsets.UTF_8).use { fileOutputStream ->
            fileOutputStream.appendln(context.htmlBody)
        }

        context.attachments?.forEach { attachment ->
            val attachmentFile = Paths.get(outputDirectory, "$savedMessageBaseFileName-${attachment.originalFilename}").toFile()
            FileCopyUtils.copy(attachment.inputStream, attachmentFile.outputStream())
        }

        return try {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().browse(outputHtmlFile.toURI())
            } else {
                Runtime.getRuntime().exec(arrayOf("open", outputHtmlFile.toURI().toString())).waitFor()
            }

            EmailResponse(200, "ok")
        } catch (e: Exception) {
            // We weren't able to open the browser for the user, but that is OK
            // because if we got here we at least captured the message that was sent
            logger.info("Unable to open browser. Email created at $outputHtmlFile")
            EmailResponse(200, "Unable to open browser. Email created at $outputHtmlFile")
        }
    }

    private fun sanitizeFilename(inputName: String): String {
        return inputName.replace(Regex("[^a-zA-Z0-9\\.\\-]"), "_")
    }
}
