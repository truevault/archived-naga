package polaris.services.support.email

import com.sendgrid.Method
import com.sendgrid.Request
import com.sendgrid.SendGrid
import com.sendgrid.helpers.mail.Mail
import com.sendgrid.helpers.mail.objects.Attachments
import com.sendgrid.helpers.mail.objects.Content
import com.sendgrid.helpers.mail.objects.Email
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.io.IOException

@Component
@Profile("live-email")
class SendGridEmailSender(
    @Value("\${polaris.mailer.sendgrid.key}")
    private val sendgridApiKey: String
) : EmailSender {
    companion object {
        private val logger = LoggerFactory.getLogger(SendGridEmailSender::class.java)
    }

    private val sg = SendGrid(sendgridApiKey)

    override fun send(context: EmailContext): EmailResponse {
        logger.info("sendgrid send: ${context.fromAddress} (${context.fromName}) / ${context.routing} / ${context.to} / ${context.subject}")

        val fromUser = context.fromAddress.substringBeforeLast("@")
        val fromDomain = context.fromAddress.substringAfterLast("@")

        val replyTo = if (context.routing == null) { context.fromAddress } else { "$fromUser.${context.routing}@$fromDomain" }
        logger.info("sendgrid replyTo: $replyTo")

        val content = Content("text/html", context.htmlBody)
        val mail = Mail(Email(context.fromAddress, context.fromName), context.subject, Email(context.to), content)

        mail.setReplyTo(Email(replyTo))

        context.attachments?.forEach { attachment ->
            mail.addAttachments(Attachments.Builder(attachment.originalFilename, attachment.inputStream).build())
        }

        if (context.plaintextBody != null) {
            mail.addContent(Content("text/plain", context.plaintextBody))
        }

        val request = Request()

        try {
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = mail.build()
            logger.info("sendgrid sending")
            val response = sg.api(request)
            logger.info("sendgrid response: ${response.statusCode}")
            logger.info("sendgrid response body: ${response.body}")
            logger.info("sendgrid response headers: ${response.headers}")
            return EmailResponse(response.statusCode, response.body, response.headers)
        } catch (ex: IOException) {
            throw ex
        }
    }
}
