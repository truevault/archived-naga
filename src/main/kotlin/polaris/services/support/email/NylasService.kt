package polaris.services.support.email

import com.nylas.HostedAuthentication
import com.nylas.Message
import com.nylas.RequestFailedException
import com.nylas.Scope
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.MailboxType
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationMailbox
import polaris.repositories.OrganizationMailboxRepository
import polaris.services.primary.PrivacyCenterService
import polaris.services.support.EmailService
import polaris.util.WebhookRoutes

@Service
class NylasService(
    @Value("\${polaris.domain}")
    private val polarisDomain: String,

    private val organizationMailboxRepository: OrganizationMailboxRepository,
    private val nylasClientFactory: NylasClientFactory,
    private val emailService: EmailService,
    private val privacyCenterService: PrivacyCenterService
) {

    companion object {
        private val log = LoggerFactory.getLogger(NylasService::class.java)
    }

    fun getHostedAuthUrl(organization: Organization): String {
        val authentication = serverAuthentication()

        return authentication.urlBuilder()
            .redirectUri("${polarisDomain}${WebhookRoutes.Nylas.OAUTH}")
            .responseType("code") // Use token for client-side apps
            .scopes(Scope.EMAIL_MODIFY, Scope.EMAIL_SEND, Scope.EMAIL_READ_ONLY)
            .state(organization.id.toString())
            .buildUrl()
    }

    fun oauthGetMailboxDetails(oauthCode: String): MailboxDetails {
        log.info("getting oauth mailbox details from oauth code")

        val accessToken = accessTokenFromOauthCode(oauthCode)

        log.info("obtained access token")

        val accountDetails = getAccountDetails(accessToken)

        log.info("got account details successfully: $accountDetails")

        val mailboxDetails = convertAccountDetailsToMailboxDetails(accountDetails, accessToken)

        log.info("converted account details to mailbox details: $mailboxDetails")

        return mailboxDetails
    }

    fun disconnectMailbox(mailbox: OrganizationMailbox, sendReconnect: Boolean) {
        mailbox.status = MailboxStatus.DISCONNECTED
        log.info("[nylas] disconnecting mailbox ${mailbox.id}")
        organizationMailboxRepository.save(mailbox)
        if (sendReconnect) {
            emailService.sendReconnectEmailToOrg(mailbox.organization!!)
        }
    }

    fun setMailboxError(nylasAccountId: String, sendReconnect: Boolean) {
        val mailbox = organizationMailboxRepository.findByNylasAccountId(nylasAccountId)

        if (mailbox != null) {
            mailbox.status = MailboxStatus.DISCONNECTED
            organizationMailboxRepository.save(mailbox)

            if (sendReconnect) {
                emailService.sendReconnectEmailToOrg(mailbox.organization!!)
            }
        }
    }

    fun setMailboxSuccess(nylasAccountId: String) {
        val mailbox = organizationMailboxRepository.findByNylasAccountId(nylasAccountId)

        if (mailbox != null) {
            log.info("mailbox ${mailbox.mailbox} (nylas account $nylasAccountId) is disconnected via the webhook.")
            mailbox.status = MailboxStatus.CONNECTED
            organizationMailboxRepository.save(mailbox)

            // POLARIS-1225 - now get the organization's privacy center, and update the email address
            val org = mailbox.organization!!
            val privacyCenter = org.privacyCenters.firstOrNull()
            if (privacyCenter != null) {
                privacyCenterService.updatePrivacyCenterRequestEmail(privacyCenter, mailbox.mailbox)
            }
        }
    }

    enum class MailboxUpdateResult {
        UNPLANNED_DISCONNECT,
        REMAINS_DISCONNECTED,
        REMAINS_CONNECTED,
        CONNECTED
    }

    fun updateMailboxStatus(mailbox: OrganizationMailbox): MailboxUpdateResult {
        log.info("updating mailbox: ${mailbox.mailbox} / ${mailbox.nylasAccountId} / ${mailbox.status}")

        val wasConnected = mailbox.status == MailboxStatus.CONNECTED
        val initialStatus = mailbox.status
        var currentStatus = mailbox.status

        try {
            val nylasAccountId = mailbox.nylasAccountId
            val nylasAccessToken = mailbox.nylasAccessToken

            log.info("fetching account details for Nylas account $nylasAccountId...")
            val account = getAccountDetails(nylasAccessToken)

            when (account.syncState) {
                "running" -> currentStatus = MailboxStatus.CONNECTED
                "stopped" -> currentStatus = MailboxStatus.DISCONNECTED
                "partial" -> currentStatus = initialStatus // partial state is transient and should resolve itself over time. preserve current local status for now
            }

            log.info("mailbox ${mailbox.mailbox} (nylas account $nylasAccountId) is $currentStatus with nylas sync state ${account.syncState}")
        } catch (e: RequestFailedException) {
            log.error("mailbox ${mailbox.mailbox} mailbox request failed: $e")
            currentStatus = MailboxStatus.DISCONNECTED
        }

        if (currentStatus != initialStatus) {
            log.info("updating status for mailbox ${mailbox.mailbox} from $initialStatus to $currentStatus")
            mailbox.status = currentStatus
            organizationMailboxRepository.save(mailbox)
        }

        return if (wasConnected && mailbox.status == MailboxStatus.DISCONNECTED) {
            MailboxUpdateResult.UNPLANNED_DISCONNECT
        } else if (wasConnected && mailbox.status == MailboxStatus.CONNECTED) {
            MailboxUpdateResult.REMAINS_CONNECTED
        } else if (!wasConnected && mailbox.status == MailboxStatus.CONNECTED) {
            MailboxUpdateResult.CONNECTED
        } else {
            MailboxUpdateResult.REMAINS_DISCONNECTED
        }
    }

    fun getMailboxByAccountId(accountId: String) = organizationMailboxRepository.findByNylasAccountId(accountId)

    fun getMessageDetailsById(accessToken: String, messageId: String): Message {
        val nylasAccount = nylasClientFactory.nylasAccount(accessToken)
        return nylasAccount.messages().get(messageId)
    }

    private fun convertAccountDetailsToMailboxDetails(
        accountDetails: NylasAccountDetails,
        accessToken: String
    ): MailboxDetails {
        val type = when (accountDetails.provider) {
            "gmail" -> MailboxType.GMAIL
            "eas" -> MailboxType.EXCHANGE
            "ews" -> MailboxType.EXCHANGE
            else -> MailboxType.UNKNOWN
        }

        return MailboxDetails(
            type = type,
            mailbox = accountDetails.emailAddress,
            nylasAccessToken = accessToken,
            nylasAccountId = accountDetails.id
        )
    }

    private fun accessTokenFromOauthCode(oauthCode: String): String {
        val authentication = serverAuthentication()
        return authentication.fetchToken(oauthCode).accessToken
    }

    private fun getAccountDetails(accessToken: String): NylasAccountDetails {
        val nylasAccount = nylasClientFactory.nylasAccount(accessToken)
        val account = nylasAccount.fetchAccountByAccessToken()
        return NylasAccountDetails(
            id = account.id,
            name = account.name,
            emailAddress = account.emailAddress,
            provider = account.provider,
            syncState = account.syncState,
            linkedAt = account.linkedAt
        )
    }

    private fun serverAuthentication(): HostedAuthentication {
        val nylasApplication = nylasClientFactory.nylasApplication()
        return nylasApplication.hostedAuthentication()
    }
}

data class NylasAccountDetails(
    val id: String,
    val name: String,
    val emailAddress: String,
    val provider: String,
    val syncState: String,
    val linkedAt: Long
)
