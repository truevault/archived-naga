package polaris.services.support

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.dto.websiteaudit.SQSMessageStartWebsiteAudit
import polaris.models.dto.websiteaudit.WebsiteAuditDto
import polaris.models.entity.PrivacyCenterNotFound
import polaris.models.entity.organization.OptOutPageType
import polaris.models.entity.organization.OrganizationWebsiteAudit
import polaris.models.entity.organization.WEBSITE_AUDIT_SURVEY_NAME
import polaris.models.entity.organization.WebsiteAuditProgress
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.OrganizationWebsiteAuditRepository
import java.time.ZonedDateTime
import java.util.UUID

@Service
class WebsiteAuditService(
    private val awsClientService: AwsClientService,
    private val orgWebsiteAuditRepo: OrganizationWebsiteAuditRepository,
    private val orgSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val optOutService: OptOutService,
    private val lookup: LookupService,
    @Value("\${polaris.aws.websiteAuditQueueUrl}") private val websiteAuditQueueUrl: String
) {

    companion object {
        private val log = LoggerFactory.getLogger(PrivacyCenterBuilderService::class.java)
    }

    fun addWebsiteAuditResult(result: WebsiteAuditDto) {
        log.info("Received website audit result: $result")
        val org = lookup.orgOrThrow(result.organizationId)
        val scan = orgWebsiteAuditRepo.findAllById(result.id).firstOrNull()

        if (scan?.status == WebsiteAuditProgress.IN_PROGRESS) {
            val orgWebsiteAudit =
                OrganizationWebsiteAudit(
                    id = scan.id,
                    organization = org,
                    status = WebsiteAuditProgress.COMPLETE,
                    scanUrl = result.scanUrl as String,
                    scanEndedAt = ZonedDateTime.now(),
                    privacyPolicyLinkTextValid = result.privacyPolicyLinkTextValid,
                    privacyPolicyLinkUrlValid = result.privacyPolicyLinkUrlValid,
                    caPrivacyNoticeLinkTextValid = result.caPrivacyNoticeLinkTextValid,
                    caPrivacyNoticeLinkUrlValid = result.caPrivacyNoticeLinkUrlValid,
                    optOutLinkTextValid = result.optOutLinkTextValid,
                    optOutLinkUrlValid = result.optOutLinkUrlValid,
                    optOutLinkYourPrivacyChoicesIcon = result.optOutLinkYourPrivacyChoicesIcon,
                    polarisJsSetup = result.polarisJsSetup,
                    polarisJsBeforeGoogleAnalytics = result.polarisJsBeforeGoogleAnalytics,
                    polarisJsNoDefer = result.polarisJsNoDefer
                )

            orgWebsiteAuditRepo.save(orgWebsiteAudit)
        }
    }

    fun getWebsiteAuditResult(organizationPublicId: OrganizationPublicId): WebsiteAuditDto {
        val org = lookup.orgOrThrow(organizationPublicId)
        val scan =
            orgWebsiteAuditRepo.findAllByOrganization(org).sortedByDescending { it.scanStartedAt }.firstOrNull { it.status != WebsiteAuditProgress.ARCHIVED }
                ?: return WebsiteAuditDto(
                    organizationId = org.publicId,
                    status = WebsiteAuditProgress.NOT_STARTED,
                    id = UUID.randomUUID(),
                    scanUrl = null,
                    error = null
                )

        if (scan.hasScanTimedOut()) {
            scan.status = WebsiteAuditProgress.ERROR
            orgWebsiteAuditRepo.save(scan)

            return WebsiteAuditDto(
                id = scan.id,
                organizationId = org.publicId,
                status = WebsiteAuditProgress.ERROR,
                scanUrl = scan.scanUrl,
                error = "Website Audit has Timed Out"
            )
        }

        return WebsiteAuditDto(
            id = scan.id,
            organizationId = org.publicId,
            status = scan.status,
            scanUrl = scan.scanUrl,
            error = scan.scanError,
            privacyPolicyLinkTextValid = scan.privacyPolicyLinkTextValid,
            privacyPolicyLinkUrlValid = scan.privacyPolicyLinkUrlValid,
            caPrivacyNoticeLinkTextValid = scan.caPrivacyNoticeLinkTextValid,
            caPrivacyNoticeLinkUrlValid = scan.caPrivacyNoticeLinkUrlValid,
            optOutLinkTextValid = scan.optOutLinkTextValid,
            optOutLinkUrlValid = scan.optOutLinkUrlValid,
            optOutLinkYourPrivacyChoicesIcon = scan.optOutLinkYourPrivacyChoicesIcon,
            polarisJsSetup = scan.polarisJsSetup,
            polarisJsBeforeGoogleAnalytics = scan.polarisJsBeforeGoogleAnalytics,
            polarisJsNoDefer = scan.polarisJsNoDefer
        )
    }

    fun resetWebsiteAudit(organizationPublicId: OrganizationPublicId) {
        val org = lookup.orgOrThrow(organizationPublicId)
        val latestScan =
            orgWebsiteAuditRepo.findAllByOrganization(org).firstOrNull { it.status == WebsiteAuditProgress.NOT_STARTED || it.status == WebsiteAuditProgress.IN_PROGRESS }
        if (latestScan != null) {
            latestScan.status = WebsiteAuditProgress.ARCHIVED
            orgWebsiteAuditRepo.save(latestScan)

            val surveyAnswers = orgSurveyQuestionRepository.findAllByOrganizationAndSlug(org, "website-audit-finished")
            if (surveyAnswers.isNotEmpty()) {
                orgSurveyQuestionRepository.deleteAll(surveyAnswers)
            }
        }
    }

    fun startWebsiteAuditRequest(organizationPublicId: OrganizationPublicId, url: String) {
        val org = lookup.orgOrThrow(organizationPublicId)
        val privacyCenter = org.privacyCenter() ?: throw PrivacyCenterNotFound(organizationPublicId.toString())
        val opt = optOutService.getOptOutPageType(org)
        val isSelling = opt == OptOutPageType.SELLING || opt == OptOutPageType.SELLING_AND_SHARING
        val isSharing = opt == OptOutPageType.SHARING || opt == OptOutPageType.SELLING_AND_SHARING
        val checkOptOut = isSelling || isSharing

        val latestScan = orgWebsiteAuditRepo.findAllByOrganization(org).firstOrNull { it.status == WebsiteAuditProgress.IN_PROGRESS }
        if (latestScan != null) {
            return
        }

        val surveyAnswers = orgSurveyQuestionRepository.findAllByOrganizationAndSurveyName(org, WEBSITE_AUDIT_SURVEY_NAME).filter {
            it.answer == "RETRY"
        }
        if (surveyAnswers.isNotEmpty()) {
            orgSurveyQuestionRepository.deleteAll(surveyAnswers)
        }

        val scans = orgWebsiteAuditRepo.findAllByOrganization(org)
        if (scans.isNotEmpty()) {
            scans.forEach {
                if (it.status == WebsiteAuditProgress.COMPLETE) {
                    it.status = WebsiteAuditProgress.ARCHIVED
                }
            }
            orgWebsiteAuditRepo.saveAll(scans)
        }

        val scanId = UUID.randomUUID()
        val sqs = awsClientService.sqsClient()

        // TODO: Check for what audits need to be done
        log.info("starting website audit $scanId")

        val message =
            SQSMessageStartWebsiteAudit(
                organizationId = organizationPublicId,
                scanId = scanId,
                websiteUrl = url,
                privacyCenterUrl = "https://${privacyCenter.customUrl}",
                auditPrivacyLink = true,
                auditCaPrivacyNoticeLink = true,
                auditOptOutLink = checkOptOut,
                auditPolarisJs = true
            )

        log.info("inserting message into SQS queue: $websiteAuditQueueUrl; json: ${message.toJson()}")
        sqs.sendMessage(websiteAuditQueueUrl, message.toJson())
        log.info("inserted message")

        orgWebsiteAuditRepo.save(
            OrganizationWebsiteAudit(
                id = scanId,
                organization = org,
                scanUrl = url,
                status = WebsiteAuditProgress.IN_PROGRESS
            )
        )
    }
}
