package polaris.services.support.events

import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import polaris.config.EventTrackingConfig
import polaris.models.dto.springevents.PublishPolarisEvents
import polaris.models.entity.organization.Event
import polaris.models.entity.organization.EventData
import polaris.models.entity.organization.Organization
import polaris.models.entity.user.User
import polaris.repositories.EventRepository
import java.util.UUID
import javax.transaction.Transactional

@Service
@Transactional
class EventTrackingService(
    private val config: EventTrackingConfig,
    private val publisher: ApplicationEventPublisher
) {
    companion object {
        private val log = LoggerFactory.getLogger(EventTrackingService::class.java)
    }

    fun publish(events: List<Event>) {
        if (events.isEmpty()) return

        if (config.enabled) {
            log.info("Event Service publishing ${events.size} events...")
            publisher.publishEvent(PublishPolarisEvents(events))
        } else {
            log.info("[DISABLED] Event Service publishing ${events.size} events: $events")
        }
    }

    fun publish(eventData: List<EventData>, org: Organization?, user: User?) {
        val events = eventData.map {
            Event(
                id = UUID.randomUUID(),
                user = user,
                organization = org,
                eventName = it.eventName,
                eventData = it
            )
        }

        publish(events)
    }
}

@Component
class PublishPolarisEventsListener(
    private val eventRepository: EventRepository
) {
    companion object {
        private val log = LoggerFactory.getLogger(PublishPolarisEventsListener::class.java)
    }

    @Async
    @EventListener
    fun handlePublishPolarisEventAsync(publishEvent: PublishPolarisEvents) {
        log.info("Event Listener processing ${publishEvent.events.size} events...")
        eventRepository.saveAll(publishEvent.events)
        log.info("Event Listener ${publishEvent.events.size} events processed.")
    }
}
