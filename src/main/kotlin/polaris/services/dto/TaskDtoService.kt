package polaris.services.dto

import org.springframework.stereotype.Service
import polaris.models.dto.task.TaskDto
import polaris.models.dto.toIso
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskCompletionType

@Service
class TaskDtoService(val userDtoService: UserDtoService) {
    fun toDto(t: Task): TaskDto {
        return TaskDto(
            id = t.id.toString(),
            name = t.name,
            description = t.description,
            createdAt = t.createdAt.toIso(),
            completedAt = t.completedAt?.toIso(),
            dueAt = t.dueAt?.toIso(),
            taskKey = t.taskKey!!,
            completedBy = if (t.completedAt == null) null else userDtoService.toDtoOrSystem(t.completedBy),
            completionType = t.completionType ?: TaskCompletionType.AUTOMATIC,
            callToAction = t.callToAction,
            callToActionUrl = t.callToActionUrl
        )
    }
}
