package polaris.services.dto

import org.springframework.stereotype.Service
import polaris.models.dto.organization.DataRetentionPolicyCategoryDto
import polaris.models.dto.organization.DataRetentionPolicyDto
import polaris.models.entity.organization.DataRetentionPolicyDetail
import polaris.models.entity.organization.DataRetentionPolicyDetailCategory
import polaris.models.entity.organization.Organization

@Service
class DataRetentionDtoService {
    fun toDto(org: Organization, details: List<DataRetentionPolicyDetail>): DataRetentionPolicyDto {
        val categories = DataRetentionPolicyDetailCategory.values().map { category ->
            val existing = details.find { it.type == category }

            DataRetentionPolicyCategoryDto(
                category,
                existing?.description ?: "",
                existing?.pic?.map { it.id.toString() } ?: emptyList()
            )
        }

        return DataRetentionPolicyDto(categories)
    }
}
