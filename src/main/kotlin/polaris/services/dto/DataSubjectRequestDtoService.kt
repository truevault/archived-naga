package polaris.services.dto

import org.springframework.stereotype.Service
import polaris.models.dto.request.DataSubjectRequestDetailDto
import polaris.models.dto.request.DataSubjectRequestSummaryDto
import polaris.models.dto.toIso
import polaris.models.entity.organization.RequestHandlingInstruction
import polaris.models.entity.request.DataSubjectRequest
import polaris.repositories.DataRequestTypeRequestStateHelpArticleRepository
import polaris.repositories.RequestHandlingInstructionsRepository
import polaris.services.primary.DataSubjectRequestService

@Service
class DataSubjectRequestDtoService(
    private val vendorDtoService: VendorDtoService,
    private val userDtoService: UserDtoService,
    private val requestHandlingInstructionsRepository: RequestHandlingInstructionsRepository,
    private val dataRequestTypeRequestStateHelpArticleRepository: DataRequestTypeRequestStateHelpArticleRepository,
    private val dataSubjectRequestService: DataSubjectRequestService
) {
    fun toSummaryDto(r: DataSubjectRequest) = DataSubjectRequestSummaryDto(
        id = r.publicId,
        dataRequestType = r.requestType!!.toDto(),

        context = r.context,

        state = r.state,
        substate = r.substate,

        consumerGroups = r.collectionGroups.map { it.toDto() },

        subjectFirstName = r.subjectFirstName,
        subjectLastName = r.subjectLastName,
        subjectEmailAddress = r.subjectEmailAddress,
        organizationCustomInput = r.organizationCustomInput,

        requestDate = r.requestDate.toIso(),
        dueDate = r.requestDueDate?.toIso(),
        originalDueDate = r.requestOriginalDueDate?.toIso(),
        closedAt = r.closedAt?.toIso()
    )

    fun toDetailDto(r: DataSubjectRequest): DataSubjectRequestDetailDto {
        val instructionType = r.requestType?.requestInstructionType()

        var instructions = emptyList<RequestHandlingInstruction>()

        if (r.organization != null && instructionType != null) {
            instructions = requestHandlingInstructionsRepository.findAllByOrganizationAndRequestType(r.organization, instructionType)
        }

        return DataSubjectRequestDetailDto(
            id = r.publicId,
            organizationId = r.organization!!.publicId,
            dataRequestType = r.requestType.toDto(),

            state = r.state,
            stateHistory = r.stateHistory,
            substate = r.substate,

            consumerGroups = r.collectionGroups.map { it.toDto() },

            subjectFirstName = r.subjectFirstName,
            subjectLastName = r.subjectLastName,
            subjectEmailAddress = r.subjectEmailAddress,
            subjectPhoneNumber = r.subjectPhoneNumber,
            subjectMailingAddress = r.subjectMailingAddress,
            subjectIpAddress = r.subjectIpAddress,
            submissionMethod = r.submissionMethod,
            submissionSource = r.submissionSource,
            organizationCustomInput = r.organizationCustomInput,

            isGpcOptOut = r.gpcOptOut,

            requestDate = r.requestDate.toIso(),
            emailVerifiedDate = r.emailVerifiedDate?.toIso(),
            dueDate = r.requestDueDate?.toIso(),
            originalDueDate = r.requestOriginalDueDate?.toIso(),
            closedAt = r.closedAt?.toIso(),

            events = r.dataSubjectRequestEvents.filter { it.eventType.public }.map { it.toEmbeddedDto(userDtoService) }.sortedBy { it.eventDate },
            vendorOutcomes = r.vendorOutcomes.map { vendorDtoService.vendorOutcomeToEmbeddedDto(it, instructions) },
            consumerGroupProcessingInstructions = r.organization.requestProcessingInstructionsCollectionGroup,
            optOutProcessingInstructions = r.organization.requestProcessingInstructionsOptOut,
            hasGaUserId = !r.gaUserId.isNullOrBlank(),

            selfDeclarationProvided = r.selfDeclarationProvided,

            consumerGroupResult = r.collectionGroupResult,
            // TODO: Remove markdown renderer when instructions have been deprecated/migrated to only HTML for the WYSIWYG editor (1/2)
            // identityVerificationInstructions = MarkdownRenderer.optRenderToHtml(
            //   r.organization.regulationsSettings.find { it.regulation!!.id == r.dataRequestType!!.regulation!!.id }?.identityVerificationInstruction
            // ),
            // TODO: Remove markdown renderer when instructions have been deprecated/migrated to only HTML for the WYSIWYG editor (2/2)
            // processingInstructions = r.dataSubjectType?.let {
            //   MarkdownRenderer.optRenderToHtml(
            //     processingInstructionsRepository.findByOrganizationAndDataSubjectTypeAndDataRequestType(
            //       r.organization,
            //       r.dataSubjectType!!,
            //       r.dataRequestType!!
            //     )?.instruction
            //   )
            // },
            helpArticleId = dataRequestTypeRequestStateHelpArticleRepository.findByRequestTypeAndRequestState(
                r.requestType, r.state
            )?.helpArticleId,
            vendorContacted = r.vendorContacted,
            contactVendorsEverHidden = r.contactVendorsEverHidden ?: false,
            contactVendorsEverShown = r.contactVendorsEverShown ?: false,
            contactVendorsHidden = dataSubjectRequestService.isContactVendorStepHidden(r),
            correctionRequest = r.correctionRequest,
            correctionFinished = r.correctionFinished,
            limitationFinished = r.limitationFinished,
            objectionRequest = r.objectionRequest,
            consentWithdrawnRequest = r.consentWithdrawnRequest,
            consentWithdrawnFinished = r.consentWithdrawnFinished,
            requestorContacted = r.requestorContacted,
            emailVerified = r.emailVerified(),
            emailVerifiedManually = r.emailVerifiedManually,
            emailVerifiedAutomatically = r.emailVerifiedAutomatically,
            context = r.context,
            isAutocreated = r.isAutocreated,
            isDuplicate = r.isDuplicate(),
            duplicatesRequestId = r.duplicatesRequest?.publicId
        )
    }
}
