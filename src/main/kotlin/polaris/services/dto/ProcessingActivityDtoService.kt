package polaris.services.dto

import org.springframework.stereotype.Service
import polaris.controllers.admin.DefProcessingActivityAdminDto
import polaris.models.dto.organization.DefProcessingActivityDto
import polaris.models.dto.organization.ProcessingActivityDto
import polaris.models.entity.organization.DefaultProcessingActivity
import polaris.models.entity.organization.ProcessingActivity

@Service
class ProcessingActivityDtoService() {

    fun defaultToDto(pa: DefaultProcessingActivity): DefProcessingActivityDto {
        return DefProcessingActivityDto(
            id = pa.id.toString(),
            name = pa.name,
            slug = pa.slug,
            materialIconKey = pa.materialIconKey,
            displayOrder = pa.displayOrder,
            deprecated = pa.deprecated,
            helpText = pa.helpText,
            mandatoryLawfulBases = pa.gdprMandatoryLawfulBases,
            optionalLawfulBases = pa.gdprOptionalLawfulBases
        )
    }

    fun toDto(pa: ProcessingActivity): ProcessingActivityDto {
        return ProcessingActivityDto(
            id = pa.id.toString(),
            name = pa.name,
            lawfulBases = pa.gdprLawfulBases,
            autocreationSlug = pa.autocreationSlug,

            isDefault = pa.defaultProcessingActivity != null,
            defaultId = pa.defaultProcessingActivity?.id?.toString(),
            defaultSlug = pa.defaultProcessingActivity?.slug,
            defaultMandatoryLawfulBases = pa.defaultProcessingActivity?.gdprMandatoryLawfulBases ?: emptyList(),
            defaultOptionalLawfulBases = pa.defaultProcessingActivity?.gdprOptionalLawfulBases ?: emptyList(),

            regions = pa.processingRegions,

            processedPic = pa.processedPic.map { it.id.toString() }
        )
    }

    fun defaultToAdminDto(pa: DefaultProcessingActivity): DefProcessingActivityAdminDto {
        return DefProcessingActivityAdminDto(
            id = pa.id.toString(),
            name = pa.name,
            slug = pa.slug,
            materialIconKey = pa.materialIconKey,
            displayOrder = pa.displayOrder,
            deprecated = pa.deprecated,
            helpText = pa.helpText,
            mandatoryLawfulBases = pa.gdprMandatoryLawfulBases,
            optionalLawfulBases = pa.gdprOptionalLawfulBases
        )
    }

    fun applyAdminDto(dto: DefProcessingActivityAdminDto, pa: DefaultProcessingActivity): DefaultProcessingActivity {
        pa.name = dto.name
        pa.slug = dto.slug
        pa.materialIconKey = dto.materialIconKey
        pa.displayOrder = dto.displayOrder
        pa.deprecated = dto.deprecated
        pa.helpText = dto.helpText
        pa.gdprMandatoryLawfulBases = dto.mandatoryLawfulBases
        pa.gdprOptionalLawfulBases = dto.optionalLawfulBases

        return pa
    }
}
