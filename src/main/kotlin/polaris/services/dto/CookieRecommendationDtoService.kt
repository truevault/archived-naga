package polaris.services.dto

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.dto.cookiescan.CookieRecommendationAdminDto
import polaris.models.dto.cookiescan.CookieRecommendationDto
import polaris.models.entity.cookieRecommendation.CookieRecommendation
import polaris.repositories.VendorRepository

@Service
class CookieRecommendationDtoService(
    private val vendorRepository: VendorRepository
) {
    companion object {
        val log = LoggerFactory.getLogger(CookieRecommendationDtoService::class.java)
    }

    fun toDto(cookieRecommendation: CookieRecommendation): CookieRecommendationDto {
        return CookieRecommendationDto(
            id = cookieRecommendation.id,
            name = cookieRecommendation.name,
            domain = cookieRecommendation.domain,
            categories = cookieRecommendation.categories,
            vendorId = cookieRecommendation.vendor?.id,
            notes = cookieRecommendation.notes
        )
    }

    fun toAdminDto(cookieRecommendation: CookieRecommendation): CookieRecommendationAdminDto {
        return CookieRecommendationAdminDto(
            id = cookieRecommendation.id,
            name = cookieRecommendation.name,
            domain = cookieRecommendation.domain,
            categories = cookieRecommendation.categories,
            vendorId = cookieRecommendation.vendor?.id,
            vendorName = cookieRecommendation.vendor?.name,
            notes = cookieRecommendation.notes
        )
    }

    fun applyAdminDto(dto: CookieRecommendationAdminDto, cr: CookieRecommendation): CookieRecommendation {
        if (dto.name != null) {
            cr.name = dto.name
        }

        if (dto.domain != null) {
            cr.domain = dto.domain
        }

        if (dto.vendorId != null) {
            val vendor = vendorRepository.getOne(dto.vendorId)
            cr.vendor = vendor
        }

        if (dto.categories.isNotEmpty()) {
            cr.categories = dto.categories
        }

        if (dto.notes != null) {
            cr.notes = dto.notes
        }

        return cr
    }
}
