package polaris.services.dto

import org.springframework.stereotype.Service
import polaris.models.dto.cookiescan.OrganizationCookieDto
import polaris.models.dto.toIso
import polaris.models.entity.organization.OrganizationCookie

@Service
class OrganizationCookieDtoService() {
    fun toDto(c: OrganizationCookie): OrganizationCookieDto {
        return OrganizationCookieDto(
            name = c.name,
            domain = c.domain,
            value = c.lastScannedValue,
            expires = c.lastScannedExpires,
            httpOnly = if (c.lastScannedHttpOnly == true) "Yes" else "No",
            secure = if (c.lastScannedSecure == true) "Yes" else "No",
            lastFetched = c.lastScannedAt?.toIso(),

            categories = c.categories,
            dataRecipients = c.dataRecipients.map { it.vendor!!.id.toString() },
            hidden = c.isHidden,
            firstParty = c.isFirstParty
        )
    }
}
