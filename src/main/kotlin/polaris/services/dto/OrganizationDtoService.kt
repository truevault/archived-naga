package polaris.services.dto

import org.springframework.stereotype.Service
import polaris.controllers.admin.ClientOrganizationAdminDto
import polaris.controllers.admin.OrganizationAdminDto
import polaris.controllers.util.LookupService
import polaris.models.dto.SurveySlug
import polaris.models.dto.organization.OrganizationDto
import polaris.models.dto.organization.OrganizationFeatureDto
import polaris.models.dto.organization.OrganizationMailboxStatus
import polaris.models.dto.organization.OrganizationMailboxStatusDto
import polaris.models.dto.organization.OrganizationMessageSettingsDto
import polaris.models.dto.organization.OrganizationRegulationDto
import polaris.models.dto.toIso
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.feature.Features
import polaris.models.entity.feature.OrganizationFeature
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationRegulation
import polaris.models.entity.organization.Phase
import polaris.models.entity.organization.SendMethod
import polaris.models.entity.organization.mappingProgressDto
import polaris.models.entity.organization.privacyNoticeProgressDto
import polaris.repositories.OrganizationFeatureRepository
import polaris.repositories.OrganizationRegulationRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.RegulationRepository
import polaris.services.primary.DataRequestFilter
import polaris.services.primary.DataSubjectRequestService
import polaris.services.primary.OrganizationService
import polaris.services.primary.TaskService
import polaris.services.support.OptOutService
import polaris.services.support.SurveyProgressService
import polaris.services.support.email.MailboxService
import polaris.util.MarkdownRenderer
import java.lang.IllegalArgumentException
import java.util.UUID

@Service
class OrganizationDtoService(
    private val dataSubjectRequestService: DataSubjectRequestService,
    private val mailboxService: MailboxService,
    private val organizationRegulationRepository: OrganizationRegulationRepository,
    private val regulationRepository: RegulationRepository,
    private val organizationRepository: OrganizationRepository,
    private val organizationFeatureRepository: OrganizationFeatureRepository,
    private val surveyProgressService: SurveyProgressService,
    private val optOutService: OptOutService,
    private val taskService: TaskService,
    private val organizationService: OrganizationService,
    private val lookupService: LookupService
) {

    fun toDto(o: Organization): OrganizationDto {
        val organizationRegulations =
            organizationRegulationRepository.findByOrganization(o).map { it.regulation!!.id to it }.toMap()

        val optOutPageType = optOutService.getOptOutPageType(o)
        val optOutAttrs = optOutService.getOptOutAttributes(o)

        return OrganizationDto(
            id = o.publicId,
            name = o.name,
            createdAt = o.createdAt.toIso(),
            logoUrl = o.logoUrl,
            faviconUrl = o.faviconUrl,
            identityVerificationMessage = renderIdentityMessage(o),
            mailbox = OrganizationMailboxStatusDto(
                viewSettings = o.sendMethod == SendMethod.NYLAS,
                mailboxStatus = mailboxStatus(o),
                mailboxOauthUrl = mailboxService.getHostedAuthUrl(o)
            ),
            messageSettings = OrganizationMessageSettingsDto(
                headerImageUrl = o.getFullHeaderImageUrl(),
                physicalAddress = o.physicalAddress,
                messageSignature = o.getSignature(),
                defaultMessageSignature = o.getDefaultSignature()
            ),
            regulations = regulationRepository.findAll().map {
                OrganizationRegulationDto(
                    id = it.id.toString(),
                    slug = it.slug,
                    name = it.name,
                    longName = it.longName,
                    description = it.description,
                    enabled = organizationRegulations.keys.contains(it.id),
                    // TODO: Remove markdown renderer when deprecated or migrated to only HTML for the WYSIWYG editor
                    identityVerificationInstruction = MarkdownRenderer.optRenderToHtml(
                        organizationRegulations.getOrDefault(
                            it.id, OrganizationRegulation(identityVerificationInstruction = "")
                        ).identityVerificationInstruction
                    )
                )
            },
            phase = o.getPhase(),
            getCompliantProgress = surveyProgressService.getSurveyProgress(o, SurveySlug.onboarding),
            getCompliantInvalidatedProgress = o.getCompliantInvalidatedProgressSteps,
            exceptionsMappingProgress = mappingProgressDto(o.exceptionsMappingProgress),
            cookiesMappingProgress = mappingProgressDto(o.cookiesMappingProgress),
            caPrivacyNoticeProgress = privacyNoticeProgressDto(o.caPrivacyNoticeProgress),
            optOutPrivacyNoticeProgress = privacyNoticeProgressDto(o.optOutPrivacyNoticeProgress),
            optOutPageType = optOutPageType,
            isServiceProvider = o.isServiceProvider,
            activeRequestCount = dataSubjectRequestService.findAllForOrganization(organizationId = o.publicId, filter = DataRequestFilter.active).count(),
            activeTaskCount = taskService.openTaskCount(o).toInt(),
            trainingComplete = o.trainingComplete ?: true,
            walkthroughEmailSent = o.walkthroughEmailSent ?: true,
            customFieldEnabled = o.customFieldEnabled,
            customFieldLabel = o.customFieldLabel,
            customFieldHelp = o.customFieldHelp,
            doesOrgProcessOptOuts = optOutService.processesOptOut(o),
            requiresNoticeOfRightToLimit = organizationService.requiresNoticeOfRightToLimit(o),
            requiresAssociations = organizationService.requiresAssociations(o),
            requiresEmployeeMapping = o.collectionGroups.any { it.collectionGroupType == CollectionGroupType.EMPLOYMENT },

            cookiePolicy = o.cookiePolicy,

            gaWebProperty = o.gaWebProperty,

            devInstructionsId = o.devInstructionsUuid.toString(),
            hrInstructionsId = o.hrInstructionsUuid.toString(),

            isSharing = optOutAttrs.anySharing || optOutAttrs.anyCustomAudience,
            isSelling = optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney,

            financialIncentiveDetails = o.financialIncentiveDetails,

            featureGdpr = lookupService.orgFeatureOrDefault(o, Features.GDPR).enabled,
            featureMultistate = lookupService.orgFeatureOrDefault(o, Features.MULTISTATE).enabled,

            nextSurvey = o.complianceVersion?.nextSurvey,
            currentSurvey = o.currentSurvey
        )
    }

    private fun mailboxStatus(o: Organization): OrganizationMailboxStatus {
        if (o.sendMethod == SendMethod.SENDGRID) {
            return OrganizationMailboxStatus.OK
        }

        if (o.organizationMailboxes.any { it.status == MailboxStatus.CONNECTED }) {
            return OrganizationMailboxStatus.OK
        }

        return OrganizationMailboxStatus.NOT_CONNECTED
    }

    // TODO: Remove markdown renderer when instructions are deprectead and/or migrated to only HTML for the WYSIWYG editor
    private fun renderIdentityMessage(o: Organization) = MarkdownRenderer.renderToHtml(o.identityVerificationMessage)

    fun toAdminDto(o: Organization) = OrganizationAdminDto(
        id = o.id.toString(),
        publicId = o.publicId,
        name = o.name,
        logoUrl = o.logoUrl,
        faviconUrl = o.faviconUrl,
        sendMethod = o.sendMethod,

        privacyPolicyUrl = o.privacyPolicyUrl,
        requestTollFreePhoneNumber = o.requestTollFreePhoneNumber,
        requestFormUrl = o.requestFormUrl,
        trainingComplete = o.trainingComplete ?: true,

        clientOrganizations = o.clientOrganizations.map {
            ClientOrganizationAdminDto(id = it.id.toString(), name = it.name)
        },
        phase = o.getPhase(),
        featureGdpr = lookupService.orgFeatureOrDefault(o, Features.GDPR).enabled,
        featureMultistate = lookupService.orgFeatureOrDefault(o, Features.MULTISTATE).enabled
    )

    fun applyAdminDto(dto: OrganizationAdminDto, o: Organization): Organization {
        o.name = dto.name
        o.logoUrl = dto.logoUrl
        o.faviconUrl = dto.faviconUrl
        o.sendMethod = dto.sendMethod

        o.privacyPolicyUrl = dto.privacyPolicyUrl
        o.requestTollFreePhoneNumber = dto.requestTollFreePhoneNumber
        o.requestFormUrl = dto.requestFormUrl
        o.trainingComplete = dto.trainingComplete

        o.complianceVersion = dto.complianceVersion

        o.clientOrganizations = organizationRepository.findAllByIdIn(
            dto.clientOrganizations.filter {
                try {
                    UUID.fromString(it.id)
                    !it.id.isNullOrBlank()
                } catch (e: IllegalArgumentException) {
                    false
                }
            }.map { UUID.fromString(it.id) }
        )

        if (dto.phase != null) {
            updateOrgFeature(o, Features.SETUP_PHASE, dto.phase == Phase.SETUP)
            updateOrgFeature(o, Features.GET_COMPLIANT_PHASE, dto.phase == Phase.GET_COMPLIANT)
        }

        if (dto.featureGdpr != null) {
            updateOrgFeature(o, Features.GDPR, dto.featureGdpr)
        }

        if (dto.featureMultistate != null) {
            updateOrgFeature(o, Features.MULTISTATE, dto.featureMultistate)
        }

        return o
    }

    fun enableOrgFeature(org: Organization, feature: String) {
        updateOrgFeature(org, feature, true)
    }

    private fun updateOrgFeature(org: Organization, feature: String, enabled: Boolean) {
        val orgFeature = lookupService.orgFeatureOrDefault(org, feature)
        orgFeature.enabled = enabled
        organizationFeatureRepository.save(orgFeature)
    }

    fun organizationFeatureDto(of: OrganizationFeature): OrganizationFeatureDto {
        return OrganizationFeatureDto(
            name = of.feature!!.name ?: "",
            description = of.feature.description ?: "",
            enabled = of.enabled
        )
    }
}
