package polaris.services.dto

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import polaris.models.dto.toIso
import polaris.models.dto.user.OrganizationUserDetailsDto
import polaris.models.dto.user.UserDetailsDto
import polaris.models.dto.user.UserDto
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationUser
import polaris.models.entity.user.User
import polaris.models.entity.user.UserStatus
import polaris.repositories.OrganizationVendorClientRepository
import polaris.services.support.SSOService

@Service
class UserDtoService(
    private val ssoService: SSOService,
    @Value("\${polaris.admin.site}")
    private val adminSite: String,
    private val organizationVendorClientRepository: OrganizationVendorClientRepository
) {
    fun toDto(u: User) = UserDto(
        email = u.email,
        lastLogin = u.lastLogin?.toIso(),
        firstName = u.firstName,
        lastName = u.lastName,
        globalRole = u.globalRole
    )

    fun toDtoOrSystem(u: User?): UserDto {
        if (u == null) {
            return UserDto("", null, "Polaris")
        }
        return toDto(u)
    }

    // NOTE: While the resulting object's `organizations` key includes "client organizations", it only looks one level deep.
    //  That is, users are not currently permitted to switch to a client-of-a-client organization.
    fun toDetailsDto(u: User): UserDetailsDto {
        val orgUsers = u.organization_users.filter { it.status == UserStatus.ACTIVE }.associateBy { it.organization!!.id }
        val orgs = orgUsers.values.map { it.organization!! }
        val orgVendorClients = organizationVendorClientRepository.findAllByVendorOrganizationIn(orgs).filter { it.clientOrganization != null && it.vendorOrganization != null }

        val orgUserDtos = orgUsers.values.map { it.toDetailsDto() }
        val clientOrgDtos = orgVendorClients.map { ovc -> orgToUserDetailsDto(ovc.clientOrganization!!, orgUsers[ovc.vendorOrganization!!.id]!!) }
        val associatedOrgDtos = (orgUserDtos + clientOrgDtos).distinctBy { it.organization }

        return UserDetailsDto(
            id = u.id.toString(),
            email = u.email,
            lastLogin = u.lastLogin?.toIso(),
            organizations = associatedOrgDtos,
            invitedOrganizations = u.organization_users.filter { it.status == UserStatus.INVITED }.map { it.toDetailsDto() },
            firstName = u.firstName,
            lastName = u.lastName,
            beaconSignature = ssoService.createBeaconSSOToken(u.email),
            productBoardJwt = ssoService.createProductBoardJWT(
                u.id.toString(), u.email, "${u.firstName} ${u.lastName}",
                u.organization_users.firstOrNull { it.status == UserStatus.ACTIVE }?.organization?.name
                    ?: ""
            ),
            globalRole = u.globalRole,
            adminSite = if (u.globalRole != null) {
                adminSite
            } else {
                null
            },
            activeOrganization = u.activeOrganization?.publicId,
            activeOrganizationPhase = u.activeOrganization?.getPhase()
        )
    }

    // Get an OrganizationUserDetailsDto (suitable for user-switching) from an Org and its parent OrgUser.
    fun orgToUserDetailsDto(o: Organization, parent: OrganizationUser): OrganizationUserDetailsDto {
        return OrganizationUserDetailsDto(
            role = parent.role,
            status = parent.status,
            organization = o.publicId,
            organizationName = o.name,
            faviconUrl = o.faviconUrl
        )
    }
}
