package polaris.services.dto

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.dto.cookiescan.GdprCookieConsentCategory
import polaris.models.dto.cookiescan.GdprCookieConsentDto
import polaris.models.dto.toIso
import polaris.models.entity.GdprCookieConsentState
import polaris.models.entity.GdprCookieConsentTracking

@Service
class GdprCookieConsentDtoService {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(GdprCookieConsentDtoService::class.java)
    }

    fun toDto(c: GdprCookieConsentTracking): GdprCookieConsentDto {
        return GdprCookieConsentDto(
            visitorId = c.key,
            consentGranted = getConsentForCookie(c),
            date = c.createdAt.toIso()
        )
    }

    private fun getConsentForCookie(c: GdprCookieConsentTracking): List<GdprCookieConsentCategory> {
        val consents = mutableListOf<GdprCookieConsentCategory>()

        val cookieConsentState: GdprCookieConsentState = try {
            jacksonObjectMapper().readValue(c.consentState)
        } catch (e: JsonProcessingException) {
            log.info("JSON processing exception while converting cookie consents to object: ", e)
            return emptyList()
        }

        if (cookieConsentState.adsPermitted) {
            consents.add(GdprCookieConsentCategory.ADS)
        }

        if (cookieConsentState.analyticsPermitted) {
            consents.add(GdprCookieConsentCategory.ANALYTICS)
        }

        if (cookieConsentState.essentialPermitted) {
            consents.add(GdprCookieConsentCategory.ESSENTIAL)
        }

        if (cookieConsentState.personalizationPermitted) {
            consents.add(GdprCookieConsentCategory.PERSONALIZATION)
        }

        return consents
    }
}
