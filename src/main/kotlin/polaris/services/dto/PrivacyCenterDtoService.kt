package polaris.services.dto

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import polaris.controllers.admin.AdminRelationshipDto
import polaris.controllers.admin.PrivacyCenterAdminDto
import polaris.models.dto.privacycenter.CustomFieldDto
import polaris.models.dto.privacycenter.PrivacyCenterBuilderDto
import polaris.models.dto.privacycenter.PrivacyCenterDetailDto
import polaris.models.dto.privacycenter.PrivacyCenterPolicyDto
import polaris.models.dto.privacycenter.PrivacyCenterPolicySectionDto
import polaris.models.dto.privacycenter.SellingAndSharingDto
import polaris.models.dto.toIso
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.feature.Features
import polaris.models.entity.organization.FINANCIAL_INCENTIVES_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.privacycenter.PrivacyCenterPolicySection
import polaris.repositories.TaskRepository
import polaris.services.primary.OrganizationService
import polaris.services.primary.PrivacyNoticeService
import polaris.services.primary.PrivacyPolicyService
import polaris.services.support.DpoService
import polaris.util.MarkdownRenderer

@Service
class PrivacyCenterDtoService(
    @Value("\${polaris.privacyCenter.bucket}")
    private val privacyCenterBucket: String,
    @Value("\${polaris.domain}")
    private val polarisDomain: String,
    private val privacyNoticeService: PrivacyNoticeService,
    private val privacyPolicyService: PrivacyPolicyService,
    private val dpoService: DpoService,
    private val organizationService: OrganizationService,
    private val taskRepository: TaskRepository

) {

    companion object {
        private val log = LoggerFactory.getLogger(PrivacyCenterDtoService::class.java)
    }

    fun toDetailDto(pc: PrivacyCenter): PrivacyCenterDetailDto {
        val org = pc.organization!!
        val dpoTask = taskRepository.findByOrganizationAndTaskKeyAndCompletedAtIsNull(org, TaskKey.ADD_DPO)
        val dpoRequired = dpoService.isDpoRequired(org)
        val defaultOptOutText = { privacyNoticeService.getDefaultCustomOptOutIntroText(pc) }

        return PrivacyCenterDetailDto(
            id = pc.publicId,
            organizationId = org.publicId,
            name = pc.name,
            faviconUrl = pc.faviconUrl,
            logoUrl = pc.logoUrl,
            logoLinkUrl = pc.logoLinkUrl,
            doNotSellMarkdown = pc.doNotSellMarkdown,
            employeePrivacyNoticeMarkdown = pc.employeePrivacyNoticeMarkdown,
            jobApplicantPrivacyNoticeMarkdown = pc.jobApplicantPrivacyNoticeMarkdown,
            defaultSubdomain = pc.defaultSubdomain,
            customUrl = pc.customUrl,
            customUrlStatus = pc.customUrlStatus,
            customUrlCnameReady = pc.customUrlCnameReady,
            hasMobileApplication = pc.hasMobileApplication,

            ccpaPrivacyNoticeIntroText = pc.ccpaIntro(),
            gdprPrivacyNoticeIntroText = pc.gdprIntro(),
            vcdpaPrivacyNoticeIntroText = pc.vcdpaIntro(),
            cpaPrivacyNoticeIntroText = pc.cpaIntro(),
            ctdpaPrivacyNoticeIntroText = pc.ctdpaIntro(),
            cookiePolicyIntroText = pc.cookiePolicyIntro(),
            optOutNoticeIntroText = pc.optOutIntro(defaultOptOutText),
            retentionIntroText = pc.retentionIntro(),

            privacyPolicyUrl = pc.privacyPolicyUrl,
            requestTollFreePhoneNumber = pc.requestTollFreePhoneNumber,
            requestFormUrl = pc.requestFormUrl,
            requestEmail = pc.requestEmail,
            url = pc.getBaseUrl(),

            dpoOfficerName = pc.dpoOfficerName,
            dpoOfficerEmail = pc.dpoOfficerEmail,
            dpoOfficerPhone = pc.dpoOfficerPhone,
            dpoOfficerAddress = pc.dpoOfficerAddress,
            dpoOfficerDoesNotApply = pc.dpoOfficerDoesNotApply,

            hasDpoTask = dpoTask != null,
            requiresDpo = dpoRequired,

            disableCaResidencyConfirmation = !pc.caResidencyConfirmation
        )
    }

    fun toBuilderDto(pc: PrivacyCenter): PrivacyCenterBuilderDto {
        val org = pc.organization!!
        val isLegacyOrg =
            organizationService.featuresForOrganization(org.publicId).any { it.feature?.name == Features.LEGACY_ORG && it.enabled }

        val incentivesSurvey = organizationService.getSurveyQuestions(org.publicId, FINANCIAL_INCENTIVES_SURVEY_NAME)
        val anyFinancialIncentives = incentivesSurvey.any { it.slug == "offer-consumer-incentives" && it.answer == "true" }

        val renderedSections = privacyPolicyService.getPrivacyPolicy(org)
        val sellingAndSharing = if (isLegacyOrg) {
            SellingAndSharingDto(
                doNotSellHtml = MarkdownRenderer.renderToHtml(pc.doNotSellMarkdown ?: ""),
                enableIbaBrowserOptOut = null,
                enableIbaDirectOptOut = null,
                selling = null,
                sharing = null
            )
        } else {
            privacyNoticeService.getPrivacyCenterSellingAndSharing(org.publicId)
        }
        val applicantGroup = org.collectionGroups.firstOrNull { it.name.contains("Job Applicant") }
        val employeesGroup = org.collectionGroups.firstOrNull { it.name.contains("California Employees") }
        val contractorsGroup = org.collectionGroups.firstOrNull { it.name.contains("California Contractors") }
        val eeaUkEmployeeGroup = org.collectionGroups.firstOrNull { it.autocreationSlug == AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE }
        // TODO: Remove markdown renderer when all policies/notices have migrated to only HTML for the WYSIWYG editor (1/2)

        return PrivacyCenterBuilderDto(
            id = pc.id.toString(),
            publicId = pc.publicId,
            organizationId = pc.organization.publicId,
            name = pc.name,
            isLegacy = isLegacyOrg,
            faviconUrl = pc.faviconUrl,
            url = pc.getBaseUrl(),
            organizationName = pc.organization.name,
            logoUrl = pc.logoUrl,
            logoLinkUrl = pc.getLogoLinkUrlWithScheme(),
            privacyPolicyName = "${pc.name} Privacy Policy",
            sellingAndSharing = sellingAndSharing,
            employeePrivacyNoticeHtml = if (!isLegacyOrg && employeesGroup != null) privacyNoticeService.getEmployeeCollectionGroupNoticeHtml(
                org.publicId, employeesGroup.id
            ) else MarkdownRenderer.renderToHtml(
                pc.employeePrivacyNoticeMarkdown ?: ""
            ),
            contractorPrivacyNoticeHtml = if (contractorsGroup != null) privacyNoticeService.getEmployeeCollectionGroupNoticeHtml(
                org.publicId, contractorsGroup.id
            ) else "",
            jobApplicantPrivacyNoticeHtml = if (!isLegacyOrg && applicantGroup != null) privacyNoticeService.getEmployeeCollectionGroupNoticeHtml(
                org.publicId, applicantGroup.id
            ) else MarkdownRenderer.renderToHtml(
                pc.jobApplicantPrivacyNoticeMarkdown ?: ""
            ),
            eeaUkEmployeePrivacyNoticeHtml = if (eeaUkEmployeeGroup != null) privacyNoticeService.getEeaUkEmployeeNoticeHtml(org, eeaUkEmployeeGroup) else "",
            privacyPolicySections = renderedSections,
            privacyPolicyLastUpdated = pc.policyLastUpdated?.toIso(),
            includeRightToLimitForm = organizationService.requiresNoticeOfRightToLimit(org),
            rightToLimitHtml = privacyNoticeService.getPrivacyCenterRightToLimitHtml(org.publicId, pc),

            hostedUrlBase = "$polarisDomain/api/hosted/${pc.publicId}",
            requestTollFreePhoneNumber = pc.requestTollFreePhoneNumber,
            requestFormUrl = pc.requestFormUrl,
            requestEmail = pc.requestEmail,
            dpoEmail = pc.dpoOfficerEmail,
            dpoPhone = pc.dpoOfficerPhone,

            featureMultistate = org.isMultistate(),

            customField = customFieldDto(pc.organization),

            bucket = privacyCenterBucket,

            caResidencyConfirmation = pc.caResidencyConfirmation,
            hideCcpaSection = pc.hideCcpaSection
        )
    }

    private fun customFieldDto(org: Organization): CustomFieldDto? {
        if (!org.customFieldEnabled || org.customFieldLabel == null) {
            return null
        }
        return CustomFieldDto(
            label = org.customFieldLabel!!,
            help = org.customFieldHelp
        )
    }

    fun toPolicyDto(privacyCenter: PrivacyCenter): PrivacyCenterPolicyDto {
        return PrivacyCenterPolicyDto(
            privacyCenterId = privacyCenter.publicId,
            policyLastUpdated = privacyCenter.policyLastUpdated?.toIso(),
            policySections = privacyCenter.policySections.map { toPolicySectionDto(it) }
        )
    }

    fun toPolicySectionDto(privacyCenterPolicySection: PrivacyCenterPolicySection): PrivacyCenterPolicySectionDto {
        return PrivacyCenterPolicySectionDto(
            id = privacyCenterPolicySection.id.toString(),
            name = privacyCenterPolicySection.name,
            anchor = privacyCenterPolicySection.anchor,
            addToMenu = privacyCenterPolicySection.addToMenu,
            noPrivacyPolicy = privacyCenterPolicySection.noPrivacyPolicy,
            // TODO: Remove markdown renderer when all policies/notices have migrated to only HTML for the WYSIWYG editor (2/2)
            body = MarkdownRenderer.renderToHtml(privacyCenterPolicySection.body),
            showHeader = privacyCenterPolicySection.showHeader,
            displayOrder = privacyCenterPolicySection.displayOrder,
            builtinSlug = null,
            custom = true
        )
    }

    fun toAdminDto(pc: PrivacyCenter) = PrivacyCenterAdminDto(
        id = pc.id.toString(),
        publicId = pc.publicId,
        name = pc.name,
        organization = pc.organization?.let { AdminRelationshipDto(it.publicId, it.name) },
        defaultSubdomain = pc.defaultSubdomain,
        faviconUrl = pc.faviconUrl,
        customUrlStatus = pc.customUrlStatus,
        customUrl = pc.customUrl,
        privacyPolicyUrl = pc.privacyPolicyUrl,
        requestTollFreePhoneNumber = pc.requestTollFreePhoneNumber,
        requestFormUrl = pc.requestFormUrl,
        requestEmail = pc.requestEmail,
        logoUrl = pc.logoUrl,
        cloudfrontDistribution = pc.cloudfrontDistribution
    )

    fun applyAdminDto(dto: PrivacyCenterAdminDto, pc: PrivacyCenter): PrivacyCenter {
        pc.name = dto.name
        pc.defaultSubdomain = dto.defaultSubdomain
        pc.faviconUrl = dto.faviconUrl
        pc.customUrlStatus = dto.customUrlStatus
        pc.customUrl = dto.customUrl
        pc.privacyPolicyUrl = dto.privacyPolicyUrl
        pc.requestTollFreePhoneNumber = dto.requestTollFreePhoneNumber
        pc.requestFormUrl = dto.requestFormUrl
        pc.requestEmail = dto.requestEmail
        pc.logoUrl = dto.logoUrl
        pc.cloudfrontDistribution = dto.cloudfrontDistribution
        return pc
    }
}
