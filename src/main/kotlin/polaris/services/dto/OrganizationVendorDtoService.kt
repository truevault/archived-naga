package polaris.services.dto

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.dto.organization.OrgDataRecipientPlatformDto
import polaris.models.dto.organization.OrganizationDataRecipientDto
import polaris.models.dto.organization.RequestHandlingInstructionEmbeddedDto
import polaris.models.dto.toIso
import polaris.models.dto.vendor.VendorClassificationDto
import polaris.models.dto.vendor.VendorContractDto
import polaris.models.dto.vendor.VendorContractReviewedDto
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.PlatformAppInstall
import polaris.models.entity.organization.mappingProgressDto
import polaris.models.entity.vendor.DataRecipientType
import polaris.models.entity.vendor.ProcessingRegion
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW
import polaris.models.entity.vendor.VendorClassification
import polaris.models.entity.vendor.VendorContract
import polaris.models.entity.vendor.VendorContractReviewed
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.PlatformAppInstallRepository
import polaris.repositories.RequestHandlingInstructionsRepository
import polaris.repositories.VendorClassificationRepository
import polaris.repositories.VendorContractRepository
import polaris.repositories.VendorContractReviewedRepository
import polaris.services.primary.OrganizationVendorSurveyService
import polaris.services.primary.RequestHandlingInstructionsService
import polaris.services.primary.ServiceProviderRecommendationService
import polaris.services.support.DataMapService

@Service
class OrganizationVendorDtoService(
    private val vendorDtoService: VendorDtoService,
    private val organizationVendorSurveyService: OrganizationVendorSurveyService,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val vendorClassificationRepository: VendorClassificationRepository,
    private val organizationDataRecipientRepository: OrganizationDataRecipientRepository,
    private val appInstallRepository: PlatformAppInstallRepository,
    private val vendorContractRepository: VendorContractRepository,
    private val vendorContractReviewedRepository: VendorContractReviewedRepository,
    private val requestHandlingInstructionsRepository: RequestHandlingInstructionsRepository,
    private val serviceProviderRecommendationService: ServiceProviderRecommendationService,
    private val requestHandlingInstructionsService: RequestHandlingInstructionsService,
    private val dataMapService: DataMapService
) {
    companion object {
        private val logger = LoggerFactory.getLogger(OrganizationVendorDtoService::class.java)
    }

    fun toDto(dataRecipient: OrganizationDataRecipient, vendorCategoryCount: Int? = null): OrganizationDataRecipientDto {
        val vendor = dataRecipient.vendor!!
        val organization = dataRecipient.organization!!
        val vendorContract = dataRecipient.vendorContract
        val vendorContractReviewed = dataRecipient.vendorContractReviewed
        val classificationOptions = vendorClassificationRepository.findAll()

        val serviceProviderRecommendation = serviceProviderRecommendationService.getDataRecipientServiceProviderRecommendation(dataRecipient)
        val defaultClassification = serviceProviderRecommendationService.getDefaultClassification(dataRecipient, classificationOptions)

        val classification = dataRecipient.vendorClassification?.let {
            if (it.slug == VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW) {
                null
            } else {
                it
            }
        } ?: defaultClassification

        val vendorDto = OrganizationDataRecipientDto(
            id = dataRecipient.id.toString(),
            vendorId = vendor.id.toString(),
            name = vendor.name,
            vendorKey = vendor.vendorKey,
            aliases = vendor.aliases,
            category = vendor.recipientCategory,
            url = vendor.url,
            ccpaRequestToDeleteLink = vendor.ccpaRequestToDeleteLink,
            ccpaRequestToKnowLink = vendor.ccpaRequestToKnowLink,
            email = dataRecipient.email,
            defaultEmail = vendor.email,
            serviceProviderRecommendation = serviceProviderRecommendation,
            serviceProviderLanguageUrl = vendor.serviceProviderLanguageUrl,
            gdprProcessorRecommendation = vendor.gdprProcessorRecommendation,
            gdprProcessorLanguageUrl = vendor.gdprProcessorLanguageUrl,
            gdprInternationalDataTransferRulesApply = vendor.gdprInternationalDataTransferRulesApply,
            gdprHasSccRecommendation = vendor.gdprHasSccRecommendation,
            gdprSccDocumentationUrl = vendor.gdprSccDocumentationUrl,
            gdprHasSccs = hasSccs(dataRecipient),
            gdprHasUserProvidedSccs = hasUserProvidedSccs(dataRecipient),
            gdprNeedsSccs = needsSccs(dataRecipient),
            dpaApplicable = vendor.dpaApplicable,
            dpaUrl = vendor.dpaUrl,
            helptextAdnetworkSharing = vendor.helptextAdnetworkSharing,
            helptextIntentionalInteractionDisclosure = vendor.helptextIntentionalInteractionDisclosure,
            dataRecipientType = vendor.dataRecipientType,
            logoUrl = vendor.logoFilename?.let { vendorDtoService.logoUrl(vendor.logoFilename) }
                ?: "/assets/images/icon-vendors-black.svg",
            dateAdded = dataRecipient.createdAt.toIso(),
            defaultClassification = vendor.vendorClassification?.let { vendorClassificationToDto(it) },
            classification = classification?.let { vendorClassificationToDto(it) },
            classificationOptions = classificationOptions.map { vendorClassificationToDto(it) },
            vendorContract = vendorContract?.let { vendorContractToDto(it) },
            vendorContractOptions = vendorContractRepository.findAll().map { vendorContractToDto(it) },
            vendorContractReviewed = vendorContractReviewed?.let { vendorContractReviewedToDto(it) },
            vendorContractReviewedOptions = vendorContractReviewedRepository.findAll().map { vendorContractReviewedToDto(it) },
            organization = organization.publicId,
            organizationName = organization.name,
            status = dataRecipient.status!!.label,
            publicTosUrl = dataRecipient.publicTosUrl,
            ccpaIsSelling = dataRecipient.ccpaIsSelling,
            ccpaIsSharing = dataRecipient.ccpaIsSharing,
            usesCustomAudience = dataRecipient.usesCustomAudience,
            deletionInstructions = dataRecipient.vendor.deletionInstructions,
            accessInstructions = dataRecipient.vendor.gdprAccessInstructions,
            optOutInstructions = dataRecipient.vendor.optOutInstructions,
            generalGuidance = dataRecipient.vendor.generalGuidance,
            specialCircumstancesGuidance = dataRecipient.vendor.specialCircumstancesGuidance,
            recommendedPersonalInformationCategories = dataRecipient.vendor.recommendedPICIds,
            isCustom = dataRecipient.vendor.organization != null,
            contacted = dataRecipient.contacted,
            isDataStorage = dataRecipient.isDataStorage,
            mappingProgress = mappingProgressDto(dataRecipient.mappingProgress),
            dataAccessibility = dataRecipient.dataAccessibility,
            dataDeletability = dataRecipient.dataDeletability,
            automaticallyClassified = dataRecipient.automaticallyClassified,
            dataMapped = if (vendorCategoryCount != null) vendorCategoryCount > 0 else null,
            instructions = requestHandlingInstructionsRepository.findAllByOrganizationAndOrganizationDataRecipient(organization, dataRecipient).map {
                RequestHandlingInstructionEmbeddedDto(
                    it.requestType!!,
                    it.processingMethod,
                    it.processingInstructions,
                    it.deleteDoNotContact,
                    it.hasRetentionExceptions,
                    it.doNotContactReason,
                    it.doNotContactExplanation
                )
            },

            tosFileName = dataRecipient.tosFileName,
            tosFileKey = dataRecipient.tosFileKey,
            gdprProcessorSetting = dataRecipient.gdprProcessorLockedSetting ?: dataRecipient.gdprProcessorSetting ?: vendor.gdprProcessorRecommendation,
            isGdprProcessorSettingLocked = dataRecipient.gdprProcessorLockedSetting != null,
            gdprProcessorGuaranteeUrl = dataRecipient.gdprProcessorGuaranteeUrl,
            gdprProcessorGuaranteeFileName = dataRecipient.gdprProcessorGuaranteeFileName,
            gdprProcessorGuaranteeFileKey = dataRecipient.gdprProcessorGuaranteeFileKey,
            gdprControllerGuaranteeUrl = dataRecipient.gdprControllerGuaranteeUrl,
            gdprControllerGuaranteeFileName = dataRecipient.gdprControllerGuaranteeFileName,
            gdprControllerGuaranteeFileKey = dataRecipient.gdprControllerGuaranteeFileKey,
            gdprContactUnsafeTransfer = dataRecipient.gdprContactUnsafeTransfer,
            gdprEnsureSccInPlace = dataRecipient.gdprEnsureSccInPlace,
            gdprContactProcessorStatus = dataRecipient.gdprContactProcessorStatus,

            gdprHasSccSetting = dataRecipient.gdprHasSccSetting,
            gdprSccUrl = dataRecipient.gdprSccUrl,
            gdprSccFileName = dataRecipient.gdprSccFileName,
            gdprSccFileKey = dataRecipient.gdprSccFileKey,

            removedFromExceptionsToScc = dataRecipient.removedFromExceptionsToScc,
            isExceptedFromSccs = dataRecipient.isExceptedFromSccs(),
            isPotentiallyExceptedFromSccs = dataRecipient.isPotentiallyExceptedFromSccs(),

            completed = isComplete(dataRecipient),

            disclosedToByEeaUkGroup = disclosedToByCollectionGroupInRegion(dataRecipient, ProcessingRegion.EEA_UK),
            disclosedToByUsGroup = disclosedToByCollectionGroupInRegion(dataRecipient, ProcessingRegion.UNITED_STATES),

            isPotentialIntentionalInteractionVendor = dataRecipient.isPotentialIntentionalInteractionVendor(),
            isAutomaticIntentionalInteractionVendor = dataRecipient.isAutomaticIntentionalInteractionVendor(),
            isPotentialSellingVendor = organizationVendorSurveyService.isPotentialSellingVendor(organization, dataRecipient),
            isAutomaticSellingVendor = organizationVendorSurveyService.isAutomaticSellingVendor(organization, dataRecipient),
            isPotentialUploadVendor = organizationVendorSurveyService.isPotentialUploadVendor(organization, dataRecipient),
            isAutomaticUploadVendor = organizationVendorSurveyService.isAutomaticUploadVendor(organization, dataRecipient),
            isUploadVendor = dataRecipient.isUploadVendor,
            isPlatform = vendor.isPlatform ?: false,
            isStandalone = vendor.isStandalone ?: false,
            isInstalled = vendor.respectsPlatformRequests && installedIn(dataRecipient).isNotEmpty(),
            respectsPlatformRequests = vendor.respectsPlatformRequests,
            defaultRetentionReasons = vendor.defaultRetentionReasons.map { it.slug },
            processingRegions = dataRecipient.processingRegions,
            collectionContext = dataRecipient.collectionContexts
        )
        return vendorDto
    }

    fun toPlatformDto(recipient: OrganizationDataRecipient): OrgDataRecipientPlatformDto {
        val orgRecipients = organizationDataRecipientRepository.findAllByOrganization(recipient.organization!!)
        val allInstalled = appInstallRepository.findAllByPlatform(recipient)

        val availableApps = orgRecipients
            .filter { r ->
                r.vendor!!.isPlatform != true &&
                    r.vendor.isStandalone != true &&
                    r.vendor.dataRecipientType == DataRecipientType.vendor
            }
            .sortedBy { r -> r.vendor!!.name }

        val available = availableApps.map { toDto(it) }
        val installed = allInstalled.filter { install -> availableApps.contains(install.app!!) }
        val installedUuid = installed.map { it.app!!.vendor!!.id }

        return OrgDataRecipientPlatformDto(
            id = recipient.id.toString(),
            vendorId = recipient.vendor!!.id.toString(),
            name = recipient.vendor.name,
            vendorKey = recipient.vendor.vendorKey,
            logoUrl = recipient.vendor.logoFilename?.let { vendorDtoService.logoUrl(recipient.vendor.logoFilename) }
                ?: "/assets/images/icon-vendors-black.svg",
            available = available,
            installed = installedUuid
        )
    }

    fun installedIn(app: OrganizationDataRecipient): List<PlatformAppInstall> {
        val orgRecipients = organizationDataRecipientRepository.findAllByOrganization(app.organization!!)
        val allMyPlatforms = appInstallRepository.findAllByApp(app)
        val availablePlatforms = orgRecipients.filter { r -> r.vendor!!.isPlatform == true }
        val installed = allMyPlatforms.filter {
                install ->
            availablePlatforms.contains(install.platform!!)
        }

        return installed
    }

    fun toAppDto(app: OrganizationDataRecipient): OrgDataRecipientPlatformDto {
        val installed = installedIn(app)
        val orgRecipients = organizationDataRecipientRepository.findAllByOrganization(app.organization!!)

        val availablePlatforms = orgRecipients.filter { r -> r.vendor!!.isPlatform == true }
        val available = availablePlatforms.map { toDto(it) }
        val installedUuid = installed.map { it.platform!!.vendor!!.id }

        return OrgDataRecipientPlatformDto(
            id = app.id.toString(),
            vendorId = app.vendor!!.id.toString(),
            name = app.vendor.name,
            vendorKey = app.vendor.vendorKey,
            logoUrl = app.vendor.logoFilename?.let { vendorDtoService.logoUrl(app.vendor.logoFilename) }
                ?: "/assets/images/icon-vendors-black.svg",
            available = available,
            installed = installedUuid
        )
    }

    fun vendorClassificationToDto(vendorClassification: VendorClassification) = VendorClassificationDto(
        id = vendorClassification.id.toString(),
        name = vendorClassification.name!!,
        slug = vendorClassification.slug!!,
        displayOrder = vendorClassification.displayOrder,
        tooltip = vendorClassification.tooltipText
    )

    fun vendorContractToDto(vendorContract: VendorContract) = VendorContractDto(
        id = vendorContract.id.toString(),
        name = vendorContract.name!!,
        slug = vendorContract.slug!!,
        displayOrder = vendorContract.displayOrder,
        tooltip = vendorContract.tooltipText,
        contractUrl = vendorContract.contractUrl
    )

    fun vendorContractReviewedToDto(vendorContractReviewed: VendorContractReviewed) = VendorContractReviewedDto(
        id = vendorContractReviewed.id.toString(),
        name = vendorContractReviewed.name!!,
        slug = vendorContractReviewed.slug!!,
        displayOrder = vendorContractReviewed.displayOrder,
        tooltip = vendorContractReviewed.tooltipText
    )

    fun hasSccs(dataRecipient: OrganizationDataRecipient): Boolean {
        val vendor = dataRecipient.vendor!!
        val hasConfiguredSccs = vendor.gdprHasSccRecommendation
        return needsSccs(dataRecipient) && hasConfiguredSccs
    }

    fun hasUserProvidedSccs(dataRecipient: OrganizationDataRecipient): Boolean {
        return !dataRecipient.gdprSccUrl.isNullOrBlank() || (!dataRecipient.gdprSccFileKey.isNullOrBlank() && !dataRecipient.gdprSccFileName.isNullOrBlank())
    }

    fun needsSccs(dataRecipient: OrganizationDataRecipient): Boolean {
        val vendor = dataRecipient.vendor!!
        return dataRecipient.processingRegions.contains(ProcessingRegion.EEA_UK) && vendor.gdprInternationalDataTransferRulesApply == true
    }

    fun isComplete(dataRecipient: OrganizationDataRecipient): Boolean {
        val isVendor = dataRecipient.vendor?.dataRecipientType == DataRecipientType.vendor
        val isStorageLocation = dataRecipient.vendor?.dataRecipientType == DataRecipientType.storage_location
        val isConsumer = dataRecipient.collectionContexts.contains(CollectionContext.CONSUMER)
        val classification = dataRecipient.vendorClassification?.let {
            if (it.slug == VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW) {
                null
            } else {
                it
            }
        }

        val hasClassification = { !isConsumer || isStorageLocation || classification != null }

        // only vendors are required to have request handling instructions
        val hasRequestHandlingInstruction = { !isVendor || !dataRecipient.requestHandlingInstructions.isNullOrEmpty() }
        // all vendor types (storage_locations, contractors, service providers, etc) are required to have deletion instructions
        val hasValidDeletionInstruction =
            { requestHandlingInstructionsService.hasValidDeletionInstruction(dataRecipient) }
        // only vendors are required to have disclosed PIC
        val hasDisclosedPic = { !isVendor || dataMapService.disclosureForDataRecipient(dataRecipient).isNotEmpty() }

        return hasClassification() && hasRequestHandlingInstruction() && hasValidDeletionInstruction() && hasDisclosedPic()
    }

    fun disclosedToByCollectionGroupInRegion(dataRecipient: OrganizationDataRecipient, processingRegion: ProcessingRegion): Boolean {
        val regionCollectionGroups = dataRecipient.organization?.collectionGroups?.filter { it.processingRegions.contains(processingRegion) }

        if (regionCollectionGroups == null || regionCollectionGroups.isEmpty()) {
            return false
        }

        return regionCollectionGroups.any { dataRecipient.vendor?.disclosuresForCollectionGroup(it)?.isNotEmpty() == true }
    }
}
