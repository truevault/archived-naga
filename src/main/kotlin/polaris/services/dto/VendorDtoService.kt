package polaris.services.dto

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.repository.findByIdOrNull
import polaris.controllers.admin.AdminRelationshipDto
import polaris.controllers.admin.VendorAdminDto
import polaris.controllers.admin.VendorAdminInboundDto
import polaris.models.UUIDString
import polaris.models.dto.request.VendorOutcomeEmbeddedDto
import polaris.models.dto.toIso
import polaris.models.dto.vendor.VendorDto
import polaris.models.entity.organization.RequestHandlingInstruction
import polaris.models.entity.request.VendorOutcome
import polaris.models.entity.vendor.Vendor
import polaris.models.entity.vendor.VendorClassification
import polaris.models.entity.vendor.VendorContract
import polaris.models.entity.vendor.VendorContractReviewed
import polaris.repositories.RetentionReasonRepository
import polaris.repositories.VendorClassificationRepository
import polaris.repositories.VendorContractRepository
import polaris.repositories.VendorContractReviewedRepository
import polaris.repositories.VendorRepository
import java.time.ZonedDateTime
import java.util.UUID

@org.springframework.stereotype.Service
class VendorDtoService(
    @Value("\${polaris.images.baseUrl}")
    private val imagesBaseUrl: String?,
    private val vendorRepository: VendorRepository,
    private val vendorClassificationRepository: VendorClassificationRepository,
    private val vendorContractRepository: VendorContractRepository,
    private val vendorContractReviewedRepository: VendorContractReviewedRepository,
    private val retentionReasonRepo: RetentionReasonRepository
) {

    companion object {
        private val log = LoggerFactory.getLogger(VendorDtoService::class.java)
    }

    fun toDto(v: Vendor) = VendorDto(
        id = v.id.toString(),
        name = v.name!!,
        vendorKey = v.vendorKey,
        aliases = v.aliases,
        category = v.recipientCategory,
        sourceCategory = v.recipientCategory,
        url = v.url,
        ccpaRequestToDeleteLink = v.ccpaRequestToDeleteLink,
        deletionInstructions = v.deletionInstructions,
        optOutInstructions = v.optOutInstructions,
        generalGuidance = v.generalGuidance,
        specialCircumstancesGuidance = v.specialCircumstancesGuidance,
        ccpaRequestToKnowLink = v.ccpaRequestToKnowLink,
        email = v.email,
        serviceProviderRecommendation = v.serviceProviderRecommendation,
        dataRecipientType = v.dataRecipientType,
        serviceProviderLanguageUrl = v.serviceProviderLanguageUrl,
        dpaApplicable = v.dpaApplicable,
        dpaUrl = v.dpaUrl,
        logoUrl = v.logoFilename?.let { logoUrl(v.logoFilename) } ?: "/assets/images/icon-vendors-black.svg",
        defaultVendorClassification = v.vendorClassification?.toDto(),
        organizationId = v.organization?.let { it.publicId },
        recommendedPersonalInformationCategories = v.recommendedPICIds,
        isCustom = v.organization != null, // this is kind of hand-wavy; is this actually a good way to check this
        isDataRecipient = v.isDataRecipient,
        isDataSource = v.isDataSource,
        isStandalone = v.isStandalone,
        isPlatform = v.isPlatform,
        defaultRetentionReasons = v.defaultRetentionReasons.map { it.slug },
        isPotentialIntentionalInteractionVendor = v.isPotentialIntentionalInteractionVendor(),
        isAutomaticIntentionalInteractionVendor = v.isAutomaticIntentionalInteractionVendor()
    )

    fun <T>logged(t: T): T {
        log.info("\n\n\n{t}\n\n\n")
        return t
    }

    fun toAdminDto(v: Vendor) = VendorAdminDto(
        id = v.id.toString(),
        name = v.name,
        vendorKey = v.vendorKey,
        aliases = v.aliases?.joinToString(separator = ", "),
        category = v.recipientCategory,
        disclosureSourceCategory = v.disclosureSourceCategory,
        url = v.url,
        ccpaRequestToDeleteLink = v.ccpaRequestToDeleteLink,
        deletionInstructions = v.deletionInstructions,
        optOutInstructions = v.optOutInstructions,
        generalGuidance = v.generalGuidance,
        specialCircumstancesGuidance = v.specialCircumstancesGuidance,
        ccpaRequestToKnowLink = v.ccpaRequestToKnowLink,
        email = v.email,
        recommendedPersonalInformationCategories = v.recommendedPICIds,
        serviceProviderRecommendation = v.serviceProviderRecommendation,
        serviceProviderLanguageUrl = v.serviceProviderLanguageUrl,
        dpaApplicable = v.dpaApplicable,
        dpaUrl = v.dpaUrl,
        logoFilename = v.logoFilename,
        logoUrl = logoUrl(v.logoFilename),
        vendorClassification = v.vendorClassification?.let {
            AdminRelationshipDto(
                it.id.toString(),
                it.name ?: "(no name)"
            )
        },
        // might need to add something here
        categories = vendorRepository.findDistinctRecipientCategories(v.organization),
        existingNames = vendorRepository.findDistinctNames(v.organization).filter { it != v.name },
        lastReviewed = v.lastReviewed?.toIso(),
        isDataRecipient = v.isDataRecipient,
        isDataSource = v.isDataSource,
        isPlatform = v.isPlatform ?: false,
        isStandalone = v.isStandalone ?: false,
        respectsPlatformRequests = v.respectsPlatformRequests,
        isMigrationOnly = v.isMigrationOnly,
        gdprDpaApplicable = v.gdprDpaApplicable,
        gdprDpaUrl = v.gdprDpaUrl,
        gdprInternationalDataTransferRulesApply = v.gdprInternationalDataTransferRulesApply,
        gdprProcessorLanguageUrl = v.gdprProcessorLanguageUrl,
        gdprProcessingPurpose = v.gdprProcessingPurpose,
        gdprProcessorRecommendation = v.gdprProcessorRecommendation,
        gdprLastReviewed = v.gdprLastReviewed?.toIso(),
        gdprAccessInstructions = v.gdprAccessInstructions,
        gdprMultiPurposeVendor = v.gdprMultiPurposeVendor,
        gdprLawfulBasis = v.gdprLawfulBasis,
        gdprHasSccRecommendation = v.gdprHasSccRecommendation,
        gdprSccDocumentationUrl = v.gdprSccDocumentationUrl,
        isWalletService = v.isWalletService,

        defaultRetentionReasons = v.defaultRetentionReasons.map { it.id.toString() }
    )

    fun applyAdminDto(dto: VendorAdminInboundDto, v: Vendor): Vendor {
        v.name = dto.name ?: v.name
        v.vendorKey = dto.vendorKey
        v.aliases = dto.aliases?.split(",")?.filter { it.isNotBlank() }?.map { it.trim() }
        v.recipientCategory = dto.category
        v.disclosureSourceCategory = dto.disclosureSourceCategory
        v.url = dto.url
        v.ccpaRequestToDeleteLink = dto.ccpaRequestToDeleteLink
        v.deletionInstructions = dto.deletionInstructions
        v.optOutInstructions = dto.optOutInstructions
        v.generalGuidance = dto.generalGuidance
        v.specialCircumstancesGuidance = dto.specialCircumstancesGuidance
        v.ccpaRequestToKnowLink = dto.ccpaRequestToKnowLink
        v.email = dto.email
        v.recommendedPICIds = dto.recommendedPersonalInformationCategories?.filter { it.isNotBlank() }?.map { it.trim() }
        v.serviceProviderRecommendation = dto.serviceProviderRecommendation
        v.serviceProviderLanguageUrl = dto.serviceProviderLanguageUrl
        v.dpaApplicable = dto.dpaApplicable
        v.dpaUrl = dto.dpaUrl
        v.logoFilename = dto.logoFilename
        v.lastReviewed = if (dto.lastReviewed == null) { null } else { ZonedDateTime.parse(dto.lastReviewed) }
        v.gdprDpaApplicable = dto.gdprDpaApplicable
        v.gdprDpaUrl = dto.gdprDpaUrl
        v.gdprInternationalDataTransferRulesApply = dto.gdprInternationalDataTransferRulesApply
        v.gdprProcessorLanguageUrl = dto.gdprProcessorLanguageUrl
        v.gdprProcessingPurpose = dto.gdprProcessingPurpose
        v.gdprProcessorRecommendation = dto.gdprProcessorRecommendation
        v.gdprLastReviewed = if (dto.gdprLastReviewed == null) { null } else { ZonedDateTime.parse(dto.gdprLastReviewed) }
        v.gdprAccessInstructions = dto.gdprAccessInstructions
        v.gdprMultiPurposeVendor = dto.gdprMultiPurposeVendor
        v.gdprLawfulBasis = dto.gdprLawfulBasis
        v.gdprHasSccRecommendation = dto.gdprHasSccRecommendation
        v.gdprSccDocumentationUrl = dto.gdprSccDocumentationUrl
        v.isWalletService = dto.isWalletService

        if (dto.isDataRecipient != null) {
            v.isDataRecipient = dto.isDataRecipient
        }

        if (dto.isDataSource != null) {
            v.isDataSource = dto.isDataSource
        }

        if (dto.isPlatform != null) {
            v.isPlatform = dto.isPlatform
        }

        if (dto.isStandalone != null) {
            v.isStandalone = dto.isStandalone
        }
        v.respectsPlatformRequests = dto.respectsPlatformRequests
        v.isMigrationOnly = dto.isMigrationOnly

        v.defaultRetentionReasons = retentionReasonRepo.findAllById(dto.defaultRetentionReasons.map { UUID.fromString(it) }).toMutableSet()

        return v
    }

    fun vendorOutcomeToEmbeddedDto(v: VendorOutcome, instructions: List<RequestHandlingInstruction>): VendorOutcomeEmbeddedDto {
        val matchingInstruction = instructions.find { it.organizationDataRecipient?.vendor == v.vendor }

        return VendorOutcomeEmbeddedDto(
            vendor = toDto(v.vendor!!),
            processingInstructions = matchingInstruction?.processingInstructions ?: "",
            processed = v.processed,
            outcome = v.outcome
        )
    }

    private fun findVendorClassification(id: UUIDString?): VendorClassification? {
        val uuid = id?.let { UUID.fromString(id) } ?: return null
        return vendorClassificationRepository.findByIdOrNull(uuid)
    }

    private fun findVendorContract(id: UUIDString?): VendorContract? {
        val uuid = id?.let { UUID.fromString(id) } ?: return null
        return vendorContractRepository.findByIdOrNull(uuid)
    }

    private fun findVendorContractReviewed(id: UUIDString?): VendorContractReviewed? {
        val uuid = id?.let { UUID.fromString(id) } ?: return null
        return vendorContractReviewedRepository.findByIdOrNull(uuid)
    }

    fun logoUrl(logoFilename: String?): String? {
        return logoFilename?.let { "$imagesBaseUrl/$logoFilename" }
    }
}
