package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.data.repository.findByIdOrNull
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.ResourceNotFoundException
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.dto.compliance.BusinessPurposeDto
import polaris.models.dto.compliance.DataInventorySnapshotDto
import polaris.models.dto.compliance.OrgPersonalInformationSnapshotDto
import polaris.models.dto.compliance.PersonalInformationCategoryDto
import polaris.models.dto.compliance.PersonalInformationCollectedDto
import polaris.models.dto.compliance.PersonalInformationExchangedDto
import polaris.models.dto.compliance.PersonalInformationSourceDto
import polaris.models.dto.compliance.SnapshotCategoryDto
import polaris.models.dto.compliance.UpdatePersonalInformationExchangedDto
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.organization.BusinessPurposeType
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.CollectionGroupCollectedPIC
import polaris.models.entity.organization.DataRecipientCollectionGroup
import polaris.models.entity.organization.DataRecipientDisclosedPIC
import polaris.models.entity.organization.OrganizationBusinessPurpose
import polaris.models.entity.organization.OrganizationDataSource
import polaris.repositories.BusinessPurposeRepository
import polaris.repositories.CollectionGroupCollectedPICRepo
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.DataRecipientCollectionGroupRepo
import polaris.repositories.DataRecipientDisclosedPICRepo
import polaris.repositories.OrganizationBusinessPurposeRepository
import polaris.repositories.OrganizationDataSourceRepository
import polaris.repositories.PersonalInformationCategoryRepository
import polaris.services.support.PrivacyCenterBuilderService
import polaris.util.PolarisException
import java.util.UUID

@org.springframework.stereotype.Service
@Transactional
class PersonalInformationService(
    private val personalInformationCategoryRepository: PersonalInformationCategoryRepository,
    private val businessPurposeRepository: BusinessPurposeRepository,
    private val collectedPICRepo: CollectionGroupCollectedPICRepo,
    private val orgDataSourceRepo: OrganizationDataSourceRepository,
    private val orgBusinessPurposeRepo: OrganizationBusinessPurposeRepository,
    private val exchangedPICRepo: DataRecipientDisclosedPICRepo,
    private val organizationVendorService: OrganizationVendorService,
    private val collectionGroupRepository: CollectionGroupRepository,
    private val dataRecipientCollectionGroupRepo: DataRecipientCollectionGroupRepo,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService,
    private val lookup: LookupService
) {

    companion object {
        val log = LoggerFactory.getLogger(PersonalInformationService::class.java)
    }

    fun getDefaultPersonalInformationCollected(organizationId: OrganizationPublicId): PersonalInformationCollectedDto {
        return PersonalInformationCollectedDto(
            personalInformationCategories = personalInformationCategoryRepository.findAll().filter { !it.deprecated }.map {
                it.toDto()
            },
            businessPurposes = businessPurposeRepository.findAll().map {
                it.toDto()
            },
            exchanged = emptyList()
        )
    }

    // Used for public-facing disclosures/notices and will therefore use different category group names for disclosure purposes
    fun getOrganizationSnapshot(organizationId: OrganizationPublicId): OrgPersonalInformationSnapshotDto {
        val organization = lookup.orgOrThrow(organizationId)
        val dataSubjectTypes = collectionGroupRepository.findAllByOrganization(organization)
        val collectedPIC = collectedPICRepo.findAllByCollectionGroup_Organization(organization).groupBy { it.collectionGroup!!.id }
        val dataSources = orgDataSourceRepo.findAllByOrganization(organization)
        val purposes = orgBusinessPurposeRepo.findAllByOrganizationAndBusinessPurposeType(organization, BusinessPurposeType.CONSUMER)
        val employmentPurposes = orgBusinessPurposeRepo.findAllByOrganizationAndBusinessPurposeType(organization, BusinessPurposeType.EMPLOYMENT)

        // val purposesByDst = .groupBy { it.collectionGroup!!.id }
        val shouldIncludeDst = fun(dst: CollectionGroup): Boolean {
            val dstIsUsed = collectedPIC.keys.toList().contains(dst.id)
            return dst.enabled && dstIsUsed
        }

        val dataInventory = dataSubjectTypes.filter(shouldIncludeDst).map {
            val isConsumer = it.collectionGroupType == CollectionGroupType.BUSINESS_TO_CONSUMER
            val isEmployment = it.collectionGroupType == CollectionGroupType.EMPLOYMENT
            val groupPurposes = if (isEmployment) employmentPurposes else purposes

            DataInventorySnapshotDto(
                dataSubjectTypeId = it.id.toString(),
                dataSubjectTypeName = it.name,
                dataSubjectTypeDescriptor = if (!it.description.isNullOrBlank()) "people who ${it.description}" else it.name,
                consumerGroupTypeName = it.collectionGroupType.label,
                consumerGroupTypeDisplayOrder = it.collectionGroupType.ordinal + 1,
                isConsumerType = isConsumer,
                personalInformationCategories = collectedPIC.getOrDefault(it.id, emptyList()).map { collected ->
                    val exchangedWith = collected.disclosedToVendors
                    val dataRecipients = exchangedWith.map { exchanged -> lookup.orgDataRecipientOrThrow(organization, exchanged.vendor!!) }
                    val sellingRecipients = dataRecipients.filter { recipient -> recipient.ccpaIsSelling }
                    val sharingRecipients = dataRecipients.filter { recipient -> recipient.ccpaIsSharing }

                    SnapshotCategoryDto(
                        id = collected.pic!!.id.toString(),
                        name = collected.pic.name!!,
                        description = collected.pic.description,
                        alwaysPreserveCasing = collected.pic.alwaysPreserveCasing,
                        groupName = if (collected.pic.group!!.useForNoticeDisclosure) collected.pic.group.name!! else collected.pic.name,
                        groupOrder = collected.pic.group.displayOrder,
                        groupDisplayOrder = collected.pic.groupDisplayOrder,
                        collected = true,
                        exchanged = exchangedWith.isNotEmpty(),
                        vendorsExchangedWith = exchangedWith.map { exchanged -> exchanged.vendor!!.id.toString() },
                        isShared = sharingRecipients.isNotEmpty(),
                        vendorsSharedWith = sharingRecipients.map { sharing -> sharing.vendor!!.id.toString() },
                        isSold = sellingRecipients.isNotEmpty(),
                        vendorsSoldTo = sellingRecipients.map { sharing -> sharing.vendor!!.id.toString() }

                    )
                },
                businessPurposes = groupPurposes.map { obp ->
                    obp.businessPurpose!!.toDto(true)
                }
            )
        }

        val sources = dataSources.map {
            val category = it.vendor!!.disclosureSourceCategory()

            PersonalInformationSourceDto(
                id = null,
                name = it.vendor.name!!,
                isCustom = it.vendor.organization != null,
                category = category,
                displayOrder = if (category == PersonalInformationSourceDto.YOU_CATEGORY) 99999 else null
            )
        }

        return OrgPersonalInformationSnapshotDto(sources, dataInventory)
    }

    fun getCollectedPIC(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID
    ): List<CollectionGroupCollectedPIC> {
        val collectionGroup = lookup.collectionGroupOrThrow(organizationId, dataSubjectTypeId)
        return collectedPICRepo.findAllByCollectionGroup(collectionGroup)
    }

    fun getCollectionGroupPIC(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID
    ): List<PersonalInformationCategoryDto> {
        val collectedPIC = getCollectedPIC(organizationId, dataSubjectTypeId)
        val allPersonalInformationCategories = personalInformationCategoryRepository.findAll()
        val collectedPICIds = collectedPIC.map { it.pic!!.id }
        val exchangedPICIds = collectedPIC.filter { it.disclosedToVendors.isNotEmpty() }.map { it.pic!!.id }
        return allPersonalInformationCategories.filter { !it.deprecated || collectedPICIds.contains(it.id) }.map {
            it.toDto(
                collected = collectedPICIds.contains(it.id),
                exchanged = exchangedPICIds.contains(it.id)
            )
        }
    }
    fun setOrganizationDataSubjectTypePersonalInformationCategories(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID,
        categoryIds: List<UUID>
    ) {
        val dataSubjectType = lookup.collectionGroupOrThrow(organizationId, dataSubjectTypeId)
        val existing = getCollectedPIC(
            organizationId,
            dataSubjectTypeId
        ).associateBy { it.pic!!.id }
        val newIds = categoryIds.subtract(existing.keys)
        val removeIds = existing.keys.subtract(categoryIds)
        collectedPICRepo.saveAll(
            newIds.map {
                CollectionGroupCollectedPIC(
                    collectionGroup = dataSubjectType,
                    pic = personalInformationCategoryRepository.findByIdOrNull(it)
                        ?: throw PolarisException("Unable to find PersonalInformationCategory id $it")
                )
            }
        )
        collectedPICRepo.deleteAll(existing.filterKeys { removeIds.contains(it) }.values)
    }

    fun addOrganizationDataSubjectTypePersonalInformationCategory(
        orgId: OrganizationPublicId,
        collectionGroupId: UUID,
        categoryId: UUID
    ): CollectionGroupCollectedPIC? {
        val collectionGroup = lookup.collectionGroupOrThrow(orgId, collectionGroupId)
        val pic = lookup.picOrThrow(categoryId)
        collectedPICRepo.insertUniquely(collectionGroup.id, pic.id)
        return collectedPICRepo.findByCollectionGroupAndPic(
            collectionGroup, pic
        )
    }

    fun removeOrganizationDataSubjectTypePersonalInformationCategory(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID,
        categoryId: UUID
    ) {
        val collectionGroup = lookup.collectionGroupOrThrow(organizationId, dataSubjectTypeId)
        val personalInformationCategory = lookup.picOrThrow(categoryId)
        collectedPICRepo.findByCollectionGroupAndPic(collectionGroup, personalInformationCategory)?.let {
            if (it.disclosedToVendors.isNotEmpty()) {
                exchangedPICRepo.deleteAll(it.disclosedToVendors)
            }
            collectedPICRepo.delete(it)
        }
    }

    fun addOrganizationDataSource(organizationId: OrganizationPublicId, vendorId: UUID) {
        val o = lookup.orgOrThrow(organizationId)
        val v = lookup.vendorForOrgOrThrow(organizationId, vendorId)
        orgDataSourceRepo.save(OrganizationDataSource(organizationId = o.id, vendorId = v.id))
    }

    fun addOrganizationDataSubjectTypePersonalInformationPurpose(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID,
        purposeId: UUID
    ) {
        val org = lookup.orgOrThrow(organizationId)
        val collectionGroup = lookup.collectionGroupOrThrow(organizationId, dataSubjectTypeId)
        val businessPurpose = lookupPurposeOrThrow(purposeId)

        orgBusinessPurposeRepo.insertUniquely(org.id, businessPurpose.id, collectionGroup.collectionGroupType.businessPurposeType.toString())
    }

    fun removeOrganizationDataSubjectTypePersonalInformationPurpose(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID,
        purposeId: UUID
    ) {
        val org = lookup.orgOrThrow(organizationId)
        val dataSubjectType = lookup.collectionGroupOrThrow(organizationId, dataSubjectTypeId)
        val businessPurpose = lookupPurposeOrThrow(purposeId)
        orgBusinessPurposeRepo.findByOrganizationAndBusinessPurposeAndBusinessPurposeType(org, businessPurpose, dataSubjectType.collectionGroupType.businessPurposeType)?.let { orgBusinessPurposeRepo.delete(it) }
    }

    fun getOrganizationDataSubjectTypeBusinessPurposes(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID
    ): List<OrganizationBusinessPurpose> {
        val org = lookup.orgOrThrow(organizationId)
        val collectionGroup = lookup.collectionGroupOrThrow(organizationId, dataSubjectTypeId)
        return orgBusinessPurposeRepo.findAllByOrganizationAndBusinessPurposeType(org, collectionGroup.collectionGroupType.businessPurposeType)
    }

    fun getOrganizationDataSubjectTypeBusinessPurposesDto(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID
    ): List<BusinessPurposeDto> {
        val organizationDstPurposes = getOrganizationDataSubjectTypeBusinessPurposes(organizationId, dataSubjectTypeId)
        val allPurposes = businessPurposeRepository.findAll()
        val orgDstPurposeIds = organizationDstPurposes.map { it.businessPurpose!!.id }
        return allPurposes.map {
            it.toDto(orgDstPurposeIds.contains(it.id))
        }
    }

    fun setOrganizationDataSubjectTypeBusinessPurposes(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID,
        sourceIds: List<UUID>
    ) {
        val collectionGroup = lookup.collectionGroupOrThrow(organizationId, dataSubjectTypeId)
        val existing = getOrganizationDataSubjectTypeBusinessPurposes(
            organizationId,
            dataSubjectTypeId
        ).associateBy { it.businessPurpose!!.id }
        val newIds = sourceIds.subtract(existing.keys)
        val removeIds = existing.keys.subtract(sourceIds)
        orgBusinessPurposeRepo.saveAll(
            newIds.map {
                OrganizationBusinessPurpose(
                    organizationId = collectionGroup.organization!!.id,
                    businessPurposeId = businessPurposeRepository.findByIdOrNull(it)?.id
                        ?: throw PolarisException("Unable to find BusinessPurpose id $it"),
                    businessPurposeType = collectionGroup.collectionGroupType.businessPurposeType
                )
            }
        )
        orgBusinessPurposeRepo.deleteAll(existing.filterKeys { removeIds.contains(it) }.values)
    }

    fun getExchangedPIC(
        organizationId: OrganizationPublicId,
        vendorId: UUID
    ): List<DataRecipientDisclosedPIC> {
        val organization = lookup.orgOrThrow(organizationId)
        val vendor = lookup.vendorForOrgOrThrow(organizationId, vendorId)
        return exchangedPICRepo.findAllByCollectedPIC_CollectionGroup_OrganizationAndVendor(organization, vendor)
    }

    fun getOrgConsumerGroupExchangedCategoriesDto(organizationId: OrganizationPublicId, dataSubjectTypeId: UUID): List<PersonalInformationExchangedDto> {
        val collectionGroup = lookup.collectionGroupOrThrow(organizationId, dataSubjectTypeId)

        val collectedPIC = getCollectedPIC(organizationId, dataSubjectTypeId)
        val collectedPICIds = collectedPIC.map { it.pic!!.id }
        val exchangedPIC = collectedPIC.flatMap { it.disclosedToVendors }.distinct()
        val exchangedVendors = exchangedPIC.map { it.vendor }.distinct()
        val noMappingVendors = dataRecipientCollectionGroupRepo
            .findAllByCollectionGroupIdAndNoDataDisclosure(collectionGroup.id, true)
            .map { it.vendor }.distinct()
        val vendors = (exchangedVendors + noMappingVendors).distinct()

        val allCategories = personalInformationCategoryRepository.findAll()

        val exchangedByVendor = exchangedPIC.groupBy(keySelector = { it.vendor!!.id }, valueTransform = { it.collectedPIC!!.pic!!.id })

        return vendors.map { vendor ->
            val mapping = dataRecipientCollectionGroupRepo.findByVendorIdAndCollectionGroupId(
                vendorId = vendor!!.id,
                consumerGroupId = collectionGroup.id
            )

            PersonalInformationExchangedDto(
                dataSubjectTypeId = collectionGroup.id.toString(),
                vendorId = vendor.id.toString(),
                noDataMapping = mapping?.noDataDisclosure,
                personalInformationCategories = allCategories.filter {
                    val isCollected = exchangedByVendor[vendor.id]?.contains(it.id) ?: false
                    isCollected || !it.deprecated
                }.map { pic ->
                    pic.toDto(
                        collected = collectedPICIds.contains(pic.id),
                        exchanged = exchangedByVendor[vendor.id]?.contains(pic.id) ?: false
                    )
                }
            )
        }
    }

    fun getDataRecipientPIC(
        organizationId: OrganizationPublicId,
        vendorId: UUID
    ): List<PersonalInformationExchangedDto> {
        val organization = lookup.orgOrThrow(organizationId)
        val dataSubjectTypes = collectionGroupRepository.findAllByOrganization(organization)
        val exchangedPIC = getExchangedPIC(organizationId, vendorId)

        log.info("EXCHANGED PIC: $exchangedPIC")

        val exchangedByCollectionGroup = exchangedPIC.groupBy(
            keySelector = { it.collectedPIC!!.collectionGroup!!.id },
            valueTransform = { it.collectedPIC!!.pic!!.id }
        )

        val collectedPIC = collectedPICRepo.findAllByCollectionGroup_Organization(organization)
        val allCategories = personalInformationCategoryRepository.findAll()

        val collectedByCollectionGroup = collectedPIC.groupBy(
            keySelector = { it.collectionGroup!!.id },
            valueTransform = { it.pic!!.id }
        )
        return dataSubjectTypes.map { dst ->
            val mapping = dataRecipientCollectionGroupRepo.findByVendorIdAndCollectionGroupId(
                vendorId = vendorId,
                consumerGroupId = dst.id
            )

            PersonalInformationExchangedDto(
                dataSubjectTypeId = dst.id.toString(),
                vendorId = vendorId.toString(),
                noDataMapping = mapping?.noDataDisclosure,
                personalInformationCategories = allCategories.filter {
                    val isCollected = collectedByCollectionGroup[dst.id]?.contains(it.id) ?: false
                    isCollected || !it.deprecated
                }.map { pic ->
                    pic.toDto(
                        collected = collectedByCollectionGroup[dst.id]?.contains(pic.id) ?: false,
                        exchanged = exchangedByCollectionGroup[dst.id]?.contains(pic.id) ?: false
                    )
                }
            )
        }
    }

    fun getOrganizationPersonalInformationExchangedDto(organizationId: OrganizationPublicId): List<PersonalInformationExchangedDto> {
        val organization = lookup.orgOrThrow(organizationId)
        val exchanged = exchangedPICRepo.findAllByCollectedPIC_CollectionGroup_Organization(organization)
        val exchangedByConsumerGroupVendor =
            exchanged.groupBy { Pair(it.collectedPIC!!.collectionGroup!!.id, it.vendor!!.id) }
        return exchangedByConsumerGroupVendor.map { (key, exchanges) ->
            PersonalInformationExchangedDto(
                dataSubjectTypeId = key.first.toString(),
                vendorId = key.second.toString(),
                noDataMapping = null,
                personalInformationCategories = exchanges.map {
                    val category = it.collectedPIC!!.pic!!
                    PersonalInformationCategoryDto(
                        id = category.id.toString(),
                        key = category.picKey,
                        groupId = category.group!!.id.toString(),
                        groupName = category.group.name!!,
                        groupOrder = category.group.displayOrder,
                        groupEmployeeOrder = category.group.employeeDisplayOrder,
                        name = category.name!!,
                        description = category.description,
                        order = category.groupDisplayOrder,
                        isSensitive = category.isSensitive,
                        collected = true,
                        exchanged = true,
                        surveyClassification = category.getClassification()
                    )
                }
            )
        }
    }

    fun getOrganizationConsumerGroupPersonalInformationCategoriesDto(
        organizationId: OrganizationPublicId,
        consumerGroupId: UUID
    ): List<PersonalInformationExchangedDto> {
        val collectionGroup = lookup.collectionGroupOrThrow(organizationId, consumerGroupId)
        val collected = getCollectedPIC(
            organizationId,
            consumerGroupId
        )
        val exchangedByVendorCategoryId = exchangedPICRepo.findAllByCollectedPIC_CollectionGroup(collectionGroup)
            .associateBy { Pair(it.vendor!!.id, it.collectedPIC!!.pic!!.id) }
        val vendorIds = organizationVendorService.allVendorsForOrganization(organizationId).map { it.vendor!!.id }
        return vendorIds.map { vendorId ->
            PersonalInformationExchangedDto(
                dataSubjectTypeId = consumerGroupId.toString(),
                vendorId = vendorId.toString(),
                noDataMapping = null,
                personalInformationCategories = collected.map { coll ->
                    val category = coll.pic!!
                    val vendorCatKey = Pair(vendorId, category.id)
                    PersonalInformationCategoryDto(
                        id = category.id.toString(),
                        key = category.picKey,
                        groupId = category.group!!.id.toString(),
                        groupName = category.group.name!!,
                        groupOrder = category.group.displayOrder,
                        groupEmployeeOrder = category.group.employeeDisplayOrder,
                        name = category.name!!,
                        description = category.description,
                        order = category.groupDisplayOrder,
                        isSensitive = category.isSensitive,
                        collected = true,
                        exchanged = exchangedByVendorCategoryId[vendorCatKey] != null,
                        surveyClassification = category.getClassification()
                    )
                }
            )
        }
    }

    fun setOrganizationVendorPersonalInformationCategoriesExchangedDto(
        organizationId: OrganizationPublicId,
        vendorId: UUID,
        updateExchanged: List<UpdatePersonalInformationExchangedDto>
    ) {
        val organization = lookup.orgOrThrow(organizationId)
        val vendor = lookup.vendorForOrgOrThrow(organizationId, vendorId)
        val orgExchangedCategories = getExchangedPIC(organizationId, vendorId)

        val exchangedByDataSubjectType = orgExchangedCategories.groupBy(
            keySelector = { it.collectedPIC!!.collectionGroup!!.id },
            valueTransform = { it.collectedPIC!!.pic!!.id }
        )
        val orgCollectedCategories = collectedPICRepo.findAllByCollectionGroup_Organization(organization)
        val categoryIdsByDataSubjectType = orgCollectedCategories.groupBy(
            keySelector = { it.collectionGroup!!.id },
            valueTransform = { it.pic!!.id }
        )

        val orgExchangedPic = exchangedPICRepo.findAllByCollectedPIC_CollectionGroup_Organization(organization)

        updateExchanged.map {
            val collectionGroup = lookup.collectionGroupOrThrow(organizationId, it.dataSubjectTypeId)
            val collectedIds: List<UUID> = categoryIdsByDataSubjectType[it.dataSubjectTypeId] ?: emptyList()
            val exchangedIds = exchangedByDataSubjectType[it.dataSubjectTypeId] ?: emptyList()

            val existing = dataRecipientCollectionGroupRepo.findByVendorIdAndCollectionGroupId(vendor.id, collectionGroup.id)
                ?: DataRecipientCollectionGroup(vendorId = vendor.id, collectionGroupId = collectionGroup.id)

            var noDataMapping: Boolean? = it.personalInformationCategories.isEmpty()
            if (noDataMapping != null && noDataMapping) {
                noDataMapping = if (it.noDataMapping) {
                    true
                } else {
                    null
                }
            }
            existing.noDataDisclosure = noDataMapping

            dataRecipientCollectionGroupRepo.save(existing)

            val newCollectIds = it.personalInformationCategories.subtract(collectedIds)
            val collected = orgCollectedCategories + collectedPICRepo.saveAll(
                newCollectIds.map { new ->
                    CollectionGroupCollectedPIC(
                        collectionGroup = collectionGroup,
                        pic = personalInformationCategoryRepository.findByIdOrNull(new)
                            ?: throw PolarisException("Unable to find PersonalInformationCategory id $new")
                    )
                }
            )

            // check all the removeExchangedIds. If this is the *last* vendor that exchanges information on this data subject type
            // then we need to remove the 'collection' of that data subject type as well. See
            // https://truevault.atlassian.net/browse/POLARIS-631 for more details.

            val noLongerCollectedPic: MutableList<UUID> = mutableListOf()
            val newExchangedIds = it.personalInformationCategories.subtract(exchangedIds)
            val removeExchangedIds = exchangedIds.subtract(it.personalInformationCategories)
            removeExchangedIds.forEach { notExchangedPic ->
                val picExchangedByOther = orgExchangedPic.find { orgsrvpic ->
                    orgsrvpic.collectedPIC!!.collectionGroup!!.id == it.dataSubjectTypeId &&
                        orgsrvpic.collectedPIC.pic!!.id == notExchangedPic &&
                        orgsrvpic.vendor != vendor
                }

                if (picExchangedByOther == null) {
                    // we were the *last* vendor that was exchanging this PIC. It should be removed
                    noLongerCollectedPic.add(notExchangedPic)
                }
            }

            val orgsrvpicToDelete = orgExchangedCategories.filter { shr ->
                shr.collectedPIC!!.collectionGroup == collectionGroup && removeExchangedIds.contains(
                    shr.collectedPIC.pic!!.id
                )
            }
            exchangedPICRepo.deleteAll(orgsrvpicToDelete)
            exchangedPICRepo.saveAll(
                newExchangedIds.map { cid ->
                    DataRecipientDisclosedPIC(
                        collectedPIC = collected.find { coll -> coll.collectionGroup == collectionGroup && coll.pic!!.id == cid }
                            ?: throw PolarisException("Fatal error trying to find collected category $cid"),
                        vendor = vendor
                    )
                }
            )

            // remove all the noLongerCollectedPic
            if (noLongerCollectedPic.size > 0) {
                val toDelete = orgCollectedCategories.filter { occ -> noLongerCollectedPic.contains(occ.pic!!.id) && occ.collectionGroup == collectionGroup }
                collectedPICRepo.deleteAll(toDelete)
            }
        }

        // rebuild the privacy center
        privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(organization, "data map updated")
    }

    private fun lookupPurposeOrThrow(purposeId: UUID) =
        businessPurposeRepository.findByIdOrNull(purposeId) ?: throw ResourceNotFoundException("purpose:$purposeId")
}
