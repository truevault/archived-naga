package polaris.services.primary.hooks

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.services.HookArgument
import polaris.services.primary.DataSubjectRequestAutoCloseService
import polaris.services.primary.DataSubjectRequestService

@Service
class AutoCloseDuplicateDataSubjectRequests(
    private val dataSubjectRequestService: DataSubjectRequestService,
    private val autoCloseService: DataSubjectRequestAutoCloseService
) :
    BaseHook<HookArgument.DataSubjectRequestCreated> {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(AutoCloseDuplicateDataSubjectRequests::class.java)
    }

    override fun run(args: HookArgument.DataSubjectRequestCreated) {
        val (request) = args
        val duplicates = dataSubjectRequestService.findDuplicateRequests(request)

        if (duplicates.isNotEmpty()) {
            autoCloseService.closeAsDuplicate(request, duplicates.first())
        }
    }
}
