package polaris.services.primary.hooks

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.dto.organization.RequestHandlingInstructionDto
import polaris.models.entity.organization.Organization
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod
import polaris.models.entity.vendor.KnownVendorKey
import polaris.models.entity.vendor.Vendor
import polaris.services.HookArgument
import polaris.services.primary.RequestHandlingInstructionsService

@Service
class SetupInitialRequestHandlingInstructionsForOrgDataRecipient(
    private val requestHandlingInstructionsService: RequestHandlingInstructionsService
) : BaseHook<HookArgument.OrganizationDataRecipientCreated> {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(SetupInitialRequestHandlingInstructionsForOrgDataRecipient::class.java)
    }

    override fun run(args: HookArgument.OrganizationDataRecipientCreated) {
        val (org, dataRecipient) = args

        if ((dataRecipient.vendor != null) && (dataRecipient.vendor.isKnownVendor(KnownVendorKey.SHOPIFY))) {
            setupShopifyRequestHandlingInstructions(org, dataRecipient.vendor)
        }
    }

    private fun setupShopifyRequestHandlingInstructions(org: Organization, vendor: Vendor) {
        requestHandlingInstructionsService.createOrUpdate(
            org.publicId,
            RequestHandlingInstructionDto(
                organizationId = org.publicId,
                requestType = DataRequestInstructionType.DELETE,
                processingMethod = ProcessingMethod.DELETE,
                vendorId = vendor.id.toString(),
                hasRetentionExceptions = false
            )
        )
    }
}
