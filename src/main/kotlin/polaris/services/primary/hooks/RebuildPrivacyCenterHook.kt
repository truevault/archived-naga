package polaris.services.primary.hooks

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.CollectionGroupCollectedPIC
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Phase
import polaris.services.Hook
import polaris.services.support.PrivacyCenterBuilderService

@Component
class RebuildPrivacyCenterHook(
    private val privacyCenterBuilderService: PrivacyCenterBuilderService
) {
    companion object {
        private val log = LoggerFactory.getLogger(RebuildPrivacyCenterHook::class.java)
    }

    fun run(
        hook: Hook,
        collectionGroup: CollectionGroup
    ) {
        if (collectionGroup.organization == null) return
        run(
            hook,
            "cg=${collectionGroup.name}",
            collectionGroup.organization,
            "collection group updated or deleted"
        )
    }

    fun run(
        hook: Hook,
        collectedPic: CollectionGroupCollectedPIC
    ) {
        if (collectedPic.collectionGroup?.organization == null) return
        run(
            hook,
            "cg=${collectedPic.collectionGroup.name}, pic=${collectedPic.pic?.name}",
            collectedPic.collectionGroup.organization,
            "collection group PIC added or removed"
        )
    }

    private fun run(
        hook: Hook,
        details: String,
        organization: Organization,
        source: String
    ) {
        if (Phase.STAY_COMPLIANT == organization.getPhase()) {
            log.info("$hook: $details")
            privacyCenterBuilderService.requestBuildPrivacyCentersForOrgAsync(organization.id, source)
        }
    }
}
