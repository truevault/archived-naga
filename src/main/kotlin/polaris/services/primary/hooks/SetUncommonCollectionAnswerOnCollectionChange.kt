package polaris.services.primary.hooks

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.entity.compliance.PICSurveyClassification
import polaris.services.HookArgument
import polaris.services.primary.SurveyService

@Service
class SetUncommonCollectionAnswerOnCollectionChange(
    private val surveyService: SurveyService
) : BaseHook<HookArgument.CollectedPicCreated> {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(SetUncommonCollectionAnswerOnCollectionChange::class.java)
    }

    override fun run(args: HookArgument.CollectedPicCreated) {
        val (org, pic, cg) = args
        val surveyName = "consumer-data-collection-${cg.id}"

        val isUncommonCollection = pic.getClassification() == PICSurveyClassification.UNCOMMON
        val uncommonCollectionIsNo = surveyService.getAnswer(org, surveyName, "uncommon-collection") != "true"

        if (isUncommonCollection && uncommonCollectionIsNo) {
            surveyService.writeSurveyAnswer(
                org,
                "true",
                surveyName = surveyName,
                questionText = "",
                slug = "uncommon-collection"
            )
        }
    }
}
