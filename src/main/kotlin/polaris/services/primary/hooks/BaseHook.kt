package polaris.services.primary.hooks

interface BaseHook<T> {
    fun run(args: T)
}
