package polaris.services.primary

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.UnrecognizedSurveyException
import polaris.controllers.UnrecognizedSurveySlugException
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.dto.ApiMode
import polaris.models.dto.orgDraftSchema.v1.Surveys
import polaris.models.dto.organization.UpdateOrganizationSurveyQuestionDto
import polaris.models.entity.organization.EventData
import polaris.models.entity.organization.FINANCIAL_INCENTIVES_SURVEY_NAME
import polaris.models.entity.organization.OrgDraftSchemaVersion
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.repositories.CollectionGroupCollectedPICRepo
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationDataSourceRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.PrivacyCenterRepository
import polaris.repositories.VendorClassificationRepository
import polaris.services.support.AutocreateProcessingActivityService
import polaris.services.support.DataMapService
import polaris.services.support.OrgDraftService
import polaris.services.support.PrivacyCenterBuilderService
import polaris.services.support.RemoveAutocreateCollectionGroupService
import polaris.util.survey.interceptors.B2BSurveyInterceptor
import polaris.util.survey.interceptors.CollectionInferencesInterceptor
import polaris.util.survey.interceptors.ConsumerSourcesSurveyInterceptor
import polaris.util.survey.interceptors.CookiePolicySurveyInterceptor
import polaris.util.survey.interceptors.EmployeeGroupsSurveyInterceptor
import polaris.util.survey.interceptors.EmployeeSourcesSurveyInterceptor
import polaris.util.survey.interceptors.FinancialIncentivesSurveyInterceptor
import polaris.util.survey.interceptors.LawfulBasesSurveyInterceptor
import polaris.util.survey.interceptors.MarketToChildrenSurveyInterceptor
import polaris.util.survey.interceptors.RetentionPolicySurveyInterceptor
import polaris.util.survey.interceptors.SellingAndSharingSurveyInterceptor
import polaris.util.survey.interceptors.ServiceProviderSurveyInterceptor
import polaris.util.survey.interceptors.SurveyInterceptor
import polaris.util.survey.interceptors.UncommonCollectionSurveyInterceptor
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties

@Service
@Transactional

class OrganizationSurveyService(
    private val organizationRepository: OrganizationRepository,
    private val privacyCenterRepo: PrivacyCenterRepository,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val dataMapService: DataMapService,
    private val collectedPICRepo: CollectionGroupCollectedPICRepo,
    private val surveyQuestionRepo: OrganizationSurveyQuestionRepository,
    private val lookup: LookupService,
    private val surveyService: SurveyService,
    private val requestHandlingInstructionsService: RequestHandlingInstructionsService,
    private val organizationVendorSurveyService: OrganizationVendorSurveyService,
    private val orgDataRecipientRepository: OrganizationDataRecipientRepository,
    private val vendorClassificationRepository: VendorClassificationRepository,
    private val serviceProviderRecommendationService: ServiceProviderRecommendationService,
    private val taskUpdateService: TaskUpdateService,
    private val orgDataSourceService: OrganizationDataSourceService,
    private val orgDataSourceRepo: OrganizationDataSourceRepository,
    private val orgDraftService: OrgDraftService,
    private val removeAutocreateCollectionGroupService: RemoveAutocreateCollectionGroupService,
    private val processingActivityService: ProcessingActivityService,
    private val autoCreateService: AutocreateProcessingActivityService
) {
    fun getSurveyQuestions(organizationId: OrganizationPublicId, mode: ApiMode, vararg surveyNames: String): List<OrganizationSurveyQuestion> {
        return if (mode == ApiMode.Live) stayCompliantGetSurveyQuestions(organizationId, *surveyNames) else getCompliantGetSurveyQuestions(organizationId, *surveyNames)
    }

    private fun stayCompliantGetSurveyQuestions(organizationId: OrganizationPublicId, vararg surveyNames: String): List<OrganizationSurveyQuestion> {
        val org = lookup.orgOrThrow(organizationId)
        val all = surveyNames.flatMap { surveyName ->
            val orgSurveyQuestions = organizationSurveyQuestionRepository.findAllByOrganizationAndSurveyName(org, surveyName)
            val interceptedQuestions = getInterceptedSurveyQuestions(org, surveyName)
            val joined = interceptedQuestions + orgSurveyQuestions
            joined.distinctBy { it.slug }
        }

        return all
    }
    private fun getCompliantGetSurveyQuestions(organizationId: OrganizationPublicId, vararg surveyNames: String): List<OrganizationSurveyQuestion> {
        val org = lookup.orgOrThrow(organizationId)

        val orgDraft = orgDraftService.getDraft(org, OrgDraftSchemaVersion.V1)

        return surveyNames.flatMap { surveyName ->
            val surveyProperty = Surveys::class.memberProperties.find { it.name == surveyName } ?: throw UnrecognizedSurveyException(surveyName)
            val surveyValue = surveyProperty.get(orgDraft.data.surveys)

            surveyValue!!::class.memberProperties.map { prop ->
                OrganizationSurveyQuestion(
                    id = 0,
                    organization = org,
                    surveyName = surveyName,
                    slug = prop.name,
                    question = "",
                    answer = prop.getter.call(surveyValue)?.toString()
                )
            }
        }
    }

    fun updateSurveyQuestions(
        organizationId: OrganizationPublicId,
        surveyName: String,
        updateQuestionsDto: List<UpdateOrganizationSurveyQuestionDto>,
        events: MutableList<EventData>,
        apiMode: ApiMode = ApiMode.Live
    ) {
        if (apiMode == ApiMode.Live)
            stayCompliantUpdateSurveyQuestions(organizationId, surveyName, updateQuestionsDto, events)
        else
            getCompliantUpdateSurveyQuestions(organizationId, surveyName, updateQuestionsDto, events)
    }

    private fun stayCompliantUpdateSurveyQuestions(
        organizationId: OrganizationPublicId,
        surveyName: String,
        updateQuestionsDto: List<UpdateOrganizationSurveyQuestionDto>,
        events: MutableList<EventData>
    ) {
        val org = lookup.orgOrThrow(organizationId)

        val remainingQuestions = saveInterceptedSurveyQuestions(org, surveyName, updateQuestionsDto)
        val existingBySlug = getSurveyQuestions(organizationId, ApiMode.Live, surveyName).associateBy { it.slug }
        val survey = remainingQuestions.map { question ->
            val existing = existingBySlug[question.slug]
            val questionText =
                if (question.question == null && existing?.question != null) existing.question else question.question
            if (existing != null) {
                events.add(EventData.SurveyResponse(surveyName = surveyName, surveySlug = question.slug, response = question.answer, oldResponse = existing.answer))
                questionText?.let { existing.question = questionText }
                existing.answer = question.answer
                existing
            } else {
                events.add(EventData.SurveyResponse(surveyName = surveyName, surveySlug = question.slug, response = question.answer, oldResponse = null))
                OrganizationSurveyQuestion(
                    organization = org,
                    surveyName = surveyName,
                    slug = question.slug,
                    question = questionText ?: "",
                    answer = question.answer
                )
            }
        }

        organizationSurveyQuestionRepository.saveAll(survey)

        if (shouldUpdateTasks(surveyName, updateQuestionsDto)) {
            taskUpdateService.updateTasks(org, create = true, close = true)
        }
    }

    private fun getCompliantUpdateSurveyQuestions(
        organizationId: OrganizationPublicId,
        surveyName: String,
        updateQuestionsDto: List<UpdateOrganizationSurveyQuestionDto>,
        events: MutableList<EventData>
    ) {
        val org = lookup.orgOrThrow(organizationId)
        val orgDraft = orgDraftService.getDraft(org, OrgDraftSchemaVersion.V1)

        val surveyProperty = Surveys::class.memberProperties.find { it.name == surveyName } ?: throw UnrecognizedSurveyException(surveyName)
        val surveyValue = surveyProperty.get(orgDraft.data.surveys)

        updateQuestionsDto.forEach { up ->
            val slugProp = surveyValue!!::class.memberProperties.find { p -> p.name == up.slug } ?: throw UnrecognizedSurveySlugException(surveyName, up.slug)
            if (slugProp is KMutableProperty<*>) {
                val serializedAnswer = orgDraftService.serializeSurveyAnswer(up.answer, slugProp)
                slugProp.setter.call(surveyValue, serializedAnswer)
                // TODO: update event tracking to account for existing answer
                // events.add(EventData.SurveyResponse(surveyName = surveyName, surveySlug = up.slug, response = up.answer, oldResponse = existing.answer))
                events.add(EventData.SurveyResponse(surveyName = surveyName, surveySlug = up.slug, response = up.answer, oldResponse = null))
            }
        }

        orgDraftService.persistDraft(orgDraft)
    }

    fun getInterceptedSurveyQuestions(org: Organization, surveyName: String): List<OrganizationSurveyQuestion> {
        val interceptors = buildInterceptors()

        val relevantInterceptors = interceptors.filter { it.intercepts(surveyName) }

        return relevantInterceptors.flatMap { int ->
            int.slugs(surveyName).map { slug -> int.get(org, surveyName, slug) }
        }
    }

    fun saveInterceptedSurveyQuestions(
        org: Organization,
        surveyName: String,
        updateQuestionsDto: List<UpdateOrganizationSurveyQuestionDto>
    ): List<UpdateOrganizationSurveyQuestionDto> {
        val interceptors = buildInterceptors()
        val relevantInterceptors = interceptors.filter { it.intercepts(surveyName) }

        @Suppress("UNCHECKED_CAST")
        val intercepted = updateQuestionsDto
            .associateWith { dto -> relevantInterceptors.find { i -> i.slugs(surveyName).contains(dto.slug) } }
            .filterValues { it != null } as Map<UpdateOrganizationSurveyQuestionDto, SurveyInterceptor>

        intercepted.forEach { (dto, i) -> i.write(org, dto.answer, dto.question, surveyName, dto.slug) }

        return updateQuestionsDto.filter { !intercepted.containsKey(it) }
    }

    private fun shouldUpdateTasks(surveyName: String, updateQuestionsDto: List<UpdateOrganizationSurveyQuestionDto>): Boolean {
        return updateQuestionsDto.any { shouldUpdateTasks(surveyName, it) }
    }

    private fun shouldUpdateTasks(surveyName: String, updateQuestionDto: UpdateOrganizationSurveyQuestionDto): Boolean {
        if (surveyName == FINANCIAL_INCENTIVES_SURVEY_NAME && updateQuestionDto.slug == "offer-consumer-incentives") {
            return true
        }

        return false
    }

    private fun buildInterceptors(): List<SurveyInterceptor> {
        return listOf(
            SellingAndSharingSurveyInterceptor(surveyService, organizationSurveyQuestionRepository, requestHandlingInstructionsService, orgDataRecipientRepository, organizationVendorSurveyService, vendorClassificationRepository, serviceProviderRecommendationService),
            ServiceProviderSurveyInterceptor(organizationRepository, privacyCenterBuilderService),
            RetentionPolicySurveyInterceptor(privacyCenterRepo, privacyCenterBuilderService),
            CookiePolicySurveyInterceptor(organizationRepository, privacyCenterBuilderService),
            B2BSurveyInterceptor(organizationSurveyQuestionRepository),
            CollectionInferencesInterceptor(dataMapService, collectedPICRepo, surveyQuestionRepo, lookup),
            UncommonCollectionSurveyInterceptor(surveyService, organizationSurveyQuestionRepository, dataMapService, collectedPICRepo, lookup),
            ConsumerSourcesSurveyInterceptor(surveyService, orgDataSourceService, organizationSurveyQuestionRepository, orgDataSourceRepo),
            EmployeeSourcesSurveyInterceptor(surveyService, orgDataSourceService, organizationSurveyQuestionRepository, orgDataSourceRepo),
            EmployeeGroupsSurveyInterceptor(surveyService, organizationSurveyQuestionRepository, removeAutocreateCollectionGroupService),
            FinancialIncentivesSurveyInterceptor(surveyService, organizationSurveyQuestionRepository),
            LawfulBasesSurveyInterceptor(surveyService, organizationSurveyQuestionRepository, processingActivityService, autoCreateService),
            MarketToChildrenSurveyInterceptor(surveyService, organizationSurveyQuestionRepository)
        )
    }
}
