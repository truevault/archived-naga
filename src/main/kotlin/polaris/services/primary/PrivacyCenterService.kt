package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.ResourceNotFoundException
import polaris.models.OrganizationPublicId
import polaris.models.PrivacyCenterPublicId
import polaris.models.UUIDString
import polaris.models.dto.privacycenter.AddOrUpdatePrivacyCenterPolicySection
import polaris.models.dto.privacycenter.BuiltinPrivacySectionSlug
import polaris.models.dto.privacycenter.CreatePrivacyCenter
import polaris.models.dto.privacycenter.UpdatePrivacyCenter
import polaris.models.dto.privacycenter.UpdatePrivacyCenterPolicy
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.PrivacyCenterNotFound
import polaris.models.entity.organization.Organization
import polaris.models.entity.privacycenter.CCPA_INTRO_SLUG
import polaris.models.entity.privacycenter.CPA_INTRO_SLUG
import polaris.models.entity.privacycenter.CTDPA_INTRO_SLUG
import polaris.models.entity.privacycenter.CustomUrlStatus
import polaris.models.entity.privacycenter.GDPR_INTRO_SLUG
import polaris.models.entity.privacycenter.OPT_OUT_INTRO_SLUG
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.privacycenter.PrivacyCenterPolicySection
import polaris.models.entity.privacycenter.RETENTION_INTRO_SLUG
import polaris.models.entity.privacycenter.VCDPA_INTRO_SLUG
import polaris.models.entity.user.User
import polaris.models.toDateTime
import polaris.repositories.OrganizationRepository
import polaris.repositories.PrivacyCenterPolicySectionRepository
import polaris.repositories.PrivacyCenterRepository
import polaris.repositories.UserRepository
import polaris.services.support.BlockStorageService
import polaris.services.support.PrivacyCenterBuilderService
import polaris.util.PolarisException
import java.util.UUID
import javax.persistence.EntityManager

@Service
@Transactional
class PrivacyCenterService(
    private val blockStorageService: BlockStorageService,
    private val organizationRepository: OrganizationRepository,
    private val userRepository: UserRepository,
    private val privacyCenterRepo: PrivacyCenterRepository,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService,
    private val privacyCenterPolicySectionRepository: PrivacyCenterPolicySectionRepository,
    private val taskUpdateService: TaskUpdateService,
    private val em: EntityManager
) {
    companion object {
        private val log = LoggerFactory.getLogger(PrivacyCenterService::class.java)
    }

    fun findByPublicId(publicId: PrivacyCenterPublicId): PrivacyCenter? {
        return lookupPrivacyCenterOrThrow(publicId)
    }

    fun findByOrganizationAndPublicId(
        organizationId: OrganizationPublicId,
        publicId: PrivacyCenterPublicId
    ): PrivacyCenter {
        lookupOrganizationOrThrow(organizationId)
        return lookupPrivacyCenterOrThrow(publicId, organizationId)
    }

    fun findAllByOrganization(organizationId: OrganizationPublicId): List<PrivacyCenter> {
        val org = lookupOrganizationOrThrow(organizationId)
        return privacyCenterRepo.findAllByOrganization(org)
    }

    fun findByEmailAddress(email: String): PrivacyCenter? {
        return privacyCenterRepo.findByRequestEmail(email)
    }

    fun create(organizationId: OrganizationPublicId, dto: CreatePrivacyCenter): PrivacyCenter {
        val org = lookupOrganizationOrThrow(organizationId)
        return privacyCenterRepo.save(
            PrivacyCenter(
                organization = org,
                name = dto.name,
                defaultSubdomain = dto.defaultSubdomain,
                faviconUrl = dto.faviconUrl,
                customUrl = dto.customUrl,
                privacyPolicyUrl = dto.privacyPolicyUrl,
                requestTollFreePhoneNumber = dto.requestTollFreePhoneNumber,
                requestFormUrl = dto.requestFormUrl,
                requestEmail = dto.requestEmail,
                logoUrl = dto.logoUrl,
                customUrlStatus = if (!dto.customUrl.isNullOrEmpty()) CustomUrlStatus.VERIFYING else null
            )
        )
    }

    fun update(publicId: PrivacyCenterPublicId, org: Organization, dto: UpdatePrivacyCenter, user: User): PrivacyCenter {
        val privacyCenter = lookupPrivacyCenterOrThrow(publicId)
        val logoUrl = if (dto.logoUrl.isNullOrEmpty()) null else dto.logoUrl
        val logoLinkUrl = if (dto.logoLinkUrl.isNullOrBlank()) null else dto.logoLinkUrl
        val hasMobileApplication = if (dto.hasMobileApplication == null) null else dto.hasMobileApplication

        var rebuild = (
            privacyCenter.name != dto.name ||
                privacyCenter.faviconUrl != dto.faviconUrl ||
                privacyCenter.logoUrl != logoUrl ||
                privacyCenter.logoLinkUrl != logoLinkUrl ||
                privacyCenter.hasMobileApplication != hasMobileApplication ||
                privacyCenter.doNotSellMarkdown != dto.doNotSellMarkdown ||
                privacyCenter.employeePrivacyNoticeMarkdown != dto.employeePrivacyNoticeMarkdown ||
                privacyCenter.jobApplicantPrivacyNoticeMarkdown != dto.jobApplicantPrivacyNoticeMarkdown ||
                privacyCenter.dpoOfficerName != dto.dpoOfficerName ||
                privacyCenter.dpoOfficerEmail != dto.dpoOfficerEmail ||
                privacyCenter.dpoOfficerPhone != dto.dpoOfficerPhone ||
                privacyCenter.dpoOfficerAddress != dto.dpoOfficerAddress ||
                privacyCenter.getCustomText(CCPA_INTRO_SLUG) != dto.ccpaPrivacyNoticeIntroText ||
                privacyCenter.getCustomText(GDPR_INTRO_SLUG) != dto.gdprPrivacyNoticeIntroText ||
                privacyCenter.getCustomText(VCDPA_INTRO_SLUG) != dto.vcdpaPrivacyNoticeIntroText ||
                privacyCenter.getCustomText(CPA_INTRO_SLUG) != dto.cpaPrivacyNoticeIntroText ||
                privacyCenter.getCustomText(CTDPA_INTRO_SLUG) != dto.ctdpaPrivacyNoticeIntroText ||
                privacyCenter.getCustomText(OPT_OUT_INTRO_SLUG) != dto.optOutNoticeIntroText ||
                privacyCenter.getCustomText(RETENTION_INTRO_SLUG) != dto.retentionIntroText
            )

        privacyCenter.name = dto.name
        if (!privacyCenter.faviconUrl.isBlank() && dto.faviconUrl != privacyCenter.faviconUrl) {
            blockStorageService.deleteImageByUrl(privacyCenter.faviconUrl)
        }
        privacyCenter.faviconUrl = dto.faviconUrl
        if (!privacyCenter.logoUrl.isNullOrBlank() && logoUrl != privacyCenter.logoUrl) {
            blockStorageService.deleteImageByUrl(privacyCenter.logoUrl!!)
        }
        privacyCenter.logoUrl = logoUrl
        privacyCenter.logoLinkUrl = logoLinkUrl
        if (hasMobileApplication != null) {
            privacyCenter.hasMobileApplication = hasMobileApplication
        }
        privacyCenter.defaultSubdomain = dto.defaultSubdomain
        privacyCenter.doNotSellMarkdown = dto.doNotSellMarkdown
        privacyCenter.employeePrivacyNoticeMarkdown = dto.employeePrivacyNoticeMarkdown
        privacyCenter.jobApplicantPrivacyNoticeMarkdown = dto.jobApplicantPrivacyNoticeMarkdown
        privacyCenter.customUrlCnameReady = dto.customUrlCnameReady ?: false

        privacyCenter.dpoOfficerName = dto.dpoOfficerName
        privacyCenter.dpoOfficerEmail = dto.dpoOfficerEmail
        privacyCenter.dpoOfficerPhone = dto.dpoOfficerPhone
        privacyCenter.dpoOfficerAddress = dto.dpoOfficerAddress
        privacyCenter.dpoOfficerDoesNotApply = dto.dpoOfficerDoesNotApply

        if (privacyCenter.getCustomText(CCPA_INTRO_SLUG) != dto.ccpaPrivacyNoticeIntroText) {
            privacyCenter.setCustomText(CCPA_INTRO_SLUG, dto.ccpaPrivacyNoticeIntroText)
        }
        if (privacyCenter.getCustomText(GDPR_INTRO_SLUG) != dto.gdprPrivacyNoticeIntroText) {
            privacyCenter.setCustomText(GDPR_INTRO_SLUG, dto.gdprPrivacyNoticeIntroText)
        }
        if (privacyCenter.getCustomText(VCDPA_INTRO_SLUG) != dto.vcdpaPrivacyNoticeIntroText) {
            privacyCenter.setCustomText(VCDPA_INTRO_SLUG, dto.vcdpaPrivacyNoticeIntroText)
        }
        if (privacyCenter.getCustomText(CPA_INTRO_SLUG) != dto.cpaPrivacyNoticeIntroText) {
            privacyCenter.setCustomText(CPA_INTRO_SLUG, dto.cpaPrivacyNoticeIntroText)
        }
        if (privacyCenter.getCustomText(CTDPA_INTRO_SLUG) != dto.ctdpaPrivacyNoticeIntroText) {
            privacyCenter.setCustomText(CTDPA_INTRO_SLUG, dto.ctdpaPrivacyNoticeIntroText)
        }
        if (privacyCenter.getCustomText(OPT_OUT_INTRO_SLUG) != dto.optOutNoticeIntroText) {
            privacyCenter.setCustomText(OPT_OUT_INTRO_SLUG, dto.optOutNoticeIntroText)
        }
        if (privacyCenter.getCustomText(RETENTION_INTRO_SLUG) != dto.retentionIntroText) {
            privacyCenter.setCustomText(RETENTION_INTRO_SLUG, dto.retentionIntroText)
        }
        if (privacyCenter.organization != null && privacyCenter.organization.cookiePolicy != dto.cookiePolicyIntroText) {
            privacyCenter.organization.cookiePolicy = dto.cookiePolicyIntroText
            organizationRepository.save(privacyCenter.organization)
        }

        // If custom URL is removed
        if (dto.customUrl.isNullOrEmpty() && !privacyCenter.customUrl.isNullOrEmpty()) {
            privacyCenter.customUrl = null
            privacyCenter.customUrlStatus = null
        }
        // If the custom URL has changed
        else if (dto.customUrl != privacyCenter.customUrl) {
            privacyCenter.customUrl = dto.customUrl
            privacyCenter.customUrlStatus = CustomUrlStatus.VERIFYING
        }

        if (dto.disableCaResidencyConfirmation != null && privacyCenter.caResidencyConfirmation != !dto.disableCaResidencyConfirmation) {
            rebuild = true
            privacyCenter.caResidencyConfirmation = !dto.disableCaResidencyConfirmation
        }

        val updated = privacyCenterRepo.save(privacyCenter)

        if (rebuild) {
            privacyCenterBuilderService.insertPrivacyCenterBuildRequest(updated, "privacy center updated")
        }

        if (privacyCenter.organization != null) {
            taskUpdateService.updateTasks(privacyCenter.organization.publicId, create = false, close = true)
        }

        return updated
    }

    fun updatePrivacyCenterRequestEmail(privacyCenter: PrivacyCenter, email: String) {
        if (email.isNotBlank() && userRepository.findByEmail(email.toLowerCase()) == null && privacyCenterRepo.findByRequestEmail(email.toLowerCase()) == null) {
            privacyCenter.requestEmail = email
            privacyCenterRepo.save(privacyCenter)
            privacyCenterBuilderService.insertPrivacyCenterBuildRequest(privacyCenter, "email updated")
        }
    }

    fun updatePolicy(
        organizationId: OrganizationPublicId,
        privacyCenterId: PrivacyCenterPublicId,
        updatePolicy: UpdatePrivacyCenterPolicy
    ): PrivacyCenter {
        var rebuild = false
        var privacyCenter = findByOrganizationAndPublicId(organizationId, privacyCenterId)

        val sectionsOrderMap = updatePolicy.policySectionsOrder
        privacyCenter.policySections.forEach {
            if (!sectionsOrderMap.containsKey(it.id.toString())) {
                throw PolarisException("Policy section ${it.id} is missing from payload")
            }
        }
        privacyCenter.policySections.forEach { section ->
            val indexOrder = sectionsOrderMap[section.id.toString()]
            if (indexOrder != null && section.displayOrder != indexOrder) {
                section.displayOrder = indexOrder
                privacyCenterPolicySectionRepository.save(section)
                rebuild = true
            }
        }

        if (privacyCenter.policyLastUpdated != updatePolicy.policyLastUpdated?.toDateTime()) {
            privacyCenter.policyLastUpdated = updatePolicy.policyLastUpdated?.toDateTime()
            rebuild = true
            privacyCenter = privacyCenterRepo.save(privacyCenter)
        }
        if (rebuild) {
            em.flush()
            em.refresh(privacyCenter)
            privacyCenterBuilderService.insertPrivacyCenterBuildRequest(privacyCenter, "privacy center policy updated")
        }
        return privacyCenter
    }

    fun addPolicySection(
        organizationId: OrganizationPublicId,
        privacyCenterId: PrivacyCenterPublicId,
        sectionDto: AddOrUpdatePrivacyCenterPolicySection
    ): PrivacyCenterPolicySection {
        val privacyCenter = findByOrganizationAndPublicId(organizationId, privacyCenterId)
        val displayOrder = displayOrderForSection(privacyCenter.policySections, sectionDto)

        // mutate other sections if necessary
        if (sectionDto.after != null) {
            // get all sections in the given range
            val toIncrement = sectionsImmediatelyAfter(privacyCenter.policySections, sectionDto.after)

            // increment them all by one
            toIncrement.forEach { it.displayOrder += 1 }

            // and persist
            privacyCenterPolicySectionRepository.saveAll(toIncrement)
        }

        // need to set the display order
        val section = privacyCenterPolicySectionRepository.save(
            PrivacyCenterPolicySection(
                privacyCenter = privacyCenter,
                name = sectionDto.name,
                anchor = sectionDto.anchor,
                addToMenu = sectionDto.addToMenu,
                body = sectionDto.body,
                noPrivacyPolicy = sectionDto.noPrivacyPolicy,
                displayOrder = displayOrder
            )
        )
        if (sectionDto.publish) {
            privacyCenterBuilderService.insertPrivacyCenterBuildRequest(privacyCenter, "privacy center policy sec added")
        }
        return section
    }

    private fun sectionsImmediatelyBefore(sections: List<PrivacyCenterPolicySection>, section: BuiltinPrivacySectionSlug): List<PrivacyCenterPolicySection> {
        val builtinStart = previousBuiltin(section)
        return sectionsBetween(sections, builtinStart, section)
    }

    private fun sectionsImmediatelyAfter(sections: List<PrivacyCenterPolicySection>, section: BuiltinPrivacySectionSlug): List<PrivacyCenterPolicySection> {
        val builtinEnd = nextBuiltin(section)
        return sectionsBetween(sections, section, builtinEnd)
    }

    private fun previousBuiltin(section: BuiltinPrivacySectionSlug): BuiltinPrivacySectionSlug? {
        return BuiltinPrivacySectionSlug.values().filter { it.displayOrder < section.displayOrder }.maxBy { it.displayOrder }
    }

    private fun nextBuiltin(section: BuiltinPrivacySectionSlug): BuiltinPrivacySectionSlug? {
        return BuiltinPrivacySectionSlug.values().filter { it.displayOrder > section.displayOrder }.minBy { it.displayOrder }
    }

    private fun sectionsBetween(sections: List<PrivacyCenterPolicySection>, builtinStart: BuiltinPrivacySectionSlug?, builtinEnd: BuiltinPrivacySectionSlug?): List<PrivacyCenterPolicySection> {
        val doStart = builtinStart?.displayOrder
        val doEnd = builtinEnd?.displayOrder

        return sections.filter {
            val meetsStart = if (doStart == null) true else it.displayOrder >= doStart
            val meetsEnd = if (doEnd == null) true else it.displayOrder < doEnd
            meetsStart && meetsEnd
        }
    }

    private fun displayOrderForSection(existingSections: List<PrivacyCenterPolicySection>, sectionDto: AddOrUpdatePrivacyCenterPolicySection): Int {
        return if (sectionDto.before != null) {
            val customSections = sectionsImmediatelyBefore(existingSections, sectionDto.before)
            val default = previousBuiltin(sectionDto.before)?.displayOrder ?: 0
            val existingDisplayOrder = customSections.maxBy { it.displayOrder }?.displayOrder ?: default

            existingDisplayOrder + 1
        } else if (sectionDto.after != null) {
            sectionDto.after.displayOrder + 1
        } else {
            1
        }
    }

    fun updatePolicySection(
        organizationId: OrganizationPublicId,
        privacyCenterId: PrivacyCenterPublicId,
        privacyCenterPolicySectionId: UUIDString,
        sectionDto: AddOrUpdatePrivacyCenterPolicySection
    ): PrivacyCenterPolicySection {
        val privacyCenter = findByOrganizationAndPublicId(organizationId, privacyCenterId)
        val privacyCenterPolicySection =
            lookupPrivacyCenterPolicySectionOrThrow(privacyCenter, privacyCenterPolicySectionId)
        val isChanged = (
            privacyCenterPolicySection.name != sectionDto.name ||
                privacyCenterPolicySection.anchor != sectionDto.anchor ||
                privacyCenterPolicySection.body != sectionDto.body ||
                privacyCenterPolicySection.addToMenu != sectionDto.addToMenu ||
                privacyCenterPolicySection.showHeader != sectionDto.showHeader ||
                privacyCenterPolicySection.noPrivacyPolicy != sectionDto.noPrivacyPolicy
            )
        if (!isChanged && !sectionDto.publish) {
            return privacyCenterPolicySection
        }
        privacyCenterPolicySection.name = sectionDto.name
        privacyCenterPolicySection.anchor = sectionDto.anchor
        privacyCenterPolicySection.body = sectionDto.body
        privacyCenterPolicySection.addToMenu = sectionDto.addToMenu
        privacyCenterPolicySection.showHeader = sectionDto.showHeader
        privacyCenterPolicySection.noPrivacyPolicy = sectionDto.noPrivacyPolicy
        val section = privacyCenterPolicySectionRepository.save(privacyCenterPolicySection)
        if (sectionDto.publish) {
            privacyCenterBuilderService.insertPrivacyCenterBuildRequest(privacyCenter, "privacy center policy (sec) updated")
        }
        return section
    }

    fun deletePolicySection(
        organizationId: OrganizationPublicId,
        privacyCenterId: PrivacyCenterPublicId,
        privacyCenterPolicySectionId: UUIDString
    ) {
        val privacyCenter = findByOrganizationAndPublicId(organizationId, privacyCenterId)
        val privacyCenterPolicySection =
            lookupPrivacyCenterPolicySectionOrThrow(privacyCenter, privacyCenterPolicySectionId)
        privacyCenterPolicySectionRepository.delete(privacyCenterPolicySection)
        privacyCenterBuilderService.insertPrivacyCenterBuildRequest(privacyCenter, "privacy center policy sec deleted")
    }

    private fun lookupPrivacyCenterPolicySectionOrThrow(
        privacyCenter: PrivacyCenter,
        privacyCenterPolicySectionId: UUIDString
    ): PrivacyCenterPolicySection {
        val privacyCenterPolicySectionOpt =
            privacyCenterPolicySectionRepository.findById(UUID.fromString(privacyCenterPolicySectionId))
        if (privacyCenterPolicySectionOpt.isEmpty()) {
            throw ResourceNotFoundException(privacyCenterPolicySectionId)
        }
        val privacyCenterPolicySection = privacyCenterPolicySectionOpt.get()
        if (privacyCenterPolicySection.privacyCenter!!.id != privacyCenter.id) {
            throw ResourceNotFoundException(privacyCenterPolicySectionId)
        }
        return privacyCenterPolicySection
    }

    private fun lookupPrivacyCenterOrThrow(
        privacyCenterId: PrivacyCenterPublicId,
        organizationId: OrganizationPublicId? = null
    ): PrivacyCenter {
        val privacyCenter =
            privacyCenterRepo.findByPublicId(privacyCenterId) ?: throw PrivacyCenterNotFound(privacyCenterId)
        if (organizationId != null && privacyCenter.organization?.publicId != organizationId) {
            throw PrivacyCenterNotFound(privacyCenterId)
        }
        return privacyCenter
    }

    private fun lookupOrganizationOrThrow(organizationId: OrganizationPublicId): Organization =
        organizationRepository.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)
}
