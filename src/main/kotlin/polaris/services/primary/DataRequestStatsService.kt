package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.OrganizationPublicId
import polaris.models.dto.request.ActiveStats
import polaris.models.dto.request.InactiveStats
import polaris.models.dto.request.StatsResponse
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.request.DataRequestState
import polaris.repositories.DataRequestRepository
import polaris.repositories.OrganizationRepository
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalAdjusters
import javax.transaction.Transactional

val INACTIVE_STATES = listOf(DataRequestState.CLOSED)

@Service
@Transactional
class DataRequestStatsService(
    private val dataRequestRepository: DataRequestRepository,
    private val organizationRepository: OrganizationRepository
) {

    companion object {
        private val log = LoggerFactory.getLogger(DataRequestStatsService::class.java)
    }

    fun getStats(organizationId: OrganizationPublicId): StatsResponse {
        val organization = lookupOrganizationOrThrow(organizationId)

        val thisweekCutoff = ZonedDateTime.now().with(TemporalAdjusters.next(DayOfWeek.FRIDAY)).plusDays(1)
        val closedCutoff = ZonedDateTime.now().plusDays(90).toLocalDate()

        // TODO: stats calculation should be improved
        val allRequests = dataRequestRepository
            .findByOrganizationAndStateIsIn(organization, DataRequestState.values().toList())
            .filter {
                val closed = it.closedAt?.toLocalDateTime()?.toLocalDate()

                it.state != DataRequestState.CLOSED ||
                    (closed?.isBefore(closedCutoff) ?: false)
            }

        val activeRequests = allRequests.filter { !INACTIVE_STATES.contains(it.state) }
        val inactiveRequests = allRequests.filter { INACTIVE_STATES.contains(it.state) }

        val active = activeRequests.count()
        val thisweek = activeRequests.filter { thisweekCutoff.isAfter(it.requestDueDate) }.count()
        val overdue = activeRequests.filter { ZonedDateTime.now().isAfter(it.requestDueDate) }.count()

        val inactive = inactiveRequests.count()
        val totalProcessingDays = inactiveRequests.map {
            val closedLocal = it.closedAt?.toLocalDateTime() ?: LocalDateTime.now()
            val createdLocal = it.createdAt.toLocalDate()

            ChronoUnit.DAYS.between(createdLocal, closedLocal).coerceAtLeast(0L)
        }.sum()
        val avgProcessingDays = totalProcessingDays.toFloat() / inactive.toFloat()
        val totalOnTime = inactiveRequests.filter { it.closedAt?.isBefore(it.requestDueDate) ?: false }.count()
        val onTimePct = totalOnTime.toFloat() / inactive.toFloat()

        return StatsResponse(
            active = ActiveStats(active, thisweek, overdue),
            inactive = InactiveStats(inactive, avgProcessingDays, onTimePct)
        )
    }

    private fun lookupOrganizationOrThrow(organizationId: OrganizationPublicId) = organizationRepository.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)
}
