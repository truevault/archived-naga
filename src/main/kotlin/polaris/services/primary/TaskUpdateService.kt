package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import polaris.models.OrganizationPublicId
import polaris.models.dto.springevents.UpdateTasksSpringEvent
import polaris.models.entity.organization.Organization
import polaris.models.entity.user.User
import javax.transaction.Transactional

@Service
@Transactional
class TaskUpdateService(private val publisher: ApplicationEventPublisher) {
    companion object {
        private val log = LoggerFactory.getLogger(TaskUpdateService::class.java)
    }
    fun updateTasks(orgPublicId: OrganizationPublicId, create: Boolean = true, close: Boolean = true, actor: User? = null) {
        log.info("Sending event to update tasks for $orgPublicId, create=$create, close=$close, actor=${actor?.email}")
        publisher.publishEvent(UpdateTasksSpringEvent(orgPublicId, autoCreate = create, autoClose = close, actorId = actor?.id))
    }
    fun updateTasks(org: Organization, create: Boolean = true, close: Boolean = true, actor: User? = null) {
        updateTasks(org.publicId, create = create, close = close, actor = actor)
    }
}
