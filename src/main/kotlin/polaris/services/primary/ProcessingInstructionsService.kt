package polaris.services.primary

import org.springframework.stereotype.Service
import polaris.models.OrganizationPublicId
import polaris.models.UUIDString
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.ProcessingInstruction
import polaris.models.entity.request.DataRequestType
import polaris.repositories.OrganizationRepository
import polaris.repositories.ProcessingInstructionsRepository
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class ProcessingInstructionsService(
    private val organizationRepository: OrganizationRepository,
    private val processingInstructionsRepository: ProcessingInstructionsRepository,
    private val collectionGroupService: CollectionGroupService
) {

    fun findAllByOrganization(organizationId: OrganizationPublicId): List<ProcessingInstruction> {
        val organization = lookupOrganizationOrThrow(organizationId)
        return processingInstructionsRepository.findAllByOrganization(organization)
    }

    fun createOrUpdate(
        organizationId: OrganizationPublicId,
        dataRequestType: DataRequestType,
        dataSubjectTypeId: UUIDString,
        processingInstruction: String
    ): ProcessingInstruction {
        val organization = lookupOrganizationOrThrow(organizationId)
        val dataSubjectType = lookupDataSubjectTypeOrThrow(organization, UUID.fromString(dataSubjectTypeId))
        var existing = processingInstructionsRepository.findByOrganizationAndCollectionGroupAndRequestType(
            organization, dataSubjectType, dataRequestType
        )
        if (existing != null) {
            existing.instruction = processingInstruction
        } else {
            existing = ProcessingInstruction(
                organization = organization,
                collectionGroup = dataSubjectType,
                requestType = dataRequestType,
                instruction = processingInstruction
            )
        }
        return processingInstructionsRepository.save(existing)
    }

    private fun lookupDataSubjectTypeOrThrow(organization: Organization, id: UUID): CollectionGroup =
        collectionGroupService.findByIdAndOrganization(id, organization)

    private fun lookupOrganizationOrThrow(organizationId: OrganizationPublicId): Organization =
        organizationRepository.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)
}
