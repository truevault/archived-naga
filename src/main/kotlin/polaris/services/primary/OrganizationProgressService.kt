package polaris.services.primary

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.OrganizationProgressUpdateDto
import polaris.models.entity.organization.EventData
import polaris.models.entity.organization.OrganizationProgress
import polaris.repositories.OrganizationProgressRepository

@Service
@Transactional
class OrganizationProgressService(
    val organizationProgressRepo: OrganizationProgressRepository,
    val lookup: LookupService
) {

    fun addOrganizationProgress(organizationId: OrganizationPublicId, key: String, progress: OrganizationProgressUpdateDto, events: MutableList<EventData>): OrganizationProgress {
        val o = lookup.orgOrThrow(organizationId)

        val existing = organizationProgressRepo.findByOrganizationIdAndProgressKey(o.id, key)

        val saved = existing ?: OrganizationProgress(
            organizationId = o.id,
            progressKey = key
        )

        saved.progressValue = progress.progress
        organizationProgressRepo.save(saved)

        val from = existing?.progressValue
        val to = saved.progressValue
        if (from != to) {
            events.add(EventData.GenericProgressUpdate(progressKey = key, from = existing?.progressValue, to = saved.progressValue))
        }

        return saved
    }
}
