package polaris.services.primary

import org.springframework.stereotype.Service
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.AddCustomOrganizationDataSourceDto
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataSource
import polaris.models.entity.vendor.KnownVendorKey
import polaris.models.entity.vendor.Vendor
import polaris.repositories.DataSourceCollectionGroupRepo
import polaris.repositories.DataSourceReceivedPICRepo
import polaris.repositories.OrganizationDataSourceRepository
import polaris.repositories.VendorRepository
import java.util.UUID
import javax.transaction.Transactional

@Service
@Transactional
class OrganizationDataSourceService(
    private val lookup: LookupService,
    private val orgDataSourceRepo: OrganizationDataSourceRepository,
    private val receivedPICRepo: DataSourceReceivedPICRepo,
    private val vendorRepo: VendorRepository,
    private val dataSourceAssociationRepo: DataSourceCollectionGroupRepo
) {
    fun addDataSource(orgId: OrganizationPublicId, custom: AddCustomOrganizationDataSourceDto, context: List<CollectionContext>): OrganizationDataSource {
        val org = lookup.orgOrThrow(orgId)
        val vendor = vendorRepo.save(
            Vendor(
                name = custom.name,
                recipientCategory = custom.category,
                url = custom.url,
                email = custom.contactEmail,
                organization = org,
                isDataSource = true
            )
        )

        return addDataSource(org, vendor, context)
    }

    fun addDataSource(orgId: OrganizationPublicId, vendorKey: KnownVendorKey, context: List<CollectionContext>): OrganizationDataSource {
        val org = lookup.orgOrThrow(orgId)
        val vendor = lookup.globalVendorByKeyOrThrow(vendorKey)

        return addDataSource(org, vendor, context)
    }

    fun addDataSource(orgId: OrganizationPublicId, vendorId: UUID, context: List<CollectionContext>): OrganizationDataSource {
        val org = lookup.orgOrThrow(orgId)
        val vendor = lookup.vendorOrThrow(vendorId)

        return addDataSource(org, vendor, context)
    }

    private fun addDataSource(org: Organization, vendor: Vendor, context: List<CollectionContext>): OrganizationDataSource {
        var source = orgDataSourceRepo.findByOrganizationAndVendor(org, vendor)

        if (source != null) {
            source.collectionContexts = (source.collectionContexts + context).distinct()
        } else {
            source = OrganizationDataSource(
                vendorId = vendor.id,
                vendor = vendor,
                organizationId = org.id,
                organization = org,
                collectionContexts = context
            )
        }

        return orgDataSourceRepo.save(source)
    }

    fun removeDataSource(orgId: OrganizationPublicId, vendorKey: KnownVendorKey, collectionContext: List<CollectionContext>) {
        val o = lookup.orgOrThrow(orgId)
        val v = lookup.globalVendorByKeyOrThrow(vendorKey)
        removeDataSource(o, v, collectionContext)
    }

    fun removeDataSource(orgId: OrganizationPublicId, vendorId: UUID, collectionContext: List<CollectionContext>) {
        val o = lookup.orgOrThrow(orgId)
        val v = lookup.vendorOrThrow(vendorId)
        removeDataSource(o, v, collectionContext)
    }

    fun removeDataSource(org: Organization, vendor: Vendor, collectionContext: List<CollectionContext>) {
        val existing = orgDataSourceRepo.findByOrganizationAndVendor(org, vendor)

        if (existing != null) {
            if (collectionContext.isEmpty()) {
                deleteSource(org, existing, vendor)
            } else {
                removeCollectionContextFromSource(org, existing, vendor, collectionContext, true)
            }
        }
    }

    private fun deleteSource(org: Organization, source: OrganizationDataSource, vendor: Vendor) {
        orgDataSourceRepo.delete(source)

        // remove source reciepts and associations
        val associations = dataSourceAssociationRepo.findAllByCollectionGroup_OrganizationAndVendor(org, vendor)
        dataSourceAssociationRepo.deleteAll(associations)
        val receipts = receivedPICRepo.findAllByCollectedPIC_CollectionGroup_OrganizationAndVendor(org, vendor)
        receivedPICRepo.deleteAll(receipts)

        if (vendor.organization != null) {
            vendorRepo.delete(vendor)
        }
    }

    private fun removeCollectionContextFromSource(org: Organization, source: OrganizationDataSource, vendor: Vendor, collectionContext: List<CollectionContext>, deleteOnEmpty: Boolean = false) {
        val newContext = source.collectionContexts - collectionContext.toSet()

        if (newContext.isNullOrEmpty() && deleteOnEmpty) {
            deleteSource(org, source, vendor)
        } else {
            source.collectionContexts = newContext
            orgDataSourceRepo.save(source)
        }
    }
}
