package polaris.services.primary
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.OrganizationPublicId
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.organization.*
import polaris.models.entity.user.User
import polaris.repositories.*
import polaris.services.support.EmailService
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class MessageTemplateService(
    private val customMessageRepo: CustomMessageRepository,
    private val organizationRepo: OrganizationRepository,
    private val emailService: EmailService
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(DataSubjectRequestService::class.java)
    }

    fun previewTemplate(organizationId: OrganizationPublicId, actor: User, messageType: CustomMessageTypeEnum): Map<String, Any> {
        val org = organizationRepo.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)
        val customMessage = getOrCreateMessage(organizationId, messageType)
        val customText = customMessage.customText
        val template = emailService.previewEmail(org, actor, messageType)

        return mapOf(
            "messageType" to messageType.toDto(),
            "customText" to customText,
            "templateHTML" to template
        )
    }

    fun customizeTemplate(
        organizationId: OrganizationPublicId,
        actor: User,
        messageType: CustomMessageTypeEnum,
        text: String
    ): Map<String, Any> {
        val customMessage = getOrCreateMessage(organizationId, messageType)

        if (text.trim().length > 0) {
            customMessage.customText = text
            customMessageRepo.save(customMessage)
        } else if (customMessage.id != null) {
            customMessageRepo.deleteById(customMessage.id)
        }

        return previewTemplate(organizationId, actor, messageType)
    }

    private fun getOrCreateMessage(orgId: OrganizationPublicId, type: CustomMessageTypeEnum): CustomMessage {
        val org = organizationRepo.findByPublicId(orgId) ?: throw OrganizationNotFound(orgId)
        val found = customMessageRepo.findByOrganizationAndMessageType(org, type)
        return found ?: CustomMessage(organization = org, messageType = type)
    }
}
