package polaris.services.primary

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE
import polaris.models.dto.request.CreateDataRequest
import polaris.models.dto.toIso
import polaris.models.entity.request.DataRequestEventType
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubmissionSourceEnum
import polaris.models.entity.request.DataRequestSubstate
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.request.DataSubjectRequestEvent
import polaris.repositories.DataRequestRepository
import polaris.services.support.EmailService
import polaris.services.support.OptOutService
import java.time.ZonedDateTime

@Service
class DataSubjectRequestAutoCloseService(
    private val dataRequestRepository: DataRequestRepository,
    private val dataSubjectRequestService: DataSubjectRequestService,
    private val emailService: EmailService,
    private val optOutService: OptOutService
) {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(DataSubjectRequestAutoCloseService::class.java)
    }

    @Transactional
    fun autoCloseRequests(at: ZonedDateTime? = null) {
        val closingAt = at ?: ZonedDateTime.now(POLARIS_TIME_ZONE)
        log.info("auto-closing requests at: {}", closingAt)

        // A missing due date only happens for unextended GDPR, in this case compare to 30 days in the past
        val gdprCompareToRequestDate = at?.minusDays(30) ?: ZonedDateTime.now(POLARIS_TIME_ZONE).minusDays(30)

        // If request due date exists that will be used
        val autoCloseable = dataRequestRepository.findAutoCloseable(closingAt, gdprCompareToRequestDate)
        log.info("found: ${autoCloseable.size} auto-closeable requests")

        autoCloseable.forEach {
            try {
                autoClose(it, DataRequestType.CCPA_OPT_OUT)
            } catch (e: Exception) {
                log.error("Encountered exception when auto-closing request {}: {}", it.publicId, e.message, e)
            }
        }
    }

    @Transactional
    fun autoClose(request: DataSubjectRequest, optOutType: DataRequestType) {
        log.info("auto-closing request: ${request.publicId}")
        val isDelete = request.isDelete()
        val isGdpr = request.isGdpr()

        var note: String? = null

        if (isDelete && !isGdpr) {
            val hasOptOut = optOutService.processesOptOut(request.organization!!)

            if (hasOptOut) {
                log.info("creating opt-out for delete request: ${request.publicId}")
                val createRequest = CreateDataRequest(
                    subjectFirstName = request.subjectFirstName,
                    subjectLastName = request.subjectLastName,
                    subjectEmailAddress = request.subjectEmailAddress,
                    subjectIpAddress = request.subjectIpAddress,
                    subjectMailingAddress = request.subjectMailingAddress,
                    subjectPhoneNumber = request.subjectPhoneNumber,
                    dataRequestType = optOutType,
                    requestDate = ZonedDateTime.now().toIso(),
                    isDataSubject = false,
                    submissionSource = DataRequestSubmissionSourceEnum.POLARIS
                )
                log.info("created opt-out dto: $createRequest")
                val optOut = dataSubjectRequestService.create(null, request.organization!!.publicId, createRequest, true)
                log.info("created opt out request: ${optOut.publicId}")
                note = "Converted to Request to Opt-out, New request: ${optOut.publicId}\n\n"
            } else {
                log.info("skipping opt-out for delete request: ${request.publicId}")
            }
        }

        request.state = DataRequestState.CLOSED
        request.substate = DataRequestSubstate.SUBJECT_NOT_VERIFIED
        request.closedAt = ZonedDateTime.now()
        request.isAutoclosed = true

        val body = emailService.getSubjectRequestClosedNotVerifiedBody(request)

        note = (note ?: "") + body

        dataSubjectRequestService.addEventsToRequest(
            request,
            listOf(
                DataSubjectRequestEvent(
                    actor = null,
                    isDataSubject = false,
                    eventType = DataRequestEventType.CLOSED_NOT_VERIFIED,
                    message = note
                )
            )
        )

        dataRequestRepository.saveAndFlush(request)

        if (request.requestType.sendClosedNotVerifiedNotification) {
            emailService.sendSubjectRequestClosedNotVerifiedBody(request)
        }
    }

    @Transactional
    fun closeAsDuplicate(duplicateRequest: DataSubjectRequest, originalRequest: DataSubjectRequest) {
        duplicateRequest.state = DataRequestState.CLOSED
        duplicateRequest.substate = DataRequestSubstate.DUPLICATE_REQUEST
        duplicateRequest.closedAt = ZonedDateTime.now()
        duplicateRequest.isAutoclosed = true
        duplicateRequest.duplicatesRequest = originalRequest

        dataRequestRepository.save(duplicateRequest)

        dataSubjectRequestService.addEventsToRequest(
            duplicateRequest,
            listOf(
                DataSubjectRequestEvent(
                    actor = null,
                    isDataSubject = false,
                    eventType = DataRequestEventType.CLOSED_AS_DUPLICATE,
                    message = null
                )
            )
        )
    }
}
