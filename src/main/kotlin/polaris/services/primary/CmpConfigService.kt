package polaris.services.primary

import com.amazonaws.services.cloudfront.model.CreateInvalidationRequest
import com.amazonaws.services.cloudfront.model.InvalidationBatch
import com.amazonaws.services.cloudfront.model.Paths
import com.amazonaws.services.s3.model.ObjectMetadata
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import polaris.models.dto.toIso
import polaris.models.entity.organization.Organization
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.repositories.OrganizationRepository
import polaris.services.support.AwsClientService
import java.time.ZonedDateTime

@Service
class CmpConfigService(
    @Value("\${polaris.aws.cmpConfig.s3Bucket}")
    private val s3ConfigBucket: String?,

    @Value("\${polaris.aws.cmpConfig.s3Path}")
    private val s3ConfigRoot: String?,

    @Value("\${polaris.aws.cmpConfig.cloudFrontDistributionId}")
    private val cloudFrontDistributionId: String?,

    private val organizationRepository: OrganizationRepository,
    private val consentConfigurationService: ConsentConfigurationService,
    private val awsClient: AwsClientService
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(CmpConfigService::class.java)
    }

    fun publishForOrg(org: Organization) {
        org.privacyCenters
            .filter { pc -> isActive(pc) }
            .forEach { pc -> publishConfig(org, pc) }
    }

    fun publishForAllOrgs() {
        val orgs = organizationRepository.findAll()
            .filter { org -> org.privacyCenters.any { pc -> isActive(pc) } }

        log.info("[cmp config] Publishing CMP configs for ${orgs.size} orgs...")

        orgs.forEach { org -> publishForOrg(org) }

        log.info("[cmp config] Finished publishing CMP configs for ${orgs.size} orgs")
    }

    private fun publishConfig(org: Organization, privacyCenter: PrivacyCenter) {
        val s3ConfigPath = "$s3ConfigRoot/${privacyCenter.publicId}.json"
        try {
            publishToS3(org, privacyCenter, s3ConfigPath)
            invalidateCloudFront(org, privacyCenter, s3ConfigPath)
        } catch (ex: Exception) {
            log.error(
                "[cmp config] Error occurred while publishing CMP configs for ${org.name} (PC ${privacyCenter.publicId}): ${ex.message}",
                ex
            )
        }
    }

    private fun publishToS3(org: Organization, privacyCenter: PrivacyCenter, s3ConfigPath: String) {
        log.info("[cmp config] Publishing CMP config for ${org.name} (PC ${privacyCenter.publicId}) to S3 (bucket=$s3ConfigBucket, path=$s3ConfigPath)...")
        val config = consentConfigurationService.buildConsentConfiguration(org, privacyCenter)
        val s3Client = awsClient.s3Client()
        val json = jacksonObjectMapper().writeValueAsString(config)
        val stream = IOUtils.toInputStream(json, "UTF-8")

        // when uploading a Stream to s3, you must specify the contentLength in the metadata
        val metadata = ObjectMetadata()
        val bytes = IOUtils.toByteArray(IOUtils.toInputStream(json, "UTF-8"))
        metadata.contentLength = bytes.size.toLong()

        s3Client.putObject(s3ConfigBucket, s3ConfigPath, stream, metadata)
    }

    private fun invalidateCloudFront(org: Organization, privacyCenter: PrivacyCenter, s3ConfigPath: String) {

        if (cloudFrontDistributionId?.isNullOrBlank()!!) {
            log.info("[cmp config] CMP config CF Distribution ID not configured. skipping invalidation for ${org.name} (PC ${privacyCenter.publicId})")
            return
        }

        // convert the s3 config path to a valid CloudFront path (e.g. staging/config/blah.json => /config/blah.json)
        val cfConfigPath = s3ConfigPath.replace(Regex("^[^/]*/"), "/")

        log.info("[cmp config] Invalidating CMP config CF Distribution $cloudFrontDistributionId for ${org.name} (PC ${privacyCenter.publicId}) with configPath=$cfConfigPath...")

        val cfClient = awsClient.cfClient()
        val invalidationPaths = Paths().withItems(cfConfigPath).withQuantity(1)
        val invalidationBatch = InvalidationBatch(invalidationPaths, ZonedDateTime.now().toIso())
        val invalidation = CreateInvalidationRequest(cloudFrontDistributionId, invalidationBatch)
        cfClient.createInvalidation(invalidation)
    }

    private fun isActive(pc: PrivacyCenter): Boolean {
        // TODO: replace with proper status flag on PrivacyCenter
        return pc.cloudfrontDistribution != null
    }
}
