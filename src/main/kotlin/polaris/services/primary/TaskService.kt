package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.util.LookupService
import polaris.models.UUIDString
import polaris.models.dto.springevents.UpdateTasksSpringEvent
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.user.User
import polaris.repositories.OrganizationAuthTokenRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.TaskRepository
import polaris.services.dto.OrganizationVendorDtoService
import polaris.services.support.DataMapService
import polaris.services.support.DpoService
import polaris.services.support.OptOutService
import polaris.util.tasks.AddDpoTask
import polaris.util.tasks.AddEmploymentDataRecipientsTask
import polaris.util.tasks.AuthorizeGoogleAnalyticsTask
import polaris.util.tasks.ClassifyVendorsTask
import polaris.util.tasks.CompleteDataRecipientsTask
import polaris.util.tasks.CompleteFinancialIncentivesTask
import polaris.util.tasks.DataMinimizationTask
import polaris.util.tasks.MapEmploymentGroupVendorsTask
import polaris.util.tasks.NoLongerSharingOrSellingTask
import polaris.util.tasks.PublishSellingOrSharingTask
import polaris.util.tasks.TaskManager
import polaris.util.tasks.UnsafeTransferReminderTask
import polaris.util.tasks.UpdateDoNotContactThirdPartyTask
import polaris.util.tasks.UpdateGoogleAnalyticsIdTask
import polaris.util.tasks.UpdatedSellingOrSharingTask
import polaris.util.tasks.UploadEmailHeaderLogoTask
import polaris.util.tasks.WebsiteAuditCANoticeTask
import polaris.util.tasks.WebsiteAuditDeferTask
import polaris.util.tasks.WebsiteAuditOptOutLinkTask
import polaris.util.tasks.WebsiteAuditOptOutLinkUpgradeTask
import polaris.util.tasks.WebsiteAuditPolarisJSTask
import polaris.util.tasks.WebsiteAuditPrivacyLinkTask
import java.time.ZonedDateTime
import java.util.UUID
import java.util.concurrent.Semaphore

enum class TaskFilter {
    active,
    complete,
}

@Service
@Transactional
class TaskService(
    private val taskRepo: TaskRepository,
    private val orgRepo: OrganizationRepository,
    private val orgDataRecipientRepo: OrganizationDataRecipientRepository,
    private val dpoService: DpoService,
    private val surveyService: OrganizationSurveyService,
    private val dataMapService: DataMapService,
    private val organizationAuthTokenRepository: OrganizationAuthTokenRepository,
    private val orgSurveyQuestionRepo: OrganizationSurveyQuestionRepository,
    private val organizationVendorDtoService: OrganizationVendorDtoService,
    private val optOutService: OptOutService,
    private val lookup: LookupService
) {
    companion object {
        private val log = LoggerFactory.getLogger(TaskService::class.java)
        private val semaphore = Semaphore(1)
    }

    private fun buildManagers(): List<TaskManager> {
        return listOf(
            // POLARIS-2521 - added back temporarily due to presence of open CLASSIFY_VENDOR tasks in the DB
            ClassifyVendorsTask(orgDataRecipientRepo),
            PublishSellingOrSharingTask(optOutService, orgRepo, taskRepo),
            UpdatedSellingOrSharingTask(optOutService, orgRepo, taskRepo),
            NoLongerSharingOrSellingTask(optOutService, orgRepo, taskRepo),
            UploadEmailHeaderLogoTask(),
            AddDpoTask(dpoService),
            AuthorizeGoogleAnalyticsTask(organizationAuthTokenRepository),
            CompleteFinancialIncentivesTask(surveyService),
            UnsafeTransferReminderTask(orgSurveyQuestionRepo),
            CompleteDataRecipientsTask(orgDataRecipientRepo, organizationVendorDtoService),
            AddEmploymentDataRecipientsTask(this),
            MapEmploymentGroupVendorsTask(dataMapService),
            UpdateDoNotContactThirdPartyTask(),
            UpdateGoogleAnalyticsIdTask(orgSurveyQuestionRepo),
            WebsiteAuditCANoticeTask(orgSurveyQuestionRepo),
            WebsiteAuditDeferTask(orgSurveyQuestionRepo),
            WebsiteAuditOptOutLinkTask(orgSurveyQuestionRepo),
            WebsiteAuditOptOutLinkUpgradeTask(orgSurveyQuestionRepo),
            WebsiteAuditPolarisJSTask(orgSurveyQuestionRepo),
            WebsiteAuditPrivacyLinkTask(orgSurveyQuestionRepo),
            DataMinimizationTask(orgSurveyQuestionRepo)
        )
    }

    fun openTaskCount(org: Organization): Long {
        return forOrg(org, TaskFilter.active).count().toLong()
    }

    fun forOrg(org: Organization, filter: TaskFilter): List<Task> {
        return when (filter) {
            TaskFilter.active -> {
                suppressDuplicateTasks(taskRepo.findAllByOrganizationAndCompletedAtIsNull(org))
            }
            TaskFilter.complete -> taskRepo.findAllByOrganizationAndCompletedAtIsNotNull(org)
        }
    }

    fun findById(org: Organization, taskId: UUIDString): Task? {
        return taskRepo.findByOrganizationAndId(organization = org, id = UUID.fromString(taskId))
    }

    fun autoCreateTasks() {
        val allOrgs = orgRepo.findAll()
        allOrgs.forEach { createTasksForOrg(it) }
    }

    fun createTasksForOrg(org: Organization) {
        val tasks = buildManagers()
        val existing = taskRepo.findAllByOrganizationAndCompletedAtIsNull(org)
        val closed = taskRepo.findAllByOrganizationAndCompletedAtIsNotNull(org)

        val newTasks = mutableListOf<Task>()

        tasks.forEach { t ->
            if (t.shouldCreate(org, existing, closed)) {
                val task = t.createFor(org)
                newTasks.add(task)
            }
        }

        if (newTasks.isNotEmpty()) {
            taskRepo.saveAll(newTasks)

            newTasks.forEach { task ->
                val manager = managerFor(task)
                manager?.afterCreate(task)
            }
        }
    }

    fun createDpoTask(org: Organization): Task? = createIndividualTask(org, AddDpoTask(dpoService))
    fun createAuthorizeGoogleAnalyticsTask(org: Organization): Task? = createIndividualTask(org, AuthorizeGoogleAnalyticsTask(organizationAuthTokenRepository))
    fun createFinancialIncentivesTask(org: Organization): Task? = createIndividualTask(org, CompleteFinancialIncentivesTask(surveyService))

    fun autoCloseTasks() {
        val allOrgs = orgRepo.findAll()
        allOrgs.forEach { closeTasksForOrg(it) }
    }

    fun closeTask(task: Task, actor: User? = null) {
        task.completedAt = ZonedDateTime.now()
        task.completedBy = actor

        taskRepo.save(task)

        val manager = managerFor(task)
        manager?.afterClose(task)
    }

    fun createTask(task: Task) {
        taskRepo.save(task)
        val manager = managerFor(task)
        manager?.afterCreate(task)
    }

    fun closeTasksForOrg(org: Organization, actor: User? = null) {
        val tasks = buildManagers()
        val existing = taskRepo.findAllByOrganizationAndCompletedAtIsNull(org)

        val closedTasks = mutableListOf<Task>()

        existing.forEach { t ->
            val manager = managerFor(t, tasks)
            if (manager != null && manager.shouldClose(t, org, existing)) {
                closedTasks.add(t)
            }
        }

        closedTasks.forEach { t ->
            t.completedAt = ZonedDateTime.now()
            t.completedBy = actor
        }

        taskRepo.saveAll(closedTasks)

        closedTasks.forEach { task ->
            val manager = managerFor(task)
            manager?.afterClose(task)
        }
    }

    private fun createIndividualTask(org: Organization, manager: TaskManager): Task? {
        val existing = taskRepo.findAllByOrganizationAndCompletedAtIsNull(org)
        val closed = taskRepo.findAllByOrganizationAndCompletedAtIsNotNull(org)

        if (manager.shouldCreate(org, existing, closed)) {
            return taskRepo.save(manager.createFor(org))
        }

        return null
    }

    private fun suppressDuplicateTasks(tasks: List<Task>): List<Task> {
        val managers = buildManagers()
        val grouped = tasks.groupBy { it.taskKey }

        return grouped.flatMap {
            if (it.key == null) {
                return emptyList()
            }
            val manager = managerFor(it.key!!, managers)

            if (manager != null && !manager.allowDuplicates()) {
                val t = it.value.firstOrNull()
                if (t != null) listOf(t) else emptyList()
            } else {
                it.value
            }
        }
    }

    fun managerFor(key: TaskKey, managers: List<TaskManager>? = null): TaskManager? {
        val internalManagers = managers ?: buildManagers()
        return internalManagers.find { it.manages() == key }
    }

    fun managerFor(task: Task, managers: List<TaskManager>? = null): TaskManager? {
        return managerFor(task.taskKey!!, managers)
    }

    fun handleUpdateTasks(update: UpdateTasksSpringEvent) {
        log.info("Update Tasks event listener for org: $update")
        semaphore.acquireUninterruptibly()
        val org = lookup.orgOrThrow(update.orgPublicId)
        val actor = update.actorId?.let { lookup.userOrThrow(it) }

        if (update.autoCreate) {
            log.info("Creating tasks for org: ${org.publicId} - ${org.name}")
            createTasksForOrg(org)
        }

        if (update.autoClose) {
            log.info("Closing tasks for org: ${org.publicId} - ${org.name}")
            closeTasksForOrg(org, actor = actor)
        }
        semaphore.release()

        log.info("Completed Update Tasks event listener for org: ${update.orgPublicId}")
    }
}

@Component
class TaskUpdateListener(private val taskService: TaskService) {
    @Async
    @EventListener
    @Transactional
    fun handleUpdateTasks(update: UpdateTasksSpringEvent) {
        Thread.sleep(500L)
        taskService.handleUpdateTasks(update)
    }
}
