package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.controllers.util.LookupService
import polaris.models.dto.organization.CreateProcessingActivityRequest
import polaris.models.dto.organization.PatchProcessingActivityRequest
import polaris.models.entity.ProcessingActivityExists
import polaris.models.entity.ProcessingActivityNotFound
import polaris.models.entity.compliance.AutocreatedProcessingActivitySlug
import polaris.models.entity.organization.DefaultProcessingActivity
import polaris.models.entity.organization.GDPR_LAWFUL_BASES_SURVEY_NAME
import polaris.models.entity.organization.LawfulBasis
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.ProcessingActivity
import polaris.models.entity.vendor.ProcessingRegion
import polaris.repositories.DefaultProcessingActivityRepo
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.ProcessingActivityRepo
import java.util.UUID
import javax.transaction.Transactional

@Service
@Transactional
class ProcessingActivityService(
    private val defaultProcessingActivityRepo: DefaultProcessingActivityRepo,
    private val processingActivityRepo: ProcessingActivityRepo,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val lookup: LookupService
) {

    companion object {
        private val logger = LoggerFactory.getLogger(ProcessingActivityService::class.java)
    }

    fun allDefaultProcessingActivities(): List<DefaultProcessingActivity> {
        return defaultProcessingActivityRepo.findAll().toList()
    }

    fun processingActivitiesForOrg(org: Organization): List<ProcessingActivity> {
        return processingActivityRepo.findAllByOrganization(org)
    }

    fun getConsentProcessingActivitiesForOrg(org: Organization): List<ProcessingActivity> {
        val all = processingActivitiesForOrg(org)
        return all.filter { it.gdprLawfulBases.contains(LawfulBasis.CONSENT) }
    }

    fun createProcessingActivity(org: Organization, create: CreateProcessingActivityRequest): ProcessingActivity {
        // do we already have a processing activity for this default
        var def: DefaultProcessingActivity? = null
        if (create.defaultId != null) {
            def = defaultProcessingActivityRepo.findById(create.defaultId).orElseThrow { ProcessingActivityNotFound(create.defaultId.toString()) }
            val existing = processingActivityRepo.findByOrganizationAndDefaultProcessingActivity(org, def)

            if (existing != null) {
                throw ProcessingActivityExists(create.defaultId.toString())
            }
        }

        val defaultLawfulBases = (def?.gdprMandatoryLawfulBases ?: emptyList()) + (def?.gdprOptionalLawfulBases ?: emptyList())
        val lawfulBases = (create.lawfulBases ?: emptyList()) + defaultLawfulBases
        val name = create.name ?: def?.name ?: throw ProcessingActivityNotFound(create.defaultId.toString())

        return processingActivityRepo.save(
            ProcessingActivity(
                id = UUID.randomUUID(),
                name = name,
                defaultProcessingActivity = def,
                gdprLawfulBases = lawfulBases.distinct(),
                processingRegions = create.regions ?: listOf(ProcessingRegion.UNITED_STATES),
                organization = org
            )
        )
    }

    fun updateProcessingActivity(org: Organization, activityId: UUID, update: PatchProcessingActivityRequest): ProcessingActivity {
        val activity = processingActivityRepo.findById(activityId).orElseThrow { ProcessingActivityNotFound(activityId.toString()) }
        if (activity.organization != org) {
            throw ProcessingActivityNotFound(activityId.toString())
        }

        val mandatoryLawfulBases = activity.defaultProcessingActivity?.gdprMandatoryLawfulBases ?: emptyList()

        if (update.name != null) {
            activity.name = update.name
        }

        if (update.lawfulBases != null) {
            activity.gdprLawfulBases = update.lawfulBases
        }

        if (update.regions != null) {
            activity.processingRegions = update.regions
        }

        activity.gdprLawfulBases += mandatoryLawfulBases
        activity.gdprLawfulBases = activity.gdprLawfulBases.distinct()

        return processingActivityRepo.save(activity)
    }

    fun deleteProcessingActivityRegion(org: Organization, activityId: UUID, regions: List<ProcessingRegion>) {
        val activity = requireProcessingActivity(org, activityId)
        val set = activity.processingRegions.toMutableSet()
        set.removeAll(regions.toSet())
        val updatedRegions = set.toList()

        if (activity.autocreationSlug == AutocreatedProcessingActivitySlug.AUTOMATED_DECISION_MAKING) {
            val automatedQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, GDPR_LAWFUL_BASES_SURVEY_NAME, "automated-decision-making")
            if (automatedQuestion != null) {
                automatedQuestion.answer = "false"
                organizationSurveyQuestionRepository.save(automatedQuestion)
            }
        } else if (activity.autocreationSlug == AutocreatedProcessingActivitySlug.COLD_SALES_COMMUNICATIONS) {
            val outboundQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, GDPR_LAWFUL_BASES_SURVEY_NAME, "outbound-sales-eea-uk")
            if (outboundQuestion != null) {
                outboundQuestion.answer = "false"
                organizationSurveyQuestionRepository.save(outboundQuestion)
            }
        }

        if (updatedRegions.isEmpty()) {
            processingActivityRepo.delete(activity)
        } else {
            activity.processingRegions = updatedRegions
            processingActivityRepo.save(activity)
        }
    }

    fun associateProcessingActivityAndPic(org: Organization, activityId: UUID, picId: UUID): ProcessingActivity {
        val pa = requireProcessingActivity(org, activityId)
        val pic = lookup.picOrThrow(picId)

        pa.processedPic.add(pic)
        processingActivityRepo.save(pa)

        return pa
    }

    fun unassociateProcessingActivityAndPic(org: Organization, activityId: UUID, picId: UUID): ProcessingActivity {
        val pa = requireProcessingActivity(org, activityId)
        val pic = lookup.picOrThrow(picId)

        pa.processedPic.remove(pic)
        processingActivityRepo.save(pa)

        return pa
    }

    private fun requireProcessingActivity(org: Organization, activityId: UUID): ProcessingActivity {
        return findProcessingActivity(org, activityId) ?: throw ProcessingActivityNotFound(activityId.toString())
    }

    private fun findProcessingActivity(org: Organization, activityId: UUID): ProcessingActivity? {
        // first try searching by the default id
        val defOpt = defaultProcessingActivityRepo.findById(activityId)
        val activity = if (defOpt.isPresent) {
            processingActivityRepo.findByOrganizationAndDefaultProcessingActivity(org, defOpt.get())
        } else {
            processingActivityRepo.findById(activityId).orElse(null)
        }

        if (activity == null || activity.organization != org) {
            return null
        }

        return activity
    }
}
