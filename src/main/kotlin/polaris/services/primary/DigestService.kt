package polaris.services.primary

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.entity.organization.Organization
import polaris.repositories.OrganizationRepository
import polaris.services.support.EmailService
import java.time.ZonedDateTime
import javax.transaction.Transactional

@Service
@Transactional
class DigestService(
    private val organizationRepository: OrganizationRepository,
    private val dataSubjectRequestService: DataSubjectRequestService,
    private val taskService: TaskService,
    private val emailService: EmailService,
    private val privacyEmailService: PrivacyEmailService
) {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(DigestService::class.java)
    }

    fun sendAllWeeklyDigestEmails() {
        // get all orgs and send each org
        val orgs = organizationRepository.findAll()
        var failed = 0
        log.info("Sending weekly digest emails for ${orgs.size} orgs...")
        orgs.forEach {
            try {
                sendWeeklyDigestEmail(it)
            } catch (ex: Exception) {
                log.error("Exception while processing weekly digest email for Org ${it.name}", ex)
                failed++
            }
        }
        log.info("Completed sending weekly digest emails for ${orgs.size} orgs. $failed orgs had errors")
    }

    fun sendWeeklyDigestEmail(org: Organization) {
        // get all org users and send to each user
        val now = ZonedDateTime.now()
        val cutoff = ZonedDateTime.now().minusWeeks(1)
        val users = org.activeUsers()
        val activeRequests =
            dataSubjectRequestService.findAllForOrganization(org.publicId, filter = DataRequestFilter.active)
        val overdueRequests = activeRequests.filter { r -> r.requestDueDate?.isBefore(now) ?: false }
        val openRequests = activeRequests.filter { r -> r.requestDueDate?.isAfter(now) ?: false }
        val pendingRequests =
            dataSubjectRequestService.findAllForOrganization(org.publicId, filter = DataRequestFilter.pending)
        val autoclosedRequests = dataSubjectRequestService.findAutoclosedAfter(org.publicId, cutoff)
        val tasks = taskService.forOrg(org, filter = TaskFilter.active)
        val receivedEmails = privacyEmailService.privacyEmailsBetween(org, cutoff, now)

        val newTasks = tasks.filter { it.createdAt.isAfter(cutoff) }

        if (openRequests.isEmpty() && overdueRequests.isEmpty() && pendingRequests.isEmpty() && newTasks.isEmpty() && receivedEmails.isEmpty()) {
            log.info("No open requests, overdue requests, pending requests, new tasks, or received emails since $cutoff, skipping weekly digest for ${org.name}")
            return
        }

        if (users.isEmpty()) {
            log.info("No active users found, skipping weekly digest for ${org.name}")
            return
        }

        // Get all my email data for the org
        log.info("Sending weekly digest email to ${users.size} active users for ${org.name}")
        users.forEach { orgUser ->
            emailService.sendWeeklyDigestEmail(
                overdueRequests = overdueRequests,
                openRequests = openRequests,
                pendingRequests = pendingRequests,
                autoclosedRequests = autoclosedRequests,
                receivedEmails = receivedEmails,
                tasks = tasks,
                org = org,
                user = orgUser.user!!
            )
        }
    }
}
