package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import polaris.controllers.InvalidResetTokenException
import polaris.models.OrganizationPublicId
import polaris.models.entity.*
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationUser
import polaris.models.entity.user.*
import polaris.repositories.InvitationRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationUserRepository
import polaris.repositories.UserRepository
import polaris.services.support.EmailService
import polaris.services.support.ValidationService
import java.time.ZonedDateTime
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class UserService(
    private val userRepository: UserRepository,
    private val organizationUserRepository: OrganizationUserRepository,
    private val privacyCenterService: PrivacyCenterService,
    private val invitationRepository: InvitationRepository,
    private val organizationRepository: OrganizationRepository,
    private val passwordEncoder: PasswordEncoder,

    private val emailService: EmailService,
    private val validationService: ValidationService
) {
    companion object {
        private val log = LoggerFactory.getLogger(UserService::class.java)
    }
    /*** Getters ***/
    fun findByEmail(email: String) = userRepository.findByEmail(email.toLowerCase())

    fun allUsersForOrganization(organizationId: OrganizationPublicId): List<OrganizationUser> {
        val organization = lookupOrganizationOrThrow(organizationId)
        return organizationUserRepository.findAllByOrganization(organization)
    }

    fun findByPasswordResetId(passwordResetId: UUID): User {
        val user = userRepository.findByUpdatePasswordId(passwordResetId) ?: throw UserNotFound()
        val hasActiveOrganization = user.organization_users.any { it.status == UserStatus.ACTIVE }

        // If the user has no active organizations, don't let them reset their password
        if (!hasActiveOrganization) {
            throw UserNotFound()
        }

        return user
    }

    fun findInvitation(invitationId: UUID) = invitationRepository.findByIdAndStatus(invitationId, InvitationStatus.OPEN) ?: throw UserNotFound()

    fun springSecurityRolesForUser(user: User): List<String> {
        val activeOrganizations = user.organization_users.filter { it.status == UserStatus.ACTIVE }
        return activeOrganizations.mapNotNull { it.organization?.let { a -> "${a.publicId}:${it.role}" } }
    }

    /*** Updaters ***/
    fun updateUserStatus(email: String, organizationId: OrganizationPublicId, status: UserStatus): OrganizationUser {
        // TODO: Cannot update your own user status
        // TODO: Cannot deactivate last active organization
        val organizationUser = lookupOrganizationUserOrThrow(email, organizationId)

        organizationUser.status = status

        organizationUserRepository.save(organizationUser)

        return organizationUser
    }

    fun updateUserPassword(userEmail: String, password: String, resetToken: String): User {
        val user = userRepository.findByEmail(userEmail) ?: throw UserNotFound()

        val resetTokenUuid = try {
            UUID.fromString(resetToken)
        } catch (e: Exception) {
            throw InvalidResetTokenException("The provided token was not valid. Attempted token was: $resetToken")
        }

        val invitation = user.invitations.find { it.id == resetTokenUuid && it.status == InvitationStatus.OPEN }
        val isValidResetToken = user.updatePasswordId == resetTokenUuid
        val isValidInvitationToken = invitation != null

        // We let you update the password if you have either the reset token ID or a valid invitation ID
        if (!isValidInvitationToken && !isValidResetToken) {
            throw InvalidResetTokenException("No such token was found for the user: ${user.id}. Attempted token was: $resetToken")
        }

        validationService.validatePassword(password)

        user.password = passwordEncoder.encode(password)
        user.updatePasswordId = null
        user.lastLogin = ZonedDateTime.now() // We want to log the user in immediately
        val savedUser = userRepository.save(user)
        log.info("Updated user password [${savedUser.id}]")
        log.debug("User ${user.id} is ${user.email}")
        return savedUser
    }

    // We probably need to rethink how and why we create OrganizationUsers as they're not currently sufficient for determining which orgs a user can use.
    // This is due to the ability to "activate" an org that's a `client` of any activatable org. Such relationships aren't reflected in the OrgUser table and probably shouldn't be.
    fun updateUserActiveOrganization(user: User, organizationId: OrganizationPublicId): User {
        val orgUsers = user.organization_users.filter { it.status == UserStatus.ACTIVE }
        val associatedOrgs = orgUsers.map { it.organization } + orgUsers.flatMap { orgUser -> orgUser.organization?.clientOrganizations ?: emptyList() }
        val organization: Organization? = associatedOrgs.firstOrNull { it?.publicId == organizationId }
        organization.let { user.activeOrganization = organization }
        return userRepository.save(user)
    }

    fun delete(email: String, organizationId: OrganizationPublicId) {
        // TODO: Cannot delete your own user
        val organizationUser = lookupOrganizationUserOrThrow(email, organizationId)

        organizationUserRepository.delete(organizationUser)
    }

    fun resetUserPassword(email: String, requestIp: String?) {
        // Don't give the user any inclination that the email actually exists/doesn't exist, just send the email
        val user = userRepository.findByEmail(email)
        val hasActiveOrganizations = user?.organization_users?.any { it.status == UserStatus.ACTIVE } ?: false
        if (user == null || !hasActiveOrganizations) {
            return
        }

        user.updatePasswordId = UUID.randomUUID()
        userRepository.save(user)

        emailService.sendForgotPassword(user, requestIp)
        log.info("Reset password for user [${user.id}]")
        log.debug("User ${user.id} is ${user.email}")
    }

    fun acceptInvitation(user: User, organizationId: OrganizationPublicId): User {
        val invitedOrgUser = user.organization_users
            .filter { it.status == UserStatus.INVITED }
            .find { it.organization?.publicId == organizationId } ?: throw OrganizationUserNotFound(user.email, organizationId)

        val invitation = user.invitations
            .find { it.organization.publicId == organizationId && it.status == InvitationStatus.OPEN } ?: throw OrganizationUserNotFound(user.email, organizationId)

        invitedOrgUser.status = UserStatus.ACTIVE
        invitation.status = InvitationStatus.ACCEPTED
        invitation.acceptedAt = ZonedDateTime.now()
        organizationUserRepository.save(invitedOrgUser)
        invitationRepository.save(invitation)

        userRepository.refresh(user)

        return user
    }

    /*** Create Users ***/
    fun createUserNoInvitation(
        email: String,
        password: String,
        firstName: String,
        lastName: String,
        organization: Organization,
        role: UserRole,
        status: UserStatus = UserStatus.ACTIVE,
        globalRole: GlobalRole? = null
    ): User {
        val (user) = createUser(email, password, firstName, lastName, organization, role, status, globalRole, sendInvitation = false)
        return user
    }

    fun inviteUser(
        email: String,
        firstName: String,
        lastName: String,
        organizationId: OrganizationPublicId,
        inviter: User? = null
    ): OrganizationUser {
        if (email.isNullOrBlank()) throw IllegalArgumentException("Email cannot be blank")

        val organization = lookupOrganizationOrThrow(organizationId)
        val user = findByEmail(email)
        val organizationUser = lookupOrganizationUser(email, organizationId)
        val status = organizationUser?.status

        // possible invitation scenarios:

        // 1 - organization user already exists in ACTIVE state
        // reject w/ specific error message
        if (status.isActive()) {
            throw OrganizationUserAlreadyExists("The user '$email' already exists for organization '$organizationId'.")
        }

        // 2 - organization user already exists in INACTIVE state
        // reject w/ specific error message
        if (status.isInactive()) {
            throw OrganizationUserAlreadyExists("The user '$email' exists but has been deactivated for organization '$organizationId'. Please activate the user instead of inviting them.")
        }

        // 2.5 - user does not exist, but the email address is in use by a privacy center
        // reject w/ non-specific error message
        if (privacyCenterService.findByEmailAddress(email) != null) {
            throw OrganizationUserAlreadyExists("The user '$email' is in use as part of a privacy center. Please use a different address.")
        }

        // 3 - organization user already exists in INVITED state
        // potentially resend the invitation email
        if (status.isInvited()) {
            // TODO: Future task to resend invitation emails
            return organizationUser!!
        }

        // 4 - organization user does not exist, user already exists
        // add the user as INVITED and send the invitation email
        if (user != null && organizationUser == null) {
            return inviteUserToOrganization(user, organization, UserRole.ADMIN, UserStatus.INVITED, sendInvitation = true)
        }

        // 5 - user does not exist
        // create the user, setup a password reset, send the user created email
        if (user == null) {
            val (_, createdOrganizationUser) = createUser(email, firstName = firstName, lastName = lastName, password = null, organization = organization, role = UserRole.ADMIN, status = UserStatus.INVITED, sendInvitation = true)
            return createdOrganizationUser
        }

        throw OrganizationUserNotFound(email, organizationId)
    }

    /*** Helpers ***/

    private fun lookupOrganizationOrThrow(organizationId: OrganizationPublicId) = organizationRepository.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)

    private fun lookupOrganizationUser(email: String, organizationId: OrganizationPublicId): OrganizationUser? {
        val organization = organizationRepository.findByPublicId(organizationId) ?: return null
        val user = findByEmail(email) ?: return null
        return organizationUserRepository.findByUserAndOrganization(user, organization)
    }

    private fun lookupOrganizationUserOrThrow(email: String, organizationId: OrganizationPublicId) =
        lookupOrganizationUser(email, organizationId) ?: throw OrganizationUserNotFound(email, organizationId)

    fun createUser(
        email: String,
        password: String?,
        firstName: String,
        lastName: String,
        organization: Organization,
        role: UserRole,
        status: UserStatus = UserStatus.ACTIVE,
        globalRole: GlobalRole? = null,
        sendInvitation: Boolean
    ): Pair<User, OrganizationUser> {
        val resetToken = if (password == null) { UUID.randomUUID() } else { null }

        val user = User(
            email = email.toLowerCase(),
            password = password?.let { passwordEncoder.encode(it) },
            firstName = firstName,
            lastName = lastName,
            globalRole = globalRole,
            updatePasswordId = resetToken
        )

        val savedUser = userRepository.save(user)
        val organizationUser = inviteUserToOrganization(user, organization, role, status, sendInvitation)
        userRepository.refresh(savedUser)

        return Pair(savedUser, organizationUser)
    }

    fun inviteUserToOrganization(
        user: User,
        organization: Organization,
        role: UserRole,
        status: UserStatus,
        sendInvitation: Boolean
    ): OrganizationUser {
        val organizationUser = OrganizationUser(user = user, organization = organization, role = role, status = status)
        val savedOrganizationUser = organizationUserRepository.save(organizationUser)

        val invitation = Invitation(user = user, organization = organization, status = InvitationStatus.OPEN)
        val savedInvitation = invitationRepository.save(invitation)

        if (sendInvitation) {
            emailService.sendUserInvite(user, savedOrganizationUser, savedInvitation)
        }

        return organizationUser
    }

    fun associateUserToOrganization(
        user: User,
        organization: Organization,
        role: UserRole,
        status: UserStatus = UserStatus.ACTIVE
    ): Pair<User, OrganizationUser> {
        // TODO Check for organization existing on user
        val organizationUser = OrganizationUser(user = user, organization = organization, role = role, status = status)
        val savedOrganizationUser = organizationUserRepository.save(organizationUser)
        userRepository.refresh(user)
        return Pair(user, savedOrganizationUser)
    }
}
