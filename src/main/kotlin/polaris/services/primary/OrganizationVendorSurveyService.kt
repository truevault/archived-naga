package polaris.services.primary

import org.springframework.stereotype.Service
import polaris.models.SurveySlugs
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.SELLING_AND_SHARING_SURVEY_NAME
import polaris.models.entity.vendor.KnownVendorCategories
import polaris.models.entity.vendor.KnownVendorKey
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_CONTRACTOR
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER
import polaris.models.entity.vendor.Vendor
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.VendorClassificationRepository

@Service
class OrganizationVendorSurveyService(
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val serviceProviderRecommendationService: ServiceProviderRecommendationService,
    private val vendorClassificationRepository: VendorClassificationRepository
) {

    fun sellsTo(org: Organization, vendor: Vendor): Boolean {
        return sellingDataRecipients(org).any { it.vendor?.id == vendor.id }
    }

    fun sellingDataRecipients(org: Organization): List<OrganizationDataRecipient> {
        val sellingVendorsQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, "vendor-selling-selection")
        val selectedSellingVendorIds = sellingVendorsQuestion?.parsedAnswerList() ?: emptyList()

        if (selectedSellingVendorIds.isEmpty()) {
            return emptyList()
        }

        return org.organizationDataRecipients.filter { selectedSellingVendorIds.contains(it.id.toString()) }
    }

    fun intentionallyInteractsWith(org: Organization, vendor: Vendor): Boolean {
        return intentionalInteractionDataRecipients(org).any { it.vendor?.id == vendor.id }
    }

    fun intentionalInteractionDataRecipients(org: Organization): List<OrganizationDataRecipient> {
        val intentionalInteractionSelection = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, "vendor-intentional-disclosure-selection")
        val intentionalInteractionsSelectedIds = intentionalInteractionSelection?.parsedAnswerList() ?: emptyList()

        if (intentionalInteractionsSelectedIds.isEmpty()) {
            return emptyList()
        }

        return org.organizationDataRecipients.filter { intentionalInteractionsSelectedIds.contains(it.id.toString()) }
    }

    fun getPotentialSellingVendors(org: Organization): List<OrganizationDataRecipient> {
        return org.organizationDataRecipients.filter { isPotentialSellingVendor(org, it) }
    }

    fun isPotentialSellingVendor(org: Organization, dataRecipient: OrganizationDataRecipient): Boolean {

        if (dataRecipient.vendor == null) {
            return false
        }

        if (dataRecipient.vendorClassification?.slug == VENDOR_CLASSIFICATION_SLUG_CONTRACTOR) {
            return false
        }

        if (isGoogleAnalyticsWithDataSharing(org, dataRecipient) || isShopifyWithAudiencesEnabled(org, dataRecipient)) {
            return true
        }

        if (intentionallyInteractsWith(org, dataRecipient.vendor)) {
            return false
        }

        val classificationOptions = vendorClassificationRepository.findAll()
        val defaultClassification = serviceProviderRecommendationService.getDefaultClassification(dataRecipient, classificationOptions)

        if (defaultClassification?.slug == VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER) {
            return false
        }

        if (dataRecipient.vendor.isKnownCategory(KnownVendorCategories.AD_NETWORK)) {
            return false
        }

        return true
    }

    fun isGoogleAnalyticsWithDataSharing(org: Organization): Boolean {
        val googleAnalytics = org.organizationDataRecipients.find { isGoogleAnalytics(it) }
        return googleAnalytics != null && isGoogleAnalyticsWithDataSharing(org, googleAnalytics)
    }

    fun isGoogleAnalyticsWithDataSharing(org: Organization, dataRecipient: OrganizationDataRecipient): Boolean {
        if (dataRecipient.vendor?.isKnownVendor(KnownVendorKey.GOOGLE_ANALYTICS) == true) {
            val gaDataSharingQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, "google-analytics-data-sharing")
            val gaDataSharingOff = gaDataSharingQuestion?.isTrue() ?: false
            return !gaDataSharingOff
        }

        return false
    }

    fun isOnlyGoogleAnalyticsWithNoDataSharing(org: Organization): Boolean {
        val googleAnalytics = org.organizationDataRecipients.find { isGoogleAnalytics(it) }
        val googleWithDataSharing = googleAnalytics != null && isGoogleAnalyticsWithDataSharing(org, googleAnalytics)

        val vendorSellingSelectionQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(
            org, SELLING_AND_SHARING_SURVEY_NAME,
            SurveySlugs.VENDOR_SELLING_QUESTION_SLUG
        )
        val selectedSellingVendorIds = vendorSellingSelectionQuestion?.parsedAnswerList() ?: emptyList()
        val onlySellsToGoogleAnalytics = selectedSellingVendorIds.size == 1 && selectedSellingVendorIds.contains(googleAnalytics?.id?.toString())

        return googleAnalytics != null && !googleWithDataSharing && onlySellsToGoogleAnalytics
    }

    fun isShopifyWithAudiencesEnabled(org: Organization): Boolean {
        val shopify = org.organizationDataRecipients.find { isShopify(it) }
        return shopify != null && isShopifyWithAudiencesEnabled(org, shopify)
    }

    fun isShopifyWithAudiencesEnabled(org: Organization, dataRecipient: OrganizationDataRecipient): Boolean {
        if (dataRecipient.vendor?.isKnownVendor(KnownVendorKey.SHOPIFY) == true) {
            val shopifyAudienceQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(
                org,
                SELLING_AND_SHARING_SURVEY_NAME,
                "shopify-audiences"
            )

            return shopifyAudienceQuestion?.isTrue() ?: false
        }

        return false
    }

    fun isAutomaticSellingVendor(org: Organization, dataRecipient: OrganizationDataRecipient): Boolean {
        return isPotentialSellingVendor(org, dataRecipient) && (isGoogleAnalyticsWithDataSharing(org, dataRecipient) || isShopifyWithAudiencesEnabled(org, dataRecipient))
    }

    fun isSelectedSellingVendor(org: Organization, dataRecipient: OrganizationDataRecipient): Boolean {
        val sellingVendorsQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, "vendor-selling-selection")
        val selectedSellingVendorIds = sellingVendorsQuestion?.parsedAnswerList() ?: emptyList()

        return isPotentialSellingVendor(org, dataRecipient) && selectedSellingVendorIds.contains(dataRecipient.id.toString())
    }

    fun isPotentialUploadVendor(org: Organization, dataRecipient: OrganizationDataRecipient): Boolean {
        return isSelectedSellingVendor(org, dataRecipient)
    }

    fun isAutomaticUploadVendor(org: Organization, dataRecipient: OrganizationDataRecipient): Boolean {
        return isSelectedSellingVendor(org, dataRecipient) && isShopifyWithAudiencesEnabled(org, dataRecipient)
    }

    fun isSelectedUploadVendor(org: Organization, dataRecipient: OrganizationDataRecipient): Boolean {
        val sellingVendorsQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(org, SELLING_AND_SHARING_SURVEY_NAME, "vendor-upload-data-selection")
        val selectedUploadVendorIds = sellingVendorsQuestion?.parsedAnswerList() ?: emptyList()

        return isPotentialUploadVendor(org, dataRecipient) && selectedUploadVendorIds.contains(dataRecipient.id.toString())
    }

    // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
    // private fun isGoogleAds(dataRecipient: OrganizationDataRecipient): Boolean = dataRecipient.vendor?.isKnownVendor(KnownVendorKey.GOOGLE_ADS) ?: false
    fun isGoogleAnalytics(dataRecipient: OrganizationDataRecipient): Boolean = dataRecipient.vendor?.isKnownVendor(KnownVendorKey.GOOGLE_ANALYTICS) ?: false
    fun isShopify(dataRecipient: OrganizationDataRecipient): Boolean = dataRecipient.vendor?.isKnownVendor(KnownVendorKey.SHOPIFY) ?: false

    // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
    // private fun isFacebookAds(dataRecipient: OrganizationDataRecipient): Boolean = dataRecipient.vendor?.isKnownVendor(KnownVendorKey.FACEBOOK_ADS) ?: false
}
