package polaris.services.primary

import freemarker.template.Configuration
import org.springframework.stereotype.Service
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils
import polaris.models.OrganizationPublicId
import polaris.models.dto.compliance.BusinessPurposeDto
import polaris.models.dto.compliance.DataInventorySnapshotDto
import polaris.models.dto.compliance.FullDataMapDto
import polaris.models.dto.compliance.SnapshotCategoryDto
import polaris.models.dto.privacycenter.SellingAndSharingDto
import polaris.models.entity.compliance.PICSurveyClassification
import polaris.models.entity.organization.BUSINESS_SURVEY_NAME
import polaris.models.entity.organization.BusinessPurposeType
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.EEA_UK_SURVEY_NAME
import polaris.models.entity.organization.EMPLOYEE_COLLECTION_PURPOSES_SURVEY
import polaris.models.entity.organization.GDPR_LAWFUL_BASES_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.repositories.OrganizationBusinessPurposeRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.services.support.DataMapService
import polaris.services.support.OptOutAttributes
import polaris.services.support.OptOutService
import polaris.util.PolarisException
import polaris.util.PrivacyNoticeUtil
import polaris.util.StringUtil
import polaris.util.sorting.CategoryDtoSort
import java.util.*
import kotlin.Comparator

const val SERVICE_PROVIDER_LABEL = "Service Providers"

@Service
class PrivacyNoticeService(
    private val collectionGroupService: CollectionGroupService,
    private val organizationService: OrganizationService,
    private val recipientRepo: OrganizationDataRecipientRepository,
    private val businessPurposeRepo: OrganizationBusinessPurposeRepository,
    private val privacyCenterService: PrivacyCenterService,
    private val personalInformationService: PersonalInformationService,
    private val optOutService: OptOutService,
    private val freemarkerConfig: Configuration,
    private val dataMapService: DataMapService
) {

    private fun getOrderedGroupsFromSnapshot(categories: List<SnapshotCategoryDto>) =
        categories
            .asSequence()
            .map {
                Triple(it.groupOrder, it.groupDisplayOrder, it.groupName)
            }
            .toSet()
            .sortedWith(Comparator<Triple<Int, Int?, String>> { t, t2 -> t.first.compareTo(t2.first) }.thenBy { it.second })
            .map { it.third }
            .toList()

    private fun getSortedCategoryGroups(categories: List<SnapshotCategoryDto>): Map<String, List<SnapshotCategoryDto>> {
        val orderedGroups = getOrderedGroupsFromSnapshot(categories)
        val groupedPic = categories.sortedBy { it.groupDisplayOrder }
            .groupBy { it.groupName }
        return groupedPic.toSortedMap(compareBy { orderedGroups.indexOf(it) })
    }

    private fun collectCategoriesHtml(piSnapshot: DataInventorySnapshotDto): String {
        val groupedCategories = getSortedCategoryGroups(piSnapshot.personalInformationCategories)
        val categoryLines = groupLabels(groupedCategories)
        return categoryLines.joinToString("\n") { "<li>$it</li>" }
    }

    private fun groupLabels(groups: Map<String, List<SnapshotCategoryDto>>): List<String> {
        return groups.map { (group, categories) -> groupLabel(group, categories) }
    }

    private fun groupLabel(group: String, categories: List<SnapshotCategoryDto>): String {
        val categoryNames = categories
            .sortedWith(compareBy<SnapshotCategoryDto> { it.groupOrder }.thenBy { it.groupDisplayOrder })
            .map {
                if (it.alwaysPreserveCasing) it.name else it.name.decapitalize()
            }

        return if (categories.size == 1 && group == categories[0].name) categories[0].name else "$group, including ${
        StringUtil.joinNatural(categoryNames)
        }"
    }

    private fun collectedPurposesHtml(businessPurposes: List<BusinessPurposeDto>): String {
        return businessPurposes
            .sortedBy { it.displayOrder }.joinToString("\n") {
                "<li>${it.name}</li>"
            }
    }

    private fun templatedBody(templateName: String, model: Map<String, Any?>): String {
        return FreeMarkerTemplateUtils.processTemplateIntoString(
            freemarkerConfig.getTemplate(templateName), model
        )
    }

    fun getPrivacyCenterSellingAndSharing(organizationId: OrganizationPublicId): SellingAndSharingDto {
        val pc = privacyCenterService.findAllByOrganization(organizationId).firstOrNull()
            ?: throw PolarisException("Privacy center not found for organization $organizationId")

        val org = pc.organization!!
        val isDirectSelling = optOutService.processesOptOut(org)
        val optOutAttrs = optOutService.getOptOutAttributes(org)

        return SellingAndSharingDto(
            doNotSellHtml = rawGetOptOutPrivacyNoticeHtml(organizationId),
            selling = optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney,
            sharing = optOutAttrs.anySharing,
            enableIbaBrowserOptOut = buildBrowserOptOut(optOutAttrs, isDirectSelling),
            enableIbaDirectOptOut = buildDirectOptOut(isDirectSelling)
        )
    }

    fun getPrivacyCenterRightToLimitHtml(organizationId: OrganizationPublicId, privacyCenter: PrivacyCenter): String {
        val org = privacyCenter.organization!!
        val sensitiveCategories = dataMapService.getAllCollectedPic(org).map { it.pic!! }.distinct()
            .filter { it.getClassification() == PICSurveyClassification.SENSITIVE }
            .map { it.name!! }
        val sensitiveCollection = sensitiveCategories.mapIndexed { index, s -> if (index > 0 && index == sensitiveCategories.size - 1) "and $s" else s }.joinToString(", ")

        return templatedBody(
            "privacy/right-to-limit-notice.ftl",
            mapOf(
                "privacyRequestEmail" to privacyCenter.emailWithFallback(),
                "sensitiveCollection" to sensitiveCollection
            )
        )
    }

  /*
  | selling | sharing | cookieOnly | browser |
  | ------- | ------- | ---------- | ------- |
  | T       | T       | T          | TRUE    |
  | T       | T       | F          | --FALSE-- TRUE | Temporarily changed, per POLARIS-837 see note below
  | T       | F       | T          | FALSE   |
  | T       | F       | F          | FALSE   |
  | F       | T       | T          | TRUE    |
  | F       | T       | F          | TRUE    |
  | F       | F       | T          | N/A     |
  | F       | F       | F          | N/A     |
  */
    fun buildBrowserOptOut(attrs: OptOutAttributes, isDirectSelling: Boolean): Boolean {
        // Temporary change for https://truevault.atlassian.net/browse/POLARIS-837:
        // We are temporarily treating all cases as if any cookie sharing was not true, while the
        // direct sharing question has been reverted
        // (POLARIS-785)
        return attrs.anySharing || (attrs.anySelling && !isDirectSelling)
    }

  /*

  w/direct flag
  | selling | sharing | cookieOnly | direct |
  | ------- | ------- | ---------- | ------ |
  | T       | T       | T          | TRUE   |
  | T       | T       | F          | TRUE   |
  | T       | F       | T          | TRUE   |
  | T       | F       | F          | TRUE   |
  | F       | T       | T          | --FALSE-- TRUE | Temporarily changed, per POLARIS-837 see note below
  | F       | T       | F          | TRUE   |
  | F       | F       | T          | TRUE   |
  | F       | F       | F          | TRUE   |

   */
    fun buildDirectOptOut(isDirectSelling: Boolean): Boolean {
        // Temporary change for https://truevault.atlassian.net/browse/POLARIS-837:
        // We are temporarily treating all cases as if any cookie sharing was not true, while the
        // direct sharing question has been reverted
        // (POLARIS-785)

        // val notDirect = (!anySelling && anySharing && anyCookieSharing)
        // val notDirect = (!attrs.anySelling && attrs.anySharing)
        return isDirectSelling
    }

    fun getOptOutPrivacyNoticeHtml(organizationId: OrganizationPublicId): String {
        val sellingAndSharing = getPrivacyCenterSellingAndSharing(organizationId)
        return sellingAndSharing.doNotSellHtml
    }

    fun getDefaultCustomOptOutIntroText(privacyCenter: PrivacyCenter): String {
        val optOutAttrs = optOutService.getOptOutAttributes(privacyCenter.organization!!)
        return getDefaultCustomOptOutIntroText(optOutAttrs)
    }

    fun getDefaultCustomOptOutIntroText(optOutAttrs: OptOutAttributes): String {
        val anySelling = optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney
        val anySharing = optOutAttrs.anySharing

        return if (anySharing && !anySelling)
            "We share personal information with third party vendors to provide more relevant, personalized ads to consumers."
        else if (!anySharing) {
            "We use third party data analytics providers whose collection of information may be considered a “sale” of information under some privacy laws."
        } else """
            We share personal information with third party vendors to provide more relevant, personalized ads to consumers.
            We also use third party data analytics providers whose collection of information may be considered a “sale” of information under some privacy laws.
        """.trimIndent()
    }

    fun getStaticOptOutIntroText(pc: PrivacyCenter, optOutAttrs: OptOutAttributes): String {
        val anySelling = optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney
        val anySharing = optOutAttrs.anySharing

        val sellingSharingLabel = if (anySharing && !anySelling) {
            "sharing"
        } else if (!anySharing) {
            "selling"
        } else {
            "sharing and selling"
        }

        return """
            To opt-out of $sellingSharingLabel, submit a request below.
            A consumer or their authorized agent may instead submit a Request to Opt-Out via email to <strong>${pc.emailWithFallback()}</strong>.
        """.trimIndent()
    }

    private fun rawGetOptOutPrivacyNoticeHtml(organizationId: OrganizationPublicId): String {
        val pc = privacyCenterService.findAllByOrganization(organizationId).firstOrNull()
            ?: throw PolarisException("Privacy center not found for organization $organizationId")

        val optOutAttrs = optOutService.getOptOutAttributes(pc.organization!!)
        val anySelling = optOutAttrs.anySelling || optOutAttrs.isSellingInExchangeForMoney
        val anySharing = optOutAttrs.anySharing

        if (!anySelling && !anySharing) {
            return ""
        }

        val defaultCustomText = { getDefaultCustomOptOutIntroText(optOutAttrs) }
        val staticText = getStaticOptOutIntroText(pc, optOutAttrs)

        if (anySharing && !anySelling) {
            return """
        <h2>Do Not Share My Personal Information</h2>
        ${pc.optOutIntro(defaultCustomText)}
        <p>
          $staticText
        </p>
        <p>
          <em>Please note that some third party vendors do not respect the
          <a style="color: #007BBD" href="https://iabtechlab.com/standards/ccpa/" rel="nofollow">U.S. Privacy String</a>, which is the mechanism we use to
          communicate your Request to Opt-Out to third parties that collect information via cookies on our website.
          If you choose, you may also use the <a style="color: #007BBD" href="http://www.aboutads.info/choices" rel="nofollow">Digital Advertising Alliance
          (DAA) WebChoices Tool</a> to globally opt-out of third-party tracking via website cookies.</em>
        </p>
            """.trimIndent()
        }

        if (!anySharing) {
            return """
        <h2>Do Not Sell My Personal Information</h2>
        ${pc.optOutIntro(defaultCustomText)}
        <p>
          $staticText
        </p>
            """.trimIndent()
        }

        return """
      <h2>Do Not Sell or Share My Personal Information</h2>
      ${pc.optOutIntro(defaultCustomText)}
      <p>
        $staticText
      </p>
      <p>
        <em>Please note that some third party vendors do not respect the
        <a style="color: #007BBD" href="https://iabtechlab.com/standards/ccpa/" rel="nofollow">U.S. Privacy String</a>, which is the mechanism we use to
        communicate your Request to Opt-Out to third parties that collect information via cookies on our website.
        If you choose, you may also use the <a style="color: #007BBD" href="http://www.aboutads.info/choices" rel="nofollow">Digital Advertising Alliance
        (DAA) WebChoices Tool</a> to globally opt-out of third-party tracking via website cookies.</em>
      </p>
        """.trimIndent()
    }

    private fun collectionGroupLabel(group: CollectionGroup) =
        collectionGroupLabel(name = group.name, description = group.description)

    private fun collectionGroupLabel(name: String, description: String?): String {
        return if (description == null || description.isBlank()) {
            name
        } else {
            description
        }
    }

    fun getEmployeeCollectionGroupNoticeIntro(organization: Organization, group: CollectionGroup): String = collectionGroupService.getEmployeeCollectionGroupNoticeIntro(organization, group)

    fun getEmployeeCollectionGroupNoticeHtml(organizationId: OrganizationPublicId, collectionGroupId: UUID): String {

        val organization = organizationService.findByPublicId(organizationId)
        val map = dataMapService.getFullDataMap(organization)
        val collectionGroup = collectionGroupService.findByIdAndOrganization(collectionGroupId, organization)

        val businessSurveyQuestions = organizationService.getSurveyQuestions(organizationId, BUSINESS_SURVEY_NAME)

        val physicalLocation =
            businessSurveyQuestions.any { it.slug == "business-physical-location" && it.answer == "true" }

        val dataRecipients = recipientRepo.findAllByOrganization(organization)
        val recipientsByVendor = dataRecipients.associateBy { it.vendor!!.id }

        val piSnapshot = personalInformationService.getOrganizationSnapshot(organizationId).dataInventory
            .find { it.dataSubjectTypeId == collectionGroupId.toString() }
            ?: return ""

        val pc = privacyCenterService.findAllByOrganization(organizationId).firstOrNull()
            ?: return ""

        val collectionSources = PrivacyNoticeUtil.sourcesListFromDataMap(map, CollectionContext.EMPLOYEE)

        val employeeSurveyResponses = organizationService.getSurveyQuestions(organizationId, EMPLOYEE_COLLECTION_PURPOSES_SURVEY)
        val isCommercial = employeeSurveyResponses.any { q -> q.slug == "does-collect-$collectionGroupId" && q.isTrue() }

        val cgDisclosures = map.disclosure.filter { it.collectionGroupId == collectionGroupId.toString() }.filter { it.disclosed.isNotEmpty() }
        val disclosureCategories = PrivacyNoticeUtil.getDisclosureCategories(cgDisclosures, map, recipientsByVendor)

        val sellingDisclosures = cgDisclosures.filter { d -> recipientsByVendor[UUID.fromString(d.vendorId)]?.ccpaIsSelling ?: false }

        val sharedDisclosures = cgDisclosures.filter { d -> recipientsByVendor[UUID.fromString(d.vendorId)]?.ccpaIsSharing ?: false }

        val sellingCategories = sellingDisclosures
            .flatMap { it.disclosed.map { id -> map.categories[id]!! } }
            .map { map.picGroups[it.picGroupId]!! }
            .map { it.name }
            .distinct()

        val sharingCategories = sharedDisclosures
            .flatMap { it.disclosed.map { id -> map.categories[id]!! } }
            .map { map.picGroups[it.picGroupId]!! }
            .map { it.name }
            .distinct()

        // This is ... silly ... to say the least; what we want is to always show
        // Employment-Related in the purposes list, and to show it first. But because
        // the user can explicitly select "Employment-Related" on the previous screen,
        // we need to make sure we don't double render it.
        //
        // So, we take it out of the list if it is already there, then hardcode
        // the item when we render out the HTML below.
        val purposes = piSnapshot.businessPurposes.filter { it.name != "Employment-Related" }
        val model = mapOf(
            "consumerGroup" to collectionGroup,
            "organization" to organization,
            "collectCategories" to collectCategoriesHtml(piSnapshot),
            "collectionSources" to StringUtil.joinNatural(collectionSources),
            "employmentRelated" to collectedPurposesHtml(purposes),
            "isCommercial" to isCommercial,
            "disclosureCategories" to disclosureCategories,
            "isSelling" to !sellingDisclosures.isEmpty(),
            "isSharing" to !sharedDisclosures.isEmpty(),
            "sellingCategories" to StringUtil.joinNatural(sellingCategories),
            "sharingCategories" to StringUtil.joinNatural(sharingCategories),
            "hasPhysicalLocation" to physicalLocation,
            "privacyRequestTollFreeNumber" to pc.requestTollFreePhoneNumber,
            "privacyPolicyUrl" to pc.privacyPolicyUrl,
            "privacyRequestUrl" to pc.getPrivacyRequestUrl(),
            "privacyRequestEmail" to pc.emailWithFallback(),
            "privacyNoticeIntroText" to getEmployeeCollectionGroupNoticeIntro(organization, collectionGroup),
            "optOutUrl" to optOutService.getOptOutUrl(pc),
            "customMessage" to organization.employeePrivacyNoticeCustom,
            "retentionPeriod" to organization.employeePrivacyNoticeRetentionPeriod
        )

        return templatedBody("privacy/employee-collection-group-notice-ccpa.ftl", model)
    }

    fun getEeaUkEmployeeNoticeHtml(org: Organization, group: CollectionGroup): String {
        val map = dataMapService.getFullDataMap(org)
        return getEeaUkEmployeeNoticeHtml(org, group, map)
    }

    fun getEeaUkEmployeeNoticeHtml(org: Organization, collectionGroup: CollectionGroup, map: FullDataMapDto): String {
        if (!org.isGdpr()) {
            return ""
        }

        val pc = org.privacyCenter()!!

        val purposes = businessPurposeRepo.findAllByOrganizationAndBusinessPurposeType(org, BusinessPurposeType.EMPLOYMENT)
        val eeaUkSurvey = organizationService.getSurveyQuestions(org.publicId, EEA_UK_SURVEY_NAME)
        val lawfulBasesSurvey = organizationService.getSurveyQuestions(org.publicId, GDPR_LAWFUL_BASES_SURVEY_NAME)

        // collection information
        val collection = map.collection.find { it.collectionGroupId == collectionGroup.id.toString() }

        val collectedPic = collection?.collected?.mapNotNull { map.categories[it] }?.sortedWith(CategoryDtoSort) ?: emptyList()
        val collectionLabel = StringUtil.joinNatural(collectedPic.map { it.name })

        // disclosure information
        val dataRecipients = recipientRepo.findAllByOrganization(org)
        val recipientsByVendor = dataRecipients.associateBy { it.vendor!!.id }
        val disclosureCategories = map.collectionGroups.values.filter { it.id == collectionGroup.id.toString() }.flatMap { cg ->
            // first get all disclosures for the group
            val cgDisclosures = map.disclosure.filter { it.collectionGroupId == cg.id }

            cgDisclosures.asSequence().map { d -> d.vendorId }
                .map { recipientsByVendor[UUID.fromString(it)] }
                .map { PrivacyNoticeUtil.getRecipientCategoryLabel(it) }
                .distinct()
                .sorted().toList()
        }

        val purposeLabels = purposes.map { p -> p.businessPurpose }.distinct().mapNotNull { p -> p?.name }.sorted()

        val establishedInEeaUk = eeaUkSurvey.any { it.slug == "established-in-eea-uk" && it.answer == "true" }
        val nonEnglishWebsite = eeaUkSurvey.any { it.slug == "non-english-website" && it.answer == "true" }
        val automatedDecisionMaking =
            lawfulBasesSurvey.any { it.slug == "automated-decision-making" && it.answer == "true" }

        val collectionSources = PrivacyNoticeUtil.sourcesListFromDataMap(map, CollectionContext.EMPLOYEE)

        val model = mapOf(
            "orgName" to org.name,
            "collectionLabel" to collectionLabel,
            "collectionSources" to StringUtil.joinNatural(collectionSources),
            "disclosureCategories" to StringUtil.joinNatural(disclosureCategories),
            "purposeLabels" to StringUtil.joinNatural(purposeLabels),
            "isAutomatedDecisionMaking" to automatedDecisionMaking,
            "isInternationalDataTransfers" to (establishedInEeaUk || nonEnglishWebsite),
            "privacyRequestUrl" to (pc.getPrivacyRequestUrl() ?: "null"),
            "privacyNoticeIntroText" to (collectionGroupService.getEmployeeCollectionGroupNoticeIntro(org, collectionGroup)),
            "dpoName" to pc.dpoOfficerName,
            "dpoEmail" to pc.dpoOfficerEmail,
            "privacyRequestEmail" to pc.emailWithFallback()
        )

        return templatedBody("privacy/employee-eea-uk-privacy-notice.ftl", model)
    }
}
