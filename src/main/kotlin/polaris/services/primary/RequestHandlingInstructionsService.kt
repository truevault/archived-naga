package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.UUIDString
import polaris.models.dto.organization.RequestHandlingInstructionDto
import polaris.models.dto.organization.UpdateOrganizationCGInstructionsDto
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.RequestHandlingInstruction
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod
import polaris.models.entity.vendor.Vendor
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.RequestHandlingInstructionsRepository
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class RequestHandlingInstructionsService(
    private val requestHandlingInstructionsRepository: RequestHandlingInstructionsRepository,
    private val organizationRepository: OrganizationRepository,
    private val organizationDataRecipientRepository: OrganizationDataRecipientRepository,
    private val lookup: LookupService
) {
    companion object {
        private val log = LoggerFactory.getLogger(RequestHandlingInstructionsService::class.java)
    }

    fun findAllByOrganization(organizationId: OrganizationPublicId): List<RequestHandlingInstruction> {
        val organization = lookupOrganizationOrThrow(organizationId)
        return requestHandlingInstructionsRepository.findAllByOrganization(organization)
    }

    fun findAllByOrganizationAndType(
        organizationId: OrganizationPublicId,
        instructionType: DataRequestInstructionType
    ): List<RequestHandlingInstruction> {
        val organization = lookupOrganizationOrThrow(organizationId)
        return requestHandlingInstructionsRepository.findAllByOrganizationAndRequestType(organization, instructionType)
    }

    fun findAllByOrganizationAndRequestTypeAndProcessingMethod(
        organizationId: OrganizationPublicId,
        instructionType: DataRequestInstructionType,
        processingMethod: ProcessingMethod
    ): List<RequestHandlingInstruction> {
        val organization = lookupOrganizationOrThrow(organizationId)
        return requestHandlingInstructionsRepository.findAllByOrganizationAndRequestTypeAndProcessingMethod(
            organization,
            instructionType,
            processingMethod
        )
    }

    fun createOrDoNothing(
        organizationId: OrganizationPublicId,
        dto: RequestHandlingInstructionDto
    ): RequestHandlingInstruction? {
        val organization = lookup.orgOrThrow(organizationId)
        val vendor = lookup.vendor(vendorId = UUID.fromString(dto.vendorId))
        val organizationVendor = if (vendor == null) null else lookupOrganizationVendor(organization, vendor)
        if (findInstruction(dto.requestType, organization, organizationVendor) != null) {
            return null
        }
        if (dto.processingMethod == ProcessingMethod.DELETE || dto.processingMethod == ProcessingMethod.INACCESSIBLE_OR_NOT_STORED) {
            dto.retentionReasons = emptyList()
        }
        val toSave = RequestHandlingInstruction(
            organization = organization,
            organizationDataRecipient = organizationVendor,
            requestType = dto.requestType,
            processingMethod = dto.processingMethod,
            exceptionsAffirmed = dto.exceptionsAffirmed,
            retentionReasons = dto.retentionReasons?.toMutableList() ?: mutableListOf(),
            processingInstructions = dto.processingInstructions,
            deleteDoNotContact = dto.deleteDoNotContact,
            hasRetentionExceptions = dto.hasRetentionExceptions,
            deletedPicIds = dto.deletedPIC?.toMutableList() ?: mutableListOf()
        )
        return requestHandlingInstructionsRepository.save(toSave)
    }

    fun createOrUpdate(
        organizationId: OrganizationPublicId,
        dto: RequestHandlingInstructionDto,
        deleteOnNull: Boolean = false,
        deleteProcessingMethodOnNull: Boolean = false
    ): RequestHandlingInstruction {
        val organization = lookup.orgOrThrow(organizationId)
        val vendor = lookup.vendor(vendorId = UUID.fromString(dto.vendorId))
        val organizationVendor = if (vendor == null) null else lookupOrganizationVendor(organization, vendor)
        var existing = findInstruction(dto.requestType, organization, organizationVendor)

        if (dto.processingMethod == ProcessingMethod.DELETE || dto.processingMethod == ProcessingMethod.INACCESSIBLE_OR_NOT_STORED) {
            dto.retentionReasons = emptyList()
        }

        val defaultReasons =
            organizationVendor?.vendor?.defaultRetentionReasons?.map { it.slug }?.toMutableList() ?: mutableListOf()

        if (existing != null) {
            val existingProcessingMethod =
                if (deleteOnNull || deleteProcessingMethodOnNull) null else existing.processingMethod
            val existingRetentionReasons = if (deleteOnNull) null else existing.retentionReasons
            val existingProcessingInstructions = if (deleteOnNull) null else existing.processingInstructions
            val existingDeleteDoNotContact = if (deleteOnNull) null else existing.deleteDoNotContact
            val existingDoNotContactReason = if (deleteOnNull) null else existing.doNotContactReason
            val existingDoNotContactExplanation = if (deleteOnNull) null else existing.doNotContactExplanation
            val existingHasRetentionExceptions = if (deleteOnNull) null else existing.hasRetentionExceptions
            val existingDeletedPic = if (deleteOnNull) mutableListOf() else existing.deletedPicIds

            val fallbackReasons =
                if (existingRetentionReasons.isNullOrEmpty()) defaultReasons else existingRetentionReasons

            existing.processingMethod = dto.processingMethod ?: existingProcessingMethod
            existing.retentionReasons = dto.retentionReasons?.toMutableList() ?: fallbackReasons
            existing.processingInstructions = dto.processingInstructions ?: existingProcessingInstructions
            existing.deleteDoNotContact = dto.deleteDoNotContact ?: existingDeleteDoNotContact
            existing.doNotContactReason = dto.doNotContactReason ?: existingDoNotContactReason
            existing.doNotContactExplanation = dto.doNotContactExplanation ?: existingDoNotContactExplanation
            existing.hasRetentionExceptions = dto.hasRetentionExceptions ?: existingHasRetentionExceptions
            existing.deletedPicIds = dto.deletedPIC?.toMutableList() ?: existingDeletedPic
            existing.exceptionsAffirmed = dto.exceptionsAffirmed
        } else {
            existing = RequestHandlingInstruction(
                organization = organization,
                organizationDataRecipient = organizationVendor,
                requestType = dto.requestType,
                processingMethod = dto.processingMethod,
                retentionReasons = dto.retentionReasons?.toMutableList() ?: defaultReasons,
                processingInstructions = dto.processingInstructions,
                exceptionsAffirmed = dto.exceptionsAffirmed,
                deleteDoNotContact = dto.deleteDoNotContact,
                doNotContactReason = dto.doNotContactReason,
                doNotContactExplanation = dto.doNotContactExplanation,
                hasRetentionExceptions = dto.hasRetentionExceptions
            )
        }

        return requestHandlingInstructionsRepository.save(existing)
    }

    fun delete(
        organizationId: OrganizationPublicId,
        instructionId: UUIDString
    ): RequestHandlingInstruction? {
        val organization = lookupOrganizationOrThrow(organizationId)
        val instruction = findInstruction(organization, UUID.fromString(instructionId))
        instruction?.let {
            requestHandlingInstructionsRepository.delete(instruction)
        }
        return instruction
    }

    fun getConsumerGroupHandlingInstructions(organizationId: OrganizationPublicId): String {
        val organization = lookupOrganizationOrThrow(organizationId)

        return organization.requestProcessingInstructionsCollectionGroup ?: ""
    }

    fun getOptOutInstructions(organizationId: OrganizationPublicId): String {
        val organization = lookupOrganizationOrThrow(organizationId)

        return organization.requestProcessingInstructionsOptOut ?: ""
    }

    fun updateConsumerGroupRequestHandlingInstructions(dto: UpdateOrganizationCGInstructionsDto): UpdateOrganizationCGInstructionsDto {
        val organization = lookupOrganizationOrThrow(dto.organizationId)

        organization.requestProcessingInstructionsCollectionGroup = dto.cgRequestHandlingInstructions
        organizationRepository.save(organization)

        return dto
    }

    fun updateOptOutInstructions(dto: UpdateOrganizationCGInstructionsDto): UpdateOrganizationCGInstructionsDto {
        val organization = lookupOrganizationOrThrow(dto.organizationId)

        organization.requestProcessingInstructionsOptOut = dto.cgRequestHandlingInstructions
        organizationRepository.save(organization)

        return dto
    }

    fun autoSelectInaccessibleForAdNetworks(organizationId: OrganizationPublicId) {
        val organization = lookupOrganizationOrThrow(organizationId)
        val adNetworks = organization.organizationDataRecipients.filter { it.vendor?.recipientCategory == "Ad Network" }

        adNetworks.forEach {
            val instruction =
                requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(
                    organization,
                    dataRequestType = DataRequestInstructionType.DELETE,
                    organizationDataRecipient = it
                )
            if (instruction != null && instruction.processingMethod == null) {
                instruction.processingMethod = ProcessingMethod.INACCESSIBLE_OR_NOT_STORED
                requestHandlingInstructionsRepository.save(instruction)
            } else if (instruction == null) {
                requestHandlingInstructionsRepository.save(
                    RequestHandlingInstruction(
                        organization = organization,
                        organizationDataRecipient = it,
                        requestType = DataRequestInstructionType.DELETE,
                        processingMethod = ProcessingMethod.INACCESSIBLE_OR_NOT_STORED
                    )
                )
            }
        }
    }

    fun hasValidDeletionInstruction(organizationDataRecipient: OrganizationDataRecipient): Boolean {
        if (organizationDataRecipient.organization == null) {
            return false
        }

        val deletionInstruction = findInstruction(
            DataRequestInstructionType.DELETE,
            organizationDataRecipient.organization,
            organizationDataRecipient
        )
            ?: return false

        if (deletionInstruction.processingMethod == ProcessingMethod.RETAIN && deletionInstruction.retentionReasons.isNullOrEmpty()) {
            return false
        }

        return true
    }

    // Look up an existing instruction based on the combination of org, vendor, and dst that's appropriate for the requestType.
    private fun findInstruction(
        requestType: DataRequestInstructionType,
        organization: Organization,
        organizationDataRecipient: OrganizationDataRecipient?
    ): RequestHandlingInstruction? {
        val instruction = when (requestType) {
            DataRequestInstructionType.OPT_OUT -> {
                requestHandlingInstructionsRepository.findByOrganizationAndRequestType(organization, requestType)
            }
            DataRequestInstructionType.KNOW -> {
                requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(
                    organization, requestType, organizationDataRecipient!!
                )
            }
            DataRequestInstructionType.DELETE -> {
                requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(
                    organization, requestType, organizationDataRecipient!!
                )
            }
        }

        return instruction
    }

    private fun findInstruction(organization: Organization, id: UUID): RequestHandlingInstruction? =
        requestHandlingInstructionsRepository.findByIdAndOrganization(id, organization)

    private fun lookupOrganizationOrThrow(organizationId: OrganizationPublicId): Organization =
        organizationRepository.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)

    private fun lookupOrganizationVendor(organization: Organization, vendor: Vendor): OrganizationDataRecipient? =
        organizationDataRecipientRepository.findByOrganizationAndVendor(organization, vendor)
}
