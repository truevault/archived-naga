package polaris.services.primary

import freemarker.template.Configuration
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils
import polaris.models.dto.compliance.CollectionDto
import polaris.models.dto.compliance.DMCollectionGroupDto
import polaris.models.dto.compliance.DisclosureDto
import polaris.models.dto.compliance.FullDataMapDto
import polaris.models.dto.compliance.PersonalInformationSourceDto
import polaris.models.dto.privacycenter.BuiltinPrivacySectionSlug
import polaris.models.dto.privacycenter.PrivacyCenterPolicySectionDto
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.compliance.PICSurveyClassification
import polaris.models.entity.feature.Features
import polaris.models.entity.organization.BUSINESS_SURVEY_NAME
import polaris.models.entity.organization.BusinessPurposeType
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.DATA_RETENTION_SURVEY_NAME
import polaris.models.entity.organization.EEA_UK_SURVEY_NAME
import polaris.models.entity.organization.FINANCIAL_INCENTIVES_SURVEY_NAME
import polaris.models.entity.organization.GDPR_LAWFUL_BASES_SURVEY_NAME
import polaris.models.entity.organization.LawfulBasis
import polaris.models.entity.organization.OptOutPageType
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.SELLING_AND_SHARING_SURVEY_NAME
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.privacycenter.PrivacyCenterPolicySection
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.models.entity.vendor.ProcessingRegion
import polaris.repositories.DataRetentionPolicyDetailRepository
import polaris.repositories.OrganizationBusinessPurposeRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.ProcessingActivityRepo
import polaris.services.support.DataMapService
import polaris.services.support.OptOutService
import polaris.util.MarkdownRenderer
import polaris.util.PrivacyNoticeUtil
import polaris.util.StringUtil
import polaris.util.sorting.CategoryDtoSort
import polaris.util.sorting.CollectionDtoSort
import polaris.util.sorting.DMCollectionGroupDtoSort
import polaris.util.sorting.PICGroupDtoSort
import polaris.util.sorting.PICSort
import java.util.SortedMap
import java.util.UUID

val AUTOMATIC_COLLECTION =
    listOf("Internet Activity", "Online Identifiers", "Geolocation Activity", "Geolocation Information")

@Service
class PrivacyPolicyService(
    private val organizationService: OrganizationService,
    private val freemarkerConfig: Configuration,
    private val dataMapService: DataMapService,
    private val recipientRepo: OrganizationDataRecipientRepository,
    private val cgBusinessPurposeRepo: OrganizationBusinessPurposeRepository,
    private val purposeActivityRepo: ProcessingActivityRepo,
    private val processingActivityRepo: ProcessingActivityRepo,
    private val dataRetentionPolicyDetailRepo: DataRetentionPolicyDetailRepository,
    private val optOutService: OptOutService
) {
    companion object {
        private val logger = LoggerFactory.getLogger(PrivacyPolicyService::class.java)
    }

    fun getPrivacyPolicy(org: Organization, includeCustom: Boolean = true): List<PrivacyCenterPolicySectionDto> {
        val pc = org.privacyCenter()!!
        val sections = mutableListOf<PrivacyCenterPolicySectionDto?>()

        if (includeCustom) {
            sections.addAll(getSectionsCustom(org, pc))
        }

        val isLegacyOrg = organizationService.featuresForOrganization(org.publicId)
            .any { it.feature?.name == Features.LEGACY_ORG && it.enabled }

        val dataRecipients = recipientRepo.findAllByOrganization(org)
        val recipientsByVendor = dataRecipients.associateBy { it.vendor!!.id }

        if (!isLegacyOrg) {
            val map = dataMapService.getFullDataMap(org)
            val optOutType = optOutService.getOptOutPageType(org)

            sections.add(getSectionInformationWeCollect(org, pc, map))
            sections.add(getSectionHowWeShareAndDiscloseInformation(org, pc, map, recipientsByVendor))
            sections.add(getSectionCaPrivacyNotice(org, pc, map, optOutType, recipientsByVendor))
            sections.add(getSectionVaPrivacyNotice(org, pc, map, optOutType, recipientsByVendor))
            sections.add(getSectionCoPrivacyNotice(org, pc, map, optOutType, recipientsByVendor))
            sections.add(getSectionCtPrivacyNotice(org, pc, map, optOutType, recipientsByVendor))
            sections.add(getSectionFinancialIncentives(org, pc, map))
            sections.add(getSectionEeaUkGdpr(org, pc, map))
        }

        return sections.filterNotNull().sortedBy { it.displayOrder }
    }

    fun getPrivacyPolicyHtml(org: Organization, includeCustom: Boolean = true): String {
        return getPrivacyPolicy(org, includeCustom).joinToString("") { "<div>${it.body}</div>" }
    }

    private fun getSectionInformationWeCollect(
        org: Organization,
        pc: PrivacyCenter,
        dataMap: FullDataMapDto
    ): PrivacyCenterPolicySectionDto {
        val geolocationData = dataMap.categories.values.find { it.key == "geolocation-data" }
        val hasGeolocationCollection = dataMap.collection.any { geolocationData != null && it.collected.contains(geolocationData.id) }

        // collection information
        val collectionDetails = collectionDetailsFromDataMap(dataMap)
        val nonAutomaticCollection =
            collectionDetails.filter { cg -> !AUTOMATIC_COLLECTION.contains(cg.collectionGroupLabel) }

        // browser cookie information
        val eeaSurvey = organizationService.getSurveyQuestions(org.publicId, DATA_RETENTION_SURVEY_NAME)
        val browserCookieDone = eeaSurvey.find { it.slug == "cookie-policy-done" }?.answer == "true"

        // Collection Category Details
        val collectionCategories = collectionCategoriesMap(dataMap)
        val hasCollectionDetails = collectionCategories.isNotEmpty()
        val collectionSources = if (hasCollectionDetails) PrivacyNoticeUtil.sourcesListFromCollectionCategoriesMap(collectionCategories) else PrivacyNoticeUtil.sourcesListFromDataMap(dataMap, CollectionContext.CONSUMER)
        val collectionCategoryLabels = collectionCategoryLabelsMap(collectionCategories)

        val retentionDetails = getDataRetentionDetailRows(org)

        val model = mapOf(
            "cookiePolicy" to if (browserCookieDone) org.cookiePolicy else null,
            "collectionGroupDetails" to nonAutomaticCollection,
            "hasGeolocationCollection" to hasGeolocationCollection,
            "collectionSources" to StringUtil.joinNatural(collectionSources),
            "collectionCategories" to collectionCategories,
            "collectionCategoryLabels" to collectionCategoryLabels,
            "hasCollectionDetails" to hasCollectionDetails,
            "retentionPolicy" to pc.retentionIntro(),
            "retentionDetails" to retentionDetails,
            "purposeLabels" to getPurposeLabels(org)
        )

        return PrivacyCenterPolicySectionDto(
            name = "Information we collect",
            anchor = "information-we-collect",
            addToMenu = true,
            body = templatedBody("privacy/01-information-we-collect.ftl", model),
            custom = false,
            builtinSlug = BuiltinPrivacySectionSlug.collect,
            displayOrder = BuiltinPrivacySectionSlug.collect.displayOrder,
            showHeader = true
        )
    }

    private fun getDataRetentionDetailRows(org: Organization): List<DataRetentionDetailRow> {
        val retentionPolicies = dataRetentionPolicyDetailRepo.findAllByOrganization(org)
            .filter { it.pic.isNotEmpty() }
            .filter { !it.description.isNullOrEmpty() }
            .sortedBy { it.type!!.ordinal }

        return retentionPolicies.map {
            val pic = it.pic.sortedWith(PICSort).joinToString(", ") { item -> item.name!! }
            val label = if (it.pic.size == 1 && it.pic[0].toString().decapitalize() == it.type!!.label.decapitalize()) {
                it.type.label
            } else {
                "${it.type!!.label}, including $pic"
            }
            DataRetentionDetailRow(label, it.description ?: "")
        }
    }

    private fun getSectionHowWeShareAndDiscloseInformation(
        org: Organization,
        pc: PrivacyCenter,
        map: FullDataMapDto,
        recipientsByVendor: Map<UUID, OrganizationDataRecipient>
    ): PrivacyCenterPolicySectionDto {

        val disclosureCategories = map.collectionGroups.values.sortedWith(DMCollectionGroupDtoSort).mapNotNull { cg ->
            if (cg.type == CollectionGroupType.EMPLOYMENT) {
                return@mapNotNull null
            }
            // first get all disclosures for the group
            val cgDisclosures = map.disclosure.filter { it.collectionGroupId == cg.id }

            val disclosureCategories = PrivacyNoticeUtil.getServiceAndThirdPartyDisclosureCategories(cgDisclosures, map, recipientsByVendor)
            CollectionGroupDisclosureByVendor(collectionGroupLabel(cg), disclosureCategories)
        }

        val model = mapOf<String, Any?>("disclosureCategories" to disclosureCategories)

        return PrivacyCenterPolicySectionDto(
            name = "How we disclose information",
            anchor = "share-and-disclosure",
            addToMenu = true,
            body = templatedBody("privacy/02-how-we-share-disclose-info.ftl", model),
            builtinSlug = BuiltinPrivacySectionSlug.disclose,
            displayOrder = BuiltinPrivacySectionSlug.disclose.displayOrder,
            showHeader = true,
            custom = false
        )
    }

    private fun getSectionCaPrivacyNotice(
        org: Organization,
        pc: PrivacyCenter,
        map: FullDataMapDto,
        optOutType: OptOutPageType,
        recipientsByVendor: Map<UUID, OrganizationDataRecipient>
    ): PrivacyCenterPolicySectionDto? {
        if (pc.hideCcpaSection) {
            return null
        }

        val businessSurveyQuestions = organizationService.getSurveyQuestions(org.publicId, BUSINESS_SURVEY_NAME)
        val sellingSurveyQuestions = organizationService.getSurveyQuestions(org.publicId, SELLING_AND_SHARING_SURVEY_NAME)

        val physicalLocation =
            businessSurveyQuestions.any { it.slug == "business-physical-location" && it.answer == "true" }
        val productsOrServicesAimedAtKids =
            businessSurveyQuestions.any { it.slug == "business-products-aimed-at-kids" && it.answer == "true" }

        val sellingUploadData =
            sellingSurveyQuestions.any { it.slug == "selling-upload-data" && it.answer == "true" }

        val noUnderageSellingOrSharing = !productsOrServicesAimedAtKids || !sellingUploadData

        val optOutMessage = when (optOutType) {
            OptOutPageType.NONE -> null
            OptOutPageType.SELLING_AND_SHARING -> "\"share\" and \"sell\""
            OptOutPageType.SELLING -> "\"sell\""
            OptOutPageType.SHARING -> "\"share\""
        }

        val hasOptOut = optOutType != OptOutPageType.NONE
        val sharedDisclosures =
            map.disclosure.filter { d -> recipientsByVendor[UUID.fromString(d.vendorId)]?.ccpaIsSharing ?: false }
        val sharedPic = sharedDisclosures.flatMap { d -> d.disclosed.mapNotNull { map.categories[it] } }
        val sharedGroupIds = sharedPic.map { it.picGroupId }
        val sharedGroups = sharedGroupIds.mapNotNull { map.picGroups[it] }.map { it.name }.distinct().sorted()
        val sharingGroups = StringUtil.joinNatural(sharedGroups)

        val collectedPic = map.collection
            .filter { it.collectionGroupType != CollectionGroupType.EMPLOYMENT }
            .flatMap { it.collected.mapNotNull { id -> map.categories.get(id) } }
            .sortedWith(CategoryDtoSort)
        val sensitivePic = collectedPic.filter { it.surveyClassification == PICSurveyClassification.SENSITIVE }
        val spiCategoryList = sensitivePic.joinToString(", ") { it.name }

        val model = mapOf(
            "purposeLabels" to getPurposeLabels(org),
            "optOutMessage" to optOutMessage,
            "hasOptOut" to hasOptOut,
            "optOutUrl" to optOutService.getOptOutUrl(pc),
            "sharingGroups" to sharingGroups,
            "noUnderageSellingOrSharing" to noUnderageSellingOrSharing,
            "includeSelling" to (optOutType == OptOutPageType.SELLING || optOutType == OptOutPageType.SELLING_AND_SHARING),
            "hasPhysicalLocation" to physicalLocation,
            "privacyRequestTollFreeNumber" to pc.requestTollFreePhoneNumber,
            "privacyRequestUrl" to pc.getPrivacyRequestUrl(),
            "privacyRequestEmail" to pc.emailWithFallback(),
            "privacyNoticeIntroText" to pc.ccpaIntro(),
            "rightToOptIn" to (productsOrServicesAimedAtKids && optOutService.hasDirectTransmissionSellingDataRecipients(org)),
            "isServiceProvider" to (org.isServiceProvider ?: false),
            "hasSpiRequest" to organizationService.requiresNoticeOfRightToLimit(org),
            "spiRequestUrl" to pc.getSPILimitUrl(),
            "spiCategoryList" to spiCategoryList
        )
        return PrivacyCenterPolicySectionDto(
            name = "California Privacy Notice (CCPA)",
            anchor = "california-privacy-notice",
            addToMenu = true,
            body = templatedBody("privacy/03-california-privacy-notice-ccpa.ftl", model),
            builtinSlug = BuiltinPrivacySectionSlug.ccpa,
            displayOrder = BuiltinPrivacySectionSlug.ccpa.displayOrder,
            showHeader = true,
            custom = false
        )
    }

    private fun getPurposeLabels(org: Organization): List<String> {
        val detailedPurposes = purposeActivityRepo.findAllByOrganization(org).filter { it.processedPic.isNotEmpty() }
        val purposes = cgBusinessPurposeRepo.findAllByOrganizationAndBusinessPurposeType(org, BusinessPurposeType.CONSUMER)
        val purposeLabels = if (detailedPurposes.isNotEmpty()) {
            detailedPurposes.map { p -> p.name }.distinct().sorted()
        } else {
            purposes.map { p -> p.businessPurpose }.distinct().mapNotNull { p -> p?.name }.sorted()
        }

        return purposeLabels
    }

    private fun getSectionVaPrivacyNotice(
        org: Organization,
        pc: PrivacyCenter,
        map: FullDataMapDto,
        optOutType: OptOutPageType,
        recipientsByVendor: Map<UUID, OrganizationDataRecipient>
    ): PrivacyCenterPolicySectionDto? {
        if (!org.isMultistate()) {
            return null
        }

        val hasOptOut = optOutType != OptOutPageType.NONE

        val businessSurveyQuestions = organizationService.getSurveyQuestions(org.publicId, BUSINESS_SURVEY_NAME)
        val sellingSurveyQuestions = organizationService.getSurveyQuestions(org.publicId, SELLING_AND_SHARING_SURVEY_NAME)

        // purposeLabels -- CCPA
        // privacyRequestUrl -- CCPA
        // hasPhysicalLocation -- CCPA
        // privacyRequestTollFreeNumber -- CCPA

        val physicalLocation =
            businessSurveyQuestions.any { it.slug == "business-physical-location" && it.answer == "true" }

        val createsProfiles = sellingSurveyQuestions.any { it.slug == "create-profiles-about-consumers" && it.answer == "true" }
        val sellsInExchangeForMoney = sellingSurveyQuestions.any { it.slug == "sell-in-exchange-for-money" && it.answer == "true" }
        val usesTrackingTech = sellingSurveyQuestions.any { it.slug == "share-data-for-advertising" && it.answer == "true" }
        val usesCustomAudience = sellingSurveyQuestions.any { it.slug == "custom-audience-feature" && !it.answer.isNullOrEmpty() && it.answer != "[]" && it.answer != "[\"none\"]" }
        val processesForTargetedAds = usesTrackingTech || usesCustomAudience

        val sharedDisclosures =
            map.disclosure.filter { d -> recipientsByVendor[UUID.fromString(d.vendorId)]?.ccpaIsSharing ?: false }
        val sharedPic = sharedDisclosures.flatMap { d -> d.disclosed.mapNotNull { map.categories[it] } }
        val sharedGroupIds = sharedPic.map { it.picGroupId }
        val sharedGroups = sharedGroupIds.mapNotNull { map.picGroups[it] }.map { it.name }.distinct().sorted()
        val sharingGroups = StringUtil.joinNatural(sharedGroups)

        val model = mapOf(
            "purposeLabels" to getPurposeLabels(org),
            // "optOutMessage" to optOutMessage,
            "hasOptOut" to hasOptOut,
            "optOutUrl" to optOutService.getOptOutUrl(pc),
            "sharingGroups" to sharingGroups,
            // "includeSelling" to (optOutType == OptOutPageType.SELLING || optOutType == OptOutPageType.SELLING_AND_SHARING),
            "hasPhysicalLocation" to physicalLocation,
            "privacyRequestTollFreeNumber" to pc.requestTollFreePhoneNumber,
            "privacyRequestUrl" to pc.getPrivacyRequestUrl(),
            "privacyRequestEmail" to pc.emailWithFallback(),
            "privacyNoticeIntroText" to pc.vcdpaIntro(),

            "sellsInExchangeForMoney" to sellsInExchangeForMoney,
            "createsProfiles" to createsProfiles,
            "processesForTargetedAds" to processesForTargetedAds
            // "rightToOptIn" to (productsOrServicesAimedAtKids && optOutService.hasDirectTransmissionSellingDataRecipients(org)),
            // "isServiceProvider" to (org.isServiceProvider ?: false),
            // "hasSpiRequest" to organizationService.requiresNoticeOfRightToLimit(org),
            // "spiRequestUrl" to pc.getSPILimitUrl(),
            // "spiCategoryList" to spiCategoryList
        )

        return PrivacyCenterPolicySectionDto(
            name = "Virginia Privacy Notice (VCDPA)",
            anchor = "virginia-privacy-notice",
            addToMenu = true,
            body = templatedBody("privacy/04-virginia-privacy-notice-vcdpa.ftl", model),
            builtinSlug = BuiltinPrivacySectionSlug.vcdpa,
            displayOrder = BuiltinPrivacySectionSlug.vcdpa.displayOrder,
            showHeader = true,
            custom = false
        )
    }

    private fun getSectionCoPrivacyNotice(
        org: Organization,
        pc: PrivacyCenter,
        map: FullDataMapDto,
        optOutType: OptOutPageType,
        recipientsByVendor: Map<UUID, OrganizationDataRecipient>
    ): PrivacyCenterPolicySectionDto? {
        if (!org.isMultistate()) {
            return null
        }

        val hasOptOut = optOutType != OptOutPageType.NONE

        val businessSurveyQuestions = organizationService.getSurveyQuestions(org.publicId, BUSINESS_SURVEY_NAME)
        val sellingSurveyQuestions = organizationService.getSurveyQuestions(org.publicId, SELLING_AND_SHARING_SURVEY_NAME)

        // purposeLabels -- CCPA
        // privacyRequestUrl -- CCPA
        // hasPhysicalLocation -- CCPA
        // privacyRequestTollFreeNumber -- CCPA

        val physicalLocation =
            businessSurveyQuestions.any { it.slug == "business-physical-location" && it.answer == "true" }

        val createsProfiles = sellingSurveyQuestions.any { it.slug == "create-profiles-about-consumers" && it.answer == "true" }
        val sellsInExchangeForMoney = sellingSurveyQuestions.any { it.slug == "sell-in-exchange-for-money" && it.answer == "true" }
        val usesTrackingTech = sellingSurveyQuestions.any { it.slug == "share-data-for-advertising" && it.answer == "true" }
        val usesCustomAudience = sellingSurveyQuestions.any { it.slug == "custom-audience-feature" && !it.answer.isNullOrEmpty() && it.answer != "[]" && it.answer != "[\"none\"]" }
        val processesForTargetedAds = usesTrackingTech || usesCustomAudience

        val sharedDisclosures =
            map.disclosure.filter { d -> recipientsByVendor[UUID.fromString(d.vendorId)]?.ccpaIsSharing ?: false }
        val sharedPic = sharedDisclosures.flatMap { d -> d.disclosed.mapNotNull { map.categories[it] } }
        val sharedGroupIds = sharedPic.map { it.picGroupId }
        val sharedGroups = sharedGroupIds.mapNotNull { map.picGroups[it] }.map { it.name }.distinct().sorted()
        val sharingGroups = StringUtil.joinNatural(sharedGroups)

        val model = mapOf(
            "purposeLabels" to getPurposeLabels(org),
            // "optOutMessage" to optOutMessage,
            "hasOptOut" to hasOptOut,
            "optOutUrl" to optOutService.getOptOutUrl(pc),
            "sharingGroups" to sharingGroups,
            "includeSelling" to (optOutType == OptOutPageType.SELLING || optOutType == OptOutPageType.SELLING_AND_SHARING),
            "hasPhysicalLocation" to physicalLocation,
            "privacyRequestTollFreeNumber" to pc.requestTollFreePhoneNumber,
            "privacyRequestUrl" to pc.getPrivacyRequestUrl(),
            "privacyRequestEmail" to pc.emailWithFallback(),
            "privacyNoticeIntroText" to pc.cpaIntro(),

            "sellsInExchangeForMoney" to sellsInExchangeForMoney,
            "createsProfiles" to createsProfiles,
            "processesForTargetedAds" to processesForTargetedAds
            // "rightToOptIn" to (productsOrServicesAimedAtKids && optOutService.hasDirectTransmissionSellingDataRecipients(org)),
            // "isServiceProvider" to (org.isServiceProvider ?: false),
            // "hasSpiRequest" to organizationService.requiresNoticeOfRightToLimit(org),
            // "spiRequestUrl" to pc.getSPILimitUrl(),
            // "spiCategoryList" to spiCategoryList
        )

        return PrivacyCenterPolicySectionDto(
            name = "Colorado Privacy Notice (CPA)",
            anchor = "cpa-privacy-notice",
            addToMenu = true,
            body = templatedBody("privacy/05-colorado-privacy-notice-cpa.ftl", model),
            builtinSlug = BuiltinPrivacySectionSlug.cpa,
            displayOrder = BuiltinPrivacySectionSlug.cpa.displayOrder,
            showHeader = true,
            custom = false
        )
    }

    private fun getSectionCtPrivacyNotice(
        org: Organization,
        pc: PrivacyCenter,
        map: FullDataMapDto,
        optOutType: OptOutPageType,
        recipientsByVendor: Map<UUID, OrganizationDataRecipient>
    ): PrivacyCenterPolicySectionDto? {
        if (!org.isMultistate()) {
            return null
        }

        val hasOptOut = optOutType != OptOutPageType.NONE

        val businessSurveyQuestions = organizationService.getSurveyQuestions(org.publicId, BUSINESS_SURVEY_NAME)
        val sellingSurveyQuestions = organizationService.getSurveyQuestions(org.publicId, SELLING_AND_SHARING_SURVEY_NAME)

        // purposeLabels -- CCPA
        // privacyRequestUrl -- CCPA
        // hasPhysicalLocation -- CCPA
        // privacyRequestTollFreeNumber -- CCPA

        val physicalLocation =
            businessSurveyQuestions.any { it.slug == "business-physical-location" && it.answer == "true" }

        val createsProfiles = sellingSurveyQuestions.any { it.slug == "create-profiles-about-consumers" && it.answer == "true" }
        val sellsInExchangeForMoney = sellingSurveyQuestions.any { it.slug == "sell-in-exchange-for-money" && it.answer == "true" }
        val usesTrackingTech = sellingSurveyQuestions.any { it.slug == "share-data-for-advertising" && it.answer == "true" }
        val usesCustomAudience = sellingSurveyQuestions.any { it.slug == "custom-audience-feature" && !it.answer.isNullOrEmpty() && it.answer != "[]" && it.answer != "[\"none\"]" }
        val processesForTargetedAds = usesTrackingTech || usesCustomAudience

        val sharedDisclosures =
            map.disclosure.filter { d -> recipientsByVendor[UUID.fromString(d.vendorId)]?.ccpaIsSharing ?: false }
        val sharedPic = sharedDisclosures.flatMap { d -> d.disclosed.mapNotNull { map.categories[it] } }
        val sharedGroupIds = sharedPic.map { it.picGroupId }
        val sharedGroups = sharedGroupIds.mapNotNull { map.picGroups[it] }.map { it.name }.distinct().sorted()
        val sharingGroups = StringUtil.joinNatural(sharedGroups)

        val model = mapOf(
            "purposeLabels" to getPurposeLabels(org),
            // "optOutMessage" to optOutMessage,
            "hasOptOut" to hasOptOut,
            "optOutUrl" to optOutService.getOptOutUrl(pc),
            "sharingGroups" to sharingGroups,
            "includeSelling" to (optOutType == OptOutPageType.SELLING || optOutType == OptOutPageType.SELLING_AND_SHARING),
            "hasPhysicalLocation" to physicalLocation,
            "privacyRequestTollFreeNumber" to pc.requestTollFreePhoneNumber,
            "privacyRequestUrl" to pc.getPrivacyRequestUrl(),
            "privacyRequestEmail" to pc.emailWithFallback(),
            "privacyNoticeIntroText" to pc.ctdpaIntro(),

            "sellsInExchangeForMoney" to sellsInExchangeForMoney,
            "createsProfiles" to createsProfiles,
            "processesForTargetedAds" to processesForTargetedAds
            // "rightToOptIn" to (productsOrServicesAimedAtKids && optOutService.hasDirectTransmissionSellingDataRecipients(org)),
            // "isServiceProvider" to (org.isServiceProvider ?: false),
            // "hasSpiRequest" to organizationService.requiresNoticeOfRightToLimit(org),
            // "spiRequestUrl" to pc.getSPILimitUrl(),
            // "spiCategoryList" to spiCategoryList
        )

        return PrivacyCenterPolicySectionDto(
            name = "Connecticut Privacy Notice (CTDPA)",
            anchor = "connecticut-privacy-notice",
            addToMenu = true,
            body = templatedBody("privacy/06-connecticut-privacy-notice-ctdpa.ftl", model),
            builtinSlug = BuiltinPrivacySectionSlug.ctdpa,
            displayOrder = BuiltinPrivacySectionSlug.ctdpa.displayOrder,
            showHeader = true,
            custom = false
        )
    }

    private fun getSectionFinancialIncentives(
        org: Organization,
        pc: PrivacyCenter,
        map: FullDataMapDto
    ): PrivacyCenterPolicySectionDto? {
        val incentivesSurvey =
            organizationService.getSurveyQuestions(org.publicId, FINANCIAL_INCENTIVES_SURVEY_NAME)

        val anyFinancialIncentives =
            incentivesSurvey.any { it.slug == "offer-consumer-incentives" && it.answer == "true" }

        val incentives = org.financialIncentiveDetails ?: ""

        if (!anyFinancialIncentives || incentives.isBlank()) {
            return null
        }

        val model = mapOf<String, Any?>(
            "incentives" to incentives
        )
        return PrivacyCenterPolicySectionDto(
            name = "Notice of Financial Incentive",
            anchor = "financial-incentive",
            addToMenu = true,
            body = templatedBody("privacy/07-notice-of-fincancial-incentives.ftl", model),
            builtinSlug = BuiltinPrivacySectionSlug.financial,
            displayOrder = BuiltinPrivacySectionSlug.financial.displayOrder,
            showHeader = true,
            custom = false
        )
    }

    private fun getSectionEeaUkGdpr(
        org: Organization,
        pc: PrivacyCenter,
        map: FullDataMapDto
    ): PrivacyCenterPolicySectionDto? {
        if (!org.isGdpr()) {
            return null
        }
        val processingActivities = processingActivityRepo.findAllByOrganization(org)

        val eeaUkSurvey = organizationService.getSurveyQuestions(org.publicId, EEA_UK_SURVEY_NAME)
        val lawfulBasesSurvey = organizationService.getSurveyQuestions(org.publicId, GDPR_LAWFUL_BASES_SURVEY_NAME)

        val establishedInEeaUk = eeaUkSurvey.any { it.slug == "established-in-eea-uk" && it.answer == "true" }
        val nonEnglishWebsite = eeaUkSurvey.any { it.slug == "non-english-website" && it.answer == "true" }
        val automatedDecisionMaking =
            lawfulBasesSurvey.any { it.slug == "automated-decision-making" && it.answer == "true" }

        // all lawful bases ->
        val allProcessingActivities = processingActivityRepo.findAllByOrganization(org)
        val allLawfulBases = allProcessingActivities.flatMap { it.gdprLawfulBases }.distinct()
        val hasLawfulBasisPublicInterest = allLawfulBases.contains(LawfulBasis.PUBLIC_INTEREST)
        val hasLawfulBasisVitalInterest = allLawfulBases.contains(LawfulBasis.VITAL_INTEREST_OF_INDIVIDUAL)

        val activities = StringUtil.joinNatural(processingActivities.mapNotNull { it.name })
        val controllers = org.organizationDataRecipients.filter { it.processingRegions.contains(ProcessingRegion.EEA_UK) }.filter { it.activeGdprProcessorSetting() == GDPRProcessorRecommendation.Controller }

        val model = mapOf(
            "optOutUrl" to optOutService.getOptOutUrl(pc),
            "privacyRequestUrl" to (pc.getPrivacyRequestUrl() ?: "null"),
            "isAutomatedDecisionMaking" to automatedDecisionMaking,
            "isInternationalDataTransfers" to (establishedInEeaUk || nonEnglishWebsite),
            "hasLawfulBasisPublicInterest" to hasLawfulBasisPublicInterest,
            "hasLawfulBasisVitalInterest" to hasLawfulBasisVitalInterest,
            "orgName" to org.name,
            "activities" to activities,
            "dpoName" to pc.dpoOfficerName,
            "dpoEmail" to pc.dpoOfficerEmail,
            "controllers" to controllers,
            "privacyNoticeIntroText" to pc.gdprIntro(),
            "privacyRequestEmail" to pc.emailWithFallback()
        )
        return PrivacyCenterPolicySectionDto(
            name = "EEA/UK Privacy Notice (GDPR)",
            anchor = "eea-uk-privacy-notice",
            addToMenu = true,
            body = templatedBody("privacy/08-eea-uk-privacy-notice-gdpr.ftl", model),
            builtinSlug = BuiltinPrivacySectionSlug.gdpr,
            displayOrder = BuiltinPrivacySectionSlug.gdpr.displayOrder,
            showHeader = true,
            custom = false
        )
    }

    private fun getSectionsCustom(org: Organization, pc: PrivacyCenter): List<PrivacyCenterPolicySectionDto> {
        return pc.policySections.map { toPolicySectionDto(it) }
    }

    private fun toPolicySectionDto(privacyCenterPolicySection: PrivacyCenterPolicySection): PrivacyCenterPolicySectionDto {
        val prefix = if (privacyCenterPolicySection.showHeader) "<h2>${privacyCenterPolicySection.name}</h2>" else ""
        return PrivacyCenterPolicySectionDto(
            name = privacyCenterPolicySection.name,
            displayOrder = privacyCenterPolicySection.displayOrder,
            showHeader = privacyCenterPolicySection.showHeader,
            anchor = privacyCenterPolicySection.anchor,
            addToMenu = privacyCenterPolicySection.addToMenu,
            // TODO: Remove markdown renderer when all policies/notices have migrated to only HTML for the WYSIWYG editor (2/2)
            body = prefix + MarkdownRenderer.renderToHtml(privacyCenterPolicySection.body),
            builtinSlug = null,
            custom = true
        )
    }

    private fun templatedBody(templateName: String, model: Map<String, Any?>): String {
        return FreeMarkerTemplateUtils.processTemplateIntoString(
            freemarkerConfig.getTemplate(templateName), model
        )
    }

    private fun collectionGroupLabel(dto: CollectionDto) =
        collectionGroupLabel(name = dto.collectionGroupName, description = dto.collectionGroupDescription)

    private fun collectionGroupLabel(dto: DisclosureDto) =
        collectionGroupLabel(name = dto.collectionGroupName, description = dto.collectionGroupDescription)

    private fun collectionGroupLabel(dto: DMCollectionGroupDto) =
        collectionGroupLabel(name = dto.name, description = dto.description)

    private fun collectionGroupLabel(group: CollectionGroup) =
        collectionGroupLabel(name = group.name, description = group.description)

    private fun collectionGroupLabel(name: String, description: String?): String {
        return if (description == null || description.isBlank()) {
            name
        } else {
            description
        }
    }

    private fun collectionCategoriesMap(dataMap: FullDataMapDto): CollectionCategoriesMap {
        val relevantReceived = dataMap.received.filter { it.vendorCategory != "Direct Collection" }.filter { source ->
            val cg = dataMap.collectionGroups[source.collectionGroupId]!!
            val isNotEmployment = cg.type != CollectionGroupType.EMPLOYMENT
            val isNotConsumerDirectly = dataMap.sources[source.vendorId]?.category != PersonalInformationSourceDto.YOU_CATEGORY
            val isNotEmpty = source.received.isNotEmpty()
            return@filter isNotEmployment && isNotConsumerDirectly && isNotEmpty
        }

        return relevantReceived.groupBy { it.vendorCategory }.mapNotNull { (category, sources) ->
            // Get all the received PICs for this Source Category
            val receivedPICs =
                dataMap.categories.values.filter { sources.any { source -> source.received.contains(it.id) } }

            val sourceCategories = receivedPICs.groupBy { it.picGroupId }.map { (groupId, categories) ->
                val category = dataMap.categories.values.find { it.picGroupId == groupId }!!
                val picNames = categories.map { it.name }.distinct()

                SourceCategory(category.picGroupName, StringUtil.joinNatural(picNames))
            }

            SourceCollectionCategories(StringUtil.hardcodedPluralize(category), sourceCategories)
        }.groupBy { it.sourceCategoryLabel }.toSortedMap(
            compareBy({ categoryLabel ->
                val source = dataMap.sources.values.find { it.category == categoryLabel } ?: return@compareBy 0
                return@compareBy source.displayOrder
            }, { it })
        )
    }

    private fun collectionCategoryLabelsMap(collectCategories: CollectionCategoriesMap): CollectionCategoryLabels {
        return collectCategories.mapValues { categories ->
            categories.value.map { category ->
                category.picGroups.map {
                    val picGroup = it.picGroup.toLowerCase()
                    val sourcedCategories = it.sourcedCategories.toLowerCase()
                    val isSingular = picGroup == sourcedCategories || picGroup == "geolocation information"
                    if (isSingular) {
                        it.picGroup
                    } else {
                        "${it.picGroup}, including ${it.sourcedCategories}"
                    }
                }
            }
        }
    }

    private fun collectionDetailsFromDataMap(dataMap: FullDataMapDto): List<CollectionGroupCollections> {
        return dataMap.collection.sortedWith(CollectionDtoSort).mapNotNull { collection ->
            if (collection.collectionGroupType == CollectionGroupType.EMPLOYMENT) {
                return@mapNotNull null
            }

            val collectedPic = collection.collected.mapNotNull { dataMap.categories[it] }
            val byPicGroup = collectedPic.groupBy { pic -> pic.picGroupId }
                .toSortedMap(PICGroupDtoSort.compareByGroupID(dataMap))

            val labels = byPicGroup.mapNotNull { (picGroupId, collections) ->
                val group = dataMap.picGroups[picGroupId]!!
                val picNames = collections
                    .sortedBy { it.order }
                    .map { v -> if (v.alwaysPreserveCasing) v.name else v.name.decapitalize() }

                if (AUTOMATIC_COLLECTION.contains(group.name)) {
                    null
                } else {
                    if (picNames.size == 1 && group.name.decapitalize() == picNames[0].decapitalize()) {
                        group.name
                    } else {
                        group.name.let { "$it, including ${StringUtil.joinNatural(picNames)}" }
                    }
                }
            }

            CollectionGroupCollections(collectionGroupLabel(collection), picGroups = labels)
        }
    }
}

typealias CollectionCategoriesMap = SortedMap<String, List<SourceCollectionCategories>>

typealias CollectionCategoryLabels = Map<String, List<List<String>>>

data class CollectionGroupCollections(val collectionGroupLabel: String, val picGroups: List<String>)

data class CollectionGroupDisclosures(val collectionGroupLabel: String, val picGroups: List<DisclosureCategory>)
data class DisclosureCategory(val picGroup: String, val recipientCategories: String)

data class CollectionGroupDisclosureByVendor(val collectionGroupLabel: String, val picGroups: List<DisclosureCategoryByVendor>)
data class DisclosureCategoryByVendor(val picGroup: String, val serviceCategories: String, val thirdPartyCategories: String)

data class SourceCollectionCategories(val sourceCategoryLabel: String, val picGroups: List<SourceCategory>)
data class SourceCategory(val picGroup: String, val sourcedCategories: String)

data class DataRetentionDetailRow(val label: String, val userText: String)
