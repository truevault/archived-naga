package polaris.services.primary

import org.slf4j.LoggerFactory
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.dto.vendor.AddOrUpdateCustomVendorDto
import polaris.models.entity.VendorNotFound
import polaris.models.entity.vendor.DataRecipientType
import polaris.models.entity.vendor.Vendor
import polaris.repositories.VendorRepository
import polaris.services.support.PrivacyCenterBuilderService
import java.util.UUID
import javax.transaction.Transactional

@org.springframework.stereotype.Service
@Transactional
/* I'm very sorry about this name... */
class VendorService(
    private val vendorRepository: VendorRepository,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService,
    private val lookup: LookupService
) {

    companion object {
        private val log = LoggerFactory.getLogger(VendorService::class.java)
    }

    fun getVendor(organizationId: OrganizationPublicId, vendorId: UUID): Vendor {
        val vendor = lookup.vendorOrThrow(vendorId)
        if (vendor.organization != null && vendor.organization != lookup.orgOrThrow(organizationId)) {
            throw VendorNotFound(vendorId.toString())
        }
        return vendor
    }

    fun getVendorCatalog(organizationId: OrganizationPublicId) =
        vendorRepository.findAllByOrganization(lookup.orgOrThrow(organizationId))

    fun findVendorsByName(organizationId: OrganizationPublicId, vendorName: String) =
        vendorRepository.findAllByOrganizationAndName(lookup.orgOrThrow(organizationId), vendorName)

    fun addCustomVendor(organizationId: OrganizationPublicId, data: AddOrUpdateCustomVendorDto): Vendor {
        val org = lookup.orgOrThrow(organizationId)
        val saved = vendorRepository.save(
            Vendor(
                name = data.name!!,
                recipientCategory = data.category,
                dataRecipientType = data.dataRecipientType ?: DataRecipientType.vendor,
                url = data.url,
                email = data.email,
                organization = org,
                isDataRecipient = true
            )
        )

        privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(org, "custom vendor added")
        return saved
    }

    fun updateCustomVendor(organizationId: OrganizationPublicId, vendorId: UUID, data: AddOrUpdateCustomVendorDto): Vendor {
        val vendor = lookup.vendorOrThrow(vendorId)
        val org = lookup.orgOrThrow(organizationId)
        vendor.name = data.name!!
        vendor.recipientCategory = data.category
        vendor.url = data.url
        vendor.email = data.email
        val saved = vendorRepository.save(vendor)
        privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(org, "custom vendor updated")
        return saved
    }

    fun getThirdPartyDataRecipients(organizationId: OrganizationPublicId): List<Vendor> {
        val org = lookup.orgOrThrow(organizationId)
        val vendors =
            vendorRepository.findAllByOrganizationAndDataRecipientType(org, DataRecipientType.third_party_recipient)

        return vendors
    }
}
