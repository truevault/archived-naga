package polaris.services.primary

import com.nylas.Message
import org.springframework.stereotype.Service
import polaris.controllers.api.hosted.POLARIS_TIME_ZONE
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.organization.PrivacyEmail
import polaris.repositories.PrivacyEmailRepository
import polaris.specs.PrivacyEmailSpecs
import java.time.Instant
import java.time.ZonedDateTime
import java.util.*

@Service
class PrivacyEmailService(
    private val privacyEmailRepository: PrivacyEmailRepository
) {
    fun logPrivacyEmail(message: Message, mailbox: OrganizationMailbox, autoProcessed: Boolean = false) {
        if (mailbox.organization != null) {
            val receivedAt = ZonedDateTime.ofInstant(Instant.ofEpochSecond(message.date), POLARIS_TIME_ZONE)
            logPrivacyEmail(org = mailbox.organization, subject = message.subject, receivedAt = receivedAt, createdPrivacyRequest = autoProcessed)
        }
    }

    fun logPrivacyEmail(org: Organization, subject: String, receivedAt: ZonedDateTime, createdPrivacyRequest: Boolean = false) {
        privacyEmailRepository.save(PrivacyEmail(subject = subject, organization = org, receivedAt = receivedAt, createdPrivacyRequest = createdPrivacyRequest))
    }

    fun privacyEmailsBetween(org: Organization, startDate: ZonedDateTime, endDate: ZonedDateTime): List<PrivacyEmail> {
        val spec = PrivacyEmailSpecs.forOrganization(org).and(PrivacyEmailSpecs.receivedInRange(startDate, endDate))
        return privacyEmailRepository.findAll(spec)
    }
}
