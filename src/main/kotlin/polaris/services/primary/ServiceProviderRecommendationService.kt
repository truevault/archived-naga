package polaris.services.primary

import org.springframework.stereotype.Service
import polaris.models.SurveySlugs.Companion.GOOGLE_ANALYTICS_DATA_SHARING_SLUG
import polaris.models.SurveySlugs.Companion.SHOPIFY_AUDIENCES_QUESTION_SLUG
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.SELLING_AND_SHARING_SURVEY_NAME
import polaris.models.entity.organization.hasFalseAnswer
import polaris.models.entity.organization.hasTrueAnswer
import polaris.models.entity.vendor.KnownVendorCategories
import polaris.models.entity.vendor.KnownVendorKey
import polaris.models.entity.vendor.ServiceProviderRecommendation
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_CONTRACTOR
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY
import polaris.models.entity.vendor.VendorClassification
import polaris.repositories.OrganizationSurveyQuestionRepository

@Service
class ServiceProviderRecommendationService(val surveyRepo: OrganizationSurveyQuestionRepository) {

    fun getDataRecipientServiceProviderRecommendation(dataRecipient: OrganizationDataRecipient): ServiceProviderRecommendation? {
        val org = dataRecipient.organization!!
        val vendor = dataRecipient.vendor!!

        if (vendor.isThirdParty()) {
            return ServiceProviderRecommendation.THIRD_PARTY
        }

        if (vendor.isContractor()) {
            return ServiceProviderRecommendation.CONTRACTOR
        }

        if (vendor.isKnownCategory(KnownVendorCategories.AD_NETWORK)) {
            if (dataRecipient.ccpaIsSharing) {
                return ServiceProviderRecommendation.THIRD_PARTY
            } else {
                return ServiceProviderRecommendation.SERVICE_PROVIDER
            }
        }

        if (vendor.isKnownVendor(KnownVendorKey.SHOPIFY)) {
            val survey = surveyRepo.findAllByOrganizationAndSurveyName(org, SELLING_AND_SHARING_SURVEY_NAME)
            if (survey.hasFalseAnswer(SHOPIFY_AUDIENCES_QUESTION_SLUG)) {
                return ServiceProviderRecommendation.SERVICE_PROVIDER
            } else if (survey.hasTrueAnswer(SHOPIFY_AUDIENCES_QUESTION_SLUG)) {
                return ServiceProviderRecommendation.THIRD_PARTY
            }
        }

        if (vendor.isKnownVendor(KnownVendorKey.GOOGLE_ANALYTICS)) {
            val survey = surveyRepo.findAllByOrganizationAndSurveyName(org, SELLING_AND_SHARING_SURVEY_NAME)
            if (survey.hasFalseAnswer(GOOGLE_ANALYTICS_DATA_SHARING_SLUG)) {
                return ServiceProviderRecommendation.THIRD_PARTY
            } else if (survey.hasTrueAnswer(GOOGLE_ANALYTICS_DATA_SHARING_SLUG)) {
                return ServiceProviderRecommendation.SERVICE_PROVIDER
            }
        }

        return vendor.serviceProviderRecommendation
    }

    fun getDefaultClassification(dataRecipient: OrganizationDataRecipient, classificationOptions: List<VendorClassification>): VendorClassification? {
        if (classificationOptions.isEmpty()) {
            return null
        }

        val unknownClassification = classificationOptions.find { it.slug == VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW }
        val serviceProviderClassification = classificationOptions.find { it.slug == VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER }
        val thirdPartyClassification = classificationOptions.find { it.slug == VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY }
        val contractorClassification = classificationOptions.find { it.slug == VENDOR_CLASSIFICATION_SLUG_CONTRACTOR }

        return when (getDataRecipientServiceProviderRecommendation(dataRecipient)) {
            ServiceProviderRecommendation.NONE -> unknownClassification
            ServiceProviderRecommendation.SERVICE_PROVIDER -> serviceProviderClassification
            ServiceProviderRecommendation.THIRD_PARTY -> thirdPartyClassification
            ServiceProviderRecommendation.CONTRACTOR -> contractorClassification
            null -> unknownClassification!!
        }
    }
}
