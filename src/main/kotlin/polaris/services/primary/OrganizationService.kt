package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.dto.ApiMode
import polaris.models.dto.SurveySlug
import polaris.models.dto.organization.EmploymentGroupNotices
import polaris.models.dto.organization.PublicOrganizationNoticeDto
import polaris.models.dto.organization.PublicPrivacyCenterDto
import polaris.models.dto.organization.UpdateOrganization
import polaris.models.dto.organization.UpdateOrganizationComplinaceChecklistItemDto
import polaris.models.dto.organization.UpdateOrganizationCustomFieldDto
import polaris.models.dto.organization.UpdateOrganizationFinancialIncentivesRequest
import polaris.models.dto.organization.UpdateOrganizationGoogleAnalyticsWebPropertyDto
import polaris.models.dto.organization.UpdateOrganizationMessageSettings
import polaris.models.dto.organization.UpdateOrganizationPrivacyNoticeProgress
import polaris.models.dto.organization.UpdateOrganizationSurveyQuestionDto
import polaris.models.dto.request.UpdateOrganizationVerificationInstruction
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.PrivacyCenterNotFound
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.feature.Features
import polaris.models.entity.feature.OrganizationFeature
import polaris.models.entity.organization.BUSINESS_SURVEY_NAME
import polaris.models.entity.organization.EventData
import polaris.models.entity.organization.FINANCIAL_INCENTIVES_SURVEY_NAME
import polaris.models.entity.organization.GetCompliantProgress
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.organization.OptOutPageType
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationComplianceChecklistItem
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.organization.OrganizationRegulation
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.Regulation
import polaris.models.entity.request.RequestWorkflow
import polaris.models.entity.user.User
import polaris.repositories.FeatureRepository
import polaris.repositories.OrgCookieRepo
import polaris.repositories.OrganizationComplianceChecklistItemRepository
import polaris.repositories.OrganizationFeatureRepository
import polaris.repositories.OrganizationMailboxRepository
import polaris.repositories.OrganizationRegulationRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.RegulationRepository
import polaris.services.NylasSendError
import polaris.services.dto.OrganizationCookieDtoService
import polaris.services.support.BlockStorageService
import polaris.services.support.CookieScannerService
import polaris.services.support.EmailService
import polaris.services.support.OptOutService
import polaris.services.support.PrivacyCenterBuilderService
import polaris.services.support.SurveyProgressService
import polaris.services.support.TestTemplateMessageService
import polaris.util.PolarisException
import java.time.ZonedDateTime
import java.util.UUID
import javax.transaction.Transactional

enum class RequestTypeVisbility {
    POLARIS,
    CONSUMER_FORM
}

@Service
@Transactional
class OrganizationService(
    private val blockStorageService: BlockStorageService,
    private val organizationComplianceChecklistItemRepository: OrganizationComplianceChecklistItemRepository,
    private val organizationRepository: OrganizationRepository,
    private val organizationRegulationRepository: OrganizationRegulationRepository,
    private val organizationMailboxRepository: OrganizationMailboxRepository,
    private val regulationRepository: RegulationRepository,
    private val testTemplateMessageService: TestTemplateMessageService,
    private val emailService: EmailService,
    private val featureRepository: FeatureRepository,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService,
    private val organizationSurveyService: OrganizationSurveyService,
    private val taskUpdateService: TaskUpdateService,
    private val optOutService: OptOutService,
    private val orgFeatureRepo: OrganizationFeatureRepository,
    private val orgCookieRepo: OrgCookieRepo,
    private val cookieDto: OrganizationCookieDtoService,
    private val surveyProgressService: SurveyProgressService,
    private val lookup: LookupService,
    private val cookieScannerService: CookieScannerService,
    private val cmpConfigService: CmpConfigService
) {

    companion object {
        private val logger = LoggerFactory.getLogger(OrganizationService::class.java)
    }

    fun findByPublicId(organizationId: OrganizationPublicId) =
        organizationRepository.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)

    fun getDataRequestTypes(
        organizationId: OrganizationPublicId,
        filterVisible: RequestTypeVisbility?,
        regulations: List<Regulation>?
    ): List<DataRequestType> {
        val org = lookup.orgOrThrow(organizationId)
        val pc = org.privacyCenter() ?: throw PrivacyCenterNotFound(organizationId.toString())
        val orgRegulations: List<Regulation> = listOfNotNull(
            if (!pc.hideCcpaSection) { Regulation.CCPA } else { null },
            if (org.isMultistate()) { Regulation.VCDPA } else { null },
            if (org.isMultistate()) { Regulation.CPA } else { null },
            if (org.isMultistate()) { Regulation.CTDPA } else { null },
            if (org.privacyCenter()?.caResidencyConfirmation == false) { Regulation.VOLUNTARY } else { null },
            if (org.isGdpr()) { Regulation.GDPR } else { null }
        )
        val selectedRegulations = regulations?.filter { orgRegulations.contains(it) } ?: orgRegulations

        val hideManualOptOut = !optOutService.processesOptOut(org)

        val allTypes = DataRequestType.values().toList()

        val requestTypes = allTypes.filter { t -> selectedRegulations.contains(t.regulation) }
        logger.info("Request types matching regulation $selectedRegulations: $requestTypes")
        val visibleTypes =
            when (filterVisible) {
                null -> requestTypes
                RequestTypeVisbility.CONSUMER_FORM -> requestTypes.filter { it.canSubjectSubmit }
                RequestTypeVisbility.POLARIS -> requestTypes.filter { it.canPolarisSubmit }
            }
        logger.info("Request types matching visibility ${filterVisible ?: "(ALL)"}: $requestTypes")

        val types = when (visibleTypes.any { it.workflow == RequestWorkflow.OPT_OUT }) {
            true -> {
                val enableOptOut = !hideManualOptOut
                visibleTypes.filter { drt -> drt.workflow != RequestWorkflow.OPT_OUT || enableOptOut }
            }
            false -> visibleTypes
        }

        return types
    }

    fun getDataRequestTypes(organizationId: OrganizationPublicId, filterVisible: RequestTypeVisbility?): List<DataRequestType> =
        getDataRequestTypes(organizationId, filterVisible, null)

    fun mailboxesForOrganization(organizationId: OrganizationPublicId): List<OrganizationMailbox> {
        val org = lookup.orgOrThrow(organizationId)
        return org.organizationMailboxes
    }

    fun featuresForOrganization(organizationId: OrganizationPublicId): List<OrganizationFeature> {
        val org = lookup.orgOrThrow(organizationId)
        val features = featureRepository.findAll()
        return features.filter {
            (it.startTs == null || it.startTs!! <= ZonedDateTime.now()) &&
                (it.endTs == null || it.endTs!! >= ZonedDateTime.now())
        }.map { feature ->
            org.organizationFeatures.firstOrNull { it.feature!!.id == feature.id } ?: OrganizationFeature(
                organization = org,
                feature = feature,
                enabled = feature.enabledByDefault ?: false
            )
        }
    }

    fun getSurveyQuestions(organizationId: OrganizationPublicId, surveyName: String, mode: ApiMode = ApiMode.Live): List<OrganizationSurveyQuestion> {
        return organizationSurveyService.getSurveyQuestions(organizationId, mode, surveyName)
    }

    fun updateSurveyQuestions(
        organizationId: OrganizationPublicId,
        surveyName: String,
        updateQuestionsDto: List<UpdateOrganizationSurveyQuestionDto>,
        events: MutableList<EventData>,
        apiMode: ApiMode = ApiMode.Live
    ) {
        return organizationSurveyService.updateSurveyQuestions(organizationId, surveyName, updateQuestionsDto, events, apiMode)
    }

    fun getComplianceChecklistItems(organizationId: OrganizationPublicId) =
        organizationComplianceChecklistItemRepository.findAllByOrganization(lookup.orgOrThrow(organizationId))

    fun updateComplianceChecklistItems(
        organizationId: OrganizationPublicId,
        updateChecklistItemsDto: List<UpdateOrganizationComplinaceChecklistItemDto>
    ) {
        val organization = lookup.orgOrThrow(organizationId)
        val existingBySlug = getComplianceChecklistItems(organizationId).associateBy { it.slug }
        organizationComplianceChecklistItemRepository.saveAll(
            updateChecklistItemsDto.map { checklistItem ->
                val existing = existingBySlug[checklistItem.slug]
                if (existing != null) {
                    checklistItem.description.let { existing.description = checklistItem.description }
                    existing.done = checklistItem.done
                    existing
                } else {
                    OrganizationComplianceChecklistItem(
                        organization = organization,
                        slug = checklistItem.slug,
                        description = checklistItem.description,
                        done = checklistItem.done
                    )
                }
            }
        )
    }

    fun updateFinancialIncentiveDetails(
        organizationId: OrganizationPublicId,
        dto: UpdateOrganizationFinancialIncentivesRequest,
        user: User
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)

        org.financialIncentiveDetails = dto.financialIncentiveDetails

        organizationRepository.save(org)

        privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(org, "organization financial incentive details update")
        taskUpdateService.updateTasks(org, create = true, close = true, actor = user)

        return org
    }

    fun updateCustomField(
        organizationId: OrganizationPublicId,
        dto: UpdateOrganizationCustomFieldDto
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)

        org.customFieldEnabled = dto.customFieldEnabled
        org.customFieldLabel = dto.customFieldLabel
        org.customFieldHelp = dto.customFieldHelp

        organizationRepository.save(org)

        privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(org, "organization custom field update")

        return org
    }

    fun updateGoogleAnalyticsWebProperty(
        organizationId: OrganizationPublicId,
        dto: UpdateOrganizationGoogleAnalyticsWebPropertyDto
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)

        org.gaWebProperty = dto.webProperty
        organizationRepository.save(org)

        return org
    }

    fun updateMessageSettings(
        actor: User,
        organizationId: OrganizationPublicId,
        dto: UpdateOrganizationMessageSettings
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)
        if (!org.headerImageUrl.isBlank() && dto.headerImageUrl != org.headerImageUrl) {
            blockStorageService.deleteImageByUrl(org.headerImageUrl)
        }
        org.headerImageUrl = dto.headerImageUrl
        org.physicalAddress = dto.physicalAddress
        org.messageSignature = dto.messageSignature
        organizationRepository.save(org)

        taskUpdateService.updateTasks(org, create = false, close = true, actor = actor)
        return org
    }

    fun updateVerificationInstruction(
        organizationId: OrganizationPublicId,
        dto: UpdateOrganizationVerificationInstruction
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)
        val regulation = regulationRepository.findBySlug(dto.regulationSlug)
            ?: throw PolarisException("Unable to find Regulation ${dto.regulationSlug}")
        val orgReg = organizationRegulationRepository.findByOrganizationAndRegulation(org, regulation)
            ?: throw PolarisException("Organization $organizationId has not enabled Regulation ${regulation.name}")
        orgReg.identityVerificationInstruction = dto.instruction
        organizationRegulationRepository.save(orgReg)
        organizationRepository.refresh(org)
        return org
    }

    fun sendSurveyEmail(organizationId: OrganizationPublicId, surveyName: String) {
        val org = lookup.orgOrThrow(organizationId)
        emailService.sendIntroSurveyEmailToStaff(
            org,
            getSurveyQuestions(organizationId, surveyName, ApiMode.Live).filter { !it.answer.isNullOrEmpty() }
        )
    }

    fun updateGetCompliantProgress(
        organizationId: OrganizationPublicId,
        getCompliantProgress: String,
        events: MutableList<EventData>
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)
        // TODO: POLARIS-1952 - wire up event tracking
        surveyProgressService.updateSurveyProgress(org, SurveySlug.onboarding, getCompliantProgress)
        return org
    }

    fun addGetCompliantInvalidatedProgress(
        organizationId: OrganizationPublicId,
        getCompliantInvalidatedProgress: String
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)
        if (!org.getCompliantInvalidatedProgressSteps.contains(getCompliantInvalidatedProgress)) {
            org.getCompliantInvalidatedProgressSteps = org.getCompliantInvalidatedProgressSteps + getCompliantInvalidatedProgress
            organizationRepository.save(org)
        }
        return org
    }

    fun removeGetCompliantInvalidatedProgress(
        organizationId: OrganizationPublicId,
        getCompliantInvalidatedProgress: String
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)
        val index = org.getCompliantInvalidatedProgressSteps.indexOf(getCompliantInvalidatedProgress)
        if (index != -1) {
            org.getCompliantInvalidatedProgressSteps = org.getCompliantInvalidatedProgressSteps.minus(getCompliantInvalidatedProgress)
        }
        organizationRepository.save(org)
        return org
    }

    fun updateExceptionsMappingProgress(
        organizationId: OrganizationPublicId,
        mappingProgress: MappingProgressEnum
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)
        if (org.exceptionsMappingProgress != mappingProgress) {
            org.exceptionsMappingProgress = mappingProgress
            organizationRepository.save(org)
        }
        return org
    }

    fun updateCookiesMappingProgress(
        organizationId: OrganizationPublicId,
        mappingProgress: MappingProgressEnum
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)
        if (org.cookiesMappingProgress != mappingProgress) {
            org.cookiesMappingProgress = mappingProgress
            organizationRepository.save(org)
        }
        return org
    }

    fun updatePrivacyNoticeProgress(
        organizationId: OrganizationPublicId,
        updatePrivacyNoticeProgress: UpdateOrganizationPrivacyNoticeProgress
    ): Organization {
        val org = lookup.orgOrThrow(organizationId)
        if (
            org.caPrivacyNoticeProgress != updatePrivacyNoticeProgress.caPrivacyNoticeProgress ||
            org.optOutPrivacyNoticeProgress != updatePrivacyNoticeProgress.optOutPrivacyNoticeProgress
        ) {
            org.caPrivacyNoticeProgress = updatePrivacyNoticeProgress.caPrivacyNoticeProgress
            org.optOutPrivacyNoticeProgress = updatePrivacyNoticeProgress.optOutPrivacyNoticeProgress
            organizationRepository.save(org)
        }
        return org
    }

    fun deleteMailbox(organizationId: OrganizationPublicId, mailbox: String) {
        val org = lookup.orgOrThrow(organizationId)
        val box = org.organizationMailboxes.find { it.mailbox == mailbox }

        if (box != null) {
            organizationMailboxRepository.delete(box)
        }
    }

    fun returnToGetCompliant(organizationId: OrganizationPublicId) {
        val org = lookup.orgOrThrow(organizationId)
        updateOrgFeature(org, Features.GET_COMPLIANT_PHASE, true)
        // TODO: POLARIS-1952 - wire up event tracking
        surveyProgressService.updateSurveyProgress(org, SurveySlug.onboarding, GetCompliantProgress.STARTING_PROGRESS)
    }

    fun automatedReturnToStayCompliant(organizationId: OrganizationPublicId, force: Boolean): Boolean {
        val org = lookup.orgOrThrow(organizationId)
        if (org.trainingComplete == true || force) {
            updateOrgFeature(org, Features.GET_COMPLIANT_PHASE, false)
            surveyProgressService.completeSurvey(org, SurveySlug.onboarding)
            if (org.trainingComplete == null || org.trainingComplete == false) {
                org.trainingComplete = true
                organizationRepository.save(org)
            }
            return true
        }
        return false
    }

    private fun updateOrgFeature(org: Organization, feature: String, enabled: Boolean) {
        val orgFeature = lookup.orgFeatureOrDefault(org, feature)
        orgFeature.enabled = enabled
        orgFeatureRepo.save(orgFeature)
    }

    fun updateOrganization(organizationId: OrganizationPublicId, dto: UpdateOrganization): Organization {
        val org = lookup.orgOrThrow(organizationId)

        org.name = dto.name
        if (!org.logoUrl.isNullOrBlank() && dto.logoUrl != org.logoUrl) {
            blockStorageService.deleteImageByUrl(org.logoUrl!!)
        }
        org.logoUrl = dto.logoUrl
        if (!org.faviconUrl.isNullOrBlank() && dto.faviconUrl != org.faviconUrl) {
            blockStorageService.deleteImageByUrl(org.faviconUrl!!)
        }
        org.faviconUrl = dto.faviconUrl

        val oldIsServiceProvider = org.isServiceProvider
        org.isServiceProvider = dto.isServiceProvider

        organizationRepository.save(org)

        val existingRegulations: List<UUID> = org.regulations.map { it.id }
        dto.regulations.forEach {
            if (it.enabled && !existingRegulations.contains(UUID.fromString(it.id))) {
                regulationRepository.findByIdOrNull(UUID.fromString(it.id))?.let { reg ->
                    organizationRegulationRepository.save(OrganizationRegulation(regulation = reg, organization = org))
                }
            }
            if (!it.enabled && existingRegulations.contains(UUID.fromString(it.id))) {
                org.regulations.filter { reg -> reg.id == UUID.fromString(it.id) }.firstOrNull()?.let { reg ->
                    organizationRegulationRepository.findByOrganizationAndRegulation(org, reg)?.let { orgReg ->
                        organizationRegulationRepository.delete(orgReg)
                    }
                }
            }
        }

        val serviceProviderUpdated = org.isServiceProvider != oldIsServiceProvider
        val needsRebuild = serviceProviderUpdated

        if (needsRebuild) {
            privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(org, "organization updated")
        }

        // Update polaris JSON configuration
        cmpConfigService.publishForOrg(org)

        return organizationRepository.findByPublicId(organizationId)!!
    }

    fun getPublicNotices(instructionId: UUID): PublicOrganizationNoticeDto {
        val org = lookup.orgByDevInstructionsId(instructionId) ?: lookup.orgByHrInstructionsId(instructionId) ?: throw OrganizationNotFound(instructionId.toString())
        val pc = org.privacyCenter() ?: throw PrivacyCenterNotFound(instructionId.toString())

        val opt = optOutService.getOptOutPageType(org)
        val cookies = orgCookieRepo.findAllByOrganization(org)
        val cookieScan = cookieScannerService.getCookieScanResult(org.publicId)

        val type = if (org.devInstructionsUuid == instructionId) "dev" else "hr"
        val finIncentives = getSurveyQuestions(org.publicId, FINANCIAL_INCENTIVES_SURVEY_NAME, ApiMode.Live)
        val bizSurvey = getSurveyQuestions(org.publicId, BUSINESS_SURVEY_NAME, ApiMode.Live)

        val caJobApplicants = org.collectionGroups.find { it.autocreationSlug == AutocreatedCollectionGroupSlug.CA_JOB_APPLICANTS || it.name.toLowerCase().contains("california job applicant") }

        val employmentGroups = org.collectionGroups.filter { it.collectionGroupType == CollectionGroupType.EMPLOYMENT && it.id != caJobApplicants?.id }

        val empNotices = employmentGroups.map {
            val isContractorGroup = it.autocreationSlug == AutocreatedCollectionGroupSlug.CA_CONTRACTORS || it.name.toLowerCase().contains("california contractor")
            val isEeaEmployees = it.autocreationSlug == AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE
            val url = if (isContractorGroup) { pc.getContractorNoticeUrl() } else if (isEeaEmployees) { pc.getEeaUkEmployeeNoticeUrl() } else { pc.getEmployeeNoticeUrl() }
            EmploymentGroupNotices(name = it.name, url = url)
        }

        return PublicOrganizationNoticeDto(
            type = type,
            privacyCenter = PublicPrivacyCenterDto(url = org.privacyCenter()!!.getBaseUrl(), id = org.privacyCenter()!!.publicId),
            isSelling = opt == OptOutPageType.SELLING || opt == OptOutPageType.SELLING_AND_SHARING,
            isSharing = opt == OptOutPageType.SHARING || opt == OptOutPageType.SELLING_AND_SHARING,
            isGoogleAnalytics = org.organizationDataRecipients.any { it.vendor?.vendorKey == "google-analytics" },
            isGdpr = org.isGdpr(),
            showLimitNotice = requiresNoticeOfRightToLimit(org),
            showFinancialIncentive = finIncentives.find { it.slug == "offer-consumer-incentives" }?.answer == "true",
            hasJobApplicants = caJobApplicants != null,
            hasPhysicalLocation = bizSurvey.find { it.slug == "business-physical-location" }?.answer == "true",
            hasMobileApplication = bizSurvey.find { it.slug == "business-has-mobile-apps" }?.answer == "true",
            employmentGroupNotices = empNotices,
            cookies = cookies.map { cookieDto.toDto(it) },
            cookieScanUrl = cookieScan.scanUrl
        )
    }

    /**
     * Send a user a test email message which will ensure the existing message settings work
     */
    fun messageSettingsTestEmail(user: User, organizationId: OrganizationPublicId) {
        val organization = lookup.orgOrThrow(organizationId)
        val body = testTemplateMessageService.testMessageTemplate(user, organization)
        val result = emailService.sendTestEmail(user, organization, body)
        if (result.statusCode == 500) {
            throw NylasSendError()
        }
    }

    fun requiresAssociations(org: Organization): Boolean {
        val collectionGroups = org.collectionGroups.filter { it.collectionGroupType.isNotEmployment() }
        return collectionGroups.size > 1
    }

    fun requiresNoticeOfRightToLimit(org: Organization): Boolean {
        val collectionGroups = org.collectionGroups
        val collectionGroupSurveys = collectionGroups.map { organizationSurveyService.getSurveyQuestions(org.publicId, ApiMode.Live, "consumer-data-collection-${it.id}") }

        // Did we answer yes for making inferences from sensitive information for any collection group
        return collectionGroupSurveys.any { survey -> survey.any { q -> q.slug == "inferences-from-sensitive-data" && q.answer == "true" } }
    }
}
