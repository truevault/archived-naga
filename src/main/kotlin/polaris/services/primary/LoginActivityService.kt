package polaris.services.primary

import org.springframework.stereotype.Service
import polaris.models.dto.user.UserDetailsDto
import polaris.models.entity.user.LoginActivity
import polaris.models.entity.user.UserStatus
import polaris.repositories.LoginActivityRepository
import polaris.repositories.UserRepository
import polaris.services.dto.UserDtoService
import java.time.ZonedDateTime
import javax.transaction.Transactional

@Service
class LoginActivityService(
    private val loginActivityRepository: LoginActivityRepository,
    private val userRepository: UserRepository,
    private val userService: UserService,
    private val userDtoService: UserDtoService
) {
    @Transactional
    fun handleLogin(email: String): UserDetailsDto {
        val user = userService.findByEmail(email)!!
        recordLoginAttempt(email = user.email, success = true)
        // Update the user's last login time
        user.lastLogin = ZonedDateTime.now()

        // If the user successfully logged in, they don't need to reset their password anymore.
        user.updatePasswordId = null
        userRepository.save(user)

        return userDtoService.toDetailsDto(user)
    }

    @Transactional
    fun recordLoginAttempt(email: String?, success: Boolean) {

        if (email != null) {
            val user = userService.findByEmail(email)
            val activity = if (user != null) {
                val activeOrganization = user.organization_users.filter { it.status == UserStatus.ACTIVE }.first().organization
                LoginActivity(email = email, success = success, userId = user.id, organizationId = activeOrganization?.id)
            } else {
                LoginActivity(email = email, success = success)
            }

            loginActivityRepository.save(activity)
        }
    }
}
