package polaris.services.primary

fun <T> getAddRemoveFromExisting(new: List<T>, existing: List<T>): Pair<List<T>, List<T>> {
    return Pair(new.subtract(existing).toList(), existing.subtract(new).toList())
}
