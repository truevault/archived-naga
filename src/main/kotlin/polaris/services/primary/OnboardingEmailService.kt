package polaris.services.primary

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import polaris.models.entity.request.DataSubjectRequest
import polaris.repositories.OrganizationRepository
import polaris.services.support.EmailService
import javax.transaction.Transactional

@Service
@Transactional
class OnboardingEmailService(
    private val organizationRepository: OrganizationRepository, // Onboarding email repo
    private val emailService: EmailService
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(OnboardingEmailService::class.java)
    }

    fun sendOnboardingEmailOnFirst(request: DataSubjectRequest) {
        val organization = request.organization
        log.info("Determining if walkthrough invite should be sent to ${organization?.publicId}")
        if (organization != null) {
            val walkthroughSent = organization.walkthroughEmailSent ?: true
            val trainingComplete = organization.trainingComplete ?: true
            if (walkthroughSent || !trainingComplete) {
                return
            } else {
                log.info("Sending walkthrough invite to ${organization.publicId}")
                emailService.sendOnboardingEmail(request)
                organization.walkthroughEmailSent = true
                organizationRepository.save(organization)
            }
        }
    }
}
