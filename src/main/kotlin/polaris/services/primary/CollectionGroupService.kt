package polaris.services.primary

import org.hibernate.exception.ConstraintViolationException
import org.slf4j.LoggerFactory
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import polaris.models.OrganizationPublicId
import polaris.models.dto.organization.CollectionGroupDetailsDto
import polaris.models.dto.organization.CollectionGroupMappingProgressDto
import polaris.models.dto.organization.UpdateCollectionGroupDto
import polaris.models.entity.CollectionGroupNotFound
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.compliance.PersonalInformationType
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.CollectionGroupMappingProgress
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.PrivacyNoticeProgress
import polaris.models.entity.organization.privacyNoticeProgressDto
import polaris.models.entity.vendor.ProcessingRegion
import polaris.repositories.CollectionGroupCollectedPICRepo
import polaris.repositories.CollectionGroupMappingProgressRepository
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.DataRecipientCollectionGroupRepo
import polaris.repositories.DataRecipientDisclosedPICRepo
import polaris.repositories.DataSourceReceivedPICRepo
import polaris.repositories.OrganizationBusinessPurposeRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.PrivacyRequestRetentionReasonRepository
import polaris.repositories.ProcessingInstructionsRepository
import polaris.util.PolarisException
import java.sql.SQLException
import java.time.ZonedDateTime
import java.util.UUID
import javax.transaction.Transactional

@Service
@Transactional
class CollectionGroupService(
    private val collectionGroupMappingProgressRepository: CollectionGroupMappingProgressRepository,
    private val collectionGroupRepository: CollectionGroupRepository,
    private val organizationRepository: OrganizationRepository,
    private val organizationDataRecipientRepository: OrganizationDataRecipientRepository,
    private val collectedPICRepo: CollectionGroupCollectedPICRepo,
    private val receivedPICRepo: DataSourceReceivedPICRepo,
    private val orgBusinessPurposeRepo: OrganizationBusinessPurposeRepository,
    private val exchangedPICRepo: DataRecipientDisclosedPICRepo,
    private val processingInstructionsRepository: ProcessingInstructionsRepository,
    private val privacyRequestRetentionReasonRepository: PrivacyRequestRetentionReasonRepository,
    private val dataRecipientCollectionGroupRepo: DataRecipientCollectionGroupRepo
) {
    companion object {
        private val logger = LoggerFactory.getLogger(CollectionGroupService::class.java)
    }

    fun findAllByOrganization(organizationId: OrganizationPublicId): List<CollectionGroup> {
        val organization: Organization = lookupOrganizationOrThrow(organizationId)
        return collectionGroupRepository.findAllByOrganization(organization).sortedBy { it.name }
    }

    fun findAllByOrganizationAndType(
        organizationId: OrganizationPublicId,
        types: List<CollectionGroupType>
    ): List<CollectionGroup> {
        val organization: Organization = lookupOrganizationOrThrow(organizationId)
        return collectionGroupRepository.findAllByOrganizationAndCollectionGroupTypeIn(organization, types).sortedBy { it.name }
    }

    fun findByIdAndOrganization(id: UUID, organization: Organization): CollectionGroup {
        val type = lookupDataSubjectTypeOrThrow(organization.publicId, id)
        if (type.organization != organization) {
            throw PolarisException("Data Subject Type $id not found")
        }
        return type
    }

    fun create(
        type: String,
        organizationId: OrganizationPublicId,
        consumerGroupType: CollectionGroupType? = null,
        gdprDataSubject: Boolean = false
    ): CollectionGroup {

        val existingTypes = findAllByOrganization(organizationId)
        if (existingTypes.map { it.name }.intersect(listOf(type)).isNotEmpty()) {
            throw PolarisException("Data Subject Type $type conflicts with an existing type")
        }

        val resetProgress = consumerGroupType?.isNotEmployment() ?: true

        val organization = lookupOrganizationOrThrow(organizationId)
        val newConsumerGroup = collectionGroupRepository.save(
            CollectionGroup(
                organization = organization,
                name = type,
                createdAsDataSubject = gdprDataSubject
            )
        )

        // reset all mappings
        if (resetProgress) {
            val orgVendors = organizationDataRecipientRepository.findAllByOrganization(organization)
            val doneVendors = orgVendors.filter { it.mappingProgress == MappingProgressEnum.DONE }
            if (doneVendors.isNotEmpty()) {
                doneVendors.forEach { it.mappingProgress = MappingProgressEnum.IN_PROGRESS }
                organizationDataRecipientRepository.saveAll(doneVendors)
            }
        }

        return newConsumerGroup
    }

    fun delete(organizationId: OrganizationPublicId, dataSubjectTypeId: UUID) {
        val org = lookupOrganizationOrThrow(organizationId)
        val collectionGroup = findByIdAndOrganization(dataSubjectTypeId, org)
        val piCollection = collectedPICRepo.findAllByCollectionGroup(collectionGroup)
        val piExchanged = exchangedPICRepo.findAllByCollectedPIC_CollectionGroup(collectionGroup)
        val piReceived = piCollection.flatMap { it.receivedFromSources }
        val cgMappingProgress = collectionGroupMappingProgressRepository.findAllByCollectionGroup(collectionGroup)
        val legacyInstructions =
            processingInstructionsRepository.findAllByOrganizationAndCollectionGroup(org, collectionGroup)
        val retentionReasons = privacyRequestRetentionReasonRepository.findAllByCollectionGroup(collectionGroup)
        val associations = dataRecipientCollectionGroupRepo.findAllByCollectionGroup(collectionGroup)

        try {
            if (retentionReasons.isNotEmpty()) {
                privacyRequestRetentionReasonRepository.deleteAll(retentionReasons)
            }
            if (cgMappingProgress.isNotEmpty()) {
                collectionGroupMappingProgressRepository.deleteAll(cgMappingProgress)
            }
            if (legacyInstructions.isNotEmpty()) {
                processingInstructionsRepository.deleteAll(legacyInstructions)
            }
            if (piExchanged.isNotEmpty()) {
                exchangedPICRepo.deleteAll(piExchanged)
            }
            if (piReceived.isNotEmpty()) {
                receivedPICRepo.deleteAll(piReceived)
            }
            if (piCollection.isNotEmpty()) {
                collectedPICRepo.deleteAll(piCollection)
            }
            if (associations.isNotEmpty()) {
                dataRecipientCollectionGroupRepo.deleteAll(associations)
            }

            collectionGroup.deletedAt = ZonedDateTime.now()
            collectionGroupRepository.save(collectionGroup)
        } catch (e: SQLException) {
            throw PolarisException("Unable to delete ${collectionGroup.name} as it is currently in use")
        } catch (e: ConstraintViolationException) {
            throw PolarisException("Unable to delete ${collectionGroup.name} as it is currently in use")
        } catch (e: DataIntegrityViolationException) {
            throw PolarisException("Unable to delete ${collectionGroup.name} as it is currently in use")
        }
    }

    fun update(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID,
        updateDto: UpdateCollectionGroupDto
    ): CollectionGroup {
        val organization = lookupOrganizationOrThrow(organizationId)
        val collectionGroup = findByIdAndOrganization(dataSubjectTypeId, organization)
        if (
            updateDto.name != collectionGroup.name ||
            updateDto.description != collectionGroup.description ||
            updateDto.enabled != collectionGroup.enabled ||
            (updateDto.collectionGroupType != null && updateDto.collectionGroupType != collectionGroup.collectionGroupType) ||
            updateDto.gdprEeaUkDataCollection != collectionGroup.gdprEeaUkDataCollection ||
            updateDto.processingRegions != null ||
            updateDto.privacyNoticeIntroText != collectionGroup.privacyNoticeIntroText
        ) {
            val oldConsumerGroupType = collectionGroup.collectionGroupType
            collectionGroup.name = updateDto.name ?: collectionGroup.name
            collectionGroup.enabled = updateDto.enabled ?: collectionGroup.enabled
            collectionGroup.collectionGroupType = updateDto.collectionGroupType ?: collectionGroup.collectionGroupType
            collectionGroup.description = updateDto.description ?: ""
            collectionGroup.gdprEeaUkDataCollection = updateDto.gdprEeaUkDataCollection ?: collectionGroup.gdprEeaUkDataCollection // Check on this
            collectionGroup.processingRegions = updateDto.processingRegions ?: collectionGroup.processingRegions
            collectionGroup.privacyNoticeIntroText = updateDto.privacyNoticeIntroText ?: collectionGroup.privacyNoticeIntroText
            autoSources(collectionGroup, oldConsumerGroupType)
            collectionGroupRepository.save(collectionGroup)
        }
        return collectionGroup
    }

    private fun autoSources(cg: CollectionGroup, oldCollectionGroupType: CollectionGroupType?) {
        logger.info("automatically adding sources and purposes for dst: $cg")
        val newCollectionGroupType = cg.collectionGroupType

        if (oldCollectionGroupType == newCollectionGroupType) {
            logger.debug("No consumer group type change, skipping automatic sources and purposes")
            return
        }
    }

    fun updateMappingProgress(
        organizationId: OrganizationPublicId,
        dataSubjectTypeId: UUID,
        informationType: PersonalInformationType,
        mappingProgress: MappingProgressEnum
    ): CollectionGroup {
        val organization = lookupOrganizationOrThrow(organizationId)
        val dataSubjectType = findByIdAndOrganization(dataSubjectTypeId, organization)
        val consumerGroupMappingProgress =
            collectionGroupMappingProgressRepository.findByCollectionGroupAndPersonalInformationType(
                dataSubjectType, informationType
            )
        if (consumerGroupMappingProgress == null) {
            collectionGroupMappingProgressRepository.save(
                CollectionGroupMappingProgress(
                    collectionGroup = dataSubjectType,
                    personalInformationType = informationType,
                    mappingProgress = mappingProgress
                )
            )
        } else if (consumerGroupMappingProgress.mappingProgress != mappingProgress) {
            consumerGroupMappingProgress.mappingProgress = mappingProgress
            collectionGroupMappingProgressRepository.save(consumerGroupMappingProgress)
        }
        return findByIdAndOrganization(dataSubjectTypeId, organization)
    }

    fun updatePrivacyNoticeProgress(
        organizationId: OrganizationPublicId,
        consumerGroupId: UUID,
        privacyNoticeProgress: PrivacyNoticeProgress
    ): CollectionGroup {
        val consumerGroup = lookupDataSubjectTypeOrThrow(organizationId, consumerGroupId)
        if (consumerGroup.privacyNoticeProgress != privacyNoticeProgress) {
            consumerGroup.privacyNoticeProgress = privacyNoticeProgress
            collectionGroupRepository.save(consumerGroup)
        }
        return consumerGroup
    }

    fun isInUse(collectionGroup: CollectionGroup) =
        collectionGroup.dataSubjectRequests.isNotEmpty() || collectedPICRepo.existsByCollectionGroup(
            collectionGroup
        )

    fun getEmployeeCollectionGroupNoticeIntro(organization: Organization, group: CollectionGroup): String {
        val privacyUrl = organization.privacyCenter()?.privacyPolicyUrl ?: organization.privacyPolicyUrl
        return getEmployeeCollectionGroupNoticeIntro(organization, group, privacyUrl)
    }

    // wondering if this logic can be entirely moved into a getter
    fun getEmployeeCollectionGroupNoticeIntro(
        organization: Organization,
        group: CollectionGroup,
        privacyUrl: String?
    ): String {
        val privacyCenterLink =
            privacyUrl?.let { "Visit <a href=\"${it}\" target=\"_blank\">here</a> for our general privacy policy applicable to consumers." }
                ?: ""

        val ccpaIntroNotice =
            "<p>This Notice is provided pursuant to the California Consumer Privacy Act (CCPA) and discloses what personal information ${organization.name} collects about ${group.name}. This Notice applies only to information collected about ${group.name} in that role. $privacyCenterLink This Notice does not apply to information that is not considered \"personal information,\" such as anonymous, deidentified, or aggregated information, nor does it apply to publicly available information as defined in the CCPA.</p>"
        val gdprIntroNotice =
            "<p>This notice provides information under the General Data Protection Regulation and the UK Data Protection Act (GDPR) for ${organization.name} employees in the European Economic Area (EEA) or United Kingdom (UK). The terms used have the same  meaning as in the GDPR.</p>"

        val eeaUkOnly = group.processingRegions.contains(ProcessingRegion.EEA_UK) && !group.processingRegions.contains(
            ProcessingRegion.UNITED_STATES
        )

        val defaultNoticeIntro = if (eeaUkOnly) gdprIntroNotice else ccpaIntroNotice

        // There is upstream logic ensuring that we only get here from ccpa employee, not sure if we want to add more validation
        // using const because it's complaining about it being mutable and won't allow ! operator with field
        return group.privacyNoticeIntroText ?: defaultNoticeIntro
    }

    fun toDetailsDto(orgId: OrganizationPublicId, collectionGroup: CollectionGroup): CollectionGroupDetailsDto {
        val org = lookupOrganizationOrThrow(orgId)
        val progressByType = collectionGroup.mappingProgress.associateBy { it.personalInformationType }
        return CollectionGroupDetailsDto(
            id = collectionGroup.id.toString(),
            name = collectionGroup.name,
            description = collectionGroup.description,
            inUse = isInUse(collectionGroup),
            enabled = collectionGroup.enabled,
            collectionGroupType = collectionGroup.collectionGroupType,
            autocreationSlug = collectionGroup.autocreationSlug,
            mappingProgress = enumValues<PersonalInformationType>().map {
                CollectionGroupMappingProgressDto(
                    personalInformationType = it,
                    value = progressByType[it]?.mappingProgress ?: MappingProgressEnum.NOT_STARTED,
                    label = progressByType[it]?.mappingProgress?.label ?: MappingProgressEnum.NOT_STARTED.label
                )
            },
            privacyNoticeProgress = privacyNoticeProgressDto(collectionGroup.privacyNoticeProgress),
            gdprEeaUkDataCollection = collectionGroup.gdprEeaUkDataCollection,
            createdAsDataSubject = collectionGroup.createdAsDataSubject,
            processingRegions = collectionGroup.processingRegions,
            businessPurposes = orgBusinessPurposeRepo.findAllByOrganizationAndBusinessPurposeType(
                org,
                collectionGroup.collectionGroupType.businessPurposeType
            ).map { it.businessPurpose!! },
            privacyNoticeIntroText = getEmployeeCollectionGroupNoticeIntro(org, collectionGroup)
        )
    }

    private fun lookupDataSubjectTypeOrThrow(organizationId: OrganizationPublicId, dataSubjectTypeId: UUID) =
        collectionGroupRepository.findByIdOrNull(dataSubjectTypeId)
            ?.let { if (it.organization!!.publicId == organizationId) it else null }
            ?: throw CollectionGroupNotFound(dataSubjectTypeId.toString())

    private fun lookupOrganizationOrThrow(organizationId: OrganizationPublicId): Organization =
        organizationRepository.findByPublicId(organizationId) ?: throw OrganizationNotFound(organizationId)
}
