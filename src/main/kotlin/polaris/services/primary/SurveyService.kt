package polaris.services.primary

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.repositories.OrganizationSurveyQuestionRepository

@Service
class SurveyService(
    private val surveyRepo: OrganizationSurveyQuestionRepository
) {
    @Transactional
    fun writeSurveyAnswer(org: Organization, answer: String?, questionText: String?, surveyName: String, slug: String) {
        val existing = surveyRepo.findByOrganizationAndSurveyNameAndSlug(org, surveyName, slug)
        var question: OrganizationSurveyQuestion? = null
        if (existing == null) {
            question = OrganizationSurveyQuestion(
                organization = org,
                surveyName = surveyName,
                slug = slug,
                question = questionText ?: "",
                answer = answer
            )
        } else {
            question = existing
            question.question = questionText ?: ""
            question.answer = answer
        }

        surveyRepo.save(question)
    }

    fun getAnswer(org: Organization, surveyName: String, slug: String): String? {
        val existing = surveyRepo.findByOrganizationAndSurveyNameAndSlug(org, surveyName, slug)
        return existing?.answer
    }
}
