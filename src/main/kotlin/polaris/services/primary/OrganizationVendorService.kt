package polaris.services.primary

import org.slf4j.LoggerFactory
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import polaris.controllers.util.LookupService
import polaris.models.OrganizationPublicId
import polaris.models.SurveySlugs.Companion.CUSTOM_AUDIENCE_QUESTION_SLUG
import polaris.models.SurveySlugs.Companion.DISCLOSURE_FOR_SELLING_SLUG
import polaris.models.SurveySlugs.Companion.SELLING_UPLOAD_DATA_SLUG
import polaris.models.SurveySlugs.Companion.VENDOR_SELLING_QUESTION_SLUG
import polaris.models.SurveySlugs.Companion.VENDOR_UPLOAD_DATA_SELECTION_SLUG
import polaris.models.dto.organization.OrganizationVendorNewAndEditDto
import polaris.models.dto.organization.OrganizationVendorSettingsDto
import polaris.models.dto.organization.RequestHandlingInstructionDto
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.VendorNotFound
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.DataAccessibilityEnum
import polaris.models.entity.organization.DataDeletabilityEnum
import polaris.models.entity.organization.DataRecipientCollectionGroup
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.OrganizationVendorStatus
import polaris.models.entity.organization.SELLING_AND_SHARING_SURVEY_NAME
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.models.entity.vendor.KnownVendorCategories
import polaris.models.entity.vendor.KnownVendorKey
import polaris.models.entity.vendor.ServiceProviderRecommendation
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER
import polaris.models.entity.vendor.Vendor
import polaris.repositories.DataRecipientCollectionGroupRepo
import polaris.repositories.DataRecipientDisclosedPICRepo
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.PlatformAppInstallRepository
import polaris.repositories.RequestHandlingInstructionsRepository
import polaris.repositories.VendorClassificationRepository
import polaris.repositories.VendorContractRepository
import polaris.repositories.VendorContractReviewedRepository
import polaris.repositories.VendorOutcomeRepository
import polaris.repositories.VendorRepository
import polaris.services.dto.OrganizationVendorDtoService
import polaris.services.support.PrivacyCenterBuilderService
import polaris.specs.OrganizationDataRecipientSpecs
import polaris.util.PolarisException
import java.time.ZonedDateTime
import java.util.UUID
import javax.transaction.Transactional

@Service
@Transactional
class OrganizationVendorService(
    private val organizationDataRecipientRepository: OrganizationDataRecipientRepository,
    private val organizationRepository: OrganizationRepository,
    private val requestHandlingInstructionsService: RequestHandlingInstructionsService,
    private val dataRecipientCollectionGroupRepo: DataRecipientCollectionGroupRepo,
    private val organizationVendorSurveyService: OrganizationVendorSurveyService,
    private val organizationVendorDtoService: OrganizationVendorDtoService,
    private val vendorPICDisclosureRepo: DataRecipientDisclosedPICRepo,
    private val vendorOutcomeRepository: VendorOutcomeRepository,
    private val vendorRepository: VendorRepository,
    private val vendorContractRepository: VendorContractRepository,
    private val vendorContractReviewedRepository: VendorContractReviewedRepository,
    private val vendorClassificationRepository: VendorClassificationRepository,
    private val privacyCenterBuilderService: PrivacyCenterBuilderService,
    private val platformAppInstallRepository: PlatformAppInstallRepository,
    private val requestHandlingInstructionsRepository: RequestHandlingInstructionsRepository,
    private val taskUpdateService: TaskUpdateService,
    private val organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository,
    private val lookup: LookupService
) {
    companion object {
        private val log = LoggerFactory.getLogger(OrganizationVendorService::class.java)
    }

    /*** Getters ***/
    fun allVendorsForOrganization(organizationId: OrganizationPublicId): List<OrganizationDataRecipient> {
        val organization = lookupOrganizationOrThrow(organizationId)
        return organizationDataRecipientRepository.findAllByOrganization(organization)
    }

    fun allConsumerGroupsForOrganizationVendor(organizationId: OrganizationPublicId, vendorId: UUID): List<DataRecipientCollectionGroup> {
        val organization = lookupOrganizationOrThrow(organizationId)
        val vendor = lookupVendorOrThrow(vendorId)
        return dataRecipientCollectionGroupRepo.findAllByCollectionGroup_OrganizationAndVendor(organization, vendor)
    }

    fun getOrganizationVendor(organizationId: OrganizationPublicId, vendorId: UUID): OrganizationDataRecipient? {
        val organization = lookupOrganizationOrThrow(organizationId)
        val vendor = lookupVendorOrThrow(vendorId)
        return organizationDataRecipientRepository.findByOrganizationAndVendor(organization, vendor)
    }

    fun getNewData(organizationId: OrganizationPublicId): OrganizationVendorNewAndEditDto {
        val organization = lookupOrganizationOrThrow(organizationId)
        return OrganizationVendorNewAndEditDto(
            existingCategories = vendorRepository.findDistinctRecipientCategories(organization),
            classificationOptions = vendorClassificationRepository.findAll().map { it.toDto() },
            vendorContractOptions = vendorContractRepository.findAll().map { it.toDto() },
            vendorContractReviewedOptions = vendorContractReviewedRepository.findAll().map { it.toDto() }
        )
    }

    fun getEditData(organizationId: OrganizationPublicId, vendorId: UUID): OrganizationVendorNewAndEditDto {
        val orgVendor = getOrganizationVendor(organizationId, vendorId) ?: throw VendorNotFound(vendorId.toString())
        val vendor = orgVendor.vendor!!
        return OrganizationVendorNewAndEditDto(
            classificationOptions = vendorClassificationRepository.findAll().map { it.toDto() },
            vendorContractOptions = vendorContractRepository.findAll().map { it.toDto() },
            vendorContractReviewedOptions = vendorContractReviewedRepository.findAll().map { it.toDto() },
            existingCategories = vendor.organization?.let { vendorRepository.findDistinctRecipientCategories(vendor.organization) }
                ?: emptyList(),
            vendor = organizationVendorDtoService.toDto(orgVendor)
        )
    }

    fun getAddData(organizationId: OrganizationPublicId, vendorId: UUID): OrganizationVendorNewAndEditDto {
        val orgVendor = getOrganizationVendor(organizationId, vendorId) ?: throw VendorNotFound(vendorId.toString())
        return OrganizationVendorNewAndEditDto(
            classificationOptions = vendorClassificationRepository.findAll().map { it.toDto() },
            vendorContractOptions = vendorContractRepository.findAll().map { it.toDto() },
            vendorContractReviewedOptions = vendorContractReviewedRepository.findAll().map { it.toDto() },
            vendor = organizationVendorDtoService.toDto(orgVendor)
        )
    }

    fun moveOutdatedUnknownOrgDataRecipientsToController() {
        val spec = OrganizationDataRecipientSpecs.forGdprProcessorSetting(GDPRProcessorRecommendation.Unknown)
            .and(OrganizationDataRecipientSpecs.hasGdprProcessorSettingSetAt())
            ?.and(OrganizationDataRecipientSpecs.gdprProcessorSettingSetAtBefore(ZonedDateTime.now().minusWeeks(3)))

        val dataRecipientsToUpdate = organizationDataRecipientRepository.findAll(spec)

        dataRecipientsToUpdate.forEach { dr ->
            dr.gdprProcessorSetting = GDPRProcessorRecommendation.Controller
            dr.gdprProcessorSettingSetAt = ZonedDateTime.now()
            organizationDataRecipientRepository.save(dr)
        }
    }

    /*** Setters ***/

    fun addDefaultVendors(organizationId: OrganizationPublicId) {
        val org = lookup.orgOrThrow(organizationId)

        // Right now we only have one default vendor, so it's just hard-coded here. If we add more
        // we should move this to a data-driven source.
        val polaris = vendorRepository.findByVendorKey(KnownVendorKey.TRUEVAULT_POLARIS.key) ?: throw VendorNotFound("TrueVault Polaris")
        val tvRecipient = organizationDataRecipientRepository.findByOrganizationAndVendor(org, polaris)
        if (tvRecipient == null) {
            addOrUpdateOrganizationVendor(
                organizationId, polaris.id,
                OrganizationVendorSettingsDto(
                    collectionContext = listOf(CollectionContext.CONSUMER, CollectionContext.EMPLOYEE)
                )
            )
        }
    }

    fun addOrUpdateOrganizationVendor(
        organizationId: OrganizationPublicId,
        vendorId: UUID,
        recipientSettings: OrganizationVendorSettingsDto?
    ): OrganizationDataRecipient {
        val organization = lookupOrganizationOrThrow(organizationId)
        val vendor = vendorRepository.findByIdOrNull(vendorId) ?: throw PolarisException("Unable to find vendor $vendorId")
        val status = if (recipientSettings != null && !recipientSettings.classificationId.isNullOrBlank())
            OrganizationVendorStatus.COMPLETED else OrganizationVendorStatus.IN_PROGRESS
        val classificationBySlug = vendorClassificationRepository.findAll().associateBy { it.slug }
        val vendorClassification =
            if (recipientSettings != null && !recipientSettings.classificationId.isNullOrBlank()) {
                vendorClassificationRepository.findByIdOrNull(UUID.fromString(recipientSettings.classificationId))
            } else null ?: if (vendor.serviceProviderRecommendation == ServiceProviderRecommendation.SERVICE_PROVIDER) {
                classificationBySlug[VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER]
            } else {
                classificationBySlug[VENDOR_CLASSIFICATION_SLUG_NEEDS_REVIEW]
            }
        val vendorContract =
            if (recipientSettings != null && recipientSettings.vendorContractId?.isBlank() != true) {
                val contractId = recipientSettings.vendorContractId
                if (contractId != null) vendorContractRepository.findByIdOrNull(UUID.fromString(contractId)) else null
            } else null ?: vendor.vendorContract
        val vendorContractReviewed =
            if (recipientSettings != null && recipientSettings.vendorContractReviewedId?.isBlank() != true) {
                val contractReviewedId = recipientSettings.vendorContractReviewedId
                if (contractReviewedId != null) vendorContractReviewedRepository.findByIdOrNull(UUID.fromString(contractReviewedId)) else null
            } else null

        var dataRecipient = organizationDataRecipientRepository.findByOrganizationAndVendor(organization, vendor)
        var existing = true

        val defaultTosUrl = dataRecipient?.publicTosUrl
        val defaultTosFileName = dataRecipient?.tosFileName
        val defaultTosFileKey = dataRecipient?.tosFileKey
        val defaultCcpaIsSelling = dataRecipient?.ccpaIsSelling ?: false
        val defaultCcpaIsSharing = dataRecipient?.ccpaIsSharing ?: false
        val defaultUsesCustomAudiences = dataRecipient?.usesCustomAudience ?: false
        val defaultIsUploadVendor = dataRecipient?.isUploadVendor ?: false

        if (dataRecipient == null) {
            dataRecipient = OrganizationDataRecipient(
                organization = organization,
                vendor = vendor,
                vendorClassification = vendorClassification,
                vendorContract = vendorContract,
                vendorContractReviewed = vendorContractReviewed,
                completed = recipientSettings?.completed ?: true
            )

            existing = false
        }

        // If setting the classification to no-data-sharing then make sure we delete any mapped disclosures/sharing
        if (vendorClassification?.slug == "no-data-sharing") {
            removeDisclosures(organization, vendor)
        }
        val publicTosUrl = recipientSettings?.publicTosUrl ?: defaultTosUrl
        val tosFileName = recipientSettings?.tosFileName ?: defaultTosFileName
        val tosFileKey = recipientSettings?.tosFileKey ?: defaultTosFileKey
        val ccpaIsSelling = recipientSettings?.ccpaIsSelling ?: defaultCcpaIsSelling
        val ccpaIsSharing = recipientSettings?.ccpaIsSharing ?: defaultCcpaIsSharing
        val usesCustomAudience = recipientSettings?.usesCustomAudience ?: defaultUsesCustomAudiences
        val isUploadVendor = recipientSettings?.isUploadVendor ?: defaultIsUploadVendor

        if (recipientSettings?.processingRegions != null) {
            dataRecipient.processingRegions = recipientSettings.processingRegions!!
        }

        if (recipientSettings?.collectionContext != null) {
            if (existing) {
                // merge with existing context list
                val newContext = mutableListOf<CollectionContext>()
                newContext.addAll(dataRecipient.collectionContexts)
                newContext.addAll(recipientSettings.collectionContext!!)

                dataRecipient.collectionContexts = newContext
            } else {
                // set consumer context directly
                dataRecipient.collectionContexts = recipientSettings.collectionContext!!
            }
        }

        dataRecipient.publicTosUrl = publicTosUrl
        dataRecipient.ccpaIsSelling = ccpaIsSelling
        dataRecipient.ccpaIsSharing = ccpaIsSharing
        dataRecipient.usesCustomAudience = usesCustomAudience
        dataRecipient.isUploadVendor = isUploadVendor
        dataRecipient.vendorClassification = vendorClassification
        dataRecipient.tosFileName = tosFileName
        dataRecipient.tosFileKey = tosFileKey

        // this is a one-way latch. You cannot reset it back to false.
        if (!dataRecipient.completed && recipientSettings?.completed == true) {
            dataRecipient.completed = true
        }

        updateGdprProcessorSettings(dataRecipient, recipientSettings)

        if (recipientSettings?.gdprContactUnsafeTransfer != null) {
            dataRecipient.gdprContactUnsafeTransfer = recipientSettings.gdprContactUnsafeTransfer
        }

        if (recipientSettings?.gdprEnsureSccInPlace != null) {
            dataRecipient.gdprEnsureSccInPlace = recipientSettings.gdprEnsureSccInPlace
        }

        dataRecipient.gdprContactProcessorStatus = recipientSettings?.gdprContactProcessorStatus

        dataRecipient.gdprHasSccSetting = recipientSettings?.gdprHasSccSetting
        dataRecipient.gdprSccUrl = recipientSettings?.gdprSccUrl
        dataRecipient.gdprSccFileName = recipientSettings?.gdprSccFileName
        dataRecipient.gdprSccFileKey = recipientSettings?.gdprSccFileKey

        if (recipientSettings?.dataAccessibility != null && dataRecipient.dataAccessibility != recipientSettings.dataAccessibility) {
            dataRecipient.dataAccessibility = recipientSettings.dataAccessibility

            // update the request handling instructions
            if (dataRecipient.dataAccessibility == DataAccessibilityEnum.ACCESSIBLE || dataRecipient.dataAccessibility == DataAccessibilityEnum.UNKNOWN) {
                if (dataRecipient.dataDeletability == DataDeletabilityEnum.NOT_DELETABLE) {
                    dataRecipient.dataDeletability = null
                    requestHandlingInstructionsService.createOrUpdate(organizationId, RequestHandlingInstructionDto(id = null, organizationId = organizationId, requestType = DataRequestInstructionType.DELETE, vendorId = vendorId.toString(), processingMethod = ProcessingMethod.INACCESSIBLE_OR_NOT_STORED))
                }
                organizationDataRecipientRepository.save(dataRecipient)
                requestHandlingInstructionsService.createOrUpdate(organizationId, RequestHandlingInstructionDto(id = null, organizationId = organizationId, requestType = DataRequestInstructionType.KNOW, vendorId = vendorId.toString(), processingMethod = null), deleteProcessingMethodOnNull = true)
            } else if (dataRecipient.dataAccessibility == DataAccessibilityEnum.NOT_ACCESSIBLE) {
                organizationDataRecipientRepository.save(dataRecipient)
                dataRecipient.dataDeletability = DataDeletabilityEnum.NOT_DELETABLE
                requestHandlingInstructionsService.createOrUpdate(organizationId, RequestHandlingInstructionDto(id = null, organizationId = organizationId, requestType = DataRequestInstructionType.DELETE, vendorId = vendorId.toString(), processingMethod = ProcessingMethod.INACCESSIBLE_OR_NOT_STORED))
                requestHandlingInstructionsService.createOrUpdate(organizationId, RequestHandlingInstructionDto(id = null, organizationId = organizationId, requestType = DataRequestInstructionType.KNOW, vendorId = vendorId.toString(), processingMethod = ProcessingMethod.INACCESSIBLE_OR_NOT_STORED))
            }
        }

        if (recipientSettings?.dataDeletability != null && dataRecipient.dataDeletability != recipientSettings.dataDeletability) {
            dataRecipient.dataDeletability = recipientSettings.dataDeletability

            // update the request handling instructions
            if (dataRecipient.dataDeletability == DataDeletabilityEnum.DELETABLE || dataRecipient.dataDeletability == DataDeletabilityEnum.UNKNOWN) {
                val existingDeleteInstruction = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(organization, DataRequestInstructionType.DELETE, dataRecipient)
                val hasExistingRetentionReasons = !existingDeleteInstruction?.retentionReasons.isNullOrEmpty()
                val hasDefaultRetentionReasons = !dataRecipient.vendor?.defaultRetentionReasons.isNullOrEmpty()
                val deletionProcessingMethod = if (hasExistingRetentionReasons || hasDefaultRetentionReasons) ProcessingMethod.RETAIN else ProcessingMethod.DELETE
                requestHandlingInstructionsService.createOrUpdate(organizationId, RequestHandlingInstructionDto(id = null, organizationId = organizationId, requestType = DataRequestInstructionType.DELETE, vendorId = vendorId.toString(), processingMethod = deletionProcessingMethod))
            } else if (dataRecipient.dataDeletability == DataDeletabilityEnum.NOT_DELETABLE) {
                requestHandlingInstructionsService.createOrUpdate(organizationId, RequestHandlingInstructionDto(id = null, organizationId = organizationId, requestType = DataRequestInstructionType.DELETE, vendorId = vendorId.toString(), processingMethod = ProcessingMethod.INACCESSIBLE_OR_NOT_STORED))
            }
        }

        if (recipientSettings?.removedFromExceptionsToScc != null) {
            dataRecipient.removedFromExceptionsToScc = recipientSettings.removedFromExceptionsToScc
        }

        if (vendorContract != null) {
            dataRecipient.vendorContract = vendorContract
        }
        if (vendorContractReviewed != null) {
            dataRecipient.vendorContractReviewed = vendorContractReviewed
        }
        dataRecipient.status = status
        dataRecipient.contacted = recipientSettings?.contacted
        if (vendor.email == recipientSettings?.email) {
            dataRecipient.email = null
        } else {
            dataRecipient.email = recipientSettings?.email
        }
        dataRecipient.automaticallyClassified = recipientSettings?.automaticallyClassified ?: false
        dataRecipient.isDataStorage = recipientSettings?.isDataStorage ?: false

        val saved = organizationDataRecipientRepository.save(dataRecipient)

        ensureCustomAudienceSurveyData(organization)
        ensureIsUploadSurveyData(organization)
        ensureSellingDisclosure(organization)

        privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(organization, "org data recipient add or update")

        taskUpdateService.updateTasks(organization)

        return saved
    }

    private fun updateGdprProcessorSettings(dataRecipient: OrganizationDataRecipient, recipientSettings: OrganizationVendorSettingsDto?) {
        if (recipientSettings == null) return

        if (dataRecipient.gdprProcessorSetting != recipientSettings.gdprProcessorSetting) {
            dataRecipient.gdprProcessorSetting = recipientSettings.gdprProcessorSetting
            dataRecipient.gdprProcessorSettingSetAt = ZonedDateTime.now()
        }

        if (recipientSettings.gdprProcessorGuaranteeUrl != null) dataRecipient.gdprProcessorGuaranteeUrl = recipientSettings.gdprProcessorGuaranteeUrl
        if (recipientSettings.gdprProcessorGuaranteeFileName != null) dataRecipient.gdprProcessorGuaranteeFileName = recipientSettings.gdprProcessorGuaranteeFileName
        if (recipientSettings.gdprProcessorGuaranteeFileKey != null) dataRecipient.gdprProcessorGuaranteeFileKey = recipientSettings.gdprProcessorGuaranteeFileKey
        if (recipientSettings.gdprControllerGuaranteeUrl != null) dataRecipient.gdprControllerGuaranteeUrl = recipientSettings.gdprControllerGuaranteeUrl
        if (recipientSettings.gdprControllerGuaranteeFileName != null) dataRecipient.gdprControllerGuaranteeFileName = recipientSettings.gdprControllerGuaranteeFileName
        if (recipientSettings.gdprControllerGuaranteeFileKey != null) dataRecipient.gdprControllerGuaranteeFileKey = recipientSettings.gdprControllerGuaranteeFileKey
    }

    fun removeCollectionContext(organizationId: OrganizationPublicId, vendorId: UUID, collectionContext: List<CollectionContext>, deleteOnEmpty: Boolean = false) {
        val dataRecipient = getOrganizationVendor(organizationId, vendorId) ?: throw VendorNotFound(vendorId.toString())
        val newContext = dataRecipient.collectionContexts - collectionContext.toSet()

        if (newContext.isNullOrEmpty() && deleteOnEmpty) {
            removeVendor(organizationId, vendorId)
        } else {
            val relevantCollectionGroupTypes = collectionContext.flatMap { it.collectionGroupTypes }.distinct()
            val org = lookup.orgOrThrow(organizationId)

            dataRecipient.collectionContexts = newContext
            organizationDataRecipientRepository.save(dataRecipient)

            // lookup relevant collection groups, and remove related disclosures
            val relevantCollectionGroups = org.collectionGroups.filter { relevantCollectionGroupTypes.contains(it.collectionGroupType) }
            removeDisclosuresForGroups(dataRecipient.vendor!!, relevantCollectionGroups)
        }
    }

    /**
     * Remove an organization vendor and all of its associated data
     *
     * Will remove request processing settings and information disclosures in addition to the vendor
     */
    fun removeVendor(organizationId: OrganizationPublicId, vendorId: UUID) {
        val orgVendor = getOrganizationVendor(organizationId, vendorId) ?: throw VendorNotFound(vendorId.toString())
        val vendor = orgVendor.vendor!!
        val organization = orgVendor.organization!!
        removeDisclosures(organization, vendor)

        removeAppInstallations(orgVendor)
        removeRequestHandlingInstructions(orgVendor)

        orgVendor.deletedAt = ZonedDateTime.now()
        organizationDataRecipientRepository.save(orgVendor)

        removeOrgVendorConsumerGroups(orgVendor)

        if (vendor.organization == organization && !vendorOutcomeRepository.existsByVendorAndDataSubjectRequest_Organization(
                vendor,
                organization
            )
        ) {
            vendor.deletedAt = ZonedDateTime.now()
            vendorRepository.save(vendor)
        }

        ensureCustomAudienceSurveyData(organization)
        ensureIsUploadSurveyData(organization)
        ensureSellingDisclosure(organization)

        // Always rebuild the privacy center
        privacyCenterBuilderService.requestBuildPrivacyCentersForOrg(organization, "org data recipient removed")
    }

    fun updateMappingProgress(
        organizationId: OrganizationPublicId,
        vendorId: UUID,
        mappingProgress: MappingProgressEnum
    ) {
        val orgVendor = getOrganizationVendor(organizationId, vendorId) ?: throw VendorNotFound(vendorId.toString())
        if (orgVendor.mappingProgress != mappingProgress) {
            orgVendor.mappingProgress = mappingProgress
            organizationDataRecipientRepository.save(orgVendor)
        }
    }

    fun removeVendorAgreementFile(organizationId: OrganizationPublicId, vendorId: UUID): OrganizationDataRecipient {
        val organization = lookupOrganizationOrThrow(organizationId)
        val vendor = vendorRepository.findByIdOrNull(vendorId) ?: throw PolarisException("Unable to find vendor $vendorId")
        var dataRecipient = organizationDataRecipientRepository.findByOrganizationAndVendor(organization, vendor)

        dataRecipient!!.tosFileName = null
        dataRecipient!!.tosFileKey = null

        val saved = organizationDataRecipientRepository.save(dataRecipient)

        return saved
    }

    private fun ensureCustomAudienceSurveyData(organization: Organization) {

        val customAudienceSurveyQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(organization, SELLING_AND_SHARING_SURVEY_NAME, CUSTOM_AUDIENCE_QUESTION_SLUG)

        if (customAudienceSurveyQuestion != null) {
            val organizationDataRecipients = allVendorsForOrganization(organization.publicId)
            val customAudienceAdNetworks = organizationDataRecipients.filter { it.vendor!!.isKnownCategory(KnownVendorCategories.AD_NETWORK) && it.usesCustomAudience }.distinct()

            val caSurveyAnswerList = if (customAudienceAdNetworks.isNotEmpty()) customAudienceAdNetworks.map { it.id.toString() } else listOf("none")

            customAudienceSurveyQuestion.answer = "[\"${caSurveyAnswerList.joinToString("\",\"")}\"]"

            organizationSurveyQuestionRepository.save(customAudienceSurveyQuestion)
        }
    }

    private fun ensureSellingDisclosure(organization: Organization) {
        // POLARIS-2512 If we add back in the LDU/RDP questions, we likely need to update this code to behave appropriately
        val sellingDisclosureSurveyQuestion =
            organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(
                organization,
                SELLING_AND_SHARING_SURVEY_NAME,
                DISCLOSURE_FOR_SELLING_SLUG
            )
        val sellingVendorsSurveyQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(
            organization,
            SELLING_AND_SHARING_SURVEY_NAME,
            VENDOR_SELLING_QUESTION_SLUG
        )

        if (sellingDisclosureSurveyQuestion != null && sellingVendorsSurveyQuestion != null) {
            val organizationDataRecipients = allVendorsForOrganization(organization.publicId)
            val recipientsList = organizationDataRecipients.filter {
                (!it.vendor!!.isKnownCategory(KnownVendorCategories.AD_NETWORK) && it.ccpaIsSelling) || organizationVendorSurveyService.isGoogleAnalyticsWithDataSharing(
                    organization,
                    it
                ) || organizationVendorSurveyService.isShopifyWithAudiencesEnabled(organization)
            }.distinct()
            val isSelling =
                recipientsList.isNotEmpty() || organizationVendorSurveyService.isGoogleAnalyticsWithDataSharing(
                    organization
                ) || organizationVendorSurveyService.isShopifyWithAudiencesEnabled(organization)
            sellingDisclosureSurveyQuestion.answer = isSelling.toString()

            val recipientsListIds = recipientsList.map { it.id.toString() }
            val recipientListOrNone = if (recipientsListIds.isNotEmpty()) recipientsListIds else listOf("none")
            sellingVendorsSurveyQuestion.answer = "[\"${recipientListOrNone.joinToString("\",\"")}\"]"
            organizationSurveyQuestionRepository.save(sellingVendorsSurveyQuestion)
        }
    }

    private fun ensureIsUploadSurveyData(organization: Organization) {

        val sellingUploadDataSurveyQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(organization, SELLING_AND_SHARING_SURVEY_NAME, SELLING_UPLOAD_DATA_SLUG)
        val sellingUploadDataVendorsSurveyQuestion = organizationSurveyQuestionRepository.findByOrganizationAndSurveyNameAndSlug(organization, SELLING_AND_SHARING_SURVEY_NAME, VENDOR_UPLOAD_DATA_SELECTION_SLUG)

        if (sellingUploadDataSurveyQuestion != null && sellingUploadDataVendorsSurveyQuestion != null) {
            val organizationDataRecipients = allVendorsForOrganization(organization.publicId)
            val recipients = organizationDataRecipients.filter { !it.vendor!!.isKnownCategory(KnownVendorCategories.AD_NETWORK) && it.isUploadVendor ?: false }.distinct()

            var caSurveyAnswerList = if (recipients.isNotEmpty()) recipients.map { it.id.toString() } else listOf("none")

            val isOnlyGaNoDataShare = organizationVendorSurveyService.isOnlyGoogleAnalyticsWithNoDataSharing(organization)
            val isShopifyEnabled = organizationVendorSurveyService.isShopifyWithAudiencesEnabled(organization)

            // override the list when either of the following conditions are true
            // is shopify still treated differently at all?
            if (isOnlyGaNoDataShare) {
                sellingUploadDataSurveyQuestion.answer = false.toString()
            } else if (isShopifyEnabled) {
                sellingUploadDataSurveyQuestion.answer = true.toString()
                if (caSurveyAnswerList[0] == "none") {
                    val shopify = organizationDataRecipients.find { organizationVendorSurveyService.isShopify(it) }
                    if (shopify != null) {
                        caSurveyAnswerList = listOf("${shopify.id}")
                    }
                }
            } else {
                sellingUploadDataSurveyQuestion.answer = (caSurveyAnswerList[0] != "none").toString()
            }

            sellingUploadDataVendorsSurveyQuestion.answer = "[\"${caSurveyAnswerList.joinToString("\",\"")}\"]"

            organizationSurveyQuestionRepository.save(sellingUploadDataSurveyQuestion)
            organizationSurveyQuestionRepository.save(sellingUploadDataVendorsSurveyQuestion)
        }
    }

    private fun removeAppInstallations(orgDataRecipient: OrganizationDataRecipient) {
        val allByApp = platformAppInstallRepository.findAllByApp(orgDataRecipient)
        val allByPlatform = platformAppInstallRepository.findAllByPlatform(orgDataRecipient)

        val all = allByApp + allByPlatform
        all.forEach { it.deletedAt = ZonedDateTime.now() }

        platformAppInstallRepository.saveAll(all)
    }

    private fun removeRequestHandlingInstructions(orgDataRecipient: OrganizationDataRecipient) {
        val all = requestHandlingInstructionsRepository.findAllByOrganizationAndOrganizationDataRecipient(orgDataRecipient.organization!!, orgDataRecipient)
        all.forEach { it.deletedAt = ZonedDateTime.now() }
        requestHandlingInstructionsRepository.saveAll(all)
    }

    private fun removeOrgVendorConsumerGroups(orgVendor: OrganizationDataRecipient) {
        val all = dataRecipientCollectionGroupRepo.findAllByCollectionGroup_OrganizationAndVendor(orgVendor.organization!!, orgVendor.vendor!!)
        dataRecipientCollectionGroupRepo.deleteAll(all)
    }

    private fun removeDisclosures(organization: Organization, vendor: Vendor) =
        vendorPICDisclosureRepo.deleteAll(
            vendorPICDisclosureRepo.findAllByCollectedPIC_CollectionGroup_OrganizationAndVendor(
                organization,
                vendor
            )
        )

    private fun removeDisclosuresForGroups(vendor: Vendor, groups: List<CollectionGroup>) {
        vendorPICDisclosureRepo.deleteAll(
            vendorPICDisclosureRepo.findAllByCollectedPIC_CollectionGroupInAndVendor(
                groups,
                vendor
            )
        )
    }

    private fun lookupOrganizationOrThrow(organizationId: OrganizationPublicId) =
        organizationRepository.findByPublicId(organizationId)
            ?: throw OrganizationNotFound(organizationId)

    private fun lookupVendorOrThrow(vendorId: UUID) =
        vendorRepository.findByIdOrNull(vendorId) ?: throw VendorNotFound(vendorId.toString())
}
