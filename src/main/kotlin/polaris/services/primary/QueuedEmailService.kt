package polaris.services.primary

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.MailboxNotConnectedException
import polaris.models.entity.email.QueuedEmail
import polaris.models.entity.email.QueuedEmailStatusType
import polaris.models.entity.organization.Organization
import polaris.repositories.QueuedEmailRepository
import polaris.services.support.EmailService
import java.time.ZonedDateTime

@Service
@Transactional
class QueuedEmailService(
    private val queuedEmailRepository: QueuedEmailRepository,
    private val emailService: EmailService
) {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(QueuedEmailService::class.java)
    }

    fun enqueueEmail(email: QueuedEmail) {
        queuedEmailRepository.save(email)
    }

    fun findAndProcessEmails() {
        val emailsToProcess = queuedEmailRepository.findByStatus(QueuedEmailStatusType.NEW)

        log.info("Processing {} NEW queued emails...", emailsToProcess.size)

        // Mark as pending and flush to DB
        emailsToProcess.forEach { it -> it.status = QueuedEmailStatusType.PENDING }
        queuedEmailRepository.saveAll(emailsToProcess)
        queuedEmailRepository.flush()

        val queuedEmailsByOrg = emailsToProcess.groupBy { it.dataSubjectRequest?.organization }

        for (organization in queuedEmailsByOrg.keys.filterNotNull()) {
            queuedEmailsByOrg[organization]?.let { processEmails(organization, it) }
        }
    }

    private fun processEmails(organization: Organization, queuedEmails: List<QueuedEmail>) {
        log.info("[{}] Processing {} queued emails...", organization.name, queuedEmails.size)

        var sentCount = 0
        var failedCount = 0

        for (email in queuedEmails) {
            try {
                val emailResponse = emailService.sendRequestGeneralEmail(email.dataSubjectRequest!!, email.recipientEmailAddress!!, email.senderEmailAddress, email.subject!!, email.message!!)

                if (emailResponse.statusCode == 200) {
                    email.status = QueuedEmailStatusType.SENT
                    sentCount++
                } else {
                    log.error(
                        "[{}] Failed to send queued email for data request {}: {}",
                        organization.name, email.dataSubjectRequest.publicId, emailResponse
                    )
                    email.status = QueuedEmailStatusType.FAILURE
                    failedCount++
                }
                email.lastUpdatedAt = ZonedDateTime.now()
            } catch (ex: MailboxNotConnectedException) {
                log.error(
                    "[{}] Failed to send queued email for data request {}. Aborting... {}",
                    organization.name, email.dataSubjectRequest?.publicId, ex.message, ex
                )
                email.status = QueuedEmailStatusType.FAILURE
                email.lastUpdatedAt = ZonedDateTime.now()
                failedCount++
                break
            } finally {
                queuedEmailRepository.saveAndFlush(email)
            }
        }

        log.info(
            "[{}] Processed {} of {} queued emails: {} sent, {} failed",
            organization.name, sentCount + failedCount, queuedEmails.size, sentCount, failedCount
        )
    }
}
