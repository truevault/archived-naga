package polaris.services.primary

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import polaris.controllers.InvalidNote
import polaris.controllers.util.LookupService
import polaris.models.DataRequestPublicId
import polaris.models.OrganizationPublicId
import polaris.models.dto.InboundEmail
import polaris.models.dto.request.ConfirmIdentityDataRequest
import polaris.models.dto.request.ContactVendorRequest
import polaris.models.dto.request.CreateDataRequest
import polaris.models.dto.request.CreateNoteRequest
import polaris.models.dto.request.ExtendDataRequest
import polaris.models.dto.request.RequestSubjectStatusUpdate
import polaris.models.dto.request.SubjectStatus
import polaris.models.dto.request.TransitionDataRequest
import polaris.models.dto.request.UpdateDataRequest
import polaris.models.dto.request.UpdateVendorRequest
import polaris.models.dto.request.VendorOutcomeValue
import polaris.models.dto.request.VerifyDataRequest
import polaris.models.entity.ConfirmationTokenDoesNotMatch
import polaris.models.entity.DataRequestNotFound
import polaris.models.entity.MissingDeclarationOrCustomField
import polaris.models.entity.attachment.Attachment
import polaris.models.entity.attachment.DataSubjectRequestEventAttachment
import polaris.models.entity.email.QueuedEmail
import polaris.models.entity.organization.EventData
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.Organization
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.request.DataRequestEventType
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubmissionMethodEnum
import polaris.models.entity.request.DataRequestSubmissionSourceEnum
import polaris.models.entity.request.DataRequestSubstate
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.request.DataSubjectRequestEvent
import polaris.models.entity.request.ProcessingMethod
import polaris.models.entity.request.RequestWorkflow
import polaris.models.entity.request.VendorOutcome
import polaris.models.entity.user.User
import polaris.models.entity.user.UserRole
import polaris.models.toDateTime
import polaris.repositories.DataRequestEventAttachmentRepository
import polaris.repositories.DataRequestEventRepository
import polaris.repositories.DataRequestRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationUserRepository
import polaris.repositories.RequestHandlingInstructionsRepository
import polaris.repositories.VendorOutcomeRepository
import polaris.services.DataRequestWorkflowService
import polaris.services.NylasSendError
import polaris.services.support.AttachmentService
import polaris.services.support.EmailService
import polaris.services.support.OptOutService
import polaris.services.support.RequestTemplateMessageService
import polaris.specs.DataSubjectRequestSpecs
import polaris.util.IdGenerator
import polaris.util.PolarisException
import polaris.util.atStartOfDay
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.UUID
import javax.persistence.EntityManager
import javax.transaction.Transactional

enum class DataRequestFilter {
    all,
    active,
    complete,
    pending,
}

sealed class DataRequestSearchParam {
    class SubjectEmail(val email: String) : DataRequestSearchParam()
    class RequestType(val requestType: DataRequestType) : DataRequestSearchParam()
    class DueDate(val dueIn: Int) : DataRequestSearchParam()
    class CreatedRange(val createdStartDate: ZonedDateTime? = null, val createdEndDate: ZonedDateTime? = null) :
        DataRequestSearchParam()

    class RequestDateRange(val requestStartDate: ZonedDateTime? = null, val requestEndDate: ZonedDateTime? = null) :
        DataRequestSearchParam()

    class RequestState(val state: String) : DataRequestSearchParam()
}

enum class DataRequestSortId {
    subjectEmailAddress,
    subjectFirstName,
    subjectLastName,
    type,
    state,
    dueIn,
    submitted,
    closedAt
}

data class DataRequestSort(
    val id: DataRequestSortId,
    val desc: Boolean?
)

enum class RequestTypeFilter {
    all,
    access,
    delete,
    erasure,
    know,
    objection,
    optout,
    rectification,
    portability,
    withdrawal
}

enum class RequestStateFilter {
    pending,
    closed,
    new,
    in_progress
}

@Service
@Transactional
class DataSubjectRequestService(
    private val dataRequestRepository: DataRequestRepository,
    private val dataRequestEventRepository: DataRequestEventRepository,
    private val organizationRepository: OrganizationRepository,
    private val organizationUserRepository: OrganizationUserRepository,
    private val collectionGroupService: CollectionGroupService,
    private val queuedEmailService: QueuedEmailService,
    private val onboardingEmailService: OnboardingEmailService,
    private val dataRequestWorkflowService: DataRequestWorkflowService,
    private val vendorOutcomeRepository: VendorOutcomeRepository,
    private val dataRequestEventAttachmentRepository: DataRequestEventAttachmentRepository,
    private val requestTemplateService: RequestTemplateMessageService,
    private val attachmentService: AttachmentService,

    private val organizationVendorService: OrganizationVendorService,
    private val requestHandlingInstructionsRepository: RequestHandlingInstructionsRepository,

    private val emailService: EmailService,
    private val optOutService: OptOutService,

    private val entityManager: EntityManager,

    private val lookup: LookupService
) {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(DataSubjectRequestService::class.java)
    }

    /*** Getters ***/
    fun findAllForOrganization(
        organizationId: OrganizationPublicId,
        filter: DataRequestFilter?,
        type: RequestTypeFilter = RequestTypeFilter.all
    ): List<DataSubjectRequest> {
        val organization = lookup.orgOrThrow(organizationId)
        val states = statesForFilterOrThrow(filter)
        val requestTypes = requestTypesForFilter(type)
        val requests = if (requestTypes != null) {
            dataRequestRepository.findByOrganizationAndStateIsInAndRequestTypeIn(organization, states, requestTypes)
        } else {
            dataRequestRepository.findByOrganizationAndStateIsIn(organization, states)
        }

        return requests.sortedBy { it.requestDueDate }
    }

    fun paginatedFindForOrganization(
        organizationId: OrganizationPublicId,
        filter: DataRequestFilter?,
        search: List<DataRequestSearchParam> = emptyList(),
        sort: DataRequestSort? = null,
        type: RequestTypeFilter = RequestTypeFilter.all,
        page: Int = 0,
        per: Int = 15
    ): Page<DataSubjectRequest> {
        val sortCol = columnForSortParam(sort?.id)
        val sortDesc = sort?.desc ?: true
        val querySort = if (sortDesc) Sort.by(sortCol).descending() else Sort.by(sortCol).ascending()

        val pageable: Pageable = PageRequest.of(page, per.coerceAtMost(500), querySort)
        val organization = lookup.orgOrThrow(organizationId)
        val states = statesForFilterOrThrow(filter)

        val requestTypes = requestTypesForFilter(type)

        var spec = DataSubjectRequestSpecs.forOrganization(organization).and(DataSubjectRequestSpecs.inStates(states))

        search.forEach { s ->
            spec = when (s) {
                is DataRequestSearchParam.RequestType -> spec.and(DataSubjectRequestSpecs.withDataRequestType(s.requestType))
                is DataRequestSearchParam.SubjectEmail -> spec.and(DataSubjectRequestSpecs.searchSubjectEmail(s.email))
                is DataRequestSearchParam.RequestState -> spec.and(
                    DataSubjectRequestSpecs.forDataRequestStateIn(
                        requestStateForFilter(RequestStateFilter.valueOf(s.state))
                    )
                )
                is DataRequestSearchParam.CreatedRange -> spec.and(
                    DataSubjectRequestSpecs.createdInRange(
                        s.createdStartDate,
                        s.createdEndDate
                    )
                )
                is DataRequestSearchParam.RequestDateRange -> spec.and(
                    DataSubjectRequestSpecs.requestDateInRange(
                        s.requestStartDate,
                        s.requestEndDate
                    )
                )
                is DataRequestSearchParam.DueDate -> spec.and(
                    DataSubjectRequestSpecs.isDueInRange(
                        ZonedDateTime.now().atStartOfDay(), ZonedDateTime.now().plusDays(s.dueIn.toLong())
                    )
                )
            }
        }

        if (requestTypes != null) {
            spec = spec.and(DataSubjectRequestSpecs.forDataRequestTypeIn(requestTypes))
        }

        return dataRequestRepository.findAll(spec, pageable)
    }

    fun unpaginatedFindForOrganization(
        organizationId: OrganizationPublicId,
        filter: DataRequestFilter?,
        search: List<DataRequestSearchParam> = emptyList(),
        sort: DataRequestSort? = null,
        type: RequestTypeFilter = RequestTypeFilter.all
    ): Page<DataSubjectRequest> {
        val sortCol = columnForSortParam(sort?.id)
        val sortDesc = sort?.desc ?: true
        val querySort = if (sortDesc) Sort.by(sortCol).descending() else Sort.by(sortCol).ascending()

        val pageable: Pageable = Pageable.unpaged()
        val organization = lookup.orgOrThrow(organizationId)
        val states = statesForFilterOrThrow(filter)

        val requestTypes = requestTypesForFilter(type)

        var spec = DataSubjectRequestSpecs.forOrganization(organization).and(DataSubjectRequestSpecs.inStates(states))

        search.forEach { s ->
            spec = when (s) {
                is DataRequestSearchParam.RequestType -> spec.and(DataSubjectRequestSpecs.withDataRequestType(s.requestType))
                is DataRequestSearchParam.SubjectEmail -> spec.and(DataSubjectRequestSpecs.searchSubjectEmail(s.email))
                is DataRequestSearchParam.RequestState -> spec.and(DataSubjectRequestSpecs.forDataRequestStateIn(requestStateForFilter(RequestStateFilter.valueOf(s.state))))
                is DataRequestSearchParam.CreatedRange -> spec.and(DataSubjectRequestSpecs.createdInRange(s.createdStartDate, s.createdEndDate))
                is DataRequestSearchParam.RequestDateRange -> spec.and(DataSubjectRequestSpecs.requestDateInRange(s.requestStartDate, s.requestEndDate))
                is DataRequestSearchParam.DueDate -> spec.and(
                    DataSubjectRequestSpecs.isDueInRange(
                        ZonedDateTime.now().atStartOfDay(), ZonedDateTime.now().plusDays(s.dueIn.toLong())
                    )
                )
            }
        }

        if (requestTypes != null) {
            spec = spec.and(DataSubjectRequestSpecs.forDataRequestTypeIn(requestTypes))
        }

        return dataRequestRepository.findAll(spec, pageable)
    }

    fun columnForSortParam(param: DataRequestSortId?): String {
        return when (param) {
            DataRequestSortId.dueIn -> "requestDueDate"
            DataRequestSortId.subjectEmailAddress -> "subjectEmailAddress"
            DataRequestSortId.type -> "requestType"
            DataRequestSortId.state -> "state"
            DataRequestSortId.submitted -> "requestDate"
            DataRequestSortId.closedAt -> "closedAt"
            else -> {
                "requestDueDate"
            }
        }
    }

    fun requestStateForFilter(type: RequestStateFilter?): List<DataRequestState>? {
        return when (type) {
            RequestStateFilter.new -> listOf(DataRequestState.VERIFY_CONSUMER)
            RequestStateFilter.pending -> listOf(DataRequestState.PENDING_EMAIL_VERIFICATION)
            RequestStateFilter.closed -> listOf(DataRequestState.CLOSED)
            RequestStateFilter.in_progress -> listOf(
                DataRequestState.CONSUMER_GROUP_SELECTION,
                DataRequestState.PROCESSING,
                DataRequestState.DELETE_INFO,
                DataRequestState.CONTACT_VENDORS,
                DataRequestState.NOTIFY_CONSUMER,
                DataRequestState.REMOVE_CONSUMER,
                DataRequestState.RETRIEVE_DATA,
                DataRequestState.SEND_DATA
            )
            else -> null
        }
    }

    fun requestTypesForFilter(type: RequestTypeFilter): List<DataRequestType>? {
        return when (type) {
            RequestTypeFilter.access -> listOf(DataRequestType.GDPR_ACCESS)
            RequestTypeFilter.delete -> listOf(DataRequestType.CCPA_RIGHT_TO_DELETE)
            RequestTypeFilter.erasure -> listOf(DataRequestType.GDPR_ERASURE)
            RequestTypeFilter.know -> listOf(DataRequestType.CCPA_RIGHT_TO_KNOW)
            RequestTypeFilter.objection -> listOf(DataRequestType.GDPR_OBJECTION)
            RequestTypeFilter.optout -> listOf(DataRequestType.CCPA_OPT_OUT)
            RequestTypeFilter.rectification -> listOf(DataRequestType.GDPR_RECTIFICATION)
            RequestTypeFilter.withdrawal -> listOf(DataRequestType.GDPR_WITHDRAWAL)
            RequestTypeFilter.portability -> listOf(DataRequestType.GDPR_PORTABILITY)
            RequestTypeFilter.all -> null
        }
    }

    fun findByPublicId(organizationId: OrganizationPublicId, requestId: DataRequestPublicId): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        return lookupDataRequestOrThrow(organization, requestId)
    }

    fun findAllBySubjectEmail(organizationId: OrganizationPublicId, email: String): List<DataSubjectRequest> {
        val organization = lookup.orgOrThrow(organizationId)
        return dataRequestRepository.findAllByOrganizationAndSubjectEmailAddress(organization, email)
    }

    fun findAutoclosedAfter(organizationId: OrganizationPublicId, cutoff: ZonedDateTime): List<DataSubjectRequest> {
        val organization = lookup.orgOrThrow(organizationId)
        val allAutoclosed = dataRequestRepository.findAllByOrganizationAndIsAutoclosed(organization, true)
        return allAutoclosed.filter { it.closedAt?.isAfter(cutoff) ?: false }
    }

    fun findDuplicateRequests(request: DataSubjectRequest): List<DataSubjectRequest> {
        if (request.organization == null || request.subjectEmailAddress == null) {
            return emptyList()
        }

        val spec = DataSubjectRequestSpecs.forOrganization(request.organization)
            .and(DataSubjectRequestSpecs.open())
            .and(DataSubjectRequestSpecs.withEmailAddress(request.subjectEmailAddress!!))
            .and(DataSubjectRequestSpecs.withSubmissionSource(request.submissionSource))
            .and(DataSubjectRequestSpecs.withDataRequestType(request.requestType))
            .and(DataSubjectRequestSpecs.withDataRequestContext(request.context))
            .and(DataSubjectRequestSpecs.notId(request.id))

        return dataRequestRepository.findAll(spec)
    }

    /*** Updaters ***/

    fun handleIncomingEmail(replyKey: UUID, email: InboundEmail) {
        val dataRequest = dataRequestRepository.findByReplyKey(replyKey)

        if (dataRequest == null) {
            log.info("received incoming email for data request with reply key: $replyKey. No such data request was found.")
            return
        }

        addEventsToRequest(
            dataRequest,
            listOf(
                DataSubjectRequestEvent(
                    actor = null, isDataSubject = true, eventType = DataRequestEventType.MESSAGE_RECEIVED,
                    message = email.message
                )
            )
        )
        emailService.sendNewDataSubjectRequestMessageNotification(dataRequest)
    }

    fun getNylasAccessToken(replyKey: UUID): String? {
        val dataRequest = dataRequestRepository.findByReplyKey(replyKey)

        if (dataRequest == null) {
            log.info("received incoming email for data request with reply key: $replyKey. No such data request was found.")
            return null
        }

        val mailbox =
            dataRequest.organization?.organizationMailboxes?.firstOrNull { it.status == MailboxStatus.CONNECTED }
                ?: return null

        return mailbox.nylasAccessToken
    }

    fun updateRequest(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        dto: UpdateDataRequest
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val dataRequest = lookupDataRequestOrThrow(organization, requestId)

        // val dataRequestType = dataRequestTypeRepository.findByIdOrNull(UUID.fromString(dto.dataRequestTypeId))
        //     ?: throw PolarisException("Could not find request type")

        // Coerce to a submission method enum
        val submissionMethod = dto.submissionMethod?.let {
            DataRequestSubmissionMethodEnum.values().firstOrNull {
                it.name.equals(dto.submissionMethod, true)
            }
        }

        val events = mutableListOf<DataSubjectRequestEvent>()

        if (dto.correctionFinished != null) {
            if (dto.correctionFinished != dataRequest.correctionFinished) {
                dataRequest.correctionFinished = dto.correctionFinished
            }
        }

        if (dto.limitationFinished != null) {
            if (dto.limitationFinished != dataRequest.limitationFinished) {
                dataRequest.limitationFinished = dto.limitationFinished
            }
        }

        if (dto.consentWithdrawnFinished != null) {
            if (dto.consentWithdrawnFinished != dataRequest.consentWithdrawnFinished) {
                dataRequest.consentWithdrawnFinished = dto.consentWithdrawnFinished
            }
        }

        if (dto.requestDate != null) {
            val updatedDate = dto.requestDate.toDateTime()

            if (updatedDate != dataRequest.requestDate) {
                val daysChanged = ChronoUnit.DAYS.between(dataRequest.requestDate, updatedDate)
                dataRequest.requestDate = updatedDate
                events.add(
                    DataSubjectRequestEvent(
                        actor = actor,
                        eventType = DataRequestEventType.REQUEST_DATE_UPDATED
                    )
                )

                dataRequest.requestDueDate = dataRequest.requestDueDate?.plusDays(daysChanged)
                dataRequest.requestOriginalDueDate = dataRequest.requestOriginalDueDate?.plusDays(daysChanged)
            }
        }

        if (
            dto.subjectEmailAddress != dataRequest.subjectEmailAddress ||
            dto.subjectMailingAddress != dataRequest.subjectMailingAddress ||
            dto.subjectPhoneNumber != dataRequest.subjectPhoneNumber ||
            dto.subjectFirstName != dataRequest.subjectFirstName ||
            dto.subjectLastName != dataRequest.subjectLastName ||
            submissionMethod != dataRequest.submissionMethod ||
            dto.dataRequestType != dataRequest.requestType
        ) {
            var message = ""
            if (dto.subjectEmailAddress != dataRequest.subjectEmailAddress) {
                val before = dataRequest.subjectEmailAddress
                dataRequest.subjectEmailAddress = if (dto.subjectEmailAddress.isNullOrBlank()) {
                    null
                } else {
                    dto.subjectEmailAddress
                }
                message += if (before == null) {
                    "Email address changed to ${dto.subjectEmailAddress}."
                } else if (dataRequest.subjectEmailAddress == null) {
                    "Email address removed."
                } else {
                    "Email address changed from $before to ${dataRequest.subjectEmailAddress}."
                } + "\n\n"
            }
            if (dto.subjectPhoneNumber != dataRequest.subjectPhoneNumber) {
                val before = dataRequest.subjectPhoneNumber
                dataRequest.subjectPhoneNumber = if (dto.subjectPhoneNumber.isNullOrBlank()) {
                    null
                } else {
                    dto.subjectPhoneNumber
                }
                message += if (before == null) {
                    "Phone number changed to ${dto.subjectPhoneNumber}."
                } else if (dataRequest.subjectPhoneNumber == null) {
                    "Phone number removed."
                } else {
                    "Phone number changed from $before to ${dataRequest.subjectPhoneNumber}."
                } + "\n\n"
            }
            if (dto.subjectMailingAddress != dataRequest.subjectMailingAddress) {
                val before = dataRequest.subjectMailingAddress
                dataRequest.subjectMailingAddress = if (dto.subjectMailingAddress.isNullOrBlank()) {
                    null
                } else {
                    dto.subjectMailingAddress
                }
                message += if (before == null) {
                    "Mailing address changed to ${dto.subjectMailingAddress}."
                } else if (dataRequest.subjectMailingAddress == null) {
                    "Mailing address removed."
                } else {
                    "Mailing address changed from $before to ${dataRequest.subjectMailingAddress}."
                } + "\n\n"
            }
            if (dto.subjectFirstName != dataRequest.subjectFirstName) {
                val before = dataRequest.subjectFirstName
                dataRequest.subjectFirstName = if (dto.subjectFirstName.isNullOrBlank()) {
                    null
                } else {
                    dto.subjectFirstName
                }
                message += if (before == null) {
                    "First name changed to ${dto.subjectFirstName}."
                } else if (dataRequest.subjectFirstName == null) {
                    "First name removed."
                } else {
                    "First name changed from $before to ${dataRequest.subjectFirstName}."
                } + "\n\n"
            }
            if (dto.subjectLastName != dataRequest.subjectLastName) {
                val before = dataRequest.subjectLastName
                dataRequest.subjectLastName = if (dto.subjectLastName.isNullOrBlank()) {
                    null
                } else {
                    dto.subjectLastName
                } + "\n\n"
                message += if (before == null) {
                    "Last name changed to ${dto.subjectLastName}."
                } else if (dataRequest.subjectLastName == null) {
                    "Last name removed."
                } else {
                    "Last name changed from $before to ${dataRequest.subjectLastName}."
                } + "\n\n"
            }
            if (submissionMethod != dataRequest.submissionMethod) {
                val before = dataRequest.submissionMethod
                dataRequest.submissionMethod = submissionMethod
                message += "Submission method changed from $before to ${dataRequest.submissionMethod}.\n\n"
            }
            if (dto.dataRequestType != dataRequest.requestType && dto.dataRequestType != null) {
                val before = dataRequest.requestType
                dataRequest.requestType = dto.dataRequestType
                message += "Data request type changed from ${before!!.label} to ${dataRequest.requestType!!.label}.\n\n"
            }
            events.add(
                DataSubjectRequestEvent(
                    actor = actor,
                    eventType = DataRequestEventType.REQUEST_UPDATED,
                    message = message.trim()
                )
            )
        }

        dataRequestRepository.save(dataRequest)
        addEventsToRequest(dataRequest, events)

        return dataRequest
    }

    fun extend(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        dto: ExtendDataRequest
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val dataRequest = lookupDataRequestOrThrow(organization, requestId)

        if (dataRequest.state == DataRequestState.CLOSED) {
            throw PolarisException("Cannot extend a closed request")
        }
        val events = mutableListOf<DataSubjectRequestEvent>()
        val updatedDueDate = dataRequest.requestDueDate!!.plusDays(dataRequest.requestType.extendDueDateDays)

        dataRequest.requestDueDate = updatedDueDate

        dataRequestRepository.save(dataRequest)

        val result = emailService.sendExtendDueDate(actor, dataRequest, dto.notice)
        val msg = emailService.getExtendDueDateBody(actor, dataRequest, dto.notice)

        events.add(
            DataSubjectRequestEvent(
                actor = actor,
                eventType = DataRequestEventType.REQUEST_DEADLINE_UPDATED,
                message = msg
            )
        )

        addEventsToRequest(dataRequest, events)

        if (result != null && result.statusCode == 500) {
            log.info("500 nylas code... throwing send error")
            throw NylasSendError()
        }

        return dataRequest
    }

    fun transition(actor: User?, request: DataSubjectRequest, dto: TransitionDataRequest, events: MutableList<EventData>): DataSubjectRequest {
        events.add(
            EventData.DataSubjectRequestTransition(
                requestId = request.publicId,
                from = request.state,
                fromSubstate = request.substate,
                to = dto.state,
                toSubstate = dto.substate
            )
        )
        dataRequestWorkflowService.transition(request, actor, dto.notes, dto.attachment, dto.substate, dto.state)

        return dataRequestRepository.save(request)
    }

    fun transition(
        actor: User?,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        dto: TransitionDataRequest,
        events: MutableList<EventData>
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val dataRequest = lookupDataRequestOrThrow(organization, requestId)
        return transition(actor, dataRequest, dto, events)
    }

    fun verify(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        verifyDataRequest: VerifyDataRequest,
        events: MutableList<EventData>
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val request = lookupDataRequestOrThrow(organization, requestId)
        if (verifyDataRequest.consumerGroupId != UUID.fromString("00000000-0000-0000-0000-000000000000")) {
            val dataSubjectType =
                collectionGroupService.findByIdAndOrganization(verifyDataRequest.consumerGroupId, organization)
            log.info("Updating request $requestId to ${dataSubjectType.name} - ${verifyDataRequest.consumerGroupId}")
            request.collectionGroups = mutableListOf(dataSubjectType)
            dataRequestRepository.save(request)
        }
        transition(
            actor,
            request,
            TransitionDataRequest(state = DataRequestState.PROCESSING, notes = verifyDataRequest.notes),
            events
        )
        return request
    }

    fun confirmEmail(
        actor: User?,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        confirmIdentityRequest: ConfirmIdentityDataRequest,
        privacyCenter: PrivacyCenter,
        confirmTime: ZonedDateTime,
        events: MutableList<EventData>
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val request = lookupDataRequestOrThrow(organization, requestId)

        if (confirmIdentityRequest.confirmationToken != request.confirmationToken) {
            throw ConfirmationTokenDoesNotMatch(confirmIdentityRequest.confirmationToken, request.id.toString())
        }

        var missingCustomField: Boolean = false
        var missingDeclaration: Boolean = false

        // ensure that we have the custom field added if needed
        log.info("checking for custom field: ${organization.customFieldEnabled} / ${confirmIdentityRequest.organizationCustomInput.isNullOrBlank()} / ${request.organizationCustomInput.isNullOrBlank()}")
        if (organization.customFieldEnabled && confirmIdentityRequest.organizationCustomInput.isNullOrBlank() && request.organizationCustomInput.isNullOrBlank()) {
            missingCustomField = true
        } else if (organization.customFieldEnabled && request.organizationCustomInput.isNullOrBlank()) {
            request.organizationCustomInput = confirmIdentityRequest.organizationCustomInput
        }

        // ensure we have a declaration if we are a Right to Know request
        if (request.requiresDeclaration()) {
            val declarationConfirmation = confirmIdentityRequest.declarationConfirmation ?: false
            if (!declarationConfirmation && !request.selfDeclarationProvided) {
                missingDeclaration = true
            } else if (declarationConfirmation) {
                request.selfDeclarationProvided = declarationConfirmation
            }
        }

        if (missingCustomField || missingDeclaration) {
            throw MissingDeclarationOrCustomField(
                dataRequest = request,
                missingDeclaration = missingDeclaration,
                missingCustomField = missingCustomField,
                customFieldLabel = organization.customFieldLabel,
                customFieldHelp = organization.customFieldHelp
            )
        }

        // Verify the email
        request.emailVerifiedAutomatically = true
        request.emailVerifiedDate = confirmTime
        request.requestDueDate = request.requestDueDate ?: confirmTime.plusDays(request.requestType.initialDueDateDays)
        dataRequestRepository.save(request)

        val event = DataSubjectRequestEvent(
            eventType = DataRequestEventType.IDENTITY_VERIFIED,
            isDataSubject = true
        )

        addEventsToRequest(request, listOf(event))

        transition(
            actor,
            request,
            TransitionDataRequest(state = request.requestType.workflow.initialState),
            events
        )

        onboardingEmailService.sendOnboardingEmailOnFirst(request)
        emailService.sendNewDataSubjectRequestNotification(request, privacyCenter.name)

        return request
    }

    fun reopen(actor: User, organizationId: OrganizationPublicId, requestId: DataRequestPublicId, events: MutableList<EventData>): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val dataRequest = lookupDataRequestOrThrow(organization, requestId)

        val state = dataRequest.stateHistory
            .lastOrNull { it != DataRequestState.CLOSED } ?: DataRequestState.PENDING_EMAIL_VERIFICATION

        return transition(actor, organizationId, requestId, TransitionDataRequest(state = state, substate = null), events)
    }

    fun createNote(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        dto: CreateNoteRequest
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val dataRequest = lookupDataRequestOrThrow(organization, requestId)

        if (dto.message.isBlank()) {
            throw InvalidNote("The note could not be added because the message was blank.")
        }

        val attachments = if (dto.attachments != null) {
            dto.attachments.mapNotNull {
                saveAttachment(it)
            }
        } else {
            null
        }

        val message = requestTemplateService.dataRequestMessageTemplate(actor, organization, dataRequest, dto.message)
        val event = DataSubjectRequestEvent(
            actor = actor,
            eventType = DataRequestEventType.NOTE_ADDED,
            message = message
        )

        if (!attachments.isNullOrEmpty()) {
            addEventWithAttachmentsToRequest(dataRequest, event, attachments = attachments)
        } else {
            addEventsToRequest(dataRequest, listOf(event))
        }

        dataRequestRepository.refresh(dataRequest)

        return dataRequest
    }

    /*** Creators ***/
    // If actor is null then we assume it was created by an anonymous user via an external host mechanism
    fun create(
        actor: User?,
        organizationId: OrganizationPublicId,
        dto: CreateDataRequest,
        isAutocreated: Boolean = false
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val dataSubjectType = dto.dataSubjectTypeId?.let {
            collectionGroupService.findByIdAndOrganization(
                UUID.fromString(dto.dataSubjectTypeId),
                organization
            )
        }

        // Coerce to a submission method enum
        val submissionMethod = dto.submissionMethod?.let {
            DataRequestSubmissionMethodEnum.values().firstOrNull {
                it.name.equals(dto.submissionMethod, true)
            }
        }

        val requestAddDays = dto.dataRequestType.initialDueDateDays
        // NOTE: this is not a scalable way to differentiate rules for requests across regulations/types
        val requestDueDate = if (dto.dataRequestType.isDueFromVerificationDate && dto.skipEmailVerification != true) null else dto.requestDate.toDateTime()
            .plusDays(requestAddDays)
        var state = if (dto.dataRequestType.requireVerification) DataRequestState.PENDING_EMAIL_VERIFICATION else dto.dataRequestType.workflow.initialState
        var emailVerifiedManually = false

        if (state == DataRequestState.PENDING_EMAIL_VERIFICATION && dto.submissionSource == DataRequestSubmissionSourceEnum.POLARIS && dto.skipEmailVerification == true) {
            emailVerifiedManually = true
            state = dto.dataRequestType.workflow.initialState
        }

        log.info("declaration: ${dto.selfDeclarationProvided}")
        var retries = 0
        while (retries < 5) {
            retries++

            try {
                val dataRequest = dataRequestRepository.save(
                    DataSubjectRequest(
                        publicId = IdGenerator.generate(),
                        organization = organization,
                        requestType = dto.dataRequestType,
                        state = state,
                        substate = null,
                        collectionGroups = dataSubjectType?.let { mutableListOf(dataSubjectType) } ?: mutableListOf(),
                        subjectFirstName = dto.subjectFirstName,
                        subjectLastName = dto.subjectLastName,
                        subjectEmailAddress = dto.subjectEmailAddress,
                        subjectPhoneNumber = dto.subjectPhoneNumber,
                        submissionMethod = submissionMethod,
                        submissionSource = dto.submissionSource,
                        subjectMailingAddress = dto.subjectMailingAddress,
                        subjectIpAddress = dto.subjectIpAddress,
                        organizationCustomInput = dto.organizationCustomInput,
                        selfDeclarationProvided = dto.selfDeclarationProvided,
                        correctionRequest = dto.correctionRequest,
                        objectionRequest = dto.objectionRequest,
                        appealDecisionId = dto.appealId,
                        consentWithdrawnRequest = dto.consentWithdrawnRequest?.toMutableList(),
                        requestDate = dto.requestDate.toDateTime(),
                        // We use the provided time-zone offset (if available) to determine the localized end-of-day
                        // which depends on the client providing an accurate offset in the request payload, if it doesn't
                        // provide one then we assume UTC time for end-of-day
                        requestDueDate = requestDueDate,
                        requestOriginalDueDate = requestDueDate,
                        requestorContacted = false,
                        isAutocreated = isAutocreated,
                        emailVerifiedManually = emailVerifiedManually,
                        emailVerifiedManuallyBy = if (emailVerifiedManually) actor else null,
                        emailVerifiedDate = if (emailVerifiedManually) ZonedDateTime.now() else null,
                        gaUserId = dto.gaUserId,
                        context = dto.context
                    )
                )

                val events = mutableListOf<DataSubjectRequestEvent>()

                events.add(
                    DataSubjectRequestEvent(
                        actor = actor,
                        isDataSubject = dto.isDataSubject,
                        eventType = DataRequestEventType.CREATED,
                        createdAt = ZonedDateTime.now(),
                        message = dto.notes
                    )
                )

                if (emailVerifiedManually) {
                    events.add(
                        DataSubjectRequestEvent(
                            actor = actor,
                            eventType = DataRequestEventType.IDENTITY_VERIFIED,
                            createdAt = ZonedDateTime.now()
                        )
                    )
                }

                if (dataRequest.subjectEmailAddress != null && state == DataRequestState.PENDING_EMAIL_VERIFICATION) {
                    val hasDuplicates = findDuplicateRequests(dataRequest).isNotEmpty()
                    if (!hasDuplicates) {
                        events.add(
                            DataSubjectRequestEvent(
                                actor = null,
                                eventType = DataRequestEventType.IDENTITY_VERIFICATION_SENT,
                                createdAt = ZonedDateTime.now(),
                                message = ""
                            )
                        )
                        emailService.sendEmailAddressVerification(dataRequest)
                    }
                }

                addEventsToRequest(dataRequest, events)
                dataRequestRepository.refresh(dataRequest)

                // Return the DSR
                return dataRequest
            } catch (e: DataIntegrityViolationException) {
                log.error("Data Integrity Violation Exception: {}", e)
                if (retries == 5) {
                    throw e
                }
            }
        }

        throw DataRequestNotFound("", organizationId)
    }

    fun createOptOutRequest(
        actor: User?,
        organizationId: OrganizationPublicId,
        dto: CreateDataRequest,
        autoClose: Boolean
    ): DataSubjectRequest {
        val request = create(actor, organizationId, dto)
        if (autoClose) {
            request.state = DataRequestState.CLOSED
            request.substate = DataRequestSubstate.COMPLETED
            request.closedAt = ZonedDateTime.now()
            request.isAutoclosed = true
            request.gpcOptOut = dto.gpcOptOut

            addEventsToRequest(
                request,
                listOf(
                    DataSubjectRequestEvent(
                        actor = null,
                        isDataSubject = false,
                        eventType = DataRequestEventType.CLOSED,
                        createdAt = ZonedDateTime.now(),
                        message = "Auto-closed by system"
                    )
                )
            )
        }
        return dataRequestRepository.findByIdOrNull(request.id)!!
    }

    internal fun addEventsToRequest(
        dataRequest: DataSubjectRequest,
        events: List<DataSubjectRequestEvent>
    ): List<DataSubjectRequestEvent> {
        val updatedEvents = events.map { it.copy(dataSubjectRequest = dataRequest) }
        dataRequestEventRepository.saveAll(updatedEvents)
        return updatedEvents
    }

    private fun addEventWithAttachmentsToRequest(
        dataRequest: DataSubjectRequest,
        event: DataSubjectRequestEvent,
        attachments: List<Attachment>
    ): List<DataSubjectRequestEventAttachment> {
        val updatedEvent = addEventsToRequest(dataRequest, listOf(event))[0]
        return attachments.map {
            dataRequestEventAttachmentRepository.save(
                DataSubjectRequestEventAttachment(
                    dataSubjectRequestEvent = updatedEvent,
                    attachment = it
                )
            )
        }
    }

    /** Workflow Step Updates **/

    fun stepForwardRequest(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        events: MutableList<EventData>
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val request = lookupDataRequestOrThrow(organization, requestId)

        return stepRequestForward(actor, organization, request, events)
    }

    fun stepBackRequest(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        events: MutableList<EventData>
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val request = lookupDataRequestOrThrow(organization, requestId)

        return stepRequestBack(actor, organization, request, events)
    }

    fun stepRequestForward(
        actor: User,
        organization: Organization,
        request: DataSubjectRequest,
        events: MutableList<EventData>
    ): DataSubjectRequest {

        val tx = request.requestType.workflow.handler.forward(request.state, actor, organization, request, this)
        if (tx != null) {
            transition(actor, request, tx, events)
        }
        return request
    }

    fun stepRequestBack(
        actor: User,
        organization: Organization,
        request: DataSubjectRequest,
        events: MutableList<EventData>
    ): DataSubjectRequest {

        val tx = request.requestType.workflow.handler.backward(request.state, actor, organization, request, this)
        if (tx != null) {
            transition(actor, request, tx, events)

            // update state history
            val stateIndex = request.stateHistory.indexOf(request.state)
            if (stateIndex != -1) {
                request.stateHistory = request.stateHistory.subList(0, stateIndex)
                dataRequestRepository.save(request)
            }
        }

        return request
    }

    fun updateVendorOutcome(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        vendorUpdate: UpdateVendorRequest
    ): DataSubjectRequest {

        val organization = lookup.orgOrThrow(organizationId)
        val request = lookupDataRequestOrThrow(organization, requestId)
        dataRequestWorkflowService.upsertVendorOutcome(
            request = request,
            vendorId = UUID.fromString(vendorUpdate.vendor),
            outcomeValue = vendorUpdate.outcome
        )

        entityManager.flush()
        entityManager.clear()

        return lookupDataRequestOrThrow(organization, requestId)
    }

    fun contactVendors(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        contactVendors: ContactVendorRequest
    ): DataSubjectRequest {

        val organization = lookup.orgOrThrow(organizationId)
        val orgPrivacyEmailAddress = organization.privacyEmailAddress()
        val request = lookupDataRequestOrThrow(organization, requestId)
        for (vendor in contactVendors.vendors) {
            val vendorId = UUID.fromString(vendor)
            val vendorOutcome = request.vendorOutcomes.find { it.vendor!!.id == vendorId }
            val orgVendor = organization.organizationDataRecipients.find { it.vendor!!.id == vendorId }
            val recipientEmailAddress =
                if (orgVendor!!.email != null) orgVendor.email!! else orgVendor!!.vendor!!.email!!

            val emailBody = when (request.requestType.workflow) {
                RequestWorkflow.LIMIT -> emailService.getServiceProviderRequestToLimitBody(actor, request)
                else -> emailService.getServiceProviderRequestToDeleteBody(actor, request)
            }

            queuedEmailService.enqueueEmail(
                QueuedEmail(
                    dataSubjectRequest = request,
                    recipientEmailAddress = recipientEmailAddress,
                    senderEmailAddress = orgPrivacyEmailAddress,
                    subject = emailService.getServiceProviderRequestSubject(request),
                    message = emailBody
                )
            )

            if (vendorOutcome != null) {
                vendorOutcome.processed = true
                vendorOutcome.processedAt = ZonedDateTime.now()
                vendorOutcomeRepository.save(vendorOutcome)
            } else {
                vendorOutcomeRepository.save(
                    VendorOutcome(
                        dataSubjectRequestId = request.id,
                        vendorId = vendorId,
                        processed = true,
                        processedAt = ZonedDateTime.now()
                    )
                )
            }
        }
        return lookupDataRequestOrThrow(organization, requestId)
    }

    fun contactRequestor(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        attachment: List<MultipartFile> = emptyList()
    ): DataSubjectRequest {

        val organization = lookup.orgOrThrow(organizationId)
        val request = lookupDataRequestOrThrow(organization, requestId)
        var notes = ""

        if (request.collectionGroupResult == SubjectStatus.NOT_FOUND) {
            // not found email
            emailService.sendSubjectRequestNotFound(actor, request)
            notes = emailService.getSubjectRequestNotFoundBody(actor, request)
        } else if (request.collectionGroupResult == SubjectStatus.PROCESSED_AS_SERVICE_PROVIDER) {
            // service provider email
            emailService.sendSubjectRequestServiceProvider(actor, request)
            notes = emailService.getSubjectRequestServiceProviderBody(actor, request)
        } else {
            notes = when (request.requestType.workflow) {
                RequestWorkflow.OPT_OUT -> {
                    emailService.sendSubjectRequestClosedOptOut(actor, request)
                    emailService.getSubjectRequestClosedOptOutBody(actor, request)
                }
                RequestWorkflow.KNOW -> {
                    emailService.sendSubjectRequestClosedKnow(actor, request, attachment)
                    emailService.getSubjectRequestClosedKnowBody(actor, request)
                }
                RequestWorkflow.DELETE -> {
                    emailService.sendSubjectRequestClosedDelete(actor, request)
                    emailService.getSubjectRequestClosedDeleteBody(actor, request)
                }
                RequestWorkflow.PORTABILITY -> {
                    emailService.sendSubjectRequestClosedPortability(actor, request, attachment)
                    emailService.getSubjectRequestClosedPortabilityBody(actor, request)
                }
                RequestWorkflow.RECTIFICATION -> {
                    emailService.sendSubjectRequestClosedRectification(actor, request)
                    emailService.getSubjectRequestClosedRectificationBody(actor, request)
                }
                RequestWorkflow.WITHDRAW_CONSENT -> {
                    emailService.sendSubjectRequestClosedWithdrawnConsent(actor, request)
                    emailService.getSubjectRequestClosedWithdrawnConsentBody(actor, request)
                }
                RequestWorkflow.OBJECTION -> {
                    emailService.sendSubjectRequestClosedObjection(actor, request)
                    emailService.getSubjectRequestClosedObjectionBody(actor, request)
                }
                RequestWorkflow.LIMIT -> {
                    emailService.sendSubjectRequestClosedLimit(actor, request)
                    emailService.getSubjectRequestClosedLimitBody(actor, request)
                }
                RequestWorkflow.NONE -> {
                    ""
                }
            }
        }
        val event = DataSubjectRequestEvent(
            actor = actor,
            dataSubjectRequest = request,
            eventType = DataRequestEventType.REQUESTOR_CONTACTED,
            message = notes
        )

        dataRequestEventRepository.save(event)

        request.dataSubjectRequestEvents.add(event)
        request.requestorContacted = true

        dataRequestRepository.save(request)

        return lookupDataRequestOrThrow(organization, requestId)
    }

    fun setRequestSubjectStatus(
        actor: User,
        organizationId: OrganizationPublicId,
        requestId: DataRequestPublicId,
        statusUpdate: RequestSubjectStatusUpdate
    ): DataSubjectRequest {
        val organization = lookup.orgOrThrow(organizationId)
        val request = lookupDataRequestOrThrow(organization, requestId)

        request.collectionGroupResult = statusUpdate.subjectStatus
        dataRequestRepository.save(request)
        return request
    }

    /*** Delete ***/
    fun deleteByPublicId(currentUser: User, organizationId: OrganizationPublicId, requestId: DataRequestPublicId) {
        val organization = lookup.orgOrThrow(organizationId)
        val organizationUser = organizationUserRepository.findByUserAndOrganization(currentUser, organization)
        if (organizationUser?.role != UserRole.ADMIN) {
            throw PolarisException("User ${currentUser.id} us not authorized to remove privacy requests")
        }
        val request = lookupDataRequestOrThrow(organization, requestId)

        val duplicates = dataRequestRepository.findAllByDuplicatesRequest(request)
        duplicates.forEach { handleDelete(it) }

        handleDelete(request)
    }

    /*** Helpers ***/

    private fun handleDelete(request: DataSubjectRequest) {
        request.dataSubjectRequestEvents
            .flatMap { e -> e.attachments.mapNotNull { ea -> ea.attachment?.id } }
            .map { id -> attachmentService.deleteAttachment(id) }
        dataRequestRepository.delete(request)
    }

    private fun saveAttachment(attachment: MultipartFile?): Attachment? {
        return if (attachment != null) {
            attachmentService.createAttachment(attachment)
        } else {
            null
        }
    }

    private final val ALL_STATES = DataRequestState.values().toList()
    private final val PENDING_STATES = listOf(DataRequestState.PENDING_EMAIL_VERIFICATION)
    private final val COMPLETE_STATES = listOf(DataRequestState.CLOSED)
    private final val ACTIVE_STATES =
        ALL_STATES.filter { !PENDING_STATES.contains(it) && !COMPLETE_STATES.contains(it) }.toList()

    private fun statesForFilterOrThrow(filter: DataRequestFilter?): List<DataRequestState> {
        return when (filter) {
            DataRequestFilter.all -> ALL_STATES
            DataRequestFilter.active -> ACTIVE_STATES
            DataRequestFilter.complete -> COMPLETE_STATES
            DataRequestFilter.pending -> PENDING_STATES
            null -> DataRequestState.values().toList()
        }
    }

    private fun lookupDataRequestOrThrow(organization: Organization, requestId: DataRequestPublicId) =
        dataRequestRepository.findByOrganizationAndPublicId(organization, requestId) ?: throw DataRequestNotFound(
            requestId,
            organization.publicId
        )

    fun contactVendorRequiredByDefault(request: DataSubjectRequest): Boolean {
        val vendors = organizationVendorService.allVendorsForOrganization(request.organization!!.publicId)

        return !vendors.all { odr ->
            val outcome = vendorOutcomeRepository.findByDataSubjectRequestAndVendor(request, odr.vendor!!)
            val instructions =
                requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(
                    request.organization,
                    DataRequestInstructionType.DELETE,
                    odr
                )

            val hasDeletionInstructions = !odr.vendor.deletionInstructions.isNullOrBlank()
            val hasOutcome = outcome?.outcome != null
            val hasRetainedOutcome = outcome?.outcome != null && outcome.outcome == VendorOutcomeValue.DATA_RETAINED
            val hasNoDataOutcome = outcome?.outcome != null && outcome.outcome == VendorOutcomeValue.NO_DATA
            val hasRetainedProcessingInstruction = instructions?.processingMethod == ProcessingMethod.RETAIN

            // skip if any of these three are true:
            //  - has deletion instructions
            //  - has *no* outcome, and has a retained instruction
            //  - has an outcome, and is a retained outcome
            val goodDeletionInstructions: Boolean = hasDeletionInstructions
            val goodProcessingInstructions = (!hasOutcome && hasRetainedProcessingInstruction)
            val goodOutcome = hasOutcome && (hasRetainedOutcome || hasNoDataOutcome)

            val canSkipVendor = goodDeletionInstructions || goodProcessingInstructions || goodOutcome
            canSkipVendor
        }
    }

    fun isContactVendorStepHidden(request: DataSubjectRequest): Boolean {
        if (request.organization == null) {
            return false
        }

        val contactRequired = contactVendorRequiredByDefault(request)
        val isStepHidden = !contactRequired

        if (isStepHidden && request.contactVendorsEverHidden != true) {
            request.contactVendorsEverHidden = true
            dataRequestRepository.save(request)
        }
        if (!isStepHidden && request.contactVendorsEverShown != true) {
            request.contactVendorsEverShown = true
            dataRequestRepository.save(request)
        }

        return isStepHidden
    }
}
