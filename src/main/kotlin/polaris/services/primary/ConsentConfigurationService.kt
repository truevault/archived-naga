package polaris.services.primary

import org.springframework.stereotype.Service
import polaris.models.PrivacyCenterPublicId
import polaris.models.entity.organization.Organization
import polaris.models.entity.privacycenter.PrivacyCenter

@Service
class ConsentConfigurationService {
    fun buildConsentConfiguration(org: Organization, privacyCenter: PrivacyCenter): CMPOptionsDto {
        return CMPOptionsDto(
            ccpa = buildCCPAOptions(org, privacyCenter),
            consent = buildConsentOptions(org, privacyCenter)
        )
    }

    private fun buildCCPAOptions(org: Organization, privacyCenter: PrivacyCenter): CCPAOptionsDto {
        return CCPAOptionsDto(
            privacyCenterId = privacyCenter.publicId,
            regionFilter = buildRegionFilter(org)
        )
    }

    private fun buildConsentOptions(org: Organization, privacyCenter: PrivacyCenter): ConsentOptionsDto {
        return ConsentOptionsDto(
            privacyCenterId = privacyCenter.publicId,
            consentRegionFilter = buildRegionFilter(org),
            enableConsentManager = org.isGdpr()
        )
    }

    private fun buildRegionFilter(org: Organization): List<String> {
        // NOTE: return just "CA" for now until we refactor how the region filter can
        //       be used to more intelligently inject/show/hide region-specific links
        // return if (org.isMultistate()) listOf("CA", "VA") else listOf("CA")
        return listOf("CA")
    }
}

enum class ButtonPosition(val position: String) {
    LEFT("left"),
    RIGHT("right")
}

data class CMPOptionsDto(
    val ccpa: CCPAOptionsDto,
    val consent: ConsentOptionsDto
)

data class ConsentOptionsDto(
    val buttonPosition: ButtonPosition = ButtonPosition.LEFT,
    val consentCountryFilter: List<String>? = null,
    val consentRegionFilter: List<String>? = null,
    val cookiePolicyUrl: String? = null,
    val debug: Boolean = false,
    val enableConsentManager: Boolean,
    val hideCookieButton: Boolean = false,
    val polarisUrl: String? = null,
    val privacyCenterId: PrivacyCenterPublicId
)

data class CCPAOptionsDto(
    val consumerLinkElementId: List<String>? = null,
    val consumerLinkElementClass: List<String>? = null,
    val consumerLinkUnhideSleepMs: Number = 20000,
    val countryFilter: List<String>? = null,
    val debug: Boolean = false,
    val domain: String? = null,
    val googleAnalyticsTrackingId: String? = null,
    val lspaCovered: Boolean = false,
    val notApplicableExpirationDays: Number = 30,
    val optOutExpirationDays: Number = 365,
    val optOutLinkClass: List<String>? = null,
    val privacyCenterId: PrivacyCenterPublicId,
    val regionFilter: List<String>? = null
)
