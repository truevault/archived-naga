package polaris.services

import polaris.models.entity.compliance.PersonalInformationCategory
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.request.DataSubjectRequest

sealed class HookArgument {
    data class DataSubjectRequestCreated(val dsr: DataSubjectRequest) : HookArgument()
    data class OrganizationDataRecipientCreated(val organization: Organization, val organizationDataRecipient: OrganizationDataRecipient) : HookArgument()

    data class CollectedPicCreated(val organization: Organization, val pic: PersonalInformationCategory, val cg: CollectionGroup) : HookArgument()
    data class CollectedPicDeleted(val organization: Organization, val pic: PersonalInformationCategory, val cg: CollectionGroup) : HookArgument()
}
