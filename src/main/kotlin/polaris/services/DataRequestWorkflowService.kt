package polaris.services

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import polaris.models.dto.request.VendorOutcomeValue
import polaris.models.entity.request.*
import polaris.models.entity.user.User
import polaris.models.entity.vendor.DataRecipientType
import polaris.repositories.*
import polaris.services.support.AttachmentService
import polaris.services.support.EmailService
import polaris.util.states.GuardResult
import polaris.util.states.StateMachine
import polaris.util.states.StateMachineState
import polaris.util.states.TransitionGuardFailedException
import java.time.ZonedDateTime
import java.util.UUID

@Service
@Transactional
class DataRequestWorkflowService(
    private val dataRequestEventRepository: DataRequestEventRepository,
    private val vendorOutcomeRepository: VendorOutcomeRepository,
    private val attachmentService: AttachmentService,
    private val emailService: EmailService
) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(DataRequestWorkflowService::class.java)
    }

    private val definition = StateMachine.define<DataRequestContext, DataRequestState> {
        initial(DataRequestState.PENDING_EMAIL_VERIFICATION)

        transition(DataRequestState.PENDING_EMAIL_VERIFICATION, DataRequestState.VERIFY_CONSUMER)
        transition(DataRequestState.PENDING_EMAIL_VERIFICATION, DataRequestState.REMOVE_CONSUMER)

        // Request To Delete Valid Steps
        // Forward
        transition(DataRequestState.VERIFY_CONSUMER, DataRequestState.DELETE_INFO)
        transition(DataRequestState.VERIFY_CONSUMER, DataRequestState.CONTACT_VENDORS)
        transition(DataRequestState.VERIFY_CONSUMER, DataRequestState.NOTIFY_CONSUMER)
        transition(DataRequestState.DELETE_INFO, DataRequestState.CONTACT_VENDORS)
        transition(DataRequestState.DELETE_INFO, DataRequestState.NOTIFY_CONSUMER)
        transition(DataRequestState.CONTACT_VENDORS, DataRequestState.NOTIFY_CONSUMER)

        // Backward
        transition(DataRequestState.NOTIFY_CONSUMER, DataRequestState.CONTACT_VENDORS)
        transition(DataRequestState.NOTIFY_CONSUMER, DataRequestState.DELETE_INFO)
        transition(DataRequestState.NOTIFY_CONSUMER, DataRequestState.VERIFY_CONSUMER)
        transition(DataRequestState.CONTACT_VENDORS, DataRequestState.DELETE_INFO)
        transition(DataRequestState.CONTACT_VENDORS, DataRequestState.VERIFY_CONSUMER)
        transition(DataRequestState.DELETE_INFO, DataRequestState.VERIFY_CONSUMER)

        // Request to Know Valid Steps
        // Forward
        transition(DataRequestState.VERIFY_CONSUMER, DataRequestState.RETRIEVE_DATA)
        transition(DataRequestState.VERIFY_CONSUMER, DataRequestState.SEND_DATA)
        transition(DataRequestState.RETRIEVE_DATA, DataRequestState.SEND_DATA)
        // Backward
        transition(DataRequestState.RETRIEVE_DATA, DataRequestState.VERIFY_CONSUMER)
        transition(DataRequestState.SEND_DATA, DataRequestState.VERIFY_CONSUMER)
        transition(DataRequestState.SEND_DATA, DataRequestState.RETRIEVE_DATA)

        // Rectification
        transition(DataRequestState.VERIFY_CONSUMER, DataRequestState.CORRECT_DATA)
        transition(DataRequestState.CORRECT_DATA, DataRequestState.NOTIFY_CONSUMER)
        transition(DataRequestState.NOTIFY_CONSUMER, DataRequestState.CORRECT_DATA)
        transition(DataRequestState.CORRECT_DATA, DataRequestState.VERIFY_CONSUMER)

        // Objection
        transition(DataRequestState.VERIFY_CONSUMER, DataRequestState.EVALUATE_OBJECTION)
        transition(DataRequestState.EVALUATE_OBJECTION, DataRequestState.NOTIFY_CONSUMER)
        transition(DataRequestState.NOTIFY_CONSUMER, DataRequestState.EVALUATE_OBJECTION)
        transition(DataRequestState.EVALUATE_OBJECTION, DataRequestState.VERIFY_CONSUMER)

        // Withdawal
        transition(DataRequestState.VERIFY_CONSUMER, DataRequestState.REMOVE_CONSENT)
        transition(DataRequestState.REMOVE_CONSENT, DataRequestState.NOTIFY_CONSUMER)
        transition(DataRequestState.NOTIFY_CONSUMER, DataRequestState.REMOVE_CONSENT)
        transition(DataRequestState.REMOVE_CONSENT, DataRequestState.VERIFY_CONSUMER)

        // Opt out
        transition(DataRequestState.REMOVE_CONSUMER, DataRequestState.NOTIFY_CONSUMER)
        transition(DataRequestState.NOTIFY_CONSUMER, DataRequestState.REMOVE_CONSUMER)

        // Limit
        transition(DataRequestState.VERIFY_CONSUMER, DataRequestState.LIMIT_DATA)
        transition(DataRequestState.LIMIT_DATA, DataRequestState.VERIFY_CONSUMER)

        transition(DataRequestState.LIMIT_DATA, DataRequestState.CONTACT_VENDORS)
        transition(DataRequestState.CONTACT_VENDORS, DataRequestState.LIMIT_DATA)

        // transition(DataRequestState.CONSUMER_GROUP_SELECTION, DataRequestState.PROCESSING)
        // transition(DataRequestState.PROCESSING, DataRequestState.CLOSED)

        // transition(DataRequestState.PROCESSING, DataRequestState.CONSUMER_GROUP_SELECTION)

        fromAny(DataRequestState.CLOSED)
        toAny(DataRequestState.CLOSED)

        // guard(DataRequestState.PENDING_EMAIL_VERIFICATION, DataRequestState.CONSUMER_GROUP_SELECTION) { GuardResult(it.dataRequest.emailVerified, "Email must be verified") }

        // guard(DataRequestState.PROCESSING, DataRequestState.CLOSED) { GuardResult(it.substate != null, "The request must have a substate when it is closed.") }
        guardFromAny(DataRequestState.CLOSED) {
            GuardResult(
                it.substate != null,
                "The request must have a substate when it is closed."
            )
        }

        // after(::updateSubstate)
        // after(::updateVendorOutcomes)
        // after(::saveAttachment)

        // after(::sendClosedEmails)
        after(::createEvent)
        after(::setClosedAt)

        after(DataRequestState.DELETE_INFO, DataRequestState.CONTACT_VENDORS, ::autoSelectRetained)
    }

    fun updateSubstate(c: DataRequestContext, f: DataRequestState, t: DataRequestState) {
        if (t == DataRequestState.CLOSED) {
            c.dataRequest.substate = c.substate
        } else {
            c.dataRequest.substate = null
        }
    }

    fun setClosedAt(c: DataRequestContext, f: DataRequestState, t: DataRequestState) {
        c.dataRequest.closedAt = if (t == DataRequestState.CLOSED) {
            ZonedDateTime.now()
        } else {
            null
        }
    }

    fun createEvent(c: DataRequestContext, f: DataRequestState, t: DataRequestState) {
        val type = eventType(c, f, t)

        if (type != null) {
            val event = DataSubjectRequestEvent(
                actor = c.actor,
                dataSubjectRequest = c.dataRequest,
                eventType = type,
                message = c.notes
            )

            dataRequestEventRepository.save(event)
            c.dataRequest.dataSubjectRequestEvents.add(event)
        }
    }

    fun saveAttachment(c: DataRequestContext, f: DataRequestState, t: DataRequestState) {
        val requiresAttachment =
            c.dataRequest.requestType == DataRequestType.CCPA_RIGHT_TO_KNOW && t == DataRequestState.CLOSED && c.substate == DataRequestSubstate.COMPLETED
        val allowsAttachment = requiresAttachment

        if (c.attachment == null && requiresAttachment) {
            throw TransitionGuardFailedException("This transition requires an attachment, but none was provided")
        }

        if (c.attachment != null && !allowsAttachment) {
            throw TransitionGuardFailedException("This transition does not allow an attachment, but one was provided")
        }

        if (c.attachment != null) {
            c.dataRequest.attachment = attachmentService.createAttachment(c.attachment)
        }
    }

    fun sendClosedEmails(c: DataRequestContext, f: DataRequestState, t: DataRequestState) {
        if (f != DataRequestState.CLOSED && t == DataRequestState.CLOSED) {
            if (c.actor == null) {
                logger.warn("A data subject request was closed with no actor")
                return
            }

            if (c.substate == DataRequestSubstate.SUBJECT_NOT_FOUND) {
                // not found email
                emailService.sendSubjectRequestNotFound(c.actor, c.dataRequest)
                c.notes = emailService.getSubjectRequestNotFoundBody(c.actor, c.dataRequest)
            }
            if (c.substate == DataRequestSubstate.PROCESSED_AS_SERVICE_PROVIDER) {
                // service provider email
                emailService.sendSubjectRequestServiceProvider(c.actor, c.dataRequest)
                c.notes = emailService.getSubjectRequestServiceProviderBody(c.actor, c.dataRequest)
            }
        }
    }

    fun autoSelectRetained(c: DataRequestContext, f: DataRequestState, t: DataRequestState) {
        // Needs to stay in sync with list on frontend.
        // TODO: Move this and frontend filtering to the same source of truth so that we don't have to keep this stuff in sync
        val recommendRetainVendors = c.dataRequest.organization?.organizationDataRecipients?.filter { odr ->
            val notThirdPartyRecipient = odr.vendor?.dataRecipientType != DataRecipientType.third_party_recipient

            val vendorOutcome = c.dataRequest.vendorOutcomes.find { it.vendor!!.id == odr.vendor?.id }
            if (vendorOutcome != null) {
                return@filter false
            }

            if (!odr.completed || !notThirdPartyRecipient) {
                return@filter false
            }
            return@filter odr.requestHandlingInstructions.any { it.requestType == DataRequestInstructionType.DELETE && it.processingMethod == ProcessingMethod.RETAIN }
        }

        recommendRetainVendors?.forEach {
            upsertVendorOutcome(request = c.dataRequest, vendorId = it.vendor!!.id, outcomeValue = VendorOutcomeValue.DATA_RETAINED)
        }
    }

    /**
     * Note: This is terrible. I'm sorry. We should fix this. This is a quick-workaround for a service dependency loop
     * between DataSubjectRequestService and DataRequestWorkflowService. This code used to live in the
     * DataSubjectRequestService, but we tried to call it from here.
     *
     * No other methods in this service actually directly commit to a repository, which is why this feels very out of
     * place to live here.
     */
    fun upsertVendorOutcome(request: DataSubjectRequest, vendorId: UUID, outcomeValue: VendorOutcomeValue) {
        val vendorOutcome = request.vendorOutcomes.find { it.vendor!!.id == vendorId }

        if (vendorOutcome != null) {
            vendorOutcome.outcome = outcomeValue
            vendorOutcomeRepository.save(vendorOutcome)
        } else {
            vendorOutcomeRepository.save(
                VendorOutcome(
                    dataSubjectRequestId = request.id,
                    vendorId = vendorId,
                    outcome = outcomeValue
                )
            )
        }
    }

    private fun eventType(c: DataRequestContext, f: DataRequestState, t: DataRequestState): DataRequestEventType? {
        return when {
            f == DataRequestState.CLOSED -> DataRequestEventType.REOPENED
            t == DataRequestState.CLOSED && c.substate == DataRequestSubstate.SUBJECT_NOT_FOUND -> DataRequestEventType.CLOSED_NOT_FOUND
            t == DataRequestState.CLOSED && c.substate == DataRequestSubstate.PROCESSED_AS_SERVICE_PROVIDER -> DataRequestEventType.CLOSED_SERVICE_PROVIDER
            t == DataRequestState.CLOSED -> DataRequestEventType.CLOSED
            c.notes != null -> DataRequestEventType.NOTE_ADDED
            else -> null
        }
    }

    fun toState(dataRequest: DataSubjectRequest) = StateMachineState(dataRequest.state, dataRequest.stateHistory)
    fun fromState(dataRequest: DataSubjectRequest, state: StateMachineState<DataRequestState>) {
        dataRequest.state = state.current
        dataRequest.stateHistory = state.history
    }

    fun transition(
        dataRequest: DataSubjectRequest,
        actor: User?,
        notes: String?,
        attachment: MultipartFile?,
        substate: DataRequestSubstate?,
        next: DataRequestState? = null
    ) {
        val state = toState(dataRequest)
        val context = DataRequestContext(dataRequest, actor, notes, attachment, substate)
        val machine = StateMachine(context, state, definition)

        // Setup listeners ....
        machine.transition(next)

        fromState(dataRequest, machine.state)
    }
}

data class DataRequestContext(
    val dataRequest: DataSubjectRequest,
    val actor: User?,
    var notes: String?,
    val attachment: MultipartFile?,
    val substate: DataRequestSubstate?
) {
    override fun toString(): String {
        return "DataRequestContext(dataRequest=${dataRequest.id} notes=$notes, substate=$substate, actor=$actor)"
    }
}
