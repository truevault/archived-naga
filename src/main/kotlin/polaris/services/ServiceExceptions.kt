// ktlint-disable filename
package polaris.services

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import polaris.util.PolarisException

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
class NylasSendError() :
    PolarisException("Unable to send message - Privacy Mailbox not connected. Go to Settings -> Mailbox Settings to re-connect.")

class MineEmailContentException(message: String) : PolarisException(message)
