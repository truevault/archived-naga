package polaris.services.debug

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import polaris.controllers.api.datainventory.UpdateDisclosureRequest
import polaris.controllers.util.LookupService
import polaris.models.DataRequestPublicId
import polaris.models.OrganizationPublicId
import polaris.models.entity.compliance.BusinessPurpose
import polaris.models.entity.compliance.CollectionGroupType
import polaris.models.entity.compliance.PICGroup
import polaris.models.entity.feature.Feature
import polaris.models.entity.feature.Features
import polaris.models.entity.feature.OrganizationFeature
import polaris.models.entity.organization.BusinessPurposeType
import polaris.models.entity.organization.CollectionContext
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.DefaultProcessingActivity
import polaris.models.entity.organization.LawfulBasis
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.MailboxType
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.organization.OrgComplianceVersion
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.OrganizationDataSource
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.organization.OrganizationRegulation
import polaris.models.entity.organization.OrganizationVendorStatus
import polaris.models.entity.organization.ProcessingInstruction
import polaris.models.entity.privacycenter.CustomUrlStatus
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.privacycenter.PrivacyCenterPolicySection
import polaris.models.entity.request.DataRequestEventType
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubmissionSourceEnum
import polaris.models.entity.request.DataRequestSubstate
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.request.DataSubjectRequestEvent
import polaris.models.entity.user.GlobalRole
import polaris.models.entity.user.User
import polaris.models.entity.user.UserRole
import polaris.models.entity.user.UserStatus
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.models.entity.vendor.ServiceProviderRecommendation
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER
import polaris.models.entity.vendor.VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY
import polaris.models.entity.vendor.Vendor
import polaris.models.entity.vendor.VendorClassification
import polaris.models.entity.vendor.VendorContract
import polaris.models.entity.vendor.VendorContractReviewed
import polaris.repositories.BusinessPurposeRepository
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.DataRequestEventRepository
import polaris.repositories.DataRequestRepository
import polaris.repositories.DefaultProcessingActivityRepo
import polaris.repositories.FeatureRepository
import polaris.repositories.OrganizationBusinessPurposeRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationDataSourceRepository
import polaris.repositories.OrganizationFeatureRepository
import polaris.repositories.OrganizationMailboxRepository
import polaris.repositories.OrganizationRegulationRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationUserRepository
import polaris.repositories.PICGroupRepo
import polaris.repositories.PersonalInformationCategoryRepository
import polaris.repositories.PrivacyCenterRepository
import polaris.repositories.RegulationRepository
import polaris.repositories.UserRepository
import polaris.repositories.VendorClassificationRepository
import polaris.repositories.VendorRepository
import polaris.services.primary.PrivacyEmailService
import polaris.services.primary.ProcessingInstructionsService
import polaris.services.primary.UserService
import polaris.services.support.DataMapService
import polaris.util.IdGenerator
import polaris.util.StringUtil
import java.time.ZonedDateTime
import java.util.UUID
import kotlin.random.Random

const val DEFAULT_USERNAME = "admin"
const val DEFAULT_PASSWORD = "admin"
const val DEFAULT_DOMAIN = "qa.truevault.com"

@Service
@Profile("dev")
class SeedService(
    private val businessPurposeRepository: BusinessPurposeRepository,
    private val collectionGroupRepository: CollectionGroupRepository,
    private val dataRequestEventRepository: DataRequestEventRepository,
    private val dataRequestRepository: DataRequestRepository,
    private val defProcessingActivityRepo: DefaultProcessingActivityRepo,
    private val featureRepository: FeatureRepository,
    private val organizationBusinessPurposeRepository: OrganizationBusinessPurposeRepository,
    private val organizationDataRecipientRepository: OrganizationDataRecipientRepository,
    private val organizationDataSourceRepository: OrganizationDataSourceRepository,
    private val organizationFeatureRepository: OrganizationFeatureRepository,
    private val organizationMailboxRepository: OrganizationMailboxRepository,
    private val organizationRegulationRepository: OrganizationRegulationRepository,
    private val organizationRepository: OrganizationRepository,
    private val organizationUserRepository: OrganizationUserRepository,
    private val personalInformationCategoryRepository: PersonalInformationCategoryRepository,
    private val picGroupRepo: PICGroupRepo,
    private val privacyCenterRepository: PrivacyCenterRepository,
    private val regulationRepository: RegulationRepository,
    private val userRepository: UserRepository,
    private val vendorClassificationRepository: VendorClassificationRepository,
    private val vendorRepository: VendorRepository,

    private val dataMapService: DataMapService,
    private val lookupService: LookupService,
    private val privacyEmailService: PrivacyEmailService,
    private val processingInstructionsService: ProcessingInstructionsService,
    private val userService: UserService
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(SeedService::class.java)
    }

    @Transactional
    fun seed() {

        purgeNonUsefulMigrations()

        val globals = loadGlobals(true)

        val organizations = listOf(
            seedFlannelLabs(globals),
            seedSomaStore(globals),
            seedLegacyCustomerTransitionOrganization("Legacy TX", "legacy"),
            seedGetCompliantOrganization("Sandbox1", "sandbox1"),
            seedGetCompliantOrganization("Sandbox2", "sandbox2"),
            seedGetCompliantOrganization("Sandbox3", "sandbox3"),
            seedGetCompliantOrganization("Sandbox4", "sandbox4"),
            seedGetCompliantOrganization("Sandbox5", "sandbox5"),
            seedGetCompliantOrganization("Demo", "demo")
        )

        seedTrueVault(globals, organizations)
    }

    @Transactional
    fun seedOrg(
        name: String,
        username: String,
        domain: String,
        features: List<String>,
        trainingComplete: Boolean = false,
        includePrivacyCenter: Boolean = true,
        uuid: UUID? = null
    ): Organization {
        val org = seedOrganization(
            name = name,
            publicId = name.toUpperCase(),
            uuid = uuid,
            trainingComplete = trainingComplete
        )

        seedUser(
            organization = org,
            username = username,
            domain = domain,
            firstName = username.capitalize(),
            lastName = name,
            password = DEFAULT_PASSWORD
        )

        for (feature in features) {
            seedOrganizationFeature(org, feature, true)
        }

        if (includePrivacyCenter) {
            seedPrivacyCenter(
                organization = org,
                publicId = name.toUpperCase(),
                name = "Privacy Center",
                defaultSubdomain = StringUtil.slugify(org.name),
                faviconUrl = "",
                customUrl = null
            )
        }

        seedOrganizationPrivacyEmails(org)

        return org
    }

    @Transactional
    fun seedDataMap(orgId: OrganizationPublicId): Organization {
        val org = lookupService.orgOrThrow(orgId)

        log.info("Seeding data map for organization ${org.name}")

        val globals = loadGlobals(false)
        val dataRecipients = seedDefaultDataRecipients(org, globals)
        val collectionGroups = seedDefaultCollectionGroups(org)
        seedDefaultDisclosedPic(org, collectionGroups, dataRecipients, globals)

        return org
    }

    /** Global  Systems **/
    data class GlobalSystems(
        val defaultProcessingActivities: List<DefaultProcessingActivity>,
        val businessPurposes: List<BusinessPurpose>,
        val features: List<Feature>,
        val picGroups: List<PICGroup>,
        val vendors: List<Vendor>,
        val vendorClassifications: List<VendorClassification>
    )

    private fun loadGlobals(seed: Boolean): GlobalSystems {

        if (seed) {
            seedDefaultProcessingActivities()
            seedFeatures()
            seedVendorClassifications()
            seedVendorDatabase()
        }

        return GlobalSystems(
            defaultProcessingActivities = defProcessingActivityRepo.findAll(),
            businessPurposes = businessPurposeRepository.findAll(),
            features = featureRepository.findAll(),
            picGroups = picGroupRepo.findAll(),
            vendorClassifications = vendorClassificationRepository.findAll(),
            vendors = vendorRepository.findAll()
        )
    }

    private fun purgeNonUsefulMigrations() {
        // Dealing with the non-useful migrations for (Other Individuals and Consumer Directly)
        vendorRepository.deleteAll()
    }

    private fun seedFeatures(): List<Feature> {
        return listOf(
            seedFeature(
                name = Features.SETUP_PHASE,
                description = "Initial 'Setup' phase when an org first signs up",
                enabledByDefault = false
            ),
            seedFeature(
                name = Features.GET_COMPLIANT_PHASE,
                description = "Post-setup 'Get Compliant' phase when an org is going through onboarding",
                enabledByDefault = false
            ),
            seedFeature(
                name = Features.GDPR,
                description = "Does the organization subscribe to the GDPR feature set",
                enabledByDefault = false
            ),
            seedFeature(
                name = Features.LEGACY_ORG,
                description = Features.LEGACY_ORG,
                enabledByDefault = false
            ),
            seedFeature(
                name = Features.LEGACY_TRANSITION,
                description = "Transition for legacy customers to be feature gated",
                enabledByDefault = false
            ),
            seedFeature(
                name = Features.MULTISTATE,
                description = "Does the organization subscribe to the US Multistate feature set",
                enabledByDefault = false
            )
        )
    }

    private fun seedVendorClassifications(): List<VendorClassification> {
        return listOf(
            seedVendorClassification(
                name = "Service Provider",
                tooltipText = "Service Provider",
                displayOrder = 1,
                slug = VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER
            ),
            seedVendorClassification(name = "Contractor", tooltipText = "Contractor", displayOrder = 2, slug = "contractor"),
            seedVendorClassification(name = "Third Party", tooltipText = "Third Party", displayOrder = 3, slug = VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY),
            seedVendorClassification(
                name = "No Data Sharing",
                tooltipText = "No information is shared with this vendor",
                displayOrder = 4,
                slug = "no-data-sharing"
            )
        )
    }

    private fun seedDefaultProcessingActivities(): List<DefaultProcessingActivity> {
        val pa = mutableListOf<DefaultProcessingActivity>()

        pa.add(DefaultProcessingActivity(name = "Processing Payments", gdprOptionalLawfulBases = listOf(LawfulBasis.COMPLY_WITH_LEGAL_OBLIGATIONS)))
        pa.add(DefaultProcessingActivity(name = "Fulfilling Customer Orders", materialIconKey = "Rule", gdprMandatoryLawfulBases = listOf(LawfulBasis.CONSENT)))
        pa.add(DefaultProcessingActivity(name = "Tracking Purchases & Customer Data", gdprMandatoryLawfulBases = listOf(LawfulBasis.CONSENT)))
        pa.add(DefaultProcessingActivity(name = "Improving our Products & Services"))
        pa.add(DefaultProcessingActivity(name = "Providing Customer Support"))
        pa.add(DefaultProcessingActivity(name = "Providing Cybersecurity"))
        pa.add(DefaultProcessingActivity(name = "Operating our Website or Mobile Apps"))
        pa.add(DefaultProcessingActivity(name = "Sending Promotional Communications"))
        pa.add(DefaultProcessingActivity(name = "Delivering Targeted Advice"))
        pa.add(DefaultProcessingActivity(name = "Creating Customer Profiles"))
        pa.add(DefaultProcessingActivity(name = "Conducting Surveys"))
        pa.add(DefaultProcessingActivity(name = "Internal Business Operations"))
        pa.add(DefaultProcessingActivity(name = "Data Storage", gdprOptionalLawfulBases = listOf(LawfulBasis.LEGITIMATE_INTERESTS)))
        pa.add(DefaultProcessingActivity(name = "Organizing & Managing Data"))

        defProcessingActivityRepo.saveAll(pa)

        return pa
    }

    private fun seedVendorDatabase(): List<Vendor> {
        val seedData: List<VendorSeed> = listOf(
            VendorSeed(
                "Google Ads",
                "Ad Network",
                "https://ads.google.com/home/",
                "logos/services/624922cc-5891-492b-a202-866934c7073b.jpg",

                ServiceProviderRecommendation.NONE,
                "https://help.truevault.com/article/135-vendor-details-google-ads",
                "",
                "",
                "Select Google Ads if your businesses uses Ad Manager/Ad Manager 360, AdMob, AdSense, ...",
                "google-ads"
            ),
            VendorSeed(
                "Facebook Ads",
                "Ad Network",
                "https://www.facebook.com/business/ads",
                "logos/services/96eb7eb8-5123-499d-ae12-7168e5033b3f.svg",
                ServiceProviderRecommendation.NONE,
                "https://help.truevault.com/article/134-vendor-details-facebook-ads",
                "",

                "https://support.google.com/marketingplatform/gethelp",
                "Select Facebook Ads if your business uses Facebook Pixel, Conversion API, App Events via Facebook SDK, App Events API, Offline Conversions, Audience Network SDK, and/or Lookalike Audiences",
                "facebook-ads",
                aliases = listOf("Facebook Pixel"),
                deletionInstructions = """<p>If you do not use the Custom Audience service, select "Inaccessible or not stored" because you do not have access to any data for deletion. Otherwise, to remove the data of a member of your Custom&nbsp;Audience:</p>
                                            <ol><li dir="ltr">Go to “Audiences.”</li><li dir="ltr">Select your customer list “Custom Audience,” then select “Update.”</li><li dir="ltr">Select “Remove Customers.”</li><li dir="ltr">Upload or paste your updated file or list that includes the information you want to replace, add, or remove from your Custom Audience. (For a list with Customer Value, ensure you include customer value specifically and identify your value column.)</li><li dir="ltr">Select “Next,” then follow the prompts to map your identifiers.</li><li dir="ltr">Select “Import &amp; Upload.” A confirmation update will display once your list has been uploaded.</li><li dir="ltr">Select from the suggested next steps or select “Done.”</li></ol>+
                                            <p><a href="https://help.truevault.com/article/134-vendor-details-facebook-ads" target="_blank">Facebook's instructions</a></p>""",
                generalGuidance = """<p>Based on our research, the personal information processed by Facebook Ads includes the categories highlighted below. You can read Facebook Ads’ documentation on data processing <a href="https://www.facebook.com/legal/terms/businesstools" target="_blank">here</a>.</p>""",
                specialCircumstancesGuidance = "<p>If your business uses the Custom Audiences feature, you should also select the specific information you directly upload, which may include: Name, email address, postal address, telephone number, and any other personal identifiers your business shares with Facebook.</p>",
                recommendedPersonalInformationCategories = personalInformationCategoryRepository.findByNameIn(listOf("Unique personal identifier", "Purchases", "Interactions with websites, apps or ads"))?.map { it.id.toString() } ?: listOf()
            ),
            VendorSeed(
                "Google Analytics",
                "Data Analytics Provider",
                "https://marketingplatform.google.com/about/analytics/",
                "logos/services/d4b339f6-39d6-47ab-9ae0-632ee91795b1.png",
                ServiceProviderRecommendation.NONE,
                "https://help.truevault.com/article/135-vendor-details-google-ads",
                "",
                "",
                "",
                "google-analytics",
                dpaUrl = "https://support.google.com/analytics/answer/3379636?hl=en",
                ccpaRequestToKnowLink = "https://support.google.com/analytics/answer/9019185?hl=en#access-portability&zippy=%2Cin-this-article",
                ccpaRequestToDeleteLink = "https://developers.google.com/analytics/devguides/config/userdeletion/v3/",
                recommendedPersonalInformationCategories = personalInformationCategoryRepository.findByNameIn(listOf("IP address", "Device identifier", "Unique personal identifier", "Interactions with websites, apps or ads"))?.map { it.id.toString() } ?: listOf()
            ),
            VendorSeed(
                "Slack",
                "Collaboration & Productivity Software",
                "https://www.slack.com",
                "logos/services/11208760-d42b-48f2-bfc5-042afac17139.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://slack.com/intl/en-ph/trust/compliance/ccpa-faq#h_a8a60c72-258a-40d6-a8e2-5d2492d620bd",
                "privacy@slack.com"
            ),
            VendorSeed(
                "Stripe",
                "Payment Processor",
                "https://www.stripe.com",
                "logos/services/87d547c3-4da8-493e-97d0-a89443b72ec0.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://support.stripe.com/questions/accept-and-download-your-data-processing-agreement-(dpa)-with-stripe",
                "support@stripe.com"
            ),
            VendorSeed(
                "UPS",
                "Shipping Service",
                "https://www.ups.com/us/en/supplychain/Home.page",
                "logos/services/9d7fe055-50aa-4ad4-a5d1-dfeec08dcabe.svg",
                ServiceProviderRecommendation.THIRD_PARTY,
                "https://www.ups.com/us/en/supplychain/Home.page",
                "support@ups.com"
            ),
            VendorSeed(
                "FedEx",
                "Shipping Service",
                "https://www.fedex.com/",
                "logos/services/13b5ba28-c6ae-49a6-b6c9-24fc1ebad71d.svg",
                ServiceProviderRecommendation.THIRD_PARTY,
                "https://www.fedex.com/",
                "support@fedex.com"
            ),

            VendorSeed(
                "Mailchimp",
                "Sales & Marketing Tool",
                "https://mailchimp.com/marketing-platform/",
                "logos/services/624da4e8-faa0-4f99-a111-b86037460f4c.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://mailchimp.com/marketing-platform/",
                "support@fedex.com"
            ),
            VendorSeed(
                "Amazon Web Services",
                "IT Infrastructure Software",
                "https://aws.amazon.com",
                "logos/services/6ae067c7-0288-483a-84e4-016c1aa61b31.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://aws.amazon.com/service-terms/#:~:text=EC.-,1.14.2,-These",
                "",
                "https://aws.amazon.com/contact-us/?nc1=f_m",
                "",
                "amazon-aws",
                listOf("S3", "RDS", "EC2", "Amazon"),
                gdprProcessorRecommendation = GDPRProcessorRecommendation.Processor,
                gdprHasSccRecommendation = true,
                gdprInternationalDataTransferRulesApply = true
            ),
            VendorSeed(
                "Amazon Marketplace",
                "IT Infrastructure Software",
                "https://aws.amazon.com",
                "logos/services/6ae067c7-0288-483a-84e4-016c1aa61b31.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://aws.amazon.com/service-terms/#:~:text=EC.-,1.14.2,-These",
                "",
                "https://aws.amazon.com/contact-us/?nc1=f_m",
                "",
                "amazon-marketplace"
            ),
            VendorSeed(
                "Affirm",
                "Payment Processor",
                "https://www.affirm.com",
                "logos/services/74e773ad-5558-4bae-b3d4-4b9ec88a5dcb",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.affirm.com",
                "",
                "https://www.affirm.com",
                "",
                "affirm"
            ),
            VendorSeed(
                "Facebook Pay",
                "Payment Processor",
                "https://pay.facebook.com/",
                "logos/services/503b46b7-f7da-4f66-befe-abefa5d8f598.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://pay.facebook.com/",
                "",
                "https://pay.facebook.com/",
                "",
                "facebook-pay"
            ),
            VendorSeed(
                "Apple Pay",
                "Payment Processor",
                "http://www.apple.com/apple-pay/",
                "logos/services/06ef2f30-8907-4f67-a37d-70454ee654b1.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "http://www.apple.com/apple-pay/",
                "",
                "http://www.apple.com/apple-pay/",
                "",
                "apple-pay"
            ),
            VendorSeed(
                "Amazon Pay",
                "Payment Processor",
                "https://pay.amazon.com",
                "logos/services/6ae067c7-0288-483a-84e4-016c1aa61b31.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://aws.amazon.com/service-terms/#:~:text=EC.-,1.14.2,-These",
                "",
                "https://aws.amazon.com/contact-us/?nc1=f_m",
                "",
                "amazon-pay"
            ),
            VendorSeed(
                "Yotpo",
                "Commerce Software",
                "https://www.yotpo.com",
                "logos/services/0b12f725-136f-4d34-b2ba-5bfaf5fa7581.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.yotpo.com/data-processing-agreement-CCPA/",
                "info@yotpo.com"
            ),
            VendorSeed(
                "ZoomInfo",
                "Data Broker",
                "https://www.zoominfo.com",
                "logos/services/0b12f725-136f-4d34-b2ba-5bfaf5fa7581.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "",
                ""
            ),
            VendorSeed(
                "Docusign",
                "Contract Software",
                "https://www.docusign.com/products/electronic-signature",
                "logos/services/7bf15a61-a2eb-4930-9d62-5d5285a598ff.svg",
                ServiceProviderRecommendation.NONE,
                "",
                "privacy@docusign.com"
            ),
            VendorSeed(
                "Shopify",
                "Commerce Software",
                "https://www.shopify.com",
                "logos/services/da079afa-f9bc-47e8-b8f4-ef4321cc7b5f.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.shopify.com/legal/dpa",
                "support@shopify.com",
                aliases = listOf("Shopify POS", "Shopify Plus"),
                ccpaRequestToKnowLink = "https://help.shopify.com/en/manual/your-account/privacy/CCPA/ccpa-requests",
                ccpaRequestToDeleteLink = "https://help.shopify.com/en/manual/your-account/privacy/CCPA/ccpa-requests",
                deletionInstructions = """<ol><li dir="ltr">From your Shopify admin, go to “Customers.”</li><li dir="ltr">Search for the name of the customer.</li><li dir="ltr">Click “Erase personal data.”</li></ol>
<p dir="ltr">Your request is then sent to third party apps you have currently installed on your store. The third party app developers will independently action or contact you about this request.</p>
<p dir="ltr"><a href="https://help.shopify.com/en/manual/your-account/privacy/CCPA/ccpa-requests#complete-deletion-requests" target="_blank">Shopify’s instructions</a></p>""",
                generalGuidance = """<p>Based on our research, the personal information processed by Shopify includes the categories highlighted below. You can read Shopify’s documentation on data processing <a href="https://www.shopify.com/legal/privacy/customers" target="_blank">here</a>.</p>""",
                recommendedPersonalInformationCategories = personalInformationCategoryRepository.findByNameIn(listOf("IP address", "Name", "Email address", "Postal address", "Telephone number", "Device identifier", "Credit card or debit card number", "Unique personal identifier", "Purchases", "Interactions with websites, apps or ads"))?.map { it.id.toString() } ?: listOf(),
                key = "shopify",
                isPlatform = true
            ),
            VendorSeed(
                "Hubspot Service Hub",
                "Sales Software",
                "https://www.hubspot.com/products/service",
                "logos/services/19af0041-98f9-47fe-91f0-b17b0963ef4c.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://legal.hubspot.com/dpa"
            ),
            VendorSeed(
                "Google Workspace",
                "Office Software",
                "https://gsuite.google.com",
                "logos/services/5d614220-f322-42a0-a427-93c298ce028c.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://workspace.google.com/terms/dpa_terms.html",
                "",
                "https://support.google.com/policies/answer/9581826?hl=en"
            ),
            VendorSeed(
                "Typeform",
                "Content Management System",
                "http://www.typeform.com/",
                "logos/services/34b547c9-93ec-4349-af43-200c7221ce1b.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER
            ),
            VendorSeed(
                "Zendesk",
                "Customer Service Software",
                "https://www.zendesk.com/support",
                "logos/services/68fde8fb-d4b8-4703-bfa1-721841354634.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.zendesk.com/company/privacy-and-data-protection/#ccpa",
                "privacy@zendesk.com"
            ),
            VendorSeed(
                "Microsoft Ads",
                "Ad Network",
                "https://about.ads.microsoft.com",
                "logos/services/97b05be5-e6ba-4d78-9bb7-1dc3b70423d3.svg",
                ServiceProviderRecommendation.NONE
            ),
            VendorSeed(
                "Twilio",
                "Development Software",
                "https://www.twilio.com",
                "logos/services/447ff7fa-a209-4ced-96b8-01c74f38ac81.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.twilio.com/legal/data-protection-addendum#jurisdiction-specific-terms",
                "privacy@twilio.com"
            ),
            VendorSeed(
                "Zapier",
                "IT Infrastructure Software",
                "https://zapier.com/pricing/",
                "logos/services/846634b8-fabc-469d-82c4-9d47f1cebbc8.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://zapier.com/help/account/data-management/zapiers-data-processing-addendum",
                "contact@zapier.com"
            ),
            VendorSeed(
                "Segment",
                "Marketing Software",
                "https://segment.com",
                "logos/services/79dd3b6c-3061-4bca-8280-f9df94878dd0.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://segment.com/legal/data-protection-addendum/",
                "privacy@segment.com"
            ),
            VendorSeed(
                "BambooHR",
                "HR Software",
                "https://www.bamboohr.com/tour.php",
                "logos/services/3e01ffcf-cda9-4062-b873-9ae5465ac4f8.svg",
                ServiceProviderRecommendation.NONE,
                "",
                "privacy@bamboohr.com"
            ),
            VendorSeed(
                "Intercom",
                "Customer Service Software",
                "https://www.intercom.com/pricing",
                "logos/services/218d5ecb-a9ee-48d1-a56d-e53e22f1dd82.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.intercom.com/legal/privacy#:~:text=This%20Section%2012%20does,provider%22%20under%20the%20CCPA.",
                "compliance@intercom.com"
            ),
            VendorSeed(
                "Calendly",
                "Office Software",
                "https://calendly.com/pages/features",
                "logos/services/7dd2e043-f6fb-4b28-9558-cdf94a412324.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://calendly.com/pages/dpa",
                "support@calendly.com"
            ),
            VendorSeed(
                "Hotjar",
                "Marketing Software",
                "https://www.hotjar.com/",
                "logos/services/657b9330-b030-46ee-8922-9961fe3b5d83.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.hotjar.com/legal/compliance/ccpa/#:~:text=Hotjar%20is%20the%20%E2%80%98service%20provider%E2%80%99",
                "legal@hotjar.com"
            ),
            VendorSeed(
                "ShipStation",
                "Supply Chain & Logistics Software",
                "https://www.shipstation.com",
                "logos/services/38b70bee-e1ac-46a0-be76-e7a3224cc985.svg",
                ServiceProviderRecommendation.THIRD_PARTY
            ),
            VendorSeed(
                "PayPal - Pay with PayPal, Venmo, or Pay Later offers",
                "Payment Processor",
                "https://www.paypal.com",
                "logos/services/458c85e9-7c2f-439b-98d7-b838fc5a48fa.svg",
                ServiceProviderRecommendation.THIRD_PARTY,
                "https://help.truevault.com/article/133-paypal",
                "",
                "https://www.paypal.com/ph/smarthelp/contact-us?email=privacy",
                "",
                "paypal"
            ),
            VendorSeed(
                "PayPal - Credit Card Processing",
                "Payment Processor",
                "https://www.paypal.com",
                "logos/services/458c85e9-7c2f-439b-98d7-b838fc5a48fa.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://help.truevault.com/article/133-paypal",
                "",
                "https://www.paypal.com/ph/smarthelp/contact-us?email=privacy",
                "",
                "paypal-credit-card"
            ),
            VendorSeed(
                "Klarna",
                "Payment Processor",
                "https://www.klarna.com/us",
                "logos/services/f980a5eb-aca7-4633-bdc4-95fe6d9fa3d0.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.klarna.com/us",
                "privacy@klarna.com",
                key = "klarna",
                gdprProcessorRecommendation = GDPRProcessorRecommendation.Processor,
                gdprInternationalDataTransferRulesApply = true
            ),
            VendorSeed(
                "Shop Pay",
                "Payment Processor",
                "https://shop.app/",
                "logos/services/a4f7d627-2834-46ad-bf6b-d64f7b375e6e.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://shop.app/",
                "privacy@shop.app",
                key = "shop-pay",
                gdprProcessorRecommendation = GDPRProcessorRecommendation.Processor,
                gdprInternationalDataTransferRulesApply = true
            ),
            VendorSeed(
                "Klaviyo",
                "Marketing Software",
                "https://www.klaviyo.com/",
                "logos/services/9b51afe4-6365-49fb-8dab-e47f8b2627b2.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.klaviyo.com/privacy/dpa",
                "privacy@klaviyo.com",
                gdprProcessorRecommendation = GDPRProcessorRecommendation.Processor,
                gdprInternationalDataTransferRulesApply = true
            ),
            VendorSeed(
                "Firebase",
                "Development Software",
                "https://firebase.google.com/",
                "logos/services/093fa8f1-1ff9-4601-a4b5-2848771c5ae5.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://cloud.google.com/terms/data-processing-terms",
                "https://cloud.google.com/contact"
            ),
            VendorSeed(
                "Webflow",
                "Design Software",
                "https://webflow.com/",
                "logos/services/ab1fb971-8f30-4c30-8508-4ced6322b8ae.png",
                ServiceProviderRecommendation.NONE,
                "",
                "contact@webflow.com",
                gdprProcessorRecommendation = GDPRProcessorRecommendation.Controller
            ),
            VendorSeed(
                "Arm Treasure Data",
                "Marketing Software",
                "https://www.treasuredata.com/solutions/marketing/",
                "logos/services/7f1d8e70-6bcb-4c12-8dbe-600983d16a65.svg"
            ),
            VendorSeed(
                "Zeta Programmatic",
                "Digital Advertising Software",
                "https://zetaglobal.com/programmatic-zeta-hub/",
                "logos/services/01b56796-2995-4625-bd8b-a7b0a2bc88ff.png"
            ),
            VendorSeed(
                "Datadog",
                "IT Infrastructure Software",
                "https://www.datadoghq.com",
                "logos/services/64537b1f-c4ec-4e93-9d98-9683bb2a0469.svg"
            ),
            VendorSeed(
                "Pinterest Ads",
                "Ad Network",
                "https://ads.pinterest.com/",
                "logos/services/7e7a433e-8d80-4a99-8116-05260b617153.svg",
                ServiceProviderRecommendation.NONE,
                "",
                "privacy-support@pinterest.com"
            ),
            VendorSeed(
                "Aircall",
                "Customer Service Software",
                "https://aircall.io/business-phone-system/",
                "logos/services/15f6dd2b-ceb1-4db0-89de-604d7efcdacb.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://aircall.io/privacy/",
                "privacy@aircall.io"
            ),
            VendorSeed(
                "PR Hunters",
                "Public Relations Software",
                "https://www.prhunters.com/",
                "logos/services/eb2750bb-9240-434e-b523-dac2dc321870.png"
            ),
            VendorSeed(
                "Authorize.Net",
                "Payment Processor",
                "https://www.authorize.net",
                "logos/services/3394e319-f6d3-4b72-8e6f-4a3b87e81163.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.authorize.net/about-us/dpa.html",
                "privacy@visa.com"
            ),
            VendorSeed(
                "Appfigures",
                "Marketing Software",
                "http://appfigures.com/",
                "logos/services/b3ee8b45-1b6f-4f38-8af9-c4351238d9cb.png"
            ),
            VendorSeed(
                "Square E-Commerce",
                "Commerce Software",
                "https://squareup.com/",
                "logos/services/f1f90959-c433-4d46-b763-69f0ea579e09.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://squareup.com/help/us/en/article/6753-ccpa-faqs",
                "privacy@squareup.com"
            ),
            VendorSeed(
                "Gorgias",
                "Customer Service Software",
                "https://www.gorgias.com/",
                "logos/services/dac5bb89-53a6-49e5-a3d3-e412455e5e4e.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.gorgias.com/privacy/ccpa",
                "support@gorgias.io"
            ),
            VendorSeed(
                "Officevibe",
                "Talent Managment Software",
                "https://www.officevibe.com/employee-engagement-solution",
                "logos/services/68789fde-58f7-4e7d-b9c4-d6689e9f0c14.svg"
            ),
            VendorSeed(
                "Google Cloud",
                "Development Software",
                "https://cloud.google.com",
                "logos/services/c27b8069-7a19-4cd7-8718-ea9d1e3fbcb3.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://cloud.google.com/terms/data-processing-terms",
                "",
                "https://cloud.google.com/contact"
            ),
            VendorSeed(
                "NetSuite",
                "Enterprise Resource Planning Software",
                "http://www.netsuite.com/portal/products/erp.shtml",
                "logos/services/4197028c-fc59-444f-826a-2ee303b5a544.svg",
                ServiceProviderRecommendation.NONE,
                "",
                "",
                "https://www.oracle.com/legal/data-privacy-inquiry-form.html"
            ),
            VendorSeed(
                "Dropbox",
                "Content Management System",
                "https://www.dropbox.com",
                "logos/services/198a9e9b-a9df-4a8a-a86f-fb6cfd5a6583.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.dropbox.com/privacy#privacy-policy-2020:~:text=Dropbox%20as%20controller%20or%20processor.%20If,as%20a%20processor%20of%20your%20data.",
                "privacy@dropbox.com"
            ),
            VendorSeed(
                "Pega Platform",
                "IT Management Software",
                "http://www.pega.com/products/business-process-management",
                "logos/services/caeb0345-94ff-4f5d-b041-40b83800a30b.svg"
            ),
            VendorSeed(
                "USPS",
                "Business Service",
                "https://www.usps.com/",
                "logos/services/ca8f72d8-2a56-4027-a4a5-9259cdfcc6d1.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://about.usps.com/who/legal/privacy-policy/full-privacy-policy.htm",
                "privacy@usps.gov"
            ),
            VendorSeed(
                "Outpost",
                "Collaboration & Productivity Software",
                "https://www.teamoutpost.com/features",
                "logos/services/f193179a-f781-4d4f-930d-a0030dec5b66.png"
            ),
            VendorSeed(
                "IBM Cloud Foundry",
                "Development Software",
                "https://www.ibm.com/cloud/cloud-foundry",
                "logos/services/aeddf850-567c-4096-8fe3-50ea7ef6624f.png"
            ),
            VendorSeed(
                "Workday HCM",
                "HR Software",
                "https://www.workday.com/en-us/applications/human-capital-management.html",
                "logos/services/f5d01df4-b069-4788-a256-0179e49b8967.svg"
            ),
            VendorSeed(
                "TimeCamp",
                "HR Software",
                "https://www.timecamp.com/en/time-tracking/",
                "logos/services/cf13a793-f284-4dba-8caa-cc73e22bc513.svg"
            ),
            VendorSeed(
                "Gmail",
                "Office Software",
                "https://gsuite.google.com/products/gmail/",
                "logos/services/ae1c6b56-7453-4d7a-8576-de5e334e0816.png",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://workspace.google.com/terms/dpa_terms.html",
                "",
                "https://support.google.com/policies/answer/9581826?hl=en"
            ),
            VendorSeed(
                "Google Tag Manager",
                "Marketing Software",
                "https://marketingplatform.google.com/about/tag-manager/",
                "logos/services/5eb33898-b8af-419a-a6fe-235bc5921c1b.svg",
                ServiceProviderRecommendation.NONE,
                "https://help.truevault.com/article/135-vendor-details-google-ads",
                "",
                "https://support.google.com/policies/answer/9581826?p=privpol_privts&hl=en&visit_id=637517747425225000-2960461095&rd=1"
            ),
            VendorSeed(
                "HubSpot Marketing Hub",
                "Marketing Software",
                "https://www.hubspot.com/products/marketing",
                "logos/services/6256daed-d706-4a99-aeb5-112c8b9f210f.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://legal.hubspot.com/dpa"
            ),
            VendorSeed(
                "Salesforce",
                "Sales Software",
                "https://www.salesforce.com",
                "logos/services/c3f56df2-328e-42c4-aa54-d8a6508620b0.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.salesforce.com/content/dam/web/en_us/www/documents/legal/Agreements/data-processing-addendum.pdf",
                "privacy@salesforce.com"
            ),
            VendorSeed(
                "Signal",
                "Digital Advertising Software",
                "http://www.signal.co/technology/",
                "logos/services/32105bfa-2352-4f46-8346-96f07e187299.svg",
                ServiceProviderRecommendation.NONE
            ),
            VendorSeed(
                "TrueVault Polaris",
                "Governance, Risk & Compliance Software",
                "https://www.truevault.com/",
                "logos/services/281f4879-ae0d-4cc7-8bcd-79a9faa6d517.svg",
                ServiceProviderRecommendation.SERVICE_PROVIDER,
                "https://www.truevault.com/terms-of-service",
                "privacy@truevault.com",
                "",
                "",
                "truevault-polaris"
            ),
            VendorSeed(
                name = "Consumer",
                category = "Direct Collection",
                logoFilename = "logos/services/f41944c8-8c73-4a51-9f4e-3afc11098842.jpeg",
                disclosureSourceCategory = "You (the consumer)",
                url = "",
                key = "source-consumer-directly",
                isDataRecipient = false
            ),
            VendorSeed(
                name = "Other Individuals",
                category = "E.g., Referrals",
                logoFilename = "logos/services/3224a594-e34d-444f-95c2-4fbf9eeeaa10.jpeg",
                disclosureSourceCategory = "Other consumers (e.g., referrals)",
                url = "https://www.truevault.com",
                key = "source-other-individuals",
                isDataRecipient = false
            )
        )

        return seedData.map { seedVendor(it) }
    }

    /** Organizations **/

    private fun seedTrueVault(globals: GlobalSystems, additionalOrgs: List<Organization>): Organization {
        val truevault = seedOrganization(
            "TrueVault",
            publicId = "UXWSDP2MQ",
            uuid = UUID.fromString("26f9d689-6f17-4264-baa1-d5a3fadb8c3f"),
            faviconUrl = "https://www.truevault.com/hubfs/favicon.png",
            headerImageUrl = "https://via.placeholder.com/150"
        )

        val tvAdmin = seedUser(
            truevault,
            DEFAULT_USERNAME,
            DEFAULT_DOMAIN,
            "Admin",
            "User",
            globalRole = GlobalRole.ADMIN,
            additionalOrganizations = additionalOrgs
        )

        seedOrganizationFeature(truevault, Features.GDPR, true)
        seedOrganizationFeature(truevault, Features.LEGACY_ORG, false)

        seedUser(truevault, "old", DEFAULT_DOMAIN, "Old", "User", status = UserStatus.INACTIVE)
        seedUser(truevault, "new", DEFAULT_DOMAIN, "New", "User", status = UserStatus.INVITED)

        val dataRecipients = seedDefaultDataRecipients(truevault, globals)

        val collectionGroups = seedDefaultCollectionGroups(truevault)

        seedDefaultDisclosedPic(truevault, collectionGroups, dataRecipients, globals)

        seedDefaultProcessingInstructions(truevault, collectionGroups)

        seedBusinessPurpose(truevault, globals.businessPurposes.get(0), BusinessPurposeType.CONSUMER)
        seedBusinessPurpose(truevault, globals.businessPurposes.get(1), BusinessPurposeType.CONSUMER)
        seedBusinessPurpose(truevault, globals.businessPurposes.get(2), BusinessPurposeType.CONSUMER)
        seedBusinessPurpose(truevault, globals.businessPurposes.get(0), BusinessPurposeType.EMPLOYMENT)
        seedBusinessPurpose(truevault, globals.businessPurposes.get(1), BusinessPurposeType.EMPLOYMENT)

        // Seed Sources
        seedDataSource(truevault, globals.vendors.first { it.name == "Stripe" })
        seedDataSource(truevault, globals.vendors.first { it.name == "Datadog" })
        seedDataSource(truevault, globals.vendors.first { it.name == "Google Ads" })

        seedOrganizationPrivacyEmails(truevault)

        var seededRequests = 0
        while (seededRequests <= 30) {
            seedDataRequest(
                publicId = "W7VH6H6A$seededRequests",
                createdBy = tvAdmin,
                organization = truevault,
                subjectEmail = "email$seededRequests@example.com",
                collectionGroup = collectionGroups.getValue("consumer"),
                dataRequestType = DataRequestType.values()[Random.Default.nextInt(DataRequestType.values().size)],
                state = DataRequestState.VERIFY_CONSUMER,
                requestDate = ZonedDateTime.now().plusDays(Random.Default.nextInt(31).toLong())
            )

            seededRequests++
        }

        seedDataRequest(
            publicId = "W7VH6H6AI",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email1@example.com",
            collectionGroup = collectionGroups.getValue("consumer"),
            dataRequestType = DataRequestType.CCPA_OPT_OUT,
            state = DataRequestState.REMOVE_CONSUMER,
            requestDate = ZonedDateTime.now().plusDays(9),
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.MESSAGE_SENT,
                    message = "Dear user, please tell us whether you have any data in our system. Thanks!"
                )
            ),
            organizationCustomInput = "Jenny - 5558675309"
        )

        seedDataRequest(
            publicId = "SUVCOYZNMS",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email6@example.com",
            collectionGroup = collectionGroups.getValue("consumer"),
            dataRequestType = DataRequestType.GDPR_OBJECTION,
            state = DataRequestState.PROCESSING,
            requestDate = ZonedDateTime.now().plusDays(9),
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.MESSAGE_SENT,
                    message = "Dear user, please tell us whether you have any data in our system. Thanks!"
                )
            ),
            organizationCustomInput = "Jenny - 5558675309"
        )

        seedDataRequest(
            publicId = "RT96A6NV1",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email2@example.com",
            collectionGroup = collectionGroups.getValue("consumer"),
            dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW,
            state = DataRequestState.VERIFY_CONSUMER,
            requestDate = ZonedDateTime.now().minusDays(30),
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.MESSAGE_SENT,
                    message = "Dear user, please tell us whether you have any data in our system. Thanks!"
                )
            ),
            submissionSource = DataRequestSubmissionSourceEnum.POLARIS,
            organizationCustomInput = "Morgan Freeman as God - 7167762323"
        )

        seedDataRequest(
            publicId = "RT96A6NV2",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email3@example.com",
            collectionGroup = collectionGroups.getValue("consumer"),
            dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW,
            state = DataRequestState.VERIFY_CONSUMER,
            requestDate = ZonedDateTime.now().minusDays(30),
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.MESSAGE_SENT,
                    message = "Dear user, please tell us whether you have any data in our system. Thanks!"
                )
            ),
            submissionSource = DataRequestSubmissionSourceEnum.PRIVACY_CENTER,
            selfDeclarationProvided = true
        )

        seedDataRequest(
            publicId = "NQ1U5Q6AE",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email3@example.com",
            collectionGroup = collectionGroups.getValue("consumer"),
            dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW,
            state = DataRequestState.VERIFY_CONSUMER,
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.MESSAGE_SENT,
                    message = "Dear user, please tell us whether you have any data in our system. Thanks!"
                )
            ),
            context = CollectionContext.EMPLOYEE,
            submissionSource = DataRequestSubmissionSourceEnum.PRIVACY_CENTER
        )

        seedDataRequest(
            publicId = "0MYXK1VMF",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email4@example.com",
            collectionGroup = collectionGroups.getValue("consumer"),
            dataRequestType = DataRequestType.CCPA_RIGHT_TO_DELETE,
            state = DataRequestState.VERIFY_CONSUMER,
            requestDate = ZonedDateTime.now().minusDays(42),
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.MESSAGE_SENT,
                    message = "Dear user, please tell us whether you have any data in our system. Thanks!"
                )
            )
        )

        seedDataRequest(
            publicId = "LF5G6LK0I",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email5@example.com",
            collectionGroup = collectionGroups.getValue("prospect"),
            dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW,
            state = DataRequestState.VERIFY_CONSUMER,
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.MESSAGE_SENT,
                    message = "Dear user, please tell us whether you have any data in our system. Thanks!"
                )
            )
        )

        seedDataRequest(
            publicId = "LF5G6LK1I",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email5@example.com",
            collectionGroup = collectionGroups.getValue("prospect"),
            dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW,
            state = DataRequestState.CLOSED,
            requestDate = ZonedDateTime.now().minusDays(10),
            closedAt = ZonedDateTime.now().minusDays(7),
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.MESSAGE_SENT,
                    message = "Dear user, please tell us whether you have any data in our system. Thanks!"
                ),
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.CLOSED_SERVICE_PROVIDER,
                    createdAt = ZonedDateTime.now().minusDays(7)
                )
            )
        )

        seedDataRequest(
            publicId = "Z3M4MNAB8",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email6@example.com",
            collectionGroup = collectionGroups.getValue("newsletter-subscriber"),
            dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW,
            state = DataRequestState.CLOSED,
            substate = DataRequestSubstate.SUBJECT_NOT_FOUND,
            requestDate = ZonedDateTime.now().minusDays(10),
            closedAt = ZonedDateTime.now().minusDays(7),
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.CLOSED_NOT_FOUND,
                    message = "No response",
                    createdAt = ZonedDateTime.now().minusDays(7)
                )
            )
        )

        seedDataRequest(
            publicId = "Z3M4MNAB7",
            createdBy = tvAdmin,
            organization = truevault,
            subjectEmail = "email6@example.com",
            collectionGroup = collectionGroups.getValue("newsletter-subscriber"),
            dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW,
            state = DataRequestState.CLOSED,
            substate = DataRequestSubstate.SUBJECT_NOT_FOUND,
            requestDate = ZonedDateTime.now().minusDays(10),
            closedAt = ZonedDateTime.now().minusDays(7),
            events = listOf(
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.MESSAGE_SENT,
                    message = "Please submit whatever ID you have",
                    createdAt = ZonedDateTime.now().minusDays(9)
                ),
                DataSubjectRequestEvent(
                    isDataSubject = true,
                    eventType = DataRequestEventType.MESSAGE_RECEIVED,
                    message = "No",
                    createdAt = ZonedDateTime.now().minusDays(8)
                ),
                DataSubjectRequestEvent(
                    actor = tvAdmin,
                    eventType = DataRequestEventType.CLOSED_NOT_VERIFIED,
                    message = "Subject refused ID verification",
                    createdAt = ZonedDateTime.now().minusDays(7)
                )
            )
        )

        seedPrivacyCenter(
            truevault,
            "BFK4MU1CF",
            "TrueVault Privacy Center",
            "truevault",
            "https://www.truevault.com/hubfs/favicon.png",
            customUrl = "truevault.staging.truevaultprivacycenter.com",
            cloudfrontDistribution = "E298K18UKLNN2K"
        )

        seedOrganizationMailbox(truevault, "privacy-staging@truevault.onmicrosoft.com", "1111111111")

        return truevault
    }

    private fun seedFlannelLabs(globals: GlobalSystems): Organization {
        val flannel = seedOrganization("Flannel Labs", publicId = "HKCX52BKC", trainingComplete = false)

        seedUser(flannel, DEFAULT_USERNAME, "flannellabs.com", "Admin", "Flannel", globalRole = GlobalRole.ADMIN)
        seedOrganizationFeature(flannel, Features.GET_COMPLIANT_PHASE, true)
        seedOrganizationFeature(flannel, Features.GDPR, true)
        seedOrganizationPrivacyEmails(flannel)
        seedPrivacyCenter(
            organization = flannel,
            publicId = "FLANNEL",
            name = "Privacy Center",
            defaultSubdomain = StringUtil.slugify(flannel.name),
            faviconUrl = "",
            customUrl = null,
            requestEmail = "flannel+privacy@example.com"
        )

        return flannel
    }

    private fun seedSomaStore(globals: GlobalSystems): Organization {
        val somaStore = seedOrganization(
            name = "Soma Store",
            publicId = "SOMASTORE",
            uuid = UUID.fromString("69760ba2-44d4-4c43-9b1e-adc01963a2d1"),
            faviconUrl = "https://www.truevault.com/hubfs/favicon.png",
            headerImageUrl = "https://via.placeholder.com/150"
        )

        val somaAdmin = seedUser(
            organization = somaStore,
            username = "soma",
            domain = DEFAULT_DOMAIN,
            firstName = "Soma",
            lastName = "Admin",
            globalRole = GlobalRole.ADMIN
        )

        seedOrganizationFeature(somaStore, Features.GDPR, true)

        val dataRecipients = seedDefaultDataRecipients(somaStore, globals)

        val collectionGroups = seedDefaultCollectionGroups(somaStore)

        seedDefaultDisclosedPic(somaStore, collectionGroups, dataRecipients, globals)

        seedDefaultProcessingInstructions(somaStore, collectionGroups)

        seedPrivacyCenter(
            organization = somaStore,
            publicId = "SOMA",
            name = "Soma Privacy Center",
            defaultSubdomain = "soma.privacy",
            faviconUrl = "https://www.truevault.com/hubfs/favicon.png",
            customUrl = "soma.privacy.truevaultstaging.com",
            cloudfrontDistribution = "E2W55ME8AU61GW",
            customUrlStatus = CustomUrlStatus.ACTIVE
        )

        seedOrganizationPrivacyEmails(somaStore)
        seedOrganizationMailbox(somaStore, "soma+privacy@qa.truevault.com", "2222222222")

        return somaStore
    }

    private fun seedGetCompliantOrganization(name: String, username: String, uuid: UUID? = null): Organization {
        return seedOrg(
            name = name,
            username = username,
            domain = DEFAULT_DOMAIN,
            features = listOf(Features.GET_COMPLIANT_PHASE),
            uuid = uuid
        )
    }

    private fun seedLegacyCustomerTransitionOrganization(name: String, username: String): Organization {
        val org = seedOrganization(name = name, publicId = name.toUpperCase())
        seedUser(
            organization = org,
            username = username,
            domain = DEFAULT_DOMAIN,
            firstName = username.capitalize(),
            lastName = name,
            password = DEFAULT_PASSWORD
        )
        seedOrganizationFeature(org, Features.LEGACY_TRANSITION, true)
        return org
    }

    private fun seedDefaultDataRecipients(organization: Organization, globals: GlobalSystems): Map<String, OrganizationDataRecipient> {
        // CONSUMER vendors
        val adVendor = globals.vendors.first { it.name == "Google Ads" }
        val salesVendor = globals.vendors.first { it.name == "Hubspot Service Hub" }
        val paymentVendor = globals.vendors.first { it.name == "Stripe" }
        val analyticsVendor = globals.vendors.first { it.name == "Datadog" }
        val noDataVendor = globals.vendors.first { it.name == "Firebase" }
        val inProgressVendor = globals.vendors.first { it.name == "Twilio" }

        // HR vendors
        val productivityVendor = globals.vendors.first { it.name == "Slack" }
        val supportVendor = globals.vendors.first { it.name == "Zendesk" }

        val thirdPartyClassification = globals.vendorClassifications.first { it.slug == VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY }
        val serviceProviderClassification = globals.vendorClassifications.first { it.slug == VENDOR_CLASSIFICATION_SLUG_SERVICE_PROVIDER }
        val noDataSharingClassification = globals.vendorClassifications.first { it.slug == "no-data-sharing" }

        return mapOf(
            // Consumer vendors
            adVendor.name!! to seedDataRecipient(organization, adVendor, thirdPartyClassification, isSharing = true, mappingProgress = MappingProgressEnum.DONE),
            paymentVendor.name!! to seedDataRecipient(organization, paymentVendor, serviceProviderClassification, mappingProgress = MappingProgressEnum.DONE),
            analyticsVendor.name!! to seedDataRecipient(organization, analyticsVendor, serviceProviderClassification, isSelling = true),
            salesVendor.name!! to seedDataRecipient(organization, salesVendor, thirdPartyClassification),
            noDataVendor.name!! to seedDataRecipient(organization, noDataVendor, noDataSharingClassification),
            inProgressVendor.name!! to seedDataRecipient(organization, inProgressVendor),

            // HR vendors
            productivityVendor.name!! to seedDataRecipient(organization, productivityVendor, serviceProviderClassification, collectionContext = CollectionContext.EMPLOYEE),
            supportVendor.name!! to seedDataRecipient(organization, supportVendor, serviceProviderClassification, collectionContext = CollectionContext.EMPLOYEE)
        )
    }

    private fun seedDefaultDisclosedPic(organization: Organization, collectionGroups: Map<String, CollectionGroup>, dataRecipients: Map<String, OrganizationDataRecipient>, globals: GlobalSystems) {
        val personalIdentifiersPicGroup = globals.picGroups.find { it.name == "Personal Identifiers" }!!
        val onlineIdentifiersPicGroup = globals.picGroups.find { it.name == "Online Identifiers" }!!
        val internetActivityPicGroup = globals.picGroups.find { it.name == "Internet Activity" }!!

        // CONSUMER context
        seedDisclosedPic(organization, dataRecipients["Datadog"]!!, collectionGroups["consumer"]!!, onlineIdentifiersPicGroup)
        seedDisclosedPic(organization, dataRecipients["Datadog"]!!, collectionGroups["consumer"]!!, internetActivityPicGroup)
        seedDisclosedPic(organization, dataRecipients["Google Ads"]!!, collectionGroups["consumer"]!!, onlineIdentifiersPicGroup)
        seedDisclosedPic(organization, dataRecipients["Google Ads"]!!, collectionGroups["consumer"]!!, internetActivityPicGroup)
        seedDisclosedPic(organization, dataRecipients["Twilio"]!!, collectionGroups["consumer"]!!, personalIdentifiersPicGroup)

        // HR context
        seedDisclosedPic(organization, dataRecipients["Slack"]!!, collectionGroups["employee"]!!, personalIdentifiersPicGroup)
        seedDisclosedPic(organization, dataRecipients["Zendesk"]!!, collectionGroups["employee"]!!, personalIdentifiersPicGroup)
    }

    private fun seedDefaultProcessingInstructions(organization: Organization, consumerGroups: Map<String, CollectionGroup>) {
        // Customer
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("consumer"),
            DataRequestType.CCPA_RIGHT_TO_KNOW,
            "Lorem Ipsum Instructem"
        )
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("consumer"),
            DataRequestType.CCPA_RIGHT_TO_KNOW,
            "Lorem Ipsum Instructem"
        )
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("consumer"),
            DataRequestType.CCPA_RIGHT_TO_DELETE,
            "Lorem Ipsum Instructem"
        )
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("consumer"),
            DataRequestType.CCPA_OPT_OUT,
            "Lorem Ipsum Instructem"
        )

        // Employee
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("employee"),
            DataRequestType.CCPA_RIGHT_TO_KNOW,
            "Lorem Ipsum Instructem"
        )
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("employee"),
            DataRequestType.CCPA_RIGHT_TO_KNOW,
            "Lorem Ipsum Instructem"
        )
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("employee"),
            DataRequestType.CCPA_RIGHT_TO_DELETE,
            "Lorem Ipsum Instructem"
        )

        // Prospect
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("prospect"),
            DataRequestType.CCPA_RIGHT_TO_KNOW,
            "Lorem Ipsum Instructem"
        )
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("prospect"),
            DataRequestType.CCPA_RIGHT_TO_KNOW,
            "Lorem Ipsum Instructem"
        )
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("prospect"),
            DataRequestType.CCPA_RIGHT_TO_DELETE,
            "Lorem Ipsum Instructem"
        )

        // Applicant
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("job-applicant"),
            DataRequestType.CCPA_RIGHT_TO_KNOW,
            "Lorem Ipsum Instructem"
        )
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("job-applicant"),
            DataRequestType.CCPA_RIGHT_TO_KNOW,
            "Lorem Ipsum Instructem"
        )
        seedProcessingInstruction(
            organization,
            consumerGroups.getValue("job-applicant"),
            DataRequestType.CCPA_RIGHT_TO_DELETE,
            "Lorem Ipsum Instructem"
        )
    }

    /** HELPERS **/
    private fun seedDefaultCollectionGroups(organization: Organization): Map<String, CollectionGroup> {
        return mapOf(
            "consumer" to seedCollectionGroup("Consumers", CollectionGroupType.BUSINESS_TO_CONSUMER, organization),
            "employee" to seedCollectionGroup("Employees", CollectionGroupType.EMPLOYMENT, organization),
            "job-applicant" to seedCollectionGroup("Job Applicants", CollectionGroupType.EMPLOYMENT, organization),
            "prospect" to seedCollectionGroup("Prospects", CollectionGroupType.BUSINESS_TO_CONSUMER, organization),
            "newsletter-subscriber" to seedCollectionGroup("Newsletter Subscribers", CollectionGroupType.BUSINESS_TO_CONSUMER, organization)
        )
    }

    private fun seedCollectionGroup(name: String, type: CollectionGroupType, organization: Organization): CollectionGroup {
        return collectionGroupRepository.findByNameAndOrganization(name, organization) ?: collectionGroupRepository.save(
            CollectionGroup(name = name, organization = organization, collectionGroupType = type)
        )
    }

    private fun seedDisclosedPic(
        organization: Organization,
        dataRecipient: OrganizationDataRecipient,
        collectionGroup: CollectionGroup,
        picGroup: PICGroup
    ) {
        dataMapService.updateDisclosure(organization, dataRecipient.vendor!!, collectionGroup, picGroup, UpdateDisclosureRequest(disclosed = true, updateCollection = true))
    }

    private fun seedOrganization(
        name: String,
        publicId: OrganizationPublicId = IdGenerator.generate(),
        uuid: UUID? = null,
        faviconUrl: String? = null,
        logoUrl: String? = null,
        headerImageUrl: String? = null,
        trainingComplete: Boolean = true,
        complianceVersion: OrgComplianceVersion? = null
    ): Organization {
        val organizationId = uuid ?: UUID.randomUUID()
        val organization = organizationRepository.findByPublicId(publicId) ?: organizationRepository.save(
            Organization(
                id = organizationId,
                publicId = publicId,
                name = name,
                identityVerificationMessage = "Check the **user's** [photo identification](https://en.wikipedia.org/wiki/Photo_identification)",
                faviconUrl = faviconUrl,
                logoUrl = logoUrl,
                headerImageUrl = headerImageUrl ?: "",
                trainingComplete = trainingComplete,
                physicalAddress = "4 SW Margaret's Ln, Zeeland, MI 49464",
                messageSignature = "",
                complianceVersion = complianceVersion
            )
        )
        regulationRepository.findBySlug("CCPA")?.let {
            organizationRegulationRepository.findByOrganizationAndRegulation(organization, it)
                ?: organizationRegulationRepository.save(
                    OrganizationRegulation(
                        regulation = it,
                        organization = organization,
                        identityVerificationInstruction = "Check the **user's** [photo identification](https://en.wikipedia.org/wiki/Photo_identification)"
                    )
                )
        }
        return organizationRepository.findByPublicId(publicId)!!
    }

    private fun seedPrivacyCenter(
        organization: Organization,
        publicId: String,
        name: String,
        defaultSubdomain: String,
        faviconUrl: String,
        customUrl: String?,
        customUrlStatus: CustomUrlStatus = CustomUrlStatus.VERIFYING,
        privacyPolicyMarkdown: String = "## Hello\n\nI am a privacy center",
        cloudfrontDistribution: String? = null,
        requestEmail: String? = "privacy@example.com"
    ): PrivacyCenter {
        var pc = privacyCenterRepository.findByPublicId(publicId)
        if (pc == null) {
            pc = PrivacyCenter(
                organization = organization,
                publicId = publicId,
                name = name,
                defaultSubdomain = defaultSubdomain,
                faviconUrl = faviconUrl,
                customUrl = customUrl,
                customUrlStatus = customUrlStatus,
                privacyPolicyUrl = if (!customUrl.isNullOrBlank()) "https://$customUrl/privacy-policy" else null,
                requestFormUrl = if (!customUrl.isNullOrBlank()) "https://$customUrl/privacy-request" else null,
                cloudfrontDistribution = cloudfrontDistribution,
                requestEmail = requestEmail
            )
            pc.policySections = listOf(
                PrivacyCenterPolicySection(
                    privacyCenter = pc,
                    name = "Overview",
                    anchor = "overview",
                    displayOrder = 1,
                    body = privacyPolicyMarkdown
                )
            )
            privacyCenterRepository.save(pc)
        }
        return pc
    }

    private fun seedProcessingInstruction(
        organization: Organization,
        collectionGroup: CollectionGroup,
        dataRequestType: DataRequestType,
        instruction: String
    ): ProcessingInstruction {
        return processingInstructionsService.createOrUpdate(
            organizationId = organization.publicId,
            dataSubjectTypeId = collectionGroup.id.toString(),
            dataRequestType = dataRequestType,
            processingInstruction = instruction
        )
    }

    private fun seedUser(
        organization: Organization,
        username: String,
        domain: String,
        firstName: String,
        lastName: String,
        role: UserRole = UserRole.ADMIN,
        status: UserStatus = UserStatus.ACTIVE,
        globalRole: GlobalRole? = null,
        additionalOrganizations: List<Organization> = listOf(),
        password: String?
    ): User {
        val userEmail = "$username@$domain"
        val user = userRepository.findByEmail(userEmail) ?: userService.createUserNoInvitation(
            email = userEmail,
            firstName = firstName,
            lastName = lastName,
            password = password ?: username,
            organization = organization,
            role = role,
            status = status,
            globalRole = globalRole
        )
        additionalOrganizations.forEach {
            organizationUserRepository.findByUserAndOrganization(user, it)
                ?: userService.associateUserToOrganization(user, it, role)
        }
        return user
    }

    private fun seedUser(
        organization: Organization,
        username: String,
        domain: String,
        firstName: String,
        lastName: String,
        role: UserRole = UserRole.ADMIN,
        status: UserStatus = UserStatus.ACTIVE,
        globalRole: GlobalRole? = null,
        additionalOrganizations: List<Organization> = listOf()
    ) = seedUser(
        organization = organization,
        username = username,
        domain = domain,
        firstName = firstName,
        lastName = lastName,
        role = role,
        status = status,
        globalRole = globalRole,
        additionalOrganizations = additionalOrganizations,
        password = null
    )

    data class VendorSeed(
        val name: String,
        val category: String? = "Productivity",
        val url: String,
        val logoFilename: String? = null,
        val spRecommendation: ServiceProviderRecommendation? = ServiceProviderRecommendation.NONE,
        val spLangaugeUrl: String? = null,
        val email: String? = null,
        val contactUrl: String? = null,
        val helptextAdnetworkSharing: String? = null,
        val key: String? = null,
        val aliases: List<String>? = listOf(),

        val deletionInstructions: String? = null,
        val ccpaRequestToKnowLink: String? = null,
        val ccpaRequestToDeleteLink: String? = null,
        val dpaUrl: String? = null,
        val generalGuidance: String? = null,
        val specialCircumstancesGuidance: String? = null,
        val recommendedPersonalInformationCategories: List<String> = listOf(),
        val disclosureSourceCategory: String? = null,
        val isPlatform: Boolean? = null,
        val isStandalone: Boolean? = null,
        val isDataSource: Boolean? = null,
        val isDataRecipient: Boolean? = null,
        val gdprProcessorRecommendation: GDPRProcessorRecommendation? = null,
        val gdprHasSccRecommendation: Boolean? = null,
        val gdprInternationalDataTransferRulesApply: Boolean? = null
    )

    private fun seedVendor(s: VendorSeed): Vendor {
        val uuid = UUID.randomUUID().toString()
        val vendorId = UUID.fromString(uuid)

        return vendorRepository.save(
            Vendor(
                id = vendorId,
                name = s.name,
                url = s.url,
                logoFilename = s.logoFilename,
                recipientCategory = s.category,
                serviceProviderRecommendation = s.spRecommendation,
                serviceProviderLanguageUrl = s.spLangaugeUrl,
                email = s.email,
                dpaUrl = s.dpaUrl,
                helptextAdnetworkSharing = s.helptextAdnetworkSharing,
                vendorKey = s.key,
                aliases = s.aliases,
                ccpaRequestToKnowLink = s.ccpaRequestToKnowLink,
                ccpaRequestToDeleteLink = s.ccpaRequestToDeleteLink,
                deletionInstructions = s.deletionInstructions,
                generalGuidance = s.generalGuidance,
                specialCircumstancesGuidance = s.specialCircumstancesGuidance,
                recommendedPICIds = s.recommendedPersonalInformationCategories,
                isDataSource = s.isDataSource ?: true,
                isDataRecipient = s.isDataRecipient ?: true,
                isPlatform = s.isPlatform,
                isStandalone = s.isStandalone,
                disclosureSourceCategory = s.disclosureSourceCategory,
                gdprInternationalDataTransferRulesApply = s.gdprInternationalDataTransferRulesApply,
                gdprProcessorRecommendation = s.gdprProcessorRecommendation,
                gdprHasSccRecommendation = s.gdprHasSccRecommendation ?: false
            )
        )
    }

    private fun seedVendorClassification(
        uuid: String = UUID.randomUUID().toString(),
        name: String,
        tooltipText: String = "",
        displayOrder: Int = 0,
        slug: String
    ): VendorClassification {
        val id = UUID.fromString(uuid)
        return vendorClassificationRepository.findBySlug(slug) ?: vendorClassificationRepository.save(
            VendorClassification(
                id = id,
                name = name,
                tooltipText = tooltipText,
                displayOrder = displayOrder,
                slug = slug
            )
        )
    }

    private fun seedDataRecipient(
        organization: Organization,
        vendor: Vendor,
        vendorClassification: VendorClassification? = null,
        vendorContract: VendorContract? = null,
        vendorContractReviewed: VendorContractReviewed? = null,
        collectionContext: CollectionContext = CollectionContext.CONSUMER,
        isSelling: Boolean = false,
        isSharing: Boolean = false,
        mappingProgress: MappingProgressEnum = MappingProgressEnum.NOT_STARTED
    ): OrganizationDataRecipient {
        return organizationDataRecipientRepository.findByOrganizationAndVendor(organization, vendor)
            ?: organizationDataRecipientRepository.save(
                OrganizationDataRecipient(
                    organization = organization,
                    vendor = vendor,
                    vendorClassification = vendorClassification,
                    vendorContract = vendorContract,
                    vendorContractReviewed = vendorContractReviewed,
                    status = if (vendorClassification != null) OrganizationVendorStatus.COMPLETED else OrganizationVendorStatus.IN_PROGRESS,
                    // status might not be true anymore
                    ccpaIsSelling = isSelling,
                    ccpaIsSharing = isSharing,
                    mappingProgress = mappingProgress,
                    collectionContexts = listOf(collectionContext)
                )
            )
    }

    private fun seedDataSource(organization: Organization, vendor: Vendor): OrganizationDataSource {
        return organizationDataSourceRepository.save(OrganizationDataSource(organizationId = organization.id, organization = organization, vendorId = vendor.id, vendor = vendor))
    }

    private fun seedBusinessPurpose(org: Organization, businessPurpose: BusinessPurpose, type: BusinessPurposeType) {
        organizationBusinessPurposeRepository.insertUniquely(org.id, businessPurpose.id, type.toString())
    }

    private fun seedOrganizationMailbox(organization: Organization, mailbox: String, nylasAccountId: String): OrganizationMailbox {
        return organization.organizationMailboxes.find { it.mailbox == mailbox } ?: organizationMailboxRepository.save(
            OrganizationMailbox(
                id = UUID.randomUUID(),
                mailbox = mailbox,
                type = MailboxType.GMAIL,
                status = MailboxStatus.CONNECTED,
                organization = organization,
                nylasAccountId = nylasAccountId
            )
        )
    }

    private fun seedOrganizationPrivacyEmails(organization: Organization) {
        repeat(10) {
            privacyEmailService.logPrivacyEmail(subject = "Privacy Email Subject Line", receivedAt = ZonedDateTime.now(), org = organization, createdPrivacyRequest = Random.nextBoolean())
        }
    }

    private fun seedFeature(name: String, description: String, enabledByDefault: Boolean): Feature {
        return featureRepository.findByName(name) ?: featureRepository.save(
            Feature(
                name = name,
                description = description,
                enabledByDefault = enabledByDefault
            )
        )
    }

    private fun seedOrganizationFeature(
        organization: Organization,
        featureName: String,
        enabled: Boolean
    ): OrganizationFeature {
        val feature = featureRepository.findByName(featureName) ?: seedFeature(featureName, featureName, false)
        return organizationFeatureRepository.findByOrganizationAndFeature(organization, feature)
            ?: organizationFeatureRepository.save(
                OrganizationFeature(
                    organization = organization,
                    feature = feature,
                    enabled = enabled
                )
            )
    }

    private fun seedDataRequest(
        publicId: DataRequestPublicId = IdGenerator.generate(),
        organization: Organization,
        collectionGroup: CollectionGroup,
        uuid: String = UUID.randomUUID().toString(),
        state: DataRequestState = DataRequestState.VERIFY_CONSUMER,
        substate: DataRequestSubstate? = null,
        createdBy: User,
        closedAt: ZonedDateTime? = null,
        subjectFirstName: String = "John",
        subjectLastName: String = "Doe",
        subjectEmail: String = "jdoe@example.com",
        subjectPhoneNumber: String = "(555) 555-1234",
        subjectMailingAddress: String = "742 Evergreen Terrace\nSpringfield, OR, 97403",
        dataRequestType: DataRequestType,
        requestDate: ZonedDateTime = ZonedDateTime.now().minusDays(3),
        requestDueDate: ZonedDateTime = requestDate.plusDays(45),
        requestOriginalDueDate: ZonedDateTime = requestDate.plusDays(45),
        events: List<DataSubjectRequestEvent> = emptyList(),
        submissionSource: DataRequestSubmissionSourceEnum = DataRequestSubmissionSourceEnum.POLARIS,
        organizationCustomInput: String? = null,
        selfDeclarationProvided: Boolean = false,
        context: CollectionContext = CollectionContext.CONSUMER
    ): DataSubjectRequest {

        val requestId = UUID.fromString(uuid)

        val request = dataRequestRepository.findByPublicId(publicId)

        if (request == null) {
            val newRequest = dataRequestRepository.save(
                DataSubjectRequest(
                    id = requestId, publicId = publicId, organization = organization,
                    state = state, substate = substate,
                    collectionGroups = mutableListOf(collectionGroup),
                    subjectFirstName = subjectFirstName,
                    subjectLastName = subjectLastName,
                    subjectEmailAddress = subjectEmail,
                    subjectPhoneNumber = subjectPhoneNumber,
                    subjectMailingAddress = subjectMailingAddress,
                    organizationCustomInput = organizationCustomInput,

                    closedAt = closedAt,
                    createdAt = requestDate,
                    requestType = dataRequestType,
                    requestDate = requestDate,
                    requestDueDate = requestDueDate,
                    requestOriginalDueDate = requestOriginalDueDate,
                    submissionSource = submissionSource,
                    requestorContacted = false,
                    selfDeclarationProvided = selfDeclarationProvided,
                    context = context
                )
            )

            val createdEvent = listOf(
                DataSubjectRequestEvent(
                    actor = createdBy,
                    eventType = DataRequestEventType.CREATED,
                    createdAt = requestDate
                )
            )

            val allEvents = createdEvent + events

            allEvents.forEach {
                dataRequestEventRepository.save(it.copy(dataSubjectRequest = newRequest))
            }

            dataRequestRepository.refresh(newRequest)

            return newRequest
        } else {
            return request
        }
    }
}
