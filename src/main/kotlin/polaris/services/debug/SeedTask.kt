package polaris.services.debug

import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Component
@Profile("ci")
class SeedTask(
    private val seedService: SeedService
) : CommandLineRunner {

    companion object {
        private val logger = LoggerFactory.getLogger(SeedTask::class.java)
    }

    override fun run(vararg args: String?) {
        logger.info("Beginning initial seed")
        seedService.seed()
        logger.info("Seed Complete")
    }
}
