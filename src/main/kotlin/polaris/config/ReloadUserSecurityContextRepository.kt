package com.truevault.atlas.config

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.BeanCreationException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.web.context.HttpRequestResponseHolder
import org.springframework.security.web.context.HttpSessionSecurityContextRepository
import polaris.services.support.AuthenticationService

class ReloadUserSecurityContextRepository : HttpSessionSecurityContextRepository() {

    companion object {
        private val logger = LoggerFactory.getLogger(ReloadUserSecurityContextRepository::class.java)
    }

    @Autowired
    private lateinit var authenticationService: AuthenticationService

    override fun loadContext(requestResponseHolder: HttpRequestResponseHolder): SecurityContext {
        val context = super.loadContext(requestResponseHolder)

        try {

            val currentUser = authenticationService.getCurrentUserDetails(context)

            if (currentUser != null) {
                context.authentication = UsernamePasswordAuthenticationToken(
                    currentUser,
                    context.authentication.credentials,
                    currentUser.authorities
                )
            }
        } catch (e: BeanCreationException) {
            // Asynchronous requests call the loadContext method after processing the request.
            // However, at that point the AuthenticationService is no longer available because it's
            // request-scoped. We don't need it (since the request has finished processing), but the
            // SecurityContextPersistenceFilter calls the loadContext method as part of persisting any
            // changes during the request. As a workaround, we ignore the exception since it doesn't
            // cause problems: the current user info was already loaded earlier in the request,
            // so it's available in the SecurityContextHolder when the SecurityContextPersistenceFilter
            // wants to save any changes.
            // TODO: Adjust the lifecycle of ReloadUserSecurityContextRepository and AuthenticationService
            // (possibly by removing request scope and using SecurityContextHolder to cache the user
            // information instead).
            ReloadUserSecurityContextRepository.logger.warn("Error loading user details: ${e.message}", e)
        }

        return context
    }
}
