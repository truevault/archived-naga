package polaris.config

import org.slf4j.LoggerFactory
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.stereotype.Component
import org.springframework.web.context.request.WebRequest
import polaris.util.PolarisException

@Component
class CustomErrorAttributes : DefaultErrorAttributes() {
    companion object {
        private val logger = LoggerFactory.getLogger(CustomErrorAttributes::class.java)
    }

    override fun getErrorAttributes(webRequest: WebRequest?, includeStackTrace: Boolean): Map<String, Any>? {
        val errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace)

        val error = getError(webRequest)
        if (error !is PolarisException) {
            logger.warn("NonPolaris exception detected. Masking message: ${errorAttributes["message"]}")
            errorAttributes["message"] = "Internal Server Error"
        }

        return errorAttributes
    }
}
