package polaris.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.truevault.atlas.config.ReloadUserSecurityContextRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.session.HttpSessionEventPublisher
import polaris.filters.SessionInvalidationFilter
import polaris.services.primary.LoginActivityService
import polaris.services.primary.UserService
import polaris.util.ApiRoutes
import java.net.URLEncoder
import java.time.Duration
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
class WebSecurityConfig(
    private val loginActivityService: LoginActivityService,
    private val userService: UserService,
    private val passwordEncoder: PasswordEncoder,
    private val objectMapper: ObjectMapper,
    @Value("#{T(java.time.Duration).parse('\${polaris.session.timeout:PT4H}')}")
    private val sessionTimeout: Duration
) : WebSecurityConfigurerAdapter() {
    companion object {
        private val log = LoggerFactory.getLogger(WebSecurityConfig::class.java)
    }

    // Most of the time you see all of the chained http configuration together, but this is much easier
    // to read and response to IntelliJ's code formatting much better
    override fun configure(http: HttpSecurity) {
        val invalidCredentialsHandler =
            { req: HttpServletRequest, response: HttpServletResponse, _: AuthenticationException ->
                val apiRequest = req.requestURI.toString().startsWith("/api")
                if (apiRequest) {
                    response.status = 401
                } else {
                    val target = URLEncoder.encode(req.requestURI, "UTF-8")
                    response.sendRedirect("/login?target=$target")
                }
            }

        http.addFilterAfter(SessionInvalidationFilter(sessionTimeout), UsernamePasswordAuthenticationFilter::class.java)

        // CSRF is a huge pain and doesn't offer us much right now, but we can always implement it later if need be.
        // Since we're on-site and internal-only, it's incredibly unlikely that CSRF will ever even affect us, so
        // this security feature adds complexity and overhead for virtually no gain.
        http.csrf().disable()

        // Since we're doing an AJAX POST to the login endpoint, this covers sending a failure response instead of
        // trying to redirect to an error URL, which is not what we want.
        http.exceptionHandling()
            .authenticationEntryPoint(invalidCredentialsHandler)

        // Default to all requests must be authenticated, but annotations will cover the specifics per-method
        http.authorizeRequests()
            .antMatchers("/login").permitAll()
            .antMatchers("/instructions/**").permitAll()
            .antMatchers("/debug/**").permitAll()
            .antMatchers("/design", "/design/atoms", "/design/molecules", "/design/organisms").permitAll()
            .antMatchers("/actuator/health").permitAll()
            .antMatchers(
                "/webhooks/email",
                "/webhooks/nylas/*",
                "/webhooks/bitbucket/*",
                "/webhooks/twilio/*",
                "/webhooks/cookiescan/*",
                "/webhooks/websiteaudit/*"
            ).permitAll()
            .antMatchers("/reset", "/reset/*", "/invite/*").permitAll()
            .antMatchers("/assets/**").permitAll()
            .antMatchers("/css/**").permitAll()
            .antMatchers("/.well-known/**").permitAll()
            .antMatchers(
                "/api/login",
                "/api/users/forgotPassword",
                "/api/users/invitations/*",
                "/api/users/updatePassword/*",
                "/api/users/*",
                "/api/users/*/activate/*",
                "/api/public/**"
            ).permitAll()
            .antMatchers("${ApiRoutes.PrivacyCenter.BUILDER}").permitAll()
            .antMatchers("${ApiRoutes.HostedForm.BASE}/**").permitAll()
            .antMatchers("/favicon.ico").permitAll()
            .anyRequest().authenticated()

        http.requiresChannel()
            .antMatchers("/api/login")

        http.sessionManagement()
            .maximumSessions(-1).expiredSessionStrategy {
                // For the moment, just send a 401 when the user tries to access something when their session is expired
                it.response.status = 401
            }

        http.formLogin()
            .loginPage("/login").permitAll()
            .loginProcessingUrl(ApiRoutes.Login.BASE).permitAll()
            .failureHandler { request: HttpServletRequest, response: HttpServletResponse, e: AuthenticationException ->
                loginActivityService.recordLoginAttempt(request.getParameter("username"), false)

                invalidCredentialsHandler(request, response, e)
            }
            .successHandler { _: HttpServletRequest, response: HttpServletResponse, authentication: Authentication ->
                val userDto = loginActivityService.handleLogin((authentication.principal as User).username)

                // Return  the DTO to the user so the user knows they're logged in.
                response.writer.write(objectMapper.writeValueAsString(userDto))
                response.status = 200
            }

        http.securityContext()
            .securityContextRepository(securityContextRepository())

        http.logout()
            .logoutUrl(ApiRoutes.Login.LOGOUT)
            .invalidateHttpSession(true)
            .deleteCookies("JSESSIONID")
            .permitAll()
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(authProvider())
    }

    @Bean
    fun httpSessionEventPublisher() = ServletListenerRegistrationBean(HttpSessionEventPublisher())

    @Bean
    fun authProvider(): DaoAuthenticationProvider {
        val authProvider = DaoAuthenticationProvider()
        authProvider.setUserDetailsService(userDetailsService())
        authProvider.setPasswordEncoder(passwordEncoder)
        return authProvider
    }

    @Bean
    override fun userDetailsService() = UserDetailsService { email ->
        val user = userService.findByEmail(email) ?: throw UsernameNotFoundException("Invalid Credentials")
        val roles = userService.springSecurityRolesForUser(user)

        User.builder()
            .username(user.email)
            .password(user.password)
            .roles(roles.joinToString("; "))
            .build()
    }

    @Bean
    fun securityContextRepository() = ReloadUserSecurityContextRepository()
}
