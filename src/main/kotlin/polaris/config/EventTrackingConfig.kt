package polaris.config

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

/**
 * Simple feature flag for now
 */
@Configuration
class EventTrackingConfig(
    @Value("\${polaris.event-tracking.enabled:false}")
    val enabled: Boolean
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(EventTrackingConfig::class.java)
    }
}
