package polaris.config

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.io.IOException
import java.sql.SQLException
import javax.servlet.http.HttpServletResponse
import javax.validation.ValidationException

@ControllerAdvice
class GlobalExceptionHandler : ResponseEntityExceptionHandler() {
    companion object {
        private val logger = LoggerFactory.getLogger(ResponseEntityExceptionHandler::class.java)
    }

    // Masking all SQL exceptions from the frontend as they usually contain the SQL statement
    @ExceptionHandler(SQLException::class)
    @Throws(IOException::class)
    fun handleSqlException(response: HttpServletResponse, exception: SQLException) {
        logger.warn("Returning Masked SQL Exception", exception)
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Error")
    }

    // Masking all ValidationException exceptions from the frontend as they usually contain parts of the SQL statement
    @ExceptionHandler(ValidationException::class)
    @Throws(IOException::class)
    fun handleValidationException(response: HttpServletResponse, exception: ValidationException) {
        logger.warn("Returning Masked Validation Exception", exception)
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Error")
    }
}
