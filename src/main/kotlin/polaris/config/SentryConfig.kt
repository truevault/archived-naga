package polaris.config

import io.sentry.Sentry
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@Configuration
class SentryConfig(
    @Value("\${sentry.environment}")
    private val environment: String?,

    @Value("\${sentry.sampleRate}")
    private val sampleRate: Double?,

    @Value("\${sentry.dsn}")
    private val dsn: String?
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(SentryConfig::class.java)
    }

    @PostConstruct
    fun configure() {
        if (dsn == null || dsn == "") {
            log.info("Sentry DSN not configured. Static Sentry API not initialized.")
            return
        }

        log.info("Static Sentry API successfully configured for environment $environment")
        Sentry.init { options ->
            options.environment = environment
            options.dsn = dsn
            options.tracesSampleRate = sampleRate
        }
    }
}
