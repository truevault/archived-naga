package polaris.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.session.config.SessionRepositoryCustomizer
import org.springframework.session.jdbc.JdbcIndexedSessionRepository

@Configuration
class SpringHttpSessionConfig {

    // JdbcIndexedSessionRepository.tableName is a private variable so need to pull directly from spring config
    @Value("\${spring.session.jdbc.table-name}")
    private val tableName: String? = null

    /**
     * Changes the create for session attributes to to an upsert on collision instead of just an insert
     * This is a known issues that manifests when making simultaneous web requests that all set session attributes.
     *
     * In this codebase it manifests from SessionInvalidationFilter#getSession where we set a timeout session attribute on every request
     *
     * This issues is described here: https://github.com/spring-projects/spring-session/issues/1031
     * but will probably not be solved as it requires specific SQL on a per platform basis
     */
    @Bean
    fun sessionRepositoryCustomizerCreateSessionsAttribute(): SessionRepositoryCustomizer<JdbcIndexedSessionRepository>? {
        return SessionRepositoryCustomizer { sessionRepository: JdbcIndexedSessionRepository -> sessionRepository.setCreateSessionAttributeQuery(this.getQuery(CREATE_SESSION_ATTRIBUTE_QUERY_ON_CONFLICT)) }
    }

    // JdbcIndexedSessionRepository.getQuery() is a private method so this is an override to support dynamic table name changes.
    private fun getQuery(query: String): String? {
        val table = this.tableName ?: JdbcIndexedSessionRepository.DEFAULT_TABLE_NAME
        return query.replace("%TABLE_NAME%", table)
    }

    // Update statement is pulled from: https://github.com/spring-projects/spring-session/pull/1481/files#diff-e8e52e344ee4f1294748810bc5100cbeR192
    private val CREATE_SESSION_ATTRIBUTE_QUERY_ON_CONFLICT = (
        "INSERT INTO %TABLE_NAME%_ATTRIBUTES(SESSION_PRIMARY_ID, ATTRIBUTE_NAME, ATTRIBUTE_BYTES) " +
            "SELECT PRIMARY_ID, ?, ? " +
            "FROM %TABLE_NAME% " +
            "WHERE SESSION_ID = ? ON CONFLICT(SESSION_PRIMARY_ID, ATTRIBUTE_NAME) DO UPDATE SET ATTRIBUTE_BYTES=EXCLUDED.ATTRIBUTE_BYTES"
        )
}
