import _ from "lodash";
const collator = new Intl.Collator(undefined, { numeric: true, caseFirst: "lower" });

export function by_keys(...keys) {
  return (objA, objB) => {
    for (const key of keys) {
      const res = collator.compare(_.get(objA, key), _.get(objB, key));
      if (res) return res;
    }
    return 0;
  };
}

export const nullLocalCompare = (a: string | null | undefined, b: string | null | undefined) => {
  if (!a && !b) {
    return 0;
  }
  if (!a) {
    return -1;
  }
  if (!b) {
    return 1;
  }

  return a.localeCompare(b);
};
