import { OrganizationDataRecipientDto } from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../common/service/server/dto/OrganizationDto";
import { VendorContractReviewedDto } from "../../common/service/server/dto/VendorDto";
import { GetCompliantStep } from "../../common/service/server/Types";
import { Routes } from "../root/routes";
import { isPayPal, recipientsByGdprProcessorRec, vendorsByType } from "./vendors";

export type StepLink = {
  url: string;
  label: string;
  progress?: GetCompliantStep;
};

const showCCPAConfirm = (recipients: OrganizationDataRecipientDto[]) => {
  const { serviceVendors } = vendorsByType(recipients || []);

  return serviceVendors.filter((ov) => ov.processingRegions.includes("UNITED_STATES")).length > 0;
};

const showCCPAFindAndLocate = (recipients: OrganizationDataRecipientDto[]): boolean => {
  const { otherVendors } = vendorsByType(recipients || []);

  const unknownVendors = otherVendors
    .filter((ov) => ov.processingRegions.includes("UNITED_STATES"))
    .filter(
      (ov) =>
        ((ov.classification?.slug == "needs-review" ||
          ov.classification?.slug == "service-provider") &&
          !isPayPal(ov) &&
          ov.serviceProviderRecommendation != "THIRD_PARTY") ||
        ov.vendorContractReviewed?.slug === "provider-language-not-found",
    );

  return unknownVendors.length > 0;
};

const showGDPRConfirmProcessors = (
  org: OrganizationDto,
  recipients: OrganizationDataRecipientDto[],
): boolean => {
  if (!org.featureGdpr) {
    return false;
  }

  const { processorRecommendation, controllerRecommendation } = recipientsByGdprProcessorRec(
    recipients || [],
  );

  return processorRecommendation.length > 0 || controllerRecommendation.length > 0;
};

const showGDPRFindAndLocate = (
  org: OrganizationDto,
  recipients: OrganizationDataRecipientDto[],
): boolean => {
  if (!org.featureGdpr) {
    return false;
  }

  const { other } = recipientsByGdprProcessorRec(recipients || []);

  return other.length > 0;
};

const Notice = {
  next: (org: OrganizationDto, recipients: OrganizationDataRecipientDto[]): StepLink => {
    if (showCCPAConfirm(recipients)) {
      return {
        url: Routes.getCompliant.dataRecipients.classifyServices,
        label: "Done",
        progress: "Classification.ConfirmServiceProvider",
      };
    }
    if (showCCPAFindAndLocate(recipients)) {
      return {
        url: Routes.getCompliant.dataRecipients.locateOther,
        label: "Done",
        progress: "Classification.FindAgreements",
      };
    } else if (showGDPRConfirmProcessors(org, recipients)) {
      return {
        url: Routes.getCompliant.dataRecipients.gdpr.confirmProcessors,
        label: "Done",
        progress: "Classification.GDPRConfirmProcessor",
      };
    } else if (showGDPRFindAndLocate(org, recipients)) {
      return {
        url: Routes.getCompliant.dataRecipients.gdpr.findAgreements,
        label: "Done",
        progress: "Classification.GDPRFindAgreements",
      };
    }
    return {
      url: Routes.getCompliant.dataCollection.consumerCollection,
      label: "Done",
      progress: "DataRecipients.DataSources",
    };
  },
};

const ClassifyServiceVendors = {
  next: (org: OrganizationDto, recipients: OrganizationDataRecipientDto[]): StepLink => {
    if (showCCPAFindAndLocate(recipients)) {
      return {
        url: Routes.getCompliant.dataRecipients.locateOther,
        label: "Next",
        progress: "Classification.FindAgreements",
      };
    } else if (showGDPRConfirmProcessors(org, recipients)) {
      return {
        url: Routes.getCompliant.dataRecipients.gdpr.confirmProcessors,
        label: "Next",
        progress: "Classification.GDPRConfirmProcessor",
      };
    } else if (showGDPRFindAndLocate(org, recipients)) {
      return {
        url: Routes.getCompliant.dataRecipients.gdpr.findAgreements,
        label: "Next",
        progress: "Classification.GDPRFindAgreements",
      };
    }

    return {
      url: Routes.getCompliant.dataCollection.consumerCollection,
      label: "Done with Classification",
      progress: "DataRecipients.DataSources",
    };
  },
  prev: (): StepLink => ({
    url: Routes.getCompliant.businessSurvey.consumerDataCollection,
    label: "Back to Data Map",
  }),
};

const LocateOtherVendors = {
  next: (
    org: OrganizationDto,
    recipients: OrganizationDataRecipientDto[],
    unknownRecipients: OrganizationDataRecipientDto[],
    languageNotFoundOption: VendorContractReviewedDto,
  ): StepLink => {
    const anyVendorsToContact = unknownRecipients.some(
      (x) => x.vendorContractReviewed && x.vendorContractReviewed.id === languageNotFoundOption?.id,
    );

    if (anyVendorsToContact) {
      return { url: Routes.getCompliant.dataRecipients.contact, label: "Next" };
    }

    if (showGDPRConfirmProcessors(org, recipients)) {
      return { url: Routes.getCompliant.dataRecipients.gdpr.confirmProcessors, label: "Next" };
    }

    if (showGDPRFindAndLocate(org, recipients)) {
      return {
        url: Routes.getCompliant.dataRecipients.gdpr.findAgreements,
        label: "Next",
        progress: "Classification.GDPRFindAgreements",
      };
    }

    return {
      url: Routes.getCompliant.dataCollection.consumerCollection,
      label: "Done",
      progress: "DataRecipients.DataSources",
    };
  },
  prev: (recipients: OrganizationDataRecipientDto[]): StepLink => {
    if (showCCPAConfirm(recipients)) {
      return { url: Routes.getCompliant.dataRecipients.classifyServices, label: "Back" };
    } else {
      return {
        url: Routes.getCompliant.businessSurvey.consumerDataCollection,
        label: "Back to Data Map",
      };
    }
  },
};

const GDPRConfirmProcessors = {
  next: (org: OrganizationDto, recipients: OrganizationDataRecipientDto[]): StepLink => {
    if (showGDPRFindAndLocate(org, recipients)) {
      return {
        url: Routes.getCompliant.dataRecipients.gdpr.findAgreements,
        label: "Next",
        progress: "Classification.GDPRFindAgreements",
      };
    }

    return {
      url: Routes.getCompliant.dataCollection.consumerCollection,
      label: "Done",
      progress: "DataRecipients.DataSources",
    };
  },
  prev: (recipients: OrganizationDataRecipientDto[]): StepLink => {
    if (showCCPAFindAndLocate(recipients)) {
      return { url: Routes.getCompliant.dataRecipients.locateOther, label: "Back" };
    }
    if (showCCPAConfirm(recipients)) {
      return { url: Routes.getCompliant.dataRecipients.classifyServices, label: "Back" };
    } else {
      return {
        url: Routes.getCompliant.businessSurvey.consumerDataCollection,
        label: "Back to Data Map",
      };
    }
  },
};

const GDPRFindAgreements = {
  next: (): StepLink => ({
    url: Routes.getCompliant.dataCollection.consumerCollection,
    label: "Done",
    progress: "DataRecipients.DataSources",
  }),
  prev: (org: OrganizationDto, recipients: OrganizationDataRecipientDto[]): StepLink => {
    if (showGDPRConfirmProcessors(org, recipients)) {
      return { url: Routes.getCompliant.dataRecipients.gdpr.confirmProcessors, label: "Back" };
    }
    if (showCCPAFindAndLocate(recipients)) {
      return { url: Routes.getCompliant.dataRecipients.locateOther, label: "Back" };
    }
    if (showCCPAConfirm(recipients)) {
      return { url: Routes.getCompliant.dataRecipients.classifyServices, label: "Back" };
    } else {
      return {
        url: Routes.getCompliant.businessSurvey.consumerDataCollection,
        label: "Back to Data Map",
      };
    }
  },
};

export const GetCompliantOrder = {
  Notice,
  ClassifyServiceVendors,
  LocateOtherVendors,
  GDPRConfirmProcessors,
  GDPRFindAgreements,
};
