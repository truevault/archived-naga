import { OrganizationFeatureDto } from "../../common/service/server/dto/OrganizationFeatureDto";

export const organizationFeatureIsEnabled = (
  featureName: string,
  organizationFeatures: OrganizationFeatureDto[] = [],
): boolean => {
  return organizationFeatures.filter((f) => f.name === featureName && f.enabled).length >= 1;
};
