// Downcase and replace whitespace with dash
export const slugify = (s?: string, separator = "-"): string =>
  !s ? "" : s.toLowerCase().replace(/\s+/g, separator);

export const isHtmlContentEmpty = (html: string) =>
  html
    .replace(/(<([^>]+)>)/gi, "")
    .trim()
    .replace(/\u200B/g, "") === "";

export const addProtocolIfNotExists = (url: string, protocol = "https") => {
  if (url.search("://") >= 0) {
    return url;
  }
  if (url.startsWith("//")) {
    return `${protocol}${url}`;
  }
  return `${protocol}://${url}`;
};

export const stripTags = (html) => {
  let doc = new DOMParser().parseFromString(html, "text/html");
  return doc.body.textContent || "";
};

type LexicalJoinType = "conjunction" | "disjunction";
export const lexicalJoin = (list: string[], fmt: LexicalJoinType = "conjunction"): string => {
  // @ts-ignore
  const formatter = new Intl.ListFormat("en", { style: "long", type: fmt });
  return formatter.format(list);
};
