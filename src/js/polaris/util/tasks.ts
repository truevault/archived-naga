import moment from "moment";
import { TaskDto } from "../../common/service/server/dto/TaskDto";

export const todoTaskComparitor = (a: TaskDto, b: TaskDto) => {
  const mDueA = a.dueAt && moment(a.dueAt).format("YYYYMMDD");
  const mDueB = b.dueAt && moment(b.dueAt).format("YYYYMMDD");
  const mCreatedA = a.createdAt && moment(a.createdAt).format("YYYYMMDD");
  const mCreatedB = b.createdAt && moment(b.createdAt).format("YYYYMMDD");

  if (mDueA && !mDueB) {
    return -1;
  }
  if (!mDueA && mDueB) {
    return 1;
  }
  // either both null, or both same date
  if (mDueA == mDueB) {
    // @ts-ignore
    return mCreatedA - mCreatedB;
  }

  // @ts-ignore
  return mDueB - mDueA;
};

export const completedTaskComparitor = (a: TaskDto, b: TaskDto) => {
  const mCompletedAtA = a.completedAt && moment(a.completedAt).format("YYYYMMDD");
  const mCompletedAtB = b.completedAt && moment(b.completedAt).format("YYYYMMDD");
  const mCreatedA = a.createdAt && moment(a.createdAt).format("YYYYMMDD");
  const mCreatedB = b.createdAt && moment(b.createdAt).format("YYYYMMDD");

  if (mCompletedAtA && !mCompletedAtB) {
    return -1;
  }
  if (!mCompletedAtA && mCompletedAtB) {
    return 1;
  }
  // either both null, or both same date
  if (mCompletedAtA == mCompletedAtB) {
    // @ts-ignore
    return mCreatedA - mCreatedB;
  }

  // @ts-ignore
  return mCompletedAtB - mCompletedAtA;
};
