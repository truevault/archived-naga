import { CategoryDto, CollectionDto } from "../../common/service/server/dto/DataMapDto";
import {
  PersonalInformationCategoryDto,
  DataInventorySnapshotDto,
} from "../../common/service/server/dto/PersonalInformationOptionsDto";

export type OrderingVariant = "default" | "employee";

export const getGroupedCategories = (
  categories: PersonalInformationCategoryDto[],
): Record<string, PersonalInformationCategoryDto[]> =>
  categories.reduce((acc, c) => {
    if (!acc[c.groupName]) {
      acc[c.groupName] = [];
    }
    acc[c.groupName].push(c);
    return acc;
  }, {});

export const getOrderedGroups = (
  categories: PersonalInformationCategoryDto[],
  variant: OrderingVariant = "default",
): string[] => {
  const sortFunc =
    variant === "employee"
      ? (a, b) => a.employeeGroupOrder - b.employeeGroupOrder
      : (a, b) => a.groupOrder - b.groupOrder;
  return categories.sort(sortFunc).reduce((acc, c) => {
    if (!acc.includes(c.groupName)) {
      acc.push(c.groupName);
    }
    return acc;
  }, []);
};

const consumerGroupIsComplete = (consumerGroupSnapshot: DataInventorySnapshotDto): boolean =>
  consumerGroupSnapshot.businessPurposes.length > 0 &&
  consumerGroupSnapshot.personalInformationCategories.length > 0;

export const getCompleteConsumerGroups = (snapshotData: DataInventorySnapshotDto[]) =>
  snapshotData.filter(consumerGroupIsComplete).map((sd) => sd.dataSubjectTypeId);

export const personalInformationCategorySorter = (
  a: PersonalInformationCategoryDto | CategoryDto,
  b: PersonalInformationCategoryDto | CategoryDto,
): number => {
  let aGroupOrder =
    (a as PersonalInformationCategoryDto).groupOrder || (a as CategoryDto).picGroupOrder;
  let bGroupOrder =
    (b as PersonalInformationCategoryDto).groupOrder || (b as CategoryDto).picGroupOrder;
  if (aGroupOrder == bGroupOrder) {
    return a.order - b.order;
  } else {
    return aGroupOrder - bGroupOrder;
  }
};

export const categorySorter = (a: CategoryDto, b: CategoryDto): number => {
  if (a.picGroupId == b.picGroupId) {
    return a.order - b.order;
  } else {
    return a.picGroupOrder - b.picGroupOrder;
  }
};

export const categoryEmploymentSorter = (a: CategoryDto, b: CategoryDto): number => {
  if (a.picGroupId == b.picGroupId) {
    return a.order - b.order;
  } else {
    return a.picGroupEmployeeOrder - b.picGroupEmployeeOrder;
  }
};

export const anyCollectedForCategory = (
  collected: CollectionDto,
  categories: CategoryDto[],
): boolean => collectedForCategory(collected, categories)?.length > 0 ?? false;

export const collectedForCategory = (
  collected: CollectionDto,
  categories: CategoryDto[],
): CategoryDto[] =>
  collected.collected
    ?.flatMap((c) => categories?.filter((pic) => pic.id === c))
    ?.sort(categorySorter);
