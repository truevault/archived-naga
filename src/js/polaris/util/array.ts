export const flatMap = <T, S>(arr: Array<T>, fn?: (v: T) => Array<S>): Array<S> =>
  arr.reduce((acc, v) => acc.concat(fn ? fn(v) : v), []);

export const unique = <T>(arr: Array<T>): Array<T> => Array.from(new Set(arr));

export const groupBy = <T>(
  arr: Array<T>,
  key: string | symbol | number,
): Record<string | symbol | number, Array<T>> =>
  arr.reduce((acc, item) => {
    const ikey = item[key];
    if (acc[ikey]) {
      acc[ikey] = acc[ikey].concat(item);
    } else {
      acc[ikey] = [item];
    }
    return acc;
  }, {});

export const interweave = <T>(arr: Array<T>): [Array<T>, Array<T>?] => {
  if (arr.length > 1) {
    const midpoint = Math.ceil(arr.length / 2);
    const col1 = arr.slice(0, midpoint);
    const col2 = arr.slice(midpoint);
    return [col1, col2];
  }
  return [arr];
};
