import { PrivacyCenterDto } from "../../common/service/server/dto/PrivacyCenterDto";
import { PublicPrivacyCenterDto } from "../../common/service/server/dto/PublicOrganizationNoticeDto";
import { addProtocolIfNotExists } from "./text";

type PrivacyDtos = PublicPrivacyCenterDto | PrivacyCenterDto;

const urlWithProtocol = (url: string, protocol = "https") => addProtocolIfNotExists(url, protocol);

const privacyCenterUrl = (privacyCenter: PrivacyDtos, suffix: string) => {
  return urlWithProtocol(`${privacyCenter.url}${suffix}`);
};

const getRootUrl = (privacyCenter: PrivacyDtos) => privacyCenterUrl(privacyCenter, "");
const getCAPrivacyNoticeUrl = (privacyCenter: PrivacyDtos) =>
  getPrivacyPolicySectionUrl(privacyCenter, "california-privacy-notice");

const getPrivacyPolicySectionUrl = (privacyCenter: PrivacyDtos, sectionAnchor: string) =>
  privacyCenterUrl(privacyCenter, `/privacy-policy#${sectionAnchor}`);

const getGDPRNoticeUrl = (privacyCenter: PrivacyDtos) =>
  getPrivacyPolicySectionUrl(privacyCenter, "eea-uk-privacy-notice");

const getVAPrivacyNoticeUrl = (privacyCenter: PrivacyDtos) =>
  getPrivacyPolicySectionUrl(privacyCenter, "virginia-privacy-notice");

const getCOPrivacyNoticeUrl = (privacyCenter: PrivacyDtos) =>
  getPrivacyPolicySectionUrl(privacyCenter, "colorado-privacy-notice");

const getCTPrivacyNoticeUrl = (privacyCenter: PrivacyDtos) =>
  getPrivacyPolicySectionUrl(privacyCenter, "connecticut-privacy-notice");

const getJobApplicantsNoticeUrl = (privacyCenter: PrivacyDtos) =>
  privacyCenterUrl(privacyCenter, "/job-applicant-privacy-notice");

const getEmployeeNoticeUrl = (privacyCenter: PrivacyDtos) =>
  privacyCenterUrl(privacyCenter, "/employee-privacy-notice");

const getContractorNoticeUrl = (privacyCenter: PrivacyDtos) =>
  privacyCenterUrl(privacyCenter, "/contractor-privacy-notice");

const getEeaUkEmployeeNoticeUrl = (privacyCenter: PrivacyDtos) =>
  privacyCenterUrl(privacyCenter, "/eea-uk-employee-privacy-notice");

const getLimitUrl = (privacyCenter: PrivacyDtos) => privacyCenterUrl(privacyCenter, "/limit");

const getOptOutUrl = (privacyCenter: PrivacyDtos, isSelling: boolean, isSharing: boolean) => {
  const suffix = optOutSuffix(isSelling, isSharing);
  if (!suffix) {
    return null;
  }
  return privacyCenterUrl(privacyCenter, suffix);
};

const optOutSuffix = (isSelling: boolean, isSharing: boolean) => {
  if (isSelling || isSharing) {
    return "/opt-out";
  }
  return undefined;
};

export const PrivacyCenterUrls = {
  getRootUrl,
  getCAPrivacyNoticeUrl,
  getPrivacyPolicySectionUrl,
  getOptOutUrl,
  getJobApplicantsNoticeUrl,
  getEmployeeNoticeUrl,
  getContractorNoticeUrl,
  getEeaUkEmployeeNoticeUrl,
  getLimitUrl,
  getVAPrivacyNoticeUrl,
  getCOPrivacyNoticeUrl,
  getCTPrivacyNoticeUrl,
  getGDPRNoticeUrl,
};
