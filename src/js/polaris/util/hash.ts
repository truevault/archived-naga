export const djb2 = (str: string): number => {
  let hash = 5381;
  for (let i = 0; i < str.length; i++) {
    hash = (hash << 5) + hash + str.charCodeAt(i); // hash * 33 + c
  }
  return hash;
};

const COLORS = ["green", "neutral", "orange", "primary", "red", "secondary", "yellow"];

const COLOR_VARIANTS = [400, 500, 600, 700, 800, 900];

export const stringToColorClass = (str: string): string => {
  const hash = djb2(str);
  const colorIdx = hash % COLORS.length;
  const variantIdx = hash % COLOR_VARIANTS.length;

  return `bg-color-${COLORS[colorIdx]}-${COLOR_VARIANTS[variantIdx]}`;
};
