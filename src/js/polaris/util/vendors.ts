import _ from "lodash";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
  OrganizationVendorSettingsDto,
} from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../common/service/server/dto/VendorDto";
import { ProcessingMethod } from "../../common/service/server/Types";
import {
  VENDOR_CATEGORY_AD_NETWORK,
  VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY,
  VENDOR_KEY_FB_ADS,
  VENDOR_KEY_GA,
  VENDOR_KEY_GOOGLE_ADS,
  VENDOR_KEY_PAYPAL,
  VENDOR_KEY_SHOPIFY,
  VENDOR_KEY_TRUEVAULT_POLARIS,
} from "../types/Vendor";
import { SurveyAnswers } from "../../common/hooks/useSurvey";
import { DataMapDto } from "../../common/service/server/dto/DataMapDto";

export const DATA_STORAGE_LOCATION_CATEGORY = "Data Storage Location";

export const sortedOrganizationVendors = (vendors: OrganizationDataRecipientDto[]) => {
  if (vendors) {
    const resultVendors: OrganizationDataRecipientDto[] = [];
    const resultVendorIds: string[] = [];
    vendors.forEach((vendor) => {
      if (!resultVendorIds.includes(vendor.vendorId)) {
        resultVendorIds.push(vendor.vendorId);
        resultVendors.push(vendor);
      }
    });
    return resultVendors.sort((a, b) => {
      if (a.status === "In-Progress" && b.status !== "In-Progress") {
        return -1;
      }
      if (b.status === "In-Progress" && a.status !== "In-Progress") {
        return 1;
      }
      if (a.status === b.status) {
        return a.name.localeCompare(b.name);
      }
      return 0;
    });
  }
  return [];
};

interface VendorsByType {
  serviceVendors: OrganizationDataRecipientDto[];
  thirdParty: OrganizationDataRecipientDto[];
  contractors: OrganizationDataRecipientDto[];
  otherVendors: OrganizationDataRecipientDto[];
}

export const vendorsByType = (vendors: OrganizationDataRecipientDto[]): VendorsByType => {
  return vendors.reduce(
    (acc, ov) => {
      if (ov.dataRecipientType === "contractor") {
        acc.contractors.push(ov);
      } else if (ov.classification.slug === VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY) {
        acc.thirdParty.push(ov);
      } else if (ov.serviceProviderRecommendation === "SERVICE_PROVIDER") {
        acc.serviceVendors.push(ov);
      } else {
        acc.otherVendors.push(ov);
      }
      return acc;
    },
    {
      serviceVendors: [] as OrganizationDataRecipientDto[],
      thirdParty: [] as OrganizationDataRecipientDto[],
      contractors: [] as OrganizationDataRecipientDto[],
      otherVendors: [] as OrganizationDataRecipientDto[],
    },
  );
};

type OrgDataRecipientBySellingSharing = {
  selling: OrganizationDataRecipientDto[];
  sharing: OrganizationDataRecipientDto[];
  other: OrganizationDataRecipientDto[];
};

export const vendorsByCcpaSellingOrSharing = (
  dataRecipients: OrganizationDataRecipientDto[],
): OrgDataRecipientBySellingSharing => {
  return _.reduce(
    dataRecipients,
    (result, dr) => {
      if (dr.ccpaIsSelling) {
        result.selling.push(dr);
      } else if (dr.ccpaIsSharing) {
        result.sharing.push(dr);
      } else {
        result.other.push(dr);
      }

      return result;
    },
    {
      selling: [] as OrganizationDataRecipientDto[],
      sharing: [] as OrganizationDataRecipientDto[],
      other: [] as OrganizationDataRecipientDto[],
    },
  );
};

interface RecipientsByGdprProcessor {
  processorRecommendation: OrganizationDataRecipientDto[];
  controllerRecommendation: OrganizationDataRecipientDto[];
  other: OrganizationDataRecipientDto[];
}

export const recipientsByGdprProcessorRec = (
  vendors: OrganizationDataRecipientDto[],
): RecipientsByGdprProcessor => {
  return vendors
    .filter((ov) => ov.processingRegions.includes("EEA_UK"))
    .reduce(
      (acc, ov) => {
        if (ov.gdprProcessorRecommendation == "Processor") {
          acc.processorRecommendation.push(ov);
        } else if (ov.gdprProcessorRecommendation == "Controller") {
          acc.controllerRecommendation.push(ov);
        } else {
          acc.other.push(ov);
        }
        return acc;
      },
      {
        processorRecommendation: [] as OrganizationDataRecipientDto[],
        controllerRecommendation: [] as OrganizationDataRecipientDto[],
        other: [] as OrganizationDataRecipientDto[],
      },
    );
};

export const recipientsByGdprProcessorRecOrLockedSetting = (
  vendors: OrganizationDataRecipientDto[],
): RecipientsByGdprProcessor => {
  return vendors
    .filter((ov) => ov.processingRegions.includes("EEA_UK"))
    .reduce(
      (acc, ov) => {
        if (
          ov.gdprProcessorRecommendation == "Processor" ||
          (ov.gdprProcessorSetting == "Processor" && ov.isGdprProcessorSettingLocked)
        ) {
          acc.processorRecommendation.push(ov);
        } else if (
          ov.gdprProcessorRecommendation == "Controller" ||
          (ov.gdprProcessorSetting == "Controller" && ov.isGdprProcessorSettingLocked)
        ) {
          acc.controllerRecommendation.push(ov);
        } else {
          acc.other.push(ov);
        }
        return acc;
      },
      {
        processorRecommendation: [] as OrganizationDataRecipientDto[],
        controllerRecommendation: [] as OrganizationDataRecipientDto[],
        other: [] as OrganizationDataRecipientDto[],
      },
    );
};

interface UnknownVendorsByType {
  contractTermsVendors: OrganizationDataRecipientDto[];
  publicTermsVendors: OrganizationDataRecipientDto[];
}

export const unknownVendorsByContract = (
  vendors: OrganizationDataRecipientDto[],
): UnknownVendorsByType => {
  return vendors.reduce(
    (acc, ov) => {
      if (ov.vendorContract?.slug == "found-contract") {
        acc.contractTermsVendors.push(ov);
      } else if (ov.vendorContract?.slug == "use-public") {
        acc.publicTermsVendors.push(ov);
      } else {
        // Error state?
      }
      return acc;
    },
    {
      contractTermsVendors: [] as OrganizationDataRecipientDto[],
      publicTermsVendors: [] as OrganizationDataRecipientDto[],
    },
  );
};

export const organizationVendorIsComplete = (ov) => ov.status === "Completed";

export const organizationVendorIsInProgress = (ov) => ov.status === "In-Progress";

export const getVendorSettingsObject = (
  vendor?: OrganizationDataRecipientDto,
): OrganizationVendorSettingsDto => {
  return {
    contacted: vendor?.contacted || false,
    classificationId: vendor?.classification?.id || "",
    vendorContractId: vendor?.vendorContract?.id || "",
    // @ts-ignore
    vendorContractReviewId: vendor?.vendorContractReviewed?.id || "",
    ccpaIsSelling: vendor?.ccpaIsSelling || false,
    ccpaIsSharing: vendor?.ccpaIsSharing || false,
    automaticallyClassified: vendor?.automaticallyClassified || false,
    isDataStorage: vendor?.isDataStorage || false,
    publicTosUrl: vendor?.publicTosUrl ?? null,

    // file attachment settings
    tosFileKey: vendor?.tosFileKey ?? null,
    tosFileName: vendor?.tosFileName ?? null,

    processingRegions: vendor?.processingRegions,

    gdprProcessorSetting: vendor?.gdprProcessorSetting ?? null,
    gdprProcessorGuaranteeUrl: vendor?.gdprProcessorGuaranteeUrl ?? null,
    gdprProcessorGuaranteeFileKey: vendor?.gdprProcessorGuaranteeFileKey ?? null,
    gdprProcessorGuaranteeFileName: vendor?.gdprProcessorGuaranteeFileName ?? null,
    gdprControllerGuaranteeUrl: vendor?.gdprControllerGuaranteeUrl ?? null,
    gdprControllerGuaranteeFileKey: vendor?.gdprControllerGuaranteeFileKey ?? null,
    gdprControllerGuaranteeFileName: vendor?.gdprControllerGuaranteeFileName ?? null,

    // For custom vendors
    name: vendor?.name ?? "",
    category: vendor?.category ?? null,
    url: vendor?.url ?? null,
    email: vendor?.email ?? null,

    dataAccessibility: vendor?.dataAccessibility,

    gdprHasSccSetting: vendor?.gdprHasSccSetting ?? null,
    gdprSccUrl: vendor?.gdprSccUrl ?? null,
    gdprSccFileName: vendor?.gdprSccFileName ?? null,
    gdprSccFileKey: vendor?.gdprSccFileKey ?? null,

    removedFromExceptionsToScc: vendor?.removedFromExceptionsToScc ?? false,

    completed: vendor?.completed ?? true,
  };
};

export const isThirdPartyRecipient = (
  organizationVendor: OrganizationDataRecipientDto | VendorDto,
): boolean => organizationVendor.dataRecipientType == "third_party_recipient";

export const isThirdParty = (organizationVendor: OrganizationDataRecipientDto): boolean =>
  ["third-party", "needs-review"].includes(organizationVendor?.classification?.slug);

export const organizationVendorIsSelling = (vendor: OrganizationDataRecipientDto): boolean =>
  vendor.ccpaIsSelling && vendor.classification?.slug !== "service-provider";

export const organizationVendorIsSharing = (vendor: OrganizationDataRecipientDto): boolean =>
  vendor.ccpaIsSharing;

export const isAdNetwork = (vendor: OrganizationDataRecipientDto | VendorDto) =>
  vendor.category === VENDOR_CATEGORY_AD_NETWORK;

// specific vendor checks

export const isSpecificVendor = (
  vendor: VendorDto | OrganizationDataRecipientDto,
  key: string,
): boolean => vendor.vendorKey == key;
const findSpecificVendor = <T extends VendorDto | OrganizationDataRecipientDto>(
  vendors: T[],
  key: string,
): T | null => vendors.find((v) => isSpecificVendor(v, key)) ?? null;

const createIsSpecificVendor =
  (key: string) => (vendor: VendorDto | OrganizationDataRecipientDto) =>
    isSpecificVendor(vendor, key);

const createFindSpecificVendor =
  (key: string) =>
  <T extends VendorDto | OrganizationDataRecipientDto>(vendors: T[]): T | null =>
    findSpecificVendor(vendors, key);

export const isShopify = createIsSpecificVendor(VENDOR_KEY_SHOPIFY);
export const findShopify = createFindSpecificVendor(VENDOR_KEY_SHOPIFY);

export const isGoogleAnalytics = createIsSpecificVendor(VENDOR_KEY_GA);
export const findGoogleAnalytics = createFindSpecificVendor(VENDOR_KEY_GA);

export const isGoogleAds = createIsSpecificVendor(VENDOR_KEY_GOOGLE_ADS);
export const findGoogleAds = createFindSpecificVendor(VENDOR_KEY_GOOGLE_ADS);

export const isFacebookAds = createIsSpecificVendor(VENDOR_KEY_FB_ADS);
export const findFacebookAds = createFindSpecificVendor(VENDOR_KEY_FB_ADS);

export const isPayPal = createIsSpecificVendor(VENDOR_KEY_PAYPAL);
export const findPayPal = createFindSpecificVendor(VENDOR_KEY_PAYPAL);

export const RETAIN_BY_DEFAULT_VENDORS = [VENDOR_KEY_TRUEVAULT_POLARIS];

export const defaultProcessingMethod = (
  vendor: OrganizationDataRecipientDto,
): ProcessingMethod | null => {
  if (!vendor) return null;

  if (vendor?.vendorKey && RETAIN_BY_DEFAULT_VENDORS.includes(vendor.vendorKey)) {
    return "RETAIN";
  }

  return null;
};

export const DEFAULT_RETENTION_REASONS_MAP = {
  [VENDOR_KEY_TRUEVAULT_POLARIS]: ["comply-with-a-legal-obligation"],
};

export const defaultVendorRetentionReasons = (vendor: OrganizationDataRecipientDto): string[] => {
  if (!vendor?.vendorKey) return [];

  return _.get(DEFAULT_RETENTION_REASONS_MAP, vendor.vendorKey, []);
};

export const isDataRetentionVendor = (v: OrganizationDataRecipientDto) =>
  v.instructions.some((i) => i.processingMethod === "RETAIN");

export const isIntentionalDisclosure = (
  answers: SurveyAnswers,
  v: OrganizationDataRecipientDto,
): boolean => {
  let intentionalDisclosures;
  try {
    intentionalDisclosures = JSON.parse(answers["vendor-intentional-disclosure-selection"]);
  } catch (e) {
    intentionalDisclosures = [];
  }
  return intentionalDisclosures.includes(v.id);
};

export const isRequestContextApplicable = (
  dataMap: DataMapDto,
  context: CollectionContext,
  v: OrganizationDataRecipientDto,
) => {
  if (dataMap && Array.isArray(dataMap.collection)) {
    const collectionGroupIds = dataMap.collection
      .filter((c) => {
        if (context === "EMPLOYEE") {
          return c.collectionGroupType === "EMPLOYMENT";
        } else {
          return c.collectionGroupType !== "EMPLOYMENT";
        }
      })
      .map((c) => c.collectionGroupId);
    return dataMap.disclosure
      .filter((d) => collectionGroupIds.includes(d.collectionGroupId))
      .map((sv) => sv.vendorId)
      .includes(v.vendorId);
  }
  return false;
};
