import { RequestHandlingInstructionType } from "../../common/service/server/Types";
import { RequestHandlingInstructionDto } from "../../common/service/server/dto/RequestHandlingInstructionDto";

const matchInstruction = (
  inst: RequestHandlingInstructionDto,
  orgId: string,
  vendorId: string,
  requestType: RequestHandlingInstructionType,
) => {
  return (
    orgId === inst.organizationId && vendorId === inst.vendorId && requestType === inst.requestType
  );
};

export const findInstruction = (
  instructions: RequestHandlingInstructionDto[],
  orgId: string,
  vendorId: string,
  requestType: RequestHandlingInstructionType,
) => {
  return instructions.find((inst) => {
    return matchInstruction(inst, orgId, vendorId, requestType);
  });
};
