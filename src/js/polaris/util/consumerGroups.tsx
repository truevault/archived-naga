import { Badge, Business, Store } from "@mui/icons-material";
import React from "react";
import { DataSubjectTypeDetails } from "../../common/models";
import { CollectionGroupType } from "../../common/service/server/controller/CollectionGroupsController";

export const consumerGroupSorter = (
  a: DataSubjectTypeDetails,
  b: DataSubjectTypeDetails,
): number => {
  if (!a.collectionGroupType && !b.collectionGroupType) {
    return a.name.localeCompare(b.name);
  } else if (a.collectionGroupType === b.collectionGroupType) {
    return a.name.localeCompare(b.name);
  } else {
    return collectionGroupTypeSorter(a.collectionGroupType, b.collectionGroupType);
  }
};

export const isEmploymentGroup = (consumerGroup: DataSubjectTypeDetails): boolean =>
  consumerGroup.collectionGroupType === "EMPLOYMENT";

export const isCaApplicantGroup = (consumerGroup: DataSubjectTypeDetails): boolean =>
  isEmploymentGroup(consumerGroup) &&
  consumerGroup.name.toLowerCase().includes("california job applicant");

export const isCaEmployeeGroup = (consumerGroup: DataSubjectTypeDetails): boolean =>
  isEmploymentGroup(consumerGroup) &&
  consumerGroup.name.toLowerCase().includes("california employee");

export const isCaContractorGroup = (consumerGroup: DataSubjectTypeDetails): boolean =>
  isEmploymentGroup(consumerGroup) &&
  consumerGroup.name.toLowerCase().includes("california contractor");

export const isEeaUkEmployeeGroup = (consumerGroup: DataSubjectTypeDetails): boolean =>
  isEmploymentGroup(consumerGroup) && consumerGroup.autocreationSlug == "EEA_UK_EMPLOYEE";

export const nameForCollectionGroupType = (collectionGroupType: CollectionGroupType) => {
  switch (collectionGroupType) {
    case "BUSINESS_TO_BUSINESS":
      return "Business to Business";
    case "BUSINESS_TO_CONSUMER":
      return "Business to Consumer";
    case "EMPLOYMENT":
      return "Employment";
  }
};

const collectionGroupTypeSorter = (a: CollectionGroupType, b: CollectionGroupType): number => {
  return orderForCollectionGroupType(a) - orderForCollectionGroupType(b);
};

const orderForCollectionGroupType = (a: CollectionGroupType): number => {
  switch (a) {
    case "BUSINESS_TO_CONSUMER":
      return 1;
    case "BUSINESS_TO_BUSINESS":
      return 2;
    case "EMPLOYMENT":
      return 3;
    default:
      return 4;
  }
};

export const getIconForCollectionGroupType = (type: CollectionGroupType) => {
  if (type === "BUSINESS_TO_CONSUMER") {
    return <Store color="primary" />;
  } else if (type === "BUSINESS_TO_BUSINESS") {
    return <Business color="primary" />;
  } else if (type === "EMPLOYMENT") {
    return <Badge color="primary" />;
  }
};

export const getShortLabelForCollectionGroupType = (type: CollectionGroupType) => {
  if (type === "BUSINESS_TO_CONSUMER") {
    return "B2C";
  } else if (type === "BUSINESS_TO_BUSINESS") {
    return "B2B";
  } else if (type === "EMPLOYMENT") {
    return "EMPLOYMENT";
  }
};
