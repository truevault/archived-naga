import { partition } from "lodash";
import lunr from "lunr";
import { useMemo } from "react";
import { DataMapDto } from "../../../common/service/server/dto/DataMapDto";
import {
  OrganizationDataRecipientDto,
  OrganizationDataRecipientPlatformDto,
} from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../../common/service/server/dto/VendorDto";

export const buildVendorSearchIndex = (vendors) =>
  lunr(function () {
    this.pipeline.remove(lunr.stemmer);
    this.searchPipeline.remove(lunr.stemmer);
    this.ref("id");
    this.field("name", { boost: 3 });
    this.field("aliases", { boost: 3 });
    this.field("category", { boost: 1 });
    vendors
      .filter((s) => s.dataRecipientType != "storage_location")
      .forEach((s) => {
        this.add({ ...s, aliases: s?.aliases?.join(" ") });
      });
  });

export const useDefaultVendors = (catalog: VendorDto[] | undefined, defaultNames: string[]) => {
  return useMemo(() => {
    const lowerCaseNames = new Set(defaultNames.map((n) => n.toLowerCase()));
    return (
      catalog
        ?.filter((v) => lowerCaseNames.has(v.name.toLowerCase()))
        ?.sort((a, b) => a.name.localeCompare(b.name)) ?? []
    );
  }, [catalog, defaultNames]);
};

export const useDataStorageLocations = (dataRecipients: OrganizationDataRecipientDto[]) =>
  useMemo(
    () => partition(dataRecipients, (dr) => dr.dataRecipientType === "storage_location"),
    [dataRecipients],
  );

export const vendorName = (
  vendor: OrganizationDataRecipientDto | VendorDto | null,
  isThirdPartyRecipient: boolean,
): string => {
  const defaultName = isThirdPartyRecipient ? "this recipient" : "this vendor";
  return vendor?.name || defaultName;
};

export const installedAppsForVendor = (
  vendor: OrganizationDataRecipientDto,
  platformApps: OrganizationDataRecipientPlatformDto,
) =>
  vendor?.isPlatform && platformApps?.installed
    ? platformApps.installed
        .map((vendorId) => platformApps.available.find((pa) => pa.vendorId === vendorId))
        .sort((appA, appB) => appA?.name?.localeCompare(appB?.name ?? "") ?? 0)
    : [];

export const installedAppsRespectingDeletionForVendor = (
  vendor: OrganizationDataRecipientDto,
  platformApps: OrganizationDataRecipientPlatformDto,
): OrganizationDataRecipientDto[] =>
  (vendor?.isPlatform && platformApps?.installed
    ? platformApps?.installed
        ?.map((vendorId) => platformApps.available.find((pa) => pa.vendorId === vendorId))
        ?.filter((v) => v?.respectsPlatformRequests)
        ?.sort((appA, appB) => appA?.name?.localeCompare(appB?.name ?? "") ?? 0) ?? []
    : []) as OrganizationDataRecipientDto[];

const filterVendorsByCollectionGroupType = (
  vendors: OrganizationDataRecipientDto[],
  dataMap: DataMapDto,
  collectionGroupType: String,
) => {
  if (dataMap && dataMap.collection && dataMap.disclosure) {
    const { collection, disclosure } = dataMap;
    const collectionGroupIds = collection.reduce((acc, col) => {
      if (col.collectionGroupType === collectionGroupType) {
        acc.push(col.collectionGroupId);
      }
      return acc;
    }, []);
    const disclosureVendorIds = disclosure.reduce((acc, dis) => {
      if (collectionGroupIds.includes(dis.collectionGroupId)) {
        acc.push(dis.vendorId);
      }
      return acc;
    }, []);
    return vendors.filter((v) => {
      return disclosureVendorIds.includes(v.vendorId);
    });
  }
};

export const getConsumerVendors = (
  vendors: OrganizationDataRecipientDto[],
  dataMap: DataMapDto,
) => {
  return filterVendorsByCollectionGroupType(vendors, dataMap, "BUSINESS_TO_CONSUMER");
};

export const getEmploymentVendors = (
  vendors: OrganizationDataRecipientDto[],
  dataMap: DataMapDto,
) => {
  return filterVendorsByCollectionGroupType(vendors, dataMap, "EMPLOYMENT");
};
