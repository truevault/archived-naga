export const addValueToSet: <T>(value: T) => (set: Set<T>) => Set<T> = (value) => (set) =>
  new Set(set.add(value));

export const removeValueFromSet: <T>(value: T) => (set: Set<T>) => Set<T> = (value) => (set) => {
  set.delete(value);
  return new Set(set);
};
