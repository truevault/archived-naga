export const unreachable = (x: never): null => {
  console.warn(`Unreachable code reached. Didn't expect to ever receive: `, x);
  return null;
};
