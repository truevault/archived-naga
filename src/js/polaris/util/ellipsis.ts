// Implemented due to the following bugs:
// https://material-ui.com/components/menus/#limitations
// https://bugs.chromium.org/p/chromium/issues/detail?id=327437
export const ellipsis = (text: string, length: number) => {
  if (text.length <= length) {
    return text;
  }
  return `${text.substr(0, length - 3)}...`;
};
