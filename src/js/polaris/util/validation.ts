import moment, { Moment } from "moment";
import validator from "validator";
import { Error as InputError } from "../copy/input";
import { isHtmlContentEmpty } from "./text";

export const isUrl = (val: string): boolean =>
  validator.isURL(val) || validator.isURL(val.replace("localhost", "foo.bar"));

export const requiredString = (values, key, errors) => {
  const val = values[key];

  if (!val || !/\S/.test(val)) {
    errors[key] = InputError.Required;
  }
};

export const requiredHtmlContent = (values, key, errors) => {
  const val: string = values[key];

  if (!val || isHtmlContentEmpty(val)) {
    errors[key] = InputError.Required;
  }
};

export const requiredPassword = (values, key, errors) => {
  const val = values[key];

  if (!val || !/\S/.test(val)) {
    errors[key] = InputError.Required;
    return;
  }

  if (val.length < 8) {
    errors[key] = InputError.PasswordTooShort;
    return;
  }

  if (val.length > 64) {
    errors[key] = InputError.PasswordTooLong;
    return;
  }
};

export const requiredEmail = (values, key, errors) => {
  const val = values[key];

  if (!val) {
    errors[key] = InputError.Required;
  } else if (!validator.isEmail(val)) {
    errors[key] = InputError.InvalidEmail;
  }
};

type ValidatorFn = (val: any) => string | undefined;
export const composeValidators =
  (...validators: ValidatorFn[]) =>
  (value: any): string =>
    validators.reduce<string>((error, validator) => error || validator(value), undefined);

export const validateEmail = (val) =>
  validator.isEmail(val ?? "") ? undefined : InputError.InvalidEmail;
export const validateRequired = (val) => (val ? undefined : InputError.Required);
export const validateDate = (val) =>
  !val || moment(val).isValid() ? undefined : InputError.InvalidDate;

export const validateNotIn =
  (collection: string[], message: string = "Value already exists", caseSensitive: boolean = true) =>
  (val: string) => {
    const included = caseSensitive
      ? collection.includes(val)
      : collection.some((v) => v.toLowerCase() === val.toLowerCase());

    return included ? message : undefined;
  };

export const requiredDate = (values, key, errors) => {
  const val = values[key];

  if (!val) {
    errors[key] = InputError.Required;
  } else if (!(val as Moment).isValid()) {
    errors[key] = InputError.InvalidDate;
  }
};

export const optionalEmail = (values, key, errors) => {
  const val = values[key];
  if (val && !validator.isEmail(val)) {
    errors[key] = InputError.InvalidEmail;
  }
};

export const requiredUrl = (values, key, errors) => {
  const val = values[key];
  if (!val) {
    errors[key] = InputError.Required;
  } else if (!isUrl(val)) {
    errors[key] = InputError.InvalidUrl;
  }
};

export const optionalUrl = (values, key, errors) => {
  const val = values[key];
  if (val && !isUrl(val)) {
    errors[key] = InputError.InvalidUrl;
  }
};
