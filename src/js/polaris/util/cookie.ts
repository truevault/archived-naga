import {
  CookieCategory,
  OrganizationCookieDto,
} from "../../common/service/server/dto/CookieScanDto";

export const cookieSorter = (a: OrganizationCookieDto, b: OrganizationCookieDto): number => {
  if (a.domain == b.domain) {
    return a.name.localeCompare(b.name);
  }
  return a.domain.localeCompare(b.name);
};

export const fmtCookieCategory = (category: CookieCategory): string => {
  switch (category) {
    case "ANALYTICS":
      return "Analytics";
    case "FUNCTIONALITY":
      return "Functionality";
    case "ADVERTISING":
      return "Advertising";
    case "PERSONALIZATION":
      return "Personalization";
    case "SECURITY":
      return "Security";
  }
};
