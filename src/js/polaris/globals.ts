declare const __ENVIRONMENT__: string;
declare const __SHA_VERSION__: string;
declare const __SENTRY_DSN__: string;
declare const __ALLOW_DRAFT_MODE__: boolean;
declare const __IS_DEVELOPMENT__: boolean;
declare const __BEACON_INIT_KEY__: string;

const env: string = __ENVIRONMENT__;
const version: string = __SHA_VERSION__;
const sentryDsn: string = __SENTRY_DSN__;
const allowDraftMode: boolean = __ALLOW_DRAFT_MODE__;
const isDevelopment: boolean = __IS_DEVELOPMENT__;
const beaconInitKey: string = __BEACON_INIT_KEY__;

export {
  env as __ENVIRONMENT__,
  isDevelopment as __IS_DEVELOPMENT__,
  version as __SHA_VERSION__,
  sentryDsn as __SENTRY_DSN__,
  allowDraftMode as __ALLOW_DRAFT_MODE__,
  beaconInitKey as __BEACON_INIT_KEY__,
};
