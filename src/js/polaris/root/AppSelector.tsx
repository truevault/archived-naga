import * as Sentry from "@sentry/react";
import React from "react";
import { Route, Switch } from "react-router";
import { NetworkRequest } from "../../common/models";
import { PageLoading } from "../components";
import { useSelf } from "../hooks";
import { useParamMessage } from "../hooks/useParamMessage";
import { LoggedInApp } from "./LoggedInApp";
import { NotLoggedInApp } from "./NotLoggedInApp";
import { Routes } from "./routes";
import { theme as appTheme } from "../../common/root/appTheme";

import { PublicPublishNoticesPage } from "../components/privacyCenter/PublicPublishNoticesPage";
import { StyledEngineProvider } from "@mui/material";
import { ThemeProvider } from "@mui/styles";
import { useIsRestoring } from "@tanstack/react-query";
import { TopBannerSlot } from "../components/molecules/TopBannerSlot";

export const AppSelector = () => {
  useParamMessage();
  return (
    <>
      <Switch>
        <Route exact path={Routes.instructions.id}>
          <StyledEngineProvider injectFirst>
            <ThemeProvider theme={appTheme}>
              <PublicPublishNoticesPage />
            </ThemeProvider>
          </StyledEngineProvider>
        </Route>

        <Route>
          <AppSwitcher />
        </Route>
      </Switch>
      <TopBannerSlot />
    </>
  );
};

const AppSwitcher = () => {
  const restoring = useIsRestoring();
  const [self, selfRequest] = useSelf();

  if (restoring || NetworkRequest.anyRunning(selfRequest)) {
    return <PageLoading />;
  }

  if (self) {
    Sentry.setUser({ id: self.id, email: self.email });
  }

  const anyOrgs = Boolean(self?.organizations?.length);
  const anyInvitations = Boolean(self?.invitedOrganizations?.length);

  if (!self) {
    return <NotLoggedInApp />;
  } else if (!anyOrgs && !anyInvitations) {
    return <h1>Unfortunately you're not associated with any organizations</h1>;
  } else {
    return <LoggedInApp />;
  }
};
