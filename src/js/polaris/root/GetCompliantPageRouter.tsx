import React from "react";
import { Redirect } from "react-router";
import { Route, Switch } from "react-router-dom";
import { useSetApiMode } from "../hooks/useSetApiMode";
import { AcceptInvitation } from "../pages/auth/AcceptInvitationPage";
import { BusinessSurvey } from "../pages/get-compliant/BusinessSurvey";
import { Complete } from "../pages/get-compliant/Complete";
import { AdditionalPrivacyLanguagePage } from "../pages/get-compliant/consumer-collection/AdditionalPrivacyLanguagePage";
import { DataRetentionCookieScanner } from "../pages/get-compliant/consumer-collection/ConsumerCollectionCookieScanner";
import { ConsumerCollectionGdprLawfulBases } from "../pages/get-compliant/consumer-collection/ConsumerCollectionGdprLawfulBases";
import { ConsumerCollectionIncentives } from "../pages/get-compliant/consumer-collection/ConsumerCollectionIncentives";
import { ConsumerCollectionNoticeReview } from "../pages/get-compliant/consumer-collection/ConsumerCollectionNoticeReview";
import { ConsumerCollectionPurposes } from "../pages/get-compliant/consumer-collection/ConsumerCollectionPurposes";
import { ConsumerDataCollection } from "../pages/get-compliant/consumer-collection/ConsumerDataCollection";
import { ConsumerDataCollectionForGroup } from "../pages/get-compliant/consumer-collection/ConsumerDataCollectionForGroup";
import { CorePrivacyConceptsPage } from "../pages/get-compliant/consumer-collection/CorePrivacyConceptsPage";
import { GDPRContactProcessors } from "../pages/get-compliant/consumer-collection/GDPRContactProcessors";
import { GDPRInternationalDataTransfers } from "../pages/get-compliant/consumer-collection/GDPRInternationalDataTransfers";
import { ContactVendors } from "../pages/get-compliant/ContactVendors";
import { CookiePolicyPage } from "../pages/get-compliant/CookiePolicyPage";
import { ConsumerDataSourcesPage } from "../pages/get-compliant/data-map/ConsumerDataSourcesPage";
import { AddDataRecipients } from "../pages/get-compliant/data-recipients/AddDataRecipients";
import { CCPAConfirmServiceProviders as ClassifyServiceVendors } from "../pages/get-compliant/data-recipients/classification/CCPAConfirmServiceProviders";
import { GDPRConfirmProcessors } from "../pages/get-compliant/data-recipients/classification/GDPRConfirmProcessors";
import { GDPRContactUnknownsPage } from "../pages/get-compliant/data-recipients/classification/GDPRContactUnknownsPage";
import { GDPRLocateProcessorGuarantees } from "../pages/get-compliant/data-recipients/classification/GDPRLocateProcessorGuarantees";
import { LocateOtherVendors } from "../pages/get-compliant/data-recipients/classification/LocateOtherVendors";
import { ContractorsPage } from "../pages/get-compliant/data-recipients/ContractorsPage";
import { DataDisclosuresPage } from "../pages/get-compliant/data-recipients/DataDisclosuresPage";
import { DataRecipientAssociation } from "../pages/get-compliant/data-recipients/DataRecipientAssociation";
import { DoubleCheck } from "../pages/get-compliant/data-recipients/DoubleCheck";
import { LimitNoticePage } from "../pages/get-compliant/data-recipients/LimitNoticePage";
import { OptOutNoticePage } from "../pages/get-compliant/data-recipients/OptOutNoticePage";
import { Platforms as DataRecipientPlatforms } from "../pages/get-compliant/data-recipients/Platforms";
import { ProcessingRegion } from "../pages/get-compliant/data-recipients/ProcessingRegion";
import { VendorSelling as DataRecipientVendorSelling } from "../pages/get-compliant/data-recipients/Selling";
import { VendorSharing as DataRecipientVendorSharing } from "../pages/get-compliant/data-recipients/Sharing";
import { ThirdPartyRecipientsPage } from "../pages/get-compliant/data-recipients/ThirdPartyRecipientsPage";
import { VendorAssociationForGroup } from "../pages/get-compliant/data-recipients/VendorAssociationForGroup";
import { DataMapIntroductionPage } from "../pages/get-compliant/DataMapIntroductionPage";
import { DataStoragePage } from "../pages/get-compliant/DataStoragePage";
import { DeletionException } from "../pages/get-compliant/DeletionExceptionPage";
import { EmployeeCollectionIndexPage } from "../pages/get-compliant/employee-collection/EmployeeCollectionIndexPage";
import { EmployeeCollectionForGroupPage } from "../pages/get-compliant/employee-collection/EmployeeDataCollectionForGroupPage";
import { EmployeeNoticeReviewPage } from "../pages/get-compliant/employee-collection/EmployeeNoticeReviewPage";
import { EmployeePrivacyNoticeReview } from "../pages/get-compliant/employee-collection/EmployeePrivacyNoticeReview";
import { EmployeeMapDataDisclosuresPage } from "../pages/get-compliant/employee-map/EmployeeMapDataDisclosuresPage";
import { EmployeeMapDataRecipientsPage } from "../pages/get-compliant/employee-map/EmployeeMapDataRecipientsPage";
import { EmployeeMapDataSourcesPage } from "../pages/get-compliant/employee-map/EmployeeMapDataSourcesPage";
import { EmployeeMapIntroductionPage } from "../pages/get-compliant/employee-map/EmployeeMapIntroductionPage";
import { HRSurvey } from "../pages/get-compliant/employee-map/HRSurvey";
import { GetCompliantDashboardPage } from "../pages/get-compliant/GetCompliantDashboardPage";
import { IntegrationsPage } from "../pages/get-compliant/IntegrationsPage";
import { DataPrivacyOfficerPage } from "../pages/get-compliant/privacy-center/DataPrivacyOfficerPage";
import { EmailAccountPage } from "../pages/get-compliant/privacy-center/EmailAccountPage";
import { EmailBrandingPage } from "../pages/get-compliant/privacy-center/EmailBrandingPage";
import { FaviconPage } from "../pages/get-compliant/privacy-center/FaviconPage";
import { WebsiteHeaderPage } from "../pages/get-compliant/privacy-center/WebsiteHeaderPage";
import { PrivacyNoticeChecklist } from "../pages/get-compliant/PrivacyNoticeChecklist";
import { PrivacyNoticeReview } from "../pages/get-compliant/PrivacyNoticeReview";
import { RetentionPolicyPage } from "../pages/get-compliant/RetentionPolicyPage";
import { ReviewVendorContracts } from "../pages/get-compliant/ReviewVendorContracts";
import { Settings } from "../pages/get-compliant/Settings";
import { Phase } from "../util/Phase";
import { DesignRoutes } from "./DesignRoutes";
import { FourOhFourRoutes } from "./FourOhFourRoutes";
import { useGetCompliantContext } from "./GetCompliantContext";
import { Routes } from "./routes";
import { SetupRoutes } from "./SetupRoutes";

type GetCompliantPageRouterProps = {
  phase: Phase;
};

export const GetCompliantPageRouter = ({ phase }: GetCompliantPageRouterProps) => {
  const { progress } = useGetCompliantContext();
  useSetApiMode("Draft", "GetCompliantPageRouter");
  const defaultRedirectTo = phase === "SETUP" ? Routes.setup.root : Routes.getCompliant.root;
  return (
    <Switch>
      <Route exact path={Routes.getCompliant.dashboard}>
        <GetCompliantDashboardPage />
      </Route>
      <Route exact path={Routes.getCompliant.businessSurvey.root}>
        <BusinessSurvey currentProgress={progress} />
      </Route>

      <Route exact path={Routes.getCompliant.dataMap.introduction}>
        <DataMapIntroductionPage />
      </Route>

      <Route exact path={Routes.getCompliant.businessSurvey.consumerDataCollection}>
        <ConsumerDataCollection />
      </Route>
      <Route exact path={Routes.getCompliant.businessSurvey.consumerDataCollectionForGroup}>
        <ConsumerDataCollectionForGroup />
      </Route>

      <Route exact path={Routes.getCompliant.dataCollection.root}>
        <Redirect to={Routes.getCompliant.dataCollection.consumerCollection} />
      </Route>
      <Route exact path={Routes.getCompliant.dataCollection.consumerCollection}>
        <Redirect to={Routes.getCompliant.dataCollection.consumerPurposes} />
      </Route>
      <Route exact path={Routes.getCompliant.dataCollection.consumerIncentives}>
        <ConsumerCollectionIncentives currentProgress={progress} />
      </Route>
      <Route exact path={Routes.getCompliant.dataCollection.gdprLawfulBases}>
        <ConsumerCollectionGdprLawfulBases currentProgress={progress} />
      </Route>

      <Route
        exact
        path={Routes.getCompliant.dataMap.consumerSources}
        component={ConsumerDataSourcesPage}
      />

      <Route exact path={Routes.getCompliant.dataCollection.consumerPurposes}>
        <ConsumerCollectionPurposes currentProgress={progress} />
      </Route>
      <Route exact path={Routes.getCompliant.dataCollection.corePrivacyConcepts}>
        <CorePrivacyConceptsPage currentProgress={progress} />
      </Route>

      <Route exact path={Routes.getCompliant.dataRetention.deletionExceptions}>
        <DeletionException currentProgress={progress} />
      </Route>

      <Route exact path={Routes.getCompliant.dataRetention.vendorIntegrations}>
        <IntegrationsPage />
      </Route>

      <Route exact path={Routes.getCompliant.dataRetention.retentionPolicy}>
        <RetentionPolicyPage />
      </Route>

      <Route exact path={Routes.getCompliant.dataRetention.dataStorage}>
        <DataStoragePage />
      </Route>

      <Route exact path={Routes.getCompliant.dataRetention.gdprCookieScanner}>
        <DataRetentionCookieScanner currentProgress={progress} />
      </Route>
      <Route exact path={Routes.getCompliant.dataRetention.cookiePolicy}>
        <CookiePolicyPage />
      </Route>

      <Route exact path={Routes.getCompliant.dataCollection.consumerNoticeReview}>
        <ConsumerCollectionNoticeReview currentProgress={progress} />
      </Route>

      <Route
        exact
        path={Routes.getCompliant.dataCollection.employmentNoticeReview}
        component={EmployeeNoticeReviewPage}
      />

      <Route exact path={Routes.getCompliant.dataCollection.additionalPrivacyLanguage}>
        <AdditionalPrivacyLanguagePage />
      </Route>

      <Route exact path={Routes.getCompliant.dataCollection.internationalDataTransfers}>
        <GDPRInternationalDataTransfers />
      </Route>

      <Route exact path={Routes.getCompliant.dataCollection.contactProcessors}>
        <GDPRContactProcessors />
      </Route>

      <Route
        exact
        path={[Routes.getCompliant.employeeMap.employeeReviewForGroup]}
        component={EmployeePrivacyNoticeReview}
      />

      <Route exact path={Routes.getCompliant.dataRecipients.root}>
        <AddDataRecipients currentProgress={progress} />
      </Route>

      <Route exact path={Routes.getCompliant.dataRecipients.platforms}>
        <DataRecipientPlatforms currentProgress={progress} />
      </Route>

      <Route exact path={Routes.getCompliant.dataRecipients.vendorAssociation}>
        <DataRecipientAssociation />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.vendorAssociationForGroup}>
        <VendorAssociationForGroup />
      </Route>

      <Route
        exact
        path={Routes.getCompliant.dataRecipients.dataDisclosures}
        component={DataDisclosuresPage}
      />

      <Route exact path={Routes.getCompliant.dataRecipients.doubleCheck}>
        <DoubleCheck />
      </Route>

      <Route
        exact
        path={Routes.getCompliant.dataRecipients.contractors}
        component={ContractorsPage}
      />
      <Route
        exact
        path={Routes.getCompliant.dataRecipients.thirdPartyRecipients}
        component={ThirdPartyRecipientsPage}
      />

      <Route exact path={Routes.getCompliant.dataRecipients.processingRegion}>
        <ProcessingRegion />
      </Route>

      <Route exact path={Routes.getCompliant.employeeMap.hrSurvey} component={HRSurvey} />

      <Route
        exact
        path={Routes.getCompliant.employeeMap.introduction}
        component={EmployeeMapIntroductionPage}
      />

      <Route
        exact
        path={Routes.getCompliant.employeeMap.employee}
        component={EmployeeCollectionIndexPage}
      />

      <Route
        exact
        path={Routes.getCompliant.employeeMap.employeeCollectionForGroup}
        component={EmployeeCollectionForGroupPage}
      />

      <Route
        exact
        path={Routes.getCompliant.employeeMap.dataRecipients}
        component={EmployeeMapDataRecipientsPage}
      />

      <Route
        exact
        path={Routes.getCompliant.employeeMap.dataDisclosures}
        component={EmployeeMapDataDisclosuresPage}
      />
      <Route
        exact
        path={Routes.getCompliant.employeeMap.dataRecipients}
        component={EmployeeMapDataRecipientsPage}
      />

      <Route
        exact
        path={Routes.getCompliant.employeeMap.dataSources}
        component={EmployeeMapDataSourcesPage}
      >
        <EmployeeMapDataSourcesPage />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.sharing}>
        <DataRecipientVendorSharing />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.selling}>
        <DataRecipientVendorSelling />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.notice}>
        <OptOutNoticePage currentProgress={progress} />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.noticeLimit}>
        <LimitNoticePage />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.classifyServices}>
        <ClassifyServiceVendors currentProgress={progress} />
      </Route>
      <Route path={Routes.getCompliant.dataRecipients.locateOther}>
        <LocateOtherVendors currentProgress={progress} />
      </Route>
      <Route path={Routes.getCompliant.dataRecipients.reviewVendorContracts}>
        <ReviewVendorContracts currentProgress={progress} />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.gdpr.confirmProcessors}>
        <GDPRConfirmProcessors currentProgress={progress} />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.gdpr.findAgreements}>
        <GDPRLocateProcessorGuarantees currentProgress={progress} />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.gdpr.contactUnknowns}>
        <GDPRContactUnknownsPage />
      </Route>
      <Route exact path={Routes.getCompliant.dataRecipients.contact}>
        <ContactVendors currentProgress={progress} />
      </Route>

      <Route exact path={Routes.getCompliant.privacyNotices.caPrivacyNotice}>
        <PrivacyNoticeReview currentProgress={progress} noticeType="CA_PRIVACY_NOTICE" />
      </Route>
      <Route exact path={Routes.getCompliant.privacyNotices.consumerGroup}>
        <PrivacyNoticeReview currentProgress={progress} noticeType="CONSUMER_GROUP" />
      </Route>
      <Route exact path={Routes.getCompliant.publishNotices.root}>
        <PrivacyNoticeChecklist currentProgress={progress} />
      </Route>

      <Route exact path={Routes.getCompliant.privacyRequests.privacyCenter}>
        <Redirect to={Routes.getCompliant.privacyCenter.emailAccount} />
      </Route>
      <Route exact path={Routes.getCompliant.privacyCenter.emailAccount}>
        <EmailAccountPage />
      </Route>
      <Route exact path={Routes.getCompliant.privacyCenter.emailBranding}>
        <EmailBrandingPage />
      </Route>
      <Route exact path={Routes.getCompliant.privacyCenter.websiteHeader}>
        <WebsiteHeaderPage />
      </Route>
      <Route exact path={Routes.getCompliant.privacyCenter.favicon}>
        <FaviconPage />
      </Route>
      <Route exact path={Routes.getCompliant.privacyCenter.dataPrivacyOfficer}>
        <DataPrivacyOfficerPage />
      </Route>

      <Route exact path={Routes.getCompliant.complete.root}>
        <Complete />
      </Route>
      <Route exact path={Routes.getCompliant.settings.user}>
        <Settings currentProgress={progress} />
      </Route>
      <Route exact path={Routes.getCompliant.settings.root}>
        <Redirect to={Routes.getCompliant.settings.user} />
      </Route>
      <Route exact path={Routes.getCompliant.root}>
        <Redirect to={Routes.getCompliant.dashboard} />
      </Route>
      <Route exact path={Routes.invite} component={AcceptInvitation} />
      <Route exact path={Routes.requests.active}>
        <Redirect to={defaultRedirectTo} />
      </Route>
      <Route exact path={Routes.login}>
        <Redirect to={defaultRedirectTo} />
      </Route>
      <Route exact path={Routes.auth.reset}>
        <Redirect to={defaultRedirectTo} />
      </Route>
      <Route exact path={Routes.auth.set}>
        <Redirect to={defaultRedirectTo} />
      </Route>
      <Route exact path={Routes.root}>
        <Redirect to={defaultRedirectTo} />
      </Route>
      <SetupRoutes phase={phase} />
      <DesignRoutes />
      <FourOhFourRoutes />
    </Switch>
  );
};
