import { StyledEngineProvider, ThemeProvider, Theme } from "@mui/material";
import React from "react";
import { Redirect } from "react-router";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { theme } from "../../common/root/generalTheme";
import { Login } from "../pages/auth/LoginPage";
import { ResetPassword } from "../pages/auth/ResetPasswordPage";
import { SetPassword } from "../pages/auth/SetPasswordPage";
import { AcceptInvitation } from "../pages/auth/AcceptInvitationPage";
import { DesignRoutes } from "./DesignRoutes";
import { Routes } from "./routes";
import { useSetApiMode } from "../hooks/useSetApiMode";

declare module "@mui/styles/defaultTheme" {
  interface DefaultTheme extends Theme {}
}

export const NotLoggedInApp = () => {
  useSetApiMode("Live", "NotLoggedInApp");
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path={Routes.design.root}>
              <DesignRoutes />
            </Route>

            <Route exact path={Routes.login} render={() => <Login />} />
            <Route exact path={Routes.invite} render={() => <AcceptInvitation />} />
            <Route exact path={Routes.reset} render={() => <ResetPassword />} />
            <Route exact path={Routes.auth.set} render={() => <SetPassword />} />

            <Route>
              <Redirect to={Routes.login} />
            </Route>
          </Switch>
        </Router>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};
