import { StyledEngineProvider, Theme, ThemeProvider } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter, useLocation } from "react-router-dom";
import { useDataRequest } from "../../common/hooks/api";
import { getCompliantProgressActions } from "../../common/reducers/getCompliantProgressSlice";
import { theme as appTheme } from "../../common/root/appTheme";
import { Api } from "../../common/service/Api";
import { PageLoading } from "../components";
import { usePrimaryOrganization, usePrimaryOrganizationId, useSelf, useSession } from "../hooks";
import { LegacyTransitionPage } from "../pages/auth/LegacyTransitionPage";
import { organizationFeatureIsEnabled } from "../util/features";
import { GetCompliantContextProvider } from "./GetCompliantContext";
import { GetCompliantPageRouter } from "./GetCompliantPageRouter";
import { StayCompliantPageRouter } from "./StayCompliantPageRouter";
import { SetupPageRouter } from "./SetupPageRouter";
import { MailboxDisconnectedBanner } from "../components/organisms/callouts/MailboxDisconnectedBanner";
import { __BEACON_INIT_KEY__ } from "../globals";

declare module "@mui/styles/defaultTheme" {
  interface DefaultTheme extends Theme {}
}

const initBeacon = () => (window as any).Beacon("init", `${__BEACON_INIT_KEY__}`);

const configureBeacon = (isGetCompliant = false) => {
  const baseConfig = {
    docsEnabled: true,
    messagingEnabled: true,
    mode: "askFirst",
    messaging: {
      chatEnabled: true,
    },
    enablePreviousMessages: true,
  };

  let configOpts: any;
  if (isGetCompliant) {
    configOpts = { display: { style: "icon" } };
  } else {
    configOpts = { display: { style: "manual" } };
  }
  const config = {
    ...baseConfig,
    ...configOpts,
  };
  (window as any).Beacon("config", config);
};

const destroyBeacon = () => {
  (window as any).Beacon("destroy");
};

const isRequestDetail = (pathname: string): boolean => {
  return (
    pathname.startsWith("/requests/") &&
    !["/requests/new", "/requests/active", "/requests/closed"].includes(pathname)
  );
};

export const LoggedInApp = () => {
  useSession();

  return (
    <BrowserRouter>
      <WrappedRouter />
    </BrowserRouter>
  );
};

const WrappedRouter = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const [locationPathname, setLocationPathname] = useState("");
  const [userIdentified, setUserIdentified] = useState(false);
  const [readyToRender, setReadyToRender] = useState(false);
  const [self, selfRequest] = useSelf();
  const organizationId = usePrimaryOrganizationId();
  const [organization, organizationRequest] = usePrimaryOrganization();

  const { request: featuresRequest, result: features } = useDataRequest({
    queryKey: ["features", organizationId],
    api: () => Api.organization.getOrganizationFeatures(organizationId),
  });

  useEffect(() => {
    if (selfRequest.success && !userIdentified) {
      (window as any).Beacon("identify", {
        name: `${self.firstName} ${self.lastName}`,
        email: self.email,
        signature: self.beaconSignature,
      });
      setUserIdentified(true);
    } else if (selfRequest.error) {
      destroyBeacon();
      initBeacon();
      configureBeacon();
    }
  }, [
    selfRequest,
    self.beaconSignature,
    self.email,
    self.firstName,
    self.lastName,
    userIdentified,
  ]);

  // HACK: When a location changes from a request detail page then close the beacon
  useEffect(() => {
    if (isRequestDetail(locationPathname) && locationPathname !== location.pathname) {
      configureBeacon();
      (window as any).Beacon("navigate", "/");
      (window as any).Beacon("close");
    }

    if (location.pathname !== locationPathname) {
      setLocationPathname(location.pathname);
    }
  }, [location, locationPathname]);

  useEffect(() => {
    if (organizationRequest.success && organization) {
      configureBeacon(organization.phase === "GET_COMPLIANT");
      dispatch(getCompliantProgressActions.set(organization.getCompliantProgress));
    }
  }, [organizationRequest, organization]);

  useEffect(() => {
    if (featuresRequest.success && organizationRequest.success && organization) {
      setReadyToRender(true);
    }
  }, [featuresRequest, organizationRequest]);

  if (!readyToRender) {
    return <PageLoading />;
  }

  // TODO: do we still need to support this feature?
  const isLegacyTransition = organizationFeatureIsEnabled("LegacyCustomerTransition", features);
  if (isLegacyTransition) {
    return <LegacyTransitionPage />;
  }

  const isAdmin = self.globalRole === "ADMIN";
  const isMailboxDisconnected = organization.mailbox.mailboxStatus == "NOT_CONNECTED";

  let router;
  switch (organization.phase) {
    case "SETUP":
      if (isAdmin) {
        router = (
          <GetCompliantContextProvider>
            <GetCompliantPageRouter phase={organization.phase} />
          </GetCompliantContextProvider>
        );
      } else {
        router = <SetupPageRouter />;
      }
      break;
    case "GET_COMPLIANT":
      router = (
        <GetCompliantContextProvider>
          <GetCompliantPageRouter phase={organization.phase} />
        </GetCompliantContextProvider>
      );
      break;
    case "STAY_COMPLIANT":
    default:
      router = (
        <>
          {isMailboxDisconnected && <MailboxDisconnectedBanner />}
          <StayCompliantPageRouter />
        </>
      );
      break;
  }

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={appTheme}>{router}</ThemeProvider>
    </StyledEngineProvider>
  );
};
