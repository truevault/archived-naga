import React, { createContext, useContext } from "react";
import { useSelector } from "react-redux";
import {
  GetCompliantStep,
  migrateStep,
  LegacyGetCompliantStep,
} from "../surveys/steps/GetCompliantSteps";

type GetCompliantContext = {
  progress: GetCompliantStep;
};

const Context = createContext<GetCompliantContext>({ progress: "BusinessSurvey.BusinessSurvey" });

export const useGetCompliantContext = () => useContext(Context);

export const GetCompliantContextProvider = ({ children }) => {
  const getCompliantProgress = useSelector(
    (state: { getCompliantProgress: GetCompliantStep | LegacyGetCompliantStep }) =>
      state.getCompliantProgress,
  );

  const migratedProgress = migrateStep(getCompliantProgress);

  return <Context.Provider value={{ progress: migratedProgress }}>{children}</Context.Provider>;
};
