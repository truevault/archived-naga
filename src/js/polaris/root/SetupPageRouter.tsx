import React from "react";
import { Redirect } from "react-router";
import { Route, Switch } from "react-router-dom";
import { Routes } from "./routes";
import { useSetApiMode } from "../hooks/useSetApiMode";
import { AddDataRecipients } from "../pages/get-compliant/data-recipients/AddDataRecipients";
import { usePrimaryOrganization } from "../hooks";

export const SetupPageRouter = () => {
  useSetApiMode("Draft", "SetupPageRouter");
  const [org] = usePrimaryOrganization();
  return (
    <Switch>
      <Route exact path={Routes.login}>
        <Redirect to={Routes.setup.root} />
      </Route>
      <Route exact path={Routes.auth.reset}>
        <Redirect to={Routes.setup.root} />
      </Route>
      <Route exact path={Routes.auth.set}>
        <Redirect to={Routes.setup.root} />
      </Route>
      <Route exact path={Routes.setup.root}>
        <Redirect to={Routes.setup.vendors} />
      </Route>
      <Route exact path={Routes.setup.vendors}>
        <AddDataRecipients phase="SETUP" currentProgress={org.getCompliantProgress} />
      </Route>
      <Route exact path={Routes.root}>
        <Redirect to={Routes.setup.root} />
      </Route>
      <Route>
        <Redirect to={Routes.setup.root} />
      </Route>
    </Switch>
  );
};
