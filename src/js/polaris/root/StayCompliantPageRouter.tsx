import React from "react";
import { Redirect } from "react-router";
import { Route, Switch } from "react-router-dom";
import { StayCompliantFinancialIncentivesPage } from "../components/privacyCenter/StayCompliantFinancialIncentivesPage";
import { StayCompliantPublishNoticesPage } from "../components/privacyCenter/StayCompliantPublishNoticesPage";
import { useSetApiMode } from "../hooks/useSetApiMode";
import { AcceptInvitation } from "../pages/auth/AcceptInvitationPage";
import { ConsumerDetailPage } from "../pages/consumer/ConsumerDetailPage";
import { CCPADisclosure } from "../pages/data-inventory/CCPADisclosure";
import { PersonalInformationDisclosed } from "../pages/data-inventory/PersonalInformationDisclosed";
import { PersonalInformationDisclosedSettings } from "../pages/data-inventory/PersonalInformationDisclosedSettings";
import { PersonalInformationSold } from "../pages/data-inventory/PersonalInformationSold";
import { PersonalInformationSoldSettings } from "../pages/data-inventory/PersonalInformationSoldSettings";
import { StayCompliantCollectionSources } from "../pages/data-inventory/StayCompliantCollectionSources";
import { StayCompliantDataMap } from "../pages/data-inventory/StayCompliantDataMap";
import { StayCompliantDataMapDetail } from "../pages/data-inventory/StayCompliantDataMapDetail";
import { CollectionGroupDetailPage } from "../pages/data-map/CollectionGroupDetailPage";
import { CollectionGroupNewPage } from "../pages/data-map/CollectionGroupNewPage";
import { CollectedPicForGroupPage } from "../pages/data-map/components/workflows/create-collection-group/CollectedPicForGroupPage";
import { DataReceiptForGroupPage } from "../pages/data-map/components/workflows/create-collection-group/DataReceiptForGroupPage";
import { DisclosuresForGroupPage } from "../pages/data-map/components/workflows/create-collection-group/DisclosuresForGroupPage";
import { ClassificationForDataRecipientPage } from "../pages/data-map/components/workflows/create-data-recipient/ClassificationForDataRecipientPage";
import { CollectionGroupAssociationForDataRecipientPage } from "../pages/data-map/components/workflows/create-data-recipient/CollectionGroupAssociationForDataRecipientPage";
import { DisclosureForDataRecipientPage } from "../pages/data-map/components/workflows/create-data-recipient/DisclosureForDataRecipientPage";
import { CollectionGroupAssociationPage } from "../pages/data-map/components/workflows/create-data-source/CollectionGroupAssociationPage";
import { DataReceiptForSourcePage } from "../pages/data-map/components/workflows/create-data-source/DataReceiptForSourcePage";
import { DataMapOverviewPage } from "../pages/data-map/DataMapOverviewPage";
import { DataRecipientDetailPage } from "../pages/data-map/DataRecipientDetailPage";
import { DataRecipientNewPage } from "../pages/data-map/DataRecipientNewPage";
import { DataSourceDetailPage } from "../pages/data-map/DataSourceDetailPage";
import { DataSourceNewPage } from "../pages/data-map/DataSourceNewPage";
import { LawfulBasesDetailPage } from "../pages/data-map/LawfulBasesDetailPage";
import { EditEmailTemplate } from "../pages/organization/EditEmailTemplatePage";
import { EmailSettingsPage as EmailSettings } from "../pages/organization/EmailSettingsPage";
import { EmailTemplateSettings } from "../pages/organization/EmailTemplateSettingsPage";
import { IntegrationsSettingsPage } from "../pages/organization/IntegrationsSettingsPage";
import { OrganizationSettingsEditorPage } from "../pages/organization/OrganizationSettingsEditorPage";
import { OrganizationSettings } from "../pages/organization/OrganizationSettingsPage";
import { PrivacyCenterDetail } from "../pages/organization/PrivacyCenterDetailPage";
import { UserSettings } from "../pages/organization/UserSettingsPage";
import { PrintPrivacyNoticeReview } from "../pages/print/PrintPrivacyNoticeReviewPage";
import { LookupPage } from "../pages/records/LookupPage";
import { RequestResultsPage } from "../pages/records/RequestResultsPage";
import { CGInstructionsPage } from "../pages/requestInstructions/CGInstructionsPage";
import { DeleteInstructionsPage } from "../pages/requestInstructions/DeleteInstructionsPage";
import { KnowInstructionsPage } from "../pages/requestInstructions/KnowInstructionsPage";
import { OptOutInstructionsPage } from "../pages/requestInstructions/OptOutInstructionsPage";
import { RequestInstructionsHome } from "../pages/requestInstructions/RequestInstructionsHome";
import { Detail as RequestDetail } from "../pages/requests/RequestDetailPage";
import { Listing as RequestListing } from "../pages/requests/RequestListingPage";
import { DataRecipientAddDetail } from "../pages/stay-compliant/DataRecipientAddDetailPage";
import { DataRecipientAdd } from "../pages/stay-compliant/DataRecipientAddPage";
import { DataRecipientDetail } from "../pages/stay-compliant/DataRecipientDetailPage";
import { AddEmployeeDataRecipientsPage } from "../pages/data-map/AddEmployeeDataRecipientsPage";
import { DataRecipientEdit } from "../pages/stay-compliant/DataRecipientEditPage";
import { StayCompliantCookiesAndOptOut } from "../pages/stay-compliant/StayCompliantCookiesAndOptOutPage";
import { DataRecipients } from "../pages/stay-compliant/StayCompliantDataRecipientsPage";
import { AddEditPrivacySectionPage } from "../pages/stay-compliant/StayCompliantNoticesAddEditPrivacySectionPage";
import { PrivacyCenter as StayCompliantNotices } from "../pages/stay-compliant/StayCompliantPrivacyCenterPage";
import { Listing as TaskListing } from "../pages/tasks/TaskListingPage";
import { Catalog as VendorCatalog } from "../pages/vendors/VendorCatalogPage";
import { Detail as VendorDetail } from "../pages/vendors/VendorDetailPage";
import { New as NewVendor } from "../pages/vendors/VendorNewPage";
import { DesignRoutes } from "./DesignRoutes";
import { FourOhFourRoutes } from "./FourOhFourRoutes";
import { Routes } from "./routes";
import { SurveyRoutes } from "./SurveyRoutes";
import { StayCompliantDataRetentionPage } from "../components/privacyCenter/StayCompliantDataRetentionPage";
import { StayCompliantIntroEditScreen } from "../components/privacyCenter/StayCompliantIntroEditScreen";
import { KnowledgeCenterPage } from "../pages/stay-compliant/KnowledgeCenterPage";
import { DataRecipientAssociationForGroupPage } from "../pages/data-map/components/workflows/create-collection-group/DataRecipientAssociationForGroupPage";
import { DataSourceAssociationForGroupPage } from "../pages/data-map/components/workflows/create-collection-group/DataSourceAssociationForGroupPage";

export const StayCompliantPageRouter = () => {
  useSetApiMode("Live", "StayCompliantPageRouter");
  return (
    <Switch>
      <Route
        exact
        path={Routes.requests.active}
        key={"active-requests"}
        render={(props) => <RequestListing {...props} filter="active" />}
      />

      <Route
        exact
        path={Routes.requests.closed}
        key={"closed-requests"}
        render={(props) => <RequestListing {...props} filter="complete" />}
      />

      <Route
        exact
        path={Routes.requests.pending}
        key={"pending-requests"}
        render={(props) => <RequestListing {...props} filter="pending" />}
      />

      <Route
        exact
        path={Routes.tasks.todo}
        render={(props) => <TaskListing {...props} filter="todo" />}
      />

      <Route
        exact
        path={Routes.tasks.completed}
        render={(props) => <TaskListing {...props} filter="completed" />}
      />

      <Route exact path={Routes.requests.detail} component={RequestDetail} />

      <Route exact path={Routes.records.lookup} component={LookupPage} />
      <Route exact path={Routes.records.requestResults} component={RequestResultsPage} />
      <Route exact path={Routes.consumer.detail} component={ConsumerDetailPage} />

      <Route exact path={Routes.dataRecipients.root} component={DataRecipients} />
      <Route exact path={Routes.dataRecipients.add} component={DataRecipientAdd} />
      <Route exact path={Routes.dataRecipients.edit} component={DataRecipientEdit} />
      <Route
        exact
        path={Routes.dataRecipients.addCustom}
        // @ts-ignore
        render={(props) => <DataRecipientAddDetail {...props} custom={true} />}
      />
      <Route exact path={Routes.dataRecipients.addDetail} component={DataRecipientAddDetail} />
      <Route exact path={Routes.dataRecipients.detail} component={DataRecipientDetail} />

      <Route exact path={Routes.vendors.catalog} component={VendorCatalog} />
      <Route exact path={Routes.vendors.new} component={NewVendor} />
      <Route exact path={Routes.vendors.detail} component={VendorDetail} />

      <Route
        exact
        path={Routes.organization.settings.edit}
        component={OrganizationSettingsEditorPage}
      />
      <Route exact path={Routes.organization.settings.root} component={OrganizationSettings} />

      <Route exact path={Routes.organization.settings.users} component={UserSettings} />
      <Route
        exact
        path={Routes.organization.settings.templates.index}
        component={EmailTemplateSettings}
      />
      <Route
        exact
        path={Routes.organization.settings.templates.detail}
        component={EditEmailTemplate}
      />
      <Route exact path={Routes.organization.settings.inbox} component={EmailSettings} />
      <Route exact path={Routes.organization.settings.center} component={PrivacyCenterDetail} />
      <Route
        exact
        path={Routes.organization.settings.integrations}
        component={IntegrationsSettingsPage}
      />

      {/*
      <Route
        exact
        path={Routes.organization.privacyCenters}
        component={PrivacyCentersListing}
      />

      <Route
        exact
        path={Routes.organization.privacyCenter.new}
        component={PrivacyCenterDetail}
      />

      <Route
        exact
        path={Routes.organization.privacyCenter.detail}
        component={PrivacyCenterDetail}
      /> */}

      <Route
        exact
        path={Routes.organization.requestHandling.base}
        component={RequestInstructionsHome}
      />

      <Route
        exact
        path={Routes.organization.requestHandling.consumerGroup}
        component={CGInstructionsPage}
      />

      <Route
        exact
        path={Routes.organization.requestHandling.requestToKnow}
        component={KnowInstructionsPage}
      />

      <Route
        exact
        path={Routes.organization.requestHandling.requestToDelete}
        component={DeleteInstructionsPage}
      />

      <Route
        exact
        path={Routes.organization.requestHandling.requestToOptOut}
        component={OptOutInstructionsPage}
      />

      {/*<Route
        exact
        path={Routes.organization.requestHandling.instructions}
        component={RequestHandlingInstructions}
      />*/}

      <Route exact path={Routes.dataInventory.collection.root} component={StayCompliantDataMap} />
      <Route exact path={Routes.dataMap.root} component={DataMapOverviewPage} />

      <Route
        exact
        path={Routes.dataMap.dataRecipient.employee}
        component={AddEmployeeDataRecipientsPage}
      />

      <Route exact path={Routes.dataMap.lawfulBases.detail} component={LawfulBasesDetailPage} />

      <Route exact path={Routes.dataMap.collectionGroup.new} component={CollectionGroupNewPage} />

      <Route
        exact
        path={Routes.dataMap.collectionGroup.workflow.collection}
        component={CollectedPicForGroupPage}
      />

      <Route
        exact
        path={Routes.dataMap.collectionGroup.workflow.dataRecipientAssociation}
        component={DataRecipientAssociationForGroupPage}
      />

      <Route
        exact
        path={Routes.dataMap.collectionGroup.workflow.disclosure}
        component={DisclosuresForGroupPage}
      />

      <Route
        exact
        path={Routes.dataMap.collectionGroup.workflow.sourceAssociation}
        component={DataSourceAssociationForGroupPage}
      />

      <Route
        exact
        path={Routes.dataMap.collectionGroup.workflow.receipt}
        component={DataReceiptForGroupPage}
      />

      <Route
        exact
        path={Routes.dataMap.collectionGroup.detail}
        component={CollectionGroupDetailPage}
      />

      <Route exact path={Routes.dataMap.dataRecipient.new} component={DataRecipientNewPage} />
      <Route
        exact
        path={Routes.dataMap.dataRecipient.workflow.classification}
        component={ClassificationForDataRecipientPage}
      />
      <Route
        exact
        path={Routes.dataMap.dataRecipient.workflow.groupAssociation}
        component={CollectionGroupAssociationForDataRecipientPage}
      />
      <Route
        exact
        path={Routes.dataMap.dataRecipient.workflow.disclosure}
        component={DisclosureForDataRecipientPage}
      />

      <Route exact path={Routes.dataMap.dataRecipient.detail} component={DataRecipientDetailPage} />

      <Route exact path={Routes.dataMap.dataSource.new} component={DataSourceNewPage} />
      <Route
        exact
        path={Routes.dataMap.dataSource.workflow.groupAssociation}
        component={CollectionGroupAssociationPage}
      />
      <Route
        exact
        path={Routes.dataMap.dataSource.workflow.receipt}
        component={DataReceiptForSourcePage}
      />

      <Route exact path={Routes.dataMap.dataSource.detail} component={DataSourceDetailPage} />

      <Route
        exact
        path={Routes.dataInventory.disclosure.root}
        component={PersonalInformationDisclosed}
      />

      <Route exact path={Routes.dataInventory.sale.root} component={PersonalInformationSold} />

      <Route
        exact
        path={Routes.dataInventory.collection.detail}
        component={StayCompliantDataMapDetail}
      />

      <Route
        exact
        path={Routes.dataInventory.sources.root}
        component={StayCompliantCollectionSources}
      />

      <Route
        exact
        path={Routes.dataInventory.disclosure.detail}
        component={PersonalInformationDisclosedSettings}
      />

      <Route
        exact
        path={Routes.dataInventory.sale.detail}
        component={PersonalInformationSoldSettings}
      />

      <Route exact path={Routes.dataInventory.ccpaDisclosure} component={CCPADisclosure} />

      <Route exact path={Routes.print.privacyNotices.caPrivacyNotice}>
        <PrintPrivacyNoticeReview noticeType="CA_PRIVACY_NOTICE" />
      </Route>
      <Route exact path={Routes.print.privacyNotices.optOutPrivacyNotice}>
        <PrintPrivacyNoticeReview noticeType="OPT_OUT_PRIVACY_NOTICE" />
      </Route>
      <Route exact path={Routes.print.privacyNotices.consumerGroup}>
        <PrintPrivacyNoticeReview noticeType="CONSUMER_GROUP" />
      </Route>

      <Route exact path={Routes.cookies.root} component={StayCompliantCookiesAndOptOut} />

      <Route exact path={Routes.privacyCenter.root} component={StayCompliantNotices} />
      <Route
        exact
        path={Routes.privacyCenter.editPrivacyIntro}
        component={StayCompliantIntroEditScreen}
      />

      <Route
        exact
        path={Routes.privacyCenter.dataRetention}
        component={StayCompliantDataRetentionPage}
      />
      <Route
        exact
        path={Routes.privacyCenter.financialIncentives}
        component={StayCompliantFinancialIncentivesPage}
      />

      <Route
        exact
        path={Routes.privacyCenter.publish}
        component={StayCompliantPublishNoticesPage}
      />
      <Route
        exact
        path={Routes.privacyCenter.addPrivacySection}
        component={AddEditPrivacySectionPage}
        // @ts-ignore
        render={(props) => <AddEditPrivacySectionPage {...props} add={true} />}
      />
      <Route
        exact
        path={Routes.privacyCenter.editPrivacySection}
        component={AddEditPrivacySectionPage}
        // @ts-ignore
        render={(props) => <AddEditPrivacySectionPage {...props} add={false} />}
      />

      <Route exact path={Routes.notices.root}>
        <Redirect to={Routes.privacyCenter.root} />
      </Route>
      <Route exact path={Routes.notices.editPrivacyIntro}>
        <Redirect to={Routes.privacyCenter.editPrivacyIntro} />
      </Route>
      <Route exact path={Routes.notices.dataRetention}>
        <Redirect to={Routes.privacyCenter.dataRetention} />
      </Route>
      <Route exact path={Routes.notices.financialIncentives}>
        <Redirect to={Routes.privacyCenter.financialIncentives} />
      </Route>
      <Route exact path={Routes.notices.publish}>
        <Redirect to={Routes.privacyCenter.publish} />
      </Route>
      <Route exact path={Routes.notices.addPrivacySection}>
        <Redirect to={Routes.privacyCenter.addPrivacySection} />
      </Route>
      <Route exact path={Routes.notices.editPrivacySection}>
        <Redirect to={Routes.privacyCenter.editPrivacySection} />
      </Route>

      <Route exact path={Routes.knowledgeCenter.root} component={KnowledgeCenterPage} />

      <Route exact path={Routes.invite} component={AcceptInvitation} />

      <Route exact path={Routes.root}>
        <Redirect to={Routes.requests.active} />
      </Route>

      <Route exact path={Routes.login}>
        <Redirect to={Routes.root} />
      </Route>

      <Route exact path={Routes.auth.reset}>
        <Redirect to={Routes.root} />
      </Route>

      <Route exact path={Routes.auth.set}>
        <Redirect to={Routes.root} />
      </Route>

      <Route path={Routes.setup.root}>
        <Redirect to={Routes.root} />
      </Route>

      <Route path={Routes.getCompliant.root}>
        <Redirect to={Routes.root} />
      </Route>

      <SurveyRoutes />

      {/* Design Routes */}
      <DesignRoutes />
      <FourOhFourRoutes />
    </Switch>
  );
};
