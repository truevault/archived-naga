// note: this needs to stay in sync with the server side routes as defined
// in AppController.kt

import QueryString from "qs";

const rep = (original: string, params: any) => {
  return original
    .replace(":id", params.id)
    .replace(":orgId", params.orgId)
    .replace(":organizationId", params.organizationId)
    .replace(":email", params.email)
    .replace(":requestId", params.requestId)
    .replace(":vendorId", params.vendorId)
    .replace(":mailbox", params.mailbox)
    .replace(":privacyCenterId", params.privacyCenterId)
    .replace(":regulationSlug", params.regulationSlug)
    .replace(":tabId", params.tabId)
    .replace(":dataRequestTypeId", params.dataRequestTypeId)
    .replace(":dataSubjectTypeId", params.dataSubjectTypeId)
    .replace(":groupId", params.groupId)
    .replace(":consumerGroupId", params.consumerGroupId)
    .replace(":pageIndex", params.pageIndex)
    .replace(":category", params.category)
    .replace(":sectionId", params.sectionId)
    .replace(":introId", params.introId)
    .replace(":consumerId", params.consumerId)
    .replace(":dataSourceId", params.dataSourceId)
    .replace(":dataRecipientId", params.dataRecipientId);
};

export const Routes = {
  root: "/",
  login: "/login",
  reset: "/reset",
  auth: {
    login: "/login",
    reset: "/reset",
    set: "/reset/:resetToken",
  },
  design: {
    root: "/design",
    atom: "/design/atoms",
    molecules: "/design/molecules",
    organisms: "/design/organisms",
  },
  invite: "/invite/:organizationId",
  requests: {
    active: "/requests/active",
    closed: "/requests/closed",
    pending: "/requests/pending",
    detail: "/requests/:requestId",
  },
  records: {
    lookup: "/records/lookup",
    requestResults: "/records/results",
  },
  consumer: {
    detail: "/consumer",
  },
  tasks: {
    todo: "/tasks/todo",
    completed: "/tasks/completed",
  },
  privacyCenter: {
    root: "/privacy-center",
    financialIncentives: "/privacy-center/financial-incentives",
    dataRetention: "/privacy-center/data-retention",
    publish: "/privacy-center/publish",
    editPrivacySection: "/privacy-center/privacy-sections/:sectionId",
    addPrivacySection: "/privacy-center/privacy-sections/add",
    editPrivacyIntro: "/privacy-center/privacy-intro/:introId",
  },
  notices: {
    root: "/notices",
    financialIncentives: "/notices/financial-incentives",
    dataRetention: "/notices/data-retention",
    publish: "/notices/publish",
    editPrivacySection: "/notices/privacy-sections/:sectionId",
    addPrivacySection: "/notices/privacy-sections/add",
    editPrivacyIntro: "/notices/privacy-intro/:introId",
  },
  knowledgeCenter: {
    root: "/knowledge-center",
  },
  cookies: {
    root: "/cookies",
  },
  dataRecipients: {
    root: "/data-recipients",
    add: "/data-recipients/add",
    addCustom: "/data-recipients/add/custom",
    addThirdParty: "/data-recipients/add/custom?thirdParty=1",
    addDetail: "/data-recipients/add/:vendorId",
    detail: "/data-recipients/:vendorId",
    edit: "/data-recipients/:vendorId/edit",
  },
  vendors: {
    root: "/vendors",
    catalog: "/vendors/catalog",
    new: "/vendors/new",
    detail: "/vendors/:vendorId",
  },
  dataMap: {
    root: "/data-map",
    lawfulBases: {
      detail: "/data-map/lawful-bases",
    },
    collectionGroup: {
      detail: "/data-map/collection/:dataSubjectTypeId",
      new: "/data-map/collection/new",
      workflow: {
        collection: "/data-map/collection/new/:dataSubjectTypeId/collection",
        dataRecipientAssociation: "/data-map/collection/new/:dataSubjectTypeId/association",
        disclosure: "/data-map/collection/new/:dataSubjectTypeId/disclosure",
        sourceAssociation: "/data-map/collection/new/:dataSubjectTypeId/source-association",
        receipt: "/data-map/collection/new/:dataSubjectTypeId/receipt",
      },
    },
    dataRecipient: {
      detail: "/data-map/recipients/:dataRecipientId",
      new: "/data-map/recipients/new",
      employee: "/data-map/employee-data-recipients",
      workflow: {
        classification: "/data-map/recipients/new/:dataRecipientId/classification",
        groupAssociation: "/data-map/recipients/new/:dataRecipientId/association",
        disclosure: "/data-map/recipients/new/:dataRecipientId/disclosure",
      },
    },
    dataSource: {
      detail: "/data-map/sources/:dataSourceId",
      new: "/data-map/sources/new",
      workflow: {
        groupAssociation: "/data-map/sources/new/:dataSourceId/association",
        receipt: "/data-map/sources/new/:dataSourceId/receipt",
      },
    },
  },
  dataInventory: {
    collection: {
      root: "/data-inventory/collection",
      detail: "/data-inventory/collection/:dataSubjectTypeId",
    },
    disclosure: {
      root: "/data-inventory/disclosure",
      detail: "/data-inventory/disclosure/:vendorId",
    },
    sale: {
      root: "/data-inventory/sale",
      detail: "/data-inventory/sale/:vendorId",
    },
    sources: {
      root: "/data-inventory/sources",
    },
    ccpaDisclosure: "/data-inventory/ccpa-disclosure",
  },
  organization: {
    settings: {
      root: "/organization/:organizationId/settings",
      edit: "/organization/:organizationId/settings/edit",
      users: "/organization/:organizationId/settings/users",
      templates: {
        index: "/organization/:organizationId/settings/templates",
        detail: "/organization/:organizationId/settings/templates/:slug",
      },
      inbox: "/organization/:organizationId/settings/inbox",
      center: "/organization/:organizationId/settings/center",
      integrations: "/organization/:organizationId/settings/integrations",
    },
    requestHandling: {
      base: "/organization/requestHandling",
      consumerGroup: "/organization/requestHandling/consumerGroup",
      requestToKnow: "/organization/requestHandling/requestToKnow",
      requestToDelete: "/organization/requestHandling/requestToDelete",
      requestToOptOut: "/organization/requestHandling/requestToOptOut",
      instructions: "/organization/:organizationId/requestHandling/:regulationSlug", // TODO: can this be eliminated yet?
    },
  },

  print: {
    privacyNotices: {
      root: "/print/privacy-notices",
      caPrivacyNotice: "/print/privacy-notices/california-privacy-notice",
      optOutPrivacyNotice: "/print/privacy-notices/opt-out-privacy-notice",
      consumerGroup: "/print/privacy-notices/consumer-group/:collectionGroupId",
    },
  },

  // Setup Routes (optionally performed prior to on-boarding)
  setup: {
    root: "/setup",
    vendors: "/setup/vendors",
  },

  // Survey Routes
  surveys: {
    "2023": {
      root: "/surveys/2023/",
      businessSurvey: "/surveys/2023/business-survey",
      collectionPurposes: "/surveys/2023/collection-purposes",
      corePrivacyConcepts: "/surveys/2023/core-privacy-concepts",
      dataRetention: "/surveys/2023/data-retention",
      dataRecipients: "/surveys/2023/data-recipients",
      tasks: "/surveys/2023/tasks",
    },
    midyear2023: {
      root: "/surveys/midyear-2023/",
      sellingSharing: "/surveys/midyear-2023/selling-and-sharing",
      dataCollection: "/surveys/midyear-2023/data-collection",
      dataRecipients: "/surveys/midyear-2023/data-recipients",
      dataSources: "/surveys/midyear-2023/data-sources",
      websiteAudit: "/surveys/midyear-2023/website-audit",
      nextSteps: "/surveys/midyear-2023/next-steps",
      dataCollectionDetail: "/surveys/midyear-2023/data-collection/:groupId",
    },
  },

  // Get Compliant Phase Routes
  getCompliant: {
    root: "/get-compliant",
    dashboard: "/get-compliant/dashboard",
    businessSurvey: {
      root: "/get-compliant/business",
      consumerDataCollection: "/get-compliant/business/consumer-data-collection",
      consumerDataCollectionForGroup: "/get-compliant/business/consumer-data-collection/:groupId",
    },
    dataMap: {
      introduction: "/get-compliant/data-map/introduction",
      consumerSources: "/get-compliant/data-map/sources",
    },
    employeeMap: {
      hrSurvey: "/get-compliant/employee-map/hr-survey",
      introduction: "/get-compliant/employee-map/introduction",

      employee: "/get-compliant/employee-map/employee",
      employeeCollectionForGroup: "/get-compliant/employee-map/employee/:consumerGroupId",
      employeeAssociationForGroup: "/get-compliant/employee-map/employee/:groupId/association",
      employeeDisclosureForGroup: "/get-compliant/employee-map/employee/:groupId/disclosure",
      employeeReviewForGroup: "/get-compliant/employee-map/employee/:consumerGroupId/review",
      dataRecipients: "/get-compliant/employee-map/data-recipients",
      dataDisclosures: "/get-compliant/employee-map/data-disclosures",
      dataSources: "/get-compliant/employee-map/collection-sources",
      eeaDataSources: "/get-compliant/employee-map/eea-collection-sources",
    },
    dataRecipients: {
      root: "/get-compliant/data-recipients",
      platforms: "/get-compliant/data-recipients/platforms",
      vendorAssociation: "/get-compliant/data-recipients/vendor-association",
      vendorAssociationForGroup: "/get-compliant/data-recipients/vendor-association/:groupId",

      dataDisclosures: "/get-compliant/data-recipients/data-disclosures",
      dataDisclosuresForGroup: "/get-compliant/data-recipients/data-disclosures/:groupId",
      doubleCheck: "/get-compliant/data-recipients/double-check",

      processingRegion: "/get-compliant/data-recipients/processing-region",
      contractors: "/get-compliant/data-recipients/contractors",
      thirdPartyRecipients: "/get-compliant/data-recipients/third-party-recipients",
      selling: "/get-compliant/data-recipients/selling",
      sharing: "/get-compliant/data-recipients/sharing",
      notice: "/get-compliant/data-recipients/notice",
      noticeLimit: "/get-compliant/data-recipients/notice-limit",
      consumerGroups: "/get-compliant/data-recipients/consumer-groups",
      consumerGroup: "/get-compliant/data-recipients/consumer-groups/:consumerGroupId",
      classifyServices: "/get-compliant/data-recipients/classify-services",
      locateOther: "/get-compliant/data-recipients/locate-other",
      reviewVendorContracts: "/get-compliant/data-recipients/review-vendor-contracts",
      classifyOther: "/get-compliant/data-recipients/classify-other",
      contact: "/get-compliant/data-recipients/contact",
      gdpr: {
        confirmProcessors: "/get-compliant/data-recipients/gdpr/confirm-processors",
        findAgreements: "/get-compliant/data-recipients/gdpr/find-agreements",
        contactUnknowns: "/get-compliant/data-recipients/gdpr/contact-unknowns",
      },
    },
    dataCollection: {
      root: "/get-compliant/data-collection",
      consumerCollection: "/get-compliant/data-collection/consumer",
      consumerPurposes: "/get-compliant/data-collection/consumer/purposes",
      corePrivacyConcepts: "/get-compliant/data-collection/consumer/core-privacy-concepts",
      consumerIncentives: "/get-compliant/data-collection/consumer/incentives",
      gdprLawfulBases: "/get-compliant/data-collection/gdpr/lawful-bases",
      consumerNoticeReview: "/get-compliant/data-collection/consumer/review",
      employmentNoticeReview: "/get-compliant/data-collection/employment/review",
      additionalPrivacyLanguage:
        "/get-compliant/data-collection/consumer/additional-privacy-language",
      internationalDataTransfers:
        "/get-compliant/data-recipients/gdpr/international-data-transfers",
      contactProcessors: "/get-compliant/data-recipients/gdpr/contact-processors",
    },
    dataRetention: {
      root: "/get-compliant/data-retention",
      deletionExceptions: "/get-compliant/data-retention/exceptions",
      vendorIntegrations: "/get-compliant/data-retention/integrations",
      retentionPolicy: "/get-compliant/data-retention/retention-policy",
      dataStorage: "/get-compliant/data-retention/data-storage",
      gdprCookieScanner: "/get-compliant/data-retention/cookie-scanner",
      cookiePolicy: "/get-compliant/data-retention/cookie-policy",
    },
    privacyNotices: {
      root: "/get-compliant/privacy-notices",
      caPrivacyNotice: "/get-compliant/privacy-notices/california-privacy-notice",
      consumerGroup: "/get-compliant/privacy-notices/consumer-group/:consumerGroupId",
    },
    publishNotices: {
      root: "/get-compliant/publish-notices",
    },
    privacyRequests: {
      root: "/get-compliant/privacy-requests",
      processing:
        "/get-compliant/privacy-requests/:consumerGroupId/request-type/:dataRequestTypeId",
      privacyCenter: "/get-compliant/privacy-requests/privacy-center",
    },
    privacyCenter: {
      emailAccount: "/get-compliant/privacy-center/email-account",
      emailBranding: "/get-compliant/privacy-center/email-branding",
      websiteHeader: "/get-compliant/privacy-center/website-header",
      favicon: "/get-compliant/privacy-center/favicon",
      dataPrivacyOfficer: "/get-compliant/privacy-center/data-privacy-officer",
    },
    complianceChecklist: {
      root: "/get-compliant/compliance-checklist",
      settings: "/get-compliant/compliance-checklist/settings",
      notices: "/get-compliant/compliance-checklist/notices",
    },
    complete: {
      root: "/get-compliant/complete",
    },
    settings: {
      root: "/get-compliant/settings",
      user: "/get-compliant/settings/user",
    },
  },

  // Instructions
  instructions: {
    id: "/instructions/:id",
  },
};

export const frontendPathToUrl = (path: string) => {
  return `${window.location.origin}${path}`;
};

export const RouteHelpers = {
  instructions: {
    id: (id) => rep(Routes.instructions.id, { id }),
  },
  survey: {
    midyear2023: {
      consumerDataCollectionForGroup: (groupId) =>
        rep(Routes.getCompliant.businessSurvey.consumerDataCollectionForGroup, { groupId }),
    },
  },
  getCompliant: {
    vendors: {
      consumerGroup: (consumerGroupId) =>
        rep(Routes.getCompliant.dataRecipients.consumerGroup, { consumerGroupId }),
    },
    dataRecipients: {
      vendorAssociationForGroup: (groupId) =>
        rep(Routes.getCompliant.dataRecipients.vendorAssociationForGroup, { groupId }),
      dataDisclosuresForGroup: (groupId) =>
        rep(Routes.getCompliant.dataRecipients.dataDisclosuresForGroup, { groupId }),
    },
    businessSurvey: {
      consumerDataCollectionForGroup: (groupId) =>
        rep(Routes.getCompliant.businessSurvey.consumerDataCollectionForGroup, { groupId }),
    },
    employeeMap: {
      employeeCollectionForGroup: (consumerGroupId) =>
        rep(Routes.getCompliant.employeeMap.employeeCollectionForGroup, { consumerGroupId }),
      employeeAssociationForGroup: (groupId) =>
        rep(Routes.getCompliant.employeeMap.employeeAssociationForGroup, { groupId }),
      employeeDisclosureForGroup: (groupId) =>
        rep(Routes.getCompliant.employeeMap.employeeDisclosureForGroup, { groupId }),
      employeeNoticeReviewForGroup: (consumerGroupId) =>
        rep(Routes.getCompliant.employeeMap.employeeReviewForGroup, { consumerGroupId }),
    },
    privacyRequests: {
      processing: (consumerGroupId, dataRequestTypeId) =>
        rep(Routes.getCompliant.privacyRequests.processing, {
          consumerGroupId,
          dataRequestTypeId,
        }),
    },
    privacyNotices: {
      consumerGroup: (consumerGroupId) =>
        rep(Routes.getCompliant.privacyNotices.consumerGroup, { consumerGroupId }),
    },
  },
  print: {
    privacyNotices: {
      consumerGroup: (consumerGroupId) =>
        rep(Routes.print.privacyNotices.consumerGroup, { consumerGroupId }),
    },
  },
  organization: {
    settings: (organizationId) => rep(Routes.organization.settings.root, { organizationId }),
    editSettings: (organizationId) => rep(Routes.organization.settings.edit, { organizationId }),
    users: (organizationId) => rep(Routes.organization.settings.users, { organizationId }),
    inbox: (organizationId) => rep(Routes.organization.settings.inbox, { organizationId }),
    center: (organizationId) => rep(Routes.organization.settings.center, { organizationId }),
    requestHandling: {
      base: (organizationId) => rep(Routes.organization.requestHandling.base, { organizationId }),
      instructions: (organizationId, regulationSlug) =>
        rep(Routes.organization.requestHandling.instructions, {
          organizationId,
          regulationSlug,
        }),
    },
  },
  invite: (organizationId) => rep(Routes.invite, { organizationId }),
  vendors: {
    detail: (vendorId) => rep(Routes.vendors.detail, { vendorId }),
  },
  privacyCenter: {
    editPrivacySection: (sectionId) => rep(Routes.privacyCenter.editPrivacySection, { sectionId }),
    editPrivacyIntro: (introId) => rep(Routes.privacyCenter.editPrivacyIntro, { introId }),
  },
  dataRecipients: {
    addDetail: (vendorId) => rep(Routes.dataRecipients.addDetail, { vendorId }),
    detail: (vendorId) => rep(Routes.dataRecipients.detail, { vendorId }),
    edit: (vendorId) => rep(Routes.dataRecipients.edit, { vendorId }),
  },
  requests: {
    detail: (requestId) => rep(Routes.requests.detail, { requestId }),
  },
  records: {
    lookup: () => Routes.records.lookup,
    requestsResults: (params) =>
      `${Routes.records.requestResults}?${QueryString.stringify(params)}`,
  },
  consumer: {
    detail: (consumerId) => `${Routes.consumer.detail}?consumerId=${consumerId}`,
  },
  dataMap: {
    collectionGroup: {
      detail: (dataSubjectTypeId) =>
        rep(Routes.dataMap.collectionGroup.detail, {
          dataSubjectTypeId,
        }),
      workflow: {
        collection: (dataSubjectTypeId) =>
          rep(Routes.dataMap.collectionGroup.workflow.collection, {
            dataSubjectTypeId,
          }),
        dataRecipientAssociation: (dataSubjectTypeId) =>
          rep(Routes.dataMap.collectionGroup.workflow.dataRecipientAssociation, {
            dataSubjectTypeId,
          }),
        disclosure: (dataSubjectTypeId) =>
          rep(Routes.dataMap.collectionGroup.workflow.disclosure, {
            dataSubjectTypeId,
          }),
        sourceAssociation: (dataSubjectTypeId) =>
          rep(Routes.dataMap.collectionGroup.workflow.sourceAssociation, {
            dataSubjectTypeId,
          }),
        receipt: (dataSubjectTypeId) =>
          rep(Routes.dataMap.collectionGroup.workflow.receipt, {
            dataSubjectTypeId,
          }),
      },
    },

    dataRecipient: {
      detail: (dataRecipientId) =>
        rep(Routes.dataMap.dataRecipient.detail, {
          dataRecipientId,
        }),
      workflow: {
        classification: (dataRecipientId) =>
          rep(Routes.dataMap.dataRecipient.workflow.classification, {
            dataRecipientId,
          }),
        groupAssociation: (dataRecipientId) =>
          rep(Routes.dataMap.dataRecipient.workflow.groupAssociation, {
            dataRecipientId,
          }),
        disclosure: (dataRecipientId) =>
          rep(Routes.dataMap.dataRecipient.workflow.disclosure, {
            dataRecipientId,
          }),
      },
    },
    dataSource: {
      detail: (dataSourceId) =>
        rep(Routes.dataMap.dataSource.detail, {
          dataSourceId,
        }),
      workflow: {
        sourceAssociation: (dataSourceId) =>
          rep(Routes.dataMap.dataSource.workflow.groupAssociation, {
            dataSourceId,
          }),
        receipt: (dataSourceId) =>
          rep(Routes.dataMap.dataSource.workflow.receipt, {
            dataSourceId,
          }),
      },
    },
  },
  dataInventory: {
    collection: {
      detail: (dataSubjectTypeId) =>
        rep(Routes.dataInventory.collection.detail, {
          dataSubjectTypeId,
        }),
    },
    disclosure: {
      detail: (vendorId) =>
        rep(Routes.dataInventory.disclosure.detail, {
          vendorId,
        }),
    },
    sale: {
      detail: (vendorId) =>
        rep(Routes.dataInventory.sale.detail, {
          vendorId,
        }),
    },
  },
};

export const RouteMatches = {
  getCompliant: {
    employeeMap: {
      employeeCollectionForGroup: new RegExp(
        `${Routes.getCompliant.employeeMap.employeeCollectionForGroup.replace(
          ":consumerGroupId",
          "([0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12})",
        )}$`,
      ),
      employeeReviewForGroup: new RegExp(
        `${Routes.getCompliant.employeeMap.employeeReviewForGroup.replace(
          ":consumerGroupId",
          "([0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12})",
        )}$`,
      ),
    },
  },
};
