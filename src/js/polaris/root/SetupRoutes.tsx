import React from "react";
import { Redirect } from "react-router";
import { Route } from "react-router-dom";
import { Routes } from "./routes";
import { AddDataRecipients } from "../pages/get-compliant/data-recipients/AddDataRecipients";
import { usePrimaryOrganization } from "../hooks";
import { Phase } from "../util/Phase";

type SetupRoutesProps = {
  phase: Phase;
};

export const SetupRoutes = ({ phase }: SetupRoutesProps) => {
  const [org] = usePrimaryOrganization();
  return (
    <>
      <Route exact path={Routes.setup.vendors}>
        <AddDataRecipients phase="SETUP" currentProgress={org.getCompliantProgress} />
      </Route>
      <Route path={Routes.setup.root}>
        <Redirect to={phase === "SETUP" ? Routes.setup.vendors : Routes.getCompliant.root} />
      </Route>
    </>
  );
};
