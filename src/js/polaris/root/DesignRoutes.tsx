import React from "react";
import { Redirect, Route } from "react-router-dom";
import { __ENVIRONMENT__ } from "../globals";
import { Atoms } from "../pages/design/Atoms";
import { Molecules } from "../pages/design/Molecules";
import { Organisms } from "../pages/design/Organisms";
import { Routes } from "./routes";

export const DesignRoutes = () => {
  if (__ENVIRONMENT__ === "production") {
    return null;
  }

  return (
    <>
      <Route exact path={Routes.design.atom} render={Atoms} />
      <Route exact path={Routes.design.molecules} render={Molecules} />
      <Route exact path={Routes.design.organisms} render={Organisms} />
      <Route exact path={Routes.design.root}>
        <Redirect to={Routes.design.atom} />
      </Route>
    </>
  );
};
