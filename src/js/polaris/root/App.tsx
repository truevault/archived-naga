import { hot } from "react-hot-loader/root";
import MomentUtils from "@date-io/moment";
import CancelIcon from "@mui/icons-material/Cancel";
import AdapterMoment from "@mui/lab/AdapterMoment";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { CssBaseline, IconButton } from "@mui/material";
import { ErrorBoundary as SentryErrorBoundary } from "@sentry/react";
import { SnackbarProvider } from "notistack";
import React from "react";
import { Provider } from "react-redux";
import { snackbarAnchor } from "../../common/components/SnackbarConfig";
import { store } from "../../common/root/redux";
import { AppSelector } from "./AppSelector";
import { Router, Route } from "react-router-dom";
import { QueryParamProvider } from "use-query-params";
import { Query, QueryClient } from "@tanstack/react-query";
import {
  PersistQueryClientProvider,
  removeOldestQuery,
} from "@tanstack/react-query-persist-client";
import { createSyncStoragePersister } from "@tanstack/query-sync-storage-persister";
import { createBrowserHistory } from "history";

import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import { __IS_DEVELOPMENT__, __ENVIRONMENT__, __SENTRY_DSN__, __SHA_VERSION__ } from "../globals";

const history = createBrowserHistory();

Sentry.init({
  dsn: __SENTRY_DSN__,
  integrations: [
    new Integrations.BrowserTracing({
      tracePropagationTargets: [
        "localhost",
        "polaris.truevault.com",
        "polaris.truevaultstaging.com",
        /^\//,
      ],
      routingInstrumentation: Sentry.reactRouterV5Instrumentation(history),
    }),
  ],
  tracesSampleRate: 0.3,
  normalizeDepth: 5, // Used for redux state tree depth
  release: __SHA_VERSION__,
  environment: __ENVIRONMENT__,
});

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      cacheTime: 1000 * 60 * 60 * 24, // 24 hours
      refetchOnWindowFocus: false,
    },
  },
});

const localStoragePersister = createSyncStoragePersister({
  storage: window.localStorage,
  retry: removeOldestQuery,
  key: "polarisData",
  throttleTime: 1000,
  serialize: (data) => JSON.stringify(data),
  deserialize: (data) => JSON.parse(data),
});

const shouldDehydrateQuery = (query: Query): boolean => {
  return query.state.status == "success" && query.queryKey[0] != "dataMap";
};

export const App = hot(() => {
  return (
    <AppWrapper>
      <AppSelector />
    </AppWrapper>
  );
});

export default hot(Sentry.withProfiler(App));

const ErrorFallback = () => {
  return (
    <div className="error-container">
      <div
        className="error-illustration"
        style={{ backgroundImage: `url('/assets/images/error/500.svg')` }}
      >
        <div className="error-message message-narrow">
          <h2>Something went wrong, but it's not your fault.</h2>
          <p>This error has been logged.</p>
          <a href="/" className="btn">
            Go Home
          </a>
        </div>
      </div>
    </div>
  );
};

const AppWrapper = ({ children }) => {
  const notistackRef = React.createRef<any>();
  const onClickDismiss = (key) => () => {
    notistackRef.current?.closeSnackbar(key);
  };

  const ErrorBoundary = __IS_DEVELOPMENT__ ? DevelopmentErrorBoundary : SentryErrorBoundary;

  return (
    <>
      <CssBaseline />

      <ErrorBoundary fallback={ErrorFallback} showDialog>
        <PersistQueryClientProvider
          client={queryClient}
          persistOptions={{
            persister: localStoragePersister,
            dehydrateOptions: {
              shouldDehydrateQuery,
            },
          }}
        >
          <Provider store={store}>
            <SnackbarProvider
              ref={notistackRef}
              anchorOrigin={snackbarAnchor}
              action={(key) => (
                <IconButton
                  onClick={onClickDismiss(key)}
                  color="inherit"
                  title="Dismiss"
                  size="small"
                >
                  <CancelIcon />
                </IconButton>
              )}
            >
              <Router history={history}>
                <QueryParamProvider ReactRouterRoute={Route}>
                  <LocalizationProvider dateAdapter={AdapterMoment} utils={MomentUtils}>
                    {children}
                  </LocalizationProvider>
                </QueryParamProvider>
              </Router>
            </SnackbarProvider>
          </Provider>
        </PersistQueryClientProvider>
      </ErrorBoundary>
    </>
  );
};

// This is useful in local where the SentryErrorBoundary will just re-render, causing infinite request loops in local.
class DevelopmentErrorBoundary extends React.Component<
  { showDialog: boolean; fallback: () => React.ReactNode },
  { hasError: boolean }
> {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(_error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    console.error("An error was caught by the fallback error boundary: ", error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return this.props.fallback();
    }

    return this.props.children;
  }
}
