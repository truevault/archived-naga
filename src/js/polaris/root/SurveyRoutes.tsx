import React from "react";
import { Redirect } from "react-router";
import { Route } from "react-router-dom";

import { Survey2023Q1BusinessSurveyPage } from "../pages/surveys/2023-q1/BusinessSurveyPage";
import { Survey2023Q1ConsumerCollectionPurposes } from "../pages/surveys/2023-q1/CollectionPurposesPage";
import { Survey2023Q1CorePrivacyConceptsPage } from "../pages/surveys/2023-q1/CorePrivacyConceptsPage";
import { Survey2023Q1DataRecipientsPage } from "../pages/surveys/2023-q1/DataRecipientsPage";
import { Survey2023Q1DataRetentionPage } from "../pages/surveys/2023-q1/DataRetentionPage";
import { Survey2023Q1TasksPage } from "../pages/surveys/2023-q1/TasksPage";

import { Survey2023Q3SellingSharingPage } from "../pages/surveys/2023-q3/SellingSharingPage";
import { Survey2023Q3DataCollectionPage } from "../pages/surveys/2023-q3/DataCollectionPage";
import { Survey2023Q3DataRecipientsPage } from "../pages/surveys/2023-q3/DataRecipientsPage";
import { Survey2023Q3DataSourcesPage } from "../pages/surveys/2023-q3/DataSourcesPage";
import { Survey2023Q3WebsiteAuditPage } from "../pages/surveys/2023-q3/WebsiteAuditPage";
import { Survey2023Q3NextStepsPage } from "../pages/surveys/2023-q3/NextStepsPage";

import { Routes } from "./routes";

export const SurveyRoutes = () => {
  return (
    <>
      <Route exact path={Routes.surveys["2023"].businessSurvey}>
        <Survey2023Q1BusinessSurveyPage />
      </Route>
      <Route exact path={Routes.surveys["2023"].collectionPurposes}>
        <Survey2023Q1ConsumerCollectionPurposes />
      </Route>
      <Route exact path={Routes.surveys["2023"].corePrivacyConcepts}>
        <Survey2023Q1CorePrivacyConceptsPage />
      </Route>
      <Route exact path={Routes.surveys["2023"].dataRetention}>
        <Survey2023Q1DataRetentionPage />
      </Route>
      <Route exact path={Routes.surveys["2023"].dataRecipients}>
        <Survey2023Q1DataRecipientsPage />
      </Route>
      <Route exact path={Routes.surveys["2023"].tasks}>
        <Survey2023Q1TasksPage />
      </Route>
      <Route exact path={Routes.surveys["2023"].root}>
        <Redirect to={Routes.surveys["2023"].businessSurvey} />
      </Route>
      <Route exact path={Routes.surveys.midyear2023.sellingSharing}>
        <Survey2023Q3SellingSharingPage />
      </Route>
      <Route exact path={Routes.surveys.midyear2023.dataCollection}>
        <Survey2023Q3DataCollectionPage />
      </Route>
      <Route exact path={Routes.surveys.midyear2023.dataRecipients}>
        <Survey2023Q3DataRecipientsPage />
      </Route>
      <Route exact path={Routes.surveys.midyear2023.dataSources}>
        <Survey2023Q3DataSourcesPage />
      </Route>
      <Route exact path={Routes.surveys.midyear2023.websiteAudit}>
        <Survey2023Q3WebsiteAuditPage />
      </Route>
      <Route exact path={Routes.surveys.midyear2023.nextSteps}>
        <Survey2023Q3NextStepsPage />
      </Route>
      <Route exact path={Routes.surveys.midyear2023.root}>
        <Redirect to={Routes.surveys.midyear2023.sellingSharing} />
      </Route>
    </>
  );
};
