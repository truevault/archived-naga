import React from "react";
import { Route } from "react-router-dom";

export const FourOhFourRoutes = () => {
  return (
    <>
      <Route exact path="/404" component={FourOhFour} />
      <Route component={FourOhFour} />
    </>
  );
};

const FourOhFour = () => {
  return <h1>404: page not found</h1>;
};
