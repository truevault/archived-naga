import _ from "lodash";
import { useMemo } from "react";
import {
  useAllCollectedPICByCollectionGroup,
  usePendingReceivedPICByVendorId,
  useReceivedPICByVendorId,
} from "../../common/hooks/dataMapHooks";
import { CollectionGroupDetailsDto } from "../../common/service/server/dto/CollectionGroupDto";
import { DataMapOperation } from "../../common/service/server/dto/DataMapDto";
import { CollectionContext } from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationPublicId } from "../../common/service/server/Types";
import { dataSourceSort } from "../surveys/util";
import { useDataMap } from "./useDataMap";
import { useOrgDataSources } from "./useOrganizationVendors";

export const useReceiptsForCollectionGroups = (
  orgId: OrganizationPublicId,
  collectionGroups: CollectionGroupDetailsDto[],
  collectionContext: CollectionContext[] = ["CONSUMER"],
) => {
  const [dataMap, dataMapRequest, _refresh, _update, _invalidated, v2, updateMutation] =
    useDataMap(orgId);
  const [sources, sourcesRequest] = useOrgDataSources(orgId);

  const relevantGroupIds = useMemo(() => collectionGroups.map((g) => g.id), [collectionGroups]);

  const contextualSources = useMemo(
    () =>
      sources
        ?.filter((r) => _.intersection(r.collectionContext, collectionContext).length > 0)
        ?.sort(dataSourceSort) ?? [],
    [sources, collectionContext],
  );

  const receiptsByVendorId = useReceivedPICByVendorId(dataMap, relevantGroupIds);
  const pendingByVendorId = usePendingReceivedPICByVendorId(v2, relevantGroupIds);
  const collectedPICByCollectionGroup = useAllCollectedPICByCollectionGroup(dataMap);
  const collectedPIC = _.uniqBy(
    collectionGroups
      .map((cg) => collectedPICByCollectionGroup[cg.id])
      .filter((x) => x)
      .flat(),
    (pic) => pic.id,
  );

  const toggleReceipt = ({
    vendorId,
    receivedIds,
    received,
  }: {
    vendorId: string;
    receivedIds: string[];
    received: boolean;
  }) => {
    const operations: DataMapOperation[] = receivedIds.flatMap((receivedId) => {
      const collectionGroupIds = _.map(collectedPICByCollectionGroup, (pic, collectionGroupId) => {
        if (
          relevantGroupIds.includes(collectionGroupId) &&
          pic.some((pg) => pg.id === receivedId)
        ) {
          return collectionGroupId;
        }

        return null;
      }).filter((x) => Boolean(x));

      return collectionGroupIds.map((groupId) => {
        return {
          target: "RECEIVED",
          update: received ? "ADD" : "REMOVE",
          groupId,
          vendorId,
          picId: receivedId,
        };
      });
    });

    updateMutation.mutate({ operations });
  };

  return {
    requests: {
      sourcesRequest,
      dataMapRequest,
    },
    actions: {
      toggleReceipt,
    },
    sources: contextualSources,
    receiptsByVendorId,
    pendingByVendorId,
    collectedPIC,
  };
};
