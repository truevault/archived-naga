import { useEffect } from "react";
import { ApiMode, setApiMode } from "../../common/service/Api";

// NOTE: This is disabled, and will ALWAYS use the live mode unless you run with `npm run transform:dev` instead of `npm run dev`
export const useSetApiMode = (mode: ApiMode, source: string) => {
  useEffect(() => {
    setApiMode(mode, source);
  }, [mode, source]);
};
