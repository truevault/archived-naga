import { useMemo } from "react";
import { useLocation } from "react-router";

export const useQuery = <T extends any>() => {
  const location = useLocation();

  return useMemo(() => {
    const params = new URLSearchParams(location.search);
    return Object.fromEntries(params.entries()) as T;
  }, [location.search]);
};
