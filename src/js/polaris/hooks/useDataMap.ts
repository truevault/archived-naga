import { useMutation, UseMutationResult, useQueryClient } from "@tanstack/react-query";
import _ from "lodash";
import { useCallback, useMemo } from "react";
import {
  InvalidateFn,
  InvalidationOptions,
  RefreshFn,
  UpdateFn,
  useActionRequest,
  useDataRequest,
} from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models";
import { Api } from "../../common/service/Api";
import {
  DataMapCollectionUpdateOperation,
  DataMapDisclosureUpdateOperation,
  DataMapDto,
  DataMapOperation,
  transformV2ToV1DataMap,
  UpdateDataMapRequest,
  V2DataMapDto,
} from "../../common/service/server/dto/DataMapDto";
import {
  BusinessPurposeDto,
  OrgPersonalInformationSnapshotDto,
  PersonalInformationCollectedDto,
} from "../../common/service/server/dto/PersonalInformationOptionsDto";
import { OrganizationPublicId } from "../../common/service/server/Types";

// NOTE: this preserves the original (legacy) dataMap invalidation implementation.
// TODO: refactor all checkboxes and radio buttons that currently rely on an
//       API refetch to update the UI state (i.e. pessimistic strategy) to instead
//       use an optimistic strategy that mains UI state locally and asyncronously
//       refetches the underlying data using a trailing debounce. this allows
//       for a more responsive user experience while also reducing network requests
const DEFAULT_INVALIDATION: InvalidationOptions = {
  wait: 300,
  leading: true,
};

// organization request
export const useDataMap = (
  organizationId: OrganizationPublicId,
  invalidation: InvalidationOptions = DEFAULT_INVALIDATION,
): [
  DataMapDto | undefined,
  NetworkRequestState,
  RefreshFn,
  UpdateFn,
  InvalidateFn,
  V2DataMapDto | undefined,
  DataMapUpdateMutation,
] => {
  const queryKey = ["dataMap", organizationId];

  const api = useCallback(() => Api.dataMap.getDataMap(organizationId), [organizationId]);

  const { result, refresh, request, invalidate } = useDataRequest({
    api,
    queryKey: queryKey,
    invalidation: invalidation,
  });

  const updateMutation = useDataMapUpdate(organizationId);

  const { fetch: updateCollection } = useActionRequest({
    api: ({ collectionGroupId, id, isCollected }) => {
      return Api.dataMap.updateCollection(organizationId, collectionGroupId, id, isCollected);
    },
    onSuccess: invalidate,
  });

  const v1DataMap = useMemo(() => (result ? transformV2ToV1DataMap(result) : undefined), [result]);

  return [v1DataMap, request, refresh, updateCollection, invalidate, result, updateMutation];
};

export type DataMapUpdateMutation = UseMutationResult<void, unknown, UpdateDataMapRequest, unknown>;
const useDataMapUpdate = (orgId: OrganizationPublicId): DataMapUpdateMutation => {
  const queryKey = ["dataMap", orgId];
  const qc = useQueryClient();
  const mutation = useMutation(
    async (update: UpdateDataMapRequest) => {
      return Api.dataMap.patchDataMap(orgId, update);
    },

    {
      onMutate: (update) => {
        const priorMap = qc.getQueryData(queryKey) as V2DataMapDto;
        const newMap = _.cloneDeep(priorMap);
        addPendingOperations(update.operations, newMap);
        qc.setQueryData(queryKey, newMap);
      },

      onSuccess: (_void, update) => {
        const priorMap = qc.getQueryData(queryKey) as V2DataMapDto;
        const newMap = _.cloneDeep(priorMap);
        if (applyOperations(update.operations, newMap)) {
          qc.invalidateQueries(queryKey);
        }
        removePendingOperations(update.operations, newMap);
        qc.setQueryData(queryKey, newMap);
      },

      onError: (error, update) => {
        const priorMap = qc.getQueryData(queryKey) as V2DataMapDto;
        const newMap = _.cloneDeep(priorMap);
        removePendingOperations(update.operations, newMap);
        qc.setQueryData(queryKey, newMap);
      },
    },
  );

  return mutation;
};

const addPendingOperations = (operations: DataMapOperation[], newMap: V2DataMapDto) => {
  const newOperations = newMap?._local?.operations ?? [];

  // remove any pending duplicate operations, before adding our operations in
  removePendingOperations(operations, newMap);
  newOperations.push(...operations);

  newMap._local = newMap._local ?? {};
  newMap._local.operations = newOperations;
};

const removePendingOperations = (operations: DataMapOperation[], newMap: V2DataMapDto) => {
  const newOperations = newMap?._local?.operations ?? [];
  operations.forEach((op) => {
    const idx = newOperations.findIndex(
      (newOp) =>
        newOp.target == op.target &&
        newOp.groupId == op.groupId &&
        (newOp as DataMapCollectionUpdateOperation).picId ==
          (op as DataMapCollectionUpdateOperation).picId &&
        (newOp as DataMapDisclosureUpdateOperation).vendorId ==
          (op as DataMapDisclosureUpdateOperation).vendorId &&
        (newOp as DataMapDisclosureUpdateOperation).picGroupId ==
          (op as DataMapDisclosureUpdateOperation).picGroupId,
    );

    if (idx >= 0) {
      newOperations.splice(idx, 1);
    }
  });

  newMap._local = newMap._local ?? {};
  newMap._local.operations = newOperations;
};

export const applyOperations = (operations: DataMapOperation[], newMap: V2DataMapDto): boolean => {
  const requireInvalidations = operations.map((op) => {
    if (op.target == "COLLECTION") {
      const idx = newMap.collection.findIndex(
        (c) => c.groupId == op.groupId && c.picId == op.picId,
      );
      if (idx >= 0) {
        newMap.collection.splice(idx, 1);
      }

      if (op.update == "ADD") {
        newMap.collection.push({ groupId: op.groupId, picId: op.picId });
      }

      // we don't need to invalidate, this patch is sufficient
      return false;
    }

    if (op.target == "DISCLOSURE" && op.update == "ADD") {
      const pics = Object.values(newMap.categories).filter(
        (pic) => pic.picGroupId == op.picGroupId,
      );
      pics.forEach((pic) => {
        newMap.disclosure.push({ groupId: op.groupId, picId: pic.id, vendorId: op.vendorId });
      });

      // we need to invalidate if we don't have the vendor as a recipient yet
      return newMap.recipients[op.vendorId] == undefined;
    }
    if (op.target == "DISCLOSURE" && op.update == "REMOVE") {
      const pics = Object.values(newMap.categories).filter(
        (pic) => pic.picGroupId == op.picGroupId,
      );

      pics.forEach((pic) => {
        const idx = newMap.disclosure.findIndex(
          (d) => d.groupId == op.groupId && d.picId == pic.id && d.vendorId == op.vendorId,
        );
        if (idx >= 0) {
          newMap.disclosure.splice(idx, 1);
        }
      });

      // we don't need to invalidate, this patch is sufficient
      return false;
    }

    if (op.target == "RECEIVED") {
      const idx = newMap.received.findIndex(
        (c) => c.groupId == op.groupId && c.picId == op.picId && c.vendorId == op.vendorId,
      );
      if (idx >= 0) {
        newMap.received.splice(idx, 1);
      }

      if (op.update == "ADD") {
        newMap.received.push({ groupId: op.groupId, picId: op.picId, vendorId: op.vendorId });
        // we need to invalidate if we don't have the vendor as a recipient yet
        return newMap.sources[op.vendorId] == undefined;
      }

      return false;
    }
  });

  return requireInvalidations.some((x) => x);
};

export const usePiSnapshot = (
  organizationId: OrganizationPublicId,
): [OrgPersonalInformationSnapshotDto | undefined, NetworkRequestState, () => void] => {
  const piSnapshotApi = useCallback(
    () => Api.dataInventory.getSnapshot(organizationId),
    [organizationId],
  );

  const { result, request, refresh } = useDataRequest({
    api: piSnapshotApi,
    queryKey: ["piSnapshot", organizationId],
  });

  return [result, request, refresh];
};

export const useDefaultCollected = (
  organizationId: OrganizationPublicId,
): [PersonalInformationCollectedDto | undefined, NetworkRequestState, () => void] => {
  const defaultCollectedApi = useCallback(
    () => Api.dataInventory.getDefaultPersonalInformationCollected(organizationId),
    [organizationId],
  );

  const { result, request, refresh } = useDataRequest({
    api: defaultCollectedApi,
    queryKey: ["defaultCollected", organizationId],
  });

  return [result, request, refresh];
};

export const nonEmploymentBusinessPurposes = (purposes: BusinessPurposeDto[]) =>
  purposes.filter((bp) => bp.name !== "Employment-Related");
