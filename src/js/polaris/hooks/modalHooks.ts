import React, { useEffect, useState } from "react";
import { useQueryParam } from "use-query-params";

// state for a modal that should show up only when a specific query param is present
type UseQueryModal = [boolean, React.Dispatch<React.SetStateAction<boolean>>];
export const useQueryModal = (queryParam: string = "modal"): UseQueryModal => {
  return useFlashQueryParam(queryParam, false, (_flashParam: unknown) => true);
};

type UseFlashQueryParam<T> = [T, React.Dispatch<React.SetStateAction<T>>];
export const useFlashQueryParam = <T>(
  queryParam: string,
  def: T,
  parse: (flashParam: unknown) => T,
): UseFlashQueryParam<T> => {
  const [flashParam, setFlashParam] = useQueryParam(queryParam);
  const [value, setValue] = useState<T>(def);

  useEffect(() => {
    if (flashParam) {
      setValue(parse(flashParam));
      setFlashParam(undefined);
    }
  }, [Boolean(flashParam)]);

  return [value, setValue];
};
