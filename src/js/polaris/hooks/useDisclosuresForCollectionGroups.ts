import _ from "lodash";
import { useMemo } from "react";
import { useActionRequest } from "../../common/hooks/api";
import {
  useAllCollectedPICGroups,
  useCollectedPICGroupsByCollectionGroup,
  useDisclosedPICGroupsByVendorId,
} from "../../common/hooks/dataMapHooks";
import { Api } from "../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../common/service/server/dto/CollectionGroupDto";
import { CollectionContext } from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationPublicId, UUIDString } from "../../common/service/server/Types";
import { alphabetically } from "../surveys/util";
import { useDataMap } from "./useDataMap";
import { useOrganizationVendors } from "./useOrganizationVendors";

type ToggleDisclosureParams = {
  vendorId: UUIDString;
  disclosedId: UUIDString;
  disclosed: boolean;
};

export const useDisclosuresForCollectionGroups = (
  orgId: OrganizationPublicId,
  collectionGroups: CollectionGroupDetailsDto[],
  collectionContext: CollectionContext[],
) => {
  const [dataMap, dataMapRequest, _refreshDataMap, _updateDataMap, invalidateDataMap] = useDataMap(
    orgId,
    { wait: 700, trailing: true },
  );
  const [recipients, recipientsRequest] = useOrganizationVendors(orgId);
  const collectionGroupTypes = useMemo(
    () => collectionGroups.map((g) => g.collectionGroupType),
    [collectionGroups],
  );

  const relevantGroupIds = useMemo(() => collectionGroups.map((g) => g.id), [collectionGroups]);

  const contextualRecipients = useMemo(
    () =>
      recipients
        ?.filter((r) => _.intersection(r.collectionContext, collectionContext).length > 0)
        ?.sort(alphabetically) ?? [],
    [recipients, collectionContext],
  );

  const disclosuresByVendorId = useDisclosedPICGroupsByVendorId(dataMap, relevantGroupIds);
  const collectedPicGroups = useAllCollectedPICGroups(dataMap, null, collectionGroupTypes);
  const collectedPicGroupsByCollectionGroup = useCollectedPICGroupsByCollectionGroup(dataMap);

  const { fetch: toggleDisclosure, request: toggleDisclosureRequest } = useActionRequest({
    api: async ({ vendorId, disclosedId, disclosed }: ToggleDisclosureParams) => {
      // Find the Collection Group IDs for each relevant group who collects a
      // PIC within this PIC Group
      const collectionGroupIds = _.map(
        collectedPicGroupsByCollectionGroup,
        (picGroups, collectionGroupId) => {
          if (
            relevantGroupIds.includes(collectionGroupId) &&
            picGroups.some((pg) => pg.id === disclosedId)
          ) {
            return collectionGroupId;
          }

          return null;
        },
      ).filter((x) => Boolean(x));

      // Ensure the CG & recipient are associated if we are
      // disclosing this PIC Group to the data recipient
      if (disclosed) {
        await Promise.all(
          collectionGroupIds.map((groupId) =>
            Api.dataMap.updateRecipientAssociation(orgId, groupId, vendorId, true),
          ),
        );
      }

      // Add to the actual disclosure
      await Promise.all(
        collectionGroupIds.map((groupId) => {
          return Api.dataMap.updateDisclosure(
            orgId,
            groupId,
            vendorId,
            disclosedId,
            disclosed,
            false,
          );
        }),
      );
    },
    onSuccess: invalidateDataMap,
  });

  return {
    requests: {
      recipientsRequest,
      dataMapRequest,
      toggleDisclosureRequest,
    },
    actions: {
      toggleDisclosure,
    },
    dataRecipients: contextualRecipients,
    disclosuresByVendorId,
    collectedPicGroups,
  };
};
