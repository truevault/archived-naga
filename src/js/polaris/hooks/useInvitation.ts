import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { useApiTrigger, useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models";
import { selfActions } from "../../common/reducers/selfSlice";
import { Api } from "../../common/service/Api";
import { UserDetailsDto } from "../../common/service/server/dto/UserDto";
import { OrganizationPublicId, UUIDString } from "../../common/service/server/Types";
import { Error as RequestsError, Success as RequestsSuccess } from "../copy/requests";
import { Routes } from "../root/routes";

type UseInvitation = {
  user: UserDetailsDto;
  actions: {
    acceptInvitation: (v: any) => void;
    setPassword: (v: any) => void;
  };
  requests: {
    userRequest: NetworkRequestState;
    acceptRequest: NetworkRequestState;
    setPasswordRequest: NetworkRequestState;
  };
};

export const useInvitation = (
  organizationId: OrganizationPublicId,
  invitationId: UUIDString,
): UseInvitation => {
  const api = useCallback(() => Api.user.getSelfByInvitationId(invitationId), [invitationId]);
  const { result: user, request: userRequest } = useDataRequest({
    queryKey: ["invitation", organizationId, invitationId],
    api,
    messages: {},
    notifyError: false,
  });

  const [acceptInvitation, acceptRequest] = useAcceptInvitation(organizationId);

  const [setPassword, setPasswordRequest] = useSetPassword(user, organizationId, invitationId);

  return {
    user,
    actions: {
      acceptInvitation,
      setPassword,
    },
    requests: {
      userRequest,
      acceptRequest,
      setPasswordRequest,
    },
  };
};

const useAcceptInvitation = (organizationId) => {
  const dispatch = useDispatch();

  const acceptApi = useCallback(() => Api.user.acceptInvitation(organizationId), [organizationId]);
  const onAcceptDone = useCallback(
    (user) => {
      dispatch(selfActions.set(user as UserDetailsDto));
      window.location.replace(Routes.root);
    },
    [dispatch],
  );

  return useApiTrigger(
    acceptApi,
    onAcceptDone,
    RequestsError.AcceptInvitation,
    RequestsSuccess.AcceptInvitation,
  );
};

const useSetPassword = (
  user: UserDetailsDto | null,
  organizationId: OrganizationPublicId,
  invitationId: UUIDString,
) => {
  const dispatch = useDispatch();

  const setPasswordApi = useCallback(
    (values) =>
      Api.user.activateUser(user?.email, organizationId, (values as any).password, invitationId),
    [organizationId, invitationId, user],
  );
  const onSetPasswordDone = useCallback(
    (user) => {
      dispatch(selfActions.set(user as UserDetailsDto));
      window.location.replace(Routes.root);
    },
    [dispatch],
  );

  return useApiTrigger(
    setPasswordApi,
    onSetPasswordDone,
    RequestsError.AcceptInvitation,
    RequestsSuccess.AcceptInvitation,
  );
};
