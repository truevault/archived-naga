import { useEffect } from "react";
import { useActionRequest, useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models";
import { Api } from "../../common/service/Api";
import { OrganizationSurveyProgressDto } from "../../common/service/server/controller/OrganizationProgressController";
import { OrganizationPublicId, SurveySlug } from "../../common/service/server/Types";
import { useOrganization } from "./useOrganization";

type UseSurveyProgress = {
  progress: OrganizationSurveyProgressDto;
  request: NetworkRequestState;
  refresh: () => void;
  update: (progress: string) => Promise<void>;
  updateRequest: NetworkRequestState;
};

export const useSurveyProgress = (
  organizationId: OrganizationPublicId,
  slug: SurveySlug,
): UseSurveyProgress => {
  const [org] = useOrganization(organizationId);
  const {
    result: progress,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["organizationSurveyProgress", organizationId, slug],
    api: () => Api.organizationProgress.getSurveyProgress(organizationId, slug),
    dependencies: [organizationId, slug],
  });

  // automatically start the survey
  useEffect(() => {
    Api.organizationProgress.startSurvey(organizationId, slug);
    if (org.currentSurvey != slug) {
      Api.organizationProgress.startSurvey(organizationId, slug);
    }
  }, [org.currentSurvey, slug]);

  // update

  const { request: updateRequest, fetch: update } = useActionRequest({
    api: (progress: string) =>
      Api.organizationProgress.updateSurveyProgress(organizationId, slug, progress),
  });

  return {
    progress,
    request,
    refresh,
    update,
    updateRequest,
  };
};
