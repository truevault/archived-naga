import { useCallback } from "react";
import { useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { OrganizationPublicId } from "../../common/service/server/Types";
import { Api } from "../../common/service/Api";
import { WebsiteAuditResultDto } from "../../common/service/server/dto/WebsiteAuditDto";

export const useWebsiteAuditResult = (
  organizationId: OrganizationPublicId,
): [WebsiteAuditResultDto, NetworkRequestState, () => Promise<void>] => {
  const api = useCallback(
    () => Api.websiteAudit.getWebsiteAuditResult(organizationId),
    [organizationId],
  );

  // TODO: not sure how this works
  const { result, request, refresh } = useDataRequest({
    queryKey: ["websiteAuditResult", organizationId],
    api,
    notReady: Boolean(!organizationId),
  });

  return [result, request, refresh];
};
