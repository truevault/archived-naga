import { useEffect } from "react";
import { StringParam, useQueryParam } from "use-query-params";
import { useSnackbar } from "notistack";

export const useParamMessage = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [error, setError] = useQueryParam("error", StringParam);
  const [errorMessage, setErrorMessage] = useQueryParam("error_message", StringParam);
  const [message, setMessage] = useQueryParam("message", StringParam);

  useEffect(() => {
    if (errorMessage) {
      enqueueSnackbar(errorMessage, { variant: "error", persist: true });
      setErrorMessage(undefined);
    }

    if (error) {
      enqueueSnackbar(`There was an error processing your request: ${error}`, {
        variant: "error",
        persist: true,
      });
      setError(undefined);
    }

    if (message) {
      enqueueSnackbar(message, { variant: "success" });
      setMessage(undefined);
    }
  }, [error, errorMessage, message, enqueueSnackbar]);
};
