import { useActionRequest } from "../../../../common/hooks/api";
import { NetworkRequestState } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import {
  AddOrUpdateCustomVendorDto,
  CollectionContext,
  OrganizationDataRecipientDto,
  OrganizationVendorSettingsDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { Error as RequestsError } from "../../../copy/requests";
import { getVendorSettingsObject } from "../../../util/vendors";

export type UpdateVendorFn = (params: UpdateVendorParams) => Promise<OrganizationDataRecipientDto>;

export type UpdateVendorParams = {
  vendorId: UUIDString;
  resetState?: () => void;
  callback?: (v?: any) => void;
} & Partial<OrganizationVendorSettingsDto>;

export const useUpdateVendor = (
  organizationId: UUIDString,
  vendors: OrganizationDataRecipientDto[],
  onSuccess?: () => void,
) => {
  const { fetchWithResult: updateVendor, request: updateVendorRequest } = useActionRequest<
    OrganizationDataRecipientDto,
    UpdateVendorParams
  >({
    api: async ({ vendorId, resetState, callback, ...vendorSettings }: UpdateVendorParams) => {
      const vendor = vendors.find((ov) => ov.vendorId === vendorId);
      const settings = Object.assign(getVendorSettingsObject(vendor), {
        ...vendorSettings,
      });
      try {
        const resp = await Api.organizationVendor.addOrUpdateVendor(
          organizationId,
          vendorId,
          settings,
        );
        if (callback) {
          callback(resp);
        }
        return resp;
      } catch (err) {
        if (resetState) {
          resetState();
        }
        throw err;
      }
    },
    messages: {
      forceError: RequestsError.UpdateVendor,
    },
    allowConcurrent: true,
    onSuccess: onSuccess,
  });

  return { updateVendor, updateVendorRequest };
};

export const useAddCustomVendor = (
  organizationId: UUIDString,
  onSuccess?: (dataRecipient: OrganizationDataRecipientDto) => void,
  collectionContext: CollectionContext[] = ["CONSUMER"],
): [(values: AddOrUpdateCustomVendorDto) => Promise<void>, NetworkRequestState] => {
  const { fetch: addVendor, request: addVendorRequest } = useActionRequest<
    OrganizationDataRecipientDto,
    AddOrUpdateCustomVendorDto
  >({
    api: (values: AddOrUpdateCustomVendorDto) =>
      Api.organizationVendor.addCustomVendor(organizationId, {
        ...values,
        collectionContext: collectionContext,
      }),
    onSuccess,
  });

  return [addVendor, addVendorRequest];
};

export const useRemoveVendor = (
  organizationId: UUIDString,
  onSuccess?: (dataRecipient: Record<string, boolean>) => void,
): [(vendorId: UUIDString) => Promise<void>, NetworkRequestState] => {
  const { fetch: removeVendor, request: removeVendorRequest } = useActionRequest<
    Record<string, boolean>,
    UUIDString
  >({
    api: (vendorId: UUIDString) => Api.organizationVendor.removeVendor(organizationId, vendorId),
    onSuccess,
  });

  return [removeVendor, removeVendorRequest];
};
