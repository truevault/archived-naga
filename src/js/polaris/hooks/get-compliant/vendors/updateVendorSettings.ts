import _ from "lodash";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { getVendorSettingsObject } from "../../../util/vendors";

const calculateVendorSettingsDelta = (desiredSettings, vendors: OrganizationDataRecipientDto[]) => {
  const delta = {};
  for (const key of Object.keys(desiredSettings)) {
    const v = vendors.find((v) => v.vendorId == key);
    const { gdprProcessorSetting: _unused, ...current } = getVendorSettingsObject(v);
    const { gdprProcessorSetting: _alsounused, ...desired } = desiredSettings[key];

    if (!_.isEqual(current, desired)) {
      delta[key] = desired;
    }
  }
  return delta;
};
const applyVendorSettingsDelta = (delta, updateVendor) => {
  const keys = Object.keys(delta);
  for (const key of keys) {
    const settings = delta[key];
    const { gdprProcessorSetting: _unused, ...vendorSettings } = settings;
    updateVendor({ vendorId: key, vendorSettings });
  }
};

export const updateVendorSettings = (desiredSettings, vendors, updateVendor) => {
  const delta = calculateVendorSettingsDelta(desiredSettings, vendors);
  applyVendorSettingsDelta(delta, updateVendor);
};
