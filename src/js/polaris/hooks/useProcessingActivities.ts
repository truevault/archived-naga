import { useCallback, useEffect, useMemo, useState } from "react";
import { useMutation, UseMutationResult, useQueryClient } from "@tanstack/react-query";
import { useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models";
import { Api } from "../../common/service/Api";
import {
  DefProcessingActivityDto,
  LawfulBasis,
  ProcessedPICPending,
  ProcessingActivityDto,
} from "../../common/service/server/dto/ProcessingActivityDto";
import { OrganizationPublicId, UUIDString } from "../../common/service/server/Types";
import {
  CustomActivityCheckboxesProps,
  DefaultActivityCheckboxesProps,
} from "../pages/get-compliant/consumer-collection/ConsumerCollectionGdprLawfulBases";
import { toggleSelection } from "../surveys/util";
import { ProcessingRegion } from "../../common/service/server/dto/OrganizationDataRecipientDto";

export const useDefaultProcessingActivites = (): [
  DefProcessingActivityDto[],
  NetworkRequestState,
] => {
  const api = useCallback(() => Api.processingActivities.getDefaultProcessingActivities(), []);
  const { result: processingActivities, request } = useDataRequest({
    queryKey: ["defaultProcessingActivities"],
    api,
  });
  return [processingActivities, request];
};

export const useProcessingActivites = (
  orgId: OrganizationPublicId,
): [ProcessingActivityDto[], NetworkRequestState, () => Promise<void>, AssociatePicMutation] => {
  const api = useCallback(() => Api.processingActivities.getProcessingActivities(orgId), [orgId]);
  const {
    result: processingActivities,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["processingActivities", orgId],
    api,
  });

  const associatePicMutation = useProcessingActivityUpdateAssociation(orgId);

  return [processingActivities, request, refresh, associatePicMutation];
};

type UpdateProcessingActivityAssociation = {
  activityId: UUIDString;
  picId: UUIDString;
  associate: boolean;
};
export type AssociatePicMutation = UseMutationResult<
  ProcessingActivityDto,
  unknown,
  UpdateProcessingActivityAssociation,
  unknown
>;
const useProcessingActivityUpdateAssociation = (
  orgId: OrganizationPublicId,
): AssociatePicMutation => {
  const qc = useQueryClient();
  const associatePicMutation = useMutation(
    async ({ activityId, picId, associate }: UpdateProcessingActivityAssociation) => {
      if (associate) {
        return Api.processingActivities.associateProcessingActivityAndPic(orgId, activityId, picId);
      } else {
        return Api.processingActivities.unassociateProcessingActivityAndPic(
          orgId,
          activityId,
          picId,
        );
      }
    },

    {
      onMutate: ({ activityId, picId, associate }) => {
        const priorActivities = qc.getQueryData([
          "processingActivities",
          orgId,
        ]) as ProcessingActivityDto[];
        const idx = priorActivities.findIndex((d) => d.id == activityId);

        const oldDto = priorActivities[idx];
        const newDto = { ...oldDto };

        addPendingUpdate({ type: associate ? "add" : "remove", id: picId }, oldDto, newDto);

        const newActivities = [...priorActivities];
        newActivities[idx] = newDto;
        qc.setQueryData(["processingActivities", orgId], newActivities);
      },

      onSuccess: (activity, { picId }) => {
        const priorActivities = qc.getQueryData([
          "processingActivities",
          orgId,
        ]) as ProcessingActivityDto[];
        const idx = priorActivities.findIndex((d) => d.id == activity.id);

        const oldDto = priorActivities[idx];

        removePendingUpdate(picId, oldDto, activity);

        const newActivities = [...priorActivities];
        newActivities[idx] = activity;

        qc.setQueryData(["processingActivities", orgId], newActivities);
      },

      onError: (error, { activityId, picId }) => {
        const priorActivities = qc.getQueryData([
          "processingActivities",
          orgId,
        ]) as ProcessingActivityDto[];
        const idx = priorActivities.findIndex((d) => d.id == activityId);
        const oldDto = priorActivities[idx];
        const newDto = { ...oldDto };

        removePendingUpdate(picId, oldDto, newDto);

        const newActivities = [...priorActivities];
        newActivities[idx] = newDto;
        qc.setQueryData(["processingActivities", orgId], newActivities);
      },
    },
  );

  return associatePicMutation;
};

export const useMandatoryLawfulBasisOptions = (activity?: ProcessingActivityDto) => {
  return useMemo(() => {
    if (!activity) {
      return [];
    }

    let baseMandatory = activity.defaultMandatoryLawfulBases.map((l) => ({
      value: l,
      label: labelForLawfulBasis(l),
    }));

    if (
      activity.autocreationSlug == "AUTOMATED_DECISION_MAKING" ||
      activity.autocreationSlug == "COLD_SALES_COMMUNICATIONS"
    ) {
      baseMandatory.push({ value: "CONSENT", label: labelForLawfulBasis("CONSENT") });
    }

    return baseMandatory;
  }, [activity]);
};

export const useRecommendedLawfulBasisOptions = (activity?: ProcessingActivityDto) => {
  return useMemo(() => {
    if (!activity) {
      return [];
    }

    return activity.defaultOptionalLawfulBases.map((l) => ({
      value: l,
      label: labelForLawfulBasis(l),
    }));
  }, [activity]);
};

export type UseCustomActivities = {
  props: CustomActivityCheckboxesProps;
};
export const useCustomActivityCheckboxes = (
  orgId: string,
  activities: ProcessingActivityDto[],
  region: ProcessingRegion,
  createRegions: ProcessingRegion[],
  onUpdateActivites?: () => void,
): UseCustomActivities => {
  const [checked, setChecked] = useState<string[]>([]);

  const customActivities = useMemo(() => {
    return activities?.filter((a) => !a.isDefault);
  }, [activities]);

  useEffect(() => {
    if (customActivities) {
      const startingChecked = customActivities
        .filter((a) => a.regions.includes(region))
        .map((a) => a.id);
      setChecked(startingChecked);
    }
  }, [customActivities, setChecked, region]);

  const handleChecked = async (id: string, isChecked: boolean) => {
    const wasChecked = checked.includes(id);
    setChecked((prevChecked) => toggleSelection(prevChecked, id, isChecked));

    if (isChecked && !wasChecked) {
      const existingActivity = activities.find((a) => a.id == id);
      if (existingActivity) {
        const regions = [...new Set([...existingActivity.regions, ...createRegions])];
        await Api.processingActivities.patchUpdateProcessingActivity(orgId, existingActivity.id, {
          regions,
          name: undefined,
          lawfulBases: undefined,
        });
      } else {
        await Api.processingActivities.createProcessingActivity(orgId, {
          defaultId: id,
          name: undefined,
          lawfulBases: undefined,
          regions: createRegions,
        });
      }

      onUpdateActivites?.();
    } else if (!isChecked && wasChecked) {
      await Api.processingActivities.deleteProcessingActivityRegion(orgId, id, createRegions);
      onUpdateActivites?.();
    }
  };

  return {
    props: {
      customActivities,
      checked,
      setChecked: handleChecked,
    },
  };
};

export type UseDefaultActivityCheckboxes = {
  requests: {
    defaultActivities: NetworkRequestState;
  };
  props: DefaultActivityCheckboxesProps;
};
export const useDefaultActivityCheckboxes = (
  orgId: string,
  activities: ProcessingActivityDto[],
  region: ProcessingRegion,
  createRegions: ProcessingRegion[],
  onUpdateActivites?: () => void,
): UseDefaultActivityCheckboxes => {
  const [checked, setChecked] = useState<string[]>([]);
  const [defActivites, defActivityRequest] = useDefaultProcessingActivites();

  const nonDeprecated = useMemo(() => {
    return defActivites?.filter((a) => !a.deprecated || checked.includes(a.id)) ?? [];
  }, [defActivites, checked]);

  useEffect(() => {
    if (activities && defActivites) {
      const defIds = defActivites.map((d) => d.id);
      const startingChecked = activities
        .filter((a) => a.regions.includes(region))
        .map((a) => a.defaultId)
        .filter((id) => id && defIds.includes(id));
      setChecked(startingChecked);
    }
  }, [activities, defActivites, region]);

  const handleChecked = async (id: string, isChecked: boolean) => {
    const wasChecked = checked.includes(id);

    setChecked((prevChecked) => toggleSelection(prevChecked, id, isChecked));

    if (isChecked && !wasChecked) {
      const existingActivity = activities.find((a) => a.defaultId == id);
      if (existingActivity) {
        const regions = [...new Set([...existingActivity.regions, ...createRegions])];
        await Api.processingActivities.patchUpdateProcessingActivity(orgId, existingActivity.id, {
          regions,
          name: undefined,
          lawfulBases: undefined,
        });
      } else {
        await Api.processingActivities.createProcessingActivity(orgId, {
          defaultId: id,
          name: undefined,
          lawfulBases: undefined,
          regions: createRegions,
        });
      }

      onUpdateActivites?.();
    } else if (!isChecked && wasChecked) {
      await Api.processingActivities.deleteProcessingActivityRegion(orgId, id, createRegions);
      onUpdateActivites?.();
    }
  };

  return {
    requests: { defaultActivities: defActivityRequest },
    props: {
      defaults: nonDeprecated,
      checked,
      setChecked: handleChecked,
    },
  };
};

export const labelForLawfulBasis = (basis: LawfulBasis): string =>
  LAWFUL_BASIS_LABELS[basis] ?? "Unknown";

const LAWFUL_BASIS_LABELS: Record<LawfulBasis, string> = {
  COMPLY_WITH_LEGAL_OBLIGATIONS: "Complying with legal obligations",
  FULFILL_CONTRACTS: "Fulfilling contracts",
  LEGITIMATE_INTERESTS: "Legitimate interests",
  CONSENT: "Consent",
  PUBLIC_INTEREST: "Public interest",
  VITAL_INTEREST_OF_INDIVIDUAL: "Vital interest of the individual",
};

const addPendingUpdate = (
  update: ProcessedPICPending,
  old: ProcessingActivityDto,
  newDto: ProcessingActivityDto,
) => {
  const oldPending = old?._local?.processedPicPendingUpdates || [];
  const newPending = [...oldPending];

  newPending.push(update);
  const newLocal = newDto?._local || {};
  newLocal.processedPicPendingUpdates = newPending;
  newDto._local = newLocal;
};

const removePendingUpdate = (
  id: UUIDString,
  old: ProcessingActivityDto,
  newDto: ProcessingActivityDto,
) => {
  const oldPending = old?._local?.processedPicPendingUpdates || [];
  const newPending = oldPending.filter((old) => old.id != id);

  const newLocal = newDto?._local || {};
  newLocal.processedPicPendingUpdates = newPending;
  newDto._local = newLocal;
};
