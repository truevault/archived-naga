import { useQueryClient } from "@tanstack/react-query";
import _ from "lodash";
import { useCallback, useMemo } from "react";
import { useActionRequest, useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { Api } from "../../common/service/Api";
import {
  CollectionGroupType,
  UpdateCollectionGroup,
} from "../../common/service/server/controller/CollectionGroupsController";
import { CollectionGroupDetailsDto } from "../../common/service/server/dto/CollectionGroupDto";
import {
  BusinessPurposeDto,
  OrgPersonalInformationSnapshotDto,
  PersonalInformationCollectedDto,
} from "../../common/service/server/dto/PersonalInformationOptionsDto";
import { OrganizationPublicId, UUIDString } from "../../common/service/server/Types";
import { Error as DataInventoryError } from "../copy/dataInventory";
import { Error as RequestsError } from "../copy/requests";
import { nonEmploymentBusinessPurposes } from "./useDataMap";
import { usePrimaryOrganization } from "./useOrganization";

export const sortByGroups = (a: CollectionGroupDetailsDto, b: CollectionGroupDetailsDto) => {
  if (a?.collectionGroupType == b?.collectionGroupType) {
    if (a?.name < b?.name) {
      return -1;
    } else if (a?.name == b?.name) {
      return 1;
    } else {
      return 0;
    }
  } else {
    if (a?.collectionGroupType === "BUSINESS_TO_CONSUMER") {
      return -1;
    } else {
      return 1;
    }
  }
};

export const EXCLUDE_GROUP_EMPLOYMENT: CollectionGroupType[] = ["EMPLOYMENT"];
export const EXCLUDE_GROUP_NON_EMPLOYMENT: CollectionGroupType[] = [
  "BUSINESS_TO_CONSUMER",
  "BUSINESS_TO_BUSINESS",
];

export const useNonEmploymentCollectionGroups = (organizationId: OrganizationPublicId) =>
  useCollectionGroups(organizationId, undefined, EXCLUDE_GROUP_EMPLOYMENT);

export const useEmploymentCollectionGroups = (organizationId: OrganizationPublicId) =>
  useCollectionGroups(organizationId, EXCLUDE_GROUP_EMPLOYMENT);

export const useCollectionGroups = (
  organizationId: OrganizationPublicId,
  includeTypeSlugs?: CollectionGroupType[],
  excludeTypeSlugs?: CollectionGroupType[],
): [CollectionGroupDetailsDto[], NetworkRequestState, () => void] => {
  const consumerGroupsApi = useCallback(
    () =>
      Api.consumerGroups.getCollectionGroups(organizationId, includeTypeSlugs, excludeTypeSlugs),
    [organizationId, includeTypeSlugs, excludeTypeSlugs],
  );

  const {
    result: consumerGroups,
    request,
    refresh,
  } = useDataRequest({
    api: consumerGroupsApi,
    queryKey: ["collectionGroups", organizationId, { includeTypeSlugs, excludeTypeSlugs }],
    notReady: !organizationId,
  });

  const sortedGroups = useMemo(() => {
    let result: CollectionGroupDetailsDto[] = consumerGroups || [];
    return [...result].sort(sortByGroups);
  }, [consumerGroups]);

  return [sortedGroups, request, refresh];
};

export const useCollectionGroup = (
  organizationId: OrganizationPublicId,
  collectionGroupId: UUIDString,
): [CollectionGroupDetailsDto, NetworkRequestState, () => void] => {
  const [groups, request, fetch] = useCollectionGroups(organizationId);

  const group = useMemo(
    () => groups.find((g) => g.id === collectionGroupId),
    [groups, collectionGroupId],
  );

  return [group, request, fetch];
};

export const useBusinessPurposes = (
  organizationId: OrganizationPublicId,
): [BusinessPurposeDto[], NetworkRequestState, () => void] => {
  const [groups, request, fetch] = useCollectionGroups(organizationId);

  const allPurposes = useMemo(() => {
    return _.sortBy(
      _.uniqBy(
        _.flatMap(groups ?? [], (g) => g.businessPurposes),
        (p) => p.id,
      ),
      (p) => p.displayOrder,
    );
  }, [groups]);

  return [allPurposes, request, fetch];
};

type UseCollectionGroups = {
  updateCollectionGroup: ({
    groupId,
    params,
  }: {
    groupId: string;
    params: UpdateCollectionGroup;
  }) => Promise<void>;
  updateCollectionGroupRequest: NetworkRequestState;
};
export const useUpdateCollectionGroups = (organizationId: string): UseCollectionGroups => {
  const { fetch: updateCollectionGroup, request: updateCollectionGroupRequest } = useActionRequest({
    api: ({ groupId, params }: { groupId: string; params: UpdateCollectionGroup }) => {
      return Api.consumerGroups.updateCollectionGroup(organizationId, groupId, params);
    },
    messages: {
      forceError: RequestsError.UpdateVendor,
    },
    allowConcurrent: true,
  });

  return { updateCollectionGroup, updateCollectionGroupRequest };
};

export const useCreateDefaultCollectionGroups = (orgId: string) => {
  const [_org, _state, refreshOrg] = usePrimaryOrganization();
  const api = useCallback(async () => {
    await Api.consumerGroups.createDefaultCollectionGroups(orgId), [orgId];
    await refreshOrg();
  }, [orgId]);
  const { fetch: create, request } = useActionRequest({
    api,
    messages: { forceError: RequestsError.UpdateCollectionGroup },
  });

  return { create, request };
};

type UseCollectionGroupPurposesDetails = {
  purposesForGroup: BusinessPurposeDto[];
  otherPurposes: BusinessPurposeDto[];
  actions: {
    removePurpose: (purposeId: string) => Promise<void>;
    addPurpose: (purposeId: string) => Promise<void>;
  };
};

export const useCollectionGroupPurposesDetails = (
  organizationId: OrganizationPublicId,
  collectionGroup: CollectionGroupDetailsDto,
  piSnapshot: OrgPersonalInformationSnapshotDto,
  defaultCollected: PersonalInformationCollectedDto,
): UseCollectionGroupPurposesDetails => {
  const queryClient = useQueryClient();

  const addPurposeApi = useCallback(
    async (purposeId) =>
      Api.dataInventory.addBusinessPurposeCollected(organizationId, collectionGroup.id, purposeId),
    [organizationId, collectionGroup],
  );

  const { fetch: addPurpose } = useActionRequest({
    api: addPurposeApi,
    messages: {
      forceError: DataInventoryError.UpdateInformationCollection,
    },
    allowConcurrent: true,
    onSuccess: () => queryClient.invalidateQueries(["piSnapshot", organizationId]),
  });

  const removePurposeApi = useCallback(
    async (purposeId) =>
      Api.dataInventory.removeBusinessPurposeCollected(
        organizationId,
        collectionGroup.id,
        purposeId,
      ),
    [organizationId, collectionGroup],
  );

  const { fetch: removePurpose } = useActionRequest({
    api: removePurposeApi,
    messages: {
      forceError: DataInventoryError.UpdateInformationCollection,
    },
    onSuccess: () => queryClient.invalidateQueries(["piSnapshot", organizationId]),
  });

  const purposesForGroup = useMemo(() => {
    const cgSnapshot = piSnapshot.dataInventory.find(
      (snap) => snap.dataSubjectTypeId === collectionGroup.id,
    );

    if (!cgSnapshot) {
      return [];
    }

    return nonEmploymentBusinessPurposes(cgSnapshot.businessPurposes);
  }, [piSnapshot, collectionGroup]);

  const otherPurposes = defaultCollected.businessPurposes.filter(
    (bp) => !purposesForGroup.some((pg) => pg.id === bp.id),
  );

  return {
    purposesForGroup,
    otherPurposes,
    actions: { removePurpose, addPurpose },
  };
};
