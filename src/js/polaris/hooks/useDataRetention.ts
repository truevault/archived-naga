import { useActionRequest, useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models";
import { Api } from "../../common/service/Api";
import {
  DataRetentionPolicyDetailCategory,
  DataRetentionPolicyDto,
  UpdateDataRetentionPolicyDetailDto,
} from "../../common/service/server/dto/DataRetentionPolicyDto";

import { OrganizationPublicId } from "../../common/service/server/Types";

// organization request
export type UpdateDataRentionPolicyFn = (args: {
  category: DataRetentionPolicyDetailCategory;
  dto: UpdateDataRetentionPolicyDetailDto;
}) => Promise<void>;

export const useDataRetentionPolicy = (
  organizationId: OrganizationPublicId,
): {
  result: DataRetentionPolicyDto;
  request: NetworkRequestState;
  refresh: () => void;
  update: UpdateDataRentionPolicyFn;
  updateRequest: NetworkRequestState;
} => {
  const { result, refresh, request } = useDataRequest({
    queryKey: ["dataRetention", organizationId],
    api: () => Api.dataRetention.getDataRetention(organizationId),
  });

  // update request
  const { fetch: update, request: updateRequest } = useActionRequest({
    api: ({
      category,
      dto,
    }: {
      category: DataRetentionPolicyDetailCategory;
      dto: UpdateDataRetentionPolicyDetailDto;
    }) => Api.dataRetention.updateDataRetentionCategory(organizationId, category, dto),
    onSuccess: () => refresh(),
  });

  return { result, request, refresh, update, updateRequest };
};
