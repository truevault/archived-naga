import { useCallback, useMemo } from "react";
import { useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { Api } from "../../common/service/Api";
import { PrivacyNoticeDto } from "../../common/service/server/controller/PrivacyNoticeController";
import {
  PrivacyCenterDto,
  PrivacyCenterPolicyDto,
} from "../../common/service/server/dto/PrivacyCenterDto";
import { OrganizationPublicId } from "../../common/service/server/Types";

export const usePrivacyCenter = (
  organizationId: OrganizationPublicId,
): [PrivacyCenterDto, NetworkRequestState, () => void] => {
  const api = useCallback(
    () => Api.privacyCenter.getPrivacyCenters(organizationId),
    [organizationId],
  );

  const {
    result: privacyCenters,
    request,
    refresh: fetch,
  } = useDataRequest({
    queryKey: ["privacyCenter", organizationId],
    api,
  });

  const privacyCenter = privacyCenters && privacyCenters.length > 0 && privacyCenters[0];

  return [privacyCenter, request, fetch];
};

export const usePrivacyPolicy = (
  orgId?: OrganizationPublicId,
  privacyCenterId?: string,
): [PrivacyCenterPolicyDto, NetworkRequestState, () => void] => {
  const hasEverything = useMemo(
    () => Boolean(orgId) && Boolean(privacyCenterId),
    [orgId, privacyCenterId],
  );

  const api = useCallback(
    () => Api.privacyCenter.getPrivacyCenterPolicy(orgId, privacyCenterId),
    [orgId, privacyCenterId],
  );

  const {
    result: privacyPolicy,
    request,
    refresh: fetch,
  } = useDataRequest({
    queryKey: ["privacyPolicy", orgId, privacyCenterId],
    api,
    notReady: !hasEverything,
  });

  return [privacyPolicy, request, fetch];
};

export const usePrivacyNotice = (
  orgId?: OrganizationPublicId,
): [PrivacyNoticeDto, NetworkRequestState, () => void] => {
  const api = useCallback(() => Api.privacyNotice.getCaPrivacyNotice(orgId), [orgId]);
  const {
    result: noticeDto,
    request: noticeRequest,
    refresh: fetch,
  } = useDataRequest({ queryKey: ["privacyNotice", orgId], api, notReady: !orgId });

  return [noticeDto, noticeRequest, fetch];
};
