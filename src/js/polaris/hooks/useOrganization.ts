import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useCallback } from "react";
import { useDataRequest } from "../../common/hooks/api";
import { useSelf } from "../../common/hooks/useSelf";
import { Organization } from "../../common/models";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { Api } from "../../common/service/Api";
import { OrganizationDto } from "../../common/service/server/dto/OrganizationDto";
import { OrganizationMailboxDto } from "../../common/service/server/dto/OrganizationMailboxDto";
import { PublicOrganizationNoticeDto } from "../../common/service/server/dto/PublicOrganizationNoticeDto";
import { OrganizationPublicId } from "../../common/service/server/Types";
import { Error as RequestsError } from "../copy/requests";

//
// Organization request
//
const orgQueryKey = (orgId: string) => ["organization", orgId];
export const useOrganization = (
  organizationId: string | null,
): [Organization | undefined, NetworkRequestState, () => Promise<void>] => {
  const apiRequest = useCallback(
    () => Api.organization.getOrganization(organizationId),
    [organizationId],
  );

  const {
    result: organization,
    request,
    refresh: fetch,
  } = useDataRequest({
    queryKey: organizationId ? orgQueryKey(organizationId) : [],
    api: apiRequest,
    messages: { defaultError: RequestsError.GetOrg },
    notReady: !organizationId,
  });

  return [organization, request, fetch];
};

export const useUpdateOrganizationFinancialIncentives = (orgId: string) => {
  const qc = useQueryClient();
  const mutation = useMutation(
    async ({ incentive }: { incentive: string }) => {
      return Api.organization.updateFinancialIncentives(orgId, incentive);
    },
    {
      mutationKey: [...orgQueryKey(orgId), "financial-incentives"],
      onSuccess: (newOrg: OrganizationDto) => {
        qc.setQueryData(orgQueryKey(orgId), newOrg);
      },
    },
  );

  return mutation;
};

export const useOrganizationMailboxes = (
  organizationId: string,
): [OrganizationMailboxDto[], NetworkRequestState, () => Promise<void>] => {
  const apiRequest = useCallback(
    () => Api.organization.getOrganizationMailboxes(organizationId),
    [organizationId],
  );

  const {
    result: mailboxes,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["organizationMailboxes", organizationId],
    api: apiRequest,
    messages: { defaultError: RequestsError.GetOrg },
    dependencies: [organizationId],
  });

  return [mailboxes, request, refresh];
};

export const useOrganizationGetCompliantDone = (
  organizationId: string,
): [boolean, NetworkRequestState] => {
  const apiRequest = useCallback(
    () => Api.organization.setGetCompliantDone(organizationId, false),
    [organizationId],
  );

  const { result: done, request } = useDataRequest({
    queryKey: ["useOrganizationGetCompliantDone", organizationId, false],
    api: apiRequest,
    messages: {
      defaultError: RequestsError.GetOrg,
    },
    notReady: !organizationId,
  });

  return [done, request];
};

export const usePublicOrganizationNotices = (
  organizationId: OrganizationPublicId,
): [PublicOrganizationNoticeDto, NetworkRequestState, () => Promise<void>] => {
  const api = useCallback(
    () => Api.publicOrg.getOrganizationNotices(organizationId),
    [organizationId],
  );

  const { result, refresh, request } = useDataRequest({
    queryKey: ["publicOrganizationNotices", organizationId],
    api,
    notReady: !organizationId,
  });

  return [result, request, refresh];
};

export const usePrimaryOrganization = (): [
  Organization,
  NetworkRequestState,
  () => Promise<void>,
] => {
  const primaryOrgId = usePrimaryOrganizationId();

  return useOrganization(primaryOrgId);
};

export const usePrimaryOrganizationId = (): string => {
  const [self] = useSelf();

  const activeOrgId =
    self &&
    self.activeOrganization &&
    self.organizations?.find((o) => o.organization == self.activeOrganization)?.organization;

  const selfOrgId = self?.organizations?.length > 0 ? self?.organizations?.[0].organization : null;

  return activeOrgId || selfOrgId;
};
