import { useCallback } from "react";
import { useActionRequest, useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models";
import { Api } from "../../common/service/Api";
import { OrganizationAuthorizationsDto } from "../../common/service/server/dto/OrganizationDto";
import { OrganizationPublicId } from "../../common/service/server/Types";

type UseAuthorizations = {
  authorizations: OrganizationAuthorizationsDto;
  request: NetworkRequestState;
  refresh: () => Promise<void>;
  actions: {
    unauthorizeGoogleAnalytics: {
      fetch: () => Promise<void>;
      request: NetworkRequestState;
    };
  };
};

export const useAuthorizations = (organizationId: OrganizationPublicId): UseAuthorizations => {
  const api = useCallback(
    () => Api.authorization.getAuthorizations(organizationId),
    [organizationId],
  );

  const dataRequest = useDataRequest({
    api,
    queryKey: ["organization", organizationId, "authorizations"],
    notReady: !organizationId,
  });

  const { fetch: unauthorizeGoogleAnalyticsFetch, request: unauthorizeGoogleAnalyticsRequest } =
    useActionRequest({
      api: () => Api.authorization.unauthorizeGoogleAnalytics(organizationId),
      onSuccess: () => dataRequest.refresh(),
    });

  return {
    authorizations: dataRequest.result,
    request: dataRequest.request,
    refresh: dataRequest.refresh,
    actions: {
      unauthorizeGoogleAnalytics: {
        fetch: unauthorizeGoogleAnalyticsFetch,
        request: unauthorizeGoogleAnalyticsRequest,
      },
    },
  };
};
