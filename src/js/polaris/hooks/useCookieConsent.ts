import { useCallback } from "react";
import { useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models";
import { Api } from "../../common/service/Api";
import { PaginatedGdprCookieConsentDto } from "../../common/service/server/dto/GdprCookieConsentDto";
import { SortRule } from "../../common/service/server/dto/SortRuleDto";
import { OrganizationPublicId } from "../../common/service/server/Types";

// organization request
export const usePaginatedCookieConsents = (
  organizationId: OrganizationPublicId,
  sortBy?: SortRule[],
  page = 0,
  per = 15,
): [PaginatedGdprCookieConsentDto, NetworkRequestState, () => void] => {
  const api = useCallback(
    () => Api.gdprCookieConsent.getCookieConsent(organizationId, sortBy, page, per),
    [organizationId, sortBy, page, per],
  );

  const {
    result: cookieConsent,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["gdprCookieCompliance", organizationId, { page, per }],
    api,
  });

  return [cookieConsent, request, refresh];
};
