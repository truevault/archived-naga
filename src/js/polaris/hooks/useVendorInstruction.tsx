import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useActionRequest } from "../../common/hooks/api";
import { Api } from "../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../common/service/server/dto/RequestHandlingInstructionDto";
import { RequestHandlingInstructionType } from "../../common/service/server/Types";
import { findInstruction } from "../util/requestInstructions";

export const useVendorInstruction = (
  orgId: string,
  vendor: OrganizationDataRecipientDto,
  instructions: RequestHandlingInstructionDto[] | null,
  setInstructions: React.Dispatch<React.SetStateAction<RequestHandlingInstructionDto[]>>,
  requestType: RequestHandlingInstructionType = "DELETE",
) => {
  const baseInstruction = {
    organizationId: orgId,
    requestType: requestType,
    vendorId: vendor.vendorId,
  } as RequestHandlingInstructionDto;

  const foundInstruction = useMemo(() => {
    return instructions
      ? findInstruction(instructions, orgId, vendor.vendorId, "DELETE")
      : undefined;
  }, [instructions, orgId, vendor.vendorId]);

  const [instruction, setInstruction] = useState<RequestHandlingInstructionDto>(
    foundInstruction || baseInstruction,
  );

  useEffect(() => {
    if (instructions) {
      setInstruction(foundInstruction || baseInstruction);
    }
  }, [instructions, foundInstruction, setInstruction]);

  const { fetch: updateRemoteInstruction } = useActionRequest({
    api: (newInstruction) => Api.requestHandlingInstructions.putInstruction(orgId, newInstruction),
  });

  const updateInstruction = useCallback(
    (newInstruction: RequestHandlingInstructionDto) => {
      setInstruction(newInstruction);

      // update the locally stored instructions
      const clonedInstructions = instructions ? [...instructions] : [];

      const idx = instructions?.indexOf(foundInstruction);

      if (idx >= 0) {
        clonedInstructions[idx] = newInstruction;
      } else {
        clonedInstructions.push(newInstruction);
      }

      setInstructions(clonedInstructions);

      updateRemoteInstruction(newInstruction);
    },
    [setInstruction, setInstructions, instructions, foundInstruction, updateRemoteInstruction],
  );

  return { instruction, updateInstruction };
};
