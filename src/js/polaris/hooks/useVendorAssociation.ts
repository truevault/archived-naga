import _ from "lodash";
import { useEffect, useMemo, useState } from "react";
import { NetworkRequestState } from "../../common/models";
import { Api } from "../../common/service/Api";
import { CollectionGroupType } from "../../common/service/server/controller/CollectionGroupsController";
import { CollectionGroupDetailsDto } from "../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../common/service/server/dto/DataMapDto";
import {
  CollectionContext,
  OrganizationDataSourceDto,
} from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationPublicId, UUIDString } from "../../common/service/server/Types";
import { DataRecipientMultiselectProps } from "../pages/design/molecules/DataRecipientMultiselect";
import { toggleSelection } from "../surveys/util";
import { EXCLUDE_GROUP_EMPLOYMENT, useCollectionGroups } from "./useCollectionGroups";
import { useDataMap } from "./useDataMap";
import { useOrgDataSources } from "./useOrganizationVendors";

export type UseVendorAssociation = {
  selected: string[];
  group: CollectionGroupDetailsDto | undefined;
  fetched: {
    collectionGroups: CollectionGroupDetailsDto[];
    dataMap: DataMapDto;
    sources: OrganizationDataSourceDto[];
  };
  requests: {
    collectionGroups: NetworkRequestState;
    sources: NetworkRequestState;
    dataMap: NetworkRequestState;
  };
  multiselect: DataRecipientMultiselectProps;
};

export const useDataSourceAssociation = (
  orgId: OrganizationPublicId,
  groupId: string,
  excludeGroups: CollectionGroupType[],
  collectionContext?: CollectionContext[],
): UseVendorAssociation => {
  const [selected, setSelected] = useState<string[]>([]);

  const [groups, groupsRequest] = useCollectionGroups(orgId, undefined, excludeGroups);
  const [sources, sourcesRequest] = useOrgDataSources(orgId);
  const [dataMap, dataMapRequest] = useDataMap(orgId);

  const relevantSources = useMemo(() => {
    if (!collectionContext) {
      return sources ?? [];
    }

    return (
      sources?.filter((s) => _.intersection(s.collectionContext, collectionContext).length > 0) ??
      []
    );
  }, [collectionContext, sources]);

  const onChange = ({ appId, included }) => {
    setSelected((prevSelected) => toggleSelection(prevSelected, appId, included));
    Api.dataMap.updateSourceAssociation(orgId, groupId, appId, included);
  };

  const onChangeSelectAll = (selected: boolean) => {
    relevantSources.forEach((s) => onChange({ appId: s.vendorId, included: selected }));
  };

  const group = useMemo(() => groups?.find((g) => g.id == groupId), [groups, groupId]);

  // deserialize remote state into local state
  useEffect(() => {
    if (dataMap && group) {
      const associatedVendorIds = dataMap.received
        .filter((d) => d.collectionGroupId == group.id)
        .map((d) => d.vendorId);
      setSelected(associatedVendorIds);
    }
  }, [dataMap, group]);

  return {
    selected,
    group,
    fetched: {
      sources: relevantSources,
      dataMap,
      collectionGroups: groups,
    },
    requests: {
      sources: sourcesRequest,
      dataMap: dataMapRequest,
      collectionGroups: groupsRequest,
    },
    multiselect: {
      recipients: relevantSources,
      selected,
      onChange,
      onChangeSelectAll,
    },
  };
};

export const useDataSourceAssociationForSource = (
  orgId: OrganizationPublicId,
  dataSourceId: UUIDString,
  excludeTypes: CollectionGroupType[] = EXCLUDE_GROUP_EMPLOYMENT,
) => {
  const [selected, setSelected] = useState<string[]>([]);

  const [groups, groupsRequest] = useCollectionGroups(orgId, undefined, excludeTypes);
  const [sources, sourcesRequest] = useOrgDataSources(orgId);
  const [dataMap, dataMapRequest] = useDataMap(orgId);

  const onChange = ({ groupId, included }) => {
    setSelected((prevSelected) => toggleSelection(prevSelected, groupId, included));
    Api.dataMap.updateSourceAssociation(orgId, groupId, dataSourceId, included);
  };

  const source = useMemo(
    () => sources?.find((s) => s.vendorId === dataSourceId),
    [sources, dataSourceId],
  );

  // deserialize remote state into local state
  useEffect(() => {
    if (dataMap && source) {
      const associatedGroupIds = dataMap.received
        .filter((d) => d.vendorId == source.vendorId)
        .map((d) => d.collectionGroupId);

      setSelected(associatedGroupIds);
    }
  }, [dataMap, source]);

  return {
    selected,
    source,
    fetched: {
      sources,
      dataMap,
      collectionGroups: groups,
    },
    requests: {
      sources: sourcesRequest,
      dataMap: dataMapRequest,
      collectionGroups: groupsRequest,
    },
    multiselect: {
      collectionGroups: groups,
      selected,
      onChange,
    },
  };
};
