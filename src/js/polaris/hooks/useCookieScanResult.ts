import { useCallback, useMemo } from "react";
import { useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { Api } from "../../common/service/Api";
import {
  CookieRecommendationDto,
  CookieScanResultDto,
  OrganizationCookieDto,
} from "../../common/service/server/dto/CookieScanDto";
import { OrganizationPublicId } from "../../common/service/server/Types";

export const useCookieScanResult = (
  organizationId: OrganizationPublicId,
): [CookieScanResultDto, NetworkRequestState, () => Promise<void>] => {
  const api = useCallback(
    () => Api.cookieScanner.getCookieScanResult(organizationId),
    [organizationId],
  );

  const { result, request, refresh } = useDataRequest({
    queryKey: ["cookieScanResult", organizationId],
    api,
    notReady: Boolean(!organizationId),
  });

  return [result, request, refresh];
};

export const useCookieRecommendations = (
  organizationId: OrganizationPublicId,
): [CookieRecommendationDto[], NetworkRequestState, () => Promise<void>] => {
  const { result, request, refresh } = useDataRequest({
    api: () => Api.cookieScanner.getCookieRecommendations(organizationId),
    queryKey: ["cookieRecommendations", organizationId],
    notReady: Boolean(!organizationId),
  });

  return [result, request, refresh];
};

export const useRelevantCookieRecommendations = (
  cookie: OrganizationCookieDto,
  cookieRecommendations: CookieRecommendationDto[],
) =>
  useMemo(() => {
    return (
      cookieRecommendations?.filter(
        (r) => r.name === cookie.name && (r.domain == "*" || cookie.domain.endsWith(r.domain)),
      ) ?? []
    );
  }, [cookie.name, cookie.domain, cookieRecommendations]);
