import { useDataRequest } from "../../common/hooks/api";
import { OrganizationUser } from "../../common/models";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { Api } from "../../common/service/Api";
import { OrganizationPublicId } from "../../common/service/server/Types";
import { Error as RequestsError } from "../copy/requests";

export const useOrganizationUsers = (
  organizationId: OrganizationPublicId,
): [OrganizationUser[], NetworkRequestState, () => void] => {
  const { refresh, request, result } = useDataRequest({
    queryKey: ["organizationUsers", organizationId],
    api: () => Api.organizationUser.getUsers(organizationId),
    messages: {
      defaultError: RequestsError.GetOrgUsers,
    },
  });

  return [result, request, refresh];
};
