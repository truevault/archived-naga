import { useQueryClient } from "@tanstack/react-query";
import { useActionRequest, useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models";
import { Api } from "../../common/service/Api";
import { TaskFilter } from "../../common/service/server/controller/TaskController";
import { TaskDto } from "../../common/service/server/dto/TaskDto";
import { OrganizationPublicId } from "../../common/service/server/Types";
import { Error as RequestsError } from "../copy/requests";

// organization request
export const useTasks = (
  organizationId: OrganizationPublicId,
  filter?: TaskFilter,
): [TaskDto[], NetworkRequestState, () => void, (task: TaskDto) => Promise<void>] => {
  const queryClient = useQueryClient();

  const {
    result: tasks,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["task", organizationId, filter],
    api: () => Api.tasks.getTasks(organizationId, filter),
    messages: {
      defaultError: RequestsError.GetRequests,
    },
  });

  const { fetch: completeTask } = useActionRequest({
    api: async (task: TaskDto) => Api.tasks.completeTask(organizationId, task.id),
    onSuccess: () => {
      queryClient.invalidateQueries(["task", organizationId, filter]);
    },
  });

  return [tasks, request, refresh, completeTask];
};
