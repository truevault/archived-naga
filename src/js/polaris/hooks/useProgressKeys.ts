import { useCallback } from "react";
import { useQueryClient } from "@tanstack/react-query";
import { useActionRequest, useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models";
import { Api } from "../../common/service/Api";
import { OrganizationProgressDto } from "../../common/service/server/controller/OrganizationProgressController";
import { MappingProgressEnum, OrganizationPublicId } from "../../common/service/server/Types";

export type UseProgressKeys = {
  progress: OrganizationProgressDto[];
  request: NetworkRequestState;
  update: ({ keyId, progress }: { keyId: string; progress: MappingProgressEnum }) => Promise<void>;
};

export const useProgressKeys = (orgId: OrganizationPublicId, keys?: string[]): UseProgressKeys => {
  const queryClient = useQueryClient();

  const api = useCallback(
    () => Api.organizationProgress.getProgressKeys(orgId, keys),
    [orgId, keys],
  );

  const updateApi = useCallback(
    ({ keyId, progress }) => Api.organizationProgress.setProgressKey(orgId, keyId, progress),
    [orgId],
  );

  const { request: request, result: progress } = useDataRequest({
    queryKey: ["progressKeys", orgId, keys],
    api: api,
  });

  const { fetch: update } = useActionRequest({
    api: updateApi,
    onSuccess: () => {
      queryClient.invalidateQueries(["progressKeys", orgId]);
    },
  });

  return { progress, request, update };
};
