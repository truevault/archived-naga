import { useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { Api } from "../../common/service/Api";
import { DataRequestPublicId, OrganizationPublicId } from "../../common/service/server/Types";
import { Error as RequestsError } from "../copy/requests";

export const useClosedMessageTemplate = (
  organizationId: OrganizationPublicId,
  requestId: DataRequestPublicId,
): [string, NetworkRequestState, () => void] => {
  const {
    result: template,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["closedMessageTemplate", organizationId, requestId],
    api: () => Api.dataRequest.getRequestClosedTemplate(organizationId, requestId),
    messages: {
      defaultError: RequestsError.GetTemplates,
    },
    dependencies: [organizationId, requestId],
  });

  return [template, request, refresh];
};

export const useContactVendorsMessageTemplate = (
  organizationId: OrganizationPublicId,
  requestId: DataRequestPublicId,
): [string, NetworkRequestState, () => void] => {
  const {
    result: template,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["contactVendorsMessageTemplate", organizationId, requestId],
    api: () => Api.dataRequest.getRequestContactVendorsMessageTemplate(organizationId, requestId),
    messages: {
      defaultError: RequestsError.GetTemplates,
    },
    dependencies: [organizationId, requestId],
  });

  return [template, request, refresh];
};
