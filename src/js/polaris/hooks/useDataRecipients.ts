import { ApiData, DataContext, useActionRequest, useDataRequest } from "../../common/hooks/api";
import { Api } from "../../common/service/Api";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
  OrganizationVendorSettingsDto,
} from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationPublicId, UUIDString } from "../../common/service/server/Types";
import { Error as RequestsError } from "../copy/requests";
import { getVendorSettingsObject } from "../util/vendors";
import { useQueryClient } from "@tanstack/react-query";

export type DataRecipientsDataContext = {
  updateItem: UpdateDataRecipientFn;
} & DataContext<OrganizationDataRecipientDto>;

export type UpdateDataRecipientFn = (
  params: UpdateDataRecipientParams,
) => Promise<OrganizationDataRecipientDto>;

export type UpdateDataRecipientParams = {
  vendorId: UUIDString;
} & Partial<OrganizationVendorSettingsDto>;

export type DataRecipientsDataContextOptions = {
  excludeRecipientCategories?: String[] | undefined;
  includeDataStorageLocations?: boolean | undefined;
  excludeIncomplete?: boolean | undefined;
  collectionContext?: CollectionContext[] | undefined;
};

export const useDataRecipients = (
  organizationId: OrganizationPublicId,
  options?: DataRecipientsDataContextOptions,
): DataRecipientsDataContext => {
  const apiData = useFetchDataRecipients(organizationId, options);
  const apiUpdate = useUpdateDataRecipient(organizationId, apiData);

  return {
    results: apiData.result,
    request: apiData.request,
    refresh: apiData.refresh,
    invalidate: apiData.invalidate,
    updateItem: apiUpdate.fetchWithResult,
    updateItemRequest: apiUpdate.request,
  };
};

const useFetchDataRecipients = (
  organizationId: OrganizationPublicId,
  options?: DataRecipientsDataContextOptions,
): ApiData<OrganizationDataRecipientDto[]> => {
  const fetchData = async () => {
    let dataRecipients = await Api.organizationVendor.getVendors(
      organizationId,
      options?.excludeRecipientCategories,
      options?.includeDataStorageLocations,
      options?.excludeIncomplete,
    );

    if (options?.collectionContext) {
      dataRecipients = dataRecipients.filter((dr) => {
        return options.collectionContext.some((c) => dr.collectionContext.includes(c));
      });
    }

    return dataRecipients;
  };

  const queryKey = ["dataRecipients", organizationId, JSON.stringify(options)];

  return useDataRequest({
    queryKey: queryKey,
    api: fetchData,
    messages: {
      defaultError: RequestsError.GetOrgVendors,
    },
    invalidation: {
      wait: 700,
      trailing: true,
    },
  });
};

const useUpdateDataRecipient = (
  organizationId: UUIDString,
  apiData: ApiData<OrganizationDataRecipientDto[]>,
) => {
  const queryClient = useQueryClient();

  const updateData = async ({ vendorId, ...params }: UpdateDataRecipientParams) => {
    const cache = queryClient.getQueryData<OrganizationDataRecipientDto[]>(apiData.queryKey);
    const cachedItem = cache.find((item) => item.vendorId === vendorId);
    const settings = Object.assign(getVendorSettingsObject(cachedItem), { ...params });
    return await Api.organizationVendor.addOrUpdateVendor(organizationId, vendorId, settings);
  };

  const updateCache = (updated: OrganizationDataRecipientDto) => {
    const cache = queryClient.getQueryData<OrganizationDataRecipientDto[]>(apiData.queryKey);
    const newCache = cache.map((item) => (item.id === updated.id ? updated : item));
    queryClient.setQueryData(apiData.queryKey, newCache);
  };

  return useActionRequest<OrganizationDataRecipientDto, UpdateDataRecipientParams>({
    api: updateData,
    onSuccess: updateCache,
    messages: {
      forceError: RequestsError.UpdateVendor,
    },
    allowConcurrent: true,
  });
};
