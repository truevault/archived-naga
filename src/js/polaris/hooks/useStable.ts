import { useMemo, useRef } from "react";

export const useStableValue = <R extends any>(val: R) => {
  const ref = useRef<R>();
  ref.current = val;
  return useMemo<R>(() => ref.current, []);
};
