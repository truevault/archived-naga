import { useQueryClient } from "@tanstack/react-query";
import _ from "lodash";
import { useCallback, useMemo } from "react";
import { useDataRecipientsCatalog, useDataSourceCatalog } from "../../common/hooks";
import {
  InvalidateFn,
  RefreshFn,
  useActionRequest,
  useDataRequest,
  useDataRequests,
} from "../../common/hooks/api";
import {
  OrganizationDataSource,
  OrganizationPlatformVendor,
  OrganizationVendor,
} from "../../common/models";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { Api } from "../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../common/service/server/dto/DataMapDto";
import {
  AddOrUpdateCustomVendorDto,
  CollectionContext,
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../common/service/server/dto/RequestHandlingInstructionDto";
import { RetentionReasonDto } from "../../common/service/server/dto/RetentionReasonDto";
import {
  OrganizationPublicId,
  RequestHandlingInstructionType,
  UUIDString,
  VendorPublicId,
} from "../../common/service/server/Types";
import { Error as RequestsError } from "../copy/requests";
import { CustomVendorFormValues } from "../pages/get-compliant/data-recipients/AddDataRecipients";
import { alphabetically, dataSourceSort } from "../surveys/util";
import { getVendorSettingsObject, vendorsByCcpaSellingOrSharing } from "../util/vendors";

export const useOrganizationVendors = (
  organizationId: OrganizationPublicId,
  excludeRecipientCategories: String[] | undefined = [],
  includeDataStorageLocations: boolean = false,
  excludeIncomplete: boolean | undefined = undefined,
  context: CollectionContext[] | undefined = undefined,
): [OrganizationVendor[], NetworkRequestState, RefreshFn, InvalidateFn] => {
  const apiRequest = useCallback(() => {
    return Api.organizationVendor.getVendors(
      organizationId,
      excludeRecipientCategories,
      includeDataStorageLocations,
      excludeIncomplete,
    );
  }, [organizationId, includeDataStorageLocations, excludeRecipientCategories, excludeIncomplete]);

  const queryKey = [
    "organizationVendors",
    organizationId,
    excludeRecipientCategories,
    includeDataStorageLocations,
  ];

  const {
    result: vendors,
    request,
    refresh: fetchOrgVendors,
    invalidate,
  } = useDataRequest({
    queryKey: queryKey,
    api: apiRequest,
    messages: {
      defaultError: RequestsError.GetOrgVendors,
    },
    dependencies: [organizationId, excludeRecipientCategories],
    invalidation: {
      wait: 700,
      trailing: true,
    },
  });

  const sortedVendors = useMemo(() => {
    let sorted = vendors?.sort(alphabetically) ?? [];
    if (context) {
      sorted = sorted.filter((vendor) => vendor.collectionContext.some((c) => context.includes(c)));
    }
    return sorted;
  }, [vendors]);

  return [sortedVendors, request, fetchOrgVendors, invalidate];
};

export const useUnmappedOrganizationVendors = (
  organizationId: OrganizationPublicId,
  collectionGroupId: UUIDString,
): [OrganizationVendor[], NetworkRequestState, RefreshFn] => {
  const apiRequest = useCallback(() => {
    return Api.organizationVendor.getUnmappedVendors(organizationId, collectionGroupId);
  }, [organizationId, collectionGroupId]);

  const {
    result: vendors,
    request,
    refresh: fetchUnmappedOrgVendors,
  } = useDataRequest({
    queryKey: ["useUnmappedOrganizationVendors", organizationId, collectionGroupId],
    api: apiRequest,
    messages: {
      defaultError: RequestsError.GetOrgVendors,
    },
    dependencies: [organizationId, collectionGroupId],
  });

  const sortedVendors = useMemo(() => {
    return vendors?.sort(alphabetically) ?? [];
  }, [vendors]);

  return [sortedVendors, request, fetchUnmappedOrgVendors];
};

export const useOrgDataSources = (
  organizationId: OrganizationPublicId,
): [OrganizationDataSource[], NetworkRequestState, () => void] => {
  const api = useCallback(
    () => Api.orgDataSource.getOrgDataSources(organizationId),
    [organizationId],
  );

  const { result, request, refresh } = useDataRequest({
    queryKey: ["orgDataSources", organizationId],
    api,
  });

  return [result, request, refresh];
};

export const useOrgDataSource = (
  organizationId: OrganizationPublicId,
  dataSourceId: UUIDString,
): [OrganizationDataSource, NetworkRequestState, () => void] => {
  const [sources, request, refresh] = useOrgDataSources(organizationId);

  const source = useMemo(
    () => sources?.find((s) => s.vendorId === dataSourceId),
    [sources, dataSourceId],
  );

  return [source, request, refresh];
};

export const useOrganizationVendorDetails = (
  organizationId: OrganizationPublicId,
  vendorId: UUIDString,
): [OrganizationDataRecipientDto, NetworkRequestState, () => void] => {
  const api = useCallback(
    () => Api.organizationVendor.getVendorDetails(organizationId, vendorId),
    [organizationId, vendorId],
  );

  const { result, request, refresh } = useDataRequest({
    queryKey: ["organizationVendorDetails", organizationId, vendorId],
    api,
  });

  return [result, request, refresh];
};

export const usePlatformAppsForVendors = (
  organizationId: OrganizationPublicId,
  vendorIds: VendorPublicId[],
): [
  Record<VendorPublicId, OrganizationPlatformVendor>,
  NetworkRequestState,
  () => Promise<void>,
] => {
  const [platformAppQueries, networkRequestState, refreshAll] = useDataRequests(
    vendorIds.map((vendorId) => ({
      queryKey: ["platformApps", organizationId, vendorId],
      queryFn: () => Api.organizationVendor.getPlatformApps(organizationId, vendorId),
    })),
  );

  const platformMap = useMemo(
    () =>
      platformAppQueries
        .filter((q) => q.status === "success")
        .reduce((acc, q) => {
          acc[q.data.vendorId] = q.data;
          return acc;
        }, {}),
    [platformAppQueries],
  );

  return [platformMap, networkRequestState, refreshAll];
};

export const usePlatformApps = (
  organizationId: OrganizationPublicId,
  vendorId: VendorPublicId,
): [OrganizationPlatformVendor, NetworkRequestState, () => Promise<void>] => {
  const api = useCallback(() => {
    if (vendorId == null) return null;
    return Api.organizationVendor.getPlatformApps(organizationId, vendorId);
  }, [organizationId, vendorId]);

  const { result, request, refresh } = useDataRequest({
    queryKey: ["platformApps", organizationId, vendorId],
    api,
  });

  return [result, request, refresh];
};

export const useAppPlatforms = (
  organizationId: OrganizationPublicId,
  vendorId: VendorPublicId,
): [OrganizationPlatformVendor, NetworkRequestState, () => void] => {
  const api = useCallback(() => {
    if (vendorId == null) return null;
    return Api.organizationVendor.getAppPlatforms(organizationId, vendorId);
  }, [organizationId, vendorId]);

  const { result, request, refresh } = useDataRequest({
    queryKey: ["appPlatforms", organizationId, vendorId],
    api,
  });

  return [result, request, refresh];
};

export const useSellingOrSharingDataRecipients = (
  dataRecipients: OrganizationDataRecipientDto[],
) => {
  return useMemo(() => vendorsByCcpaSellingOrSharing(dataRecipients), [dataRecipients]);
};

export const useDisclosingDataRecipients = (
  collectionGroup: CollectionGroupDetailsDto,
  dataMap: DataMapDto,
  dataRecipients: OrganizationDataRecipientDto[],
): [OrganizationDataRecipientDto[], OrganizationDataRecipientDto[]] => {
  const cgDisclosures = dataMap.disclosure.filter(
    (d) => d.collectionGroupId === collectionGroup.id,
  );

  return useMemo(
    () =>
      _.partition(dataRecipients, (dr) => {
        const disclosures = cgDisclosures.filter((d) => d.vendorId === dr.vendorId);
        return disclosures.some((d) => d?.disclosed?.length > 0 ?? false);
      }),
    [cgDisclosures, dataRecipients],
  );
};

export const useSourcingDataRecipients = (
  collectionGroup: CollectionGroupDetailsDto,
  dataMap: DataMapDto,
  dataSources: OrganizationDataSourceDto[],
): [OrganizationDataSourceDto[], OrganizationDataSourceDto[]] => {
  const cgReceived = dataMap.received.filter((r) => r.collectionGroupId === collectionGroup.id);

  return useMemo(
    () =>
      _.partition(dataSources, (dr) => {
        const receipts = cgReceived.filter((r) => r.vendorId === dr.vendorId);
        return receipts.some((r) => r?.received?.length > 0 ?? false);
      }),
    [cgReceived, dataSources],
  );
};

export const useDataRecipientSearch = (
  orgId: UUIDString,
  {
    onAddSuccess,
    onAddCustomSuccess,
    onRemoveSuccess,
    onEditSuccess,
    onAddContractorSuccess,
    onAddThirdParyRecipientSuccess,
  }: {
    onAddSuccess?: (dr: OrganizationDataRecipientDto) => void;
    onAddCustomSuccess?: (dr: OrganizationDataRecipientDto) => void;
    onRemoveSuccess?: () => void;
    onEditSuccess?: (dr: OrganizationDataRecipientDto) => void;
    onAddContractorSuccess?: (dr: OrganizationDataRecipientDto) => Promise<void>;
    onAddThirdParyRecipientSuccess?: (dr: OrganizationDataRecipientDto) => Promise<void>;
  } = {},
) => {
  const queryClient = useQueryClient();
  const [orgDataRecipients, orgDataRecipientsRequest] = useOrganizationVendors(orgId);
  const [catalog, catalogRequest] = useDataRecipientsCatalog(orgId);

  const invalidate = () => {
    queryClient.invalidateQueries(["organizationVendors", orgId]);
    queryClient.invalidateQueries(["vendorCatalog", orgId]);
  };

  const orgDataRecipientIds = useMemo(
    () => orgDataRecipients?.map((o) => o.vendorId) ?? [],
    [orgDataRecipients],
  );

  const selectedCatalog = useMemo(
    () => catalog.filter((c) => orgDataRecipientIds.includes(c.id)),
    [orgDataRecipientIds, catalog],
  );

  const allVendorsMap = useMemo(
    () => catalog && new Map(catalog?.map((v) => [v.id, v])),
    [catalog],
  );

  const enabledVendors = useMemo(
    () => selectedCatalog && new Set(selectedCatalog?.map((v) => v.id)),
    [selectedCatalog],
  );

  const { fetch: handleAddVendor, request: addingVendor } = useActionRequest({
    api: async (vendorId) =>
      await Api.organizationVendor.addOrUpdateVendor(orgId, vendorId, {
        classificationId: null,
        gdprHasSccSetting: false,
        gdprSccUrl: null,
        gdprSccFileKey: null,
        gdprSccFileName: null,
        removedFromExceptionsToScc: false,
        completed: false,
      }),
    onSuccess: (dr: OrganizationDataRecipientDto) => {
      invalidate();

      if (onAddSuccess) {
        onAddSuccess(dr);
      }
    },
  });

  const { fetch: handleAddCustomVendor, request: addingCustomVendor } = useActionRequest({
    api: async (fields: CustomVendorFormValues) =>
      await Api.organizationVendor.addCustomVendor(orgId, fields),
    onSuccess: (dr: OrganizationDataRecipientDto) => {
      invalidate();

      if (onAddCustomSuccess) {
        onAddCustomSuccess(dr);
      }
    },
  });

  const { fetch: handleRemoveVendor, request: removingVendor } = useActionRequest({
    api: (dataRecipient: OrganizationDataRecipientDto) =>
      Api.organizationVendor.removeVendor(orgId, dataRecipient.vendorId),
    onSuccess: () => {
      invalidate();

      if (onRemoveSuccess) {
        onRemoveSuccess();
      }
    },
  });

  const { fetch: handleUpdateVendor, request: updatingVendor } =
    useActionRequest<OrganizationDataRecipientDto>({
      api: async ({
        dataRecipient,
        values,
      }: {
        dataRecipient: OrganizationDataRecipientDto;
        values: CustomVendorFormValues;
      }) => {
        return await Api.organizationVendor.addOrUpdateVendor(
          orgId,
          dataRecipient.vendorId,
          Object.assign(getVendorSettingsObject(dataRecipient), values),
        );
      },
      onSuccess: (dr: OrganizationDataRecipientDto) => {
        invalidate();

        if (onEditSuccess) {
          onEditSuccess(dr);
        }
      },
    });

  const { fetch: handleAddThirdParyRecipient, request: addingThirdPartyRecipient } =
    useAddThirdPartyRecipient(orgId, onAddThirdParyRecipientSuccess);

  const { fetch: handleAddContractor, request: addingContractor } = useAddContractors(
    orgId,
    onAddContractorSuccess,
  );

  return {
    requests: {
      orgDataRecipientsRequest,
      catalogRequest,
      addingVendor,
      addingCustomVendor,
      removingVendor,
      updatingVendor,
      addingThirdPartyRecipient,
      addingContractor,
    },
    data: {
      allVendorsMap,
      enabledVendors,
      catalog,
    },
    actions: {
      handleAddVendor,
      handleAddCustomVendor,
      handleUpdateVendor,
      handleRemoveVendor,
      handleAddThirdParyRecipient,
      handleAddContractor,
    },
  };
};

export const useDataSourceSearch = (
  orgId: UUIDString,
  {
    onAddSuccess,
    onAddCustomSuccess,
    onRemoveSuccess,
  }: {
    onAddSuccess?: (dr: OrganizationDataSourceDto) => void;
    onAddCustomSuccess?: (dr: OrganizationDataSourceDto) => void;
    onRemoveSuccess?: () => void;
  } = {},
) => {
  const queryClient = useQueryClient();

  const [orgDataSources, orgDataSourcesRequest] = useOrgDataSources(orgId);
  const [catalog, catalogRequest] = useDataSourceCatalog(orgId);

  const invalidate = () => {
    queryClient.invalidateQueries(["orgDataSources", orgId]);
    queryClient.invalidateQueries(["vendorCatalog", orgId]);
  };

  const orgSourceIds = useMemo(
    () => orgDataSources?.map((o) => o.vendorId) ?? [],
    [orgDataSources],
  );

  const [selectedCatalog, remainingCatalog] = useMemo(() => {
    const [a, b] = _.partition(catalog, (c) => orgSourceIds.includes(c.id));
    return [a.sort(dataSourceSort), b.sort(dataSourceSort)];
  }, [orgSourceIds, catalog]);

  const allVendorsMap = useMemo(
    () => catalog && new Map(catalog?.map((v) => [v.id, v])),
    [catalog],
  );

  const enabledVendors = useMemo(
    () => selectedCatalog && new Set(selectedCatalog?.map((v) => v.id)),
    [selectedCatalog],
  );

  const { fetch: handleAddVendor, request: addingVendor } = useActionRequest({
    api: async (vendorId) => await Api.orgDataSource.addOrgDataSource(orgId, { vendorId }),
    onSuccess: (ds: OrganizationDataSourceDto) => {
      invalidate();

      if (onAddSuccess) {
        onAddSuccess(ds);
      }
    },
  });

  const { fetch: handleAddCustomVendor, request: addingCustomVendor } = useActionRequest({
    api: async (fields: CustomVendorFormValues) =>
      await Api.orgDataSource.addOrgDataSource(orgId, {
        custom: fields,
      }),
    onSuccess: (ds: OrganizationDataSourceDto) => {
      invalidate();

      if (onAddCustomSuccess) {
        onAddCustomSuccess(ds);
      }
    },
  });

  const { fetch: handleRemoveVendor, request: removingVendor } = useActionRequest({
    api: async (vendorId: string) => await Api.orgDataSource.removeOrgDataSource(orgId, vendorId),
    onSuccess: () => {
      invalidate();

      if (onRemoveSuccess) {
        onRemoveSuccess();
      }
    },
  });

  return {
    requests: {
      orgDataSourcesRequest,
      catalogRequest,
      addingVendor,
      addingCustomVendor,
      removingVendor,
    },
    data: {
      allVendorsMap,
      enabledVendors,
      remainingCatalog,
      selectedCatalog,
      catalog,
    },
    actions: {
      handleAddVendor,
      handleAddCustomVendor,
      handleRemoveVendor,
    },
  };
};

export const useRetentionReasons = (): [
  RetentionReasonDto[],
  NetworkRequestState,
  () => Promise<void>,
] => {
  const retentionReasonsApi = useCallback(() => Api.polarisData.getRetentionReasons(), []);

  const {
    result: retentionReasons,
    request: retentionReasonsRequest,
    refresh,
  } = useDataRequest({
    queryKey: ["retentionReasons"],
    api: retentionReasonsApi,
  });

  return [retentionReasons, retentionReasonsRequest, refresh];
};

export const useRequestInstructions = (
  orgId: OrganizationPublicId,
  requestType?: RequestHandlingInstructionType,
  onSuccess?: (data: RequestHandlingInstructionDto[]) => void,
): [RequestHandlingInstructionDto[], NetworkRequestState, () => Promise<void>] => {
  const queryKey = requestType
    ? ["fetchedInstructions", orgId, requestType]
    : ["fetchedInstructions", orgId];

  const {
    result: instructions,
    request: instructionsRequest,
    refresh,
  } = useDataRequest({
    queryKey,
    api: () => Api.requestHandlingInstructions.getInstructionsByType(orgId, requestType),
    onSuccess,
  });

  return [instructions, instructionsRequest, refresh];
};

export const useAddThirdPartyRecipient = (
  organizationId: OrganizationPublicId,
  onSuccess: (dataRecipient: OrganizationDataRecipientDto) => Promise<void>,
  collectionContext?: CollectionContext[],
) =>
  useActionRequest({
    api: async ({ name, category, url }: CustomVendorFormValues) => {
      const settings: any = {
        name,
        category,
        url,
        dataRecipientType: "third_party_recipient",
        collectionContext,
      };

      return await Api.organizationVendor.addCustomVendor(organizationId, settings);
    },
    onSuccess,
  });

export const useAddContractors = (
  organizationId: OrganizationPublicId,
  onSuccess: (dataRecipient: OrganizationDataRecipientDto) => Promise<void>,
  collectionContext?: CollectionContext[],
) =>
  useActionRequest({
    api: async ({ name, url }: CustomVendorFormValues) => {
      const settings: AddOrUpdateCustomVendorDto = {
        name,
        category: "Contractor",
        url,
        dataRecipientType: "contractor",
        collectionContext,
      };

      // SO SO DUMB, I HATE IT. The API provides classification options only after you have an existing data recipient.
      // We'll some day update the classification options to not be a separate table, and just be an enum.
      const recipient = await Api.organizationVendor.addCustomVendor(organizationId, settings);
      const updateSettings = getVendorSettingsObject(recipient);
      updateSettings.classificationId = recipient.classificationOptions.find(
        (o) => o.slug == "contractor",
      )?.id;

      return await Api.organizationVendor.addOrUpdateVendor(
        organizationId,
        recipient.vendorId,
        updateSettings,
      );
    },
    onSuccess: onSuccess,
  });
