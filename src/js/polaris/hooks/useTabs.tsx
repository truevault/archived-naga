import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Tabs } from "../components";

export type TabOption<T extends string = string> = { label: string; value: T };
export type TabOptions<T extends string = string> = TabOption<T>[];

export const useTabs = <T extends string = string>(
  options: TabOptions<T>,
  def: T = null,
  ariaLabel: string = null,
  onChange?: (value: T) => void,
  textColor: "secondary" | "primary" | "inherit" = "secondary",
): [string, React.ReactNode, (value: T) => void] => {
  const actualDefault = def || (options.length > 0 && options[0].value) || "none";
  const [tab, setTab] = useState(actualDefault);

  useEffect(() => {
    setTab(actualDefault);
  }, [actualDefault]);

  const tabs = (
    <Tabs
      options={options}
      tab={tab}
      setTab={setTab}
      ariaLabel={ariaLabel}
      onChange={onChange}
      textColor={textColor}
    />
  );

  return [tab, tabs, setTab];
};

const useCustomQueryParam = <T extends string = string>(
  param: string,
  def: T,
): [T, React.Dispatch<React.SetStateAction<T>>] => {
  const history = useHistory();
  const searchParams = new URLSearchParams(history.location.search);

  const initialValue = searchParams.get(param || def) as T;

  const [value, setValue] = useState<T>(initialValue);

  // update the url when the value is set
  useEffect(() => {
    const searchParams = new URLSearchParams(history.location.search);
    const current = searchParams.get(param);

    const isDefault = value == def && current == undefined;

    if (current != value && !isDefault) {
      searchParams.set(param, value);
      history.push({ pathname: history.location.pathname, search: searchParams.toString() });
    }
  }, [value]);

  // update the value when the query param changes
  useEffect(() => {
    const searchParams = new URLSearchParams(history.location.search);
    const val = (searchParams.get(param) || def) as T;

    if (val != value) {
      setValue(val);
    }
  }, [history.location.search, value]);

  return [value, setValue];
};

export const useQueryTabs = <T extends string = string>(
  options: TabOptions<T>,
  def: T = null,
  ariaLabel: string = null,
  onChange?: (value: T) => void,
  textColor: "secondary" | "primary" | "inherit" = "secondary",
): [string, React.ReactNode, (value: T) => void] => {
  const actualDefault: T = (def || (options.length > 0 && options[0].value) || "none") as T;
  const [tab, setTab] = useCustomQueryParam<T>("tab", actualDefault);

  const tabs = (
    <Tabs
      options={options}
      tab={tab}
      setTab={(t: T) => setTab(t)}
      ariaLabel={ariaLabel}
      onChange={onChange}
      textColor={textColor}
    />
  );

  return [tab, tabs, setTab];
};
