import { useMutation, UseMutationResult, useQueryClient } from "@tanstack/react-query";
import _ from "lodash";
import { useCallback, useEffect } from "react";
import { useDataRequest } from "../../common/hooks/api";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { Api } from "../../common/service/Api";
import {
  DataSubjectRequestDetailDto,
  PaginatedSubjectRequestDto,
} from "../../common/service/server/dto/DataSubjectRequestDto";
import { Filter } from "../../common/service/server/dto/FilterDto";
import { SortRule } from "../../common/service/server/dto/SortRuleDto";
import { UpdateVendorRequest } from "../../common/service/server/dto/UpdateVendorRequest";
import {
  DataRequestFilter,
  DataRequestPublicId,
  OrganizationPublicId,
  RequestTypeFilter,
  UUIDString,
} from "../../common/service/server/Types";
import { Error as RequestsError } from "../copy/requests";

// organization request
export const useRequestSummaries = (
  organizationId: OrganizationPublicId,
  filter?: DataRequestFilter,
  filters?: Filter[],
  sortBy?: SortRule[],
  type: RequestTypeFilter = "all",
  page = 0,
  per = 15,
): [PaginatedSubjectRequestDto, NetworkRequestState, () => void] => {
  const qc = useQueryClient();

  useEffect(() => {
    // cleanup similar variations of the query from the React Query cache
    qc.removeQueries({
      predicate: (query) => {
        const params = (query.queryKey[2] ?? {}) as any;
        return (
          query.queryKey[0] === "dataRequests" &&
          query.queryKey[1] === organizationId &&
          params.filter === filter &&
          params.type === type
        );
      },
    });
  }, [organizationId, filter, filters, sortBy, type, page, per]);

  const api = useCallback(
    () => Api.dataRequest.getRequests(organizationId, filter, filters, sortBy, type, page, per),
    [organizationId, filter, filters, sortBy, type, page, per],
  );

  const {
    result: dataRequests,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["dataRequests", organizationId, { filter, filters, sortBy, type, page, per }],
    api,
    messages: { defaultError: RequestsError.GetRequests },
  });

  return [dataRequests, request, refresh];
};

export const useRequestsForConsumer = (
  organizationId: OrganizationPublicId,
  consumerId: string,
): [DataSubjectRequestDetailDto[], NetworkRequestState, () => void] => {
  const apiRequest = useCallback(
    () => Api.dataRequest.getRequestsForConsumer(organizationId, consumerId),
    [organizationId, consumerId],
  );

  const {
    result: requests,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["dataRequests", organizationId, consumerId],
    api: apiRequest,
    messages: {
      defaultError: RequestsError.GetRequests,
    },
    dependencies: [organizationId, consumerId],
  });

  return [requests, request, refresh];
};

export const useRequest = (
  organizationId: OrganizationPublicId,
  requestId: DataRequestPublicId,
): [DataSubjectRequestDetailDto, NetworkRequestState, () => void, UpdateVendorOutcomeMutation] => {
  const apiRequest = useCallback(
    () => Api.dataRequest.getRequest(organizationId, requestId),
    [organizationId, requestId],
  );

  const {
    result: dataRequest,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["dataRequest", organizationId, requestId],
    api: apiRequest,
    messages: { defaultError: RequestsError.GetRequests },
    dependencies: [organizationId, requestId],
  });

  const updateVendorOutcomeMutation = useUpdateVendorOutcomeMutation(organizationId, requestId);

  return [dataRequest, request, refresh, updateVendorOutcomeMutation];
};

export type UpdateVendorOutcomeMutation = UseMutationResult<
  DataSubjectRequestDetailDto,
  unknown,
  UpdateVendorRequest,
  unknown
>;
const useUpdateVendorOutcomeMutation = (
  orgId: OrganizationPublicId,
  requestId: DataRequestPublicId,
): UpdateVendorOutcomeMutation => {
  const qc = useQueryClient();
  const updateMutation = useMutation(
    async (values: UpdateVendorRequest) => Api.dataRequest.updateVendor(orgId, requestId, values),

    {
      onMutate: (values: UpdateVendorRequest) => {
        const oldDto = qc.getQueryData([
          "dataRequest",
          orgId,
          requestId,
        ]) as DataSubjectRequestDetailDto;
        const newDto = _.cloneDeep(oldDto);
        addPendingVendorOutcome(values, oldDto, newDto);
        qc.setQueryData(["dataRequest", orgId, requestId], newDto);
      },

      onSuccess: (remoteDto, { vendor }) => {
        const oldDto = qc.getQueryData([
          "dataRequest",
          orgId,
          requestId,
        ]) as DataSubjectRequestDetailDto;
        const newDto = _.cloneDeep(oldDto);
        removePendingVendorOutcome(vendor, oldDto, newDto);
        // don't patch the *entire* object, only update the specific vendor outcome
        updateRemoteVendorOutcome(vendor, remoteDto, newDto);
        qc.setQueryData(["dataRequest", orgId, requestId], newDto);
      },

      onError: (error, { vendor }) => {
        const oldDto = qc.getQueryData([
          "dataRequest",
          orgId,
          requestId,
        ]) as DataSubjectRequestDetailDto;
        const newDto = _.cloneDeep(oldDto);
        removePendingVendorOutcome(vendor, oldDto, newDto);
        qc.setQueryData(["dataRequest", orgId, requestId], newDto);
      },
    },
  );

  return updateMutation;
};

const addPendingVendorOutcome = (
  update: UpdateVendorRequest,
  oldDto: DataSubjectRequestDetailDto,
  newDto: DataSubjectRequestDetailDto,
) => {
  const oldPending = oldDto?._local?.pendingVendorOutcomeUpdates || [];
  const newPending = [...oldPending];

  const idx = newPending.findIndex((np) => np.vendorId == update.vendor);
  const newItem = { vendorId: update.vendor, outcome: update.outcome };
  if (idx < 0) {
    newPending.push({ vendorId: update.vendor, outcome: update.outcome });
  } else {
    newPending.splice(idx, 1, newItem);
  }

  const newLocal = newDto?._local || {};
  newLocal.pendingVendorOutcomeUpdates = newPending;
  newDto._local = newLocal;
};

const removePendingVendorOutcome = (
  vendorId: UUIDString,
  oldDto: DataSubjectRequestDetailDto,
  newDto: DataSubjectRequestDetailDto,
) => {
  const oldPending = oldDto?._local?.pendingVendorOutcomeUpdates || [];
  const newPending = oldPending.filter((old) => old.vendorId != vendorId);

  const newLocal = newDto?._local || {};
  newLocal.pendingVendorOutcomeUpdates = newPending;
  newDto._local = newLocal;
};

const updateRemoteVendorOutcome = (
  vendorId: UUIDString,
  remote: DataSubjectRequestDetailDto,
  newDto: DataSubjectRequestDetailDto,
) => {
  const outcome = remote.vendorOutcomes.find((vo) => vo.vendor.id == vendorId);
  const newIdx = newDto.vendorOutcomes.findIndex((vo) => vo.vendor.id == vendorId);
  if (outcome) {
    if (newIdx < 0) {
      newDto.vendorOutcomes.push(outcome);
    } else {
      newDto.vendorOutcomes.splice(newIdx, 1, outcome);
    }
  } else {
    if (newIdx >= 0) {
      newDto.vendorOutcomes.splice(newIdx, 1);
    }
  }
};
