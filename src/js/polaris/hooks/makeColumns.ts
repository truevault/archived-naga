import _ from "lodash";
import { useMemo } from "react";
import { Column } from "react-table";

type ColumnFn = (deps?: any) => Column<any>[];

export const makeColumns = (columnFn: ColumnFn) => (args?: any) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const columns = useMemo(() => columnFn(args), _.castArray(args));

  return columns;
};
