export const VENDOR_CATEGORY_AD_NETWORK = "Ad Network";
export const VENDOR_CATEGORY_RETAIL_PARTNER = "Retail Partner";
export const VENDOR_CATEGORY_DATA_BROKER = "Data Broker";
export const VENDOR_CATEGORY_DATA_ANALYTICS_PROVIDER = "Data Analytics Provider";
export const VENDOR_CATEGORY_APPLICANT_TRACKING = "Recruitment & Applicant Tracking System";
export const VENDOR_CATEGORY_RECRUITMENT_SERVICE = "Recruitment Service";
export const VENDOR_CATEGORY_BACKGROUND_CHECK_PROVIDER = "Background Check Provider";

export const VENDOR_KEY_TRUEVAULT_POLARIS = "truevault-polaris";
export const VENDOR_KEY_GA = "google-analytics";
export const VENDOR_KEY_SHOPIFY = "shopify";
export const VENDOR_KEY_GOOGLE_ADS = "google-ads";
export const VENDOR_KEY_FB_ADS = "facebook-ads";
export const VENDOR_KEY_PAYPAL = "paypal";

export const SOURCE_KEY_CONSUMER_DIRECTLY = "source-consumer-directly";
export const SOURCE_KEY_OTHER_INDIVIDUALS = "source-other-individuals";

export const VENDOR_CLASSIFICATION_SLUG_THIRD_PARTY = "third-party";
