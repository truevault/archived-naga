import React, { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router-dom";
import { useDataRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { UUIDString } from "../../../common/service/server/Types";
import { Loading } from "../../components";
import { useCollectionGroups } from "../../hooks/useCollectionGroups";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";

type NoticeType = "CA_PRIVACY_NOTICE" | "OPT_OUT_PRIVACY_NOTICE" | "CONSUMER_GROUP";

interface PrivacynoticeReviewProps {
  noticeType: NoticeType;
}
export const PrintPrivacyNoticeReview: React.FC<PrivacynoticeReviewProps> = ({ noticeType }) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();
  const { consumerGroupId } = useParams<{ consumerGroupId: UUIDString }>();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  const [collectionGroups] = useCollectionGroups(organizationId);
  const consumerGroup = useMemo(
    () => collectionGroups?.find((cg) => cg.id === consumerGroupId),
    [consumerGroupId, collectionGroups],
  );

  // Requests
  const { result: noticeDto, request: noticeRequest } = useDataRequest({
    queryKey: ["notice", organizationId, consumerGroupId],
    api: () =>
      consumerGroupId
        ? Api.privacyNotice.getConsumerGroupPrivacyNotice(organizationId, consumerGroupId)
        : noticeType === "CA_PRIVACY_NOTICE"
        ? Api.privacyNotice.getCaPrivacyNotice(organizationId)
        : Api.privacyNotice.getOptOutPrivacyNotice(organizationId),
  });

  useEffect(() => {
    if (noticeRequest.success && (!consumerGroupId || consumerGroup)) {
      setReadyToRender(true);
    }
  }, [consumerGroupId, consumerGroup, noticeRequest]);

  if (!readyToRender) {
    return <Loading />;
  }

  return (
    <div
      className="privacy-notices--review-container privacy-notices--print"
      dangerouslySetInnerHTML={{ __html: noticeDto.notice }}
    />
  );
};
