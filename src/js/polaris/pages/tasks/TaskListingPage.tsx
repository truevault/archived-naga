import React, { useMemo } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import { ColumnDefinition, TileTable } from "../../../common/components/TileTable";
import { TaskFilter } from "../../../common/service/server/controller/TaskController";
import { TaskDto } from "../../../common/service/server/dto/TaskDto";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { EmptyTodoTasks, TaskCard } from "../../components/organisms/tasks/TaskCard";
import { Fmt } from "../../formatters";
import { useOrganization, usePrimaryOrganizationId, useTabs } from "../../hooks";
import { useTasks } from "../../hooks/useTasks";
import { completedTaskComparitor, todoTaskComparitor } from "../../util/tasks";

const filterForTab = (tab: TasksTab): TaskFilter => {
  if (tab == "todo") {
    return "active";
  }
  if (tab == "completed") {
    return "complete";
  }
  return "active";
};

type TasksTab = "todo" | "completed";
type Props = {
  filter: TasksTab;
};

type PageProps = RouteComponentProps & Props;

export const Listing = ({ filter: defaultTab }: PageProps) => {
  const primaryOrgId = usePrimaryOrganizationId();

  const [, , refreshOrg] = useOrganization(primaryOrgId);

  const history = useHistory();
  const [tab, tabs] = useTabs(
    [
      { label: "To-Do", value: "todo" },
      { label: "Completed", value: "completed" },
    ],
    defaultTab,
    null,
    (newVal) => {
      const url = new URL(location.toString());
      url.pathname = url.pathname.split("/").slice(0, -1).concat(newVal).join("/");
      history.push(url.href.replace(url.origin, ""));
      refreshOrg();
    },
  );

  const filter = filterForTab(tab as TasksTab);
  const [tasks, tasksRequest, _refreshTasks, handleComplete] = useTasks(primaryOrgId, filter);

  return (
    <StandardPageWrapper requests={[tasksRequest]}>
      <PageHeader titleContent="Task List" />

      <div className="mt-md mb-xl">{tabs}</div>

      {filter == "active" && <TodoTasks tasks={tasks} onComplete={handleComplete} />}
      {filter == "complete" && <CompletedTasks tasks={tasks} />}
    </StandardPageWrapper>
  );
};

type TodoTaskProps = {
  tasks: TaskDto[];
  onComplete: (task: TaskDto) => Promise<void>;
};

const TodoTasks: React.FC<TodoTaskProps> = ({ tasks, onComplete }) => {
  const sortedTasks = useMemo(() => tasks?.sort(todoTaskComparitor), [tasks]);

  if (!tasks?.length) {
    return <EmptyTodoTasks />;
  }

  return (
    <>
      <p className="mb-xl">
        Complete the tasks below. Once completed, they will be moved to the Completed tab.
      </p>

      {sortedTasks.map((t) => (
        <div className="mb-md" key={t.id}>
          <TaskCard task={t} onComplete={onComplete} />
        </div>
      ))}
    </>
  );
};

type CompletedTasksProps = {
  tasks: TaskDto[];
};

const CompletedTasks: React.FC<CompletedTasksProps> = ({ tasks }) => {
  return (
    <>
      {tasks.length > 0 && <p>Here are all the tasks you have completed.</p>}
      <CompletedTasksTable rows={tasks} />
    </>
  );
};

const COMPLETED_TASK_COLUMNS: ColumnDefinition<TaskDto>[] = [
  {
    heading: "Task",
    // eslint-disable-next-line react/display-name
    accessor: (r) => <span style={{ fontWeight: 500 }}>{r.name}</span>,
  },
  {
    heading: "Completed By",
    accessor: (r) => Fmt.User.fmtFullName(r.completedBy),
  },
  {
    heading: "Completed On",
    accessor: (r) => Fmt.Date.fmtDate(r.completedAt),
  },
];

type TasksTableProps = {
  rows: TaskDto[];
};

const CompletedTasksTable: React.FC<TasksTableProps> = ({ rows }) => {
  const tasks = useMemo(() => rows.sort(completedTaskComparitor), [rows]);

  if (rows.length === 0) {
    return <p className="empty-state-message">You have no completed tasks.</p>;
  }

  return <TileTable table={{ columns: COMPLETED_TASK_COLUMNS, data: tasks }} variant="filled" />;
};
