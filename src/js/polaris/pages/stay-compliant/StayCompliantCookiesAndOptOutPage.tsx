import { Link } from "@mui/material";
import React from "react";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import {
  CookieConsentListing,
  useCookieConsentListing,
} from "../../components/organisms/cookies/CookieConsentListing";
import {
  CookieScanner,
  CookieScannerProps,
  useCookieScanner,
} from "../../components/organisms/cookies/CookieScanner";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { useTabs } from "../../hooks";

export const StayCompliantCookiesAndOptOut = () => {
  // header tabs
  const [tab, tabs] = useTabs(
    [
      { label: "Consent Records", value: "records" },
      { label: "Cookie Scanner", value: "scanner" },
    ],
    "records",
  );

  const consentListing = useCookieConsentListing();
  const scanner = useCookieScanner();

  return (
    <StandardPageWrapper
      requests={[
        consentListing.requests.cookieConsent,
        scanner.requests.cookieScanRequest,
        scanner.requests.cookieRecommendationsRequest,
        scanner.requests.organizationDataRecipientsRequest,
        scanner.requests.vendorCatalogRequest,
      ]}
    >
      <div className="mt-md">{tabs}</div>
      {tab == "records" && <CookieConsentListing {...consentListing.props} />}
      {tab == "scanner" && <CookieScannerTab {...scanner.props} />}
    </StandardPageWrapper>
  );
};

const CookieScannerTab: React.FC<CookieScannerProps> = (props) => {
  const instructions = (
    <Paras>
      <Para />
      <Para>
        Add your website URL to get a list of your cookies. Designate a “Type” and “Data Recipient”
        for each cookie.{" "}
        <Link href="https://polaris.truevault.com/notices/publish" target="_blank" rel="noreferrer">
          Click here
        </Link>{" "}
        to access developer instructions for implementing your cookie settings with our consent
        management tool.
      </Para>
    </Paras>
  );

  return (
    <>
      <CookieScanner {...props} showDeveloperInstructions={false} instructions={instructions} />
    </>
  );
};
