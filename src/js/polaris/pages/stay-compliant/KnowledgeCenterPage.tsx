import React, { useState } from "react";
import { PageWrapper } from "../../../common/layout/PageWrapper";
import { OmniBarProvider } from "../../../common/hooks/useOmniBar";
import { OmniBar } from "../../components/dialog/OmniBar";
import { StayCompliantNav } from "../../components/layout/nav/StayCompliantNav";
import { PageLoading } from "../../../common/components/Loading";
import clsx from "clsx";

export const KnowledgeCenterPage = () => {
  const [loading, setLoading] = useState(true);

  return (
    <PageWrapper>
      <OmniBarProvider>
        <OmniBar></OmniBar>
        <StayCompliantNav />
        <div className={clsx("page-content")}>
          {loading && <PageLoading />}
          <iframe
            src="https://knowledge.truevault.com"
            title={"TrueVault Knowledge Center"}
            onLoad={() => setLoading(false)}
            style={{
              display: loading ? "none" : "block",
              height: "100vh",
              width: "calc(100vw - 272px)",
              border: "none",
            }}
          />
        </div>
      </OmniBarProvider>
    </PageWrapper>
  );
};
