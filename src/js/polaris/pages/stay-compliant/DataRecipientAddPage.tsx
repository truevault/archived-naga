import React, { useEffect, useMemo, useState } from "react";
import { useHistory } from "react-router-dom";
import { PageLoading } from "../../../common/components/Loading";
import { useDataRecipientsCatalog } from "../../../common/hooks";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../common/service/server/Types";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { VendorSearchContents } from "../../components/VendorSearchContents";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { RouteHelpers, Routes } from "../../root/routes";
import { CustomVendorFormValues } from "../get-compliant/data-recipients/AddDataRecipients";
import { GetCompliantContainer } from "../get-compliant/GetCompliantContainer";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

const AddHeader: React.FC = () => {
  return <PageHeader titleHeaderContent={"Data Recipients"} titleContent={"Add Vendor"} />;
};

export const DataRecipientAdd = () => {
  const organizationId = usePrimaryOrganizationId();
  const history = useHistory();

  // State
  const [readyToRender, setReadyToRender] = useState(false);
  const [, setOrgVendorsMap] = useState<Map<UUIDString, OrganizationDataRecipientDto>>(null);
  const [enabledVendors, setEnabledVendors] = useState<Set<UUIDString>>(null);
  const [selectedVendor, setSelectedVendor] = useState<UUIDString>(null);

  // Content
  const [currentVendors, vendorsRequest] = useOrganizationVendors(organizationId);
  const [allVendors, allVendorsRequest] = useDataRecipientsCatalog(organizationId);

  const handleAddVendor = (vendorId: UUIDString) => {
    setSelectedVendor(vendorId);
  };

  const handleAddCustomVendor = (customVendor: CustomVendorFormValues) => {
    history.push({
      pathname: RouteHelpers.dataRecipients.addDetail("custom"),
      search: Object.entries(customVendor)
        .reduce((acc, [key, val]) => {
          if (val) {
            acc.set(key, val);
          }
          return acc;
        }, new URLSearchParams())
        .toString(),
    });
  };

  const allVendorsMap = useMemo(
    () => allVendors && new Map(allVendors.map((v) => [v.id, v])),
    [allVendors],
  );

  // Effects
  useEffect(() => {
    if (vendorsRequest.success && allVendorsRequest.success) {
      const vendors = currentVendors.filter((v) => v.dataRecipientType == "vendor");
      setEnabledVendors(new Set(vendors.map(({ vendorId }) => vendorId)));
      setOrgVendorsMap(
        vendors.reduce(
          (acc, ov) => acc.set(ov.vendorId, ov),
          new Map<UUIDString, OrganizationDataRecipientDto>(),
        ),
      );
      setReadyToRender(true);
    }
  }, [vendorsRequest, allVendorsRequest, currentVendors]);

  if (!readyToRender) {
    return (
      <StandardPageWrapper>
        <AddHeader />
        <PageLoading />
      </StandardPageWrapper>
    );
  }

  return (
    <StandardPageWrapper
      footer={
        <Footer
          onDone={() => {
            history.push(RouteHelpers.dataRecipients.addDetail(selectedVendor));
          }}
          nextDisabled={!selectedVendor}
        />
      }
    >
      {
        <>
          <AddHeader />

          <GetCompliantContainer className="request-instructions" wide={true}>
            <div className="add-vendors--search-container">
              <h2 className="add-vendors--search-container-header">Search for a vendor</h2>
              <VendorSearchContents
                allVendorsMap={allVendorsMap}
                enabledVendors={enabledVendors}
                defaultVendors={[]}
                type="vendor"
                context={["CONSUMER"]}
                handleAddVendor={handleAddVendor}
                handleAddCustomVendor={handleAddCustomVendor}
                selectedVendor={selectedVendor}
                variant="radio"
              />
            </div>
          </GetCompliantContainer>
        </>
      }
    </StandardPageWrapper>
  );
};

const Footer = ({ onDone, nextDisabled }) => {
  const history = useHistory();
  return (
    <ProgressFooter
      actions={
        <ProgressFooterActions
          nextContent="Next"
          nextOnClick={onDone}
          nextDisabled={nextDisabled}
          prevContent="Cancel"
          prevOnClick={() => history.push(Routes.dataRecipients.root)}
        />
      }
    />
  );
};
