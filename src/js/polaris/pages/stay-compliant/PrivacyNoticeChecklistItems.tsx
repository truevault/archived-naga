import { Done, FileCopyOutlined, Print } from "@mui/icons-material";
import React, { useEffect, useState } from "react";
import { ExternalLink } from "../../../common/components/ExternalLink";
import { NetworkRequest } from "../../../common/models";
import { OrganizationCookieDto } from "../../../common/service/server/dto/CookieScanDto";
import {
  EmploymentGroupNotices,
  PublicOrganizationNoticeDto,
  PublicPrivacyCenterDto,
} from "../../../common/service/server/dto/PublicOrganizationNoticeDto";
import { OrganizationPublicId } from "../../../common/service/server/Types";
import { TabPanel } from "../../components/Tabs";
import { DeveloperCookieTable } from "../../components/organisms/cookies/DeveloperCookieTable";
import { SurveyGateCollapse } from "../../components/organisms/survey/SurveyGateCollapse";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { usePrimaryOrganization, useTabs } from "../../hooks";
import { usePublicOrganizationNotices } from "../../hooks/useOrganization";
import { frontendPathToUrl, RouteHelpers, Routes } from "../../root/routes";
import { PrivacyCenterUrls } from "../../util/privacyCenterUtil";
import { addProtocolIfNotExists } from "../../util/text";
import { PrivacyNoticeActions } from "../../components/organisms/privacy-notice/Foo";
import { OptOutIconSelector } from "../../components/organisms/OptOutIconSelector";

type UsePrivacyNoticeChecklistItems = {
  checklistProps: PrivacyNoticeChecklistItemsProps;
  ready: boolean;
};

export const usePrivacyNoticeChecklistItems = (
  orgId: OrganizationPublicId,
): UsePrivacyNoticeChecklistItems => {
  const [ready, setReady] = useState(false);

  // network fetches
  const [notices, noticesRequest] = usePublicOrganizationNotices(orgId);

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(noticesRequest)) {
      setReady(true);
    }
  }, [noticesRequest]);

  return {
    checklistProps: { notices },
    ready,
  };
};

type InstructionsProps = {
  dev: boolean;
  hr: boolean;
};
const InstructionsLinks: React.FC<InstructionsProps> = ({ dev, hr }) => {
  const [org] = usePrimaryOrganization();
  return (
    <>
      {dev && (
        <>
          <h4>Developer Instructions</h4>
          <UrlNoticeAction
            url={frontendPathToUrl(RouteHelpers.instructions.id(org.devInstructionsId))}
          />
        </>
      )}

      {hr && (
        <>
          <h4>HR Instructions</h4>
          <UrlNoticeAction
            url={frontendPathToUrl(RouteHelpers.instructions.id(org.hrInstructionsId))}
          />
        </>
      )}
    </>
  );
};

export type PrivacyNoticeChecklistItemsProps = {
  notices: PublicOrganizationNoticeDto;
  instructions?: boolean;
  visible?: PublishNoticesTabs[];
};

export type PublishNoticesTabs = "web" | "cookie" | "mobile" | "in-store" | "employment";

type TabOption = { label: string; value: PublishNoticesTabs };

export const PrivacyNoticeChecklistItems: React.FC<PrivacyNoticeChecklistItemsProps> = ({
  notices,
  instructions = true,
  visible = ["web", "cookie", "mobile", "in-store", "employment"],
}) => {
  const {
    privacyCenter,
    isGoogleAnalytics,
    isGdpr,
    isSelling,
    isSharing,
    showFinancialIncentive,
    showLimitNotice,
    hasJobApplicants,
    hasPhysicalLocation,
    hasMobileApplication,
    employmentGroupNotices: employmentGroups,
  } = notices;

  const empNotices = (employmentGroups && employmentGroups.length > 0) || hasJobApplicants;

  const tabOptions: TabOption[] = [];

  if (visible.includes("web")) {
    tabOptions.push({ label: "Web Notices", value: "web" });
  }

  if (hasMobileApplication && visible.includes("mobile")) {
    tabOptions.push({ label: "Mobile Notices", value: "mobile" });
  }

  if (isGdpr && visible.includes("cookie")) {
    tabOptions.push({ label: "Cookie Notices", value: "cookie" });
  }

  if (hasPhysicalLocation && visible.includes("in-store")) {
    tabOptions.push({ label: "In Store Notice", value: "in-store" });
  }

  if (empNotices && visible.includes("employment")) {
    tabOptions.push({ label: "Employment Notices", value: "employment" });
  }

  const first = tabOptions.length > 0 ? tabOptions[0].value : "web";

  const [tab, tabs] = useTabs(tabOptions, first);
  return (
    <>
      {instructions && <InstructionsLinks dev={true} hr={empNotices || hasPhysicalLocation} />}

      <div className="mt-xl">{tabs}</div>

      <TabPanel value="web" tab={tab}>
        <WebInstructions
          isGoogleAnalytics={isGoogleAnalytics}
          isGdpr={isGdpr}
          isSelling={isSelling}
          isSharing={isSharing}
          privacyCenter={privacyCenter}
          showFinancialIncentive={showFinancialIncentive}
          showLimitNotice={showLimitNotice}
        />
      </TabPanel>

      {empNotices && (
        <TabPanel value="employment" tab={tab}>
          <EmploymentNotices
            privacyCenter={privacyCenter}
            hasJobApplicants={hasJobApplicants}
            employmentGroups={employmentGroups}
          />
        </TabPanel>
      )}

      {hasMobileApplication && (
        <TabPanel value="mobile" tab={tab}>
          <MobileNotices
            privacyCenter={privacyCenter}
            isSelling={isSelling}
            isSharing={isSharing}
          />
        </TabPanel>
      )}

      {isGdpr && (
        <TabPanel value="cookie" tab={tab}>
          <CookieNotices
            cookies={notices.cookies}
            scanUrl={notices.cookieScanUrl ?? "your website"}
          />
        </TabPanel>
      )}

      {hasPhysicalLocation && (
        <TabPanel value="in-store" tab={tab}>
          <InStoreNotices privacyCenter={privacyCenter} />
        </TabPanel>
      )}
    </>
  );
};

type MobileNoticesProps = {
  privacyCenter: PublicPrivacyCenterDto;

  isSelling: boolean;
  isSharing: boolean;
};

const MobileNotices: React.FC<MobileNoticesProps> = ({ privacyCenter, isSelling, isSharing }) => {
  return (
    <div className="compliance-checklist--notices">
      <div className="compliance-checklist--notices-items">
        <ol className="custom p-0">
          <PrivacyNoticeTask
            description={
              <>
                On the mobile app’s download page (Apple App store, Google Play), update the link to
                your business’s Privacy Policy
              </>
            }
          >
            <UrlNoticeAction url={urlWithProtocol(privacyCenter.url + "/privacy-policy")} />
          </PrivacyNoticeTask>

          <PrivacyNoticeTask
            description={<>Update app description to include other privacy notices</>}
            helpText={
              <>Paste the text below into the app description on your app’s download page.</>
            }
          >
            <PrivacyNoticeActions
              content={
                <span className="text-t1 text-font-mono" style={{ whiteSpace: "pre-wrap" }}>
                  {getMobilePrivacyNoticesText(
                    PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter),
                    privacyCenter,
                    isSelling,
                    isSharing,
                  )}
                </span>
              }
              actions={[
                getCopyAction(
                  "update-mobile-app-description-privacy-notices-copy-text",
                  "copy text",
                  getMobilePrivacyNoticesText(
                    PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter),
                    privacyCenter,
                    isSelling,
                    isSharing,
                  ),
                ),
              ]}
            />
          </PrivacyNoticeTask>
          <PrivacyNoticeTask
            description={<>Within your business’s mobile app, post (or update) the links below</>}
            helpText={
              <>
                For example, add links to each notice in the app’s settings menu. Title the links
                using the exact language below.
              </>
            }
          >
            <UrlNoticeAction
              label="California Privacy Notice"
              url={PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter)}
            />
            <UrlNoticeAction url={urlWithProtocol(privacyCenter.url + "/privacy-policy")} />

            {showOptOutNotice(isSelling, isSharing) && (
              <UrlNoticeAction
                url={PrivacyCenterUrls.getOptOutUrl(privacyCenter, isSelling, isSharing)}
                label={optOutLabel()}
              />
            )}
          </PrivacyNoticeTask>
        </ol>
      </div>
    </div>
  );
};

type CookieNoticeProps = {
  cookies: OrganizationCookieDto[];
  scanUrl?: string;
};
const CookieNotices: React.FC<CookieNoticeProps> = ({ cookies, scanUrl }) => {
  const nonGtmExampleHtml = getNonGtmExampleHtml();
  return (
    <div className="compliance-checklist--notices">
      <div className="compliance-checklist--notices-items">
        <ol className="custom p-0">
          <PrivacyNoticeTask
            description="Configure Cookie Consent"
            helpText={
              <Paras>
                <Para paragraph className="text-t1">
                  European e-privacy laws require consent for all website cookies beyond those that
                  are strictly necessary for the operation of your website. Below are a list of
                  cookies identified on <code>{scanUrl}</code> and the category they belong to.
                  Follow the applicable instructions below to update your site such that each cookie
                  listed does not load unless the user has consented to the category of the cookie.
                </Para>
                <SurveyGateCollapse label="See cookies">
                  <DeveloperCookieTable cookies={cookies} />
                </SurveyGateCollapse>
              </Paras>
            }
          >
            <Paras>
              <NoticeSubHeading>Using Google Tag Manager?</NoticeSubHeading>
              <Para paragraph className="text-t1">
                If you are using Google Tag Manager, follow{" "}
                <ExternalLink className="text-link" href={GTM_COOKIE_URL} target="_blank">
                  these instructions
                </ExternalLink>{" "}
                to integrate TrueVault’s consent management platform and configure the consent
                requirements for your tags.
              </Para>

              <NoticeSubHeading>Not Using Google Tag Manager?</NoticeSubHeading>
              <Para paragraph className="text-t1">
                If you are not using Google Tag Manager, configure consent on all relevant script
                tags by adding a data attribute <strong>data-polaris-consent</strong> with a
                comma-separate list of values representing all the consent flags required to execute
                the script. The supported consent flags are <strong>essential</strong>,{" "}
                <strong>marketing</strong>, <strong>preferences</strong>, and{" "}
                <strong>statistics</strong>.
              </Para>
              <PrivacyNoticeActions
                content={
                  <span className="text-t1 text-font-mono text-pre-wrap">{nonGtmExampleHtml}</span>
                }
                actions={[
                  getCopyAction("add-dns-link-to-website-script", "copy HTML", nonGtmExampleHtml),
                ]}
              />
              <Para className="text-t1 mt-lg">
                <strong>Note:</strong> To ensure that Polaris can scan and intercept your scripts
                before they are executed, do not add the "defer" attribute to the{" "}
                <strong>polaris.js</strong> script
              </Para>
            </Paras>
          </PrivacyNoticeTask>

          <PrivacyNoticeTask
            description="Customize Cookie Consent Widget (Optional)"
            helpText={
              <Paras>
                <Para paragraph className="text-t1">
                  Our Cookie Consent Widget comes with a default set of styles out of the box. If
                  you’d like to customize the look and feel of the Cookie Consent Widget, you can
                  find documentation on the available options in{" "}
                  <ExternalLink
                    className="text-link"
                    href={CUSTOMIZE_COOKIE_CONSENT_URL}
                    target="_blank"
                  >
                    this help article
                  </ExternalLink>
                  .
                </Para>
              </Paras>
            }
          />
        </ol>
      </div>
    </div>
  );
};

type InStoreNoticesProps = {
  privacyCenter: PublicPrivacyCenterDto;
};

const InStoreNotices: React.FC<InStoreNoticesProps> = ({ privacyCenter }) => {
  return (
    <div className="compliance-checklist--notices">
      <div className="compliance-checklist--notices-items">
        <ol className="custom p-0">
          <PrivacyNoticeTask
            description="Post California Privacy Notice at consumer-facing stores or offices"
            helpText={
              <>
                1. Post prominent signage directing consumers to the URL where your business's
                online California Privacy Notice can be found, OR
                <br />
                2. Offer paper copies of your business's California Privacy Notice at your
                business's store or office
              </>
            }
          >
            <UrlNoticeAction
              url={PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter)}
              label="California Privacy Notice"
              printUrl={Routes.getCompliant.privacyNotices.caPrivacyNotice + "?print=1"}
            />
          </PrivacyNoticeTask>
          <PrivacyNoticeTask
            description="Add California Privacy Notice to any other offline points of collection"
            helpText={
              <>
                Add the California Privacy Notice URL or the actual text of the notice to all other
                offline places where your business collects information from consumers in
                California, such as on paper forms.
              </>
            }
          >
            <UrlNoticeAction
              url={PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter)}
              label="California Privacy Notice"
              printUrl={Routes.getCompliant.privacyNotices.caPrivacyNotice + "?print=1"}
            />
          </PrivacyNoticeTask>
        </ol>
      </div>
    </div>
  );
};

type EmploymentNoticesProps = {
  privacyCenter: PublicPrivacyCenterDto;

  employmentGroups: EmploymentGroupNotices[];
  hasJobApplicants: boolean;
};
const EmploymentNotices: React.FC<EmploymentNoticesProps> = ({
  privacyCenter,
  employmentGroups,
  hasJobApplicants,
}) => {
  return (
    <>
      <div className="compliance-checklist--notices">
        <div className="compliance-checklist--notices-items">
          <ol className="custom p-0">
            {employmentGroups.length > 0 && (
              <PrivacyNoticeTask
                description="Post each Employment Notice at point of collection"
                helpText={
                  <>
                    Include each notice at or before the point where your business collects personal
                    information from California employees, such as accompanying an offer letter;
                    included with onboarding materials; as an addendum or enclosure with a contract
                    (e.g., for contractors); or in an employee handbook.
                  </>
                }
              >
                {employmentGroups
                  .sort((a, b) => a.name.localeCompare(b.name))
                  .map((cg) => {
                    return (
                      <UrlNoticeAction key={cg.name} url={cg.url} label={`Notice to ${cg.name}`} />
                    );
                  })}
              </PrivacyNoticeTask>
            )}
            {hasJobApplicants && (
              <PrivacyNoticeTask
                description="Link to Job Applicant Notice at point of collection"
                helpText={
                  <>
                    Add a "Notice to California Job Applicants" link where your business collects
                    personal information from job applicants in California, such as where it posts
                    hiring ads online.
                  </>
                }
              >
                <UrlNoticeAction
                  url={PrivacyCenterUrls.getJobApplicantsNoticeUrl(privacyCenter)}
                  label="Notice to California Job Applicants"
                />
              </PrivacyNoticeTask>
            )}
          </ol>
        </div>
      </div>
    </>
  );
};

type WebInstructionsProps = {
  privacyCenter: PublicPrivacyCenterDto;
  isSelling: boolean;
  isGdpr: boolean;
  isSharing: boolean;
  isGoogleAnalytics: boolean;
  showFinancialIncentive: boolean;
  showLimitNotice: boolean;
};

const GTM_COOKIE_URL: string =
  "https://help.truevault.com/article/165-setting-up-consent-management-in-google-tag-manager";

const CUSTOMIZE_COOKIE_CONSENT_URL: string =
  "https://help.truevault.com/article/167-how-to-customize-your-gdpr-consent-tool";

const WebInstructions: React.FC<WebInstructionsProps> = ({
  privacyCenter,
  isGdpr,
  isSelling,
  isSharing,
  isGoogleAnalytics,
  showFinancialIncentive,
  showLimitNotice,
}) => {
  const polarisJsHtml = getPolarisHtml(isGoogleAnalytics, isGdpr, privacyCenter.id);

  return (
    <div className="compliance-checklist--notices">
      <div className="compliance-checklist--notices-items">
        <ol className="custom p-0">
          <PrivacyNoticeTask
            description="Update all Privacy Policy links on your website"
            helpText="Edit all Privacy Policy links on your business's website so that they link to the new Privacy Policy."
          >
            <UrlNoticeAction url={privacyCenter.url + "/privacy-policy"} />
          </PrivacyNoticeTask>
          <PrivacyNoticeTask
            description="Add Polaris Client-Side JavaScript to your website"
            helpText={
              <>
                <div className="text-t1">
                  Add the following JavaScript in the top of the <code>&#60;head&#62;</code> element
                  of your business's website. It must be the{" "}
                  <strong>first functional script</strong> (important: if you use Google Tag
                  Manager, place the Polaris script above the GTM script).
                </div>

                <div className="text-t1 mt-lg">
                  This script is required for Polaris to function properly. It performs several
                  critical functions including limiting the visibility of state-specific links and
                  notices on your business's website so that visitors from states with privacy laws
                  will see the notices that apply to them.
                </div>

                {isGoogleAnalytics && (
                  <>
                    <div className="text-t1 mt-lg">
                      <strong>Note:</strong> If you are using Google Analytics 4, replace
                      G-XXXXXXXXXX with your Google Analytics Measurement ID (starts with G-). If
                      you are using previous versions of Google Analytics, replace G-XXXXXXXXXX with
                      your Web Property ID (starts with UA-).
                    </div>
                    <div className="text-t1">
                      <strong>Note:</strong> To ensure that Polaris can evaluate GA opt-outs prior
                      to the Google Analytics script getting executed, do not add the "defer"
                      attribute to the <strong>polaris.js</strong> script.
                    </div>
                  </>
                )}
              </>
            }
          >
            <PrivacyNoticeActions
              content={
                <span className="text-t1 text-font-mono text-pre-wrap">{polarisJsHtml}</span>
              }
              actions={[
                getCopyAction("add-dns-link-to-website-script", "copy HTML", polarisJsHtml),
              ]}
            />
          </PrivacyNoticeTask>
          <PrivacyNoticeTask
            description="Add California Privacy Notice to website footer"
            helpText={
              <>
                Place the script below where you'd like the link to the{" "}
                <ExternalLink
                  className="text-link text-medium"
                  href={PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter)}
                  target="_blank"
                >
                  California Privacy Notice
                </ExternalLink>{" "}
                to appear. The link is typically placed in the website footer near the Privacy
                Policy link.
              </>
            }
          >
            <PrivacyNoticeActions
              content={
                <span className="text-t1 text-font-mono">
                  {getCaPrivacyNoticeHtml(PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter))}
                </span>
              }
              actions={[
                getCopyAction(
                  "add-ca-privacy-notice-website-home-script",
                  "copy HTML",
                  getCaPrivacyNoticeHtml(PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter)),
                ),
              ]}
            />
          </PrivacyNoticeTask>
          <PrivacyNoticeTask
            description="Add California Privacy Notice to any other points of collection on website"
            helpText={
              <>
                Points on a website where a{" "}
                <ExternalLink
                  className="text-link text-medium"
                  href={PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter)}
                  target="_blank"
                >
                  California Privacy Notice
                </ExternalLink>{" "}
                link should be added include pages where people can sign up for emails, create an
                account, make a purchase (checkout), or fill out a form.
              </>
            }
          >
            <PrivacyNoticeActions
              content={
                <span className="text-t1 text-font-mono">
                  {getCaPrivacyNoticeHtml(PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter))}
                </span>
              }
              actions={[
                getCopyAction(
                  "add-ca-privacy-notice-website-collection-script",
                  "copy HTML",
                  getCaPrivacyNoticeHtml(PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter)),
                ),
              ]}
            />
          </PrivacyNoticeTask>

          {showFinancialIncentive && (
            <PrivacyNoticeTask
              description="Add Notice of Financial Incentive to opt-in points"
              helpText={
                "Post the Notice of Financial Incentive link at all points on your website where a consumer can opt-in to the financial incentive or price or service difference described in the Notice."
              }
            >
              <PrivacyNoticeActions
                content={
                  <span className="text-t1 text-mono">
                    {getIncentiveNoticeHtml(privacyCenter.url)}
                  </span>
                }
                actions={[
                  getCopyAction(
                    "add-ca-privacy-notice-website-collection-script",
                    "copy HTML",
                    getIncentiveNoticeHtml(privacyCenter.url),
                  ),
                ]}
              />
            </PrivacyNoticeTask>
          )}
          <SellingOrSharingOptOut
            privacyCenter={privacyCenter}
            isSelling={isSelling}
            isSharing={isSharing}
          />
          {showLimitNotice && (
            <PrivacyNoticeTask
              description="Add Sensitive Personal Information link to website footer"
              helpText={
                <>
                  Place the link below where you’d like the{" "}
                  <ExternalLink
                    className="text-link text-medium"
                    href={PrivacyCenterUrls.getLimitUrl(privacyCenter)}
                    target="_blank"
                  >
                    Limit the Use of My Sensitive Personal Information
                  </ExternalLink>{" "}
                  link to appear. The link is typically placed in the footer near the Privacy Policy
                  link. Do not change the link text. The link will be visible only to California web
                  visitors.
                </>
              }
            >
              <PrivacyNoticeActions
                content={
                  <span className="text-t1 text-font-mono">
                    {getLimitNoticeHtml(PrivacyCenterUrls.getLimitUrl(privacyCenter))}
                  </span>
                }
                actions={[
                  getCopyAction(
                    "add-ca-limit-footer-link",
                    "copy HTML",
                    getLimitNoticeHtml(PrivacyCenterUrls.getLimitUrl(privacyCenter)),
                  ),
                ]}
              />
            </PrivacyNoticeTask>
          )}
          <PrivacyNoticeTask
            description="Verify Privacy Policy link displays on a mobile device"
            helpText={
              "Visit your business's website on a mobile device and check that the Privacy " +
              "Policy link is visible."
            }
          />
        </ol>
      </div>
    </div>
  );
};

const showOptOutNotice = (isSelling: boolean, isSharing: boolean) => isSelling || isSharing;

const optOutLabel = () => "Your Privacy Choices";

const SellingOrSharingOptOut = ({ privacyCenter, isSelling, isSharing }) => {
  const [html, setHtml] = useState(null);

  if (!isSelling && !isSharing) {
    return null;
  }

  const linkCopy = optOutLabel();
  const link = PrivacyCenterUrls.getOptOutUrl(privacyCenter, isSelling, isSharing);

  return (
    <PrivacyNoticeTask
      description="Add Opt-Out Notice to website footer"
      helpText={
        <>
          Place the link below where you’d like the{" "}
          <ExternalLink className="text-link text-medium" href={link} target="_blank">
            {linkCopy}
          </ExternalLink>{" "}
          link to appear. The link is typically placed in the footer near the Privacy Policy link.
          You may change the style of the link, but do not change the link text. The link will be
          visible only to California web visitors.
        </>
      }
    >
      <OptOutIconSelector linkUrl={link} linkCopy={linkCopy} onUpdate={setHtml} />
      <PrivacyNoticeActions
        content={<span className="text-t1 text-font-mono text-pre-wrap">{html}</span>}
        actions={[getCopyAction("add-opt-out-notice-website-home-script", "copy HTML", html)]}
      />
    </PrivacyNoticeTask>
  );
};

const urlWithProtocol = (url: string) =>
  addProtocolIfNotExists(url, window.location.protocol.slice(0, -1));

const POLARIS_JS_HTML_START = "<!-- Start of TrueVault Polaris Code Block -->\n";

const POLARIS_JS_HTML_END =
  '<script src="https://polaris.truevaultcdn.com/static/polaris.js"></script>\n' +
  "<!-- End of TrueVault Polaris Code Block -->";

const getPolarisHtml = (
  isGoogleAnalytics: boolean,
  isGdpr: boolean,
  privacyCenterId: string,
): string => {
  return (
    POLARIS_JS_HTML_START +
    getPolarisOptions(isGoogleAnalytics, isGdpr, privacyCenterId) +
    POLARIS_JS_HTML_END
  );
};

const getNonGtmExampleHtml = (): string => {
  return `<!-- external script examples -->
<script data-polaris-consent="statistics" src="statistics-consent-required.js"></script>
<script data-polaris-consent="essential,preferences" src="essential-and-preferences-consent-required.js"></script>

<!-- inline script example without consent -->
<script type="text/javascript">
  console.log("I don't need any consent to run")
</script>

<!-- inline script example with consent -->
<script type="text/javascript" data-polaris-consent="essential">
  console.log("I need essential consent to run")
</script>
<script type="text/javascript" data-polaris-consent="statistics,marketing">
  console.log("I need both statistics and marketing consent to run")
</script>
 `;
};

const POLARIS_JS_OPTIONS_START = "<script>\n" + "  window.polarisOptions = {\n";
const POLARIS_JS_OPTIONS_END = "\n" + "  };\n" + "</script>\n";
const getPolarisOptions = (
  isGoogleAnalytics: boolean,
  isGdpr: boolean,
  privacyCenterId: string,
): string => {
  let options: string[] = [];

  if (isGoogleAnalytics) {
    options.push('GoogleAnalyticsTrackingId: "G-XXXXXXXXXX"');
  }

  if (isGdpr) {
    options.push("enableConsentManager: true");
    options.push(`privacyCenterId: "${privacyCenterId}"`);
  }

  const formattedOptions = options.map((o) => `    ${o},`).join("\n");

  if (options.length > 0) {
    return POLARIS_JS_OPTIONS_START + formattedOptions + POLARIS_JS_OPTIONS_END;
  }

  return "";
};

const getCaPrivacyNoticeHtml = (url: string) =>
  `<a class="truevault-polaris-privacy-notice" href="${url}" noreferrer noopener hidden>California Privacy Notice</a>`;

export const getIncentiveNoticeHtml = (url: string) =>
  `<a class="truevault-polaris-privacy-notice" href="${url}/privacy-policy#financial-incentive" noreferrer noopener hidden>Notice of Financial Incentive</a>`;

const getLimitNoticeHtml = (url: string) =>
  `<a class="truevault-polaris-privacy-notice" href="${url}" noreferrer noopener hidden>Sensitive Personal Information</a>`;

const getMobilePrivacyNoticesText = (
  caPrivacyNoticeUrl: string,
  privacyCenter: PublicPrivacyCenterDto,
  isSelling: boolean,
  isSharing: boolean,
) => {
  const caNotice = `California Privacy Notice:\n${caPrivacyNoticeUrl}\n\n`;

  const showOptOut = showOptOutNotice(isSelling, isSharing);
  const label = optOutLabel();
  const link = PrivacyCenterUrls.getOptOutUrl(privacyCenter, isSelling, isSharing);
  const dnsNotice = showOptOut ? `${label}:\n${link}\n\n` : "";
  return `${caNotice}${dnsNotice}See our Privacy Policy to access ${
    showOptOut ? "these links" : "this link"
  }.`;
};

export type PrivacyNoticeChecklistNoticeItemAction = {
  id: string;
  content: React.ReactNode;
  onClick: () => void;
};

export const getCopyAction = (
  id: string | undefined,
  text: string,
  copyText: string,
): PrivacyNoticeChecklistNoticeItemAction => {
  return {
    id,
    content: (
      <>
        <span className="compliance-checklist--copy-done">
          <Done />{" "}
        </span>
        <span className="compliance-checklist--copy-copy">
          <FileCopyOutlined />
        </span>
        {text}
      </>
    ),
    onClick: () => {
      navigator.clipboard.writeText(copyText);
      document.getElementById(id)?.classList.add("copy-done");
      setTimeout(() => document.getElementById(id)?.classList.remove("copy-done"), 2000);
    },
  };
};

const getPrintAction = (id: string | undefined, text: string, url: string) => {
  return {
    id: id,
    content: (
      <>
        <Print />
        {text}
      </>
    ),
    onClick: () => window.open(url),
  };
};

type UrlNoticeActionProps = {
  url: string;
  label?: string;
  printUrl?: string;
};

const UrlNoticeAction: React.FC<UrlNoticeActionProps> = ({ url, label, printUrl }) => {
  const actions = [];

  if (printUrl) {
    actions.push(getPrintAction(`print-${url}`, "print view", printUrl));
  }
  actions.push(getCopyAction(`copy-${url}`, "copy URL", url));

  return (
    <PrivacyNoticeActions
      content={<span className="text-t1 text-mono">{label || url}</span>}
      actions={actions}
    />
  );
};

interface PrivacyNoticeTaskProps {
  description: string | React.ReactNode;
  helpText?: string | React.ReactNode;
  children?: React.ReactNode | React.ReactNodeArray;
}
const PrivacyNoticeTask: React.FC<PrivacyNoticeTaskProps> = ({
  description,
  helpText,
  children,
}) => {
  return (
    <li className="compliance-checklist--notices-item">
      <div className="compliance-checklist--notices-item-content">
        <div className="compliance-checklist--notices-item-description">{description}</div>
        <div className="compliance-checklist--notices-item-help-text">{helpText}</div>
        {children && <div className="compliance-checklist--notices-item-actions">{children}</div>}
      </div>
    </li>
  );
};

const NoticeSubHeading: React.FC = ({ children }) => (
  <h4 className={`text-t1 text-spacing-spread text-muted text-weight-regular`}>{children}</h4>
);
