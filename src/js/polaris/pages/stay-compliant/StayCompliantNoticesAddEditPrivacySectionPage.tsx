import React, { useEffect, useMemo, useState } from "react";
import { RouteComponentProps, useLocation } from "react-router";
import { NetworkRequest } from "../../../common/models";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { PageHeaderBack } from "../../components/layout/header/PageHeaderBack";
import { PrivacyPolicySectionEditorWrapper } from "../../components/privacyCenter/PrivacyPolicySectionEditor";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { usePrivacyCenter, usePrivacyPolicy } from "../../hooks/usePrivacyCenter";
import { Routes } from "../../root/routes";

type MatchProps = {
  sectionId: string;
};
type Props = RouteComponentProps<MatchProps> & {
  add: boolean;
};

export const AddEditPrivacySectionPage: React.FC<Props> = ({ match }) => {
  const sectionId = match.params.sectionId;
  const orgId = usePrimaryOrganizationId();
  const { search } = useLocation();
  const query = useMemo(() => new URLSearchParams(search), [search]);

  const before = query.get("before");
  const after = query.get("after");

  const [remotePrivacyCenter, privacyCenterRequest] = usePrivacyCenter(orgId);

  const [remotePrivacyPolicy, privacyPolicyRequest] = usePrivacyPolicy(
    orgId,
    remotePrivacyCenter?.id,
  );

  const [everRendered, setEverRendered] = useState(false);

  // ever-loaded effect
  useEffect(() => {
    if (NetworkRequest.areFinished(privacyCenterRequest, privacyPolicyRequest)) {
      setEverRendered(true);
    }
  }, [privacyCenterRequest, privacyPolicyRequest]);

  const onDone = (sId: string) => {
    // go to the privacy center page with the new param
    const isNew = sId != sectionId;
    const url = isNew ? `${Routes.privacyCenter.root}?new=${sId}` : Routes.privacyCenter.root;
    window.location.href = url;
  };

  const section = useMemo(() => {
    if (sectionId && remotePrivacyPolicy) {
      return remotePrivacyPolicy.policySections.find((s) => s.id == sectionId);
    } else {
      return {
        privacyCenterId: remotePrivacyCenter?.id,
        showHeader: true,
      };
    }
  }, [sectionId, remotePrivacyPolicy, remotePrivacyCenter]);

  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  if (privacyCenterRequest.error || privacyPolicyRequest.error) {
    return (
      <StandardPageWrapper>
        <PageHeader
          titleContent="Uh oh... there was an error loading this section"
          titleHeaderContent={
            <PageHeaderBack to={Routes.privacyCenter.root}>Notices</PageHeaderBack>
          }
        />
      </StandardPageWrapper>
    );
  }

  return (
    <StandardPageWrapper>
      <PageHeader
        titleContent="Privacy Policy"
        titleHeaderContent={<PageHeaderBack to={Routes.privacyCenter.root}>Notices</PageHeaderBack>}
      />

      <PrivacyPolicySectionEditorWrapper
        organizationId={orgId}
        privacyPolicy={remotePrivacyPolicy}
        section={section}
        onClose={onDone}
        before={before}
        after={after}
      />
    </StandardPageWrapper>
  );
};
