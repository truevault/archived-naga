import { KeyboardArrowLeft as KeyboardArrowLeftIcon } from "@mui/icons-material";
import qs from "query-string";
import React, { useMemo } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import { useLocation } from "react-router-dom";
import { useActionRequest } from "../../../common/hooks/api";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { useDataRecipientsCatalog } from "../../../common/hooks/useVendors";
import { Api } from "../../../common/service/Api";
import { UpdateOrganizationSurveyQuestionDto } from "../../../common/service/server/controller/OrganizationController";
import {
  OrganizationDataRecipientDto,
  OrganizationVendorSettingsDto,
} from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import { UUIDString } from "../../../common/service/server/Types";
import { VendorPageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import {
  DataRecipientBasicSettings,
  saveBasicUpdates,
  useDataRecipientBasicSettings,
} from "../../components/organisms/data-recipients/DataRecipientBasicSettings";
import {
  DataRecipientDisclosureSettings,
  saveDisclosureUpdates,
  useDataRecipientDisclosureSettings,
} from "../../components/organisms/data-recipients/DataRecipientDisclosureSettings";
import {
  DataRecipientRetentionSettings,
  saveRetentionUpdates,
  useDataRecipientRetentionSettings,
} from "../../components/organisms/data-recipients/DataRecipientRetentionSettings";
import { Error as RequestsError } from "../../copy/requests";
import { useOrganizationVendors, usePrimaryOrganization } from "../../hooks";
import {
  useAppPlatforms,
  useRequestInstructions,
  usePlatformApps,
  useRetentionReasons,
} from "../../hooks/useOrganizationVendors";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../pages/get-compliant/data-recipients/shared";
import { RouteHelpers } from "../../root/routes";
import { isThirdPartyRecipient } from "../../util/vendors";
import { DataRecipientMultiselect } from "../design/molecules/DataRecipientMultiselect";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

export type UpdateVendorArgs = {
  vendorId: string;
  vendorSettings: OrganizationVendorSettingsDto;
};

type MatchParams = {
  vendorId: UUIDString;
};

type Props = RouteComponentProps<MatchParams>;

const DataRecipientDescription: React.FC<{ dataRecipient: OrganizationDataRecipientDto }> = ({
  dataRecipient,
}) => {
  const descLine = [dataRecipient.category, dataRecipient.url, dataRecipient.email]
    .filter((x) => x)
    .join(" · ");

  return (
    <>
      <p className="text-muted text-t1 my-0">{descLine}</p>
      {dataRecipient.dpaUrl && (
        <p className="text-t1 my-0">
          <a href={dataRecipient.dpaUrl} target="_blank" rel="noopener noreferrer">
            Data Processing Agreement (DPA)
          </a>
        </p>
      )}
    </>
  );
};

export const DataRecipientEdit = ({ match }: Props) => {
  // Variables
  const location = useLocation();
  const history = useHistory();
  const [org] = usePrimaryOrganization();
  const vendorId = match.params.vendorId;

  // State
  const [vendorDetails, vendorDetailsRequest] = useDataRecipientsCatalog(org.id);

  // Requests
  const [dataRecipients, dataRecipientsRequest, refreshDataRecipients] = useOrganizationVendors(
    org.id,
    undefined,
    true,
  );

  const [appPlatforms, appPlatformsRequest] = useAppPlatforms(org.id, vendorId);

  const [platformApps, platformAppsRequest, refreshPlatformApps] = usePlatformApps(
    org.id,
    vendorId,
  );

  const { answers, surveyRequest } = useSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME);
  const [fetchedInstructions, instructionsRequest, refreshInstructions] = useRequestInstructions(
    org.id,
    "DELETE",
  );

  const [retentionReasons, retentionReasonsRequest] = useRetentionReasons();

  const installedApps = useMemo(() => platformApps?.installed ?? [], [platformApps]);
  const dataRecipient = useMemo(
    () => dataRecipients?.find((ov) => ov.vendorId == vendorId),
    [dataRecipients, vendorId],
  );
  const canShowPlatforms = useMemo(
    () =>
      dataRecipient &&
      dataRecipient.isPlatform &&
      !dataRecipient.isStandalone &&
      !isThirdPartyRecipient(dataRecipient),
    [dataRecipient],
  );

  const canShowBasic = dataRecipient && dataRecipient?.isCustom;

  const updateVendor = async (v: UpdateVendorArgs) => {
    const { vendorId, vendorSettings } = v;
    await Api.organizationVendor.addOrUpdateVendor(org.id, vendorId, vendorSettings);
    refreshDataRecipients();
  };

  const updateRemoteInstruction = async (newInstruction: RequestHandlingInstructionDto) => {
    await Api.requestHandlingInstructions.putInstruction(org.id, newInstruction);
    refreshInstructions();
  };

  const { fetch: updateSurveyQuestions } = useActionRequest({
    api: ({ questions }: { questions: UpdateOrganizationSurveyQuestionDto[] }) =>
      Api.organization.updateOrganizationSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME, questions),
    allowConcurrent: true,
  });

  const { fetch: toggleInstall } = useActionRequest({
    api: async ({
      platformId,
      appId,
      included,
    }: {
      platformId: string;
      appId: string;
      included: boolean;
    }) => {
      if (included) {
        await Api.organizationVendor.addPlatformApplication(org.id, platformId, appId);
      } else {
        await Api.organizationVendor.removePlatformApplication(org.id, platformId, appId);
      }
      return refreshPlatformApps();
    },
    messages: {
      forceError: RequestsError.TogglePlatformApp,
    },
  });

  const initial = useMemo(
    () => ({
      name: dataRecipient?.name,
      category: dataRecipient?.category,
      email: dataRecipient?.email,
      url: dataRecipient?.url,
    }),
    [dataRecipient],
  );

  const { state: basicState, updateState: basicUpdate } = useDataRecipientBasicSettings(initial);

  const {
    state: disclosureState,
    meta: disclosureMeta,
    updateState: disclosureUpdate,
  } = useDataRecipientDisclosureSettings(
    dataRecipient,
    answers,
    dataRecipient?.isCustom,
    basicState.category,
  );

  const {
    state: retentionState,
    meta: retentionMeta,
    updateState: retentionUpdate,
  } = useDataRecipientRetentionSettings(
    dataRecipient,
    dataRecipients,
    appPlatforms,
    retentionReasons,
    fetchedInstructions,
  );

  const { section } = qs.parse(location.search);

  const showBasic = canShowBasic && (!section || section == "basic");
  const showDisclosure = !section || section == "data-disclosure";
  const showRetention = !section || section == "data-retention";
  const showPlatforms = canShowPlatforms && (!section || section == "platforms");

  const onDone = async () => {
    if (showBasic) {
      await saveBasicUpdates(org, dataRecipient, basicState);
    }

    if (showDisclosure) {
      await saveDisclosureUpdates(
        org,
        dataRecipient,
        answers,
        disclosureState,
        updateVendor,
        updateSurveyQuestions,
      );
    }

    if (showRetention) {
      await saveRetentionUpdates(
        org,
        dataRecipient,
        dataRecipients,
        appPlatforms,
        updateRemoteInstruction,
        retentionMeta,
        retentionState,
      );
    }

    history.push(RouteHelpers.dataRecipients.detail(dataRecipient.vendorId));
  };

  if (!dataRecipient) {
    return null;
  }

  return (
    <StandardPageWrapper
      requests={[
        dataRecipientsRequest,
        surveyRequest,
        instructionsRequest,
        retentionReasonsRequest,
        appPlatformsRequest,
        platformAppsRequest,
        vendorDetailsRequest,
      ]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextContent={"Done"}
              nextOnClick={onDone}
              prevContent={
                <span className="flex-center-vertical">
                  <KeyboardArrowLeftIcon />
                  Back
                </span>
              }
              prevOnClick={() =>
                history.push(RouteHelpers.dataRecipients.detail(dataRecipient?.vendorId))
              }
            />
          }
        />
      }
    >
      <VendorPageHeader
        vendor={dataRecipient}
        titleContent={dataRecipient?.name}
        descriptionContent={<DataRecipientDescription dataRecipient={dataRecipient} />}
      />

      <div className="w-896">
        {showBasic && (
          <DataRecipientBasicSettings
            orgVendors={dataRecipients}
            vendors={vendorDetails}
            isThirdParty={isThirdPartyRecipient(dataRecipient)}
            state={basicState}
            updateState={basicUpdate}
          />
        )}

        {showDisclosure && (
          <DataRecipientDisclosureSettings
            org={org}
            vendor={dataRecipient}
            isThirdParty={isThirdPartyRecipient(dataRecipient)}
            state={disclosureState}
            meta={disclosureMeta}
            updateState={disclosureUpdate}
          />
        )}

        {showRetention && (
          <DataRecipientRetentionSettings
            dataRecipient={dataRecipient}
            isThirdPartyRecipient={isThirdPartyRecipient(dataRecipient)}
            state={retentionState}
            meta={retentionMeta}
            updateState={retentionUpdate}
          />
        )}

        {showPlatforms && (
          <div className="mt-xl">
            <div className="text-t5 text-weight-bold color-primary-700 mb-md">Installed Apps</div>

            <p className="mb-xl">
              Select which vendors below are installed apps in your Shopify store. To see a list of
              your installed apps, go to the{" "}
              <a target="_blank" rel="noreferrer noopener" href="https://shopify.com/admin/apps">
                Apps
              </a>{" "}
              section of your Shopify account.
            </p>

            {dataRecipient?.isPlatform && (
              <DataRecipientMultiselect
                selected={installedApps}
                recipients={platformApps.available}
                onChange={({ appId, included }) =>
                  toggleInstall({
                    platformId: dataRecipient?.vendorId,
                    appId,
                    included,
                  })
                }
                onChangeSelectAll={(selected) => {
                  platformApps.available.forEach((r) =>
                    toggleInstall({
                      platformId: dataRecipient?.vendorId,
                      appId: r.vendorId,
                      included: selected,
                    }),
                  );
                }}
              />
            )}
          </div>
        )}
      </div>
    </StandardPageWrapper>
  );
};
