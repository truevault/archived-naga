import { Add } from "@mui/icons-material";
import { Button } from "@mui/material";
import React, { useEffect, useState } from "react";
import { NetworkRequest } from "../../../common/models";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { ActionRow, ActionRowsHeader, EmptyActionRow } from "../../components/ActionRow";
import { PageHeader } from "../../components/layout/header/Header";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { RouteHelpers, Routes } from "../../root/routes";
import { alphabetically } from "../../surveys/util";
import { ProgressSpan } from "../get-compliant/ProgressSpan";

export const DataRecipients = () => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  // Requests
  const [organizationVendors, organizationVendorsRequest] = useOrganizationVendors(
    organizationId,
    undefined,
    true,
  );

  useEffect(() => {
    if (NetworkRequest.areFinished(organizationVendorsRequest)) {
      setReadyToRender(true);
    }
  }, [organizationVendorsRequest]);

  if (!readyToRender) {
    return <LoadingPageWrapper />;
  }

  const vendors = organizationVendors.filter(
    (v) => !v.isDataStorage && v.dataRecipientType == "vendor",
  );
  const thirdParty = organizationVendors.filter(
    (v) => v.dataRecipientType == "third_party_recipient",
  );

  return (
    <StandardPageWrapper>
      <PageHeader titleContent="Data Recipients" />

      <div className="data-sharing--container mt-md">
        <DataRecipientsBody vendors={vendors} thirdParty={thirdParty} />
      </div>
    </StandardPageWrapper>
  );
};

const DataRecipientsBody = ({ vendors, thirdParty }) => {
  return (
    <>
      <DataRecipientList
        label="Vendors"
        addLabel="Vendor"
        addRoute={Routes.dataRecipients.add}
        help="Vendors process personal information of your business’s consumers to provide services to your business."
        dataRecipients={vendors}
      />
      <div className="my-xl" />
      <DataRecipientList
        label="Third Party Recipients"
        addLabel="Recipient"
        addRoute={Routes.dataRecipients.addThirdParty}
        help="Third party recipients are individuals or organizations to whom your business discloses or sells personal information but who do not provide services to it."
        classification="Third Party"
        dataRecipients={thirdParty}
      />
    </>
  );
};

type DataRecipientListProps = {
  label: string;
  addLabel: string;
  addRoute: string;
  help: string;
  dataRecipients: OrganizationDataRecipientDto[];
  classification?: string;
};
const DataRecipientList: React.FC<DataRecipientListProps> = ({
  label,
  help,
  addLabel,
  addRoute,
  dataRecipients,
  classification,
}) => {
  return (
    <>
      <ActionRowsHeader
        label={label}
        statusLabel="Classification"
        wideStatus
        labelHelpText={help}
      />

      {dataRecipients.length > 0 ? (
        dataRecipients
          .slice(0)
          .sort(alphabetically)
          .map((ov) => {
            return (
              <DataRecipientRow
                key={`vendor-${ov.vendorId}`}
                ov={ov}
                classification={classification}
              />
            );
          })
      ) : (
        <EmptyActionRow label={`There are no ${label}`} />
      )}

      <div className="mt-md">
        <Button href={addRoute} variant="contained" startIcon={<Add />}>
          Add {addLabel}
        </Button>
      </div>
    </>
  );
};

type DataRecipientRowProps = {
  ov: OrganizationDataRecipientDto;
  classification?: string;
};

const DataRecipientRow: React.FC<DataRecipientRowProps> = ({ ov, classification }) => {
  const unclassified =
    !classification && (!ov.classification?.name || ov.classification?.name == "Unknown");
  const status = unclassified ? (
    <ProgressSpan progress="NEEDS_REVIEW" label="Needs Classification" wide />
  ) : (
    classification || ov.classification.name
  );

  return (
    <ActionRow
      iconUrl={ov.logoUrl}
      label={ov.name}
      status={status}
      rowHref={RouteHelpers.dataRecipients.detail(ov.vendorId)}
      wideStatus
    />
  );
};
