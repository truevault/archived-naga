import React, { useEffect, useMemo, useState } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { useDataRecipientsCatalog } from "../../../common/hooks/useVendors";
import { Api } from "../../../common/service/Api";
import { UpdateOrganizationSurveyQuestionDto } from "../../../common/service/server/controller/OrganizationController";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import { UUIDString } from "../../../common/service/server/Types";
import { PageHeader, VendorPageHeader } from "../../components/layout/header/Header";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { CollectionGroupSelect } from "../../components/organisms/data-recipients/CollectionGroupSelect";
import {
  DataRecipientBasicSettings,
  useDataRecipientBasicSettings,
} from "../../components/organisms/data-recipients/DataRecipientBasicSettings";
import {
  DataRecipientDisclosureSettings,
  finishedDisclosure,
  saveDisclosureUpdates,
  useDataRecipientDisclosureSettings,
} from "../../components/organisms/data-recipients/DataRecipientDisclosureSettings";
import {
  DataRecipientRetentionSettings,
  finishedRetention,
  saveRetentionUpdates,
  useDataRecipientRetentionSettings,
} from "../../components/organisms/data-recipients/DataRecipientRetentionSettings";
import {
  useOrganizationVendors,
  usePrimaryOrganization,
  usePrimaryOrganizationId,
} from "../../hooks";
import { RouteHelpers, Routes } from "../../root/routes";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../get-compliant/data-recipients/shared";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";
import { finishedCustom } from "./CustomFields";
import { UpdateVendorArgs } from "./DataRecipientEditPage";

type MatchParams = {
  vendorId: UUIDString;
};

type Props = RouteComponentProps<MatchParams> & {
  custom?: boolean;
};

export const DataRecipientAddDetail = ({ match, custom, location }: Props) => {
  // Variables
  const history = useHistory();
  const organizationId = usePrimaryOrganizationId();
  const [org] = usePrimaryOrganization();
  const vendorId = match.params.vendorId;

  const thirdParty = new URLSearchParams(location.search).has("thirdParty");

  const { name, email, category, url } = Object.fromEntries(
    new URLSearchParams(location.search) as any,
  );
  const initialValues = useMemo(
    () => ({ name, email, category, url }),
    [name, email, category, url],
  );

  // State
  const [readyToRender, setReadyToRender] = useState(false);
  const [selectedCollectionGroups, setSelectedCollectionGroups] = useState([]);

  const [vendorDetails, vendorDetailsRequest] = useDataRecipientsCatalog(organizationId);
  const [orgVendors] = useOrganizationVendors(organizationId);

  // Requests
  const { result: retentionReasons, request: retentionReasonsRequest } = useDataRequest({
    queryKey: ["retentionReasons", organizationId],
    api: () => Api.polarisData.getRetentionReasons(),
  });

  const { result: surveyQuestions, request: surveyQuestionsRequest } = useDataRequest({
    queryKey: ["survey", organizationId, SELLING_AND_SHARING_SURVEY_NAME],
    api: () =>
      Api.organization.getOrganizationSurvey(organizationId, SELLING_AND_SHARING_SURVEY_NAME),
  });

  const updateRemoteInstruction = async (newInstruction: RequestHandlingInstructionDto) => {
    await Api.requestHandlingInstructions.putInstruction(org.id, newInstruction);
  };

  const [answers, setAnswers] = useState<Record<string, string>>({});

  useEffect(() => {
    if (
      surveyQuestionsRequest.success &&
      vendorDetailsRequest.success &&
      retentionReasonsRequest.success
    ) {
      setAnswers(Object.fromEntries(surveyQuestions.map((q) => [q.slug, q.answer])));
      setReadyToRender(true);
    }
  }, [surveyQuestionsRequest, surveyQuestions, vendorDetailsRequest, retentionReasonsRequest]);

  const vendor = !custom && vendorDetails.find((v) => v.id == vendorId);

  const { state: basicState, updateState: basicUpdate } =
    useDataRecipientBasicSettings(initialValues);

  const {
    state: disclosureState,
    meta: disclosureMeta,
    updateState: disclosureUpdate,
  } = useDataRecipientDisclosureSettings(vendor, null, custom, basicState.category);

  const {
    state: retentionState,
    meta: retentionMeta,
    updateState: retentionUpdate,
  } = useDataRecipientRetentionSettings(vendor, orgVendors, null, retentionReasons, null);

  const updateVendor = async (v: UpdateVendorArgs) => {
    const { vendorId, vendorSettings } = v;
    await Api.organizationVendor.addOrUpdateVendor(org.id, vendorId, vendorSettings);
  };

  const { fetch: updateSurveyQuestions } = useActionRequest({
    api: ({ questions }: { questions: UpdateOrganizationSurveyQuestionDto[] }) =>
      Api.organization.updateOrganizationSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME, questions),
    allowConcurrent: true,
  });

  if (!readyToRender) {
    return <LoadingPageWrapper />;
  }

  const vendorName = custom ? "Custom Vendor" : vendor.name;

  const descLine = [vendor?.category, vendor?.url, vendor?.email].filter((x) => x).join(" · ");
  const desc = (
    <>
      <p className="text-muted text-t1 my-0">{descLine}</p>
      {vendor?.dpaUrl && (
        <p className="text-t1 my-0">
          <a href={vendor?.dpaUrl} target="_blank" rel="noopener noreferrer">
            Data Processing Agreement (DPA)
          </a>
        </p>
      )}
    </>
  );

  const finCustom = !custom || finishedCustom(basicState);
  const finishedAssociation = selectedCollectionGroups.length > 0;

  const finished =
    finishedDisclosure(disclosureState, disclosureMeta, thirdParty) &&
    finishedRetention(retentionState, retentionMeta) &&
    finishedAssociation &&
    finCustom;

  const onDone = async () => {
    const settings: any = {
      ccpaIsSelling: disclosureState.ccpaIsSelling,
      dataRecipientType: thirdParty ? "third_party_recipient" : "vendor",
    };

    if (custom) {
      Object.assign(settings, basicState);
    }

    // we need to add before we can set the classification options
    let res;
    if (custom || thirdParty) {
      res = await Api.organizationVendor.addCustomVendor(organizationId, settings);
    } else {
      res = await Api.organizationVendor.addOrUpdateVendor(organizationId, vendorId, settings);
    }

    const newVendorId = res.vendorId ?? res.id;

    await saveDisclosureUpdates(
      org,
      res,
      answers,
      disclosureState,
      updateVendor,
      updateSurveyQuestions,
    );

    await saveRetentionUpdates(
      org,
      res,
      orgVendors,
      null,
      updateRemoteInstruction,
      retentionMeta,
      retentionState,
    );

    // Associate the new vendor to the selected collection groups
    await Promise.all(
      selectedCollectionGroups.map((groupId) =>
        Api.dataMap.updateRecipientAssociation(org.id, groupId, newVendorId, true),
      ),
    );

    history.push({
      pathname: RouteHelpers.dataRecipients.detail(newVendorId),
      search: "added=true",
    });
  };

  return (
    <StandardPageWrapper
      footer={
        <Footer
          onDone={onDone}
          nextDisabled={!finished}
          isThirdParty={thirdParty}
          // updateInstructionsRequest={updateInstructionsRequest}
          // doneHandlerRunning={doneHandlerRunning}
        />
      }
    >
      {Boolean(vendor) && (
        <VendorPageHeader
          vendor={vendor}
          titleContent={vendorName}
          descriptionContent={!custom && desc}
        />
      )}

      {!vendor && (
        <PageHeader
          titleContent={
            thirdParty ? "Add Third Party Recipient" : custom ? "Add Custom Vendor" : "Add Vendor"
          }
        />
      )}

      {custom && (
        <DataRecipientBasicSettings
          orgVendors={orgVendors}
          vendors={vendorDetails}
          isThirdParty={thirdParty}
          state={basicState}
          updateState={basicUpdate}
        />
      )}

      <DataRecipientDisclosureSettings
        org={org}
        vendor={vendor}
        isThirdParty={thirdParty}
        state={disclosureState}
        meta={disclosureMeta}
        updateState={disclosureUpdate}
      />

      <DataRecipientRetentionSettings
        dataRecipient={vendor}
        isThirdPartyRecipient={thirdParty}
        state={retentionState}
        meta={retentionMeta}
        updateState={retentionUpdate}
      />

      <div className="text-t5 text-weight-bold color-primary-700 mb-md">Data Map</div>

      <CollectionGroupSelect
        label={`Which Collection Groups have information processed by ${vendor.name}?`}
        helpText={`Select which Collection Groups about which ${vendor.name} receives or process data.`}
        selectedGroupIds={selectedCollectionGroups}
        onChange={setSelectedCollectionGroups}
      />
    </StandardPageWrapper>
  );
};

const Footer = ({ onDone, nextDisabled, isThirdParty }) => {
  const history = useHistory();
  return (
    <ProgressFooter
      actions={
        <ProgressFooterActions
          nextContent={isThirdParty ? "Add Recipient" : "Add Vendor"}
          nextOnClick={onDone}
          nextDisabled={nextDisabled}
          nextTooltip={
            nextDisabled ? "Please complete all questions in order to add this vendor." : null
          }
          prevContent="Cancel"
          prevOnClick={() => history.push(Routes.dataRecipients.root)}
        />
      }
    />
  );
};
