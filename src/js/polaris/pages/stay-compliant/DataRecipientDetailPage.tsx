import {
  DeleteForever,
  Engineering,
  FileUpload,
  Gavel,
  Google,
  Home,
  ManageAccounts,
  MonetizationOn,
  RecentActors,
  Pending,
  RecordVoiceOver,
  RoomPreferences,
  SafetyDivider,
  VerifiedUser,
  Lock,
  LockOpenOutlined,
} from "@mui/icons-material";
import { Button } from "@mui/material";
import React, { ReactNode, useEffect, useMemo, useState } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import { useDataRequest } from "../../../common/hooks/api";
import { SurveyAnswers } from "../../../common/hooks/useSurvey";
import { ApiRouteHelpers } from "../../../common/root/apiRoutes";
import { Api } from "../../../common/service/Api";
import {
  OrganizationDataRecipientDto,
  OrganizationDataRecipientPlatformDto,
} from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../common/service/server/dto/OrganizationDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import { RetentionReasonDto } from "../../../common/service/server/dto/RetentionReasonDto";
import { VendorClassificationDto } from "../../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../../common/service/server/Types";
import { Abbr } from "../../components/Abbr";
import { Callout, CalloutVariant } from "../../components/Callout";
import { DataRecipientLogo } from "../../components/DataRecipientLogo";
import { RemoveVendorDialog } from "../../components/dialog/RemoveVendorDialog";
import { VendorPageHeader } from "../../components/layout/header/Header";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { VendorSectionCallout } from "../../components/organisms/callouts/VendorSectionCallout";
import {
  DisclosureMeta,
  DisclosureState,
  GoogleAnalyticsHelp,
  useDataRecipientDisclosureSettings,
} from "../../components/organisms/data-recipients/DataRecipientDisclosureSettings";
import { SettingsItem } from "../../components/organisms/data-recipients/SettingsIndicator";
import { useOrganizationVendors, usePrimaryOrganization } from "../../hooks";
import {
  useAppPlatforms,
  usePlatformApps,
  useRequestInstructions,
  useRetentionReasons,
} from "../../hooks/useOrganizationVendors";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../pages/get-compliant/data-recipients/shared";
import { RouteHelpers, Routes } from "../../root/routes";
import { installedAppsForVendor } from "../../util/resources/vendorUtils";
import { isThirdParty, isThirdPartyRecipient } from "../../util/vendors";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

type MatchParams = {
  vendorId: UUIDString;
};

type Props = RouteComponentProps<MatchParams>;

export const DataRecipientDetail = ({ match }: Props) => {
  // Variables
  const history = useHistory();
  const [org] = usePrimaryOrganization();
  const vendorId = match.params.vendorId;

  const added = new URLSearchParams(location.search).has("added");

  // State
  const [readyToRender, setReadyToRender] = useState(false);
  const [confirmRemove, setConfirmRemove] = useState(false);

  // Requests
  const [organizationVendors, organizationVendorsRequest] = useOrganizationVendors(
    org.id,
    undefined,
    true,
  );
  const [appPlatforms, appPlatformsRequest] = useAppPlatforms(org.id, vendorId);
  const [platformApps, platformAppsRequest] = usePlatformApps(org.id, vendorId);
  const { result: surveyQuestions, request: surveyQuestionsRequest } = useDataRequest({
    queryKey: ["survey", org.id, SELLING_AND_SHARING_SURVEY_NAME],
    api: () => Api.organization.getOrganizationSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME),
  });

  const [fetchedInstructions, instructionsRequest] = useRequestInstructions(org.id, "DELETE");
  const [retentionReasons, retentionReasonsRequest] = useRetentionReasons();

  const [answers, setAnswers] = useState<SurveyAnswers>({});

  const vendor = useMemo(
    () => organizationVendors?.find((ov) => ov.vendorId == vendorId),
    [organizationVendors, vendorId],
  );

  useEffect(() => {
    if (
      Boolean(vendor) &&
      surveyQuestionsRequest.success &&
      organizationVendorsRequest.success &&
      appPlatformsRequest.success &&
      instructionsRequest.success &&
      platformAppsRequest.success &&
      retentionReasonsRequest.success
    ) {
      const ans: SurveyAnswers = Object.fromEntries(
        surveyQuestions?.map((q) => [q.slug, q.answer]) || [],
      );
      setAnswers(ans);

      setReadyToRender(true);
    }
  }, [
    vendor,
    organizationVendorsRequest.success,
    surveyQuestionsRequest.success,
    surveyQuestions,
    instructionsRequest.success,
    retentionReasonsRequest.success,
    appPlatformsRequest.success,
    platformAppsRequest.success,
  ]);

  const afterRemove = async () => {
    history.push(Routes.dataRecipients.root);
  };

  const { state: disclosureState, meta: disclosureMeta } = useDataRecipientDisclosureSettings(
    vendor,
    answers,
    vendor?.isCustom,
    vendor?.category,
  );

  const dataDisclosureItems = useDataDisclosureItems(
    org,
    vendor,
    disclosureState,
    disclosureMeta,
    answers,
  );

  const dataRetentionItems = useDataRetentionItems(
    vendor,
    appPlatforms,
    organizationVendors,
    fetchedInstructions,
    retentionReasons,
  );

  if (!readyToRender) {
    return <LoadingPageWrapper />;
  }

  const descLine = [vendor.category, vendor.url, vendor.email].filter((x) => x).join(" · ");
  const desc = (
    <>
      <p className="text-muted text-t1 my-0">{descLine}</p>
      {vendor.dpaUrl && (
        <p className="text-t1 my-0">
          <a href={vendor.dpaUrl} target="_blank" rel="noopener noreferrer">
            Data Processing Agreement (DPA)
          </a>
        </p>
      )}
    </>
  );

  const installedApps = installedAppsForVendor(vendor, platformApps).map((app) => ({
    id: app.id,
    label: (
      <>
        <img
          width={24}
          src={app.logoUrl || "/assets/images/icon-vendors-black.svg"}
          className="align-middle mr-md"
        />
        {app.name}
      </>
    ),
    response: null,
    isNested: false,
    hideBorder: true,
  }));

  return (
    <StandardPageWrapper
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextContent={"Done"}
              nextOnClick={() => history.push(Routes.dataRecipients.root)}
            />
          }
        />
      }
    >
      <VendorPageHeader
        vendor={vendor}
        titleContent={vendor.name}
        descriptionContent={desc}
        actionUrl={
          vendor.dataRecipientType === "vendor" && !vendor?.isCustom
            ? null
            : `${RouteHelpers.dataRecipients.edit(vendorId)}?section=basic`
        }
        wide
      />

      {added && (
        <Callout variant={CalloutVariant.Green} className="mt-xl">
          <p>{vendor.name} has been added to your Data Recipients.</p>
          <p>
            Next, map your data sharing practices for {vendor.name} in your{" "}
            <a href={Routes.dataInventory.collection.root}>Data Map</a>.
          </p>
        </Callout>
      )}

      {vendor.dataRecipientType !== "storage_location" && (
        <VendorSectionCallout
          title="Data Disclosure"
          className="mt-xl"
          items={dataDisclosureItems}
          actionUrl={`${RouteHelpers.dataRecipients.edit(vendorId)}?section=data-disclosure`}
        />
      )}

      <VendorSectionCallout
        title="Data Retention"
        className="my-xl"
        items={dataRetentionItems}
        actionUrl={`${RouteHelpers.dataRecipients.edit(vendorId)}?section=data-retention`}
      />

      {vendor?.isPlatform && (
        <VendorSectionCallout
          title="Installed Apps"
          className="my-xl"
          dense
          items={installedApps}
          noItemsView={
            <div className="mt-md border-color-neutral-400 border-top">
              <p className="py-lg text-center">
                Click “Edit Settings” to select vendors that are installed apps in your Shopify
                store
              </p>
            </div>
          }
          actionUrl={`${RouteHelpers.dataRecipients.edit(vendorId)}?section=platforms`}
        />
      )}

      <Button variant="contained" onClick={() => setConfirmRemove(true)}>
        Remove Vendor
      </Button>
      <RemoveVendorDialog
        open={confirmRemove}
        onClose={() => setConfirmRemove(false)}
        onAfterRemoved={afterRemove}
        vendorId={vendorId}
        collectionContext={vendor.collectionContext}
      />
    </StandardPageWrapper>
  );
};

export const useDataRetentionItems = (
  vendor: OrganizationDataRecipientDto,
  appPlatforms: OrganizationDataRecipientPlatformDto,
  organizationVendors: OrganizationDataRecipientDto[],
  fetchedInstructions: RequestHandlingInstructionDto[],
  retentionReasons: RetentionReasonDto[],
): SettingsItem[] => {
  const platforms = useMemo(
    () => organizationVendors?.filter((ov) => ov.isPlatform),
    [organizationVendors],
  );
  const installedIn = useMemo(() => new Set(appPlatforms?.installed ?? []), [appPlatforms]);

  const visiblePlatforms = useMemo(
    () => vendor && !vendor.isPlatform && !vendor.isStandalone && !isThirdPartyRecipient(vendor),
    [vendor],
  );

  const foundInstruction = useMemo(
    () => fetchedInstructions?.find((i) => i.vendorId === vendor?.vendorId),
    [fetchedInstructions],
  );

  const vendorRetentionReasons = useMemo(
    () =>
      foundInstruction?.retentionReasons?.map((r) => retentionReasons.find((x) => x.slug === r)),
    [foundInstruction],
  );

  return useMemo(() => {
    const platformSettings = platforms
      .filter(() => visiblePlatforms)
      .map((platform) => {
        return {
          id: `platform-${platform.id}`,
          icon: <DataRecipientLogo size={24} dataRecipient={platform} />,
          label: installedIn.has(platform.vendorId) ? (
            <>
              {vendor?.name} is an installed app in <strong>{platform.name}</strong>
            </>
          ) : (
            <>
              {vendor?.name} is not an installed app in <strong>{platform.name}</strong>
            </>
          ),
          response: installedIn.has(platform.vendorId),
        };
      });

    const hasExceptionsToDeletion =
      !!foundInstruction && foundInstruction.processingMethod != "DELETE";
    const dataNotAccessible =
      !!foundInstruction && foundInstruction.processingMethod == "INACCESSIBLE_OR_NOT_STORED";

    return [
      ...platformSettings,
      dataNotAccessible
        ? null
        : {
            id: "deletion-exception",
            label: hasExceptionsToDeletion ? (
              <>
                <strong>Exception to deletion</strong> apply to {vendor?.name}
              </>
            ) : (
              <>
                <strong>No Exceptions to Deletion</strong>
              </>
            ),
            icon: DeleteForever,
            subtitle: vendorRetentionReasons?.map((r) => r.reason).join(", "),
            response: hasExceptionsToDeletion,
          },
      {
        id: "accessibility",
        label: dataNotAccessible ? (
          <>
            Data in {vendor?.name} is <strong>not accessible</strong>
          </>
        ) : (
          <>
            Data in {vendor?.name} is <strong>accessible</strong>
          </>
        ),
        icon: dataNotAccessible ? Lock : LockOpenOutlined,
      },
    ].filter((x) => x);
  }, [platforms, installedIn, visiblePlatforms, foundInstruction, vendorRetentionReasons, vendor]);
};

export const useInstalledApps = (
  vendor: OrganizationDataRecipientDto,
  platformApps: OrganizationDataRecipientPlatformDto,
): SettingsItem[] => {
  return useMemo(
    () =>
      installedAppsForVendor(vendor, platformApps).map((app) => ({
        id: app?.id,
        label: app?.name,
        icon: <DataRecipientLogo size={24} dataRecipient={app} />,
        response: null,
        isNested: false,
      })),
    [vendor, platformApps],
  );
};

export const useDataDisclosureItems = (
  org: OrganizationDto,
  dataRecipient: OrganizationDataRecipientDto,
  state: DisclosureState,
  meta: DisclosureMeta,
  answers: Record<string, string>,
) =>
  useMemo(
    () => disclosureItems(org, dataRecipient, state, meta, answers),
    [org, dataRecipient, answers, state, meta],
  );

const getIconAndLabelForVendorClassification = (classification?: VendorClassificationDto) => {
  const isServiceProvider = classification?.slug === "service-provider";
  const isContractor = classification?.slug === "contractor";

  let typeLabel = "Third Party";
  let typeIcon = SafetyDivider;

  if (isServiceProvider) {
    typeLabel = "Service Provider";
    typeIcon = VerifiedUser;
  }

  if (isContractor) {
    typeLabel = "Contractor";
    typeIcon = Engineering;
  }

  return { typeLabel, typeIcon };
};

export const disclosureItems = (
  org: OrganizationDto,
  vendor: OrganizationDataRecipientDto,
  state: DisclosureState,
  meta: DisclosureMeta,
  answers: Record<string, string>,
): SettingsItem[] => {
  if (!vendor) {
    return [];
  }

  const items: SettingsItem[] = [];

  if (!isThirdPartyRecipient(vendor) && meta.showClassification) {
    const docActions = actionsForDataRecipientDocumentation(org, vendor);
    const isServiceProvider = vendor.classification?.slug === "service-provider";
    const isContractor = vendor.classification?.slug === "contractor";
    const isThirdParty = vendor.classification?.slug === "third-party";
    const needsClassification = !isServiceProvider && !isContractor && !isThirdParty;

    let label: ReactNode;
    let icon: ReactNode;
    if (needsClassification) {
      label = (
        <>
          {vendor.name} <strong>needs classification</strong>
        </>
      );

      icon = Pending;
    } else {
      const { typeLabel, typeIcon } = getIconAndLabelForVendorClassification(vendor.classification);

      label = (
        <>
          {vendor.name} is a <strong>{typeLabel}</strong> to {org.name} (CCPA)
        </>
      );
      icon = typeIcon;
    }

    items.push({
      id: "service-provider",
      label,
      icon,
      response: isServiceProvider,
      action: docActions.map((a) => (
        <Button
          key={a.href}
          className="ml-md"
          target="_blank"
          rel="noopener noreferrer"
          color="inherit"
          href={a.href}
        >
          {a.label}
        </Button>
      )),
    });
  }

  if (org.featureGdpr) {
    items.push({
      id: "processor-classification",
      icon: state.processorClassification === "Processor" ? RoomPreferences : ManageAccounts,
      label: (
        <>
          {vendor.name} is a <strong>{state.processorClassification}</strong> of personal data
          (GDPR)
        </>
      ),
      response: state.processorClassification === "Processor",
      action: actionsForProcessorDocumentation(org, vendor).map((a) => (
        <Button
          key={a.href}
          className="ml-md"
          target="_blank"
          rel="noopener noreferrer"
          color="inherit"
          href={a.href}
        >
          {a.label}
        </Button>
      )),
    });

    items.push({
      id: "international-data-transfers",
      icon: Gavel,
      label: (
        <>
          <strong>International transfer rules</strong>{" "}
          {vendor.gdprInternationalDataTransferRulesApply ? "apply" : "do not apply"} to{" "}
          {vendor.name}'s data processing
        </>
      ),
      response: vendor.gdprInternationalDataTransferRulesApply,
      action: actionsForSccDocumentation(org, vendor).map((a) => (
        <Button
          key={a.href}
          className="ml-md"
          target="_blank"
          rel="noopener noreferrer"
          color="inherit"
          href={a.href}
        >
          {a.label}
        </Button>
      )),
    });
  }

  // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
  // if (meta.showFacebookLdu) {
  //   items.push({
  //     id: "facebook-ldu",
  //     icon: FacebookOutlined,
  //     label: `${org.name} ${state.facebookLdu ? "will" : "will not"} enable LDU for ${vendor.name}`,
  //     response: state.facebookLdu,
  //     helpText: <FacebookLduHelp />,
  //   });
  // }

  // if (meta.showGoogleRdp) {
  //   items.push({
  //     id: "google-rdp",
  //     icon: Google,
  //     label: `${org.name} ${state.googleRdp ? "will" : "will not"} enable RDP for ${vendor.name}`,
  //     response: state.googleRdp,
  //     helpText: <GoogleRdpHelp />,
  //   });
  // }

  if (meta.showIntInteraction) {
    const intentionalInteractionAbbr =
      'An intentional interaction is when the third party is visible and named on your website or application, and the consumer is choosing to interact with and send their data to that specific party. For example, clicking a "Pay with PayPal" button, choosing FedEx as a shipping option, or clicking "share my information with local contractors."';
    items.push({
      id: "intentional-interaction",
      icon: RecordVoiceOver,
      label: state.isIntentionalInteraction ? (
        <>
          Consumers{" "}
          <Abbr title={intentionalInteractionAbbr}>
            <strong>intentionally interact</strong>
          </Abbr>{" "}
          with {vendor.name}
        </>
      ) : (
        <>
          Consumers{" "}
          <Abbr title={intentionalInteractionAbbr}>
            <strong>do not intentionally interact</strong>
          </Abbr>{" "}
          with {vendor.name}
        </>
      ),
      response: state.isIntentionalInteraction,
      helpText: intentionalInteractionAbbr,
    });
  }

  if (meta.showSharing) {
    const sharingDefinition =
      "“Sharing” means providing personal information to ad networks for purposes of behavioral, or interest-based advertising.";
    items.push({
      id: "sharing",
      icon: Home,
      label: state.ccpaIsSharing ? (
        <>
          {org.name}{" "}
          <Abbr title={sharingDefinition}>
            <strong>“shares” personal information</strong>
          </Abbr>{" "}
          with {vendor.name}
        </>
      ) : (
        <>
          {org.name}{" "}
          <Abbr title={sharingDefinition}>
            <strong>does not “share” personal information</strong>
          </Abbr>{" "}
          with {vendor.name}
        </>
      ),
      response: state.ccpaIsSharing,
      helpText: sharingDefinition,
    });
  }

  if (meta.showGoogleAnalytics) {
    const isGaDataSharingOff = answers["google-analytics-data-sharing"] == "true";
    items.push({
      id: "google-analytics",
      icon: Google,
      label: isGaDataSharingOff
        ? `${org.name} has turned off (or will turn off) data sharing in Google Analytics`
        : `${org.name} has not turned off (nor will it turn off) data sharing in Google Analytics`,
      response: isGaDataSharingOff,
      helpText: <GoogleAnalyticsHelp />,
    });
  }

  if (meta.showSelling) {
    const sellingDefinition =
      '"Selling" means providing personal data in exchange for money or anything else of value.';
    items.push({
      id: "sells-info",
      icon: MonetizationOn,
      label: state.ccpaIsSelling ? (
        <>
          {org.name}{" "}
          <Abbr title={sellingDefinition}>
            <strong>"sells" personal information</strong>
          </Abbr>{" "}
          to {vendor.name}
        </>
      ) : (
        <>
          {org.name}{" "}
          <Abbr title={sellingDefinition}>
            <strong>does not “sell”</strong>
          </Abbr>{" "}
          personal data to {vendor.name}
        </>
      ),
      response: state.ccpaIsSelling,
      helpText: sellingDefinition,
    });
  }

  if (meta.showSelling && state.ccpaIsSelling && state.ccpaIsDirectSelling) {
    items.push({
      id: "uploads-info",
      icon: FileUpload,
      label: (
        <>
          {org.name} <strong>transmits or uploads</strong> consumer data to {vendor.name}
        </>
      ),
      response: state.ccpaIsDirectSelling,
      isNested: true,
    });
  }

  if (meta.showCustomAudience) {
    const customAudienceDefinition =
      'Custom Audience is a service offered by most ad networks in which a business can upload a list of their customers or contacts, and the ad network will either deliver ads directly to those people and/or use it to create a "look-alike" audience.';
    items.push({
      id: "custom-audience",
      icon: RecentActors,
      label: state.usesCustomAudience ? (
        <>
          {org.name} uses the{" "}
          <Abbr title={customAudienceDefinition}>
            <strong>Audience feature</strong>
          </Abbr>{" "}
          with {vendor.name}
        </>
      ) : (
        <>
          {org.name} does not use the{" "}
          <Abbr title={customAudienceDefinition}>
            <strong>Audience feature</strong>
          </Abbr>{" "}
          with {vendor.name}
        </>
      ),
      response: state.usesCustomAudience,
      helpText: customAudienceDefinition,
    });
  }

  return items;
};

type ActionProps = {
  href: string;
  label: React.ReactNode;
};
const actionsForDataRecipientDocumentation = (
  org: OrganizationDto,
  dataRecipient: OrganizationDataRecipientDto,
): ActionProps[] => {
  let actions = [];

  if (isThirdParty(dataRecipient)) {
    return actions;
  }

  if (dataRecipient.publicTosUrl) {
    actions.push({
      href: dataRecipient.publicTosUrl,
      label: "Public TOS",
    });
  }

  if (dataRecipient.tosFileKey) {
    actions.push({
      href: ApiRouteHelpers.storage.vendorAgreements.vendor(org.id, dataRecipient.vendorId),
      label: "View Agreement",
    });
  } else if (dataRecipient.serviceProviderLanguageUrl) {
    actions.push({
      href: dataRecipient.serviceProviderLanguageUrl,
      label: "View Agreement",
    });
  }

  return actions;
};

const actionsForProcessorDocumentation = (
  org: OrganizationDto,
  dataRecipient: OrganizationDataRecipientDto,
): ActionProps[] => {
  let actions = [];

  if (!org.featureGdpr) {
    return actions;
  }

  if (dataRecipient.gdprProcessorGuaranteeUrl) {
    actions.push({
      href: dataRecipient.gdprProcessorGuaranteeUrl,
      label: "Public TOS",
    });
  } else if (dataRecipient.gdprControllerGuaranteeUrl) {
    actions.push({
      href: dataRecipient.gdprControllerGuaranteeUrl,
      label: "Public TOS",
    });
  }

  if (dataRecipient.gdprProcessorGuaranteeFileKey) {
    actions.push({
      href: ApiRouteHelpers.storage.vendorAgreements.specific(
        org.id,
        dataRecipient.vendorId,
        dataRecipient.gdprProcessorGuaranteeFileKey,
      ),
      label: "View Agreement",
    });
  } else if (dataRecipient.gdprControllerGuaranteeFileKey) {
    actions.push({
      href: ApiRouteHelpers.storage.vendorAgreements.specific(
        org.id,
        dataRecipient.vendorId,
        dataRecipient.gdprControllerGuaranteeFileKey,
      ),
      label: "View Agreement",
    });
  } else if (dataRecipient.gdprProcessorLanguageUrl) {
    actions.push({
      href: dataRecipient.gdprProcessorLanguageUrl,
      label: "View Agreement",
    });
  }

  return actions;
};

const actionsForSccDocumentation = (
  org: OrganizationDto,
  dataRecipient: OrganizationDataRecipientDto,
): ActionProps[] => {
  let actions = [];

  if (!org.featureGdpr) {
    return actions;
  }

  if (dataRecipient.gdprSccUrl) {
    actions.push({
      href: dataRecipient.gdprSccUrl,
      label: "Public TOS",
    });
  }

  if (dataRecipient.gdprSccFileKey) {
    actions.push({
      href: ApiRouteHelpers.storage.vendorAgreements.specific(
        org.id,
        dataRecipient.vendorId,
        dataRecipient.gdprSccFileKey,
      ),
      label: "View Agreement",
    });
  } else if (dataRecipient.gdprSccDocumentationUrl) {
    actions.push({
      href: dataRecipient.gdprSccDocumentationUrl,
      label: "View Agreement",
    });
  }

  return actions;
};
