import React, { ReactElement } from "react";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { DataRecipientLogo } from "../../../components/DataRecipientLogo";
import { ActionTile } from "../../../organisms/ActionTile";
import { ClickThroughButton, VendorTileProps } from "./VendorTile";

type RowVendorTileProps = Omit<VendorTileProps, "vendor"> & {
  logoIcon?: ReactElement;
  handleRowClick?: () => void;
  vendor?: VendorDto | OrganizationDataRecipientDto | OrganizationDataSourceDto;
};

export const RowVendorTile: React.FC<RowVendorTileProps> = ({
  name,
  category,
  logoUrl,
  logoIcon,
  handleRowClick,
  handleAddVendor,
  handleRemoveVendor,
  selected = false,
  added = false,
  pending = false,
  disabled = false,
  vendor = null,
}) => {
  const showEndComponent = !handleRowClick && (handleAddVendor || handleRemoveVendor);

  return (
    <ActionTile
      onClick={handleRowClick}
      title={name}
      description={category}
      selected={selected}
      icon={
        <>
          {Boolean(logoIcon) && logoIcon}
          {!logoIcon && <DataRecipientLogo size={32} dataRecipient={vendor} logoUrl={logoUrl} />}
        </>
      }
      actions={
        showEndComponent ? (
          <div style={{ whiteSpace: "nowrap" }}>
            <ClickThroughButton
              added={added}
              pending={pending}
              disabled={disabled}
              className=""
              handleAddVendor={handleAddVendor}
              handleRemoveVendor={handleRemoveVendor}
            />
          </div>
        ) : null
      }
    />
  );
};
