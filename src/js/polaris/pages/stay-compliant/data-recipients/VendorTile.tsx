import { Add, Done as DoneIcon, Delete } from "@mui/icons-material";
import { Button } from "@mui/material";
import clsx from "clsx";
import React from "react";
import { LoadingButton } from "../../../../common/components/LoadingButton";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { RowVendorTile } from "./RowVendorTile";

export type TileBehavior = "click-through" | "radio" | "row";

export type VendorTileProps = {
  id?: UUIDString;
  name: string;
  category?: string;
  logoUrl?: string;
  handleAddVendor?: () => void;
  handleRemoveVendor?: () => void;
  selected?: boolean;
  added?: boolean;
  pending?: boolean;
  disabled?: boolean;
  variant?: TileBehavior;
  twoWide?: boolean;
  vendor?: VendorDto;
};

export const VendorTile = ({
  name,
  category,
  logoUrl,
  handleAddVendor,
  handleRemoveVendor,
  selected = false,
  added = false,
  pending = false,
  disabled = false,
  twoWide = false,
  variant,
}: VendorTileProps) => {
  if (variant == "row") {
    return (
      <RowVendorTile
        name={name}
        category={category}
        logoUrl={logoUrl}
        handleAddVendor={handleAddVendor}
        handleRemoveVendor={handleRemoveVendor}
        selected={selected}
        added={added}
        pending={pending}
        disabled={disabled}
      />
    );
  }

  const className = "add-vendors__search-vendor-tile";
  const isRadio = variant == "radio";
  const isClickThrough = variant == "click-through";

  return (
    <div
      onClick={!added && isRadio ? handleAddVendor : undefined}
      className={clsx(className, {
        [`${className}--click-through`]: isClickThrough,
        [`${className}--two-wide`]: twoWide,
        [`${className}--radio`]: isRadio,
        [`${className}--radio--selected`]: selected && isRadio,
        [`${className}--added`]: added && isRadio,
      })}
    >
      <div
        className={clsx(`${className}__logo`, {
          [`${className}__logo--radio`]: isRadio,
          [`${className}__logo--radio--added`]: added && isRadio,
        })}
      >
        <img src={logoUrl} />
      </div>
      <div
        className={clsx(`${className}__details`, {
          [`${className}__details--radio`]: isRadio,
          [`${className}__details--radio--selected`]: selected && isRadio,
          [`${className}__details--radio--added`]: added && isRadio,
        })}
      >
        <div
          title={name}
          className={clsx(`${className}__details__name`, {
            [`${className}__details__name--radio--selected`]: selected && isRadio,
            [`${className}__details__name--radio--added`]: added && isRadio,
          })}
        >
          {name}
        </div>
        <div
          className={clsx(`${className}__details__category`, {
            [`${className}__details__category--radio--selected`]: selected && isRadio,
            [`${className}__details__category--raddio--added`]: added && isRadio,
          })}
        >
          {category}
        </div>
        {isClickThrough && (
          <ClickThroughButton
            added={added}
            pending={pending}
            disabled={disabled}
            className={className}
            handleAddVendor={handleAddVendor}
            handleRemoveVendor={handleRemoveVendor}
          />
        )}
      </div>
    </div>
  );
};

export const ClickThroughButton = ({
  added,
  pending,
  disabled,
  className,
  handleAddVendor,
  handleRemoveVendor,
}) => {
  let content: React.ReactNode = null;

  if (added && !handleRemoveVendor) {
    content = (
      <Button startIcon={<DoneIcon />} disabled>
        Added
      </Button>
    );
  } else if (added) {
    content = (
      <Button
        onClick={() => handleRemoveVendor()}
        variant="text"
        color="secondary"
        disabled={disabled}
        startIcon={<Delete />}
      >
        Remove
      </Button>
    );
  } else {
    content = (
      <LoadingButton
        onClick={() => handleAddVendor()}
        variant="contained"
        disabled={disabled}
        startIcon={<Add />}
        loading={pending}
      >
        {pending ? "Adding" : "Add"}
      </LoadingButton>
    );
  }

  return <div className={clsx(`${className}__details__add`)}>{content}</div>;
};
