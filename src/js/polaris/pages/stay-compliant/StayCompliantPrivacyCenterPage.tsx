import React, { useEffect, useState } from "react";
import { NetworkRequest } from "../../../common/models";
import { Callout, CalloutVariant } from "../../components/Callout";
import { PageHeader } from "../../components/layout/header/Header";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import {
  EditPrivacyCenter,
  useEditPrivacyCenter,
} from "../../components/privacyCenter/EditPrivacyCenter";
import { Routes } from "../../root/routes";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

export const PrivacyCenter = () => {
  const editPrivacyCenter = useEditPrivacyCenter();

  const [everRendered, setEverRendered] = useState(false);

  // ever-loaded effect
  useEffect(() => {
    if (
      NetworkRequest.areFinished(
        editPrivacyCenter.requests.privacyCenter,
        editPrivacyCenter.requests.privacyPolicy,
        editPrivacyCenter.requests.notice,
        editPrivacyCenter.requests.incentivesSurvey,
        editPrivacyCenter.requests.collectionGroups,
      )
    ) {
      setEverRendered(true);
    }
  }, [
    editPrivacyCenter.requests.privacyCenter,
    editPrivacyCenter.requests.privacyPolicy,
    editPrivacyCenter.requests.notice,
    editPrivacyCenter.requests.incentivesSurvey,
    editPrivacyCenter.requests.collectionGroups,
  ]);

  const handleSave = () => editPrivacyCenter.onSave();

  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  const description = (
    <p>
      View your{" "}
      <a href={Routes.privacyCenter.publish} rel="noreferrer">
        developer instructions
      </a>{" "}
      for publishing your privacy notices below.
    </p>
  );
  return (
    <StandardPageWrapper
      footer={<Footer onSave={handleSave} isSaving={editPrivacyCenter.props.saving} />}
    >
      <PageHeader titleContent="Privacy Center" descriptionContent={description} />

      {editPrivacyCenter.props.hasSaved && (
        <Callout variant={CalloutVariant.Yellow} className="mb-mdlg">
          Your changes have been saved and will take a few minutes to appear in your Privacy Policy.
        </Callout>
      )}

      <EditPrivacyCenter {...editPrivacyCenter.props} />
    </StandardPageWrapper>
  );
};

type FooterProps = {
  onSave: () => void;
  isSaving: boolean;
};
const Footer: React.FC<FooterProps> = ({ onSave, isSaving }) => {
  return (
    <ProgressFooter
      actions={
        <ProgressFooterActions nextContent={"Save"} nextOnClick={onSave} nextLoading={isSaving} />
      }
    />
  );
};
