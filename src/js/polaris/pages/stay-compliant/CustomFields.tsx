import React, { useEffect, useState } from "react";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../../common/service/server/dto/VendorDto";
import { THIRD_PARTY_CATEGORIES } from "../get-compliant/data-recipients/ThirdPartyRecipientsPage";
import {
  CustomVendorFields,
  CustomVendorForm,
  CustomVendorFormValues,
} from "../get-compliant/data-recipients/AddDataRecipients";

export const finishedCustom = (state: CustomVendorFormValues) => {
  return Boolean(state.name) && Boolean(state.category);
};

type CustomFieldsProps = {
  vendors: VendorDto[];
  enabledVendors: OrganizationDataRecipientDto[];
  isThirdParty?: boolean;
  onValuesChanged: (values: CustomVendorFormValues) => void;
  initialValues: {
    name: string;
    category: string;
    url: string;
  };
};
export const CustomFields: React.FC<CustomFieldsProps> = ({
  vendors,
  enabledVendors,
  onValuesChanged,
  initialValues,
  isThirdParty = false,
}) => {
  const [values, setValues] = useState<CustomVendorFormValues>(initialValues);
  const [catVal, setCatVal] = useState(initialValues.category);

  useEffect(() => {
    onValuesChanged({ ...values, category: catVal });
  }, [values, catVal, onValuesChanged]);

  if (!vendors) {
    return null;
  }

  const allCategories = vendors
    .filter((v) => v.dataRecipientType == "vendor")
    .map((v) => v.category);

  const availableCategories = isThirdParty
    ? new Set(THIRD_PARTY_CATEGORIES)
    : new Set(allCategories);

  const isChanged = (newVals: CustomVendorFormValues, oldVals: CustomVendorFormValues) => {
    return (
      newVals?.name !== oldVals?.name ||
      newVals?.category !== oldVals?.category ||
      newVals?.url !== oldVals?.url
    );
  };

  const handleChange = ({
    values: vals,
    valid,
  }: {
    values: CustomVendorFormValues;
    valid: boolean;
  }) => {
    if (isChanged(vals, values)) {
      if (valid) {
        setTimeout(() => setValues({ ...vals, category: catVal }), 0);
      } else {
        setValues(null);
      }
    }
  };

  return (
    <>
      <div className="w-512 mb-lg">
        <CustomVendorForm
          category={catVal}
          initialValues={initialValues}
          onSubmit={() => {
            // no-op
          }}
          onValuesChanged={handleChange}
        >
          <CustomVendorFields
            category={catVal}
            setCategory={setCatVal}
            enabledVendors={enabledVendors}
            existingCategories={availableCategories}
            showClassifcation={false}
          />
        </CustomVendorForm>
      </div>
    </>
  );
};
