import { FormGroup, InputLabel } from "@mui/material";
import React, { useEffect, useState } from "react";
import { SelectOption, SimpleSelect } from "../../../common/components/input/Select";
import { NetworkRequest } from "../../../common/models";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { EmailLookupForm } from "../../components/records/EmailLookupForm";
import { RequestLookupForm } from "../../components/records/RequestLookupForm";
import { usePrimaryOrganization } from "../../hooks";
import { useQuery } from "../../hooks/useQuery";

type Props = {};

type LookupType = "email" | "request";

const LookupFormOptions: SelectOption[] = [
  {
    label: "Consumer Email",
    value: "email",
  },
  {
    label: "Request",
    value: "request",
  },
];

export const LookupPage: React.FC<Props> = () => {
  const params = useQuery<{ lookupType: LookupType }>();
  const [everRendered, setEverRendered] = useState(false);
  const [lookupType, setLookupType] = useState<LookupType>(params.lookupType ?? "email");
  const [org, orgRequest] = usePrimaryOrganization();

  const LookupForm = lookupType === "email" ? EmailLookupForm : RequestLookupForm;

  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest)) {
      setEverRendered(true);
    }
  }, [orgRequest]);

  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  return (
    <StandardPageWrapper>
      <PageHeader className="mb-lg" titleContent="Record Lookup" />

      <FormGroup className="mb-md">
        <InputLabel className="mb-sm">Lookup By&hellip;</InputLabel>

        <SimpleSelect
          options={LookupFormOptions}
          value={lookupType}
          onChange={(e) => setLookupType(e.target.value as LookupType)}
        />
      </FormGroup>

      <LookupForm org={org} />
    </StandardPageWrapper>
  );
};
