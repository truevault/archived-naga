import { Button } from "@mui/material";
import pluralize from "pluralize";
import React, { useEffect, useMemo, useState } from "react";
import { NetworkRequest } from "../../../common/models";
import { ApiRouteHelpers, formatUrl } from "../../../common/root/apiRoutes";
import { filtersToParams } from "../../../common/service/server/controller/DataRequestController";
import { Filter } from "../../../common/service/server/dto/FilterDto";
import { SortRule } from "../../../common/service/server/dto/SortRuleDto";
import { DataRequestFilter, Iso8601Date } from "../../../common/service/server/Types";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { PageHeaderBack } from "../../components/layout/header/PageHeaderBack";
import { usePrimaryOrganization, useRequestSummaries } from "../../hooks";
import { useQuery } from "../../hooks/useQuery";
import { Routes } from "../../root/routes";
import { RequestsTable } from "../requests/RequestsTable";

type Props = {};

const columnSetForRequestState = (requestState: string): DataRequestFilter => {
  if (requestState === "new" || requestState === "in_progress") {
    return "active";
  }

  return requestState as DataRequestFilter;
};

const queryParamsToFilters = (params: RequestResultQueryParams): Filter[] => {
  let filters = [];

  if (params.requestState) {
    filters.push({
      id: "state",
      value: params.requestState,
    });
  }

  if (params.requestType) {
    filters.push({
      id: "type",
      value: params.requestType,
    });
  }

  if (params.startDate) {
    filters.push({
      id: "startDate",
      value: new Date(params.startDate).toISOString(),
    });
  }

  if (params.endDate) {
    filters.push({
      id: "endDate",
      value: new Date(params.endDate).toISOString(),
    });
  }

  return filters;
};

type RequestResultQueryParams = {
  requestState?: string;
  requestType?: string;
  startDate?: Iso8601Date;
  endDate?: Iso8601Date;
};

export const RequestResultsPage: React.FC<Props> = () => {
  const [everRendered, setEverRendered] = useState(false);
  const [org, orgRequest] = usePrimaryOrganization();
  const params = useQuery<RequestResultQueryParams>();

  const [pageParam, setPageParam] = useState(0);
  const [sortParam, setSortParam] = useState<SortRule[]>([]);

  const filterParams = useMemo(() => queryParamsToFilters(params), [params]);

  const [dataRequestPage, requestsRequest] = useRequestSummaries(
    org.id,
    undefined,
    filterParams,
    sortParam,
    "all",
    Math.max(pageParam - 1, 0),
  );

  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest, requestsRequest)) {
      setEverRendered(true);
    }
  }, [orgRequest, requestsRequest]);

  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  const downloadUrl = formatUrl(
    ApiRouteHelpers.organizationRequestsCsv(org.id),
    filtersToParams(filterParams),
  );

  const resultCount = dataRequestPage?.meta?.totalCount ?? 0;
  const pageLabel = pluralize("Results", resultCount, true);

  return (
    <StandardPageWrapper fullWidth={true}>
      <PageHeader
        className="mb-lg"
        titleContent={`Found ${pageLabel}`}
        titleHeaderContent={
          <PageHeaderBack to={`${Routes.records.lookup}?lookupType=request`}>
            Back to Lookup
          </PageHeaderBack>
        }
        descriptionContent={
          resultCount > 0 && (
            <Button href={downloadUrl} download variant="contained" color="primary">
              Download as CSV
            </Button>
          )
        }
      />

      <RequestsTable
        columnSet={params.requestState ? columnSetForRequestState(params.requestState) : "active"}
        data={dataRequestPage?.data}
        loading={requestsRequest.running}
        showFilters={false}
        includeName={true}
        pageMeta={dataRequestPage?.meta}
        onPageChange={setPageParam}
        onSortChange={setSortParam}
        initialPageIndex={pageParam - 1}
        initialSortBy={sortParam}
      />
    </StandardPageWrapper>
  );
};
