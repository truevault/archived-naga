import { Check, Email, HourglassBottom, Refresh } from "@mui/icons-material";
import { Chip } from "@mui/material";
import _ from "lodash";
import React, { useCallback, useMemo } from "react";
import { Column, Filters, SortingRule } from "react-table";
import { SelectOption } from "../../../common/components/input/Select";
import { SelectFilter } from "../../../common/components/table/filters/SelectFilter";
import {
  PaginatedTable,
  PaginatedTableProps,
} from "../../../common/components/table/PaginatedTable";
import { DataSubjectRequestSummaryDto } from "../../../common/service/server/dto/DataSubjectRequestDto";
import { PaginatedMetaDto } from "../../../common/service/server/dto/PaginatedDto";
import { DataRequestFilter } from "../../../common/service/server/Types";
import { Fmt } from "../../formatters";
import { fmtRequestTypeState, fmtShortRequestTypeWithRegulation } from "../../formatters/request";
import { usePrimaryOrganization } from "../../hooks";
import { makeColumns } from "../../hooks/makeColumns";
import { RouteHelpers } from "../../root/routes";
import { useHistory } from "react-router";

const formatDate = (date?: string): string => {
  if (date === undefined || date === null || date === "") {
    return "";
  }

  return new Date(date).toLocaleDateString(undefined, {
    month: "short",
    day: "numeric",
    year: "numeric",
  });
};

const relevantDate = (tab: string, row: DataSubjectRequestSummaryDto): string => {
  switch (tab.toLowerCase()) {
    case "active":
      return row.dueDate;
    case "complete":
      return row.closedAt;
    case "pending":
      return row.requestDate;
    default:
      return "";
  }
};

const dueDateId = (tab: string): string => {
  switch (tab.toLowerCase()) {
    case "active":
      return "dueIn";
    case "complete":
      return "closedAt";
    case "pending":
      return "submitted";
    default:
      return "dueIn";
  }
};

const getDueDateColumn = (tab: string) => {
  let dueDateLabel = "Due";

  switch (tab.toLowerCase()) {
    case "complete":
      dueDateLabel = "Closed";
      break;
    case "pending":
      dueDateLabel = "Submitted";
      break;
  }

  return dueDateLabel;
};

export const CCPA_TYPE_FILTER_OPTIONS: SelectOption[] = [
  {
    value: "delete",
    label: "Delete",
  },
  {
    value: "know",
    label: "Know",
  },
  {
    value: "optout",
    label: "Opt-Out",
  },
];

export const GDPR_TYPE_FILTER_OPTIONS: SelectOption[] = [
  {
    value: "access",
    label: "Access",
  },
  {
    value: "erasure",
    label: "Erasure",
  },
  {
    value: "objection",
    label: "Objection",
  },
  {
    value: "rectification",
    label: "Rectification",
  },
  {
    value: "portability",
    label: "Portability",
  },
  {
    value: "withdrawal",
    label: "Withdrawal",
  },
];

const DATE_FILTER_OPTIONS: SelectOption[] = [
  {
    value: 1,
    label: "Tomorrow",
  },
  {
    value: 7,
    label: "Next 7 Days",
  },
  {
    value: 30,
    label: "Next 30 Days",
  },
];

export const STATE_FILTER_OPTIONS: SelectOption[] = [
  {
    value: "pending",
    label: "Pending",
  },
  {
    value: "closed",
    label: "Closed",
  },
  {
    value: "new",
    label: "New",
  },
  {
    value: "in_progress",
    label: "In Progress",
  },
];

const useColumns = (includeName: boolean = false) =>
  makeColumns((tab: string): Column<any>[] => {
    return [
      {
        Header: "",
        id: "icon",
        disableSortBy: true,
        disableFilters: true,
        Cell: RequestIconCell,
      },
      {
        Header: "Requester Email",
        id: "subjectEmailAddress",
        accessor: "subjectEmailAddress",
        Cell: ({ value, row }) => (
          <b>
            {value || Fmt.DataSubject.fmtFullName(row.original as DataSubjectRequestSummaryDto)}
          </b>
        ),
      },
      includeName && {
        Header: "First Name",
        id: "subjectFirstName",
        accessor: "subjectFirstName",
      },
      includeName && {
        Header: "Last Name",
        id: "subjectLastName",
        accessor: "subjectLastName",
      },
      {
        Header: "Request Type",
        id: "type",
        accessor: "dataRequestType.type",
        Cell: ({ value, row }) => (
          <>
            {fmtShortRequestTypeWithRegulation(value)}
            {row.original.context == "EMPLOYEE" ? (
              <Chip className="ml-md" label="HR" color="secondary" size="small" />
            ) : undefined}
          </>
        ),
        Filter: (props) => {
          const [org, orgRequest] = usePrimaryOrganization();

          const options = useMemo(
            () =>
              org.featureGdpr
                ? _.concat([], CCPA_TYPE_FILTER_OPTIONS, GDPR_TYPE_FILTER_OPTIONS)
                : _.concat([], CCPA_TYPE_FILTER_OPTIONS),
            [org],
          );

          return <SelectFilter {...props} loading={orgRequest.running} options={options} />;
        },
      },
      {
        Header: "Status",
        id: "state",
        accessor: "state",
        Filter: (props) => <SelectFilter {...props} options={STATE_FILTER_OPTIONS} />,
        Cell: ({ value }) => <>{fmtRequestTypeState(value)}</>,
      },
      {
        Header: getDueDateColumn(tab),
        id: dueDateId(tab),
        disableFilters: tab !== "active",
        accessor: (row) => relevantDate(tab, row as DataSubjectRequestSummaryDto),
        Cell: ({ value }) => <>{formatDate(value)}</>,
        Filter: (props) => <SelectFilter {...props} options={DATE_FILTER_OPTIONS} />,
      },
    ].filter((column) => column);
  });

const RequestIconCell: React.FC<{ row: any }> = ({ row }) => {
  const status = fmtRequestTypeState(row.original.state);
  let StateIcon = null;

  if (status === "New") {
    StateIcon = Email;
  } else if (status === "Closed") {
    StateIcon = Check;
  } else if (status === "Pending Verification") {
    StateIcon = HourglassBottom;
  } else if (status === "In Progress") {
    StateIcon = Refresh;
  }

  return StateIcon ? (
    <div className="d-flex align-items-center">
      <StateIcon fontSize="small" color="disabled" />
    </div>
  ) : null;
};

type RequestsTableProps = {
  columnSet: DataRequestFilter;
  data: DataSubjectRequestSummaryDto[];
  loading: boolean;
  pageMeta: PaginatedMetaDto;
  showFilters?: boolean;
  includeName?: boolean;
  onPageChange: (pageIndex: number) => void | Promise<void>;
  onSortChange: (sortBy: SortingRule<any>[]) => void | Promise<void>;
  onFiltersChange?: (sortBy: Filters<any>) => void | Promise<void>;
  initialPageIndex: number;
  initialSortBy: SortingRule<any>[];
  initialFilters?: Filters<any>;
  emptyState?: React.ReactNode;
} & Partial<PaginatedTableProps>;

export const RequestsTable: React.FC<RequestsTableProps> = ({
  columnSet,
  includeName,
  ...rest
}) => {
  const history = useHistory();
  const columns = useColumns(includeName)(columnSet);

  const selectRequest = useCallback((id: string) => {
    history.push(RouteHelpers.requests.detail(id));
  }, []);

  const canClick = columnSet !== "pending";

  return (
    <PaginatedTable
      enableRowClick={() => canClick}
      onRowClick={(row) => selectRequest(row.original.id)}
      columns={columns}
      {...rest}
    />
  );
};
