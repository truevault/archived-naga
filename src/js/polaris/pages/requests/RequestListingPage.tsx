import { Add } from "@mui/icons-material";
import { Button, Typography } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import { Filters } from "react-table";
import { NetworkRequest } from "../../../common/models";
import { Filter } from "../../../common/service/server/dto/FilterDto";
import { SortRule } from "../../../common/service/server/dto/SortRuleDto";
import { DataRequestFilter, DataRequestShowType } from "../../../common/service/server/Types";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { NewRequestDialog } from "../../components/dialog/NewRequestDialog";
import { PageHeader } from "../../components/layout/header/Header";
import {
  useOrganization,
  usePrimaryOrganizationId,
  useRequestSummaries,
  useTabs,
} from "../../hooks";
import { Routes } from "../../root/routes";
import { RequestsTable } from "./RequestsTable";
import { ScheduleTrainingDialog } from "../../components/dialog/ScheduleTrainingDialog";
import { useQueryModal } from "../../hooks/modalHooks";

type Props = {
  filter: DataRequestFilter;
  show?: DataRequestShowType;
};

type PageProps = RouteComponentProps & Props;

const EmptyInboxView = () => (
  <div className="w-100 text-center py-xl">
    <img src="/assets/images/stay_compliant/empty_inbox.svg" className="mb-lg" />

    <Typography variant="h6">Nice work! You have no open requests.</Typography>
    <Typography variant="subtitle1" className="text-muted">
      Nothing left to do but party
    </Typography>
  </div>
);

export const Listing = ({ filter: defaultTab }: PageProps) => {
  const [showScheduleTrainingModal, setShowScheduleTrainingModal] =
    useQueryModal("finished_get_compliant");

  const [everRendered, setEverRendered] = useState(false);
  const primaryOrgId = usePrimaryOrganizationId();

  const [, , refreshOrg] = useOrganization(primaryOrgId);

  const history = useHistory();

  const [pageParam, setPageParam] = useState(0);
  const [sortParam, setSortParam] = useState<SortRule[]>(() => {
    if (defaultTab === "active") {
      return [{ id: "dueIn", desc: false }];
    } else if (defaultTab === "complete") {
      return [{ id: "closedAt", desc: true }];
    } else if (defaultTab === "pending") {
      return [{ id: "submitted", desc: true }];
    }

    return [];
  });
  const [filterParam, setFilterParam] = useState<Filter[]>([]);

  const [filterPanelOpen, setFilterPanelOpen] = useState<boolean>(filterParam?.length > 0);

  const [tab, tabs] = useTabs(
    [
      { label: "Open", value: "active" },
      { label: "Closed", value: "complete" },
      { label: "Pending", value: "pending" },
    ],
    defaultTab,
    null,
    (newVal) => {
      if (newVal === "active") {
        history.push(Routes.requests.active);
      } else if (newVal === "complete") {
        history.push(Routes.requests.closed);
      } else if (newVal === "pending") {
        history.push(Routes.requests.pending);
      }

      refreshOrg();
    },
  );

  const [dataRequestPage, requestsRequest, fetch] = useRequestSummaries(
    primaryOrgId,
    tab as DataRequestFilter,
    filterParam,
    sortParam,
    "all",
    Math.max(pageParam - 1, 0),
  );

  const loading = useMemo(() => !NetworkRequest.areFinished(requestsRequest), [requestsRequest]);
  const [newRequestDialogOpen, setNewRequestDialogOpen] = useState(false);

  // ever rendered flag
  useEffect(() => {
    if (NetworkRequest.areFinished(requestsRequest)) {
      setEverRendered(true);
    }
  }, [requestsRequest]);

  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  return (
    <StandardPageWrapper>
      <PageHeader titleContent="Request Inbox" />

      <div className="mt-md">{tabs}</div>

      <div className="flex-row flex--end mt-xl">
        <Button variant="contained" onClick={() => setFilterPanelOpen(!filterPanelOpen)}>
          Filters
        </Button>

        <Button
          startIcon={<Add />}
          variant="contained"
          color="primary"
          onClick={() => setNewRequestDialogOpen(true)}
        >
          New Request
        </Button>
      </div>

      <RequestsTable
        columnSet={tab as DataRequestFilter}
        data={dataRequestPage?.data}
        loading={loading}
        filterPanelOpen={filterPanelOpen}
        pageMeta={dataRequestPage?.meta}
        onPageChange={setPageParam}
        onSortChange={setSortParam}
        onFiltersChange={setFilterParam}
        initialPageIndex={pageParam - 1}
        initialSortBy={sortParam}
        initialFilters={filterParam as Filters<any>}
        emptyState={tab === "active" ? <EmptyInboxView /> : undefined}
      />

      <NewRequestDialog
        open={newRequestDialogOpen}
        onClose={() => {
          setNewRequestDialogOpen(false);
          fetch();
        }}
      />

      <ScheduleTrainingDialog
        open={showScheduleTrainingModal}
        onClose={() => setShowScheduleTrainingModal(false)}
      />
    </StandardPageWrapper>
  );
};
