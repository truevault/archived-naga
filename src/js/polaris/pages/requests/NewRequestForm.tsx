import { Button } from "@mui/material";
import React from "react";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { Text } from "../../../common/components/input/Text";
import { Select } from "../../../common/components/input/Select";
import { DatePicker } from "../../../common/components/input/Date";
import { requiredString, requiredEmail, requiredDate } from "../../util/validation";
import { Checkbox } from "../../../common/components/input/Checkbox";
import { HelpPopover } from "../../components/HelpPopover";
import { DataRequestType } from "../../../common/service/server/Types";

export const newRequestFormValidation = (values: any[]): any => {
  const errors = {} as any;

  requiredString(values, "subjectFirstName", errors);
  requiredString(values, "subjectLastName", errors);
  requiredString(values, "submissionMethod", errors);
  requiredString(values, "dataRequestType", errors);
  requiredEmail(values, "subjectEmailAddress", errors);
  requiredDate(values, "requestDate", errors);

  return errors;
};

const SUBMISSION_METHOD_OPTIONS = [
  { label: "Telephone", value: "Telephone" },
  { label: "Email", value: "Email" },
];

type NewRequestFormProps = {
  handleSubmit: () => void;
  onCancel: () => void;
  dataRequestTypeOptions: Array<{ label: string; value: string }>;
  values: any;
  submitting: boolean;
};

const SKIP_HIDDEN_TYPES: DataRequestType[] = ["CCPA_RIGHT_TO_LIMIT", "CCPA_OPT_OUT"];
export const NewRequestForm: React.FC<NewRequestFormProps> = ({
  handleSubmit,
  dataRequestTypeOptions,
  onCancel,
  values,
  submitting,
}) => {
  return (
    <form onSubmit={handleSubmit} className="flex-row flex--expand new-request-form pt-xs">
      <div className="flex-col">
        <div className="flex-row flex--expand">
          <Text field="subjectFirstName" label="First Name" autoFocus required />
          <Text field="subjectLastName" label="Last Name" required />
        </div>

        <Select
          field="dataRequestType"
          label="Request Type"
          options={dataRequestTypeOptions}
          required
        />

        <Select
          field="submissionMethod"
          label="Submission Method"
          options={SUBMISSION_METHOD_OPTIONS}
          required
        />

        <Text field="subjectEmailAddress" label="Email Address" type="email" required />

        {values.submissionMethod === "Telephone" && (
          <Text field="subjectPhoneNumber" label="Telephone Number" type="tel" />
        )}

        {values.submissionMethod === "Email" &&
          !SKIP_HIDDEN_TYPES.includes(values.dataRequestType) && (
            <Checkbox
              field="skipEmailVerification"
              label={
                <>
                  Skip email verification{" "}
                  <HelpPopover label="Check this box if the consumer emailed you directly from the email address associated with the request and email verificiation is unnecessary." />
                </>
              }
            />
          )}

        <DatePicker field="requestDate" label="Request Date" required />

        <div>
          <LoadingButton
            loading={submitting}
            type="submit"
            variant="contained"
            color="primary"
            className="mr-md"
          >
            Create
          </LoadingButton>

          {onCancel && (
            <Button
              color="primary"
              disabled={submitting}
              onClick={(e) => {
                e.preventDefault();
                onCancel();
              }}
            >
              Cancel
            </Button>
          )}
        </div>
      </div>
    </form>
  );
};
