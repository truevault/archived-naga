import { Step, StepLabel, Stepper } from "@mui/material";
import React, { useEffect, useState, useCallback } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import SkipIcon from "../../../../static/assets/images/icon-skip-right-arrow.svg";
import { useApiTrigger } from "../../../common/hooks/api";
import { DataRequestDetail, NetworkRequest } from "../../../common/models";
import { NetworkRequestState } from "../../../common/models/NetworkRequest";
import { Api } from "../../../common/service/Api";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { DataRequestPublicId } from "../../../common/service/server/Types";
import { DataRequest } from "../../components";
import { DataRequestBody } from "../../components/dataRequest/stayCompliant/DataRequestBody";
import {
  DataRequestProcessingProvider,
  useDataRequestProcessingContext,
} from "../../components/dataRequest/stayCompliant/DataRequestProcessingContext";
import { PageHeader } from "../../components/layout/header/Header";
import { PageHeaderBack } from "../../components/layout/header/PageHeaderBack";
import { Error as RequestsError } from "../../copy/requests";
import {
  useOrganization,
  useOrganizationVendors,
  usePrimaryOrganizationId,
  useRequest,
} from "../../hooks";
import { Routes } from "../../root/routes";
import {
  ProgressFooter,
  ProgressFooterActions,
  ProgressFooterBack,
  ProgressFooterSaveNotification,
} from "../get-compliant/ProgressFooter";

type MatchParams = {
  requestId: DataRequestPublicId;
};

type Props = RouteComponentProps<MatchParams>;

export const Detail = ({ match }: Props) => {
  const primaryOrgId = usePrimaryOrganizationId();

  const [, organizationRequest, refreshOrganization] = useOrganization(primaryOrgId);
  const [request, requestRequest, refreshRequest, updateVendorOutcomeMutation] = useRequest(
    primaryOrgId,
    match.params.requestId,
  );
  const [vendors, vendorRequest, refreshVendors] = useOrganizationVendors(
    primaryOrgId,
    undefined,
    true,
  );

  const [everRendered, setEverRendered] = useState(false);

  const refreshAll = useCallback(() => {
    refreshOrganization();
    refreshRequest();
    refreshVendors();
  }, [refreshOrganization, refreshRequest, refreshVendors]);

  const forwardApi = useCallback(
    () => Api.dataRequest.stepRequestForward(primaryOrgId, request.id),
    [primaryOrgId, request?.id],
  );
  const [forwardStep, forwardStepRequest] = useApiTrigger(
    forwardApi,
    refreshAll,
    RequestsError.TransitionProcessing,
  );

  const backApi = useCallback(
    () => Api.dataRequest.stepRequestBack(primaryOrgId, request.id),
    [primaryOrgId, request?.id],
  );
  const [backStep, backStepRequest] = useApiTrigger(
    backApi,
    refreshAll,
    RequestsError.TransitionProcessing,
  );

  useEffect(() => {
    if (NetworkRequest.areFinished(requestRequest, vendorRequest, organizationRequest)) {
      setEverRendered(true);
    }
  }, [requestRequest, vendorRequest, organizationRequest]);

  // Page Load
  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  if (!request) {
    <StandardPageWrapper>
      <PageHeader titleContent="Uh oh... we couldn't find that request." />
    </StandardPageWrapper>;
  }

  const titleHeaderContent = (
    <PageHeaderBack to={`/requests/${request.state != "CLOSED" ? "active" : "closed"}`}>
      {request.state != "CLOSED" ? "Open" : "Closed"} Requests
    </PageHeaderBack>
  );

  return (
    <DataRequestProcessingProvider
      request={request}
      vendors={vendors}
      refreshRequest={refreshRequest}
      updateVendorOutcomeMutation={updateVendorOutcomeMutation}
    >
      <StandardPageWrapper
        footer={
          <Footer
            request={request}
            forwardStep={forwardStep}
            forwardStepRequest={forwardStepRequest}
            backStep={backStep}
            backStepRequest={backStepRequest}
          />
        }
      >
        <div className="max-width-896">
          <PageHeader
            titleContent={request.subjectEmailAddress}
            titleHeaderContent={titleHeaderContent}
          />
          <DataRequest.SubjectCard request={request} primaryOrgId={primaryOrgId} />
          <RequestStepper />
          <DataRequestBody request={request} />
        </div>
      </StandardPageWrapper>
    </DataRequestProcessingProvider>
  );
};

function SkipStepIcon() {
  return <SkipIcon />;
}

const RequestStepper = () => {
  const { steps, activeStep } = useDataRequestProcessingContext();

  return (
    <>
      <Stepper activeStep={activeStep} alternativeLabel className="stepper pt-std mb-std">
        {steps.map(({ name, status }, i) => (
          <Step key={name} active={i === activeStep}>
            {i < activeStep && status === "skipped" ? (
              <StepLabel StepIconComponent={SkipStepIcon}>{name}</StepLabel>
            ) : (
              <StepLabel>{name}</StepLabel>
            )}
          </Step>
        ))}
      </Stepper>
    </>
  );
};

type ContentProps = {
  request: DataRequestDetail;
  forwardStep: any;
  forwardStepRequest: NetworkRequestState;
  backStep: any;
  backStepRequest: NetworkRequestState;
};

const Footer = ({
  request,
  forwardStep,
  forwardStepRequest,
  backStep,
  backStepRequest,
}: ContentProps) => {
  const history = useHistory();

  const { stepCompleted, activeStep, lastStep, updateRequests } = useDataRequestProcessingContext();

  if (request.state === "CLOSED") {
    return null;
  }

  const buttonDisabled = backStepRequest.running || forwardStepRequest.running;

  return (
    <ProgressFooter
      actions={
        <ProgressFooterActions
          prevContent={<ProgressFooterBack />}
          prevDisabled={buttonDisabled}
          prevOnClick={
            activeStep === 0
              ? () => {
                  history.push(Routes.requests.active); // This needs to be able to go back to active or closed
                }
              : () => {
                  backStep();
                }
          }
          nextContent={lastStep ? "Close request" : "Next step"}
          nextDisabled={!stepCompleted || buttonDisabled}
          nextOnClick={() => {
            forwardStep();
          }}
        />
      }
      content={<ProgressFooterSaveNotification request={updateRequests} />}
    />
  );
};
