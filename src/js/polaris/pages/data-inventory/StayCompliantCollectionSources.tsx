import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useDataSourceCatalog } from "../../../common/hooks";
import { NetworkRequest } from "../../../common/models";
import { PageLoading } from "../../../common/components/Loading";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { ManageCollectionSources } from "../../components/collectionSources/ManageCollectionSources";
import { PageHeader } from "../../components/layout/header/Header";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { useOrgDataSources } from "../../hooks/useOrganizationVendors";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

export const StayCompliantCollectionSources: React.FC<void> = () => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  // Requests
  const [sources, sourcesRequest] = useOrgDataSources(organizationId);
  const [catalog, catalogRequest] = useDataSourceCatalog(organizationId);

  // Gate
  useEffect(() => {
    if (NetworkRequest.areFinished(catalogRequest, sourcesRequest)) {
      setReadyToRender(true);
    }
  }, [catalogRequest, sourcesRequest]);

  if (!readyToRender) {
    return <PageLoading />;
  }

  return (
    <StandardPageWrapper footer={<Footer />}>
      <PageHeader titleContent="Collection Sources" titleHeaderContent="Data Map" />
      <div className="data-collection--subheader mb-xl">
        <p className="text-large">
          From which sources does your business collection information about the consumer?
        </p>
      </div>

      <ManageCollectionSources sources={sources} orgId={organizationId} catalog={catalog} />
    </StandardPageWrapper>
  );
};

const Footer = () => {
  const history = useHistory();
  return (
    <ProgressFooter
      actions={
        <ProgressFooterActions
          nextContent="Done"
          nextOnClick={() => history.goBack()}
          prevOnClick={() => history.goBack()}
        />
      }
    />
  );
};
