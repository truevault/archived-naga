import React from "react";

import { NavLink } from "react-router-dom";
import { Routes } from "../../root/routes";

import { variables } from "../../../common/root/variables";

export const DataInventorySubnav = () => {
  return (
    <>
      <h2>Data Inventory</h2>

      <ul>
        <li>
          <NavLink
            to={Routes.dataInventory.collection.root}
            exact
            isActive={(match, location) =>
              location.pathname.startsWith(Routes.dataInventory.collection.root)
            }
            activeStyle={{ color: variables.colors.primary }}
          >
            Information Collection
          </NavLink>
        </li>
        <li>
          <NavLink
            to={Routes.dataInventory.disclosure.root}
            exact
            isActive={(match, location) =>
              location.pathname.startsWith(Routes.dataInventory.disclosure.root)
            }
            activeStyle={{ color: variables.colors.primary }}
          >
            Information Disclosure
          </NavLink>
        </li>
        <li>
          <NavLink
            to={Routes.dataInventory.sale.root}
            exact
            isActive={(match, location) =>
              location.pathname.startsWith(Routes.dataInventory.sale.root)
            }
            activeStyle={{ color: variables.colors.primary }}
          >
            Information Selling
          </NavLink>
        </li>
        <li>
          <NavLink
            to={Routes.dataInventory.ccpaDisclosure}
            exact
            isActive={(match, location) =>
              location.pathname.startsWith(Routes.dataInventory.ccpaDisclosure)
            }
            activeStyle={{ color: variables.colors.primary }}
          >
            CCPA Disclosures
          </NavLink>
        </li>
      </ul>
    </>
  );
};
