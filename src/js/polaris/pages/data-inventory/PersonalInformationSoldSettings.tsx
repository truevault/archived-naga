import { Button, Checkbox, FormControlLabel, Paper, Tooltip } from "@mui/material";
import clsx from "clsx";
import React, { useEffect, useState } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import { PageLoading } from "../../../common/components/Loading";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { PageHeader } from "../../../common/layout/PageHeader";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { UUIDString } from "../../../common/service/server/Types";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { Error as DataInventoryError } from "../../copy/dataInventory";
import { useOrganizationVendorDetails, usePrimaryOrganizationId } from "../../hooks";
import { useCollectionGroups } from "../../hooks/useCollectionGroups";
import { Routes } from "../../root/routes";
import { getGroupedCategories, getOrderedGroups } from "../../util/dataInventory";
import { DataInventorySubnav } from "./DataInventorySubnav";

type MatchParams = {
  vendorId: UUIDString;
};

type Props = RouteComponentProps<MatchParams>;

type SoldCategoriesMap = Map<UUIDString, Record<string, boolean>>;
type SoldSettingsMap = Map<UUIDString, SoldCategoriesMap>;

export const PersonalInformationSoldSettings = ({ match }: Props) => {
  const vendorId = match.params.vendorId;
  const organizationId = usePrimaryOrganizationId();
  const history = useHistory();
  const [everRendered, setEverRendered] = useState(false);
  const [orderedGroups, setOrderedGroups] = useState([]);
  const [groupedCategoriesByDst, setGroupedCategoriesByDst] = useState({});
  const [enabledSoldSettings, setEnabledSoldSettings] = useState(
    // data-subject-type->personal-information-category->enabled/sold
    new Map() as SoldSettingsMap,
  );
  const [showAllCategoriesState, setShowAllCategoriesState] = useState(new Set<string>());
  const [showAllDst, setShowAllDst] = useState(false);

  const [vendorDetails, vendorDetailsRequest] = useOrganizationVendorDetails(
    organizationId,
    vendorId,
  );
  const [dataSubjectTypes, dataSubjectTypesRequest] = useCollectionGroups(organizationId);

  const { result: personalInformationExchanged, request: getExchangedRequest } = useDataRequest({
    queryKey: ["personalInformationExchanged", organizationId, vendorId],
    api: () => Api.dataInventory.getPersonalInformationExchangedVendor(organizationId, vendorId),
  });

  const { fetch: updatePersonalInformationSold, request: updateRequest } = useActionRequest({
    api: (values) =>
      Api.dataInventory.setPersonalInformationExchanged(organizationId, vendorId, values),
    messages: {
      forceError: DataInventoryError.UpdateInformationSelling,
    },
    onSuccess: () => history.push(Routes.dataInventory.sale.root),
  });

  const handleSoldChanged = (dstId, categoryId, sold) => {
    setEnabledSoldSettings((prev) => {
      const prevSettings = prev.get(dstId);
      const newState = Object.assign(
        { enabled: false, disclosed: false },
        prevSettings.get(categoryId),
        { sold: sold },
      );
      return new Map(prev.set(dstId, prevSettings.set(categoryId, newState)));
    });
  };

  const handleShowAllCategories = (dstId) =>
    setShowAllCategoriesState((prev) => {
      prev.has(dstId) ? prev.delete(dstId) : prev.add(dstId);
      return new Set(prev);
    });

  const handleShowAllDst = () => setShowAllDst((prev) => !prev);

  const handleSelectAll = (dstId) =>
    setEnabledSoldSettings((prev) => {
      const updateAll = showAllCategoriesState.has(dstId);
      const dstSettings =
        enabledSoldSettings.get(dstId) ||
        (new Map(
          personalInformationExchanged
            .find((pie) => pie.dataSubjectTypeId === dstId)
            .personalInformationCategories.map((c) => [
              c.id,
              { enabled: false, disclosed: false, sold: false },
            ]),
        ) as SoldCategoriesMap);
      const currentSettings = Array.from(dstSettings.entries());
      const newState = new Map(
        currentSettings.map(([cid, setting]) => [
          cid,
          Object.assign({ enabled: false }, setting, {
            sold: updateAll
              ? !setting.disclosed
              : !setting.disclosed && (setting.enabled || setting.sold),
          }),
        ]),
      );
      return new Map(prev.set(dstId, newState));
    });

  const handleSave = () => {
    updatePersonalInformationSold(
      Array.from(enabledSoldSettings.entries()).map(([dstId, settings]) => {
        return {
          dataSubjectTypeId: dstId,
          personalInformationCategories: Array.from(settings.entries())
            .filter(([, { sold }]) => sold)
            .map(([catId]) => catId),
        };
      }),
    );
  };

  const dstHasEnabled = (dstId) =>
    Array.from(enabledSoldSettings.get(dstId)?.values() || []).some((c) => c.enabled);

  const dstHasDisclosed = (dstId) =>
    Array.from(enabledSoldSettings.get(dstId)?.values() || []).some((c) => c.disclosed);

  const dstHasSold = (dstId) =>
    Array.from(enabledSoldSettings.get(dstId)?.values() || []).some((c) => c.sold);

  useEffect(() => {
    if (
      NetworkRequest.areFinished(vendorDetailsRequest, getExchangedRequest, dataSubjectTypesRequest)
    ) {
      setOrderedGroups(
        getOrderedGroups(personalInformationExchanged[0].personalInformationCategories),
      );
      setGroupedCategoriesByDst(
        personalInformationExchanged.reduce(
          (acc, pie) =>
            Object.assign(acc, {
              [pie.dataSubjectTypeId]: getGroupedCategories(pie.personalInformationCategories),
            }),
          {},
        ),
      );
      // For the enabled/sold settings we need to go through each data-subject-type/consumer-group
      const soldByDstId = {};
      personalInformationExchanged.forEach(
        (pis) => (soldByDstId[pis.dataSubjectTypeId] = pis.personalInformationCategories),
      );
      const updatedSettings = new Map(
        dataSubjectTypes.map((dst) => {
          const soldSettings = soldByDstId[dst.id];
          const enabledSoldMap = new Map(
            soldSettings.map((category) => [
              category.id,
              {
                enabled: category.enabled,
                disclosed: category.disclosed,
                sold: category.sold,
              },
            ]),
          );
          return [dst.id, enabledSoldMap];
        }),
      ) as SoldSettingsMap;
      setEnabledSoldSettings(updatedSettings);
      setEverRendered(true);
    }
  }, [vendorDetailsRequest, getExchangedRequest, dataSubjectTypesRequest]);

  // Page Load
  if (!everRendered) {
    return <PageLoading />;
  }

  return (
    <StandardPageWrapper navContent={<DataInventorySubnav />}>
      <PageHeader header="Information Selling" />

      <Paper>
        <div className="flex-row p-xl personal-information-options-header">
          <div className="flex-col options-header--name">
            <dl className="data-point-1 mt-0 mb-0">
              <dt>Vendor</dt>
              <dd>
                <div className="service-header--service">
                  <img className="service-header--logo" src={vendorDetails.logoUrl} />
                  {vendorDetails.name}
                </div>
              </dd>
            </dl>
          </div>
          <div className="flex-col options-header--actions">
            <div className="service-header--buttons">
              <div className="service-header--buttons-block">
                <Button
                  fullWidth={false}
                  className="mr-md"
                  href={Routes.dataInventory.sale.root}
                  disabled={updateRequest.running}
                >
                  Cancel
                </Button>
                <LoadingButton
                  fullWidth={false}
                  color="primary"
                  onClick={handleSave}
                  loading={updateRequest.running}
                >
                  Save
                </LoadingButton>
              </div>
            </div>
          </div>
        </div>

        <div className="consumer-groups-container">
          {dataSubjectTypes
            .slice(0)
            .map((dst) =>
              Object.assign({}, dst, {
                hasEnabled: dstHasEnabled(dst.id),
                hasDisclosed: dstHasDisclosed(dst.id),
                hasSold: dstHasSold(dst.id),
              }),
            )
            .filter((dst) => showAllDst || dst.hasEnabled || dst.hasSold)
            .sort((a, b) => {
              if (a.hasEnabled && !b.hasEnabled) {
                return -1;
              } else if (!a.hasEnabled && b.hasEnabled) {
                return 1;
              }
              return a.name.localeCompare(b.name);
            })
            .map((dst) => {
              const showAllCategories = showAllCategoriesState.has(dst.id);
              return (
                <div className="personal-information-options-section" key={`dst-section-${dst.id}`}>
                  <div className="flex-row">
                    <div className="flex-col options-header--name">
                      <h2 className="mt-0 mb-lg">{dst.name}</h2>
                    </div>
                    <div className="flex-col options-header--actions">
                      <div className="toggle-buttons-container">
                        <Button
                          className="toggle-button mr-sm"
                          color="primary"
                          onClick={() => handleShowAllCategories(dst.id)}
                        >
                          {showAllCategories ? "Show collected" : "Show all"}
                        </Button>
                        <Button
                          color="primary"
                          className="toggle-button"
                          onClick={() => handleSelectAll(dst.id)}
                        >
                          select all
                        </Button>
                      </div>
                    </div>
                  </div>
                  {!(dst.hasEnabled || dst.hasSold || showAllCategories)
                    ? 'Click "show all" to map personal information your organization is collecting and selling.'
                    : orderedGroups.map((group) => {
                        const categories = groupedCategoriesByDst[dst.id][group];
                        const categorySettings = enabledSoldSettings.get(dst.id);
                        const categoryIds = categories.map((c) => c.id);
                        const visibleCategoryIds = Array.from(categorySettings.entries())
                          .filter(([cid, s]) => categoryIds.includes(cid) && (s.enabled || s.sold))
                          .map(([cid]) => cid);
                        const hasVisible = showAllCategories || visibleCategoryIds.length > 0;
                        return hasVisible ? (
                          <div
                            className="personal-information-category-section"
                            key={`category-group-${group}`}
                          >
                            <div className="flex-row personal-information-category-group">
                              {group}
                            </div>
                            <div className="flex-row personal-information-options-row">
                              {categories
                                .slice(0)
                                .filter(
                                  (category) =>
                                    showAllCategories || visibleCategoryIds.includes(category.id),
                                )
                                .sort((a, b) => {
                                  const aVis = categorySettings.get(a.id).enabled;
                                  const bVis = categorySettings.get(b.id).enabled;
                                  if (aVis && !bVis) {
                                    return -1;
                                  } else if (!aVis && bVis) {
                                    return 1;
                                  }
                                  return a.groupDisplayOrder - b.groupDisplayOrder;
                                })
                                .map((category) => {
                                  const isDisclosed = categorySettings.get(category.id).disclosed;
                                  const isSold = categorySettings.get(category.id).sold;
                                  const isCollected = categorySettings.get(category.id).enabled;
                                  const checkbox = (
                                    <Checkbox
                                      onChange={({ target: { value, checked, name } }) => {
                                        handleSoldChanged(name, value, checked);
                                      }}
                                      disabled={isDisclosed}
                                      checked={isSold}
                                      value={category.id}
                                      name={dst.id}
                                      color="primary"
                                    />
                                  );
                                  return (
                                    <div
                                      key={`category-option-${category.id}`}
                                      className={clsx("personal-information-checkbox", {
                                        "newly-collected": isSold && !isCollected,
                                        "not-collected": !(isCollected || isSold),
                                      })}
                                    >
                                      <Tooltip title={category.description}>
                                        <FormControlLabel
                                          className="mr-0"
                                          control={
                                            isDisclosed ? (
                                              <Tooltip
                                                placement="top-start"
                                                title="Already specified as disclosure, you must remove the disclosure setting to specify as selling"
                                              >
                                                <div>{checkbox}</div>
                                              </Tooltip>
                                            ) : (
                                              checkbox
                                            )
                                          }
                                          label={category.name}
                                        />
                                      </Tooltip>
                                    </div>
                                  );
                                })}
                            </div>
                          </div>
                        ) : (
                          ""
                        );
                      })}
                </div>
              );
            })}
        </div>
        <div className="p-xl">
          <Button color="primary" onClick={handleShowAllDst}>
            {showAllDst ? "Show Configured Consumer Groups" : "Show All Consumer Groups"}
          </Button>
        </div>
      </Paper>
    </StandardPageWrapper>
  );
};
