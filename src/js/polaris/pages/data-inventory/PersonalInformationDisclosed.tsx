import ErrorIcon from "@mui/icons-material/Error";
import { Button, Paper } from "@mui/material";
import clsx from "clsx";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { PageLoading } from "../../../common/components/Loading";
import { useDataRequest } from "../../../common/hooks/api";
import { PageHeader } from "../../../common/layout/PageHeader";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { useCollectionGroups } from "../../hooks/useCollectionGroups";
import { RouteHelpers, Routes } from "../../root/routes";
import { flatMap, uniqueArray } from "../../util";
import { getCompleteConsumerGroups } from "../../util/dataInventory";
import { isThirdParty, organizationVendorIsComplete } from "../../util/vendors";
import { DataInventorySubnav } from "./DataInventorySubnav";

export const PersonalInformationDisclosed = () => {
  const organizationId = usePrimaryOrganizationId();
  const [everRendered, setEverRendered] = useState(false);
  const [vendorsByClassification, setVendorsByClassification] = useState({
    "third-party": [],
    "service-provider": [],
    contractor: [],
  } as Record<string, OrganizationDataRecipientDto[]>);
  const [completeVendors, setCompleteVendors] = useState([]);
  const [incompleteConsumerGroups, setIncompleteConsumerGroups] = useState(0);
  const vendorClassifications: [string, string, string][] = [
    ["third-party", "Third Party", "Third Parties"],
    ["service-provider", "Service Provider", "Service Providers"],
    ["contractor", "Contractor", "Contractors"],
  ];

  const [consumerGroups, consumerGroupsRequest] = useCollectionGroups(organizationId);
  const [vendors, vendorsRequest] = useOrganizationVendors(organizationId);
  const { result: snapshotData, request: snapshotDataRequest } = useDataRequest({
    queryKey: ["snapshotData", organizationId],
    api: () => Api.dataInventory.getSnapshot(organizationId),
  });

  useEffect(() => {
    if (NetworkRequest.areFinished(vendorsRequest, snapshotDataRequest, consumerGroupsRequest)) {
      setVendorsByClassification((prev) => {
        return vendors
          .filter(
            (ov) =>
              organizationVendorIsComplete(ov) && ov.classification?.slug !== "no-data-sharing",
          )
          .reduce((acc, ov) => {
            const slug = isThirdParty(ov) ? "third-party" : ov.classification?.slug;
            if (!acc[slug]) {
              acc[slug] = [];
            }
            acc[slug].push(ov);
            return acc;
          }, Object.assign({}, prev));
      });

      // @ts-ignore
      const completedGroups = getCompleteConsumerGroups(snapshotData);
      setIncompleteConsumerGroups(
        consumerGroups
          .filter((cg) => cg.enabled)
          .map((cg) => cg.id)
          .filter((cgid) => !completedGroups.includes(cgid)).length,
      );
      setCompleteVendors(
        uniqueArray(
          // @ts-ignore
          flatMap(snapshotData, (sd) =>
            // @ts-ignore
            flatMap(sd.personalInformationCategories, (c) => c.disclosedVendorIds),
          ),
        ),
      );
      setEverRendered(true);
    }
  }, [vendorsRequest, snapshotDataRequest, consumerGroupsRequest]);

  // Page Load
  if (!everRendered) {
    return <PageLoading />;
  }

  return (
    <StandardPageWrapper navContent={<DataInventorySubnav />}>
      <PageHeader header="Information Disclosure" />

      {incompleteConsumerGroups > 0 && (
        <Paper className="mt-0 mb-xl p-md flex-container flex--align-center">
          <ErrorIcon className="service-in-progress-warning" />
          <span className="ml-md">
            You have {incompleteConsumerGroups} in-progress consumer group
            {incompleteConsumerGroups > 1 && "s"} to complete
          </span>
          <Button
            className="ml-md"
            color="primary"
            component={Link}
            to={Routes.dataInventory.collection.root}
          >
            Resume
          </Button>
        </Paper>
      )}

      <Paper>
        <div className="personal-information-settings p-xl">
          <h2 className="mt-0">Map Disclosure by Vendor</h2>
          {vendorClassifications.map(([vcSlug, vcSingular, vcPlural]) => {
            return (
              <div className="service-type-container" key={`service-type-${vcSlug}`}>
                <h3 className="service-type-header">{vcPlural}</h3>
                <div>
                  {vendorsByClassification[vcSlug].length === 0 ? (
                    <div className="services-empty-state">
                      Add a <a href={Routes.vendors.root}>{vcSingular}</a> first.
                    </div>
                  ) : (
                    vendorsByClassification[vcSlug]
                      .sort((a, b) => a.name.localeCompare(b.name))
                      .map((v) => {
                        const isComplete = completeVendors.includes(v.vendorId);
                        return (
                          <div className="service-row" key={`dst-${v.id}`}>
                            <div className="service-col__logo">
                              <div className="service-logo">
                                <img src={v.logoUrl} />
                              </div>
                            </div>
                            <div className="service-col__name">{v.name}</div>
                            <div
                              className={clsx("service-col__status", {
                                "text-warning": isComplete,
                              })}
                            >
                              {!isComplete ? "In-Progress" : ""}
                            </div>
                            <div className="service-col__edit">
                              <div className="mt-xs">
                                <Button
                                  color="primary"
                                  href={RouteHelpers.dataInventory.disclosure.detail(v.vendorId)}
                                >
                                  Edit
                                </Button>
                              </div>
                            </div>
                          </div>
                        );
                      })
                  )}
                </div>
              </div>
            );
          })}
        </div>
      </Paper>
    </StandardPageWrapper>
  );
};
