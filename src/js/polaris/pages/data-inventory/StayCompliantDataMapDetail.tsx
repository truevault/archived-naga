import { Button, Typography } from "@mui/material";
import clsx from "clsx";
import React, { useEffect, useMemo, useState } from "react";
import { Redirect, RouteComponentProps, useHistory } from "react-router";
import { PageLoading } from "../../../common/components/Loading";
import { useDataRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../../common/service/server/dto/CollectionGroupDto";
import { CategoryDto, DataMapDto } from "../../../common/service/server/dto/DataMapDto";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  PersonalInformationCategoryDto,
  PersonalInformationCollectedDto,
} from "../../../common/service/server/dto/PersonalInformationOptionsDto";
import { UUIDString } from "../../../common/service/server/Types";
import { Callout, CalloutVariant } from "../../components/Callout";
import { CollectionPurposesDialog } from "../../components/dialog/data-map/CollectionPurposesDialog";
import { InternalCollectionDialog } from "../../components/dialog/data-map/InternalCollectionDialog";
import { MapVendorFieldsDialog } from "../../components/dialog/data-map/MapVendorFieldsDialog";
import { HelpPopover } from "../../components/HelpPopover";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { useCollectionGroups } from "../../hooks/useCollectionGroups";
import { useDataMap } from "../../hooks/useDataMap";
import {
  useOrgDataSources,
  useUnmappedOrganizationVendors,
} from "../../hooks/useOrganizationVendors";
import { Routes } from "../../root/routes";
import { alphabetically } from "../../surveys/util";
import { isEmploymentGroup } from "../../util/consumerGroups";
import { personalInformationCategorySorter } from "../../util/dataInventory";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

type MatchParams = {
  dataSubjectTypeId: UUIDString;
};

type Props = RouteComponentProps<MatchParams>;

export const StayCompliantDataMapDetail = ({ match }: Props) => {
  const dataSubjectTypeId = match.params.dataSubjectTypeId;
  const organizationId = usePrimaryOrganizationId();
  const [everRendered, setEverRendered] = useState(false);
  const [dataSubjectType, setDataSubjectType] = useState(null);

  const [dataSubjectTypes, dataSubjectTypesRequest] = useCollectionGroups(organizationId);

  // requests
  const [orgVendors, orgVendorsRequest] = useOrganizationVendors(organizationId);
  const [unmappedVendors, unmappedVendorsRequest, refreshUnmappedVendors] =
    useUnmappedOrganizationVendors(organizationId, dataSubjectTypeId);

  const {
    result: personalInformationCollected,
    request: getCollectedRequest,
    refresh: refreshPersonalInformationCollected,
  } = useDataRequest({
    queryKey: ["personalInformationCollected", organizationId, dataSubjectTypeId],
    api: () => Api.dataInventory.getPersonalInformationCollected(organizationId, dataSubjectTypeId),
  });

  const [sources, sourcesRequest] = useOrgDataSources(organizationId);
  const [dataMap, dataMapRequest] = useDataMap(organizationId);

  useEffect(() => {
    if (
      NetworkRequest.areFinished(
        dataSubjectTypesRequest,
        getCollectedRequest,
        orgVendorsRequest,
        sourcesRequest,
        dataMapRequest,
        unmappedVendorsRequest,
      )
    ) {
      const dst = dataSubjectTypes.filter((t) => t.id === dataSubjectTypeId);
      if (dst.length == 1) {
        setDataSubjectType(dst[0]);
      } else {
        console.error("Unable to find consumer group with id " + dataSubjectTypeId);
      }
      setEverRendered(true);
    }
  }, [
    dataSubjectTypeId,
    dataSubjectTypes,
    dataSubjectTypesRequest,
    getCollectedRequest,
    orgVendorsRequest,
    sourcesRequest,
    dataMapRequest,
    unmappedVendorsRequest,
  ]);

  const sortedOrgVendors = useMemo(() => {
    return orgVendors?.sort(alphabetically);
  }, [orgVendors]);

  // Page Load
  if (!everRendered) {
    return <PageLoading />;
  }

  // Catch incorrect ID
  if (!dataSubjectType) {
    return <Redirect to="/404" />;
  }

  const isEmployment = isEmploymentGroup(dataSubjectType);

  const mappedVendorIds = [
    ...new Set(personalInformationCollected.exchanged.map((exc) => exc.vendorId)),
  ];

  const mappedVendors = sortedOrgVendors?.filter((v) => mappedVendorIds.includes(v.vendorId));

  const sharingVendors = mappedVendors?.filter((v) => v.ccpaIsSharing);
  const sellingVendors = mappedVendors?.filter((v) => v.ccpaIsSelling && !v.ccpaIsSharing);
  const disclosureVendors = mappedVendors?.filter((v) => !v.ccpaIsSelling && !v.ccpaIsSharing);

  const refreshAll = () =>
    Promise.all([refreshPersonalInformationCollected(), refreshUnmappedVendors()]);
  return (
    <StandardPageWrapper footer={<Footer />}>
      <PageHeader titleContent={dataSubjectType.name} titleHeaderContent="Data Map" />

      {!isEmployment && (
        <>
          <UnmappedVendors
            vendors={unmappedVendors}
            collected={personalInformationCollected}
            consumerGroup={dataSubjectType}
            refresh={refreshAll}
            dataMap={dataMap}
          />
          <NonEmploymentSections
            disclosureVendors={disclosureVendors}
            sharingVendors={sharingVendors}
            personalInformationCollected={personalInformationCollected}
            dataSubjectType={dataSubjectType}
            sources={sources}
            refreshPersonalInformationCollected={refreshAll}
            sellingVendors={sellingVendors}
            dataMap={dataMap}
          />
        </>
      )}

      {isEmployment && (
        <EmploymentSections
          personalInformationCollected={personalInformationCollected}
          dataSubjectType={dataSubjectType}
          refreshPersonalInformationCollected={refreshAll}
        />
      )}
    </StandardPageWrapper>
  );
};

const NonEmploymentSections = ({
  disclosureVendors,
  sharingVendors,
  personalInformationCollected,
  dataSubjectType,
  refreshPersonalInformationCollected,
  sources,
  sellingVendors,
  dataMap,
}) => {
  return (
    <>
      <SectionHeader help="Personal information your business discloses to service providers and third parties for business purposes.">
        Data Disclosure
      </SectionHeader>
      <VendorsMappings
        vendors={disclosureVendors}
        collected={personalInformationCollected}
        consumerGroup={dataSubjectType}
        refresh={refreshPersonalInformationCollected}
        dataMap={dataMap}
        empty={<Empty>You have no disclosure vendors.</Empty>}
      />
      <SectionHeader help="Personal information your business provides to third parties for purposes of interest-based or behavioral advertising.">
        Data Sharing
      </SectionHeader>
      <VendorsMappings
        vendors={sharingVendors}
        collected={personalInformationCollected}
        consumerGroup={dataSubjectType}
        refresh={refreshPersonalInformationCollected}
        dataMap={dataMap}
        empty={
          <Empty>
            You have indicated that you aren’t sharing data. If this is incorrect, update sharing
            settings in <a href={Routes.dataRecipients.root}>Data Recipients</a>.
          </Empty>
        }
      />

      <SectionHeader help="Personal information your business provides to third parties in exchange for money or anything else of value.">
        Data Selling
      </SectionHeader>
      <VendorsMappings
        vendors={sellingVendors}
        collected={personalInformationCollected}
        consumerGroup={dataSubjectType}
        refresh={refreshPersonalInformationCollected}
        dataMap={dataMap}
        empty={
          <Empty>
            You have indicated that you aren’t selling data. If this is incorrect, update selling
            settings in <a href={Routes.dataRecipients.root}>Data Recipients</a>.
          </Empty>
        }
      />

      <SectionHeader help="Personal information your business collects but that it does not disclose, share or sell to any other organization.">
        Undisclosed Data (Internal Use)
      </SectionHeader>
      <UndisclosedData
        collected={personalInformationCollected}
        consumerGroup={dataSubjectType}
        refresh={refreshPersonalInformationCollected}
      />

      <SectionHeader help="Categories of persons or entities from which your business collects personal information.">
        Collection Sources
      </SectionHeader>
      <CollectionSources sources={sources} />

      <SectionHeader help="The business purposes for which your business collects personal information about consumers.">
        Collection Purposes
      </SectionHeader>
      <CollectionPurposes
        collected={personalInformationCollected}
        consumerGroup={dataSubjectType}
        refresh={refreshPersonalInformationCollected}
      />
    </>
  );
};

const EmploymentSections = ({
  personalInformationCollected,
  dataSubjectType,
  refreshPersonalInformationCollected,
}) => {
  return (
    <>
      <SectionHeader help="The categories of personal information your business collects about consumers.">
        Data Collection
      </SectionHeader>
      <UndisclosedData
        collected={personalInformationCollected}
        consumerGroup={dataSubjectType}
        refresh={refreshPersonalInformationCollected}
      />

      <SectionHeader help="The business purposes for which your business collects personal information about consumers.">
        Collection Purposes
      </SectionHeader>
      <CollectionPurposes
        collected={personalInformationCollected}
        consumerGroup={dataSubjectType}
        refresh={refreshPersonalInformationCollected}
      />
    </>
  );
};

type UndisclosedDataProps = {
  consumerGroup: CollectionGroupDetailsDto;
  collected: PersonalInformationCollectedDto;
  refresh: () => void;
};

const UndisclosedData: React.FC<UndisclosedDataProps> = ({ collected, consumerGroup, refresh }) => {
  const [open, setOpen] = useState(false);
  const close = () => {
    setOpen(false);
    refresh();
  };
  const internal = collected.personalInformationCategories.filter(
    (pic) => pic.collected && !pic.exchanged,
  );

  return (
    <>
      {internal.length == 0 ? (
        <LabelAndEdit label="No internal collection" italic onEdit={() => setOpen(true)} />
      ) : (
        <LabelAndEdit label={categoryDisplay(internal)} onEdit={() => setOpen(true)} />
      )}

      <InternalCollectionDialog open={open} onClose={close} consumerGroup={consumerGroup} />
    </>
  );
};

type CollectionSourcesProps = {
  sources: OrganizationDataSourceDto[];
};

const CollectionSources: React.FC<CollectionSourcesProps> = ({ sources }) => {
  const label = sources
    ?.sort((a, b) => a.name.localeCompare(b.name))
    ?.map((s) => s.name)
    ?.join(", ");
  const editUrl = Routes.dataInventory.sources.root;

  return (
    <>
      {sources.length == 0 ? (
        <LabelAndEdit label="No collection sources" italic editUrl={editUrl} />
      ) : (
        <LabelAndEdit label={label} editUrl={editUrl} />
      )}
    </>
  );
};

const CollectionPurposes: React.FC<UndisclosedDataProps> = ({
  consumerGroup,
  collected,
  refresh,
}) => {
  const [open, setOpen] = useState(false);
  const close = () => {
    setOpen(false);
    refresh();
  };
  const purposes = collected.businessPurposes.filter((p) => p.enabled);
  const label = purposes.map((s) => s.name).join(", ");

  return (
    <>
      {purposes.length == 0 ? (
        <LabelAndEdit label="No collection purposes" italic onEdit={() => setOpen(true)} />
      ) : (
        <LabelAndEdit label={label} onEdit={() => setOpen(true)} />
      )}
      <CollectionPurposesDialog
        open={open}
        onClose={close}
        consumerGroup={consumerGroup}
        purposes={collected.businessPurposes}
      />
    </>
  );
};

type VendorsMappingProps = {
  vendors: OrganizationDataRecipientDto[];
  collected: PersonalInformationCollectedDto;
  empty?: React.ReactNode;
  consumerGroup: CollectionGroupDetailsDto;
  refresh: () => void;
  dataMap: DataMapDto;
};

const UnmappedVendors: React.FC<VendorsMappingProps> = ({
  consumerGroup,
  vendors,
  empty,
  refresh,
}) => {
  const [vendor, setVendor] = useState<OrganizationDataRecipientDto | null>(null);
  const open = (vendor: OrganizationDataRecipientDto) => setVendor(vendor);
  const close = () => {
    setVendor(null);
    refresh();
  };

  if (!vendors || vendors.length == 0) {
    return empty ? <>{empty}</> : null;
  }

  return (
    <>
      <div className="mt-md">
        <Callout variant={CalloutVariant.Yellow} className={"flex_between"}>
          <Typography variant="body2" gutterBottom>
            You have {vendors.length} unmapped {vendors.length == 1 ? "vendor" : "vendors"}.
          </Typography>

          <Typography variant="body2" component="div">
            <div>
              {vendors.map((v) => {
                return (
                  <div
                    key={v.id}
                    className="mb-xs"
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    <strong>{v.name}</strong>
                    <Button
                      color="warning"
                      size="xs"
                      variant="flat"
                      className="ml-md"
                      onClick={() => open(v)}
                    >
                      map
                    </Button>
                  </div>
                );
              })}
            </div>
          </Typography>
        </Callout>
      </div>

      {Boolean(vendor) && (
        <MapVendorFieldsDialog
          open={Boolean(vendor)}
          onClose={close}
          consumerGroup={consumerGroup}
          vendor={vendor}
        />
      )}
    </>
  );
};

const VendorsMappings: React.FC<VendorsMappingProps> = ({
  vendors,
  collected,
  empty,
  consumerGroup,
  refresh,
  dataMap,
}) => {
  const [vendor, setVendor] = useState<OrganizationDataRecipientDto | null>(null);
  const open = (vendor: OrganizationDataRecipientDto) => setVendor(vendor);
  const close = () => {
    setVendor(null);
    refresh();
  };

  if (!vendors || vendors.length == 0) {
    return empty ? <>{empty}</> : null;
  }

  return (
    <>
      {vendors
        .map<React.ReactNode>((dv) => {
          const disclosedIds =
            dataMap?.disclosure.find(
              (d) => d.collectionGroupId == consumerGroup.id && d.vendorId == dv.vendorId,
            )?.disclosed || [];
          const noDataHandled = `${dv.name} does not handle any data about ${consumerGroup.name}`;
          const disclosed = dataMap.categories.filter((pic) => disclosedIds.includes(pic.id));

          collected.exchanged
            .find((exc) => exc.vendorId == dv.vendorId)
            ?.personalInformationCategories.filter((pic) => pic.collected);
          return (
            <div key={dv.id}>
              <h4>{dv.name}</h4>
              <LabelAndEdit
                label={categoryDisplay(disclosed) || noDataHandled}
                onEdit={() => open(dv)}
              />
            </div>
          );
        })
        .reduce((prev, curr) => [prev, <hr key="hr" />, curr])}

      {Boolean(vendor) && (
        <MapVendorFieldsDialog
          open={Boolean(vendor)}
          onClose={close}
          consumerGroup={consumerGroup}
          vendor={vendor}
        />
      )}
    </>
  );
};

type SectionHeaderProps = {
  help?: React.ReactNode;
};

const SectionHeader: React.FC<SectionHeaderProps> = ({ children, help }) => {
  return (
    <>
      <h4 className="intermediate mt-xl mb-0 flex-center-vertical">
        {children} <HelpPopover className="ml-sm" label={help} />
      </h4>
      <hr />
    </>
  );
};

type LabelAndEditProps = {
  label: string;
  italic?: boolean;
  onEdit?: () => void;
  editUrl?: string;
};
const LabelAndEdit: React.FC<LabelAndEditProps> = ({ label, italic, onEdit, editUrl }) => {
  const textClass = clsx("text-t3", {
    "text-style-italic": italic,
  });

  editUrl = editUrl || "#";

  return (
    <p className={textClass}>
      {label}
      <span className="ml-md">
        <a href={editUrl} onClick={() => onEdit && onEdit()}>
          Edit
        </a>
      </span>
    </p>
  );
};

const Empty: React.FC = ({ children }) => {
  return <p className="text-t3 text-style-italic">{children}</p>;
};

const categoryDisplay = (categories: PersonalInformationCategoryDto[] | CategoryDto[]) => {
  return categories
    ?.sort(personalInformationCategorySorter)
    ?.map((pic) => pic.name)
    ?.join(", ");
};

const Footer = () => {
  const history = useHistory();
  return (
    <ProgressFooter
      actions={
        <ProgressFooterActions
          nextContent="Done Editing"
          nextOnClick={() => history.push(Routes.dataInventory.collection.root)}
        />
      }
    />
  );
};
