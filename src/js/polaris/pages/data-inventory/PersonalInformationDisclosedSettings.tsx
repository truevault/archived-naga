import { Button, Checkbox, FormControlLabel, Paper, Tooltip } from "@mui/material";
import clsx from "clsx";
import _ from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import { PageLoading } from "../../../common/components/Loading";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { PageHeader } from "../../../common/layout/PageHeader";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { UUIDString } from "../../../common/service/server/Types";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { Error as DataInventoryError } from "../../copy/dataInventory";
import { useOrganizationVendorDetails, usePrimaryOrganizationId } from "../../hooks";
import { useCollectionGroups } from "../../hooks/useCollectionGroups";
import { Routes } from "../../root/routes";
import { getGroupedCategories, getOrderedGroups } from "../../util/dataInventory";
import { DataInventorySubnav } from "./DataInventorySubnav";

type MatchParams = {
  vendorId: UUIDString;
};

type Props = RouteComponentProps<MatchParams>;

type DisclosedCategoriesMap = Map<UUIDString, Record<string, boolean>>;
type DisclosedSettingsMap = Map<UUIDString, DisclosedCategoriesMap>;

export const PersonalInformationDisclosedSettings = ({ match }: Props) => {
  const vendorId = match.params.vendorId;
  const organizationId = usePrimaryOrganizationId();
  const history = useHistory();
  const [everRendered, setEverRendered] = useState(false);
  const [orderedGroups, setOrderedGroups] = useState([]);
  const [groupedCategoriesByDst, setGroupedCategoriesByDst] = useState({});
  const [enabledDisclosedSettings, setEnabledDisclosedSettings] = useState(
    // data-subject-type->personal-information-category->enabled/disclosed
    new Map() as DisclosedSettingsMap,
  );
  const [showAllCategoriesState, setShowAllCategoriesState] = useState(new Set<string>());
  const [showAllDst, setShowAllDst] = useState(false);

  const [vendorDetails, vendorDetailsRequest] = useOrganizationVendorDetails(
    organizationId,
    vendorId,
  );
  const [dataSubjectTypes, dataSubjectTypesRequest] = useCollectionGroups(organizationId);

  const { result: personalInformationExchanged, request: getExchangedRequest } = useDataRequest({
    queryKey: ["personalInformationExchanged", organizationId, vendorId],
    api: () => Api.dataInventory.getPersonalInformationExchangedVendor(organizationId, vendorId),
  });

  const updatePiApi = useCallback(
    (values) => Api.dataInventory.setPersonalInformationExchanged(organizationId, vendorId, values),
    [organizationId, vendorId],
  );

  const { fetch: updatePersonalInformationDisclosed, request: updateRequest } = useActionRequest({
    api: updatePiApi,
    messages: {
      forceError: DataInventoryError.UpdateInformationDisclosure,
    },
    onSuccess: () => history.push(Routes.dataInventory.disclosure.root),
  });

  const handleDisclosedChanged = (dstId, categoryId, disclosed) => {
    setEnabledDisclosedSettings((prev) => {
      const prevSettings = prev.get(dstId);
      const newState = Object.assign(
        { enabled: false, sold: false },
        prevSettings.get(categoryId),
        { disclosed: disclosed },
      );
      return new Map(prev.set(dstId, prevSettings.set(categoryId, newState)));
    });
  };

  const handleShowAllCategories = (dstId) =>
    setShowAllCategoriesState((prev) => {
      prev.has(dstId) ? prev.delete(dstId) : prev.add(dstId);
      return new Set(prev);
    });

  const handleShowAllDst = () => setShowAllDst((prev) => !prev);

  const handleSelectAll = (dstId) =>
    setEnabledDisclosedSettings((prev) => {
      const updateAll = showAllCategoriesState.has(dstId);
      const dstSettings =
        enabledDisclosedSettings.get(dstId) ||
        (new Map(
          personalInformationExchanged
            .find((pie) => pie.dataSubjectTypeId === dstId)
            .personalInformationCategories.map((c) => [
              c.id,
              { enabled: false, disclosed: false, sold: false },
            ]),
        ) as DisclosedCategoriesMap);
      const currentSettings = Array.from(dstSettings.entries());
      const newState = new Map(
        currentSettings.map(([cid, setting]) => [
          cid,
          Object.assign({ enabled: false, sold: false }, setting, {
            disclosed: updateAll
              ? !setting.sold
              : !setting.sold && (setting.enabled || setting.disclosed),
          }),
        ]),
      );
      return new Map(prev.set(dstId, newState));
    });

  const updatePI = useCallback(
    _.debounce(updatePersonalInformationDisclosed, 750, {
      maxWait: 3000,
    }),
    [updatePersonalInformationDisclosed],
  );

  const handleSave = () =>
    updatePI(
      Array.from(enabledDisclosedSettings.entries()).map(([dstId, settings]) => {
        return {
          dataSubjectTypeId: dstId,
          personalInformationCategories: Array.from(settings.entries())
            .filter(([, { disclosed }]) => disclosed)
            .map(([catId]) => catId),
        };
      }),
    );

  const dstHasEnabled = (dstId) =>
    Array.from(enabledDisclosedSettings.get(dstId)?.values() || []).some((c) => c.enabled);

  useEffect(() => {
    if (
      NetworkRequest.areFinished(vendorDetailsRequest, getExchangedRequest, dataSubjectTypesRequest)
    ) {
      setOrderedGroups(
        getOrderedGroups(personalInformationExchanged[0].personalInformationCategories),
      );
      setGroupedCategoriesByDst(
        personalInformationExchanged.reduce(
          (acc, pie) =>
            Object.assign(acc, {
              [pie.dataSubjectTypeId]: getGroupedCategories(pie.personalInformationCategories),
            }),
          {},
        ),
      );
      const updatedSettings = new Map(
        personalInformationExchanged.map(({ dataSubjectTypeId, personalInformationCategories }) => {
          const enabledDisclosedMap = new Map(
            personalInformationCategories.map((category) => [
              category.id,
              {
                enabled: category.collected,
              },
            ]),
          );
          return [dataSubjectTypeId, enabledDisclosedMap];
        }),
      ) as DisclosedSettingsMap;
      setEnabledDisclosedSettings(updatedSettings);
      setEverRendered(true);
    }
  }, [vendorDetailsRequest, getExchangedRequest, dataSubjectTypesRequest]);

  // Page Load
  if (!everRendered) {
    return <PageLoading />;
  }

  return (
    <StandardPageWrapper navContent={<DataInventorySubnav />}>
      <PageHeader header="Information Disclosure" />

      <Paper>
        <div className="flex-row p-xl personal-information-options-header">
          <div className="flex-col options-header--name">
            <dl className="data-point-1 mt-0 mb-0">
              <dt>Vendor</dt>
              <dd>
                <div className="service-header--service">
                  <img className="service-header--logo" src={vendorDetails.logoUrl} />
                  {vendorDetails.name}
                </div>
              </dd>
            </dl>
          </div>
          <div className="flex-col options-header--actions">
            <div className="service-header--buttons">
              <div className="service-header--buttons-block">
                <Button
                  fullWidth={false}
                  className="mr-md"
                  href={Routes.dataInventory.disclosure.root}
                  disabled={updateRequest.running}
                >
                  Cancel
                </Button>
                <LoadingButton
                  fullWidth={false}
                  color="primary"
                  onClick={handleSave}
                  loading={updateRequest.running}
                >
                  Save
                </LoadingButton>
              </div>
            </div>
          </div>
        </div>

        <div className="consumer-groups-container">
          {dataSubjectTypes
            .slice(0)
            .map((dst) =>
              Object.assign({}, dst, {
                hasEnabled: dstHasEnabled(dst.id),
              }),
            )
            .filter((dst) => showAllDst || dst.hasEnabled)
            .sort((a, b) => {
              if (a.hasEnabled && !b.hasEnabled) {
                return -1;
              } else if (!a.hasEnabled && b.hasEnabled) {
                return 1;
              }
              return a.name.localeCompare(b.name);
            })
            .map((dst) => {
              const showAllCategories = showAllCategoriesState.has(dst.id);
              return (
                <div className="personal-information-options-section" key={`dst-section-${dst.id}`}>
                  <div className="flex-row">
                    <div className="flex-col options-header--name">
                      <h2 className="mt-0 mb-lg">{dst.name}</h2>
                    </div>
                    <div className="flex-col options-header--actions">
                      <div className="toggle-buttons-container">
                        <Button
                          className="toggle-button mr-sm"
                          color="primary"
                          onClick={() => handleShowAllCategories(dst.id)}
                        >
                          {showAllCategories ? "Show collected" : "Show all"}
                        </Button>
                        <Button
                          color="primary"
                          className="toggle-button"
                          onClick={() => handleSelectAll(dst.id)}
                        >
                          select all
                        </Button>
                      </div>
                    </div>
                  </div>
                  {!(dst.hasEnabled || showAllCategories)
                    ? 'Click "show all" to map personal information your organization is collecting and disclosing.'
                    : orderedGroups.map((group) => {
                        const categories = groupedCategoriesByDst[dst.id][group];
                        const categorySettings = enabledDisclosedSettings.get(dst.id);
                        const categoryIds = categories.map((c) => c.id);
                        const visibleCategoryIds = Array.from(categorySettings.entries())
                          .filter(
                            ([cid, s]) => categoryIds.includes(cid) && (s.enabled || s.disclosed),
                          )
                          .map(([cid]) => cid);
                        const hasVisible = showAllCategories || visibleCategoryIds.length > 0;
                        return hasVisible ? (
                          <div
                            className="personal-information-category-section"
                            key={`category-group-${group}`}
                          >
                            <div className="flex-row personal-information-category-group">
                              {group}
                            </div>
                            <div className="flex-row personal-information-options-row">
                              {categories
                                .slice(0)
                                .filter(
                                  (category) =>
                                    showAllCategories || visibleCategoryIds.includes(category.id),
                                )
                                .sort((a, b) => {
                                  const aVis = categorySettings.get(a.id).enabled;
                                  const bVis = categorySettings.get(b.id).enabled;
                                  if (aVis && !bVis) {
                                    return -1;
                                  } else if (!aVis && bVis) {
                                    return 1;
                                  }
                                  return a.groupDisplayOrder - b.groupDisplayOrder;
                                })
                                .map((category) => {
                                  const isDisclosed = categorySettings.get(category.id).disclosed;
                                  const isSold = categorySettings.get(category.id).sold;
                                  const isCollected = categorySettings.get(category.id).enabled;
                                  const checkbox = (
                                    <Checkbox
                                      onChange={({ target: { value, checked, name } }) => {
                                        handleDisclosedChanged(name, value, checked);
                                      }}
                                      disabled={isSold}
                                      checked={isDisclosed}
                                      value={category.id}
                                      name={dst.id}
                                      color="primary"
                                    />
                                  );
                                  return (
                                    <div
                                      key={`category-option-${category.id}`}
                                      className={clsx("personal-information-checkbox", {
                                        "newly-collected": isDisclosed && !isCollected,
                                        "not-collected": !(isCollected || isDisclosed),
                                      })}
                                    >
                                      <Tooltip title={category.description}>
                                        <FormControlLabel
                                          className="mr-0"
                                          control={
                                            isSold ? (
                                              <Tooltip
                                                placement="top-start"
                                                title="Already specified as selling, you must remove the selling setting to specify as disclosure"
                                              >
                                                <div>{checkbox}</div>
                                              </Tooltip>
                                            ) : (
                                              checkbox
                                            )
                                          }
                                          label={category.name}
                                        />
                                      </Tooltip>
                                    </div>
                                  );
                                })}
                            </div>
                          </div>
                        ) : (
                          ""
                        );
                      })}
                </div>
              );
            })}
        </div>
        <div className="p-xl">
          <Button color="primary" onClick={handleShowAllDst}>
            {showAllDst ? "Show Configured Consumer Groups" : "Show All Consumer Groups"}
          </Button>
        </div>
      </Paper>
    </StandardPageWrapper>
  );
};
