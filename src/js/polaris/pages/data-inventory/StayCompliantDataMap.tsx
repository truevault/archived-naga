import { Add, MoreVert as MenuIcon } from "@mui/icons-material";
import {
  Button,
  CircularProgress,
  ClickAwayListener,
  Grow,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Typography,
} from "@mui/material";
import React, { useEffect, useMemo, useRef, useState } from "react";
import { useHistory } from "react-router-dom";
import { PageLoading } from "../../../common/components/Loading";
import { useActionRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { CollectionGroupType } from "../../../common/service/server/controller/CollectionGroupsController";
import { CollectionGroupDetailsDto } from "../../../common/service/server/dto/CollectionGroupDto";
import { UUIDString } from "../../../common/service/server/Types";
import { ConfirmDialog } from "../../components/dialog";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { Error as OrganizationError } from "../../copy/organization";
import { useCollectionGroups } from "../../hooks/useCollectionGroups";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { useUnmappedOrganizationVendors } from "../../hooks/useOrganizationVendors";
import { RouteHelpers } from "../../root/routes";
import { isEmploymentGroup, nameForCollectionGroupType } from "../../util/consumerGroups";
import { AddOrEditConsumerGroupDialog } from "../get-compliant/collection-groups/AddOrEditConsumerGroupDialog";
import { ProgressSpan } from "../get-compliant/ProgressSpan";

const DataMapHeader: React.FC = () => {
  const description = (
    <Paras>
      <Para>View and update the data collection practices for each consumer group.</Para>
    </Paras>
  );

  return <PageHeader titleContent={"Data Map"} descriptionContent={description} />;
};

export const StayCompliantDataMap = () => {
  const organizationId = usePrimaryOrganizationId();
  const history = useHistory();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  const [deleteGroup, setDeleteGroup] = useState<CollectionGroupDetailsDto>(null);
  const [addEditGroupType, setAddEditGroupType] = useState<CollectionGroupType>(null);
  const [editGroup, setEditGroup] = useState(null);

  // Requests
  const [collectionGroups, dstRequest, refreshData] = useCollectionGroups(organizationId);

  const { fetch: removeGroup, request: removeGroupRequest } = useActionRequest({
    api: (consumerGroup) =>
      Api.consumerGroups.deleteDataSubjectType(organizationId, consumerGroup.id),
    messages: {
      forceError: OrganizationError.RemoveDataSubjectType,
    },
    onSuccess: () => {
      setDeleteGroup(null);
      refreshData();
    },
  });

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(dstRequest)) {
      setReadyToRender(true);
    }
  }, [dstRequest]);

  const onLinkClick = (groupTypeId: UUIDString) => {
    history.push(RouteHelpers.dataInventory.collection.detail(groupTypeId));
  };

  // Page Load
  if (!readyToRender) {
    return <PageLoading />;
  }

  const b2bDataSubjectTypes = collectionGroups.filter(
    (dst) => dst.collectionGroupType == "BUSINESS_TO_BUSINESS",
  );
  const b2cDataSubjectTypes = collectionGroups.filter(
    (dst) => dst.collectionGroupType == "BUSINESS_TO_CONSUMER",
  );

  const employmentDataSubjectTypes = collectionGroups.filter(
    (dst) => dst.collectionGroupType == "EMPLOYMENT",
  );

  const hasDataSubjectTypes =
    b2bDataSubjectTypes.length > 0 ||
    b2cDataSubjectTypes.length > 0 ||
    employmentDataSubjectTypes.length > 0;

  return (
    <StandardPageWrapper>
      <DataMapHeader />
      <DataMapBody
        b2cGroups={b2cDataSubjectTypes}
        b2bGroups={b2bDataSubjectTypes}
        employmentGroups={employmentDataSubjectTypes}
        hasDataSubjectTypes={hasDataSubjectTypes}
        onLinkClick={onLinkClick}
        setAddEditGroupType={setAddEditGroupType}
        setEditGroup={setEditGroup}
        setDeleteGroup={setDeleteGroup}
      />

      <ConfirmDialog
        open={!!deleteGroup}
        onClose={() => setDeleteGroup(null)}
        onConfirm={() => removeGroup(deleteGroup)}
        confirmTitle={deleteGroup ? `Delete ${deleteGroup.name}?` : "Delete Consumer Group?"}
        contentText={
          "Are you sure you want to delete this consumer group? " +
          "Any information you've mapped to this group will be lost."
        }
        confirmText="Yes"
        confirmLoading={removeGroupRequest.running}
        cls="get-compliant-dialog"
        transitionDuration={{ enter: 200, exit: 0 }}
      />
      <AddOrEditConsumerGroupDialog
        open={!!(editGroup || addEditGroupType)}
        onClose={() => {
          setAddEditGroupType(null);
          setEditGroup(null);
        }}
        onDone={() => {
          setAddEditGroupType(null);
          setEditGroup(null);
          refreshData();
        }}
        editGroup={editGroup}
        collectionGroupType={addEditGroupType}
        existingGroups={collectionGroups}
      />
    </StandardPageWrapper>
  );
};

const DataMapBody = ({
  b2cGroups,
  b2bGroups,
  employmentGroups,
  hasDataSubjectTypes,
  onLinkClick,
  setAddEditGroupType,
  setEditGroup,
  setDeleteGroup,
}) => {
  if (!hasDataSubjectTypes) {
    return (
      <div className="empty-state text-style-italic">
        <Typography variant="body1" paragraph>
          Because you have no B2C, B2B or Employment consumer groups, you are not required to
          complete a Data Map.
        </Typography>
        <Typography variant="body1">
          Select ‘Done with Mapping’ to proceed to the next step.
        </Typography>
      </div>
    );
  }
  return (
    <div>
      <div className="my-xl" />
      <DataRecipientList
        groupType={"BUSINESS_TO_CONSUMER" as CollectionGroupType}
        groups={b2cGroups}
        onRowClick={onLinkClick}
        noValuesMsg={
          "You indicated that your business does not provide products or services to individuals in California. If this is incorrect, add a Group below."
        }
        setAddEditGroupType={setAddEditGroupType}
        setEditGroup={setEditGroup}
        setDeleteGroup={setDeleteGroup}
      />
      <div className="my-xl" />
      <DataRecipientList
        groupType={"BUSINESS_TO_BUSINESS" as CollectionGroupType}
        groups={b2bGroups}
        onRowClick={onLinkClick}
        noValuesMsg={
          "Based on your survey responses, you are not required to map any Business-to-Business consumers under the CCPA’s B2B exemption."
        }
        setAddEditGroupType={setAddEditGroupType}
        setEditGroup={setEditGroup}
        setDeleteGroup={setDeleteGroup}
      />
      <div className="my-xl" />
      <DataRecipientList
        groupType={"EMPLOYMENT" as CollectionGroupType}
        groups={employmentGroups}
        onRowClick={onLinkClick}
        noValuesMsg={
          "You indicated that your organization does not hire or employ people in California. If this is incorrect, add a Group below."
        }
        setAddEditGroupType={setAddEditGroupType}
        setEditGroup={setEditGroup}
        setDeleteGroup={setDeleteGroup}
      />
    </div>
  );
};

const DataRecipientList = ({
  groupType,
  groups,
  onRowClick,
  noValuesMsg,
  setAddEditGroupType,
  setEditGroup,
  setDeleteGroup,
}) => {
  return (
    <div className="data-sharing data-sharing--container">
      <h4 className="data-sharing--section-header mt-0">{nameForCollectionGroupType(groupType)}</h4>
      {groups.length > 0 &&
        groups
          .slice(0)
          .sort((a, b) => a.name.localeCompare(b.name))
          .map((cg) => {
            return (
              <DataRecipientListRow
                key={`cg-row-${cg.id}`}
                cg={cg}
                setEditGroup={setEditGroup}
                onRowClick={onRowClick}
                setDeleteGroup={setDeleteGroup}
              />
            );
          })}

      {groups.length === 0 && <div className="data-sharing--no-values">{noValuesMsg}</div>}
      <div className="border-above">
        <Button
          className="consumer-groups--add-group-btn"
          variant="contained"
          onClick={() => {
            setAddEditGroupType(groupType);
          }}
          startIcon={<Add />}
        >
          Add Group
        </Button>
      </div>
    </div>
  );
};

type DataRecipientListRowProps = {
  cg: CollectionGroupDetailsDto;
  setEditGroup: (cg: CollectionGroupDetailsDto) => void;
  onRowClick: (id: UUIDString) => void;
  setDeleteGroup: (cg: CollectionGroupDetailsDto) => void;
};

const DataRecipientListRow: React.FC<DataRecipientListRowProps> = ({
  cg,
  setEditGroup,
  onRowClick,
  setDeleteGroup,
}) => {
  const organizationId = usePrimaryOrganizationId();
  const [unmappedVendors, unmappedVendorsRequest] = useUnmappedOrganizationVendors(
    organizationId,
    cg.id,
  );

  const isEmployment = isEmploymentGroup(cg);
  const menuRef = useRef(null);

  const [openMenu, setOpenMenu] = useState(false);

  const handleCloseMenu = (event) => {
    if (menuRef.current && menuRef.current.contains(event.target)) {
      return;
    }
    setOpenMenu(false);
  };

  const hasUnmappedVendors = useMemo(() => unmappedVendors.length > 0, [unmappedVendors]);

  return (
    <div key={`cg-${cg.id}`} className="data-sharing--row" onClick={() => onRowClick(cg.id)}>
      <div className="data-sharing--name">
        <span className="text-medium">{cg.name}</span>
      </div>
      <div className="data-sharing--progress">
        {unmappedVendorsRequest.running ? (
          <CircularProgress size={16} />
        ) : (
          <>{hasUnmappedVendors && !isEmployment && <ProgressSpan progress={"INCOMPLETE"} />}</>
        )}
      </div>
      <div className="data-sharing--menu-action" ref={menuRef}>
        <Button
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            setOpenMenu(true);
          }}
        >
          <MenuIcon />
        </Button>

        <Popper
          open={openMenu}
          anchorEl={menuRef.current}
          role={undefined}
          transition
          disablePortal
          className={"menu-content"}
        >
          {({ TransitionProps }) => (
            <Grow {...TransitionProps}>
              <Paper>
                <ClickAwayListener onClickAway={handleCloseMenu}>
                  <MenuList id="split-button-menu" className="split-button-popover-menu">
                    <MenuItem
                      key={"rename"}
                      onClick={(e) => {
                        e.preventDefault();
                        e.stopPropagation();
                        setEditGroup(cg);
                        setOpenMenu(false);
                      }}
                    >
                      Rename
                    </MenuItem>
                    <MenuItem
                      key={"delete"}
                      onClick={(e) => {
                        e.preventDefault();
                        e.stopPropagation();
                        setDeleteGroup(cg);
                        setOpenMenu(false);
                      }}
                    >
                      Delete
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    </div>
  );
};
