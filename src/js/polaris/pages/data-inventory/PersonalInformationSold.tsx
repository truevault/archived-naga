import ErrorIcon from "@mui/icons-material/Error";
import { Button, Paper } from "@mui/material";
import clsx from "clsx";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { PageLoading } from "../../../common/components/Loading";
import { useDataRequest } from "../../../common/hooks/api";
import { PageHeader } from "../../../common/layout/PageHeader";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { useCollectionGroups } from "../../hooks/useCollectionGroups";
import { RouteHelpers, Routes } from "../../root/routes";
import { flatMap, uniqueArray } from "../../util";
import { getCompleteConsumerGroups } from "../../util/dataInventory";
import { organizationVendorIsSelling } from "../../util/vendors";
import { DataInventorySubnav } from "./DataInventorySubnav";

export const PersonalInformationSold = () => {
  const organizationId = usePrimaryOrganizationId();
  const [everRendered, setEverRendered] = useState(false);
  const [completeVendors, setCompleteVendors] = useState([]);
  const [vendorsSoldTo, setVendorsSoldTo] = useState([]);
  const [incompleteConsumerGroups, setIncompleteConsumerGroups] = useState(0);
  const [consumerGroups, consumerGroupsRequest] = useCollectionGroups(organizationId);
  const [vendors, vendorsRequest] = useOrganizationVendors(organizationId);
  const { result: snapshotData, request: snapshotDataRequest } = useDataRequest({
    queryKey: ["snapshotData", organizationId],
    api: () => Api.dataInventory.getSnapshot(organizationId),
  });

  useEffect(() => {
    if (NetworkRequest.areFinished(vendorsRequest, snapshotDataRequest, consumerGroupsRequest)) {
      // @ts-ignore
      const completedGroups = getCompleteConsumerGroups(snapshotData);
      setIncompleteConsumerGroups(
        consumerGroups
          .filter((cg) => cg.enabled)
          .map((cg) => cg.id)
          .filter((cgid) => !completedGroups.includes(cgid)).length,
      );
      setVendorsSoldTo(vendors.filter((ov) => organizationVendorIsSelling(ov)));
      setCompleteVendors(
        uniqueArray(
          // @ts-ignore
          flatMap(snapshotData, (sd) =>
            // @ts-ignore
            flatMap(sd.personalInformationCategories, (c) => c.soldVendorIds),
          ),
        ),
      );
      setEverRendered(true);
    }
  }, [vendorsRequest, snapshotDataRequest, consumerGroupsRequest]);

  // Page Load
  if (!everRendered) {
    return <PageLoading />;
  }

  return (
    <StandardPageWrapper navContent={<DataInventorySubnav />}>
      <PageHeader header="Information Selling" />

      {incompleteConsumerGroups > 0 && (
        <Paper className="mt-0 mb-xl p-md flex-container flex--align-center">
          <ErrorIcon className="service-in-progress-warning" />
          <span className="ml-md">
            You have {incompleteConsumerGroups} in-progress consumer group
            {incompleteConsumerGroups > 1 && "s"} to complete
          </span>
          <Button
            className="ml-md"
            color="primary"
            component={Link}
            to={Routes.dataInventory.collection.root}
          >
            Resume
          </Button>
        </Paper>
      )}

      <Paper>
        <div className="personal-information-settings p-xl">
          <h2 className="mt-0">Map Selling by Third Party</h2>
          <div className="service-type-container">
            {vendorsSoldTo.length === 0 ? (
              <div className="services-empty-state">
                Add a <a href={Routes.vendors.root}>third party vendor</a> to whom you sell
                information first.
              </div>
            ) : (
              vendorsSoldTo
                .sort((a, b) => a.name.localeCompare(b.name))
                .map((v) => {
                  const isComplete = completeVendors.includes(v.vendorId);
                  return (
                    <div className="service-row" key={`dst-${v.id}`}>
                      <div className="service-col__logo">
                        <div className="service-logo">
                          <img src={v.logoUrl} />
                        </div>
                      </div>
                      <div className="service-col__name">{v.name}</div>
                      <div className={clsx("service-col__status", { "text-warning": isComplete })}>
                        {!isComplete ? "In-Progress" : ""}
                      </div>
                      <div className="service-col__edit">
                        <div className="mt-xs">
                          <Button
                            color="primary"
                            href={RouteHelpers.dataInventory.sale.detail(v.vendorId)}
                          >
                            Edit
                          </Button>
                        </div>
                      </div>
                    </div>
                  );
                })
            )}
          </div>
        </div>
      </Paper>
    </StandardPageWrapper>
  );
};
