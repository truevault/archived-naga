import { Button, Paper } from "@mui/material";
import clsx from "clsx";
import React, { useEffect, useState } from "react";
import { useCallbackRef } from "use-callback-ref";
import { PageLoading } from "../../../common/components/Loading";
import { useDataRequest } from "../../../common/hooks/api";
import { PageHeader } from "../../../common/layout/PageHeader";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { DataInventorySnapshotDto } from "../../../common/service/server/dto/PersonalInformationOptionsDto";
import { UUIDString } from "../../../common/service/server/Types";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { flatMap, uniqueArray } from "../../util";
import { DataInventorySubnav } from "./DataInventorySubnav";

type VendorsByCategory = Record<string, Set<UUIDString>>;
type VendorsByStringId = Record<string, UUIDString[]>;

export const CCPADisclosure = () => {
  const organizationId = usePrimaryOrganizationId();
  const [everRendered, setEverRendered] = useState(false);
  const [vendors, vendorsRequest] = useOrganizationVendors(organizationId);
  const [vendorsByCategory, setVendorsByCategory] = useState({} as VendorsByCategory);
  const [vendorsByClassification, setVendorsByClassification] = useState(
    {} as Record<string, UUIDString[]>,
  );

  const { result: snapshotData, request: snapshotRequest } = useDataRequest({
    queryKey: ["snapshotData", organizationId],
    api: () => Api.dataInventory.getSnapshot(organizationId),
  });

  useEffect(() => {
    if (NetworkRequest.areFinished(snapshotRequest, vendorsRequest)) {
      setVendorsByCategory((prev) =>
        vendors.reduce((acc, s) => {
          const category = s.category || "Other Vendors";
          acc[category] = acc[category] ? acc[category].add(s.vendorId) : new Set([s.vendorId]);
          return acc as VendorsByCategory;
        }, prev),
      );
      setVendorsByClassification(
        vendors.reduce((acc, v) => {
          const slug = v.classification?.slug;
          if (!slug) return acc;
          acc[slug] = acc[slug] ? acc[slug].concat(v.vendorId) : [v.vendorId];
          return acc;
        }, {} as VendorsByStringId),
      );
      setEverRendered(true);
    }
  }, [snapshotRequest, vendorsRequest]);

  // Page Load
  if (!everRendered) {
    return <PageLoading />;
  }

  const consumerGroupsMarkup = snapshotData
    // @ts-ignore
    .sort((a, b) => a.consumerGroupTypeDisplayOrder - b.consumerGroupTypeDisplayOrder)
    .map((data) => {
      return (
        <ConsumerGroupDisclosureSection
          data={data}
          vendors={vendors}
          vendorsByCategory={vendorsByCategory}
          vendorsByClassification={vendorsByClassification}
          key={`conumer-row-${data.dataSubjectTypeId}`}
        />
      );
    });

  return (
    <StandardPageWrapper navContent={<DataInventorySubnav />}>
      <PageHeader cls="mb-0" header="CCPA Disclosures" />
      <Paper>
        <div className="personal-information-snapshot">
          {/* @ts-ignore */}
          {snapshotData.length > 0 ? (
            <>{consumerGroupsMarkup}</>
          ) : (
            <div className="p-xl text-center">
              <div className="p-xl">Nothing to show yet.</div>
            </div>
          )}
        </div>
      </Paper>
    </StandardPageWrapper>
  );
};

const getVendorCategory = (
  vendors: OrganizationDataRecipientDto[],
  vendorsByClassification: VendorsByStringId,
  vendorId: UUIDString,
) => {
  if (vendorsByClassification["service-provider"]?.includes(vendorId)) {
    return "Service Providers";
  }
  if (vendorsByClassification["contractor"]?.includes(vendorId)) {
    return "Contractors";
  }
  return vendors.filter((v) => v.vendorId === vendorId)[0].category;
};

interface ConsumerGroupDisclosureSectionProps {
  data: DataInventorySnapshotDto;
  vendors: OrganizationDataRecipientDto[];
  vendorsByCategory: VendorsByCategory;
  vendorsByClassification: VendorsByStringId;
}

const ConsumerGroupDisclosureSection = ({
  data,
  vendors,
  vendorsByCategory,
  vendorsByClassification,
}: ConsumerGroupDisclosureSectionProps) => {
  const [showMore, setShowMore] = useState(false);
  const [, forceUpdate] = useState(0);
  const heightRef = useCallbackRef(null, () => forceUpdate(Math.random()));

  // determine if the instructions are tall enough to need the "show more" banner
  let needsMore = true;
  if (heightRef.current) {
    const rect = heightRef.current.getBoundingClientRect();
    needsMore = rect.height && rect.height > 450;
  }

  const disclosedData = data.personalInformationCategories.filter((pic) => pic.exchanged);
  const soldVendors = Array.from(Object.entries(vendorsByCategory)).filter(([, vendorIds]) => {
    const categoryVendorIds = new Set(
      flatMap(data.personalInformationCategories, (c) => c.vendorsSoldTo),
    );
    return [...vendorIds].some((s) => categoryVendorIds.has(s));
  });
  const soldMarkup = soldVendors
    .sort(([categoryA], [categoryB]) => categoryA.localeCompare(categoryB))
    .map(([vendorCategory, vendorIds], index) => {
      const categories = data.personalInformationCategories.filter((category) =>
        category.vendorsSoldTo.some((s) => vendorIds.has(s)),
      );
      return (
        <div
          key={`cg-${data.dataSubjectTypeId}-sold-vendor-group-${index}`}
          className="personal-information-category-section"
        >
          <h3 className="mt-0 mb-md">{vendorCategory}</h3>
          {categories.map((c) => (
            <div
              key={`cg-${data.dataSubjectTypeId}-sold-category-${c.id}`}
              className="snapshot-sold-category"
            >
              {c.name}
            </div>
          ))}
        </div>
      );
    });

  const emptySoldMarkup = (
    <div className="mt-xl">No sale of personal information for business purposes</div>
  );

  const sellingMarkup = soldVendors.length ? soldMarkup : emptySoldMarkup;

  return (
    <>
      <div
        className={clsx("consumer-group-snapshot-section", {
          "snapshot-show-more": needsMore && !showMore,
        })}
      >
        <div className="flex-col w-100">
          <div ref={heightRef}>
            <div className="flex-row">
              <div className="consumer-group-snapshot--name">
                <h2 className="m-0">{data.dataSubjectTypeName}</h2>
              </div>
              <div className="consumer-group-snapshot--collected">
                <h2 className="m-0">Information Collected, Sources, &amp; Purposes</h2>
              </div>
              <div className="consumer-group-snapshot--disclosed">
                <h2 className="m-0">
                  Information Disclosed for Business Purposes &amp; Categories of Parties Disclosed
                  To
                </h2>
              </div>
              <div className="consumer-group-snapshot--sold">
                <h2 className="m-0">Information Sold to Third Parties</h2>
              </div>
            </div>
            <div className="flex-row mt-sm">
              <div className="consumer-group-snapshot--name"></div>
              <div className="consumer-group-snapshot--collected">
                <div className="snapshot-collected-content">
                  <h3 className="mt-0 mb-md">Categories of Information</h3>
                  {data.personalInformationCategories
                    .sort((a, b) => a.name.localeCompare(b.name))
                    .map((category) => {
                      return (
                        <div
                          key={`cg-${data.dataSubjectTypeId}-category-${category.id}`}
                          className="snapshot-collected-category"
                        >
                          {category.name}
                        </div>
                      );
                    })}
                  <h3 className="mt-lg mb-md">Purposes for Collection</h3>
                  {data.businessPurposes
                    .sort((a, b) => a.displayOrder - b.displayOrder)
                    .map((purpose) => {
                      return (
                        <div
                          key={`cg-${data.dataSubjectTypeId}-purpose-${purpose.id}`}
                          className="snapshot-collected-purpose"
                        >
                          {purpose.name}
                        </div>
                      );
                    })}
                </div>
              </div>
              <div className="consumer-group-snapshot--disclosed">
                {disclosedData.length ? (
                  disclosedData.map((pic) => {
                    return (
                      <div
                        className="snapshot-disclosed--category"
                        key={`cg-${data.dataSubjectTypeId}-disclosed-category-${pic.id}`}
                      >
                        <div className="flex-col" style={{ width: "50%" }}>
                          {pic.name}
                        </div>
                        <div className="flex-col" style={{ width: "50%" }}>
                          {pic.vendorsSharedWith
                            .reduce(
                              (acc, vid) =>
                                uniqueArray(
                                  acc.concat(
                                    getVendorCategory(vendors, vendorsByClassification, vid),
                                  ),
                                ),
                              [],
                            )
                            .sort((a, b) => {
                              const atTop = ["Contractors", "Service Providers"];
                              if (atTop.includes(a) && !atTop.includes(b)) {
                                return -1;
                              }
                              if (!atTop.includes(a) && atTop.includes(b)) {
                                return 1;
                              }
                              return a.localeCompare(b);
                            })
                            .map((vendorCategory) => {
                              return (
                                <div className="flex-row mb-0" key={vendorCategory}>
                                  {vendorCategory}
                                </div>
                              );
                            })}
                        </div>
                      </div>
                    );
                  })
                ) : (
                  <div className="snapshot-disclosed--empty">
                    No disclosure of personal information for business purposes
                  </div>
                )}
              </div>
              <div className="consumer-group-snapshot--sold">{sellingMarkup}</div>
            </div>
            {needsMore && !showMore && <ShowMoreOverlay setShowMore={setShowMore} />}
            {needsMore && showMore && <ShowLessButton setShowMore={setShowMore} />}
          </div>
        </div>
      </div>
    </>
  );
};

const ShowMoreOverlay = ({ setShowMore }) => {
  return (
    <div className="snapshot-show-more-fade-out snapshot-show-button">
      <Button variant="text" color="primary" className="mb-xs" onClick={() => setShowMore(true)}>
        Show More
      </Button>
    </div>
  );
};

const ShowLessButton = ({ setShowMore }) => {
  return (
    <div className="snapshot-show-button">
      <Button variant="text" color="primary" className="mb-xs" onClick={() => setShowMore(false)}>
        Show Less
      </Button>
    </div>
  );
};
