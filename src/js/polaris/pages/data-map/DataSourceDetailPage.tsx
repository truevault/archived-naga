import { DeleteForever } from "@mui/icons-material";
import { Button, Link, Typography } from "@mui/material";
import React, { useState } from "react";
import { useHistory, useParams } from "react-router";
import { useActionRequest } from "../../../common/hooks/api";
import { useSourcedDetails } from "../../../common/hooks/dataMapHooks";
import { Api } from "../../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../../common/service/server/dto/DataMapDto";
import { OrganizationDataSourceDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../common/service/server/Types";
import { Callout } from "../../components/Callout";
import { DataRecipientLogo } from "../../components/DataRecipientLogo";
import { ConfirmDialog } from "../../components/dialog";
import { EmptyInfo } from "../../components/Empty";
import { PageHeader } from "../../components/layout/header/Header";
import { PageHeaderBack } from "../../components/layout/header/PageHeaderBack";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { PicGroupList } from "../../components/organisms/collection-groups/PicGroupList";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { usePrimaryOrganizationId } from "../../hooks";
import { useCollectionGroups } from "../../hooks/useCollectionGroups";
import { useDataMap } from "../../hooks/useDataMap";
import { useOrgDataSource } from "../../hooks/useOrganizationVendors";
import { Routes } from "../../root/routes";
import { nameForCollectionGroupType } from "../../util/consumerGroups";

const SourceDetailCard: React.FC<{
  source: OrganizationDataSourceDto;
  collectionGroup: CollectionGroupDetailsDto;
  dataMap: DataMapDto;
}> = ({ source, collectionGroup, dataMap }) => {
  const { sourcedPicGroups } = useSourcedDetails(dataMap, collectionGroup, source);

  return (
    <Callout
      title={collectionGroup.name}
      subtitle={nameForCollectionGroupType(collectionGroup.collectionGroupType)}
    >
      {sourcedPicGroups.length > 0 ? (
        <>
          <Paras>
            <Para>
              <strong>{source.name}</strong> sources the following information:
            </Para>
          </Paras>

          <PicGroupList picGroups={sourcedPicGroups} />
        </>
      ) : (
        <EmptyInfo>No sourced information</EmptyInfo>
      )}
    </Callout>
  );
};

type DataSourceDetailParams = {
  dataSourceId: UUIDString;
};

export const DataSourceDetailPage = () => {
  const history = useHistory();
  const { dataSourceId } = useParams<DataSourceDetailParams>();
  const orgId = usePrimaryOrganizationId();

  const [deleteConfirmOpen, setDeleteConfirmOpen] = useState(false);

  const [dataSource, dataSourceRequest] = useOrgDataSource(orgId, dataSourceId);
  const [collectionGroups, cgRequest] = useCollectionGroups(orgId);
  const [dataMap, dataMapRequest] = useDataMap(orgId);
  const sourcedData = dataMap?.received.filter(
    (source) => source.vendorId === dataSourceId && source.received.length > 0,
  );

  const { fetch: removeDataSource, request: removeSourceRequest } = useActionRequest({
    api: (source: OrganizationDataSourceDto) =>
      Api.orgDataSource.removeOrgDataSource(orgId, source.vendorId),
    onSuccess: () => {
      setDeleteConfirmOpen(false);
      history.push(Routes.dataMap.root);
    },
  });

  return (
    <StandardPageWrapper requests={[dataSourceRequest, dataMapRequest, cgRequest]}>
      <PageHeader
        logo={<DataRecipientLogo dataRecipient={dataSource} size={48} />}
        titleContent={dataSource?.name}
        titleHeaderContent={
          <PageHeaderBack to={`${Routes.dataMap.root}?tab=data_sources`}>
            Back to Data Map
          </PageHeaderBack>
        }
        actions={
          <Button
            onClick={() => setDeleteConfirmOpen(true)}
            color="error"
            startIcon={<DeleteForever />}
            variant="text"
          >
            Remove
          </Button>
        }
      />

      <div className="my-lg">
        <Paras>
          <Para>
            <strong>{dataSource?.name}</strong> sources information about the following collection
            groups:
          </Para>
        </Paras>
      </div>

      {sourcedData?.map((sd) => {
        const collectionGroup = collectionGroups.find((cg) => cg.id === sd.collectionGroupId);

        return (
          collectionGroup && (
            <div className="my-md" key={collectionGroup.id}>
              <SourceDetailCard
                source={dataSource}
                collectionGroup={collectionGroup}
                dataMap={dataMap}
              />
            </div>
          )
        );
      })}

      <Typography className="color-neutral-600 mt-lg">
        Need to add or modify a Collection Group? Visit your{" "}
        <Link href={Routes.dataMap.root}>Collection Groups List</Link>
      </Typography>

      <ConfirmDialog
        open={deleteConfirmOpen}
        onClose={() => setDeleteConfirmOpen(false)}
        onConfirm={() => removeDataSource(dataSource)}
        confirmTitle={`Delete ${dataSource?.name}?`}
        contentText={
          "Are you sure you want to delete this data source? " +
          "Any information you've mapped to this data source will be lost."
        }
        confirmText="Yes"
        confirmLoading={removeSourceRequest.running}
      />
    </StandardPageWrapper>
  );
};
