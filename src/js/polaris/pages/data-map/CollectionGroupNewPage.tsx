import { InputAdornment } from "@mui/material";
import React from "react";
import { Form } from "react-final-form";
import { useHistory } from "react-router";
import { Select, SelectOption } from "../../../common/components/input/Select";
import { Text } from "../../../common/components/input/Text";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { CollectionGroupType } from "../../../common/service/server/controller/CollectionGroupsController";
import { CollectionGroupDetailsDto } from "../../../common/service/server/dto/CollectionGroupDto";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { Error } from "../../copy/organization";
import { usePrimaryOrganizationId } from "../../hooks";
import { useCollectionGroups } from "../../hooks/useCollectionGroups";
import { RouteHelpers } from "../../root/routes";
import { composeValidators, validateNotIn, validateRequired } from "../../util/validation";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

const COLLECTION_GROUP_OPTIONS: SelectOption<CollectionGroupType>[] = [
  {
    value: "BUSINESS_TO_CONSUMER",
    label: "Business to Consumer",
  },
  {
    value: "BUSINESS_TO_BUSINESS",
    label: "Business to Business",
  },
  {
    value: "EMPLOYMENT",
    label: "Employment",
  },
];

export const CollectionGroupNewPage = () => {
  let submit;
  let submitting;
  const history = useHistory();
  const orgId = usePrimaryOrganizationId();
  const [collectionGroups, collectionGroupsRequest] = useCollectionGroups(orgId);

  const { fetch: handleCreate } = useActionRequest({
    api: async (values) => {
      return await Api.consumerGroups.createCollectionGroup(orgId, {
        name: values.name,
        description: values.description,
        enabled: true,
        privacyCenterEnabled: true,
        collectionGroupType: values.collectionGroupType,
        gdprDataSubject: false,
        processingRegions: null,
      });
    },
    messages: {
      forceError: Error.AddDataSubjectType,
    },
    onSuccess: (newGroup: CollectionGroupDetailsDto) => {
      history.push(RouteHelpers.dataMap.collectionGroup.workflow.collection(newGroup.id));
    },
  });

  return (
    <StandardPageWrapper
      requests={[collectionGroupsRequest]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextLoading={submitting}
              nextContent={"Next"}
              nextOnClick={() => submit()}
              prevContent="Back"
              prevOnClick={() => history.goBack()}
            />
          }
        />
      }
    >
      <PageHeader titleContent="New Collection Group" />

      <Form onSubmit={(v) => handleCreate(v)}>
        {(form) => {
          submit = form.handleSubmit;
          submitting = form.submitting;
          return (
            <form onSubmit={form.handleSubmit}>
              <Text
                field="name"
                label='First, create a Nickname for this group (ex: "Retail Shoppers")'
                placeholder="Nickname"
                required
                validate={composeValidators(
                  validateRequired,
                  validateNotIn(collectionGroups.map((cg) => cg.name)),
                )}
              />

              <Text
                field="description"
                label="Next, describe how this group interacts with you"
                required
                validate={validateRequired}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <strong>{form.values.name || "Nickname"}</strong>&nbsp;are people who:
                    </InputAdornment>
                  ),
                }}
              />

              <Select
                fullWidth
                label="Finally, what type of Collection Group is this?"
                field="collectionGroupType"
                options={COLLECTION_GROUP_OPTIONS}
                required
                formControlProps={{ fullWidth: true }}
                validate={validateRequired}
              />
            </form>
          );
        }}
      </Form>
    </StandardPageWrapper>
  );
};
