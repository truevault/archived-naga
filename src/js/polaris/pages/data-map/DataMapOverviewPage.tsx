import { Button, Toolbar } from "@mui/material";
import React from "react";
import { useHistory } from "react-router";
import { TabPanel } from "../../components";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { usePrimaryOrganization } from "../../hooks";
import { useDataMap } from "../../hooks/useDataMap";
import { TabOptions, useQueryTabs } from "../../hooks/useTabs";
import { Routes } from "../../root/routes";
import { CollectionGroupTable } from "./components/CollectionGroupTable";
import { CollectionPurposesTable } from "./components/CollectionPurposesTable";
import { DataRecipientsTable } from "./components/DataRecipientsTable";
import { DataSourcesTable } from "./components/DataSourcesTable";
import { LawfulBasesTable } from "./components/LawfulBasesTable";
import { SankeyDataMap } from "./components/visualizations/SankeyDataMap";

type DataMapTabOptions =
  | "collection_groups"
  | "data_recipients"
  | "data_sources"
  | "collection_purposes"
  | "lawful_bases";

const TAB_OPTIONS: TabOptions<DataMapTabOptions> = [
  {
    label: "Collection Groups",
    value: "collection_groups",
  },
  {
    label: "Data Recipients",
    value: "data_recipients",
  },
  {
    label: "Data Sources",
    value: "data_sources",
  },
  {
    label: "Collection Purposes",
    value: "collection_purposes",
  },
  {
    label: "Lawful Bases",
    value: "lawful_bases",
  },
];

export const DataMapOverviewPage = () => {
  const history = useHistory();
  const [org, orgRequest] = usePrimaryOrganization();
  const [tab, tabs] = useQueryTabs<DataMapTabOptions>(TAB_OPTIONS, "collection_groups");

  const [dataMap, dataMapRequest] = useDataMap(org.id);

  return (
    <StandardPageWrapper
      requests={[orgRequest, dataMapRequest]}
      hero={<SankeyDataMap datamap={dataMap} />}
    >
      {tabs}

      <TabPanel tab={tab} value="collection_groups" className="py-md">
        <Toolbar disableGutters>
          <Button
            className="ml-auto"
            variant="contained"
            onClick={() => history.push(Routes.dataMap.collectionGroup.new)}
          >
            Add Collection Group
          </Button>
        </Toolbar>

        <CollectionGroupTable />
      </TabPanel>

      <TabPanel tab={tab} value="data_recipients" className="py-md">
        <Toolbar disableGutters>
          <Button
            className="ml-auto"
            variant="contained"
            onClick={() => history.push(Routes.dataMap.dataRecipient.new)}
          >
            Add Data Recipient
          </Button>
        </Toolbar>

        <DataRecipientsTable />
      </TabPanel>

      <TabPanel tab={tab} value="data_sources" className="py-md">
        <Toolbar disableGutters>
          <Button
            className="ml-auto"
            variant="contained"
            onClick={() => history.push(Routes.dataMap.dataSource.new)}
          >
            Add Data Source
          </Button>
        </Toolbar>

        <DataSourcesTable />
      </TabPanel>

      <TabPanel tab={tab} value="collection_purposes" className="py-md">
        <CollectionPurposesTable />
      </TabPanel>

      <TabPanel tab={tab} value="lawful_bases" className="py-md">
        <Button
          className="ml-auto"
          variant="contained"
          onClick={() => history.push(Routes.dataMap.lawfulBases.detail)}
        >
          Edit Lawful Bases
        </Button>

        <LawfulBasesTable />
      </TabPanel>
    </StandardPageWrapper>
  );
};
