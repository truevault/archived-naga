import React, { useMemo } from "react";
import { useActionRequest } from "../../../common/hooks/api";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { Api } from "../../../common/service/Api";
import { LawfulBasis } from "../../../common/service/server/dto/ProcessingActivityDto";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { AddProcessingActivityDialog } from "../../components/organisms/AddProcessingActivityDialog";
import { ProcessingActivitySettingsTable } from "../../components/organisms/ProcessingActivitySettingsTable";
import { usePrimaryOrganizationId } from "../../hooks";
import { useProcessingActivites } from "../../hooks/useProcessingActivities";
import { Routes } from "../../root/routes";
import { GDPR_LAWFUL_BASES_SURVEY } from "../get-compliant/consumer-collection/ConsumerCollectionGdprLawfulBases";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

export const LawfulBasesDetailPage = () => {
  const orgId = usePrimaryOrganizationId();
  const { answers, surveyRequest } = useSurvey(orgId, GDPR_LAWFUL_BASES_SURVEY);
  const [activities, activityRequest, refreshActivities] = useProcessingActivites(orgId);

  const extendedOptions = useMemo(() => answers["extra-lawful-bases"] === "true", [answers]);

  const { fetch: updateLawfulBasis } = useActionRequest({
    api: ({ activityId, bases }: { activityId: string; bases: LawfulBasis[] }) =>
      Api.processingActivities.patchUpdateProcessingActivity(orgId, activityId, {
        name: undefined,
        lawfulBases: bases,
        regions: undefined,
      }),
    onSuccess: () => refreshActivities(),
  });

  const footer = (
    <ProgressFooter
      actions={<ProgressFooterActions nextContent="Done" nextUrl={Routes.dataMap.root} />}
    />
  );

  return (
    <StandardPageWrapper requests={[surveyRequest, activityRequest]} footer={footer}>
      <PageHeader titleContent="Lawful Bases" />

      <div className="mb-md">
        <ProcessingActivitySettingsTable
          activities={activities}
          onChange={updateLawfulBasis}
          showExtendedOptions={extendedOptions}
          onRemove={async () => await refreshActivities()}
        />
      </div>

      <AddProcessingActivityDialog
        existingActivities={activities}
        orgId={orgId}
        onAdd={() => {
          refreshActivities();
        }}
      />
    </StandardPageWrapper>
  );
};
