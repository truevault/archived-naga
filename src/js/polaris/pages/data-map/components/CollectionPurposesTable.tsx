import { Check } from "@mui/icons-material";
import { Button, Typography } from "@mui/material";
import React, { useState } from "react";
import { CellProps, ColumnDefinition } from "../../../../common/components/TileTable";
import { BusinessPurposeDto } from "../../../../common/service/server/dto/PersonalInformationOptionsDto";
import { Loading } from "../../../components";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useNonEmploymentCollectionGroups } from "../../../hooks/useCollectionGroups";
import { CollectionPurposes } from "../../get-compliant/consumer-collection/CollectionPurposes";

const NameCell: React.FC<CellProps<BusinessPurposeDto>> = ({ data }) => (
  <span className="d-flex flex-row">
    <Check color="primary" />

    <div>
      <Typography>{data.name}</Typography>

      {Boolean(data.description) && (
        <Typography className="color-neutral-600" variant="caption">
          {data.description}
        </Typography>
      )}
    </div>
  </span>
);

export const COLLECTION_GROUP_COLUMNS: ColumnDefinition<BusinessPurposeDto>[] = [
  {
    heading: null,
    component: NameCell,
  },
];

export const CollectionPurposesTable = () => {
  const orgId = usePrimaryOrganizationId();
  const [collectionGroups, collectionGroupRequest] = useNonEmploymentCollectionGroups(orgId);

  const [editing, setEditing] = useState(false);

  if (collectionGroupRequest.running) {
    return <Loading />;
  }

  return (
    <>
      <Button className="ml-auto my-md" variant="contained" onClick={() => setEditing(!editing)}>
        {editing ? "Done Editing" : "Edit Collection Purposes"}
      </Button>
      <CollectionPurposes picWarning="top" consumerGroups={collectionGroups} readonly={!editing} />
    </>
  );
};
