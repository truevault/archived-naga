import { Button, FormGroup, Toolbar } from "@mui/material";
import React, { ReactNode, useState } from "react";
import { PageContentFooterPortal } from "../../../../common/layout/PageContent";
import { ProgressFooter, ProgressFooterActions } from "../../get-compliant/ProgressFooter";

type EditTabProps = {
  isEditing: boolean;
  editLabel?: ReactNode;
  saveLabel?: ReactNode;
  saveDisabled?: boolean;
  editDisabled?: boolean;
  onEnableEditing: () => void;
  onDisableEditing: () => void;
};
export const EditTab: React.FC<EditTabProps> = ({
  children,
  isEditing,
  editLabel = "Edit",
  saveLabel = "Save",
  saveDisabled = false,
  editDisabled = false,
  onEnableEditing,
  onDisableEditing,
}) => {
  const [saving, setSaving] = useState(false);

  const handleDisableEdit = async () => {
    setSaving(true);
    await onDisableEditing();
    setSaving(false);
  };

  return (
    <>
      {!isEditing && (
        <Toolbar disableGutters>
          <FormGroup className="ml-auto">
            <Button
              onClick={onEnableEditing}
              disabled={editDisabled}
              variant="contained"
              color="primary"
            >
              {editLabel}
            </Button>
          </FormGroup>
        </Toolbar>
      )}

      {children}

      <PageContentFooterPortal visible={isEditing}>
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextLoading={saving}
              nextContent={saveLabel}
              nextOnClick={handleDisableEdit}
              nextDisabled={saveDisabled}
            />
          }
        />
      </PageContentFooterPortal>
    </>
  );
};
