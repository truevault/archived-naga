import React from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../../../common/service/server/dto/DataMapDto";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { EmptyInfo } from "../../../components/Empty";

type EditableDataRecipientCardRenderProps = {
  dataRecipient: OrganizationDataRecipientDto;
  collectionGroup: CollectionGroupDetailsDto;
  dataMap: DataMapDto;
  forceEdit: boolean;
};

type Props = {
  title: React.ReactNode;
  dataRecipients: OrganizationDataRecipientDto[] | OrganizationDataSourceDto[];
  collectionGroup: CollectionGroupDetailsDto;
  dataMap: DataMapDto;
  forceEditRecipients?: OrganizationDataRecipientDto[] | OrganizationDataSourceDto[];
  className?: string;
  emptyText?: string;
  render: (renderProps: EditableDataRecipientCardRenderProps) => React.ReactNode;
};

export const EditableDataRecipientSection: React.FC<Props> = ({
  title,
  dataMap,
  collectionGroup,
  className,
  render,
  forceEditRecipients = [],
  dataRecipients = [],
  emptyText = "No data recipients",
}) => {
  return (
    <section className={className}>
      {title}

      {dataRecipients.length > 0 ? (
        <>
          {dataRecipients.map((dr, idx) => {
            const forceEdit = forceEditRecipients.some((ar) => ar.vendorId === dr.vendorId);

            return (
              <div key={`${dr.vendorId}-${idx}`} id={`dr-${dr.vendorId}`}>
                {render({
                  dataRecipient: dr as OrganizationDataRecipientDto,
                  collectionGroup,
                  dataMap,
                  forceEdit,
                })}
              </div>
            );
          })}
        </>
      ) : (
        <EmptyInfo>{emptyText}</EmptyInfo>
      )}
    </section>
  );
};
