import { Stack } from "@mui/material";
import React, { useState } from "react";
import { Organization } from "../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../../../common/service/server/dto/DataMapDto";
import { ConsumerDataCollectionCheckboxes } from "../../../components/organisms/collection-groups/ConsumerDataCollectionCheckboxes";
import { isEmploymentGroup } from "../../../util/consumerGroups";
import { CollectionGroupCollectedPicTable } from "./CollectionGroupCollectedPicTable";
import { EditTab } from "./EditTab";

export const CollectedPicTab: React.FC<{
  org: Organization;
  collectionGroup: CollectionGroupDetailsDto;
  dataMap: DataMapDto;
  refreshGroup: () => Promise<void>;
}> = ({ org, collectionGroup, dataMap, refreshGroup }) => {
  const [editMode, setEditMode] = useState(false);

  const isEmployment = isEmploymentGroup(collectionGroup);

  const enableEditMode = () => {
    setEditMode(true);
  };

  const disableEditMode = async () => {
    await refreshGroup();
    setEditMode(false);
  };

  return (
    <EditTab
      isEditing={editMode}
      onEnableEditing={enableEditMode}
      onDisableEditing={disableEditMode}
    >
      {editMode ? (
        <Stack spacing={3}>
          <ConsumerDataCollectionCheckboxes
            dataMap={dataMap}
            org={org}
            collectionGroup={collectionGroup}
            orderingVariant={isEmployment ? "employee" : "default"}
          />
        </Stack>
      ) : (
        <Stack spacing={3}>
          <CollectionGroupCollectedPicTable collectionGroup={collectionGroup} dataMap={dataMap} />
        </Stack>
      )}
    </EditTab>
  );
};
