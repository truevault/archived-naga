import { Button, Chip, Typography } from "@mui/material";
import React from "react";
import {
  CellProps,
  ColumnDefinition,
  TableDefinition,
  TileTable,
} from "../../../../common/components/TileTable";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Loading } from "../../../components";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { RouteHelpers } from "../../../root/routes";
import { DataRecipientLogo } from "../../../components/DataRecipientLogo";
import { EmptyInfo } from "../../../components/Empty";
import { useQueryModal } from "../../../hooks/modalHooks";
import { FinishDataRecipientMappingDialog } from "../../../components/dialog/FinishDataRecipientMappingDialog";

const NameCell: React.FC<CellProps<OrganizationDataRecipientDto>> = ({ data }) => (
  <span className="d-flex flex-row align-items-center">
    <DataRecipientLogo className="mr-md" dataRecipient={data} size={32} />
    <Typography>{data.name}</Typography>
  </span>
);

const ActionCell: React.FC<CellProps<OrganizationDataRecipientDto>> = ({ data }) => (
  <>
    {data.completed ? (
      <Button href={RouteHelpers.dataMap.dataRecipient.detail(data.vendorId)} color="secondary">
        View Details
      </Button>
    ) : (
      <>
        <Chip label="Incomplete" color="warning" className="mr-sm" />

        <Button
          href={
            data.dataRecipientType == "storage_location"
              ? RouteHelpers.dataMap.dataRecipient.workflow.classification(data.vendorId)
              : RouteHelpers.dataMap.dataRecipient.workflow.groupAssociation(data.vendorId)
          }
          color="secondary"
        >
          Continue
        </Button>
      </>
    )}
  </>
);

export const DATA_RECIPIENT_COLUMNS: ColumnDefinition<OrganizationDataRecipientDto>[] = [
  {
    heading: null,
    component: NameCell,
  },
  {
    heading: null,
    cellClass: "text-right",
    component: ActionCell,
  },
];

export const DataRecipientsTable = () => {
  const orgId = usePrimaryOrganizationId();
  const [finishEmploymentMapping, setFinishedEmploymentMapping] =
    useQueryModal("finish_employment_modal");
  const [dataRecipients, dataRecipientsRequest] = useOrganizationVendors(orgId, undefined, true);

  if (dataRecipientsRequest.running) {
    return <Loading />;
  }

  if (!dataRecipients || dataRecipients.length < 1) {
    return <EmptyInfo>No data recipients</EmptyInfo>;
  }

  const table: TableDefinition<OrganizationDataRecipientDto> = {
    data: dataRecipients,
    columns: DATA_RECIPIENT_COLUMNS,
  };

  return (
    <>
      <FinishDataRecipientMappingDialog
        open={finishEmploymentMapping}
        onClose={() => setFinishedEmploymentMapping(false)}
      />
      <TileTable variant="filled" table={table} />
    </>
  );
};
