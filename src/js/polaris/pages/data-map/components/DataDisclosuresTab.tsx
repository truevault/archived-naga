import { Button } from "@mui/material";
import _ from "lodash";
import React, { useMemo, useState } from "react";
import { Organization } from "../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../../../common/service/server/dto/DataMapDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Abbr } from "../../../components/Abbr";
import { CalloutVariant } from "../../../components/Callout";
import { DataRecipientCollapseCard } from "../../../components/DataRecipientCollapseCard";
import { SectionHeader } from "../../../components/molecules/SectionHeader";
import { EditableDisclosureCard } from "../../../components/organisms/data-recipients/EditableDisclosureCard";
import {
  useDisclosingDataRecipients,
  useSellingOrSharingDataRecipients,
} from "../../../hooks/useOrganizationVendors";
import { alphabetically } from "../../../surveys/util";
import { EditableDataRecipientSection } from "./EditableDataRecipientSection";

type DataDisclosureTabProps = {
  handleRefresh: () => Promise<void>;
  dataMap: DataMapDto;
  collectionGroup: CollectionGroupDetailsDto;
  org: Organization;
  dataRecipients: OrganizationDataRecipientDto[];
};

export const DataDisclosuresTab: React.FC<DataDisclosureTabProps> = ({
  dataMap,
  collectionGroup,
  org,
  dataRecipients,
  handleRefresh,
}) => {
  const {
    data: {
      addingDataRecipients,
      shareSectionDataRecipients,
      sellSectionDataRecipients,
      disclosesSectionDataRecipients,
      unassociatedDataRecipients,
    },
    actions: { setAddingDataRecipients },
  } = useEditDataDisclosuresData(collectionGroup, dataMap, dataRecipients);

  const afterSave = async (dr: OrganizationDataRecipientDto) => {
    await handleRefresh();

    if (addingDataRecipients.some((ar) => ar.id === dr.id)) {
      setAddingDataRecipients((prev) => prev.filter((ar) => ar.id !== dr.id));
    }
  };

  return (
    <>
      <EditableDataRecipientSection
        title={
          <SectionHeader>
            {org.name}{" "}
            <Abbr title="“Sharing” is providing customer data to third parties for the purpose of interest-based or behavioral advertising.">
              <span>shares</span>
            </Abbr>{" "}
            data with&hellip;
          </SectionHeader>
        }
        collectionGroup={collectionGroup}
        dataMap={dataMap}
        dataRecipients={shareSectionDataRecipients}
        forceEditRecipients={addingDataRecipients}
        render={(props) => (
          <EditableDisclosureCard
            className="mb-sm"
            {...props}
            afterSave={() => afterSave(props.dataRecipient)}
          />
        )}
        className="mb-lg"
      />

      <EditableDataRecipientSection
        title={
          <SectionHeader>
            {org.name}{" "}
            <Abbr title="“Selling” is providing customer data to third parties in exchange for money or anything else of value.">
              <span>sells</span>
            </Abbr>{" "}
            data to&hellip;
          </SectionHeader>
        }
        collectionGroup={collectionGroup}
        dataMap={dataMap}
        dataRecipients={sellSectionDataRecipients}
        forceEditRecipients={addingDataRecipients}
        render={(props) => (
          <EditableDisclosureCard
            className="mb-sm"
            {...props}
            afterSave={() => afterSave(props.dataRecipient)}
          />
        )}
        className="mb-lg"
      />

      <EditableDataRecipientSection
        title={
          <SectionHeader>
            {org.name}{" "}
            <Abbr title="“Disclosing” is providing customer data to service providers and third parties for a business purpose.">
              <span>discloses</span>
            </Abbr>{" "}
            data to&hellip;
          </SectionHeader>
        }
        collectionGroup={collectionGroup}
        dataMap={dataMap}
        dataRecipients={disclosesSectionDataRecipients}
        forceEditRecipients={addingDataRecipients}
        render={(props) => (
          <EditableDisclosureCard
            className="mb-sm"
            {...props}
            afterSave={() => afterSave(props.dataRecipient)}
          />
        )}
        className="mb-lg"
      />

      {unassociatedDataRecipients.length > 0 && (
        <section>
          <SectionHeader>
            Recipients that are not associated with {collectionGroup.name}...
          </SectionHeader>

          {unassociatedDataRecipients.map((dr) => {
            const handleAddDisclosures = () => {
              setAddingDataRecipients((r) => [...r, dr]);
              setTimeout(() => window.polaris.scrollSelectorIntoView(`#dr-${dr.id}`), 150);
            };

            return (
              <DataRecipientCollapseCard
                className="mb-md"
                key={dr.id}
                recipient={dr}
                collapsible={false}
                variant={CalloutVariant.LightGray}
                headerContent={
                  <Button onClick={handleAddDisclosures} color="secondary">
                    Add Disclosures
                  </Button>
                }
              />
            );
          })}
        </section>
      )}
    </>
  );
};

type UseEditDataDisclosures = {
  data: {
    addingDataRecipients: OrganizationDataRecipientDto[];
    shareSectionDataRecipients: OrganizationDataRecipientDto[];
    disclosesSectionDataRecipients: OrganizationDataRecipientDto[];
    sellSectionDataRecipients: OrganizationDataRecipientDto[];
    unassociatedDataRecipients: OrganizationDataRecipientDto[];
  };
  actions: {
    setAddingDataRecipients: React.Dispatch<React.SetStateAction<OrganizationDataRecipientDto[]>>;
  };
};

export const useEditDataDisclosuresData = (
  collectionGroup: CollectionGroupDetailsDto,
  dataMap: DataMapDto,
  dataRecipients: OrganizationDataRecipientDto[],
): UseEditDataDisclosures => {
  const [addingDataRecipients, setAddingDataRecipients] = useState<OrganizationDataRecipientDto[]>(
    [],
  );

  const [disclosureDataRecipients, nonDisclosureDataRecipients] = useDisclosingDataRecipients(
    collectionGroup,
    dataMap,
    dataRecipients,
  );

  const {
    selling: sellingDataRecipients,
    sharing: sharingDataRecipients,
    other: otherDataRecipients,
  } = useSellingOrSharingDataRecipients(disclosureDataRecipients);

  const {
    selling: addingSellingRecipients,
    sharing: addingSharingRecipients,
    other: addingOtherRecipients,
  } = useSellingOrSharingDataRecipients(addingDataRecipients);

  const shareSectionDataRecipients = useMemo(
    () =>
      _.uniqBy([...sharingDataRecipients, ...addingSharingRecipients], "vendorId").sort(
        alphabetically,
      ),
    [sharingDataRecipients, addingSharingRecipients],
  );

  const sellSectionDataRecipients = useMemo(
    () =>
      _.uniqBy([...sellingDataRecipients, ...addingSellingRecipients], "vendorId").sort(
        alphabetically,
      ),
    [sellingDataRecipients, addingSellingRecipients],
  );

  const disclosesSectionDataRecipients = useMemo(
    () =>
      _.uniqBy([...otherDataRecipients, ...addingOtherRecipients], "vendorId").sort(alphabetically),
    [otherDataRecipients, addingOtherRecipients],
  );

  const unassociatedDataRecipients = nonDisclosureDataRecipients.filter(
    (dr) => !addingDataRecipients.includes(dr),
  );

  return {
    data: {
      addingDataRecipients,
      shareSectionDataRecipients,
      sellSectionDataRecipients,
      disclosesSectionDataRecipients,
      unassociatedDataRecipients,
    },
    actions: {
      setAddingDataRecipients,
    },
  };
};
