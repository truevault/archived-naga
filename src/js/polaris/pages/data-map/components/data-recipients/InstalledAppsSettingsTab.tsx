import React, { useMemo, useState } from "react";
import { NetworkRequestGate } from "../../../../../common/components/NetworkRequestGate";
import { useActionRequest } from "../../../../../common/hooks/api";
import { Organization } from "../../../../../common/models";
import { Api } from "../../../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Callout } from "../../../../components/Callout";
import { DataRecipientLogo } from "../../../../components/DataRecipientLogo";
import { Error } from "../../../../copy/requests";
import { usePlatformApps } from "../../../../hooks/useOrganizationVendors";
import { DataRecipientMultiselect } from "../../../design/molecules/DataRecipientMultiselect";
import { EditTab } from "../EditTab";

type InstalledAppsSettingTabProps = {
  handleRefresh: () => Promise<void>;
  org: Organization;
  dataRecipient: OrganizationDataRecipientDto;
};

export const InstalledAppsSettingTab: React.FC<InstalledAppsSettingTabProps> = ({
  org,
  dataRecipient,
  handleRefresh,
}) => {
  const [editMode, setEditMode] = useState(false);

  const enableEditMode = () => {
    setEditMode(true);
  };

  const [platformApps, platformAppsRequest, refreshPlatformApps] = usePlatformApps(
    org.id,
    dataRecipient.vendorId,
  );

  const installedApps = useMemo(() => platformApps?.installed ?? [], [platformApps]);

  const { fetch: toggleInstall } = useActionRequest({
    api: async ({
      platformId,
      appId,
      included,
    }: {
      platformId: string;
      appId: string;
      included: boolean;
    }) => {
      if (included) {
        return await Api.organizationVendor.addPlatformApplication(org.id, platformId, appId);
      } else {
        return await Api.organizationVendor.removePlatformApplication(org.id, platformId, appId);
      }
    },
    onSuccess: () => refreshPlatformApps(),
    messages: {
      forceError: Error.TogglePlatformApp,
    },
  });

  const disableEditMode = async () => {
    await handleRefresh();
    setEditMode(false);
  };

  return (
    <NetworkRequestGate requests={[platformAppsRequest]}>
      <EditTab
        isEditing={editMode}
        onEnableEditing={enableEditMode}
        onDisableEditing={disableEditMode}
      >
        {editMode ? (
          <>
            <div className="mt-xl">
              <div className="text-t5 text-weight-bold color-primary-700 mb-md">Installed Apps</div>
              <p className="mb-xl">
                Select which vendors below are installed apps in your Shopify store. To see a list
                of your installed apps, go to the{" "}
                <a target="_blank" rel="noreferrer noopener" href="https://shopify.com/admin/apps">
                  Apps
                </a>{" "}
                section of your Shopify account.
              </p>

              {dataRecipient?.isPlatform && (
                <DataRecipientMultiselect
                  selected={installedApps}
                  recipients={platformApps?.available}
                  onChange={({ appId, included }) =>
                    toggleInstall({
                      platformId: dataRecipient.vendorId,
                      appId,
                      included,
                    })
                  }
                  onChangeSelectAll={(selected) =>
                    platformApps?.available.forEach((dr) =>
                      toggleInstall({
                        platformId: dataRecipient.vendorId,
                        appId: dr.vendorId,
                        included: selected,
                      }),
                    )
                  }
                />
              )}
            </div>
          </>
        ) : (
          <>
            {installedApps.map((app) => {
              const dr = platformApps?.available?.find((pa) => pa.vendorId === app);

              if (!dr) {
                return null;
              }

              return (
                <Callout
                  className="mb-sm"
                  key={dr.id}
                  icon={<DataRecipientLogo dataRecipient={dr} size={24} />}
                  title={dr.name}
                />
              );
            })}
          </>
        )}
      </EditTab>
    </NetworkRequestGate>
  );
};
