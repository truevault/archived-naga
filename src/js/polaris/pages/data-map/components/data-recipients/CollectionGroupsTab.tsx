import { Link, Typography } from "@mui/material";
import React from "react";
import { NetworkRequestGate } from "../../../../../common/components/NetworkRequestGate";
import { useDisclosureDetails } from "../../../../../common/hooks/dataMapHooks";
import { Organization } from "../../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../../../../common/service/server/dto/DataMapDto";
import { OrganizationDataRecipientDto } from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Callout } from "../../../../components/Callout";
import { EmptyInfo } from "../../../../components/Empty";
import { PicGroupList } from "../../../../components/organisms/collection-groups/PicGroupList";
import { Para } from "../../../../components/typography/Para";
import { Paras } from "../../../../components/typography/Paras";
import { useCollectionGroups } from "../../../../hooks/useCollectionGroups";
import { Routes } from "../../../../root/routes";
import { nameForCollectionGroupType } from "../../../../util/consumerGroups";

const DisclosureDetailCard: React.FC<{
  dataRecipient: OrganizationDataRecipientDto;
  collectionGroup: CollectionGroupDetailsDto;
  dataMap: DataMapDto;
}> = ({ dataRecipient, collectionGroup, dataMap }) => {
  const { disclosedPicGroups } = useDisclosureDetails(dataMap, collectionGroup, dataRecipient);

  return (
    <Callout
      title={collectionGroup.name}
      subtitle={nameForCollectionGroupType(collectionGroup.collectionGroupType)}
    >
      {disclosedPicGroups.length > 0 ? (
        <>
          <Paras>
            <Para>
              <strong>{dataRecipient.name}</strong> handles the following information:
            </Para>
          </Paras>

          <PicGroupList picGroups={disclosedPicGroups} />
        </>
      ) : (
        <EmptyInfo>No disclosed information</EmptyInfo>
      )}
    </Callout>
  );
};

type Props = {
  org: Organization;
  dataRecipient: OrganizationDataRecipientDto;
  dataMap: DataMapDto;
};

export const CollectionGroupsTab: React.FC<Props> = ({ org, dataRecipient, dataMap }) => {
  const [collectionGroups, cgRequest] = useCollectionGroups(org.id);
  const disclosedData =
    dataMap?.disclosure.filter(
      (disclosure) =>
        disclosure.vendorId === dataRecipient.vendorId && disclosure.disclosed.length > 0,
    ) ?? [];

  return (
    <NetworkRequestGate requests={[cgRequest]}>
      {disclosedData.length > 0 ? (
        <>
          {disclosedData?.map((dd) => {
            const collectionGroup = collectionGroups.find((cg) => cg.id === dd.collectionGroupId);

            return (
              collectionGroup && (
                <div className="my-md" key={collectionGroup.id}>
                  <DisclosureDetailCard
                    dataRecipient={dataRecipient}
                    collectionGroup={collectionGroup}
                    dataMap={dataMap}
                  />
                </div>
              )
            );
          })}
        </>
      ) : (
        <EmptyInfo>
          No collection groups are associated with <strong>{dataRecipient.name}</strong>
        </EmptyInfo>
      )}

      <Typography className="color-neutral-600 mt-lg">
        Need to add or modify a Collection Group? Visit your{" "}
        <Link href={Routes.dataMap.root}>Collection Groups List</Link>
      </Typography>
    </NetworkRequestGate>
  );
};
