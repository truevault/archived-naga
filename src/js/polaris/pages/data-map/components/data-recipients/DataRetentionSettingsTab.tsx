import React, { useState } from "react";
import { NetworkRequestGate } from "../../../../../common/components/NetworkRequestGate";
import { useActionRequest } from "../../../../../common/hooks/api";
import { Organization } from "../../../../../common/models";
import { Api } from "../../../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../../../common/service/server/dto/RequestHandlingInstructionDto";
import {
  DataRecipientRetentionSettings,
  finishedRetention,
  saveRetentionUpdates,
  useDataRecipientRetentionSettings,
} from "../../../../components/organisms/data-recipients/DataRecipientRetentionSettings";
import { SettingsIndicator } from "../../../../components/organisms/data-recipients/SettingsIndicator";
import { useOrganizationVendors } from "../../../../hooks";
import {
  useAppPlatforms,
  useRequestInstructions,
  useRetentionReasons,
} from "../../../../hooks/useOrganizationVendors";
import { isThirdPartyRecipient } from "../../../../util/vendors";
import { useDataRetentionItems } from "../../../stay-compliant/DataRecipientDetailPage";
import { EditTab } from "../EditTab";

type DataDisclosureTabProps = {
  handleRefresh: () => Promise<void>;
  org: Organization;
  dataRecipient: OrganizationDataRecipientDto;
};

export const DataRetentionSettingsTab: React.FC<DataDisclosureTabProps> = ({
  org,
  dataRecipient,
  handleRefresh,
}) => {
  const [editMode, setEditMode] = useState(false);

  const enableEditMode = () => {
    setEditMode(true);
  };

  const [organizationVendors, organizationVendorsRequest, refreshVendors] = useOrganizationVendors(
    org.id,
  );
  const [appPlatforms, appPlatformsRequest, refreshAppPlatforms] = useAppPlatforms(
    org.id,
    dataRecipient.vendorId,
  );

  const [fetchedInstructions, instructionsRequest, refreshInstructions] = useRequestInstructions(
    org.id,
    "DELETE",
  );

  const [retentionReasons, retentionReasonsRequest, refreshRetentionReasons] =
    useRetentionReasons();

  const { fetch: updateRemoteInstruction } = useActionRequest({
    api: async (newInstruction: RequestHandlingInstructionDto) => {
      await Api.requestHandlingInstructions.putInstruction(org.id, newInstruction);
    },
    onSuccess: async () =>
      await Promise.all([
        handleRefresh(),
        refreshInstructions(),
        refreshRetentionReasons(),
        refreshVendors(),
        refreshAppPlatforms(),
      ]),
  });

  const {
    state: retentionState,
    meta: retentionMeta,
    updateState: retentionUpdate,
  } = useDataRecipientRetentionSettings(
    dataRecipient,
    organizationVendors,
    appPlatforms,
    retentionReasons,
    fetchedInstructions,
  );

  const dataDisclosureSettingsItems = useDataRetentionItems(
    dataRecipient,
    appPlatforms,
    organizationVendors,
    fetchedInstructions,
    retentionReasons,
  );

  const disableEditMode = async () => {
    await saveRetentionUpdates(
      org,
      dataRecipient,
      organizationVendors,
      appPlatforms,
      updateRemoteInstruction,
      retentionMeta,
      retentionState,
    );

    setEditMode(false);
  };

  return (
    <NetworkRequestGate
      requests={[
        appPlatformsRequest,
        instructionsRequest,
        retentionReasonsRequest,
        organizationVendorsRequest,
      ]}
    >
      <EditTab
        isEditing={editMode}
        onEnableEditing={enableEditMode}
        onDisableEditing={disableEditMode}
        saveDisabled={!finishedRetention(retentionState, retentionMeta)}
      >
        {editMode ? (
          <DataRecipientRetentionSettings
            dataRecipient={dataRecipient}
            isThirdPartyRecipient={isThirdPartyRecipient(dataRecipient)}
            state={retentionState}
            meta={retentionMeta}
            updateState={retentionUpdate}
          />
        ) : (
          <>
            {dataDisclosureSettingsItems.map((item) => {
              return <SettingsIndicator key={item.id} settingsItem={item} />;
            })}
          </>
        )}
      </EditTab>
    </NetworkRequestGate>
  );
};
