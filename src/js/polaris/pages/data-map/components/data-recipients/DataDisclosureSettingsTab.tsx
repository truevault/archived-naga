import React, { useState } from "react";
import { NetworkRequestGate } from "../../../../../common/components/NetworkRequestGate";
import { useActionRequest } from "../../../../../common/hooks/api";
import { useSurvey } from "../../../../../common/hooks/useSurvey";
import { Organization } from "../../../../../common/models";
import { Api } from "../../../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  DataRecipientDisclosureSettings,
  finishedDisclosure,
  saveDisclosureUpdates,
  useDataRecipientDisclosureSettings,
} from "../../../../components/organisms/data-recipients/DataRecipientDisclosureSettings";
import { SettingsIndicator } from "../../../../components/organisms/data-recipients/SettingsIndicator";
import { isThirdPartyRecipient } from "../../../../util/vendors";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../../get-compliant/data-recipients/shared";
import { useDataDisclosureItems } from "../../../stay-compliant/DataRecipientDetailPage";
import { UpdateVendorArgs } from "../../../stay-compliant/DataRecipientEditPage";
import { EditTab } from "../EditTab";

type DataDisclosureTabProps = {
  handleRefresh: () => Promise<void>;
  org: Organization;
  dataRecipient: OrganizationDataRecipientDto;
};

export const DataDisclosureSettingsTab: React.FC<DataDisclosureTabProps> = ({
  org,
  dataRecipient,
  handleRefresh,
}) => {
  const [editMode, setEditMode] = useState(false);

  const enableEditMode = () => {
    setEditMode(true);
  };

  const { answers, surveyRequest, updateSurveyQuestions, refreshSurvey } = useSurvey(
    org.id,
    SELLING_AND_SHARING_SURVEY_NAME,
  );

  const { fetch: updateDataRecipient } = useActionRequest({
    api: async (v: UpdateVendorArgs) => {
      const { vendorId, vendorSettings } = v;
      await Api.organizationVendor.addOrUpdateVendor(org.id, vendorId, vendorSettings);
    },
    onSuccess: async () => await Promise.all([handleRefresh(), refreshSurvey()]),
  });

  const {
    state: disclosureState,
    meta: disclosureMeta,
    updateState: disclosureUpdate,
  } = useDataRecipientDisclosureSettings(
    dataRecipient,
    answers,
    dataRecipient?.isCustom,
    dataRecipient?.category,
  );

  const dataDisclosureSettingsItems = useDataDisclosureItems(
    org,
    dataRecipient,
    disclosureState,
    disclosureMeta,
    answers,
  );

  const disableEditMode = async () => {
    await saveDisclosureUpdates(
      org,
      dataRecipient,
      answers,
      disclosureState,
      updateDataRecipient,
      updateSurveyQuestions,
    );

    setEditMode(false);
  };

  const finished = finishedDisclosure(
    disclosureState,
    disclosureMeta,
    dataRecipient.dataRecipientType == "third_party_recipient",
  );

  return (
    <NetworkRequestGate requests={[surveyRequest]}>
      <EditTab
        isEditing={editMode}
        saveDisabled={!finished}
        onEnableEditing={enableEditMode}
        onDisableEditing={disableEditMode}
      >
        {editMode ? (
          <DataRecipientDisclosureSettings
            org={org}
            vendor={dataRecipient}
            isThirdParty={isThirdPartyRecipient(dataRecipient)}
            state={disclosureState}
            meta={disclosureMeta}
            updateState={disclosureUpdate}
          />
        ) : (
          <>
            {dataDisclosureSettingsItems.map((item) => {
              return <SettingsIndicator settingsItem={item} key={item.id} />;
            })}
          </>
        )}
      </EditTab>
    </NetworkRequestGate>
  );
};
