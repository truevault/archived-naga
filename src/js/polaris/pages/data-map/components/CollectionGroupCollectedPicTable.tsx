import { Typography } from "@mui/material";
import _ from "lodash";
import React, { useMemo } from "react";
import {
  CellProps,
  ColumnDefinition,
  TableDefinition,
  TileTable,
} from "../../../../common/components/TileTable";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import {
  DataMapDto,
  groupsFromPIC,
  PICGroup,
} from "../../../../common/service/server/dto/DataMapDto";
import { EmptyInfo } from "../../../components/Empty";
import { GroupIcon } from "../../get-compliant/data-map/GroupIcon";

const NameCell: React.FC<CellProps<PICGroup>> = ({ data }) => (
  <span className="d-flex flex-row">
    <GroupIcon groupName={data.name} color="primary" />

    <div>
      <Typography>{data.name}</Typography>
      <Typography className="color-neutral-600" variant="caption">
        {data.categories.map((g) => g.name).join(", ")}
      </Typography>
    </div>
  </span>
);

export const COLLECTION_GROUP_COLUMNS: ColumnDefinition<PICGroup>[] = [
  {
    heading: null,
    component: NameCell,
  },
];

export const CollectionGroupCollectedPicTable: React.FC<{
  collectionGroup: CollectionGroupDetailsDto;
  dataMap: DataMapDto;
}> = ({ collectionGroup, dataMap }) => {
  const collectionDetails = dataMap.collection.find(
    (c) => c.collectionGroupId === collectionGroup.id,
  );

  const collectedPic = dataMap.categories.filter(
    (c) => collectionDetails?.collected.includes(c.id) ?? false,
  );
  const collectedPicGroups = useMemo(() => groupsFromPIC(collectedPic), [collectedPic]);
  const sortedPicGroups = useMemo(
    () => _.sortBy(collectedPicGroups, ["order", "name"]),
    [collectedPicGroups],
  );

  if (!sortedPicGroups || sortedPicGroups.length < 1) {
    return <EmptyInfo>No data collected</EmptyInfo>;
  }

  const table: TableDefinition<PICGroup> = {
    data: sortedPicGroups,
    columns: COLLECTION_GROUP_COLUMNS,
  };

  return <TileTable variant="filled" table={table} />;
};
