import { Button } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router";
import { UUIDString } from "../../../../../../common/service/server/Types";
import { CollectionGroupDetailsDto } from "../../../../../../common/service/server/dto/CollectionGroupDto";
import { CollectionGroupCollapseCard } from "../../../../../components/CollectionGroupCollapseCard";
import { StandardPageWrapper } from "../../../../../components/layout/StandardPageWrapper";
import { PageHeader } from "../../../../../components/layout/header/Header";
import { usePrimaryOrganizationId } from "../../../../../hooks";
import { useCollectionGroups } from "../../../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../../../hooks/useDataMap";
import { useOrganizationVendorDetails } from "../../../../../hooks/useOrganizationVendors";
import { RouteHelpers } from "../../../../../root/routes";
import { toggleSelection } from "../../../../../surveys/util";
import { ProgressFooter, ProgressFooterActions } from "../../../../get-compliant/ProgressFooter";
import { DataRecipientCollectionCheckboxes } from "../../../../get-compliant/data-recipients/DataRecipientCollectionCheckboxes";

type Params = {
  dataRecipientId: UUIDString;
};

const CollectionGroupDisclosureCard = ({
  dataMap,
  collectionGroup,
  dataRecipient,
  collapsed,
  onExpand,
  setHasSelection,
  onNext,
}) => {
  const orgId = usePrimaryOrganizationId();

  return (
    <CollectionGroupCollapseCard
      className="mb-md"
      collectionGroup={collectionGroup}
      collapsed={collapsed}
      onExpand={onExpand}
    >
      <DataRecipientCollectionCheckboxes
        recipient={dataRecipient}
        dataMap={dataMap}
        orgId={orgId}
        group={collectionGroup}
        setHasSelection={setHasSelection}
      />

      {Boolean(onNext) && (
        <Button variant="contained" onClick={onNext}>
          Next Group
        </Button>
      )}
    </CollectionGroupCollapseCard>
  );
};

export const DisclosureForDataRecipientPage = () => {
  const orgId = usePrimaryOrganizationId();
  const { dataRecipientId } = useParams<Params>();

  const [dataMap, dataMapRequest] = useDataMap(orgId);
  const [dataRecipient, dataRecipientRequest] = useOrganizationVendorDetails(
    orgId,
    dataRecipientId,
  );
  const [collectionGroups, collectionGroupsRequest] = useCollectionGroups(orgId);
  const [activeGroup, setActiveGroup] = useState<CollectionGroupDetailsDto>(null);

  const receipts = useMemo(
    () => dataMap?.disclosure.filter((d) => d.vendorId == dataRecipient?.vendorId) || [],
    [dataMap?.disclosure, dataRecipient],
  );

  const groupIds = useMemo(() => receipts.map((d) => d.collectionGroupId), [receipts]);

  const relevantGroups = useMemo(
    () => collectionGroups?.filter((r) => groupIds?.includes(r.id)),
    [collectionGroups, groupIds],
  );

  const handleExpand = (cg: CollectionGroupDetailsDto) => {
    setActiveGroup(cg);
    window.polaris.scrollSelectorIntoView(`#cg-${cg.id}`);
  };

  const [groupsWithSelection, setGroupsWithSelection] = useState([]);

  const allSelected = useMemo(
    () => relevantGroups.every((cg) => groupsWithSelection.includes(cg.id)),
    [relevantGroups, groupsWithSelection],
  );

  useEffect(() => {
    setActiveGroup((group) => {
      if (!group) {
        return relevantGroups[0];
      }
    });
  }, [relevantGroups]);

  return (
    <StandardPageWrapper
      requests={[collectionGroupsRequest, dataMapRequest, dataRecipientRequest]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextUrl={RouteHelpers.dataMap.dataRecipient.workflow.classification(dataRecipientId)}
              nextContent="Next"
              prevContent="Back"
              nextDisabled={!allSelected}
              prevUrl={RouteHelpers.dataMap.dataRecipient.workflow.groupAssociation(
                dataRecipientId,
              )}
            />
          }
        />
      }
    >
      <PageHeader titleContent={"Add Data Recipient"} />

      {relevantGroups.map((cg, idx) => {
        let nextGroup: CollectionGroupDetailsDto | null = null;

        if (idx < relevantGroups.length - 1) {
          nextGroup = relevantGroups[idx + 1];
        }

        return (
          <div key={cg.id} id={`cg-${cg.id}`} className="mb-md">
            <CollectionGroupDisclosureCard
              dataRecipient={dataRecipient}
              dataMap={dataMap}
              collectionGroup={cg}
              collapsed={activeGroup?.id !== cg.id}
              onExpand={() => handleExpand(cg)}
              setHasSelection={(hasSelection: boolean) =>
                setGroupsWithSelection((prev) => toggleSelection(prev, cg.id, hasSelection))
              }
              onNext={nextGroup ? () => handleExpand(nextGroup) : undefined}
            />
          </div>
        );
      })}
    </StandardPageWrapper>
  );
};
