import React, { useMemo } from "react";
import { useHistory, useParams } from "react-router";
import { useActionRequest } from "../../../../../../common/hooks/api";
import { useSurvey } from "../../../../../../common/hooks/useSurvey";
import { Api } from "../../../../../../common/service/Api";
import { UUIDString } from "../../../../../../common/service/server/Types";
import { UpdateOrganizationSurveyQuestionDto } from "../../../../../../common/service/server/controller/OrganizationController";
import { OrganizationDataRecipientDto } from "../../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../../../../common/service/server/dto/RequestHandlingInstructionDto";
import { StandardPageWrapper } from "../../../../../components/layout/StandardPageWrapper";
import { PageHeader, VendorPageHeader } from "../../../../../components/layout/header/Header";
import {
  DataRecipientBasicSettings,
  useDataRecipientBasicSettings,
} from "../../../../../components/organisms/data-recipients/DataRecipientBasicSettings";
import {
  DataRecipientDisclosureSettings,
  finishedDisclosure,
  saveDisclosureUpdates,
  useDataRecipientDisclosureSettings,
} from "../../../../../components/organisms/data-recipients/DataRecipientDisclosureSettings";
import {
  DataRecipientRetentionSettings,
  finishedRetention,
  saveRetentionUpdates,
  useDataRecipientRetentionSettings,
} from "../../../../../components/organisms/data-recipients/DataRecipientRetentionSettings";
import {
  useDataRecipientsCatalog,
  useOrganizationVendors,
  usePrimaryOrganization,
  usePrimaryOrganizationId,
} from "../../../../../hooks";
import {
  useRequestInstructions,
  useRetentionReasons,
} from "../../../../../hooks/useOrganizationVendors";
import { RouteHelpers } from "../../../../../root/routes";
import { getVendorSettingsObject } from "../../../../../util/vendors";
import { ProgressFooter, ProgressFooterActions } from "../../../../get-compliant/ProgressFooter";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../../../get-compliant/data-recipients/shared";
import { finishedCustom } from "../../../../stay-compliant/CustomFields";
import { UpdateVendorArgs } from "../../../../stay-compliant/DataRecipientEditPage";

type Params = {
  dataRecipientId: UUIDString;
};

export const ClassificationForDataRecipientPage = () => {
  // Variables
  const history = useHistory();
  const organizationId = usePrimaryOrganizationId();
  const [org] = usePrimaryOrganization();
  const { dataRecipientId } = useParams<Params>();

  const [fetchedInstructions, instructionsRequest] = useRequestInstructions(org.id, "DELETE");

  const thirdParty = new URLSearchParams(location.search).has("thirdParty");

  const { name, email, category, url } = Object.fromEntries(
    new URLSearchParams(location.search) as any,
  );

  const initialValues = useMemo(
    () => ({ name, email, category, url }),
    [name, email, category, url],
  );

  // State
  const [vendorDetails, vendorDetailsRequest] = useDataRecipientsCatalog(organizationId);
  const [orgVendors, orgVendorsRequest] = useOrganizationVendors(organizationId, undefined, true);

  // Requests
  const [retentionReasons, retentionReasonsRequest] = useRetentionReasons();

  const { answers, surveyRequest: surveyQuestionsRequest } = useSurvey(
    organizationId,
    SELLING_AND_SHARING_SURVEY_NAME,
  );

  const updateRemoteInstruction = async (newInstruction: RequestHandlingInstructionDto) => {
    await Api.requestHandlingInstructions.putInstruction(org.id, newInstruction);
  };

  const custom = false;
  const vendor = !custom && vendorDetails.find((v) => v.id == dataRecipientId);

  const existingDataRecipient = orgVendors.find((v) => v.vendorId == dataRecipientId);

  const unionVendor = existingDataRecipient || vendor;

  const isStorageLocation = unionVendor.dataRecipientType == "storage_location";

  const { state: basicState, updateState: basicUpdate } =
    useDataRecipientBasicSettings(initialValues);

  const {
    state: disclosureState,
    meta: disclosureMeta,
    updateState: disclosureUpdate,
  } = useDataRecipientDisclosureSettings(unionVendor, answers, custom, basicState.category);

  const {
    state: retentionState,
    meta: retentionMeta,
    updateState: retentionUpdate,
  } = useDataRecipientRetentionSettings(
    unionVendor,
    orgVendors,
    null,
    retentionReasons,
    fetchedInstructions,
  );

  const updateVendor = async (v: UpdateVendorArgs) => {
    const { vendorId, vendorSettings } = v;
    await Api.organizationVendor.addOrUpdateVendor(org.id, vendorId, vendorSettings);
  };

  const { fetch: updateSurveyQuestions } = useActionRequest({
    api: ({ questions }: { questions: UpdateOrganizationSurveyQuestionDto[] }) =>
      Api.organization.updateOrganizationSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME, questions),
    allowConcurrent: true,
  });

  const { fetch: setDataRecipientComplete } = useActionRequest({
    api: (dataRecipient: OrganizationDataRecipientDto) => {
      const currentSettings = getVendorSettingsObject(dataRecipient);
      return Api.organizationVendor.addOrUpdateVendor(org.id, dataRecipient.vendorId, {
        ...currentSettings,
        completed: true,
      });
    },
  });

  const vendorName = custom ? "Custom Vendor" : unionVendor?.name;

  const descLine = [vendor?.category, vendor?.url, vendor?.email].filter((x) => x).join(" · ");
  const desc = (
    <>
      <p className="text-muted text-t1 my-0">{descLine}</p>
      {vendor?.dpaUrl && (
        <p className="text-t1 my-0">
          <a href={vendor?.dpaUrl} target="_blank" rel="noopener noreferrer">
            Data Processing Agreement (DPA)
          </a>
        </p>
      )}
    </>
  );

  const finCustom = !custom || finishedCustom(basicState);

  const finished =
    (isStorageLocation || finishedDisclosure(disclosureState, disclosureMeta, thirdParty)) &&
    finishedRetention(retentionState, retentionMeta) &&
    finCustom;

  const onDone = async () => {
    const settings: any = {
      ccpaIsSelling: disclosureState.ccpaIsSelling,
      dataRecipientType: thirdParty ? "third_party_recipient" : "vendor",
    };

    if (custom) {
      Object.assign(settings, basicState);
    }

    // we need to add before we can set the classification option
    let res;
    if (custom || thirdParty) {
      res = await Api.organizationVendor.addCustomVendor(organizationId, settings);
    } else {
      res = await Api.organizationVendor.addOrUpdateVendor(
        organizationId,
        dataRecipientId,
        settings,
      );
    }

    if (!isStorageLocation) {
      await saveDisclosureUpdates(
        org,
        res,
        answers,
        disclosureState,
        updateVendor,
        updateSurveyQuestions,
      );
    }

    await saveRetentionUpdates(
      org,
      res,
      orgVendors,
      null,
      updateRemoteInstruction,
      retentionMeta,
      retentionState,
    );

    if (existingDataRecipient) {
      setDataRecipientComplete(existingDataRecipient);
    }

    history.push(RouteHelpers.dataMap.dataRecipient.detail(dataRecipientId));
  };

  if (!unionVendor) {
    return null;
  }

  return (
    <StandardPageWrapper
      requests={[
        surveyQuestionsRequest,
        vendorDetailsRequest,
        retentionReasonsRequest,
        instructionsRequest,
        orgVendorsRequest,
      ]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextOnClick={onDone}
              nextDisabled={!finished}
              nextContent="Done"
              prevContent="Back"
              prevUrl={RouteHelpers.dataMap.dataRecipient.workflow.disclosure(dataRecipientId)}
            />
          }
        />
      }
    >
      {Boolean(unionVendor) && (
        <VendorPageHeader
          vendor={vendor}
          titleContent={vendorName}
          descriptionContent={!custom && desc}
        />
      )}

      {!unionVendor && (
        <PageHeader
          titleContent={
            thirdParty ? "Add Third Party Recipient" : custom ? "Add Custom Vendor" : "Add Vendor"
          }
        />
      )}

      {custom && (
        <DataRecipientBasicSettings
          orgVendors={orgVendors}
          vendors={vendorDetails}
          isThirdParty={thirdParty}
          state={basicState}
          updateState={basicUpdate}
        />
      )}

      {!isStorageLocation && (
        <DataRecipientDisclosureSettings
          org={org}
          vendor={unionVendor}
          isThirdParty={thirdParty}
          state={disclosureState}
          meta={disclosureMeta}
          updateState={disclosureUpdate}
        />
      )}

      <DataRecipientRetentionSettings
        dataRecipient={unionVendor}
        isThirdPartyRecipient={thirdParty}
        state={retentionState}
        meta={retentionMeta}
        updateState={retentionUpdate}
      />
    </StandardPageWrapper>
  );
};
