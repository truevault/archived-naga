import { Typography } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { useHistory, useParams } from "react-router";
import { useActionRequest } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { CollectionGroupType } from "../../../../../../common/service/server/controller/CollectionGroupsController";
import { OrganizationPublicId, UUIDString } from "../../../../../../common/service/server/Types";
import { PageHeader } from "../../../../../components/layout/header/Header";
import { StandardPageWrapper } from "../../../../../components/layout/StandardPageWrapper";
import { CollectionGroupMultiSelect } from "../../../../../components/organisms/collection-groups/CollectionGroupMultiSelect";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../../../hooks";
import {
  EXCLUDE_GROUP_EMPLOYMENT,
  useCollectionGroups,
} from "../../../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../../../hooks/useDataMap";
import { RouteHelpers, Routes } from "../../../../../root/routes";
import { toggleSelection } from "../../../../../surveys/util";
import { ProgressFooter, ProgressFooterActions } from "../../../../get-compliant/ProgressFooter";

type Params = {
  dataRecipientId: UUIDString;
};

export const CollectionGroupAssociationForDataRecipientPage = () => {
  const history = useHistory();
  const orgId = usePrimaryOrganizationId();
  const { dataRecipientId } = useParams<Params>();

  const data = useDataRecipientAssociationForGroup(orgId, dataRecipientId, null);

  return (
    <StandardPageWrapper
      requests={[...Object.values(data.requests)]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextOnClick={() =>
                history.push(
                  RouteHelpers.dataMap.dataRecipient.workflow.disclosure(dataRecipientId),
                )
              }
              nextContent="Next"
              prevContent="Cancel"
              nextDisabled={data.updateAssociationRequest.running || !data.selected?.length}
              prevOnClick={() => history.push(Routes.dataMap.root)}
            />
          }
        />
      }
    >
      <PageHeader titleContent={"Add Data Recipient"} />

      <Typography>
        Which Collection Groups have information sourced from{" "}
        <strong>{data?.dataRecipient?.name}</strong>?
      </Typography>

      <CollectionGroupMultiSelect {...data.multiselect} />
    </StandardPageWrapper>
  );
};

const useDataRecipientAssociationForGroup = (
  orgId: OrganizationPublicId,
  dataRecipientId: UUIDString,
  excludeTypes: CollectionGroupType[] = EXCLUDE_GROUP_EMPLOYMENT,
) => {
  const [selected, setSelected] = useState<UUIDString[]>([]);

  const [collectionGroups, groupsRequest] = useCollectionGroups(orgId, undefined, excludeTypes);
  const [orgDataRecipients, orgDataRecipientsRequest] = useOrganizationVendors(orgId);
  const [dataMap, dataMapRequest] = useDataMap(orgId);

  const dataRecipient = useMemo(
    () => orgDataRecipients?.find((s) => s.vendorId === dataRecipientId),
    [orgDataRecipients, dataRecipientId],
  );

  const { fetch: updateRecipientAssociation, request: updateAssociationRequest } = useActionRequest(
    {
      api: ({ groupId, associated }) =>
        Api.dataMap.updateRecipientAssociation(orgId, groupId, dataRecipientId, associated),
    },
  );

  const onChange = ({ groupId, included }) => {
    setSelected((prevSelected) => toggleSelection(prevSelected, groupId, included));
    updateRecipientAssociation({ groupId, associated: included });
  };

  useEffect(() => {
    if (dataMap && dataRecipient) {
      const associatedGroupIds = dataMap.disclosure
        .filter((d) => d.vendorId == dataRecipient.vendorId)
        .map((d) => d.collectionGroupId);

      setSelected(associatedGroupIds);
    }
  }, [dataMap, dataRecipient]);

  return {
    selected,
    dataRecipient,
    updateAssociationRequest,
    fetched: {
      orgDataRecipients,
      dataMap,
      collectionGroups,
    },
    requests: {
      sources: orgDataRecipientsRequest,
      dataMap: dataMapRequest,
      collectionGroups: groupsRequest,
    },
    multiselect: {
      collectionGroups,
      selected,
      onChange,
    },
  };
};
