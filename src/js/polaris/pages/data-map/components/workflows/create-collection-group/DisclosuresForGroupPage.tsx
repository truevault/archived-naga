import { Stack } from "@mui/material";
import _ from "lodash";
import React, { useMemo } from "react";
import { useHistory, useParams } from "react-router";
import { useSurvey } from "../../../../../../common/hooks/useSurvey";
import { CollectionContext } from "../../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../../../common/service/server/Types";
import { PageHeader } from "../../../../../components/layout/header/Header";
import { StandardPageWrapper } from "../../../../../components/layout/StandardPageWrapper";
import {
  FcraCallout,
  GlbaCallout,
  HipaaCallout,
} from "../../../../../components/organisms/callouts/RegulationCallouts";
import { DataRecipientCollectionCards } from "../../../../../components/organisms/DataRecipientCollectionCards";
import { DataRecipientDisclosureCheckboxes } from "../../../../../components/organisms/DataRecipientDisclosureCheckboxes";
import { usePrimaryOrganizationId } from "../../../../../hooks";
import {
  EXCLUDE_GROUP_EMPLOYMENT,
  EXCLUDE_GROUP_NON_EMPLOYMENT,
  useCollectionGroup,
} from "../../../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../../../hooks/useDataMap";
import { useDisclosuresForCollectionGroups } from "../../../../../hooks/useDisclosuresForCollectionGroups";
import { RouteHelpers } from "../../../../../root/routes";
import { BUSINESS_SURVEY_NAME } from "../../../../../surveys/businessSurvey";
import { ProgressFooter, ProgressFooterActions } from "../../../../get-compliant/ProgressFooter";
import { useVendorAssociation } from "../../../../get-compliant/data-recipients/VendorAssociationForGroup";
import { Api } from "../../../../../../common/service/Api";
import { useDataSourceAssociation } from "../../../../../hooks/useVendorAssociation";
import { useActionRequest } from "../../../../../../common/hooks/api";

type Params = {
  dataSubjectTypeId: UUIDString;
};

export const DisclosuresForGroupPage = () => {
  const history = useHistory();

  const { dataSubjectTypeId: collectionGroupId } = useParams<Params>();

  const orgId = usePrimaryOrganizationId();
  const [dataMap] = useDataMap(orgId);
  const { answers, surveyRequest } = useSurvey(orgId, BUSINESS_SURVEY_NAME);
  const [collectionGroup, collectionGroupsRequest] = useCollectionGroup(orgId, collectionGroupId);
  const isEmployment = collectionGroup?.collectionGroupType == "EMPLOYMENT";
  const context: CollectionContext[] = isEmployment ? ["EMPLOYEE"] : ["CONSUMER"];
  const disclosureData = useDisclosuresForCollectionGroups(orgId, [collectionGroup], context);

  const { fetched } = useDataSourceAssociation(
    orgId,
    collectionGroupId,
    isEmployment ? EXCLUDE_GROUP_NON_EMPLOYMENT : EXCLUDE_GROUP_EMPLOYMENT,
    isEmployment ? ["EMPLOYEE"] : ["CONSUMER"],
  );

  const hasSources = Boolean(fetched?.sources.length > 0);

  const { fetch: completeTasks } = useActionRequest({
    api: () => Api.tasks.completeTasks(orgId),
    onSuccess: () => {
      history.push(RouteHelpers.dataMap.collectionGroup.detail(collectionGroupId));
    },
  });

  const handleDone = () => completeTasks();

  const { dataSubjectTypeId } = useParams<Params>();

  const { selected } = useVendorAssociation(
    orgId,
    dataSubjectTypeId,
    isEmployment ? EXCLUDE_GROUP_NON_EMPLOYMENT : EXCLUDE_GROUP_EMPLOYMENT,
    undefined,
    isEmployment ? ["EMPLOYEE"] : ["CONSUMER"],
  );

  const selectedDataRecipients = useMemo(() => {
    return disclosureData.dataRecipients.filter((dr) => selected.some((id) => id == dr.vendorId));
  }, [disclosureData.dataRecipients, selected]);

  const allComplete = useMemo(
    () =>
      _.every(
        selectedDataRecipients,
        (dr) => disclosureData.disclosuresByVendorId[dr.vendorId]?.length > 0 ?? false,
      ),
    [selectedDataRecipients, disclosureData.disclosuresByVendorId],
  );

  return (
    <StandardPageWrapper
      requests={[
        collectionGroupsRequest,
        surveyRequest,
        disclosureData.requests.dataMapRequest,
        disclosureData.requests.recipientsRequest,
      ]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextContent={hasSources ? "Next" : "Done"}
              nextDisabled={!allComplete}
              prevOnClick={() =>
                history.push(
                  RouteHelpers.dataMap.collectionGroup.workflow.dataRecipientAssociation(
                    collectionGroupId,
                  ),
                )
              }
              nextOnClick={() =>
                hasSources
                  ? history.push(
                      RouteHelpers.dataMap.collectionGroup.workflow.sourceAssociation(
                        collectionGroupId,
                      ),
                    )
                  : handleDone()
              }
            />
          }
        />
      }
    >
      <PageHeader titleContent={collectionGroup?.name} />

      <h4 className="intermediate">
        What categories of information are collected, analyzed by, stored in, or otherwise processed
        by your Data Recipients?
      </h4>

      <p>
        For each of your Data Recipients, identify which categories of personal data they process,
        including any categories of digital information that may be collected and processed
        automatically.
      </p>

      {answers["business-hipaa-compliant"] === "true" && (
        <div className="mb-md">
          <HipaaCallout />
        </div>
      )}
      {answers["business-glba-compliant"] === "true" && (
        <div className="mb-md">
          <GlbaCallout />
        </div>
      )}
      {answers["business-fcra-compliant"] === "true" && (
        <div className="mb-md">
          <FcraCallout />
        </div>
      )}

      <Stack spacing={3}>
        <DataRecipientCollectionCards
          recipients={selectedDataRecipients}
          context={context}
          disclosuresByVendorId={disclosureData.disclosuresByVendorId}
          renderer={(r) => (
            <DataRecipientDisclosureCheckboxes
              dataRecipient={r}
              collectedPicGroups={disclosureData.collectedPicGroups}
              selected={disclosureData.disclosuresByVendorId[r.vendorId]?.flatMap((d) => d.id)}
              dataMap={dataMap}
              showRecommendations
              toggleDisclosure={disclosureData.actions.toggleDisclosure}
            />
          )}
        />
      </Stack>
    </StandardPageWrapper>
  );
};
