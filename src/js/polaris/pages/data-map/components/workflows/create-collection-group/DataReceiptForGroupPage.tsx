import { Stack, Typography } from "@mui/material";
import React, { useMemo } from "react";
import { useHistory, useParams } from "react-router";
import { useActionRequest } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { CollectionContext } from "../../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../../../common/service/server/Types";
import { PageHeader } from "../../../../../components/layout/header/Header";
import { StandardPageWrapper } from "../../../../../components/layout/StandardPageWrapper";
import { DataSourceReceiptCards } from "../../../../../components/organisms/DataSourceReceiptCards";
import { DataSourceReceiptCheckboxes } from "../../../../../components/organisms/DataSourceReceiptCheckboxes";
import { usePrimaryOrganizationId } from "../../../../../hooks";
import {
  EXCLUDE_GROUP_EMPLOYMENT,
  EXCLUDE_GROUP_NON_EMPLOYMENT,
  useCollectionGroup,
} from "../../../../../hooks/useCollectionGroups";
import { useReceiptsForCollectionGroups } from "../../../../../hooks/useReceiptsForCollectionGroups";
import { RouteHelpers } from "../../../../../root/routes";
import { useUpdateDataSources } from "../../../../get-compliant/data-map/ConsumerDataSourcesPage";
import { ProgressFooter, ProgressFooterActions } from "../../../../get-compliant/ProgressFooter";
import { useDataSourceAssociation } from "../../../../../hooks/useVendorAssociation";

type Params = {
  dataSubjectTypeId: UUIDString;
};

export const DataReceiptForGroupPage = () => {
  const orgId = usePrimaryOrganizationId();
  const { dataSubjectTypeId: collectionGroupId } = useParams<Params>();

  const history = useHistory();

  const [collectionGroup, collectionGroupsRequest] = useCollectionGroup(orgId, collectionGroupId);
  const context: CollectionContext[] =
    collectionGroup?.collectionGroupType == "EMPLOYMENT" ? ["EMPLOYEE"] : ["CONSUMER"];
  const receiptData = useReceiptsForCollectionGroups(
    orgId,
    [collectionGroup].filter((x) => x),
    context,
  );
  const updateDataSources = useUpdateDataSources(orgId, { collectionContext: context });

  const isEmployment = collectionGroup?.collectionGroupType === "EMPLOYMENT";

  const { selected } = useDataSourceAssociation(
    orgId,
    collectionGroupId,
    isEmployment ? EXCLUDE_GROUP_NON_EMPLOYMENT : EXCLUDE_GROUP_EMPLOYMENT,
    isEmployment ? ["EMPLOYEE"] : ["CONSUMER"],
  );

  const { fetch: completeTasks } = useActionRequest({
    api: () => Api.tasks.completeTasks(orgId),
    onSuccess: () => {
      history.push(RouteHelpers.dataMap.collectionGroup.detail(collectionGroupId));
    },
  });

  const handleDone = () => completeTasks();

  const selectedSources = useMemo(() => {
    return receiptData.sources.filter((r) => selected.find((s) => s == r.vendorId));
  }, [selected, receiptData.sources]);

  const everyDataSourceHasPIC = updateDataSources.data.sources
    .filter((ds) => selected.find((s) => s == ds.id))
    .every((source) => receiptData.receiptsByVendorId?.[source.id]?.length > 0);
  const isFinished = everyDataSourceHasPIC;

  return (
    <StandardPageWrapper
      requests={[
        collectionGroupsRequest,
        receiptData.requests.dataMapRequest,
        receiptData.requests.sourcesRequest,
      ]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextContent={"Done"}
              nextDisabled={!isFinished}
              prevOnClick={() =>
                history.push(
                  RouteHelpers.dataMap.collectionGroup.workflow.sourceAssociation(
                    collectionGroupId,
                  ),
                )
              }
              nextOnClick={handleDone}
            />
          }
        />
      }
    >
      <PageHeader titleContent={collectionGroup?.name} />
      <Typography>
        Your third party collection sources are listed below. Identify which categories of personal
        information you collect from each source.
      </Typography>

      <Stack spacing={3} className="my-lg">
        <DataSourceReceiptCards
          sources={selectedSources}
          disclosuresByVendorId={receiptData.receiptsByVendorId}
          consumerDirectlyName={isEmployment ? "Worker" : "Consumer"}
          renderer={(r) => (
            <DataSourceReceiptCheckboxes
              collectedPic={receiptData.collectedPIC}
              selected={receiptData.receiptsByVendorId[r.vendorId]?.flatMap((d) => d.id)}
              includeSelectAll={r.vendorKey === "source-consumer-directly"}
              pending={receiptData.pendingByVendorId[r.vendorId]?.flatMap((d) => d)}
              onSelectAll={(received: boolean) => {
                receiptData.actions.toggleReceipt({
                  vendorId: r.vendorId,
                  receivedIds: receiptData.collectedPIC.map((pic) => pic.id),
                  received,
                });
              }}
              onChange={(receivedId: UUIDString, received: boolean) =>
                receiptData.actions.toggleReceipt({
                  vendorId: r.vendorId,
                  receivedIds: [receivedId],
                  received,
                })
              }
            />
          )}
        />
      </Stack>
    </StandardPageWrapper>
  );
};
