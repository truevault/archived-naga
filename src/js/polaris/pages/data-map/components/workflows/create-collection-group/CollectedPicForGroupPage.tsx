import React, { useState } from "react";
import { useHistory, useParams } from "react-router";
import { useSurvey } from "../../../../../../common/hooks/useSurvey";
import { CollectionDto } from "../../../../../../common/service/server/dto/DataMapDto";
import { UUIDString } from "../../../../../../common/service/server/Types";
import { PageHeader } from "../../../../../components/layout/header/Header";
import { StandardPageWrapper } from "../../../../../components/layout/StandardPageWrapper";
import { ConsumerDataCollectionForm } from "../../../../../components/organisms/collection-groups/ConsumerDataCollectionForm";
import { EmployeeDataCollectionForm } from "../../../../../components/organisms/collection-groups/EmployeeDataCollectionForm";
import {
  collectsCommercialInfoSlugById,
  EMPLOYEE_COLLECTION_PURPOSES_SURVEY,
} from "../../../../../components/organisms/collection-groups/EmploymentGroupPurposesQuestion";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../../../../hooks";
import { useCollectionGroup } from "../../../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../../../hooks/useDataMap";
import { RouteHelpers, Routes } from "../../../../../root/routes";
import {
  consumerCollectionSurvey,
  SENSITIVE_INFERENCES_QUESTION_SLUG,
} from "../../../../../surveys/consumerCollectionCategoriesSurvey";
import { isFinished } from "../../../../../surveys/survey";
import { ProgressFooter, ProgressFooterActions } from "../../../../get-compliant/ProgressFooter";

type Params = {
  dataSubjectTypeId: UUIDString;
};

export const CollectedPicForGroupPage = () => {
  const history = useHistory();

  const { dataSubjectTypeId: collectionGroupId } = useParams<Params>();
  const orgId = usePrimaryOrganizationId();
  const [org, orgRequest] = usePrimaryOrganization();
  const [dataMap, dataMapRequest] = useDataMap(orgId);
  const [collectionGroup, collectionGroupsRequest] = useCollectionGroup(orgId, collectionGroupId);
  const [sensitiveInferencesQuestionIsShown, setSensitiveInferencesQuestionIsShown] =
    useState<Boolean>(false);

  const { answers, setAnswer, updateRequest } = useSurvey(
    orgId,
    collectionGroup ? `consumer-data-collection-${collectionGroup?.id}` : undefined,
  );

  const { answers: collectionAnswers } = useSurvey(orgId, EMPLOYEE_COLLECTION_PURPOSES_SURVEY);
  const collectionSlug = collectsCommercialInfoSlugById(collectionGroupId);

  const collected: CollectionDto = dataMap?.collection?.find(
    (c) => c.collectionGroupId == collectionGroup?.id,
  ) || {
    collected: [],
    collectionGroupId: collectionGroup?.id,
    collectionGroupName: collectionGroup?.name,
    collectionGroupType: collectionGroup?.collectionGroupType,
  };

  const survey = consumerCollectionSurvey(answers, dataMap, collected);
  const isEmploymentGroup = collectionGroup?.collectionGroupType === "EMPLOYMENT";

  const excludedQuestions = sensitiveInferencesQuestionIsShown
    ? []
    : [SENSITIVE_INFERENCES_QUESTION_SLUG];
  const hasFinishedSurvey = isEmploymentGroup
    ? Boolean(collectionAnswers[collectionSlug])
    : isFinished(survey, answers, excludedQuestions) && Boolean(answers["inferences-done"]);

  return (
    <StandardPageWrapper
      requests={[orgRequest, dataMapRequest, collectionGroupsRequest]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextContent={"Next"}
              nextLoading={updateRequest.running}
              nextDisabled={!hasFinishedSurvey || updateRequest.running}
              prevOnClick={() => history.push(Routes.dataMap.root)}
              nextOnClick={() =>
                history.push(
                  RouteHelpers.dataMap.collectionGroup.workflow.dataRecipientAssociation(
                    collectionGroupId,
                  ),
                )
              }
            />
          }
        />
      }
    >
      <PageHeader titleContent={collectionGroup?.name} />

      {isEmploymentGroup ? (
        <EmployeeDataCollectionForm
          answers={answers}
          setAnswer={setAnswer}
          dataMap={dataMap}
          org={org}
          collectionGroup={collectionGroup}
        />
      ) : (
        <ConsumerDataCollectionForm
          answers={answers}
          setAnswer={setAnswer}
          dataMap={dataMap}
          org={org}
          survey={survey}
          collected={collected}
          collectionGroup={collectionGroup}
          setSensitiveInferencesQuestionIsShown={setSensitiveInferencesQuestionIsShown}
        />
      )}
    </StandardPageWrapper>
  );
};
