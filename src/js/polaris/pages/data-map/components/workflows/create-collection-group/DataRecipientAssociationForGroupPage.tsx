import { Typography } from "@mui/material";
import React from "react";
import { useHistory, useParams } from "react-router";
import { UUIDString } from "../../../../../../common/service/server/Types";
import { PageHeader } from "../../../../../components/layout/header/Header";
import { StandardPageWrapper } from "../../../../../components/layout/StandardPageWrapper";
import { usePrimaryOrganizationId } from "../../../../../hooks";
import {
  EXCLUDE_GROUP_EMPLOYMENT,
  EXCLUDE_GROUP_NON_EMPLOYMENT,
  useCollectionGroup,
} from "../../../../../hooks/useCollectionGroups";
import { RouteHelpers } from "../../../../../root/routes";
import { DataRecipientMultiselect } from "../../../../design/molecules/DataRecipientMultiselect";
import { useVendorAssociation } from "../../../../get-compliant/data-recipients/VendorAssociationForGroup";
import { ProgressFooter, ProgressFooterActions } from "../../../../get-compliant/ProgressFooter";

type Params = {
  dataSubjectTypeId: UUIDString;
};

export const DataRecipientAssociationForGroupPage = () => {
  const history = useHistory();

  const { dataSubjectTypeId } = useParams<Params>();
  const orgId = usePrimaryOrganizationId();
  const [collectionGroup, collectionGroupsRequest] = useCollectionGroup(orgId, dataSubjectTypeId);
  const isEmployment = collectionGroup?.collectionGroupType === "EMPLOYMENT";

  const { multiselect, requests } = useVendorAssociation(
    orgId,
    dataSubjectTypeId,
    isEmployment ? EXCLUDE_GROUP_NON_EMPLOYMENT : EXCLUDE_GROUP_EMPLOYMENT,
    undefined,
    isEmployment ? ["EMPLOYEE"] : ["CONSUMER"],
  );

  return (
    <StandardPageWrapper
      requests={[collectionGroupsRequest, ...Object.values(requests)]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextContent={"Next"}
              nextDisabled={multiselect.selected.length === 0}
              prevOnClick={() =>
                history.push(
                  RouteHelpers.dataMap.collectionGroup.workflow.collection(dataSubjectTypeId),
                )
              }
              nextOnClick={() =>
                history.push(
                  RouteHelpers.dataMap.collectionGroup.workflow.disclosure(dataSubjectTypeId),
                )
              }
            />
          }
        />
      }
    >
      <PageHeader titleContent={collectionGroup?.name} />
      <Typography>
        Which of your Data Recipients process information about{" "}
        <strong>{collectionGroup?.name}</strong>?
      </Typography>

      <DataRecipientMultiselect {...multiselect} />
    </StandardPageWrapper>
  );
};
