import { Typography } from "@mui/material";
import React from "react";
import { useHistory, useParams } from "react-router";
import { UUIDString } from "../../../../../../common/service/server/Types";
import { PageHeader } from "../../../../../components/layout/header/Header";
import { StandardPageWrapper } from "../../../../../components/layout/StandardPageWrapper";
import { CollectionGroupMultiSelect } from "../../../../../components/organisms/collection-groups/CollectionGroupMultiSelect";
import { usePrimaryOrganizationId } from "../../../../../hooks";
import { useDataSourceAssociationForSource } from "../../../../../hooks/useVendorAssociation";
import { RouteHelpers, Routes } from "../../../../../root/routes";

import { ProgressFooter, ProgressFooterActions } from "../../../../get-compliant/ProgressFooter";

type Params = {
  dataSourceId: UUIDString;
};

export const CollectionGroupAssociationPage = () => {
  const history = useHistory();
  const orgId = usePrimaryOrganizationId();
  const { dataSourceId } = useParams<Params>();

  const data = useDataSourceAssociationForSource(orgId, dataSourceId, null);

  return (
    <StandardPageWrapper
      requests={[...Object.values(data.requests)]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextOnClick={() =>
                history.push(RouteHelpers.dataMap.dataSource.workflow.receipt(dataSourceId))
              }
              nextContent="Next"
              prevContent="Cancel"
              prevOnClick={() => history.push(Routes.dataMap.root)}
            />
          }
        />
      }
    >
      <PageHeader titleContent="Add Data Source" />

      <Typography>
        Which Collection Groups have information sourced from <strong>{data.source?.name}</strong>?
      </Typography>

      <CollectionGroupMultiSelect {...data.multiselect} />
    </StandardPageWrapper>
  );
};
