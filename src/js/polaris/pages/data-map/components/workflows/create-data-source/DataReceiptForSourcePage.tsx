import { Button, Typography } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { useHistory, useParams } from "react-router";
import { CollectionGroupDetailsDto } from "../../../../../../common/service/server/dto/CollectionGroupDto";
import { UUIDString } from "../../../../../../common/service/server/Types";
import { CollectionGroupCollapseCard } from "../../../../../components/CollectionGroupCollapseCard";
import { PageHeader } from "../../../../../components/layout/header/Header";
import { StandardPageWrapper } from "../../../../../components/layout/StandardPageWrapper";
import { DataRecipientSourceCheckboxes } from "../../../../../components/organisms/data-sources/DataRecipientSourceCheckboxes";
import { usePrimaryOrganizationId } from "../../../../../hooks";
import { useCollectionGroups } from "../../../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../../../hooks/useDataMap";
import { useOrgDataSource } from "../../../../../hooks/useOrganizationVendors";
import { RouteHelpers, Routes } from "../../../../../root/routes";
import { toggleSelection } from "../../../../../surveys/util";
import { ProgressFooter, ProgressFooterActions } from "../../../../get-compliant/ProgressFooter";

type Params = {
  dataSourceId: UUIDString;
};

const CollectionGroupReceiptCard = ({
  dataMap,
  collectionGroup,
  dataSource,
  collapsed,
  onExpand,
  onNext,
  setHasSelection,
}) => {
  const orgId = usePrimaryOrganizationId();

  return (
    <CollectionGroupCollapseCard
      className="mb-md"
      collectionGroup={collectionGroup}
      collapsed={collapsed}
      onExpand={onExpand}
    >
      <DataRecipientSourceCheckboxes
        source={dataSource}
        dataMap={dataMap}
        orgId={orgId}
        group={collectionGroup}
        includeSelectAll={dataSource.vendorKey === "source-consumer-directly"}
        setHasSelection={setHasSelection}
      />

      {Boolean(onNext) && (
        <Button variant="contained" onClick={onNext}>
          Next Group
        </Button>
      )}
    </CollectionGroupCollapseCard>
  );
};

export const DataReceiptForSourcePage = () => {
  const history = useHistory();
  const orgId = usePrimaryOrganizationId();
  const { dataSourceId } = useParams<Params>();

  const [dataMap, dataMapRequest] = useDataMap(orgId);
  const [dataSource, dataSourceRequest] = useOrgDataSource(orgId, dataSourceId);
  const [collectionGroups, collectionGroupsRequest] = useCollectionGroups(orgId);

  const [activeGroup, setActiveGroup] = useState<CollectionGroupDetailsDto>(null);

  const receipts = useMemo(
    () => dataMap?.received.filter((d) => d.vendorId == dataSource?.vendorId) || [],
    [dataMap?.received, dataSource],
  );

  const groupIds = useMemo(() => receipts.map((d) => d.collectionGroupId), [receipts]);

  const relevantGroups = useMemo(
    () => collectionGroups?.filter((r) => groupIds?.includes(r.id)),
    [collectionGroups, groupIds],
  );

  const handleExpand = (cg: CollectionGroupDetailsDto) => {
    setActiveGroup(cg);

    window.polaris.scrollSelectorIntoView(`#cg-${cg.id}`);
  };

  const [groupsWithSelection, setGroupsWithSelection] = useState([]);

  const allSelected = useMemo(
    () => relevantGroups.every((cg) => groupsWithSelection.includes(cg.id)),
    [relevantGroups, groupsWithSelection],
  );

  useEffect(() => {
    setActiveGroup((group) => {
      if (!group) {
        return relevantGroups[0];
      }
    });
  }, [relevantGroups]);

  return (
    <StandardPageWrapper
      requests={[collectionGroupsRequest, dataMapRequest, dataSourceRequest]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextOnClick={() => history.push(Routes.dataMap.root)}
              nextContent="Done"
              nextDisabled={!allSelected}
              prevOnClick={() =>
                history.push(
                  RouteHelpers.dataMap.dataSource.workflow.sourceAssociation(dataSourceId),
                )
              }
            />
          }
        />
      }
    >
      <PageHeader titleContent="Add Data Source" />

      <Typography className="mb-lg">
        For each of your Collection Groups, identify which categories of personal data are sourced
        from <strong>{dataSource?.name}</strong>.
      </Typography>

      {relevantGroups.map((cg, idx) => {
        let nextGroup: CollectionGroupDetailsDto | null = null;
        if (idx < relevantGroups.length - 1) {
          nextGroup = relevantGroups[idx + 1];
        }
        return (
          <div key={cg.id} id={`cg-${cg.id}`} className="mb-md">
            <CollectionGroupReceiptCard
              dataSource={dataSource}
              dataMap={dataMap}
              collectionGroup={cg}
              collapsed={activeGroup?.id !== cg.id}
              onExpand={() => handleExpand(cg)}
              onNext={nextGroup ? () => handleExpand(nextGroup) : undefined}
              setHasSelection={(hasSelection: boolean) =>
                setGroupsWithSelection((prev) => toggleSelection(prev, cg.id, hasSelection))
              }
            />
          </div>
        );
      })}
    </StandardPageWrapper>
  );
};
