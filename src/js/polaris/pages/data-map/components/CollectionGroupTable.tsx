import { Button, Typography } from "@mui/material";
import React, { useMemo } from "react";
import { NetworkRequestGate } from "../../../../common/components/NetworkRequestGate";
import {
  CellProps,
  ColumnDefinition,
  TableDefinition,
  TileTable,
} from "../../../../common/components/TileTable";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { RouteHelpers } from "../../../root/routes";
import {
  consumerGroupSorter,
  getIconForCollectionGroupType,
  getShortLabelForCollectionGroupType,
} from "../../../util/consumerGroups";

const NameCell: React.FC<CellProps<CollectionGroupDetailsDto>> = ({ data }) => (
  <span className="d-flex flex-row align-items-center">
    <span>{getIconForCollectionGroupType(data?.collectionGroupType)}</span>

    <div>
      <Typography>{data.name}</Typography>
      <Typography className="color-neutral-600" variant="caption">
        {getShortLabelForCollectionGroupType(data?.collectionGroupType)}
      </Typography>
    </div>
  </span>
);

const ActionCell: React.FC<CellProps<CollectionGroupDetailsDto>> = ({ data }) => (
  <Button href={RouteHelpers.dataMap.collectionGroup.detail(data.id)} color="secondary">
    View Details
  </Button>
);

export const COLLECTION_GROUP_COLUMNS: ColumnDefinition<CollectionGroupDetailsDto>[] = [
  {
    heading: null,
    component: NameCell,
  },
  {
    heading: null,
    cellClass: "text-right",
    component: ActionCell,
  },
];

export const CollectionGroupTable = () => {
  const orgId = usePrimaryOrganizationId();
  const [collectionGroups, collectionGroupRequest] = useCollectionGroups(orgId);

  const sortedCollectionGroups = useMemo<CollectionGroupDetailsDto[]>(
    () => collectionGroups?.sort(consumerGroupSorter) ?? [],
    [collectionGroups],
  );

  const table: TableDefinition<CollectionGroupDetailsDto> = {
    data: sortedCollectionGroups,
    columns: COLLECTION_GROUP_COLUMNS,
  };

  return (
    <NetworkRequestGate requests={[collectionGroupRequest]}>
      <TileTable variant="filled" table={table} />
    </NetworkRequestGate>
  );
};
