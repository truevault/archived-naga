import { Check } from "@mui/icons-material";
import { Typography } from "@mui/material";
import React from "react";
import { NetworkRequestGate } from "../../../../common/components/NetworkRequestGate";
import {
  CellProps,
  ColumnDefinition,
  TableDefinition,
  TileTable,
} from "../../../../common/components/TileTable";
import { ProcessingActivityDto } from "../../../../common/service/server/dto/ProcessingActivityDto";
import { usePrimaryOrganizationId } from "../../../hooks";
import {
  labelForLawfulBasis,
  useProcessingActivites,
} from "../../../hooks/useProcessingActivities";

const NameCell: React.FC<CellProps<ProcessingActivityDto>> = ({ data }) => (
  <span className="d-flex flex-row">
    <Check color="primary" />

    <div>
      <Typography>{data.name}</Typography>

      {data.lawfulBases.length > 0 && (
        <Typography className="color-neutral-600" variant="caption">
          {data.lawfulBases.map((l) => labelForLawfulBasis(l)).join(", ")}
        </Typography>
      )}
    </div>
  </span>
);

export const PROCESSING_ACTIVITY_COLUMNS: ColumnDefinition<ProcessingActivityDto>[] = [
  {
    heading: null,
    component: NameCell,
  },
];

export const LawfulBasesTable = () => {
  const orgId = usePrimaryOrganizationId();
  const [activities, activityRequest] = useProcessingActivites(orgId);

  const table: TableDefinition<ProcessingActivityDto> = {
    data: activities,
    columns: PROCESSING_ACTIVITY_COLUMNS,
  };

  return (
    <NetworkRequestGate requests={[activityRequest]}>
      <TileTable variant="filled" table={table} />
    </NetworkRequestGate>
  );
};
