import { Button } from "@mui/material";
import React, { useMemo, useState } from "react";
import { Organization } from "../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../../../common/service/server/dto/DataMapDto";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { CalloutVariant } from "../../../components/Callout";
import { DataRecipientCollapseCard } from "../../../components/DataRecipientCollapseCard";
import { SectionHeader } from "../../../components/molecules/SectionHeader";
import { EditableSourcesCard } from "../../../components/organisms/data-recipients/EditableSourcesCard";
import { useSourcingDataRecipients } from "../../../hooks/useOrganizationVendors";
import { alphabetically, toggleSelection } from "../../../surveys/util";
import { EditableDataRecipientSection } from "./EditableDataRecipientSection";

type DataSourcesTabProps = {
  handleRefresh: () => Promise<void>;
  dataMap: DataMapDto;
  collectionGroup: CollectionGroupDetailsDto;
  org: Organization;
  dataSources: OrganizationDataSourceDto[];
};

export const DataSourcesTab: React.FC<DataSourcesTabProps> = ({
  dataMap,
  collectionGroup,
  dataSources,
  handleRefresh,
}) => {
  const {
    data: { addingDataSources, sourcingDataSources, nonSourcingDataSources },
    actions: { setAddingDataRecipients },
  } = useEditDataSources(collectionGroup, dataMap, dataSources);

  const afterSave = async (dr: OrganizationDataRecipientDto) => {
    await handleRefresh();

    if (addingDataSources.some((ar) => ar.vendorId === dr.vendorId)) {
      setAddingDataRecipients((prev) => prev.filter((ar) => ar.vendorId !== dr.vendorId));
    }
  };

  return (
    <>
      <EditableDataRecipientSection
        title={<SectionHeader>You collect data from&hellip;</SectionHeader>}
        collectionGroup={collectionGroup}
        dataMap={dataMap}
        dataRecipients={sourcingDataSources}
        forceEditRecipients={addingDataSources}
        emptyText="No sources"
        render={(props) => (
          <EditableSourcesCard
            className="mb-sm"
            {...props}
            afterSave={() => afterSave(props.dataRecipient)}
          />
        )}
        className="mb-lg"
      />

      <section>
        <SectionHeader>
          Sources that are not associated with {collectionGroup.name}...
        </SectionHeader>

        {nonSourcingDataSources.map((dr) => {
          const handleAddDisclosures = () => {
            setAddingDataRecipients((r) => toggleSelection(r, dr, true));
            setTimeout(() => window.polaris.scrollSelectorIntoView(`#dr-${dr.vendorId}`), 150);
          };

          return (
            <DataRecipientCollapseCard
              className="mb-md"
              key={dr.vendorId}
              recipient={dr}
              collapsible={false}
              variant={CalloutVariant.LightGray}
              headerContent={
                <Button onClick={handleAddDisclosures} color="secondary">
                  Make this a Source
                </Button>
              }
            />
          );
        })}
      </section>
    </>
  );
};

type UseEditDataSources = {
  data: {
    sourcingDataSources: OrganizationDataSourceDto[];
    nonSourcingDataSources: OrganizationDataSourceDto[];
    addingDataSources: OrganizationDataSourceDto[];
  };
  actions: {
    setAddingDataRecipients: React.Dispatch<React.SetStateAction<OrganizationDataSourceDto[]>>;
  };
};

const useEditDataSources = (
  collectionGroup: CollectionGroupDetailsDto,
  dataMap: DataMapDto,
  dataSources: OrganizationDataSourceDto[],
): UseEditDataSources => {
  const [addingDataSources, setAddingDataRecipients] = useState<OrganizationDataSourceDto[]>([]);

  const [sourcingDataSources, nonSourcingDataSources] = useSourcingDataRecipients(
    collectionGroup,
    dataMap,
    dataSources,
  );

  const sortedSourcingDataRecipients = useMemo(
    () => [...sourcingDataSources, ...addingDataSources].sort(alphabetically),
    [sourcingDataSources, addingDataSources],
  );

  const sortedNonSourcingDataSources = useMemo(
    () =>
      nonSourcingDataSources.filter((dr) => !addingDataSources.includes(dr)).sort(alphabetically),
    [nonSourcingDataSources, addingDataSources],
  );

  return {
    data: {
      sourcingDataSources: sortedSourcingDataRecipients,
      nonSourcingDataSources: sortedNonSourcingDataSources,
      addingDataSources: addingDataSources,
    },
    actions: {
      setAddingDataRecipients,
    },
  };
};
