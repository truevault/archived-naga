import { Button, Typography } from "@mui/material";
import React from "react";
import {
  CellProps,
  ColumnDefinition,
  TableDefinition,
  TileTable,
} from "../../../../common/components/TileTable";
import { OrganizationDataSourceDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";

import { Loading } from "../../../components";
import { DataRecipientLogo } from "../../../components/DataRecipientLogo";
import { EmptyInfo } from "../../../components/Empty";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useOrgDataSources } from "../../../hooks/useOrganizationVendors";
import { RouteHelpers } from "../../../root/routes";
import { alphabetically } from "../../../surveys/util";

const NameCell: React.FC<CellProps<OrganizationDataSourceDto>> = ({ data }) => (
  <span className="d-flex flex-row align-items-center">
    <DataRecipientLogo className="mr-md" dataRecipient={data} size={32} />
    <Typography>{data.name}</Typography>
  </span>
);

const ActionCell: React.FC<CellProps<OrganizationDataSourceDto>> = ({ data }) => (
  <Button href={RouteHelpers.dataMap.dataSource.detail(data.vendorId)} color="secondary">
    View Details
  </Button>
);

export const DATA_SOURCE_COLUMNS: ColumnDefinition<OrganizationDataSourceDto>[] = [
  {
    heading: null,
    component: NameCell,
  },
  {
    heading: null,
    cellClass: "text-right",
    component: ActionCell,
  },
];

export const DataSourcesTable = () => {
  const orgId = usePrimaryOrganizationId();
  const [dataSources, dataSourceRequest] = useOrgDataSources(orgId);

  if (dataSourceRequest.running) {
    return <Loading />;
  }

  if (!dataSources || dataSources.length < 1) {
    return <EmptyInfo>No data sources</EmptyInfo>;
  }

  const sortedSources = dataSources.sort(alphabetically);

  const table: TableDefinition<OrganizationDataSourceDto> = {
    data: sortedSources,
    columns: DATA_SOURCE_COLUMNS,
  };

  return <TileTable variant="filled" table={table} />;
};
