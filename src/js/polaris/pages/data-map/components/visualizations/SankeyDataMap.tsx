import { Toolbar } from "@mui/material";
import _ from "lodash";
import React, { useEffect, useRef, useState } from "react";
import { Sankey } from "react-vis";
import { SimpleSelect } from "../../../../../common/components/input/Select";
import { DataMapDto } from "../../../../../common/service/server/dto/DataMapDto";

export const SankeyDataMap: React.FC<{ datamap: DataMapDto }> = ({ datamap }) => {
  const allCollectionGroups = datamap.collection
    .filter((c) => c.collectionGroupType !== "EMPLOYMENT")
    .map((c) => c.collectionGroupName);

  const ref = useRef(null);
  const [width, setWidth] = useState(0);

  useEffect(() => {
    setWidth(ref.current.offsetWidth);
  }, []);

  const [activeLink, setActiveLink] = useState(null);
  const [collectionGroupName, setCollectionGroupName] = useState(allCollectionGroups[0]);
  const categoryNodes = _.uniqBy(
    datamap.categories
      .filter((c) => {
        return (
          datamap.disclosure
            .filter((d) => d.collectionGroupName === collectionGroupName)
            .some((dd) => dd.disclosed.includes(c.id)) ||
          datamap.collection
            .filter((c) => c.collectionGroupName === collectionGroupName)
            .some((cc) => cc.collected.includes(c.id))
        );
      })
      .map((c) => ({ name: c.picGroupName, key: c.picGroupKey, color: "#BBADEB" })),
    "key",
  );

  const collectionGroupNodes = datamap.collection
    .filter((c) => c.collectionGroupName === collectionGroupName)
    .map((c) => ({
      name: c.collectionGroupName,
      key: c.collectionGroupId,
      color: "#BBADEB",
    }));

  const vendorCategoryNodes = _.uniqBy(
    datamap.disclosure
      .filter((d) => d.collectionGroupName === collectionGroupName)
      .filter((d) => d.disclosed.length > 0)
      .map((d) => ({
        name: d.vendorCategory,
        key: d.vendorCategory,
        color: "#BBADEB",
      })),
    "key",
  );

  const dmNodes = [...categoryNodes, ...collectionGroupNodes, ...vendorCategoryNodes];

  const collectionLinks = _.flatMap(
    datamap.collection.filter((c) => c.collectionGroupName === collectionGroupName),
    (c) =>
      c.collected.map((cc) => {
        const pic = datamap.categories.find((cat) => cat.id === cc);

        return {
          source: _.findIndex(dmNodes, (n) => n.key === c.collectionGroupId),
          target: _.findIndex(dmNodes, (n) => n.key === pic.picGroupKey),
          color: "#e8e5ee",
          value: c.collected.length,
        };
      }),
  ).filter((l) => l.source >= 0 && l.target >= 0);

  const disclosureLinks = _.flatMap(
    datamap.disclosure.filter((d) => d.disclosed.length > 0),
    (d) => {
      return d.disclosed.map((dd) => {
        const pic = datamap.categories.find((cat) => cat.id === dd);

        return {
          source: _.findIndex(dmNodes, (n) => n.key === pic.picGroupKey),
          target: _.findIndex(dmNodes, (n) => n.key === d.vendorCategory),
          color: "#e8e5ee",
          value: d.disclosed.length,
        };
      });
    },
  ).filter((l) => l.source >= 0 && l.target >= 0);

  const uniqueCollectionLinks = _.uniqBy(collectionLinks, (l) => `${l.source}-${l.target}`);
  const uniqueDisclosureLinks = _.uniqBy(disclosureLinks, (l) => `${l.source}-${l.target}`);

  const links = [...uniqueCollectionLinks, ...uniqueDisclosureLinks];
  const collectionGroupOptions = allCollectionGroups.map((cg) => ({ value: cg, label: cg }));
  return (
    <div ref={ref}>
      <Toolbar className="mb-md">
        <SimpleSelect
          className="ml-auto"
          size="small"
          options={collectionGroupOptions}
          value={collectionGroupName}
          onChange={(e) => setCollectionGroupName(e.target.value as string)}
        />
      </Toolbar>

      {width > 0 && (
        <Sankey
          animation
          align={"center"}
          nodes={dmNodes}
          links={links.map((link, i) => {
            return {
              ...link,
              color: activeLink && i === activeLink.index ? "#BBADEB" : "#e8e5ee",
            };
          })}
          width={width}
          height={400}
          layout={24}
          style={{
            links: {
              opacity: 0.1,
            },
            labels: {
              fontFamily: "Roboto",
              fontWeight: "bold",
              fontSize: 11,
            },
          }}
          onLinkMouseOver={(node) => setActiveLink(node)}
          onLinkMouseOut={() => setActiveLink(null)}
        />
      )}
    </div>
  );
};
