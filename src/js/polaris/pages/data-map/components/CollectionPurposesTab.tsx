import { ShoppingCart } from "@mui/icons-material";
import { Stack } from "@mui/material";
import React, { useState } from "react";
import { Organization } from "../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { EditTab } from "./EditTab";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import {
  collectsCommercialInfoSlug,
  EMPLOYEE_COLLECTION_PURPOSES_SURVEY,
  EmploymentGroupPurposesQuestion,
} from "../../../components/organisms/collection-groups/EmploymentGroupPurposesQuestion";

export const CollectionPurposesTab: React.FC<{
  org: Organization;
  collectionGroup: CollectionGroupDetailsDto;
  refreshGroup: () => Promise<void>;
}> = ({ org, collectionGroup, refreshGroup }) => {
  const [editMode, setEditMode] = useState(false);

  const { answers } = useSurvey(org.id, EMPLOYEE_COLLECTION_PURPOSES_SURVEY);
  const commercialInfoSlug = collectsCommercialInfoSlug(collectionGroup);
  const usesInfoForCommercialPurposes = answers[commercialInfoSlug] === "true";

  const enableEditMode = () => {
    setEditMode(true);
  };

  const disableEditMode = async () => {
    await refreshGroup();
    setEditMode(false);
  };

  return (
    <EditTab
      isEditing={editMode}
      onEnableEditing={enableEditMode}
      onDisableEditing={disableEditMode}
    >
      {editMode ? (
        <Stack spacing={3}>
          <EmploymentGroupPurposesQuestion employeeGroup={collectionGroup} />
        </Stack>
      ) : (
        <Stack spacing={3}>
          <Callout
            variant={CalloutVariant.LightPurple}
            icon={<ShoppingCart color="primary" />}
            title={
              <CommercialLabel
                org={org}
                collectionGroup={collectionGroup}
                usesInfoForCommercialPurposes={usesInfoForCommercialPurposes}
              />
            }
          />
        </Stack>
      )}
    </EditTab>
  );
};

const CommercialLabel: React.FC<{
  org: Organization;
  collectionGroup: CollectionGroupDetailsDto;
  usesInfoForCommercialPurposes: boolean;
}> = ({ org, collectionGroup, usesInfoForCommercialPurposes }) => {
  return (
    <>
      <strong>{org.name}</strong>{" "}
      <strong>{usesInfoForCommercialPurposes ? "uses" : "does not use"}</strong> information about{" "}
      <strong>{collectionGroup.name}</strong> for commercial purposes
    </>
  );
};
