import React from "react";
import { useHistory } from "react-router";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { VendorSearchContents } from "../../components/VendorSearchContents";
import { usePrimaryOrganizationId } from "../../hooks";
import { useDataSourceSearch } from "../../hooks/useOrganizationVendors";
import { RouteHelpers } from "../../root/routes";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

export const DataSourceNewPage = () => {
  const history = useHistory();
  const orgId = usePrimaryOrganizationId();

  const { actions, requests, data } = useDataSourceSearch(orgId, {
    onAddSuccess: (source) =>
      history.push(RouteHelpers.dataMap.dataSource.workflow.sourceAssociation(source.vendorId)),
    onAddCustomSuccess: (source) =>
      history.push(RouteHelpers.dataMap.dataSource.workflow.sourceAssociation(source.vendorId)),
  });

  return (
    <StandardPageWrapper
      requests={[requests.catalogRequest, requests.orgDataSourcesRequest]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              hideNext={true}
              prevContent="Back"
              prevDisabled={requests.addingVendor.running || requests.addingCustomVendor.running}
              prevOnClick={() => history.goBack()}
            />
          }
        />
      }
    >
      <PageHeader titleContent={"Add Data Source"} />

      <VendorSearchContents
        allVendorsMap={data.allVendorsMap}
        enabledVendors={data.enabledVendors}
        defaultVendors={data.catalog.slice(0, 8)}
        headingLabel="Search for a data source..."
        type="source"
        context={["CONSUMER"]}
        handleAddVendor={actions.handleAddVendor}
        handleAddCustomVendor={actions.handleAddCustomVendor}
        handleRemoveVendor={actions.handleRemoveVendor}
        variant="radio"
        customModal={{
          header: "Add Custom Data Source",
          confirmLabel: "Add Custom Data Source",
          includeUrl: false,
        }}
      />
    </StandardPageWrapper>
  );
};
