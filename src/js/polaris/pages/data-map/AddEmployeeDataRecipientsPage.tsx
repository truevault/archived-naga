import { Button, Divider, Stack } from "@mui/material";
import React from "react";
import { CollectionContext } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { EmptyInfo } from "../../components/Empty";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { VendorList } from "../../components/molecules/vendor/VendorList";
import { VendorSearchPopup } from "../../components/organisms/vendor/AddVendorPopup";
import { CategorizedVendorInventory } from "../../components/organisms/vendor/CategorizedVendorInventory";
import { KNOWN_EMPLOYMENT_VENDOR_CATEGORIES } from "../../components/organisms/vendor/VendorCategories";
import { VendorSearchBox } from "../../components/organisms/vendor/VendorSearchBox";
import { usePrimaryOrganizationId } from "../../hooks";
import { useTasks } from "../../hooks/useTasks";
import { Routes } from "../../root/routes";
import { useUpdateDataSources } from "../get-compliant/data-map/ConsumerDataSourcesPage";
import { useUpdateDataRecipients } from "../get-compliant/data-recipients/AddDataRecipients";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

const CONSUMER_CONTEXT: CollectionContext[] = ["EMPLOYEE"];

export const AddEmployeeDataRecipientsPage = () => {
  const orgId = usePrimaryOrganizationId();

  const updateDataRecipients = useUpdateDataRecipients(orgId, {
    collectionContext: CONSUMER_CONTEXT,
  });

  const { requests } = updateDataRecipients;

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const updateDataSources = useUpdateDataSources(orgId, { collectionContext: CONSUMER_CONTEXT });

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const [tasks, tasksRequest, _refreshTasks, completeTask] = useTasks(orgId);
  const addEmployeeVendorTask = tasks?.find((t) => t.taskKey === "ADD_EMPLOYMENT_DATA_RECIPIENTS");

  const handleDone = async () => {
    if (addEmployeeVendorTask) {
      await completeTask(addEmployeeVendorTask);
    }

    window.location.href = `${Routes.dataMap.root}?tab=data_recipients&finish_employment_modal=true`;
  };

  const hasRecipients = updateDataRecipients.enabledVendors.size > 0;
  const hasSources = updateDataSources.data.sources.length > 0;
  const allDone = hasRecipients && hasSources;

  return (
    <StandardPageWrapper
      requests={[tasksRequest, requests.vendorsRequest, requests.allVendorsRequest]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextContent="Done"
              nextOnClick={handleDone}
              nextDisabled={!allDone}
            />
          }
        />
      }
    >
      <PageHeader
        titleContent="Add Employment Data Recipients & Sources"
        descriptionContent={
          <p>
            Search our database to find your remaining Data Recipients. Create a Custom Recipient
            for any you can’t find.
          </p>
        }
      />

      <Stack spacing={3}>
        <VendorSearchBox {...updateDataRecipients} />

        <CategorizedVendorInventory
          {...updateDataRecipients}
          categories={KNOWN_EMPLOYMENT_VENDOR_CATEGORIES}
          orgVendors={updateDataRecipients.currentVendors}
        />

        <Divider />

        <Stack spacing={3}>
          <header>
            <h4>Employment Data Sources</h4>
            <p>
              Use the input below to locate your third-party data sources for your employees,
              contractors, or job applicants.
            </p>
          </header>

          <div className="w-512 my-lg">
            <VendorList
              vendors={updateDataSources.data.sources}
              onRemove={updateDataSources.actions.handleRemoveVendor}
              empty={<EmptyInfo>No sources added</EmptyInfo>}
            />
          </div>

          <div>
            <Button variant="contained" size="small" onClick={handleClick}>
              Add Source
            </Button>
          </div>

          <VendorSearchPopup
            open={open}
            onClose={() => setAnchorEl(null)}
            el={anchorEl}
            vendors={updateDataSources.data.remainingCatalog}
            onAddVendor={updateDataSources.actions.handleAddVendor}
            onAddCustomVendor={updateDataSources.actions.handleAddCustomVendor}
            customModal={{
              header: "Add Source",
              confirmLabel: "Add Source",
              includeUrl: false,
            }}
          />
        </Stack>
      </Stack>
    </StandardPageWrapper>
  );
};
