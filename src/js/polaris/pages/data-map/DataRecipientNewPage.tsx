import React from "react";
import { useHistory } from "react-router";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { VendorSearchContents } from "../../components/VendorSearchContents";
import { usePrimaryOrganizationId } from "../../hooks";
import { useDataRecipientSearch, useOrganizationVendors } from "../../hooks/useOrganizationVendors";
import { RouteHelpers } from "../../root/routes";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";

export const DataRecipientNewPage = () => {
  const history = useHistory();
  const orgId = usePrimaryOrganizationId();

  const [, , refreshOrgVendors] = useOrganizationVendors(orgId, undefined, true);

  const { actions, requests, data } = useDataRecipientSearch(orgId, {
    onAddSuccess: (dr: OrganizationDataRecipientDto) => {
      history.push(RouteHelpers.dataMap.dataRecipient.workflow.groupAssociation(dr.vendorId));
    },
    onAddCustomSuccess: async (dr: OrganizationDataRecipientDto) => {
      await refreshOrgVendors();
      history.push(RouteHelpers.dataMap.dataRecipient.workflow.groupAssociation(dr.vendorId));
    },
    onAddContractorSuccess: async (dr: OrganizationDataRecipientDto) => {
      await refreshOrgVendors();
      history.push(RouteHelpers.dataMap.dataRecipient.workflow.groupAssociation(dr.vendorId));
    },
    onAddThirdParyRecipientSuccess: async (dr: OrganizationDataRecipientDto) => {
      await refreshOrgVendors();
      history.push(RouteHelpers.dataMap.dataRecipient.workflow.groupAssociation(dr.vendorId));
    },
  });

  return (
    <StandardPageWrapper
      requests={[requests.catalogRequest, requests.orgDataRecipientsRequest]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              hideNext={true}
              prevContent="Back"
              prevDisabled={requests.addingVendor.running || requests.addingCustomVendor.running}
              prevOnClick={() => history.goBack()}
            />
          }
        />
      }
    >
      <PageHeader titleContent={"Add Data Recipient"} />

      <VendorSearchContents
        allVendorsMap={data.allVendorsMap}
        enabledVendors={data.enabledVendors}
        defaultVendors={data.catalog.slice(0, 8)}
        type="vendor"
        context={["CONSUMER"]}
        handleAddVendor={actions.handleAddVendor}
        handleAddCustomVendor={actions.handleAddCustomVendor}
        handleAddContractor={actions.handleAddContractor}
        handleAddThirdParyRecipient={actions.handleAddThirdParyRecipient}
        showCustomVendorControls={true}
        variant="radio"
      />
    </StandardPageWrapper>
  );
};
