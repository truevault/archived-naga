import { DeleteForever, Edit } from "@mui/icons-material";
import { Button } from "@mui/material";
import _ from "lodash";
import React, { useMemo, useState } from "react";
import { useHistory, useParams } from "react-router";
import { VendorDto } from "../../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../../common/service/server/Types";
import { TabPanel } from "../../components";
import { DataRecipientLogo } from "../../components/DataRecipientLogo";
import { ConfirmDialog } from "../../components/dialog";
import { PageHeader } from "../../components/layout/header/Header";
import { PageHeaderBack } from "../../components/layout/header/PageHeaderBack";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { useOrganizationVendorDetails, useTabs } from "../../hooks";
import { useDataMap } from "../../hooks/useDataMap";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { useDataRecipientSearch } from "../../hooks/useOrganizationVendors";
import { TabOptions } from "../../hooks/useTabs";
import { Routes } from "../../root/routes";
import { isThirdPartyRecipient } from "../../util/vendors";
import {
  CustomVendorDialog,
  CustomVendorFormValues,
} from "../get-compliant/data-recipients/AddDataRecipients";
import { CollectionGroupsTab } from "./components/data-recipients/CollectionGroupsTab";
import { DataDisclosureSettingsTab } from "./components/data-recipients/DataDisclosureSettingsTab";
import { DataRetentionSettingsTab } from "./components/data-recipients/DataRetentionSettingsTab";
import { InstalledAppsSettingTab } from "./components/data-recipients/InstalledAppsSettingsTab";

type DataRecipientDetailPageTabOptions =
  | "data_disclosure"
  | "data_retention"
  | "collection_groups"
  | "installed_apps";

const useTabOptions = (
  showInstalledApps: boolean,
  isStorageLocation: boolean,
): TabOptions<DataRecipientDetailPageTabOptions> => {
  return _.compact([
    !isStorageLocation
      ? {
          label: "Data Disclosure",
          value: "data_disclosure",
        }
      : undefined,
    {
      label: "Data Retention",
      value: "data_retention",
    },
    showInstalledApps && !isStorageLocation
      ? {
          label: "Installed Apps",
          value: "installed_apps",
        }
      : undefined,
    !isStorageLocation
      ? {
          label: "Collection Groups",
          value: "collection_groups",
        }
      : undefined,
  ]);
};

type Params = {
  dataRecipientId: UUIDString;
};

export const DataRecipientDetailPage = () => {
  const { dataRecipientId } = useParams<Params>();
  const orgId = usePrimaryOrganizationId();
  const history = useHistory();
  const [org, orgRequest] = usePrimaryOrganization();

  const [deleteConfirmOpen, setDeleteConfirmOpen] = useState(false);
  const [editDialogOpen, setEditDialogOpen] = useState(false);

  const [dataMap, dataMapRequest] = useDataMap(orgId);

  const [dataRecipient, dataRecipientRequest, refreshDataRecipient] = useOrganizationVendorDetails(
    orgId,
    dataRecipientId,
  );

  const { actions, requests, data } = useDataRecipientSearch(orgId, {
    onRemoveSuccess: () => {
      setDeleteConfirmOpen(false);
      history.push(Routes.dataMap.root);
    },
    onEditSuccess: () => {
      setEditDialogOpen(false);
      refreshDataRecipient();
    },
  });

  const showInstalledApps = useMemo(
    () =>
      dataRecipient &&
      dataRecipient.isPlatform &&
      !dataRecipient.isStandalone &&
      !isThirdPartyRecipient(dataRecipient),
    [dataRecipient],
  );

  const { handleRemoveVendor, handleUpdateVendor } = actions;
  const { removingVendor } = requests;

  const cats = Array.from(data?.allVendorsMap.values())
    .map(({ category }) => category)
    .filter((x) => x);

  const isStorageLocation = dataRecipient?.dataRecipientType === "storage_location";
  const defaultTab = isStorageLocation ? "data_retention" : "data_disclosure";
  const tabOptions = useTabOptions(showInstalledApps ?? false, isStorageLocation);

  const [tab, tabs] = useTabs<DataRecipientDetailPageTabOptions>(tabOptions, defaultTab);
  const contexts = dataRecipient?.collectionContext ?? [];

  const deleteConfirmCopy =
    contexts.length <= 1 ? (
      "Are you sure you want to delete this data recipient? Any information you've mapped to this data recipient will be lost."
    ) : (
      <>
        Are you sure you want to delete this Data Recipient? Clicking ‘yes’ will remove the Data
        Recipient from both your consumer group{" "}
        <strong>
          <em>and</em>
        </strong>{" "}
        your employment groups. If you have questions, contact{" "}
        <a href="mailto:help@truevault.com">help@truevault.com</a>.
      </>
    );

  return (
    <StandardPageWrapper
      requests={[
        dataRecipientRequest,
        orgRequest,
        dataMapRequest,
        requests.orgDataRecipientsRequest,
        requests.catalogRequest,
      ]}
    >
      <PageHeader
        titleContent={dataRecipient?.name}
        titleHeaderContent={
          <PageHeaderBack to={`${Routes.dataMap.root}?tab=data_recipients`}>
            Back to Data Map
          </PageHeaderBack>
        }
        logo={<DataRecipientLogo dataRecipient={dataRecipient} size={48} />}
        actions={
          <>
            {dataRecipient?.isCustom && (
              <Button
                onClick={() => setEditDialogOpen(true)}
                color="secondary"
                className="mr-sm"
                startIcon={<Edit />}
                variant="text"
              >
                Edit
              </Button>
            )}

            <Button
              onClick={() => setDeleteConfirmOpen(true)}
              color="error"
              startIcon={<DeleteForever />}
              variant="text"
            >
              Remove
            </Button>
          </>
        }
      />

      {tabs}

      <TabPanel tab={tab} value="data_disclosure" className="py-lg">
        <DataDisclosureSettingsTab
          org={org}
          dataRecipient={dataRecipient}
          handleRefresh={async () => refreshDataRecipient()}
        />
      </TabPanel>

      <TabPanel tab={tab} value="data_retention" className="py-lg">
        <DataRetentionSettingsTab
          org={org}
          dataRecipient={dataRecipient}
          handleRefresh={async () => refreshDataRecipient()}
        />
      </TabPanel>

      {showInstalledApps && (
        <TabPanel tab={tab} value="installed_apps" className="py-lg">
          <InstalledAppsSettingTab
            org={org}
            dataRecipient={dataRecipient}
            handleRefresh={async () => refreshDataRecipient()}
          />
        </TabPanel>
      )}

      <TabPanel tab={tab} value="collection_groups" className="py-lg">
        <CollectionGroupsTab org={org} dataRecipient={dataRecipient} dataMap={dataMap} />
      </TabPanel>

      <ConfirmDialog
        open={deleteConfirmOpen}
        onClose={() => setDeleteConfirmOpen(false)}
        onConfirm={() => handleRemoveVendor(dataRecipient)}
        confirmTitle={`Delete ${dataRecipient?.name}?`}
        contentText={deleteConfirmCopy}
        confirmText="Yes"
        confirmLoading={removingVendor.running}
      />

      <CustomVendorDialog
        open={editDialogOpen}
        vendor={dataRecipient}
        onClose={() => setEditDialogOpen(false)}
        onDone={(ov: CustomVendorFormValues) => {
          handleUpdateVendor({ dataRecipient, values: ov });
        }}
        enabledVendors={
          Array.from(data.enabledVendors)
            .map((id) => data.allVendorsMap.get(id))
            .filter((x) => x) as VendorDto[]
        }
        existingCategories={new Set(cats)}
      />
    </StandardPageWrapper>
  );
};
