import React, { useState } from "react";
import { Error as OrganizationError } from "../../copy/organization";
import { useHistory, useParams } from "react-router";
import { Api } from "../../../common/service/Api";
import { UUIDString } from "../../../common/service/server/Types";
import { TabPanel } from "../../components";
import { ConfirmDialog } from "../../components/dialog";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import {
  useOrganizationVendors,
  usePrimaryOrganization,
  usePrimaryOrganizationId,
} from "../../hooks";
import { useCollectionGroup, useCollectionGroups } from "../../hooks/useCollectionGroups";
import { useDataMap } from "../../hooks/useDataMap";
import { useOrgDataSources } from "../../hooks/useOrganizationVendors";
import { TabOptions, useQueryTabs } from "../../hooks/useTabs";
import { CollectedPicTab } from "./components/CollectedPicTab";
import { DataDisclosuresTab } from "./components/DataDisclosuresTab";
import { DataSourcesTab } from "./components/DataSourcesTab";
import { CollectionPurposesTab } from "./components/CollectionPurposesTab";
import { Routes } from "../../root/routes";
import { useActionRequest } from "../../../common/hooks/api";
import { CollectionGroupDetailsDto } from "../../../common/service/server/dto/CollectionGroupDto";
import { Button } from "@mui/material";
import { DeleteForever, Edit } from "@mui/icons-material";
import { AddOrEditConsumerGroupDialog } from "../get-compliant/collection-groups/AddOrEditConsumerGroupDialog";
import { isEmploymentGroup } from "../../util/consumerGroups";
import { PageHeaderBack } from "../../components/layout/header/PageHeaderBack";

type CollectionGroupDetailTabOptions =
  | "data_collected"
  | "data_recipients"
  | "data_sources"
  | "collection_purposes";

const TAB_OPTIONS: TabOptions<CollectionGroupDetailTabOptions> = [
  {
    label: "Data Collected",
    value: "data_collected",
  },
  {
    label: "Data Recipients",
    value: "data_recipients",
  },
  {
    label: "Data Sources",
    value: "data_sources",
  },
  {
    label: "Collection Purposes",
    value: "collection_purposes",
  },
];

type CollectionGroupDetailPageParams = {
  dataSubjectTypeId: UUIDString;
};

export const CollectionGroupDetailPage = () => {
  const { dataSubjectTypeId } = useParams<CollectionGroupDetailPageParams>();
  const orgId = usePrimaryOrganizationId();
  const history = useHistory();
  const [org, orgRequest] = usePrimaryOrganization();

  const [deleteConfirmOpen, setDeleteConfirmOpen] = useState(false);
  const [editDialogOpen, setEditDialogOpen] = useState(false);

  const [dataMap, dataMapRequest, refreshDataMap] = useDataMap(orgId);
  const [collectionGroup, collectionGroupRequest, refreshGroup] = useCollectionGroup(
    orgId,
    dataSubjectTypeId,
  );
  const isEmployment = isEmploymentGroup(collectionGroup);
  const [orgDataRecipients, orgDataRecipientsRequest, refreshOrgDataRecipients] =
    useOrganizationVendors(
      orgId,
      undefined,
      undefined,
      undefined,
      isEmployment ? ["EMPLOYEE"] : ["CONSUMER"],
    );

  const [collectionGroups, collectionGroupsRequest, refreshAllGroups] = useCollectionGroups(orgId);

  const filteredTabOptions = isEmployment
    ? TAB_OPTIONS
    : TAB_OPTIONS.filter((x) => x.value !== "collection_purposes");
  const [tab, tabs] = useQueryTabs<CollectionGroupDetailTabOptions>(
    filteredTabOptions,
    "data_collected",
  );

  const { fetch: removeGroup, request: removeGroupRequest } = useActionRequest({
    api: (collectionGroup: CollectionGroupDetailsDto) =>
      Api.consumerGroups.deleteDataSubjectType(orgId, collectionGroup.id),
    messages: {
      forceError: OrganizationError.RemoveDataSubjectType,
    },
    onSuccess: () => {
      setDeleteConfirmOpen(false);
      history.push(Routes.dataMap.root);
    },
  });

  const [orgDataSources, orgDataSourcesRequest] = useOrgDataSources(orgId);

  const handleRefresh = async () => {
    await Promise.all([refreshGroup(), refreshDataMap(), refreshOrgDataRecipients()]);
  };

  return (
    <StandardPageWrapper
      requests={[
        collectionGroupsRequest,
        collectionGroupRequest,
        dataMapRequest,
        orgRequest,
        orgDataRecipientsRequest,
        orgDataSourcesRequest,
      ]}
    >
      <PageHeader
        titleContent={collectionGroup?.name}
        titleHeaderContent={
          <PageHeaderBack to={`${Routes.dataMap.root}?tab=collection_groups`}>
            Back to Data Map
          </PageHeaderBack>
        }
        actions={
          <>
            <Button
              onClick={() => setEditDialogOpen(true)}
              color="secondary"
              className="mr-sm"
              startIcon={<Edit />}
              variant="text"
            >
              Edit Name
            </Button>

            <Button
              onClick={() => setDeleteConfirmOpen(true)}
              color="error"
              startIcon={<DeleteForever />}
              variant="text"
            >
              Remove
            </Button>
          </>
        }
      />

      {tabs}

      <TabPanel tab={tab} value="data_collected" className="py-lg">
        <CollectedPicTab
          refreshGroup={handleRefresh}
          org={org}
          dataMap={dataMap}
          collectionGroup={collectionGroup}
        />
      </TabPanel>

      <TabPanel tab={tab} value="data_recipients" className="py-lg">
        <DataDisclosuresTab
          org={org}
          dataMap={dataMap}
          collectionGroup={collectionGroup}
          dataRecipients={orgDataRecipients}
          handleRefresh={handleRefresh}
        />
      </TabPanel>

      <TabPanel tab={tab} value="data_sources" className="py-lg">
        <DataSourcesTab
          org={org}
          dataMap={dataMap}
          collectionGroup={collectionGroup}
          dataSources={orgDataSources}
          handleRefresh={handleRefresh}
        />
      </TabPanel>

      <TabPanel tab={tab} value="collection_purposes" className="py-lg">
        <CollectionPurposesTab
          refreshGroup={handleRefresh}
          org={org}
          collectionGroup={collectionGroup}
        />
      </TabPanel>

      <ConfirmDialog
        open={deleteConfirmOpen}
        onClose={() => setDeleteConfirmOpen(false)}
        onConfirm={() => removeGroup(collectionGroup)}
        confirmTitle={`Delete ${collectionGroup?.name}?`}
        contentText={
          "Are you sure you want to delete this consumer group? " +
          "Any information you've mapped to this group will be lost."
        }
        confirmText="Yes"
        confirmLoading={removeGroupRequest.running}
      />

      <AddOrEditConsumerGroupDialog
        open={editDialogOpen}
        onClose={() => setEditDialogOpen(false)}
        onDone={() => {
          setEditDialogOpen(false);
          refreshGroup();
          refreshAllGroups();
        }}
        editGroup={collectionGroup}
        collectionGroupType={collectionGroup?.collectionGroupType}
        existingGroups={collectionGroups}
      />
    </StandardPageWrapper>
  );
};
