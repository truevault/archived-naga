import { Button, Divider } from "@mui/material";
import React, { useEffect, useState } from "react";
import { NetworkRequest } from "../../../common/models";
import { ApiRouteHelpers, formatUrl } from "../../../common/root/apiRoutes";
import { DataRequestEventCard } from "../../components/dataRequest/DataRequestEventCard";
import { Empty } from "../../components/Empty";
import { PageHeader } from "../../components/layout/header/Header";
import { PageHeaderBack } from "../../components/layout/header/PageHeaderBack";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { usePrimaryOrganization } from "../../hooks";
import { useQuery } from "../../hooks/useQuery";
import { useRequestsForConsumer } from "../../hooks/useRequests";
import { Routes } from "../../root/routes";

type Props = {};

type ConsumerDetailPageParams = {
  consumerId: string;
};

export const ConsumerDetailPage: React.FC<Props> = () => {
  const { consumerId } = useQuery<ConsumerDetailPageParams>();
  const [everRendered, setEverRendered] = useState(false);
  const [org, orgRequest] = usePrimaryOrganization();

  const [requests, requestsRequest] = useRequestsForConsumer(org.id, consumerId);

  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest, requestsRequest)) {
      setEverRendered(true);
    }
  }, [orgRequest, requestsRequest]);

  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  if (!consumerId) {
    return (
      <StandardPageWrapper>
        <PageHeader
          className="mb-lg"
          titleContent="No Consumer"
          titleHeaderContent={
            <PageHeaderBack to={Routes.records.lookup}>Back to Lookup</PageHeaderBack>
          }
        />
      </StandardPageWrapper>
    );
  }

  const hasRequests = requests.length > 0;

  return (
    <StandardPageWrapper>
      <PageHeader
        className="mb-lg"
        titleContent={consumerId}
        titleHeaderContent={
          <PageHeaderBack to={Routes.records.lookup}>Back to Lookup</PageHeaderBack>
        }
        descriptionContent={
          consumerId ? (
            <Button
              variant="contained"
              href={formatUrl(ApiRouteHelpers.consumer.export(org.id), {
                subjectEmailAddress: consumerId,
              })}
              target="_blank"
            >
              Export to PDF
            </Button>
          ) : null
        }
      />

      {hasRequests ? (
        <>
          {requests.map((r) => (
            <React.Fragment key={r.id}>
              <DataRequestEventCard startOpen request={r} />
              <Divider className="mt-md mb-xl" />
            </React.Fragment>
          ))}
        </>
      ) : (
        <Empty>No requests for {consumerId}</Empty>
      )}
    </StandardPageWrapper>
  );
};
