import React, { useEffect, useState } from "react";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { NetworkRequest } from "../../../common/models";
import { Loading } from "../../../common/components/Loading";
import {
  SurveyQuestion,
  SurveyQuestionProps,
} from "../../components/get-compliant/survey/SurveyQuestion";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { usePrimaryOrganizationId } from "../../hooks";
import { prepareSurvey, Survey } from "../../surveys/survey";
import { GetCompliantFooter } from "./ProgressFooter";
import { DATA_RETENTION_SURVEY_NAME } from "../../components/organisms/data-retention/DataRetentionModule";

export const CookiePolicyPage: React.FC = () => {
  const organizationId = usePrimaryOrganizationId();
  const [readyToRender, setReadyToRender] = useState(false);

  const { answers, setAnswer, surveyRequest } = useSurvey(
    organizationId,
    DATA_RETENTION_SURVEY_NAME,
  );

  useEffect(() => {
    if (NetworkRequest.areFinished(surveyRequest)) {
      setReadyToRender(true);
    }
  }, [surveyRequest]);

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  const survey = calculateSurvey(answers);
  const questions = prepareSurvey(survey, answers, true);

  const handleNext = async () => {
    await setAnswer("cookie-policy-done", "true");
  };

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRetention.CookiePolicy"
          nextLabel="Done"
          nextDisabledTooltip="Please make a selection for each Data Recipient before proceeding."
          nextOnClick={handleNext}
        />
      }
    >
      <PageHeader
        titleContent="Cookie Policy"
        descriptionContent="We’ll include a short paragraph in your Privacy Policy about how your business uses cookies and how consumers can control them. We’ll use the default language below. If needed, update the text to accurately describe how your website uses cookies."
      />

      <div className="mt-xl">
        {questions.map((q, i) => {
          return (
            <div className="mb-xl" key={i}>
              <SurveyQuestion
                key={q.slug}
                question={q}
                answer={answers[q.slug]}
                onAnswer={(ans) =>
                  setAnswer(q.slug, ans, q.plaintextQuestion ?? (q.question as string))
                }
              />
            </div>
          );
        })}
      </div>
    </GetCompliantPageWrapper>
  );
};

const questions = (): SurveyQuestionProps[] => {
  return [
    {
      type: "freetext",
      slug: "cookie-policy",
      question: "",
      rows: 5,
    },
  ];
};

const visibility = (_answers: Record<string, string>): Record<string, boolean> => {
  return {
    "cookie-policy": true,
  };
};

const calculateSurvey = (answers: Record<string, string>): Survey => {
  return {
    questions: questions(),
    visibility: visibility(answers),
    disabled: {},
  };
};
