import React from "react";
import { GetCompliantStep } from "../../../../common/service/server/Types";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { SimpleBooleanQuestion } from "../../../components/organisms/SimpleBooleanQuestion";
import { usePrivacyCenterSettings } from "../../../components/superorg/settings/PrivacyCenterSettings";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { usePrimaryOrganization } from "../../../hooks";
import { Routes } from "../../../root/routes";
import { GetCompliantFooter } from "../ProgressFooter";

const BASE_IMAGE_URL = "/assets/images/get_compliant/notice/";

const getHelpImageFilename = (
  selling: boolean,
  sharing: boolean,
  longForm: boolean,
  disableCaNotice: boolean,
) => {
  let sellingSharingPart = "sh";
  let lengthPart = longForm ? "lf" : "sf";
  let caNoticePart = disableCaNotice ? "nca" : "ca";

  if (selling && sharing) {
    sellingSharingPart = "ss";
  } else if (selling) {
    sellingSharingPart = "se";
  } else if (sharing) {
    sellingSharingPart = "sh";
  }

  return `${BASE_IMAGE_URL}${sellingSharingPart}${lengthPart}${caNoticePart}.png`;
};

type VendorsProps = {
  currentProgress: GetCompliantStep;
};

export const OptOutNoticePage = ({ currentProgress }: VendorsProps) => {
  const [org, orgRequest] = usePrimaryOrganization();

  const { settings: pcSettings, orgRequest: pcOrgRequest } = usePrivacyCenterSettings();

  const usesLongForm = org?.doesOrgProcessOptOuts;
  const noticeRequired = org?.optOutPageType !== "NONE";
  const imgUrl = getHelpImageFilename(
    org.isSelling,
    org.isSharing,
    usesLongForm,
    pcSettings.disableCaResidencyConfirmation,
  );

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRecipients.NoticeOfRightToOptOut"
          prevUrl={Routes.getCompliant.dataRecipients.selling}
        />
      }
      currentProgress={currentProgress}
      requests={[orgRequest, pcOrgRequest]}
    >
      <PageHeader
        titleContent="Opt-Outs"
        descriptionContent={noticeRequired ? <OptOutNoticeRequired /> : <NoOptOutNotice />}
      />

      {noticeRequired && <img src={imgUrl} className="w-100 my-lg" />}

      {noticeRequired && (
        <div className="my-lg">
          <SimpleBooleanQuestion
            question="Would you like to remove the state residency affirmation?"
            helpText="By default, we configure your Notice of Right to Opt-Out to require a state residency affirmation from the consumer before they can submit a request. This is because the Right to Opt-Out is granted by only some privacy laws. You may remove the residency checkbox if you would like to make opt-outs available to all website visitors. However, you are not requried to do so."
            yesLabel="Yes, remove the checkbox"
            noLabel="No, leave as is"
            value={pcSettings.disableCaResidencyConfirmation}
            onChange={(disableCaResidencyConfirmation) =>
              pcSettings.handleFormSubmit({ disableCaResidencyConfirmation })
            }
          />
        </div>
      )}
    </GetCompliantPageWrapper>
  );
};

const NoOptOutNotice = () => (
  <Paras>
    <Para>
      Based on your responses, you will not need to post an opt-out link on your website. To edit
      your responses, return back to the Data Sharing and Data Selling questions.
    </Para>
  </Paras>
);

const OptOutNoticeRequired = () => (
  <Paras>
    <Para>
      Based on your responses, you’ll post a link on your website to the opt-out notice below. We’ll
      help you do that in a later step. Our consumer opt-out tool will be included along with the
      notice.
    </Para>
    <Para>To edit your responses, return back to the Data Sharing and Data Selling questions.</Para>
  </Paras>
);
