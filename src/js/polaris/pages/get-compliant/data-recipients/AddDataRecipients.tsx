import {
  Autocomplete,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  TextField,
  TextFieldProps,
} from "@mui/material";
import clsx from "clsx";
import _ from "lodash";
import debounce from "lodash.debounce";
import React, { ReactNode, useCallback, useEffect, useMemo, useState } from "react";
import { Field, Form, useField, useFormState } from "react-final-form";
import { OnChange } from "react-final-form-listeners";
import { Text } from "../../../../common/components/input/Text";
import { NetworkRequestGate } from "../../../../common/components/NetworkRequestGate";
import { useActionRequest } from "../../../../common/hooks/api";
import { SurveyAnswerFn, SurveyAnswers, useSurvey } from "../../../../common/hooks/useSurvey";
import { NetworkRequestState } from "../../../../common/models/NetworkRequest";
import { Api } from "../../../../common/service/Api";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
  OrganizationVendorSettingsDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import {
  GetCompliantStep,
  OrganizationPublicId,
  UUIDString,
} from "../../../../common/service/server/Types";
import { CloseableDialogTitle } from "../../../components/dialog/CloseableDialogTitle";
import { DashboardPageWrapper } from "../../../components/layout/DashboardPageWrapper";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { SURVEY_GATES_SURVEY_NAME } from "../../../components/organisms/survey/SurveyScanEffect";
import { CategorizedVendorInventory } from "../../../components/organisms/vendor/CategorizedVendorInventory";
import { VendorSearchBox } from "../../../components/organisms/vendor/VendorSearchBox";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { Error as RequestsError } from "../../../copy/requests";
import {
  useDataRecipientsCatalog,
  useOrganizationVendors,
  usePrimaryOrganization,
  usePrimaryOrganizationId,
  useSelf,
} from "../../../hooks";
import { useAddCustomVendor } from "../../../hooks/get-compliant/vendors/useUpdateVendor";
import { Validation } from "../../../util";
import { Phase } from "../../../util/Phase";
import { getVendorSettingsObject } from "../../../util/vendors";
import { CommonDataRecipients } from "../../design/organisms/data-recipients/CommonDataRecipients";
import { GetCompliantContainer } from "../GetCompliantContainer";
import { GetCompliantFooter } from "../ProgressFooter";

const validateVendorNameAsync =
  (
    organizationId: OrganizationPublicId,
    currentName: string,
    enabledVendors: (VendorDto | OrganizationDataRecipientDto)[],
    setResult: (res) => void,
  ) =>
  async (value: string) => {
    const cur = (currentName || "").toLowerCase();
    const val = value.trim().toLowerCase();
    const hasAddedResult = <span>You've already added this vendor</span>;
    // Don't warn if we match the current vendor name (only when editing custom vendor)
    if (val === cur) {
      setResult("");
      return;
    }

    // If an already added organization matches the name short-circuit with already-added
    if (enabledVendors.filter((v) => v.name.toLowerCase() === val).length) {
      setResult(hasAddedResult);
      return;
    }

    // If neither short-circuit case then fetch any vendors matching the name from the server
    const existing = await Api.organizationVendor.searchVendors(organizationId, value);
    const match = existing.filter((e) => e.name.toLowerCase() === val);
    // If no matches then short-circuit to no warning result
    if (match.length === 0) {
      setResult("");
      return;
    }
    // If there are any matches then we show already-exists message
    // We know it's not already-added because we would have caught that above
    setResult(<span>This vendor already exists, try searching for it instead</span>);
  };

interface CustomVendorDialogProps {
  open: boolean;
  onClose: () => void;
  onDone: (v: CustomVendorFormValues) => void;
  enabledVendors: VendorDto[];
  existingCategories: Set<string>;
  vendor?: OrganizationDataRecipientDto;
  defaultName?: string;
  defaultCategory?: string;
  defaultEmail?: string;
  defaultUrl?: string;
  showCategories?: boolean;
  categoryPlaceholder?: string;

  header?: ReactNode;
  confirmLabel?: ReactNode;
  includeUrl?: boolean;
  type?: "source" | "vendor";
  customValidation?: (v: CustomVendorFormValues) => Record<string, string>;
}
export const CustomVendorDialog = ({
  open,
  onClose,
  onDone,
  enabledVendors,
  existingCategories,
  vendor,
  defaultName,
  defaultCategory,
  defaultUrl,
  defaultEmail,
  header,
  confirmLabel,
  includeUrl,
  customValidation,
  showCategories = true,
  categoryPlaceholder,
  type = "vendor",
}: CustomVendorDialogProps) => {
  const [category, setCategory] = useState(vendor?.category || defaultCategory || "");

  const cleanupState = () => {
    if (mode !== "Edit") {
      setCategory(vendor?.category || defaultCategory || "");
    }
  };

  useEffect(
    () => setCategory(vendor?.category || defaultCategory || ""),
    [vendor, defaultCategory],
  );

  const onSubmit = (values: CustomVendorFormValues) => {
    onDone({ ...values, category });
    cleanupState();
  };
  const localOnClose = () => {
    cleanupState();
    onClose();
  };
  const mode = !vendor?.id ? "Add" : "Edit";

  const initialValues: any = vendor || {};
  if (!initialValues.name && defaultName) {
    initialValues.name = defaultName;
  }
  if (!initialValues.category && defaultCategory) {
    initialValues.category = defaultCategory;
  }
  if (!initialValues.email && defaultEmail) {
    initialValues.email = defaultEmail;
  }
  if (!initialValues.url && defaultUrl) {
    initialValues.url = defaultUrl;
  }

  const resource = type == "source" ? "Source" : "Vendor";

  const displayHeader = header || `${mode} Custom ${resource}`;
  const displayLabel = confirmLabel || (mode === "Edit" ? "Save" : `Add ${resource}`);

  return (
    <Dialog open={open} onClose={localOnClose} className="add-vendors--custom-vendor-dialog">
      <CloseableDialogTitle
        id="add-vendors--custom-vendor-dialog-title"
        className="add-vendors--custom-vendor-dialog-title"
        onClose={onClose}
      >
        {displayHeader}
      </CloseableDialogTitle>
      <CustomVendorForm
        onSubmit={onSubmit}
        initialValues={initialValues}
        category={category}
        customValidation={customValidation}
      >
        <DialogContent>
          <CustomVendorFields
            enabledVendors={enabledVendors}
            showClassifcation={false}
            category={category}
            setCategory={setCategory}
            existingCategories={existingCategories}
            includeUrl={includeUrl}
            showCategories={showCategories}
            categoryPlaceholder={categoryPlaceholder}
          />
        </DialogContent>
        <DialogActions>
          <Button variant="text" color="secondary" onClick={onClose}>
            Cancel
          </Button>
          <Button variant="contained" color="primary" type="submit">
            {displayLabel}
          </Button>
        </DialogActions>
      </CustomVendorForm>
    </Dialog>
  );
};

export type CustomVendorFormValues = {
  name: string;
  category: string;
  email?: string;
  url?: string;
};

type CustomVendorFormProps = {
  onSubmit: (values: CustomVendorFormValues) => void;
  onValuesChanged?: ({ values, valid }: { values: CustomVendorFormValues; valid: boolean }) => void;
  initialValues: CustomVendorFormValues;
  category: string;
  customValidation?: (v: CustomVendorFormValues) => Record<string, string>;
};
export const CustomVendorForm: React.FC<CustomVendorFormProps> = ({
  onSubmit,
  onValuesChanged,
  initialValues,
  category,
  children,
  customValidation,
}) => {
  return (
    <Form
      onSubmit={onSubmit}
      initialValues={initialValues}
      validate={(values) => {
        const errors = {};
        Validation.requiredString(values, "name", errors);
        Validation.requiredString({ category }, "category", errors);
        Validation.optionalEmail(values, "email", errors);
        Validation.optionalUrl(values, "url", errors);

        const customErrors = customValidation ? customValidation(values) : {};
        _.assign(errors, customErrors);

        return errors;
      }}
      render={({ handleSubmit }) => {
        return (
          <>
            {onValuesChanged && <ValuesSpy onValuesChanged={onValuesChanged} />}
            <form onSubmit={handleSubmit}>{children}</form>
          </>
        );
      }}
    />
  );
};

const ValuesSpy = ({ onValuesChanged }) => {
  const state = useFormState({ subscription: { values: true, valid: true } });
  const { values, valid } = state;
  onValuesChanged({ values, valid });
  return null;
};

type CustomVendorFieldsProps = {
  enabledVendors: VendorDto[] | OrganizationDataRecipientDto[];
  showClassifcation: boolean;
  category: string;
  setCategory: (cat: string) => void;
  existingCategories: Set<string>;
  includeUrl?: boolean;
  showCategories?: boolean;
  categoryPlaceholder?: string;
};
export const CustomVendorFields: React.FC<CustomVendorFieldsProps> = ({
  enabledVendors,
  showClassifcation,
  category,
  setCategory,
  existingCategories,
  includeUrl = true,
  showCategories = true,
  categoryPlaceholder = "Type your own or choose from our list",
}) => {
  const organizationId = usePrimaryOrganizationId();

  const nameField = useField("name");

  const [nameWarning, setNameWarning] = useState("");
  const validateNameWarning = debounce(
    validateVendorNameAsync(organizationId, nameField.input.value, enabledVendors, setNameWarning),
    500,
  );

  return (
    <>
      <Field className="add-vendors--custom-vendor-dialog-input" name="name">
        {({ meta }) => {
          const extraConfig: TextFieldProps = {};
          // We should only override the helperText for warning state
          if (!meta.error && nameWarning) {
            extraConfig.helperText = nameWarning;
          }
          return (
            <Text
              field="name"
              label="Name"
              required
              autoFocus
              className={clsx({ "input-warn": nameWarning })}
              {...extraConfig}
            />
          );
        }}
      </Field>
      <OnChange name="name">
        {async (value) => {
          validateNameWarning(value);
        }}
      </OnChange>

      {showCategories && (
        <div className="add-vendors--custom-vendor-dialog-input">
          <Autocomplete
            id="category-autocomplete"
            className="service-settings--catgory-autocomplete"
            options={Array.from(new Set([...existingCategories])).sort()}
            defaultValue={category || ""}
            inputValue={category || ""}
            value={category ?? ""}
            onInputChange={(event, value) => setCategory(value)}
            freeSolo
            renderInput={(params) => {
              return (
                <TextField
                  {...params}
                  id="category"
                  fullWidth={true}
                  variant="outlined"
                  placeholder={categoryPlaceholder}
                  InputProps={{ ...params.InputProps, type: "text" }}
                  InputLabelProps={{ required: false }}
                  label="Category"
                  required
                />
              );
            }}
            autoCorrect="off"
            autoCapitalize="off"
            spellCheck={false}
          />
        </div>
      )}

      {showClassifcation && (
        <div className="add-vendors--custom-vendor-dialog-input">
          <TextField
            id="my-input"
            fullWidth
            variant="outlined"
            label="Classification"
            value="Third Party"
            disabled
          />
        </div>
      )}

      {includeUrl && (
        <div className="add-vendors--custom-vendor-dialog-input">
          <Text fullWidth field="url" label="URL" />
        </div>
      )}
    </>
  );
};

const DEFAULT_CONSUMER_CONTEXT: CollectionContext[] = ["CONSUMER"];

type UpdateDataRecipientsOptions = {
  addCompleted?: boolean;
  collectionContext?: CollectionContext[];
};
export const useUpdateDataRecipients = (
  orgId: OrganizationPublicId,
  options: UpdateDataRecipientsOptions = {},
): UseUpdateDataRecipients => {
  const { addCompleted = true, collectionContext = DEFAULT_CONSUMER_CONTEXT } = options;

  const [enabledVendors, setEnabledVendors] = useState<Set<UUIDString>>(new Set());
  const [pendingVendor, setPendingVendor] = useState<UUIDString>(null);

  // Requests
  const [currentVendors, vendorsRequest, refreshOrgVendors] = useOrganizationVendors(orgId);
  const [allVendors, allVendorsRequest, refreshAllVendors] = useDataRecipientsCatalog(orgId);

  // Refresh all vendor lists
  const refresh = async () => await Promise.all([refreshOrgVendors(), refreshAllVendors()]);
  const refreshVendorCatalog = async () => refreshAllVendors();

  const handleEnabledVendorsState = (vendorId: UUIDString, enabled: boolean) => {
    setEnabledVendors((prev) => {
      enabled ? prev.add(vendorId) : prev.delete(vendorId);
      return new Set(prev);
    });
  };

  const addVendorApi = useCallback(
    async (vendorId) => {
      try {
        const resp = await Api.organizationVendor.addOrUpdateVendor(orgId, vendorId, {
          completed: addCompleted,
          collectionContext,
        } as OrganizationVendorSettingsDto);
        const { id } = await resp;
        if (!id) {
          handleEnabledVendorsState(vendorId, false);
        }
        return resp;
      } catch (e) {
        handleEnabledVendorsState(vendorId, false);
        throw e;
      }
    },
    [orgId, addCompleted, collectionContext],
  );

  const addVendorMessages = useMemo(
    () => ({
      forceError: RequestsError.AddVendor,
    }),
    [],
  );

  const addVendorSuccess = useCallback(() => {
    setPendingVendor(null);
  }, [setPendingVendor]);

  const addVendorError = useCallback(() => {
    setPendingVendor(null);
  }, [setPendingVendor]);

  const { fetch: addVendor, request: addVendorRequest } = useActionRequest({
    api: addVendorApi,
    messages: addVendorMessages,
    onSuccess: addVendorSuccess,
    onError: addVendorError,
  });

  const handleAddVendor = async (vendorId: UUIDString) => {
    setPendingVendor(vendorId);
    handleEnabledVendorsState(vendorId, true);
    await addVendor(vendorId);
    refresh();
  };

  const handleRemoveVendor = (vendorId: UUIDString) => {
    handleEnabledVendorsState(vendorId, false);
    refreshVendorCatalog();
  };

  const [handleAddCustomVendor] = useAddCustomVendor(
    orgId,
    (ov) => {
      refresh();
      setEnabledVendors((prev) => new Set(prev.add(ov.vendorId)));
    },
    collectionContext,
  );

  const handleEditedVendor = async (
    ov: OrganizationDataRecipientDto,
    values: CustomVendorFormValues,
  ) => {
    ov = await Api.organizationVendor.addOrUpdateVendor(
      orgId,
      ov.vendorId,
      Object.assign(getVendorSettingsObject(ov), values),
    );
  };

  // Effects
  useEffect(() => {
    if (vendorsRequest.success && allVendorsRequest.success) {
      const vendors = currentVendors
        .filter((v) => v.dataRecipientType == "vendor")
        .filter((v) => _.intersection(v.collectionContext, collectionContext).length > 0);

      setEnabledVendors(new Set(vendors.map(({ vendorId }) => vendorId)));
    }
  }, [allVendors, currentVendors, vendorsRequest, allVendorsRequest, collectionContext]);

  return {
    requests: {
      vendorsRequest,
      allVendorsRequest,
      addVendorRequest,
      refresh,
    },
    collectionContext,
    allVendors,
    currentVendors,
    enabledVendors,
    pendingVendor,
    handleAddVendor,
    handleAddCustomVendor,
    handleEditedVendor,
    handleRemoveVendor,
  };
};

export type UseUpdateDataRecipients = {
  requests: {
    vendorsRequest: NetworkRequestState;
    allVendorsRequest: NetworkRequestState;
    addVendorRequest: NetworkRequestState;
    refresh: () => void;
  };
  allVendors: VendorDto[];
  currentVendors: OrganizationDataRecipientDto[];
  enabledVendors: Set<UUIDString>;
  pendingVendor: UUIDString;
  handleAddVendor: (v: UUIDString) => void;
  handleAddCustomVendor: (values: CustomVendorFormValues) => void;
  handleEditedVendor: (ov: OrganizationDataRecipientDto, values: CustomVendorFormValues) => void;
  handleRemoveVendor: (v: UUIDString) => void;
  collectionContext: CollectionContext[];
};

const COMMON_VENDORS_SLUG = "add-data-recipients-common-vendors";

type AddDataRecipientsProps = {
  currentProgress: GetCompliantStep;
  phase?: Phase;
};

export const AddDataRecipients = ({ currentProgress, phase }: AddDataRecipientsProps) => {
  const [self] = useSelf();
  const [org, orgRequest] = usePrimaryOrganization();
  const { answers, setAnswer, surveyRequest } = useSurvey(org.id, SURVEY_GATES_SURVEY_NAME);
  const dataRecipientsContext = useUpdateDataRecipients(org.id);

  const { requests, currentVendors } = dataRecipientsContext;

  // add default organizations
  useEffect(() => {
    const doit = async () => {
      await Api.organizationVendor.addDefaultVendors(org.id);
      requests.refresh();
    };

    doit();
  }, [org]);

  const doneWithCommon = Boolean(answers[COMMON_VENDORS_SLUG]);

  if (phase === "SETUP") {
    const isAdmin = self.globalRole === "ADMIN";
    const title = `Hello ${self?.firstName || org?.name || "there"} 👋`;
    return (
      <DashboardPageWrapper requests={[orgRequest]} useMinimalNav={!isAdmin}>
        <DataRecipientsHeader title={title} />
        <NetworkRequestGate requests={[requests.vendorsRequest, requests.allVendorsRequest]}>
          <AddDataRecipientsModule
            dataRecipientsContext={dataRecipientsContext}
            doneWithCommon={doneWithCommon}
            answers={answers}
            setAnswer={setAnswer}
            fullWidth
          />
        </NetworkRequestGate>
      </DashboardPageWrapper>
    );
  } else {
    const hasVendors = currentVendors && currentVendors.length > 0;
    const nextDisabled = !hasVendors || !doneWithCommon;
    return (
      <GetCompliantPageWrapper
        requests={[requests.vendorsRequest, requests.allVendorsRequest, surveyRequest]}
        currentProgress={currentProgress}
        progressFooter={
          <GetCompliantFooter
            presentStep="DataRecipients.AddDataRecipients"
            nextDisabled={nextDisabled}
            nextDisabledTooltip="Please add vendors before proceeding to the next step"
            saveRequests={[requests.addVendorRequest]}
            recipients={currentVendors}
          />
        }
      >
        <DataRecipientsHeader title="Vendors" />
        <AddDataRecipientsModule
          dataRecipientsContext={dataRecipientsContext}
          doneWithCommon={doneWithCommon}
          answers={answers}
          setAnswer={setAnswer}
        />
      </GetCompliantPageWrapper>
    );
  }
};

type DataRecipientsHeaderProps = {
  title: string;
};

export const DataRecipientsHeader = ({ title }: DataRecipientsHeaderProps) => {
  const description = (
    <Paras>
      <Para>
        Create a list of the vendors your business uses.{" "}
        <strong>Exclude vendors that handle aggregated or publicly available customer data.</strong>{" "}
        For example, a social networking site should be included as a vendor if your website has a
        social login, but not if you simply post content on public pages.
      </Para>
      <Para>
        Try to build a complete list. You can go back and edit your vendor list at any point, but
        it's much easier if you have all of the vendors ready when starting your Data Map.
      </Para>
    </Paras>
  );
  return <PageHeader titleContent={title} descriptionContent={description} />;
};

type AddDataRecipientsModuleProps = {
  dataRecipientsContext: UseUpdateDataRecipients;
  doneWithCommon: boolean;
  answers: SurveyAnswers;
  setAnswer: SurveyAnswerFn;
  fullWidth?: boolean;
};

export const AddDataRecipientsModule = ({
  dataRecipientsContext,
  doneWithCommon,
  answers,
  setAnswer,
  fullWidth,
}: AddDataRecipientsModuleProps) => {
  const {
    requests,
    allVendors,
    currentVendors,
    enabledVendors,
    pendingVendor,
    handleAddVendor,
    handleAddCustomVendor,
    handleEditedVendor,
    handleRemoveVendor,
    collectionContext,
  } = dataRecipientsContext;

  return (
    <GetCompliantContainer wide={!fullWidth} fullWidth={fullWidth}>
      {!doneWithCommon && (
        <div className="mb-xl">
          <CommonDataRecipients
            allVendors={allVendors}
            addedVendors={enabledVendors}
            pendingVendor={pendingVendor}
            handleAddVendor={handleAddVendor}
          />
        </div>
      )}
      <SurveyGateButton
        label="Done with Common Vendors"
        survey={answers}
        updateResponse={setAnswer}
        slug={COMMON_VENDORS_SLUG}
      >
        <div className="mb-lg">
          <VendorSearchBox
            allVendors={allVendors}
            enabledVendors={enabledVendors}
            pendingVendor={pendingVendor}
            handleAddVendor={handleAddVendor}
            handleRemoveVendor={handleRemoveVendor}
            handleAddCustomVendor={handleAddCustomVendor}
            addRequest={requests.addVendorRequest}
            collectionContext={collectionContext}
          />
        </div>
        <CategorizedVendorInventory
          allVendors={allVendors}
          orgVendors={currentVendors}
          enabledVendors={enabledVendors}
          pendingVendor={pendingVendor}
          handleEditedVendor={handleEditedVendor}
          handleRemoveVendor={handleRemoveVendor}
          collectionContext={collectionContext}
        />
      </SurveyGateButton>
    </GetCompliantContainer>
  );
};
