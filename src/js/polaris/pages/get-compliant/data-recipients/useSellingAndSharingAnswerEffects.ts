import { useEffect } from "react";

const useClearSelections = (
  orgId: string,
  answers: any,
  setAnswer: (slug: string, answer: string) => void,
) => {
  useEffect(() => {
    const changes = {};
    if (
      answers["share-data-for-advertising"] === "false" &&
      answers["vendor-sharing-categories"] !== "[]"
    ) {
      changes["vendor-sharing-categories"] = "[]";
    }
    if (
      answers["disclosure-for-intentional-interaction"] === "false" &&
      answers["vendor-intentional-disclosure-selection"] !== "[]"
    ) {
      changes["vendor-intentional-disclosure-selection"] = "[]";
    }
    if (
      answers["disclosure-for-selling"] === "false" &&
      answers["vendor-selling-selection"] !== "[]"
    ) {
      changes["vendor-selling-selection"] = "[]";
    }
    if (answers["disclosure-for-selling"] === "false" && answers["selling-upload-data"] !== "") {
      changes["selling-upload-data"] = "";
    }
    if (
      answers["disclosure-for-selling"] === "false" &&
      answers["vendor-upload-data-selection"] !== "[]"
    ) {
      changes["vendor-upload-data-selection"] = "[]";
    }
    if (
      answers["selling-upload-data"] === "false" &&
      answers["vendor-upload-data-selection"] !== "[]"
    ) {
      changes["vendor-upload-data-selection"] = "[]";
    }
    if (Object.keys(changes).length > 0) {
      for (const slug of Object.keys(changes)) {
        const answer = changes[slug];
        setAnswer(slug, answer);
      }
    }
  }, [orgId, answers, setAnswer]);
};

// Form side-effect responses. This is done here to be in keeping with the general design of the form to have
// these kinds of "automatic classification" actions happen on the front-end. In general, this overall design
// should probably be moved to the back-end, and have the service be responsible for handling these kinds of
// automatic actions based on individual questions.
export const useSellingAndSharingAnswerEffects = (
  orgId: string,
  answers: Record<string, string>,
  setAnswer: (slug: string, answer: string, question?: string) => void,
): void => {
  useClearSelections(orgId, answers, setAnswer);
};
