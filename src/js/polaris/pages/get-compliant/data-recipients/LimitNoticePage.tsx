import React from "react";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { GetCompliantFooter } from "../ProgressFooter";

const NOTICE_IMAGE_URL = "/assets/images/get_compliant/notice/limit/notice.png";

export const LimitNoticePage = () => {
  return (
    <GetCompliantPageWrapper
      progressFooter={<GetCompliantFooter presentStep="DataRecipients.NoticeOfRightToLimit" />}
    >
      <PageHeader titleContent="Notice of Right to Limit" descriptionContent={<LimitNotice />} />

      <img src={NOTICE_IMAGE_URL} className="w-100 my-lg" />
    </GetCompliantPageWrapper>
  );
};

const LimitNotice = () => (
  <Paras>
    <Para>
      Based on your responses, you'll post a link on your website to the notice below. We'll help
      you do that in a later step. To edit your responses, return back to the Sensitive Personal
      Information questions.
    </Para>
  </Paras>
);
