import { Stack } from "@mui/material";
import _ from "lodash";
import React, { useMemo } from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { CollectionGroupType } from "../../../../common/service/server/controller/CollectionGroupsController";
import { CollectionContext } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import {
  FcraCallout,
  GlbaCallout,
  HipaaCallout,
  IncludeInformationCollectedForOtherBusinessesCallout,
} from "../../../components/organisms/callouts/RegulationCallouts";
import { DataRecipientCollectionCards } from "../../../components/organisms/DataRecipientCollectionCards";
import { DataRecipientDisclosureCheckboxes } from "../../../components/organisms/DataRecipientDisclosureCheckboxes";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../hooks/useDataMap";
import { useDisclosuresForCollectionGroups } from "../../../hooks/useDisclosuresForCollectionGroups";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import {
  BUSINESS_SURVEY_NAME,
  COLLECTION_FOR_OTHER_BUSINESSES_SLUG,
} from "../../../surveys/businessSurvey";
import { GetCompliantFooter, ProgressFooter } from "../ProgressFooter";

const EXCLUDE_COLLECTION_GROUP_TYPE: CollectionGroupType[] = ["EMPLOYMENT"];
const CONSUMER_CONTEXT: CollectionContext[] = ["CONSUMER"];

export const DataDisclosuresPage: React.FC = () => {
  const orgId = usePrimaryOrganizationId();

  const [groups, groupsRequest] = useCollectionGroups(
    orgId,
    undefined,
    EXCLUDE_COLLECTION_GROUP_TYPE,
  );

  const [dataMap, dataMapRequest] = useDataMap(orgId);
  const disclosureData = useDisclosuresForCollectionGroups(orgId, groups, CONSUMER_CONTEXT);
  const { answers, surveyRequest } = useSurvey(orgId, BUSINESS_SURVEY_NAME);

  const allComplete = useMemo(
    () =>
      _.every(
        disclosureData.dataRecipients,
        (dr) => disclosureData.disclosuresByVendorId[dr.vendorId]?.length > 0 ?? false,
      ),
    [disclosureData.dataRecipients, disclosureData.disclosuresByVendorId],
  );

  return (
    <GetCompliantPageWrapper
      requests={[
        dataMapRequest,
        groupsRequest,
        surveyRequest,
        disclosureData.requests.dataMapRequest,
        disclosureData.requests.recipientsRequest,
      ]}
      progressFooter={
        <ProgressFooter
          actions={
            <GetCompliantFooter
              saveRequests={[disclosureData.requests.toggleDisclosureRequest]}
              nextDisabledTooltip="Please indicate disclosures for each vendor before continuing."
              presentStep="DataRecipients.DataDisclosures"
              prevSkip={["DataRecipients.VendorAssociation"]}
              nextDisabled={!allComplete}
            />
          }
        />
      }
    >
      <PageHeader
        titleContent="Disclosures"
        descriptionContent={
          <>
            <h4 className="intermediate">
              What categories of information are collected, analyzed by, stored in, or otherwise
              processed by your Data Recipients?
            </h4>

            <p>
              For each of your Data Recipients, identify which categories of personal data they
              process, including any categories of digital information that may be collected and
              processed automatically.
            </p>
          </>
        }
      />

      <Stack spacing={2} className="mb-lg">
        {answers["business-hipaa-compliant"] === "true" && <HipaaCallout />}
        {answers["business-glba-compliant"] === "true" && <GlbaCallout />}
        {answers["business-fcra-compliant"] === "true" && <FcraCallout />}
        {answers[COLLECTION_FOR_OTHER_BUSINESSES_SLUG] === "true" && (
          <IncludeInformationCollectedForOtherBusinessesCallout />
        )}
      </Stack>

      <Stack spacing={3}>
        <DataRecipientCollectionCards
          recipients={disclosureData.dataRecipients}
          context={["CONSUMER"]}
          disclosuresByVendorId={disclosureData.disclosuresByVendorId}
          renderer={(r) => (
            <DataRecipientDisclosureCheckboxes
              dataRecipient={r}
              collectedPicGroups={disclosureData.collectedPicGroups}
              selected={disclosureData.disclosuresByVendorId[r.vendorId]?.flatMap((d) => d.id)}
              dataMap={dataMap}
              showRecommendations
              toggleDisclosure={disclosureData.actions.toggleDisclosure}
            />
          )}
        />
      </Stack>
    </GetCompliantPageWrapper>
  );
};
