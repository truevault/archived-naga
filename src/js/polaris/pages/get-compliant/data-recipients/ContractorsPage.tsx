import { Add } from "@mui/icons-material";
import { Stack } from "@mui/material";
import React, { useMemo, useState } from "react";
import { Form } from "react-final-form";
import { Text } from "../../../../common/components/input/Text";
import { LoadingButton } from "../../../../common/components/LoadingButton";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RemoveVendorDialog } from "../../../components/dialog/RemoveVendorDialog";
import { SurveyBooleanQuestion } from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { VendorColumn } from "../../../components/organisms/vendor/CategoryCollapseCard";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { QuestionHeading } from "../../../components/vendor/QuestionHeading";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { useAddContractors } from "../../../hooks/useOrganizationVendors";
import { composeValidators, validateNotIn, validateRequired } from "../../../util/validation";
import { GetCompliantFooter, ProgressFooterSaveNotification } from "../ProgressFooter";
import { CustomVendorFormValues } from "./AddDataRecipients";

const THIRD_PARTY_RECIPIENTS_SURVEY = "third-party-recipients-survey";
const CONTRACTORS_DOES_NOT_APPLY = "contractors-does-not-apply";
const CONSUMER_CONTEXT: CollectionContext[] = ["CONSUMER"];

export const ContractorsPage = (): React.ReactElement => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // Modal States
  const [deleteTpr, setDeleteTpr] = useState<OrganizationDataRecipientDto | null>(null);
  const openDeleteModal = (tpr: OrganizationDataRecipientDto) => setDeleteTpr(tpr);
  const closeDeleteModal = () => setDeleteTpr(null);

  const { answers, setAnswer, surveyRequest } = useSurvey(
    organizationId,
    THIRD_PARTY_RECIPIENTS_SURVEY,
  );

  const [organizationVendors, organizationVendorsRequest, refresh] =
    useOrganizationVendors(organizationId);

  const contractors = useMemo(
    () => organizationVendors?.filter((v) => v.dataRecipientType === "contractor") || [],
    [organizationVendors],
  );

  const hasContractors = useMemo(
    () => answers[CONTRACTORS_DOES_NOT_APPLY] === "true",
    [answers[CONTRACTORS_DOES_NOT_APPLY]],
  );

  const isFinished = contractors.length > 0 || answers[CONTRACTORS_DOES_NOT_APPLY] === "false";

  const nextDisabled = !isFinished;

  const { fetch: addContractor, request: addContractorRequest } = useAddContractors(
    organizationId,
    () => refresh(),
    ["CONSUMER"],
  );

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRecipients.Contractors"
          nextDisabled={nextDisabled}
          nextDisabledTooltip={"Please complete recipients before proceeding."}
          content={<ProgressFooterSaveNotification request={addContractorRequest} />}
        />
      }
      requests={[organizationVendorsRequest, surveyRequest]}
    >
      <PageHeader
        titleContent="Contractors"
        descriptionContent={
          <Paras>
            <Para>
              In this step, you’ll add any contractors your business works with that use or have
              access to consumer personal information. For example, individual contractors,
              Marketing Agencies running your ad campaigns, or outside vendors with access to your
              Shopify.
            </Para>
          </Paras>
        }
      />

      <div className="mb-xl">
        <SurveyBooleanQuestion
          question={{
            slug: CONTRACTORS_DOES_NOT_APPLY,
            question:
              "Does your business have contractors that access personal data about your consumers?",
            visible: true,
            type: "boolean",
            isDisabled: contractors.length > 0,
            disabledReason: "You have at least one contractor.",
          }}
          answer={answers[CONTRACTORS_DOES_NOT_APPLY]}
          onAnswer={(ans) => setAnswer(CONTRACTORS_DOES_NOT_APPLY, ans)}
        />
      </div>
      {hasContractors && (
        <div>
          <div className="mb-md">
            <QuestionHeading
              condensed
              question="Add each of your contractors below that access personal information."
            />
          </div>
          <div className="mb-xl">
            <AddContractorForm
              existing={contractors.map((tp) => tp.name)}
              onSubmit={addContractor}
            />
          </div>
        </div>
      )}

      {
        contractors.length > 0 && Boolean(answers[CONTRACTORS_DOES_NOT_APPLY]) && (
          <VendorColumn
            vendors={contractors}
            pendingVendor={null}
            handleRemove={(v) => openDeleteModal(v as unknown as OrganizationDataRecipientDto)}
          />
        ) // Maybe remove this
      }

      <RemoveVendorDialog
        open={Boolean(deleteTpr)}
        onClose={closeDeleteModal}
        vendorName={deleteTpr?.name}
        vendorId={deleteTpr?.vendorId}
        collectionContext={CONSUMER_CONTEXT}
      />
    </GetCompliantPageWrapper>
  );
};

export const AddContractorForm: React.FC<{
  onSubmit: (v: CustomVendorFormValues) => Promise<void>;
  existing: string[];
}> = ({ onSubmit, existing = [] }) => {
  return (
    <Form onSubmit={onSubmit}>
      {(formProps) => (
        <form onSubmit={(e) => formProps.handleSubmit(e).then(() => formProps.form.restart())}>
          <Stack direction="row" spacing={2} alignItems="flex-start">
            <Text
              required
              field="name"
              placeholder="Name"
              label={null}
              validate={composeValidators(
                validateRequired,
                validateNotIn(existing, "A Contractor already exists with that name", false),
              )}
              style={{ margin: 0, flex: 1 }}
            />

            <LoadingButton
              type="submit"
              color="primary"
              size="large"
              loading={formProps.submitting}
              disabled={formProps.pristine}
              startIcon={<Add />}
            >
              Add Contractor
            </LoadingButton>
          </Stack>
        </form>
      )}
    </Form>
  );
};
