import React, { useEffect, useMemo, useState } from "react";
import { useHistory } from "react-router-dom";
import { NetworkRequest } from "../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { Loading } from "../../../components";
import { ActionRow } from "../../../components/ActionRow";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { useNonEmploymentCollectionGroups } from "../../../hooks/useCollectionGroups";
import { useProgressKeys } from "../../../hooks/useProgressKeys";
import { useGetCompliantContext } from "../../../root/GetCompliantContext";
import { RouteHelpers } from "../../../root/routes";
import { GetCompliantFooter } from "../ProgressFooter";
import { ProgressSpan } from "../ProgressSpan";

export const DataRecipientAssociation: React.FC = () => {
  const { progress } = useGetCompliantContext();
  // Variables
  const orgId = usePrimaryOrganizationId();
  const history = useHistory();

  const [groups, groupsRequest] = useNonEmploymentCollectionGroups(orgId);

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  const keys = useMemo(() => groups?.map((g) => `vendor-association-${g.id}`), [groups]);
  const {
    progress: progressValues,
    request: progressRequest,
    update: updateProgress,
  } = useProgressKeys(orgId, keys);

  const gotoGroupPage = async (group: CollectionGroupDetailsDto) => {
    const key = `vendor-association-${group.id}`;
    const prev = progressValues.find((it) => it.key == key);

    const saveStatus = prev?.progress?.value === "DONE" ? "DONE" : "IN_PROGRESS";
    await updateProgress({ keyId: key, progress: saveStatus });
    history.push(RouteHelpers.getCompliant.dataRecipients.vendorAssociationForGroup(group.id));
  };

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(groupsRequest, progressRequest)) {
      setReadyToRender(true);
    }
  }, [groupsRequest, progressRequest]);

  // Skip to the specific group page if there's only 1
  useEffect(() => {
    if (readyToRender && groups?.length == 1) {
      gotoGroupPage(groups[0]);
    }
  }, [readyToRender, groups]);

  const isFinished = groups?.every((g) => {
    const key = `vendor-association-${g.id}`;
    const progress = progressValues?.find((it) => it.key == key);
    return progress?.progress?.value == "DONE";
  });

  if (!readyToRender || groups?.length == 1) {
    return (
      <GetCompliantPageWrapper currentProgress={progress}>
        <PageHeader titleContent="" />
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  return (
    <GetCompliantPageWrapper
      currentProgress={progress}
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRecipients.VendorAssociation"
          nextDisabled={!isFinished}
        />
      }
    >
      <PageHeader titleContent="Associations" />

      <div className="my-xl text-t3">
        Select which Data Recipients receive or process data about each collection group below.
      </div>

      {groups.map((g, idx) => {
        const p = progressValues.find((it) => it.key == `vendor-association-${g.id}`);
        return (
          <ActionRow
            topBorder={idx == 0}
            bottomBorder={idx != groups.length - 1}
            key={g.id}
            label={g.name}
            status={<ProgressSpan progress={p?.progress?.value || "NOT_STARTED"} />}
            onRowClick={() => gotoGroupPage(g)}
          />
        );
      })}
    </GetCompliantPageWrapper>
  );
};
