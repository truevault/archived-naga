import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { NetworkRequest } from "../../../../common/models";
import { NetworkRequestState } from "../../../../common/models/NetworkRequest";
import {
  OrganizationDataRecipientDto,
  ProcessingRegion as ProcessingRegionEnum,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Loading } from "../../../components";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import {
  DataRecipientRadioOptions,
  RadioOption,
} from "../../../components/organisms/data-recipients/DataRecipientRadioOptions";
import { useOrganizationVendors, usePrimaryOrganization } from "../../../hooks";
import { useUpdateVendor } from "../../../hooks/get-compliant/vendors/useUpdateVendor";
import { useNonEmploymentCollectionGroups } from "../../../hooks/useCollectionGroups";
import { Routes } from "../../../root/routes";
import { GetCompliantContainer } from "../GetCompliantContainer";
import { GetCompliantFooter } from "../ProgressFooter";

// skip if not GDPR
export const useRequireGdpr = (nextUrl: string) => {
  const [org] = usePrimaryOrganization();
  const history = useHistory();
  useEffect(() => {
    if (org.featureGdpr === false) {
      history.push(nextUrl);
    }
  }, [org.featureGdpr]);
};

export const ProcessingRegion: React.FC = () => {
  // Variables
  const [org] = usePrimaryOrganization();
  const [recipients, recipientsRequest] = useOrganizationVendors(org.id);
  const [cg, cgRequest] = useNonEmploymentCollectionGroups(org.id);

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  const nextUrl = Routes.getCompliant.dataRecipients.sharing;
  useRequireGdpr(nextUrl);

  useEffect(() => {
    if (NetworkRequest.areFinished(recipientsRequest, cgRequest)) {
      setReadyToRender(true);
    }
  }, [recipientsRequest, cgRequest]);

  const { updateVendor, updateVendorRequest } = useUpdateVendor(org.id, recipients);

  const updateRecipientRegion = (
    recipient: OrganizationDataRecipientDto,
    regions: ProcessingRegionEnum[],
  ) => updateVendor({ vendorId: recipient.vendorId, processingRegions: regions });

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  const canNext = true;

  const footer = (
    <ProcessingRegionFooter
      nextDisabled={!canNext}
      request={updateVendorRequest}
      singleGroup={cg?.length == 1}
    />
  );

  return (
    <GetCompliantPageWrapper progressFooter={footer}>
      <ProcessionRegionHeader />

      <GetCompliantContainer>
        {recipients?.map((recipient) => (
          <RecipientRegionSelection
            recipient={recipient}
            key={recipient.id}
            updateRegions={(regions) => updateRecipientRegion(recipient, regions)}
          />
        ))}
      </GetCompliantContainer>
    </GetCompliantPageWrapper>
  );
};

type RecipientRegionSelectionProps = {
  recipient: OrganizationDataRecipientDto;
  updateRegions: (regions: ProcessingRegionEnum[]) => Promise<OrganizationDataRecipientDto>;
};

const RecipientRegionSelection: React.FC<RecipientRegionSelectionProps> = ({
  recipient,
  updateRegions,
}) => {
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState(regionsToValue(recipient.processingRegions));

  const handleChange = async (value: string) => {
    const regions = valueToRegions(value);
    setValue(value);
    setLoading(true);
    try {
      await updateRegions(regions);
    } finally {
      setLoading(false);
    }
  };

  const options: RadioOption[] = [
    { value: "us-only", label: "US Only" },
    { value: "eea-uk-only", label: "EEA/UK Only" },
    { value: "both", label: "Both US & EEA/UK" },
  ];

  return (
    <DataRecipientRadioOptions
      recipient={recipient}
      options={options}
      value={value}
      loading={loading}
      onChange={handleChange}
    />
  );
};

const regionsToValue = (regions: ProcessingRegionEnum[]): string => {
  const hasUs = regions.includes("UNITED_STATES");
  const hasEEa = regions.includes("EEA_UK");

  if (hasUs && !hasEEa) {
    return "us-only";
  }
  if (!hasUs && hasEEa) {
    return "eea-uk-only";
  }
  // we treat having neither as "both" as it's the default option, and having neither is not a valid state
  return "both";
};

const valueToRegions = (value: string): ProcessingRegionEnum[] => {
  if (value == "us-only") {
    return ["UNITED_STATES"];
  }
  if (value == "eea-uk-only") {
    return ["EEA_UK"];
  }
  return ["UNITED_STATES", "EEA_UK"];
};

const ProcessionRegionHeader = () => {
  const description = (
    <>
      In the table below, indicate whether any of your Data Recipients process the data of consumers
      located only in the US or only in Europe. By default, we’ve selected “Both US & EEA/UK.”
      Update the defaults if needed.
    </>
  );
  const help = (
    <>
      For example, if your business’s shipping services in the US do not typically handle the data
      of European customers, they would be “US Only.”
    </>
  );
  return <PageHeader titleContent="Regions" descriptionContent={description} helpContent={help} />;
};

type FooterProps = {
  nextDisabled?: boolean;
  request?: NetworkRequestState;
  singleGroup: boolean;
};
const ProcessingRegionFooter: React.FC<FooterProps> = ({ nextDisabled, request, singleGroup }) => {
  return (
    <GetCompliantFooter
      presentStep="DataRecipients.ProcessingRegion"
      nextDisabled={nextDisabled}
      nextDisabledTooltip="Please complete recipients before proceeding."
      saveRequests={request}
      prevSkip={singleGroup ? ["DataRecipients.VendorAssociation"] : []}
    />
  );
};
