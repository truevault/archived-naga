import React from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { SurveyRenderer } from "../../../components/get-compliant/survey/SurveyRenderer";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { sharingSurvey } from "../../../surveys/sharingSurvey";
import { isFinished } from "../../../surveys/survey";
import { GetCompliantContainer } from "../GetCompliantContainer";
import { GetCompliantFooter } from "../ProgressFooter";
import { SELLING_AND_SHARING_SURVEY_NAME } from "./shared";
import { useSellingAndSharingAnswerEffects } from "./useSellingAndSharingAnswerEffects";

export const VendorSharing = () => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // Requests
  const [vendors, vendorsRequest] = useOrganizationVendors(organizationId);

  // save/load current questions/answers
  const {
    answers,
    setAnswer,
    surveyRequest: surveyQuestionsRequest,
  } = useSurvey(organizationId, SELLING_AND_SHARING_SURVEY_NAME);

  useSellingAndSharingAnswerEffects(organizationId, answers, setAnswer);

  const survey = sharingSurvey(answers, vendors);
  const nextDisabled = !isFinished(survey, answers);

  return (
    <GetCompliantPageWrapper
      requests={[vendorsRequest, surveyQuestionsRequest]}
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRecipients.DataSharing"
          nextDisabled={nextDisabled}
          nextDisabledTooltip="Please answer all questions before proceeding to the next step."
        />
      }
    >
      <PageHeader
        titleContent="Data Sharing"
        descriptionContent="Next, we’ll determine if you need to allow consumers to opt-out of data selling or data sharing."
      />

      <GetCompliantContainer>
        <SurveyRenderer survey={survey} answers={answers} setAnswer={setAnswer} offsetFirst />
      </GetCompliantContainer>
    </GetCompliantPageWrapper>
  );
};
