import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VENDOR_CATEGORY_AD_NETWORK } from "../../../types/Vendor";

export const SELLING_AND_SHARING_SURVEY_NAME = "selling-and-sharing-survey";

export const getAdNetworkVendors = (
  vendors: OrganizationDataRecipientDto[],
): OrganizationDataRecipientDto[] =>
  vendors.filter((v) => v.category == VENDOR_CATEGORY_AD_NETWORK);

export const getSelectedIntentionalInteractionVendors = (
  answers: Record<string, string>,
): string[] => JSON.parse(answers["vendor-intentional-disclosure-selection"] || "[]");

export const getSelectedDirectSellingVendors = (answers: Record<string, string>): string[] =>
  JSON.parse(answers["vendor-upload-data-selection"] || "[]");
