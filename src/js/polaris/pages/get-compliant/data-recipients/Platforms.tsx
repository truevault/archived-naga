import React, { useMemo, useState } from "react";
import { useActionRequest } from "../../../../common/hooks/api";
import { NetworkRequest } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { GetCompliantStep } from "../../../../common/service/server/Types";
import { Loading } from "../../../components";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { Error as RequestsError } from "../../../copy/requests";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { usePlatformApps } from "../../../hooks/useOrganizationVendors";
import { DataRecipientMultiselect } from "../../design/molecules/DataRecipientMultiselect";
import { GetCompliantFooter } from "../ProgressFooter";

interface PlatformsProps {
  currentProgress: GetCompliantStep;
}
export const Platforms = ({ currentProgress }: PlatformsProps) => {
  const organizationId = usePrimaryOrganizationId();

  const [allVendors, vendorsRequest] = useOrganizationVendors(organizationId);
  const platforms = useMemo(
    () => allVendors && allVendors.filter((x) => x.isPlatform),
    [allVendors],
  );

  const [currPlatformIx] = useState(0);

  if (!NetworkRequest.areFinished(vendorsRequest)) {
    return <Loading />;
  }

  const currPlatform = platforms[currPlatformIx];

  return (
    <PlatformView
      currentProgress={currentProgress}
      organizationId={organizationId}
      platform={currPlatform}
    />
  );
};

interface PlatformViewProps {
  currentProgress: GetCompliantStep;
  organizationId: string;
  platform: OrganizationDataRecipientDto;
}

const PlatformView = ({ currentProgress, organizationId, platform }: PlatformViewProps) => {
  const [platformVendors, platformVendorsRequest, refresh] = usePlatformApps(
    organizationId,
    platform.vendorId,
  );

  const installedVendors = useMemo(() => platformVendors?.installed || [], [platformVendors]);
  const [updating, setUpdating] = useState(false);

  const { fetch: updatePlatformVendor } = useActionRequest({
    api: async ({ appId, included }: { appId: string; included: boolean }) => {
      if (included) {
        await Api.organizationVendor.addPlatformApplication(
          organizationId,
          platform.vendorId,
          appId,
        );
      } else {
        await Api.organizationVendor.removePlatformApplication(
          organizationId,
          platform.vendorId,
          appId,
        );
      }
    },
    messages: {
      forceError: RequestsError.TogglePlatformApp,
    },
  });

  const onToggleAllSelected = async (included: boolean) => {
    try {
      setUpdating(true);
      await Promise.all(
        platformVendors?.available.map((dr) =>
          updatePlatformVendor({ appId: dr.vendorId, included }),
        ),
      );

      await refresh();
    } finally {
      setUpdating(false);
    }
  };

  const onToggleVendor = async ({ appId, included }: { appId: string; included: boolean }) => {
    try {
      setUpdating(true);
      await updatePlatformVendor({ appId, included });
      await refresh();
    } finally {
      setUpdating(false);
    }
  };

  // TODO: Eventually this copy will need updating based on the currently selected platform
  // Everything else about this should work ok, though.
  return (
    <GetCompliantPageWrapper
      requests={[platformVendorsRequest]}
      currentProgress={currentProgress}
      progressFooter={<GetCompliantFooter presentStep="DataRecipients.Shopify" />}
    >
      <PageHeader
        titleContent="Shopify Store Apps"
        descriptionContent={
          <p>
            Select which vendors below are installed apps in your Shopify store. To see a list of
            your installed apps, go to the{" "}
            <a target="_blank" rel="noreferrer noopener" href="https://shopify.com/admin/apps">
              Apps
            </a>{" "}
            section of your Shopify account.
          </p>
        }
      />
      <DataRecipientMultiselect
        recipients={platformVendors?.available}
        selected={installedVendors}
        onChange={onToggleVendor}
        onChangeSelectAll={onToggleAllSelected}
        loading={updating}
      />
    </GetCompliantPageWrapper>
  );
};
