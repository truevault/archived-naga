import { Alert, Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import _ from "lodash";
import React, { useEffect, useMemo, useState } from "react";
import { LoadingButton } from "../../../../common/components/LoadingButton";
import { NetworkRequest } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import {
  CategoryDto,
  DataMapDto,
  groupsFromPIC,
  PICGroup,
} from "../../../../common/service/server/dto/DataMapDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Loading } from "../../../components";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { useNonEmploymentCollectionGroups } from "../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../hooks/useDataMap";
import { GetCompliantFooter } from "../ProgressFooter";

export const DoubleCheck: React.FC = () => {
  // Variables
  const orgId = usePrimaryOrganizationId();

  const [groups, groupsRequest] = useNonEmploymentCollectionGroups(orgId);
  const [dataMap, dataMapRequest] = useDataMap(orgId);
  const [recipients, recipientsRequest] = useOrganizationVendors(orgId);

  const unsatisifed = useMemo(() => {
    if (recipients && dataMap && groups) {
      return unsatisfiedRecommendations(recipients, groups, dataMap);
    }
    return null;
  }, [recipients, dataMap, groups]);

  const unsatCollectionGroups = useMemo(
    () => _.uniqBy(unsatisifed?.map((u) => u.collectionGroup) || [], (u) => u.id),
    [unsatisifed],
  );

  const [readyToRender, setReadyToRender] = useState(false);

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(groupsRequest, dataMapRequest, recipientsRequest)) {
      setReadyToRender(true);
    }
  }, [groupsRequest, dataMapRequest, recipientsRequest]);

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper>
        <PageHeader titleContent="Disclosures Double Check" />
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  if (unsatisifed?.length == 0) {
    return (
      <GetCompliantPageWrapper
        progressFooter={<GetCompliantFooter presentStep="DataRecipients.DataMapDoubleCheck" />}
      >
        <PageHeader titleContent="Disclosures Double Check" />

        <h4 className="intermediate">Everything looks good</h4>

        <p>
          All of your disclosures match our suggestions for data categories to include in your
          mapping.
        </p>
      </GetCompliantPageWrapper>
    );
  }

  return (
    <GetCompliantPageWrapper
      progressFooter={<GetCompliantFooter presentStep="DataRecipients.DataMapDoubleCheck" />}
    >
      <PageHeader titleContent="Disclosures Double Check" />

      <h4 className="intermediate">We noticed some things you might have missed...</h4>

      <p>
        Some of your vendors have common data disclosures that were not part of your data map.
        Review our suggestions for data categories to include in your mapping.
      </p>

      {unsatCollectionGroups.map((cg) => {
        return (
          <UnsatisifedCollectionGroup
            key={cg.id}
            collectionGroup={cg}
            unsatisifed={unsatisifed}
            header={groups.length > 1}
          />
        );
      })}
    </GetCompliantPageWrapper>
  );
};

type UnsatisifedCollectionGroupProps = {
  collectionGroup: CollectionGroupDetailsDto;
  unsatisifed: UnsatisfiedRecommendation[];
  header?: boolean;
};
const UnsatisifedCollectionGroup: React.FC<UnsatisifedCollectionGroupProps> = ({
  collectionGroup,
  unsatisifed,
  header,
}) => {
  const cgUnsatisfied = useMemo(
    () => unsatisifed.filter((u) => u.collectionGroup.id == collectionGroup.id),
    [unsatisifed, collectionGroup],
  );

  if (cgUnsatisfied.length == 0) {
    return null;
  }

  return (
    <>
      {header && <h5>{collectionGroup.name}</h5>}

      {cgUnsatisfied.map((u) => {
        return <UnsatisfiedVendorCard key={u.recipient.id} unsatisifed={u} />;
      })}
    </>
  );
};

type UnsatisfiedPicSelectionRowProps = {
  unsatisfied: UnsatisfiedRecommendation;
  picGroup: PICGroup;
  onChange: (picGroupId: string) => void;
  checked: boolean;
};

const UnsatisfiedPicSelectionRow: React.FC<UnsatisfiedPicSelectionRowProps> = ({
  unsatisfied,
  picGroup,
  onChange,
  checked,
}) => {
  const pic = useMemo(
    () =>
      unsatisfied.unsatisifedPIC
        .filter((pic) => pic.picGroupId == picGroup.id)
        .map((pic) => pic.name)
        .join(", "),
    [unsatisfied.unsatisifedPIC, picGroup.id],
  );

  return (
    <FormControlLabel
      key={picGroup.id}
      control={<Checkbox checked={checked} onChange={() => onChange(picGroup.id)} />}
      label={
        <>
          <strong>{picGroup.name}</strong>: {pic}
        </>
      }
    />
  );
};

const getAddedMessage = (
  addedPicGroups: PICGroup[],
  addedPic: CategoryDto[],
  recipient: string,
): string => {
  const allPic = addedPic.map((pic) => pic.name).join(", ");
  const allGroups = addedPicGroups.map((g) => g.name).join(", ");

  return `We have added <strong>${allPic}</strong> to your data collected and we have added <strong>${allGroups}</strong> to your data disclosed to <strong>${recipient}</strong>.`;
};

type UnsatisfiedVendorCardProps = {
  unsatisifed: UnsatisfiedRecommendation;
};
const UnsatisfiedVendorCard: React.FC<UnsatisfiedVendorCardProps> = ({ unsatisifed }) => {
  const orgId = usePrimaryOrganizationId();
  const [adding, setAdding] = useState(false);
  const [selectedPicGroupIds, setSelectedPicGroupIds] = useState([]);
  const [addedPicGroupIds, setAddedPicGroupIds] = useState([]);

  const handlAddSelected = async () => {
    setAdding(true);

    try {
      let allPr: Promise<unknown>[] = [];

      const selectedUnsatisfiedPIC = unsatisifed.unsatisifedPIC.filter((pic) =>
        selectedPicGroupIds.includes(pic.picGroupId),
      );

      selectedUnsatisfiedPIC.forEach((pic) =>
        allPr.push(
          Api.dataMap.updateCollection(orgId, unsatisifed.collectionGroup.id, pic.id, true),
        ),
      );

      await Promise.all(allPr);
      allPr = [];

      const selectedUnsatisfiedPICGroups = unsatisifed.unsatisfiedPICGroup.filter((picGroup) =>
        selectedPicGroupIds.includes(picGroup.id),
      );

      selectedUnsatisfiedPICGroups.forEach((group) =>
        allPr.push(
          Api.dataMap.updateDisclosure(
            orgId,
            unsatisifed.collectionGroup.id,
            unsatisifed.recipient.vendorId,
            group.id,
            true,
            false,
          ),
        ),
      );

      await Promise.all(allPr);

      // Clear the selection
      setAddedPicGroupIds((v) => [...v, ...selectedPicGroupIds]);
      setSelectedPicGroupIds([]);
    } catch (err) {
      console.warn(
        "Something went wrong while adding recommended collections and disclosures: ",
        err,
      );
    } finally {
      setAdding(false);
    }
  };

  const handleSelectPicGroup = (picGroupId: string) => {
    if (selectedPicGroupIds.includes(picGroupId)) {
      setSelectedPicGroupIds((v) => v.filter((id) => id !== picGroupId));
    } else {
      setSelectedPicGroupIds((v) => [...v, picGroupId]);
    }
  };

  const remainingUnsatisfiedPICGroups = useMemo(
    () => unsatisifed.unsatisfiedPICGroup.filter((g) => !addedPicGroupIds.includes(g.id)),
    [unsatisifed, addedPicGroupIds],
  );

  const addedPicGroups = useMemo(
    () => unsatisifed.unsatisfiedPICGroup.filter((g) => addedPicGroupIds.includes(g.id)),
    [unsatisifed, addedPicGroupIds],
  );

  const addedPic = useMemo(
    () => unsatisifed.unsatisifedPIC.filter((pic) => addedPicGroupIds.includes(pic.picGroupId)),
    [unsatisifed, addedPicGroupIds],
  );

  return (
    <div className="my-xl">
      <Callout variant={CalloutVariant.LightGray}>
        <div className="flex-row">
          <div>
            <img className="size-lg" src={unsatisifed.recipient.logoUrl} />
          </div>
          <div>
            <h5 className="flex-grow my-0">{unsatisifed.recipient.name}</h5>

            <p>
              The following data may be processed by <strong>{unsatisifed.recipient.name}</strong>.
              Do you want to add it to your data map?
            </p>

            <FormGroup className="my-md">
              {remainingUnsatisfiedPICGroups.map((picGroup) => (
                <UnsatisfiedPicSelectionRow
                  picGroup={picGroup}
                  unsatisfied={unsatisifed}
                  checked={selectedPicGroupIds.includes(picGroup.id)}
                  onChange={() => handleSelectPicGroup(picGroup.id)}
                  key={picGroup.id}
                />
              ))}
            </FormGroup>

            {addedPicGroupIds.length > 0 && (
              <Alert className="my-md" severity="warning" icon={false}>
                <div
                  dangerouslySetInnerHTML={{
                    __html: getAddedMessage(addedPicGroups, addedPic, unsatisifed.recipient.name),
                  }}
                />
              </Alert>
            )}

            {remainingUnsatisfiedPICGroups.length > 0 && (
              <LoadingButton
                variant="contained"
                size="small"
                color="primary"
                className="px-lg"
                onClick={handlAddSelected}
                loading={adding}
                disabled={selectedPicGroupIds.length === 0}
              >
                Add Selected
              </LoadingButton>
            )}
          </div>
        </div>
      </Callout>
    </div>
  );
};

type UnsatisfiedRecommendation = {
  unsatisifedPIC: CategoryDto[];
  unsatisfiedPICGroup: PICGroup[];
  recipient: OrganizationDataRecipientDto;
  collectionGroup: CollectionGroupDetailsDto;
};

const unsatisfiedRecommendations = (
  recipients: OrganizationDataRecipientDto[],
  groups: CollectionGroupDetailsDto[],
  dataMap: DataMapDto,
): UnsatisfiedRecommendation[] => {
  const unsatisifed: UnsatisfiedRecommendation[] = [];

  for (const group of groups) {
    const associatedVendorIds = dataMap.disclosure
      .filter((d) => d.collectionGroupId == group.id)
      .map((d) => d.vendorId);

    const collectedPICIds =
      dataMap.collection.find((c) => c.collectionGroupId == group.id)?.collected || [];
    const disclosures = dataMap.disclosure.filter((d) => d.collectionGroupId == group?.id) || [];

    for (const recipient of recipients) {
      if (!associatedVendorIds.includes(recipient.vendorId)) {
        continue;
      }

      const disclosedPICIds =
        disclosures.find((d) => d.vendorId == recipient.vendorId)?.disclosed || [];

      const recommendedPIC = dataMap.categories.filter((pic) =>
        recipient.recommendedPersonalInformationCategories.includes(pic.id),
      );

      const unsatCollectionPIC = recommendedPIC.filter((pic) => !collectedPICIds.includes(pic.id));
      const unsatDisclosurePIC = recommendedPIC.filter((pic) => !disclosedPICIds.includes(pic.id));
      const unsatPIC = _.uniqBy([...unsatCollectionPIC, ...unsatDisclosurePIC], (c) => c.id);

      const unsatGroups = groupsFromPIC(unsatPIC);

      if (unsatPIC.length > 0 || unsatGroups.length > 0) {
        unsatisifed.push({
          unsatisifedPIC: unsatPIC,
          unsatisfiedPICGroup: unsatGroups,
          recipient: recipient,
          collectionGroup: group,
        });
      }
    }
  }

  return unsatisifed;
};
