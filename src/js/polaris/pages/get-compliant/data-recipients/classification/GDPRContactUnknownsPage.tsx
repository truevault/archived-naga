import { Check } from "@mui/icons-material";
import { Link } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { useSurvey } from "../../../../../common/hooks/useSurvey";
import { NetworkRequest } from "../../../../../common/models";
import {
  GDPRContactProcessorStatus,
  OrganizationDataRecipientDto,
} from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Loading } from "../../../../components";
import { Callout, CalloutVariant } from "../../../../components/Callout";
import { EmptyInfo } from "../../../../components/Empty";
import { QuestionLabel } from "../../../../components/get-compliant/survey/QuestionContainer";
import { GetCompliantPageWrapper } from "../../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../../components/layout/header/Header";
import { CopyTextCallout } from "../../../../components/organisms/callouts/CopyTextCallout";
import {
  DataRecipientRadioOptions,
  RadioDirection,
  RadioOption,
} from "../../../../components/organisms/data-recipients/DataRecipientRadioOptions";
import { SurveyGateButton } from "../../../../components/organisms/survey/SurveyGateButton";
import { Para } from "../../../../components/typography/Para";
import { Paras } from "../../../../components/typography/Paras";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../../hooks";
import {
  UpdateVendorFn,
  useUpdateVendor,
} from "../../../../hooks/get-compliant/vendors/useUpdateVendor";
import { VENDOR_CLASSIFICATION_SURVEY_NAME } from "../../consumer-collection/GDPRInternationalDataTransfers";
import { GetCompliantFooter } from "../../ProgressFooter";

const fullEmail = `Hello,
You are one of our vendors and we are completing steps for GDPR compliance. I am trying to classify our vendors as either “controllers” or “processors” under GDPR.

I am writing to ask if there is a Data Processing Agreement (DPA) in place — or available to sign — that governs your data processing activities on our behalf. If so, please send me a copy. If not, would you be willing to complete and sign a DPA provided by us?`;

export const GDPRContactUnknownsPage: React.FC<{}> = () => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  // Requests
  const [orgVendors, orgVendorsRequest, refresh] = useOrganizationVendors(organizationId);
  const other = useMemo(
    () =>
      orgVendors.filter(
        (o) =>
          o.gdprProcessorSetting === "Unknown" ||
          (o.gdprProcessorSetting === "Controller" &&
            o.gdprContactProcessorStatus === "CONTROLLER"),
      ),
    [orgVendors],
  );
  const { updateVendor } = useUpdateVendor(organizationId, other);

  const { answers, setAnswer, surveyRequest } = useSurvey(
    organizationId,
    VENDOR_CLASSIFICATION_SURVEY_NAME,
  );

  useEffect(() => {
    if (NetworkRequest.areFinished(orgVendorsRequest, surveyRequest)) {
      setReadyToRender(true);
    }
  }, [orgVendorsRequest, surveyRequest]);

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  const allFinished = other.every((r) => r.gdprContactProcessorStatus != null);

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          presentStep="Classification.GDPRContactUnknowns"
          nextDisabledTooltip="Please make a selection for each vendor"
          nextDisabled={!allFinished}
        />
      }
    >
      <PageHeader
        titleContent="Data Recipients with an Unknown GDPR Status"
        descriptionContent={
          <>
            <Paras>
              <Para>
                We highly recommend that you contact the data recipients below to determine their
                GDPR status. If you believe they are “processors” under GDPR — i.e., they process
                data according to your business’s instructions and on its behalf — then they are
                required to have processor guarantees in place.
              </Para>
              <Para>
                Contact the data recipients to ask if they have an existing DPA in place. If they do
                not, you can ask them to sign a controller-processor agreement template published by
                the European Commission.{" "}
                <Link href="/assets/files/2022-08-23-ec-sccs.zip" download>
                  Download EC Template
                </Link>
                .
              </Para>
              <Para>
                Alternatively, you may choose to treat one or more of these data recipients as
                “controllers.” Controllers will be disclosed by name in your privacy policy.
              </Para>
            </Paras>
          </>
        }
      />

      <SurveyGateButton
        survey={answers}
        slug="gdpr-contact-unknowns-get-started"
        updateResponse={setAnswer}
        label="Get Started"
      >
        <CopyTextCallout
          copy={fullEmail}
          title="Use the email template below (or create your own) to contact vendors and inquire about service provider status"
        />

        {other.length > 0 ? (
          <ContactProcessorStatusBlock
            recipients={other}
            updateVendor={updateVendor}
            refresh={refresh}
          />
        ) : (
          <EmptyInfo Icon={Check}>
            None of your Data Recipients have an Unknown Processor Status
          </EmptyInfo>
        )}

        {other.some((o) => o.gdprContactProcessorStatus === "CONTACTED") && (
          <Callout variant={CalloutVariant.Yellow} className="mt-lg">
            We will treat the vendors you’ve contacted as “Processor pending documentation” in your
            account for 3 weeks. If no documentation has been added to the vendor’s settings by that
            time, we will change the classification to “controller” and we will update your Privacy
            Notice to list these data recipients by name as controllers.
          </Callout>
        )}
      </SurveyGateButton>
    </GetCompliantPageWrapper>
  );
};

type ContactProcessorStatusBlockProps = {
  recipients: OrganizationDataRecipientDto[];
  updateVendor: UpdateVendorFn;
  refresh: () => Promise<void>;
};

const PROCESSOR_STATUS_OPTIONS: RadioOption<GDPRContactProcessorStatus>[] = [
  { label: "I contacted this vendor", value: "CONTACTED" },
  {
    label: "Classify as a controller",
    value: "CONTROLLER",
  },
];

const ContactProcessorStatusBlock: React.FC<ContactProcessorStatusBlockProps> = ({
  recipients,
  updateVendor,
  refresh,
}) => {
  return (
    <div className="mt-xl">
      <QuestionLabel label="Unclassified GDPR Vendors" />

      {recipients.map((r) => {
        const onChange = async (updated: GDPRContactProcessorStatus) => {
          await updateVendor({
            vendorId: r.vendorId,
            gdprProcessorSetting: updated === "CONTROLLER" ? "Controller" : "Unknown",
            gdprContactProcessorStatus: updated,
          });

          await refresh();
        };

        return (
          <VendorRadioSelection
            key={r.id}
            initial={r.gdprContactProcessorStatus}
            recipient={r}
            options={PROCESSOR_STATUS_OPTIONS}
            direction={"row"}
            onChange={onChange}
          />
        );
      })}
    </div>
  );
};

type VendorRowProps = {
  recipient: OrganizationDataRecipientDto;
  initial: GDPRContactProcessorStatus | null;
  options: RadioOption<GDPRContactProcessorStatus>[];
  direction: RadioDirection;
  onChange: (updated: GDPRContactProcessorStatus) => void;
};
const VendorRadioSelection: React.FC<VendorRowProps> = ({
  recipient,
  initial,
  options,
  direction,
  onChange,
}) => {
  const [value, setValue] = useState<GDPRContactProcessorStatus | null>(initial);

  const handleChange = (updated: GDPRContactProcessorStatus) => {
    setValue(updated);
    onChange(updated);
  };

  return (
    <DataRecipientRadioOptions
      recipient={recipient}
      options={options}
      value={value}
      onChange={handleChange}
      fullWidth
      direction={direction}
    />
  );
};
