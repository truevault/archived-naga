import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useSurvey } from "../../../../../common/hooks/useSurvey";
import { OrganizationDataRecipientDto } from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../../../common/service/server/dto/OrganizationDto";
import { GetCompliantStep, UUIDString } from "../../../../../common/service/server/Types";
import { ClassifyVendorsHeader } from "../../../../components/get-compliant/vendors/ClassifyVendorsHeader";
import { VendorClassifier } from "../../../../components/get-compliant/vendors/VendorClassifier";
import { GetCompliantPageWrapper } from "../../../../components/layout/GetCompliantPageWrapper";
import { SurveyGateButton } from "../../../../components/organisms/survey/SurveyGateButton";
import {
  SurveyScanEffect,
  SURVEY_GATES_SURVEY_NAME,
} from "../../../../components/organisms/survey/SurveyScanEffect";
import { VendorClassificationDto } from "../../../../../common/service/server/dto/VendorDto";
import { Para } from "../../../../components/typography/Para";
import { Paras } from "../../../../components/typography/Paras";
import { useOrganizationVendors, usePrimaryOrganization } from "../../../../hooks";
import { useUpdateVendor } from "../../../../hooks/get-compliant/vendors/useUpdateVendor";
import { GetCompliantOrder } from "../../../../util/GetCompliantClassificationOrder";
import { vendorsByType } from "../../../../util/vendors";
import { GetCompliantFooter } from "../../ProgressFooter";

type ClassifyVendorsProps = {
  currentProgress: GetCompliantStep;
};

export const CCPAConfirmServiceProviders = ({ currentProgress }: ClassifyVendorsProps) => {
  // Variables
  const [org] = usePrimaryOrganization();
  const organizationId = org.id;

  // State
  const [complete, setComplete] = useState(false);
  const [spContractorsCompleted, setSpContractorsCompleted] = useState(false);
  const [spThirdPartiesCompleted, setSpThirdPartiesCompleted] = useState(false);

  useEffect(() => {
    setComplete(false);
  }, [organizationId]);

  // Requests
  const { answers, setAnswer, surveyRequest } = useSurvey(org.id, SURVEY_GATES_SURVEY_NAME);
  const [orgVendors, orgVendorsRequest] = useOrganizationVendors(organizationId);

  const usVendors = useMemo(
    () =>
      orgVendors
        .filter((sv) => sv.collectionContext.includes("CONSUMER"))
        .filter((sv) => sv.processingRegions.includes("UNITED_STATES")),
    [orgVendors],
  );

  const classificationOptions = useMemo(
    () =>
      orgVendors?.[0]?.classificationOptions?.reduce(
        (acc, c) => Object.assign(acc, { [c.slug]: c }),
        {} as Record<string, VendorClassificationDto>,
      ),
    [orgVendors],
  );

  const { updateVendor, updateVendorRequest } = useUpdateVendor(organizationId, usVendors);

  let {
    serviceVendors: serviceProviders,
    thirdParty,
    contractors,
    otherVendors,
  } = useMemo(() => vendorsByType(usVendors || []), [usVendors]);

  let thirdParties = useMemo(
    () =>
      otherVendors
        .filter((other) => other.serviceProviderRecommendation === "THIRD_PARTY")
        .concat([...thirdParty]),
    [otherVendors, thirdParty],
  );

  const answersChanged = useCallback(
    (classifications: Map<UUIDString, UUIDString>) => {
      setComplete(!Array.from(classifications.values()).some((cid) => !cid));
    },
    [setComplete],
  );

  const nextInfo = useNextInfo(org, orgVendors);

  const hasContractors = Boolean(contractors?.length);
  const hasThirdParties = Boolean(thirdParties?.length);

  const checkSpTos = (items: any) =>
    !!items
      .filter((x) => x.classification.id == classificationOptions["service-provider"].id)
      ?.every((x) => !!x.tosFileKey || !!x.publicTosUrl);

  useEffect(() => {
    setSpContractorsCompleted(checkSpTos(contractors));
    setSpThirdPartiesCompleted(checkSpTos(thirdParties));
  }, [organizationId]);

  const checkContractorSpStatus = (a?: any) => {
    contractors = contractors.map((x) => {
      if (x.vendorId == a.vendorId) {
        x.classification = a.classification;
        x.publicTosUrl = a.publicTosUrl;
        x.tosFileKey = a.tosFileKey;
        x.tosFileName = a.tosFileName;
      }
      return x;
    });
    setSpContractorsCompleted(checkSpTos(contractors));
  };

  const checkThirdPartySpStatus = (a?: any) => {
    thirdParties = thirdParties.map((x) => {
      if (x.vendorId == a.vendorId) {
        x.classification = a.classification;
        x.publicTosUrl = a.publicTosUrl;
        x.tosFileKey = a.tosFileKey;
        x.tosFileName = a.tosFileName;
      }
      return x;
    });
    setSpThirdPartiesCompleted(checkSpTos(thirdParties));
  };

  const hasStarted = answers["ccpa-classification-scan"] == "true";
  const doneReviewingServiceProviders =
    answers["done-with-classification-service-provider-review"] == "true";
  const doneReviewingContractors = answers["done-with-classification-contractor"] == "true";

  const isFinished =
    hasStarted &&
    ((!hasContractors && !hasThirdParties) || doneReviewingServiceProviders) &&
    (!hasContractors || !hasThirdParties || doneReviewingContractors) &&
    complete &&
    spContractorsCompleted &&
    spThirdPartiesCompleted;

  return (
    <GetCompliantPageWrapper
      requests={[orgVendorsRequest, surveyRequest]}
      currentProgress={currentProgress}
      progressFooter={
        <GetCompliantFooter
          presentStep="Classification.ConfirmServiceProvider"
          saveRequests={[updateVendorRequest]}
          nextUrl={nextInfo.url}
          nextLabel={nextInfo.label}
          nextDisabled={!isFinished}
          nextDisabledTooltip="Please make a selection for each vendor"
        />
      }
    >
      <ClassifyVendorsHeader title="Documentation">
        <Paras>
          <Para>
            Privacy laws require that your business’s Data Recipients acting as "service providers"
            or "contractors" agree in writing to certain limitations on how they use personal data
            about your customers. If a vendor is involved in data "sharing" or "selling," or if it
            otherwise does not agree to use limitations, then it cannot be a "service provider" or
            "contractor." Instead, it is considered a "third party" to your business.
          </Para>
          <Para>
            In this step, we will classify your Data Recipients based on your previous answers and
            our research of public documentation. Click below to view the classifications.
          </Para>
        </Paras>
      </ClassifyVendorsHeader>

      <div className="mt-xxl">
        <SurveyScanEffect slug="ccpa-classification-scan" label="Get Started">
          <h4>Service Providers</h4>
          <p>
            We’ve automatically designated the Recipients below as "service providers" based on our
            research. To be considered a "service provider," a vendor must agree that it will not
            use the personal data of your customers for{" "}
            <i>any purpose other than providing a service to your business</i>. Otherwise, the
            vendor is a "third party" to your business. Review the documentation and change the
            selection if you disagree.
          </p>
          <VendorClassifier
            vendors={serviceProviders}
            hideNeedsReview
            defaultClassification="service-provider"
            onChange={answersChanged}
            updateVendor={updateVendor}
          >
            {(hasContractors || hasThirdParties) && (
              <div className="mt-xl">
                <SurveyGateButton
                  slug="done-with-classification-service-provider-review"
                  survey={answers}
                  updateResponse={setAnswer}
                  label="Done Reviewing Service Providers"
                >
                  {hasContractors && (
                    <>
                      <h4>Contractors</h4>
                      <p>
                        Review the "Contractor" designations below and update them if needed. A
                        Contractor is an individual or company that your business discloses customer
                        personal data to pursuant to a written agreement that prevents the
                        contractor from selling, sharing or using the data for non-service purposes.
                      </p>
                      <VendorClassifier
                        vendors={contractors}
                        hideNeedsReview
                        defaultClassification="contractor"
                        onChange={answersChanged}
                        updateVendor={updateVendor}
                        afterUpdateCallback={checkContractorSpStatus}
                        showVendorFilename={true}
                      ></VendorClassifier>
                    </>
                  )}
                  {hasThirdParties && (
                    <div className="mt-xl">
                      <SurveyGateButton
                        slug="done-with-classification-contractor"
                        survey={answers}
                        updateResponse={setAnswer}
                        label="Done Reviewing Contractors"
                        bypass={!hasContractors}
                      >
                        <h4>Third Parties</h4>
                        <p>
                          Review the "Third Party" designations below and update them if needed. A
                          Third Party is any party your business discloses personal data to that is
                          not a Service Provider or Contractor. We’ve automatically designated
                          certain Data Recipients as "Third Parties" based on our research or based
                          on your answers to previous questions.
                        </p>
                        <VendorClassifier
                          vendors={thirdParties}
                          hideNeedsReview
                          defaultClassification="third-party"
                          onChange={answersChanged}
                          updateVendor={updateVendor}
                          afterUpdateCallback={checkThirdPartySpStatus}
                          showVendorFilename={true}
                        ></VendorClassifier>
                      </SurveyGateButton>
                    </div>
                  )}
                </SurveyGateButton>
              </div>
            )}
            <div className="text-component-help mt-md">
              Vendor classification defaults are provided for informational purposes and do not
              constitute legal advice. The documentation provided is not guaranteed to be
              up-to-date, and may not reflect facts specific to your business.
            </div>
          </VendorClassifier>
        </SurveyScanEffect>
      </div>
    </GetCompliantPageWrapper>
  );
};

const useNextInfo = (org: OrganizationDto, recipients: OrganizationDataRecipientDto[]) => {
  return useMemo(
    () => GetCompliantOrder.ClassifyServiceVendors.next(org, recipients),
    [org, recipients],
  );
};
