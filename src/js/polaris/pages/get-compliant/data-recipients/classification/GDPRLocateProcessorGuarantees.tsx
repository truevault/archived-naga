import { Link } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { useSurvey } from "../../../../../common/hooks/useSurvey";
import { NetworkRequest } from "../../../../../common/models";
import { GetCompliantStep } from "../../../../../common/service/server/Types";
import { Loading } from "../../../../components";
import { Callout, CalloutVariant } from "../../../../components/Callout";
import { GDPRProcessorComplianceDetailsDialog } from "../../../../components/dialog/GDPRProcessorComplianceDetailsDialog";
import { GDPRProcessorLanguageLocator } from "../../../../components/get-compliant/vendors/GDPRProcessorLanguageLocator";
import { GetCompliantPageWrapper } from "../../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../../components/layout/header/Header";
import { SurveyGateButton } from "../../../../components/organisms/survey/SurveyGateButton";
import { Para } from "../../../../components/typography/Para";
import { Paras } from "../../../../components/typography/Paras";
import { useDataRecipients, usePrimaryOrganizationId } from "../../../../hooks";
import { VENDOR_CATEGORY_AD_NETWORK } from "../../../../types/Vendor";
import { recipientsByGdprProcessorRec } from "../../../../util/vendors";
import { GetCompliantFooter } from "../../ProgressFooter";

interface LocateOtherVendorsProps {
  currentProgress: GetCompliantStep;
}

const VENDOR_CLASSIFICATION_SURVEY_NAME = "vendor-classification-survey";

const EXCLUDE_RECIPIENT_CATEGORIES = [VENDOR_CATEGORY_AD_NETWORK];

export const GDPRLocateProcessorGuarantees = ({ currentProgress }: LocateOtherVendorsProps) => {
  const organizationId = usePrimaryOrganizationId();
  const [readyToRender, setReadyToRender] = useState(false);

  const {
    request: dataRecipientsRequest,
    results: dataRecipients,
    updateItem: updateDataRecipient,
  } = useDataRecipients(organizationId, {
    excludeRecipientCategories: EXCLUDE_RECIPIENT_CATEGORIES,
    collectionContext: ["CONSUMER", "EMPLOYEE"],
  });

  const { other: unclassifiedDataRecipients } = useMemo(
    () => recipientsByGdprProcessorRec(dataRecipients ?? []),
    [dataRecipients],
  );

  const { answers, setAnswer, surveyRequest } = useSurvey(
    organizationId,
    VENDOR_CLASSIFICATION_SURVEY_NAME,
  );

  useEffect(() => {
    if (NetworkRequest.areFinished(dataRecipientsRequest, surveyRequest)) {
      setReadyToRender(true);
    }
  }, [dataRecipientsRequest, surveyRequest]);

  const [helpOpen, setHelpOpen] = useState(false);
  const closeHelp = () => setHelpOpen(false);

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper currentProgress={currentProgress}>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  const allFinished = unclassifiedDataRecipients.every(
    (r) =>
      (r.gdprProcessorSetting === "Controller" &&
        (Boolean(r.gdprControllerGuaranteeUrl) || Boolean(r.gdprControllerGuaranteeFileKey))) ||
      (r.gdprProcessorSetting === "Processor" &&
        (Boolean(r.gdprProcessorGuaranteeUrl) || Boolean(r.gdprProcessorGuaranteeFileKey))) ||
      r.gdprProcessorSetting === "Unknown",
  );

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <GetCompliantFooter
          presentStep="Classification.GDPRFindAgreements"
          nextDisabled={!allFinished}
          nextDisabledTooltip="Please make a selection for each vendor"
        />
      }
    >
      <PageHeader
        titleContent="GDPR Research"
        descriptionContent={
          <>
            <Paras>
              <Para>
                We are unable to make a Processor or Controller recommendation for your vendors
                below. In this step, you’ll do an online search for each vendor’s documentation to
                see if they self-classify as a Processor or a Controller.
              </Para>
              <Callout variant={CalloutVariant.LightPurple} className="mb-md">
                {unclassifiedDataRecipients.map((v) => v.name).join(", ")}
              </Callout>
              <Para>
                These recipients can be Processors under GDPR only if they make certain guarantees.
                Specifically, their contracts must impose certain obligations for the processor,
                limiting data processing activities to the controller’s documented instructions.{" "}
                <Link
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    setHelpOpen(true);
                  }}
                >
                  See the full list of requirements
                </Link>
                .
              </Para>
            </Paras>
          </>
        }
      />

      <div className="mt-lg">
        <SurveyGateButton survey={answers} slug="gdpr-continue" updateResponse={setAnswer}>
          <Paras>
            <Para>
              In this step, you’ll look for the necessary guarantees in a written agreement. For
              each vendor, you should look in the following locations, in this order:
            </Para>

            <ol className="custom p-0">
              <li className="pb-md">
                A Data Processing Agreement (DPA), often available on the vendor’s website
              </li>
              <li className="pb-md">
                A service contract that your business signed or clicked to accept, if available
              </li>
              <li className="pb-md">
                Terms of service or other service contract available on the vendor’s website
              </li>
              <li className="pb-md">
                Privacy policy, if it deals with the personal data of your consumers (not just
                theirs)
              </li>
            </ol>
          </Paras>

          <SurveyGateButton
            survey={answers}
            slug="gdpr-get-started"
            updateResponse={setAnswer}
            label="Get Started"
          >
            <GDPRProcessorLanguageLocator
              recipients={unclassifiedDataRecipients}
              updateVendor={updateDataRecipient}
            />
          </SurveyGateButton>
        </SurveyGateButton>
      </div>
      <GDPRProcessorComplianceDetailsDialog open={helpOpen} onClose={closeHelp} />
    </GetCompliantPageWrapper>
  );
};
