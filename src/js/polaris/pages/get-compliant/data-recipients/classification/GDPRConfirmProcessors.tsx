import { Check } from "@mui/icons-material";
import React, { useEffect, useMemo, useState } from "react";
import { ExternalLink } from "../../../../../common/components/ExternalLink";
import { useSurvey } from "../../../../../common/hooks/useSurvey";
import { NetworkRequest } from "../../../../../common/models";
import {
  GDPRProcessorRecommendation,
  OrganizationDataRecipientDto,
} from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../../../common/service/server/dto/OrganizationDto";
import { GetCompliantStep } from "../../../../../common/service/server/Types";
import { Loading } from "../../../../../common/components/Loading";
import { EmptyInfo } from "../../../../components/Empty";
import { ClassifyVendorsFooter } from "../../../../components/get-compliant/vendors/ClassifyVendorsFooter";
import { GetCompliantPageWrapper } from "../../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../../components/layout/header/Header";
import { DataRecipientRadioOptions } from "../../../../components/organisms/data-recipients/DataRecipientRadioOptions";
import { SurveyGateButton } from "../../../../components/organisms/survey/SurveyGateButton";
import { useOrganizationVendors, usePrimaryOrganization } from "../../../../hooks";
import { useUpdateVendor } from "../../../../hooks/get-compliant/vendors/useUpdateVendor";
import { GetCompliantOrder } from "../../../../util/GetCompliantClassificationOrder";
import { recipientsByGdprProcessorRecOrLockedSetting } from "../../../../util/vendors";
import { useRequireGdpr } from "../ProcessingRegion";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../shared";
import { SurveyScanEffect } from "../../../../components/organisms/survey/SurveyScanEffect";

type ClassifyVendorsProps = {
  currentProgress: GetCompliantStep;
};

export const GDPRConfirmProcessors = ({ currentProgress }: ClassifyVendorsProps) => {
  // Variables
  const [org] = usePrimaryOrganization();
  const orgId = org.id;

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  // Requests
  const [orgVendors, orgVendorsRequest] = useOrganizationVendors(
    orgId,
    undefined,
    undefined,
    undefined,
    ["CONSUMER", "EMPLOYEE"],
  );

  const { processorRecommendation, controllerRecommendation } = useMemo(
    () => recipientsByGdprProcessorRecOrLockedSetting(orgVendors ?? []),
    [orgVendors],
  );

  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, SELLING_AND_SHARING_SURVEY_NAME);

  const { updateVendor, updateVendorRequest } = useUpdateVendor(orgId, processorRecommendation);

  const updateRecipientGdprProcessorSetting = async (
    recipient: OrganizationDataRecipientDto,
    setting: GDPRProcessorRecommendation,
  ) => {
    await updateVendor({ vendorId: recipient.vendorId, gdprProcessorSetting: setting });
  };

  useEffect(() => {
    if (NetworkRequest.areFinished(orgVendorsRequest, surveyRequest)) {
      setReadyToRender(true);
    }
  }, [orgVendorsRequest, surveyRequest]);

  const nextInfo = useNextInfo(org, orgVendors);

  useRequireGdpr(nextInfo.url);

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper currentProgress={currentProgress}>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  const hasStarted = answers["confirm-gdpr-processors-acknowledgement"] == "true";
  const hasControllers = Boolean(controllerRecommendation?.length);
  const doneWithProcessors = answers["done-with-gdpr-processor-review"] == "true";

  const isFinished = hasStarted && (!hasControllers || doneWithProcessors);

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <ClassifyVendorsFooter
          organizationId={orgId}
          currentRequest={updateVendorRequest}
          nextDisabled={!isFinished}
          prev={GetCompliantOrder.GDPRConfirmProcessors.prev(orgVendors)}
          next={nextInfo}
          currentProgress={currentProgress}
          disabledNextTooltip="Please make a selection for each vendor"
        />
      }
    >
      <PageHeader
        titleContent="GDPR Documentation"
        descriptionContent={
          <>
            <p>
              To be considered a "processor" under GDPR, a vendor must have sufficient guarantees in
              place about the security of its data processing and must adhere to an approved code of
              conduct. Otherwise, the vendor is a “controller” of personal data it handles in
              relation to your business.
            </p>
            <p>
              We’ve automatically designated the Recipients below as "Processors" based on our
              research. Review the documentation and change the selection if you disagree.
            </p>
          </>
        }
      />

      <SurveyScanEffect
        slug="confirm-gdpr-processors-acknowledgement"
        survey={SELLING_AND_SHARING_SURVEY_NAME}
        label="Scan my Vendors"
      >
        <div className="mt-xl">
          {processorRecommendation?.length ? (
            <>
              {processorRecommendation?.map((recipient) => {
                return (
                  <VendorRadioSelection
                    key={recipient.id}
                    recipient={recipient}
                    disabled={recipient.isGdprProcessorSettingLocked}
                    disabledTooltip="This classification is required based on your previous answers."
                    onChange={(val) => updateRecipientGdprProcessorSetting(recipient, val)}
                  />
                );
              })}
            </>
          ) : (
            <EmptyInfo Icon={Check}>
              None of your data recipients require Classification Review.
            </EmptyInfo>
          )}
        </div>

        {hasControllers && (
          <div className="mt-xl">
            <SurveyGateButton
              slug="done-with-gdpr-processor-review"
              survey={answers}
              updateResponse={setAnswer}
              label="Done Reviewing Processors"
            >
              <h4>Confirm GDPR Controllers</h4>
              <p>
                We've automatically designated the Recipients below as "Controllers" based on our
                research. Review the documentation and change the selection if you disagree.{" "}
                <strong>
                  Note that your Data Recipients who are Controllers will be disclosed by name in
                  your Privacy Policy.
                </strong>
              </p>
              {controllerRecommendation?.map((recipient) => {
                return (
                  <VendorRadioSelection
                    key={recipient.id}
                    recipient={recipient}
                    onChange={(val) => updateRecipientGdprProcessorSetting(recipient, val)}
                    disabled={recipient.isGdprProcessorSettingLocked}
                    disabledTooltip="This classification is required based on your previous answers."
                  />
                );
              })}
            </SurveyGateButton>
          </div>
        )}
      </SurveyScanEffect>
    </GetCompliantPageWrapper>
  );
};

type VendorRowProps = {
  recipient: OrganizationDataRecipientDto;
  onChange: (updated: GDPRProcessorRecommendation) => void;
  disabled?: boolean;
  disabledTooltip?: string;
};

const VendorRadioSelection: React.FC<VendorRowProps> = ({
  recipient,
  onChange,
  disabled,
  disabledTooltip,
}) => {
  const [value, setValue] = useState<GDPRProcessorRecommendation | null>(
    recipient.gdprProcessorSetting || recipient.gdprProcessorRecommendation,
  );

  const handleChange = (updated: GDPRProcessorRecommendation) => {
    setValue(updated);
    onChange(updated);
  };

  const description = (
    <>
      {recipient.gdprProcessorLanguageUrl && (
        <ExternalLink
          className="text-t2 text-medium text-link"
          href={recipient.gdprProcessorLanguageUrl}
        >
          View documentation
        </ExternalLink>
      )}
    </>
  );

  return (
    <DataRecipientRadioOptions
      recipient={recipient}
      description={description}
      options={[{ value: "Processor" }, { value: "Controller" }]}
      value={value}
      onChange={handleChange}
      disabled={disabled}
      disabledTooltip={disabledTooltip}
    />
  );
};

const useNextInfo = (org: OrganizationDto, recipients: OrganizationDataRecipientDto[]) => {
  return useMemo(
    () => GetCompliantOrder.GDPRConfirmProcessors.next(org, recipients),
    [org, recipients],
  );
};
