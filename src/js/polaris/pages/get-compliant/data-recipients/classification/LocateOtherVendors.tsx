import { Button } from "@mui/material";
import React, { useMemo } from "react";
import { useSurvey } from "../../../../../common/hooks/useSurvey";
import { OrganizationDataRecipientDto } from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../../../common/service/server/dto/OrganizationDto";
import {
  VendorClassificationDto,
  VendorContractReviewedDto,
} from "../../../../../common/service/server/dto/VendorDto";
import { GetCompliantStep } from "../../../../../common/service/server/Types";
import { Callout, CalloutVariant } from "../../../../components/Callout";
import { ClassifyVendorsFooter } from "../../../../components/get-compliant/vendors/ClassifyVendorsFooter";
import { ClassifyVendorsHeader } from "../../../../components/get-compliant/vendors/ClassifyVendorsHeader";
import { OtherVendorsLocator } from "../../../../components/get-compliant/vendors/OtherVendorsLocator";
import { GetCompliantPageWrapper } from "../../../../components/layout/GetCompliantPageWrapper";
import {
  useOrganizationVendors,
  usePrimaryOrganization,
  usePrimaryOrganizationId,
} from "../../../../hooks";
import { useUpdateVendor } from "../../../../hooks/get-compliant/vendors/useUpdateVendor";
import { alphabetically } from "../../../../surveys/util";
import { GetCompliantOrder } from "../../../../util/GetCompliantClassificationOrder";
import { isPayPal, vendorsByType } from "../../../../util/vendors";

type LocateOtherVendorsProps = {
  currentProgress: GetCompliantStep;
};

const VENDOR_CLASSIFICATION_SURVEY_NAME = "vendor-classification-survey";
export const LocateOtherVendors = ({ currentProgress }: LocateOtherVendorsProps) => {
  // Variables
  const [org] = usePrimaryOrganization();
  const organizationId = usePrimaryOrganizationId();

  // Requests
  const [orgVendors, orgVendorsRequest, refresh] = useOrganizationVendors(
    organizationId,
    undefined,
    undefined,
    undefined,
    ["CONSUMER"],
  );
  const { otherVendors } = useMemo(() => vendorsByType(orgVendors || []), [orgVendors]);
  const unknownVendors = useUnknownVendors(otherVendors);
  const { updateVendor, updateVendorRequest } = useUpdateVendor(organizationId, unknownVendors);

  const {
    answers,
    setAnswer: updateSurveyQuestions,
    surveyRequest,
  } = useSurvey(organizationId, VENDOR_CLASSIFICATION_SURVEY_NAME);

  const setAnswer = (slug: string) => {
    updateSurveyQuestions(slug, "true");
  };

  const vendorContractReviewedBySlug = useMemo(
    () =>
      orgVendors?.[0]?.vendorContractReviewedOptions?.reduce(
        (acc, c) => Object.assign(acc, { [c.slug]: c }),
        {} as Record<string, VendorContractReviewedDto>,
      ),
    [orgVendors],
  );

  const vendorClassificationBySlug = useMemo(
    () =>
      orgVendors?.[0]?.classificationOptions?.reduce(
        (acc, c) => Object.assign(acc, { [c.slug]: c }),
        {} as Record<string, VendorClassificationDto>,
      ),
    [orgVendors],
  );

  //  const answersChanged = useCallback((): void => {
  const allResponded = useMemo(
    () =>
      unknownVendors.every(
        (vendor) =>
          vendor.vendorContractReviewed != null &&
          (vendor.vendorContractReviewed.id ===
            vendorContractReviewedBySlug["provider-language-not-found"].id ||
            (vendor.vendorContractReviewed.id ===
              vendorContractReviewedBySlug["provider-language-found"].id &&
              (vendor.tosFileKey != null || vendor.publicTosUrl != null))),
      ),
    [unknownVendors, vendorContractReviewedBySlug],
  );

  const next = useNextInfo(
    org,
    orgVendors,
    unknownVendors,
    vendorContractReviewedBySlug && vendorContractReviewedBySlug["provider-language-not-found"],
  );

  return (
    <GetCompliantPageWrapper
      requests={[orgVendorsRequest, surveyRequest]}
      currentProgress={currentProgress}
      progressFooter={
        <ClassifyVendorsFooter
          organizationId={organizationId}
          currentRequest={updateVendorRequest}
          nextDisabled={!allResponded}
          prev={GetCompliantOrder.LocateOtherVendors.prev(orgVendors)}
          next={next}
          currentProgress={currentProgress}
          disabledNextTooltip="Please make a selection for each vendor"
        />
      }
    >
      <ClassifyVendorsHeader title="Vendor Research" />
      <div className="w-896">
        <div className="mt-xl text-t3">
          <p>
            We are unable to make a Service Provider recommendation for your vendors listed below,
            either because they are not in our database, or because we could not locate the
            necessary language in our research.
          </p>

          <Callout
            variant={CalloutVariant.LightPurple}
            className="mt-mdlg mb-xl text-medium w-640 text-weight-bold"
          >
            {unknownVendors.map((v) => v.name).join(", ")}
          </Callout>

          <p>
            These vendors can be Service Providers under the CCPA only if there’s a written
            agreement prohibiting them from using your consumers’ personal data for any other
            purpose than providing a service to you. For some vendors, the agreement may not be
            publicly available and will need to be requested.
          </p>
        </div>

        {!answers["continue"] ? (
          <Button
            className="w-192 h-48"
            color="default"
            variant="outlined"
            onClick={() => setAnswer("continue")}
          >
            Continue
          </Button>
        ) : (
          <>
            <div className="text-t3 mb-mdlg">
              <p className="mt-0">
                In this step, you’ll gather any agreements or service terms for review. We’ll show
                you the language you’re looking for, and you’ll determine if it exists in any of
                those agreements.
              </p>
              <p>For each vendor, you should look in the following locations, in this order:</p>
            </div>

            <div className="text-t3 mb-xl">
              <ol className="custom p-0">
                <li className="pb-md">
                  A service contract that your business signed or clicked to accept, if available
                </li>
                <li className="pb-md">
                  A Data Processing Agreement (DPA), often available on the vendor’s website
                </li>
                <li className="pb-md">
                  Terms of service or other service contract available on the vendor’s website
                </li>
                <li className="pb-md">
                  Privacy policy, if it deals with the personal data of your consumers (not just
                  theirs)
                </li>
              </ol>
            </div>

            {!answers["get-started"] ? (
              <Button
                className="w-192 h-48"
                variant="outlined"
                onClick={() => setAnswer("get-started")}
              >
                Get Started
              </Button>
            ) : (
              <>
                <div className="text-t3 mb-mdlg">
                  <p className="mt-0">Look for statements like this for each vendor:</p>
                </div>

                <Callout variant={CalloutVariant.LightPurple} className="mb-xl">
                  <p>"[Vendor] is a 'service provider' as defined in the CCPA."</p>

                  <p>or</p>
                  <p className="mb-xl">
                    "[Vendor] will not sell, retain, use, or disclose Personal Information for any
                    purpose other than for the specific purpose of performing the services specified
                    herein."
                  </p>
                  <p>
                    <i>
                      Note: The language does not have to be verbatim, but it does have to have the
                      same effect of restricting the use of your consumers' personal information to
                      what is necessary to provide their service.
                    </i>
                    <a
                      href="https://help.truevault.com/article/154-identifying-service-provider-language"
                      target="_blank"
                      rel="noopener noreferrer"
                      className="text-normal ml-xxs"
                    >
                      See more examples
                    </a>
                  </p>
                </Callout>
              </>
            )}
          </>
        )}
        {answers["continue"] && answers["get-started"] && (
          <OtherVendorsLocator
            vendors={unknownVendors}
            contractOptions={vendorContractReviewedBySlug}
            updateVendor={updateVendor}
            vendorCallback={refresh}
            classificationOptions={vendorClassificationBySlug}
          />
        )}
      </div>
    </GetCompliantPageWrapper>
  );
};

const useNextInfo = (
  org: OrganizationDto,
  recipients: OrganizationDataRecipientDto[],
  unknownRecipients: OrganizationDataRecipientDto[],
  languageNotFoundOption: VendorContractReviewedDto,
) => {
  return useMemo(
    () =>
      GetCompliantOrder.LocateOtherVendors.next(
        org,
        recipients,
        unknownRecipients,
        languageNotFoundOption,
      ),
    [org, unknownRecipients, languageNotFoundOption],
  );
};

const useUnknownVendors = (
  vendors: OrganizationDataRecipientDto[],
): OrganizationDataRecipientDto[] => {
  return useMemo(
    () =>
      vendors
        .filter(
          (ov) =>
            ov.processingRegions.includes("UNITED_STATES") &&
            (((ov.classification?.slug == "needs-review" ||
              ov.classification?.slug == "service-provider") &&
              !isPayPal(ov) &&
              ov.serviceProviderRecommendation != "THIRD_PARTY") ||
              ov.vendorContractReviewed?.slug === "provider-language-not-found"),
        )
        .sort(alphabetically),
    [vendors],
  );
};
