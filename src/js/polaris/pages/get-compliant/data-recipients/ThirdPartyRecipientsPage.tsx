import {
  Add,
  Analytics,
  FlutterDash,
  Groups,
  Handshake,
  SupervisorAccount,
} from "@mui/icons-material";
import { Autocomplete, Stack, TextField } from "@mui/material";
import React, { useMemo, useState } from "react";
import { Form, useField } from "react-final-form";
import { SelectOption } from "../../../../common/components/input/Select";
import { Text } from "../../../../common/components/input/Text";
import { LoadingButton } from "../../../../common/components/LoadingButton";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RemoveVendorDialog } from "../../../components/dialog/RemoveVendorDialog";
import { SurveyBooleanQuestion } from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { CategoryCollapseCard } from "../../../components/organisms/vendor/CategoryCollapseCard";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { useAddThirdPartyRecipient } from "../../../hooks/useOrganizationVendors";
import { composeValidators, validateNotIn, validateRequired } from "../../../util/validation";
import { GetCompliantFooter, ProgressFooterSaveNotification } from "../ProgressFooter";
import { CustomVendorFormValues } from "./AddDataRecipients";
import { QuestionHeading } from "../../../components/vendor/QuestionHeading";

const THIRD_PARTY_RECIPIENTS_SURVEY = "third-party-recipients-survey";

const TPR_DOES_NOT_APPLY_SLUG = "third-party-recipients-does-not-apply";

const COMARKETER_CATEGORY = "Co-Marketing Partner";
const DATA_BUYER_CATEGORY = "Data Buyer";
const PARTNER_CATEGORY = "Business Partner";
const AFFILIATE_CATEGORY = "Affiliate";

export const THIRD_PARTY_CATEGORIES = [
  "Co-Marketing Partner",
  "Data Buyer",
  "Business Partner",
  "Affiliate",
];

export const ThirdPartyRecipientsPage = (): React.ReactElement => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // Modal States
  const [deleteTpr, setDeleteTpr] = useState<OrganizationDataRecipientDto | null>(null);
  const openDeleteModal = (tpr: OrganizationDataRecipientDto) => setDeleteTpr(tpr);
  const closeDeleteModal = () => setDeleteTpr(null);

  const { answers, setAnswer, surveyRequest } = useSurvey(
    organizationId,
    THIRD_PARTY_RECIPIENTS_SURVEY,
  );

  const [organizationVendors, organizationVendorsRequest, refresh] =
    useOrganizationVendors(organizationId);

  const thirdParty = useMemo(
    () => organizationVendors?.filter((v) => v.dataRecipientType === "third_party_recipient") || [],
    [organizationVendors],
  );

  const comarketerParties = thirdParty.filter((tp) => tp.category == COMARKETER_CATEGORY);
  const buyerParties = thirdParty.filter((tp) => tp.category == DATA_BUYER_CATEGORY);
  const partnerParties = thirdParty.filter((tp) => tp.category == PARTNER_CATEGORY);
  const affiliateParties = thirdParty.filter((tp) => tp.category == AFFILIATE_CATEGORY);
  const otherParties = thirdParty.filter(
    (tp) =>
      ![COMARKETER_CATEGORY, DATA_BUYER_CATEGORY, PARTNER_CATEGORY, AFFILIATE_CATEGORY].includes(
        tp.category,
      ),
  );

  const hasTpr = useMemo(
    () => answers[TPR_DOES_NOT_APPLY_SLUG] === "true",
    [answers[TPR_DOES_NOT_APPLY_SLUG]],
  );

  const isFinished = thirdParty.length > 0 || answers[TPR_DOES_NOT_APPLY_SLUG] === "false";

  const nextDisabled = !isFinished;

  const { fetch: addThirdPartyRecipient, request: addThirdPartyRecipientRequest } =
    useAddThirdPartyRecipient(organizationId, () => refresh(), ["CONSUMER"]);

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRecipients.ThirdPartyRecipients"
          nextDisabled={nextDisabled}
          nextDisabledTooltip={"Please complete recipients before proceeding."}
          content={<ProgressFooterSaveNotification request={addThirdPartyRecipientRequest} />}
        />
      }
      requests={[organizationVendorsRequest, surveyRequest]}
    >
      <PageHeader
        titleContent="Other Parties"
        descriptionContent={
          <Paras>
            <Para>
              In addition to your vendors, your business may disclose consumer data to other parties
              that may not provide an explicit service for your business. These may include
              co-marketing partners, data buyers, affiliates, investors, or business partners. Add
              those parties below if they handle consumer data. For any that you added on the
              previous screen, they do not need to be added again here.
            </Para>
          </Paras>
        }
      />

      <div className="mb-xl">
        <SurveyBooleanQuestion
          question={{
            slug: TPR_DOES_NOT_APPLY_SLUG,
            question: "Does your business disclose data to any third-party data recipients?",
            visible: true,
            type: "boolean",
            isDisabled: thirdParty.length > 0,
            disabledReason: "You have at least one third-party data recipient.",
          }}
          answer={answers[TPR_DOES_NOT_APPLY_SLUG]}
          onAnswer={(ans) => setAnswer(TPR_DOES_NOT_APPLY_SLUG, ans)}
        />
      </div>
      {hasTpr && (
        <div>
          <div className="mb-md">
            <QuestionHeading condensed question="Add each of your third-party data recipients." />
          </div>

          <div className="mb-lg">
            <AddTprForm
              categories={THIRD_PARTY_CATEGORIES.map((o) => ({ label: o, value: o }))}
              existing={thirdParty.map((tp) => tp.name)}
              onSubmit={addThirdPartyRecipient}
            />
          </div>
          <Stack spacing={2}>
            <CategoryCollapseCard
              category={{
                label: COMARKETER_CATEGORY,
                icon: SupervisorAccount,
                description:
                  "Other businesses with which you share and receive customer contact information.",
                category: COMARKETER_CATEGORY,
              }}
              vendors={comarketerParties}
              handleRemove={(v) => openDeleteModal(v as unknown as OrganizationDataRecipientDto)}
            />

            <CategoryCollapseCard
              category={{
                label: DATA_BUYER_CATEGORY,
                icon: Analytics,
                description:
                  "If you provide customer data to other parties in exchange for money or a service.",
                category: DATA_BUYER_CATEGORY,
              }}
              vendors={buyerParties}
              handleRemove={(v) => openDeleteModal(v as unknown as OrganizationDataRecipientDto)}
            />

            <CategoryCollapseCard
              category={{
                label: PARTNER_CATEGORY,
                icon: Handshake,
                description:
                  "Any business you work with that does not provide a service and that has access to your customer data.",
                category: PARTNER_CATEGORY,
              }}
              vendors={partnerParties}
              handleRemove={(v) => openDeleteModal(v as unknown as OrganizationDataRecipientDto)}
            />

            <CategoryCollapseCard
              category={{
                label: AFFILIATE_CATEGORY,
                icon: FlutterDash,
                description:
                  "Another company that has an ownership interest or other relationship with your business.",
                category: AFFILIATE_CATEGORY,
              }}
              vendors={affiliateParties}
              handleRemove={(v) => openDeleteModal(v as unknown as OrganizationDataRecipientDto)}
            />

            <CategoryCollapseCard
              category={{
                label: "Other Data Recipients",
                icon: Groups,
                category: "Other",
              }}
              vendors={otherParties}
              handleRemove={(v) => openDeleteModal(v as unknown as OrganizationDataRecipientDto)}
            />
          </Stack>
        </div>
      )}
      <RemoveVendorDialog
        open={Boolean(deleteTpr)}
        onClose={closeDeleteModal}
        vendorName={deleteTpr?.name}
        vendorId={deleteTpr?.vendorId}
        collectionContext={["CONSUMER"]}
      />
    </GetCompliantPageWrapper>
  );
};

export const AddTprForm: React.FC<{
  onSubmit: (v: CustomVendorFormValues) => Promise<void>;
  existing: string[];
  categories: SelectOption[];
}> = ({ onSubmit, existing = [], categories = [] }) => {
  return (
    <Form onSubmit={onSubmit}>
      {(formProps) => (
        <form onSubmit={(e) => formProps.handleSubmit(e).then(() => formProps.form.restart())}>
          <Stack direction="row" spacing={2} alignItems="flex-start">
            <Text
              required
              field="name"
              placeholder="Name"
              label={null}
              validate={composeValidators(
                validateRequired,
                validateNotIn(
                  existing,
                  "A Third Party Recipient already exists with that name",
                  false,
                ),
              )}
              style={{ margin: 0, flex: 1 }}
            />
            <CategoryAutocomplete categories={categories} />

            <LoadingButton
              type="submit"
              color="primary"
              size="large"
              loading={formProps.submitting}
              disabled={formProps.pristine}
              startIcon={<Add />}
            >
              Add Recipient
            </LoadingButton>
          </Stack>
        </form>
      )}
    </Form>
  );
};

type CategoryAutocompleteProps = {
  categories: SelectOption[];
};
const CategoryAutocomplete: React.FC<CategoryAutocompleteProps> = ({ categories }) => {
  const [isFocused, setIsFocused] = useState<boolean>(false);
  const { input } = useField("category");

  const label = useMemo(
    () => (isFocused || input.value ? "Choose a category or create your own" : "Category"),
    [isFocused, input.value],
  );

  return (
    <Autocomplete
      id="category-autocomplete"
      className="w-300"
      options={Array.from(new Set([...categories])).sort()}
      value={input.value}
      onInputChange={(_event, value) => input.onChange(value)}
      freeSolo
      renderInput={(params) => {
        return (
          <TextField
            {...params}
            id="category"
            fullWidth={true}
            variant="outlined"
            placeholder={"Category"}
            label={label}
            InputProps={{ ...params.InputProps, type: "text" }}
            InputLabelProps={{ required: false }}
            required
            onFocus={() => setIsFocused(true)}
            onBlur={() => setIsFocused(false)}
          />
        );
      }}
      autoCorrect="off"
      autoCapitalize="off"
      spellCheck={false}
    />
  );
};
