import { Alert } from "@mui/material";
import _ from "lodash";
import React, { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router";
import { NetworkRequestState } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import { CollectionGroupType } from "../../../../common/service/server/controller/CollectionGroupsController";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../../../common/service/server/dto/DataMapDto";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
  ProcessingRegion,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationPublicId, UUIDString } from "../../../../common/service/server/Types";
import { Loading } from "../../../components";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import {
  useOrganizationVendors,
  usePrimaryOrganization,
  usePrimaryOrganizationId,
} from "../../../hooks";
import { EXCLUDE_GROUP_EMPLOYMENT, useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../hooks/useDataMap";
import { useProgressKeys } from "../../../hooks/useProgressKeys";
import { useGetCompliantContext } from "../../../root/GetCompliantContext";
import { Routes } from "../../../root/routes";
import { toggleSelection } from "../../../surveys/util";
import { useNextStep } from "../../../surveys/steps/GetCompliantSteps";
import {
  DataRecipientMultiselect,
  DataRecipientMultiselectProps,
} from "../../design/molecules/DataRecipientMultiselect";
import { GetCompliantFooter } from "../ProgressFooter";

export const VendorAssociationForGroup: React.FC = () => {
  const { progress } = useGetCompliantContext();

  // Variables
  const [org] = usePrimaryOrganization();
  const params = useParams() as { groupId: UUIDString };
  const orgId = usePrimaryOrganizationId();

  const { group, fetched, requests, multiselect, error } = useVendorAssociation(
    orgId,
    params.groupId,
    EXCLUDE_GROUP_EMPLOYMENT,
  );

  // State
  const isOnlyGroup = fetched?.collectionGroups?.length == 1;

  const keys = useMemo(
    () => fetched?.collectionGroups?.map((g) => `vendor-association-${g.id}`),
    [fetched?.collectionGroups],
  );

  const { update: updateProgress } = useProgressKeys(orgId, keys);

  const onNext = async () => {
    const key = `vendor-association-${group.id}`;
    await updateProgress({ keyId: key, progress: "DONE" });
  };

  const [handleAutoNext] = useNextStep("DataRecipients.VendorAssociation", async () => {
    await onNext();

    await Promise.all(
      fetched.recipients.map((recipient) =>
        Api.dataMap.updateRecipientAssociation(org.id, group.id, recipient.vendorId, true),
      ),
    );
  });

  const nextUrl = isOnlyGroup ? undefined : Routes.getCompliant.dataRecipients.vendorAssociation;
  const prevUrl = isOnlyGroup ? undefined : Routes.getCompliant.dataRecipients.vendorAssociation;
  const noProgress = isOnlyGroup ? false : true;

  // automatically associate everything and go to the next screen if there's only a single group
  useEffect(() => {
    if (isOnlyGroup && group?.id) {
      handleAutoNext();
    }
  }, [isOnlyGroup, group, handleAutoNext]);

  if (isOnlyGroup) {
    return (
      <GetCompliantPageWrapper currentProgress={progress}>
        <PageHeader titleContent="" />
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  if (error) {
    return (
      <GetCompliantPageWrapper currentProgress={progress}>
        <PageHeader titleContent="" />
        <Alert color="error">There was an error while fetching this data. Please try again.</Alert>
      </GetCompliantPageWrapper>
    );
  }

  const isFinished = true;

  return (
    <GetCompliantPageWrapper
      currentProgress={progress}
      requests={[requests.collectionGroups, requests.dataMap, requests.recipients]}
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRecipients.VendorAssociation"
          nextUrl={nextUrl}
          nextOnClick={onNext}
          nextDisabled={!isFinished}
          prevUrl={prevUrl}
          noProgress={noProgress}
        />
      }
    >
      <PageHeader titleContent="Associations" />

      <h4 className="intermediate">
        Which of your Data Recipients process information about {group?.name}?
      </h4>

      <DataRecipientMultiselect {...multiselect} />
    </GetCompliantPageWrapper>
  );
};

export type UseVendorAssociation = {
  error: boolean;
  selected: string[];
  group: CollectionGroupDetailsDto | undefined;
  fetched: {
    collectionGroups: CollectionGroupDetailsDto[];
    dataMap: DataMapDto;
    recipients: OrganizationDataRecipientDto[];
  };
  requests: {
    collectionGroups: NetworkRequestState;
    recipients: NetworkRequestState;
    dataMap: NetworkRequestState;
  };
  multiselect: DataRecipientMultiselectProps;
};

export const useVendorAssociation = (
  orgId: OrganizationPublicId,
  groupId: string,
  excludeGroups: CollectionGroupType[],
  requiredProcessingRegion?: ProcessingRegion,
  collectionContext: CollectionContext[] = ["CONSUMER"],
): UseVendorAssociation => {
  const [selected, setSelected] = useState<string[]>([]);

  const [groups, groupsRequest] = useCollectionGroups(orgId, undefined, excludeGroups);
  const [allRecipients, recipientsRequest] = useOrganizationVendors(orgId);
  const [dataMap, dataMapRequest] = useDataMap(orgId);

  const recipients = useMemo(() => {
    let rec = allRecipients;

    if (requiredProcessingRegion) {
      rec = rec?.filter((r) => r.processingRegions.includes(requiredProcessingRegion)) || [];
    }

    if (collectionContext) {
      rec = rec?.filter((r) => _.intersection(r.collectionContext, collectionContext).length > 0);
    }

    return rec;
  }, [allRecipients, requiredProcessingRegion, collectionContext]);

  const onChange = ({ appId, included }) => {
    setSelected((prevSelected) => toggleSelection(prevSelected, appId, included));
    Api.dataMap.updateRecipientAssociation(orgId, groupId, appId, included);
  };

  const onChangeSelectAll = (selected: boolean) => {
    recipients.forEach((r) => onChange({ appId: r.vendorId, included: selected }));
  };

  const group = useMemo(() => groups?.find((g) => g.id == groupId), [groups, groupId]);

  // deserialize remote state into local state
  useEffect(() => {
    if (dataMap && group) {
      const associatedVendorIds = dataMap.disclosure
        .filter((d) => d.collectionGroupId == group.id)
        .map((d) => d.vendorId);
      setSelected(associatedVendorIds);
    }
  }, [dataMap, group]);

  const error = groupsRequest.error ?? recipientsRequest.error ?? dataMapRequest.error;

  return {
    error,
    selected,
    group,
    fetched: {
      recipients,
      dataMap,
      collectionGroups: groups,
    },
    requests: {
      recipients: recipientsRequest,
      dataMap: dataMapRequest,
      collectionGroups: groupsRequest,
    },
    multiselect: {
      recipients,
      selected,
      onChange,
      onChangeSelectAll,
    },
  };
};
