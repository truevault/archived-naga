import React, { useMemo } from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { SurveyRenderer } from "../../../components/get-compliant/survey/SurveyRenderer";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { hasIntentionalInteractionVendors, sellingSurvey } from "../../../surveys/sellingSurvey";
import { isFinished } from "../../../surveys/survey";
import { GetCompliantContainer } from "../GetCompliantContainer";
import { GetCompliantFooter } from "../ProgressFooter";
import { SELLING_AND_SHARING_SURVEY_NAME } from "./shared";
import { useSellingAndSharingAnswerEffects } from "./useSellingAndSharingAnswerEffects";

export const VendorSelling = () => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // Requests
  const [vendors, vendorsRequest, refreshVendors] = useOrganizationVendors(organizationId);

  // save/load current questions/answers
  const {
    answers,
    setAnswer,
    surveyRequest: surveyQuestionsRequest,
  } = useSurvey(organizationId, SELLING_AND_SHARING_SURVEY_NAME, refreshVendors);

  if (hasIntentionalInteractionVendors(vendors)) {
    answers["disclosure-for-intentional-interaction"] = "true";
  }

  useSellingAndSharingAnswerEffects(organizationId, answers, setAnswer);

  const survey = useMemo(() => sellingSurvey(answers, vendors), [answers, vendors]);
  const nextDisabled = useMemo(() => !isFinished(survey, answers), [survey, answers]);

  return (
    <GetCompliantPageWrapper
      requests={[vendorsRequest, surveyQuestionsRequest]}
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRecipients.DataSelling"
          nextDisabled={nextDisabled}
          nextDisabledTooltip="Please answer all questions before proceeding to the next step."
        />
      }
    >
      <PageHeader titleContent="Data Selling" />

      <GetCompliantContainer>
        <SurveyRenderer survey={survey} answers={answers} setAnswer={setAnswer} offsetFirst />
      </GetCompliantContainer>
    </GetCompliantPageWrapper>
  );
};
