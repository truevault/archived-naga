import React from "react";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import {
  DataRetentionModule,
  useDataRetentionModule,
} from "../../components/organisms/data-retention/DataRetentionModule";
import { SurveyGateButton } from "../../components/organisms/survey/SurveyGateButton";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { QuestionHeading } from "../../components/vendor/QuestionHeading";
import { GetCompliantFooter } from "./ProgressFooter";

export const RetentionPolicyPage: React.FC = () => {
  const dataRetention = useDataRetentionModule("retention-policy-done");
  const { answers, setAnswer } = dataRetention.props;

  return (
    <GetCompliantPageWrapper
      requests={[
        dataRetention.requests.surveyRequest,
        dataRetention.requests.dataMapRequest,
        dataRetention.requests.dataRetentionRequest,
      ]}
      progressFooter={
        <GetCompliantFooter
          saveRequests={[dataRetention.updateRequest]}
          presentStep="DataRetention.RetentionPolicy"
          nextDisabled={!dataRetention.isFinished}
          nextDisabledTooltip="Please make sure every collection category is selected and every selected purpose has a written description."
        />
      }
    >
      <PageHeader
        titleContent="Retention Policy"
        descriptionContent={
          <Paras>
            <Para>
              Privacy laws allow your business to retain consumer data only as long as necessary to
              serve the purpose for which it was collected. For each category of information you
              collect, your privacy policy must disclose either (a) the period for which your
              business stores personal data, or (b) the criteria used to determine that period.
            </Para>
            <Para>
              In this step, we’ll help you establish your business’s retention period for the data
              it collects.{" "}
            </Para>
          </Paras>
        }
      />

      <div className="mt-xl">
        <SurveyGateButton
          label="Got it"
          survey={answers}
          updateResponse={setAnswer}
          slug="retention-got-it"
        >
          <QuestionHeading
            question="Data Retention Details"
            helpText={
              <>
                For each set of data below, describe how long your business stores the data. Use the
                example text below as a starting point if you don’t have an existing retention
                policy. The table below will be included in your Privacy Policy with the
                introductory language provided. You can optionally edit or remove the intro language
                now, or at any time, in your Privacy Policy settings.
              </>
            }
          />

          <DataRetentionModule {...dataRetention.props} />
        </SurveyGateButton>
      </div>
    </GetCompliantPageWrapper>
  );
};
