import { Stack } from "@mui/material";
import { useQueryClient } from "@tanstack/react-query";
import React, { useMemo } from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { UUIDString } from "../../../../common/service/server/Types";
import {
  QuestionContainer,
  QuestionLabel,
} from "../../../components/get-compliant/survey/QuestionContainer";
import { SurveyQuestionContainer } from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import { SurveyRenderer } from "../../../components/get-compliant/survey/SurveyRenderer";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { DataSourceReceiptCards } from "../../../components/organisms/DataSourceReceiptCards";
import { DataSourceReceiptCheckboxes } from "../../../components/organisms/DataSourceReceiptCheckboxes";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { useReceiptsForCollectionGroups } from "../../../hooks/useReceiptsForCollectionGroups";
import {
  employeeSourcesSurvey,
  EMPLOYEE_OTHER_EXCLUDED_CATEGORIES,
} from "../../../surveys/employeeSourcesSurvey";
import { isFinished } from "../../../surveys/survey";
import {
  VENDOR_CATEGORY_APPLICANT_TRACKING,
  VENDOR_CATEGORY_BACKGROUND_CHECK_PROVIDER,
  VENDOR_CATEGORY_DATA_BROKER,
} from "../../../types/Vendor";
import { lexicalJoin } from "../../../util/text";
import { useUpdateDataSources } from "../data-map/ConsumerDataSourcesPage";
import { GetCompliantFooter, ProgressFooter } from "../ProgressFooter";

const EMPLOYEE_SOURCES_SURVEY_NAME = "employee-sources-survey";

export const EmployeeMapDataSourcesPage: React.FC = () => {
  const orgId = usePrimaryOrganizationId();

  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, EMPLOYEE_SOURCES_SURVEY_NAME);
  const [recipients, recipientsRequest] = useOrganizationVendors(
    orgId,
    undefined,
    undefined,
    undefined,
    ["EMPLOYEE"],
  );
  const [employmentGroups, groupsRequest] = useCollectionGroups(orgId, ["EMPLOYMENT"]);
  const receiptData = useReceiptsForCollectionGroups(orgId, employmentGroups, ["EMPLOYEE"]);
  const updateDataSources = useUpdateDataSources(orgId, { collectionContext: ["EMPLOYEE"] });
  const queryClient = useQueryClient();

  const hasApplicantTracking = useMemo(
    () =>
      updateDataSources.data.sources.some((s) => s.category == VENDOR_CATEGORY_APPLICANT_TRACKING),
    [updateDataSources.data.sources],
  );
  const hasDataBroker = useMemo(
    () => updateDataSources.data.sources.some((s) => s.category == VENDOR_CATEGORY_DATA_BROKER),
    [updateDataSources.data.sources],
  );
  const hasBackgroundCheck = useMemo(
    () =>
      updateDataSources.data.sources.some(
        (s) => s.category == VENDOR_CATEGORY_BACKGROUND_CHECK_PROVIDER,
      ),
    [updateDataSources.data.sources],
  );
  const hasOtherSource = useMemo(
    () =>
      updateDataSources.data.sources.some(
        (s) => !EMPLOYEE_OTHER_EXCLUDED_CATEGORIES.includes(s.category),
      ),
    [updateDataSources.data.sources],
  );

  const groupLabel = employmentGroups
    ? lexicalJoin(employmentGroups.map((g) => g.name))
    : "your employment groups";

  const survey = useMemo(
    () =>
      employeeSourcesSurvey(
        answers,
        groupLabel,
        updateDataSources.data.catalog,
        receiptData.sources,
        recipients,
        {
          handleAddVendor: updateDataSources.actions.handleAddVendor,
          handleAddCustomVendor: updateDataSources.actions.handleAddCustomVendor,
          handleRemoveVendor: updateDataSources.actions.handleRemoveVendor,
        },
      ),
    [
      answers,
      groupLabel,
      updateDataSources.data.catalog,
      receiptData.sources,
      recipients,
      updateDataSources.actions,
    ],
  );
  const surveyFinished = isFinished(survey, answers);

  const handleSetAnswer = async (slug: string, answer: string, question?: string) => {
    await setAnswer(slug, answer, question);
    if (slug == "worker-directly") {
      queryClient.invalidateQueries(["orgDataSources", orgId]);
    }
  };

  const dataBrokersDone = answers["data-brokers"] === "false" || hasDataBroker;
  const applicantTrackingDone =
    answers["applicant-tracking-systems"] === "false" || hasApplicantTracking;
  const backgroundCheckDone =
    answers["background-check-providers"] === "false" || hasBackgroundCheck;
  const otherDone = answers["other-sources"] === "false" || hasOtherSource;

  const everyDataSourceHasPIC = updateDataSources.data.sources.every(
    (source) => receiptData.receiptsByVendorId?.[source.id]?.length > 0,
  );
  const hasDataSource = updateDataSources.data.sources.length > 0;
  const isDone =
    surveyFinished &&
    applicantTrackingDone &&
    dataBrokersDone &&
    backgroundCheckDone &&
    otherDone &&
    everyDataSourceHasPIC &&
    hasDataSource;

  return (
    <GetCompliantPageWrapper
      requests={[
        groupsRequest,
        updateDataSources.requests.catalogRequest,
        updateDataSources.requests.sourcesRequest,
        surveyRequest,
        recipientsRequest,
      ]}
      progressFooter={
        <ProgressFooter
          actions={
            <GetCompliantFooter presentStep={"EmployeeMap.DataSources"} nextDisabled={!isDone} />
          }
        />
      }
    >
      <PageHeader
        titleContent="Sources"
        descriptionContent={
          <p>
            In this step, you’ll identify the sources of personal data for your {groupLabel}. As
            with consumers, the individual themselves is generally the source of most data. But your
            business may also collect data from other sources too.
          </p>
        }
      />

      <SurveyGateButton
        slug="sources-acknowledgement"
        survey={answers}
        updateResponse={setAnswer}
        label="Get Started"
      >
        <SurveyRenderer survey={survey} answers={answers} setAnswer={handleSetAnswer} />

        {surveyFinished && (
          <div className="my-xl">
            <SurveyQuestionContainer>
              <div className="survey--question">
                <QuestionContainer>
                  <QuestionLabel
                    label="Almost Done! Finally, let us know what personal information you receive from each source."
                    helpText="The data you previously indicated you collect is listed below each source. Select which types of data are collected from each of your sources. If you need to remove a source, update your answer above. "
                    recommendation={
                      !receiptData || receiptData.sources.length == 0
                        ? "You are required to add at least one data source to continue. Change an answer to 'yes' above to add a source."
                        : null
                    }
                  />

                  <Stack spacing={3} className="my-lg">
                    <DataSourceReceiptCards
                      sources={receiptData.sources}
                      disclosuresByVendorId={receiptData.receiptsByVendorId}
                      consumerDirectlyName="Worker"
                      renderer={(r) => (
                        <DataSourceReceiptCheckboxes
                          collectedPic={receiptData.collectedPIC}
                          selected={receiptData.receiptsByVendorId[r.vendorId]?.flatMap(
                            (d) => d.id,
                          )}
                          includeSelectAll={r.vendorKey === "source-consumer-directly"}
                          onSelectAll={(received: boolean) => {
                            receiptData.actions.toggleReceipt({
                              vendorId: r.vendorId,
                              receivedIds: receiptData.collectedPIC.map((pic) => pic.id),
                              received,
                            });
                          }}
                          onChange={(receivedId: UUIDString, received: boolean) =>
                            receiptData.actions.toggleReceipt({
                              vendorId: r.vendorId,
                              receivedIds: [receivedId],
                              received,
                            })
                          }
                        />
                      )}
                    />
                  </Stack>
                </QuestionContainer>
              </div>
            </SurveyQuestionContainer>
          </div>
        )}
      </SurveyGateButton>
    </GetCompliantPageWrapper>
  );
};
