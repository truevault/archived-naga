import { Stack } from "@mui/material";
import React from "react";
import { CollectionContext } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { CategorizedVendorInventory } from "../../../components/organisms/vendor/CategorizedVendorInventory";
import { KNOWN_EMPLOYMENT_VENDOR_CATEGORIES } from "../../../components/organisms/vendor/VendorCategories";
import { VendorSearchBox } from "../../../components/organisms/vendor/VendorSearchBox";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useUpdateDataRecipients } from "../data-recipients/AddDataRecipients";
import { GetCompliantFooter, ProgressFooter } from "../ProgressFooter";

const CONTEXT: CollectionContext[] = ["EMPLOYEE"];

export const EmployeeMapDataRecipientsPage: React.FC = () => {
  const orgId = usePrimaryOrganizationId();

  const updateDataRecipients = useUpdateDataRecipients(orgId, {
    collectionContext: CONTEXT,
  });

  const { requests } = updateDataRecipients;

  return (
    <GetCompliantPageWrapper
      requests={[requests.vendorsRequest, requests.allVendorsRequest]}
      progressFooter={
        <ProgressFooter
          actions={
            <GetCompliantFooter
              nextDisabled={updateDataRecipients.enabledVendors.size === 0}
              nextDisabledTooltip="Please add vendors before proceeding to the next step"
              presentStep="EmployeeMap.DataRecipients"
            />
          }
        />
      }
    >
      <PageHeader
        titleContent="Recipients"
        descriptionContent={
          <p>
            Search our database to find your remaining Data Recipients. Create a Custom Recipient
            for any you can’t find.
          </p>
        }
      />

      <Stack spacing={3}>
        <VendorSearchBox {...updateDataRecipients} />

        <CategorizedVendorInventory
          {...updateDataRecipients}
          collectionContext={["EMPLOYEE"]}
          categories={KNOWN_EMPLOYMENT_VENDOR_CATEGORIES}
          orgVendors={updateDataRecipients.currentVendors}
        />
      </Stack>
    </GetCompliantPageWrapper>
  );
};
