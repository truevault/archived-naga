import React from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { SurveyRenderer } from "../../../components/get-compliant/survey/SurveyRenderer";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { usePrimaryOrganization } from "../../../hooks";
import { useCreateDefaultCollectionGroups } from "../../../hooks/useCollectionGroups";
import { hrSurvey, HR_SURVEY_NAME } from "../../../surveys/hrSurvey";
import { isFinished } from "../../../surveys/survey";
import { GetCompliantFooter } from "../ProgressFooter";

export const HRSurvey: React.FC = () => {
  // State
  const [org] = usePrimaryOrganization();
  const organizationId = org?.id;

  const {
    answers: answers,
    setAnswer: setAnswer,
    surveyRequest: request,
    updateRequest: updateRequest,
  } = useSurvey(organizationId, HR_SURVEY_NAME);

  const { create: createDefaults, request: createDefaultsRequest } =
    useCreateDefaultCollectionGroups(organizationId);

  // Effects

  const survey = hrSurvey(answers, org.featureGdpr);
  const surveyFinished = isFinished(survey, answers);

  const nextDisabled = !surveyFinished;

  return (
    <GetCompliantPageWrapper
      requests={[request]}
      progressFooter={
        <GetCompliantFooter
          presentStep="EmployeeMap.HRSurvey"
          nextDisabled={nextDisabled}
          nextDisabledTooltip="Please answer all the questions before proceeding."
          nextOnClick={createDefaults}
          nextLabel="Next"
          prevLabel="Back"
          saveRequests={[updateRequest, createDefaultsRequest]}
        />
      }
    >
      <PageHeader
        titleContent="HR Survey"
        descriptionContent="Answer the following questions about your business's HR data collection."
      />
      <SurveyRenderer survey={survey} answers={answers} setAnswer={setAnswer} />
    </GetCompliantPageWrapper>
  );
};
