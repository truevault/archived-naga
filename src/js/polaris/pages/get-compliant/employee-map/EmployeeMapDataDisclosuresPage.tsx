import { Stack } from "@mui/material";
import _ from "lodash";
import React, { useMemo } from "react";
import { CollectionGroupType } from "../../../../common/service/server/controller/CollectionGroupsController";
import { CollectionContext } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { DataRecipientCollectionCards } from "../../../components/organisms/DataRecipientCollectionCards";
import { DataRecipientDisclosureCheckboxes } from "../../../components/organisms/DataRecipientDisclosureCheckboxes";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../hooks/useDataMap";
import { useDisclosuresForCollectionGroups } from "../../../hooks/useDisclosuresForCollectionGroups";
import { lexicalJoin } from "../../../util/text";
import { GetCompliantFooter, ProgressFooter } from "../ProgressFooter";

const COLLECTION_GROUP_TYPE: CollectionGroupType[] = ["EMPLOYMENT"];
const CONSUMER_CONTEXT: CollectionContext[] = ["EMPLOYEE"];

export const EmployeeMapDataDisclosuresPage: React.FC = () => {
  const orgId = usePrimaryOrganizationId();
  const [groups, groupsRequest] = useCollectionGroups(orgId, COLLECTION_GROUP_TYPE);

  const disclosureData = useDisclosuresForCollectionGroups(orgId, groups, CONSUMER_CONTEXT);

  const [dataMap, dataMapRequest] = useDataMap(orgId);

  const allComplete = useMemo(
    () =>
      _.every(
        disclosureData.dataRecipients,
        (dr) => disclosureData.disclosuresByVendorId[dr.vendorId]?.length > 0 ?? false,
      ),
    [disclosureData.dataRecipients, disclosureData.disclosuresByVendorId],
  );

  const groupLabel = groups ? lexicalJoin(groups.map((g) => g.name)) : "your employment groups";

  return (
    <GetCompliantPageWrapper
      requests={[
        dataMapRequest,
        groupsRequest,
        disclosureData.requests.dataMapRequest,
        disclosureData.requests.recipientsRequest,
      ]}
      progressFooter={
        <ProgressFooter
          actions={
            <GetCompliantFooter
              saveRequests={[disclosureData.requests.toggleDisclosureRequest]}
              nextDisabledTooltip="Please indicate disclosures for each vendor before continuing."
              presentStep="EmployeeMap.DataDisclosures"
              nextDisabled={!allComplete}
            />
          }
        />
      }
    >
      <PageHeader
        titleContent="Disclosures"
        descriptionContent={
          <>
            <h4 className="intermediate">
              What categories of information are collected, analyzed by, stored in, or otherwise
              processed by your Data Recipients?
            </h4>
            <p>
              Select the Data Recipients process information about {groupLabel}. For each Data
              Recipient you select, identify which categories of personal data they process,
              including any categories of digital information that may be collected and processed
              automatically.
            </p>
          </>
        }
      />

      <Stack spacing={3}>
        <DataRecipientCollectionCards
          recipients={disclosureData.dataRecipients}
          context={["EMPLOYEE"]}
          disclosuresByVendorId={disclosureData.disclosuresByVendorId}
          renderer={(r) => (
            <DataRecipientDisclosureCheckboxes
              showRecommendations
              dataMap={dataMap}
              dataRecipient={r}
              collectedPicGroups={disclosureData.collectedPicGroups}
              selected={disclosureData.disclosuresByVendorId[r.vendorId]?.flatMap((d) => d.id)}
              toggleDisclosure={disclosureData.actions.toggleDisclosure}
            />
          )}
        />
      </Stack>
    </GetCompliantPageWrapper>
  );
};
