import React from "react";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { ImageStepper, ImageStepperStep } from "../../../components/organisms/ImageStepper";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useProgressKeys } from "../../../hooks/useProgressKeys";
import { useNextStep, usePrevStep } from "../../../surveys/steps/GetCompliantSteps";

const BASE_IMAGE_URL = "/assets/images/get_compliant/employeemap_intro";

const INTRODUCTION_STEPS: ImageStepperStep[] = [
  {
    title: (
      <>
        It's time to map data practices for your <strong>Employees</strong>.
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/employee_map_slide_1.svg`,
  },
  {
    title: (
      <>
        It might sound strange, but some privacy laws consider <strong>Employees</strong> to be{" "}
        <strong>Consumers</strong>.
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/employee_map_slide_2.svg`,
  },
  {
    title: (
      <>
        They have the same <strong>rights</strong> over <em>their</em> data as your business's{" "}
        <strong>Consumers</strong>, and can make the same <strong>Privacy Requests</strong>.
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/employee_map_slide_3.svg`,
  },
  {
    title: (
      <>
        To comply with these laws, we'll build a <strong>Data Map</strong> for your{" "}
        <strong>Employee</strong> information just like we did for <strong>Consumers</strong>.
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/employee_map_slide_4.svg`,
  },
  {
    title: <>Ready to go?</>,
    imageSrc: `${BASE_IMAGE_URL}/employee_map_slide_5.svg`,
    nextButtonLabel: "Get Started",
    canSkip: false,
  },
];

export const EmployeeMapIntroductionPage: React.FC = () => {
  const orgId = usePrimaryOrganizationId();
  const { update } = useProgressKeys(orgId);
  const [handleBack, backRequests] = usePrevStep("EmployeeMap.Introduction");
  const [handleDone, nextRequests] = useNextStep("EmployeeMap.Introduction", async () => {
    await update({ keyId: "employee_map_intro", progress: "DONE" });
  });

  return (
    <GetCompliantPageWrapper requests={[...nextRequests, ...backRequests]} disablePadding>
      <ImageStepper
        steps={INTRODUCTION_STEPS}
        handleDone={handleDone}
        handleFirstBack={handleBack}
      />
    </GetCompliantPageWrapper>
  );
};
