import { Stack } from "@mui/material";
import { useQueryClient } from "@tanstack/react-query";
import _ from "lodash";
import React, { useMemo } from "react";
import { useDataSourceCatalog } from "../../../../common/hooks";
import { useActionRequest } from "../../../../common/hooks/api";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { Api } from "../../../../common/service/Api";
import { CollectionContext } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationPublicId, UUIDString } from "../../../../common/service/server/Types";
import { Callout, CalloutVariant } from "../../../components/Callout";
import {
  QuestionContainer,
  QuestionLabel,
} from "../../../components/get-compliant/survey/QuestionContainer";
import { SurveyQuestionContainer } from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import { SurveyRenderer } from "../../../components/get-compliant/survey/SurveyRenderer";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { DataSourceReceiptCards } from "../../../components/organisms/DataSourceReceiptCards";
import { DataSourceReceiptCheckboxes } from "../../../components/organisms/DataSourceReceiptCheckboxes";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { useOrganizationVendors, useOrgDataSources } from "../../../hooks/useOrganizationVendors";
import { useReceiptsForCollectionGroups } from "../../../hooks/useReceiptsForCollectionGroups";
import {
  dataMapSourcesSurvey,
  OTHER_EXCLUDED_CATEGORIES,
} from "../../../surveys/dataMapSourcesSurvey";
import { isFinished } from "../../../surveys/survey";
import { dataSourceSort } from "../../../surveys/util";
import {
  VENDOR_CATEGORY_AD_NETWORK,
  VENDOR_CATEGORY_DATA_BROKER,
  VENDOR_CATEGORY_RETAIL_PARTNER,
} from "../../../types/Vendor";
import { CustomVendorFormValues } from "../data-recipients/AddDataRecipients";
import { GetCompliantFooter } from "../ProgressFooter";

export const CONSUMER_SOURCES_SURVEY_NAME = "consumer-sources-survey";

export const useUpdateDataSources = (
  orgId: OrganizationPublicId,
  options: { collectionContext: CollectionContext[] } = { collectionContext: ["CONSUMER"] },
) => {
  const { collectionContext } = options;
  const queryClient = useQueryClient();

  const [orgSources, sourcesRequest] = useOrgDataSources(orgId);

  const [catalog, catalogRequest] = useDataSourceCatalog(orgId);

  const selectedVendorIds = useMemo(() => {
    return (
      orgSources
        ?.filter((s) => _.intersection(s.collectionContext, collectionContext).length > 0)
        ?.map((o) => o.vendorId) ?? []
    );
  }, [orgSources, collectionContext]);

  const remainingCatalog = useMemo(
    () => catalog?.filter((c) => !selectedVendorIds?.includes(c.id)) ?? [],
    [selectedVendorIds, catalog],
  );

  const { fetch: handleAddVendor } = useActionRequest({
    api: (vendorId: string) =>
      Api.orgDataSource.addOrgDataSource(orgId, { vendorId, collectionContext: collectionContext }),
    onSuccess: () => {
      queryClient.invalidateQueries(["orgDataSources", orgId]);
    },
  });

  const { fetch: handleRemoveVendor } = useActionRequest({
    api: (vendorId: string) =>
      Api.orgDataSource.removeOrgDataSource(orgId, vendorId, collectionContext),
    onSuccess: () => {
      queryClient.invalidateQueries(["orgDataSources", orgId]);
      queryClient.invalidateQueries(["dataMap", orgId]);
    },
  });

  const { fetch: handleAddCustomVendor } = useActionRequest({
    api: (fields: CustomVendorFormValues) =>
      Api.orgDataSource.addOrgDataSource(orgId, {
        collectionContext: collectionContext,
        custom: fields,
      }),
    onSuccess: () => {
      queryClient.invalidateQueries(["orgDataSources", orgId]);
      queryClient.invalidateQueries(["vendorCatalog", orgId]);
    },
  });

  const mySources = useMemo(
    () => catalog?.filter((c) => selectedVendorIds?.includes(c.id)).sort(dataSourceSort) ?? [],
    [selectedVendorIds, catalog],
  );

  return {
    requests: {
      sourcesRequest,
      catalogRequest,
    },
    actions: {
      handleAddVendor,
      handleRemoveVendor,
      handleAddCustomVendor,
    },
    data: {
      sources: mySources,
      allSources: orgSources,
      remainingCatalog,
      catalog,
    },
  };
};

export const ConsumerDataSourcesPage: React.FC = () => {
  // Variables
  const orgId = usePrimaryOrganizationId();

  // State
  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, CONSUMER_SOURCES_SURVEY_NAME);
  const [recipients, recipientsRequest] = useOrganizationVendors(
    orgId,
    undefined,
    undefined,
    undefined,
    ["CONSUMER"],
  );
  const [groups, groupsRequest] = useCollectionGroups(orgId, [
    "BUSINESS_TO_BUSINESS",
    "BUSINESS_TO_CONSUMER",
  ]);
  const receiptData = useReceiptsForCollectionGroups(orgId, groups, ["CONSUMER"]);
  const updateDataSources = useUpdateDataSources(orgId);
  const queryClient = useQueryClient();

  const hasRetailPartner = useMemo(
    () => updateDataSources.data.sources.some((s) => s.category == VENDOR_CATEGORY_RETAIL_PARTNER),
    [updateDataSources.data.sources],
  );
  const hasDataBroker = useMemo(
    () => updateDataSources.data.sources.some((s) => s.category == VENDOR_CATEGORY_DATA_BROKER),
    [updateDataSources.data.sources],
  );
  const hasAdNetwork = useMemo(
    () => updateDataSources.data.sources.some((s) => s.category == VENDOR_CATEGORY_AD_NETWORK),
    [updateDataSources.data.sources],
  );
  const hasOtherSource = useMemo(
    () =>
      updateDataSources.data.sources.some((s) => !OTHER_EXCLUDED_CATEGORIES.includes(s.category)),
    [updateDataSources.data.sources],
  );

  const survey = useMemo(
    () =>
      dataMapSourcesSurvey(
        answers,
        updateDataSources.data.catalog,
        receiptData.sources,
        recipients,
        {
          handleAddVendor: updateDataSources.actions.handleAddVendor,
          handleAddCustomVendor: updateDataSources.actions.handleAddCustomVendor,
          handleRemoveVendor: updateDataSources.actions.handleRemoveVendor,
        },
      ),
    [
      answers,
      updateDataSources.data.catalog,
      receiptData.sources,
      recipients,
      updateDataSources.actions,
    ],
  );
  const surveyFinished = isFinished(survey, answers);

  // Two questions cause the sources to be updated, so we hook here to invalidate our source query, so we re-fetch the sources
  const handleSetAnswer = async (slug: string, answer: string, question?: string) => {
    await setAnswer(slug, answer, question);
    if (slug == "consumer-directly" || slug == "other-consumers") {
      queryClient.invalidateQueries(["orgDataSources", orgId]);
    }
  };

  const retailPartnersDone = answers["retail-partners"] === "false" || hasRetailPartner;
  const dataBrokersDone = answers["data-brokers"] === "false" || hasDataBroker;
  const adNetworksDone = answers["ad-networks"] === "false" || hasAdNetwork;
  const otherDone = answers["other-sources"] === "false" || hasOtherSource;

  const everyDataSourceHasPIC = updateDataSources.data.sources.every(
    (source) => receiptData.receiptsByVendorId?.[source.id]?.length > 0,
  );
  const hasDataSource = updateDataSources.data.sources.length > 0;
  const isDone =
    surveyFinished &&
    retailPartnersDone &&
    dataBrokersDone &&
    adNetworksDone &&
    otherDone &&
    everyDataSourceHasPIC &&
    hasDataSource;

  return (
    <GetCompliantPageWrapper
      requests={[
        updateDataSources.requests.catalogRequest,
        updateDataSources.requests.sourcesRequest,
        surveyRequest,
        groupsRequest,
        recipientsRequest,
      ]}
      progressFooter={
        <GetCompliantFooter presentStep="DataRecipients.DataSources" nextDisabled={!isDone} />
      }
    >
      <PageHeader
        titleContent="Sources"
        descriptionContent={
          <Paras>
            <Para>
              For most businesses, <strong>the consumer</strong> themselves is the primary{" "}
              <strong>source</strong> of personal data. Data collection can happen directly (such as
              on a checkout page) or indirectly (website visitor activity). Every business is
              different, and yours may collect information from <strong>other</strong> sources too.
            </Para>
            <Para>
              The <strong>source</strong> of personal data is the place where the data originates
              when your business collects it.
            </Para>
          </Paras>
        }
      />

      <div className="my-xl">
        <Callout variant={CalloutVariant.Purple} icon="Info" title="Example">
          <Paras>
            <Para>
              When a customer buys a product on your site, their data goes into a system like
              Shopify. Shopify is <strong>not</strong> the source of the data. The consumer is the
              source because they provided the data to your business. Shopify just retrieves and
              stores it for your business.
            </Para>
            <Para>
              Conversely, if your business purchases a list of potential customers for marketing
              purposes, and the list has names, emails, or other identifying information, the
              consumer is not the source of that data to your business. The source is the company
              that sold the list of potential customers to your business.
            </Para>
          </Paras>
        </Callout>
      </div>

      <Para className="mb-md">
        In this step, you’ll identify all of your business’s data sources.
      </Para>

      <SurveyGateButton
        slug="sources-acknowledgement"
        survey={answers}
        updateResponse={setAnswer}
        label="Get Started"
      >
        <SurveyRenderer survey={survey} answers={answers} setAnswer={handleSetAnswer} />

        {surveyFinished && (
          <div className="my-xl">
            <SurveyQuestionContainer>
              <div className="survey--question">
                <QuestionContainer>
                  <QuestionLabel
                    label="Almost Done! Finally, let us know what personal information you receive from each source."
                    helpText="The data you previously indicated you collect is listed below each source. Select which types of data are collected from each of your sources. If you need to remove a source, update your answer above. "
                    recommendation={
                      !receiptData || receiptData.sources.length == 0
                        ? "You are required to add at least one data source to continue. Change an answer to 'yes' above to add a source."
                        : null
                    }
                  />

                  <Stack spacing={3} className="my-lg">
                    <DataSourceReceiptCards
                      sources={receiptData.sources}
                      disclosuresByVendorId={receiptData.receiptsByVendorId}
                      consumerDirectlyName="Consumer"
                      renderer={(r) => (
                        <DataSourceReceiptCheckboxes
                          collectedPic={receiptData.collectedPIC}
                          selected={receiptData.receiptsByVendorId[r.vendorId]?.flatMap(
                            (d) => d.id,
                          )}
                          includeSelectAll={r.vendorKey === "source-consumer-directly"}
                          pending={receiptData.pendingByVendorId[r.vendorId]?.flatMap((d) => d)}
                          onSelectAll={(received: boolean) => {
                            receiptData.actions.toggleReceipt({
                              vendorId: r.vendorId,
                              receivedIds: receiptData.collectedPIC.map((pic) => pic.id),
                              received,
                            });
                          }}
                          onChange={(receivedId: UUIDString, received: boolean) =>
                            receiptData.actions.toggleReceipt({
                              vendorId: r.vendorId,
                              receivedIds: [receivedId],
                              received,
                            })
                          }
                        />
                      )}
                    />
                  </Stack>
                </QuestionContainer>
              </div>
            </SurveyQuestionContainer>
          </div>
        )}
      </SurveyGateButton>
    </GetCompliantPageWrapper>
  );
};
