import React from "react";

import {
  AccountCircle as OnlineIdentifiersIcon,
  Assessment as InferencesIcon,
  AssignmentInd as PersonalIdentifiersIcon,
  EnhancedEncryption as MedicalIcon,
  Face as CharacteristicsIcon,
  Fingerprint as BiometricIcon,
  Laptop as InternetActivityIcon,
  Lock as SensitiveIcon,
  MyLocation as LocationIcon,
  PlaylistAddCheck as DefaultIcon,
  School as ProfessionalIcon,
  ShoppingCart as CommercialIcon,
  Visibility as PhysicalIcon,
} from "@mui/icons-material";

import { IconProps } from "@mui/material";

type GroupIconProps = {
  groupName: string;
} & IconProps;

const iconMap = {
  "Personal Identifiers": PersonalIdentifiersIcon,
  "Online Identifiers": OnlineIdentifiersIcon,
  "Internet Activity": InternetActivityIcon,
  "Commercial and Financial Information": CommercialIcon,
  "Biometric Information": BiometricIcon,
  "Geolocation Information": LocationIcon,
  "Physical and Audio Data": PhysicalIcon,
  "Characteristics of Protected Classifications": CharacteristicsIcon,
  "Other Sensitive Data": SensitiveIcon,
  Medical: MedicalIcon,
  Professional: ProfessionalIcon,
  Inferences: InferencesIcon,
};

export const GroupIcon: React.FC<GroupIconProps> = ({ groupName, ...rest }) => {
  const IconComponent = iconMap[groupName] ?? DefaultIcon;
  return <IconComponent {...rest} />;
};
