import React from "react";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { ImageStepper, ImageStepperStep } from "../../components/organisms/ImageStepper";
import { usePrimaryOrganizationId } from "../../hooks";
import { useProgressKeys } from "../../hooks/useProgressKeys";
import { useNextStep, usePrevStep } from "../../surveys/steps/GetCompliantSteps";

const BASE_IMAGE_URL = "/assets/images/get_compliant/datamap_intro";

const INTRODUCTION_STEPS: ImageStepperStep[] = [
  {
    title: (
      <>
        Next, let’s build your <strong>Data Map</strong>
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/data_map_slide_1.svg`,
  },
  {
    title: (
      <>
        Your business collects <strong>Personal Information</strong> about{" "}
        <strong>Consumers</strong>
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/data_map_slide_2.svg`,
  },
  {
    title: (
      <>
        This information comes from <strong>Data Sources</strong>, like the Consumer themselves...
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/data_map_slide_3.svg`,
  },
  {
    title: (
      <>
        ...and your business discloses it to <strong>Data Recipients</strong>, like your Payment
        Processor.
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/data_map_slide_4.svg`,
  },
  {
    title: (
      <>
        Your <strong>Data Map</strong> shows the flow of personal information through your business
        — <strong>where it comes from</strong> and <strong>where it goes</strong>...
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/data_map_slide_5.svg`,
  },
  {
    title: (
      <>
        ...and it's TrueVault's <em>most</em> important tool in helping you{" "}
        <strong>get compliant</strong> and <strong>stay compliant</strong>.
      </>
    ),
    imageSrc: `${BASE_IMAGE_URL}/data_map_slide_6.svg`,
  },
  {
    title: <>Ready to go?</>,
    imageSrc: `${BASE_IMAGE_URL}/data_map_slide_7.svg`,
    nextButtonLabel: "Get Started",
    canSkip: false,
  },
];

export const DataMapIntroductionPage: React.FC = () => {
  const orgId = usePrimaryOrganizationId();
  const { update } = useProgressKeys(orgId);
  const [handleBack, backRequests] = usePrevStep("DataMap.Introduction");
  const [handleDone, nextRequests] = useNextStep("DataMap.Introduction", async () => {
    await update({ keyId: "data_map_intro", progress: "DONE" });
  });

  return (
    <GetCompliantPageWrapper requests={[...nextRequests, ...backRequests]} disablePadding>
      <ImageStepper
        steps={INTRODUCTION_STEPS}
        handleDone={handleDone}
        handleFirstBack={handleBack}
      />
    </GetCompliantPageWrapper>
  );
};
