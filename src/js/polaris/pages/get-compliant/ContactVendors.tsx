import { FormControlLabel, Radio, RadioGroup, Typography } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { GetCompliantStep } from "../../../common/service/server/Types";
import { Loading } from "../../components";
import { DataRecipientLogo } from "../../components/DataRecipientLogo";
import { ClassifyVendorsFooter } from "../../components/get-compliant/vendors/ClassifyVendorsFooter";
import { ClassifyVendorsHeader } from "../../components/get-compliant/vendors/ClassifyVendorsHeader";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { CopyTextCallout } from "../../components/organisms/callouts/CopyTextCallout";
import {
  useOrganizationVendors,
  usePrimaryOrganization,
  usePrimaryOrganizationId,
} from "../../hooks";
import { useUpdateVendor } from "../../hooks/get-compliant/vendors/useUpdateVendor";
import { Routes } from "../../root/routes";

interface ContactVendorsProps {
  currentProgress: GetCompliantStep;
}
export const ContactVendors = ({ currentProgress }: ContactVendorsProps) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  // Requests
  const [orgVendors, orgVendorsRequest] = useOrganizationVendors(organizationId);

  const contactVendors = useMemo(
    () =>
      orgVendors?.filter(
        (ov) => ov.vendorContractReviewed?.slug === "provider-language-not-found",
      ) || [],
    [orgVendors],
  );

  const localOrgVendorUpdate = (orgVendor) => {
    const updateIndex = contactVendors.findIndex((ov) => ov.vendorId === orgVendor.vendorId);
    if (updateIndex !== -1) {
      contactVendors[updateIndex] = orgVendor;
    }
  };

  const { updateVendor, updateVendorRequest } = useUpdateVendor(organizationId, contactVendors);

  // Effects
  useEffect(() => {
    if (orgVendorsRequest.success) {
      setReadyToRender(true);
    }
  }, [orgVendorsRequest, contactVendors, setReadyToRender]);

  // Handlers
  const handleContacted = (vendor: OrganizationDataRecipientDto, value: string) => {
    if (value === "contacted") {
      const needsReviewClassifcation = vendor.classificationOptions.find(
        (c) => c.slug === "needs-review",
      );
      updateVendor({
        vendorId: vendor.vendorId,
        contacted: true,
        classificationId: needsReviewClassifcation?.id,
        callback: localOrgVendorUpdate,
      });
    } else if (value === "third_party") {
      const thirdPartyClassification = vendor.classificationOptions.find(
        (c) => c.slug === "third-party",
      );
      updateVendor({
        vendorId: vendor.vendorId,
        contacted: false,
        classificationId: thirdPartyClassification?.id,
        callback: localOrgVendorUpdate,
      });
    }
  };
  const [org] = usePrimaryOrganization();

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper currentProgress={currentProgress}>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  const nextDisabled = !contactVendors.every(
    (x) => x.contacted || x.classification?.slug === "third-party",
  );

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <ClassifyVendorsFooter
          currentProgress={currentProgress}
          organizationId={organizationId}
          currentRequest={updateVendorRequest}
          nextDisabled={nextDisabled}
          prevLabel="Back"
          prevUrl={Routes.getCompliant.dataRecipients.locateOther}
          nextLabel="Done with Classification"
          nextUrl={
            org.featureGdpr
              ? Routes.getCompliant.dataRecipients.gdpr.confirmProcessors
              : Routes.getCompliant.dataCollection.consumerCollection
          }
          nextProgress={
            org.featureGdpr
              ? "Classification.GDPRConfirmProcessor"
              : "ConsumerCollection.CollectionPurposes"
          }
          disabledNextTooltip="Please contact each vendor or update the classification on the previous screen before proceeding to the next step"
        />
      }
    >
      <ClassifyVendorsHeader title="Contact Vendors">
        <Typography variant="body1" className="mb-sm">
          The vendors for which you did not locate Service Provider language are listed below. You
          may contact them about their Service Provider status using the email template below. We'll
          mark them “pending” for now. You'll be able to update their classification later in Stay
          Compliant.
        </Typography>

        <Typography variant="body1">
          If you do not wish to contact these vendors or do not believe they are Service Providers,
          mark them as "Third Parties" below.
        </Typography>
      </ClassifyVendorsHeader>

      <NotesContent />

      <div className="contact-vendors--container w-896 pt-xxl">
        {contactVendors
          .sort((a, b) => a.name.localeCompare(b.name))
          .filter((ov) => !ov.automaticallyClassified)
          .map((ov) => (
            <VendorRow
              key={`vendor-row-${ov.vendorId}`}
              organizationVendor={ov}
              handleChecked={handleContacted}
            />
          ))}
      </div>
    </GetCompliantPageWrapper>
  );
};

interface VendorRowProps {
  organizationVendor: OrganizationDataRecipientDto;
  handleChecked: (vendor: OrganizationDataRecipientDto, contacted: string) => void;
}
const VendorRow: React.FC<VendorRowProps> = ({ organizationVendor, handleChecked }) => {
  const contact =
    organizationVendor.email || (organizationVendor.organization ? "" : "No email address found");

  let val = "";

  if (organizationVendor.classification?.slug === "third-party") {
    val = "third_party";
  }

  if (organizationVendor.contacted === true) {
    val = "contacted";
  }

  return (
    <div className="data-recipient-radio-options">
      <div className="data-recipient-radio-options__logo">
        <DataRecipientLogo dataRecipient={organizationVendor} size={24} />
      </div>
      <div className="data-recipient-radio-options__info">
        <div className="contact-vendors--vendor-name">{organizationVendor.name}</div>
        <div className="contact-vendors--contact-details">{contact}</div>
      </div>
      <div className="contact-vendors--contacted-selection">
        <div className="contact-vendors--checkbox">
          <RadioGroup
            row
            value={val}
            onChange={(e, value) => handleChecked(organizationVendor, value)}
          >
            <FormControlLabel
              value="contacted"
              label="I contacted this vendor"
              control={<Radio checked={val == "contacted"} />}
            />
            <FormControlLabel
              value="third_party"
              label="Classify as a Third Party"
              control={<Radio checked={val == "third_party"} />}
            />
          </RadioGroup>
        </div>
      </div>
    </div>
  );
};

const CONTACT_TEMPLATE = `Hello,

You are one of our vendors and we are trying to determine if you are a “service provider” to our business under the California Consumer Privacy Act (CCPA). If you are a service provider, could you direct me to the supporting language in your privacy policy, terms of service, DPA, or other documentation? The definition of a “service provider” is provided in the CCPA at Section 1798.140(v).`;

const NotesContent = () => (
  <>
    <CopyTextCallout
      copy={CONTACT_TEMPLATE}
      title="Use the email template below (or create your own) to contact vendors and inquire about service provider status."
    />
  </>
);
