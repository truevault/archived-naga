import React, { useCallback, useEffect, useMemo, useState } from "react";
import { ExternalLink } from "../../../common/components/ExternalLink";
import { GetCompliantStep, UUIDString } from "../../../common/service/server/Types";
import { Loading } from "../../../common/components/Loading";
import { ClassifyVendorsFooter } from "../../components/get-compliant/vendors/ClassifyVendorsFooter";
import { ClassifyVendorsHeader } from "../../components/get-compliant/vendors/ClassifyVendorsHeader";
import { VendorClassifier } from "../../components/get-compliant/vendors/VendorClassifier";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { useUpdateVendor } from "../../hooks/get-compliant/vendors/useUpdateVendor";
import { Routes } from "../../root/routes";
import { vendorsByType } from "../../util/vendors";

interface ClassifyVendorsProps {
  currentProgress: GetCompliantStep;
}

export const ClassifyOtherVendors: React.FC<ClassifyVendorsProps> = ({ currentProgress }) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);
  const [complete, setComplete] = useState(false);
  const [nextUrl, setNextUrl] = useState(Routes.getCompliant.privacyRequests.privacyCenter);
  const [nextProgress, setNextProgress] = useState("PRIVACY_NOTICES");
  const [nextLabel, setNextLabel] = useState("Done with Classification");
  const [needsContactId, setNeedsContactId] = useState(null);

  useEffect(() => {
    setComplete(false);
  }, [organizationId]);

  // Requests
  const [orgVendors, orgVendorsRequest] = useOrganizationVendors(organizationId);
  const { serviceVendors, otherVendors } = useMemo(
    () => vendorsByType(orgVendors || []),
    [orgVendors],
  );

  useEffect(() => {
    setNeedsContactId(
      orgVendors?.[0]?.classificationOptions?.find((x) => x.slug === "needs-review")?.id,
    );
  }, [orgVendors, setNeedsContactId]);

  const { updateVendor, updateVendorRequest } = useUpdateVendor(organizationId, otherVendors);

  useEffect(() => {
    if (orgVendorsRequest.success) {
      setReadyToRender(true);
    }
  }, [orgVendorsRequest, setReadyToRender]);

  const answersChanged = useCallback(
    (classifications: Map<UUIDString, UUIDString>) => {
      const answers = Array.from(classifications.values());
      setComplete(!answers.some((cid) => !cid));
      const shouldContact = answers.some(
        (cid) => typeof cid === "string" && cid === needsContactId,
      );
      setNextLabel(shouldContact ? "Next" : "Done with Classification");
      setNextUrl(
        shouldContact
          ? Routes.getCompliant.dataRecipients.locateOther
          : Routes.getCompliant.privacyRequests.privacyCenter,
      );
      setNextProgress(shouldContact ? "CLASSIFY_VENDORS" : "PRIVACY_NOTICES");
    },
    [needsContactId, setComplete, setNextUrl],
  );

  const prevUrl = useMemo(
    () =>
      serviceVendors.length == 0
        ? Routes.getCompliant.employeeMap.employee
        : Routes.getCompliant.dataRecipients.classifyServices,
    [serviceVendors],
  );
  const prevLabel = useMemo(
    () =>
      serviceVendors.length == 0
        ? "Back to Employee Collection"
        : "Back to Service Provider Classification",
    [serviceVendors],
  );

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper currentProgress={currentProgress}>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <ClassifyVendorsFooter
          currentProgress={currentProgress}
          organizationId={organizationId}
          currentRequest={updateVendorRequest}
          nextDisabled={!complete}
          prevLabel={prevLabel}
          prevUrl={prevUrl}
          nextLabel={nextLabel}
          nextUrl={nextUrl}
          nextProgress={nextProgress as GetCompliantStep}
          disabledNextTooltip="Please make a selection for each vendor"
        />
      }
    >
      <ClassifyVendorsHeader title="Other Vendors">
        <Paras>
          <Para>We have not found public service provider statements for the vendors below.</Para>
          <Para>
            If you can find Service Provider language in the vendor’s legal documentation or within
            their contract with your business, change the classification to Service Provider.
            Otherwise, leave the status as Unknown. You will handle Unknowns in the next step.
          </Para>
          <Para>
            <ExternalLink
              href="https://help.truevault.com/article/127-what-is-a-service-provider"
              className="text-medium text-link"
              target="_blank"
            >
              Guide to Determining Service Provider Status
            </ExternalLink>
          </Para>
        </Paras>
      </ClassifyVendorsHeader>

      <VendorClassifier
        vendors={otherVendors}
        onChange={answersChanged}
        updateVendor={updateVendor}
      />
    </GetCompliantPageWrapper>
  );
};
