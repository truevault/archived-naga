import { LoadingButton } from "@mui/lab";
import React from "react";
import { ExternalLink } from "../../../../common/components/ExternalLink";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { PrivacyCenterDpoRow } from "../../../components/organisms/settings/rows/PrivacyCenterDpoRow";
import { usePrivacyCenterSettings } from "../../../components/superorg/settings/PrivacyCenterSettings";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { GetCompliantFooter } from "../ProgressFooter";

const DPO_HELP_TEXT = (
  <Paras>
    <Para>
      A DPO is required for businesses that regularly and systematically monitor individuals in
      Europe or the UK on a large scale, or that process their sensitive data on a large scale.{" "}
      <ExternalLink
        target="_blank"
        href="https://help.truevault.com/article/162-data-protection-officers-dpos"
      >
        You can read more about data privacy officers here
      </ExternalLink>
      .
    </Para>
    <Para>
      Click below to add the contact details of your DPO. If you believe the DPO requirement does
      not apply to you, select “I don’t need a DPO.” If you do not currently have a DPO but will get
      one, select “I’ll do this later”; we’ll send you reminders to complete this task in Stay
      Compliant. If you would like help finding a DPO, email{" "}
      <ExternalLink href="mailtp:help@truevault.com">help@truevault.com</ExternalLink>.
    </Para>
  </Paras>
);

export const DataPrivacyOfficerPage: React.FC = () => {
  const {
    settings: pcSettings,
    orgRequest: pcOrgRequest,
    privacyCenter,
    privacyCentersRequest,
    updatePrivacyPolicyRequest,
  } = usePrivacyCenterSettings();

  const dpoFinished =
    privacyCenter?.dpoOfficerDoesNotApply ||
    !privacyCenter?.requiresDpo ||
    privacyCenter.hasDpoTask ||
    (Boolean(pcSettings.dpoName) && Boolean(pcSettings.dpoEmail));

  const nextDisabled = !dpoFinished;

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          presentStep="PrivacyCenter.DataPrivacyOfficer"
          nextDisabled={nextDisabled}
          nextDisabledTooltip="Set a favicon above to continue."
        />
      }
      requests={[pcOrgRequest, privacyCentersRequest]}
    >
      <PageHeader
        titleContent="Privacy Officer"
        descriptionContent={
          <Paras>
            <Para>
              Based on your Survey answers and Data Map, your business may be required to have a
              data privacy officer (DPO). A DPO monitors your organization’s compliance activities
              and serves as the point of contact for both consumers and the data protection
              authorities in Europe and the UK.
            </Para>
          </Paras>
        }
      />

      <div className="my-lg">
        <PrivacyCenterDpoRow
          vertical
          helpText={DPO_HELP_TEXT}
          dpoName={pcSettings.dpoName}
          dpoEmail={pcSettings.dpoEmail}
          dpoPhone={pcSettings.dpoPhone}
          dpoAddress={pcSettings.dpoAddress}
          requiresDpo={privacyCenter?.requiresDpo}
          hasDpoTask={pcSettings.hasDpoTask}
          handleFormSubmit={pcSettings.handleFormSubmit}
          handleAddTask={pcSettings.handleDoItLaterTask}
          hideFooter={privacyCenter?.dpoOfficerDoesNotApply}
        />

        {privacyCenter?.dpoOfficerDoesNotApply ? (
          <Callout className="mt-md" variant={CalloutVariant.LightPurple}>
            We've marked that you do not require a data privacy officer.
            <LoadingButton
              loading={updatePrivacyPolicyRequest.running}
              onClick={() => pcSettings.handleDpoDoesNotApply(false)}
            >
              Undo
            </LoadingButton>
          </Callout>
        ) : (
          <LoadingButton
            className={"mt-md"}
            loading={updatePrivacyPolicyRequest.running}
            color="secondary"
            onClick={() => pcSettings.handleDpoDoesNotApply(true)}
          >
            I do not require a Data Privacy Officer
          </LoadingButton>
        )}
      </div>
    </GetCompliantPageWrapper>
  );
};
