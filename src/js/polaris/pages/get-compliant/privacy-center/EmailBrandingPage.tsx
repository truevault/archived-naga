import React from "react";
import emailHighlightImage from "../../../../../static/assets/images/get_compliant/email_highlight.png";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { EmailLogoSettingRow } from "../../../components/organisms/settings/rows/EmailLogoSettingRow";
import { EmailSignatureSettingRow } from "../../../components/organisms/settings/rows/EmailSignatureSettingRow";
import { useEmailSettings } from "../../../components/superorg/settings/EmailSettings";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { GetCompliantFooter } from "../ProgressFooter";

export const EmailBrandingPage: React.FC = () => {
  const { settings: emailSettings, orgRequest, mailboxesRequest } = useEmailSettings();

  const emailFinished =
    Boolean(emailSettings.messageSettings.messageSignature) &&
    Boolean(emailSettings.messageSettings.headerImageUrl);

  const nextDisabled = !emailFinished;

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          presentStep="PrivacyCenter.EmailBranding"
          nextDisabled={nextDisabled}
          nextDisabledTooltip="Complete the settings above to continue."
        />
      }
      requests={[orgRequest, mailboxesRequest]}
    >
      <PageHeader
        titleContent="Email Branding"
        descriptionContent={
          <Paras>
            <Para>
              All emails that Polaris sends to consumers on your behalf will include your company
              logo at the top. Upload your logo below.
            </Para>
          </Paras>
        }
      />

      <img src={emailHighlightImage as string} className="my-lg w-512" />

      <div className="my-lg">
        <EmailLogoSettingRow
          vertical
          headerImageUrl={emailSettings.messageSettings.headerImageUrl}
          updateMessageSettings={emailSettings.updateMessageSettings}
        />
      </div>

      <div className="mb-lg">
        <EmailSignatureSettingRow
          vertical
          signature={emailSettings.messageSettings.messageSignature}
          defaultSignature={emailSettings.messageSettings.defaultMessageSignature}
          onSignatureChange={emailSettings.updateSignature}
        />
      </div>
    </GetCompliantPageWrapper>
  );
};
