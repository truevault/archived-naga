import React from "react";
import faviconHighlightImage from "../../../../../static/assets/images/get_compliant/favicon_highlight.png";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { PrivacyCenterFaviconSettingRow } from "../../../components/organisms/settings/rows/PrivacyCenterFavIconSettingRow";
import { usePrivacyCenterSettings } from "../../../components/superorg/settings/PrivacyCenterSettings";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { GetCompliantFooter } from "../ProgressFooter";

export const FaviconPage: React.FC = () => {
  const {
    settings: pcSettings,
    orgRequest: pcOrgRequest,
    privacyCentersRequest,
  } = usePrivacyCenterSettings();

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          presentStep="PrivacyCenter.Favicon"
          nextDisabledTooltip="Set a favicon above to continue."
        />
      }
      requests={[pcOrgRequest, privacyCentersRequest]}
    >
      <PageHeader
        titleContent="Favicon Logo"
        descriptionContent={
          <Paras>
            <Para>
              A favicon is the small, square image that displays on each open tab of a web browser,
              next to the page title. Adding your business’s favicon to your Privacy Center further
              reinforces the impression that the consumer has not left your site.
            </Para>
          </Paras>
        }
      />

      <img src={faviconHighlightImage as string} className="my-lg" />

      <div className="my-lg">
        <PrivacyCenterFaviconSettingRow
          organizationId={pcSettings.organizationId}
          faviconUrl={pcSettings.faviconUrl}
          handleFormSubmit={pcSettings.handleFormSubmit}
          vertical
        />
      </div>
    </GetCompliantPageWrapper>
  );
};
