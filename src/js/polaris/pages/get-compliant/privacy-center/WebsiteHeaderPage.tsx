import React from "react";
import websiteHeaderImage from "../../../../../static/assets/images/get_compliant/logo_highlight.png";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { PrivacyCenterHeaderLogoSettingRow } from "../../../components/organisms/settings/rows/PrivacyCenterHeaderLogoSettingRow";
import { PrivacyCenterLogoLinkSettingRow } from "../../../components/organisms/settings/rows/PrivacyCenterLogoLinkSettingRow";
import { PrivacyCenterUrlSettingRow } from "../../../components/organisms/settings/rows/PrivacyCenterUrlSettingsRow";
import { usePrivacyCenterSettings } from "../../../components/superorg/settings/PrivacyCenterSettings";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { GetCompliantFooter } from "../ProgressFooter";

export const WebsiteHeaderPage: React.FC = () => {
  const {
    settings: pcSettings,
    orgRequest: pcOrgRequest,
    privacyCentersRequest: pcRequest,
  } = usePrivacyCenterSettings();

  const pcFinished = Boolean(pcSettings?.logoUrl) && Boolean(pcSettings?.logoLinkUrl);
  const nextDisabled = !pcFinished;

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter presentStep="PrivacyCenter.WebsiteHeader" nextDisabled={nextDisabled} />
      }
      requests={[pcOrgRequest, pcRequest]}
    >
      <PageHeader
        titleContent="Website Header"
        descriptionContent={
          <Paras>
            <Para>
              Adding a header logo to your Privacy Center lets you brand the page to make it your
              own, and creates a seamless experience for your consumers. Including a link to your
              homepage also gives consumers an easy path back to your site when they click on your
              logo.
            </Para>
          </Paras>
        }
      />

      <img src={websiteHeaderImage as string} className="my-lg w-100" />

      {pcSettings.includeUrlField && (
        <div className="my-lg">
          <PrivacyCenterUrlSettingRow url={pcSettings.privacyCenterUrl} vertical />
        </div>
      )}

      <div className="my-lg">
        <PrivacyCenterHeaderLogoSettingRow
          organizationId={pcSettings.organizationId}
          logoUrl={pcSettings.logoUrl}
          handleFormSubmit={pcSettings.handleFormSubmit}
          vertical
        />
      </div>

      <div className="mb-lg">
        <PrivacyCenterLogoLinkSettingRow
          logoLinkUrl={pcSettings.logoLinkUrl}
          handleFormSubmit={pcSettings.handleFormSubmit}
          vertical
        />
      </div>
    </GetCompliantPageWrapper>
  );
};
