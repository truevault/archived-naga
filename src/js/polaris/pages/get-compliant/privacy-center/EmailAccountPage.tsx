import React from "react";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { EmailAccountSettingRow } from "../../../components/organisms/settings/rows/EmailAccountSettingRow";
import { useEmailSettings } from "../../../components/superorg/settings/EmailSettings";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { GetCompliantFooter } from "../ProgressFooter";

export const EmailAccountPage: React.FC = () => {
  const { settings: emailSettings, orgRequest, mailboxesRequest } = useEmailSettings();
  const nextDisabled = !emailSettings.mailboxIsConnected;

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          presentStep="PrivacyCenter.EmailAccount"
          nextDisabled={nextDisabled}
          nextDisabledTooltip="Please connect an email account to continue."
        />
      }
      requests={[orgRequest, mailboxesRequest]}
    >
      <PageHeader
        titleContent="Email Account"
        descriptionContent={
          <Paras>
            <Para>
              Complete the following items to configure your privacy email. These settings will
              apply to all emails that Polaris sends to consumers on your behalf.
            </Para>
          </Paras>
        }
      />

      <div className="mt-lg">
        <EmailAccountSettingRow vertical label="Connect an Email Account" {...emailSettings} />
      </div>
    </GetCompliantPageWrapper>
  );
};
