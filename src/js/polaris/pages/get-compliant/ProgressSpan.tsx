import React from "react";
import {
  Cached as CachedIcon,
  Lens as LensIcon,
  CheckCircle as CheckCircleIcon,
  SvgIconComponent,
} from "@mui/icons-material";
import { ProgressEnum } from "../../../common/service/server/Types";
import { unique } from "../../util/array";
import clsx from "clsx";

const PROGRESS_ICONS: Record<ProgressEnum, SvgIconComponent> = {
  NOT_STARTED: LensIcon,
  INCOMPLETE: LensIcon,
  IN_PROGRESS: CachedIcon,
  DONE: CheckCircleIcon,
  NEEDS_REVIEW: LensIcon,
  APPROVED: CheckCircleIcon,
};

const PROGRESS_LABELS: Record<ProgressEnum, string> = {
  NOT_STARTED: "not started",
  IN_PROGRESS: "in progress",
  INCOMPLETE: "incomplete",
  DONE: "done",
  NEEDS_REVIEW: "needs review",
  APPROVED: "approved",
};

const progressToClassName = (progress: ProgressEnum, prefix = "mapping-progress--") =>
  `${prefix}${progress.toLowerCase().replace("_", "-")}`;

export const ProgressIcon = ({ progress }: { progress: ProgressEnum }) => {
  const IconClass = PROGRESS_ICONS[progress];
  return <IconClass className={`mapping-progress-icon ${progressToClassName(progress)}`} />;
};

export const progressFromSet = (progressSet: ProgressEnum[]): ProgressEnum => {
  if (progressSet.length == 0) return "NOT_STARTED";

  const uniques = unique(progressSet);
  if (uniques.length > 1) return "IN_PROGRESS";

  return uniques[0];
};

export const ProgressSpan: React.FC<{
  progress: ProgressEnum;
  label?: string;
  wide?: boolean;
}> = ({ progress, label, wide }) => {
  return (
    <span className={clsx("mapping-progress", { "mapping-progress__wide": wide })}>
      <ProgressIcon progress={progress} />
      <span className="mapping-progress-text">{label || PROGRESS_LABELS[progress]}</span>
    </span>
  );
};
