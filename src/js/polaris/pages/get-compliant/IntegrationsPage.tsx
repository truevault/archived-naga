import _ from "lodash";
import React, { useCallback, useMemo } from "react";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { EmptyInfo } from "../../components/Empty";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import {
  useOrganizationVendors,
  usePrimaryOrganization,
  usePrimaryOrganizationId,
} from "../../hooks";
import { useAuthorizations } from "../../hooks/useAuthorizations";
import { useTasks } from "../../hooks/useTasks";
import { isGoogleAnalytics } from "../../util/vendors";
import { OrganizationGoogleAnalyticsForm } from "../organization/OrganizationGoogleAnalyticsForm";
import { GetCompliantFooter } from "./ProgressFooter";

export const IntegrationsPage: React.FC = () => {
  const orgId = usePrimaryOrganizationId();
  const [org, orgRequest] = usePrimaryOrganization();
  const [dataRecipients, dataRecipientsRequest] = useOrganizationVendors(orgId);

  const updateOrganizationGAApi = useCallback(
    (submitValues) => Api.organization.updateGaWebProperty(orgId, submitValues),
    [orgId],
  );

  const { fetch: updateOrgGa, request: updateOrgGaRequest } = useActionRequest({
    api: updateOrganizationGAApi,
  });
  const debouncedUpdateOrgGa = _.debounce(updateOrgGa, 300);

  const googleAnalytics = useMemo(
    () => dataRecipients?.find((dr) => isGoogleAnalytics(dr)),
    [dataRecipients],
  );

  const { authorizations, request: authorizationsRequest } = useAuthorizations(orgId);

  const [tasks, tasksRequest, refreshTasks] = useTasks(orgId);

  const {
    fetch: addAuthorizeGoogleAnalyticsTask,
    request: addAuthorizeGoogleAnalyticsTaskRequest,
  } = useActionRequest({
    api: () => Api.tasks.createAuthorizeGoogleAnalytics(orgId),
    onSuccess: () => refreshTasks(),
  });

  const hasAuthorizeGoogleAnalyticsTask = useMemo(
    () => tasks?.some((t) => t.taskKey === "AUTHORIZE_GOOGLE_ANALYTICS") ?? false,
    [tasks],
  );

  const nextDisabled = !authorizations?.hasGoogle && !hasAuthorizeGoogleAnalyticsTask;

  return (
    <GetCompliantPageWrapper
      requests={[orgRequest, dataRecipientsRequest, authorizationsRequest, tasksRequest]}
      progressFooter={
        <GetCompliantFooter
          saveRequests={[addAuthorizeGoogleAnalyticsTaskRequest, updateOrgGaRequest]}
          presentStep="DataRetention.VendorIntegrations"
          nextDisabled={nextDisabled}
        />
      }
    >
      <PageHeader
        titleContent="API Integrations"
        descriptionContent={
          <Paras>
            <Para>
              Connect your accounts for the following services to enable our integrations with these
              services. These integrations facilitate functionality like automatic deletion of
              consumer data and respect for GPC signals.
            </Para>
          </Paras>
        }
      />

      {googleAnalytics ? (
        <OrganizationGoogleAnalyticsForm
          showDoItLater
          organization={org}
          onUpdateGAPropertyId={debouncedUpdateOrgGa}
          googleAnalytics={googleAnalytics}
          hasAuthorizeGoogleAnalyticsTask={hasAuthorizeGoogleAnalyticsTask}
          addAuthorizeGoogleAnalyticsTask={addAuthorizeGoogleAnalyticsTask}
          addAuthorizeGoogleAnalyticsTaskRequest={addAuthorizeGoogleAnalyticsTaskRequest}
        />
      ) : (
        <EmptyInfo>There are no integrations to configure</EmptyInfo>
      )}
    </GetCompliantPageWrapper>
  );
};
