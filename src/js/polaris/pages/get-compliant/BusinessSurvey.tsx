import React from "react";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { GetCompliantStep } from "../../../common/service/server/Types";
import { SurveyRenderer } from "../../components/get-compliant/survey/SurveyRenderer";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { usePrimaryOrganization } from "../../hooks";
import { useCreateDefaultCollectionGroups } from "../../hooks/useCollectionGroups";
import { Routes } from "../../root/routes";
import { businessSurvey, BUSINESS_SURVEY_NAME } from "../../surveys/businessSurvey";
import { eeaUkSurvey, EEA_UK_SURVEY_NAME } from "../../surveys/eeaUkSurvey";
import { isFinished } from "../../surveys/survey";
import { GetCompliantFooter } from "./ProgressFooter";

interface BusinessSurveyProps {
  currentProgress: GetCompliantStep;
}
export const BusinessSurvey: React.FC<BusinessSurveyProps> = ({ currentProgress }) => {
  // State
  const [org] = usePrimaryOrganization();
  const organizationId = org?.id;

  const {
    answers: bizAnswers,
    setAnswer: setAnswer,
    surveyRequest: bizSurveyRequest,
    updateRequest: bizSurveyUpdateReq,
  } = useSurvey(organizationId, BUSINESS_SURVEY_NAME);

  const {
    answers: eeaAnswers,
    setAnswer: setEeaAnswer,
    surveyRequest: eeaSurveyRequest,
    updateRequest: eeaSurveyUpdateReq,
  } = useSurvey(organizationId, EEA_UK_SURVEY_NAME);

  const { create: createDefaults, request: createDefaultsRequest } =
    useCreateDefaultCollectionGroups(organizationId);

  // Effects

  const survey = businessSurvey(bizAnswers);
  const regularFinished = isFinished(survey, bizAnswers);

  const eeaSurvey = eeaUkSurvey(eeaAnswers);
  const eeaFinished = isFinished(eeaSurvey, eeaAnswers) || !org.featureGdpr;

  const nextDisabled = !regularFinished || !eeaFinished;

  return (
    <GetCompliantPageWrapper
      requests={[bizSurveyRequest, eeaSurveyRequest]}
      currentProgress={currentProgress}
      progressFooter={
        <GetCompliantFooter
          presentStep="BusinessSurvey.BusinessSurvey"
          nextDisabled={nextDisabled}
          nextDisabledTooltip="Please answer all the questions before proceeding."
          nextOnClick={createDefaults}
          nextLabel="Done with Business Survey"
          prevLabel="Back to Dashboard"
          prevUrl={Routes.getCompliant.dashboard}
          saveRequests={[bizSurveyUpdateReq, eeaSurveyUpdateReq, createDefaultsRequest]}
        />
      }
    >
      <PageHeader
        titleContent="Business Survey"
        descriptionContent="Answer the following questions about your business's data collection practices. We'll use
        your answers to customize your onboarding steps."
      />
      <SurveyRenderer
        survey={survey}
        answers={bizAnswers}
        setAnswer={setAnswer}
        data-cy="survey.ccpa"
      />
      {regularFinished && org.featureGdpr && (
        <>
          <h4 className="intermediate">European Data Practices</h4>
          <SurveyRenderer
            survey={eeaSurvey}
            answers={eeaAnswers}
            setAnswer={setEeaAnswer}
            data-cy="survey.gdpr"
          />
        </>
      )}
    </GetCompliantPageWrapper>
  );
};
