import { Add } from "@mui/icons-material";
import { Button } from "@mui/material";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { RemoveVendorDialog } from "../../components/dialog/RemoveVendorDialog";
import {
  SurveyQuestion,
  SurveyQuestionProps,
} from "../../components/get-compliant/survey/SurveyQuestion";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { SectionHeader } from "../../components/molecules/SectionHeader";
import { DataAccessDeleteSettings } from "../../components/organisms/data-recipients/DataAccessDeleteSettings";
import { DataRecipientDocumentedInstructions } from "../../components/organisms/data-recipients/DataRecipientDocumentedInstructions";
import { StorageLocationsTable } from "../../components/organisms/data-recipients/StorageLocationsTable";
import { SurveyGateButton } from "../../components/organisms/survey/SurveyGateButton";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { useDataRecipientsCatalog, useOrganizationVendors } from "../../hooks";
import {
  UpdateVendorParams,
  useAddCustomVendor,
  useUpdateVendor,
} from "../../hooks/get-compliant/vendors/useUpdateVendor";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { VENDOR_CATEGORY_AD_NETWORK } from "../../types/Vendor";
import { useDataStorageLocations } from "../../util/resources/vendorUtils";
import { DATA_STORAGE_LOCATION_CATEGORY, getVendorSettingsObject } from "../../util/vendors";
import { CustomVendorDialog, CustomVendorFormValues } from "./data-recipients/AddDataRecipients";
import { SELLING_AND_SHARING_SURVEY_NAME } from "./data-recipients/shared";
import { GetCompliantFooter } from "./ProgressFooter";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Loading } from "../../../common/components/Loading";

const DATA_STORAGE_SURVEY_NAME = "data-storage-survey";

const DATA_STORAGE_CATEGORIES = new Set([DATA_STORAGE_LOCATION_CATEGORY]);

const ADDITIONAL_STORAGE_QUESTION: SurveyQuestionProps = {
  type: "boolean",
  slug: "store-data-internal-systems",
  visible: true,
  question:
    "Does your business store consumer personal data in any internal systems that are not represented in your Data Recipients above?",
  helpText: "For example self-managed applications or databases which store consumer data.",
};

export const DataStoragePage: React.FC = () => {
  const [addingVendor, setAddingVendor] = useState(false);
  const [confirmRemoveVendor, setConfirmRemoveVendor] = useState(null);
  const [editVendor, setEditVendor] = useState(null);
  const [isInitializing, setIsInitializing] = useState(false);
  const [isInitialized, setIsInitialized] = useState(false);

  const orgId = usePrimaryOrganizationId();
  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, DATA_STORAGE_SURVEY_NAME);
  const { answers: sharingAnswers, surveyRequest: sharingSurveyRequest } = useSurvey(
    orgId,
    SELLING_AND_SHARING_SURVEY_NAME,
  );

  const [orgVendors, orgVendorsRequest, refreshOrgVendors, invalidateOrgVendors] =
    useOrganizationVendors(orgId, undefined, true);

  const [storageLocations, regularRecipients] = useDataStorageLocations(orgVendors);
  const [vendors, vendorsRequest, refreshCatalog] = useDataRecipientsCatalog(orgId);
  const { updateVendor, updateVendorRequest } = useUpdateVendor(orgId, regularRecipients);

  useEffect(() => {
    setIsInitialized(answers && Boolean(answers["data-storage-get-started"]));
  }, [answers]);

  const enabledVendors = useMemo(
    () => regularRecipients.map((dr) => vendors.find((v) => v.id === dr.vendorId)).filter(Boolean),
    [vendors, regularRecipients],
  );

  const doneWithAccessibility = useMemo(
    () =>
      regularRecipients
        .filter((dr) => vendors.find((v) => v.id == dr.vendorId) != null)
        .every(
          (dr) =>
            Boolean(dr.dataAccessibility) &&
            (dr.dataAccessibility === "NOT_ACCESSIBLE" || Boolean(dr.dataDeletability)),
        ),
    [vendors, regularRecipients],
  );

  const answeredAdditional = Boolean(answers[ADDITIONAL_STORAGE_QUESTION.slug]);
  const hasAdditional = answers[ADDITIONAL_STORAGE_QUESTION.slug] === "true";
  const needsAdditional = hasAdditional && storageLocations.length < 1;
  const nextDisabled = !doneWithAccessibility || !answeredAdditional || needsAdditional;

  const customAudienceSelections = useMemo(
    () => JSON.parse(sharingAnswers["custom-audience-feature"] || "[]"),
    [sharingAnswers],
  );

  const unselectedAdNetworks = useMemo(
    () =>
      regularRecipients
        .filter(
          (dr) =>
            dr.category == VENDOR_CATEGORY_AD_NETWORK && !customAudienceSelections.includes(dr.id),
        )
        .map((dr) => dr.id),
    [regularRecipients, customAudienceSelections],
  );

  const noInstructionsRecipients = useMemo(
    () =>
      regularRecipients.filter(
        (r) => !r.deletionInstructions || unselectedAdNetworks.includes(r.id),
      ),
    [regularRecipients, unselectedAdNetworks],
  );

  const instructionsRecipients = useMemo(
    () =>
      regularRecipients.filter(
        (r) =>
          (r.accessInstructions || r.deletionInstructions) && !unselectedAdNetworks.includes(r.id),
      ),
    [regularRecipients, unselectedAdNetworks],
  );

  const handleRefresh = useCallback(
    () => Promise.all([refreshOrgVendors(), refreshCatalog()]),
    [refreshOrgVendors, refreshCatalog],
  );

  useEffect(() => {
    const updateInstructionRecipients = async () => {
      let updatedCount = 0;
      for (const recipient of instructionsRecipients) {
        const shouldBeDefaultDeletable =
          recipient.deletionInstructions && !unselectedAdNetworks.includes(recipient.id);
        const isDataAccessible = recipient.dataAccessibility == "ACCESSIBLE";
        const isDeletable = recipient.dataDeletability == "DELETABLE";

        // Only set deletable when the deletion instructions or is custom audience enabled (not unselected ad network)
        if (shouldBeDefaultDeletable && !isDeletable) {
          await updateVendor({
            vendorId: recipient.vendorId,
            dataAccessibility: "ACCESSIBLE",
            dataDeletability: "DELETABLE",
          });
          updatedCount++;
          // Otherwise just set accessible (the data is always accessible in this block)
        } else if (!isDataAccessible) {
          await updateVendor({
            vendorId: recipient.vendorId,
            dataAccessibility: "ACCESSIBLE",
          });
          updatedCount++;
        }
      }

      if (updatedCount > 0) {
        await handleRefresh();
      }
    };

    updateInstructionRecipients();
  }, [unselectedAdNetworks, instructionsRecipients, updateVendor, handleRefresh]);

  const updateVendorAndInvalidate = async (
    params: UpdateVendorParams,
  ): Promise<OrganizationDataRecipientDto> => {
    const updated = await updateVendor(params);
    invalidateOrgVendors();
    return updated;
  };

  const [handleAddCustomVendor, addCustomVendorRequest] = useAddCustomVendor(orgId, handleRefresh);

  const getStarted = async (slug: string, answer: string) => {
    setIsInitializing(true);
    await setAnswer(slug, answer);
    await refreshOrgVendors();
    setIsInitialized(true);
    setIsInitializing(false);
  };

  return (
    <GetCompliantPageWrapper
      requests={[surveyRequest, orgVendorsRequest, vendorsRequest, sharingSurveyRequest]}
      progressFooter={
        <GetCompliantFooter
          saveRequests={[updateVendorRequest, addCustomVendorRequest]}
          presentStep="DataRetention.DataStorage"
          nextDisabled={nextDisabled}
          nextDisabledTooltip="Please make a selection for each Data Recipient before proceeding."
        />
      }
    >
      <PageHeader
        titleContent="Access/Deletion"
        descriptionContent="Your business will be required to retrieve or delete a consumer’s personal data from your records when they make an Access or Deletion Request. On this screen, you’ll identify the locations where your business keeps consumer data."
      />

      <SurveyGateButton
        label="Got it"
        survey={answers}
        updateResponse={setAnswer}
        slug="data-storage-got-it"
      >
        <Paras>
          <Para className="mb-lg">
            Many of your vendors make consumer data accessible to you via a dashboard, where you can
            look up a particular customer by name or email address. For example, Shopify, Klaviyo,
            Zendesk and many others provide data via a dashboard and search function. In addition,
            your business may keep data in internal documents (such as in Google Workspace) and/or
            in custom-built databases or dashboards. We’ll ask you to identify any such locations on
            this screen.
          </Para>
        </Paras>

        <SurveyGateButton
          label="Get Started"
          survey={answers}
          updateResponse={getStarted}
          slug="data-storage-get-started"
        >
          {isInitializing && <Loading message={"initializing..."} />}
          {isInitialized && !isInitializing && (
            <>
              <SectionHeader helpText="Some vendors, like web hosting services and ad networks, don’t give businesses a way to look up personal information by consumer name, email address, or other identifier. Even when consumer data is accessible and searchable within a system, it’s not always possible to delete it yourself. For each vendor below, indicate whether personal data is (a) accessible to you, and (b) capable of being deleted by your organization.">
                Data Access and Deletion
              </SectionHeader>

              <DataAccessDeleteSettings
                dataRecipients={noInstructionsRecipients}
                unselectedAdNetworks={unselectedAdNetworks}
                updateVendor={updateVendorAndInvalidate}
              />

              {instructionsRecipients?.length > 0 && (
                <>
                  <SectionHeader
                    className="mt-xxl"
                    helpText="We found access and/or deletion instructions for the vendors listed below. We'll mark them as accessible."
                  >
                    Documented Access & Deletion Instructions
                  </SectionHeader>
                  <DataRecipientDocumentedInstructions dataRecipients={instructionsRecipients} />
                </>
              )}

              <SurveyQuestion
                question={ADDITIONAL_STORAGE_QUESTION}
                answer={answers[ADDITIONAL_STORAGE_QUESTION.slug]}
                onAnswer={(ans) =>
                  setAnswer(
                    ADDITIONAL_STORAGE_QUESTION.slug,
                    ans,
                    ADDITIONAL_STORAGE_QUESTION.question as string,
                  )
                }
              />

              {hasAdditional && (
                <div className="mt-lg">
                  <div className="mb-md">
                    <StorageLocationsTable
                      storageLocations={storageLocations}
                      onEdit={(storageLocation) => setEditVendor(storageLocation)}
                      onRemove={(storageLocation) =>
                        setConfirmRemoveVendor(
                          orgVendors.find((v) => v.vendorId === storageLocation.vendorId),
                        )
                      }
                    />
                  </div>

                  <Button
                    onClick={() => setAddingVendor(true)}
                    startIcon={<Add />}
                    color="secondary"
                  >
                    Add
                  </Button>
                </div>
              )}
            </>
          )}
        </SurveyGateButton>
      </SurveyGateButton>

      {/*Add Vendor Dialog*/}
      <CustomVendorDialog
        open={addingVendor}
        existingCategories={DATA_STORAGE_CATEGORIES}
        showCategories={false}
        includeUrl={false}
        defaultCategory={DATA_STORAGE_LOCATION_CATEGORY}
        header={"Add Data Storage Location"}
        confirmLabel={"Add"}
        onClose={() => setAddingVendor(false)}
        onDone={async (values) => {
          await handleAddCustomVendor({
            ...values,
            dataAccessibility: "ACCESSIBLE",
            dataRecipientType: "storage_location",
            completed: true,
          });
          setAddingVendor(false);
        }}
        enabledVendors={enabledVendors}
      />

      {/*Edit Vendor Dialog*/}
      <CustomVendorDialog
        open={!!editVendor}
        onClose={() => setEditVendor(null)}
        onDone={async (values: CustomVendorFormValues) => {
          const newValues = Object.assign(getVendorSettingsObject(editVendor), values);
          await updateVendorAndInvalidate(newValues as UpdateVendorParams);
          setEditVendor(null);
        }}
        enabledVendors={enabledVendors}
        header={"Edit Data Storage Location"}
        existingCategories={DATA_STORAGE_CATEGORIES}
        showCategories={false}
        includeUrl={false}
        vendor={editVendor}
      />

      <RemoveVendorDialog
        open={!!confirmRemoveVendor}
        onClose={() => setConfirmRemoveVendor(null)}
        onAfterRemoved={handleRefresh}
        vendorId={confirmRemoveVendor?.vendorId}
        vendorName={confirmRemoveVendor?.name}
        collectionContext={["CONSUMER"]}
      />
    </GetCompliantPageWrapper>
  );
};
