import React, { useMemo } from "react";
import { useGetCompliantContext } from "../../../polaris/root/GetCompliantContext";
import { PageLoading } from "../../components";
import { DashboardPageWrapper } from "../../components/layout/DashboardPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import {
  MappingProgressStep,
  ProgressCard,
  ProgressEnum,
} from "../../components/organisms/callouts/ProgressCard";
import {
  useOrganizationVendors,
  usePrimaryOrganization,
  usePrimaryOrganizationId,
  useSelf,
} from "../../hooks";
import { useProgressKeys } from "../../hooks/useProgressKeys";
import { getStepLogic, StepLogicContext } from "../../surveys/steps/GetCompliantStepLogic";
import {
  GetCompliantMajorStep,
  GetCompliantStep,
  MAJOR_STEPS,
  migrateStep,
  StepComparison,
} from "../../surveys/steps/GetCompliantSteps";
import {
  getIndividualStepsForMajorStep,
  getMajorStepLogic,
  getMajorStepStatus,
  MajorStepLogic,
} from "../../surveys/steps/GetCompliantMajorStepLogic";
import { ProgressFooter, ProgressFooterActions } from "./ProgressFooter";

export const GetCompliantDashboardPage: React.FC = () => {
  const { progress: currentProgress } = useGetCompliantContext();
  const organizationId = usePrimaryOrganizationId()!!;
  const [org] = usePrimaryOrganization();
  const [recipients, recipientsRequest] = useOrganizationVendors(organizationId);
  const [self] = useSelf();
  const { progress: orgProgress } = useProgressKeys(organizationId);

  const updatedProgress = migrateStep(currentProgress);
  const active = useMemo(
    () => MAJOR_STEPS.find((s) => getMajorStepStatus(s, updatedProgress).active) || MAJOR_STEPS[0],
    [updatedProgress],
  );
  const status = useMemo(
    () => getMajorStepStatus(active, updatedProgress),
    [active, updatedProgress],
  );

  const stepContext = useMemo<StepLogicContext>(
    () => ({ org, recipients, orgProgress }),
    [org, recipients, orgProgress],
  );

  if (recipientsRequest.running) {
    <PageLoading />;
  }

  return (
    <DashboardPageWrapper footer={<Footer label={status.action} url={status.href} />}>
      <PageHeader titleContent={`Hello ${self.firstName || "there"} 👋`} />

      <p className="mb-xl">
        Let's work together to get <span className="text-weight-medium">{org.name}</span> compliant
        with US Privacy Laws{org.featureGdpr ? " and GDPR" : ""}
      </p>

      {MAJOR_STEPS.filter((major) => {
        const steps = getIndividualStepsForMajorStep(major);
        return steps.some((minor) => {
          const { skip, visible } = getStepLogic(minor);
          return !skip(stepContext) && visible(stepContext);
        });
      }).map((major) => {
        const logic = getMajorStepLogic(major);
        if (!logic) {
          return undefined;
        }

        return (
          <StepProgressCard
            stepContext={stepContext}
            key={logic.label}
            logic={logic}
            step={major}
            current={updatedProgress}
          />
        );
      })}
    </DashboardPageWrapper>
  );
};

type StepProgressCardProps = {
  stepContext: StepLogicContext;
  step: GetCompliantMajorStep;
  logic: MajorStepLogic;
  current: GetCompliantStep;
};
const StepProgressCard: React.FC<StepProgressCardProps> = ({
  stepContext,
  step,
  logic,
  current,
}) => {
  const status = getMajorStepStatus(step, current);

  const steps: MappingProgressStep[] = useMemo(() => {
    const steps = getIndividualStepsForMajorStep(step);
    return steps
      .filter((minor) => {
        const { skip, visible } = getStepLogic(minor);
        return !skip(stepContext) && visible(stepContext);
      })
      .map((minor, idx) => {
        const { label, route } = getStepLogic(minor);

        return {
          label: label,
          route: route,
          progress: getMinorStepProgress(minor, current, idx),
        };
      });
  }, [step, current, stepContext]);

  return (
    <ProgressCard
      {...status}
      title={logic.label}
      className="mb-md"
      details={logic.description}
      steps={steps}
      disabled={status.progress == "NOT_STARTED"}
    />
  );
};

interface FooterProps {
  label: string;
  url: string;
}

const Footer: React.FC<FooterProps> = ({ label = "Start!", url = "#" }) => (
  <ProgressFooter
    actions={<ProgressFooterActions nextContent={label} nextUrl={url} />}
    content="You're doing great!"
  />
);

const getMinorStepProgress = (
  minor: GetCompliantStep,
  current: GetCompliantStep,
  idx: number,
): ProgressEnum => {
  if (StepComparison.isEqual(current, minor) && idx == 0) {
    return "NEXT";
  } else if (StepComparison.isEqual(current, minor)) {
    return "IN_PROGRESS";
  } else if (StepComparison.isAfter(current, minor)) {
    return "DONE";
  }
  return "NOT_STARTED";
};
