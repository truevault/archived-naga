import React, { useState } from "react";
import { CSSTransition } from "react-transition-group";
import useTimeout from "../../../common/hooks/useTimeout";

type FadeTransitionProps = {
  cls: string;
  enter?: number;
  exit?: number;
  in?: boolean;
  unmountOnExit?: boolean;
  appear?: boolean;
};
export const FadeTransition: React.FC<FadeTransitionProps> = ({
  cls,
  enter = 250,
  exit = 250,
  ...props
}) => <CSSTransition classNames={cls} timeout={{ enter, exit }} {...props} />;

export const DelayFadeTransition: React.FC<FadeTransitionProps & { delay?: number }> = ({
  delay,
  ...props
}) => {
  const [delayIn, setDelayIn] = useState(props.in ?? false);
  useTimeout(() => setDelayIn(true), delay);
  return <FadeTransition {...props} in={delayIn} />;
};
