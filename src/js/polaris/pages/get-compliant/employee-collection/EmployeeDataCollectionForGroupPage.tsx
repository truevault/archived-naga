import React, { useCallback, useEffect, useMemo } from "react";
import { useParams } from "react-router";
import { useActionRequest } from "../../../../common/hooks/api";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { Api } from "../../../../common/service/Api";
import { MappingProgressEnum, UUIDString } from "../../../../common/service/server/Types";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { EmployeeDataCollectionForm } from "../../../components/organisms/collection-groups/EmployeeDataCollectionForm";
import {
  collectsCommercialInfoSlug,
  EMPLOYEE_COLLECTION_PURPOSES_SURVEY,
} from "../../../components/organisms/collection-groups/EmploymentGroupPurposesQuestion";
import { Error as OrganizationError } from "../../../copy/organization";
import { usePrimaryOrganization } from "../../../hooks";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../hooks/useDataMap";
import { useGetCompliantContext } from "../../../root/GetCompliantContext";
import { Routes } from "../../../root/routes";
import { GetCompliantFooter } from "../ProgressFooter";

export const EmployeeCollectionForGroupPage: React.FC = () => {
  // Variables
  const { progress } = useGetCompliantContext();

  const [org, orgRequest] = usePrimaryOrganization();
  const params = useParams() as { consumerGroupId: UUIDString };
  const orgId = org.id;

  // Requests
  const [dataMap, dataMapRequest] = useDataMap(orgId);
  const [collectionGroups, consumerGroupsRequest] = useCollectionGroups(orgId);

  const group = useMemo(
    () => collectionGroups?.find((cg) => cg.id === params.consumerGroupId),
    [collectionGroups, params.consumerGroupId],
  );

  const { answers, setAnswer, surveyRequest } = useSurvey(
    orgId,
    group ? `consumer-data-collection-${group.id}` : undefined,
  );

  const { answers: collectionAnswers } = useSurvey(orgId, EMPLOYEE_COLLECTION_PURPOSES_SURVEY);
  const collectionSlug = collectsCommercialInfoSlug(group);

  const updateMappingProgressApi = useCallback(
    (progress: MappingProgressEnum) => {
      return Api.consumerGroups.updateMappingProgress(orgId, group.id, "CATEGORY", progress);
    },
    [orgId, group?.id],
  );

  const updateMappingMessages = useMemo(
    () => ({
      forceError: OrganizationError.UpdateMappingProgress,
    }),
    [],
  );

  const { fetch: updateMappingProgress } = useActionRequest({
    api: updateMappingProgressApi,
    messages: updateMappingMessages,
  });

  // Effects
  useEffect(() => {
    const current = group?.mappingProgress?.find((p) => p.personalInformationType == "CATEGORY");
    if (current?.value == "NOT_STARTED") {
      updateMappingProgress("IN_PROGRESS");
    }
  }, [updateMappingProgress, group?.mappingProgress]);

  const nextDisabled = !collectionAnswers[collectionSlug];

  const footer = (
    <GetCompliantFooter
      presentStep="EmployeeCollection.DataCollection"
      prevUrl={Routes.getCompliant.employeeMap.employee}
      nextUrl={Routes.getCompliant.employeeMap.employee}
      nextOnClick={async () => {
        await updateMappingProgress("DONE");
      }}
      nextLabel="Done"
      nextDisabled={nextDisabled}
      nextDisabledTooltip="Please make a selection."
    />
  );

  return (
    <GetCompliantPageWrapper
      currentProgress={progress}
      progressFooter={footer}
      requests={[orgRequest, consumerGroupsRequest, dataMapRequest, surveyRequest]}
    >
      <PageHeader titleContent={group?.name} />

      <>
        <h4 className="intermediate">What information do you collect about {group?.name}?</h4>

        <EmployeeDataCollectionForm
          dataMap={dataMap}
          org={org}
          collectionGroup={group}
          answers={answers}
          setAnswer={setAnswer}
        />
      </>
    </GetCompliantPageWrapper>
  );
};
