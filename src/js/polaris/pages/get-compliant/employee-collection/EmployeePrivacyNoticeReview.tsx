import React, { useCallback } from "react";
import { useHistory, useParams } from "react-router-dom";
import { useActionRequest, useDataRequest } from "../../../../common/hooks/api";
import { Api } from "../../../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { GetCompliantStep, UUIDString } from "../../../../common/service/server/Types";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../../hooks";
import { useCollectionGroup } from "../../../hooks/useCollectionGroups";
import { Routes } from "../../../root/routes";
import { ProgressFooter, ProgressFooterActions } from "../ProgressFooter";

const EmployeePrivacyNoticeHeader: React.FC<{
  title: string;
  groupName: string;
}> = ({ title, groupName }) => (
  <PageHeader
    titleContent={title}
    descriptionContent={
      <p className="text-t3 m-0">
        Your notice to {groupName} is below. If the information looks incorrect, go back to make
        changes. We will help you post this notice in a later step. The intro paragraph in the
        notice can be customized in your account after onboarding is complete.
      </p>
    }
  />
);

const PureEmployeePrivacyNoticeHeader = React.memo(EmployeePrivacyNoticeHeader);

interface EmployeePrivacyNoticeReviewFooterProps {
  organization: OrganizationDto;
  consumerGroup?: CollectionGroupDetailsDto;
}
const EmployeePrivacyNoticeReviewFooter: React.FC<EmployeePrivacyNoticeReviewFooterProps> = ({
  organization,
  consumerGroup,
}) => {
  // Variables
  const history = useHistory();

  const updateConsumerGroupApi = useCallback(
    () =>
      Api.consumerGroups.updatePrivacyNoticeProgress(organization.id, consumerGroup.id, "APPROVED"),
    [organization.id, consumerGroup.id],
  );

  const { fetch: updateConsumerGroup } = useActionRequest({
    api: updateConsumerGroupApi,
    onSuccess: () => history.push(Routes.getCompliant.dataCollection.employmentNoticeReview),
  });

  const onNext = async () => {
    updateConsumerGroup();
  };

  return (
    <ProgressFooter
      actions={
        <ProgressFooterActions
          prevUrl={Routes.getCompliant.dataCollection.employmentNoticeReview}
          nextContent="Approve Notice"
          nextOnClick={onNext}
        />
      }
    />
  );
};

const PureEmployeePrivacyNoticeReviewFooter = React.memo(EmployeePrivacyNoticeReviewFooter);

interface EmployeePrivacynoticeReviewProps {
  currentProgress: GetCompliantStep;
}
export const EmployeePrivacyNoticeReview: React.FC<EmployeePrivacynoticeReviewProps> = ({
  currentProgress,
}) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();
  const { consumerGroupId } = useParams<{ consumerGroupId: UUIDString }>();

  // Requests
  const [organization, organizationRequest] = usePrimaryOrganization();

  const [consumerGroup, collectionGroupsRequest] = useCollectionGroup(
    organizationId,
    consumerGroupId,
  );

  const noticeDtoApi = useCallback(
    () =>
      consumerGroupId
        ? Api.privacyNotice.getConsumerGroupPrivacyNotice(organizationId, consumerGroupId)
        : Api.privacyNotice.getCaPrivacyNotice(organizationId),
    [organizationId, consumerGroupId],
  );

  const { result: noticeDto, request: noticeRequest } = useDataRequest({
    queryKey: ["notice", organizationId, consumerGroupId],
    api: noticeDtoApi,
  });

  const header = (
    <PureEmployeePrivacyNoticeHeader
      title={`Notice to ${consumerGroup.name}`}
      groupName={consumerGroup.name}
    />
  );

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      requests={[collectionGroupsRequest, organizationRequest, noticeRequest]}
      progressFooter={
        <PureEmployeePrivacyNoticeReviewFooter
          organization={organization}
          consumerGroup={consumerGroup}
        />
      }
    >
      {header}
      <div className="privacy-notices--review-container">
        <Callout className="w-896" variant={CalloutVariant.Clear}>
          <div
            key={consumerGroup.id}
            className="p-md text-color-black callout--children trim-first-child "
            dangerouslySetInnerHTML={{ __html: noticeDto?.notice }}
          />
        </Callout>
      </div>
    </GetCompliantPageWrapper>
  );
};
