import { Typography } from "@mui/material";
import React, { useMemo } from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { ActionRow } from "../../../components/ActionRow";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { RouteHelpers } from "../../../root/routes";
import { GetCompliantContainer } from "../GetCompliantContainer";
import { GetCompliantFooter } from "../ProgressFooter";
import { ProgressSpan } from "../ProgressSpan";

export const EmployeeNoticeReviewPage: React.FC = () => {
  // Variables
  const organizationId = usePrimaryOrganizationId()!!;

  // State
  const [dataSubjectTypes, dstRequest] = useCollectionGroups(organizationId);

  const employeeGroups = useMemo(
    () => dataSubjectTypes.filter((dst) => dst.collectionGroupType == "EMPLOYMENT"),
    [dataSubjectTypes],
  );

  const anyNotDone = employeeGroups.some((eg) => eg.privacyNoticeProgress.value === "NEEDS_REVIEW");

  return (
    <GetCompliantPageWrapper
      requests={[dstRequest]}
      progressFooter={
        <GetCompliantFooter
          nextDisabled={anyNotDone}
          nextDisabledTooltip="Map your employment groups before proceeding."
          presentStep="ConsumerCollection.ReviewEmploymentPrivacyNotices"
        />
      }
    >
      <PageHeader
        titleContent="HR Notices"
        descriptionContent={
          <p>Review the employment notices for each of your employment groups below.</p>
        }
      />

      <GetCompliantContainer className="data-sharing">
        <CollectionBody employeeGroups={employeeGroups} />
      </GetCompliantContainer>
    </GetCompliantPageWrapper>
  );
};

type CollectionBodyProps = {
  employeeGroups: CollectionGroupDetailsDto[];
};

const CollectionBody: React.FC<CollectionBodyProps> = ({ employeeGroups }) => {
  if (employeeGroups.length == 0) {
    return (
      <div className="empty-state text-style-italic">
        <Typography variant="body1" paragraph>
          Based on your Survey responses, this step is not required. Click “Done” to proceed to the
          next step.
        </Typography>
      </div>
    );
  }
  return (
    <>
      <EmployeeGroupsList employeeGroups={employeeGroups} />
    </>
  );
};

type EmployeeGroupsListProps = {
  employeeGroups: CollectionGroupDetailsDto[];
};

const EmployeeGroupsList: React.FC<EmployeeGroupsListProps> = ({ employeeGroups }) => {
  return (
    <>
      {employeeGroups
        .slice(0)
        .sort((a, b) => a.name.localeCompare(b.name))
        .map((cg, idx) => {
          return (
            <ActionRow
              key={`group-${cg.id}`}
              label={cg.name}
              topBorder={idx == 0}
              status={<ProgressSpan progress={cg.privacyNoticeProgress.value} />}
              rowHref={RouteHelpers.getCompliant.employeeMap.employeeNoticeReviewForGroup(cg.id)}
            />
          );
        })}
    </>
  );
};
