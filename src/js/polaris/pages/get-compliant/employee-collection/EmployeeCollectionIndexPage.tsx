import { Typography } from "@mui/material";
import React, { useMemo } from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { ActionRow } from "../../../components/ActionRow";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { RouteHelpers } from "../../../root/routes";
import { GetCompliantContainer } from "../GetCompliantContainer";
import { GetCompliantFooter, ProgressFooter } from "../ProgressFooter";
import { ProgressSpan } from "../ProgressSpan";

const EmployeeCollectionHeader: React.FC = () => {
  const description = (
    <Paras>
      <Para>Map your business’s collection practices for each Employment Group below.</Para>
      <Para>This section may require input from your HR team.</Para>
    </Paras>
  );
  return <PageHeader titleContent="Collection" descriptionContent={description} />;
};

interface EmployeeCollectionFooterProps {
  employeeGroups: CollectionGroupDetailsDto[];
}
const EmployeeCollectionFooter: React.FC<EmployeeCollectionFooterProps> = ({ employeeGroups }) => {
  const anyNotDone = employeeGroups.some(
    (eg) =>
      eg.mappingProgress.find((p) => p.personalInformationType == "CATEGORY")?.value != "DONE",
  );

  return (
    <ProgressFooter
      actions={
        <GetCompliantFooter
          nextDisabled={anyNotDone}
          nextDisabledTooltip="Map your employment groups before proceeding."
          presentStep="EmployeeCollection.DataCollection"
        />
      }
    />
  );
};

export const EmployeeCollectionIndexPage: React.FC = () => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [dataSubjectTypes, dstRequest] = useCollectionGroups(organizationId);

  const employeeGroups = useMemo(
    () => dataSubjectTypes.filter((dst) => dst.collectionGroupType == "EMPLOYMENT"),
    [dataSubjectTypes],
  );

  return (
    <GetCompliantPageWrapper
      requests={[dstRequest]}
      progressFooter={<EmployeeCollectionFooter employeeGroups={employeeGroups} />}
    >
      <EmployeeCollectionHeader />

      <GetCompliantContainer className="data-sharing">
        <CollectionBody employeeGroups={employeeGroups} />
      </GetCompliantContainer>
    </GetCompliantPageWrapper>
  );
};

type CollectionBodyProps = {
  employeeGroups: CollectionGroupDetailsDto[];
};

const CollectionBody: React.FC<CollectionBodyProps> = ({ employeeGroups }) => {
  if (employeeGroups.length == 0) {
    return (
      <div className="empty-state text-style-italic">
        <Typography variant="body1" paragraph>
          Based on your Survey responses, this step is not required. Click “Done” to proceed to the
          next step.
        </Typography>
      </div>
    );
  }
  return (
    <>
      <EmployeeGroupsList employeeGroups={employeeGroups} />
    </>
  );
};

type EmployeeGroupsListProps = {
  employeeGroups: CollectionGroupDetailsDto[];
};

const EmployeeGroupsList: React.FC<EmployeeGroupsListProps> = ({ employeeGroups }) => {
  return (
    <>
      {employeeGroups
        .slice(0)
        .sort((a, b) => a.name.localeCompare(b.name))
        .map((cg, idx) => {
          const categoryProgress = cg.mappingProgress.find(
            (p) => p.personalInformationType == "CATEGORY",
          );
          return (
            <ActionRow
              key={`group-${cg.id}`}
              label={cg.name}
              topBorder={idx == 0}
              status={<ProgressSpan progress={categoryProgress?.value} />}
              rowHref={RouteHelpers.getCompliant.employeeMap.employeeCollectionForGroup(cg.id)}
            />
          );
        })}
    </>
  );
};
