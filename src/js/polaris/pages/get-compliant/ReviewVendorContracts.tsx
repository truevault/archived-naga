import { Typography } from "@mui/material";
import _ from "lodash";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { VendorContractReviewedDto } from "../../../common/service/server/dto/VendorDto";
import { GetCompliantStep, UUIDString } from "../../../common/service/server/Types";
import { Loading } from "../../components";
import { Callout, CalloutVariant } from "../../components/Callout";
import { ClassifyVendorsFooter } from "../../components/get-compliant/vendors/ClassifyVendorsFooter";
import { ClassifyVendorsHeader } from "../../components/get-compliant/vendors/ClassifyVendorsHeader";
import { VendorContractReview } from "../../components/get-compliant/vendors/VendorContractReview";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { useUpdateVendor } from "../../hooks/get-compliant/vendors/useUpdateVendor";
import { Routes } from "../../root/routes";
import { unknownVendorsByContract, vendorsByType } from "../../util/vendors";

interface ReviewVendorContractsProps {
  currentProgress: GetCompliantStep;
}

export const ReviewVendorContracts = ({ currentProgress }: ReviewVendorContractsProps) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);
  const [reviewContractComplete, setReviewContractComplete] = useState(false);
  const [reviewPublicTosComplete, setReviewTosComplete] = useState(false);
  const [anyContractNotFound, setAnyContractNotFound] = useState(false);
  const [anyPublicTosNotFound, setAnyPublicTosNotFound] = useState(false);

  useEffect(() => {
    setReviewContractComplete(false);
    setReviewTosComplete(false);
  }, [organizationId]);

  // Requests
  const [orgVendors, orgVendorsRequest] = useOrganizationVendors(organizationId);
  const { otherVendors } = useMemo(() => vendorsByType(orgVendors || []), [orgVendors]);

  const { contractTermsVendors, publicTermsVendors } = useMemo(
    () => unknownVendorsByContract(otherVendors || []),
    [otherVendors],
  );

  const { updateVendor, updateVendorRequest } = useUpdateVendor(organizationId, orgVendors);

  useEffect(() => {
    if (orgVendorsRequest.success) {
      setReadyToRender(true);
    }
  }, [otherVendors, orgVendorsRequest, setReadyToRender]);

  const vendorContractReviewedBySlug = useMemo(
    () =>
      orgVendors?.[0]?.vendorContractReviewedOptions?.reduce(
        (acc, c) => Object.assign(acc, { [c.slug]: c }),
        {} as Record<string, VendorContractReviewedDto>,
      ),
    [orgVendors],
  );

  const notFoundId = vendorContractReviewedBySlug?.["provider-language-not-found"]?.id;

  const reviewContractAnswersChanged = useCallback(
    (vendorContractsReviewed: Record<UUIDString, UUIDString>) => {
      const allOptionsHaveSelection = _.every(vendorContractsReviewed, Boolean);
      const anyNotFound = _.some(vendorContractsReviewed, (id) => notFoundId && id == notFoundId);
      setReviewContractComplete(allOptionsHaveSelection);
      setAnyContractNotFound(anyNotFound);
    },
    [setReviewContractComplete, notFoundId],
  );

  const reviewPublicTosAnswersChanged = useCallback(
    (vendorContractsReviewed: Record<UUIDString, UUIDString>) => {
      const allOptionsHaveSelection = _.every(vendorContractsReviewed, Boolean);
      const anyNotFound = _.some(vendorContractsReviewed, (id) => notFoundId && id == notFoundId);
      setReviewTosComplete(allOptionsHaveSelection);
      setAnyPublicTosNotFound(anyNotFound);
    },
    [setReviewTosComplete, notFoundId],
  );

  useEffect(() => {
    const contractAnswers = _.mapValues(
      _.keyBy(contractTermsVendors, "vendorId"),
      (cv) => cv.vendorContractReviewed?.id,
    );
    const publicTosAnswers = _.mapValues(
      _.keyBy(publicTermsVendors, "vendorId"),
      (cv) => cv.vendorContractReviewed?.id,
    );

    reviewContractAnswersChanged(contractAnswers);
    reviewPublicTosAnswersChanged(publicTosAnswers);
  }, [
    contractTermsVendors,
    publicTermsVendors,
    reviewContractAnswersChanged,
    reviewPublicTosAnswersChanged,
  ]);

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper currentProgress={currentProgress}>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  const nextUrl =
    anyContractNotFound || anyPublicTosNotFound
      ? Routes.getCompliant.dataRecipients.contact
      : Routes.getCompliant.privacyRequests.privacyCenter;

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <ClassifyVendorsFooter
          organizationId={organizationId}
          currentRequest={updateVendorRequest}
          nextDisabled={!reviewContractComplete || !reviewPublicTosComplete}
          prevLabel="Back"
          prevUrl={Routes.getCompliant.dataRecipients.locateOther}
          nextUrl={nextUrl}
          nextLabel="Next"
          nextProgress="PrivacyCenter.EmailAccount"
          currentProgress={currentProgress}
          disabledNextTooltip="Please make a selection for each vendor"
        />
      }
    >
      <ClassifyVendorsHeader title="Review Vendor Contracts">
        <Paras>
          <Para>Next, review the documentation you located in the previous step.</Para>
          <Para>
            Review your contract or the vendor's public terms for service provider language.
          </Para>
        </Paras>
      </ClassifyVendorsHeader>

      <div className="mt-md mb-xxl">
        <Callout variant={CalloutVariant.Purple}>
          <Paras>
            <Typography variant="body2">Look for statements like:</Typography>
            <Typography variant="body2">
              “[Vendor] is a ‘service provider’ as defined in the CCPA.”
            </Typography>
            <Typography variant="body2">or</Typography>
            <Typography variant="body2">
              “[Vendor] will not sell, retain, use, or disclose Personal Information for any purpose
              other than for the specific purpose of performing the services specified herein."
            </Typography>
            <Typography variant="body2">
              <a
                href="https://help.truevault.com/article/154-identifying-service-provider-language"
                target="_blank"
                rel="noopener noreferrer"
                className="link--primary"
              >
                See more examples
              </a>
            </Typography>
          </Paras>
        </Callout>
      </div>

      <VendorContractReview
        title={"Review Contract"}
        contractReviewedOptions={vendorContractReviewedBySlug}
        vendors={contractTermsVendors}
        hideNeedsReview
        onChange={reviewContractAnswersChanged}
        updateVendor={updateVendor}
      />

      <VendorContractReview
        title={"Review Public Terms"}
        contractReviewedOptions={vendorContractReviewedBySlug}
        vendors={publicTermsVendors}
        hideNeedsReview
        onChange={reviewPublicTosAnswersChanged}
        updateVendor={updateVendor}
        showTos
      />
    </GetCompliantPageWrapper>
  );
};
