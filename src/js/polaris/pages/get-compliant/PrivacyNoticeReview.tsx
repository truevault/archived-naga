import { KeyboardArrowLeft } from "@mui/icons-material";
import clsx from "clsx";
import qs from "query-string";
import React, { useEffect, useState } from "react";
import { Link, useHistory, useLocation, useParams } from "react-router-dom";
import { ExternalLink } from "../../../common/components/ExternalLink";
import { Loading } from "../../../common/components/Loading";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { UpdateOrganizationPrivacyNoticeProgressDto } from "../../../common/service/server/controller/OrganizationController";
import { CollectionGroupDetailsDto } from "../../../common/service/server/dto/CollectionGroupDto";
import { OrganizationDto } from "../../../common/service/server/dto/OrganizationDto";
import { GetCompliantStep, UUIDString } from "../../../common/service/server/Types";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../hooks";
import { Routes } from "../../root/routes";
import { ProgressFooter, ProgressFooterActions } from "./ProgressFooter";

type NoticeType = "CA_PRIVACY_NOTICE" | "CONSUMER_GROUP";

const PrivacyNoticeHeader: React.FC<{
  title: string;
  noticeType?: NoticeType;
  detailText?: React.ReactNode;
}> = ({ title, detailText, noticeType }) => (
  <PageHeader
    titleHeaderContent="Privacy Center"
    titleContent={title}
    descriptionContent={
      <Paras>
        <Para>Review the notice and select Approve Notice when finished.</Para>

        {detailText}

        {noticeType === "CA_PRIVACY_NOTICE" && (
          <Para>
            <ExternalLink
              className="text-link text-medium"
              href="https://help.truevault.com/article/136-requirements-for-ccpa-notices"
              target="_blank"
            >
              What legal requirements does this notice satisfy?
            </ExternalLink>
          </Para>
        )}
      </Paras>
    }
  />
);

const PurePrivacyNoticeHeader = React.memo(PrivacyNoticeHeader);

interface PrivacyNoticeReviewFooterProps {
  organization: OrganizationDto;
  refreshOrganization: () => void;
  noticeType: NoticeType;
  consumerGroup?: CollectionGroupDetailsDto;
}
const PrivacyNoticeReviewFooter: React.FC<PrivacyNoticeReviewFooterProps> = ({
  organization,
  refreshOrganization,
  noticeType,
  consumerGroup,
}) => {
  // Variables
  const history = useHistory();
  const currentOrgProgress: UpdateOrganizationPrivacyNoticeProgressDto = {
    caPrivacyNoticeProgress: organization.caPrivacyNoticeProgress.value,
    optOutPrivacyNoticeProgress: organization.optOutPrivacyNoticeProgress.value,
  };
  const newOrgProgress =
    noticeType === "CA_PRIVACY_NOTICE"
      ? Object.assign({}, currentOrgProgress, { caPrivacyNoticeProgress: "APPROVED" })
      : currentOrgProgress;

  // Requests
  const { fetch: updateOrganization } = useActionRequest({
    api: () => Api.organization.updatePrivacyNoticeProgress(organization.id, newOrgProgress),
    onSuccess: () => {
      refreshOrganization();
      history.push(Routes.getCompliant.privacyRequests.privacyCenter);
    },
  });

  const { fetch: updateConsumerGroup } = useActionRequest({
    api: () =>
      Api.consumerGroups.updatePrivacyNoticeProgress(organization.id, consumerGroup.id, "APPROVED"),
    onSuccess: () => history.push(Routes.getCompliant.privacyRequests.privacyCenter),
  });

  const approve = consumerGroup ? updateConsumerGroup : updateOrganization;

  return (
    <ProgressFooter
      actions={
        <ProgressFooterActions
          prevContent={
            <span className="flex-center-vertical">
              <KeyboardArrowLeft /> Back to Privacy Center
            </span>
          }
          prevOnClick={() => history.push(Routes.getCompliant.privacyRequests.privacyCenter)}
          nextContent="Approve Notice"
          nextOnClick={approve}
        />
      }
    />
  );
};

const PurePrivacyNoticeReviewFooter = React.memo(PrivacyNoticeReviewFooter);

interface PrivacynoticeReviewProps {
  currentProgress: GetCompliantStep;
  noticeType: NoticeType;
}
export const PrivacyNoticeReview: React.FC<PrivacynoticeReviewProps> = ({
  currentProgress,
  noticeType,
}) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();
  const { consumerGroupId } = useParams<{ consumerGroupId: UUIDString }>();
  const location = useLocation();
  const { print } = qs.parse(location.search);
  const printView = print == "1" || print == "true";

  // State
  const [readyToRender, setReadyToRender] = useState(false);
  const [consumerGroup, setConsumerGroup] = useState<CollectionGroupDetailsDto>(null);

  // Requests
  const [organization, organizationRequest, refreshOrganization] = usePrimaryOrganization();

  const { fetch: getConsumerGroups } = useActionRequest({
    api: () => Api.consumerGroups.getCollectionGroups(organizationId),
    onSuccess: (result: CollectionGroupDetailsDto[]) => {
      setConsumerGroup(result.find((cg) => cg.id === consumerGroupId));
    },
  });

  const { result: noticeDto, request: noticeRequest } = useDataRequest({
    queryKey: ["notice", organizationId, consumerGroup],
    api: () =>
      consumerGroupId
        ? Api.privacyNotice.getConsumerGroupPrivacyNotice(organizationId, consumerGroupId)
        : Api.privacyNotice.getCaPrivacyNotice(organizationId),
  });

  useEffect(() => {
    if (consumerGroupId) {
      getConsumerGroups();
    }
  }, [consumerGroupId, getConsumerGroups]);

  useEffect(() => {
    if (
      organizationRequest.success &&
      noticeRequest.success &&
      (!consumerGroupId || consumerGroup)
    ) {
      setReadyToRender(true);
    }
  }, [consumerGroupId, organizationRequest, consumerGroup, noticeRequest]);

  if (!readyToRender) {
    return <Loading />;
  }

  const header = consumerGroup ? (
    <PurePrivacyNoticeHeader
      title={`Notice to ${consumerGroup.name}`}
      noticeType={noticeType}
      detailText={
        <Para>
          If the disclosures are incomplete or inaccurate, review your{" "}
          <Link className="text-link text-medium" to={Routes.getCompliant.employeeMap.employee}>
            Employee Collection
          </Link>
          .
        </Para>
      }
    />
  ) : (
    <PurePrivacyNoticeHeader
      title="Review Your Privacy Notice"
      noticeType={noticeType}
      detailText={
        <Para>
          If the disclosures are incomplete or inaccurate, review your{" "}
          <Link
            className="text-link text-medium"
            to={Routes.getCompliant.dataRecipients.dataDisclosures}
          >
            Data Disclosures
          </Link>
          .
        </Para>
      }
    />
  );

  if (printView) {
    return (
      <div
        className={clsx("privacy-notices--review-container", {
          "privacy-notices--print": printView,
        })}
        dangerouslySetInnerHTML={{ __html: noticeDto.notice }}
      />
    );
  }

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <PurePrivacyNoticeReviewFooter
          organization={organization}
          refreshOrganization={refreshOrganization}
          consumerGroup={consumerGroup}
          noticeType={noticeType}
        />
      }
    >
      {header}
      <div className={"privacy-notices--review-container"}>
        <div dangerouslySetInnerHTML={{ __html: noticeDto.notice }} />
      </div>
    </GetCompliantPageWrapper>
  );
};
