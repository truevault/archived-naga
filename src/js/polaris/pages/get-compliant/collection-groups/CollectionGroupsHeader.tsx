import React from "react";
import { PageHeader } from "../../../components/layout/header/Header";

export const CollectionGroupsHeader = () => {
  const description =
    "Over the next few screens, we will guide you through the data-mapping process where we identify what personal information you collect and disclose about consumers. We've created the following collection group(s) for you to map based on your Survey answers. Edit the Description or Nickname it needed to match your business.";

  return <PageHeader titleContent="Collection Groups" descriptionContent={description} />;
};
