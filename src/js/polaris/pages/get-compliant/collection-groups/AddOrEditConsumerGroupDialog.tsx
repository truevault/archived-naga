import { Dialog } from "@mui/material";
import React from "react";
import { useActionRequest } from "../../../../common/hooks/api";
import { Api } from "../../../../common/service/Api";
import { CollectionGroupType } from "../../../../common/service/server/controller/CollectionGroupsController";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { CloseableDialogTitle } from "../../../components/dialog/CloseableDialogTitle";
import { Error as OrganizationError } from "../../../copy/organization";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { AddOrEditConsumerGroupForm } from "./AddOrEditConsumerGroupForm";

interface AddEditConsumerGroupDialogProps {
  open: boolean;
  onClose: () => void;
  onDone: () => void;
  existingGroups: CollectionGroupDetailsDto[];
  collectionGroupType?: CollectionGroupType;
  editGroup?: CollectionGroupDetailsDto;
  gdprDataSubject?: boolean;
}

export const AddOrEditConsumerGroupDialog = ({
  open,
  onClose,
  onDone,
  existingGroups,
  collectionGroupType,
  editGroup,
  gdprDataSubject = false,
}: AddEditConsumerGroupDialogProps) => {
  const organizationId = usePrimaryOrganizationId();
  const initialValues = editGroup
    ? {
        name: editGroup.name,
        description: editGroup.description,
        collectionGroupType: editGroup.collectionGroupType,
      }
    : { collectionGroupType: collectionGroupType };

  const { fetch: create, request: createRequest } = useActionRequest({
    api: (values) => {
      return Api.consumerGroups.createCollectionGroup(organizationId, {
        name: values.name,
        description: values.description,
        enabled: true,
        privacyCenterEnabled: true,
        collectionGroupType: values.collectionGroupType,
        gdprDataSubject: gdprDataSubject,
        processingRegions: gdprDataSubject ? ["EEA_UK"] : null,
      });
    },
    messages: {
      forceError: OrganizationError.AddDataSubjectType,
    },
    onSuccess: () => onDone(),
  });

  const { fetch: update, request: updateRequest } = useActionRequest({
    api: (values) => {
      return Api.consumerGroups.updateCollectionGroup(organizationId, editGroup.id, {
        name: values.name,
        description: values.description,
        enabled: true,
        privacyCenterEnabled: true,
        collectionGroupType: values.collectionGroupType,
        gdprEeaUkDataCollection: editGroup.gdprEeaUkDataCollection,
      });
    },
    messages: {
      forceError: OrganizationError.UpdateCollectionGroup,
    },
    onSuccess: () => onDone(),
  });

  const actionFn = !editGroup ? create : update;
  const actionRequest = !editGroup ? createRequest : updateRequest;
  const submitLoading = actionRequest.running;

  return (
    <Dialog
      className="add-edit-consumer-group-dialog"
      fullWidth={true}
      open={open}
      onClose={onClose}
      aria-labelledby="add-edit-consumer-group-dialog-title"
      transitionDuration={{ enter: 200, exit: 0 }}
    >
      <CloseableDialogTitle onClose={onClose}>
        {editGroup ? "Edit" : "Add"} Collection Group
      </CloseableDialogTitle>

      <AddOrEditConsumerGroupForm
        onSubmit={actionFn}
        initialValues={initialValues}
        existingGroups={existingGroups}
        editGroup={editGroup}
        submitLoading={submitLoading}
      />
    </Dialog>
  );
};
