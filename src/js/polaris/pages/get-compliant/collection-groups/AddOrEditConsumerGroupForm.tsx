import { DialogActions, DialogContent, FormLabel, InputAdornment } from "@mui/material";
import React, { useMemo } from "react";
import { Field, Form } from "react-final-form";
import { Text } from "../../../../common/components/input/Text";
import { LoadingButton } from "../../../../common/components/LoadingButton";
import { CollectionGroupType } from "../../../../common/service/server/controller/CollectionGroupsController";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { composeValidators, validateNotIn, validateRequired } from "../../../util/validation";

interface EditCGValues {
  name?: string;
  description?: string;
  collectionGroupType?: CollectionGroupType;
}

interface SubmitCGValues {
  name?: string;
  description?: string;
  collectionGroupType?: CollectionGroupType;
}

interface AddOrEditConsumerGroupFormProps {
  onSubmit: (values: SubmitCGValues) => void;
  initialValues: EditCGValues;
  existingGroups: CollectionGroupDetailsDto[];
  editGroup?: CollectionGroupDetailsDto;
  submitLoading: boolean;
}

export const AddOrEditConsumerGroupForm = ({
  onSubmit,
  initialValues,
  existingGroups,
  editGroup,
  submitLoading,
}: AddOrEditConsumerGroupFormProps) => {
  // State
  const otherExistingGroupNames = useMemo(
    () => existingGroups.map((g) => g.name).filter((n) => n !== initialValues.name),
    [existingGroups, initialValues.name],
  );

  const groupType: CollectionGroupType | undefined = initialValues.collectionGroupType;

  const handleSubmit = (values) => {
    onSubmit({
      name: values.name,
      description: values.description,
      collectionGroupType: values.collectionGroupType,
    });
  };

  return (
    <div>
      <Form onSubmit={handleSubmit} initialValues={initialValues}>
        {(form) => {
          return (
            <form onSubmit={form.handleSubmit}>
              <DialogContent>
                <FormLabel>
                  First, create a Nickname for this group (ex: "Retail Shoppers")
                </FormLabel>

                <Text
                  field="name"
                  placeholder="Nickname"
                  required
                  margin="normal"
                  validate={composeValidators(
                    validateRequired,

                    validateNotIn(
                      otherExistingGroupNames,
                      "A Collection Group with this name already exists.",
                    ),
                  )}
                />

                <FormLabel>Next, describe how this group interacts with you</FormLabel>

                <Text
                  field="description"
                  label=""
                  required
                  validate={validateRequired}
                  margin="normal"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <strong>{form.values.name || "Nickname"}</strong>&nbsp;are people who:
                      </InputAdornment>
                    ),
                  }}
                />

                <Field
                  type="hidden"
                  component={"input"}
                  value={groupType}
                  name="collectionGroupType"
                />
              </DialogContent>

              <DialogActions>
                <LoadingButton
                  type="submit"
                  color="primary"
                  className="add-edit-consumer-group-btn"
                  onClick={form.handleSubmit}
                  loading={submitLoading || form.submitting}
                  disabled={submitLoading || !form.valid}
                >
                  {editGroup ? "Save Group" : "Add Group"}
                </LoadingButton>
              </DialogActions>
            </form>
          );
        }}
      </Form>
    </div>
  );
};
