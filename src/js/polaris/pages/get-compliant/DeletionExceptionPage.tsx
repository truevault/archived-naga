import _ from "lodash";
import React, { useCallback, useMemo, useState } from "react";
import { useActionRequest } from "../../../common/hooks/api";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { Api } from "../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import { RetentionReason } from "../../../common/service/server/dto/RetentionReasonDto";
import { GetCompliantStep, VendorPublicId } from "../../../common/service/server/Types";
import { Callout, CalloutVariant } from "../../components/Callout";
import { DeletionExceptionsVendorRow } from "../../components/deletionExceptions/VendorRow";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { DATA_RETENTION_SURVEY_NAME } from "../../components/organisms/data-retention/DataRetentionModule";
import { SurveyGateButton } from "../../components/organisms/survey/SurveyGateButton";
import { REASONABLE_INTERNAL_USES } from "../../components/vendor/VendorDeletionExceptionSetting";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { useRequestInstructions, useRetentionReasons } from "../../hooks/useOrganizationVendors";
import { GetCompliantFooter } from "./ProgressFooter";

type Props = {
  currentProgress: GetCompliantStep;
};

const isInstructionDone = (
  dataRecipient: OrganizationDataRecipientDto,
  instruction?: RequestHandlingInstructionDto,
): boolean => {
  if (!instruction) {
    return false;
  }

  const needsAffirmation = Boolean(
    instruction?.retentionReasons?.includes(REASONABLE_INTERNAL_USES),
  );
  const hasAffirmation = instruction.exceptionsAffirmed;
  const isRetained = instruction.processingMethod === "RETAIN";
  const hasRetentionReasons = Boolean(
    instruction?.retentionReasons && instruction?.retentionReasons.length > 0,
  );

  const isRetainedFinished =
    isRetained &&
    hasRetentionReasons &&
    ((needsAffirmation && hasAffirmation) || !needsAffirmation);

  const isDeleteFinished = instruction.processingMethod === "DELETE";
  const isDisabled = dataRecipient.isPlatform || dataRecipient.isInstalled;

  return isDeleteFinished || isRetainedFinished || isDisabled;
};

export const DeletionException: React.FC<Props> = ({ currentProgress }) => {
  const organizationId = usePrimaryOrganizationId();

  const [pageDirty, setPageDirty] = useState(false);

  const { answers, setAnswer, surveyRequest } = useSurvey(
    organizationId,
    DATA_RETENTION_SURVEY_NAME,
  );

  const [retentionReasons, retentionReasonsRequest] = useRetentionReasons();

  const [instructions, instructionsRequest, refreshInstructions] = useRequestInstructions(
    organizationId,
    "DELETE",
  );

  const { fetch: updateRemoteInstruction, request: updateRequest } = useActionRequest({
    api: (newInstruction: RequestHandlingInstructionDto) =>
      Api.requestHandlingInstructions.putInstruction(organizationId, newInstruction),
    onSuccess: () => refreshInstructions(),
  });

  const [orgVendors, vendorsRequest] = useOrganizationVendors(organizationId, undefined, true);

  const vendorsNotMarkedInaccessible = useMemo(() => {
    return orgVendors.filter((v) => {
      const instruction = instructions?.find((ins) => ins.vendorId === v.vendorId);

      // Return things that either don't have an instruction, or have
      // an instruction that is _not_ marked as INACCESSIBLE_NOT_STORED
      return (
        !instruction ||
        (instruction && instruction.processingMethod !== "INACCESSIBLE_OR_NOT_STORED")
      );
    });
  }, [orgVendors, instructions]);

  const instructionVendorMap: Record<VendorPublicId, RequestHandlingInstructionDto> = useMemo(
    () => _.keyBy(instructions, "vendorId"),
    [instructions],
  );

  const handleRadioChanged = useCallback(
    (vendor: OrganizationDataRecipientDto, e) => {
      updateRemoteInstruction({
        organizationId: organizationId,
        requestType: "DELETE",
        vendorId: vendor.vendorId,
        processingMethod: e.target.value,
      });

      setPageDirty(true);
    },
    [organizationId, updateRemoteInstruction],
  );

  const handleReasonChanged = useCallback(
    (vendor: OrganizationDataRecipientDto, reasons: RetentionReason[]) => {
      updateRemoteInstruction({
        organizationId: organizationId,
        requestType: "DELETE",
        vendorId: vendor.vendorId,
        processingMethod: "RETAIN",
        retentionReasons: reasons,
      });

      setPageDirty(true);
    },
    [organizationId, updateRemoteInstruction],
  );

  const handleAffirmationChanged = useCallback(
    (instruction: RequestHandlingInstructionDto, affirmed: boolean) => {
      updateRemoteInstruction({
        ...instruction,
        exceptionsAffirmed: affirmed,
      });

      setPageDirty(true);
    },
    [updateRemoteInstruction],
  );

  const allAnswered =
    answers["exceptions-got-it"] === "true" &&
    vendorsNotMarkedInaccessible.every((v) => {
      const inst = instructionVendorMap[v.vendorId];
      return isInstructionDone(v, inst);
    });

  const firstReasonableInternalUseVendor = useMemo(
    () =>
      vendorsNotMarkedInaccessible.find((v) =>
        instructionVendorMap[v.vendorId]?.retentionReasons?.includes("reasonable-internal-uses"),
      ),
    [vendorsNotMarkedInaccessible, instructionVendorMap],
  );

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      requests={[surveyRequest, vendorsRequest, retentionReasonsRequest, instructionsRequest]}
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRetention.ExceptionsToDeletion"
          nextDisabled={!allAnswered}
          nextDisabledTooltip="Please make a selection for each Data Recipient before proceeding."
        />
      }
    >
      <PageHeader titleContent="Exceptions" />

      <div className="w-896">
        <div className="mt-xl">
          <p className="text-t3">
            Your business will be required to delete personal data about consumers when they make a
            Request to Delete. However, not all data is subject to deletion. Privacy laws allow
            businesses to retain a consumer’s personal data for certain approved business purposes,
            known as "exceptions" to deletion.
          </p>

          <p className="text-t3 mb-0">
            In this step, you’ll determine if the any exceptions to deletion apply to any of the
            personal data you store about consumers.
          </p>
        </div>

        <SurveyGateButton
          slug="exceptions-got-it"
          survey={answers}
          updateResponse={setAnswer}
          label="Got It"
        >
          <div className="mt-xl">
            <p className="text-t3 mb-xl">
              If any exceptions apply to the data stored in the vendors/systems below, select the
              applicable exception in the drop-down.
            </p>

            <Callout
              variant={CalloutVariant.Purple}
              title="Exceptions to deletion"
              className="w-640"
            >
              <ul className="ml-sm text-t2">
                <li>Fulfill a transaction or contract</li>
                <li>Reasonable internal uses (excluding marketing, outbound sales)</li>
                <li>Comply with a legal obligation</li>
                <li>Detect or prevent security incidents</li>
                <li>Debug or repair errors</li>
                <li>Exercise free speech or other rights</li>
                <li>Scientific or historical research</li>
              </ul>
            </Callout>

            <div className="mt-xl">
              {vendorsNotMarkedInaccessible.map((v) => {
                const instruction = instructionVendorMap[v.vendorId];
                const isFirstReasonableInternalUseVendor =
                  v.vendorId === firstReasonableInternalUseVendor?.vendorId;

                return (
                  <DeletionExceptionsVendorRow
                    key={v.vendorId}
                    vendor={v}
                    retentionReasons={retentionReasons}
                    instruction={instruction}
                    onRadioChange={handleRadioChanged}
                    onReasonChange={handleReasonChanged}
                    onAffirmationChange={handleAffirmationChanged}
                    showError={pageDirty}
                    disabled={updateRequest.running}
                    showShortAffirmation={
                      Boolean(firstReasonableInternalUseVendor) &&
                      !isFirstReasonableInternalUseVendor
                    }
                  />
                );
              })}
            </div>
          </div>
        </SurveyGateButton>
      </div>
    </GetCompliantPageWrapper>
  );
};
