import { Add } from "@mui/icons-material";
import { Button } from "@mui/material";
import React, { useEffect, useState } from "react";
import { TransitionGroup } from "react-transition-group";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { GetCompliantStep } from "../../../common/service/server/Types";
import { DisableableButton, Loading, TabPanel } from "../../components";
import { InviteUserDialog } from "../../components/dialog";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { Error as RequestsError, Success as RequestsSuccess } from "../../copy/requests";
import { Fmt } from "../../formatters";
import { useOrganizationUsers, usePrimaryOrganizationId, useSelf, useTabs } from "../../hooks";
import { FadeTransition } from "./FadeTransition";

interface SettingsProps {
  currentProgress: GetCompliantStep;
}
export const Settings: React.FC<SettingsProps> = ({ currentProgress }) => {
  const tabOptions = [
    {
      label: "Users",
      value: "users",
    },
  ];
  const [tab, tabs] = useTabs(tabOptions, "users", null, null, "inherit");

  return (
    <GetCompliantPageWrapper currentProgress={currentProgress}>
      <PageHeader titleContent="Settings" />
      <div className="legacy-settings--container">
        <div className="mt-md">{tabs}</div>
        <TabPanel value="users" tab={tab}>
          <UserSettings />
        </TabPanel>
      </div>
    </GetCompliantPageWrapper>
  );
};

const UserSettings: React.FC = () => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);
  const [inviteDialog, setInviteDialog] = useState(false);

  // Requests
  const [self, selfRequest] = useSelf();
  const [users, usersRequest, refreshUsers] = useOrganizationUsers(organizationId);

  const { fetch: deleteUser } = useActionRequest({
    api: ({ email }) => Api.organizationUser.deleteUser(organizationId, email),
    messages: {
      forceError: RequestsError.DeleteUser,
      success: RequestsSuccess.DeleteUser,
    },
    onSuccess: () => refreshUsers(),
  });

  const { fetch: deactivateUser } = useActionRequest({
    api: ({ email }) => Api.organizationUser.deactivateUser(organizationId, email),
    messages: {
      forceError: RequestsError.DeactivateUser,
      success: RequestsSuccess.DeactivateUser,
    },
    onSuccess: () => refreshUsers(),
  });

  // Effects
  useEffect(() => {
    if (usersRequest.success && selfRequest.success) {
      setReadyToRender(true);
    }
  }, [usersRequest, selfRequest]);

  if (!readyToRender) {
    return <Loading />;
  }

  return (
    <>
      <div className="settings--users-add">
        <Button
          className="settings--users-add-btn"
          variant="contained"
          color="primary"
          onClick={() => setInviteDialog(true)}
          startIcon={<Add />}
        >
          Add User
        </Button>
        <InviteUserDialog
          open={inviteDialog}
          organizationId={organizationId}
          onClose={() => setInviteDialog(false)}
          onDone={() => {
            setInviteDialog(false);
            refreshUsers();
          }}
        />
      </div>
      <div className="settings--users-table">
        <div className="settings--users-table-header">
          <span className="settings--users--user-header">User</span>
          <span className="settings--users--last-login-header">Last Login</span>
          <span className="settings--users--actions-header">Actions</span>
        </div>
        <TransitionGroup>
          {users
            .filter((u) => u.status !== "INACTIVE")
            .sort((a, b) => {
              if (a.status === "INVITED" && b.status !== "INVITED") {
                return -1;
              } else if (a.status !== "INVITED" && b.status === "INVITED") {
                return 1;
              } else {
                const aName = `${a.firstName} ${a.lastName} ${a.email}`.trim();
                const bName = `${b.firstName} ${b.lastName} ${b.email}`.trim();
                return aName.localeCompare(bName);
              }
            })
            .map((u) => {
              return (
                <FadeTransition
                  cls="settings--users"
                  in
                  appear
                  unmountOnExit
                  key={`user-${u.email}`}
                >
                  <div className="settings--users--user-row">
                    <div className="settings--users--user">
                      <div className="settings--users--name">
                        {u.firstName} {u.lastName}
                      </div>
                      <div className="settings--users--email">{u.email}</div>
                    </div>
                    <div className="settings--users--last-login">
                      {u.status === "ACTIVE" ? (
                        Fmt.Date.fmtDate(u.lastLogin)
                      ) : (
                        <span className="text-style-italic">Pending</span>
                      )}
                    </div>
                    <div className="settings--users--actions">
                      {u.status === "ACTIVE" && (
                        <DisableableButton
                          variant="text"
                          onClick={
                            u.email !== self.email
                              ? () => deactivateUser({ email: u.email })
                              : undefined
                          }
                          disabled={u.email === self.email}
                          disabledTooltip="You cannot remove yourself"
                        >
                          Remove user
                        </DisableableButton>
                      )}
                      {u.status === "INVITED" && (
                        <Button variant="text" onClick={() => deleteUser({ email: u.email })}>
                          Cancel invite
                        </Button>
                      )}
                    </div>
                  </div>
                </FadeTransition>
              );
            })}
        </TransitionGroup>
      </div>
    </>
  );
};
