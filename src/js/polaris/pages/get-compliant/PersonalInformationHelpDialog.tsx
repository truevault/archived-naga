import React from "react";
import { HelpDialog } from "../../components/dialog";

const personalInformationHelpDialogTitle = "What counts as personal information?";

interface ContentProps {
  isStandalone: boolean;
}
const Content: React.FC<ContentProps> = ({ isStandalone = true }) => {
  return (
    <>
      {!isStandalone && (
        <p className="mt-0 mb-xs">
          A significant part of CCPA compliance involves mapping your personal information
          collection, so it’s important to understand what it is and isn’t.
        </p>
      )}
      <p className="my-0">
        <span className="text-weight-bold">Personal information</span> means any information that
        identifies, relates to, describes, or is associated with a particular person, household, or
        device.
      </p>
      <p className="mb-0 mt-xs">
        Examples include name, email address, purchases, payment information, IP address, email
        opens and click-throughs, and browsing history. It also includes inferences about a person's
        preferences based on other data, like using demographics to suggest products.
      </p>
      <p className="mb-0 mt-xs">
        <span className="text-weight-bold">Personal information collection</span> includes any
        personal information your business accesses about a consumer,{" "}
        <span className="text-style-italic">whether you save it in your systems or not.</span>
      </p>
      <h3 className="mb-0 mt-lg text-t4 text-weight-bold">
        What is <span className="text-style-italic">not</span> personal information under the CCPA?
      </h3>
      <ul className="mb-0 mt-md">
        <li>
          <span className="text-medium">Publicly available data</span>
          <br />
          <span className="text-style-italic">
            Data lawfully made available by the government, the consumer, or a service on behalf of
            the consumer (such as a public social network profile)
          </span>
        </li>
        <li>
          <span className="text-medium">Deidentified data</span>
          <br />
          <span className="text-style-italic">
            Data can’t be linked back to a particular person
          </span>
        </li>
        <li>
          <span className="text-medium">Aggregated data</span>
          <br />
          <span className="text-style-italic">
            Data has been grouped/summarized with individual consumer identities removed
          </span>
        </li>
      </ul>
    </>
  );
};

const PersonalInformationHelpDialogContent = React.memo(Content);

interface PersonalInformationDialogProps {
  open: boolean;
  onClose: () => void;
}
const PersonalInformationDialog: React.FC<PersonalInformationDialogProps> = ({ open, onClose }) => {
  return (
    <HelpDialog open={open} onClose={onClose} title={personalInformationHelpDialogTitle}>
      <PersonalInformationHelpDialogContent isStandalone={true} />
    </HelpDialog>
  );
};

export const PersonalInformationHelpDialog = React.memo(PersonalInformationDialog);
