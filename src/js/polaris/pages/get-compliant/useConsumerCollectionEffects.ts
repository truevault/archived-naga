import { useEffect } from "react";

const useClearSelections = (
  orgId: string,
  answers: any,
  setAnswer: (slug: string, answer: string) => void,
) => {
  useEffect(() => {
    const changes = {};
    if (
      answers["collected-data-from-consumers-undisclosed"] === "false" &&
      answers["collected-data-from-consumers-undisclosed-categories"] !== "[]"
    ) {
      changes["collected-data-from-consumers-undisclosed-categories"] = "[]";
    }
    if (Object.keys(changes).length > 0) {
      for (const slug of Object.keys(changes)) {
        const answer = changes[slug];
        setAnswer(slug, answer);
      }
    }
  }, [orgId, answers, setAnswer]);
};

// Form side-effect responses. This is done here to be in keeping with the general design of the form to have
// these kinds of "automatic classification" actions happen on the front-end. In general, this overall design
// should probably be moved to the back-end, and have the service be responsible for handling these kinds of
// automatic actions based on individual questions.
export const useConsumerCollectionEffects = (
  orgId: string,
  answers: Record<string, string>,
  setAnswer: (slug: string, answer: string, question?: string) => void,
): void => {
  useClearSelections(orgId, answers, setAnswer);
};
