import {
  Cached as CachedIcon,
  Close as CloseIcon,
  Done as DoneIcon,
  KeyboardArrowLeft,
} from "@mui/icons-material";
import { Tooltip } from "@mui/material";
import React, { ReactNode, useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { useActionRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models";
import { NetworkRequestState } from "../../../common/models/NetworkRequest";
import { getCompliantProgressActions } from "../../../common/reducers/getCompliantProgressSlice";
import { Api } from "../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationPublicId } from "../../../common/service/server/Types";
import { InlineLoading } from "../../components";
import { Error as OrganizationError } from "../../copy/organization";
import { usePrimaryOrganizationId } from "../../hooks";
import { StepLink } from "../../util/GetCompliantClassificationOrder";
import {
  GetCompliantStep,
  StepComparison,
  useNextStep,
  usePrevStep,
} from "../../surveys/steps/GetCompliantSteps";
import {
  Survey2023Q1Step,
  useNextStep as useSurvey2023Q1NextStep,
  usePrevStep as useSurvey2023Q1PrevStep,
} from "../../surveys/steps/Survey2023Q1Steps";
import {
  Survey2023Q3Step,
  useNextStep as useSurvey2023Q3NextStep,
  usePrevStep as useSurvey2023Q3PrevStep,
} from "../../surveys/steps/Survey2023Q3Steps";

interface ProgressFooterProps {
  content?: ReactNode | string;
  actions?: ReactNode | string;
}
export const ProgressFooter = ({ content, actions }: ProgressFooterProps) => {
  return (
    <div className="progress-footer">
      {content && <span className="progress-footer-content">{content}</span>}
      {actions && <span className="progress-footer-actions">{actions}</span>}
    </div>
  );
};

type ProgressFooterSaveProps = {
  request?: NetworkRequestState | NetworkRequestState[];
};
export const ProgressFooterSaveNotification: React.FC<ProgressFooterSaveProps> = ({ request }) => {
  if (!request) {
    return null;
  }

  if (!Array.isArray(request)) {
    request = [request];
  }

  const unstarted = NetworkRequest.areUnstarted(...request);
  const anyErrors = NetworkRequest.anyFailed(...request);
  const running = NetworkRequest.anyRunning(...request);

  if (unstarted) {
    return null;
  }

  return (
    <>
      <span className="progress-footer-save-icon">
        {running ? <CachedIcon /> : anyErrors ? <CloseIcon /> : <DoneIcon />}
      </span>
      <span className="progress-footer-save-text">
        {running ? "Saving" : anyErrors ? "Error Saving" : "Saved"}
      </span>
    </>
  );
};

type ProgressFooterBackProps = {
  label?: React.ReactNode;
};

export const ProgressFooterBack: React.FC<ProgressFooterBackProps> = ({ label = "Back" }) => {
  return (
    <span className="flex-center-vertical">
      <KeyboardArrowLeft /> {label}
    </span>
  );
};

interface ProgressFooterActionsProps {
  nextContent?: ReactNode | string;
  nextLoading?: boolean;
  nextOnClick?: () => void;
  nextUrl?: string;
  nextTooltip?: string;
  nextDisabledTooltip?: string;
  nextDisabled?: boolean;
  nextForm?: string;
  nextType?: "button" | "submit" | "reset";
  hideNext?: boolean;
  prevContent?: ReactNode | string;
  prevTooltip?: string;
  prevDisabledTooltip?: string;
  prevOnClick?: () => void;
  prevUrl?: string;
  prevDisabled?: boolean;
  prevLoading?: boolean;
}
export const ProgressFooterActions = ({
  nextContent,
  nextOnClick,
  nextUrl,
  nextTooltip,
  nextDisabled = false,
  nextLoading = false,
  nextDisabledTooltip,
  nextForm,
  nextType,
  prevContent = (
    <span className="flex-center-vertical">
      <KeyboardArrowLeft /> Back
    </span>
  ),
  prevTooltip,
  prevDisabledTooltip,
  prevOnClick,
  prevUrl,
  prevDisabled = false,
  prevLoading = false,
  hideNext = false,
}: ProgressFooterActionsProps) => {
  if (nextDisabled && nextDisabledTooltip) {
    nextTooltip = nextDisabledTooltip;
  }

  if (prevDisabled && prevDisabledTooltip) {
    prevTooltip = prevDisabledTooltip;
  }

  const prevComponent = prevDisabled && prevTooltip ? "div" : undefined;
  const nextComponent = nextDisabled && nextTooltip ? "div" : undefined;

  const hasPrevAction = Boolean(prevOnClick || prevUrl);
  const prevButton = hasPrevAction && prevContent && (
    <LoadingButton
      className="progress-footer-actions--prev-btn mr-md min-width-256"
      color="primary"
      onClick={prevDisabled ? undefined : prevOnClick}
      href={prevUrl}
      disabled={prevDisabled}
      size="large"
      variant="text"
      loading={prevLoading}
      component={prevComponent}
    >
      {prevContent}
    </LoadingButton>
  );

  const hasNextAction = Boolean(nextOnClick || nextUrl || nextType === "submit");
  const nextButton =
    hasNextAction && nextContent && nextComponent ? (
      <LoadingButton
        className="progress-footer-actions--next-btn min-width-256"
        color="primary"
        variant="contained"
        onClick={nextDisabled ? undefined : nextOnClick}
        href={nextUrl}
        disabled={nextDisabled}
        loading={nextLoading}
        size="large"
        component={nextComponent}
      >
        {nextContent}
      </LoadingButton>
    ) : (
      <LoadingButton
        className="progress-footer-actions--next-btn min-width-256"
        color="primary"
        variant="contained"
        onClick={nextDisabled ? undefined : nextOnClick}
        href={nextUrl}
        disabled={nextDisabled}
        loading={nextLoading}
        size="large"
        type={nextType}
        form={nextForm}
      >
        {nextContent}
      </LoadingButton>
    );

  return (
    <>
      {prevButton &&
        (prevTooltip ? (
          <Tooltip title={prevTooltip} placement="top-end">
            <span>{prevButton}</span>
          </Tooltip>
        ) : (
          prevButton
        ))}
      {!hideNext &&
        nextButton &&
        (nextTooltip ? (
          <Tooltip title={nextTooltip} placement="top-end">
            <span>{nextButton}</span>
          </Tooltip>
        ) : (
          nextButton
        ))}
    </>
  );
};

interface GetCompliantFooterProps {
  presentStep: GetCompliantStep;
  nextLabel?: string | ReactNode;

  nextUrl?: string;
  nextOnClick?: () => Promise<void>;
  nextDisabled?: boolean;
  nextDisabledTooltip?: string;
  nextTooltip?: string;

  noProgress?: boolean;
  prevLabel?: string | ReactNode;
  prevUrl?: string;
  prevOnClick?: () => Promise<void>;
  prevSkip?: GetCompliantStep[];
  nextSkip?: GetCompliantStep[];
  content?: ReactNode | string;
  saveRequests?: NetworkRequestState[] | NetworkRequestState;
  recipients?: OrganizationDataRecipientDto[];
}
export const GetCompliantFooter: React.FC<GetCompliantFooterProps> = ({
  presentStep,
  nextLabel = "Next",
  nextDisabled,
  nextDisabledTooltip,
  nextTooltip,
  nextOnClick,
  nextUrl,
  nextSkip,
  noProgress = false,
  prevLabel = (
    <span className="flex-center-vertical">
      <KeyboardArrowLeft /> Back
    </span>
  ),
  prevUrl,
  prevOnClick,
  prevSkip,
  content,
  saveRequests,
  recipients,
}) => {
  if (typeof prevLabel == "string" || prevLabel instanceof String) {
    prevLabel = (
      <span className="flex-center-vertical">
        <KeyboardArrowLeft /> {prevLabel}
      </span>
    );
  }

  const [handleNext, nextRequests, navigatingNext] = useNextStep(
    presentStep,
    nextOnClick,
    nextUrl,
    noProgress,
    nextSkip,
    recipients,
  );

  const [handlePrev, prevRequests, navigatingPrev] = usePrevStep(
    presentStep,
    prevOnClick,
    prevUrl,
    prevSkip,
  );

  const navigating = navigatingNext || navigatingPrev;

  const actions = (
    <ProgressFooterActions
      nextContent={
        <>
          {navigating && (
            <>
              <InlineLoading />{" "}
            </>
          )}
          {nextLabel}
        </>
      }
      nextOnClick={handleNext}
      nextDisabled={nextDisabled || navigating}
      nextDisabledTooltip={nextDisabledTooltip}
      nextTooltip={nextTooltip}
      prevContent={prevLabel}
      prevOnClick={handlePrev}
      prevDisabled={navigating}
      nextLoading={nextRequests.some((r) => r.running)}
      prevLoading={prevRequests.some((r) => r.running)}
    />
  );

  content = content || (saveRequests && <ProgressFooterSaveNotification request={saveRequests} />);

  return <ProgressFooter content={content} actions={actions} />;
};

interface Survey2023Q1FooterProps {
  presentStep: Survey2023Q1Step;
  progressStep: Survey2023Q1Step;
  nextLabel?: string | ReactNode;

  nextOnClick?: () => Promise<void | boolean>;
  nextDisabled?: boolean;
  nextDisabledTooltip?: string;

  noProgress?: boolean;
  prevLabel?: string | ReactNode;
  prevOnClick?: () => Promise<void>;
  noPrev?: boolean;
  content?: ReactNode | string;
  saveRequests?: NetworkRequestState[] | NetworkRequestState;
}
export const Survey2023Q1Footer: React.FC<Survey2023Q1FooterProps> = ({
  presentStep,
  progressStep,
  nextLabel = "Next",
  nextDisabled,
  nextDisabledTooltip,
  nextOnClick,
  prevLabel = (
    <span className="flex-center-vertical">
      <KeyboardArrowLeft /> Back
    </span>
  ),
  prevOnClick,
  noPrev,
  content,
  saveRequests,
}) => {
  if (typeof prevLabel == "string" || prevLabel instanceof String) {
    prevLabel = (
      <span className="flex-center-vertical">
        <KeyboardArrowLeft /> {prevLabel}
      </span>
    );
  }

  const orgId = usePrimaryOrganizationId();

  const [handleNext, navigatingNext] = useSurvey2023Q1NextStep(
    orgId,
    presentStep,
    progressStep,
    nextOnClick,
  );

  const [handlePrev, navigatingPrev] = useSurvey2023Q1PrevStep(orgId, presentStep, prevOnClick);

  const navigating = navigatingNext || navigatingPrev;

  const actions = (
    <ProgressFooterActions
      nextContent={
        <>
          {navigating && (
            <>
              <InlineLoading />{" "}
            </>
          )}
          {nextLabel}
        </>
      }
      nextOnClick={handleNext}
      nextDisabled={nextDisabled || navigating}
      nextDisabledTooltip={nextDisabledTooltip}
      prevContent={noPrev ? undefined : prevLabel}
      prevOnClick={noPrev ? undefined : handlePrev}
      prevDisabled={noPrev ? undefined : navigating}
    />
  );

  content = content || (saveRequests && <ProgressFooterSaveNotification request={saveRequests} />);

  return <ProgressFooter content={content} actions={actions} />;
};

interface Survey2023Q3FooterProps {
  presentStep: Survey2023Q3Step;
  progressStep: Survey2023Q3Step;
  nextLabel?: string | ReactNode;

  hideNext?: boolean;
  nextOnClick?: () => Promise<void | boolean>;
  nextDisabled?: boolean;
  nextDisabledTooltip?: string;

  noProgress?: boolean;
  prevLabel?: string | ReactNode;
  prevOnClick?: () => Promise<void>;
  noPrev?: boolean;
  content?: ReactNode | string;
  saveRequests?: NetworkRequestState[] | NetworkRequestState;
}
export const Survey2023Q3Footer: React.FC<Survey2023Q3FooterProps> = ({
  presentStep,
  progressStep,
  hideNext,
  nextLabel = "Next",
  nextDisabled,
  nextDisabledTooltip,
  nextOnClick,
  prevLabel = (
    <span className="flex-center-vertical">
      <KeyboardArrowLeft /> Back
    </span>
  ),
  prevOnClick,
  noPrev,
  content,
  saveRequests,
}) => {
  if (typeof prevLabel == "string" || prevLabel instanceof String) {
    prevLabel = (
      <span className="flex-center-vertical">
        <KeyboardArrowLeft /> {prevLabel}
      </span>
    );
  }

  const orgId = usePrimaryOrganizationId();

  const [handleNext, navigatingNext] = useSurvey2023Q3NextStep(
    orgId,
    presentStep,
    progressStep,
    nextOnClick,
  );

  const [handlePrev, navigatingPrev] = useSurvey2023Q3PrevStep(orgId, presentStep, prevOnClick);

  const navigating = navigatingNext || navigatingPrev;

  const actions = (
    <ProgressFooterActions
      nextContent={
        <>
          {navigating && (
            <>
              <InlineLoading />{" "}
            </>
          )}
          {nextLabel}
        </>
      }
      nextOnClick={handleNext}
      nextDisabled={nextDisabled || navigating}
      nextDisabledTooltip={nextDisabledTooltip}
      prevContent={noPrev ? undefined : prevLabel}
      prevOnClick={noPrev ? undefined : handlePrev}
      prevDisabled={noPrev ? undefined : navigating}
      hideNext={hideNext}
    />
  );

  content = content || (saveRequests && <ProgressFooterSaveNotification request={saveRequests} />);

  return <ProgressFooter content={content} actions={actions} />;
};

interface ProgressFooterProgressRequestWrapperProps {
  organizationId: OrganizationPublicId;
  contextProgress: GetCompliantStep;
  currentProgress: GetCompliantStep;
  nextProgress?: GetCompliantStep;
  nextLabel?: string | ReactNode;
  nextUrl?: string;
  nextOnClick?: () => void;
  nextDisabled?: boolean;
  nextDisabledTooltip?: string;
  nextTooltip?: string;
  nextOpenConfirmSignal?: () => void;
  nextConfirmedSignal?: boolean;
  nextConfirmCleanup?: () => void;
  prevLabel?: string | ReactNode;
  prevUrl?: string;
  prevOnClick?: () => void;
  content?: ReactNode | string;
  next?: StepLink;
  prev?: StepLink;
}
/**
 * This is DEPRECATED. Use `GetCompliantFooter` instead wherever possible
 * @deprecated Use `GetCompliantFooter` instead wherever possible
 * @param param0
 * @returns
 */
export const ProgressFooterProgressRequestWrapper = ({
  organizationId,
  currentProgress,
  nextProgress,
  nextLabel,
  nextUrl,
  nextDisabled,
  nextDisabledTooltip,
  nextTooltip,
  nextOnClick,
  nextOpenConfirmSignal = null,
  nextConfirmedSignal,
  nextConfirmCleanup,
  next,
  prev,
  prevLabel = (
    <span className="flex-center-vertical">
      <KeyboardArrowLeft /> {prev?.label || "Back"}
    </span>
  ),
  prevUrl,
  prevOnClick,
  content,
}: ProgressFooterProgressRequestWrapperProps) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [updateProgressSignal, setUpdateProgressSignal] = useState(false);

  nextLabel = next?.label || nextLabel;
  nextUrl = next?.url || nextUrl;
  nextProgress = next?.progress || nextProgress;
  prevUrl = prev?.url || prevUrl;

  const handleNextClick =
    nextOnClick ?? nextOpenConfirmSignal
      ? nextOpenConfirmSignal
      : () => setUpdateProgressSignal(true);
  const confirmedSignal = nextOpenConfirmSignal ? nextConfirmedSignal : updateProgressSignal;

  const { fetch: updateProgress, request: updateProgressRequest } = useActionRequest({
    api: () => Api.organization.updateGetCompliantProgress(organizationId, nextProgress),
    messages: {
      forceError: OrganizationError.UpdateGetCompliantProgress,
    },
    onSuccess: () => {
      dispatch(getCompliantProgressActions.set(nextProgress));
      history.push(nextUrl);
    },
  });

  const doUpdateProgress = useCallback(() => {
    if (StepComparison.isAfter(nextProgress, currentProgress)) {
      updateProgress();
    } else {
      history.push(nextUrl);
    }
  }, [currentProgress, history, nextProgress, nextUrl, updateProgress]);

  useEffect(() => {
    if (confirmedSignal) {
      doUpdateProgress();
      nextConfirmCleanup && nextConfirmCleanup();
    }
  }, [nextConfirmCleanup, confirmedSignal, doUpdateProgress]);

  const actions = (
    <ProgressFooterActions
      nextContent={
        updateProgressRequest.running ? (
          <>
            <InlineLoading />
            {next?.label || nextLabel}
          </>
        ) : (
          next?.label || nextLabel
        )
      }
      nextOnClick={handleNextClick}
      nextDisabled={nextDisabled || updateProgressRequest.running}
      nextDisabledTooltip={nextDisabledTooltip}
      nextTooltip={nextTooltip}
      prevContent={prevLabel}
      prevOnClick={prevOnClick ? prevOnClick : undefined}
      prevUrl={prevUrl}
      prevDisabled={updateProgressRequest.running}
    />
  );

  return <ProgressFooter content={content} actions={actions} />;
};
