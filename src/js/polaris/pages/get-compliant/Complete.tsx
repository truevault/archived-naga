import { Button } from "@mui/material";
import React, { useCallback, useEffect } from "react";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { usePrimaryOrganizationId } from "../../hooks";
import { useOrganizationGetCompliantDone } from "../../hooks/useOrganization";

export const Complete: React.FC = () => {
  const orgId = usePrimaryOrganizationId();

  // Requests
  const [isDone] = useOrganizationGetCompliantDone(orgId);
  const apiRequest = useCallback(() => Api.organization.setGetCompliantDone(orgId, true), [orgId]);

  const { fetch: transitionToStayCompliant } = useActionRequest({
    api: apiRequest,
    onSuccess: () => {
      window.location.href = "/requests/active?finished_get_compliant=true";
    },
  });

  const handleClick = () => {
    transitionToStayCompliant();
  };

  // redirect to the home screen if we have finished
  useEffect(() => {
    if (isDone) {
      window.location.href = "/";
    }
  }, [isDone]);

  return (
    <GetCompliantPageWrapper showNavSteps={false} fullWidth={true} disablePadding={true}>
      <div className="get-compliant-complete--header">
        <h1>Congratulations!</h1>
      </div>
      <div className="get-compliant-complete--container">
        <div className="get-compliant-complete--text-container">
          <p className="my-0">Privacy onboarding is DONE!</p>
          <p className="mt-xs">
            Nice work! Click the button below to access your privacy dashboard that gives you access
            to new privacy requests and allows you to view and edit your Data Map and Privacy
            Policy.
          </p>

          <div className="text-center mt-lg">
            <Button variant="contained" color="primary" onClick={handleClick}>
              Go to Privacy Dashboard
            </Button>
          </div>
        </div>
      </div>
    </GetCompliantPageWrapper>
  );
};
