import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { GetCompliantStep } from "../../../common/service/server/Types";
import { Loading } from "../../components";
import { GetCompliantPageWrapper } from "../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { usePrimaryOrganization } from "../../hooks";
import { Routes } from "../../root/routes";
import {
  PrivacyNoticeChecklistItems,
  usePrivacyNoticeChecklistItems,
} from "../stay-compliant/PrivacyNoticeChecklistItems";
import { GetCompliantFooter } from "./ProgressFooter";

const PrivacyNoticeChecklistHeader: React.FC = () => {
  const description = (
    <Paras>
      <Para>
        Complete the steps below to update your business’s Privacy Policy links and post privacy
        disclosures on your business’s website and anywhere else it collects information. If you
        need assistance from your teammates,{" "}
        <Link className="text-medium text-link" to={Routes.getCompliant.settings.user}>
          invite them to Polaris
        </Link>{" "}
        or share the link below. Once these instructions are shared with your team, you can click
        “Done.” You do not have to wait until these steps are completed to proceed.
      </Para>
    </Paras>
  );

  return <PageHeader titleContent="Publish Notices" descriptionContent={description} />;
};

interface PrivacyNoticeChecklistProps {
  currentProgress: GetCompliantStep;
}
export const PrivacyNoticeChecklist: React.FC<PrivacyNoticeChecklistProps> = ({
  currentProgress,
}) => {
  const [org] = usePrimaryOrganization();
  const [readyToRender, setReadyToRender] = useState(false);

  const { checklistProps, ready } = usePrivacyNoticeChecklistItems(org.devInstructionsId);

  useEffect(() => {
    if (ready) {
      setReadyToRender(true);
    }
  }, [ready]);

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper currentProgress={currentProgress}>
        <PageHeader titleContent="Publish Notices" titleHeaderContent="Privacy Center" />
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <GetCompliantFooter presentStep="PublishNotices" nextLabel="Done Publishing Notices" />
      }
    >
      <PrivacyNoticeChecklistHeader />
      <PrivacyNoticeChecklistItems {...checklistProps} />
    </GetCompliantPageWrapper>
  );
};
