import React from "react";
import clsx from "clsx";

export const GetCompliantContainer: React.FC<{
  sidebarContent?: React.ReactNode;
  className?: string;
  wide?: boolean;
  fullWidth?: boolean;
}> = ({ children, sidebarContent, className, wide, fullWidth }) => {
  return (
    <div
      className={clsx("get-compliant-container", className, {
        [`${className}--container`]: !!className,
        "get-compliant-container--wide": wide,
        "get-compliant-container--full-width": fullWidth,
      })}
    >
      <div
        className={clsx("get-compliant-container--content", {
          [`${className}--content`]: !!className,
        })}
      >
        {children}
      </div>

      {sidebarContent && (
        <div
          className={clsx("get-compliant-container--sidebar", {
            [`${className}--sidebar`]: !!className,
          })}
        >
          {sidebarContent}
        </div>
      )}
    </div>
  );
};
