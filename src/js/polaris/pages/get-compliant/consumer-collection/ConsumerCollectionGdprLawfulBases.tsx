import { Button } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { useActionRequest } from "../../../../common/hooks/api";
import { SurveyResponses, useSurvey } from "../../../../common/hooks/useSurvey";
import { NetworkRequest } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import {
  DefProcessingActivityDto,
  LawfulBasis,
  ProcessingActivityDto,
} from "../../../../common/service/server/dto/ProcessingActivityDto";
import { GetCompliantStep } from "../../../../common/service/server/Types";
import { Loading } from "../../../components";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { GDPRProcessingActivitiesDialog } from "../../../components/dialog/GDPRProcessingActivitiesDialog";
import { QuestionLabel } from "../../../components/get-compliant/survey/QuestionContainer";
import { SurveyBooleanQuestion } from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { ProcessingActivitySettingsTable } from "../../../components/organisms/ProcessingActivitySettingsTable";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import {
  useDefaultActivityCheckboxes,
  useProcessingActivites,
} from "../../../hooks/useProcessingActivities";
import { Routes } from "../../../root/routes";

import { GetCompliantFooter } from "../ProgressFooter";

export const GDPR_LAWFUL_BASES_SURVEY = "gdpr-lawful-bases-survey";

interface ConsumerCollectionIncentivesProps {
  currentProgress: GetCompliantStep;
}
export const ConsumerCollectionGdprLawfulBases: React.FC<ConsumerCollectionIncentivesProps> = ({
  currentProgress,
}) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  const [readyToRender, setReadyToRender] = useState(false);

  const [allActivities, activityRequest, refreshActivities] =
    useProcessingActivites(organizationId);
  const defCheckboxes = useDefaultActivityCheckboxes(organizationId, allActivities, "EEA_UK", [
    "EEA_UK",
  ]);
  const activities = useMemo(
    () => allActivities?.filter((a) => a?.regions?.includes("EEA_UK")) ?? [],
    [allActivities],
  );
  const { answers, setAnswer, surveyRequest } = useSurvey(
    organizationId,
    GDPR_LAWFUL_BASES_SURVEY,
    () => refreshActivities(),
  );

  const [paDialog, setPADialog] = useState(false);
  const openPADialog = () => setPADialog(true);
  const closePADialog = () => setPADialog(false);

  // Effects
  useEffect(() => {
    if (
      NetworkRequest.areFinished(
        surveyRequest,
        defCheckboxes.requests.defaultActivities,
        activityRequest,
      )
    ) {
      setReadyToRender(true);
    }
  }, [surveyRequest, defCheckboxes.requests.defaultActivities, activityRequest]);

  const extendedOptions = answers[EXTRA_LAWFUL_BASES_SLUG] === "true";

  const { fetch: updateLawfulBasis } = useActionRequest({
    api: ({ activityId, bases }: { activityId: string; bases: LawfulBasis[] }) =>
      Api.processingActivities.patchUpdateProcessingActivity(organizationId, activityId, {
        name: undefined,
        lawfulBases: bases,
        regions: undefined,
      }),
    onSuccess: () => refreshActivities(),
  });

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper
        currentProgress={currentProgress}
        progressFooter={
          <GetCompliantFooter
            presentStep="ConsumerCollection.LawfulBases"
            nextDisabled={true}
            nextDisabledTooltip="Please select at least one purpose for each consumer group before continuing"
          />
        }
      >
        <PageHeader titleContent="Lawful Bases" />
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  const finishedProcessingActivities = activities?.every((a) => a.lawfulBases.length > 0);
  const hasClickedDone = answers["lawful-basis-processing-activities-done"];

  const isDone = finishedProcessingActivities && hasClickedDone;

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <GetCompliantFooter
          presentStep="ConsumerCollection.LawfulBases"
          nextLabel="Done With Lawful Bases"
          nextDisabled={!isDone}
        />
      }
    >
      <PageHeader titleContent="Lawful Bases" />

      <QuestionLabel label="The GDPR allows businesses to collect and process data only for legitimate, lawful purposes. In this step, we'll walk through the lawful bases in the GDPR and determine which ones apply to your processing activities." />
      <SurveyGateButton slug="lawful-bases-get-started" survey={answers} updateResponse={setAnswer}>
        <div>
          <p className="mt-lg text-weight-medium">
            Listed below are the core lawful reasons for processing data. These apply to just about
            every business, so we’ll include them as lawful bases for your business.
          </p>

          <p className="text-color-primary mb-0 text-weight-medium">
            Complying with legal obligations
          </p>
          <p className="mt-0">
            Data is processed in order to meet legal or regulatory obligations. For example,
            complying with tax or privacy laws. You use TrueVault Polaris, so this one is easy!
          </p>

          <p className="text-color-primary mb-0 text-weight-medium">Fulfilling contracts</p>
          <p className="mt-0">
            Data is processed to honor a promise or commitment to the individual whose data you are
            processing. For example, fulfilling customer orders.
          </p>

          <p className="text-color-primary mb-0 text-weight-medium">Legitimate interests</p>
          <p className="mt-0">
            Data is processed for a business operational purpose, such as for sales, marketing, data
            storage, customer support, website management, or improving products/services, among
            others.
          </p>

          <p className="text-color-primary mb-0 text-weight-medium">Consent</p>
          <p className="mt-0">
            Consent is the lawful basis for certain processing activities, like data collection via
            website cookies and a few other less common processing activities.
          </p>
        </div>

        <SurveyGateButton
          slug="lawful-basis-reviewed-main-lawful-bases"
          survey={answers}
          updateResponse={setAnswer}
          label="Got it"
        >
          <ExtraLawfulBasisQuestion answers={answers} setAnswer={setAnswer} />

          {Boolean(answers[EXTRA_LAWFUL_BASES_SLUG]) && (
            <>
              <div className="mt-lg">
                <QuestionLabel
                  label={"Let's define your processing activities in a little more detail."}
                />
              </div>
              <AutomatedDecisionMakingQuestion answers={answers} setAnswer={setAnswer}>
                <OutboundSalesQuestion answers={answers} setAnswer={setAnswer}>
                  <div className="my-lg">
                    <QuestionLabel
                      label={
                        <>
                          <p className="text-weight-regular">
                            Based on your answers in{" "}
                            <a href={Routes.getCompliant.dataCollection.consumerPurposes}>
                              Collection Purposes
                            </a>
                            , these are the data processing activities your company is currently
                            engaged in.
                          </p>
                          <p className="text-weight-regular">
                            Use the table below to assign at least one lawful basis to each of your
                            processing activities. We’ve pre-filled Lawful Bases for your Processing
                            Activities according to our research. If you added any additional
                            Processing Activities, please select a Lawful Basis below. Feel free to
                            edit the Lawful Bases for the pre-selected Processing Activities if you
                            need to.
                          </p>
                        </>
                      }
                    />
                    <ProcessingActivitySettingsTable
                      activities={activities}
                      region="EEA_UK"
                      onChange={updateLawfulBasis}
                      showExtendedOptions={extendedOptions}
                      disableRemovalForMandatory
                      onRemove={async () => await refreshActivities()}
                    />
                  </div>
                  <div className="my-lg">
                    <Button variant="contained" className="my-xs" onClick={openPADialog}>
                      Edit GDPR Data Processing Activities
                    </Button>
                  </div>

                  <SurveyGateButton
                    slug="lawful-basis-processing-activities-done"
                    survey={answers}
                    updateResponse={setAnswer}
                    label="Done"
                  />
                </OutboundSalesQuestion>
              </AutomatedDecisionMakingQuestion>
            </>
          )}
        </SurveyGateButton>
      </SurveyGateButton>

      <GDPRProcessingActivitiesDialog open={paDialog} onClose={closePADialog} />
    </GetCompliantPageWrapper>
  );
};

export type CustomActivityCheckboxesProps = {
  customActivities: ProcessingActivityDto[];
  checked: string[];
  setChecked: (defaultId: string, checked: boolean) => void;
};

export type DefaultActivityCheckboxesProps = {
  defaults: DefProcessingActivityDto[];
  checked: string[];
  setChecked: (defaultId: string, checked: boolean) => void;
};

export type UseCustomActivityInputs = {
  applyChanges: () => Promise<void>;
  clearActivities: () => void;
  props: CustomActivityInputsProps;
};

export const useCustomActivityInputs = (
  orgId: string,
  activities: ProcessingActivityDto[],
): UseCustomActivityInputs => {
  const [localActivities, setLocalActivities] = useState<CustomActivity[]>([]);

  // Extract and memoize custom activities from remote
  const remoteActivities = useMemo(() => {
    return (activities || [])
      .filter((activity) => !activity.defaultId && !activity.autocreationSlug)
      .map((activity) => ({
        activityId: activity.id,
        name: activity.name,
      }));
  }, [activities]);

  // Set our initial local activities whenever we get updates from remote
  useEffect(() => {
    const activities = remoteActivities?.length == 0 ? [{ name: "" }] : [...remoteActivities];
    setLocalActivities(activities);
  }, [remoteActivities]);

  const handleAdd = () => {
    const updated = [...localActivities];
    updated.push({ name: "" });
    setLocalActivities(updated);
  };

  const handleRemove = (idx: number) => {
    if (idx == 0) {
      return;
    }
    const updated = [...localActivities];
    updated.splice(idx, 1);
    setLocalActivities(updated);
  };

  const handleUpdate = (idx: number, val: string) => {
    const current = localActivities[idx];
    const updated = [...localActivities];
    updated.splice(idx, 1, { ...current, name: val });
    setLocalActivities(updated);
  };

  const applyChanges = async () => {
    const created = localActivities.filter((a) => !a.activityId && a.name);
    const updated = localActivities.filter((a) => {
      const custom = remoteActivities.find((ca) => a.activityId && ca.activityId == a.activityId);
      return Boolean(custom);
    });
    const activityIds = localActivities.map((a) => a.activityId);
    const removed = remoteActivities.filter((ca) => !activityIds.includes(ca.activityId));

    const createPr = created.map((a) =>
      Api.processingActivities.createProcessingActivity(orgId, {
        name: a.name,
        defaultId: undefined,
        lawfulBases: undefined,
        regions: ["EEA_UK"],
      }),
    );
    const updatePr = updated.map((a) =>
      Api.processingActivities.patchUpdateProcessingActivity(orgId, a.activityId, {
        name: a.name,
        lawfulBases: undefined,
        regions: undefined,
      }),
    );
    const removePr = removed.map((a) =>
      Api.processingActivities.deleteProcessingActivityRegion(orgId, a.activityId, ["EEA_UK"]),
    );

    const all: Promise<any>[] = [...createPr, ...updatePr, ...removePr];

    await Promise.all(all);
  };

  const clearActivities = () => {
    setLocalActivities([]);
  };

  return {
    applyChanges,
    clearActivities,
    props: {
      activities: localActivities,
      onAdd: handleAdd,
      onRemove: handleRemove,
      onUpdate: handleUpdate,
    },
  };
};

type CustomActivityInputsProps = {
  activities: CustomActivity[];
  onAdd: () => void;
  onRemove: (idx: number) => void;
  onUpdate: (idx: number, val: string) => void;
};

type CustomActivity = {
  name: string;
  activityId?: string;
};

type QuestionProps = {
  answers: SurveyResponses;
  setAnswer: (slug: string, answer: string, question?: string) => Promise<void>;
};
const AUTOMATED_DECISION_MAKING_SLUG = "automated-decision-making";
const AutomatedDecisionMakingQuestion: React.FC<QuestionProps> = ({
  answers,
  setAnswer,
  children,
}) => {
  const slug = AUTOMATED_DECISION_MAKING_SLUG;
  return (
    <>
      <SurveyBooleanQuestion
        answer={answers[slug]}
        onAnswer={(ans) => setAnswer(slug, ans)}
        question={{
          type: "boolean",
          slug: slug,
          question:
            "Does your business engage in automated decision making (including individual profiling) which produces legal or similarly significant effects for EEA/UK residents?",
          helpText:
            "“Similarly significant effects” include automated decisions by your business to provide or deny a person money or credit, housing, insurance, education enrollment, criminal justice, employment opportunities, health care services, or access to basic necessities, such as food and water.",
          visible: true,
        }}
      />
      {Boolean(answers[slug]) && children}
    </>
  );
};

const OUTBOUND_SALES_SLUG = "outbound-sales-eea-uk";
const OutboundSalesQuestion: React.FC<QuestionProps> = ({ answers, setAnswer, children }) => {
  const slug = OUTBOUND_SALES_SLUG;
  return (
    <>
      <SurveyBooleanQuestion
        answer={answers[slug]}
        onAnswer={(ans) => setAnswer(slug, ans)}
        question={{
          type: "boolean",
          slug: slug,
          question:
            "Do you send outbound sales emails or phone calls to people in the EEA/UK that you don’t have a preexisting relationship with?",
          helpText:
            "Answer “Yes” if you send emails (or make telephone calls) to people who are not existing customers, or who have not provided their contact information directly to you for the purpose of sales/marketing.",
          visible: true,
        }}
      />
      {Boolean(answers[slug]) && children}
    </>
  );
};

type ExtraLawfulBasisQuestionProps = {
  answers: SurveyResponses;
  setAnswer: (slug: string, answer: string, question?: string) => Promise<void>;
};
const EXTRA_LAWFUL_BASES_SLUG = "extra-lawful-bases";
const ExtraLawfulBasisQuestion: React.FC<ExtraLawfulBasisQuestionProps> = ({
  answers,
  setAnswer,
}) => {
  return (
    <Callout variant={CalloutVariant.LightPurple}>
      <SurveyBooleanQuestion
        answer={answers[EXTRA_LAWFUL_BASES_SLUG]}
        onAnswer={(ans) => setAnswer(EXTRA_LAWFUL_BASES_SLUG, ans)}
        question={{
          type: "boolean",
          slug: EXTRA_LAWFUL_BASES_SLUG,
          yesLabel: "These may apply",
          noLabel: "None of these apply",
          noFirst: true,
          question: (
            <div className="text-weight-regular">
              <p>
                Here are two additional lawful bases in the GDPR that very rarely apply to
                for-profit businesses. Let us know if you think they do apply to your business.
              </p>
              <ul>
                <li>
                  <strong>Public interest</strong>: Data processing is necessary for the performance
                  of a task carried out in the public interest or in the exercise of official
                  authority vested in your business. This only applies to private organizations in
                  rare circumstances, such as a utility company providing services to the public.
                </li>
                <li>
                  <strong>Vital interest of the individual</strong>: Data processing is necessary in
                  order to protect information essential for someone's life, or in a situation
                  involving public health or safety.
                </li>
              </ul>
            </div>
          ),
          visible: true,
        }}
      />
    </Callout>
  );
};
