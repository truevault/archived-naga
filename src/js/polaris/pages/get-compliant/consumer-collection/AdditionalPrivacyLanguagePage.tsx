import { LoadingButton } from "@mui/lab";
import { Dialog, DialogContent } from "@mui/material";
import React, { useState } from "react";
import { CloseableDialogTitle } from "../../../components/dialog/CloseableDialogTitle";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import {
  PrivacyPolicySettings,
  usePrivacyPolicySettings,
} from "../../../components/superorg/settings/PrivacyPolicySettings";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { usePrivacyNotice } from "../../../hooks/usePrivacyCenter";
import { GetCompliantFooter } from "../ProgressFooter";

const PreviewPrivacyNoticeDialog: React.FC<{ privacyPolicyHtml: string }> = ({
  privacyPolicyHtml,
}) => {
  const [open, setOpen] = useState(false);
  const organizationId = usePrimaryOrganizationId();
  const [noticeDto, noticeRequest] = usePrivacyNotice(organizationId);

  return (
    <>
      <LoadingButton
        variant="contained"
        loading={noticeRequest.running}
        onClick={() => setOpen(!open)}
      >
        Preview Privacy Policy
      </LoadingButton>

      <Dialog fullWidth maxWidth="lg" open={open}>
        <CloseableDialogTitle onClose={() => setOpen(false)}>
          Privacy Policy Preview
        </CloseableDialogTitle>

        <DialogContent>
          {privacyPolicyHtml && <div dangerouslySetInnerHTML={{ __html: privacyPolicyHtml }} />}
          {noticeDto && <div dangerouslySetInnerHTML={{ __html: noticeDto.notice }} />}
        </DialogContent>
      </Dialog>
    </>
  );
};

export const AdditionalPrivacyLanguagePage: React.FC = () => {
  const { settings: ppSetting, privacyCentersRequest } = usePrivacyPolicySettings();

  return (
    <GetCompliantPageWrapper
      progressFooter={
        <GetCompliantFooter
          nextLabel="Done with Privacy Policy"
          presentStep="ConsumerCollection.AdditionalPrivacyLanguage"
        />
      }
      requests={[privacyCentersRequest]}
    >
      <PageHeader
        titleContent="Customization"
        descriptionContent={
          <Paras>
            <Para>
              Many businesses have an existing privacy policy and want to include their introductory
              paragraph or other content at the top of their Privacy Notice. If you have something
              like that, you can copy and paste your existing language in the text field below. You
              can add, edit, or remove it at any time. If you do not want to add anything, you can
              leave the text area blank and click <strong>“Next”</strong> to continue.
            </Para>
          </Paras>
        }
      />

      <div className="my-lg">
        <PrivacyPolicySettings {...ppSetting} showHelp={false} />
        <div className="my-md d-flex flex-row">
          <div className="ml-auto">
            <PreviewPrivacyNoticeDialog privacyPolicyHtml={ppSetting.privacyPolicyHtml} />
          </div>
        </div>
      </div>
    </GetCompliantPageWrapper>
  );
};
