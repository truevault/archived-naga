import React, { useEffect, useState } from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { GetCompliantStep } from "../../../../common/service/server/Types";
import { Loading } from "../../../components";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { useDataSubjectTypes, usePrimaryOrganizationId } from "../../../hooks";
import { consumerGroupSorter, isEmploymentGroup } from "../../../util/consumerGroups";
import { GetCompliantFooter } from "../ProgressFooter";
import { CollectionPurposes } from "./CollectionPurposes";

const Header: React.FC = () => {
  return (
    <PageHeader
      titleContent="Purposes"
      descriptionContent={
        <>
          <p>
            In this step, you’ll identify the purposes for which you collect and process personal
            data. Select from the list of purposes below and then select the personal data used for
            each purpose.
          </p>
          <p>
            It’s okay if the categories you select aren’t a perfect fit as long as they provide a
            general idea to consumers about why you are using their data. If you’re torn between two
            categories, go with the one that is more specific.
          </p>
          <Callout variant={CalloutVariant.Purple}>
            💡 Tip: Include any data processing performed by your Data Recipients on your business’s
            behalf. For example, if you have a vendor that provides data storage, select Data
            Storage below.
          </Callout>
        </>
      }
    />
  );
};

interface ConsumerCollectionPurposesProps {
  currentProgress: GetCompliantStep;
}
export const ConsumerCollectionPurposes: React.FC<ConsumerCollectionPurposesProps> = ({
  currentProgress,
}) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);
  const [nextDisabled, setNextDisabled] = useState(false);
  const [consumerGroups, setConsumerGroups] = useState<CollectionGroupDetailsDto[]>(null);

  // Requests
  const [fetchedConsumerGroups, consumerGroupsRequest] = useDataSubjectTypes(organizationId);

  // Effects
  useEffect(() => {
    if (consumerGroupsRequest.success) {
      setConsumerGroups(
        fetchedConsumerGroups.filter((cg) => !isEmploymentGroup(cg)).sort(consumerGroupSorter),
      );
      setReadyToRender(true);
    }
  }, [fetchedConsumerGroups, consumerGroupsRequest]);

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper currentProgress={currentProgress}>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <GetCompliantFooter
          presentStep="ConsumerCollection.CollectionPurposes"
          nextDisabled={nextDisabled}
          nextLabel="Done with Review"
          nextDisabledTooltip="Please select at least one purpose for each consumer group before continuing"
        />
      }
    >
      <Header />
      <CollectionPurposes
        picWarning="bottom"
        consumerGroups={consumerGroups}
        setComplete={(complete) => setNextDisabled(!complete)}
      />
    </GetCompliantPageWrapper>
  );
};
