import React, { useEffect, useState } from "react";
import { useActionRequest } from "../../../../common/hooks/api";
import { Api } from "../../../../common/service/Api";
import { UpdateOrganizationPrivacyNoticeProgressDto } from "../../../../common/service/server/controller/OrganizationController";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { GetCompliantStep } from "../../../../common/service/server/Types";
import { Loading } from "../../../components";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../../hooks";
import { usePrivacyNotice } from "../../../hooks/usePrivacyCenter";
import { GetCompliantFooter } from "../ProgressFooter";

const ConsumerCollectionNoticeHeader: React.FC<{
  title: string;
}> = ({ title }) => (
  <PageHeader
    titleContent={title}
    descriptionContent={
      <div className="text-t3 mt-xl">
        <p>
          Review the Privacy Notice we generated for your business below. We’ll help you post your
          Privacy Notice on your website in a later step. If the information in the notice is
          incomplete or inaccurate, go back and review your Data Map.
        </p>
        <Callout variant={CalloutVariant.LightPurple}>
          <p className="text-t3">
            The Privacy Notice is a statement to consumers that (1) describes the personal
            information your business collects, why it’s collected, and how it’s used, and (2)
            informs consumers about their privacy rights and how to exercise them.
          </p>
        </Callout>
      </div>
    }
  />
);

const PureConsumerCollectionNoticeHeader = React.memo(ConsumerCollectionNoticeHeader);

interface ConsumerCollectionNoticeReviewFooterProps {
  currentProgress: GetCompliantStep;
  organization: OrganizationDto;
  refreshOrganization: () => void;
}
const ConsumerCollectionNoticeReviewFooter: React.FC<ConsumerCollectionNoticeReviewFooterProps> = ({
  organization,
}) => {
  // Variables
  const currentOrgProgress: UpdateOrganizationPrivacyNoticeProgressDto = {
    caPrivacyNoticeProgress: organization.caPrivacyNoticeProgress.value,
    optOutPrivacyNoticeProgress: organization.optOutPrivacyNoticeProgress.value,
  };

  const newOrgProgress = Object.assign({}, currentOrgProgress, {
    caPrivacyNoticeProgress: "APPROVED",
  });

  const { fetch: updateOrganization } = useActionRequest({
    api: () => Api.organization.updatePrivacyNoticeProgress(organization.id, newOrgProgress),
  });

  const handleNext = async () => {
    await updateOrganization();
  };

  return (
    <GetCompliantFooter
      presentStep="ConsumerCollection.ReviewPrivacyNotice"
      nextOnClick={handleNext}
    />
  );
};

const PureConsumerCollectionNoticeReviewFooter = React.memo(ConsumerCollectionNoticeReviewFooter);

interface ConsumerCollectionNoticeReviewProps {
  currentProgress: GetCompliantStep;
}
export const ConsumerCollectionNoticeReview: React.FC<ConsumerCollectionNoticeReviewProps> = ({
  currentProgress,
}) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  // Requests
  const [organization, organizationRequest, refreshOrganization] = usePrimaryOrganization();

  const [noticeDto, noticeRequest] = usePrivacyNotice(organizationId);

  useEffect(() => {
    if (organizationRequest.success && noticeRequest.success) {
      setReadyToRender(true);
    }
  }, [organizationRequest, noticeRequest]);

  if (!readyToRender) {
    return <Loading />;
  }

  const header = <PureConsumerCollectionNoticeHeader title="Preview Notice" />;

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <PureConsumerCollectionNoticeReviewFooter
          currentProgress={currentProgress}
          organization={organization}
          refreshOrganization={refreshOrganization}
        />
      }
    >
      {header}
      <div className={"privacy-notices--review-container"}>
        <div dangerouslySetInnerHTML={{ __html: noticeDto.notice }} />
      </div>
    </GetCompliantPageWrapper>
  );
};
