import _ from "lodash";
import React, { useMemo, useState } from "react";
import { useParams } from "react-router";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { CollectionDto } from "../../../../common/service/server/dto/DataMapDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { IncludeInformationCollectedForOtherBusinessesCallout } from "../../../components/organisms/callouts/RegulationCallouts";
import { ConsumerDataCollectionForm } from "../../../components/organisms/collection-groups/ConsumerDataCollectionForm";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../../hooks";
import {
  useCollectionGroup,
  useNonEmploymentCollectionGroups,
} from "../../../hooks/useCollectionGroups";
import { useDataMap } from "../../../hooks/useDataMap";
import { useProgressKeys } from "../../../hooks/useProgressKeys";
import { useGetCompliantContext } from "../../../root/GetCompliantContext";
import { Routes } from "../../../root/routes";
import {
  consumerCollectionSurvey,
  consumerCollectionSurveyName,
  SENSITIVE_INFERENCES_QUESTION_SLUG,
} from "../../../surveys/consumerCollectionCategoriesSurvey";
import { isFinished } from "../../../surveys/survey";

import {
  BUSINESS_SURVEY_NAME,
  COLLECTION_FOR_OTHER_BUSINESSES_SLUG,
} from "../../../surveys/businessSurvey";
import { GetCompliantFooter } from "../ProgressFooter";

export const ConsumerDataCollectionForGroup: React.FC = () => {
  const { progress } = useGetCompliantContext();

  // Variables
  const { groupId } = useParams<{ groupId: UUIDString }>();
  const [org] = usePrimaryOrganization();
  const orgId = usePrimaryOrganizationId();

  const [groups, groupsRequest] = useNonEmploymentCollectionGroups(orgId);
  const [dataMap, dataMapRequest, refreshDataMap] = useDataMap(orgId);
  const [group, groupRequest] = useCollectionGroup(orgId, groupId);

  // This is not great, noah told me to
  const [sensitiveInferencesQuestionIsShown, setSensitiveInferencesQuestionIsShown] =
    useState<Boolean>(false);

  const { answers: businessSurvey, surveyRequest: bizSurveyRequest } = useSurvey(
    orgId,
    BUSINESS_SURVEY_NAME,
  );

  const isOnlyGroup = !_.isNil(groups) && groups?.length < 2;

  const { answers, setAnswer, surveyRequest, updateRequest, refreshSurvey } = useSurvey(
    orgId,
    group ? consumerCollectionSurveyName(group.id) : undefined,
  );

  const collected: CollectionDto = dataMap?.collection?.find(
    (c) => c.collectionGroupId == group?.id,
  ) || {
    collected: [],
    collectionGroupId: group?.id,
    collectionGroupName: group?.name,
    collectionGroupType: group?.collectionGroupType,
  };

  const keys = useMemo(() => groups?.map((g) => consumerCollectionSurveyName(g.id)), [groups]);
  const { update: updateProgress } = useProgressKeys(orgId, keys);

  const handleNext = async () => {
    const key = consumerCollectionSurveyName(group?.id);
    await updateProgress({ keyId: key, progress: "DONE" });
  };

  const handleRefresh = async () => {
    await Promise.all([refreshSurvey(), refreshDataMap()]);
  };

  const nextUrl = isOnlyGroup
    ? undefined
    : Routes.getCompliant.businessSurvey.consumerDataCollection;
  const prevUrl = isOnlyGroup
    ? undefined
    : Routes.getCompliant.businessSurvey.consumerDataCollection;

  const nextLabel = "Next";
  const noProgress = isOnlyGroup ? false : true;

  const survey = consumerCollectionSurvey(answers, dataMap, collected);
  const excludedQuestions = sensitiveInferencesQuestionIsShown
    ? []
    : [SENSITIVE_INFERENCES_QUESTION_SLUG];
  const hasFinishedSurvey =
    isFinished(survey, answers, excludedQuestions) && Boolean(answers["inferences-done"]);

  return (
    <GetCompliantPageWrapper
      requests={[groupsRequest, surveyRequest, dataMapRequest, groupRequest, bizSurveyRequest]}
      currentProgress={progress}
      progressFooter={
        <GetCompliantFooter
          presentStep="BusinessSurvey.DataCollection"
          nextLabel={nextLabel}
          nextUrl={nextUrl}
          saveRequests={[updateRequest]}
          nextOnClick={handleNext}
          nextDisabled={!hasFinishedSurvey}
          prevUrl={prevUrl}
          noProgress={noProgress}
        />
      }
    >
      <PageHeader titleContent="Collection" />

      <h4 className="intermediate">
        What information do you collect directly from {group?.name ?? "this group"}?
      </h4>

      {businessSurvey[COLLECTION_FOR_OTHER_BUSINESSES_SLUG] == "true" && (
        <div className="my-md">
          <IncludeInformationCollectedForOtherBusinessesCallout />
        </div>
      )}

      <ConsumerDataCollectionForm
        answers={answers}
        setAnswer={setAnswer}
        dataMap={dataMap}
        collected={collected}
        survey={survey}
        org={org}
        collectionGroup={group}
        onCollectionChange={handleRefresh}
        setSensitiveInferencesQuestionIsShown={setSensitiveInferencesQuestionIsShown}
      />
    </GetCompliantPageWrapper>
  );
};
