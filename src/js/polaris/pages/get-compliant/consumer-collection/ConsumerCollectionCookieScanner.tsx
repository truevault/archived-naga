import React from "react";
import { GetCompliantStep } from "../../../../common/service/server/Types";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import {
  CookieScanner,
  useCookieScanner,
} from "../../../components/organisms/cookies/CookieScanner";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { GetCompliantFooter } from "../ProgressFooter";

type DataRetentionCookieScannerProps = {
  currentProgress: GetCompliantStep;
};

export const DataRetentionCookieScanner: React.FC<DataRetentionCookieScannerProps> = ({
  currentProgress,
}) => {
  const scanner = useCookieScanner();

  const instructions = (
    <Paras>
      <Para>
        European e-privacy laws require consent for all website cookies beyond those that are
        strictly necessary for the operation of your website. Consent is typically collected via an
        online consent pop-up. On this screen you’ll identify and categorize your cookies. We’ll
        help you integrate our consent management tool in a later step.
      </Para>
    </Paras>
  );

  return (
    <GetCompliantPageWrapper
      requests={[
        scanner.requests.cookieScanRequest,
        scanner.requests.cookieRecommendationsRequest,
        scanner.requests.organizationDataRecipientsRequest,
        scanner.requests.vendorCatalogRequest,
      ]}
      currentProgress={currentProgress}
      progressFooter={
        <GetCompliantFooter
          presentStep="DataRetention.CookieScan"
          nextDisabled={!scanner.isFinished}
          nextDisabledTooltip="Please make selections for each cookie before proceeding."
        />
      }
    >
      <PageHeader titleContent="Cookie Scanner" titleHeaderContent="GDPR Compliance" />

      <CookieScanner {...scanner.props} instructions={instructions} />
    </GetCompliantPageWrapper>
  );
};
