import React, { useCallback, useEffect, useMemo } from "react";
import { useHistory } from "react-router-dom";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { ActionRow } from "../../../components/ActionRow";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { useNonEmploymentCollectionGroups } from "../../../hooks/useCollectionGroups";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { useProgressKeys } from "../../../hooks/useProgressKeys";
import { useGetCompliantContext } from "../../../root/GetCompliantContext";
import { RouteHelpers } from "../../../root/routes";
import { GetCompliantFooter } from "../ProgressFooter";
import { ProgressSpan } from "../ProgressSpan";

export const ConsumerDataCollection: React.FC = () => {
  const { progress } = useGetCompliantContext();
  // Variables
  const orgId = usePrimaryOrganizationId();
  const history = useHistory();

  const [groups, groupsRequest] = useNonEmploymentCollectionGroups(orgId);

  const keys = useMemo(() => groups?.map((g) => `consumer-data-collection-${g.id}`), [groups]);

  const {
    progress: progressValues,
    request: progressRequest,
    update: updateProgress,
  } = useProgressKeys(orgId, keys);

  const gotoGroupPage = useCallback(
    async (group: CollectionGroupDetailsDto) => {
      const key = `consumer-data-collection-${group.id}`;
      const prev = progressValues?.find((it) => it.key == key);

      const saveStatus = prev?.progress?.value === "DONE" ? "DONE" : "IN_PROGRESS";
      await updateProgress({ keyId: key, progress: saveStatus });

      history.push(
        RouteHelpers.getCompliant.businessSurvey.consumerDataCollectionForGroup(group.id),
      );
    },
    [progressValues, updateProgress, history],
  );

  // Skip to the specific group page if there's only 1
  useEffect(() => {
    if (groups?.length == 1) {
      gotoGroupPage(groups[0]);
    }
  }, [groups, gotoGroupPage]);

  const isFinished = groups?.every((g) => {
    const key = `consumer-data-collection-${g.id}`;
    const progress = progressValues?.find((it) => it.key == key);
    return progress?.progress?.value == "DONE";
  });

  return (
    <GetCompliantPageWrapper
      currentProgress={progress}
      requests={[groupsRequest, progressRequest]}
      progressFooter={
        <GetCompliantFooter
          presentStep="BusinessSurvey.DataCollection"
          nextDisabled={!isFinished}
          nextLabel="Next"
        />
      }
    >
      <PageHeader titleContent="Collection" />

      <div className="my-xl text-t3">
        Map the data that your business collects for each collection group below.
      </div>

      {groups.map((g, idx) => {
        const p = progressValues?.find((it) => it.key == `consumer-data-collection-${g.id}`);

        return (
          <ActionRow
            topBorder={idx == 0}
            bottomBorder={idx != groups.length - 1}
            key={g.id}
            label={g.name}
            status={<ProgressSpan progress={p?.progress?.value || "NOT_STARTED"} />}
            onRowClick={() => gotoGroupPage(g)}
          />
        );
      })}
    </GetCompliantPageWrapper>
  );
};
