import _ from "lodash";
import React, { useMemo, useState } from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import {
  GDPRContactUnsafeTransfer,
  GDPREnsureSccInPlace,
  OrganizationDataRecipientDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { QuestionLabel } from "../../../components/get-compliant/survey/QuestionContainer";
import { SurveyQuestion } from "../../../components/get-compliant/survey/SurveyQuestion";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { CopyTextCallout } from "../../../components/organisms/callouts/CopyTextCallout";
import {
  DataRecipientRadioOptions,
  RadioDirection,
  RadioOption,
} from "../../../components/organisms/data-recipients/DataRecipientRadioOptions";
import { Para } from "../../../components/typography/Para";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import {
  UpdateVendorFn,
  useUpdateVendor,
} from "../../../hooks/get-compliant/vendors/useUpdateVendor";
import { reminderSurvey } from "../../../surveys/reminderSurvey";
import { isFinished, prepareSurvey } from "../../../surveys/survey";
import { GetCompliantFooter } from "../ProgressFooter";

const REMINDER_SURVEY_NAME = "reminder-survey";

const fullEmail = `Hello,
You are one of our providers who processes personal \
data and we are trying to determine if you have Standard \
Contractual Clauses (SCCs) in place that govern our \
transfer of personal data to you from the EEA/UK, \
in accordance with GDPR. If you have SCCs in place, \
could you please direct me to the supporting documentation?`;

export const GDPRContactProcessors = () => {
  // Variables
  const orgId = usePrimaryOrganizationId();

  // Requests
  const [dataRecipients, orgVendorsRequest, refresh] = useOrganizationVendors(orgId);
  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, REMINDER_SURVEY_NAME);
  const { updateVendor } = useUpdateVendor(orgId, dataRecipients);

  const recipientsToContact = useMemo(
    () =>
      dataRecipients.filter(
        (dr) =>
          !dr.gdprHasSccs &&
          !dr.gdprHasUserProvidedSccs &&
          dr.disclosedToByEeaUkGroup &&
          dr.gdprNeedsSccs &&
          !dr.isExceptedFromSccs,
      ),
    [dataRecipients],
  );

  const unsafeTransferRecipients = useMemo(() => {
    return recipientsToContact.filter((r) => r.gdprContactUnsafeTransfer === "UNSAFE");
  }, [recipientsToContact]);

  const ensureRecipients = useMemo(
    () => unsafeTransferRecipients.filter((r) => r.gdprEnsureSccInPlace === "IN_PLACE"),
    [unsafeTransferRecipients],
  );

  const survey = reminderSurvey(answers);
  const questions = prepareSurvey(survey, answers);
  const finishedWithSurvey = isFinished(survey, answers);
  const hasRecipients = recipientsToContact?.length > 0;

  // Does every recipient to contact have an unsafe contact setting?
  const doneWithUnsafeRecipients = useMemo(
    () => recipientsToContact.every((dr) => !_.isNil(dr.gdprContactUnsafeTransfer)),
    [recipientsToContact],
  );

  // Does every unsafe transfer recipient have an ensure SCCs are in place setting?
  const doneWithEnsureRecipients = useMemo(
    () => unsafeTransferRecipients.every((dr) => !_.isNil(dr.gdprEnsureSccInPlace)),
    [unsafeTransferRecipients],
  );

  const needsSurvey = ensureRecipients.length > 0;

  const nextDisabled =
    hasRecipients &&
    (!doneWithUnsafeRecipients ||
      !doneWithEnsureRecipients ||
      (needsSurvey && !finishedWithSurvey));

  return (
    <GetCompliantPageWrapper
      requests={[surveyRequest, orgVendorsRequest]}
      progressFooter={
        <GetCompliantFooter
          presentStep="ConsumerCollection.ContactProcessors"
          nextDisabled={nextDisabled}
          nextLabel="Done"
        />
      }
    >
      <PageHeader
        titleContent="Contact Processors"
        descriptionContent={
          <>
            <Para>
              The processors for which you did not locate Standard Contractual Clauses are listed
              below. You may contact them about SCCs using the email template below. We'll mark them
              “pending” for now. You'll be able to update them later in Stay Compliant.
            </Para>
            <Para>
              If you do not wish to contact these vendors or do not believe they have SCCs, mark
              them as "Unsafe transfer” below.
            </Para>
          </>
        }
      />
      <CopyTextCallout
        copy={fullEmail}
        title="Use the email template below (or create your own) to contact vendors and inquire about service provider status"
      />

      <ContactedProcessorBlock
        recipients={recipientsToContact}
        updateVendor={updateVendor}
        refresh={refresh}
      />

      {unsafeTransferRecipients.length > 0 && (
        <UnsafeTransfersBlock
          recipients={unsafeTransferRecipients}
          updateVendor={updateVendor}
          refresh={refresh}
        />
      )}

      {unsafeTransferRecipients.length > 0 &&
        ensureRecipients.length > 0 &&
        questions.map((q) => {
          return (
            <SurveyQuestion
              key={q.slug}
              question={q}
              answer={answers[q.slug]}
              onAnswer={(answer) =>
                setAnswer(q.slug, answer, q.plaintextQuestion ?? (q.question as string))
              }
            />
          );
        })}
    </GetCompliantPageWrapper>
  );
};

type ContactedProcessorBlockProps = {
  recipients: OrganizationDataRecipientDto[];
  updateVendor: UpdateVendorFn;
  refresh: () => void;
};

const CONTACT_PROCESSOR_OPTIONS: RadioOption<GDPRContactUnsafeTransfer>[] = [
  { label: "I contacted this vendor", value: "CONTACTED" },
  { label: "This is an unsafe transfer", value: "UNSAFE" },
];

const ContactedProcessorBlock: React.FC<ContactedProcessorBlockProps> = ({
  recipients,
  updateVendor,
  refresh,
}) => {
  return (
    <div className="mt-xl">
      {recipients.map((r) => {
        const onChange = async (updated: GDPRContactUnsafeTransfer) => {
          await updateVendor({
            vendorId: r.vendorId,
            gdprContactUnsafeTransfer: updated,
            callback: refresh,
          });
        };

        return (
          <VendorRadioSelection
            key={r.id}
            initial={r?.gdprContactUnsafeTransfer}
            recipient={r}
            options={CONTACT_PROCESSOR_OPTIONS}
            direction={"row"}
            onChange={onChange}
          />
        );
      })}
    </div>
  );
};

type UnsafeTransfersBlockProps = {
  recipients: OrganizationDataRecipientDto[];
  updateVendor: UpdateVendorFn;
  refresh: () => void;
};

const UNSAFE_TRANSFER_OPTIONS: RadioOption<GDPREnsureSccInPlace>[] = [
  { label: "We will ensure SCCs are in place", value: "IN_PLACE" },
  { label: "We will stop using this provider", value: "STOP_USING" },
];

const UnsafeTransfersBlock: React.FC<UnsafeTransfersBlockProps> = ({
  recipients,
  updateVendor,
  refresh,
}) => {
  return (
    <div className="mt-xl">
      <QuestionLabel
        label="Unsafe Transfers"
        helpText={
          <>
            <Para className="pb-md">
              Transferring personal data from the EEA/UK to the processors below is considered
              unsafe and in violation of GDPR.
            </Para>
            <Para>
              To comply with GDPR, your business should either have these processors agree to SCCs,
              or cease its use of these providers and, if needed, use an alternative service that
              meets GDPR requirements.
            </Para>
          </>
        }
      />
      {recipients.map((r) => {
        const onChange = async (updated: GDPREnsureSccInPlace) => {
          await updateVendor({
            vendorId: r.vendorId,
            gdprEnsureSccInPlace: updated,
            callback: refresh,
          });
        };

        return (
          <VendorRadioSelection
            key={r.id}
            initial={r?.gdprEnsureSccInPlace}
            recipient={r}
            options={UNSAFE_TRANSFER_OPTIONS}
            direction={"col"}
            onChange={onChange}
          />
        );
      })}
    </div>
  );
};

type VendorRowProps = {
  recipient: OrganizationDataRecipientDto;
  initial: string;
  options: RadioOption[];
  direction: RadioDirection;
  onChange: (updated: string) => void;
};
const VendorRadioSelection: React.FC<VendorRowProps> = ({
  recipient,
  initial,
  options,
  direction,
  onChange,
}) => {
  const [value, setValue] = useState<string | null>(initial);

  const handleChange = (updated: string) => {
    setValue(updated);
    onChange(updated);
  };

  return (
    <DataRecipientRadioOptions
      recipient={recipient}
      options={options}
      value={value}
      onChange={handleChange}
      fullWidth
      direction={direction}
    />
  );
};
