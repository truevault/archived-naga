import React, { useEffect, useState } from "react";
import { useAllCollectedPIC } from "../../../../common/hooks/dataMapHooks";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { GetCompliantStep } from "../../../../common/service/server/Types";
import { Loading } from "../../../components";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { CorePrivacyConceptsSurvey } from "../../../components/organisms/survey/CorePrivacyConcepts";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { useDataSubjectTypes, usePrimaryOrganizationId } from "../../../hooks";
import { useDataMap } from "../../../hooks/useDataMap";
import { useProcessingActivites } from "../../../hooks/useProcessingActivities";
import { GetCompliantFooter } from "../ProgressFooter";

export const CORE_PRIVACY_CONCEPTS_SURVEY = "core-privacy-concepts-survey";
const Header: React.FC = () => {
  return (
    <PageHeader
      titleContent="Minimization"
      descriptionContent={
        <>
          Privacy laws require your business to comply with a few core principles. The prompts on
          this screen will help you understand <strong>the two core privacy concepts</strong> and
          see if your business is complying with them. If not, we’ll work with you to update and
          implement best practices. We will cover:
        </>
      }
    />
  );
};

interface Props {
  currentProgress: GetCompliantStep;
}
export const CorePrivacyConceptsPage: React.FC<Props> = ({ currentProgress }) => {
  // Variables
  const organizationId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  const { answers, setAnswer, surveyRequest } = useSurvey(
    organizationId,
    CORE_PRIVACY_CONCEPTS_SURVEY,
  );

  const [dataMap, dataMapRequest] = useDataMap(organizationId);
  const collectedPic = useAllCollectedPIC(dataMap);

  const [activities, activityRequest] = useProcessingActivites(organizationId);

  // Requests
  const [fetchedConsumerGroups, consumerGroupsRequest] = useDataSubjectTypes(organizationId);

  const [allAnswered, setAllAnswered] = useState(false);

  // Effects
  useEffect(() => {
    setReadyToRender(true);
  }, [
    fetchedConsumerGroups,
    consumerGroupsRequest,
    activityRequest,
    dataMapRequest,
    surveyRequest,
  ]);

  if (!readyToRender) {
    return (
      <GetCompliantPageWrapper currentProgress={currentProgress}>
        <Loading />
      </GetCompliantPageWrapper>
    );
  }

  const isFinished = answers["cpc-done"] === "true" && allAnswered;

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      progressFooter={
        <GetCompliantFooter
          presentStep="ConsumerCollection.Minimization"
          nextDisabled={!isFinished}
          nextDisabledTooltip="Please select at least one purpose for each consumer group before continuing"
        />
      }
    >
      <Header />

      <ol className="custom p-0">
        <li className="pb-md">Data Minimization</li>
        <li className="pb-md">Purpose Limitation</li>
      </ol>

      <SurveyGateButton
        slug="cpc-getting-started"
        survey={answers}
        updateResponse={setAnswer}
        label="Get Started"
      >
        <CorePrivacyConceptsSurvey
          answers={answers}
          setAnswer={setAnswer}
          activities={activities}
          collectedPic={collectedPic}
          onAllAnsweredChanged={(allAnswered) => setAllAnswered(allAnswered)}
        />
      </SurveyGateButton>
    </GetCompliantPageWrapper>
  );
};
