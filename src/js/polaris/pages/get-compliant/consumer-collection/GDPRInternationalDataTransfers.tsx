import { CheckCircle } from "@mui/icons-material";
import { Button } from "@mui/material";
import clsx from "clsx";
import _ from "lodash";
import React, { useCallback, useMemo } from "react";
import { ExternalLink } from "../../../../common/components/ExternalLink";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { EmptyInfo } from "../../../components/Empty";
import { QuestionLabel } from "../../../components/get-compliant/survey/QuestionContainer";
import { GDPRSccDocumentationLocator } from "../../../components/get-compliant/vendors/GDPRSccDocumentationLocator";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { DataRecipientRow } from "../../../components/organisms/data-recipients/DataRecipientRow";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import {
  UpdateVendorFn,
  useUpdateVendor,
} from "../../../hooks/get-compliant/vendors/useUpdateVendor";
import { useProgressKeys } from "../../../hooks/useProgressKeys";
import { Routes } from "../../../root/routes";
import { GetCompliantFooter } from "../ProgressFooter";

export const VENDOR_CLASSIFICATION_SURVEY_NAME = "vendor-classification-survey";

export const GDPRInternationalDataTransfers = () => {
  // Variables
  const orgId = usePrimaryOrganizationId();

  // Requests
  const [dataRecipients, orgVendorsRequest, refresh] = useOrganizationVendors(orgId);
  const { update: updateProgress } = useProgressKeys(orgId);
  const { updateVendor } = useUpdateVendor(orgId, dataRecipients);

  const relevantRecipients = useMemo(
    () => dataRecipients.filter((dr) => dr.disclosedToByEeaUkGroup && dr.gdprNeedsSccs),
    [dataRecipients],
  );

  const potentiallyExceptedRecipients = useMemo(
    () => relevantRecipients.filter((dr) => dr.isPotentiallyExceptedFromSccs),
    [relevantRecipients],
  );

  const remainingRecipients = useMemo(
    () => relevantRecipients.filter((dr) => !dr.isExceptedFromSccs),
    [relevantRecipients],
  );

  const [recipientsWithSccs, recipientsWithoutSccs] = useMemo(
    () => _.partition(remainingRecipients, (dr) => dr.gdprHasSccs && dr.gdprNeedsSccs),
    [remainingRecipients],
  );

  const needsContactProcessors = useMemo(
    () => recipientsWithoutSccs.some((dr) => !dr.gdprHasSccSetting),
    [recipientsWithoutSccs],
  );

  const handleNext = useCallback(() => {
    if (!needsContactProcessors) {
      return updateProgress({ keyId: "cc_gdpr_int_data_transfers", progress: "DONE" });
    }
  }, [needsContactProcessors, updateProgress]);

  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, VENDOR_CLASSIFICATION_SURVEY_NAME);

  const isFinished = recipientsWithoutSccs.every(
    (v) => !v.gdprHasSccSetting || v.gdprSccUrl || v.gdprSccFileKey,
  );

  return (
    <GetCompliantPageWrapper
      requests={[surveyRequest, orgVendorsRequest]}
      progressFooter={
        <GetCompliantFooter
          presentStep="ConsumerCollection.InternationalDataTransfers"
          nextUrl={
            needsContactProcessors
              ? Routes.getCompliant.dataCollection.contactProcessors
              : undefined
          }
          nextSkip={needsContactProcessors ? undefined : ["ConsumerCollection.ContactProcessors"]}
          nextOnClick={handleNext}
          nextLabel={needsContactProcessors ? "Next" : "Done"}
          nextDisabled={!isFinished}
          nextDisabledTooltip="Please answer all the questions before proceeding."
        />
      }
    >
      <PageHeader
        titleContent="Data Transfers"
        descriptionContent={
          <>
            The GDPR imposes special rules on the transfer of personal data outside of the EEA/UK.
            If your business transfers data to certain "unsafe" third countries — including the
            U.S., such as for processing on U.S.-based servers — your business is required to take
            additional compliance steps. Answer a few questions to determine if these rules apply.
          </>
        }
      />
      <div className="mt-xl">
        <SurveyGateButton
          survey={answers}
          slug="gdpr-international-data-transfers-get-started"
          updateResponse={setAnswer}
          label="Get Started"
        >
          <div className="mb-xl">
            <ExceptedRecipientsQuestion
              exceptedRecipients={potentiallyExceptedRecipients}
              updateVendor={updateVendor}
              refresh={refresh}
            />
          </div>

          <SurveyGateButton
            survey={answers}
            slug="gdpr-excepted-recipients-done"
            updateResponse={setAnswer}
            label="Done"
            bypass={potentiallyExceptedRecipients.length == 0}
          >
            <div className="mb-xl">
              <Paras>
                <Para>
                  Your Data Recipients in the table below process personal data from the EEA/UK in
                  "unsafe" countries. The GDPR requires these processors to incorporate certain
                  promises, called Standard Contractual Clauses (SCCs) into their terms of service.{" "}
                  <ExternalLink
                    href="https://help.truevault.com/article/164-standard-contractual-clauses"
                    target="_blank"
                  >
                    What are SCCs?
                  </ExternalLink>
                </Para>
              </Paras>
            </div>

            <div className="mb-xl">
              <DataRecipientsSccRecommendation recipients={recipientsWithSccs} />
            </div>

            <div className="mb-xl">
              <DataRecipientsSccSelection
                recipients={recipientsWithoutSccs}
                updateVendor={updateVendor}
                refresh={refresh}
              />
            </div>
          </SurveyGateButton>
        </SurveyGateButton>
      </div>
    </GetCompliantPageWrapper>
  );
};

type ExceptedRecipientsQuestionProps = {
  exceptedRecipients: OrganizationDataRecipientDto[];
  updateVendor: UpdateVendorFn;
  refresh: () => void;
};

const ExceptedRecipientsQuestion: React.FC<ExceptedRecipientsQuestionProps> = ({
  exceptedRecipients,
  updateVendor,
  refresh,
}) => {
  const handleChange = useCallback(
    async (r: OrganizationDataRecipientDto, removedFromExceptionsToScc: boolean) => {
      await updateVendor({
        vendorId: r.vendorId,
        removedFromExceptionsToScc,
        callback: refresh,
      });
    },
    [updateVendor, refresh],
  );

  if (exceptedRecipients.length == 0) {
    return null;
  }

  return (
    <div className="mb-md">
      <QuestionLabel
        label="We’ve identified the following data processors as processing data to help your business fulfill a contract to the consumer, such as processing and shipping an order. If you disagree, remove the Data Recipients below. Otherwise, click “Done.”"
        helpText="The additional requirements for international data transfers do not apply to data processing that is necessary for your business to perform a contract with the consumer. As such, these Data Recipients will be removed from the following steps."
      />
      <div className="my-lg">
        {exceptedRecipients.map((excepted) => (
          <DataRecipientRow
            recipient={excepted}
            key={excepted.vendorId}
            className={clsx({ "del text-muted": excepted.removedFromExceptionsToScc })}
            logoProps={{
              className: clsx({ "opacity-50": excepted.removedFromExceptionsToScc }),
            }}
          >
            <div className="w-100 text-right">
              {excepted.removedFromExceptionsToScc ? (
                <Button
                  variant="text"
                  color="secondary"
                  onClick={() => handleChange(excepted, false)}
                >
                  Restore
                </Button>
              ) : (
                <Button
                  variant="text"
                  color="secondary"
                  onClick={() => handleChange(excepted, true)}
                >
                  Remove
                </Button>
              )}
            </div>
          </DataRecipientRow>
        ))}
      </div>
    </div>
  );
};

type DataRecipientsSccRecommendationProps = {
  recipients: OrganizationDataRecipientDto[];
};
const DataRecipientsSccRecommendation: React.FC<DataRecipientsSccRecommendationProps> = ({
  recipients,
}) => {
  if (recipients.length < 1) {
    return (
      <>
        <QuestionLabel label="Data Recipients with Standard Contractual Clauses" />
        <EmptyInfo>
          Based on our research, none of your Data Recipients have incorporated SCCs into their
          documentation.
        </EmptyInfo>
      </>
    );
  }

  return (
    <>
      <div className="mt-xl">
        <QuestionLabel
          label="Data Recipients with Standard Contractual Clauses"
          helpText="Based on our research, the Data Recipients below have incorporated SCCs into their documentation."
        />
        {recipients.map((r) => {
          return (
            <DataRecipientRow
              key={r.id}
              recipient={r}
              annotation={
                r.gdprSccDocumentationUrl && (
                  <ExternalLink href={r.gdprSccDocumentationUrl}>View documentation</ExternalLink>
                )
              }
            />
          );
        })}
      </div>
    </>
  );
};

type DataRecipientsSccSelectionProps = {
  recipients: OrganizationDataRecipientDto[];
  updateVendor: UpdateVendorFn;
  refresh: () => void;
};
const DataRecipientsSccSelection: React.FC<DataRecipientsSccSelectionProps> = ({
  recipients,
  updateVendor,
  refresh,
}) => {
  if (recipients.length < 1) {
    return (
      <>
        <QuestionLabel label="Standard Contractual Clauses not confirmed" />
        <EmptyInfo Icon={CheckCircle}>None of your Data Recipients are missing SCCs.</EmptyInfo>
      </>
    );
  }

  return (
    <>
      <div className="mt-xl">
        <QuestionLabel
          label="Standard Contractual Clauses not confirmed"
          helpText="We could not confirm SCCs for the Data Recipients below. Search for the SCCs yourself, or select ”not found” to contact the Data Recipient in the next step and ask about SCCs for international transfers."
        />

        <Callout variant={CalloutVariant.Purple}>
          Look for language like: “The Parties agree that the terms of the Standard Contractual
          Clauses are hereby incorporated by reference and shall be deemed to have been executed by
          the Parties and apply to any cross-border transfers of personal data.”
        </Callout>

        <GDPRSccDocumentationLocator
          recipients={recipients}
          updateVendor={updateVendor}
          refresh={refresh}
        />
      </div>
    </>
  );
};
