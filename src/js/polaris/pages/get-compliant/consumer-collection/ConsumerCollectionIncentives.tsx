import React, { useCallback } from "react";
import { useParsedSurveyAnswers, useSurvey } from "../../../../common/hooks/useSurvey";
import { GetCompliantStep } from "../../../../common/service/server/Types";
import { FinancialIncentivesQuestion } from "../../../components/get-compliant/survey/custom/FinancialIncentivesQuestion";
import { SurveyQuestion } from "../../../components/get-compliant/survey/SurveyQuestion";
import { GetCompliantPageWrapper } from "../../../components/layout/GetCompliantPageWrapper";
import { PageHeader } from "../../../components/layout/header/Header";
import { usePrimaryOrganization } from "../../../hooks/useOrganization";
import {
  consumerCollectionIncentivesSurvey,
  CONSUMER_INCENTIVES_SURVEY_NAME,
  OFFERS_CONSUMER_INCENTIVES_SLUG,
} from "../../../surveys/consumerCollectionIncentivesSurvey";
import { OrgSettingQuestion } from "../../../surveys/orgSettings";
import { isFinished, prepareSurvey } from "../../../surveys/survey";
import { GetCompliantFooter } from "../ProgressFooter";

type ConsumerCollectionIncentivesProps = {
  currentProgress: GetCompliantStep;
};

export const ConsumerCollectionIncentives: React.FC<ConsumerCollectionIncentivesProps> = ({
  currentProgress,
}) => {
  // Variables
  const [organization] = usePrimaryOrganization();

  const { answers, setAnswer, surveyRequest } = useSurvey(
    organization.id,
    CONSUMER_INCENTIVES_SURVEY_NAME,
  );

  // Handlers

  const handleAnswer = useCallback(
    (slug: string, question?: string) => (answer: string) =>
      setAnswer(slug, answer, typeof question === "string" ? question : undefined),
    [setAnswer],
  );

  const handleOrgSettingAnswer = useCallback(
    (question: OrgSettingQuestion, answer: string) =>
      setAnswer(question.slug, answer, typeof question === "string" ? question : undefined),
    [setAnswer],
  );

  const survey = consumerCollectionIncentivesSurvey(answers, true);
  const questions = prepareSurvey(survey, answers);
  const parsedAnswers = useParsedSurveyAnswers(answers);

  const nextEnabled = isFinished(survey, answers);

  return (
    <GetCompliantPageWrapper
      currentProgress={currentProgress}
      requests={[surveyRequest]}
      progressFooter={
        <GetCompliantFooter
          presentStep="ConsumerCollection.FinancialIncentives"
          nextDisabled={!nextEnabled}
        />
      }
    >
      <PageHeader titleContent="Financial Incentives" />
      <div className="w-896">
        <div>
          {questions.map((q, i) => {
            switch (q.slug) {
              case OFFERS_CONSUMER_INCENTIVES_SLUG:
                return (
                  <FinancialIncentivesQuestion
                    key={q.slug}
                    org={organization}
                    question={{
                      slug: q.slug,
                      source: CONSUMER_INCENTIVES_SURVEY_NAME,
                      label: undefined,
                      question: q.question as string,
                      help: q.helpText,
                    }}
                    answer={parsedAnswers[q.slug]}
                    setAnswer={handleOrgSettingAnswer}
                    enableTask={false}
                  />
                );
              default:
                return (
                  <div className="mb-xl" key={i}>
                    <SurveyQuestion
                      key={q.slug}
                      question={q}
                      answer={answers[q.slug]}
                      onAnswer={handleAnswer(q.slug, q.plaintextQuestion ?? (q.question as string))}
                    />
                  </div>
                );
            }
          })}
        </div>
      </div>
    </GetCompliantPageWrapper>
  );
};
