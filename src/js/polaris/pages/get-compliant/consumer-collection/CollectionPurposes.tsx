import { Add, Info } from "@mui/icons-material";
import { Checkbox, OutlinedInput, Stack } from "@mui/material";
import clsx from "clsx";
import _ from "lodash";
import React, { useEffect, useMemo, useState } from "react";
import { LoadingButton } from "../../../../common/components/LoadingButton";
import { LoadingButton as MuiLoadingButton } from "@mui/lab";
import { NetworkRequestGate } from "../../../../common/components/NetworkRequestGate";
import { useAllCollectedPIC } from "../../../../common/hooks/dataMapHooks";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { NetworkRequestState } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import {
  AllCollectionGroupTypes,
  CollectionGroupType,
} from "../../../../common/service/server/controller/CollectionGroupsController";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { CategoryDto } from "../../../../common/service/server/dto/DataMapDto";
import {
  DefProcessingActivityDto,
  ProcessingActivityDto,
} from "../../../../common/service/server/dto/ProcessingActivityDto";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { EmptyInfo } from "../../../components/Empty";
import { QuestionLabel } from "../../../components/get-compliant/survey/QuestionContainer";
import { SurveyBooleanQuestion } from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import { CheckboxControlLabel } from "../../../components/input/CheckboxControlLabel";
import { usePrimaryOrganization } from "../../../hooks";
import { useDataMap } from "../../../hooks/useDataMap";
import { ConfirmDialog } from "../../../components/dialog";
import {
  AssociatePicMutation,
  useDefaultActivityCheckboxes,
  useProcessingActivites,
} from "../../../hooks/useProcessingActivities";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { Disableable } from "../../../components";
import { ProcessingRegion } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";

type CollectionPurposesProps = {
  consumerGroups: CollectionGroupDetailsDto[];
  picWarning: "top" | "bottom";
  excludeCollectionGroupType?: CollectionGroupType[];
  hideTitles?: boolean;
  setCurrentRequest?: (req: NetworkRequestState) => void;
  setComplete?: (complete: boolean) => void;
  readonly?: boolean;
};

export const COLLECTION_PURPOSE_SURVEY_SLUG = "collection-purpose-survey";

export const CollectionPurposes: React.FC<CollectionPurposesProps> = ({
  consumerGroups,
  picWarning,
  setComplete,
  readonly,
}) => {
  const [org] = usePrimaryOrganization();
  const orgId = org?.id;

  const [dataMap, dataMapRequest] = useDataMap(orgId);
  const groupTypes = consumerGroups.map((cg) => cg.collectionGroupType);
  const excluded = AllCollectionGroupTypes.filter((type) => !groupTypes.includes(type));
  const collectedPic = useAllCollectedPIC(dataMap, excluded);
  const { answers, setAnswer } = useSurvey(orgId, COLLECTION_PURPOSE_SURVEY_SLUG);

  const [activities, activityRequest, refreshActivities, assocationMutation] =
    useProcessingActivites(orgId);

  // CUSTOM ACTIVITIES
  const custom = useMemo(
    () => activities?.filter((a) => !a.defaultId && a.regions?.includes("UNITED_STATES")) ?? [],
    [activities],
  );
  const [customDeleteId, setCustomDeleteId] = useState(null);
  const [addCustomLoading, setAddCustomLoading] = useState(false);
  const handleAddCustomActivity = async (name: string) => {
    if (activities.map((a) => a.name).includes(name)) return;

    setAddCustomLoading(true);
    try {
      await Api.processingActivities.createProcessingActivity(orgId, {
        name: name,
        defaultId: undefined,
        lawfulBases: undefined,
        regions: ["UNITED_STATES", "EEA_UK"],
      });
      await refreshActivities();
    } finally {
      setAddCustomLoading(false);
    }
  };

  const finishRemoveCustomActivity = async (id: string) => {
    let regions: ProcessingRegion[] = ["UNITED_STATES"];
    if (activities?.find((a) => a.id == id && a.regions?.includes("EEA_UK"))) {
      regions.push("EEA_UK");
    }
    await Api.processingActivities.deleteProcessingActivityRegion(orgId, id, regions);
    await refreshActivities();
  };

  const handleRemoveCustomActivity = (id: string) => {
    setCustomDeleteId(id);
  };

  const closeRemoveCustomActivity = () => setCustomDeleteId(null);
  // END CUSTOM ACTIVITIES

  const notProcessedPic = useMemo(() => {
    const allProcessedPic = _.uniq((activities ?? []).flatMap((a) => a.processedPic));
    return collectedPic?.filter((c) => !allProcessedPic.includes(c.id)) ?? [];
  }, [activities, collectedPic]);

  const defCheckboxes = useDefaultActivityCheckboxes(
    orgId,
    activities,
    "UNITED_STATES",
    ["UNITED_STATES", "EEA_UK"],
    refreshActivities,
  );

  const anySelected = useMemo(
    () => activities?.some((a) => a.processedPic.length > 0),
    [activities],
  );

  const allCheckedHaveSelections = useMemo(() => {
    return defCheckboxes.props.checked.every((id) => {
      const act = activities?.find((a) => a.defaultId == id);
      if (act) {
        return act.processedPic.length > 0;
      }
    });
  }, [defCheckboxes.props.checked, activities]);

  const finishedCustom =
    answers["has-custom-processing-activities"] == "false" || custom.length > 0;

  useEffect(() => {
    setComplete?.(
      anySelected && allCheckedHaveSelections && notProcessedPic.length === 0 && finishedCustom,
    );
  }, [anySelected, allCheckedHaveSelections, notProcessedPic, finishedCustom, setComplete]);

  return (
    <NetworkRequestGate
      requests={[dataMapRequest, activityRequest, defCheckboxes.requests.defaultActivities]}
    >
      <h4 className="intermediate">{org.name} uses personal data for:</h4>
      <Stack className="ml-xl" spacing={2}>
        {notProcessedPic.length > 0 && picWarning == "top" && (
          <Callout variant={CalloutVariant.Yellow} icon={Info}>
            <p>
              The following personal information categories are not selected for any collection
              purposes.
            </p>
            <ul>
              {notProcessedPic.map((c) => (
                <li key={c.id}>
                  <strong>{c.name}</strong>
                </li>
              ))}
            </ul>
          </Callout>
        )}
        <div className="mt-lg" />
        {defCheckboxes.props.defaults?.map((defActivity) => {
          return (
            <CollectionPurposeCollapseCard
              key={defActivity.id}
              defaultActivity={defActivity}
              activities={activities}
              checked={defCheckboxes.props.checked}
              setChecked={defCheckboxes.props.setChecked}
              categories={collectedPic}
              assocationMutation={assocationMutation}
              readonly={readonly}
              variant="checkbox"
            />
          );
        })}
      </Stack>
      <Stack spacing={2}>
        <div className="mt-lg" />
        <SurveyGateButton
          className="ml-xl w-192"
          label="Done"
          slug="lawful-bases-default-done"
          survey={answers}
          updateResponse={setAnswer}
        >
          <QuestionLabel label="Additional Processing Activities" />
          <SurveyBooleanQuestion
            answer={answers["has-custom-processing-activities"]}
            onAnswer={(ans) => setAnswer("has-custom-processing-activities", ans)}
            question={{
              type: "boolean",
              slug: "has-custom-processing-activities",
              question:
                "Does your business engage in any core data processing activities that are not listed above?",
              helpText:
                " Think about data processing specific to your type of business, like “Connecting people with local contractors,” “Providing eye prescriptions,” or “Determining eligibility for scholarships.” If you’re an eCommerce company, the list above likely covers your core data processing activities.",
              yesLabel: "Yes, add more",
              noLabel: "Nope, that covers it",
              visible: true,
            }}
          />
          {answers["has-custom-processing-activities"] === "true" && (
            <CustomPA
              custom={custom}
              categories={collectedPic}
              associationMutation={assocationMutation}
              variant="text"
              open
              onAdd={handleAddCustomActivity}
              addLoading={addCustomLoading}
              onRemove={handleRemoveCustomActivity}
            />
          )}

          {notProcessedPic.length > 0 && picWarning == "bottom" && (
            <Callout variant={CalloutVariant.Yellow} icon={Info}>
              <p>
                The following personal information categories are not selected for any collection
                purposes.
              </p>
              <ul>
                {notProcessedPic.map((c) => (
                  <li key={c.id}>
                    <strong>{c.name}</strong>
                  </li>
                ))}
              </ul>
            </Callout>
          )}
        </SurveyGateButton>
      </Stack>
      <ConfirmDialog
        open={Boolean(customDeleteId)}
        onClose={closeRemoveCustomActivity}
        onConfirm={() => {
          finishRemoveCustomActivity(customDeleteId);
          closeRemoveCustomActivity();
        }}
        confirmTitle="Are you sure?"
        contentText="Are you sure you would like to remove this Purpose?"
        confirmText="Yes"
        cancelText="No"
      />
    </NetworkRequestGate>
  );
};

type CustomPAProps = {
  custom: ProcessingActivityDto[];
  categories: CategoryDto[];
  associationMutation: AssociatePicMutation;
  variant: CollectionPurposeCollapseCardVariant;
  open?: boolean;
  addLoading: boolean;
  indentAddRow?: boolean;
  onAdd: (name: string) => Promise<void>;
  onRemove: (id: string) => void;
};

export const CustomPA: React.FC<CustomPAProps> = ({
  custom,
  categories,
  associationMutation,
  variant,
  open,
  addLoading,
  indentAddRow = false,
  onAdd,
  onRemove,
}) => {
  const [customName, setCustomName] = useState("");

  const handleAddClick = async () => {
    if (customName == "") return;
    await onAdd(customName);
    setCustomName("");
  };

  return (
    <>
      <div className={clsx("flex flex-row my-mdlg", { "ml-lg": indentAddRow })}>
        <OutlinedInput
          className="w-512"
          value={customName}
          onChange={(e) => {
            setCustomName(e.target.value);
          }}
          placeholder="Add a processing activity, keep it short and sweet!"
          disabled={addLoading}
        />
        <LoadingButton
          loading={addLoading}
          onClick={handleAddClick}
          variant="contained"
          color="primary"
          startIcon={<Add />}
          className="ml-md px-md"
        >
          Add
        </LoadingButton>
      </div>

      <Stack spacing={4} className="ml-lg">
        {custom.map((a) => {
          return (
            <div key={a.id}>
              <CollectionPurposeCollapseCard
                activity={a}
                activities={custom}
                variant={variant}
                overrideText={variant == "text" ? "Remove" : undefined}
                open={open}
                checked={[]}
                setChecked={(id, _) => onRemove(id)}
                categories={categories}
                assocationMutation={associationMutation}
                readonly={false}
                collapsible={true}
              />
            </div>
          );
        })}
      </Stack>
    </>
  );
};

export type CollectionPurposeCollapseCardVariant = "checkbox" | "text" | "none";
type CollectionPurposeCollapseCardProps = {
  defaultActivity?: DefProcessingActivityDto | undefined;
  activity?: ProcessingActivityDto | undefined;
  activities: ProcessingActivityDto[];
  variant: CollectionPurposeCollapseCardVariant;
  overrideText?: string;
  checked: string[];
  setChecked: (defaultId: string, checked: boolean) => void;
  categories: CategoryDto[];
  assocationMutation: AssociatePicMutation;
  readonly?: boolean;
  open?: boolean;
  collapsible?: boolean;
};

export const CollectionPurposeCollapseCard: React.FC<CollectionPurposeCollapseCardProps> = ({
  defaultActivity,
  activity,
  activities,
  variant,
  overrideText,
  checked,
  setChecked,
  categories,
  assocationMutation,
  readonly,
  open,
  collapsible = true,
}) => {
  const isEnabled = checked.includes(defaultActivity?.id) || checked.includes(activity?.id);

  const realActivity: ProcessingActivityDto | undefined = useMemo(
    () => activity ?? activities.find((a) => a.defaultId == defaultActivity.id),
    [activity, defaultActivity, activities],
  );

  const processed = useMemo(() => {
    const processedPic = realActivity ? [...realActivity.processedPic] : [];
    const pendingUpdates = realActivity?._local?.processedPicPendingUpdates ?? [];

    pendingUpdates.forEach((up) => {
      const existingIdx = processedPic.indexOf(up.id);
      if (up.type == "add" && existingIdx == -1) {
        processedPic.push(up.id);
      } else if (up.type == "remove" && existingIdx >= 0) {
        processedPic.splice(existingIdx, 1);
      }
    });

    return new Set(processedPic);
  }, [realActivity]);

  const localProcessedUpdate = useMemo(
    () => new Set(realActivity?._local?.processedPicPendingUpdates?.map((up) => up.id) || []),
    [realActivity?._local?.processedPicPendingUpdates],
  );

  const enabledLabel = useMemo(() => {
    return [...processed]
      .map((id) => categories.find((c) => c.id == id)?.name)
      .filter((c) => c)
      .join(", ");
  }, [processed, categories]);

  if (!isEnabled && readonly) {
    return null;
  }

  const colEnd = Math.ceil(categories.length / 2);
  const cols =
    categories.length >= 4 ? [categories.slice(0, colEnd), categories.slice(colEnd)] : [categories];

  const showCheckbox = variant == "checkbox" && !readonly;
  const showText = variant == "text" && !readonly;

  const checkbox = (
    <Checkbox
      checked={isEnabled}
      onChange={(e, checked) => setChecked(defaultActivity?.id ?? realActivity?.id, checked)}
    />
  );

  const disableRemove = Boolean(realActivity?.autocreationSlug);

  const removeText = (
    <Disableable
      disabledTooltip="You cannot remove this processing activity due your answers to the questions on the Lawful Bases page."
      disabled={disableRemove}
    >
      <MuiLoadingButton
        color="secondary"
        disabled={disableRemove}
        loading={false}
        onClick={(e) => {
          e.preventDefault();
          setChecked(defaultActivity?.id ?? realActivity?.id, !isEnabled);
        }}
      >
        {overrideText ?? (isEnabled ? "Remove from GDPR" : "Add to GDPR")}
      </MuiLoadingButton>
    </Disableable>
  );

  const action = showCheckbox ? checkbox : showText ? removeText : undefined;

  return (
    <Callout
      variant={CalloutVariant.LightPurpleBordered}
      icon={defaultActivity?.materialIconKey}
      title={realActivity?.name ?? defaultActivity?.name}
      subtitle={readonly ? enabledLabel : defaultActivity?.helpText}
      collapsed={open ? false : !collapsible || !isEnabled || readonly}
      collapsible={collapsible}
      hideCollapseToggle
      action={action}
    >
      {categories.length > 0 ? (
        <div className="flex flex-row flex-between px-xl">
          {cols.map((col, idx) => {
            return (
              <div className="flex-col w-50" key={idx}>
                {col.map((pic) => {
                  const activityId = realActivity?.id;
                  const isUpdating = localProcessedUpdate.has(pic.id);
                  const isChecked = processed.has(pic.id);

                  if (!activityId) {
                    return null;
                  }

                  return (
                    <CheckboxControlLabel
                      key={pic.id}
                      label={pic.name}
                      loading={isUpdating}
                      checked={isChecked}
                      onChange={(e, checked) => {
                        assocationMutation.mutate({
                          activityId: activityId,
                          picId: pic.id,
                          associate: checked,
                        });
                      }}
                    />
                  );
                })}
              </div>
            );
          })}
        </div>
      ) : (
        <EmptyInfo>No personal information is collected</EmptyInfo>
      )}
    </Callout>
  );
};
