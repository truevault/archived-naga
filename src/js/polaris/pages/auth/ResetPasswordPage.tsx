import { Button } from "@mui/material";
import React, { useState } from "react";
import { Form } from "react-final-form";
import { Link } from "react-router-dom";
import { Text } from "../../../common/components/input/Text";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { useApiTrigger } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { AuthPageWrapper } from "../../components/layout/AuthPageWrapper";
import { Error as RequestsError } from "../../copy/requests";
import { Routes } from "../../root/routes";
import { requiredEmail } from "../../util/validation";

export const ResetPassword = () => {
  const [hasReset, setHasReset] = useState(false);
  const [resetPassword, resetPasswordRequest] = useApiTrigger(
    (values) => Api.login.resetPassword((values as any).email),
    () => setHasReset(true),
    RequestsError.ResetPassword,
  );

  return (
    <AuthPageWrapper image="/assets/images/reset-password.svg">
      {hasReset && (
        <p>
          If an account exists in our system, you should receive a password reset email in a few
          moments. Please follow the link in the email to reset your password.
        </p>
      )}

      {!hasReset && (
        <>
          <p>Enter your email address below and we'll send you a link to reset your password.</p>

          <Form
            onSubmit={resetPassword}
            validate={resetPasswordValidation}
            render={(props) => (
              <ResetPasswordForm {...props} resetPasswordRequest={resetPasswordRequest} />
            )}
          />
        </>
      )}
    </AuthPageWrapper>
  );
};

const resetPasswordValidation = (values) => {
  const errors = {} as any;

  requiredEmail(values, "email", errors);

  return errors;
};

const ResetPasswordForm = ({ handleSubmit, resetPasswordRequest }) => {
  return (
    <form onSubmit={handleSubmit} className="w-100">
      <Text
        autoFocus
        field="email"
        label="Email Address"
        variant="outlined"
        className="mb-md"
        fullWidth
        required
      />

      <div className="button-row">
        <LoadingButton
          type="submit"
          color="primary"
          size="large"
          loading={resetPasswordRequest.running}
          className="mr-sm"
        >
          Send Email
        </LoadingButton>

        <Button component={Link} to={Routes.login} variant="text">
          Go Back
        </Button>
      </div>
    </form>
  );
};
