import React, { useState } from "react";
import axios from "axios";
import { useQueryClient } from "@tanstack/react-query";
import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import { useActionRequest } from "../../../common/hooks/api";
import { selfActions } from "../../../common/reducers/selfSlice";
import { Api } from "../../../common/service/Api";
import { UserDetailsDto } from "../../../common/service/server/dto/UserDto";
import { Forms } from "../../components";
import { AuthPageWrapper } from "../../components/layout/AuthPageWrapper";
import { Error as RequestsError } from "../../copy/requests";

export const Login = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const [error, setError] = useState(null);

  const target = new URLSearchParams(location.search).get("target");
  const admin = new URLSearchParams(location.search).get("admin");
  const queryClient = useQueryClient();

  const forwardLocation = target || admin;

  const { fetch: login, request: loginRequest } = useActionRequest({
    api: async (values) => Api.login.login((values as any).email, (values as any).password),
    onSuccess: (user) => {
      queryClient.invalidateQueries(["self"]);
      if (forwardLocation) {
        window.location.href = forwardLocation;
      } else {
        dispatch(selfActions.set(user as UserDetailsDto));
      }
    },
    onError: (_state, e) => {
      if (axios.isAxiosError(e) && e.response?.status == 401) {
        setError("Incorrect Email/Password");
      } else {
        setError("Something went wrong. Please try again later.");
      }
    },
    messages: {
      defaultError: RequestsError.Login,
    },
  });

  return (
    <AuthPageWrapper image="/assets/images/login.svg">
      <p>Welcome! Login to your account</p>

      <Forms.Login onSubmit={login} loginRequest={loginRequest} error={error} />
    </AuthPageWrapper>
  );
};
