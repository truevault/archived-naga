import React from "react";
import { PageHeader } from "../../components/layout/header/Header";
import { TransitionPageWrapper } from "../../components/layout/TransitionPageWrapper";
import { TipBox } from "../../components/TipBox";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { useSetApiMode } from "../../hooks/useSetApiMode";
import { GetCompliantContainer } from "../get-compliant/GetCompliantContainer";

const Header: React.FC = () => {
  return <PageHeader titleContent={"A reimagined compliance experience is here."} />;
};

export const LegacyTransitionPage = () => {
  useSetApiMode("Draft", "LegacyTransitionPage");
  return (
    <TransitionPageWrapper>
      <>
        <Header />

        <GetCompliantContainer className="request-instructions action-table--container">
          <Paras>
            <Para>We’ve created a new version of Polaris that we can’t wait for you to try.</Para>
            <Para>Schedule a call with us to chat about the transition process.</Para>
          </Paras>

          <TipBox title="Feature highlights" className="my-md">
            <ul>
              <li>Streamlined request processing</li>
              <li>Automated verification and response templates </li>
              <li>Fully customizable, more granular request instructions</li>
              <li>New configuration settings to address updated legal guidance</li>
            </ul>
          </TipBox>

          <h2 className="mt-xxl">Schedule your transition call</h2>
          <iframe
            src="https://calendly.com/jasonwang/polaris-transition?month=2021-08"
            style={{
              width: "100%",
              color: "transparent",
              height: "900px",
            }}
          />
        </GetCompliantContainer>
      </>
    </TransitionPageWrapper>
  );
};
