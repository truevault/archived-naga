import { useSnackbar } from "notistack";
import qs from "query-string";
import React from "react";
import { Redirect, useLocation, useParams } from "react-router";
import { NetworkRequest } from "../../../common/models";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { PageLoading } from "../../../common/components/Loading";
import { AuthPageWrapper } from "../../components/layout/AuthPageWrapper";
import { useSelf } from "../../hooks";
import { useInvitation } from "../../hooks/useInvitation";
import { Routes } from "../../root/routes";
import { SetPasswordFormWrapper } from "./SetPasswordPage";

export const AcceptInvitation = () => {
  const location = useLocation();
  const params = useParams() as any;

  const [cuSelf, cuSelfRequest] = useSelf();
  const parsed = qs.parse(location.search);
  const reset = parsed?.reset as string;

  const { enqueueSnackbar } = useSnackbar();

  const { user, requests, actions } = useInvitation(params.organizationId, reset);

  if (reset && !NetworkRequest.areFinished(requests.userRequest, cuSelfRequest)) {
    return <PageLoading />;
  }

  // Invitation link does not have reset and user isn't logged in so redirect to login
  if (!cuSelf && !reset) {
    return (
      <Redirect
        to={{
          pathname: Routes.login,
          search: `?target=${location.pathname}`,
        }}
      />
    );
  }

  // Invitation link does have a reset but does not correspond to any user, is likely stale
  // so redirect to login with a message
  if (reset && !user && !cuSelf) {
    enqueueSnackbar(
      "Invitation not found. If you already accepted please use the login page. If you haven't " +
        "accepted the invitation please contact your Polaris administrator so they may resend it.",
      { variant: "info", autoHideDuration: 10000 },
    );
    return (
      <Redirect
        to={{
          pathname: Routes.login,
        }}
      />
    );
  }

  // If user is already in the organization then just redirect back to root
  // NOTE: this does not switch their organization, they still need to do so manually
  if (cuSelf?.organizations?.find((o) => o.organization == params.organizationId) ?? false) {
    return <Redirect to={Routes.root} />;
  }

  const invitedOrg = reset
    ? user &&
      user.invitedOrganizations.find((o) => o.organization == params.organizationId)
        ?.organizationName
    : cuSelf &&
      cuSelf.invitedOrganizations.find((o) => o.organization == params.organizationId)
        ?.organizationName;

  return (
    <AuthPageWrapper image="/assets/images/reset-password.svg">
      <p>You've been invited to join {invitedOrg}.</p>

      {cuSelf && (
        <LoadingButton
          color="primary"
          size="large"
          fullWidth
          loading={requests.acceptRequest.running}
          className="mb-sm"
          onClick={actions.acceptInvitation}
        >
          Accept Invitation
        </LoadingButton>
      )}

      {!cuSelf && (
        <>
          <p>Set a password to get started</p>
          <SetPasswordFormWrapper
            setPassword={actions.setPassword}
            setPasswordRequest={requests.setPasswordRequest}
            showTos={true}
          />
        </>
      )}
    </AuthPageWrapper>
  );
};
