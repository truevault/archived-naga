import { Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import React, { useState } from "react";
import { Form } from "react-final-form";
import { useDispatch } from "react-redux";
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
import { Text } from "../../../common/components/input/Text";
import { PageLoading } from "../../../common/components/Loading";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models";
import { selfActions } from "../../../common/reducers/selfSlice";
import { Api } from "../../../common/service/Api";
import { UserDetailsDto } from "../../../common/service/server/dto/UserDto";
import { AuthPageWrapper } from "../../components/layout/AuthPageWrapper";
import { Error as RequestsError } from "../../copy/requests";
import { Routes } from "../../root/routes";
import { Validation } from "../../util";

export const SetPassword = () => {
  const dispatch = useDispatch();
  const params = useParams() as any;

  const { result: self, request: selfRequest } = useDataRequest({
    queryKey: ["self", params.resetToken],
    api: () => Api.user.getSelfByPasswordUpdateId(params.resetToken),
    messages: {
      defaultError: RequestsError.GetWithResetToken,
    },
  });

  const { fetch: setPassword, request: setPasswordRequest } = useActionRequest({
    api: (values) =>
      Api.user.updateUserPassword(self?.email, (values as any).password, params.resetToken),
    messages: {
      defaultError: RequestsError.SetPassword,
    },
    onSuccess: (user) => {
      dispatch(selfActions.set(user as UserDetailsDto));
      window.location.replace(Routes.root);
    },
  });

  if (NetworkRequest.areFinished(selfRequest) && !self) {
    // redirect to login
    return <Redirect to={Routes.auth.login} />;
  }

  if (!NetworkRequest.areFinished(selfRequest)) {
    return <PageLoading />;
  }

  return (
    <AuthPageWrapper image="/assets/images/reset-password.svg">
      <p>Reset your password</p>

      <SetPasswordFormWrapper
        setPassword={setPassword}
        setPasswordRequest={setPasswordRequest}
        showTos={false}
      />
    </AuthPageWrapper>
  );
};

export const SetPasswordFormWrapper = ({ setPassword, setPasswordRequest, showTos }) => {
  return (
    <Form
      onSubmit={setPassword}
      validate={setPasswordValidation}
      render={(props) => (
        <SetPasswordForm {...props} setPasswordRequest={setPasswordRequest} showTos={showTos} />
      )}
    />
  );
};

const setPasswordValidation = (values) => {
  const errors = {} as any;

  Validation.requiredPassword(values, "password", errors);

  return errors;
};

const SetPasswordForm = ({ handleSubmit, invalid, setPasswordRequest, showTos }) => {
  const [tos, setTos] = useState(false);
  const tosDisabled = showTos && !tos;
  return (
    <form onSubmit={handleSubmit}>
      <Text
        autoFocus
        field="password"
        label="New Password"
        variant="outlined"
        className="mb-md"
        required
        type="password"
      />

      {Boolean(showTos) && (
        <FormGroup className="mb-md">
          <FormControlLabel
            control={
              <Checkbox
                checked={tos}
                onChange={(e) => setTos(e.target.checked)}
                name="tos"
                color="primary"
              />
            }
            label={
              <>
                I agree to{" "}
                <a
                  href="https://www.truevault.com/legal/truevault-terms-of-use"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Terms of Use
                </a>
              </>
            }
          />
        </FormGroup>
      )}

      <LoadingButton
        type="submit"
        color="primary"
        size="large"
        disabled={tosDisabled || invalid}
        loading={setPasswordRequest.running}
        className="mb-sm"
      >
        Set Password
      </LoadingButton>
    </form>
  );
};
