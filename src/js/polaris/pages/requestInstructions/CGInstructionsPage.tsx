import { TextField } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { PageLoading } from "../../../common/components/Loading";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { StandardLabel } from "../../components/input/Labels";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { RequestInstructionsProgressFooter } from "../../components/requestInstructions";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { usePrimaryOrganization } from "../../hooks";
import { Routes } from "../../root/routes";
import { GetCompliantContainer } from "../get-compliant/GetCompliantContainer";

const CGInstructionsHeader: React.FC = () => {
  const description = (
    <Paras>
      <Para>
        When you receive a new privacy request, you’ll first want to look the consumer up to see if
        you have any data about them. Create instructions for your team to aid identification.
      </Para>
    </Paras>
  );

  return (
    <PageHeader
      titleHeaderContent={"Request Instructions"}
      titleContent="Manage Consumer Lookup Settings"
      descriptionContent={description}
    />
  );
};

export const CGInstructionsPage = () => {
  // Constants

  // State
  const history = useHistory();
  const [readyToRender, setReadyToRender] = useState(false);
  const [instructions, setInstructions] = useState("");

  // Requests
  const [organization, orgRequest] = usePrimaryOrganization();

  const updateInstructionsApi = useCallback(
    () =>
      Api.requestHandlingInstructions.updateConsumerGroupRequestHandlingInstructions({
        organizationId: organization.id,
        cgRequestHandlingInstructions: instructions,
      }),
    [organization.id, instructions],
  );
  const { fetch: updateInstructions, request: updateInstructionsRequest } = useActionRequest({
    api: updateInstructionsApi,
  });

  const fetchedInstructionsApi = useCallback(
    () => Api.requestHandlingInstructions.getCGInstructions(organization.id),
    [organization.id],
  );
  const { result: fetchedInstructions, request: instructionsRequest } = useDataRequest({
    queryKey: ["instructions", organization.id],
    api: fetchedInstructionsApi,
  });

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest)) {
      setReadyToRender(true);
    }
  }, [orgRequest]);

  useEffect(() => {
    if (instructionsRequest.success) {
      setInstructions(fetchedInstructions);
    }
  }, [fetchedInstructions, instructionsRequest]);

  // When all active requests are finished, load the request handling index
  useEffect(() => {
    if (NetworkRequest.areFinished(updateInstructionsRequest)) {
      history.push(Routes.organization.requestHandling.base);
    }
  }, [history, updateInstructionsRequest]);

  // Callbacks
  const onDone = () => {
    updateInstructions();
  };

  // Content

  if (!readyToRender) {
    return (
      <StandardPageWrapper>
        <CGInstructionsHeader />
        <PageLoading />
      </StandardPageWrapper>
    );
  }

  return (
    <StandardPageWrapper
      footer={<Footer onDone={onDone} updateInstructionsRequest={updateInstructionsRequest} />}
    >
      {organization && (
        <>
          <CGInstructionsHeader />

          <GetCompliantContainer className="request-instructions" wide={true}>
            <StandardLabel label="Instructions" subLabel=" (optional)" />

            <TextField
              className="instructions--text-field"
              variant="outlined"
              value={instructions}
              onChange={({ target }) => setInstructions(target.value)}
              fullWidth={false}
              multiline={true}
              rows={8}
            />
          </GetCompliantContainer>
        </>
      )}
    </StandardPageWrapper>
  );
};

const Footer = ({ onDone, updateInstructionsRequest }) => {
  return (
    <RequestInstructionsProgressFooter
      nextContent="Save"
      nextDisabled={false}
      nextOnClick={onDone}
      currentRequest={updateInstructionsRequest}
    />
  );
};
