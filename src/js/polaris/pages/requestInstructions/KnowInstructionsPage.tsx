import { Checkbox, FormControlLabel, TextField } from "@mui/material";
import clsx from "clsx";
import debounce from "lodash.debounce";
import groupBy from "lodash.groupby";
import keyBy from "lodash.keyby";
import uniqBy from "lodash.uniqby";
import React, { useEffect, useMemo, useState } from "react";
import { useHistory } from "react-router-dom";
import { ExternalLink } from "../../../common/components/ExternalLink";
import { useActionRequest } from "../../../common/hooks/api";
import { SurveyAnswers, useSurvey } from "../../../common/hooks/useSurvey";
import { Api } from "../../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../../common/service/server/dto/CollectionGroupDto";
import {
  CollectionDto,
  DataMapDto,
  DisclosureDto,
  groupsFromPIC,
} from "../../../common/service/server/dto/DataMapDto";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import {
  OrganizationPublicId,
  RequestHandlingInstructionType,
  UUIDString,
} from "../../../common/service/server/Types";
import { Disableable } from "../../components";
import { Callout, CalloutVariant } from "../../components/Callout";
import { DataRecipientLogo } from "../../components/DataRecipientLogo";
import { HelpPopover } from "../../components/HelpPopover";
import { StandardLabel } from "../../components/input/Labels";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { PicGroupList } from "../../components/organisms/collection-groups/PicGroupList";
import { RequestInstructionsProgressFooter } from "../../components/requestInstructions";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { useOrganizationVendors, usePrimaryOrganization } from "../../hooks";
import { useDataMap } from "../../hooks/useDataMap";
import { useRequestInstructions } from "../../hooks/useOrganizationVendors";
import { Routes } from "../../root/routes";
import { alphabetically } from "../../surveys/util";
import { VENDOR_CATEGORY_AD_NETWORK } from "../../types/Vendor";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../get-compliant/data-recipients/shared";
import { GetCompliantContainer } from "../get-compliant/GetCompliantContainer";

const requestType = "KNOW" as RequestHandlingInstructionType;

interface HeaderProps {
  consumerGroups?: CollectionGroupDetailsDto[];
}
const KnowInstructionsHeader: React.FC<HeaderProps> = () => {
  const description = (
    <Paras>
      <Para>
        When you receive a Request to Know, you’ll send the requester all of the data you have about
        them, whether stored internally or in your vendor’s systems.
      </Para>
      <Para>
        Write instructions to help your team retrieve the requester’s personal data from each
        accessible system.
      </Para>
    </Paras>
  );

  return (
    <PageHeader
      titleHeaderContent="Request Instructions"
      titleContent="Manage Request to Know Settings"
      descriptionContent={description}
      wide={true}
    />
  );
};

export const KnowInstructionsPage = () => {
  // Constants

  // State
  const history = useHistory();
  const [instructions, setInstructions] = useState<null | RequestHandlingInstructionDto[]>(null);

  // Requests
  const [organization, orgRequest] = usePrimaryOrganization();
  const [vendors, vendorsRequest] = useOrganizationVendors(organization.id);
  const [dataMap, dataMapRequest] = useDataMap(organization.id);
  const [, instructionsRequest] = useRequestInstructions(
    organization.id,
    requestType,
    setInstructions,
  );

  // Tear this out soon, when moved to vendor field
  const { answers: sharingAnswers, surveyRequest: sharingSurveyRequest } = useSurvey(
    organization.id,
    SELLING_AND_SHARING_SURVEY_NAME,
  );

  const sortedVendors = useMemo(() => {
    return vendors?.sort(alphabetically)?.filter((v) => !v.isInstalled) ?? [];
  }, [vendors]);

  const onDone = () => {
    history.push(Routes.organization.requestHandling.base);
  };

  // Content
  return (
    <StandardPageWrapper
      requests={[
        orgRequest,
        vendorsRequest,
        instructionsRequest,
        dataMapRequest,
        sharingSurveyRequest,
      ]}
      footer={
        <RequestInstructionsProgressFooter
          nextContent="Save"
          nextDisabled={false}
          nextOnClick={onDone}
          currentRequest={null}
        />
      }
    >
      {organization && (
        <>
          <KnowInstructionsHeader />

          <GetCompliantContainer className="request-instructions" wide={true}>
            <VendorsPanel
              orgId={organization.id}
              vendors={sortedVendors}
              instructions={instructions}
              setInstructions={setInstructions}
              dataMap={dataMap}
              sharingAnswers={sharingAnswers}
            />
          </GetCompliantContainer>
        </>
      )}
    </StandardPageWrapper>
  );
};

const debouncedFn = debounce((fn, ...args) => fn(...args), 500);

// TODO: checkboxize, not the copied radio
const NotStoredCheckbox = ({ onChange, method, disabled }) => {
  const onCheckboxChange = (e) => {
    onChange(e.target.checked);
  };

  return (
    <>
      <FormControlLabel
        control={
          <Checkbox
            onChange={onCheckboxChange}
            checked={method == "INACCESSIBLE_OR_NOT_STORED"}
            value="INACCESSIBLE_OR_NOT_STORED"
            disabled={disabled}
          />
        }
        className={"inaccessible"}
        label="Data is inaccessible or not stored "
      />
      <HelpPopover label="Select if the system does not store data, or the data is inaccessible (e.g., routinely deleted within 30 days.)" />
    </>
  );
};

// TODO: everything below here should get probably moved to Components

const matchInstruction = (inst: RequestHandlingInstructionDto, orgId, vendorId, requestType) => {
  return (
    orgId === inst.organizationId && vendorId === inst.vendorId && requestType == inst.requestType
  );
};

const findInstruction = (
  instructions: null | RequestHandlingInstructionDto[],
  orgId: OrganizationPublicId,
  vendorId: UUIDString,
) => {
  return instructions?.find((inst) => matchInstruction(inst, orgId, vendorId, requestType));
};

type VendorCardProps = {
  orgId: any;
  instructions: null | RequestHandlingInstructionDto[];
  setInstructions: any;
  vendor: OrganizationDataRecipientDto;
  index: number; // Gross, stop using utility classes for styles
  disclosures: DisclosureDto[];
  collection: CollectionDto[];
  dataMap: DataMapDto;
  sharingAnswers: SurveyAnswers;
};

const VendorCard: React.FC<VendorCardProps> = ({
  orgId,
  vendor,
  instructions,
  setInstructions,
  index,
  disclosures,
  collection,
  dataMap,
  sharingAnswers,
}) => {
  const baseInstruction = {
    organizationId: orgId,
    requestType: requestType,
    vendorId: vendor.vendorId,
  } as RequestHandlingInstructionDto;
  const cgById = collection ? keyBy(dataMap.collection, (c) => c.collectionGroupId) : {};

  const disclosedPIC = useMemo(() => {
    return uniqBy(
      disclosures
        .filter((d) => cgById[d.collectionGroupId].collectionGroupType != "EMPLOYMENT")
        .flatMap((d) => d.disclosed.map((d) => dataMap.categories.find((pic) => pic.id == d))),
      (c) => c.id,
    );
  }, [disclosures]);
  const disclosedPicGroups = useMemo(() => groupsFromPIC(disclosedPIC), [disclosedPIC]);

  const foundInstruction = findInstruction(instructions, orgId, vendor.vendorId);

  // State
  const [instruction, setInstruction] = useState<RequestHandlingInstructionDto>(
    foundInstruction || baseInstruction,
  );

  useEffect(() => {
    if (instructions) {
      setInstruction(foundInstruction || baseInstruction);
    }
  }, [Boolean(instructions)]);

  const [instructionText, setInstructionText] = useState("");

  const updateInstruction = (newInstruction) => {
    setInstruction(newInstruction);

    // update the locally stored instructions
    const clonedInstructions = instructions ? [...instructions] : [];

    const idx = instructions?.indexOf(foundInstruction) ?? -1;

    if (idx >= 0) {
      clonedInstructions[idx] = newInstruction;
    } else {
      clonedInstructions.push(newInstruction);
    }

    setInstructions(clonedInstructions);

    updateRemoteInstruction(newInstruction);
  };

  // Network
  const { fetch: updateRemoteInstruction } = useActionRequest({
    api: (newInstruction) => Api.requestHandlingInstructions.putInstruction(orgId, newInstruction),
  });

  // Callbacks
  const setIsInaccessible = (isInaccessible: boolean) => {
    updateInstruction({
      ...instruction,
      processingMethod: isInaccessible ? "INACCESSIBLE_OR_NOT_STORED" : null,
    });
  };

  useEffect(() => {
    if (instruction?.processingInstructions) {
      setInstructionText(instruction.processingInstructions);
    }
  }, [instruction]);

  const handleTextChange = ({ target }) => {
    setInstructionText(target.value);
    debouncedFn(updateInstruction, { ...instruction, processingInstructions: target.value });
  };

  const vendorHeader = (
    <div className="vendor-instructions--container">
      <DataRecipientLogo dataRecipient={vendor} size={64} />
      <div className="vendor-instructions--vendor-info">
        <h4 className="intermediate my-0">{vendor.name}</h4>
        {vendor.ccpaRequestToKnowLink && (
          <div className="classify-vendors--classification-details">
            <ExternalLink
              href={vendor.ccpaRequestToKnowLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              {vendor.name}’s retrieval instructions
            </ExternalLink>
          </div>
        )}
      </div>
    </div>
  );

  const processingInstructions = (
    <>
      <StandardLabel label="Processing instructions" subLabel=" (optional)" />

      <TextField
        className="instructions--text-field"
        variant="outlined"
        value={instructionText}
        onChange={handleTextChange}
        fullWidth={false}
        multiline={true}
        rows={4}
      />
    </>
  );

  // This should be cleaned up to drive the customAudienceSelections from the backend, possibly from the interceptor to popuplate a field on OrgDataRecipient?
  const customAudienceSelections = useMemo(
    () => JSON.parse(sharingAnswers["custom-audience-feature"] || "[]"),
    [sharingAnswers],
  );

  const inUnselectedAdNetworks = useMemo(
    () =>
      vendor.category == VENDOR_CATEGORY_AD_NETWORK &&
      !customAudienceSelections.includes(vendor.id),
    [vendor.category, vendor.id, customAudienceSelections],
  );

  const instructionsVisible =
    instruction && instruction.processingMethod !== "INACCESSIBLE_OR_NOT_STORED";

  const notStoredDisabled =
    instruction?.processingMethod !== "INACCESSIBLE_OR_NOT_STORED" &&
    Boolean(vendor.accessInstructions) &&
    !inUnselectedAdNetworks;

  return (
    <div
      key={vendor.vendorId}
      className={clsx("paper-divider-bottom", { "py-xl": !!index, "pb-xl": !index })}
    >
      {vendorHeader}
      <div className="mb-md">
        <Disableable
          disabled={notStoredDisabled}
          disabledTooltip="Data is accessible via the instructions shown below."
        >
          <NotStoredCheckbox
            onChange={setIsInaccessible}
            method={instruction?.processingMethod}
            disabled={notStoredDisabled}
          />
        </Disableable>
      </div>
      {Boolean(disclosedPicGroups.length) && (
        <Callout
          variant={CalloutVariant.Clear}
          className="mb-md"
          cardContentClassName="pt-0"
          title={`Data stored in ${vendor.name}`}
        >
          <PicGroupList picGroups={disclosedPicGroups} />
        </Callout>
      )}
      {Boolean(vendor.accessInstructions) && (
        <Callout
          variant={CalloutVariant.Gray}
          title="Access Instructions"
          className={"mb-md"}
          __dangerousChildrenHtml={vendor.accessInstructions}
        />
      )}

      {instructionsVisible && processingInstructions}
    </div>
  );
};

type VendorsPanelProps = {
  orgId: any;
  vendors: OrganizationDataRecipientDto[];
  instructions: null | RequestHandlingInstructionDto[];
  setInstructions: any;
  dataMap: DataMapDto;
  sharingAnswers: SurveyAnswers;
};

const VendorsPanel: React.FC<VendorsPanelProps> = ({
  orgId,
  vendors,
  instructions,
  setInstructions,
  dataMap,
  sharingAnswers,
}) => {
  const disclosuresByVendor = useMemo(() => {
    if (!dataMap?.disclosure) {
      return {};
    }

    return groupBy(dataMap.disclosure, (d) => d.vendorId);
  }, [dataMap?.disclosure]);

  return (
    <div>
      {vendors.map((vendor, index) => {
        const disclosures = disclosuresByVendor[vendor.vendorId] || [];
        return (
          <VendorCard
            key={vendor.vendorId}
            orgId={orgId}
            vendor={vendor}
            instructions={instructions}
            setInstructions={setInstructions}
            index={index}
            dataMap={dataMap}
            disclosures={disclosures}
            collection={dataMap.collection}
            sharingAnswers={sharingAnswers}
          />
        );
      })}
    </div>
  );
};
