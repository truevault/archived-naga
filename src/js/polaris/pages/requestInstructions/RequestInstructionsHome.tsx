import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { PageLoading } from "../../../common/components/Loading";
import { NetworkRequest } from "../../../common/models";
import { ProgressEnum, RequestHandlingInstructionType } from "../../../common/service/server/Types";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { ActionRow, ActionRowsHeader } from "../../components/ActionRow";
import { PageHeader } from "../../components/layout/header/Header";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { usePrimaryOrganization } from "../../hooks";
import { Routes } from "../../root/routes";
import { GetCompliantContainer } from "../get-compliant/GetCompliantContainer";

const HomeHeader: React.FC = () => {
  const description = (
    <Paras>
      <Para>View and edit request instructions below.</Para>
      <Para>
        These instructions will be used to help your teammates process requests in the{" "}
        <Link to={Routes.requests.active}>Request Inbox</Link>
      </Para>
    </Paras>
  );

  return <PageHeader titleContent={"Request Instructions"} descriptionContent={description} />;
};

type RequestInstructionStatus = {
  label: string;
  type: "CONSUMER_GROUP" | RequestHandlingInstructionType;
  currentProgress: ProgressEnum;
  route: string;
};

export const RequestInstructionsHome = () => {
  // Constants

  // State
  const [requestTypeOptions, setRequestTypeOptions] = useState([...BASE_REQUEST_TYPE_OPTIONS]);
  const history = useHistory();
  const [readyToRender, setReadyToRender] = useState(false);

  // Requests
  const [organization, orgRequest] = usePrimaryOrganization();

  // Callbacks

  const onLinkClick = (option: RequestInstructionStatus) => {
    history.push(option.route);
  };

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest)) {
      setReadyToRender(true);
    }
  }, [orgRequest]);

  useEffect(() => {
    if (organization) {
      let options = [...BASE_REQUEST_TYPE_OPTIONS];
      if (!organization.doesOrgProcessOptOuts) {
        options = options.filter((o) => o.type != "OPT_OUT");
      }
      setRequestTypeOptions(options);
    }
  }, [organization]);

  // Content

  if (!readyToRender) {
    return (
      <StandardPageWrapper>
        <HomeHeader />
        <PageLoading />
      </StandardPageWrapper>
    );
  }

  return (
    <StandardPageWrapper>
      {organization && (
        <>
          <HomeHeader />

          <GetCompliantContainer className="request-instructions">
            <InstructionsTable
              label={"Your Instructions"}
              options={requestTypeOptions}
              onRowClick={onLinkClick}
            />
          </GetCompliantContainer>
        </>
      )}
    </StandardPageWrapper>
  );
};

type TableProps = {
  label: string;
  options: RequestInstructionStatus[];
  onRowClick: (option: RequestInstructionStatus) => void;
};

const InstructionsTable = ({ label, options, onRowClick }: TableProps) => {
  return (
    <>
      <ActionRowsHeader label={label} />
      {options.slice(0).map((option) => {
        return (
          <ActionRow
            key={`vendor-${option.type}`}
            label={option.label}
            onRowClick={() => onRowClick(option)}
          />
        );
      })}
    </>
  );
};

const BASE_REQUEST_TYPE_OPTIONS = [
  {
    label: "Manage Consumer Lookup Settings",
    type: "CONSUMER_GROUP",
    route: Routes.organization.requestHandling.consumerGroup,
  },
  {
    label: "Manage Request to Know Settings",
    type: "KNOW",
    route: Routes.organization.requestHandling.requestToKnow,
  },
  {
    label: "Manage Request to Delete Settings",
    type: "DELETE",
    route: Routes.organization.requestHandling.requestToDelete,
  },
  {
    label: "Manage Request to Opt-Out Settings",
    type: "OPT_OUT",
    route: Routes.organization.requestHandling.requestToOptOut,
  },
] as RequestInstructionStatus[];
