import React, { useMemo, useState } from "react";
import { useHistory } from "react-router-dom";
import { TabPanel } from "../../components";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { CollectionGroupDetailsDto } from "../../../common/service/server/dto/CollectionGroupDto";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import { RequestHandlingInstructionType } from "../../../common/service/server/Types";
import { Callout, CalloutVariant } from "../../components/Callout";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import {
  applyDataRecipientFilters,
  DataRecipientFilter,
  useDataRecipientFilter,
} from "../../components/molecules/vendor/DataRecipientFilter";
import { RequestInstructionsProgressFooter } from "../../components/requestInstructions";
import { DeletionInstructionsCard } from "../../components/requestInstructions/DeletionInstructionsCard";
import { useOrganizationVendors, usePrimaryOrganization } from "../../hooks";
import { useDataMap } from "../../hooks/useDataMap";
import { useRequestInstructions, useRetentionReasons } from "../../hooks/useOrganizationVendors";
import { TabOptions, useQueryTabs } from "../../hooks/useTabs";
import { Routes } from "../../root/routes";
import { alphabetically } from "../../surveys/util";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../get-compliant/data-recipients/shared";
import { GetCompliantContainer } from "../get-compliant/GetCompliantContainer";
import { getConsumerVendors, getEmploymentVendors } from "../../util/resources/vendorUtils";

const requestType = "DELETE" as RequestHandlingInstructionType;

const isFinished = (
  instructions: RequestHandlingInstructionDto[] | null,
  vendors: OrganizationDataRecipientDto[],
) => {
  const hasInstructions = Boolean(instructions?.length);

  if (!hasInstructions) {
    return false;
  }

  const relevantInstructions =
    instructions?.filter((i) => {
      const vendor = vendors.find((v) => v.vendorId == i.vendorId);
      return Boolean(vendor) && i.requestType == "DELETE";
    }) ?? [];

  const allInstructionsSelected = relevantInstructions?.every((i) => {
    const isRetainKind = i.processingMethod == "RETAIN";
    const hasRetentionReason = (i.retentionReasons?.length ?? 0) > 0;

    // Processing method must exist, and we must have a retention reason if we're a retain-ish
    // processing method.
    return hasRetentionReason || !isRetainKind;
  });

  return allInstructionsSelected;
};

interface HeaderProps {
  consumerGroups?: CollectionGroupDetailsDto[];
}

const DeleteInstructionsHeader: React.FC<HeaderProps> = () => {
  return (
    <PageHeader
      titleHeaderContent="Request Instructions"
      titleContent="Manage Request to Delete Settings"
      descriptionContent={
        <>
          <p>
            To simplify the process of handling requests to delete data, provide information and
            instructions for how to handle these requests by vendor.
          </p>
          <p>
            Providing information and instructions for how to handle requests for data deletion
            allows us to simplify the process for you.
          </p>
          <p>
            Different vendors will have different information they need to set up a request to
            delete process.
          </p>
        </>
      }
      wide={true}
    />
  );
};

type DeleteInstructionsTabOptions =
  | "consumer_vendors"
  | "employment_vendors"
  | "exceptions_to_deletion";

const TAB_OPTIONS: TabOptions<DeleteInstructionsTabOptions> = [
  {
    label: "Consumer Vendors",
    value: "consumer_vendors",
  },
  {
    label: "HR Vendors",
    value: "employment_vendors",
  },
  {
    label: "Exceptions to Deletion",
    value: "exceptions_to_deletion",
  },
];

export const DeleteInstructionsPage = () => {
  // Constants

  // State
  const history = useHistory();
  const [instructions, setInstructions] = useState<null | RequestHandlingInstructionDto[]>(null);

  // Requests
  const [organization, orgRequest] = usePrimaryOrganization();
  const [tab, tabs] = useQueryTabs<DeleteInstructionsTabOptions>(TAB_OPTIONS, "consumer_vendors");
  const [unfilteredVendors, vendorsRequest, fetchVendors] = useOrganizationVendors(organization.id);
  const [dataMap, dataMapRequest] = useDataMap(organization.id);

  const filters = useDataRecipientFilter();
  const vendors = useMemo(
    () => applyDataRecipientFilters(unfilteredVendors, filters.props.filters),
    [unfilteredVendors, filters],
  );

  // Tear this out soon, when moved to vendor field
  const { answers: sharingAnswers, surveyRequest: sharingSurveyRequest } = useSurvey(
    organization.id,
    SELLING_AND_SHARING_SURVEY_NAME,
  );

  const [retentionReasons, retentionReasonsRequest] = useRetentionReasons();
  const [, instructionsRequest, fetchInstructions] = useRequestInstructions(
    organization.id,
    requestType,
    (data: RequestHandlingInstructionDto[]) => {
      setInstructions(data);
    },
  );

  // Calculated State
  const sortedVendors = useMemo(() => {
    return vendors?.sort(alphabetically)?.filter((v) => !v.isInstalled) ?? [];
  }, [vendors]);

  const consumerVendors = useMemo(() => {
    return getConsumerVendors(vendors, dataMap);
  }, [dataMap, vendors]);

  const employmentVendors = useMemo(() => {
    return getEmploymentVendors(vendors, dataMap);
  }, [dataMap, vendors]);

  const vendorsWithExceptionsToDeletion = useMemo(() => {
    return sortedVendors?.filter((v) =>
      v.instructions.some((i) => i.requestType == "DELETE" && i.processingMethod === "RETAIN"),
    );
  }, [sortedVendors]);

  const nextDisabled = !isFinished(instructions, vendors);

  // Callbacks
  const onDone = () => {
    history.push(Routes.organization.requestHandling.base);
  };

  const makeDataCard = (cardVendors: OrganizationDataRecipientDto[]) => {
    return (
      <>
        <DataRecipientFilter {...filters.props} />
        <GetCompliantContainer className="request-instructions" wide={true}>
          <hr />
          {cardVendors &&
            cardVendors.map((v, idx) => {
              {
                return (
                  <DeletionInstructionsCard
                    key={v.vendorId}
                    index={idx}
                    vendor={v}
                    dataMap={dataMap}
                    orgId={organization.id}
                    fetchVendors={fetchVendors}
                    instructions={instructions}
                    fetchInstructions={fetchInstructions}
                    setInstructions={setInstructions}
                    reasons={retentionReasons || []}
                    sharingAnswers={sharingAnswers}
                  />
                );
              }
            })}
        </GetCompliantContainer>
      </>
    );
  };

  return (
    <StandardPageWrapper
      requests={[
        instructionsRequest,
        orgRequest,
        vendorsRequest,
        retentionReasonsRequest,
        sharingSurveyRequest,
        dataMapRequest,
      ]}
      footer={
        <RequestInstructionsProgressFooter
          nextContent="Save"
          nextDisabled={nextDisabled}
          nextOnClick={onDone}
          currentRequest={null}
        />
      }
    >
      {organization && (
        <>
          <DeleteInstructionsHeader />

          <Callout variant={CalloutVariant.Purple}>
            <div className="mx-lg">
              <h4 className="intermediate">
                <img
                  src="/assets/images/deletion/self-service-deletion-icon.svg"
                  className="service-label__service-icon"
                />{" "}
                Self-Service Deletion
              </h4>

              <p>
                According to your Data Map, you have access to data stored with these vendors. If
                this needs updating, and data in these systems is inaccessible or not stored, you
                can change the vendor’s settings in your <a href={Routes.dataMap.root}>Data Map</a>.
              </p>
              <p>We’ve added deletion instructions for many commonly-used vendors.</p>
              <p>
                Add your own processing notes to help your team with the vendors that do not have
                deletion instructions. Notes added here will show up as instructions in the Request
                Inbox.
              </p>

              <h4 className="intermediate">
                <img
                  src="/assets/images/deletion/contact-deletion-icon.svg"
                  className="service-label__service-icon"
                />{" "}
                Deletion Handled by Contacting Vendor
              </h4>

              <p>
                <strong>Service Providers & Contractors</strong>
              </p>
              <p>
                These vendors are required to allow you to delete data. Some of these vendors may
                allow for self-service deletion, but if not, you must contact them to delete it for
                you.
              </p>

              <p>
                <strong>Third Parties</strong>
              </p>
              <p>Third Parties are not required to process deletion request on your behalf.</p>
              <p>
                If you are able to contact a vendor with deletion requests, enter their contact
                information. If you are unable to contact them, provide an explanation of why they
                aren’t able to be reached.
              </p>
            </div>
          </Callout>
          <div className="mt-md">{tabs}</div>
          <TabPanel tab={tab} value="consumer_vendors" className="py-md">
            {makeDataCard(consumerVendors)}
          </TabPanel>

          <TabPanel tab={tab} value="employment_vendors" className="py-md">
            {makeDataCard(employmentVendors)}
          </TabPanel>

          <TabPanel tab={tab} value="exceptions_to_deletion" className="py-md">
            {makeDataCard(vendorsWithExceptionsToDeletion)}
          </TabPanel>
        </>
      )}
    </StandardPageWrapper>
  );
};
