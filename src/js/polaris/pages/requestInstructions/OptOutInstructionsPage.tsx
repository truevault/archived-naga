import { TextField } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { useHistory } from "react-router-dom";
import { PageLoading } from "../../../common/components/Loading";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { StandardLabel } from "../../components/input/Labels";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { RequestInstructionsProgressFooter } from "../../components/requestInstructions";
import { Para } from "../../components/typography/Para";
import { Paras } from "../../components/typography/Paras";
import { useOrganizationVendors, usePrimaryOrganization } from "../../hooks";
import { Routes } from "../../root/routes";
import { GetCompliantContainer } from "../get-compliant/GetCompliantContainer";

const OptOutInstructionsHeader: React.FC = () => {
  const description = (
    <Paras>
      <Para>
        When you receive a Request to Opt-Out, you’ll need to remove the requester from any data
        sharing or selling with third parties.
      </Para>
      <Para>Write instructions for your team to use when processing a Request to Opt-Out.</Para>
    </Paras>
  );

  return (
    <PageHeader
      titleHeaderContent="Request Instructions"
      titleContent="Manage Request to Opt-Out Settings"
      descriptionContent={description}
    />
  );
};

export const OptOutInstructionsPage = () => {
  // Constants

  // State
  const history = useHistory();
  const [readyToRender, setReadyToRender] = useState(false);
  const [instructionsText, setInstructionsText] = useState("");

  // Requests
  const [organization, orgRequest] = usePrimaryOrganization();
  const [allVendors, vendorsRequest] = useOrganizationVendors(organization.id);

  const vendors = useMemo(
    () => allVendors.filter((v) => v.isUploadVendor || v.usesCustomAudience),
    [allVendors],
  );

  const sellingAndSharingVendors = vendors?.map((v) => v.name);
  const optOutVendors = sellingAndSharingVendors?.join(", ") || "";

  const { fetch: updateInstructions, request: updateInstructionsRequest } = useActionRequest({
    api: () =>
      Api.requestHandlingInstructions.updateOptOutInstructions({
        organizationId: organization.id,
        cgRequestHandlingInstructions: instructionsText,
      }),
  });

  const { result: fetchedInstructions, request: instructionsRequest } = useDataRequest({
    queryKey: ["fetchedOptOutInstructions", organization.id],
    api: () => Api.requestHandlingInstructions.getOptOutInstructions(organization.id),
  });

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest, instructionsRequest, vendorsRequest)) {
      setReadyToRender(true);
    }
  }, [orgRequest, instructionsRequest, vendorsRequest]);

  useEffect(() => {
    if (instructionsRequest.success) {
      setInstructionsText(fetchedInstructions);
    }
  }, [fetchedInstructions, instructionsRequest]);

  // Callbacks
  const onDone = async () => {
    await updateInstructions();

    history.push(Routes.organization.requestHandling.base);
  };

  // Content

  if (!readyToRender) {
    return (
      <StandardPageWrapper>
        <OptOutInstructionsHeader />
        <PageLoading />
      </StandardPageWrapper>
    );
  }

  const helpText = optOutVendors
    ? `Your business’s opt-out parties: ${optOutVendors}`
    : "Your business has no opt-out parties.";

  return (
    <StandardPageWrapper
      footer={
        <RequestInstructionsProgressFooter
          nextContent="Save"
          nextDisabled={false}
          nextOnClick={onDone}
          currentRequest={updateInstructionsRequest}
        />
      }
    >
      {organization && (
        <>
          <OptOutInstructionsHeader />

          <GetCompliantContainer className="request-instructions" wide={true}>
            <StandardLabel
              label="Processing instructions"
              subLabel=" (optional)"
              helpText={helpText}
            />

            <TextField
              className="instructions--text-field"
              variant="outlined"
              value={instructionsText}
              onChange={({ target }) => setInstructionsText(target.value)}
              fullWidth={false}
              multiline={true}
              rows={8}
            />
          </GetCompliantContainer>
        </>
      )}
    </StandardPageWrapper>
  );
};
