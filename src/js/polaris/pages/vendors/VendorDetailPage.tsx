import React, { useEffect, useState } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { PageHeader } from "../../../common/layout/PageHeader";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import {
  DataInventorySnapshotDto,
  SnapshotCategoryDto,
} from "../../../common/service/server/dto/PersonalInformationOptionsDto";
import { UUIDString } from "../../../common/service/server/Types";
import { VendorSettings } from "../../components/forms";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import {
  Content as VendorContent,
  Panel as VendorPanel,
} from "../../components/vendor/VendorCatalogPanel";
import { DetailsHeader } from "../../components/vendor/VendorDetailsHeader";
import { Error as RequestsError } from "../../copy/requests";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { Routes } from "../../root/routes";
import { flatMap } from "../../util";
import { VendorsSubnav } from "./VendorsSubnav";

type MatchParams = {
  vendorId: UUIDString;
};

type Props = RouteComponentProps<MatchParams>;

export const Detail = ({ match }: Props) => {
  const vendorId = match.params.vendorId;

  const history = useHistory();

  const primaryOrgId = usePrimaryOrganizationId();

  const [everRendered, setEverRendered] = useState(false);

  const [orgVendors, vendorsRequest] = useOrganizationVendors(primaryOrgId);

  const [vendorDisclosedTo, setVendorDisclosedTo] = useState(false);
  const [vendorSoldTo, setVendorSoldTo] = useState(false);

  const { request: editDataRequest, result: editData } = useDataRequest({
    queryKey: ["editData", primaryOrgId, vendorId],
    api: () => Api.organizationVendor.getEditData(primaryOrgId, vendorId),
    messages: {
      forceError: RequestsError.GetVendors,
    },
  });

  const { result: snapshotData, request: snapshotRequest } = useDataRequest({
    queryKey: ["snapshotData", primaryOrgId],
    api: () => Api.dataInventory.getSnapshot(primaryOrgId),
  });

  const { fetch: updateVendorSettings } = useActionRequest({
    api: (values) => Api.organizationVendor.addOrUpdateVendor(primaryOrgId, vendorId, values),
    messages: {
      forceError: RequestsError.UpdateVendor,
    },
    onSuccess: () => {
      history.push(Routes.vendors.root);
    },
  });

  const handleSubmit = (values) => {
    updateVendorSettings(values);
  };

  // trigger that we've ever rendered
  useEffect(() => {
    if (NetworkRequest.areFinished(editDataRequest, vendorsRequest, snapshotRequest)) {
      setEverRendered(true);
      setVendorDisclosedTo(
        // @ts-ignore
        flatMap(snapshotData, (sd: DataInventorySnapshotDto) =>
          flatMap(
            sd.personalInformationCategories,
            (c: SnapshotCategoryDto) => c.vendorsSharedWith,
          ),
        ).includes(vendorId),
      );
      setVendorSoldTo(
        // @ts-ignore
        flatMap(snapshotData, (sd: DataInventorySnapshotDto) =>
          flatMap(sd.personalInformationCategories, (c: SnapshotCategoryDto) => c.vendorsSoldTo),
        ).includes(vendorId),
      );
    }
  }, [vendorId, snapshotData, editDataRequest, vendorsRequest, snapshotRequest]);

  // Page Load
  if (!everRendered) {
    return <LoadingPageWrapper navContent={<VendorsSubnav />} />;
  }

  return (
    <StandardPageWrapper navContent={<VendorsSubnav />}>
      <PageHeader header="Edit Vendor" />

      <VendorPanel>
        {!editData.vendor?.isCustom && <DetailsHeader vendor={editData.vendor} />}
        <VendorContent padded={false}>
          <VendorSettings
            organizationId={primaryOrgId}
            existingOrganizationVendors={orgVendors}
            data={editData}
            vendorDisclosedTo={vendorDisclosedTo}
            vendorSoldTo={vendorSoldTo}
            onSubmit={handleSubmit}
          />
        </VendorContent>
      </VendorPanel>
    </StandardPageWrapper>
  );
};
