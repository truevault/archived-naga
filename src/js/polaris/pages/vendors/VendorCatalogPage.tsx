import { Button, Card, Grid, TextField } from "@mui/material";
import lunr from "lunr";
import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { useActionRequest } from "../../../common/hooks/api";
import { PageHeader } from "../../../common/layout/PageHeader";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { OrganizationVendorSettingsDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import {
  Content as VendorContent,
  Panel as VendorPanel,
  Sidebar as VendorSidebar,
} from "../../components/vendor/VendorCatalogPanel";
import { Error as RequestsError } from "../../copy/requests";
import {
  useDataRecipientsCatalog,
  useOrganizationVendors,
  usePrimaryOrganizationId,
} from "../../hooks";
import { RouteHelpers, Routes } from "../../root/routes";
import { sortedOrganizationVendors } from "../../util/vendors";
import { VendorsSubnav } from "./VendorsSubnav";

export const Catalog = () => {
  const primaryOrgId = usePrimaryOrganizationId();

  const [currentVendors, vendorsRequest] = useOrganizationVendors(primaryOrgId);

  const [allVendors, allVendorsRequest] = useDataRecipientsCatalog(primaryOrgId);

  const [everRendered, setEverRendered] = useState(false);

  const [allVendorsMap, setAllVendorsMap] = useState(null);

  const [vendorsSearchIdx, setVendorsSearchIdx] = useState(null);

  const [addVendorId, setAddVendorId] = useState(null);

  const history = useHistory();

  const { fetch: addVendor, request: addVendorRequest } = useActionRequest({
    api: (vendorId) =>
      Api.organizationVendor.addOrUpdateVendor(
        primaryOrgId,
        vendorId,
        {} as OrganizationVendorSettingsDto,
      ),
    messages: {
      forceError: RequestsError.AddVendor,
    },
    onSuccess: () => {
      setAddVendorId(null);
      history.push(RouteHelpers.vendors.detail(addVendorId));
    },
  });

  // trigger that we've ever rendered
  useEffect(() => {
    if (NetworkRequest.areFinished(vendorsRequest, allVendorsRequest)) {
      setAllVendorsMap(new Map(allVendors.map((s) => [s.id, s])));
      setVendorsSearchIdx(
        lunr(function () {
          this.pipeline.remove(lunr.stemmer);
          this.searchPipeline.remove(lunr.stemmer);
          this.ref("id");
          this.field("name", { boost: 3 });
          this.field("aliases", { boost: 3 });
          this.field("category", { boost: 1 });
          allVendors.forEach((s) => this.add(s));
        }),
      );
      setEverRendered(true);
    }
  }, [allVendors, vendorsRequest, allVendorsRequest]);

  useEffect(() => {
    if (addVendorId) {
      addVendor(addVendorId);
    }
  }, [addVendor, addVendorId]);

  // Page Load
  if (!everRendered) {
    return <LoadingPageWrapper navContent={<VendorsSubnav />} />;
  }

  return (
    <StandardPageWrapper navContent={<VendorsSubnav />}>
      <PageHeader header="Add Vendor" />

      <VendorPanel row classes="service-catalog-panel">
        <VendorsSidebar vendors={currentVendors} />
        <VendorsContent
          vendorsSearchIdx={vendorsSearchIdx}
          allVendorsMap={allVendorsMap}
          currentVendors={currentVendors}
          addVendorId={addVendorId}
          setAddVendorId={setAddVendorId}
          addVendorRequest={addVendorRequest}
        />
      </VendorPanel>
    </StandardPageWrapper>
  );
};

const VendorsSidebar = ({ vendors }) => {
  const sortedVendors = sortedOrganizationVendors(vendors);

  return (
    <>
      {sortedVendors.length > 0 && (
        <VendorSidebar>
          <p className="mt-0">Vendors already added:</p>

          {sortedVendors.map((vendor) => (
            <p className="mt-xs mb-sm" key={vendor.url}>
              <img src={vendor.logoUrl} className="service-icon" />
              {vendor.name}
            </p>
          ))}
        </VendorSidebar>
      )}
    </>
  );
};

const VendorsContent = ({
  vendorsSearchIdx,
  allVendorsMap,
  currentVendors,
  addVendorId,
  setAddVendorId,
  addVendorRequest,
}) => {
  const [search, setSearch] = useState("");
  const searching = search.length >= 2;
  const term = search
    .replace(/[+*:;]/, "")
    .replace("-", " ")
    .trim()
    .split(" ")
    .map((s) => `+${s}*`)
    .join(" ");

  const allSearch = !searching
    ? []
    : vendorsSearchIdx.search(`${term}*`).map(({ ref }) => allVendorsMap.get(ref));

  return (
    <VendorContent>
      <TextField
        id="outlined-basic"
        variant="outlined"
        placeholder="Start typing to search vendors by name"
        fullWidth
        autoFocus
        value={search}
        onChange={(e) => {
          setSearch(e.target.value);
        }}
        autoComplete="off"
        autoCorrect="off"
        autoCapitalize="off"
        spellCheck="false"
      />

      <Grid container spacing={2} className="service-tiles w-100 mt-md">
        {searching ? (
          <>
            {allSearch.map((s) => {
              const alreadyAdded = Boolean(
                currentVendors.find((c) => {
                  return c.vendorId == s.id;
                }),
              );
              return (
                <Grid item xs={4} key={s.id}>
                  <VendorTile
                    vendor={s}
                    canAdd={!alreadyAdded}
                    addVendorId={addVendorId}
                    setAddVendorId={setAddVendorId}
                    addVendorRequest={addVendorRequest}
                  />
                </Grid>
              );
            })}
            {searching && allSearch.length >= 1 && (
              <Grid item xs={4} key="add-custom-vendor-tile">
                <AddVendorTile
                  imageUrl="/assets/images/icon-vendors-search-map.svg"
                  bodyText="Can't find a vendor?"
                  buttonText="Add Custom Vendor"
                />
              </Grid>
            )}
            {searching && allSearch.length == 0 && (
              <Grid item xs={4} key="empty-results-tile">
                <AddVendorTile
                  imageUrl="/assets/images/icon-vendors-search-map.svg"
                  bodyText="The vendor you are looking for is not in our catalog."
                  buttonText="Add Custom Vendor"
                />
              </Grid>
            )}
          </>
        ) : (
          <Grid item xs={4} key="empty-results-tile">
            <AddVendorTile
              imageUrl="/assets/images/icon-vendors-black.svg"
              bodyText="Want to add an uncommon vendor, contractor, or business partner?"
              buttonText="Add Custom Vendor"
            />
          </Grid>
        )}
      </Grid>
    </VendorContent>
  );
};

const AddVendorTile = ({ imageUrl, bodyText, buttonText }) => {
  return (
    <Card variant="outlined" className="service-tile">
      <div className="service-tile--logo">
        <img src={imageUrl} />
      </div>

      <div className="flex-grow"></div>

      <p className="mt-0 ml-md mr-md text-center">
        <span className="text-body-2">{bodyText}</span>
      </p>

      <div className="flex-grow"></div>

      <Button
        className="mb-md"
        variant="contained"
        color="primary"
        component={Link}
        to={Routes.vendors.new}
      >
        {buttonText}
      </Button>
    </Card>
  );
};

const VendorTile = ({ vendor, canAdd, addVendorId, setAddVendorId, addVendorRequest }) => {
  return (
    <Card variant="outlined" className="service-tile">
      <div className="service-tile--logo">
        <img src={vendor.logoUrl} />
      </div>

      <p className="mt-0 text-center">
        <span className="text-body-1-bo.d">{vendor.name}</span>
        <br />
        <span className="text-body-2">{vendor.category || <span>&nbsp;</span>}</span>
        <br />
        <span className="text-body-2">
          <a color="primary" href={vendor.url}>
            website
          </a>
        </span>
      </p>

      <div className="flex-grow"></div>

      {canAdd ? (
        <LoadingButton
          className="mb-md"
          color="primary"
          loading={addVendorRequest.running && addVendorId === vendor.id}
          onClick={() => setAddVendorId(vendor.id)}
        >
          Add
        </LoadingButton>
      ) : (
        <Button disabled className="mb-md already-added">
          Already Added
        </Button>
      )}
    </Card>
  );
};
