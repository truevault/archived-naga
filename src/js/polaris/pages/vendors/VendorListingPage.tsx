import { Error } from "@mui/icons-material";
import { Button, Paper } from "@mui/material";
import clsx from "clsx";
import qs from "query-string";
import React, { useEffect, useState } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { OverflowTooltip } from "../../../common/components/OverflowTooltip";
import { TileTable } from "../../../common/components/TileTable";
import { useActionRequest } from "../../../common/hooks/api";
import { PageHeader } from "../../../common/layout/PageHeader";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { ConfirmDialog } from "../../components/dialog/ConfirmDialog";
import { EmptyState } from "../../components/EmptyState";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { Error as RequestsError } from "../../copy/requests";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../hooks";
import { RouteHelpers, Routes } from "../../root/routes";
import {
  organizationVendorIsComplete,
  organizationVendorIsInProgress,
  sortedOrganizationVendors,
} from "../../util/vendors";
import { VendorsSubnav } from "./VendorsSubnav";

export const Listing = () => {
  const primaryOrgId = usePrimaryOrganizationId();

  const [orgVendors, vendorsRequest, refreshOrgVendors] = useOrganizationVendors(primaryOrgId);

  const [dataOrgVendors, setDataOrgVendors] = useState([]);

  const [everRendered, setEverRendered] = useState(false);

  const [numInProgress, setNumInProgress] = useState(0);

  const [removeInProgressId, setRemoveInProgressId] = useState("");

  const history = useHistory();

  const location = useLocation();
  const parsed = qs.parse(location.search);
  const showInProgress = parsed.inProgress === "true";

  const { fetch: removeOrgVendor, request: removeOrgVendorRequest } = useActionRequest({
    api: () => {
      return Api.organizationVendor.removeVendor(primaryOrgId, removeInProgressId);
    },
    messages: {
      forceError: RequestsError.RemoveVendor,
      success: null,
    },
    onError: () => {
      setRemoveInProgressId("");
    },
    onSuccess: () => {
      setRemoveInProgressId("");
      setEverRendered(false);
      refreshOrgVendors();
    },
  });

  // trigger that we've ever rendered
  useEffect(() => {
    if (NetworkRequest.areFinished(vendorsRequest)) {
      setNumInProgress(orgVendors.filter(organizationVendorIsInProgress).length);
      const _vendors = orgVendors.filter((ov) =>
        showInProgress ? organizationVendorIsInProgress(ov) : organizationVendorIsComplete(ov),
      );
      setDataOrgVendors(_vendors);
      if (showInProgress && _vendors.length == 0) {
        history.push(Routes.vendors.root);
      }
      setEverRendered(true);
    }
  }, [vendorsRequest, showInProgress, history, orgVendors]);

  // Page Load
  if (!everRendered) {
    return <LoadingPageWrapper navContent={<VendorsSubnav />} />;
  }

  let tableBody;
  if (dataOrgVendors.length) {
    const table = {
      columns: columns({ removeOrgVendor: setRemoveInProgressId, removeInProgressId }),
      data: sortedOrganizationVendors(dataOrgVendors) || [],
    };
    tableBody = <TileTable table={table} />;
  } else {
    tableBody = (
      <EmptyState
        image="/assets/images/icon-vendors-empty.svg"
        message={
          numInProgress ? "You have no completed vendors" : "Go ahead, add your first Vendor"
        }
      />
    );
  }

  return (
    <StandardPageWrapper navContent={<VendorsSubnav />}>
      <PageHeader header={`${showInProgress ? "In-Progress" : "Active"} Vendors`}>
        <Button variant="contained" color="primary" component={Link} to={Routes.vendors.catalog}>
          Add Vendor
        </Button>
      </PageHeader>

      {!showInProgress && numInProgress ? (
        <Paper className="mt-0 mb-xl p-md flex-container flex--align-center">
          <Error className="service-in-progress-warning" />
          <span className="ml-md">
            You have {numInProgress} in-progress vendor{numInProgress > 1 && "s"} to complete
          </span>
          <Button
            className="ml-md"
            color="primary"
            component={Link}
            to={Routes.vendors.root + "?inProgress=true"}
          >
            Resume
          </Button>
        </Paper>
      ) : (
        <div className="mt-lg mb-lg" />
      )}

      {tableBody}
      <ConfirmDialog
        open={!!removeInProgressId}
        onConfirm={() => removeOrgVendor(null)}
        onClose={() => setRemoveInProgressId("")}
        confirmTitle="Remove Vendor?"
        confirmText="Yes"
        contentText={
          "Removing this vendor will remove any mapping and instructions involving " +
          "this vendor. Your CCPA Disclosures will update automatically. Are you sure " +
          "you'd like to remove it?"
        }
        confirmLoading={removeOrgVendorRequest.running}
      />
    </StandardPageWrapper>
  );
};

type VendorCellProps = { data: OrganizationDataRecipientDto };
const VendorIdComponent = ({ data }: VendorCellProps) => {
  return (
    <div className="service-label">
      <Link to={RouteHelpers.vendors.detail(data.vendorId)}>
        <img
          src={data.logoUrl || "/assets/images/icon-vendors-black.svg"}
          className="service-label__service-icon"
        />

        <OverflowTooltip title={data.name} placement="top" cls="service-label__service-name">
          <span>{data.name}</span>
        </OverflowTooltip>
      </Link>
    </div>
  );
};

const VendorClassificationComponent = ({ data }: VendorCellProps) => {
  return data.classification ? (
    <span title={data.classification?.tooltip}>{data.classification?.name}</span>
  ) : (
    <VendorStatusComponent data={data} />
  );
};

const VendorCategoryComponent = ({ data }: VendorCellProps) => {
  return (
    <OverflowTooltip title={data.category} placement="top" cls="service-label__service-category">
      <span>{data.category}</span>
    </OverflowTooltip>
  );
};

const VendorStatusComponent = ({ data }: VendorCellProps) => {
  return (
    <span className={clsx({ "text-warning": data.status === "In-Progress" })}>{data.status}</span>
  );
};

const VendorActionsComponent = (removeOrgVendor, removeInProgressId) =>
  function VendorActionsComponentCore({ data }: VendorCellProps) {
    return (
      <div className="w-100 text-right">
        <LoadingButton
          variant="text"
          color="primary"
          onClick={() => removeOrgVendor(data.vendorId)}
          loading={removeInProgressId === data.vendorId}
          disabled={!!removeInProgressId}
        >
          Remove
        </LoadingButton>

        <Button
          className="ml-sm"
          variant="text"
          color="primary"
          href={RouteHelpers.vendors.detail(data.vendorId)}
        >
          Edit
        </Button>
      </div>
    );
  };

const columns = ({ removeOrgVendor, removeInProgressId }) => {
  return [
    {
      width: "27%",
      heading: "Name",
      component: VendorIdComponent,
    },
    {
      width: "25%",
      heading: "Classification",
      component: VendorClassificationComponent,
    },
    {
      width: "28%",
      heading: "Category",
      component: VendorCategoryComponent,
    },
    {
      width: "20%",
      heading: "",
      component: VendorActionsComponent(removeOrgVendor, removeInProgressId),
    },
  ];
};
