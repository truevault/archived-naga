import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import {
  Content as VendorContent,
  Panel as VendorPanel,
} from "../../components/vendor/VendorCatalogPanel";
import { PageHeader } from "../../../common/layout/PageHeader";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { useDataRequest, useActionRequest } from "../../../common/hooks/api";
import { useOrganizationVendors } from "../../hooks/useOrganizationVendors";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { NetworkRequest } from "../../../common/models";
import { Routes } from "../../root/routes";
import { Api } from "../../../common/service/Api";
import { VendorsSubnav } from "./VendorsSubnav";
import { VendorSettings } from "../../components/forms";
import { Error as RequestsError } from "../../copy/requests";

export const New = () => {
  const history = useHistory();

  const primaryOrgId = usePrimaryOrganizationId();

  const [everRendered, setEverRendered] = useState(false);

  const [orgVendors, vendorsRequest] = useOrganizationVendors(primaryOrgId);

  const { request: newDataRequest, result: newData } = useDataRequest({
    queryKey: ["newDataRequest", primaryOrgId],
    api: () => Api.organizationVendor.getNewData(primaryOrgId),
    messages: {
      forceError: RequestsError.GetVendors,
    },
  });

  const { fetch: updateVendorSettings } = useActionRequest({
    api: (values) => Api.organizationVendor.addCustomVendor(primaryOrgId, values),
    messages: {
      forceError: RequestsError.UpdateVendor,
    },
    onSuccess: () => {
      history.push(Routes.vendors.root);
    },
  });

  const handleSubmit = (values) => {
    updateVendorSettings(values);
  };

  // trigger that we've ever rendered
  useEffect(() => {
    if (NetworkRequest.areFinished(newDataRequest, vendorsRequest)) {
      setEverRendered(true);
    }
  }, [newDataRequest, vendorsRequest]);

  // Page Load
  if (!everRendered) {
    return <LoadingPageWrapper navContent={<VendorsSubnav />} />;
  }

  return (
    <StandardPageWrapper navContent={<VendorsSubnav />}>
      <PageHeader header="Add Vendor" />

      <VendorPanel>
        <VendorContent padded={false}>
          <VendorSettings
            organizationId={primaryOrgId}
            existingOrganizationVendors={orgVendors}
            data={newData}
            onSubmit={handleSubmit}
          />
        </VendorContent>
      </VendorPanel>
    </StandardPageWrapper>
  );
};
