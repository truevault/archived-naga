import React from "react";

import { NavLink } from "react-router-dom";
import { Routes } from "../../root/routes";

import { variables } from "../../../common/root/variables";

export const VendorsSubnav = () => {
  return (
    <>
      <h2>Vendors</h2>

      <ul>
        <li>
          <NavLink to={Routes.vendors.root} exact activeStyle={{ color: variables.colors.primary }}>
            Active Vendors
          </NavLink>
        </li>
      </ul>
    </>
  );
};
