import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useActionRequest } from "../../../common/hooks/api";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { PageHeader } from "../../components/layout/header/Header";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { Error as OrganizationError } from "../../copy/organization";
import { useOrganization, usePrimaryOrganization } from "../../hooks";
import { BUSINESS_SURVEY_NAME } from "../../surveys/businessSurvey";
import { CONSUMER_INCENTIVES_SURVEY_NAME } from "../../surveys/consumerCollectionIncentivesSurvey";
import { EEA_UK_SURVEY_NAME } from "../../surveys/eeaUkSurvey";
import { OrganizationSettingsView } from "./OrganizationSettingsView";
import { SettingsTabs } from "./SettingsTabs";

export const OrganizationSettings = () => {
  const [org] = usePrimaryOrganization();
  const organizationId = org.id;

  const [organization, orgRequest, orgRefresh] = useOrganization(organizationId);

  const { answers: businessSurveyAnswers, surveyRequest: businessSurveyAnswersRequest } = useSurvey(
    organizationId,
    BUSINESS_SURVEY_NAME,
  );

  const {
    answers: financialIncentiveSurveyAnswers,
    surveyRequest: financialIncentiveSurveyAnswersRequest,
  } = useSurvey(organizationId, CONSUMER_INCENTIVES_SURVEY_NAME);

  const { answers: gdprSurveyAnswers, surveyRequest: gdprSurveyAnswersRequest } = useSurvey(
    organizationId,
    EEA_UK_SURVEY_NAME,
  );

  const [everRendered, setEverRendered] = useState(false);

  const updateOrganizationApi = useCallback(
    (submitValues) => Api.organization.updateOrganization(organizationId, submitValues),
    [organizationId],
  );

  const updateOrganizationMessages = useMemo(
    () => ({
      forceError: OrganizationError.UpdateOrganization,
    }),
    [],
  );

  const { fetch: updateOrganization } = useActionRequest({
    api: updateOrganizationApi,
    messages: updateOrganizationMessages,
    onSuccess: orgRefresh,
  });

  useEffect(() => {
    if (
      NetworkRequest.areFinished(
        orgRequest,
        businessSurveyAnswersRequest,
        financialIncentiveSurveyAnswersRequest,
        gdprSurveyAnswersRequest,
      )
    ) {
      setEverRendered(true);
    }
  }, [
    orgRequest,
    businessSurveyAnswersRequest,
    financialIncentiveSurveyAnswersRequest,
    gdprSurveyAnswersRequest,
  ]);

  const surveyQuestions = useMemo(() => {
    if (everRendered) {
      if (org.featureGdpr) {
        return {
          ...businessSurveyAnswers,
          ...gdprSurveyAnswers,
          ...financialIncentiveSurveyAnswers,
        };
      }
      return { ...businessSurveyAnswers, ...financialIncentiveSurveyAnswers };
    } else {
      return null;
    }
  }, [
    org.featureGdpr,
    gdprSurveyAnswers,
    businessSurveyAnswers,
    financialIncentiveSurveyAnswers,
    everRendered,
  ]);

  const handleFormSubmit = (values) => {
    updateOrganization(Object.assign({}, organization, values));
  };

  // Page Load
  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  // Page Content
  return (
    <StandardPageWrapper>
      <div className="max-width-896">
        <PageHeader titleContent="Settings" />
        <SettingsTabs current="settings" />
        <OrganizationSettingsView
          organization={organization}
          updateOrganization={handleFormSubmit}
          surveyAnswers={surveyQuestions}
        />
      </div>
    </StandardPageWrapper>
  );
};
