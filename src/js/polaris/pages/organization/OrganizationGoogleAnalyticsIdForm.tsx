import { Alert, FormLabel } from "@mui/material";
import React from "react";
import { Form } from "react-final-form";
import { Text } from "../../../common/components/input/Text";
import { UpdateOrganizationGoogleAnalyticsWebPropertyDto } from "../../../common/service/server/controller/OrganizationController";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../common/service/server/dto/OrganizationDto";
import { DataRecipientLogo } from "../../components/DataRecipientLogo";

type OrganizationGoogleAnalyticsIdFormProps = {
  organization: OrganizationDto;
  googleAnalytics: OrganizationDataRecipientDto;
  onUpdateGAPropertyId: (values: UpdateOrganizationGoogleAnalyticsWebPropertyDto) => void;
};

const formValidation = ({ webProperty }) => {
  const errors = {} as any;

  if (webProperty && webProperty.startsWith("UA-")) {
    errors.webProperty = "Universal Analytics IDs are no longer supported.";
  }

  if (webProperty && !/^\d{9}$/.test(webProperty)) {
    errors.webProperty = "Your Google Analytics Property ID must be 9 numbers.";
  }

  return errors;
};

export function OrganizationGoogleAnalyticsIdForm({
  organization,
  googleAnalytics,
  onUpdateGAPropertyId,
}: OrganizationGoogleAnalyticsIdFormProps) {
  return (
    <div className="flex-row align-items-start">
      <div className="mr-lg">
        <DataRecipientLogo dataRecipient={googleAnalytics} />
      </div>

      <div>
        <h3 className="text-t3 m-0 text-weight-bold">Google Analytics</h3>

        <div>
          <Form
            initialValues={{ webProperty: organization.gaWebProperty }}
            onSubmit={onUpdateGAPropertyId}
            validate={formValidation}
            render={({ handleSubmit, errors }) => {
              return (
                <>
                  <FormLabel className="text-bold">
                    Add your GA Property ID for automatic processing of opt-outs on your website
                  </FormLabel>

                  <Text
                    label="Google Analytics Property ID"
                    helperText="Your GA Property ID is 9 numbers (e.g. XXXXXXXXX). To find it, login to your Google Analytics account. At the top left corner click on your Company name next to the Analytics logo. Locate the proper Property for your website. Your GA ID will be listed under the property name."
                    size="small"
                    field="webProperty"
                    variant="outlined"
                    onChange={handleSubmit}
                    required
                  />

                  {errors?.webProperty && (
                    <div className="flex-row">
                      <Alert severity="error">{errors.webProperty}</Alert>
                    </div>
                  )}
                </>
              );
            }}
          />
        </div>
      </div>
    </div>
  );
}
