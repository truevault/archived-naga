import React from "react";
import { Loading } from "../../../common/components/Loading";
import { useDataRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { ActionRow, ActionRowsHeader } from "../../components/ActionRow";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { SettingsTabs } from "./SettingsTabs";

function buildUrl(orgId: string, type: string) {
  return `/organization/${orgId}/settings/templates/${type}`;
}

export const EmailTemplateSettings = () => {
  const organizationId = usePrimaryOrganizationId();

  const { result: messageTypes, request } = useDataRequest({
    queryKey: ["messageTypes", organizationId],
    api: () => Api.organization.getOrganizationMessageTypes(organizationId),
  });

  // Page Content
  return (
    <StandardPageWrapper>
      <PageHeader titleContent="Settings" />
      <SettingsTabs current="templates" />

      <p className="text-t3 mt-xl mb-xxl">View and customize your message templates.</p>

      <ActionRowsHeader label="Email messages saying..." small />
      {NetworkRequest.isFinished(request) ? (
        messageTypes
          .sort((a, b) => a.sortKey - b.sortKey)
          .map(({ slug, description }) => (
            <ActionRow key={slug} label={description} rowHref={buildUrl(organizationId, slug)} />
          ))
      ) : (
        <Loading />
      )}
    </StandardPageWrapper>
  );
};
