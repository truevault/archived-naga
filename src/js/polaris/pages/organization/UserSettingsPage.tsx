import React, { useState, useEffect } from "react";
import { Users, TabPanelLoading, TabPanelEmpty, TabPanelError } from "../../components";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";

import { Button } from "@mui/material";
import { Add as AddIcon } from "@mui/icons-material";

import { useOrganization, useOrganizationUsers, usePrimaryOrganizationId } from "../../hooks";
import { Error as RequestsError } from "../../copy/requests";
import { InviteUserDialog } from "../../components/dialog/InviteUserDialog";
import { NetworkRequest } from "../../../common/models";
import { PageHeader } from "../../components/layout/header/Header";
import { SettingsTabs } from "./SettingsTabs";

export const UserSettings = () => {
  const primaryOrgId = usePrimaryOrganizationId();

  const [, orgRequest] = useOrganization(primaryOrgId);
  const [users, usersRequest, refreshUsers] = useOrganizationUsers(primaryOrgId);

  const [everRendered, setEverRendered] = useState(false);

  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest, usersRequest)) {
      setEverRendered(true);
    }
  }, [orgRequest, usersRequest]);

  const [inviteDialog, setInviteDialog] = useState(false);

  // Page Load
  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  // Page Content
  return (
    <StandardPageWrapper>
      <div className="max-width-896">
        <InviteUserDialog
          open={inviteDialog}
          organizationId={primaryOrgId}
          onClose={() => setInviteDialog(false)}
          onDone={() => {
            setInviteDialog(false);
            refreshUsers();
          }}
        />
        <PageHeader titleContent="Settings" />
        <SettingsTabs current="users" />
        <div className="user-settings mt-lg">
          <Button
            variant="contained"
            color="primary"
            className="mb-sm add-user"
            onClick={() => setInviteDialog(true)}
          >
            <AddIcon /> Add User
          </Button>
          <UsersTableLEE
            emptyMessage="There are no users."
            users={users}
            usersRequest={usersRequest}
            onNeedsRefresh={refreshUsers}
          />
        </div>
      </div>
    </StandardPageWrapper>
  );
};

// LEE = LoadingErrorEmpty
const UsersTableLEE = ({ users, usersRequest, emptyMessage, onNeedsRefresh }) => {
  if (usersRequest.running) {
    return <TabPanelLoading />;
  } else if (!users || usersRequest.error) {
    return <TabPanelError message={usersRequest.errorMessage || RequestsError.GetOrgUsers} />;
  } else if (users.length == 0) {
    return <TabPanelEmpty message={emptyMessage} />;
  } else {
    return <Users.OrganizationUsersTable users={users} onNeedsRefresh={onNeedsRefresh} />;
  }
};
