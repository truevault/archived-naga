import qs from "query-string";
import React, { useEffect, useState } from "react";
import { NetworkRequest } from "../../../common/models";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import { EmailSettings, useEmailSettings } from "../../components/superorg/settings/EmailSettings";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { SettingsTabs } from "./SettingsTabs";

const REFRESH_MAIL = "refreshMailboxes";

const checkAndCloseWindow = () => {
  const parsedQs = qs.parse(location.search);
  if (parsedQs["auth_redirect"] === "1") {
    // Using a random number so that the storage event always fires
    window.localStorage.setItem(REFRESH_MAIL, Math.random().toString(10));
  }
};

export const EmailSettingsPage = () => {
  const organizationId = usePrimaryOrganizationId();
  const { settings, orgRequest, mailboxesRequest, refreshMailboxes } = useEmailSettings();

  const [everRendered, setEverRendered] = useState(false);

  // ever-loaded effect
  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest, mailboxesRequest)) {
      setEverRendered(true);
    }
  }, [orgRequest, mailboxesRequest]);

  useEffect(() => {
    const storageChange = (e: StorageEvent) => {
      if (e.key === REFRESH_MAIL) {
        refreshMailboxes();
        window.localStorage.removeItem(REFRESH_MAIL);
      }
    };

    window.addEventListener("storage", storageChange);

    // Closes the window if comming from Nylas Auth
    checkAndCloseWindow();

    // Cleanup
    return () => {
      window.removeEventListener("storage", storageChange);
    };
  }, [refreshMailboxes, organizationId]);

  // Page Load
  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  // Page Content
  return (
    <StandardPageWrapper>
      <div className="max-width-896">
        <PageHeader titleContent="Settings" />
        <SettingsTabs current="inbox" />

        <div className="mt-xl">
          <EmailSettings {...settings} topDivider />
        </div>
      </div>
    </StandardPageWrapper>
  );
};
