import React from "react";
import { useHistory } from "react-router";
import { usePrimaryOrganizationId, useTabs } from "../../hooks";
import { TabOptions } from "../../hooks/useTabs";

type Tabs = "settings" | "users" | "templates" | "inbox" | "center" | "integrations";

const TAB_OPTIONS: TabOptions<Tabs> = [
  { label: "Organization", value: "settings" },
  { label: "Users", value: "users" },
  { label: "Email Templates", value: "templates" },
  { label: "Privacy Email", value: "inbox" },
  { label: "Privacy Center", value: "center" },
  { label: "Integrations", value: "integrations" },
];

export const SettingsTabs = ({ current = "settings" }: { current?: Tabs }) => {
  const history = useHistory<Tabs>();
  const orgId = usePrimaryOrganizationId();
  const [, tabs] = useTabs(TAB_OPTIONS, current, null, (newVal) => {
    let path = newVal === "settings" ? "" : newVal;
    history.push(`/organization/${orgId}/settings/${path}`.replace(/\/$/, ""));
  });

  return <div className="mt-md">{tabs}</div>;
};
