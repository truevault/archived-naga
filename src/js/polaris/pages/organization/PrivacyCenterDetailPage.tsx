import React, { useEffect, useState } from "react";
import { ExternalLink } from "../../../common/components/ExternalLink";
import { NetworkRequest } from "../../../common/models";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { PageHeader } from "../../components/layout/header/Header";
import {
  PrivacyCenterSettings,
  usePrivacyCenterSettings,
} from "../../components/superorg/settings/PrivacyCenterSettings";
import { SettingsTabs } from "./SettingsTabs";

export const PrivacyCenterDetail = () => {
  const { settings, privacyCenter, orgRequest, privacyCentersRequest } = usePrivacyCenterSettings();

  const [everRendered, setEverRendered] = useState(false);

  // ever-loaded effect
  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest, privacyCentersRequest)) {
      setEverRendered(true);
    }
  }, [orgRequest, privacyCentersRequest]);

  // Page Load
  if (!everRendered || !privacyCenter) {
    return <LoadingPageWrapper />;
  }

  return (
    <StandardPageWrapper>
      <div className="max-width-896">
        <PageHeader titleContent="Settings" />
        <SettingsTabs current="center" />
        <div className={"privacy-center-settings privacy-center"}>
          <div className="text-t3 mb-xl">
            On this page you can make changes to your hosted Privacy Center so that it matches the
            look and the practices of your business.{" "}
            <ExternalLink
              href="https://help.truevault.com/article/156-privacy-center-settings"
              target="_blank"
              className="text-style-normal"
            >
              Learn More
            </ExternalLink>
          </div>

          <PrivacyCenterSettings {...settings} includeUrlField includeRequestFormField topDivider />
        </div>
      </div>
    </StandardPageWrapper>
  );
};
