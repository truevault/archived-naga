import { Edit } from "@mui/icons-material";
import React, { useMemo } from "react";
import { Link } from "react-router-dom";
import { SurveyAnswers, useParsedSurveyAnswers } from "../../../common/hooks/useSurvey";
import { OrganizationDto } from "../../../common/service/server/dto/OrganizationDto";
import { BooleanAnswer } from "../../components/molecules/BooleanAnswer";
import { SurveyView } from "../../components/molecules/SurveyView";
import { RouteHelpers } from "../../root/routes";
import { organizationSettingsQuestions } from "../../surveys/orgSettings";

type OrganizationBusinessProfileViewProps = {
  organization: OrganizationDto;
  surveyAnswers: SurveyAnswers;
};

export const OrganizationBusinessProfileView: React.FC<OrganizationBusinessProfileViewProps> = ({
  organization,
  surveyAnswers,
}) => {
  const parsedAnswers = useParsedSurveyAnswers(surveyAnswers);

  const surveyQ = useMemo(() => {
    return organizationSettingsQuestions(organization).map((q) => ({
      id: q.slug,
      label: q.label,
      response:
        parsedAnswers[q.slug] == null ? null : <BooleanAnswer answer={parsedAnswers[q.slug]} />,
    }));
  }, [organization, parsedAnswers]);

  return (
    <>
      <SurveyView title={`${organization.name}…`} survey={surveyQ} />
      <Link className="mt-md" to={RouteHelpers.organization.editSettings(organization.id)}>
        <Edit fontSize="inherit" className="mr-xxs" />
        Edit Business Profile
      </Link>
    </>
  );
};
