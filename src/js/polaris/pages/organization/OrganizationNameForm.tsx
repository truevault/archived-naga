import React from "react";
import { Form } from "react-final-form";
import { Text } from "../../../common/components/input/Text";
import { OrganizationDto } from "../../../common/service/server/dto/OrganizationDto";
import { requiredString } from "../../util/validation";

interface OrganiationNameFormProps {
  organization: OrganizationDto;
  updateOrganization: (values: any) => void;
}

const formValidation = (values) => {
  const errors = {} as any;

  requiredString(values, "name", errors);

  return errors;
};

export function OrganizationNameForm({
  organization,
  updateOrganization,
}: OrganiationNameFormProps) {
  return (
    <Form
      initialValues={organization}
      onSubmit={updateOrganization}
      validate={formValidation}
      render={({ handleSubmit }) => {
        return (
          <>
            <Text
              label="Organization name"
              className="org-name-input"
              fullWidth={false}
              field="name"
              variant="outlined"
              onBlur={handleSubmit}
              required
            />
          </>
        );
      }}
    />
  );
}
