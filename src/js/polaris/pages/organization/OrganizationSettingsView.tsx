import React from "react";
import { SurveyAnswers } from "../../../common/hooks/useSurvey";
import { OrganizationDto } from "../../../common/service/server/dto/OrganizationDto";
import { OrganizationBusinessProfileView } from "./OrganizationBusinessProfileView";
import { OrganizationNameForm } from "./OrganizationNameForm";

type OrganizationSettingsViewProps = {
  organization: OrganizationDto;
  updateOrganization(values: any): void;
  surveyAnswers: SurveyAnswers;
};

export const OrganizationSettingsView = ({
  organization,
  updateOrganization,
  surveyAnswers,
}: OrganizationSettingsViewProps) => {
  return (
    <div className={"org-settings"}>
      <OrganizationNameForm organization={organization} updateOrganization={updateOrganization} />
      <OrganizationBusinessProfileView organization={organization} surveyAnswers={surveyAnswers} />
    </div>
  );
};
