import _ from "lodash";
import React, { useCallback, useMemo, useState } from "react";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { UpdateOrganizationGoogleAnalyticsWebPropertyDto } from "../../../common/service/server/controller/OrganizationController";
import { PageHeader } from "../../components/layout/header/Header";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { useOrganizationVendors, usePrimaryOrganization } from "../../hooks";
import { isGoogleAnalytics } from "../../util/vendors";
import { ProgressFooter, ProgressFooterActions } from "../get-compliant/ProgressFooter";
import { OrganizationGoogleAnalyticsForm } from "./OrganizationGoogleAnalyticsForm";
import { SettingsTabs } from "./SettingsTabs";

export const IntegrationsSettingsPage = () => {
  const [org, orgRequest] = usePrimaryOrganization();
  const organizationId = org.id;

  const [dataRecipients, dataRecipientsRequest] = useOrganizationVendors(organizationId);

  const [orgGASettings, setOrgGASettings] =
    useState<UpdateOrganizationGoogleAnalyticsWebPropertyDto>(() => ({
      webProperty: org.gaWebProperty,
    }));

  const updateOrganizationGAApi = useCallback(
    (submitValues) => Api.organization.updateGaWebProperty(organizationId, submitValues),
    [organizationId],
  );

  const { fetch: updateOrgGa, request: updateOrgGaRequest } = useActionRequest({
    api: updateOrganizationGAApi,
  });

  const handleUpdateOrgGa = (values: UpdateOrganizationGoogleAnalyticsWebPropertyDto) => {
    updateOrgGa(values);
  };

  const googleAnalytics = useMemo(
    () => dataRecipients?.find((dr) => isGoogleAnalytics(dr)),
    [dataRecipients],
  );

  const debouncedSetOrgGASettings = _.debounce(setOrgGASettings, 300);

  // Page Content
  return (
    <StandardPageWrapper
      requests={[orgRequest, dataRecipientsRequest]}
      footer={
        <ProgressFooter
          actions={
            <ProgressFooterActions
              nextLoading={updateOrgGaRequest.running}
              nextContent="Save"
              nextOnClick={() => handleUpdateOrgGa(orgGASettings)}
              nextDisabled={!orgGASettings.webProperty}
            />
          }
        />
      }
    >
      <PageHeader titleContent="Settings" />
      <SettingsTabs current="integrations" />
      <div className="my-lg">
        <OrganizationGoogleAnalyticsForm
          organization={org}
          onUpdateGAPropertyId={debouncedSetOrgGASettings}
          googleAnalytics={googleAnalytics}
          saveOnAuthorizeGaWebProperty={() => handleUpdateOrgGa(orgGASettings)}
          showDoItLater={false}
        />
      </div>
    </StandardPageWrapper>
  );
};
