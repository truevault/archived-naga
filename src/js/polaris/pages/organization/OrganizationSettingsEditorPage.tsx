import React, { FC, useCallback, useEffect, useMemo, useState } from "react";
import { useParsedSurveyAnswers, useSurvey } from "../../../common/hooks/useSurvey";
import { NetworkRequest } from "../../../common/models";
import { PageHeader } from "../../components/layout/header/Header";
import { PageHeaderBack } from "../../components/layout/header/PageHeaderBack";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { usePrimaryOrganization } from "../../hooks";
import { RouteHelpers } from "../../root/routes";
import { BUSINESS_SURVEY_NAME } from "../../surveys/businessSurvey";
import {
  CONSUMER_INCENTIVES_SURVEY_NAME,
  OFFERS_CONSUMER_INCENTIVES_SLUG,
} from "../../surveys/consumerCollectionIncentivesSurvey";
import { EEA_UK_SURVEY_NAME } from "../../surveys/eeaUkSurvey";
import { organizationSettingsQuestions, OrgSettingQuestion } from "../../surveys/orgSettings";
import { Loading } from "../../../common/components/Loading";
import { SurveyBooleanQuestion } from "../../components/get-compliant/survey/SurveyBooleanQuestion";
import { FinancialIncentivesQuestion } from "../../components/get-compliant/survey/custom/FinancialIncentivesQuestion";

export const OrganizationSettingsEditorPage: FC = () => {
  const [loading, setLoading] = useState(true);
  const [org] = usePrimaryOrganization();

  const surveys = {
    [BUSINESS_SURVEY_NAME]: useSurvey(org.id, BUSINESS_SURVEY_NAME),
    [CONSUMER_INCENTIVES_SURVEY_NAME]: useSurvey(org.id, CONSUMER_INCENTIVES_SURVEY_NAME),
    [EEA_UK_SURVEY_NAME]: useSurvey(org.id, EEA_UK_SURVEY_NAME),
  };

  useEffect(() => {
    const requests = Object.values(surveys).map((survey) => survey.surveyRequest);
    setLoading(!NetworkRequest.areFinished(...requests));
  }, [surveys]);

  const setAnswer = useCallback(
    async (question: OrgSettingQuestion, answer: string) => {
      const survey = question.source;
      if (surveys[survey]) {
        return await surveys[survey].setAnswer(question.slug, answer, question.question ?? "");
      }
    },
    [surveys],
  );

  const surveyAnswers = useMemo(() => {
    if (loading) return null;

    if (org.featureGdpr)
      return {
        ...surveys[BUSINESS_SURVEY_NAME].answers,
        ...surveys[CONSUMER_INCENTIVES_SURVEY_NAME].answers,
        ...surveys[EEA_UK_SURVEY_NAME].answers,
      };

    return {
      ...surveys[BUSINESS_SURVEY_NAME].answers,
      ...surveys[CONSUMER_INCENTIVES_SURVEY_NAME].answers,
    };
  }, [loading, surveys, org.featureGdpr]);

  const answers = useParsedSurveyAnswers(surveyAnswers);
  const questions = organizationSettingsQuestions(org);

  return (
    <StandardPageWrapper>
      <PageHeader
        titleHeaderContent={
          <PageHeaderBack to={RouteHelpers.organization.settings(org.id)}>
            Organization Settings
          </PageHeaderBack>
        }
        titleContent="Business Profile"
      />
      <div className="mt-xl">
        {loading && <Loading />}
        {!loading &&
          questions.map((q) => {
            switch (q.slug) {
              case OFFERS_CONSUMER_INCENTIVES_SLUG:
                return (
                  <FinancialIncentivesQuestion
                    key={q.slug}
                    org={org}
                    question={q}
                    answer={answers[q.slug]}
                    setAnswer={setAnswer}
                    enableTask={true}
                  />
                );
              default:
                return (
                  <SurveyBooleanQuestion
                    key={q.slug}
                    question={{
                      type: "boolean",
                      slug: q.slug,
                      question: q.question,
                      visible: q.visibility ? q.visibility(answers) : true,
                      helpText: q.help,
                    }}
                    answer={answers[q.slug]?.toString()}
                    onAnswer={(answer) => setAnswer(q, answer)}
                  />
                );
            }
          })}
      </div>
    </StandardPageWrapper>
  );
};
