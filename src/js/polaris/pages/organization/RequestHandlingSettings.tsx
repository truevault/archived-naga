import React, { useEffect, useState } from "react";
import { RouteComponentProps, Redirect } from "react-router";

import { RouteHelpers } from "../../root/routes";
import { OrganizationPublicId } from "../../../common/service/server/Types";
import { useOrganization } from "../../hooks";
import { NetworkRequest } from "../../../common/models";
import {
  LoadingPageWrapper,
  StandardPageWrapper,
} from "../../components/layout/StandardPageWrapper";
import { PageHeader } from "../../../common/layout/PageHeader";
import { ArrowForwardIos } from "@mui/icons-material";
import { Card, Link } from "@mui/material";

type MatchParams = {
  organizationId: OrganizationPublicId;
};

type Props = RouteComponentProps<MatchParams>;

export const RequestHandlingSettings = ({ match }: Props) => {
  const organizationId = match.params.organizationId;

  const [organization, orgRequest] = useOrganization(organizationId);

  const [everRendered, setEverRendered] = useState(false);

  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest)) {
      setEverRendered(true);
    }
  }, [orgRequest]);

  // Page Load
  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  const regulations = organization.regulations
    .filter((regulation) => regulation.enabled)
    .sort((a, b) => a.longName.localeCompare(b.longName));

  // If there is only one regulation just redirect as though it was selected
  if (regulations.length == 1) {
    return (
      <Redirect
        to={RouteHelpers.organization.requestHandling.instructions(
          organizationId,
          regulations[0].slug,
        )}
      />
    );
  }

  return (
    <StandardPageWrapper>
      <PageHeader header="Request Handling" />
      <div className="request-handling-settings regulation-selector">
        <div className="regulation-selector-label">
          <div>Select a regulation to Configure:</div>
        </div>
        <div className="regulation-selector-values">
          {organization.regulations
            .filter((regulation) => regulation.enabled)
            .map((regulation) => {
              return (
                <Card
                  key={`regulation-card-${regulation.slug}`}
                  className="regulation-selector-card"
                >
                  <Link
                    className="regulation-selector-card--link"
                    href={RouteHelpers.organization.requestHandling.instructions(
                      organizationId,
                      regulation.slug,
                    )}
                  >
                    <div className="regulation-selector-card--label">{`${regulation.longName} (${regulation.name})`}</div>
                    <ArrowForwardIos
                      className="regulation-selector-card--icon"
                      fontSize="inherit"
                    />
                  </Link>
                </Card>
              );
            })}
        </div>
      </div>
    </StandardPageWrapper>
  );
};
