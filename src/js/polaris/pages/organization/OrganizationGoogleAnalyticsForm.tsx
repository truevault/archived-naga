import { CheckCircle } from "@mui/icons-material";
import { Alert, Button, FormGroup, FormLabel } from "@mui/material";
import React from "react";
import { Form } from "react-final-form";
import { Text } from "../../../common/components/input/Text";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { NetworkRequestState } from "../../../common/models";
import { ApiRouteHelpers } from "../../../common/root/apiRoutes";
import { UpdateOrganizationGoogleAnalyticsWebPropertyDto } from "../../../common/service/server/controller/OrganizationController";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../common/service/server/dto/OrganizationDto";
import { Callout, CalloutVariant } from "../../components/Callout";
import { DataRecipientLogo } from "../../components/DataRecipientLogo";
import { useAuthorizations } from "../../hooks/useAuthorizations";

type OrganizationGoogleAnalyticsFormProps = {
  organization: OrganizationDto;
  googleAnalytics: OrganizationDataRecipientDto;
  onUpdateGAPropertyId: (values: UpdateOrganizationGoogleAnalyticsWebPropertyDto) => void;
  saveOnAuthorizeGaWebProperty?: () => void;
  showDoItLater: boolean;
  hasAuthorizeGoogleAnalyticsTask?: boolean;
  addAuthorizeGoogleAnalyticsTask?: () => Promise<void>;
  addAuthorizeGoogleAnalyticsTaskRequest?: NetworkRequestState;
};

const formValidation = ({ webProperty }) => {
  const errors = {} as any;

  if (webProperty && webProperty.startsWith("UA-")) {
    errors.webProperty = "Universal Analytics IDs are no longer supported.";
  }

  if (webProperty && !/^\d{9}$/.test(webProperty)) {
    errors.webProperty = "Your Google Analytics Property ID must be 9 numbers.";
  }

  return errors;
};

export function OrganizationGoogleAnalyticsForm({
  organization,
  googleAnalytics,
  onUpdateGAPropertyId,
  hasAuthorizeGoogleAnalyticsTask,
  saveOnAuthorizeGaWebProperty,
  showDoItLater,
  addAuthorizeGoogleAnalyticsTask,
  addAuthorizeGoogleAnalyticsTaskRequest,
}: OrganizationGoogleAnalyticsFormProps) {
  const {
    authorizations,
    actions: { unauthorizeGoogleAnalytics },
  } = useAuthorizations(organization.id);

  const handleAuthorizeGoogle = () => {
    window.location.href = ApiRouteHelpers.organization.authorization.google(organization.id);
  };

  return (
    <div className="flex-row align-items-start">
      <div className="mr-lg">
        <DataRecipientLogo dataRecipient={googleAnalytics} />
      </div>

      <div>
        <h3 className="text-t3 m-0 text-weight-bold">Google Analytics</h3>

        <ol className="custom p-0">
          <li className="flex-row mb-md">
            <div>
              <Form
                initialValues={{ webProperty: organization.gaWebProperty }}
                onSubmit={onUpdateGAPropertyId}
                validate={formValidation}
                render={({ handleSubmit, errors }) => {
                  return (
                    <>
                      <FormLabel className="text-bold">
                        Add your GA Property ID for automatic processing of opt-outs on your website
                      </FormLabel>

                      <Text
                        label="Google Analytics Property ID"
                        helperText="Your GA Property ID is 9 numbers (e.g. XXXXXXXXX). To find it, login to your Google Analytics account. At the top left corner click on your Company name next to the Analytics logo. Locate the proper Property for your website. Your GA ID will be listed under the property name."
                        size="small"
                        field="webProperty"
                        variant="outlined"
                        onChange={handleSubmit}
                        required
                      />

                      {errors?.webProperty && (
                        <div className="flex-row">
                          <Alert severity="error">{errors.webProperty}</Alert>
                        </div>
                      )}
                    </>
                  );
                }}
              />
            </div>
          </li>

          <li className="flex-row  mb-md">
            <div className="w-100">
              <FormGroup className="mb-md">
                <FormLabel className="text-bold">
                  Connect your Google Analytics account so your team can automatically process
                  consumer data deletion requests for Google Analytics.{" "}
                  <span className="text-weight-bold">
                    Note that a Google Analytics admin login is required.
                  </span>
                </FormLabel>
              </FormGroup>

              <div className="flex-container">
                {authorizations?.hasGoogle ? (
                  <div className="flex-column">
                    <div className="flex-row flex-center-vertical mb-md">
                      <CheckCircle className="email-status-ok mr-sm" />
                      <span>Google Analytics is Authorized</span>
                    </div>

                    <LoadingButton
                      variant="contained"
                      onClick={unauthorizeGoogleAnalytics.fetch}
                      loading={unauthorizeGoogleAnalytics.request.running}
                    >
                      Disconnect Google Analytics
                    </LoadingButton>
                  </div>
                ) : (
                  <div>
                    <div className="flex-row">
                      <Button
                        variant="contained"
                        onClick={() => {
                          saveOnAuthorizeGaWebProperty ? saveOnAuthorizeGaWebProperty() : null;
                          handleAuthorizeGoogle();
                        }}
                      >
                        Authorize Google Analytics
                      </Button>

                      {!hasAuthorizeGoogleAnalyticsTask && showDoItLater && (
                        <LoadingButton
                          variant="text"
                          color="secondary"
                          onClick={addAuthorizeGoogleAnalyticsTask}
                          loading={addAuthorizeGoogleAnalyticsTaskRequest?.running}
                        >
                          Do this later
                        </LoadingButton>
                      )}
                    </div>

                    {hasAuthorizeGoogleAnalyticsTask && showDoItLater && (
                      <Callout className="mt-mdlg" padding="md" variant={CalloutVariant.Yellow}>
                        No problem! We’ll add this to your Task List and send you periodic reminders
                        to set this up. This task will be closed automatically once you authorize an
                        account.
                      </Callout>
                    )}
                  </div>
                )}
              </div>
            </div>
          </li>
        </ol>
      </div>
    </div>
  );
}
