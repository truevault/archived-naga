import { Alert } from "@mui/material";
import { ValidationErrors } from "final-form";
import React, { useCallback, useEffect, useState } from "react";
import { Form } from "react-final-form";
import { RouteComponentProps } from "react-router-dom";
import { Text } from "../../../common/components/input/Text";
import { InlineLoading, Loading } from "../../../common/components/Loading";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { OrganizationPublicId } from "../../../common/service/server/Types";
import { Callout, CalloutVariant } from "../../components/Callout";
import { PageHeader } from "../../components/layout/header/Header";
import { PageHeaderBack } from "../../components/layout/header/PageHeaderBack";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import {
  ProgressFooter,
  ProgressFooterActions,
  ProgressFooterBack,
} from "../get-compliant/ProgressFooter";

type MatchParams = {
  organizationId: OrganizationPublicId;
  slug: string;
};

type Props = RouteComponentProps<MatchParams>;

const FORM_ID = "edit-email-form";

export const EditEmailTemplate = ({
  match: {
    params: { organizationId, slug: messageTypeSlug },
  },
}: Props) => {
  // Network
  const { fetch: updateCustomText } = useActionRequest({
    api: (customMessage) =>
      Api.organization.updateOrganizationMessageSample(
        organizationId,
        messageTypeSlug,
        customMessage?.trim() ?? "",
      ),
  });

  const [saving, setSaving] = useState(false);
  const [saveError, setSaveError] = useState(null);
  const save = useCallback(
    async (values) => {
      if (saving) return;

      try {
        setSaving(true);
        setSaveError(null);
        await updateCustomText(values.customText);
      } catch (err) {
        setSaveError(err);
      } finally {
        setSaving(false);
      }
    },
    [saving, setSaving, setSaveError, updateCustomText],
  );

  const { result: sample, request } = useDataRequest({
    queryKey: ["emailTemplate", organizationId, messageTypeSlug],
    api: () => Api.organization.getOrganizationMessageSample(organizationId, messageTypeSlug),
  });

  const { messageType, customText, templateHTML } = sample ?? {};

  return (
    <StandardPageWrapper footer={<PageFooter saving={saving} organizationId={organizationId} />}>
      <PageHeader
        titleHeaderContent={
          <PageHeaderBack to={`/organization/${organizationId}/settings/templates`}>
            Email Templates
          </PageHeaderBack>
        }
        titleContent={messageType?.title ?? "Loading..."}
        descriptionContent={messageType?.instructions ?? null}
        descriptionLarge
      />

      <Callout variant={CalloutVariant.Gray} className="mt-xxl p-md">
        <div className="text-t3">
          {NetworkRequest.isFinished(request) ? (
            request.success ? (
              <>
                {saveError && <Alert severity="error">{saveError}</Alert>}

                <Form
                  onSubmit={save}
                  initialValues={{ customText }}
                  validate={(values) => {
                    const errors = {} as ValidationErrors;
                    if (values?.customText?.length > 300) {
                      errors.customText = "Custom Text cannot exceed 300 characters in length.";
                    }
                    return errors;
                  }}
                  render={({ handleSubmit }) => (
                    <EmailForm handleSubmit={handleSubmit} templateHTML={templateHTML} />
                  )}
                />
              </>
            ) : (
              <Alert severity="error">There was an error loading the template.</Alert>
            )
          ) : (
            <Loading />
          )}
        </div>
      </Callout>
    </StandardPageWrapper>
  );
};

interface FooterProps {
  organizationId: OrganizationPublicId;
  saving: boolean;
}

const PageFooter = ({ organizationId, saving }: FooterProps) => (
  <ProgressFooter
    actions={
      <ProgressFooterActions
        nextContent={
          <>
            Save
            {saving && <InlineLoading />}
          </>
        }
        nextForm={FORM_ID}
        nextType="submit"
        prevContent={<ProgressFooterBack label="Cancel" />}
        prevUrl={`/organization/${organizationId}/settings/templates`}
      />
    }
  />
);

interface EmailFormProps {
  handleSubmit: (x: any) => void;
  templateHTML: string;
}

const EmailForm = ({ handleSubmit, templateHTML }: EmailFormProps) => {
  const [beforeHTML, setBeforeHTML] = useState("");
  const [afterHTML, setAfterHTML] = useState("");

  useEffect(() => {
    if (templateHTML.includes("[[[CUSTOM_TEXT]]]")) {
      const [before, after] = templateHTML
        .split('<p class="custom-text">[[[CUSTOM_TEXT]]]</p>')
        .map((t) => t.replace(/href/gi, "xhref"));

      setBeforeHTML(before);
      setAfterHTML(after);
    } else {
      setBeforeHTML("");
      setAfterHTML("");
    }
  }, [templateHTML]);

  return (
    <form id={FORM_ID} onSubmit={handleSubmit}>
      <p>
        <strong>To: consumer@example.com</strong>
      </p>
      <p>
        <strong>Subject: A message about your request [ABC123EFG]</strong>
      </p>

      <div dangerouslySetInnerHTML={{ __html: beforeHTML }} />
      <Text
        isMultiLine
        field={"customText"}
        maxRows={10}
        placeholder="(optional) add your own language here"
      />
      <div dangerouslySetInnerHTML={{ __html: afterHTML }} />
    </form>
  );
};
