import { Stack } from "@mui/material";
import React, { useMemo, useState } from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q3Nav } from "../../../components/layout/nav/surveys/2023-q3/Survey2023Q3Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { SurveyGateHero } from "../../../components/organisms/survey/SurveyGateHero";
import { CategorizedVendorInventory } from "../../../components/organisms/vendor/CategorizedVendorInventory";
import { VendorSearchBox } from "../../../components/organisms/vendor/VendorSearchBox";
import { usePrimaryOrganizationId } from "../../../hooks";
import {
  useAddContractors,
  useAddThirdPartyRecipient,
  useDataSourceSearch,
  useOrganizationVendors,
} from "../../../hooks/useOrganizationVendors";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { Survey2023Q3Step } from "../../../surveys/steps/Survey2023Q3Steps";
import { useUpdateDataRecipients } from "../../get-compliant/data-recipients/AddDataRecipients";
import { Survey2023Q3Footer } from "../../get-compliant/ProgressFooter";
import { SURVEY_2023Q3_GATES } from "./SellingSharingPage";
import {
  CategoryCollapseCard,
  VendorColumn,
} from "../../../components/organisms/vendor/CategoryCollapseCard";
import { RemoveVendorDialog } from "../../../components/dialog/RemoveVendorDialog";
import { QuestionHeading } from "../../../components/vendor/QuestionHeading";
import { AddContractorForm } from "../../get-compliant/data-recipients/ContractorsPage";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  AddTprForm,
  THIRD_PARTY_CATEGORIES,
} from "../../get-compliant/data-recipients/ThirdPartyRecipientsPage";
import { Analytics, FlutterDash, Groups, Handshake, SupervisorAccount } from "@mui/icons-material";
import { SurveyQuestionReviewTable } from "../../../components/organisms/survey/SurveyReviewTable";
import { UUIDString } from "../../../../common/service/server/Types";

const THIRD_PARTY_RECIPIENTS_SURVEY = "third-party-recipients-survey";
const CONTRACTORS_DOES_NOT_APPLY = "contractors-does-not-apply";
const CONSUMER_CONTEXT: CollectionContext[] = ["CONSUMER"];
const TPR_DOES_NOT_APPLY_SLUG = "third-party-recipients-does-not-apply";
const COMARKETER_CATEGORY = "Co-Marketing Partner";
const DATA_BUYER_CATEGORY = "Data Buyer";
const PARTNER_CATEGORY = "Business Partner";
const AFFILIATE_CATEGORY = "Affiliate";

export const Survey2023Q3DataRecipientsPage = () => {
  const orgId = usePrimaryOrganizationId();
  const { request, progress } = useSurveyProgress(orgId, "s2023_3");

  const { requests } = useDataSourceSearch(orgId);

  // Modal States
  const [deleteTpr, setDeleteTpr] = useState<OrganizationDataRecipientDto | null>(null);
  const openDeleteModal = (tpr: OrganizationDataRecipientDto) => setDeleteTpr(tpr);
  const closeDeleteModal = () => setDeleteTpr(null);

  const { fetch: addContractor } = useAddContractors(orgId, () => refresh(), CONSUMER_CONTEXT);
  const updateDataRecipients = useUpdateDataRecipients(orgId, { addCompleted: false });

  const [organizationVendors, organizationVendorsRequest, refresh] = useOrganizationVendors(orgId);

  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, SURVEY_2023Q3_GATES, refresh);

  const contractors = useMemo(
    () => organizationVendors?.filter((v) => v.dataRecipientType === "contractor") || [],
    [organizationVendors],
  );

  const thirdParty = useMemo(
    () => organizationVendors?.filter((v) => v.dataRecipientType === "third_party_recipient") || [],
    [organizationVendors],
  );

  const { answers: recipientAnswers, setAnswer: setRecipientAnswer } = useSurvey(
    orgId,
    THIRD_PARTY_RECIPIENTS_SURVEY,
  );

  const expanded: string[] = useMemo(
    () => JSON.parse(answers?.["updated-survey-questions"] || "[]"),
    [answers],
  );

  const surveys = useMemo(() => {
    return {
      [THIRD_PARTY_RECIPIENTS_SURVEY]: {
        [CONTRACTORS_DOES_NOT_APPLY]:
          recipientAnswers[CONTRACTORS_DOES_NOT_APPLY] === "true" || contractors.length > 0
            ? "true"
            : "false",
        [TPR_DOES_NOT_APPLY_SLUG]:
          recipientAnswers[TPR_DOES_NOT_APPLY_SLUG] === "true" || thirdParty.length > 0
            ? "true"
            : "false",
      },
      [SURVEY_2023Q3_GATES]: answers,
    };
  }, [recipientAnswers, contractors, thirdParty]);

  const hasContractors = useMemo(
    () => recipientAnswers[CONTRACTORS_DOES_NOT_APPLY] === "true" || contractors.length > 0,
    [recipientAnswers, contractors],
  );
  const hasTpr = useMemo(
    () => recipientAnswers[TPR_DOES_NOT_APPLY_SLUG] === "true" || thirdParty.length > 0,
    [recipientAnswers, thirdParty],
  );

  const updaters = useMemo(() => {
    return {
      [THIRD_PARTY_RECIPIENTS_SURVEY]: setRecipientAnswer,
      [SURVEY_2023Q3_GATES]: setAnswer,
    };
  }, [setRecipientAnswer]);

  const visibility = {
    [THIRD_PARTY_RECIPIENTS_SURVEY]: {
      [CONTRACTORS_DOES_NOT_APPLY]: true,
      [TPR_DOES_NOT_APPLY_SLUG]: true,
    },
  };

  const disabledReasons = useMemo(() => {
    return {
      [THIRD_PARTY_RECIPIENTS_SURVEY]: {
        [CONTRACTORS_DOES_NOT_APPLY]:
          contractors.length > 0 && "You currently have at least one contractor.",
        [TPR_DOES_NOT_APPLY_SLUG]:
          thirdParty.length > 0 && "You currently have at least one third-party data recipient.",
      },
    };
  }, [contractors, thirdParty]);

  const comarketerParties = thirdParty.filter((tp) => tp.category == COMARKETER_CATEGORY);
  const buyerParties = thirdParty.filter((tp) => tp.category == DATA_BUYER_CATEGORY);
  const partnerParties = thirdParty.filter((tp) => tp.category == PARTNER_CATEGORY);
  const affiliateParties = thirdParty.filter((tp) => tp.category == AFFILIATE_CATEGORY);
  const otherParties = thirdParty.filter(
    (tp) =>
      ![COMARKETER_CATEGORY, DATA_BUYER_CATEGORY, PARTNER_CATEGORY, AFFILIATE_CATEGORY].includes(
        tp.category,
      ),
  );

  const { fetch: addThirdPartyRecipient } = useAddThirdPartyRecipient(orgId, () => refresh(), [
    "CONSUMER",
  ]);

  const introFinished = answers["data-recipients-intro-got-it"] === "true";
  const contractorsFinished = answers["data-recipients-done-with-contractors"] === "true";
  const recipientsFinished = answers["data-recipients-done-with-recipients"] === "true";
  const thirdPartyFinished = answers["data-recipients-done-with-tpr"] === "true";

  const isFinished =
    introFinished && contractorsFinished && recipientsFinished && thirdPartyFinished;

  let maxResults = 3;
  if (answers["data-recipients-done-with-recipients"] === "true") {
    maxResults = 5;
  }
  if (answers["data-recipients-done-with-contractors"]) {
    maxResults = 8;
  }

  const [initialEnabledVendors, newEnabledVendors] = useMemo(() => {
    const initialIds = JSON.parse(answers["initial-data-recipient-ids"] ?? "[]");
    const { currentVendors } = updateDataRecipients;
    const initialVendorIds = new Set<UUIDString>();
    const newVendorIds = new Set<UUIDString>();
    for (const { id, vendorId } of currentVendors) {
      if (initialIds.includes(parseInt(id, 10))) {
        initialVendorIds.add(vendorId);
      } else {
        newVendorIds.add(vendorId);
      }
    }
    return [initialVendorIds, newVendorIds];
  }, [updateDataRecipients, answers]);

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q3Nav progress={progress?.progress as Survey2023Q3Step} />}
      requests={[
        request,
        surveyRequest,
        requests.catalogRequest,
        requests.orgDataSourcesRequest,
        organizationVendorsRequest,
      ]}
      footer={
        <Survey2023Q3Footer
          presentStep="DataRecipients"
          progressStep={progress?.progress as Survey2023Q3Step}
          nextDisabled={!isFinished}
          saveRequests={[
            requests.addingVendor,
            requests.addingCustomVendor,
            requests.removingVendor,
          ]}
          prevLabel="Back to Data Collection"
        />
      }
    >
      <PageHeader titleContent="Data Recipients Review" />

      <SurveyGateHero
        slug="data-recipients-intro-got-it"
        survey={answers}
        updateResponse={setAnswer}
        label="Got it"
        description={
          <>
            This step is a <strong>quick review</strong> of your vendors and other Data Recipients,
            such as contractors. If you see a vendor you no longer use, you can remove it here by
            clicking <strong>Remove</strong>. If you need to add a vendor, search and add it below.
          </>
        }
        imageUrl="/assets/images/surveys/2023q1/data-map-hero.svg"
      >
        <header className="mb-lg">
          <h4>Step 1: Review your vendors</h4>
        </header>

        <CategorizedVendorInventory
          {...updateDataRecipients}
          enabledVendors={initialEnabledVendors}
          collectionContext={["CONSUMER"]}
        />

        <SurveyGateButton
          slug="data-recipients-done-with-vendor-review"
          survey={answers}
          updateResponse={setAnswer}
          label="Done with Vendor Review"
          className="mb-xxxl"
        >
          <header className="mt-xl mb-lg">
            <h4>Step 2: Add new vendors</h4>
          </header>

          <div className="mb-xl">
            <VendorSearchBox {...updateDataRecipients} maxResults={maxResults} />
          </div>

          {newEnabledVendors.size > 0 && (
            <div className="mb-xl">
              <CategorizedVendorInventory
                {...updateDataRecipients}
                enabledVendors={newEnabledVendors}
                collectionContext={["CONSUMER"]}
                hideEmptyCategories
              />
            </div>
          )}

          <SurveyGateButton
            slug="data-recipients-done-with-recipients"
            survey={answers}
            updateResponse={setAnswer}
            label="Done with Data Recipients"
            className="mb-xxxl"
          >
            <header className="mt-xl">
              <h4>Step 3: Add contractors (if applicable)</h4>
            </header>
            <div className="mb-xl">
              <SurveyQuestionReviewTable
                surveys={surveys}
                surveyUpdaters={updaters}
                visibility={visibility}
                disabledReasons={disabledReasons}
                reviewQuestions={[
                  {
                    survey: THIRD_PARTY_RECIPIENTS_SURVEY,
                    question: {
                      slug: CONTRACTORS_DOES_NOT_APPLY,
                      question:
                        "Does your business have contractors that access personal data about your consumers?",
                      type: "boolean",
                    },
                  },
                ]}
                expanded={expanded}
                surveyGateSlug={SURVEY_2023Q3_GATES}
              />
            </div>
            {hasContractors && (
              <div>
                <div className="mb-md">
                  <QuestionHeading
                    condensed
                    question="Add each of your contractors below that access personal information."
                  />
                </div>
                <div className="mb-xl">
                  <AddContractorForm
                    existing={contractors.map((tp) => tp.name)}
                    onSubmit={addContractor}
                  />
                </div>
              </div>
            )}

            {hasContractors && (
              <VendorColumn
                vendors={contractors}
                pendingVendor={null}
                handleRemove={(v) => openDeleteModal(v as unknown as OrganizationDataRecipientDto)}
              />
            )}

            <RemoveVendorDialog
              open={Boolean(deleteTpr)}
              onClose={closeDeleteModal}
              vendorName={deleteTpr?.name}
              vendorId={deleteTpr?.vendorId}
              collectionContext={CONSUMER_CONTEXT}
            />
            <SurveyGateButton
              slug="data-recipients-done-with-contractors"
              survey={answers}
              updateResponse={setAnswer}
              label="Done with Contractors"
              className="mt-md"
            >
              <header className="mt-xl">
                <h4>Step 4: Add other parties (if applicable)</h4>
              </header>
              <div className="mb-xl">
                <SurveyQuestionReviewTable
                  surveys={surveys}
                  surveyUpdaters={updaters}
                  visibility={visibility}
                  disabledReasons={disabledReasons}
                  reviewQuestions={[
                    {
                      survey: THIRD_PARTY_RECIPIENTS_SURVEY,
                      question: {
                        slug: TPR_DOES_NOT_APPLY_SLUG,
                        question:
                          "Does your business disclose data to any third-party data recipients?",
                        type: "boolean",
                      },
                    },
                  ]}
                  expanded={expanded}
                  surveyGateSlug={SURVEY_2023Q3_GATES}
                />
              </div>
              {hasTpr && (
                <div>
                  <div className="mb-md">
                    <QuestionHeading
                      condensed
                      question="Add each of your third-party data recipients."
                    />
                  </div>

                  <div className="mb-lg">
                    <AddTprForm
                      categories={THIRD_PARTY_CATEGORIES.map((o) => ({ label: o, value: o }))}
                      existing={thirdParty.map((tp) => tp.name)}
                      onSubmit={addThirdPartyRecipient}
                    />
                  </div>
                  <Stack spacing={2}>
                    <CategoryCollapseCard
                      category={{
                        label: COMARKETER_CATEGORY,
                        icon: SupervisorAccount,
                        description:
                          "Other businesses with which you share and receive customer contact information.",
                        category: COMARKETER_CATEGORY,
                      }}
                      vendors={comarketerParties}
                      handleRemove={(v) =>
                        openDeleteModal(v as unknown as OrganizationDataRecipientDto)
                      }
                    />

                    <CategoryCollapseCard
                      category={{
                        label: DATA_BUYER_CATEGORY,
                        icon: Analytics,
                        description:
                          "If you provide customer data to other parties in exchange for money or a service.",
                        category: DATA_BUYER_CATEGORY,
                      }}
                      vendors={buyerParties}
                      handleRemove={(v) =>
                        openDeleteModal(v as unknown as OrganizationDataRecipientDto)
                      }
                    />

                    <CategoryCollapseCard
                      category={{
                        label: PARTNER_CATEGORY,
                        icon: Handshake,
                        description:
                          "Any business you work with that does not provide a service and that has access to your customer data.",
                        category: PARTNER_CATEGORY,
                      }}
                      vendors={partnerParties}
                      handleRemove={(v) =>
                        openDeleteModal(v as unknown as OrganizationDataRecipientDto)
                      }
                    />

                    <CategoryCollapseCard
                      category={{
                        label: AFFILIATE_CATEGORY,
                        icon: FlutterDash,
                        description:
                          "Another company that has an ownership interest or other relationship with your business.",
                        category: AFFILIATE_CATEGORY,
                      }}
                      vendors={affiliateParties}
                      handleRemove={(v) =>
                        openDeleteModal(v as unknown as OrganizationDataRecipientDto)
                      }
                    />

                    <CategoryCollapseCard
                      category={{
                        label: "Other Data Recipients",
                        icon: Groups,
                        category: "Other",
                      }}
                      vendors={otherParties}
                      handleRemove={(v) =>
                        openDeleteModal(v as unknown as OrganizationDataRecipientDto)
                      }
                    />
                  </Stack>
                </div>
              )}
              <RemoveVendorDialog
                open={Boolean(deleteTpr)}
                onClose={closeDeleteModal}
                vendorName={deleteTpr?.name}
                vendorId={deleteTpr?.vendorId}
                collectionContext={["CONSUMER"]}
              />

              <SurveyGateButton
                slug="data-recipients-done-with-tpr"
                survey={answers}
                updateResponse={setAnswer}
                label="Done with Other Parties"
                className="mt-md"
              ></SurveyGateButton>
            </SurveyGateButton>
          </SurveyGateButton>
        </SurveyGateButton>
      </SurveyGateHero>
    </StandardPageWrapper>
  );
};
