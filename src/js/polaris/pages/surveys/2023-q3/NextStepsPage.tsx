import _ from "lodash";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { Api } from "../../../../common/service/Api";
import { TaskDto } from "../../../../common/service/server/dto/TaskDto";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q3Nav } from "../../../components/layout/nav/surveys/2023-q3/Survey2023Q3Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { SurveyGateHero } from "../../../components/organisms/survey/SurveyGateHero";
import { EmptyTodoTasks, TaskCard } from "../../../components/organisms/tasks/TaskCard";
import { useOrganizationVendors, usePrimaryOrganization } from "../../../hooks";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { useTasks } from "../../../hooks/useTasks";
import { Survey2023Q3Step } from "../../../surveys/steps/Survey2023Q3Steps";
import { todoTaskComparitor } from "../../../util/tasks";
import { Survey2023Q3Footer } from "../../get-compliant/ProgressFooter";
import { SURVEY_2023Q3_GATES } from "./SellingSharingPage";
import { useActionRequest } from "../../../../common/hooks/api";
import { isGoogleAnalytics } from "../../../util/vendors";
import { OrganizationGoogleAnalyticsIdForm } from "../../organization/OrganizationGoogleAnalyticsIdForm";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { Button } from "@mui/material";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { makeStyles } from "@mui/styles";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import clsx from "clsx";
import { DisableableButton } from "../../../components";
import Confetti from "react-confetti";

const useClasses = makeStyles({
  nextStepsTitle: {
    fontSize: "2rem",
    fontWeight: "bold",
    marginTop: "0",
    marginBottom: "0",
  },
  nextStepsBody: {
    fontSize: "1.25rem",
  },
  nextStepsNoTasks: {
    marginTop: "50px",
  },
});

export const Survey2023Q3NextStepsPage = () => {
  const [org, orgRequest, refreshRequest] = usePrimaryOrganization();
  const orgId = org.id;
  const { request, progress } = useSurveyProgress(orgId, "s2023_3");
  const classes = useClasses();

  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, SURVEY_2023Q3_GATES);
  const [gaComplete, setGaComplete] = useState(
    answers["next-steps-done-with-google-analytics"] === "true",
  );
  const [tasks, tasksRequest] = useTasks(orgId, "active");
  const sortedTasks = useMemo(() => tasks?.sort(todoTaskComparitor), [tasks]);

  const [dataRecipients, dataRecipientsRequest] = useOrganizationVendors(orgId);

  const updateOrganizationGAApi = useCallback(
    (submitValues) => Api.organization.updateGaWebProperty(orgId, submitValues),
    [orgId],
  );

  const { fetch: updateOrgGa, request: updateOrgGaRequest } = useActionRequest({
    api: async (submitValues) => {
      await updateOrganizationGAApi(submitValues);
      await refreshRequest();
    },
  });
  const debouncedUpdateOrgGa = _.debounce(updateOrgGa, 300);

  const googleAnalytics = useMemo(
    () => dataRecipients?.find((dr) => isGoogleAnalytics(dr)),
    [dataRecipients],
  );

  const hasUAGooglePropertyId = useMemo(
    () => !org?.gaWebProperty || !/^\d{9}$/.test(org?.gaWebProperty),
    [org],
  );

  const markSurveyComplete = async () => Api.organizationProgress.completeSurvey(orgId, "s2023_3");

  const hasTasks = sortedTasks && sortedTasks.length > 0;

  const destination = hasTasks ? "/tasks/todo" : "/";

  const handleFinishSurvey = async () => {
    await markSurveyComplete();

    // navigate to slash
    window.location.href = destination;

    // cancel the standard progress actions
    return true;
  };

  const showGAQuestion =
    (googleAnalytics && hasUAGooglePropertyId) || answers["tasks-intro-google-analytics"];
  const gaQuestionComplete = !showGAQuestion || answers["tasks-intro-google-analytics"];

  useEffect(() => {
    if (!showGAQuestion || answers["next-steps-done-with-google-analytics"] === "true") {
      markSurveyComplete();
    }
  }, [showGAQuestion, answers]);

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q3Nav progress={progress?.progress as Survey2023Q3Step} />}
      requests={[request, surveyRequest, tasksRequest, orgRequest, dataRecipientsRequest]}
      footer={
        <Survey2023Q3Footer
          hideNext={true}
          presentStep="NextSteps"
          progressStep={progress?.progress as Survey2023Q3Step}
          prevLabel="Back to Website Audit"
          saveRequests={[updateOrgGaRequest]}
        />
      }
    >
      <PageHeader titleContent="Next Steps" />

      {showGAQuestion && (
        <>
          <Paras>
            <Para>
              We noticed your business uses a Universal Analytics ID in our Google Analytics
              integration. Starting July 1, 2023, standard Universal Analytics properties will stop
              processing new data.
            </Para>
            <Para className="mb-md">
              Please update your GA ID in our system to the new Google Analytics 4 ID as soon as
              possible.
            </Para>
          </Paras>
          {!answers["tasks-intro-google-analytics"] && (
            <div className="mb-lg">
              <Button
                color="primary"
                variant="contained"
                className="mr-md"
                onClick={() => {
                  setAnswer("tasks-intro-google-analytics", "true");
                }}
              >
                Do this now
              </Button>
              <Button
                variant="outlined"
                onClick={() => {
                  setAnswer("tasks-intro-google-analytics", "deferred");
                }}
              >
                Do this later
              </Button>
            </div>
          )}
          {answers["tasks-intro-google-analytics"] === "true" && (
            <OrganizationGoogleAnalyticsIdForm
              organization={org}
              onUpdateGAPropertyId={debouncedUpdateOrgGa}
              googleAnalytics={googleAnalytics}
            />
          )}
          {answers["tasks-intro-google-analytics"] === "deferred" && (
            <Callout className="mt-mdlg" padding="md" variant={CalloutVariant.Yellow}>
              No problem! We’ll add this to your Task List and send you periodic reminders to update
              your GA property ID. This task will be closed automatically once you do so.
            </Callout>
          )}
        </>
      )}

      {gaQuestionComplete && (
        <>
          {showGAQuestion && !gaComplete && (
            <div className="d-inline-block min-width-192 h-48 mt-mdlg">
              <DisableableButton
                disabled={
                  answers["tasks-intro-google-analytics"] === "true" && hasUAGooglePropertyId
                }
                disabledTooltip="Please enter a valid Google Analytics property ID."
                variant="outlined"
                className="d-inline-block min-width-192 h-48"
                onClick={() => setGaComplete(true)}
              >
                Done
              </DisableableButton>
            </div>
          )}
          {(gaComplete || !showGAQuestion) && (
            <>
              <Confetti
                gravity={0.05}
                numberOfPieces={1000}
                tweenDuration={50000}
                recycle={false}
              />
              <SurveyGateButton
                slug="next-steps-done-with-google-analytics"
                survey={answers}
                updateResponse={setAnswer}
                className="mb-xxxl mt-mdlg"
                bypass={gaComplete || !showGAQuestion}
              >
                <SurveyGateHero
                  slug="tasks-intro-got-it"
                  survey={answers}
                  updateResponse={setAnswer}
                  description={
                    <>
                      <p
                        className={clsx({
                          [classes.nextStepsTitle]: true,
                          [classes.nextStepsNoTasks]: !hasTasks,
                        })}
                      >
                        Congratulations, your 2023 Q3 Survey is complete!
                      </p>
                      {hasTasks && (
                        <p className={classes.nextStepsBody}>
                          We noticed you have these <strong>Open Tasks</strong> to complete. We
                          recommend taking care of these as soon as possible.
                        </p>
                      )}
                      {!hasTasks && (
                        <div className="mt-md">
                          <Button
                            size="large"
                            color="primary"
                            variant="contained"
                            onClick={handleFinishSurvey}
                          >
                            Go To Dashboard
                          </Button>
                        </div>
                      )}
                    </>
                  }
                  imageUrl="/assets/images/surveys/2023q3/survey-complete.svg"
                  imageWidth="300"
                  bypass={true}
                >
                  {hasTasks && (
                    <>
                      <div className="mb-lg" />
                      <TaskBody tasks={sortedTasks} titleOnly={true} />
                      <div className="mt-xl">
                        <Button
                          size="large"
                          color="primary"
                          variant="contained"
                          onClick={handleFinishSurvey}
                        >
                          Go To Task List
                        </Button>
                      </div>
                    </>
                  )}
                </SurveyGateHero>
              </SurveyGateButton>
            </>
          )}
        </>
      )}
    </StandardPageWrapper>
  );
};

const TaskBody: React.FC<{ tasks: TaskDto[]; titleOnly?: boolean }> = ({ tasks, titleOnly }) => {
  if (!tasks?.length) {
    return <EmptyTodoTasks />;
  }

  return (
    <>
      {tasks.map((t) => (
        <div className="mb-md" key={t.id}>
          <TaskCard task={t} titleOnly={titleOnly} />
        </div>
      ))}
    </>
  );
};
