import React from "react";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q3Nav } from "../../../components/layout/nav/surveys/2023-q3/Survey2023Q3Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { Survey2023Q3Step } from "../../../surveys/steps/Survey2023Q3Steps";
import { Survey2023Q3Footer } from "../../get-compliant/ProgressFooter";
import {
  WebsiteAudit,
  useWebsiteAudit,
} from "../../../components/organisms/website-audit/WebsiteAudit";
import { SURVEY_2023Q3_GATES } from "./SellingSharingPage";
import { useSurvey } from "../../../../common/hooks/useSurvey";

export const CORE_PRIVACY_CONCEPTS_SURVEY = "core-privacy-concepts-survey";
const Header: React.FC = () => {
  return (
    <PageHeader
      titleContent="Website Audit"
      descriptionContent={<>We’ll scan your site to ensure all required links are present.</>}
    />
  );
};

export const Survey2023Q3WebsiteAuditPage: React.FC = () => {
  const orgId = usePrimaryOrganizationId();
  const { request, progress } = useSurveyProgress(orgId, "s2023_3");
  const {
    answers: gateAnswers,
    setAnswer: setGateAnswers,
    surveyRequest: surveyGateRequest,
    refreshSurvey,
  } = useSurvey(orgId, SURVEY_2023Q3_GATES);
  const websiteAudit = useWebsiteAudit();
  return (
    <StandardPageWrapper
      navContent={<Survey2023Q3Nav progress={progress?.progress as Survey2023Q3Step} />}
      requests={[request, surveyGateRequest, ...Object.values(websiteAudit.requests)]}
      footer={
        <Survey2023Q3Footer
          presentStep="WebsiteAudit"
          progressStep={progress?.progress as Survey2023Q3Step}
          nextDisabled={gateAnswers["website-audit-finished"] !== "true"}
          prevLabel="Back to Data Sources"
        />
      }
    >
      <Header />

      <WebsiteAudit
        {...websiteAudit.props}
        isFinished={gateAnswers["website-audit-finished"] === "true"}
        markFinished={() => {
          setGateAnswers("website-audit-finished", "true");
        }}
        onRescan={() => {
          refreshSurvey();
          websiteAudit.refreshSurvey();
        }}
      />
    </StandardPageWrapper>
  );
};
