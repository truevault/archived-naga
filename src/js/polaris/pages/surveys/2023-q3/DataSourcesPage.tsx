import { Stack } from "@mui/material";
import React, { useMemo } from "react";

import { SurveyQuestionContainer } from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q3Nav } from "../../../components/layout/nav/surveys/2023-q3/Survey2023Q3Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { useDataRetentionModule } from "../../../components/organisms/data-retention/DataRetentionModule";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { Survey2023Q3Step } from "../../../surveys/steps/Survey2023Q3Steps";
import { Survey2023Q3Footer } from "../../get-compliant/ProgressFooter";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import {
  QuestionContainer,
  QuestionLabel,
} from "../../../components/get-compliant/survey/QuestionContainer";
import { DataSourceReceiptCards } from "../../../components/organisms/DataSourceReceiptCards";
import { DataSourceReceiptCheckboxes } from "../../../components/organisms/DataSourceReceiptCheckboxes";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import {
  CONSUMER_SOURCES_SURVEY_NAME,
  useUpdateDataSources,
} from "../../get-compliant/data-map/ConsumerDataSourcesPage";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import { useReceiptsForCollectionGroups } from "../../../hooks/useReceiptsForCollectionGroups";
import { useQueryClient } from "@tanstack/react-query";
import {
  SOURCE_KEY_CONSUMER_DIRECTLY,
  SOURCE_KEY_OTHER_INDIVIDUALS,
  VENDOR_CATEGORY_AD_NETWORK,
  VENDOR_CATEGORY_DATA_BROKER,
  VENDOR_CATEGORY_RETAIL_PARTNER,
} from "../../../types/Vendor";
import {
  OTHER_EXCLUDED_CATEGORIES,
  dataMapSourcesSurvey,
} from "../../../surveys/dataMapSourcesSurvey";
import { isFinished } from "../../../surveys/survey";
import { UUIDString } from "../../../../common/service/server/Types";
import { SURVEY_2023Q3_GATES } from "./SellingSharingPage";
import { SurveyQuestionReviewTable } from "../../../components/organisms/survey/SurveyReviewTable";

export const Survey2023Q3DataSourcesPage = () => {
  const orgId = usePrimaryOrganizationId();
  const dataRetention = useDataRetentionModule("data-sources-2023-q3-catchup-done-with-review");
  const { request: progressRequest, progress } = useSurveyProgress(orgId, "s2023_3");

  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, CONSUMER_SOURCES_SURVEY_NAME);
  const {
    answers: gateAnswers,
    setAnswer: setGateAnswer,
    surveyRequest: surveyGateRequest,
  } = useSurvey(orgId, SURVEY_2023Q3_GATES);

  const [recipients, recipientsRequest] = useOrganizationVendors(
    orgId,
    undefined,
    undefined,
    undefined,
    ["CONSUMER"],
  );
  const [groups, groupsRequest] = useCollectionGroups(orgId, [
    "BUSINESS_TO_BUSINESS",
    "BUSINESS_TO_CONSUMER",
  ]);
  const receiptData = useReceiptsForCollectionGroups(orgId, groups, ["CONSUMER"]);
  const updateDataSources = useUpdateDataSources(orgId);
  const queryClient = useQueryClient();

  const hasRetailPartner = useMemo(
    () => updateDataSources.data.sources.some((s) => s.category == VENDOR_CATEGORY_RETAIL_PARTNER),
    [updateDataSources.data.sources],
  );
  const hasDataBroker = useMemo(
    () => updateDataSources.data.sources.some((s) => s.category == VENDOR_CATEGORY_DATA_BROKER),
    [updateDataSources.data.sources],
  );
  const hasAdNetwork = useMemo(
    () => updateDataSources.data.sources.some((s) => s.category == VENDOR_CATEGORY_AD_NETWORK),
    [updateDataSources.data.sources],
  );
  const hasOtherSource = useMemo(
    () =>
      updateDataSources.data.sources.some((s) => !OTHER_EXCLUDED_CATEGORIES.includes(s.category)),
    [updateDataSources.data.sources],
  );

  const survey = useMemo(
    () =>
      dataMapSourcesSurvey(
        answers,
        updateDataSources.data.catalog,
        receiptData.sources,
        recipients,
        {
          handleAddVendor: updateDataSources.actions.handleAddVendor,
          handleAddCustomVendor: updateDataSources.actions.handleAddCustomVendor,
          handleRemoveVendor: updateDataSources.actions.handleRemoveVendor,
        },
      ),
    [
      answers,
      updateDataSources.data.catalog,
      receiptData.sources,
      recipients,
      updateDataSources.actions,
    ],
  );

  // Auto-check questions if source disclosures are already present.
  if (updateDataSources.data.sources.some((s) => s.vendorKey == SOURCE_KEY_CONSUMER_DIRECTLY)) {
    answers["consumer-directly"] = "true";
  }

  if (updateDataSources.data.sources.some((s) => s.vendorKey == SOURCE_KEY_OTHER_INDIVIDUALS)) {
    answers["other-consumers"] = "true";
  }

  const surveyFinished =
    isFinished(survey, answers) && gateAnswers["sources-acknowledgement"] === "true";

  // Two questions cause the sources to be updated, so we hook here to invalidate our source query, so we re-fetch the sources
  const handleSetAnswer = async (slug: string, answer: string, question?: string) => {
    await setAnswer(slug, answer, question);
    if (slug == "consumer-directly" || slug == "other-consumers") {
      queryClient.invalidateQueries(["orgDataSources", orgId]);
    }
  };

  const retailPartnersDone = answers["retail-partners"] === "false" || hasRetailPartner;
  const dataBrokersDone = answers["data-brokers"] === "false" || hasDataBroker;
  const adNetworksDone = answers["ad-networks"] === "false" || hasAdNetwork;
  const otherDone = answers["other-sources"] === "false" || hasOtherSource;

  const everyDataSourceHasPIC = updateDataSources.data.sources.every(
    (source) => receiptData.receiptsByVendorId?.[source.id]?.length > 0,
  );

  const surveys = useMemo(() => {
    return {
      [SURVEY_2023Q3_GATES]: gateAnswers,
      [CONSUMER_SOURCES_SURVEY_NAME]: answers,
    };
  }, [gateAnswers, answers]);

  const updaters = useMemo(() => {
    return {
      [SURVEY_2023Q3_GATES]: setGateAnswer,
      [CONSUMER_SOURCES_SURVEY_NAME]: handleSetAnswer,
    };
  }, [setGateAnswer, setAnswer, handleSetAnswer]);

  const visibility = useMemo(() => {
    return {
      [CONSUMER_SOURCES_SURVEY_NAME]: survey.visibility,
    };
  }, [survey]);

  const disabledReasons = useMemo(() => {
    return {
      [CONSUMER_SOURCES_SURVEY_NAME]: survey.disabled,
    };
  }, [survey]);

  const expanded: string[] = useMemo(
    () => JSON.parse(gateAnswers?.["updated-survey-questions"] || "[]"),
    [gateAnswers],
  );

  const hasDataSource = updateDataSources.data.sources.length > 0;
  const isDone =
    surveyFinished &&
    retailPartnersDone &&
    dataBrokersDone &&
    adNetworksDone &&
    otherDone &&
    everyDataSourceHasPIC &&
    hasDataSource;

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q3Nav progress={progress?.progress as Survey2023Q3Step} />}
      requests={[
        progressRequest,
        dataRetention.requests.dataMapRequest,
        dataRetention.requests.dataRetentionRequest,
        dataRetention.requests.surveyRequest,
        surveyGateRequest,
        surveyRequest,
        recipientsRequest,
        groupsRequest,
      ]}
      footer={
        <Survey2023Q3Footer
          presentStep="DataSources"
          progressStep={progress?.progress as Survey2023Q3Step}
          nextDisabled={!isDone}
          nextDisabledTooltip="Please make sure every collection category is selected and every selected purpose has a written description."
          prevLabel="Back to Data Recipients"
        />
      }
    >
      <PageHeader
        titleContent="Sources"
        descriptionContent="In this step, you’ll review all of your business’s data sources."
      />

      <SurveyGateButton
        slug="sources-acknowledgement"
        survey={gateAnswers}
        updateResponse={setGateAnswer}
        label="Get Started"
      >
        <SurveyQuestionReviewTable
          reviewQuestions={survey.questions.map((question) => ({
            question,
            survey: CONSUMER_SOURCES_SURVEY_NAME,
          }))}
          surveys={surveys}
          surveyUpdaters={updaters}
          visibility={visibility}
          disabledReasons={disabledReasons}
          expanded={expanded}
          surveyGateSlug={SURVEY_2023Q3_GATES}
        />

        {surveyFinished && (
          <div className="my-xl">
            <SurveyQuestionContainer>
              <div className="survey--question">
                <QuestionContainer>
                  <QuestionLabel
                    label="Almost Done! Finally, review the personal information you receive from each source."
                    helpText="The data you previously indicated you collect is listed below each source. Select which types of data are collected from each of your sources. If you need to remove a source, update your answer above."
                    recommendation={
                      !receiptData || receiptData.sources.length == 0
                        ? "You are required to add at least one data source to continue. Change an answer to 'yes' above to add a source."
                        : null
                    }
                  />

                  <Stack spacing={3} className="my-lg">
                    <DataSourceReceiptCards
                      sources={receiptData.sources.filter(
                        (s) =>
                          !["Data Brokers", "Data Analytics Providers", "Ad Networks"].includes(
                            s.name,
                          ),
                      )}
                      disclosuresByVendorId={receiptData.receiptsByVendorId}
                      consumerDirectlyName="Consumer"
                      renderer={(r) => (
                        <DataSourceReceiptCheckboxes
                          collectedPic={receiptData.collectedPIC}
                          selected={receiptData.receiptsByVendorId[r.vendorId]?.flatMap(
                            (d) => d.id,
                          )}
                          includeSelectAll={r.vendorKey === "source-consumer-directly"}
                          pending={receiptData.pendingByVendorId[r.vendorId]?.flatMap((d) => d)}
                          onSelectAll={(received: boolean) => {
                            receiptData.actions.toggleReceipt({
                              vendorId: r.vendorId,
                              receivedIds: receiptData.collectedPIC.map((pic) => pic.id),
                              received,
                            });
                          }}
                          onChange={(receivedId: UUIDString, received: boolean) =>
                            receiptData.actions.toggleReceipt({
                              vendorId: r.vendorId,
                              receivedIds: [receivedId],
                              received,
                            })
                          }
                        />
                      )}
                    />
                  </Stack>
                </QuestionContainer>
              </div>
            </SurveyQuestionContainer>
          </div>
        )}
      </SurveyGateButton>
    </StandardPageWrapper>
  );
};
