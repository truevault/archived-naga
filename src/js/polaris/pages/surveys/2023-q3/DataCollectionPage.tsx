import React, { useEffect, useMemo, useState } from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q3Nav } from "../../../components/layout/nav/surveys/2023-q3/Survey2023Q3Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../../hooks";
import { useDataMap } from "../../../hooks/useDataMap";
import { useProcessingActivites } from "../../../hooks/useProcessingActivities";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { Survey2023Q3Step } from "../../../surveys/steps/Survey2023Q3Steps";
import { Survey2023Q3Footer } from "../../get-compliant/ProgressFooter";
import {
  Button,
  CircularProgress,
  Grid,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Tooltip,
} from "@mui/material";
import { ConsumerDataCollectionCheckboxes } from "../../../components/organisms/collection-groups/ConsumerDataCollectionCheckboxes";
import { ConsumerDataCollectionReview } from "../../../components/organisms/collection-groups/ConsumerDataCollectionReview";
import { CalloutVariant } from "../../../components/Callout";
import { Callout } from "../../../components/Callout";
import { useCollectionGroups } from "../../../hooks/useCollectionGroups";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import { UUIDString } from "../../../../common/service/server/Types";
import { SurveyButton } from "../../../components/organisms/survey/SurveyButton";
import { makeStyles } from "@mui/styles";
import { anyCollectedForCategory } from "../../../util/dataInventory";
import { useSurveyClassification } from "../../../components/organisms/collection-groups/ConsumerDataCollectionForm";
import {
  SurveyBooleanQuestion,
  SurveyBooleanQuestionProps,
} from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import {
  SENSITIVE_INFERENCES_QUESTION_SLUG,
  consumerCollectionSurvey,
  consumerCollectionSurveyName,
} from "../../../surveys/consumerCollectionCategoriesSurvey";
import { SurveyQuestionShortAnswer } from "../../../components/get-compliant/survey/answers/SurveyQuestionShortAnswer";
import { Edit, NewReleases } from "@mui/icons-material";
import { CollectionCheckboxes } from "../../../components/get-compliant/collection-groups/CollectionCheckboxes";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";

const SURVEY_2023Q3_GATES = "2023q3-survey";
const TRANSITION_DELAY = 350;

const NEW_GEOLOCATION_PIC = ["geolocation-data", "geolocation-information"];
const NEW_PIC_CHANGES = [
  "racial-or-ethnic-origin",
  "religious-or-philosophical-beliefs",
  "sexual-orientation",
];
const HIDDEN_PIC_KEYS = [...NEW_PIC_CHANGES, ...NEW_GEOLOCATION_PIC];

const useStyles = makeStyles(() => ({
  reviewIcon: {
    position: "relative",
    top: "3px",
  },
}));

const Header: React.FC = () => {
  return <PageHeader titleContent="Data Collection Review" />;
};

export const Survey2023Q3DataCollectionPage: React.FC = () => {
  // Variables
  const classes = useStyles({});
  const orgId = usePrimaryOrganizationId();
  const [org] = usePrimaryOrganization();
  const { request, progress } = useSurveyProgress(orgId, "s2023_3");

  // State
  const [groupIndex, setGroupIndex] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const [dataMap, dataMapRequest, _refreshDataMap, updateCollection] = useDataMap(orgId);

  const [_activities, activityRequest] = useProcessingActivites(orgId);

  // TODO: Do we want to review all collection groups, or just consumer ones?
  const [groups] = useCollectionGroups(orgId);

  const {
    answers: gateAnswers,
    setAnswer: setGateAnswers,
    surveyRequest: surveyGateRequest,
  } = useSurvey(orgId, SURVEY_2023Q3_GATES);

  const group = useMemo(
    () => groups[groupIndex] || ({} as CollectionGroupDetailsDto),
    [groups, groupIndex],
  );
  const initialCollectedPIC: UUIDString[] = useMemo(() => {
    const initialPIC = JSON.parse(gateAnswers?.["initial-collected-pic"] || "{}");
    if (group && initialPIC[group.id]) {
      return initialPIC[group.id];
    }
    return [];
  }, [gateAnswers, group]);

  const sensitive = useSurveyClassification(dataMap, "SENSITIVE");

  const inferencesCategoryId = dataMap?.categories.find((cat) => cat.key === "inferences")?.id;
  const collected = dataMap?.collection.find((dto) => dto.collectionGroupId === group.id);
  const collectsSensitiveInformation = collected
    ? anyCollectedForCategory(collected, sensitive)
    : false;
  const makesInferences = collected?.collected.includes(inferencesCategoryId);
  const showSensitiveInferences = makesInferences && collectsSensitiveInformation;

  const surveyName = group ? consumerCollectionSurveyName(group.id) : undefined;
  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, surveyName);

  let sensitiveInferencesQuestion: SurveyBooleanQuestionProps | undefined;
  if (showSensitiveInferences) {
    const survey = consumerCollectionSurvey(answers, dataMap, collected);
    sensitiveInferencesQuestion = survey.questions.find(
      (q) => q.slug === SENSITIVE_INFERENCES_QUESTION_SLUG,
    ) as SurveyBooleanQuestionProps;
  }

  const expanded: string[] = useMemo(
    () => JSON.parse(gateAnswers?.["updated-survey-questions"] || "[]"),
    [gateAnswers],
  );

  const isExpanded = useMemo(
    () => expanded.includes(`${SENSITIVE_INFERENCES_QUESTION_SLUG}-${group.id}`),
    [expanded, group],
  );

  const handleExpand = () => {
    const updatedArr = [...expanded];
    updatedArr.push(`${SENSITIVE_INFERENCES_QUESTION_SLUG}-${group.id}`);
    setGateAnswers("updated-survey-questions", JSON.stringify(updatedArr));
  };

  useEffect(() => {
    if (answers && !answers[SENSITIVE_INFERENCES_QUESTION_SLUG]) {
      handleExpand();
    }
  }, [answers]);

  const isFinished = useMemo(
    () =>
      groupIndex + 1 >= groups.length &&
      (!sensitiveInferencesQuestion || answers[SENSITIVE_INFERENCES_QUESTION_SLUG]),
    [groupIndex, answers],
  );

  const lastPageTooltip = useMemo(() => {
    if (groupIndex + 1 >= groups.length && !isFinished) {
      return "Answer the question above before proceeding.";
    }
  }, [isFinished]);

  const geolocationPIC = dataMap?.categories.filter((cat) => NEW_GEOLOCATION_PIC.includes(cat.key));
  const newPICChanges = dataMap?.categories.filter((cat) => NEW_PIC_CHANGES.includes(cat.key));
  newPICChanges?.forEach((cat) => {
    switch (cat.key) {
      case "racial-or-ethnic-origin":
        cat.description = "(Previously Racial or ethnic origin)";
        break;
      case "religious-or-philosophical-beliefs":
        cat.description = "(Previously Religious or philosophical beliefs)";
        break;
      case "sexual-orientation":
        cat.description = "(Previously Sexual orientation)";
        break;
    }
  });
  const showGeolocationPIC = Array.isArray(geolocationPIC) && geolocationPIC.length > 0;
  const showNewPICChanges = Array.isArray(newPICChanges) && newPICChanges.length > 0;
  const showNewCategories = showGeolocationPIC || showNewPICChanges;

  const nextGroup = () => {
    if (groupIndex + 1 < groups.length) {
      setIsLoading(true);
      setTimeout(() => {
        if (window.polaris) {
          window.polaris.scrollPolarisTo(0);
        }
        setIsLoading(false);
        setGroupIndex(groupIndex + 1);
      }, TRANSITION_DELAY);
    }
  };

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q3Nav progress={progress?.progress as Survey2023Q3Step} />}
      requests={[request, activityRequest, dataMapRequest, surveyRequest, surveyGateRequest]}
      footer={
        <Survey2023Q3Footer
          presentStep="DataCollection"
          progressStep={progress?.progress as Survey2023Q3Step}
          nextDisabled={!isFinished}
          nextDisabledTooltip={lastPageTooltip}
          prevLabel="Back to Selling and Sharing Survey"
        />
      }
    >
      <Header />

      {groups.length > 1 && (
        <Callout className="mb-md text-t5" variant={CalloutVariant.PurpleBordered}>
          <strong>
            Group {groupIndex + 1} of {groups.length}: {group.name}
          </strong>
        </Callout>
      )}

      <Grid
        container
        wrap="nowrap"
        className="my-mdlg"
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        spacing={1}
      >
        <Grid item>
          <NewReleases color="primary" className={classes.reviewIcon} fontSize="large" />
        </Grid>
        <Grid item xs className="text-t4">
          <strong>
            Step 1: In accordance with new regulations, review the following updates to your data
            collection:
          </strong>
        </Grid>
      </Grid>

      {showNewCategories && (
        <Callout className="mb-lg" variant={CalloutVariant.LightGray}>
          {showGeolocationPIC && (
            <Callout
              className={showNewPICChanges ? "mb-md" : ""}
              variant={CalloutVariant.PurpleBordered}
            >
              <CollectionCheckboxes
                label={
                  "Which, if any, location data do you collect about your " +
                  (group.collectionGroupType === "EMPLOYMENT" ? "employees" : "consumers") +
                  "?"
                }
                group={group}
                orgId={orgId}
                categories={geolocationPIC}
                collected={collected}
                byGroup={false}
                containerClassName="my-sm"
              />
            </Callout>
          )}

          {showNewPICChanges && (
            <Callout variant={CalloutVariant.PurpleBordered}>
              <CollectionCheckboxes
                label="Do you collect any of these new or updated categories of data?"
                group={group}
                orgId={orgId}
                categories={newPICChanges}
                collected={collected}
                byGroup={false}
                containerClassName="my-sm"
              />
            </Callout>
          )}
        </Callout>
      )}

      <Grid
        container
        wrap="nowrap"
        className="my-mdlg"
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        spacing={1}
      >
        <Grid item>
          <CheckCircleIcon color="primary" className={classes.reviewIcon} fontSize="large" />
        </Grid>
        <Grid item xs className="text-t4">
          <strong>Step 2: Review the data you collect about {group.name}</strong>
        </Grid>
      </Grid>

      <Callout
        className="mb-lg"
        variant={CalloutVariant.Purple}
        title={`Data you currently collect from ${group.name}:`}
      ></Callout>
      <ConsumerDataCollectionReview
        dataMap={dataMap}
        org={org}
        collectionGroup={group}
        initialCollectedPIC={initialCollectedPIC}
        orderingVariant={group.collectionGroupType === "EMPLOYMENT" ? "employee" : "default"}
        updateCollection={updateCollection}
      />

      <Grid
        container
        wrap="nowrap"
        className="my-mdlg"
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        spacing={1}
      >
        <Grid item>
          <AddCircleIcon color="primary" className={classes.reviewIcon} fontSize="large" />
        </Grid>
        <Grid item xs className="text-t4">
          <strong>Step 3: Add any new categories to your data collection</strong>
        </Grid>
      </Grid>

      <Stack spacing={3}>
        <ConsumerDataCollectionCheckboxes
          dataMap={dataMap}
          org={org}
          collectionGroup={group}
          orderingVariant={group.collectionGroupType === "EMPLOYMENT" ? "employee" : "default"}
          disabledCategoryIds={initialCollectedPIC}
          disabledTooltipFn={(checked) => {
            if (!checked) {
              return 'To add this category, click "Restore" in the step above.';
            } else {
              return 'To remove this category, click "Remove" in the step above.';
            }
          }}
          hiddenPicKeys={HIDDEN_PIC_KEYS}
        />
      </Stack>

      {sensitiveInferencesQuestion && (
        <div className="mb-xl">
          {!isExpanded && answers[SENSITIVE_INFERENCES_QUESTION_SLUG] && (
            <Table className="mt-mdlg">
              <TableBody>
                <TableRow>
                  <TableCell>
                    <div className="text-component-question">
                      {sensitiveInferencesQuestion.question}
                    </div>
                    {Boolean(sensitiveInferencesQuestion.helpText) && (
                      <p className="text-component-help">{sensitiveInferencesQuestion.helpText}</p>
                    )}
                  </TableCell>
                  <TableCell>
                    <SurveyQuestionShortAnswer
                      key={sensitiveInferencesQuestion.slug}
                      question={sensitiveInferencesQuestion}
                      answer={answers[sensitiveInferencesQuestion.slug]}
                      onAnswer={(answer) =>
                        setAnswer(
                          sensitiveInferencesQuestion.slug,
                          answer,
                          sensitiveInferencesQuestion.plaintextQuestion ??
                            (sensitiveInferencesQuestion.question as string),
                        )
                      }
                    />
                  </TableCell>
                  <TableCell>
                    <Button
                      variant="text"
                      color="secondary"
                      onClick={() => {
                        handleExpand();
                      }}
                      startIcon={<Edit />}
                    >
                      Update my Answer
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          )}
          {(isExpanded || !answers[SENSITIVE_INFERENCES_QUESTION_SLUG]) && (
            <SurveyBooleanQuestion
              answer={answers[SENSITIVE_INFERENCES_QUESTION_SLUG]}
              onAnswer={(ans) => setAnswer(SENSITIVE_INFERENCES_QUESTION_SLUG, ans)}
              question={sensitiveInferencesQuestion as SurveyBooleanQuestionProps}
            />
          )}
        </div>
      )}

      {!isFinished && groupIndex + 1 < groups.length && (
        <>
          <p>
            <strong>
              Next, review your data collection for your group: {groups[groupIndex + 1].name}.
            </strong>
          </p>
          {sensitiveInferencesQuestion && !answers[SENSITIVE_INFERENCES_QUESTION_SLUG] ? (
            <Tooltip title="Answer the question above before proceeding.">
              <span>
                <SurveyButton disabled={true} onClick={nextGroup}>
                  Get Started
                </SurveyButton>
              </span>
            </Tooltip>
          ) : (
            <Grid container>
              <Grid item>
                <SurveyButton disabled={isLoading} onClick={nextGroup}>
                  Get Started
                </SurveyButton>
              </Grid>
              <Grid item className="p-xxxs">
                {isLoading && <CircularProgress />}
              </Grid>
            </Grid>
          )}
        </>
      )}
    </StandardPageWrapper>
  );
};
