import _ from "lodash";
import React, { useMemo } from "react";
import { SurveyAnswers, useSurvey } from "../../../../common/hooks/useSurvey";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q3Nav } from "../../../components/layout/nav/surveys/2023-q3/Survey2023Q3Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { SurveyGateHero } from "../../../components/organisms/survey/SurveyGateHero";
import { useOrganizationVendors, usePrimaryOrganization } from "../../../hooks";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import {
  BUSINESS_SURVEY_NAME,
  businessKidsQuestion,
  businessKidsUnder13Question,
  businessSurvey,
} from "../../../surveys/businessSurvey";
import {
  consumerCollectionIncentivesSurvey,
  CONSUMER_INCENTIVES_SURVEY_NAME,
} from "../../../surveys/consumerCollectionIncentivesSurvey";
import { EEA_UK_SURVEY_NAME } from "../../../surveys/eeaUkSurvey";
import { hrSurvey, HR_SURVEY_NAME } from "../../../surveys/hrSurvey";
import {
  googleAnalyticsQuestion,
  hasIntentionalInteractionVendors,
  intentionalInteractionQuestion,
  intentionalInteractionVendorQuestion,
  sellInExchangeForMoneyQuestion,
  sellingDisclosureQuestion,
  sellingSurvey,
  sellingUploadDataQuestion,
  sellingVendorsQuestion,
} from "../../../surveys/sellingSurvey";
import { customAudienceFeatureQuestion, sharingSurvey } from "../../../surveys/sharingSurvey";
import { Survey2023Q3Step } from "../../../surveys/steps/Survey2023Q3Steps";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../get-compliant/data-recipients/shared";
import { Survey2023Q3Footer } from "../../get-compliant/ProgressFooter";
import { SurveyQuestionReviewTable } from "../../../components/organisms/survey/SurveyReviewTable";
import {
  QuestionReview,
  SurveyQuestionWrapper,
} from "../../../components/organisms/survey/SurveyQuestionWrapper";

export const SURVEY_2023Q3_GATES = "2023q3-survey";

export const Survey2023Q3SellingSharingPage = () => {
  const [org] = usePrimaryOrganization();
  const orgId = org.id;

  const { request, progress } = useSurveyProgress(orgId, "s2023_3");

  const [recipients, recipientsRequest, refreshVendors] = useOrganizationVendors(orgId);

  const {
    answers: gateAnswers,
    setAnswer: setGateAnswers,
    surveyRequest: surveyGateRequest,
  } = useSurvey(orgId, SURVEY_2023Q3_GATES, refreshVendors);

  const {
    answers: bizAnswers,
    setAnswer: bizSetAnswer,
    surveyRequest: bizRequest,
    updateRequest: bizUpdateRequest,
  } = useSurvey(orgId, BUSINESS_SURVEY_NAME, refreshVendors);
  const {
    answers: eeaAnswers,
    setAnswer: eeaSetAnswer,
    surveyRequest: eeaRequest,
    updateRequest: eeaUpdateRequest,
  } = useSurvey(orgId, EEA_UK_SURVEY_NAME, refreshVendors);
  const {
    answers: hrAnswers,
    setAnswer: hrSetAnswer,
    surveyRequest: hrRequest,
    updateRequest: hrUpdateRequest,
  } = useSurvey(orgId, HR_SURVEY_NAME, refreshVendors);
  const {
    answers: sharingAnswers,
    setAnswer: sharingSetAnswer,
    surveyRequest: sharingRequest,
    updateRequest: sharingUpdateRequest,
  } = useSurvey(orgId, SELLING_AND_SHARING_SURVEY_NAME, refreshVendors);
  const {
    answers: incentivesAnswers,
    setAnswer: incentivesSetAnswer,
    surveyRequest: incentivesRequest,
    updateRequest: incentivesUpdateRequest,
  } = useSurvey(orgId, CONSUMER_INCENTIVES_SURVEY_NAME, refreshVendors);

  if (hasIntentionalInteractionVendors(recipients)) {
    sharingAnswers["disclosure-for-intentional-interaction"] = "true";
  }

  const selling = useMemo(() => {
    sharingAnswers["selling-definition-acknowledgement"] = "true";
    return sellingSurvey(sharingAnswers, recipients);
  }, [sharingAnswers, recipients]);
  const sharing = useMemo(() => {
    sharingAnswers["sharing-definition-acknowledgement"] = "true";
    return sharingSurvey(sharingAnswers, recipients);
  }, [sharingAnswers, recipients]);
  const bizSurveyDefinition = useMemo(() => {
    return businessSurvey(bizAnswers);
  }, [sharingAnswers, recipients]);
  const hrSurveyDefinition = useMemo(() => {
    return hrSurvey(hrAnswers, org.featureGdpr);
  }, [hrAnswers, org.featureGdpr]);
  const incentivesSurvey = useMemo(() => {
    return consumerCollectionIncentivesSurvey(incentivesAnswers, true);
  }, [incentivesAnswers]);

  const questions = useMemo(
    () => constructReviewQuestions(org, hrAnswers || {}, recipients || []),
    [org, recipients],
  );

  const surveys = useMemo(() => {
    return {
      [SURVEY_2023Q3_GATES]: gateAnswers,
      [BUSINESS_SURVEY_NAME]: bizAnswers,
      [HR_SURVEY_NAME]: hrAnswers,
      [EEA_UK_SURVEY_NAME]: eeaAnswers,
      [SELLING_AND_SHARING_SURVEY_NAME]: sharingAnswers,
      [CONSUMER_INCENTIVES_SURVEY_NAME]: incentivesAnswers,
    };
  }, [gateAnswers, bizAnswers, eeaAnswers, sharingAnswers, incentivesAnswers]);

  const updaters = useMemo(() => {
    return {
      [SURVEY_2023Q3_GATES]: setGateAnswers,
      [BUSINESS_SURVEY_NAME]: bizSetAnswer,
      [HR_SURVEY_NAME]: hrSetAnswer,
      [EEA_UK_SURVEY_NAME]: eeaSetAnswer,
      [SELLING_AND_SHARING_SURVEY_NAME]: sharingSetAnswer,
      [CONSUMER_INCENTIVES_SURVEY_NAME]: incentivesSetAnswer,
    };
  }, [setGateAnswers, bizSetAnswer, eeaSetAnswer, sharingSetAnswer, incentivesSetAnswer]);

  const visibility = useMemo(() => {
    return {
      [CONSUMER_INCENTIVES_SURVEY_NAME]: incentivesSurvey.visibility,
      [HR_SURVEY_NAME]: hrSurveyDefinition.visibility,
      [SELLING_AND_SHARING_SURVEY_NAME]: _.merge({}, selling.visibility, sharing.visibility),
      [BUSINESS_SURVEY_NAME]: bizSurveyDefinition.visibility,
    };
  }, [
    incentivesSurvey.visibility,
    selling.visibility,
    sharing.visibility,
    bizSurveyDefinition.visibility,
    hrSurveyDefinition.visibility,
  ]);

  const disabledReasons = useMemo(() => {
    return {
      [CONSUMER_INCENTIVES_SURVEY_NAME]: incentivesSurvey.disabled,
      [SELLING_AND_SHARING_SURVEY_NAME]: _.merge({}, selling.disabled, sharing.disabled),
    };
  }, [incentivesSurvey.disabled, selling.disabled, sharing.disabled]);

  const isFinished = useMemo(
    () =>
      questions.every((q) => {
        const hasAnswered =
          surveys[q.survey]?.[q.question.slug] != undefined &&
          surveys[q.survey]?.[q.question.slug] != "" &&
          surveys[q.survey]?.[q.question.slug] != "[]";
        const visible = visibility[q.survey]?.[q.question.slug] ?? true;
        return hasAnswered || !visible;
      }) &&
      gateAnswers["business-survey-intro"] === "true" &&
      (!hasVisibleUnansweredQuestions ||
        (gateAnswers["selling-sharing-done-with-review"] === "true" &&
          gateAnswers["selling-sharing-new-questions"] === "true")),
    [questions, surveys, visibility],
  );

  // Why aren't we just differentiating between questions with or without answers?
  const unansweredQuestionsCompositeKeys: string[] = useMemo(
    () => JSON.parse(gateAnswers?.["unanswered-survey-questions"] || "[]"),
    [questions, gateAnswers],
  );

  const [unansweredQuestions, reviewQuestions] = useMemo(
    () =>
      _.partition(questions, (q) => {
        const compositeKey = `${q.survey}/${q.question.slug}`;
        return unansweredQuestionsCompositeKeys.includes(compositeKey);
      }),
    [unansweredQuestionsCompositeKeys, questions],
  );

  const hasVisibleUnansweredQuestions = useMemo(
    () =>
      unansweredQuestions.length > 0 &&
      unansweredQuestions.some((q) => visibility[q.survey][q.question.slug]),
    [visibility, unansweredQuestions],
  );

  const expanded: string[] = useMemo(
    () => JSON.parse(gateAnswers?.["updated-survey-questions"] || "[]"),
    [gateAnswers],
  );

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q3Nav progress={progress?.progress as Survey2023Q3Step} />}
      requests={[
        request,
        recipientsRequest,
        surveyGateRequest,
        bizRequest,
        eeaRequest,
        hrRequest,
        sharingRequest,
        incentivesRequest,
      ]}
      footer={
        <Survey2023Q3Footer
          presentStep="SellingSharing"
          progressStep={progress?.progress as Survey2023Q3Step}
          nextDisabled={!isFinished}
          nextDisabledTooltip="Please be sure every question has an answer."
          noPrev
          saveRequests={[
            bizUpdateRequest,
            eeaUpdateRequest,
            hrUpdateRequest,
            sharingUpdateRequest,
            incentivesUpdateRequest,
          ]}
        />
      }
    >
      <PageHeader titleContent="Compliance Checkup" />

      <SurveyGateHero
        slug="business-survey-intro"
        survey={gateAnswers}
        updateResponse={setGateAnswers}
        label="Get Started"
        description={
          <>
            First up, let’s review your previous data selling and sharing answers. If your answers
            have changed, click <strong>Update my Answer</strong>.
          </>
        }
        imageUrl="/assets/images/surveys/2023q1/business-survey-hero.svg"
      >
        <SurveyQuestionReviewTable
          surveys={surveys}
          surveyUpdaters={updaters}
          visibility={visibility}
          disabledReasons={disabledReasons}
          reviewQuestions={reviewQuestions}
          expanded={expanded}
          surveyGateSlug={SURVEY_2023Q3_GATES}
        />

        {hasVisibleUnansweredQuestions && (
          <>
            <div className="mt-md" />

            <SurveyGateButton
              slug="selling-sharing-done-with-review"
              survey={gateAnswers}
              updateResponse={setGateAnswers}
              label="Done with Review"
            >
              <SurveyGateHero
                slug="selling-sharing-new-questions"
                survey={gateAnswers}
                updateResponse={setGateAnswers}
                label="Got it"
                description="Next we have some new questions. These are questions we have added since the last time you went through Get Compliant."
                imageUrl="/assets/images/surveys/2023q1/business-survey-new-questions-hero.svg"
              >
                {unansweredQuestions.map((q) => {
                  return (
                    <SurveyQuestionWrapper
                      surveys={surveys}
                      surveyUpdaters={updaters}
                      visibility={visibility}
                      disabledReasons={disabledReasons}
                      review={q}
                      key={q.question.slug}
                    />
                  );
                })}
              </SurveyGateHero>
            </SurveyGateButton>
          </>
        )}
      </SurveyGateHero>
    </StandardPageWrapper>
  );
};

const constructReviewQuestions = (
  org: OrganizationDto,
  hrAnswers: SurveyAnswers,
  dataRecipients: OrganizationDataRecipientDto[],
): QuestionReview[] => {
  return [
    {
      survey: SELLING_AND_SHARING_SURVEY_NAME,
      question: intentionalInteractionQuestion(dataRecipients),
    },
    {
      survey: SELLING_AND_SHARING_SURVEY_NAME,
      question: intentionalInteractionVendorQuestion(dataRecipients),
    },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: googleAnalyticsQuestion() },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: sellingDisclosureQuestion() },
    { survey: BUSINESS_SURVEY_NAME, question: businessKidsQuestion() },
    {
      survey: BUSINESS_SURVEY_NAME,
      question: businessKidsUnder13Question(),
      visibility: (answers) => answers[businessKidsQuestion().slug],
    },
    {
      survey: SELLING_AND_SHARING_SURVEY_NAME,
      question: customAudienceFeatureQuestion(dataRecipients),
    },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: sellInExchangeForMoneyQuestion() },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: sellingVendorsQuestion(dataRecipients) },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: sellingUploadDataQuestion() },
  ].filter((x) => x);
};
