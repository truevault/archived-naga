import React, { useMemo } from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { Api } from "../../../../common/service/Api";
import { TaskDto } from "../../../../common/service/server/dto/TaskDto";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q1Nav } from "../../../components/layout/nav/surveys/2023-q1/Survey2023Q1Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { SurveyGateHero } from "../../../components/organisms/survey/SurveyGateHero";
import { EmptyTodoTasks, TaskCard } from "../../../components/organisms/tasks/TaskCard";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { useTasks } from "../../../hooks/useTasks";
import { Survey2023Q1Step } from "../../../surveys/steps/Survey2023Q1Steps";
import { todoTaskComparitor } from "../../../util/tasks";
import { Survey2023Q1Footer } from "../../get-compliant/ProgressFooter";
import { SURVEY_2023Q1_GATES } from "./BusinessSurveyPage";

export const Survey2023Q1TasksPage = () => {
  const orgId = usePrimaryOrganizationId();
  const { request, progress } = useSurveyProgress(orgId, "s2023_1");

  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, SURVEY_2023Q1_GATES);
  const [tasks, tasksRequest] = useTasks(orgId, "active");
  const sortedTasks = useMemo(() => tasks?.sort(todoTaskComparitor), [tasks]);

  const isFinished = answers["tasks-intro-got-it"] === "true";

  const handleFinishSurvey = async () => {
    // todo: finish the survey...
    await Api.organizationProgress.completeSurvey(orgId, "s2023_1");

    // navigate to slash
    window.location.href = "/";

    // cancel the standard progress actions
    return true;
  };

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q1Nav progress={progress?.progress as Survey2023Q1Step} />}
      requests={[request, surveyRequest, tasksRequest]}
      footer={
        <Survey2023Q1Footer
          presentStep="Tasks"
          progressStep={progress?.progress as Survey2023Q1Step}
          nextDisabled={!isFinished}
          nextOnClick={handleFinishSurvey}
          nextLabel="Done"
          prevLabel="Back to Data Recipients"
        />
      }
    >
      <PageHeader titleContent="Next Steps" />

      <SurveyGateHero
        slug="tasks-intro-got-it"
        survey={answers}
        updateResponse={setAnswer}
        label="Got it"
        description="Last, let’s take a quick look at your open tasks. These items will be listed in your Task List until they are completed."
        imageUrl="/assets/images/surveys/2023q1/tasks-hero.svg"
      >
        <TaskBody tasks={sortedTasks} />
      </SurveyGateHero>
    </StandardPageWrapper>
  );
};

const TaskBody: React.FC<{ tasks: TaskDto[] }> = ({ tasks }) => {
  if (!tasks?.length) {
    return <EmptyTodoTasks />;
  }

  return (
    <>
      {tasks.map((t) => (
        <div className="mb-md" key={t.id}>
          <TaskCard task={t} />
        </div>
      ))}
    </>
  );
};
