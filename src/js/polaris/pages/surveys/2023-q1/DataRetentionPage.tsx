import { Stack } from "@mui/material";
import React, { useMemo } from "react";

import { SurveyBooleanQuestion } from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q1Nav } from "../../../components/layout/nav/surveys/2023-q1/Survey2023Q1Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import {
  DataRetentionModule,
  useDataRetentionModule,
} from "../../../components/organisms/data-retention/DataRetentionModule";
import { SurveyGateHero } from "../../../components/organisms/survey/SurveyGateHero";
import { useOrganizationVendors, usePrimaryOrganizationId } from "../../../hooks";
import { useRequestInstructions } from "../../../hooks/useOrganizationVendors";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { Survey2023Q1Step } from "../../../surveys/steps/Survey2023Q1Steps";
import { Survey2023Q1Footer } from "../../get-compliant/ProgressFooter";
import { RowVendorTile } from "../../stay-compliant/data-recipients/RowVendorTile";

const COLLECTION_CONSISTENT_WITH_EXPECTATIONS_SLUG =
  "all-collected-data-consistent-with-consumer-expectations";

export const Survey2023Q1DataRetentionPage = () => {
  const orgId = usePrimaryOrganizationId();
  const dataRetention = useDataRetentionModule("data-retention-2023-q1-catchup-done-with-review");
  const { request: progressRequest, progress } = useSurveyProgress(orgId, "s2023_1");

  const [recipients, recipientsRequest] = useOrganizationVendors(orgId);
  const [instructions, instructionsRequest] = useRequestInstructions(orgId, "DELETE");

  const { answers, setAnswer } = dataRetention.props;

  const internalUseRecipients = useMemo(
    () =>
      recipients?.filter((r) =>
        instructions
          ?.find((i) => i.vendorId == r.vendorId)
          ?.retentionReasons?.includes("reasonable-internal-uses"),
      ) || [],
    [recipients, instructions],
  );
  const doneWithAttestation =
    internalUseRecipients.length == 0 || answers[COLLECTION_CONSISTENT_WITH_EXPECTATIONS_SLUG];

  const isFinished = doneWithAttestation && dataRetention.isFinished;

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q1Nav progress={progress?.progress as Survey2023Q1Step} />}
      requests={[
        progressRequest,
        recipientsRequest,
        instructionsRequest,
        dataRetention.requests.dataMapRequest,
        dataRetention.requests.dataRetentionRequest,
        dataRetention.requests.surveyRequest,
      ]}
      footer={
        <Survey2023Q1Footer
          presentStep="DataRetention"
          progressStep={progress?.progress as Survey2023Q1Step}
          nextDisabled={!isFinished}
          nextDisabledTooltip="Please make sure every collection category is selected and every selected purpose has a written description."
          prevLabel="Back to Collection Purposes"
        />
      }
    >
      <PageHeader titleContent="Retention Checkup" />

      <SurveyGateHero
        slug="data-retention-intro-got-it"
        survey={answers}
        updateResponse={setAnswer}
        label="Got it"
        description="Updated privacy laws have provided further guidance and requirements on data retention practices. The following questions are about your data retention policies, and will be be published in an updated privacy notice."
        imageUrl="/assets/images/surveys/2023q1/retention-hero.svg"
      >
        {internalUseRecipients.length > 0 && (
          <>
            <SurveyBooleanQuestion
              answer={answers[COLLECTION_CONSISTENT_WITH_EXPECTATIONS_SLUG]}
              onAnswer={(ans) => setAnswer(COLLECTION_CONSISTENT_WITH_EXPECTATIONS_SLUG, ans)}
              question={{
                type: "boolean",
                slug: COLLECTION_CONSISTENT_WITH_EXPECTATIONS_SLUG,
                question: (
                  <>
                    New regulations have narrowed the scope of one of the exceptions to deletion
                    that your business relies on to retain data. You previously indicated that your
                    business will retain data with the following vendor(s) for the purpose of “
                    <strong>reasonable internal uses</strong>”. Is your business’s use of this data{" "}
                    <strong>consistent with consumer expectations</strong> at time the data was
                    collected?
                  </>
                ),
                helpText: (
                  <>
                    <p>
                      If you answer ‘no,' we’ll prompt you to review your retention settings for
                      these vendors in a later step.
                    </p>
                    <Stack spacing={2}>
                      {internalUseRecipients.map((r) => (
                        <RowVendorTile logoUrl={r.logoUrl} name={r.name} key={r.id} />
                      ))}
                    </Stack>
                  </>
                ),
                visible: true,
              }}
            />
          </>
        )}

        {doneWithAttestation && (
          <>
            <div className="mt-xl">
              <p>
                Privacy laws allow your business to retain consumer data only as long as necessary
                to serve the purpose for which it was collected. For each category of information
                you collect, your privacy policy must disclose either (a) the period for which your
                business stores personal data, or (b) the criteria used to determine that period.
              </p>
              <p>
                In this step, we’ll help you establish your business’s retention period for the data
                it collects. This table will be included in your online Privacy Notice.
              </p>
            </div>
            <DataRetentionModule {...dataRetention.props} />
          </>
        )}
      </SurveyGateHero>
    </StandardPageWrapper>
  );
};
