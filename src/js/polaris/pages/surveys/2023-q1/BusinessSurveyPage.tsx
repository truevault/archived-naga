import { Edit } from "@mui/icons-material";
import { Button, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import _ from "lodash";
import React, { useMemo } from "react";
import { SurveyAnswerFn, SurveyAnswers, useSurvey } from "../../../../common/hooks/useSurvey";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { EmptyInfo } from "../../../components/Empty";
import { SurveyQuestionShortAnswer } from "../../../components/get-compliant/survey/answers/SurveyQuestionShortAnswer";
import {
  SurveyQuestion,
  SurveyQuestionProps,
} from "../../../components/get-compliant/survey/SurveyQuestion";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q1Nav } from "../../../components/layout/nav/surveys/2023-q1/Survey2023Q1Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { SurveyGateHero } from "../../../components/organisms/survey/SurveyGateHero";
import { useOrganizationVendors, usePrimaryOrganization } from "../../../hooks";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import {
  businessCaliforniaNonenglishQuestion,
  businessCollectionForOtherBusinessesQuestion,
  businessFcraCompliantQuestion,
  businessGlbaCompliantQuestion,
  businessHipaaCompliantQuestion,
  businessKidsQuestion,
  businessMobileAppsQuestion,
  businessServiceProviderQuestion,
  businessTenMillionCaliforniansQuestion,
  BUSINESS_SURVEY_NAME,
  physicalLocationQuestion,
} from "../../../surveys/businessSurvey";
import {
  collectedDataIncentivesExistsQuestion,
  consumerCollectionIncentivesSurvey,
  CONSUMER_INCENTIVES_SURVEY_NAME,
} from "../../../surveys/consumerCollectionIncentivesSurvey";
import {
  EEA_UK_SURVEY_NAME,
  establishedQuestion,
  largeScaleQuestion,
  nonEnglishQuestion,
} from "../../../surveys/eeaUkSurvey";
import {
  californiaContractorsQuestion,
  californiaEmployeesQuestion,
  californiaJobApplicantsQuestion,
  eeaUkEmployeesQuestion,
  hrSurvey,
  HR_SURVEY_NAME,
} from "../../../surveys/hrSurvey";
import {
  createProfilesAboutConsumersQuestion,
  googleAnalyticsQuestion,
  hasIntentionalInteractionVendors,
  intentionalInteractionQuestion,
  intentionalInteractionVendorQuestion,
  sellInExchangeForMoneyQuestion,
  sellingDisclosureQuestion,
  sellingSurvey,
  sellingUploadDataQuestion,
  sellingUploadVendorsSelectionQuestion,
  sellingVendorsQuestion,
  shopifyAudiencesQuestion,
} from "../../../surveys/sellingSurvey";
import {
  customAudienceFeatureQuestion,
  shareDataQuestion,
  sharingSurvey,
  vendorSharingCategories,
} from "../../../surveys/sharingSurvey";
import { Survey2023Q1Step } from "../../../surveys/steps/Survey2023Q1Steps";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../get-compliant/data-recipients/shared";
import { Survey2023Q1Footer } from "../../get-compliant/ProgressFooter";

export const SURVEY_2023Q1_GATES = "2023q1-survey";

export const Survey2023Q1BusinessSurveyPage = () => {
  const [org] = usePrimaryOrganization();
  const orgId = org.id;

  const { request, progress } = useSurveyProgress(orgId, "s2023_1");

  const [recipients, recipientsRequest, refreshVendors] = useOrganizationVendors(orgId);

  const {
    answers: gateAnswers,
    setAnswer: setGateAnswers,
    surveyRequest: surveyGateRequest,
  } = useSurvey(orgId, SURVEY_2023Q1_GATES, refreshVendors);

  const {
    answers: bizAnswers,
    setAnswer: bizSetAnswer,
    surveyRequest: bizRequest,
    updateRequest: bizUpdateRequest,
  } = useSurvey(orgId, BUSINESS_SURVEY_NAME, refreshVendors);
  const {
    answers: eeaAnswers,
    setAnswer: eeaSetAnswer,
    surveyRequest: eeaRequest,
    updateRequest: eeaUpdateRequest,
  } = useSurvey(orgId, EEA_UK_SURVEY_NAME, refreshVendors);
  const {
    answers: hrAnswers,
    setAnswer: hrSetAnswer,
    surveyRequest: hrRequest,
    updateRequest: hrUpdateRequest,
  } = useSurvey(orgId, HR_SURVEY_NAME, refreshVendors);
  const {
    answers: sharingAnswers,
    setAnswer: sharingSetAnswer,
    surveyRequest: sharingRequest,
    updateRequest: sharingUpdateRequest,
  } = useSurvey(orgId, SELLING_AND_SHARING_SURVEY_NAME, refreshVendors);
  const {
    answers: incentivesAnswers,
    setAnswer: incentivesSetAnswer,
    surveyRequest: incentivesRequest,
    updateRequest: incentivesUpdateRequest,
  } = useSurvey(orgId, CONSUMER_INCENTIVES_SURVEY_NAME, refreshVendors);

  if (hasIntentionalInteractionVendors(recipients)) {
    sharingAnswers["disclosure-for-intentional-interaction"] = "true";
  }

  const selling = useMemo(() => {
    sharingAnswers["selling-definition-acknowledgement"] = "true";
    return sellingSurvey(sharingAnswers, recipients);
  }, [sharingAnswers, recipients]);
  const sharing = useMemo(() => {
    sharingAnswers["sharing-definition-acknowledgement"] = "true";
    return sharingSurvey(sharingAnswers, recipients);
  }, [sharingAnswers, recipients]);

  const hrSurveyDefinition = useMemo(() => {
    return hrSurvey(hrAnswers, org.featureGdpr);
  }, [hrAnswers, org.featureGdpr]);
  const incentivesSurvey = useMemo(() => {
    return consumerCollectionIncentivesSurvey(incentivesAnswers, true);
  }, [incentivesAnswers]);

  const questions = useMemo(
    () => constructReviewQuestions(org, hrAnswers || {}, recipients || []),
    [org, recipients],
  );

  const surveys = useMemo(() => {
    return {
      [SURVEY_2023Q1_GATES]: gateAnswers,
      [BUSINESS_SURVEY_NAME]: bizAnswers,
      [HR_SURVEY_NAME]: hrAnswers,
      [EEA_UK_SURVEY_NAME]: eeaAnswers,
      [SELLING_AND_SHARING_SURVEY_NAME]: sharingAnswers,
      [CONSUMER_INCENTIVES_SURVEY_NAME]: incentivesAnswers,
    };
  }, [gateAnswers, bizAnswers, eeaAnswers, sharingAnswers, incentivesAnswers]);

  const updaters = useMemo(() => {
    return {
      [SURVEY_2023Q1_GATES]: setGateAnswers,
      [BUSINESS_SURVEY_NAME]: bizSetAnswer,
      [HR_SURVEY_NAME]: hrSetAnswer,
      [EEA_UK_SURVEY_NAME]: eeaSetAnswer,
      [SELLING_AND_SHARING_SURVEY_NAME]: sharingSetAnswer,
      [CONSUMER_INCENTIVES_SURVEY_NAME]: incentivesSetAnswer,
    };
  }, [setGateAnswers, bizSetAnswer, eeaSetAnswer, sharingSetAnswer, incentivesSetAnswer]);

  const visibility = useMemo(() => {
    return {
      [CONSUMER_INCENTIVES_SURVEY_NAME]: incentivesSurvey.visibility,
      [HR_SURVEY_NAME]: hrSurveyDefinition.visibility,
      [SELLING_AND_SHARING_SURVEY_NAME]: _.merge({}, selling.visibility, sharing.visibility),
    };
  }, [incentivesSurvey.visibility, selling.visibility, sharing.visibility]);

  const disabledReasons = useMemo(() => {
    return {
      [CONSUMER_INCENTIVES_SURVEY_NAME]: incentivesSurvey.disabled,
      [SELLING_AND_SHARING_SURVEY_NAME]: _.merge({}, selling.disabled, sharing.disabled),
    };
  }, [incentivesSurvey.disabled, selling.disabled, sharing.disabled]);

  const allAnswered = useMemo(
    () =>
      questions.every((q) => {
        const hasAnswered =
          surveys[q.survey]?.[q.question.slug] != undefined &&
          surveys[q.survey]?.[q.question.slug] != "[]";
        const visible = visibility[q.survey]?.[q.question.slug] ?? true;

        return hasAnswered || !visible;
      }),
    [questions, surveys, visibility],
  );

  const isFinished = gateAnswers["business-survey-new-questions"] === "true" && allAnswered;

  const unansweredQuestionsCompositeKeys: string[] = useMemo(
    () => JSON.parse(gateAnswers?.["unanswered-survey-questions"] || "[]"),
    [gateAnswers],
  );

  const [unansweredQuestions, reviewQuestions] = useMemo(
    () =>
      _.partition(questions, (q) => {
        const compositeKey = `${q.survey}/${q.question.slug}`;
        return unansweredQuestionsCompositeKeys.includes(compositeKey);
      }),
    [unansweredQuestionsCompositeKeys, questions],
  );

  const expanded: string[] = useMemo(
    () => JSON.parse(gateAnswers?.["updated-survey-questions"] || "[]"),
    [gateAnswers],
  );

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q1Nav progress={progress?.progress as Survey2023Q1Step} />}
      requests={[
        request,
        recipientsRequest,
        surveyGateRequest,
        bizRequest,
        eeaRequest,
        hrRequest,
        sharingRequest,
        incentivesRequest,
      ]}
      footer={
        <Survey2023Q1Footer
          presentStep="BusinessSurvey"
          progressStep={progress?.progress as Survey2023Q1Step}
          nextDisabled={!isFinished}
          nextDisabledTooltip="Please be sure every question has an answer."
          noPrev
          saveRequests={[
            bizUpdateRequest,
            eeaUpdateRequest,
            hrUpdateRequest,
            sharingUpdateRequest,
            incentivesUpdateRequest,
          ]}
        />
      }
    >
      <PageHeader titleContent="Compliance Checkup" />

      <SurveyGateHero
        slug="business-survey-intro"
        survey={gateAnswers}
        updateResponse={setGateAnswers}
        label="Get Started"
        description={
          <>
            First up, let’s review the answers you gave last time. If your answers have changed,
            click <strong>Update my Answer</strong>.
          </>
        }
        imageUrl="/assets/images/surveys/2023q1/business-survey-hero.svg"
      >
        <Table className="mt-mdlg">
          <TableHead>
            <TableRow>
              <TableCell width={420}>Question</TableCell>
              <TableCell>Previous Answer</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {reviewQuestions.length === 0 ? (
              <TableRow>
                <TableCell colSpan={99999}>
                  <EmptyInfo>You have no survey questions to review</EmptyInfo>
                </TableCell>
              </TableRow>
            ) : (
              <>
                {reviewQuestions.map((q) => {
                  return (
                    <SurveyQuestionRow
                      surveys={surveys}
                      surveyUpdaters={updaters}
                      visibility={visibility}
                      disabledReasons={disabledReasons}
                      review={q}
                      expanded={expanded}
                      key={q.question.slug}
                    />
                  );
                })}
              </>
            )}
          </TableBody>
        </Table>

        <div className="mt-md" />

        <SurveyGateButton
          slug="business-survey-done-with-review"
          survey={gateAnswers}
          updateResponse={setGateAnswers}
          label="Done with Review"
        >
          <SurveyGateHero
            slug="business-survey-new-questions"
            survey={gateAnswers}
            updateResponse={setGateAnswers}
            label="Got it"
            description="Next we have some new questions. These are questions we have added since the last time you went through Get Compliant."
            imageUrl="/assets/images/surveys/2023q1/business-survey-new-questions-hero.svg"
          >
            {unansweredQuestions.map((q) => {
              return (
                <SurveyQuestionWrapper
                  surveys={surveys}
                  surveyUpdaters={updaters}
                  visibility={visibility}
                  disabledReasons={disabledReasons}
                  review={q}
                  key={q.question.slug}
                />
              );
            })}
          </SurveyGateHero>
        </SurveyGateButton>
      </SurveyGateHero>
    </StandardPageWrapper>
  );
};

type QuestionReview = { survey: string; question: SurveyQuestionProps; onlyGdpr?: boolean };

const constructReviewQuestions = (
  org: OrganizationDto,
  hrAnswers: SurveyAnswers,
  dataRecipients: OrganizationDataRecipientDto[],
): QuestionReview[] => {
  return [
    { survey: BUSINESS_SURVEY_NAME, question: physicalLocationQuestion() },
    { survey: BUSINESS_SURVEY_NAME, question: businessKidsQuestion() },
    { survey: BUSINESS_SURVEY_NAME, question: businessMobileAppsQuestion() },
    { survey: BUSINESS_SURVEY_NAME, question: businessCaliforniaNonenglishQuestion() },
    { survey: BUSINESS_SURVEY_NAME, question: businessTenMillionCaliforniansQuestion() },
    { survey: BUSINESS_SURVEY_NAME, question: businessCollectionForOtherBusinessesQuestion() },
    { survey: BUSINESS_SURVEY_NAME, question: businessServiceProviderQuestion() },

    { survey: BUSINESS_SURVEY_NAME, question: businessHipaaCompliantQuestion() },
    { survey: BUSINESS_SURVEY_NAME, question: businessGlbaCompliantQuestion() },
    { survey: BUSINESS_SURVEY_NAME, question: businessFcraCompliantQuestion() },
    { survey: HR_SURVEY_NAME, question: californiaEmployeesQuestion(hrAnswers) },
    { survey: HR_SURVEY_NAME, question: californiaContractorsQuestion(hrAnswers) },
    { survey: HR_SURVEY_NAME, question: californiaJobApplicantsQuestion(hrAnswers) },

    org.featureGdpr ? { survey: EEA_UK_SURVEY_NAME, question: largeScaleQuestion() } : null,
    org.featureGdpr
      ? { survey: HR_SURVEY_NAME, question: eeaUkEmployeesQuestion(hrAnswers) }
      : null,
    org.featureGdpr ? { survey: EEA_UK_SURVEY_NAME, question: establishedQuestion() } : null,
    org.featureGdpr ? { survey: EEA_UK_SURVEY_NAME, question: nonEnglishQuestion() } : null,

    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: shareDataQuestion() },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: vendorSharingCategories(dataRecipients) },
    // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
    // { survey: SELLING_AND_SHARING_SURVEY_NAME, question: facebookLduQuestion() },
    // { survey: SELLING_AND_SHARING_SURVEY_NAME, question: googleRdpQuestion() },
    {
      survey: SELLING_AND_SHARING_SURVEY_NAME,
      question: customAudienceFeatureQuestion(dataRecipients),
    },
    {
      survey: SELLING_AND_SHARING_SURVEY_NAME,
      question: intentionalInteractionQuestion(dataRecipients),
    },
    {
      survey: SELLING_AND_SHARING_SURVEY_NAME,
      question: intentionalInteractionVendorQuestion(dataRecipients),
    },

    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: googleAnalyticsQuestion() },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: shopifyAudiencesQuestion() },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: createProfilesAboutConsumersQuestion() },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: sellInExchangeForMoneyQuestion() },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: sellingDisclosureQuestion() },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: sellingVendorsQuestion(dataRecipients) },
    { survey: SELLING_AND_SHARING_SURVEY_NAME, question: sellingUploadDataQuestion() },
    {
      survey: SELLING_AND_SHARING_SURVEY_NAME,
      question: sellingUploadVendorsSelectionQuestion(dataRecipients),
    },

    { survey: CONSUMER_INCENTIVES_SURVEY_NAME, question: collectedDataIncentivesExistsQuestion() },
  ].filter((x) => x) as QuestionReview[];
};

type BundledSurveys = Record<string, SurveyAnswers>;
type BundledSurveyUpdates = Record<string, SurveyAnswerFn>;
type BundledSurveyVisibility = Record<string, Record<string, boolean>>;
type BundledDisabledReasons = Record<string, Record<string, string | boolean>>;

type SurveyQuestionRowProps = {
  surveys: BundledSurveys;
  surveyUpdaters: BundledSurveyUpdates;
  visibility: BundledSurveyVisibility;
  disabledReasons: BundledDisabledReasons;
  review: QuestionReview;
  expanded: string[];
};

const SurveyQuestionRow: React.FC<SurveyQuestionRowProps> = ({
  surveys,
  surveyUpdaters,
  visibility,
  disabledReasons,
  review,
  expanded,
}) => {
  const isExpanded = useMemo(
    () => expanded.includes(review.question.slug),
    [expanded, review.question.slug],
  );

  const handleExpand = () => {
    const updatedArr = [...expanded];
    updatedArr.push(review.question.slug);
    surveyUpdaters[SURVEY_2023Q1_GATES]("updated-survey-questions", JSON.stringify(updatedArr));
  };

  const visible = visibility[review.survey]?.[review.question.slug] ?? true;

  if (!visible) {
    return null;
  }

  return isExpanded ? (
    <SurveyQuestionRowWrapper
      surveys={surveys}
      surveyUpdaters={surveyUpdaters}
      review={review}
      visibility={visibility}
      disabledReasons={disabledReasons}
    />
  ) : (
    <SurveyQuestionReviewRow
      surveys={surveys}
      surveyUpdaters={surveyUpdaters}
      visibility={visibility}
      disabledReasons={disabledReasons}
      review={review}
      onExpand={handleExpand}
    />
  );
};

type SurveyQuestionRowWrapperProps = {
  surveys: BundledSurveys;
  surveyUpdaters: BundledSurveyUpdates;
  review: QuestionReview;
  visibility: BundledSurveyVisibility;
  disabledReasons: BundledDisabledReasons;
};

// Review mode in a table row
const SurveyQuestionReviewRow: React.FC<
  SurveyQuestionRowWrapperProps & { onExpand: () => void }
> = ({ surveys, surveyUpdaters, review, onExpand }) => {
  const answers = surveys[review.survey];
  const handleAnswer = surveyUpdaters[review.survey];
  const q = review.question;

  return (
    <TableRow>
      <TableCell>
        <div className="text-component-question">{q.question}</div>
        {Boolean(q.helpText) && <p className="text-component-help">{q.helpText}</p>}
      </TableCell>
      <TableCell>
        <SurveyQuestionShortAnswer
          key={q.slug}
          question={q}
          answer={answers[q.slug]}
          onAnswer={(answer) =>
            handleAnswer(q.slug, answer, q.plaintextQuestion ?? (q.question as string))
          }
        />
      </TableCell>
      <TableCell>
        <Button
          variant="text"
          color="secondary"
          onClick={() => {
            onExpand();
          }}
          startIcon={<Edit />}
        >
          Update my Answer
        </Button>
      </TableCell>
    </TableRow>
  );
};

// Edit mode in a table row
const SurveyQuestionRowWrapper: React.FC<SurveyQuestionRowWrapperProps> = ({
  surveys,
  surveyUpdaters,
  review,
  visibility,
  disabledReasons,
}) => {
  return (
    <TableRow>
      <TableCell colSpan={3}>
        <SurveyQuestionWrapper
          surveys={surveys}
          surveyUpdaters={surveyUpdaters}
          review={{ survey: review.survey, question: { ...review.question, visible: true } }}
          visibility={visibility}
          disabledReasons={disabledReasons}
        />
      </TableCell>
    </TableRow>
  );
};

const SurveyQuestionWrapper: React.FC<SurveyQuestionRowWrapperProps> = ({
  surveys,
  surveyUpdaters,
  review,
  visibility,
  disabledReasons = {},
}) => {
  const answers = surveys[review.survey];
  const handleAnswer = surveyUpdaters[review.survey];
  const q = review.question;
  const visible = visibility[review.survey]?.[q.slug] ?? true;
  const disabledReason = disabledReasons[review.survey]?.[q.slug];
  const isDisabled = Boolean(disabledReason);

  if (!visible) {
    return null;
  }

  return (
    <SurveyQuestion
      key={q.slug}
      question={{
        ...q,
        visible: visible,
        isDisabled,
        disabledReason: typeof disabledReason === "string" ? disabledReason : undefined,
      }}
      answer={answers[q.slug]}
      onAnswer={(answer) => handleAnswer(q.slug, answer, q.plaintextQuestion)}
    />
  );
};
