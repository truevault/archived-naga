import React, { useState } from "react";
import { useAllCollectedPIC } from "../../../../common/hooks/dataMapHooks";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q1Nav } from "../../../components/layout/nav/surveys/2023-q1/Survey2023Q1Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { CorePrivacyConceptsSurvey } from "../../../components/organisms/survey/CorePrivacyConcepts";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useDataMap } from "../../../hooks/useDataMap";
import { useProcessingActivites } from "../../../hooks/useProcessingActivities";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { Survey2023Q1Step } from "../../../surveys/steps/Survey2023Q1Steps";
import { Survey2023Q1Footer } from "../../get-compliant/ProgressFooter";

export const CORE_PRIVACY_CONCEPTS_SURVEY = "core-privacy-concepts-survey";
const Header: React.FC = () => {
  return (
    <PageHeader
      titleContent="Core Privacy Concepts"
      descriptionContent={
        <>
          Privacy laws require your business to comply with a few core principles. The prompts on
          this screen will help you understand <strong>the two core privacy concepts</strong> and
          see if your business is complying with them. If not, we’ll work with you to update and
          implement best practices. We will cover:
        </>
      }
    />
  );
};

export const Survey2023Q1CorePrivacyConceptsPage: React.FC = () => {
  // Variables
  const orgId = usePrimaryOrganizationId();
  const { request, progress } = useSurveyProgress(orgId, "s2023_1");

  // State
  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, CORE_PRIVACY_CONCEPTS_SURVEY);

  const [dataMap, dataMapRequest] = useDataMap(orgId);
  const collectedPic = useAllCollectedPIC(dataMap);

  const [activities, activityRequest] = useProcessingActivites(orgId);

  const [allAnswered, setAllAnswered] = useState(false);

  const isFinished = answers["cpc-done"] === "true" && allAnswered;

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q1Nav progress={progress?.progress as Survey2023Q1Step} />}
      requests={[request, activityRequest, dataMapRequest, surveyRequest]}
      footer={
        <Survey2023Q1Footer
          presentStep="CollectionPurposes"
          progressStep={progress?.progress as Survey2023Q1Step}
          nextDisabled={!isFinished}
          prevLabel="Back to Business Survey"
        />
      }
    >
      <Header />

      <ol className="custom p-0">
        <li className="pb-md">Data Minimization</li>
        <li className="pb-md">Purpose Limitation</li>
      </ol>

      <SurveyGateButton
        slug="cpc-getting-started"
        survey={answers}
        updateResponse={setAnswer}
        label="Get Started"
      >
        <CorePrivacyConceptsSurvey
          answers={answers}
          setAnswer={setAnswer}
          activities={activities}
          collectedPic={collectedPic}
          onAllAnsweredChanged={(allAnswered) => setAllAnswered(allAnswered)}
        />
      </SurveyGateButton>
    </StandardPageWrapper>
  );
};
