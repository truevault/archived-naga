import { Button } from "@mui/material";
import React from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { EmptyInfo } from "../../../components/Empty";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q1Nav } from "../../../components/layout/nav/surveys/2023-q1/Survey2023Q1Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { VendorList } from "../../../components/molecules/vendor/VendorList";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { SurveyGateHero } from "../../../components/organisms/survey/SurveyGateHero";
import { VendorSearchPopup } from "../../../components/organisms/vendor/AddVendorPopup";
import { CategorizedVendorInventory } from "../../../components/organisms/vendor/CategorizedVendorInventory";
import { VendorSearchBox } from "../../../components/organisms/vendor/VendorSearchBox";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useDataSourceSearch } from "../../../hooks/useOrganizationVendors";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { Survey2023Q1Step } from "../../../surveys/steps/Survey2023Q1Steps";
import { useUpdateDataRecipients } from "../../get-compliant/data-recipients/AddDataRecipients";
import { Survey2023Q1Footer } from "../../get-compliant/ProgressFooter";
import { SURVEY_2023Q1_GATES } from "./BusinessSurveyPage";

export const Survey2023Q1DataRecipientsPage = () => {
  const orgId = usePrimaryOrganizationId();
  const { request, progress } = useSurveyProgress(orgId, "s2023_1");

  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, SURVEY_2023Q1_GATES);

  const { actions, requests, data } = useDataSourceSearch(orgId);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const updateDataRecipients = useUpdateDataRecipients(orgId, { addCompleted: false });

  const open = Boolean(anchorEl);

  const isFinished = answers["data-recipients-done-with-sources"] === "true";

  return (
    <StandardPageWrapper
      navContent={<Survey2023Q1Nav progress={progress?.progress as Survey2023Q1Step} />}
      requests={[request, surveyRequest, requests.catalogRequest, requests.orgDataSourcesRequest]}
      footer={
        <Survey2023Q1Footer
          presentStep="DataRecipients"
          progressStep={progress?.progress as Survey2023Q1Step}
          nextDisabled={!isFinished}
          saveRequests={[
            requests.addingVendor,
            requests.addingCustomVendor,
            requests.removingVendor,
          ]}
          prevLabel="Back to Data Retention"
        />
      }
    >
      <PageHeader titleContent="Data Map Checkup" />

      <SurveyGateHero
        slug="data-recipients-intro-got-it"
        survey={answers}
        updateResponse={setAnswer}
        label="Got it"
        description={
          <>
            Next, let’s quickly go over your Data Map — that is, where your consumer data comes
            from, and who it is disclosed to. This is a <strong>quick review</strong>, if you see a
            vendor you no longer use, you can remove it here by clicking <strong>Remove</strong>. If
            you need to add a vendor, search and add it below.
          </>
        }
        imageUrl="/assets/images/surveys/2023q1/data-map-hero.svg"
      >
        <header>
          <h4>Data Recipients</h4>
          <p>
            Data Recipients include all of your vendors and third parties that handle personal data
            about your customers.
          </p>
        </header>

        <div className="mb-lg">
          <VendorSearchBox {...updateDataRecipients} />
        </div>
        <CategorizedVendorInventory
          {...updateDataRecipients}
          orgVendors={updateDataRecipients.currentVendors}
          collectionContext={["CONSUMER"]}
        />

        <SurveyGateButton
          slug="data-recipients-done-with-recipients"
          survey={answers}
          updateResponse={setAnswer}
          label="Done with Data Recipients"
        >
          <header>
            <h4>Data Sources</h4>
            <p>Sources are people and parties from which your business collects consumer data.</p>
          </header>

          <div className="w-512 my-lg">
            <VendorList
              vendors={data.selectedCatalog}
              onRemove={actions.handleRemoveVendor}
              empty={<EmptyInfo>You don't have any data sources.</EmptyInfo>}
            />
          </div>

          <div className="mb-lg">
            <Button className="px-lg" variant="contained" size="small" onClick={handleClick}>
              Add Source
            </Button>

            <VendorSearchPopup
              open={open}
              onClose={() => setAnchorEl(null)}
              el={anchorEl}
              vendors={data.remainingCatalog}
              onAddVendor={actions.handleAddVendor}
              onAddCustomVendor={actions.handleAddCustomVendor}
              customModal={{
                header: "Add Source",
                confirmLabel: "Add Source",
                includeUrl: false,
              }}
            />
          </div>

          <SurveyGateButton
            slug="data-recipients-done-with-sources"
            survey={answers}
            updateResponse={setAnswer}
            label="Done with Data Sources"
          />
        </SurveyGateButton>
      </SurveyGateHero>
    </StandardPageWrapper>
  );
};
