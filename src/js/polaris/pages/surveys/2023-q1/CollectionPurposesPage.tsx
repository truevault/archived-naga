import { KeyboardArrowLeft } from "@mui/icons-material";
import React, { useMemo, useState } from "react";
import { Callout, CalloutVariant } from "../../../components/Callout";
import { PageHeader } from "../../../components/layout/header/Header";
import { Survey2023Q1Nav } from "../../../components/layout/nav/surveys/2023-q1/Survey2023Q1Nav";
import { StandardPageWrapper } from "../../../components/layout/StandardPageWrapper";
import { useDataSubjectTypes, usePrimaryOrganizationId } from "../../../hooks";
import { useSurveyProgress } from "../../../hooks/useSurveyProgress";
import { Routes } from "../../../root/routes";
import { Survey2023Q1Step } from "../../../surveys/steps/Survey2023Q1Steps";
import { consumerGroupSorter, isEmploymentGroup } from "../../../util/consumerGroups";
import { CollectionPurposes } from "../../get-compliant/consumer-collection/CollectionPurposes";
import { ProgressFooter, ProgressFooterActions } from "../../get-compliant/ProgressFooter";

const Header: React.FC = () => {
  return (
    <PageHeader
      titleContent="Collection Purposes"
      descriptionContent={
        <>
          <p>
            In this step, you’ll identify the purposes for which you collect and process personal
            data. Select from the list of purposes below and then select the personal data used for
            each purpose.
          </p>
          <p>
            It’s okay if the categories you select aren’t a perfect fit as long as they provide a
            general idea to consumers about why you are using their data. If you’re torn between two
            categories, go with the one that is more specific.
          </p>
          <Callout variant={CalloutVariant.Purple}>
            💡 Tip: Include any data processing performed by your Data Recipients on your business’s
            behalf. For example, if you have a vendor that provides data storage, select Data
            Storage below.
          </Callout>
        </>
      }
    />
  );
};

interface FooterProps {
  nextDisabled: boolean;
}
const Footer: React.FC<FooterProps> = ({ nextDisabled }) => (
  <ProgressFooter
    actions={
      <ProgressFooterActions
        prevUrl={Routes.surveys["2023"].businessSurvey}
        nextContent="Done with Review"
        prevContent={
          <span className="flex-center-vertical">
            <KeyboardArrowLeft /> Back
          </span>
        }
        nextDisabled={nextDisabled}
        nextDisabledTooltip="Please select at least one purpose for each consumer group before continuing"
        nextUrl={Routes.surveys["2023"].corePrivacyConcepts}
      />
    }
  />
);

export const Survey2023Q1ConsumerCollectionPurposes: React.FC = () => {
  const orgId = usePrimaryOrganizationId();
  const { request, progress } = useSurveyProgress(orgId, "s2023_1");

  // State
  const [nextDisabled, setNextDisabled] = useState(false);

  // Requests
  const [fetchedConsumerGroups, consumerGroupsRequest] = useDataSubjectTypes(orgId);

  const consumerGroups = useMemo(
    () => fetchedConsumerGroups.filter((cg) => !isEmploymentGroup(cg)).sort(consumerGroupSorter),
    [fetchedConsumerGroups],
  );

  return (
    <StandardPageWrapper
      requests={[consumerGroupsRequest, request]}
      navContent={<Survey2023Q1Nav progress={progress?.progress as Survey2023Q1Step} />}
      footer={<Footer nextDisabled={nextDisabled} />}
    >
      <Header />
      <CollectionPurposes
        picWarning="bottom"
        consumerGroups={consumerGroups}
        setComplete={(complete) => setNextDisabled(!complete)}
      />
    </StandardPageWrapper>
  );
};
