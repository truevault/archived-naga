import React from "react";
import { NavLink } from "react-router-dom";
import { PageContent } from "../../../../common/layout/PageContent";
import { PageHeader } from "../../../../common/layout/PageHeader";
import { PageWrapper } from "../../../../common/layout/PageWrapper";
import { BaseNav } from "../../../components/layout/nav/BaseNav";

interface Props {
  title: string;
  description: string;
}

type DesignSection = {
  path: string;
  label: string;
  children?: DesignSubsection[];
};

type DesignSubsection = {
  path: string;
  label: string;
};

const SECTIONS: DesignSection[] = [
  { path: "/design/atoms", label: "Atoms" },
  { path: "/design/molecules", label: "Molecules" },
  {
    path: "/design/organisms",
    label: "Organisms",
    children: [
      { path: "#callouts", label: "Callouts" },
      { path: "#data-recipients", label: "Data Recipients" },
    ],
  },
];

export const DesignLayout: React.FC<Props> = ({ title, description, children }) => {
  const navItems = SECTIONS.map((s) => {
    const isActive = location.pathname.startsWith(s.path);

    return (
      <>
        <DesignLink key={s.path} path={s.path}>
          {s.label}
        </DesignLink>
        {isActive &&
          s.children &&
          s.children.map((child) => (
            <ChildLink key={child.path} path={child.path}>
              {child.label}
            </ChildLink>
          ))}
      </>
    );
  });
  return (
    <PageWrapper>
      <BaseNav showUserNavItem={false} showHelp={false} navContent={navItems} />
      <PageContent>
        <PageHeader header={title} description={description}></PageHeader>
        {children}
      </PageContent>
    </PageWrapper>
  );
};

const DesignLink: React.FC<{ path: string }> = ({ path, children }) => {
  return (
    <div className="primary-nav__link">
      <NavLink to={path}>{children}</NavLink>
    </div>
  );
};

const ChildLink: React.FC<{ path: string }> = ({ path, children }) => {
  return (
    <div className="primary-nav__link pl-xl">
      <a href={path}>{children}</a>
    </div>
  );
};
