import React from "react";

export const DesignSection: React.FC<{ title: string }> = ({ title, children }) => {
  return (
    <section className="border p-md mb-md">
      <h3 className="text-h3 mt-0">{title}</h3>
      {children}
    </section>
  );
};
