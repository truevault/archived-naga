import React from "react";

export const DesignSubSection: React.FC<{ title: string }> = ({ title, children }) => {
  return (
    <section className="mb-mdlg border-bottom pb-mdlg border-bottom--grey">
      <h3 className="text-h4">{title}</h3>
      {children}
    </section>
  );
};
