import { makeStyles } from "@mui/styles";
import { range } from "lodash";
import React from "react";
import clsx from "clsx";
import { DesignSection } from "../layout/DesignSection";

export const Colors = () => {
  return (
    <DesignSection title="Colors">
      <ColorSwatch name="Primary" min={0} main={600} />
      <ColorSwatch name="Secondary" />
      <ColorSwatch name="Neutral" min={0} main={300} />
      <ColorSwatch name="Red" main={500} />
      <ColorSwatch name="Orange" main={500} />
      <ColorSwatch name="Yellow" />
      <ColorSwatch name="Green" main={600} />
    </DesignSection>
  );
};

interface SwatchProps {
  name: string;
  min?: number;
  main?: number;
}

const useStyles = makeStyles({
  row: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "start",
    marginTop: "1em",
  },
  sample: {
    width: 80,
    height: 80,
  },
  main: {
    borderTopLeftRadius: "1em",
  },
  name: {
    fontWeight: "bold",
    minWidth: "6em",
  },
});

const ColorSwatch = ({ name, main, min = 100 }: SwatchProps) => {
  const styles = useStyles();
  const samples = range(900, min - 1, -100)
    .map((num) => ({
      backColor: `--color-${name.toLowerCase()}-${num.toString().padStart(3, "0")}`,
      isMain: main === num,
    }))
    .map(({ backColor, isMain }) => {
      return (
        <div
          key={backColor}
          title={backColor}
          className={clsx({ [styles.sample]: true, [styles.main]: isMain })}
          style={{ backgroundColor: `var(${backColor})` }}
        />
      );
    });
  return (
    <div className={styles.row}>
      <div className={styles.name}>{name}</div>
      {samples}
    </div>
  );
};
