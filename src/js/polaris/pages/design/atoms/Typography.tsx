import React from "react";
import { DesignSection } from "../layout/DesignSection";

export const Typography = () => {
  return (
    <DesignSection title="Typography">
      <h1>h1</h1>
      <h2>h2</h2>
      <h3>h3</h3>
      <h3 className="text-color-primary-dark">h3.text-color-primary-dark - common page title</h3>
      <h4 className="intermediate">h4.intermediate</h4>
      <h4>h4</h4>
      <h4 className="text-spacing-spread text-muted text-weight-regular text-transform-uppercase">
        h4.text-spacing-spread.text-muted.text-weight-regular.text-transform-uppercase - section
        header
      </h4>
      <h4 className="text-spacing-spread text-muted text-weight-regular text-t1 text-transform-none">
        h4.text-spacing-spread.text-muted.text-weight-regular.text-t1.text-transform-none - small
        action row header
      </h4>

      <h5>h5</h5>
      <p>
        p - standard body copy, with an <a href="#">inline link</a> contained
      </p>
      <p className="text-body-1-medium">p.text-body-1-medium - strong body copy</p>

      <p className="text-component-question">p.text-component-question - Question text component</p>
      <p className="text-component-help">
        p.text-component-help - Help text component with an <a href="#">inline link</a> here
      </p>
    </DesignSection>
  );
};
