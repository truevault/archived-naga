import React from "react";
import { ProgressCard } from "../../../../components/organisms/callouts/ProgressCard";
import { Routes } from "../../../../../polaris/root/routes";

export const ProgressCardDesign = () => {
  return (
    <>
      <ProgressCard
        progress="DONE"
        title="Business Survey"
        action="Resume"
        className="mb-lg"
        details="To complete your compliance process, we need to know what information you collect, who you share data with, and what data is shared. Data mapping is the core of setting up a privacy compliance program."
        steps={[
          {
            label: "Data Collection",
            progress: "DONE",
            route: Routes.getCompliant.businessSurvey.consumerDataCollection,
          },
          {
            label: "Vendor List",
            progress: "DONE",
            route: Routes.getCompliant.dataRecipients.root,
          },
          {
            label: "Third Parties",
            progress: "IN_PROGRESS",
            route: Routes.getCompliant.dataRecipients.thirdPartyRecipients,
          },
          {
            label: "Processing Region",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataRecipients.processingRegion,
          },
          {
            label: "Data Selling/Sharing",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataRecipients.selling,
          },
          {
            label: "Data Disclosure",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataRecipients.dataDisclosures,
          },
          {
            label: "Sources",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataMap.consumerSources,
          },
        ]}
      />

      <ProgressCard
        progress="NEXT"
        title="Business Survey"
        active
        action="Start"
        className="mb-lg"
        details="To complete your compliance process, we need to know what information you collect, who you share data with, and what data is shared. Data mapping is the core of setting up a privacy compliance program."
        steps={[
          {
            label: "Data Collection",
            progress: "DONE",
            route: Routes.getCompliant.businessSurvey.consumerDataCollection,
          },
          {
            label: "Vendor List",
            progress: "DONE",
            route: Routes.getCompliant.dataRecipients.root,
          },
          {
            label: "Third Parties",
            progress: "IN_PROGRESS",
            route: Routes.getCompliant.dataRecipients.thirdPartyRecipients,
          },
          {
            label: "Processing Region",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataRecipients.processingRegion,
          },
          {
            label: "Data Selling/Sharing",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataRecipients.selling,
          },
          {
            label: "Data Disclosure",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataRecipients.dataDisclosures,
          },
          {
            label: "Sources",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataMap.consumerSources,
          },
        ]}
      />

      <ProgressCard
        progress="NOT_STARTED"
        title="Business Survey"
        action="Start"
        disabled
        details="To complete your compliance process, we need to know what information you collect, who you share data with, and what data is shared. Data mapping is the core of setting up a privacy compliance program."
        steps={[
          {
            label: "Data Collection",
            progress: "DONE",
            route: Routes.getCompliant.businessSurvey.consumerDataCollection,
          },
          {
            label: "Vendor List",
            progress: "DONE",
            route: Routes.getCompliant.dataRecipients.root,
          },
          {
            label: "Third Parties",
            progress: "IN_PROGRESS",
            route: Routes.getCompliant.dataRecipients.thirdPartyRecipients,
          },
          {
            label: "Processing Region",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataRecipients.processingRegion,
          },
          {
            label: "Data Selling/Sharing",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataRecipients.selling,
          },
          {
            label: "Data Disclosure",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataRecipients.dataDisclosures,
          },
          {
            label: "Sources",
            progress: "NOT_STARTED",
            route: Routes.getCompliant.dataMap.consumerSources,
          },
        ]}
      />
    </>
  );
};
