import React from "react";
import { CopyTextCallout } from "../../../../components/organisms/callouts/CopyTextCallout";
import { SectionCallout } from "../../../../components/organisms/callouts/SectionCallout";
import { DesignSection } from "../../layout/DesignSection";

export const SectionCalloutDesign = () => {
  return (
    <DesignSection title="Section Callout">
      <SectionCallout title="Section" actionUrl="https://www.google.com">
        <p>Body of the callout here</p>
      </SectionCallout>

      <CopyTextCallout
        title="Use the email template below (or create your own) to contact vendors and inquire about service provider status."
        copy={`Hello,

You are one of our vendors and we are trying to determine if you are a “service provider” to our business under the California Consumer Privacy Act (CCPA). If you are a service provider, could you direct me to the supporting language in your privacy policy, terms of service, DPA, or other documentation? The definition of a “service provider” is provided in the CCPA at Section 1798.140(v).`}
      />
    </DesignSection>
  );
};
