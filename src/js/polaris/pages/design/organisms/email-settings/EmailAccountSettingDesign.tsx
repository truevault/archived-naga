import React from "react";
import { EmailAccountSettingRow } from "../../../../components/organisms/settings/rows/EmailAccountSettingRow";
import { DesignSection } from "../../layout/DesignSection";
import { DesignSubSection } from "../../layout/DesignSubSection";

export const EmailAccountSettingDesign = () => {
  return (
    <DesignSection title="Email Account Setting">
      <DesignSubSection title="Not Connected - No Mailbox">
        <EmailAccountSettingRow
          mailbox=""
          mailboxIsConnected={false}
          mailboxOauthUrl=""
          sendTestMessage={() => {}}
          testMessageRunning={false}
          deleteMailbox={() => {}}
        />
      </DesignSubSection>
      <DesignSubSection title="Not Connected - Mailbox Failed">
        <EmailAccountSettingRow
          mailbox="Mailbox Name"
          mailboxIsConnected={false}
          mailboxOauthUrl=""
          sendTestMessage={() => {}}
          testMessageRunning={false}
          deleteMailbox={() => {}}
        />
      </DesignSubSection>
      <DesignSubSection title="Mailbox Connected">
        <EmailAccountSettingRow
          mailbox="Mailbox Name"
          mailboxIsConnected={true}
          mailboxOauthUrl={""}
          sendTestMessage={() => {}}
          testMessageRunning={false}
          deleteMailbox={() => {}}
        />
      </DesignSubSection>
    </DesignSection>
  );
};
