import React from "react";
import { EmailLogoSettingRow } from "../../../../components/organisms/settings/rows/EmailLogoSettingRow";
import { DesignSection } from "../../layout/DesignSection";
import { DesignSubSection } from "../../layout/DesignSubSection";

export const EmailHeaderLogoSettingDesign = () => {
  return (
    <DesignSection title="Email Header Logo Setting">
      <DesignSubSection title="Email Header Logo Not Set">
        <EmailLogoSettingRow headerImageUrl="" updateMessageSettings={() => {}} />
      </DesignSubSection>
      <DesignSubSection title="Email Header Logo Set">
        <EmailLogoSettingRow
          headerImageUrl="/assets/images/nav-icon-truevault.svg"
          updateMessageSettings={() => {}}
        />
      </DesignSubSection>
    </DesignSection>
  );
};
