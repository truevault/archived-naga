import React from "react";
import { EmailSettings } from "../../../../components/superorg/settings/EmailSettings";
import { DesignSection } from "../../layout/DesignSection";

export const EmailSettingsDesign = () => {
  return (
    <DesignSection title="Email Settings">
      <EmailSettings
        mailbox={"Mailbox Name"}
        mailboxIsConnected={true}
        mailboxOauthUrl={""}
        sendTestMessage={() => {}}
        testMessageRunning={false}
        deleteMailbox={() => {}}
        updateMessageSettings={() => {}}
        updateSignature={() => {}}
        messageSettings={{
          headerImageUrl: "",
          messageSignature: "Org Privacy Team\nhttps://privacy.truevault.com",
          defaultMessageSignature: "Org Privacy Team\nhttps://privacy.truevault.com",
          physicalAddress: "",
        }}
      />
    </DesignSection>
  );
};
