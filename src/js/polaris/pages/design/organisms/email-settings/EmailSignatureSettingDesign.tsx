import React from "react";
import { EmailSignatureSettingRow } from "../../../../components/organisms/settings/rows/EmailSignatureSettingRow";
import { DesignSection } from "../../layout/DesignSection";

export const EmailSignatureSettingDesign = () => {
  return (
    <DesignSection title="Email Signature Setting">
      <EmailSignatureSettingRow
        signature="${user.first_name} @ ${organization.name} Compliance Team\n${organization.privacy_policy_url}"
        defaultSignature="This is my default signature"
        onSignatureChange={(_sig) => {}}
      />
    </DesignSection>
  );
};
