import React from "react";
import { PrivacyCenterRequestFormSettingsRow } from "../../../../components/organisms/settings/rows/PrivacyCenterRequestFormSettingsRow";
import { DesignSection } from "../../layout/DesignSection";
import { DesignSubSection } from "../../layout/DesignSubSection";

export const RequestFormSettingDesign = () => {
  return (
    <DesignSection title="Request Form Setting">
      <DesignSubSection title="Request Form Unchecked">
        <PrivacyCenterRequestFormSettingsRow
          customFieldEnabled={false}
          customFieldHelp=""
          customFieldLabel=""
          handleCustomFieldSubmit={(_values) => {}}
        />
      </DesignSubSection>
      <DesignSubSection title="Request Form Checked">
        {" "}
        {/* Disable checkbox? */}
        <PrivacyCenterRequestFormSettingsRow
          customFieldEnabled={true}
          customFieldHelp="Help with custom field"
          customFieldLabel="Label for custom field"
          handleCustomFieldSubmit={(_values) => {}}
        />
      </DesignSubSection>
    </DesignSection>
  );
};
