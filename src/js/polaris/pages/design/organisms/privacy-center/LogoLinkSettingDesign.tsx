import React from "react";
import { PrivacyCenterLogoLinkSettingRow } from "../../../../components/organisms/settings/rows/PrivacyCenterLogoLinkSettingRow";
import { DesignSection } from "../../layout/DesignSection";
import { DesignSubSection } from "../../layout/DesignSubSection";

export const LogoLinkSettingsDesign = () => {
  return (
    <DesignSection title="Logo Link Setting">
      <DesignSubSection title="No Logo Link">
        <PrivacyCenterLogoLinkSettingRow logoLinkUrl={""} handleFormSubmit={(_values) => {}} />
      </DesignSubSection>
      <DesignSubSection title="With Logo Link">
        <PrivacyCenterLogoLinkSettingRow
          logoLinkUrl={"http://www.example.com"}
          handleFormSubmit={(_values) => {}}
        />
      </DesignSubSection>
    </DesignSection>
  );
};

<h3>Logo Link Setting </h3>;
