import React from "react";
import { PrivacyCenterFaviconSettingRow } from "../../../../components/organisms/settings/rows/PrivacyCenterFavIconSettingRow";
import { usePrimaryOrganizationId } from "../../../../hooks/useOrganization";
import { DesignSection } from "../../layout/DesignSection";
import { DesignSubSection } from "../../layout/DesignSubSection";

export const FaviconSettingDesign = () => {
  const orgId = usePrimaryOrganizationId();
  return (
    <DesignSection title="Favicon Setting">
      <DesignSubSection title="No Favicon">
        <PrivacyCenterFaviconSettingRow
          faviconUrl=""
          handleFormSubmit={(_values) => {}}
          organizationId={orgId}
        />
      </DesignSubSection>
      <DesignSubSection title="With Favicon">
        <PrivacyCenterFaviconSettingRow
          organizationId={orgId}
          faviconUrl="/assets/images/favicon.ico"
          handleFormSubmit={(_values) => {}}
        />
      </DesignSubSection>
    </DesignSection>
  );
};
