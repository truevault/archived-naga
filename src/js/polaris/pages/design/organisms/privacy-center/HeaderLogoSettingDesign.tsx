import React from "react";
import { PrivacyCenterHeaderLogoSettingRow } from "../../../../components/organisms/settings/rows/PrivacyCenterHeaderLogoSettingRow";
import { usePrimaryOrganizationId } from "../../../../hooks/useOrganization";
import { DesignSection } from "../../layout/DesignSection";
import { DesignSubSection } from "../../layout/DesignSubSection";

export const HeaderLogoSettingDesign = () => {
  const orgId = usePrimaryOrganizationId();
  return (
    <DesignSection title="Header Logo Setting">
      <DesignSubSection title="No Logo">
        <PrivacyCenterHeaderLogoSettingRow
          logoUrl=""
          handleFormSubmit={(_values) => {}}
          organizationId={orgId}
        />
      </DesignSubSection>
      <DesignSubSection title="With Logo">
        <PrivacyCenterHeaderLogoSettingRow
          organizationId={orgId}
          logoUrl="/assets/images/nav-icon-truevault.svg"
          handleFormSubmit={(_values) => {}}
        />
      </DesignSubSection>
    </DesignSection>
  );
};
