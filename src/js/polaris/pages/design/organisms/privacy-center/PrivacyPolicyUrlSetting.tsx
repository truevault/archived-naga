import React from "react";
import { PrivacyCenterUrlSettingRow } from "../../../../components/organisms/settings/rows/PrivacyCenterUrlSettingsRow";
import { DesignSection } from "../../layout/DesignSection";

export const PrivacyCenterUrlSettingDesign = () => {
  return (
    <DesignSection title="Privacy Center Url Setting">
      <PrivacyCenterUrlSettingRow url="http://www.example.com" />
    </DesignSection>
  );
};
