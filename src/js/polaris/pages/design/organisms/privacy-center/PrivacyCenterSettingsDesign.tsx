import React from "react";
import { PrivacyCenterSettings } from "../../../../components/superorg/settings/PrivacyCenterSettings";
import { usePrimaryOrganizationId } from "../../../../hooks/useOrganization";
import { DesignSection } from "../../layout/DesignSection";

export const PrivacyCenterSettingsDesign = () => {
  const orgId = usePrimaryOrganizationId();
  return (
    <DesignSection title="Privacy Center Settings">
      <PrivacyCenterSettings
        organizationId={orgId}
        privacyCenterUrl={"http://www.example.com"}
        logoUrl=""
        handleFormSubmit={async (_values) => {}}
        logoLinkUrl={""}
        faviconUrl=""
        customFieldEnabled={false}
        customFieldHelp=""
        customFieldLabel=""
        dpoName=""
        dpoEmail=""
        dpoPhone=""
        dpoAddress=""
        dpoDoesNotApply={false}
        hasDpoTask={false}
        requiresDpo={false}
        handleDoItLaterTask={async () => {}}
        handleDpoDoesNotApply={async (_doesNotApply: boolean) => {}}
        disableCaResidencyConfirmation={false}
        handleCustomFieldSubmit={(_values) => {}}
      />
    </DesignSection>
  );
};
