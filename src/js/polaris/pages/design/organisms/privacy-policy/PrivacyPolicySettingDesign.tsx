import React from "react";
import { PrivacyPolicySettings } from "../../../../components/superorg/settings/PrivacyPolicySettings";
import { DesignSection } from "../../layout/DesignSection";

export const PrivacyPolicySettingDesign = () => {
  return (
    <DesignSection title="Privacy Policy Setting">
      <PrivacyPolicySettings
        privacyCenterId={"0"}
        updatePrivacyCenterPolicy={(_values) => {}}
        privacyPolicyHtml={"Hello! I am a privacy center"}
        setPrivacyPolicyHtml={(_html) => {}}
      />
    </DesignSection>
  );
};
