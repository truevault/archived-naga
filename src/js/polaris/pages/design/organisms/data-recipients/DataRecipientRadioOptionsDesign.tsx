import React, { useState } from "react";
import { ExternalLink } from "../../../../../common/components/ExternalLink";
import { OrganizationDataRecipientDto } from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { DataRecipientRadioOptions } from "../../../../components/organisms/data-recipients/DataRecipientRadioOptions";
import { DesignSection } from "../../layout/DesignSection";

const STUB_RECIPIENT: OrganizationDataRecipientDto = {
  name: "Stripe",
  id: "stub-id",
  vendorId: "stub-vendor-id",
  organization: "stub-org-id",
  organizationName: "Stub Org Name",
  url: "",
  email: "",
  defaultEmail: null,
  logoUrl: "/images/logos/services/87d547c3-4da8-493e-97d0-a89443b72ec0.svg",
  dateAdded: "2020-01-01",
  dataRecipientType: "vendor",
  classification: {
    id: "classification-id",
    displayOrder: 0,
    slug: "service-provider",
    name: "Serivce Provider",
    tooltip: "service provider",
  },
  classificationOptions: [],
  vendorContractOptions: [],
  vendorContractReviewedOptions: [],
  status: "status",
  isCustom: false,
  isAccessible: true,
  isStandalone: false,
  isPlatform: false,
  isInstalled: false,
  retentionReasons: [],
  contacted: false,
  dpaApplicable: false,
  dpaUrl: "",
  mappingProgress: {
    label: "DONE",
    value: "DONE",
  },
  instructions: [],
  deletionInstructions: "",
  accessInstructions: "",
  optOutInstructions: undefined,
  specialCircumstancesGuidance: "",
  generalGuidance: "",
  recommendedPersonalInformationCategories: [],
  respectsPlatformRequests: true,
  processingRegions: ["UNITED_STATES"],
  collectionContext: ["CONSUMER"],

  category: "Ad Vendor",
  ccpaIsSelling: false,
  ccpaIsSharing: false,
  usesCustomAudience: false,
  serviceProviderRecommendation: "SERVICE_PROVIDER",
  serviceProviderLanguageUrl: "",

  gdprProcessorRecommendation: "Processor",
  gdprProcessorLanguageUrl: "https://www.google.com/?q=processors",
  gdprProcessorSetting: "Processor",
  isGdprProcessorSettingLocked: false,

  defaultRetentionReasons: [],

  gdprHasSccRecommendation: false,
  gdprSccDocumentationUrl: "google.com/?q=scc",
  gdprHasSccSetting: null,
  gdprSccUrl: "google.com/?q=scc",
  gdprSccFileKey: null,
  gdprSccFileName: null,
  gdprContactUnsafeTransfer: null,
  gdprEnsureSccInPlace: null,
  gdprHasSccs: false,
  gdprNeedsSccs: false,
  gdprHasUserProvidedSccs: false,
  removedFromExceptionsToScc: false,
  disclosedToByUsGroup: true,
  disclosedToByEeaUkGroup: false,
  isPotentialIntentionalInteractionVendor: false,
  isAutomaticIntentionalInteractionVendor: false,
  isPotentialSellingVendor: false,
  isAutomaticSellingVendor: false,
  isUploadVendor: false,
  isPotentialUploadVendor: false,
  isAutomaticUploadVendor: false,
  dataAccessibility: null,
  dataDeletability: null,
  isPotentiallyExceptedFromSccs: false,
  isExceptedFromSccs: false,
  completed: true,
};

export const DataRecipientRadioOptionsDesign = () => {
  const [value, setValue] = useState(null);
  return (
    <DesignSection title="Radio Options">
      <DataRecipientRadioOptions
        recipient={STUB_RECIPIENT}
        options={[
          {
            label: "Service Provider",
            value: "service-provider",
          },
          {
            label: "Third Party",
            value: "third-party",
          },
        ]}
        value={value}
        onChange={setValue}
        description={<ExternalLink href="https://www.google.com">View documentation</ExternalLink>}
      />
    </DesignSection>
  );
};
