import React from "react";
import { VendorDto } from "../../../../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../../../../common/service/server/Types";
import { QuestionHeading } from "../../../../components/vendor/QuestionHeading";
import { DEFAULT_DATA_RECIPIENT_NAMES } from "../../../../components/VendorSearchContents";
import { VENDOR_KEY_TRUEVAULT_POLARIS } from "../../../../types/Vendor";
import { useDefaultVendors } from "../../../../util/resources/vendorUtils";
import { RowVendorTile } from "../../../stay-compliant/data-recipients/RowVendorTile";

type CommonDataRecipientsProps = {
  allVendors: VendorDto[];
  addedVendors: Set<UUIDString>;
  pendingVendor?: UUIDString;
  handleAddVendor: (v: UUIDString) => void;
};

export const CommonDataRecipients: React.FC<CommonDataRecipientsProps> = ({
  allVendors,
  addedVendors,
  pendingVendor,
  handleAddVendor,
}) => {
  const defaultVendors = useDefaultVendors(allVendors, DEFAULT_DATA_RECIPIENT_NAMES);

  return (
    <>
      <QuestionHeading question="Do you use any of these common vendors?" />

      <div className="vendor_grid">
        {defaultVendors.map((v) => {
          const isAdded = addedVendors.has(v.id);
          return (
            <RowVendorTile
              key={v.id}
              name={v.name}
              category={v.category}
              logoUrl={v.logoUrl}
              vendor={v}
              pending={pendingVendor == v.id}
              disabled={Boolean(pendingVendor) || vendorDisabled(v, isAdded)}
              added={isAdded}
              selected={isAdded}
              handleAddVendor={() => handleAddVendor(v.id)}
            />
          );
        })}
      </div>
    </>
  );
};

export const vendorDisabled = (v: VendorDto, isAdded: boolean): boolean => {
  return v.vendorKey == VENDOR_KEY_TRUEVAULT_POLARIS && isAdded;
};
