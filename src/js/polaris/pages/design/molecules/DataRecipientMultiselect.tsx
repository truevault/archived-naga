import { Typography } from "@mui/material";
import React, { useMemo } from "react";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { ToggleButton } from "../../../components/buttons/ToggleButton";
import { DataRecipientLogo } from "../../../components/DataRecipientLogo";

export const DataRecipientMultiselect: React.FC<DataRecipientMultiselectProps> = ({
  recipients: vendors,
  selected,
  loading,
  onChange: updatePlatformVendor,
  onChangeSelectAll,
}) => {
  const platformAppIds = useMemo(() => new Set(selected), [selected]);
  const allSelected = vendors.every((v) => platformAppIds.has(v.vendorId));

  return (
    <ul className="vertical-list mt-lg w-400">
      <li className="mt-mdlg">
        <ToggleButton checked={allSelected} loading={loading} onChange={onChangeSelectAll}>
          <Typography className="text-truncate-ellipsis">Select All</Typography>
        </ToggleButton>
      </li>

      {vendors.map((vendor) => {
        const { vendorId, name } = vendor;

        return (
          <li key={vendorId} className="mt-mdlg">
            <ToggleButton
              checked={platformAppIds.has(vendorId)}
              loading={loading}
              onChange={(x) => {
                // Not using setState because mutating and allowing the vendor update to rerender
                if (x) {
                  platformAppIds.add(vendorId);
                } else {
                  platformAppIds.delete(vendorId);
                }
                return updatePlatformVendor({ appId: vendorId, included: x });
              }}
            >
              <div className="d-flex align-items-center w-85">
                <DataRecipientLogo className="mr-sm" dataRecipient={vendor} />
                <Typography className="text-truncate-ellipsis">{name}</Typography>
              </div>
            </ToggleButton>
          </li>
        );
      })}
    </ul>
  );
};

export interface DataRecipientMultiselectProps {
  recipients: (OrganizationDataRecipientDto | OrganizationDataSourceDto)[];
  selected: string[];
  onChange: ({ appId, included }: { appId: string; included: boolean }) => void;
  onChangeSelectAll: (included: boolean) => void;
  loading?: boolean;
}
