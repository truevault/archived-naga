import React, { useState } from "react";
import { ToggleButton } from "../../../components/buttons/ToggleButton";

import { DesignSection } from "../layout/DesignSection";
import { DesignSubSection } from "../layout/DesignSubSection";

export const ToggleButtonExamples = () => {
  const [constrain, setConstrain] = useState(false);
  const [width, setWidth] = useState(250);
  const [isChecked, setIsChecked] = useState(false);

  return (
    <DesignSection title="Toggle Button">
      <fieldset>
        <label>
          Constraint Width?{" "}
          <input
            type="checkbox"
            checked={constrain}
            onChange={(x) => setConstrain(x.target.checked)}
          />
        </label>
        <label>
          Width{" "}
          <input
            disabled={!constrain}
            type="range"
            min="100"
            max="500"
            step="50"
            value={width}
            onChange={(x) => setWidth(x.target.valueAsNumber)}
          />
        </label>
      </fieldset>
      <div style={{ width: constrain ? `${width}px` : undefined }}>
        <DesignSubSection title="Text Content">
          <ToggleButton checked={isChecked} onChange={setIsChecked}>
            <div className="p-md">Content Goes Here</div>
          </ToggleButton>
        </DesignSubSection>
        <DesignSubSection title="Complex Content">
          <ToggleButton checked={isChecked} onChange={setIsChecked}>
            <div className="p-md d-flex flex--start flex--align-center">
              <img src="https://picsum.photos/48"></img>
              <p className="ml-md">Lorem Picsum</p>
            </div>
          </ToggleButton>
        </DesignSubSection>
      </div>
    </DesignSection>
  );
};
