import React from "react";
import { Colors } from "./atoms/Colors";
import { Typography } from "./atoms/Typography";
import { DesignLayout } from "./layout/DesignLayout";

export const Atoms = () => {
  return (
    <DesignLayout
      title="Atoms"
      description="Atoms are the building blocks that make up our designs."
    >
      <Colors />
      <Typography />
    </DesignLayout>
  );
};
