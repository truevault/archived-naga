import React from "react";
import { DesignLayout } from "./layout/DesignLayout";
// import { BooleanAnswerExample } from "./molecules/BooleanAnswerExample";
import { ToggleButtonExamples } from "./molecules/ToggleButtonExamples";

export const Molecules = () => {
  return (
    <DesignLayout
      title="Molecules"
      description="Molecules use atoms to build highly reusable low-level components."
    >
      {/* <BooleanAnswerExample /> */}
      <ToggleButtonExamples />
    </DesignLayout>
  );
};
