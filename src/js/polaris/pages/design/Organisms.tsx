import React from "react";
import { Error } from "../../components/Error";
import { HipaaCallout } from "../../components/organisms/callouts/RegulationCallouts";
import { DesignLayout } from "./layout/DesignLayout";
import { ProgressCardDesign } from "./organisms/callouts/ProgressCardDesign";
import { SectionCalloutDesign } from "./organisms/callouts/SectionCalloutDesign";
import { DataRecipientRadioOptionsDesign } from "./organisms/data-recipients/DataRecipientRadioOptionsDesign";
import { EmailAccountSettingDesign } from "./organisms/email-settings/EmailAccountSettingDesign";
import { EmailHeaderLogoSettingDesign } from "./organisms/email-settings/EmailLogoSettingDesign";
import { EmailSettingsDesign } from "./organisms/email-settings/EmailSettingsDesign";
import { EmailSignatureSettingDesign } from "./organisms/email-settings/EmailSignatureSettingDesign";
import { FaviconSettingDesign } from "./organisms/privacy-center/FaviconSettingDesign";
import { HeaderLogoSettingDesign } from "./organisms/privacy-center/HeaderLogoSettingDesign";
import { LogoLinkSettingsDesign } from "./organisms/privacy-center/LogoLinkSettingDesign";
import { PrivacyCenterSettingsDesign } from "./organisms/privacy-center/PrivacyCenterSettingsDesign";
import { PrivacyCenterUrlSettingDesign } from "./organisms/privacy-center/PrivacyPolicyUrlSetting";
import { RequestFormSettingDesign } from "./organisms/privacy-center/RequestFormSettingDesign";
import { PrivacyPolicySettingDesign } from "./organisms/privacy-policy/PrivacyPolicySettingDesign";

export const Organisms = () => {
  return (
    <DesignLayout
      title="Organisms"
      description="Organisms combine molecules to create business specific tools"
    >
      <PrivacyCenterUrlSettingDesign />
      <HeaderLogoSettingDesign />
      <LogoLinkSettingsDesign />
      <FaviconSettingDesign />
      <RequestFormSettingDesign />
      <PrivacyCenterSettingsDesign />
      <EmailAccountSettingDesign />
      <EmailHeaderLogoSettingDesign />
      <EmailSignatureSettingDesign />
      <EmailSettingsDesign />
      <PrivacyPolicySettingDesign />
      <Error message="Demo Error" />

      <DesignSection anchor="callouts" label="Callouts" />
      <HipaaCallout />
      <SectionCalloutDesign />

      <DesignSection anchor="data-recipients" label="Data Recipients" />
      <DataRecipientRadioOptionsDesign />

      <DesignSection anchor="progress-cards" label="Progress Cards" />
      <ProgressCardDesign />
    </DesignLayout>
  );
};

type DesignSectionProps = {
  anchor?: string;
  label: string;
};
const DesignSection: React.FC<DesignSectionProps> = ({ anchor, label }) => {
  const content = <h2>{label}</h2>;
  if (anchor) {
    return (
      <>
        <a id={anchor} />
        {content}
      </>
    );
  }
  return content;
};
