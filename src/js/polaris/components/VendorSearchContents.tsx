import { Add, Groups, ReceiptLong } from "@mui/icons-material";
import WidgetsOutlinedIcon from "@mui/icons-material/WidgetsOutlined";
import { Button, Stack } from "@mui/material";
import _ from "lodash";
import React, { ReactNode, useCallback, useEffect, useMemo, useState } from "react";
import { NetworkRequestState } from "../../common/models/NetworkRequest";
import { CollectionContext } from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../common/service/server/Types";
import { ActionTile } from "../organisms/ActionTile";
import { vendorDisabled } from "../pages/design/organisms/data-recipients/CommonDataRecipients";
import {
  CustomVendorDialog,
  CustomVendorFormValues,
} from "../pages/get-compliant/data-recipients/AddDataRecipients";
import { THIRD_PARTY_CATEGORIES } from "../pages/get-compliant/data-recipients/ThirdPartyRecipientsPage";
import { RowVendorTile } from "../pages/stay-compliant/data-recipients/RowVendorTile";
import { TileBehavior } from "../pages/stay-compliant/data-recipients/VendorTile";
import { buildVendorSearchIndex } from "../util/resources/vendorUtils";
import { VendorSearchBox } from "./organisms/vendor/VendorSearchBox";

export const DEFAULT_DATA_RECIPIENT_NAMES = [
  "Amazon Web Services",
  "Facebook Ads",
  "Google Ads",
  "Google Analytics",
  "Google Workspace",
  "Instagram Ads",
  "Klaviyo",
  "Microsoft Ads",
  "PayPal - Pay with PayPal, Venmo, Pay Later",
  "Pinterest Ads",
  "Shopify",
  "Stripe",
  "TrueVault",
  "TrueVault Polaris",
  "Yotpo",
];

export const DEFAULT_DATA_SOURCE_NAMES = ["Other Individuals", "Amazon Marketplace", "ZoomInfo"];

export type UseVendorSearch = {
  isSearchLongEnough: boolean;
  isLoading: boolean;
  results: VendorDto[];
  search: string;
};

export const useVendorSearch = (vendors: VendorDto[], search: string): UseVendorSearch => {
  const [searchingIndex, setSearchingIndex] = useState(false);
  const [searchResults, setSearchResults] = useState<VendorDto[]>([]);

  const [validSearchTerm, setValidSearchTerm] = useState(false);

  const vendorMap = useMemo(() => _.keyBy(vendors, "id"), [vendors]);

  const searchIndex = useMemo(() => buildVendorSearchIndex(vendors), [vendors]);

  const applySearch = useCallback(
    (term: string) => {
      setSearchResults(
        searchIndex
          .search(term)
          .filter(({ ref }) => Boolean(vendorMap[ref]))
          .map(({ ref }) => vendorMap[ref]) as VendorDto[],
      );
      setSearchingIndex(false);
    },
    [vendorMap, searchIndex, setSearchResults, setSearchingIndex],
  );

  useEffect(() => {
    const term = search
      .replace(/[+*:;]/g, "")
      .replace("-", " ")
      .trim()
      .split(" ")
      .map((s) => (s && s.length > 0 ? `+${s}*` : ""))
      .join(" ");
    const _isValid = term.length >= 4;
    setValidSearchTerm(_isValid);
    if (_isValid) {
      setSearchingIndex(true);
      applySearch(term);
    }
  }, [search, applySearch]);

  return {
    isSearchLongEnough: validSearchTerm,
    isLoading: searchingIndex,
    results: searchResults,
    search,
  };
};

export interface AddCustomModalProps {
  header?: ReactNode;
  confirmLabel?: ReactNode;
  includeUrl?: boolean;
}

interface VendorSearchContentsProps {
  allVendorsMap: Map<UUIDString, VendorDto>;
  enabledVendors: Set<UUIDString>;
  defaultVendors: VendorDto[];
  context: CollectionContext[];
  selectedVendor?: UUIDString;
  pendingVendor?: UUIDString;
  headingLabel?: string;
  handleAddVendor: (v: UUIDString) => void;
  handleRemoveVendor?: (v: UUIDString) => void;
  handleAddCustomVendor: (values: CustomVendorFormValues) => void;
  handleAddContractor?: (values: CustomVendorFormValues) => void;
  handleAddThirdParyRecipient?: (values: CustomVendorFormValues) => void;
  addRequest?: NetworkRequestState;
  variant: TileBehavior;

  twoWide?: boolean;
  showCustomVendorControls?: boolean;

  customModal?: AddCustomModalProps;
  type: "source" | "vendor";
}

export const VendorSearchContents = ({
  allVendorsMap,
  enabledVendors,
  defaultVendors,
  type,
  context,
  pendingVendor,
  headingLabel = "Search our vendor database...",
  handleAddVendor,
  handleAddCustomVendor,
  handleAddThirdParyRecipient,
  handleAddContractor,
  handleRemoveVendor,
  addRequest,
  customModal,
  showCustomVendorControls = false,
}: VendorSearchContentsProps) => {
  const allVendors = useMemo(() => [...allVendorsMap.values()], [allVendorsMap]);

  const [customVendorOpen, setCustomVendorOpen] = useState(false);
  const [thirdPartyOpen, setThirdPartyOpen] = useState(false);
  const [contractorOpen, setContractorOpen] = useState(false);

  const enabled = useMemo(
    () => allVendors.filter((v) => enabledVendors.has(v.id)),
    [allVendors, enabledVendors],
  );

  const cats = useMemo(
    () => new Set(allVendors.map(({ category }) => category).filter((x) => x)),
    [allVendors],
  );

  return (
    <>
      <h4>{headingLabel}</h4>

      <Stack gap={3}>
        <VendorSearchBox
          allVendors={allVendors}
          enabledVendors={enabledVendors}
          pendingVendor={pendingVendor}
          handleAddVendor={handleAddVendor}
          handleAddCustomVendor={handleAddCustomVendor}
          handleRemoveVendor={handleRemoveVendor}
          addRequest={addRequest}
          customModal={customModal}
          collectionContext={context}
          type={type}
        />

        <div className="vendor_grid">
          {defaultVendors.map((v) => {
            const isAdded = enabledVendors.has(v.id);

            return (
              <RowVendorTile
                key={v.id}
                name={v.name}
                category={v.category}
                logoUrl={v.logoUrl}
                vendor={v}
                pending={pendingVendor == v.id}
                disabled={Boolean(pendingVendor) || vendorDisabled(v, isAdded)}
                added={isAdded}
                selected={isAdded}
                handleAddVendor={() => handleAddVendor(v.id)}
              />
            );
          })}
        </div>
      </Stack>

      {showCustomVendorControls && (
        <>
          <h4>Or add a custom data recipient...</h4>

          <Stack gap={2}>
            <ActionTile
              title="Add a Custom Vendor"
              description="Add a vendor you can't find in our database"
              icon={<WidgetsOutlinedIcon />}
              actions={
                <Button
                  startIcon={<Add />}
                  variant="contained"
                  onClick={() => setCustomVendorOpen(true)}
                >
                  Add
                </Button>
              }
            />

            <ActionTile
              title="Add a Third Party Data Recipient"
              description="Non-vendor data recipients, such as business partners, co-marketers, data buyer, or affiliates"
              icon={<Groups />}
              actions={
                <Button
                  startIcon={<Add />}
                  variant="contained"
                  onClick={() => setThirdPartyOpen(true)}
                >
                  Add
                </Button>
              }
            />

            <ActionTile
              title="Add a Contractor"
              description="Add an external contractor to whom you disclose consumer data"
              icon={<ReceiptLong />}
              actions={
                <Button
                  startIcon={<Add />}
                  variant="contained"
                  onClick={() => setContractorOpen(true)}
                >
                  Add
                </Button>
              }
            />
          </Stack>

          {/*Custom Vendor*/}
          <CustomVendorDialog
            open={customVendorOpen}
            onClose={() => setCustomVendorOpen(false)}
            onDone={(ov: CustomVendorFormValues) => {
              handleAddCustomVendor(ov);
              setCustomVendorOpen(false);
            }}
            enabledVendors={enabled}
            existingCategories={cats}
            confirmLabel="Add Custom Vendor"
          />

          {/*Third Party Recipient*/}
          <CustomVendorDialog
            header="Add Third Party Data Recipient"
            open={thirdPartyOpen}
            onClose={() => setThirdPartyOpen(false)}
            onDone={(ov: CustomVendorFormValues) => {
              handleAddThirdParyRecipient?.(ov);
              setThirdPartyOpen(false);
            }}
            enabledVendors={enabled}
            existingCategories={new Set(THIRD_PARTY_CATEGORIES)}
            confirmLabel="Add Third Party Recipient"
          />

          {/*Contractor*/}
          <CustomVendorDialog
            header="Add Contractor"
            open={contractorOpen}
            onClose={() => setContractorOpen(false)}
            onDone={(ov: CustomVendorFormValues) => {
              handleAddContractor?.(ov);
              setContractorOpen(false);
            }}
            enabledVendors={enabled}
            showCategories={false}
            defaultCategory="Contractor"
            existingCategories={new Set(["Contractor"])}
            confirmLabel="Add Contractor"
          />
        </>
      )}
    </>
  );
};
