import React from "react";

type Props = {
  title?: string;
  className?: string;
  children: React.ReactNode;
};
export const TipBox: React.FC<Props> = ({ title, children, className }) => {
  return (
    <div className={`tip-box p-md my-md ${className}`}>
      {title && <h4 className="tip-box__title mb-sm mt-0">{title}</h4>}

      {children}
    </div>
  );
};
