import { Event } from "@mui/icons-material";
import moment from "moment";
import React from "react";
import { Iso8601Date } from "../../common/service/server/Types";
import clsx from "clsx";
import { daysRemaining, fmtDateForRow } from "./dataRequest/DataRequestSubjectCard";

type DueDateProps = {
  date: Iso8601Date;
  icon?: boolean;
  condensed?: boolean;
};
export const DueDate: React.FC<DueDateProps> = ({ date, icon = false, condensed = false }) => {
  const deadline = moment(date);

  const originalRemaining = daysRemaining(deadline);
  const remaining = Math.abs(originalRemaining);

  let altDateString: string | undefined = undefined;
  let dateString = fmtDateForRow(date);
  let warning = false;
  let danger = false;
  if (originalRemaining < 0) {
    altDateString = dateString;
    dateString = `Due ${remaining} days ago`;
    danger = true;
  } else if (originalRemaining <= 5) {
    altDateString = dateString;
    dateString = `Due in ${remaining} days`;
    warning = true;
  } else {
    dateString = `Due ${dateString}`;
  }

  return (
    <div
      className={clsx("request-due-date due-date-card", {
        "request-due-date--warning": warning,
        "due-date-card--warning": warning,
        "due-date-card--danger": danger,
        "due-date-card--condensed": condensed,
      })}
      title={altDateString}
    >
      {icon && <Event className="mr-xs" />}
      {dateString}
    </div>
  );
};
