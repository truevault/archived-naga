import { CloudUpload } from "@mui/icons-material";
import { Button } from "@mui/material";
import React, { useRef } from "react";

export const FileUploadButton: React.FC<{
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  disabled: boolean;
}> = ({ onChange, disabled = false }) => {
  const uploadEl = useRef<HTMLInputElement>();

  return (
    <>
      <input
        style={{ display: "none" }}
        ref={uploadEl}
        id="note-upload"
        type="file"
        multiple={false}
        onChange={onChange}
      />

      <Button
        variant="contained"
        onClick={() => uploadEl.current?.click()}
        startIcon={<CloudUpload />}
        disabled={disabled}
      >
        Upload agreement
      </Button>
    </>
  );
};
