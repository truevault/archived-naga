import { Attachment, Close } from "@mui/icons-material";
import {
  Alert,
  FormControlLabel,
  Icon,
  IconButton,
  InputAdornment,
  Radio,
  TextField,
} from "@mui/material";
import React from "react";
import { UrlOrFileInput } from "./UrlOrFileInput";

export const FILE_TYPE_UPLOADED = "file_type_uploaded";
export const FILE_TYPE_URL = "file_type_url";

type RadioWithFileReferenceProps = {
  label: string;
  value: string | number | null;
  url: string;
  filename: string;
  error?: string;
  onUrlChanged: (newUrl: string) => void;
  onFileDeleted: () => void;
  onFileChanged: React.ChangeEventHandler<HTMLInputElement>;
  onRadioChanged?: (e: React.SyntheticEvent<Element, Event>, checked: boolean) => void;
  hideUrlInput?: boolean;
  disabled?: boolean;
  loading?: boolean;
};

export const RadioWithFileReference: React.FC<RadioWithFileReferenceProps> = ({
  label,
  value,
  url,
  filename,
  onUrlChanged,
  onFileDeleted,
  onFileChanged,
  onRadioChanged,
  hideUrlInput = false,
  error = null,
  disabled = false,
  loading = false,
}) => {
  const hasFile = Boolean(filename);

  return (
    <>
      <FormControlLabel
        value={value}
        control={<Radio color="primary" />}
        onChange={onRadioChanged}
        label={label}
        disabled={disabled || loading}
      />

      {!disabled && !hideUrlInput && (
        <div className="ml-xl">
          {hasFile ? (
            <TextField
              size="small"
              variant="outlined"
              disabled
              value={filename ?? ""}
              fullWidth
              InputProps={{
                readOnly: true,
                startAdornment: (
                  <InputAdornment position="start">
                    <Icon>
                      <Attachment />
                    </Icon>
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={onFileDeleted} size="small" edge="end">
                      <Close />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          ) : (
            <UrlOrFileInput
              url={url}
              onUrlChanged={onUrlChanged}
              filename={filename}
              onFileChanged={onFileChanged}
              disabled={disabled}
              error={error}
            />
          )}

          {Boolean(error) && (
            <Alert className="mt-sm" color="error">
              {error}
            </Alert>
          )}
        </div>
      )}
    </>
  );
};
