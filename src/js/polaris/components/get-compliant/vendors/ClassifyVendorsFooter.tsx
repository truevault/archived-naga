import { KeyboardArrowLeft } from "@mui/icons-material";
import React, { ReactNode } from "react";
import { NetworkRequestState } from "../../../../common/models/NetworkRequest";
import { OrganizationPublicId, GetCompliantStep } from "../../../../common/service/server/Types";
import {
  ProgressFooterProgressRequestWrapper,
  ProgressFooterSaveNotification,
} from "../../../pages/get-compliant/ProgressFooter";
import { StepLink } from "../../../util/GetCompliantClassificationOrder";

interface ClassifyVendorsFooterProps {
  organizationId: OrganizationPublicId;
  currentRequest: NetworkRequestState;
  nextDisabled: boolean;
  prevLabel?: ReactNode;
  prevUrl?: string;
  nextLabel?: ReactNode;
  nextUrl?: string;
  nextProgress?: GetCompliantStep;
  prev?: StepLink;
  next?: StepLink;
  nextOnClick?: () => void;
  currentProgress: GetCompliantStep;
  disabledNextTooltip: string;
}

export const ClassifyVendorsFooter = ({
  organizationId,
  currentRequest,
  nextDisabled,
  prevLabel = "Back",
  prevUrl,
  nextLabel = "Next",
  nextUrl,
  nextProgress,
  next,
  nextOnClick,
  prev,
  currentProgress,
  disabledNextTooltip,
}: ClassifyVendorsFooterProps) => {
  const prevContent = (
    <span className="flex-center-vertical">
      <KeyboardArrowLeft /> {prev?.label || prevLabel}
    </span>
  );

  return (
    <ProgressFooterProgressRequestWrapper
      organizationId={organizationId}
      content={<ProgressFooterSaveNotification request={currentRequest} />}
      currentProgress={currentProgress}
      contextProgress="Classification.ContactVendors"
      nextProgress={nextProgress}
      prevLabel={prevContent}
      prevUrl={prevUrl}
      nextLabel={nextLabel}
      nextUrl={nextUrl}
      next={next}
      prev={prev}
      nextOnClick={nextOnClick}
      nextDisabled={nextDisabled}
      nextTooltip={nextDisabled && disabledNextTooltip}
    />
  );
};
