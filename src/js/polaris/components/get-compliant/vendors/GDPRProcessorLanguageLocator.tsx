import React from "react";
import {
  GDPRProcessorRecommendation,
  OrganizationDataRecipientDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  UpdateVendorFn,
  UpdateVendorParams,
} from "../../../hooks/get-compliant/vendors/useUpdateVendor";
import { VendorFlagWithDocumentation, VendorRadioOption } from "./VendorFlagWithDocumentation";

type Props = {
  recipients: OrganizationDataRecipientDto[];
  updateVendor: UpdateVendorFn;
};

type GDPRProcessorRecommendationKey = "processor" | "controller" | "unknown";

export const PROCESSOR_CLASSIFICATION_OPTIONS: VendorRadioOption<
  GDPRProcessorRecommendation,
  GDPRProcessorRecommendationKey
>[] = [
  {
    value: "Processor",
    type: "file",
    label: "I have confirmed this data recipient is a processor",
    key: "processor",
  },
  {
    value: "Controller",
    type: "file",
    label: "I have confirmed this data recipient is a controller",
    key: "controller",
  },
  {
    value: "Unknown",
    type: "normal",
    label: "I'm not sure how to classify this data recipient",
    key: "unknown",
  },
];

const initGdprProcessorParams = (params: UpdateVendorParams): UpdateVendorParams => ({
  gdprProcessorGuaranteeUrl: "",
  gdprProcessorGuaranteeFileKey: "",
  gdprProcessorGuaranteeFileName: "",
  gdprControllerGuaranteeUrl: "",
  gdprControllerGuaranteeFileKey: "",
  gdprControllerGuaranteeFileName: "",
  gdprContactProcessorStatus: null,
  ...params,
});

export const GDPRProcessorLanguageLocator: React.FC<Props> = ({ recipients, updateVendor }) => {
  const updateFlag = async (
    r: OrganizationDataRecipientDto,
    setting: GDPRProcessorRecommendation,
  ) => {
    await updateVendor(
      initGdprProcessorParams({
        vendorId: r.vendorId,
        gdprProcessorSetting: setting,
      }),
    );
  };

  const updateUrl = async (
    r: OrganizationDataRecipientDto,
    url: string,
    key: GDPRProcessorRecommendationKey,
  ) => {
    let updateParams = initGdprProcessorParams({
      vendorId: r.vendorId,
    });

    if (key === "processor") {
      updateParams.gdprProcessorSetting = "Processor";
      updateParams.gdprProcessorGuaranteeUrl = url;
    } else if (key === "controller") {
      updateParams.gdprProcessorSetting = "Controller";
      updateParams.gdprControllerGuaranteeUrl = url;
    }

    await updateVendor(updateParams);
  };

  const updateFile = async (
    r: OrganizationDataRecipientDto,
    filename: string,
    filekey: string,
    key: GDPRProcessorRecommendationKey,
  ) => {
    let updateParams = initGdprProcessorParams({
      vendorId: r.vendorId,
    });

    if (key === "processor") {
      updateParams.gdprProcessorGuaranteeFileName = filename;
      updateParams.gdprProcessorGuaranteeFileKey = filekey;
    } else if (key === "controller") {
      updateParams.gdprControllerGuaranteeFileName = filename;
      updateParams.gdprControllerGuaranteeFileKey = filekey;
    }

    await updateVendor(updateParams);
  };

  return (
    <VendorFlagWithDocumentation
      options={PROCESSOR_CLASSIFICATION_OPTIONS}
      recipients={recipients}
      onFlagChanged={updateFlag}
      onUrlChanged={updateUrl}
      onFileChanged={updateFile}
      getFlag={(r) => r.gdprProcessorSetting}
      getUrl={(r) =>
        r.gdprProcessorSetting === "Processor"
          ? r.gdprProcessorGuaranteeUrl
          : r.gdprControllerGuaranteeUrl
      }
      getFileName={(r) =>
        r.gdprProcessorSetting === "Processor"
          ? r.gdprProcessorGuaranteeFileName
          : r.gdprControllerGuaranteeFileName
      }
      getFileKey={(r) =>
        r.gdprProcessorSetting === "Processor"
          ? r.gdprProcessorGuaranteeFileKey
          : r.gdprControllerGuaranteeFileKey
      }
    />
  );
};
