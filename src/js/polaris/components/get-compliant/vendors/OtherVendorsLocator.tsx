import React, { ReactNode, useCallback } from "react";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  VendorClassificationDto,
  VendorContractDto,
} from "../../../../common/service/server/dto/VendorDto";
import { UpdateVendorFn } from "../../../hooks/get-compliant/vendors/useUpdateVendor";
import { VendorFlagWithDocumentation } from "./VendorFlagWithDocumentation";

type OtherVendorsLocatorProps = {
  vendors: OrganizationDataRecipientDto[];
  contractOptions: Record<string, VendorContractDto>;
  classificationOptions: Record<string, VendorClassificationDto>;
  updateVendor: UpdateVendorFn;
  vendorCallback: (v?: any) => void;
  children?: ReactNode;
};

export const OtherVendorsLocator = ({
  vendors,
  contractOptions,
  updateVendor,
  vendorCallback,
  classificationOptions,
}: OtherVendorsLocatorProps) => {
  //
  // Handlers
  //

  // TODO: This needs to be reworked in a way to allow us to debounce the
  // save independent form updating the form value;
  const handleRadioSelection = useCallback(
    (vendorId, vendorContractReviewedId) => {
      if (vendorContractReviewedId === contractOptions["provider-language-not-found"].id) {
        updateVendor({
          vendorId,
          vendorContractReviewedId,
          callback: vendorCallback,
          publicTosUrl: null,
          tosFileKey: null,
          tosFileName: null,
          classificationId: classificationOptions["needs-review"].id,
        });
      } else {
        updateVendor({
          vendorId,
          vendorContractReviewedId,
          callback: vendorCallback,
          classificationId: classificationOptions["service-provider"].id,
        });
      }
    },
    [contractOptions, updateVendor],
  );

  // TODO: This needs to be reworked in a way to allow us to debounce the
  // save independent form updating the form value;
  const handleUrl = useCallback(
    (vendorId, vendorUrl) => {
      updateVendor({
        vendorId,
        publicTosUrl: vendorUrl,
        vendorContractReviewedId: contractOptions["provider-language-found"].id,
        callback: vendorCallback,
        classificationId: classificationOptions["service-provider"].id,
      });
    },
    [updateVendor],
  );

  const handleFileUpload = useCallback(
    (vendorId, fileName, fileKey) => {
      updateVendor({
        vendorId: vendorId,
        tosFileName: fileName,
        tosFileKey: fileKey,
        callback: vendorCallback,
        vendorContractReviewedId: contractOptions["provider-language-found"].id,
        classificationId: classificationOptions["service-provider"].id,
      });
    },
    [updateVendor],
  );

  return (
    <VendorFlagWithDocumentation
      options={[
        {
          label: "Service Provider language found",
          value: contractOptions["provider-language-found"].id,
          type: "file",
          key: "provider-language-found",
        },
        {
          label: "Language not found (contact them in the next step)",
          value: contractOptions["provider-language-not-found"].id,
          type: "normal",
          key: "provider-language-not-found",
        },
      ]}
      recipients={vendors}
      onFlagChanged={async (dr, flag) => {
        handleRadioSelection(dr.vendorId, flag);
      }}
      onUrlChanged={async (dr, url) => {
        handleUrl(dr.vendorId, url);
      }}
      onFileChanged={async (dr, fileName, fileKey) => {
        handleFileUpload(dr.vendorId, fileName, fileKey);
      }}
      getFlag={(r) => r.vendorContractReviewed?.id}
      getUrl={(r) => r.publicTosUrl}
      getFileName={(r) => r.tosFileName}
      getFileKey={(r) => r.tosFileKey}
    />
  );
};
