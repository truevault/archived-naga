import React, { ReactNode, useCallback, useEffect, useMemo, useState } from "react";
import { ExternalLink } from "../../../../common/components/ExternalLink";
import { useActionRequest } from "../../../../common/hooks/api";
import { Api } from "../../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  VendorClassificationDto,
  VendorContractReviewedDto,
} from "../../../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { usePrimaryOrganizationId } from "../../../hooks";
import {
  DataRecipientRadioOptions,
  RadioOption,
} from "../../organisms/data-recipients/DataRecipientRadioOptions";

interface VendorClassifierProps {
  vendors: OrganizationDataRecipientDto[];
  updateVendor: (a?: any) => void;
  onChange: (classifications: Map<UUIDString, UUIDString>) => void;
  defaultClassification?: "service-provider" | "third-party" | "contractor" | "needs-review";
  hideNeedsReview?: true;
  children?: ReactNode;
  afterUpdateCallback?: (a?: any) => void;
  showVendorFilename?: boolean;
}

export const VendorClassifier = ({
  hideNeedsReview,
  vendors,
  updateVendor,
  onChange,
  defaultClassification,
  children,
  afterUpdateCallback = null,
  showVendorFilename = false,
}: VendorClassifierProps) => {
  const orgId = usePrimaryOrganizationId();
  const classificationBySlug = useMemo(
    () =>
      vendors?.[0]?.classificationOptions.reduce(
        (acc, c) => Object.assign(acc, { [c.slug]: c }),
        {} as Record<string, VendorClassificationDto>,
      ),
    [vendors],
  );
  const [vendorClassifications, setVendorClassifications] = useState(
    new Map<UUIDString, UUIDString>(),
  );

  const contractOptions = useMemo(
    () =>
      vendors?.[0]?.vendorContractReviewedOptions?.reduce(
        (acc, c) => Object.assign(acc, { [c.slug]: c }),
        {} as Record<string, VendorContractReviewedDto>,
      ),
    [vendors],
  );

  useEffect(() => {
    const classifications = vendors.reduce((acc, ov) => {
      const id = ov.vendorId;
      const classification =
        ov.classification?.id ?? classificationBySlug[defaultClassification]?.id ?? "";
      return acc.set(id, classification);
    }, new Map<UUIDString, UUIDString>());

    vendors
      .filter((ov) => !ov.classification)
      .forEach(({ vendorId }) => {
        if (classifications.get(vendorId)) {
          updateVendor({
            vendorId,
            classificationId: classifications.get(vendorId),
            callback: afterUpdateCallback,
          });
        }
      });

    setVendorClassifications(classifications);
  }, [vendors, defaultClassification, classificationBySlug, updateVendor]);

  // Handlers
  const handleRadioSelection = useCallback(
    (vendorId, classificationId) => {
      const currentClassificationId = vendorClassifications.get(vendorId);
      const resetState = () =>
        setVendorClassifications((prev) => new Map(prev).set(vendorId, currentClassificationId));
      setVendorClassifications((prev) => new Map(prev).set(vendorId, classificationId));
      updateVendor({ vendorId, classificationId, resetState, callback: afterUpdateCallback });
    },
    [vendorClassifications, setVendorClassifications, updateVendor],
  );

  const handleUrl = useCallback(
    async (r: OrganizationDataRecipientDto, url: string) => {
      updateVendor({
        vendorId: r.vendorId,
        publicTosUrl: url,
        vendorContractReviewedId: contractOptions["provider-language-found"].id,
        classificationId: classificationBySlug["service-provider"].id,
        callback: afterUpdateCallback,
      });
    },
    [updateVendor],
  );

  const { fetch: postVendorAgreement } = useActionRequest({
    api: async ({ vendorId, filename, filedata }) => {
      const result = await Api.storage.postOrganizationVendorAgreement(
        orgId,
        vendorId,
        filename,
        filedata,
      );

      await updateVendor({
        vendorId: vendorId,
        tosFileName: result.fileName,
        tosFileKey: result.fileKey,
        vendorContractReviewedId: contractOptions["provider-language-found"].id,
        classificationId: classificationBySlug["service-provider"].id,
        callback: afterUpdateCallback,
      });
    },
    messages: {
      forceError: "Failed to upload vendor agreement",
      success: "Uploaded vendor agreement",
    },
  });

  const { fetch: deleteVendorAgreement } = useActionRequest({
    api: async ({ vendorId, filekey }) => {
      const result = await Api.storage.deleteOrganizationVendorAgreement(orgId, vendorId, filekey);
      if (!result.error && result.vendorId) {
        const removeResult = await Api.organizationVendor.removeVendorAgreementFile(
          orgId,
          vendorId,
        );
        afterUpdateCallback?.(removeResult);
      }
    },
    messages: {
      forceError: "Failed to delete vendor agreement",
      success: "Deleted vendor agreement",
    },
  });

  const handleFileUpload = useCallback(
    async (r: OrganizationDataRecipientDto, fileName: string, file: File) => {
      await postVendorAgreement({ vendorId: r.vendorId, filename: fileName, filedata: file });
    },
    [updateVendor],
  );

  const handleFileDelete = useCallback(
    async (vendorId: UUIDString, filekey: string) => {
      await deleteVendorAgreement({ vendorId: vendorId, filekey: filekey });
    },
    [updateVendor],
  );

  useEffect(() => {
    onChange(vendorClassifications);
  }, [vendorClassifications, onChange]);

  return (
    <div className="classify-vendors--container">
      <div className="classify-vendors--vendors-container">
        {vendors
          .sort((a, b) => a.name.localeCompare(b.name))
          .map((ov) => {
            return (
              <VendorRadioSelection
                key={ov.id}
                organizationVendor={ov}
                hideNeedsReview={hideNeedsReview}
                vendorClassifications={vendorClassifications}
                classificationBySlug={classificationBySlug}
                handleRadioSelection={handleRadioSelection}
                disabled={ov.automaticallyClassified}
                disabledTooltip="This classification is required based on your previous answers."
                handleUrlChanged={handleUrl}
                handleFileUpload={handleFileUpload}
                handleFileDelete={handleFileDelete}
                showVendorFilename={showVendorFilename}
                context={
                  ov.serviceProviderLanguageUrl && (
                    <ExternalLink
                      className="text-t2 text-medium text-link"
                      href={ov.serviceProviderLanguageUrl}
                    >
                      View documentation
                    </ExternalLink>
                  )
                }
              />
            );
          })}
        {children}
      </div>
    </div>
  );
};
interface VendorRowProps {
  organizationVendor: OrganizationDataRecipientDto;
  vendorClassifications: Map<UUIDString, UUIDString>;
  classificationBySlug: Record<string, VendorClassificationDto>;
  handleRadioSelection: (vendorId: UUIDString, classificationId: string) => void;
  hideContractor?: false;
  hideNeedsReview?: true;
  context?: React.ReactNode | string;
  disabled?: boolean;
  disabledTooltip?: string;
  handleUrlChanged?: (r: OrganizationDataRecipientDto, url: string) => Promise<void>;
  handleFileDelete?: (vendorId: UUIDString, fileKey: string, key?: string) => Promise<void>;
  handleFileUpload?: (
    r: OrganizationDataRecipientDto,
    filename: string,
    file: File,
  ) => Promise<void>;
  showVendorFilename?: boolean;
}
const VendorRadioSelection: React.FC<VendorRowProps> = ({
  organizationVendor,
  hideNeedsReview,
  hideContractor,
  vendorClassifications,
  classificationBySlug,
  handleRadioSelection,
  context,
  disabled,
  disabledTooltip,
  handleUrlChanged,
  handleFileUpload,
  handleFileDelete = null,
  showVendorFilename = false,
}) => {
  const value = useMemo(
    () => vendorClassifications.get(organizationVendor.vendorId) || "",
    [vendorClassifications, organizationVendor.vendorId],
  );
  const onChange = (classificationId) => {
    handleRadioSelection(organizationVendor.vendorId, classificationId);
  };

  const options: RadioOption[] = [
    {
      value: classificationBySlug["service-provider"].id,
      label: classificationBySlug["service-provider"].name,
    },
    {
      value: classificationBySlug["third-party"].id,
      label: classificationBySlug["third-party"].name,
    },
  ];

  if (!hideContractor) {
    options.push({
      value: classificationBySlug["contractor"].id,
      label: classificationBySlug["contractor"].name,
    });
  }

  if (!hideNeedsReview) {
    options.push({
      value: classificationBySlug["needs-review"].id,
      label: classificationBySlug["needs-review"].name,
    });
  }

  return (
    <>
      <DataRecipientRadioOptions
        recipient={organizationVendor}
        description={context}
        options={options}
        value={value}
        onChange={onChange}
        disabled={disabled}
        disabledTooltip={disabledTooltip}
        showDocumentation={
          value == classificationBySlug["service-provider"].id &&
          organizationVendor.serviceProviderRecommendation != "SERVICE_PROVIDER"
        }
        url={organizationVendor.publicTosUrl}
        fileName={organizationVendor.tosFileName}
        fileKey={organizationVendor.tosFileKey}
        onUrlChanged={handleUrlChanged}
        onFileUpload={handleFileUpload}
        handleFileDelete={handleFileDelete}
        showFilename={showVendorFilename}
      />
    </>
  );
};
