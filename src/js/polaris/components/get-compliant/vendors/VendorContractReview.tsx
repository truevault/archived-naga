import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import React, { ReactNode, useCallback, useEffect, useState } from "react";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorContractReviewedDto } from "../../../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { UpdateVendorFn } from "../../../hooks/get-compliant/vendors/useUpdateVendor";

interface VendorContractReviewProps {
  title: string;
  contractReviewedOptions: Record<string, VendorContractReviewedDto>;
  vendors: OrganizationDataRecipientDto[];
  updateVendor: UpdateVendorFn;
  onChange: (vendorContractsReviewed: Record<UUIDString, UUIDString>) => void;
  hideNeedsReview?: true;
  showTos?: boolean;
  children?: ReactNode;
}

export const VendorContractReview = ({
  title,
  contractReviewedOptions,
  showTos,
  vendors,
  updateVendor,
  onChange,
}: VendorContractReviewProps) => {
  const [vendorContractReviewed, setVendorContractReviewed] = useState<
    Record<UUIDString, UUIDString>
  >({});

  useEffect(() => {
    const vendorContractReviewed = vendors.reduce((acc, ov) => {
      const id = ov.vendorId;
      const vendorContractReviewed = ov.vendorContractReviewed?.id ?? "";
      acc[id] = vendorContractReviewed;
      return acc;
    }, {});

    vendors
      .filter((ov) => !ov.vendorContractReviewed)
      .forEach(({ vendorId }) => {
        if (vendorContractReviewed[vendorId]) {
          updateVendor({ vendorId, contractReviewedId: vendorContractReviewed[vendorId] });
        }
      });

    setVendorContractReviewed(vendorContractReviewed);
  }, [vendors]);

  // Handlers
  const handleRadioSelection = useCallback(
    (vendorId, vendorContractReviewedId) => {
      const vendor = vendors.find((ov) => ov.vendorId == vendorId);
      const review = vendor?.vendorContractReviewedOptions?.find(
        (r) => r.id == vendorContractReviewedId,
      );
      const needsReviewId = vendor?.classificationOptions?.find(
        (c) => c.slug == "needs-review",
      )?.id;
      const serviceProviderId = vendor?.classificationOptions?.find(
        (c) => c.slug == "service-provider",
      )?.id;

      let classificationId = undefined;
      if (review) {
        if (review.slug == "provider-language-found") {
          classificationId = serviceProviderId;
        } else {
          classificationId = needsReviewId;
        }
      }

      const currentVendorContractReviewedId = vendorContractReviewed[vendorContractReviewedId];
      const resetState = () =>
        setVendorContractReviewed((prev) => ({
          ...prev,
          [vendorId]: currentVendorContractReviewedId,
        }));
      setVendorContractReviewed((prev) => ({ ...prev, [vendorId]: vendorContractReviewedId }));
      updateVendor({
        vendorId,
        vendorContractReviewedId,
        classificationId,
        resetState,
      });
    },
    [vendorContractReviewed, setVendorContractReviewed, updateVendor, vendors],
  );

  useEffect(() => {
    onChange(vendorContractReviewed);
  }, [vendorContractReviewed]);

  return (
    <div className="classify-vendors--container">
      <div className="classify-vendors--container--heading">
        <h3>{title}</h3>
      </div>

      <div className="classify-vendors--vendors-container">
        {vendors.length > 0 ? (
          vendors
            .sort((a, b) => a.name.localeCompare(b.name))
            .map((ov) => (
              <VendorRow
                key={`vendor-row-${ov.vendorId}`}
                organizationVendor={ov}
                vendorContractsReviewed={vendorContractReviewed}
                vendorContractReviewedBySlug={contractReviewedOptions}
                showTos={showTos}
                handleRadioSelection={handleRadioSelection}
              />
            ))
        ) : (
          <span className="text-style-italic">No vendor contracts to review.</span>
        )}
      </div>
    </div>
  );
};

interface VendorRadioSectionProps {
  vendorContractReviewId: UUIDString;
  vendorContractReviewedBySlug: Record<string, VendorContractReviewedDto>;
  handleSelection: (vendorContractReviewId: string) => void;
}

const FOUND_SLUG = "provider-language-found";
const NOT_FOUND_SLUG = "provider-language-not-found";
const VendorRadioSection: React.FC<VendorRadioSectionProps> = ({
  vendorContractReviewId: vendorContractReviewId,
  vendorContractReviewedBySlug,
  handleSelection,
}) => {
  const found = vendorContractReviewedBySlug?.[FOUND_SLUG];
  const notFound = vendorContractReviewedBySlug?.[NOT_FOUND_SLUG];

  return (
    <div className="data-recipient-radio-options__options">
      <RadioGroup
        value={vendorContractReviewId || ""}
        onChange={({ target }) => handleSelection(target.value)}
        className="form-group--row"
      >
        <FormControlLabel
          value={found?.id}
          control={<Radio color="primary" />}
          label={found?.name}
        />
        <FormControlLabel
          value={notFound?.id}
          control={<Radio color="primary" />}
          label={notFound?.name}
        />
      </RadioGroup>
    </div>
  );
};

interface VendorRowProps {
  organizationVendor: OrganizationDataRecipientDto;
  showTos: boolean;
  vendorContractsReviewed: Record<UUIDString, UUIDString>;
  vendorContractReviewedBySlug: Record<string, VendorContractReviewedDto>;
  handleRadioSelection: (vendorId: UUIDString, vendorContractReviewedId: string) => void;
}
const VendorRow: React.FC<VendorRowProps> = ({
  organizationVendor,
  showTos,
  vendorContractsReviewed,
  vendorContractReviewedBySlug,
  handleRadioSelection,
}) => {
  const tosLink = Boolean(showTos && organizationVendor.publicTosUrl);
  return (
    <div className="data-recipient-radio-options">
      <div className="data-recipient-radio-options__logo">
        <img src={organizationVendor.logoUrl} />
      </div>
      <div className="data-recipient-radio-options__info flex-grow">
        <div className="classify-vendors--vendor-name">{organizationVendor.name}</div>
        {tosLink && (
          <a href={organizationVendor.publicTosUrl} target="_blank" rel="noopener noreferrer">
            View documentation
          </a>
        )}
      </div>

      <VendorRadioSection
        vendorContractReviewedBySlug={vendorContractReviewedBySlug}
        vendorContractReviewId={vendorContractsReviewed[organizationVendor.vendorId] || ""}
        handleSelection={(vendorContractReviewId) =>
          handleRadioSelection(organizationVendor.vendorId, vendorContractReviewId)
        }
      />
    </div>
  );
};
