import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { useActionRequest } from "../../../../common/hooks/api";
import { Api } from "../../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { isUrl } from "../../../util/validation";
import { DataRecipientLogo } from "../../DataRecipientLogo";
import { RadioWithFileReference } from "./RadioWithFileReference";

type Props = {
  recipients: OrganizationDataRecipientDto[];
  options: VendorRadioOption[];

  onFlagChanged: (
    recipient: OrganizationDataRecipientDto,
    flag: VendorFlagOption,
    key?: string,
  ) => Promise<void>;

  onUrlChanged: (
    recipient: OrganizationDataRecipientDto,
    url: string,
    key?: string,
  ) => Promise<void>;

  onFileChanged: (
    recipient: OrganizationDataRecipientDto,
    filename: string,
    filekey: string,
    key?: string,
  ) => Promise<void>;

  getFlag: (recipient: OrganizationDataRecipientDto) => VendorFlagOption;
  getUrl: (recipient: OrganizationDataRecipientDto) => string | undefined;
  getFileName: (recipient: OrganizationDataRecipientDto) => string | undefined;
  getFileKey: (recipient: OrganizationDataRecipientDto) => string | undefined;
};

export const VendorFlagWithDocumentation: React.FC<Props> = ({
  recipients,
  options,

  onFlagChanged,
  onUrlChanged,
  onFileChanged,

  getFlag,
  getUrl,
  getFileName,
  getFileKey,
}) => {
  const orgId = usePrimaryOrganizationId();

  //
  // Handlers
  //

  const { fetch: postVendorAgreement } = useActionRequest({
    api: async ({ vendorId, filename, filedata, key }) => {
      const result = await Api.storage.postOrganizationVendorAgreement(
        orgId,
        vendorId,
        filename,
        filedata,
      );

      if (!result.error && result.vendorId) {
        const r = recipients.find((r) => r.vendorId == result.vendorId);
        await onFileChanged(r, result.fileName, result.fileKey, key);
      }
    },
    messages: {
      forceError: "Failed to upload vendor agreement",
      success: "Uploaded vendor agreement",
    },
  });

  const { fetch: deleteVendorAgreement } = useActionRequest({
    api: async ({ vendorId, filekey, key }) => {
      const result = await Api.storage.deleteOrganizationVendorAgreement(orgId, vendorId, filekey);
      if (!result.error && result.vendorId) {
        const r = recipients.find((r) => r.vendorId == result.vendorId);
        await onFileChanged(r, result.fileName, result.fileKey, key);
      }
    },
    messages: {
      forceError: "Failed to delete vendor agreement",
      success: "Deleted vendor agreement",
    },
  });

  const handleFileUpload = async (
    vendorId: string,
    filename: string,
    filedata: File,
    key?: string,
  ) => postVendorAgreement({ vendorId, filename, filedata, key });

  const handleFileDelete = (vendorId: string, filekey: string, key?: string) =>
    deleteVendorAgreement({ vendorId, filekey, key });

  return (
    <div className="classify-vendors--container">
      <div className="classify-vendors--vendors-container">
        {recipients
          .sort((a, b) => a.name.localeCompare(b.name))
          .map((r) => {
            return (
              <VendorRow
                key={r.id}
                options={options}
                recipient={r}
                selected={getFlag(r)}
                url={getUrl(r)}
                fileName={getFileName(r)}
                fileKey={getFileKey(r)}
                onFlagChanged={(flag, key) => onFlagChanged(r, flag, key)}
                onUrlChanged={(url, key) => onUrlChanged(r, url, key)}
                handleFileUpload={handleFileUpload}
                handleFileDelete={handleFileDelete}
              />
            );
          })}
      </div>
    </div>
  );
};

type VendorRowProps = {
  recipient: OrganizationDataRecipientDto;

  options: VendorRadioOption[];

  selected: VendorFlagOption;
  url?: string;
  fileName?: string;
  fileKey?: string;

  onFlagChanged: (selected: VendorFlagOption, key?: string) => Promise<void>;
  onUrlChanged: (url: string, key?: string) => Promise<void>;
  handleFileUpload: (
    vendorId: UUIDString,
    fileName: string,
    fileData: File,
    key?: string,
  ) => Promise<void>;
  handleFileDelete: (vendorId: UUIDString, fileKey: string, key?: string) => Promise<void>;
};
const VendorRow: React.FC<VendorRowProps> = ({
  recipient,
  options,

  selected,
  url,
  fileName,
  fileKey,

  onFlagChanged,
  onUrlChanged,
  handleFileUpload,
  handleFileDelete,
}) => {
  return (
    <div className="data-recipient-radio-options">
      <div className="data-recipient-radio-options__logo">
        <DataRecipientLogo dataRecipient={recipient} size={24} />
      </div>

      <div className="data-recipient-radio-options__info">
        <div className="classify-vendors--vendor-name">{recipient.name}</div>
      </div>

      <VendorRadioSection
        value={selected}
        options={options}
        url={url}
        fileName={fileName}
        fileKey={fileKey}
        onSelectionChange={onFlagChanged}
        onUrlChange={onUrlChanged}
        onFileUpload={(filename, filedata, key) =>
          handleFileUpload(recipient.vendorId, filename, filedata, key)
        }
        onFileDelete={(fileKey, optionKey) =>
          handleFileDelete(recipient.vendorId, fileKey, optionKey)
        }
      />
    </div>
  );
};

export type VendorFlagOption = string | number | null;

export type VendorRadioOption<
  TValue extends VendorFlagOption = VendorFlagOption,
  TKey extends string = string,
> = {
  key: TKey;
  label: string;
  value: TValue;
  type: "file" | "normal";
  disabled?: boolean;
  visible?: boolean;
};

type VendorRadioSectionProps = {
  options: VendorRadioOption[];
  value: VendorFlagOption;
  url?: string;
  fileName?: string;
  fileKey?: string;
  disabled?: boolean;
  hideDocumentationInput?: boolean;
  onSelectionChange: (option: VendorFlagOption, key?: string) => Promise<void>;
  onUrlChange: (vendorContractUrl: string, key?: string) => Promise<void>;
  onFileUpload: (fileName: string, fileData: File, key?: string) => Promise<void>;
  onFileDelete: (fileKey: string, key?: string) => Promise<void>;
};

export const VendorRadioSection: React.FC<VendorRadioSectionProps> = ({
  options,
  value,
  url,
  fileName,
  fileKey,
  hideDocumentationInput = false,
  onSelectionChange,
  onUrlChange,
  onFileUpload,
  onFileDelete,
  disabled = false,
}) => {
  const errorMessage = useMemo(
    () => (url && !isUrl(url) ? "Please provide a valid URL" : null),
    [url],
  );

  const [loading, setLoading] = useState(false);
  const [localSelection, setLocalSelection] = useState(value);
  const [fileError, setFileError] = useState({});
  const visibleOptions = useMemo(() => options.filter((o) => o.visible !== false), [options]);

  useEffect(() => {
    setLocalSelection(value);
  }, [value]);

  const handleSelectionChange = async ({ target }) => {
    setLoading(true);
    try {
      const selectedOption = options.find((option) => option.value === target.value);
      setLocalSelection(selectedOption.value);
      await onSelectionChange(selectedOption.value, selectedOption.key);
    } finally {
      setLoading(false);
    }
  };

  const handleUrlChange = async (newUrl: string, key: string) => {
    setLoading(true);
    try {
      await onUrlChange(newUrl, key);
    } finally {
      setLoading(false);
    }
  };

  const handleFileUpload = async (e, key: string) => {
    const file = e.target.files[0];
    if (file && file.size <= 15000000) {
      setLoading(true);
      setFileError((error) => ({ ...error, [key]: null }));
      try {
        await onFileUpload(file.name, file, key);
      } finally {
        setLoading(false);
      }
    } else {
      setFileError((error) => ({ ...error, [key]: "(max file size: 15MB)" }));
    }
  };

  const handleFileDelete = async () => {
    setLoading(true);
    try {
      await onFileDelete(fileKey);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="data-recipient-radio-options__options">
      <RadioGroup value={localSelection} onChange={handleSelectionChange} className="w-100">
        {visibleOptions.map((o) => {
          return o.type === "file" ? (
            <RadioWithFileReference
              key={o.key}
              error={errorMessage ?? fileError[o.key]}
              url={url}
              onUrlChanged={(newUrl) => handleUrlChange(newUrl, o.key)}
              onFileChanged={(e) => handleFileUpload(e, o.key)}
              onFileDeleted={handleFileDelete}
              filename={fileName}
              label={o.label}
              value={o.value}
              hideUrlInput={hideDocumentationInput || localSelection !== o.value}
              disabled={disabled || o.disabled === true}
              loading={loading}
            />
          ) : (
            <FormControlLabel
              key={o.key}
              value={o.value}
              control={<Radio color="primary" />}
              label={o.label}
              disabled={disabled || o.disabled === true || loading}
            />
          );
        })}
      </RadioGroup>
    </div>
  );
};
