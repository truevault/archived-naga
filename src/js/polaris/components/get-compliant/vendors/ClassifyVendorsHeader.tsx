import React, { ReactNode } from "react";
import { PageHeader } from "../../layout/header/Header";

interface ClasifyVendorsHeaderProps {
  title: string;
  notesContent?: ReactNode;
  children?: ReactNode;
}

export const ClassifyVendorsHeader = ({
  title,
  notesContent,
  children,
}: ClasifyVendorsHeaderProps) => {
  return (
    <PageHeader titleContent={title} notesContent={notesContent} descriptionContent={children} />
  );
};
