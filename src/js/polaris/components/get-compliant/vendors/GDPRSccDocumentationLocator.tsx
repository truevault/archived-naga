import React from "react";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UpdateVendorFn } from "../../../hooks/get-compliant/vendors/useUpdateVendor";
import {
  VendorFlagOption,
  VendorFlagWithDocumentation,
  VendorRadioOption,
} from "./VendorFlagWithDocumentation";

type Props = {
  recipients: OrganizationDataRecipientDto[];
  updateVendor: UpdateVendorFn;
  refresh: () => void;
};

type SelectedBoolean = "selected" | "not-selected";

export const SCC_OPTIONS: VendorRadioOption<SelectedBoolean, SelectedBoolean>[] = [
  {
    value: "selected",
    type: "file",
    label: "SCCs Found",
    key: "selected",
  },
  {
    value: "not-selected",
    type: "normal",
    label: "SCCs not found",
    key: "not-selected",
  },
];

export const GDPRSccDocumentationLocator: React.FC<Props> = ({
  recipients,
  updateVendor,
  refresh,
}) => {
  const updateSelection = async (r: OrganizationDataRecipientDto, flag: VendorFlagOption) => {
    let setting: boolean | null = null;
    if (flag == "selected") {
      setting = true;
    } else if (flag == "not-selected") {
      setting = false;
    }
    await updateVendor({
      vendorId: r.vendorId,
      gdprHasSccSetting: setting,
      gdprSccUrl: "",
      gdprSccFileKey: "",
      gdprSccFileName: "",
    });
    refresh();
  };

  const updateUrl = async (r: OrganizationDataRecipientDto, url: string) => {
    await updateVendor({ vendorId: r.vendorId, gdprSccUrl: url });
    refresh();
  };

  const updateFile = async (r: OrganizationDataRecipientDto, name: string, key: string) => {
    await updateVendor({
      vendorId: r.vendorId,
      gdprSccFileName: name,
      gdprSccFileKey: key,
    });
    refresh();
  };

  return (
    <VendorFlagWithDocumentation
      recipients={recipients}
      options={SCC_OPTIONS}
      onFlagChanged={updateSelection}
      onUrlChanged={updateUrl}
      onFileChanged={updateFile}
      getFlag={(r) =>
        r.gdprHasSccSetting === true
          ? "selected"
          : r.gdprHasSccSetting === false
          ? "not-selected"
          : null
      }
      getUrl={(r) => r.gdprSccUrl}
      getFileName={(r) => r.gdprSccFileName}
      getFileKey={(r) => r.gdprSccFileKey}
    />
  );
};
