import { Link, UploadFile, Attachment, Close } from "@mui/icons-material";
import {
  TextField,
  ToggleButton,
  ToggleButtonGroup,
  Tooltip,
  Icon,
  IconButton,
  InputAdornment,
} from "@mui/material";
import _ from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import { FileUploadButton } from "./FileUploadButton";
import { FILE_TYPE_UPLOADED, FILE_TYPE_URL } from "./RadioWithFileReference";
import {} from "@mui/icons-material";

export const UrlOrFileInput: React.FC<{
  url: string;
  filename: string;
  error?: string;
  onUrlChanged: (newUrl: string) => void;
  onFileChanged: React.ChangeEventHandler<HTMLInputElement>;
  disabled?: boolean;
  showUploadedFile?: boolean;
  handleFileDelete?: () => void;
}> = ({
  url,
  onUrlChanged,
  filename,
  onFileChanged,
  disabled,
  error,
  showUploadedFile = false,
  handleFileDelete,
}) => {
  const [internalUrl, setInternalUrl] = useState(url);
  const [originalUrl, setOriginalUrl] = useState(url);

  useEffect(() => {
    setInternalUrl(url);
    setOriginalUrl(url);
  }, [url]);

  const hasFile = Boolean(filename);
  const [fileType, setFileType] = useState(() => (hasFile ? FILE_TYPE_UPLOADED : FILE_TYPE_URL));

  const persistUrlChange = (newUrl: string) => {
    if (newUrl != originalUrl) {
      onUrlChanged(newUrl);
    }
  };

  const debouncedPersistUrlChange = useCallback(_.debounce(persistUrlChange, 2000), []);

  const handleOnUrlChange = (e) => {
    const newUrl = e.target.value;
    switch (e.type) {
      case "change":
        setInternalUrl(newUrl);
        debouncedPersistUrlChange(newUrl);
        break;
      case "blur":
        debouncedPersistUrlChange.cancel();
        persistUrlChange(newUrl);
        break;
      case "keyup":
        if (e.keyCode === 13) {
          // user clicked "Enter"
          debouncedPersistUrlChange.cancel();
          persistUrlChange(newUrl);
        }
        break;
    }
  };

  return (
    <div className="d-flex flex-row align-items-center w-400">
      {!!filename && showUploadedFile ? (
        <TextField
          size="small"
          variant="outlined"
          disabled
          value={filename ?? ""}
          fullWidth
          InputProps={{
            readOnly: true,
            startAdornment: (
              <InputAdornment position="start">
                <Icon>
                  <Attachment />
                </Icon>
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={handleFileDelete} size="small" edge="end">
                  <Close />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      ) : (
        <>
          <ToggleButtonGroup
            size="small"
            value={fileType}
            onChange={(_e, newType) => setFileType(newType)}
            exclusive
            className="mr-sm"
            disabled={disabled}
          >
            <ToggleButton disabled={disabled} value={FILE_TYPE_URL}>
              <Tooltip title="Set a Link">
                <Link />
              </Tooltip>
            </ToggleButton>

            <ToggleButton disabled={disabled} value={FILE_TYPE_UPLOADED}>
              <Tooltip title="Upload a File">
                <UploadFile />
              </Tooltip>
            </ToggleButton>
          </ToggleButtonGroup>

          {fileType === FILE_TYPE_UPLOADED ? (
            <FileUploadButton onChange={onFileChanged} disabled={disabled} />
          ) : (
            <TextField
              size="small"
              variant="outlined"
              fullWidth
              value={internalUrl ?? ""}
              error={Boolean(error)}
              disabled={disabled}
              onChange={handleOnUrlChange}
              onBlur={handleOnUrlChange}
              onKeyUp={handleOnUrlChange}
              placeholder="http://example.com/terms-of-service"
              InputProps={{ type: "text" }}
              InputLabelProps={{ required: false }}
              label="URL to public documentation"
              required
            />
          )}
        </>
      )}
    </div>
  );
};
