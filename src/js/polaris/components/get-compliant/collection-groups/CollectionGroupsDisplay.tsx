import { Add } from "@mui/icons-material";
import { Button, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import React from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { Callout, CalloutVariant } from "../../Callout";

type Props = {
  groups: CollectionGroupDetailsDto[];
  setEditGroup?: (CollectionGroupDetailsDto) => void;
  setDeleteGroup?: (string) => void;
  onAdd?: () => void;
  description?: boolean;
};

export const CollectionGroupsDisplay: React.FC<Props> = ({
  groups,
  setDeleteGroup,
  setEditGroup,
  onAdd,
  description = true,
}) => {
  if (groups.length == 0) {
    return (
      <Callout variant={CalloutVariant.Clear}>
        You haven’t added any collection groups yet.{" "}
        <a
          href="#"
          onClick={(e) => {
            e.preventDefault();
            onAdd();
          }}
        >
          Add a new collection group.
        </a>
      </Callout>
    );
  }

  return (
    <GroupsTable
      groups={groups}
      setDeleteGroup={setDeleteGroup}
      setEditGroup={setEditGroup}
      onAdd={onAdd}
      description={description}
    />
  );
};

type GroupsTableProps = {
  groups: CollectionGroupDetailsDto[];
  setEditGroup?: (CollectionGroupDetailsDto) => void;
  setDeleteGroup?: (string) => void;
  onAdd?: () => void;
  description?: boolean;
};
const GroupsTable: React.FC<GroupsTableProps> = ({
  groups,
  setEditGroup,
  setDeleteGroup,
  onAdd,
  description = true,
}) => {
  return (
    <>
      <Table aria-label="simple table" className="mb-lg">
        <TableHead>
          <TableRow>
            {description && <TableCell>Description</TableCell>}
            <TableCell>Nickname</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {groups.map((group) => {
            return (
              <TableRow key={group.id}>
                {description && <TableCell>{group.description}</TableCell>}
                <TableCell>{group.name}</TableCell>
                <TableCell align="right" style={{ whiteSpace: "nowrap" }}>
                  {Boolean(setEditGroup) && (
                    <Button
                      color="secondary"
                      onClick={(e) => {
                        e.preventDefault();
                        setEditGroup(group);
                      }}
                    >
                      Edit
                    </Button>
                  )}
                  {Boolean(setDeleteGroup) && (
                    <Button
                      color="error"
                      className="ml-sm"
                      onClick={(e) => {
                        e.preventDefault();
                        setDeleteGroup(group);
                      }}
                    >
                      Remove
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>

      {Boolean(onAdd) && (
        <div>
          <Button
            variant="text"
            color="secondary"
            onClick={(e) => {
              e.preventDefault();
              onAdd();
            }}
            startIcon={<Add />}
          >
            Add
          </Button>
        </div>
      )}
    </>
  );
};
