import React, { ReactNode, useCallback, useEffect, useMemo } from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { CategoryDto, CollectionDto } from "../../../../common/service/server/dto/DataMapDto";
import { OrganizationPublicId, UUIDString } from "../../../../common/service/server/Types";
import { DescriptionCheckbox } from "../../input/DescriptionCheckbox";
import { categorySorter } from "../../../util/dataInventory";
import { QuestionLabel } from "../survey/QuestionContainer";
import { useDataMap } from "../../../hooks/useDataMap";

type CollectionCheckboxesProps = {
  label?: string | ReactNode;
  helpText?: ReactNode;
  categories: CategoryDto[];
  orgId: OrganizationPublicId;
  group: CollectionGroupDetailsDto;
  collected: CollectionDto;
  byGroup?: boolean;
  allCheckbox?: boolean;
  disabledCategoryIds?: UUIDString[];
  containerClassName?: string;
  onChange?: (collectedIds: string[]) => void;
  disabledTooltipFn?: (checked: boolean) => string;
};

export const CollectionCheckboxes: React.FC<CollectionCheckboxesProps> = ({
  label,
  helpText,
  categories,
  orgId,
  group,
  collected,
  byGroup = true,
  allCheckbox = false,
  disabledCategoryIds = [],
  containerClassName = "my-xl",
  disabledTooltipFn,
  onChange,
}) => {
  const [, , , updateCollection] = useDataMap(orgId);

  const collectedIds = useMemo(() => collected?.collected ?? [], [collected]);

  const sorted = useMemo(() => categories?.sort(categorySorter) ?? [], [categories]);

  const allChecked = useMemo(
    () => categories.every((c) => collectedIds.includes(c.id)),
    [categories, collectedIds],
  );

  const handleChecked = useCallback(
    (id: UUIDString, isCollected: boolean) =>
      updateCollection({ collectionGroupId: group.id, id, isCollected }),
    [group, updateCollection],
  );

  const handleAllChecked = (all: boolean) => {
    categories.forEach((c) => handleChecked(c.id, all));
  };

  useEffect(() => {
    if (onChange) {
      onChange(collectedIds);
    }
    // onChange is not usually memoized and causes a runaway
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [collectedIds]);

  let picGroupDisplays = [{ label: undefined, categories: sorted }];

  if (byGroup) {
    const picGroups = [...new Set(categories.map((c) => c.picGroupId))];
    picGroupDisplays = picGroups.map((groupId) => {
      const matching = categories.filter((c) => c.picGroupId == groupId);
      return {
        label: matching[0].picGroupName,
        categories: matching,
      };
    });
  }

  return (
    <>
      {Boolean(label) && <QuestionLabel label={label} helpText={helpText} />}

      <div className={containerClassName}>
        {allCheckbox && (
          <DescriptionCheckbox
            className="mb-lg"
            label="Select All"
            checked={allChecked}
            onChange={(e, checked) => handleAllChecked(checked)}
          />
        )}

        {picGroupDisplays.map((picGroup, idx) => {
          return (
            <CollectionCheckboxGroup
              key={idx}
              {...picGroup}
              collectedIds={collectedIds}
              onChecked={handleChecked}
              disabledCategoryIds={disabledCategoryIds}
              disabledTooltipFn={disabledTooltipFn}
            />
          );
        })}
      </div>
    </>
  );
};
type CollectionCheckboxesGroupProps = {
  categories: CategoryDto[];
  collectedIds: string[];
  label: string | undefined;
  onChecked: (id: string, isCollected: boolean) => void;
  disabledCategoryIds: string[];
  disabledTooltipFn?: (checked: boolean) => string;
};
const CollectionCheckboxGroup: React.FC<CollectionCheckboxesGroupProps> = ({
  categories,
  collectedIds,
  label,
  onChecked,
  disabledCategoryIds,
  disabledTooltipFn,
}) => {
  const render = (cat: CategoryDto) => {
    let disabledTooltip: string | undefined;
    if (disabledTooltipFn) {
      disabledTooltip = disabledTooltipFn(collectedIds.includes(cat.id));
    }
    return (
      <DescriptionCheckbox
        key={cat.id}
        label={cat.name}
        description={cat.description}
        checked={collectedIds.includes(cat.id)}
        onChange={(e, checked) => {
          onChecked(cat.id, checked);
        }}
        disabled={disabledCategoryIds.includes(cat.id)}
        disabledTooltip={disabledTooltip}
        color="primary"
      />
    );
  };

  const left = categories.filter((_, idx) => idx % 2 == 0);
  const right = categories.filter((_, idx) => idx % 2 == 1);

  return (
    <>
      {Boolean(label) && <h5>{label}</h5>}
      <div className="flex-row">
        <div className="flex-col w-50">{left.map(render)}</div>
        <div className="flex-col w-50">{right.map(render)}</div>
      </div>
    </>
  );
};
