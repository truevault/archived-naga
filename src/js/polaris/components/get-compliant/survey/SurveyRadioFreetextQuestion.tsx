import React, { ReactNode } from "react";
import { FadeTransition } from "../../../pages/get-compliant/FadeTransition";
import { RadioSelectionFreeTextQuestion } from "../../organisms/survey/RadioSelectionFreeTextQuestion";
import { BaseQuestion, BaseQuestionProps } from "./BaseQuestion";
import { SurveyRadioOption } from "./SurveyRadioQuestion";

export type SurveyRadioFreetextQuestionProps = BaseQuestion & {
  type: "radio-freetext";
  options: SurveyRadioOption[];
  validateFreetext: (answer) => string | null;
  otherCGAnswers: string[];
  emptyText?: string | ReactNode;
};

type Props = BaseQuestionProps & {
  question: SurveyRadioFreetextQuestionProps;
};

export const SurveyRadioFreetextQuestion = ({ question, answer, onAnswer, ...rest }: Props) => {
  if (question.notPresent) {
    return null;
  }

  return (
    <FadeTransition
      key={`question-${question.slug}`}
      cls="survey-question-fade"
      in={!!answer || question.visible}
      unmountOnExit
      appear
    >
      <div className="survey--question" ref={question.ref}>
        <RadioSelectionFreeTextQuestion
          question={question.question}
          options={question.options}
          validateFreeText={question.validateFreetext}
          otherAnswers={question.otherCGAnswers}
          disabled={rest.disabled}
          disabledTooltip={rest.disabledTooltip}
          answer={answer}
          setAnswer={onAnswer}
        />
      </div>
    </FadeTransition>
  );
};
