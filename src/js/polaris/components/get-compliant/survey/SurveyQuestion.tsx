import React from "react";
import { BaseQuestionProps } from "./BaseQuestion";
import { AddDataSourceQuestion, AddDataSourceQuestionProps } from "./custom/AddDataSourceQuestion";
import {
  CustomFinancialIncentivesQuestion,
  CustomFinancialIncentivesQuestionProps,
} from "./custom/CustomFinancialIncentivesSurveyQuestion";
import {
  NamesForIndividualConsumerQuestion,
  NamesForIndividualConsumerQuestionProps,
} from "./custom/NamesForIndividualConsumersQuestion";
import {
  SurveyAcknowledgementQuestion,
  SurveyAcknowledgementQuestionProps,
} from "./SurveyAcknowledgementQuestion";
import { SurveyBooleanQuestion, SurveyBooleanQuestionProps } from "./SurveyBooleanQuestion";
import {
  SurveyCheckboxesQuestion,
  SurveyCheckboxesQuestionProps,
} from "./SurveyCheckboxesQuestion";
import { SurveyFreetextQuestion, SurveyFreetextQuestionProps } from "./SurveyFreetextQuestion";
import {
  SurveyRadioFreetextQuestion,
  SurveyRadioFreetextQuestionProps,
} from "./SurveyRadioFreetextQuestion";
import { SurveyRadioQuestion, SurveyRadioQuestionProps } from "./SurveyRadioQuestion";

export type SurveyQuestionProps =
  | SurveyBooleanQuestionProps
  | SurveyCheckboxesQuestionProps
  | SurveyRadioQuestionProps
  | SurveyRadioFreetextQuestionProps
  | SurveyAcknowledgementQuestionProps
  | SurveyFreetextQuestionProps
  | NamesForIndividualConsumerQuestionProps
  | AddDataSourceQuestionProps
  | CustomFinancialIncentivesQuestionProps;

type Props = BaseQuestionProps & {
  question: SurveyQuestionProps;
};

export const SurveyQuestion = ({ question: q, ...rest }: Props) => {
  switch (q.type) {
    case "boolean":
      return <SurveyBooleanQuestion question={q} {...rest} />;
    case "checkboxes":
      return <SurveyCheckboxesQuestion question={q} {...rest} />;
    case "radio":
      return <SurveyRadioQuestion question={q} {...rest} />;
    case "radio-freetext":
      return <SurveyRadioFreetextQuestion question={q} {...rest} />;
    case "acknowledgement":
      return <SurveyAcknowledgementQuestion question={q} {...rest} />;
    case "freetext":
      return <SurveyFreetextQuestion question={q} {...rest} />;

    case "custom-names-for-individual-consumer":
      return <NamesForIndividualConsumerQuestion question={q} {...rest} />;
    case "custom-add-data-source":
      return <AddDataSourceQuestion question={q} {...rest} />;
    case "custom-edit-financial-incentives":
      return <CustomFinancialIncentivesQuestion question={q} {...rest} />;
  }
};
