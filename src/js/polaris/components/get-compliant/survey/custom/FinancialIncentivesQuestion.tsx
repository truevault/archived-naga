import React, { FC, useCallback, useState } from "react";
import { Organization } from "../../../../../common/models";
import { OrgSettingQuestion } from "../../../../surveys/orgSettings";
import { useQueryClient } from "@tanstack/react-query";
import { Api } from "../../../../../common/service/Api";
import { SurveyBooleanQuestion } from "../SurveyBooleanQuestion";
import { HelpDialog } from "../../../dialog";
import { Button } from "@mui/material";
import { Routes } from "../../../../root/routes";

export const FinancialIncentivesQuestion: FC<{
  org: Organization;
  question: OrgSettingQuestion;
  answer: boolean | undefined;
  setAnswer: (question: OrgSettingQuestion, answer: string) => Promise<void>;
  enableTask: boolean;
}> = ({ org, question, answer, setAnswer, enableTask }) => {
  const qc = useQueryClient();
  const [alertOpen, setAlertOpen] = useState<boolean>(false);

  const submitAnswer = useCallback(
    async (newAnswer: string) => {
      await setAnswer(question, newAnswer);

      if (enableTask) {
        if (newAnswer === "true") {
          const task = await Api.tasks.createCompleteFinancialIncentives(org.id);
          if (task) {
            setAlertOpen(true);
          }
        }
      }
      // invalidate org queries to re-fetch
      setTimeout(() => qc.invalidateQueries(["organization", org.id]), 1000);
    },
    [question, setAnswer, qc],
  );

  const onCloseAlert = () => setAlertOpen(false);

  return (
    <>
      <SurveyBooleanQuestion
        answer={answer?.toString()}
        onAnswer={submitAnswer}
        question={{
          type: "boolean",
          slug: question.slug,
          question: question.question,
          helpText: question.help,
          visible: true,
          noConfirmation:
            Boolean(org.financialIncentiveDetails) &&
            "Changing your answer to 'No' will remove your existing Financial Incentive Notice",
        }}
      />
      <HelpDialog
        open={alertOpen}
        onClose={onCloseAlert}
        title="Task Created"
        maxWidth="sm"
        actions={
          <Button variant="contained" color="primary" onClick={onCloseAlert}>
            Close
          </Button>
        }
      >
        <p>
          We've added a task to your{" "}
          <a href={Routes.tasks.todo} target="_blank" rel="noreferrer">
            Task List
          </a>{" "}
          to add more details about the financial incentives you offer. To complete your Financial
          Incentive disclosure now, go to the{" "}
          <a href={Routes.privacyCenter.root} target="_blank" rel="noreferrer">
            Notices page
          </a>
          .
        </p>
      </HelpDialog>
    </>
  );
};
