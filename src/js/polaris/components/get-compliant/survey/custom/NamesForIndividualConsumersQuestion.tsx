import { Add, Close as CloseIcon } from "@mui/icons-material";
import { Button, IconButton, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import { FadeTransition } from "../../../../pages/get-compliant/FadeTransition";
import { BaseQuestion, BaseQuestionProps } from "../BaseQuestion";
import { QuestionContainer, QuestionLabel } from "../QuestionContainer";

export type NamesForIndividualConsumerQuestionProps = BaseQuestion & {
  type: "custom-names-for-individual-consumer";
  otherCGAnswers: string[];
};

type Props = BaseQuestionProps & {
  question: NamesForIndividualConsumerQuestionProps;
};

const validateAnswers = (
  answers: string[],
  otherGCAnswers: string[],
  setError: (e: string) => void,
) => {
  // 1 - We must have at least one answer
  if (answers.length < 1 || (answers.length == 1 && !answers[0])) {
    setError("You must enter at least 1 consumer group name");
    return false;
  }

  // 2 - Every internal answer must be unique
  const uniq = Array.from(new Set(answers));
  if (uniq.length != answers.length) {
    setError("You cannot have two groups with the same name");
    return false;
  }

  // 3 - Every answer must not be in otherGCAnswers
  const duplicated = answers.find((a) => otherGCAnswers.includes(a));
  if (duplicated) {
    setError(`You have already used the name "${duplicated}". Please choose a unique name.`);
    return false;
  }

  setError("");
  return true;
};

// This question is tightly coupled to its use in the IntroSurvey
export const NamesForIndividualConsumerQuestion: React.FC<Props> = ({
  question,
  answer,
  onAnswer,
}) => {
  const [questionError, setQuestionError] = useState("");
  const [isEditing, setIsEditing] = useState(false);
  const [answers, setAnswers] = useState([""]);

  // keep `answers` updated based on what is set
  useEffect(() => {
    if (answer) {
      try {
        const _answers = JSON.parse(answer) as string[];
        if (_answers.length == 1 && !_answers[0]) {
          setIsEditing(true);
        }
        setAnswers(_answers);
      } catch (e) {
        setIsEditing(true);
        setAnswers([""]);
      }
    } else {
      setIsEditing(true);

      setAnswers((currentAnswers) => {
        // If the current answers are empty, reset to the default current answer state
        if (currentAnswers.length == 0 || (currentAnswers.length == 1 && !currentAnswers[0])) {
          return [""];
        }

        return currentAnswers;
      });
    }
  }, [answer]);

  const setQuestionAnswer = (a, i) => {
    const answerCopy = answers.slice();
    answerCopy[i] = a;
    setAnswers(answerCopy);
  };

  const addAnswer = () => {
    const answerCopy = answers.slice();
    answerCopy.push("");
    setAnswers(answerCopy);
  };

  const onRemove = (idx) => {
    // can't remove an out of bounds row
    if (idx < 0 || idx >= answers.length) {
      return;
    }

    // can't remove the last row
    if (idx == 0 && answers.length == 1) {
      return;
    }
    const answerCopy = answers.slice();
    answerCopy.splice(idx, 1);
    setAnswers(answerCopy);
  };

  const onDone = () => {
    // first validate!
    const updatedAnswers = answers
      .filter((a) => a)
      .map((a) => a.trim())
      .filter((a) => a);
    const isValid = validateAnswers(updatedAnswers, question.otherCGAnswers, setQuestionError);

    // then update
    if (isValid) {
      setIsEditing(false);
      onAnswer(JSON.stringify(updatedAnswers));
    }
  };

  const onEdit = () => {
    setIsEditing(true);
  };

  const canRemove = answers.length > 1;

  return (
    <FadeTransition
      key={`question-${question.slug}`}
      cls="survey-question-fade"
      in={question.visible}
      unmountOnExit
      appear
    >
      <div className="survey--question" ref={question.ref}>
        <QuestionContainer>
          <QuestionLabel label={question.question} helpText={question.helpText}></QuestionLabel>
          {questionError && <div className="radio-freetext--error">{questionError}</div>}
          {isEditing && (
            <>
              {" "}
              <div className="d-flex flex-column">
                {answers.map((a, idx) => {
                  return (
                    <InputRow
                      key={idx}
                      answer={a}
                      setAnswer={(v) => setQuestionAnswer(v, idx)}
                      canRemove={canRemove}
                      onRemove={() => onRemove(idx)}
                    />
                  );
                })}
              </div>
            </>
          )}
          {!isEditing && (
            <>
              <ul>
                {answers.map((a, idx) => {
                  return <li key={idx}>{a}</li>;
                })}
              </ul>
            </>
          )}
          <QuestionControls
            isEditing={isEditing}
            onAdd={addAnswer}
            onDone={onDone}
            onEdit={onEdit}
          />
        </QuestionContainer>
      </div>
    </FadeTransition>
  );
};

const QuestionControls = ({ isEditing, onAdd, onDone, onEdit }) => {
  if (isEditing) {
    return (
      <div className="d-flex mt-sm">
        <Button variant="contained" onClick={onAdd} startIcon={<Add />}>
          Add Another Group
        </Button>
        <div className="ml-sm">
          <Button variant="contained" color="primary" onClick={onDone}>
            Done Adding Groups
          </Button>
        </div>
      </div>
    );
  }

  return (
    <div className="d-flex mt-sm">
      <Button variant="contained" onClick={onEdit} size="small">
        Edit Groups
      </Button>
    </div>
  );
};

const InputRow = ({ answer, setAnswer, canRemove, onRemove }) => {
  return (
    <div className="d-flex align-items-center mb-sm">
      <TextField
        className="radio-freetext--textfield"
        variant="outlined"
        value={answer}
        onChange={({ target }) => setAnswer(target.value)}
        placeholder="for example, Customers or Users"
      />
      {canRemove && (
        <IconButton className="ml-sm" aria-label="delete" onClick={onRemove} size="large">
          <CloseIcon />
        </IconButton>
      )}
    </div>
  );
};
