import React, { useCallback, useEffect, useState } from "react";
import { BaseQuestion, BaseQuestionProps } from "../BaseQuestion";
import { WYSIWYGEditor } from "../../../../../common/components/input/WYSIWYGEditor";
import { usePrimaryOrganization } from "../../../../hooks";
import { DisableableButton } from "../../../Disableable";
import { ConfirmDialog } from "../../../dialog";
import { useUpdateOrganizationFinancialIncentives } from "../../../../hooks/useOrganization";
import _ from "lodash";

export type CustomFinancialIncentivesQuestionProps = BaseQuestion & {
  type: "custom-edit-financial-incentives";
};

type Props = BaseQuestionProps & {
  question: CustomFinancialIncentivesQuestionProps;
};

const DEFAULT_FINANCIAL_INCENTIVE_HTML =
  "<p>Consumers who sign up for our marketing emails/SMS texts receive a 10% discount on their first purchase. To opt in, a consumer must enter their email address/phone number into the form and consent to receive emails in exchange for a discount provided via coupon code. A consumer may unsubscribe from our marketing emails by using the unsubscribe link in the email footer/replying STOP via text at any time. We calculate the value of the offer and financial incentive by using the expense related to the offer.</p>";
export const CustomFinancialIncentivesQuestion: React.FC<Props> = ({
  question,
  answer,
  onAnswer,
}) => {
  const [org] = usePrimaryOrganization();
  const [confirmRestore, setConfirmRestore] = useState(false);
  const updateFinancialIncetives = useUpdateOrganizationFinancialIncentives(org.id);
  const [incentive, setIncentive] = useState(
    org.financialIncentiveDetails || DEFAULT_FINANCIAL_INCENTIVE_HTML,
  );

  const setDefault = () => setIncentive(DEFAULT_FINANCIAL_INCENTIVE_HTML);
  const isDefault = incentive == DEFAULT_FINANCIAL_INCENTIVE_HTML;
  const handleRestoreDefaultClick = () => {
    if (incentive) {
      setConfirmRestore(true);
    } else {
      setDefault();
    }
  };

  // debounced mutation
  const delayedMutation = useCallback(
    _.debounce((q) => updateFinancialIncetives.mutate(q), 1500),
    [updateFinancialIncetives],
  );

  useEffect(() => {
    // throttle
    if (incentive != org.financialIncentiveDetails && question.visible) {
      delayedMutation({ incentive });
    }
  }, [org.financialIncentiveDetails, incentive, question.visible]);

  useEffect(() => {
    if (incentive && question.visible && answer !== "true") {
      onAnswer("true");
    }
    if (!incentive && question.visible && answer !== null) {
      onAnswer(null);
    }
  }, [incentive, answer, question.visible]);

  if (!question.visible) {
    return null;
  }

  return (
    <>
      <p>
        The default text below applies to a program offering a discount for marketing email signups.
      </p>

      <p>
        If you have more than one incentive program (e.g.,a separate loyalty program) add an
        additional paragraph for each one.{" "}
        <a
          href="https://help.truevault.com/article/173-financial-incentive-notice-loyalty-programs"
          target="_blank"
          rel="noreferrer"
        >
          See an example for a loyalty program
        </a>
        .
      </p>

      <div className="my-lg">
        <WYSIWYGEditor
          height="150px"
          defaultValue={incentive}
          value={incentive}
          onChange={(content) => setIncentive(content)}
        />
      </div>

      <DisableableButton
        variant="outlined"
        onClick={handleRestoreDefaultClick}
        disabled={isDefault}
        disabledTooltip="The current financial incentive is the default text"
      >
        Restore Default Text
      </DisableableButton>

      <ConfirmDialog
        open={confirmRestore}
        confirmTitle="Are you sure you want to restore default text?"
        contentText="Restoring default text will remove the text you’ve added."
        onClose={() => setConfirmRestore(false)}
        onConfirm={() => {
          setDefault();
          setConfirmRestore(false);
        }}
      />
    </>
  );
};
