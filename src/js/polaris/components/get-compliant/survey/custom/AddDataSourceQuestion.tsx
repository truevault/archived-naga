import React, { useMemo } from "react";
import { QuestionContainer, QuestionLabel } from "../QuestionContainer";
import { BaseQuestion, BaseQuestionProps } from "../BaseQuestion";
import { FadeTransition } from "../../../../pages/get-compliant/FadeTransition";
import { VendorSearchBox } from "../../../organisms/vendor/VendorSearchBox";
import { VendorDto } from "../../../../../common/service/server/dto/VendorDto";
import pluralize from "pluralize";
import { Stack } from "@mui/material";
import { RowVendorTile } from "../../../../pages/stay-compliant/data-recipients/RowVendorTile";
import { Para } from "../../../typography/Para";
import { DisableableButton } from "../../../Disableable";
import {
  CollectionContext,
  OrganizationDataSourceDto,
} from "../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../../common/service/server/Types";
import { CustomVendorFormValues } from "../../../../pages/get-compliant/data-recipients/AddDataRecipients";

export type AddDataSourceQuestionProps = BaseQuestion & {
  type: "custom-add-data-source";

  allVendors: VendorDto[];
  sources: OrganizationDataSourceDto[];
  collectionContext?: CollectionContext;
  category?: string | string[];
  excludeCategories?: string[];
  excludeVendorKeys?: string[];

  handleAddVendor: (v: UUIDString) => void;
  handleRemoveVendor: (v: UUIDString) => void;
  handleAddCustomVendor: (values: CustomVendorFormValues) => void;
};

type Props = BaseQuestionProps & {
  question: AddDataSourceQuestionProps;
};

export const AddDataSourceQuestion: React.FC<Props> = ({ question, answer, onAnswer }) => {
  const customCategory: string | undefined = question.category
    ? [].concat(question.category)[0]
    : undefined;
  const categorySingular = customCategory ?? "Source";
  const categoryPlural = pluralize(categorySingular);

  const filteredVendors = useMemo(() => {
    let vendors = question.allVendors;
    if (question.category) {
      const categories: string[] = [].concat(question.category);
      vendors = vendors.filter((v) => categories.includes(v.category));
    }

    if (question.excludeCategories) {
      vendors = vendors.filter((v) => !question.excludeCategories.includes(v.category));
    }

    if (question.excludeVendorKeys) {
      vendors = vendors.filter((v) => !question.excludeVendorKeys.includes(v.vendorKey));
    }

    return vendors;
  }, [question.allVendors, question.category]);

  const filteredVendorIds = useMemo(
    () => new Set(filteredVendors.map((v) => v.id)),
    [filteredVendors],
  );

  const relevantSources = useMemo(
    () => question.sources.filter((s) => filteredVendorIds.has(s.vendorId)),
    [question.sources, filteredVendorIds],
  );
  const enabledSources = useMemo(
    () => new Set(relevantSources.map((s) => s.vendorId)),
    [relevantSources],
  );

  const collectionContext: CollectionContext = useMemo(
    () => question.collectionContext ?? "CONSUMER",
    [question.collectionContext],
  );

  return (
    <FadeTransition
      key={`question-${question.slug}`}
      cls="survey-question-fade"
      in={!!answer || question.visible}
      unmountOnExit
      appear
    >
      <div className="survey--question" ref={question.ref}>
        <QuestionContainer>
          <QuestionLabel {...question} label={question.question} />

          <div className="my-lg">
            <VendorSearchBox
              allVendors={filteredVendors}
              collectionContext={[collectionContext]}
              enabledVendors={enabledSources}
              handleAddVendor={question.handleAddVendor}
              handleAddCustomVendor={question.handleAddCustomVendor}
              handleRemoveVendor={question.handleRemoveVendor}
              showCustomButton={false}
              customButtonLabel={`Add Custom ${categorySingular}`}
              customCategory={customCategory}
              autoFocus={false}
              type="source"
            />
          </div>

          <Stack spacing={3} className="my-lg">
            {relevantSources.map((source) => {
              return (
                <RowVendorTile
                  key={source.vendorId}
                  name={source.name}
                  vendor={source}
                  category={source.category}
                  logoUrl={source.logoUrl}
                  added={true}
                  // pending={vendor.id == pendingVendor}
                  // disabled={vendorDisabled(vendor, true)}
                  selected={false}
                  handleRemoveVendor={() => question.handleRemoveVendor(source.vendorId)}
                />
              );
            })}

            {relevantSources.length === 0 && (
              <Para>
                <em>
                  You haven’t added any {categoryPlural} yet. Use the search above to add a{" "}
                  {categorySingular}.
                </em>
              </Para>
            )}
          </Stack>

          {!answer && (
            <DisableableButton
              variant="outlined"
              disabled={relevantSources.length === 0}
              disabledTooltip={`You haven't added any ${categoryPlural} yet.`}
              onClick={() => onAnswer("true")}
            >
              Done adding {categoryPlural}
            </DisableableButton>
          )}
        </QuestionContainer>
      </div>
    </FadeTransition>
  );
};
