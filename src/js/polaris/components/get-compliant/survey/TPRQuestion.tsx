import { Button, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { Api } from "../../../../common/service/Api";
import { Callout, CalloutVariant } from "../../Callout";
import { ConfirmDialog } from "../../dialog";
import { SurveyBooleanQuestion } from "./SurveyBooleanQuestion";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { SurveyResponses } from "../../../../common/hooks/useSurvey";
import { Add } from "@mui/icons-material";

type TPRQuestionProps = {
  orgId: string;
  visible: boolean;
  answers: SurveyResponses;
  setAnswer: (slug: string, answer: string) => void;
  slug: string;
  doneSlug?: string;
  questionLabel: string;
  followUpLabel: string;
  emptyLabel: string;
  helpLabel?: string;
  recipients: OrganizationDataRecipientDto[];
  refresh: () => void;

  onAdd: () => void;
  onEdit: (tpr: OrganizationDataRecipientDto) => void;
  onDelete: (tpr: OrganizationDataRecipientDto) => void;
};
export const TPRQuestion: React.FC<TPRQuestionProps> = ({
  orgId,
  visible,
  answers,
  setAnswer,
  slug,
  doneSlug,
  questionLabel,
  followUpLabel,
  emptyLabel,
  helpLabel,
  recipients,
  refresh,
  onAdd,
  onEdit,
  onDelete,
}) => {
  const [noConfirm, setNoConfirm] = useState(false);
  const [clickedDone, setClickedDone] = useState(false);

  const handleAnswer = async (answer: string) => {
    if (answer == "false" && recipients?.length > 0) {
      setNoConfirm(true);
    } else {
      await setAnswer(slug, answer);
    }
  };

  const handleFinishNo = async () => {
    const promises = recipients.map((r) => Api.organizationVendor.removeVendor(orgId, r.vendorId));
    await Promise.all(promises);
    await refresh();
    setAnswer(slug, "false");
  };

  useEffect(() => {
    if ((!answers[slug] || answers[slug] == "false") && recipients.length > 0) {
      setAnswer(slug, "true");
    }
  }, [recipients.length]);

  const doneAnswer = useMemo(() => Boolean(doneSlug) && answers[doneSlug], [answers, doneSlug]);
  const isDone = doneAnswer === "true" || clickedDone;

  const handleDone = () => {
    setClickedDone(true);
    setAnswer(doneSlug, "true");
  };

  if (!visible) {
    return null;
  }

  return (
    <>
      <SurveyBooleanQuestion
        answer={answers[slug]}
        onAnswer={handleAnswer}
        question={{
          type: "boolean",
          slug: slug,
          question: questionLabel,
          helpText: helpLabel,
          visible: true,
        }}
      />
      {answers[slug] == "true" && (
        <>
          <p className="text-component-question mt-xl">{followUpLabel}</p>
          <TPRDisplay
            recipients={recipients}
            emptyLabel={emptyLabel}
            onAdd={onAdd}
            onEdit={onEdit}
            onDelete={onDelete}
          />

          {!isDone && recipients.length > 0 && Boolean(doneSlug) && (
            <Button
              variant="contained"
              color="default"
              size="small"
              className="mt-md"
              onClick={handleDone}
            >
              Done
            </Button>
          )}
        </>
      )}
      <ConfirmDeleteAll
        open={noConfirm}
        onClose={() => setNoConfirm(false)}
        onRemove={handleFinishNo}
      />
    </>
  );
};
type ConfirmDeleteAllProps = {
  open: boolean;
  onClose: () => void;
  onRemove: () => void;
};

const ConfirmDeleteAll: React.FC<ConfirmDeleteAllProps> = ({ open, onClose, onRemove }) => {
  return (
    <ConfirmDialog
      open={open}
      onClose={onClose}
      onConfirm={() => {
        onRemove();
        onClose();
      }}
      confirmTitle="Are you sure?"
      contentText={`Changing your answer to “no” will remove any Third Party Recipients you added for this question.`}
    />
  );
};
type TPRDisplayProps = {
  recipients: OrganizationDataRecipientDto[];
  emptyLabel: string;
  onAdd: () => void;
  onEdit: (tpr: OrganizationDataRecipientDto) => void;
  onDelete: (tpr: OrganizationDataRecipientDto) => void;
};
const TPRDisplay: React.FC<TPRDisplayProps> = ({
  recipients,
  emptyLabel,
  onAdd,
  onEdit,
  onDelete,
}) => {
  if (recipients.length == 0) {
    return (
      <Callout variant={CalloutVariant.Clear}>
        <a
          href="#"
          onClick={(e) => {
            e.preventDefault();
            onAdd();
          }}
        >
          {emptyLabel}
        </a>
      </Callout>
    );
  }

  return <TPRTable recipients={recipients} onAdd={onAdd} onEdit={onEdit} onDelete={onDelete} />;
};
type TPRTableProps = {
  recipients: OrganizationDataRecipientDto[];
  onAdd: () => void;
  onEdit: (tpr: OrganizationDataRecipientDto) => void;
  onDelete: (tpr: OrganizationDataRecipientDto) => void;
};
const TPRTable: React.FC<TPRTableProps> = ({ recipients, onAdd, onEdit, onDelete }) => {
  return (
    <>
      <Table aria-label="simple table" className="mb-lg">
        <TableHead>
          <TableRow>
            <TableCell>Data Recipient Name</TableCell>
            <TableCell>Category</TableCell>
            <TableCell>URL</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {recipients.map((r) => {
            return (
              <TableRow key={r.id}>
                <TableCell>{r.name}</TableCell>
                <TableCell>{r.category}</TableCell>
                <TableCell>
                  {Boolean(r.url) && (
                    <a href={r.url} target="_blank" rel="noopener noreferrer">
                      Website
                    </a>
                  )}
                </TableCell>
                <TableCell align="right">
                  <a
                    href="#"
                    className="mr-lg"
                    onClick={(e) => {
                      e.preventDefault();
                      onEdit(r);
                    }}
                  >
                    Edit
                  </a>
                  <a
                    href="#"
                    onClick={(e) => {
                      e.preventDefault();
                      onDelete(r);
                    }}
                  >
                    Remove
                  </a>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>

      <div>
        <a
          href="#"
          onClick={(e) => {
            e.preventDefault();
            onAdd();
          }}
        >
          <Add /> Add
        </a>
      </div>
    </>
  );
};
