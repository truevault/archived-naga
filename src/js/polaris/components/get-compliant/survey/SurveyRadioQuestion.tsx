import React, { ReactNode } from "react";
import { FadeTransition } from "../../../pages/get-compliant/FadeTransition";
import { RadioSelectionQuestion } from "../../organisms/survey/RadioSelectionQuestion";
import { BaseQuestion, BaseQuestionProps } from "./BaseQuestion";

export type SurveyRadioOption = {
  value: string;
  label: string;
};

export type SurveyRadioQuestionProps = BaseQuestion & {
  type: "radio";
  options: SurveyRadioOption[];
  emptyText?: string | ReactNode;
};

type Props = BaseQuestionProps & {
  question: SurveyRadioQuestionProps;
};

export const SurveyRadioQuestion = ({ question, answer, onAnswer, ...rest }: Props) => {
  if (question.notPresent) {
    return null;
  }

  return (
    <FadeTransition
      key={`question-${question.slug}`}
      cls="survey-question-fade"
      in={!!answer || question.visible}
      unmountOnExit
      appear
    >
      <div className="survey--question" ref={question.ref}>
        <RadioSelectionQuestion
          question={question.question}
          helpText={question.helpText}
          options={question.options}
          answer={answer}
          setAnswer={onAnswer}
          disabled={rest.disabled}
          disabledTooltip={rest.disabledTooltip}
        />
      </div>
    </FadeTransition>
  );
};
