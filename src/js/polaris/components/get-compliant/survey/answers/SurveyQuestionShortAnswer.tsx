import React from "react";
import { BaseQuestionProps } from "../BaseQuestion";
import { SurveyQuestionProps } from "../SurveyQuestion";
import { SurveyBooleanQuestionShortAnswer } from "./SurveyBooleanQuestionShortAnswer";
import { SurveyCheckboxesQuestionShortAnswer } from "./SurveyCheckboxesQuestionShortAnswer";
import { SurveyFreetextQuestionShortAnswer } from "./SurveyFreetextQuestionShortAnswer copy";

type Props = BaseQuestionProps & {
  question: SurveyQuestionProps;
};

export const SurveyQuestionShortAnswer = ({ question: q, ...rest }: Props) => {
  switch (q.type) {
    case "boolean":
      return <SurveyBooleanQuestionShortAnswer question={q} {...rest} />;
    case "checkboxes":
      return <SurveyCheckboxesQuestionShortAnswer question={q} {...rest} />;
    case "freetext":
      return <SurveyFreetextQuestionShortAnswer question={q} {...rest} />;
    default:
      return null;

    // case "radio":
    //   return <SurveyRadioQuestion question={q} {...rest} />;
    // case "radio-freetext":
    //   return <SurveyRadioFreetextQuestion question={q} {...rest} />;
    // case "acknowledgement":
    //   return <SurveyAcknowledgementQuestion question={q} {...rest} />;
    // case "custom-names-for-individual-consumer":
    //   return <NamesForIndividualConsumerQuestion question={q} {...rest} />;
    // case "freetext":
    //   return <SurveyFreetextQuestion question={q} {...rest} />;
  }
};
