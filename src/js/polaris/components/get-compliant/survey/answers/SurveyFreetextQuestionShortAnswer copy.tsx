import React from "react";
import { BaseQuestionProps } from "../BaseQuestion";
import { SurveyFreetextQuestionProps } from "../SurveyFreetextQuestion";

type Props = BaseQuestionProps & {
  question: SurveyFreetextQuestionProps;
};

export const SurveyFreetextQuestionShortAnswer: React.FC<Props> = ({ answer }) => {
  if (!answer) {
    return null;
  }
  if (answer.length > 50) {
    return <p className="m-0 text-t1">{`${answer.slice(0, 50)}...`}</p>;
  }
  return <p className="m-0 text-t1">{answer}</p>;
};
