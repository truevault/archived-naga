import React from "react";
import { Chip } from "@mui/material";
import { BaseQuestionProps } from "../BaseQuestion";
import { SurveyBooleanQuestionProps } from "../SurveyBooleanQuestion";

type Props = BaseQuestionProps & {
  question: SurveyBooleanQuestionProps;
};

export const SurveyBooleanQuestionShortAnswer: React.FC<Props> = ({ question, answer }) => {
  if (question.notPresent) {
    return null;
  }

  if (answer === "true") {
    return <Chip label={question.yesLabel || "Yes"} color="success" />;
  }

  if (answer === "false") {
    return <Chip label={question.noLabel || "No"} color="error" />;
  }

  return null;
};
