import React, { useMemo } from "react";
import { BaseQuestionProps } from "../BaseQuestion";
import { SurveyCheckboxesQuestionProps } from "../SurveyCheckboxesQuestion";

type Props = BaseQuestionProps & {
  question: SurveyCheckboxesQuestionProps;
};

export const SurveyCheckboxesQuestionShortAnswer: React.FC<Props> = ({ question, answer }) => {
  const parsedAnswer: string[] = useMemo(() => JSON.parse(answer || "[]") || [], [answer]);

  if (question.notPresent) {
    return null;
  }

  // Merge any true option values into the preview
  const parsedAnswerSet = new Set(parsedAnswer);
  for (const option of question.options) {
    if (option.value) {
      parsedAnswerSet.add(option.key);
    }
  }

  const labels = Array.from(parsedAnswerSet)
    .map((value) => question.options.find((o) => o.key == value)?.label)
    .filter((x) => x)
    .sort();

  if (labels.length > 0) {
    return <>{labels.join(", ")}</>;
  }

  return null;
};
