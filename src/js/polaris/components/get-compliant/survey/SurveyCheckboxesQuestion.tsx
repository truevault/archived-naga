import { Checkbox, FormControlLabel, Tooltip } from "@mui/material";
import React, { ReactNode, useEffect, useMemo, useState } from "react";
import { FadeTransition } from "../../../pages/get-compliant/FadeTransition";
import { safeJsonParse, toggleSelection } from "../../../surveys/util";
import { BaseQuestion, BaseQuestionProps } from "./BaseQuestion";
import { QuestionContainer, QuestionLabel } from "./QuestionContainer";

export type SurveyCheckboxOption = {
  key: string;
  label: string;
  title?: string;
  helpText?: string | ReactNode;
  disabled?: true;
  value?: boolean;
};

export type SurveyCheckboxesQuestionProps = BaseQuestion & {
  type: "checkboxes";
  emptyText?: string | ReactNode;
  options: SurveyCheckboxOption[];
  noneOption?: boolean;
};

type Props = BaseQuestionProps & {
  question: SurveyCheckboxesQuestionProps;
  noneOption?: boolean;
};

const NONE = "none";

export const SurveyCheckboxesQuestion: React.FC<Props> = ({ question, answer, onAnswer }) => {
  const { helpText, options } = question;

  const [answering, setAnswering] = useState(false);
  const [localAnswer, setLocalAnswer] = useState(answer);
  const answers: string[] = useMemo(() => safeJsonParse(localAnswer) ?? [], [localAnswer]);
  const hasAnswer = answers.length > 0;

  const disableByNoneOptionSelected = useMemo(
    () => question.noneOption && hasAnswer && answers.some((a) => a === NONE),
    [question.noneOption, answers, hasAnswer],
  );

  const setAnswer = async (key: string, checked: boolean) => {
    setAnswering(true);
    try {
      const newAnswers = key === NONE && checked ? [NONE] : toggleSelection(answers, key, checked);

      await onAnswer(JSON.stringify(newAnswers));
      setLocalAnswer(JSON.stringify(newAnswers));
    } finally {
      setAnswering(false);
    }
  };

  // Get disabled answers with a value that are
  // not already part of the selected answer
  const answersToAutoSet = useMemo(
    () =>
      question.options
        .filter((o) => o.disabled && o.value && !answers.includes(o.key))
        .map((o) => o.key),

    // question.options is an unstable object so we stringify
    // it when setting it as a dependency
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [answers, JSON.stringify(question.options)],
  );

  // TODO: BURN THIS WITH FIRE
  useEffect(() => {
    const autoAnswer = async () => {
      // If we render a question that has a disabled answer
      // then we want to ensure it gets set as an answer;
      // this hook calls out to the server with the current
      // answers _plus_ any disabled answers that aren't
      // already in the answers key
      const toAutoset = answersToAutoSet.filter((x) => !answers || !answers.includes(x));

      if (question.visible && toAutoset.length > 0) {
        setAnswering(true);

        try {
          onAnswer(JSON.stringify(answers.concat(...toAutoset)));
        } finally {
          setAnswering(false);
        }
      }
    };

    autoAnswer();
    // on answer is unstable and including it causes recursive
    // network requests that _usually_ settle --- but don't always
    //
    // We have to stringify the options because we care when they change
    // but we can't interrogate the array
    //
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [answers, question.visible, answersToAutoSet]);

  return (
    <FadeTransition
      key={`question-${question.slug}`}
      cls="survey-question-fade"
      in={hasAnswer || question.visible}
      unmountOnExit
      appear
    >
      <div className="survey--question" ref={question.ref}>
        <QuestionContainer>
          <QuestionLabel label={question.question} helpText={helpText} />
          <div className="survey-question--checkboxes">
            {options.map((opt) => {
              return (
                <div key={opt.key}>
                  <Tooltip title={opt.title || ""}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          color="primary"
                          checked={opt.value ?? answers.includes(opt.key)}
                          onChange={(e, checked) => {
                            setAnswer(opt.key, checked);
                          }}
                          disabled={
                            answering ||
                            opt.disabled ||
                            (disableByNoneOptionSelected && opt.key != NONE)
                          }
                        />
                      }
                      label={opt.label}
                      className="base-size"
                    />
                  </Tooltip>

                  {opt.helpText && <div className="survey-question--help-text">{opt.helpText}</div>}
                </div>
              );
            })}
            {options.length == 0 && question.emptyText && (
              <div className="survey-question--empty-text">{question.emptyText}</div>
            )}
          </div>
        </QuestionContainer>
      </div>
    </FadeTransition>
  );
};
