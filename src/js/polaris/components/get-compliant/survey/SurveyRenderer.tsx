import React, { useMemo } from "react";
import { SurveyAnswerFn, SurveyAnswers } from "../../../../common/hooks/useSurvey";
import { prepareSurvey, Survey } from "../../../surveys/survey";
import { SurveyQuestionContainer } from "./SurveyBooleanQuestion";
import { SurveyQuestion } from "./SurveyQuestion";

type SurveyRendererProps = {
  survey: Survey;
  answers: SurveyAnswers;
  setAnswer: SurveyAnswerFn;
  offsetFirst?: boolean;
};

export const SurveyRenderer: React.FC<SurveyRendererProps> = ({
  survey,
  answers,
  setAnswer,
  ...rest
}) => {
  const questions = useMemo(() => prepareSurvey(survey, answers), [survey, answers]);

  return (
    <SurveyQuestionContainer {...rest}>
      {questions.map((q) => {
        let questionText: string | undefined = q.plaintextQuestion;
        if (!questionText && typeof q.question == "string") {
          questionText = q.question;
        }

        return (
          <SurveyQuestion
            key={q.slug}
            question={q}
            answer={answers[q.slug]}
            onAnswer={(ans) => setAnswer(q.slug, ans, questionText)}
          />
        );
      })}
    </SurveyQuestionContainer>
  );
};
