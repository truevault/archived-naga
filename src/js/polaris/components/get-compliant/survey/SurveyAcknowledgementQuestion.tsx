import React, { ReactNode } from "react";
import { FadeTransition } from "../../../pages/get-compliant/FadeTransition";
import { AcknowledgementQuestion } from "../../organisms/survey/AcknowledgementQuestion";
import { BaseQuestion, BaseQuestionProps } from "./BaseQuestion";

export type SurveyAcknowledgementQuestionProps = BaseQuestion & {
  type: "acknowledgement";
  text: ReactNode;
  buttonLabel?: ReactNode;
};

type Props = BaseQuestionProps & {
  question: SurveyAcknowledgementQuestionProps;
};

export const SurveyAcknowledgementQuestion = ({ question, answer, onAnswer, disabled }: Props) => {
  if (question.notPresent) return null;

  return (
    <FadeTransition
      key={`question-${question.slug}`}
      cls="survey-question-fade"
      in={!!answer || question.visible}
      unmountOnExit
      appear
    >
      <div className="survey--question" ref={question.ref}>
        <AcknowledgementQuestion
          question={question.question}
          text={question.text}
          disabled={disabled}
          answer={answer}
          setAnswer={onAnswer}
          buttonLabel={question.buttonLabel}
        />
      </div>
    </FadeTransition>
  );
};
