import React, { ReactNode } from "react";
import { Callout, CalloutVariant } from "../../Callout";

export const QuestionContainer: React.FC = ({ children }) => {
  return <div className="survey-question--container">{children}</div>;
};

export const QuestionLabel: React.FC<{
  label: ReactNode;
  helpText?: ReactNode;
  header?: ReactNode;
  description?: ReactNode;
  recommendation?: ReactNode;
}> = ({ label, helpText, header, description, recommendation }) => {
  return (
    <>
      {header && <div className="survey-question--header">{header}</div>}
      <div className="text-component-question">{label}</div>
      {description && <div>{description}</div>}
      {helpText && <div className="text-component-help">{helpText}</div>}
      {recommendation && (
        <Callout className="my-lg" variant={CalloutVariant.Yellow} icon="Info">
          {recommendation}
        </Callout>
      )}
    </>
  );
};
