import React from "react";
import { FadeTransition } from "../../../pages/get-compliant/FadeTransition";
import clsx from "clsx";
import { BaseQuestion, BaseQuestionProps } from "./BaseQuestion";
import { BooleanQuestion } from "../../organisms/survey/BooleanQuestion";

export type SurveyBooleanQuestionProps = BaseQuestion & {
  type: "boolean";

  yesLabel?: string;
  noLabel?: string;

  yesDisabled?: boolean;
  yesDisabledTooltip?: string;
  yesConfirmation?: React.ReactNode;
  noDisabled?: boolean;
  noDisabledTooltip?: string;
  noConfirmation?: React.ReactNode;

  noFirst?: boolean;
};

type Props = BaseQuestionProps & {
  question: SurveyBooleanQuestionProps;
};

export const SurveyBooleanQuestion: React.FC<Props> = ({ question, answer, onAnswer }) => {
  if (question.notPresent) {
    return null;
  }

  return (
    <FadeTransition
      key={`question-${question.slug}`}
      cls="survey-question-fade"
      in={!!answer || question.visible}
      unmountOnExit
      appear
    >
      <div className="survey--question" ref={question.ref}>
        <BooleanQuestion
          question={question.question}
          header={question.header}
          description={question.description}
          recommendation={question.recommendation}
          helpText={question.helpText}
          answer={answer}
          disabled={question.isDisabled}
          disabledTooltip={question.disabledReason}
          setAnswer={onAnswer}
          yesLabel={question.yesLabel}
          yesDisabled={question.yesDisabled}
          yesDisabledTooltip={question.yesDisabledTooltip}
          yesConfirmation={question.yesConfirmation}
          noLabel={question.noLabel}
          noDisabled={question.noDisabled}
          noDisabledTooltip={question.noDisabledTooltip}
          noConfirmation={question.noConfirmation}
          noFirst={question.noFirst}
        />
      </div>
    </FadeTransition>
  );
};

export const SurveyQuestionContainer = ({ children, offsetFirst = false, ...props }) => {
  const className = clsx("intro-survey--questions-container", {
    "intro-survey--questions-container-offset-first": offsetFirst,
  });
  return (
    <div className={className} {...props}>
      {children}
    </div>
  );
};
