import React, { ReactNode } from "react";

export type BaseQuestion = {
  slug: string;
  helpText?: string | ReactNode;
  description?: string | ReactNode;
  header?: string;
  visible?: boolean;
  notPresent?: boolean;
  question: string | ReactNode;
  plaintextQuestion?: string;
  isDisabled?: boolean;
  disabledReason?: string;
  recommendation?: string | ReactNode;
  ref?: React.MutableRefObject<any>;
};

export type BaseQuestionProps = {
  answer: string | undefined;
  onAnswer: (answer: string) => Promise<void>;
  disabled?: boolean;
  disabledTooltip?: string;
};
