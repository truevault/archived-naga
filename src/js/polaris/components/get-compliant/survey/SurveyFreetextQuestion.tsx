import React, { ReactNode, useCallback, useState } from "react";
import { FadeTransition } from "../../../pages/get-compliant/FadeTransition";
import { BaseQuestion, BaseQuestionProps } from "./BaseQuestion";
import { TextField } from "@mui/material";
import _ from "lodash";
import { QuestionLabel } from "./QuestionContainer";
import { WYSIWYGEditor } from "../../../../common/components/input/WYSIWYGEditor";

export type SurveyFreetextQuestionProps = BaseQuestion & {
  type: "freetext";
  wysiwyg?: boolean;
  validateFreetext?: (answer) => string | null;
  emptyText?: string | ReactNode;
  rows?: number;
};

type Props = BaseQuestionProps & {
  question: SurveyFreetextQuestionProps;
};

export const SurveyFreetextQuestion: React.FC<Props> = ({ question, answer, onAnswer }) => {
  const [localValue, setLocalValue] = useState(answer);
  const debouncedAnswer = useCallback(_.debounce(onAnswer, 400), [onAnswer]);

  const handleChange = ({ target }) => {
    setLocalValue(target.value);
    debouncedAnswer(target.value);
  };

  const handleWsyiwygChange = (contents: string) => {
    setLocalValue(contents);
    debouncedAnswer(contents);
  };

  if (question.notPresent) {
    return null;
  }

  return (
    <FadeTransition
      key={`question-${question.slug}`}
      cls="survey-question-fade"
      in={!!answer || question.visible}
      unmountOnExit
      appear
    >
      <div className="survey--question" ref={question.ref}>
        <QuestionLabel
          label={question.question}
          helpText={question.helpText}
          header={question.header}
          description={question.description}
        />

        {question.wysiwyg && (
          <WYSIWYGEditor
            height="250px"
            defaultValue={localValue ?? ""}
            value={localValue ?? ""}
            onChange={handleWsyiwygChange}
          />
        )}

        {!question.wysiwyg && (
          <TextField
            className="freetext--textfield"
            variant="outlined"
            value={localValue ?? ""}
            fullWidth={true}
            onChange={handleChange}
            multiline={true}
            rows={question.rows ?? 3}
          />
        )}
      </div>
    </FadeTransition>
  );
};
