import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  FormControlLabel,
  Radio,
  TextField,
} from "@mui/material";
import _ from "lodash";
import React, { useCallback } from "react";
import { Field, Form, FormSpy } from "react-final-form";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { CloseableDialogTitle } from "../dialog/CloseableDialogTitle";
import { AddEmailInput } from "./AddEmailInput";
import { SelectEmailInput } from "./SelectEmailInput";

type Props = {
  vendor: OrganizationDataRecipientDto;
  instruction: RequestHandlingInstructionDto;
  setInstructions: React.Dispatch<React.SetStateAction<RequestHandlingInstructionDto[]>>;
  email: string | null;
  title: React.ReactNode;
  open: boolean;
  closeModal: (refresh?: true) => void;
};

export const VendorEmailFormModal: React.FC<Props> = ({
  vendor,
  instruction,
  setInstructions,
  email,
  title,
  open,
  closeModal,
}) => {
  const organizationId = usePrimaryOrganizationId();

  const { fetch: updateOrganizationVendor } = useActionRequest({
    api: async ({ email, deleteDoNotContact, doNotContactReason, doNotContactExplanation }) => {
      // @ts-ignore
      await Api.organizationVendor.addOrUpdateVendor(organizationId, vendor.vendorId, {
        email,
        classificationId: vendor.classification.id,
      });
      const updatedInstruction: RequestHandlingInstructionDto = {
        ...instruction,
        deleteDoNotContact,
        doNotContactReason:
          (isServiceProvider || isContractor) && deleteDoNotContact
            ? "SELF_SERVICE_DELETION"
            : doNotContactReason,
        doNotContactExplanation,
      };
      await Api.requestHandlingInstructions.putInstruction(organizationId, updatedInstruction);
      setInstructions((oldInstructions) => {
        const newInstructions = _.cloneDeep(oldInstructions);
        const idx = newInstructions.findIndex((i) => i.id == instruction.id);
        if (idx >= 0) {
          newInstructions[idx] = updatedInstruction;
        }
        return newInstructions;
      });
    },
  });

  const onSubmit = useCallback(
    async (formVal: any) => {
      await updateOrganizationVendor(formVal);

      closeModal(true);
    },
    [closeModal, updateOrganizationVendor],
  );

  const isServiceProvider = vendor.classification.slug == "service-provider";
  const isContractor = vendor.classification.slug == "contractor";

  const onClose = () => closeModal();

  return (
    <Dialog fullWidth maxWidth="md" open={open} onClose={onClose}>
      <CloseableDialogTitle onClose={onClose} id={`vendor-add-edit-email-dialog`}>
        {title}
      </CloseableDialogTitle>
      <Form
        onSubmit={onSubmit}
        initialValues={{
          email,
          deleteDoNotContact: instruction?.deleteDoNotContact,
          doNotContactReason: instruction?.doNotContactReason,
          doNotContactExplanation: instruction?.doNotContactExplanation,
        }}
      >
        {({ handleSubmit, values }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent>
              <EmailField vendor={vendor} />
              <Field
                name="deleteDoNotContact"
                type="checkbox"
                render={({ input }) => {
                  return (
                    <FormControlLabel
                      className="mt-md"
                      control={<Checkbox {...input} onChange={input.onChange} />}
                      label={
                        isServiceProvider || isContractor ? (
                          "Do not contact: Self-Service Deletion is sufficient to delete all data"
                        ) : (
                          <strong>Do Not Contact {vendor.name} for Deletion Requests</strong>
                        )
                      }
                    />
                  );
                }}
              />
              {!isServiceProvider && !isContractor && (
                <>
                  <DoNotContactFields vendor={vendor} />
                  <DoNotContactExplanationField />
                </>
              )}
            </DialogContent>
            <DialogActions>
              <Button color="default" variant="contained" className="mr-sm" onClick={onClose}>
                Cancel
              </Button>
              <Button
                type="submit"
                color="primary"
                variant="contained"
                disabled={
                  values.doNotContactReason == "IMPOSSIBLE_OR_DIFFICULT" &&
                  !values.doNotContactExplanation
                }
              >
                Save Changes
              </Button>
            </DialogActions>
          </form>
        )}
      </Form>
    </Dialog>
  );
};

const EmailField = ({ vendor }) => {
  return (
    <div>
      <FormSpy subscription={{ values: true }}>
        {({ values }) => {
          return (
            <Field name="email" type="email" disabled={values["deleteDoNotContact"]}>
              {(props) =>
                vendor.defaultEmail ? (
                  <SelectEmailInput {...props} disabled={props.disabled} vendor={vendor} />
                ) : (
                  <AddEmailInput {...props} disabled={props.disabled} />
                )
              }
            </Field>
          );
        }}
      </FormSpy>
    </div>
  );
};

const DoNotContactFields = ({ vendor }) => {
  return (
    <div className="ml-mdlg">
      <FormSpy subscription={{ values: true }}>
        {({ values }) => {
          if (!values.deleteDoNotContact) {
            return null;
          }
          return (
            <>
              <Field
                name="doNotContactReason"
                type="radio"
                value="CONTACT_VIA_OTHER_MEANS"
                render={({ input }) => {
                  return (
                    <FormControlLabel
                      control={<Radio color="primary" {...input} />}
                      label={`We will notify ${vendor.name} of the consumer's deletion request through other means (e.g., webform, custom API). Email contact is not necessary.`}
                    />
                  );
                }}
              />
              <Field
                name="doNotContactReason"
                type="radio"
                value="IMPOSSIBLE_OR_DIFFICULT"
                render={({ input }) => {
                  return (
                    <FormControlLabel
                      control={<Radio color="primary" {...input} />}
                      label="It is impossible or extremely difficult to contact this Data Recipient."
                    />
                  );
                }}
              />
            </>
          );
        }}
      </FormSpy>
    </div>
  );
};

const DoNotContactExplanationField = () => {
  return (
    <div className="ml-xl max-width-512">
      <FormSpy subscription={{ values: true }}>
        {({ values }) => {
          if (
            !values.deleteDoNotContact ||
            values.doNotContactReason != "IMPOSSIBLE_OR_DIFFICULT"
          ) {
            return null;
          }
          return (
            <>
              <p className="text-t1">
                <strong>
                  Describe why you cannot contact this Recipient. Your explanation will be included
                  with your response to consumer deletion requests.
                </strong>
              </p>

              <Field
                name="doNotContactExplanation"
                type="text"
                render={({ input }) => {
                  return (
                    <TextField
                      {...input}
                      multiline
                      fullWidth
                      minRows={4}
                      placeholder="E.g., They are no longer in business."
                    />
                  );
                }}
              />
            </>
          );
        }}
      </FormSpy>
    </div>
  );
};
