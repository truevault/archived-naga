import { KeyboardArrowLeft as KeyboardArrowLeftIcon } from "@mui/icons-material";
import React from "react";
import { useHistory } from "react-router-dom";
import { NetworkRequestState } from "../../../common/models/NetworkRequest";
import {
  ProgressFooter,
  ProgressFooterActions,
  ProgressFooterSaveNotification,
} from "../../pages/get-compliant/ProgressFooter";
import { Routes } from "../../root/routes";

type Props = {
  nextContent: string;
  nextDisabled: boolean;
  nextOnClick: () => void;
  currentRequest: NetworkRequestState;
};

export const RequestInstructionsProgressFooter: React.FC<Props> = ({
  nextContent,
  nextDisabled,
  nextOnClick,
  currentRequest,
}) => {
  const history = useHistory();

  return (
    <ProgressFooter
      actions={
        <ProgressFooterActions
          nextContent={nextContent}
          nextOnClick={nextOnClick}
          nextDisabled={nextDisabled}
          nextTooltip={
            nextDisabled ? "Please make a selection for each data recipient above" : null
          }
          prevContent={
            <span className="flex-center-vertical">
              <KeyboardArrowLeftIcon />
              Back to Request Instructions
            </span>
          }
          prevOnClick={() => history.push(Routes.organization.requestHandling.base)}
        />
      }
      content={<ProgressFooterSaveNotification request={currentRequest} />}
    />
  );
};
