import { Button } from "@mui/material";
import React, { useState, useCallback } from "react";
import { VendorEmailFormModal } from "./VendorEmailFormModal";
import { CategoryChip } from "./VendorInstructionCard";

export const EmailModal = ({
  vendor,
  instruction,
  setInstructions,
  refreshVendors,
  refreshInstructions,
}) => {
  const [open, setOpen] = useState(false);
  const toggleModal = useCallback(
    (newVal?: boolean) => {
      setOpen((x) => {
        if (newVal != null) return newVal;
        return !x;
      });
    },
    [setOpen],
  );
  const openModal = useCallback(() => toggleModal(true), [toggleModal]);

  const closeModal = useCallback(
    (refresh = false) => {
      if (refresh) {
        refreshVendors();
        refreshInstructions();
      }
      toggleModal(false);
    },
    [toggleModal, refreshVendors, refreshInstructions],
  );

  const email = vendor.email;

  return (
    <>
      <Button className="ml-mdlg" size="small" variant="text" color="secondary" onClick={openModal}>
        Edit
      </Button>

      <VendorEmailFormModal
        vendor={vendor}
        instruction={instruction}
        setInstructions={setInstructions}
        email={email}
        title={
          <div className="d-flex flex-row align-items-center">
            <img
              src="/assets/images/deletion/contact-deletion-icon.svg"
              className="mr-sm"
              width={32}
              height={32}
            />
            Edit Contact for {vendor.name}
            <CategoryChip recipient={vendor} className="ml-md" />
          </div>
        }
        open={open}
        closeModal={closeModal}
      />
    </>
  );
};
