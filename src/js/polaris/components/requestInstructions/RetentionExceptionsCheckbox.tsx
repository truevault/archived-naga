import { Checkbox, FormControlLabel } from "@mui/material";
import React from "react";
import { HelpPopover } from "../HelpPopover";

export const RetentionExceptionsCheckbox: React.FC<{
  onChange: (checked: boolean) => void;
  hasRetentionExceptions: boolean;
}> = ({ onChange, hasRetentionExceptions }) => {
  return (
    <>
      <FormControlLabel
        control={
          <Checkbox
            onChange={(_, checked: boolean) => onChange(checked)}
            checked={hasRetentionExceptions}
          />
        }
        label="Some data will be deleted in this system."
      />
      <HelpPopover label="Any data that cannot be retained under a deletion exception should be deleted." />
    </>
  );
};
