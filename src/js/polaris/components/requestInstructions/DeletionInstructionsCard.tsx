import { ExpandLess, ExpandMore, Lock } from "@mui/icons-material";
import { IconButton, Stack, TextField } from "@mui/material";
import clsx from "clsx";
import debounce from "lodash.debounce";
import React, { useEffect, useMemo, useState } from "react";
import { SurveyAnswers } from "../../../common/hooks/useSurvey";
import { DataMapDto } from "../../../common/service/server/dto/DataMapDto";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import { RetentionReasonDto } from "../../../common/service/server/dto/RetentionReasonDto";
import { Callout, CalloutVariant } from "../../components/Callout";
import { StandardLabel } from "../../components/input/Labels";
import { usePrimaryOrganizationId } from "../../hooks";
import { usePlatformApps } from "../../hooks/useOrganizationVendors";
import { useVendorInstruction } from "../../hooks/useVendorInstruction";
import { installedAppsRespectingDeletionForVendor } from "../../util/resources/vendorUtils";
import { isShopify } from "../../util/vendors";
import { EmailInput } from "./EmailInput";
import { VendorInstructionCard } from "./VendorInstructionCard";

export type VendorCardProps = {
  orgId: string;
  instructions: RequestHandlingInstructionDto[];
  dataMap: DataMapDto;
  setInstructions: React.Dispatch<React.SetStateAction<RequestHandlingInstructionDto[]>>;
  reasons: RetentionReasonDto[];
  vendor: OrganizationDataRecipientDto;
  fetchVendors: () => void;
  fetchInstructions: () => void;
  onLoadingComplete?: (vendor: OrganizationDataRecipientDto) => void;
  sharingAnswers: SurveyAnswers;

  children?: React.FC<VendorCardProps>;
  index: number; // Gross. Factor away from tailwind-style utility classes.
};

export const DeletionInstructionsCard: React.FC<VendorCardProps> = ({
  orgId,
  vendor,
  dataMap,
  fetchVendors,
  instructions,
  setInstructions,
  fetchInstructions,
  index,
}) => {
  const { instruction, updateInstruction } = useVendorInstruction(
    orgId,
    vendor,
    instructions,
    setInstructions,
  );

  const [instructionText, setInstructionText] = useState<null | string>(null);

  const debouncedUpdate = useMemo(
    () =>
      debounce((instruction) => {
        updateInstruction(instruction);
      }, 500),
    [updateInstruction],
  );

  const handleTextChange = ({ target }) => {
    setInstructionText(target.value);
    debouncedUpdate({ ...instruction, processingInstructions: target.value });
  };

  const [expandSelfService, setExpandSelfService] = useState(false);

  const deletionInstructionsVisible = vendor?.deletionInstructions != null;
  const instructionsFieldVisible = instruction?.processingMethod != "INACCESSIBLE_OR_NOT_STORED";

  useEffect(() => {
    if (instruction?.processingInstructions && instructionText === null) {
      setInstructionText(instruction?.processingInstructions ?? "");
    }
  }, [instruction?.processingInstructions, instructionText]);

  const contactDisabled = instruction?.deleteDoNotContact;

  const doNotContactIncomplete =
    contactDisabled &&
    (!instruction.doNotContactReason ||
      (instruction.doNotContactReason == "IMPOSSIBLE_OR_DIFFICULT" &&
        !instruction.doNotContactExplanation));
  const emailIncomplete = !contactDisabled && !(vendor.email || vendor.defaultEmail);

  const contactWarning = doNotContactIncomplete || emailIncomplete;
  const dataInaccessible = instruction?.processingMethod == "INACCESSIBLE_OR_NOT_STORED";
  const hasExceptions =
    instruction?.hasRetentionExceptions && instruction.processingMethod == "RETAIN";

  return (
    <VendorInstructionCard index={index} vendor={vendor}>
      <div className="ml-mdlg">
        <Stack spacing={2}>
          {hasExceptions && (
            <>
              <p className="mb-xs">
                <Lock fontSize="inherit" /> Some data will be retained under exceptions to deletion.
              </p>
              {instruction.deletedPIC && instruction.deletedPIC.length > 0 && (
                <p className="mt-xs ml-mdlg">
                  <strong>The following data can be deleted:</strong>
                  <br />
                  <span className="text-t1 text-muted">
                    {instruction.deletedPIC
                      .map((pic) => dataMap.categories.find((c) => c.id == pic)?.name)
                      .filter((x) => x)
                      .join(", ")}
                  </span>
                </p>
              )}
            </>
          )}

          <DeleteInstructionCard
            icon="self-service"
            title={contactDisabled ? "Vendor not contacted:" : "Deletion Requests sent to:"}
            headerContent={
              <EmailInput
                vendor={vendor}
                instruction={instruction}
                setInstructions={setInstructions}
                refreshVendors={fetchVendors}
                refreshInstructions={fetchInstructions}
              />
            }
            disabled={contactDisabled}
            warning={contactWarning}
          />
          <DeleteInstructionCard
            icon="contact"
            title={
              dataInaccessible
                ? "Self Service Deletion Unavailable: Data inaccessible or not stored"
                : "Self Service Deletion Instructions"
            }
            headerContent={
              dataInaccessible ? undefined : (
                <IconButton onClick={() => setExpandSelfService(!expandSelfService)}>
                  {expandSelfService ? <ExpandLess /> : <ExpandMore />}
                </IconButton>
              )
            }
            disabled={dataInaccessible}
            warning={!dataInaccessible && !deletionInstructionsVisible && !instructionText}
          >
            {expandSelfService && (
              <div className="p-sm">
                {isShopify(vendor) && <ShopifyCallout vendor={vendor} />}

                {deletionInstructionsVisible && (
                  <Callout variant={CalloutVariant.Purple} className={clsx("mb-mdlg")}>
                    <div className="text-weight-bold">Vendor Deletion Instructions</div>
                    <div dangerouslySetInnerHTML={{ __html: vendor.deletionInstructions }}></div>
                  </Callout>
                )}

                {instructionsFieldVisible && (
                  <div className="mb-mdlg">
                    <StandardLabel
                      label={
                        deletionInstructionsVisible
                          ? "Add Your Instructions (optional)"
                          : "Add Your Instructions"
                      }
                    />

                    <TextField
                      className="instructions--text-field"
                      variant="outlined"
                      value={instructionText || ""}
                      onChange={handleTextChange}
                      fullWidth={false}
                      multiline={true}
                      rows={8}
                      style={{ backgroundColor: "white" }}
                    />
                  </div>
                )}
              </div>
            )}
          </DeleteInstructionCard>
        </Stack>
      </div>
    </VendorInstructionCard>
  );
};

type DeleteInstructionIcon = "unused" | "self-service" | "contact";

type DeleteInstructionCardProps = {
  icon: DeleteInstructionIcon;
  title: string;
  disabled?: boolean;
  warning?: boolean;
  headerContent?: React.ReactNode;
  children?: React.ReactNode;
};

const DeleteInstructionCard: React.FC<DeleteInstructionCardProps> = ({
  icon,
  title,
  disabled,
  headerContent,
  warning,
  children,
}) => {
  return (
    <div
      className={clsx("box-rounded px-sm py-xs", {
        "background-color-neutral-100": !warning,
        "background-color-yellow-100": warning,
      })}
    >
      <div
        className={clsx("d-flex flex-row align-items-center", { "neutral-500": disabled })}
        style={{ minHeight: 50 }}
      >
        <img src={getIconSrc(disabled ? "unused" : icon)} />
        <span>{disabled ? title : <strong>{title}</strong>}</span>
        {Boolean(headerContent) && <div className="ml-sm">{headerContent}</div>}
      </div>
      {children}
    </div>
  );
};

const getIconSrc = (icon: DeleteInstructionIcon): string => {
  switch (icon) {
    case "self-service":
      return "/assets/images/deletion/self-service-deletion-icon.svg";
    case "contact":
      return "/assets/images/deletion/contact-deletion-icon.svg";
    case "unused":
    default:
      return "/assets/images/deletion/unused-icon.svg";
  }
};

type ShopifyCalloutProps = {
  vendor: OrganizationDataRecipientDto;
};
const ShopifyCallout: React.FC<ShopifyCalloutProps> = ({ vendor }) => {
  const orgId = usePrimaryOrganizationId();
  const [platformApps, request] = usePlatformApps(orgId, vendor.vendorId);

  if (!request.success) {
    return null;
  }

  const installedApps = installedAppsRespectingDeletionForVendor(vendor, platformApps);

  if (installedApps.length == 0) {
    return null;
  }

  return (
    <Callout variant={CalloutVariant.Yellow} className={clsx("mb-mdlg")}>
      <p>
        Shopify will automatically delete data from your installed apps:{" "}
        {installedApps.map((a) => a.name).join(", ")}
      </p>
    </Callout>
  );
};
