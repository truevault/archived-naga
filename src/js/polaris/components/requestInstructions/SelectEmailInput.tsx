import { FormControlLabel, Radio, RadioGroup, TextField } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";

type Props = {
  input: any;
  vendor: OrganizationDataRecipientDto;
  disabled: boolean;
};

export const SelectEmailInput: React.FC<Props> = ({ input, vendor, disabled }) => {
  const [which, setWhich] = useState(vendor.email ? "custom" : "default");
  const [email, setEmail] = useState(vendor.email ?? "");

  const handleEmailChange = useCallback(
    (evt) => {
      setEmail(evt.target.value);
      if (evt.target.value.trim()) {
        setWhich("custom");
      } else {
        setWhich("default");
      }
    },
    [setEmail],
  );
  const handleWhichChange = useCallback(
    (evt) => {
      setWhich(evt.target.value);
    },
    [setWhich],
  );
  const handleEmailFocus = useCallback(() => {
    setWhich("custom");
  }, [setWhich]);

  useEffect(() => {
    if (which === "default") {
      input.onChange(vendor.defaultEmail);
    } else {
      input.onChange(email);
    }
  }, [which, email, input, vendor.defaultEmail]);

  const customEmailInput = (
    <FormControlLabel
      label={`Use alternate email`}
      labelPlacement="start"
      disabled={disabled}
      control={
        <TextField
          type="email"
          value={email}
          onChange={handleEmailChange}
          onClick={handleEmailFocus}
          variant="outlined"
          size="small"
          className="ml-md"
        />
      }
    />
  );

  return (
    <>
      <RadioGroup value={which} onChange={handleWhichChange} name="which">
        <FormControlLabel
          value="default"
          control={<Radio name="whichEmail" className="pl-0" />}
          disabled={disabled}
          label={vendor.defaultEmail}
        />

        <FormControlLabel
          value="custom"
          control={<Radio name="whichEmail" className="pl-0" />}
          disabled={disabled}
          label={customEmailInput}
        />
      </RadioGroup>
    </>
  );
};
