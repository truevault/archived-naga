import { Checkbox, FormControlLabel } from "@mui/material";
import React from "react";
import { HelpPopover } from "../../components/HelpPopover";

export const DoNotContactCheckbox = ({ checked, onChange }) => {
  return (
    <>
      <FormControlLabel
        control={<Checkbox checked={checked} onChange={(_, checked) => onChange(checked)} />}
        label="Do not contact about requests to delete"
      />

      <HelpPopover label="Select only if the data recipient will not act on deletion requests or if it is impossible or extremely difficult to contact them." />
    </>
  );
};
