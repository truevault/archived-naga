import React from "react";
import { StandardLabel } from "../../components/input/Labels";
import clsx from "clsx";

type Props = {
  label: string;
  spacingBottom?: boolean;
};

export const VendorCardRow: React.FC<Props> = ({ label, spacingBottom = false, children }) => (
  <div className="flex-container">
    <div className="w-256">
      <StandardLabel label={label} />
    </div>
    <div
      className={clsx("spacer flex-grow", {
        "pb-mdlg mb-mdlg": spacingBottom,
        "border-bottom border-bottom--grey": spacingBottom,
      })}
    >
      {children}
    </div>
  </div>
);
