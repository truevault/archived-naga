import { WarningAmberOutlined } from "@mui/icons-material";
import { Chip } from "@mui/material";
import clsx from "clsx";
import React from "react";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import { EmailModal } from "./EmailModal";

type Props = {
  vendor: OrganizationDataRecipientDto;
  instruction: RequestHandlingInstructionDto;
  setInstructions: React.Dispatch<React.SetStateAction<RequestHandlingInstructionDto[]>>;
  refreshVendors: () => void;
  refreshInstructions: () => void;
};
export const EmailInput: React.FC<Props> = ({
  vendor,
  instruction,
  setInstructions,
  refreshVendors,
  refreshInstructions,
}) => {
  const hasEmail = vendor.email || vendor.defaultEmail;
  return (
    <div className="d-flex flex-row align-items-center">
      <div className={clsx("email", { vendor: hasEmail })}>{label(vendor, instruction)}</div>
      <EmailModal
        vendor={vendor}
        instruction={instruction}
        setInstructions={setInstructions}
        refreshVendors={refreshVendors}
        refreshInstructions={refreshInstructions}
      />
    </div>
  );
};

const label = (
  vendor: OrganizationDataRecipientDto,
  instruction: RequestHandlingInstructionDto,
) => {
  if (instruction?.deleteDoNotContact) {
    if (
      !instruction.doNotContactReason ||
      (instruction.doNotContactReason == "IMPOSSIBLE_OR_DIFFICULT" &&
        !instruction.doNotContactExplanation)
    ) {
      return (
        <Chip icon={<WarningAmberOutlined fontSize="small" />} color="warning" label="Incomplete" />
      );
    }

    if (instruction.doNotContactReason == "CONTACT_VIA_OTHER_MEANS") {
      return "Notification sent through other means";
    }

    if (instruction.doNotContactReason == "IMPOSSIBLE_OR_DIFFICULT") {
      return (
        <div className="d-flex flex-col">
          <Chip color="default" label="Extremely Difficult or Impossible to Contact" />
          {instruction.doNotContactExplanation}
        </div>
      );
    }

    if (instruction.doNotContactReason == "SELF_SERVICE_DELETION") {
      return "Self Service Deletion is sufficient";
    }
  }

  const email = vendor.email || vendor.defaultEmail;

  if (!email) {
    return (
      <Chip
        icon={<WarningAmberOutlined fontSize="small" />}
        color="warning"
        label="No Contact Added"
      />
    );
  }

  return email;
};
