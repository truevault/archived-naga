import React from "react";

export const RetentionReasonList = ({ reasons, instruction }) => {
  const reasonSlug = instruction?.retentionReasons || [];

  return (
    <ul className="retention-reason-list">
      {reasons.map((reason) => {
        if (!reasonSlug.includes(reason.slug)) {
          return null;
        }
        return <li key={reason.slug}>{reason.reason}</li>;
      })}
    </ul>
  );
};
