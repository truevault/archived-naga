import { FormControlLabel, TextField } from "@mui/material";
import React from "react";

type Props = {
  input: any;
  disabled: boolean;
};
export const AddEmailInput: React.FC<Props> = ({ input, disabled }) => {
  return (
    <FormControlLabel
      label="Contact email:"
      labelPlacement="start"
      name={input.name}
      value={input.value}
      onChange={input.onChange}
      control={
        <TextField
          autoFocus
          type="email"
          variant="outlined"
          size="small"
          className="ml-md"
          disabled={disabled}
        />
      }
      disabled={disabled}
      className="mb-lg"
    />
  );
};
