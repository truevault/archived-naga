import React from "react";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import clsx from "clsx";
import { DataRecipientLogo } from "../DataRecipientLogo";
import { Chip } from "@mui/material";
import { RouteHelpers } from "../../root/routes";

type Props = {
  vendor: OrganizationDataRecipientDto;
  index: number;
};

export const VendorInstructionCard: React.FC<Props> = ({ children, vendor, index }) => (
  <div
    key={vendor.vendorId}
    className={clsx("paper-divider-bottom", { "py-xl": !!index, "pb-xl": !index })}
  >
    <div className="mb-mdlg d-flex flex-row align-items-center flex--between">
      <div className="d-flex flex-row align-items-center">
        <DataRecipientLogo dataRecipient={vendor} size={32} />
        <h5 className="my-0 ml-sm">{vendor.name}</h5>
        <CategoryChip recipient={vendor} className="ml-md" />
      </div>

      <a href={RouteHelpers.dataMap.dataRecipient.detail(vendor.vendorId)}>Update in Data Map</a>
    </div>

    {children}
  </div>
);

type CategoryChipProps = {
  recipient: OrganizationDataRecipientDto;
  className?: string;
};
export const CategoryChip: React.FC<CategoryChipProps> = ({ recipient, className }) => {
  const deletionType = getDeletionType(recipient);

  return (
    <Chip
      label={getChipLabel(deletionType)}
      variant="muted"
      // @ts-ignore
      color={getChipColor(deletionType)}
      className={className}
    />
  );
};

type DeletionType =
  | "service-provider"
  | "third-party"
  | "storage-location"
  | "contractor"
  | "unknown";
const getDeletionType = (recipient: OrganizationDataRecipientDto): DeletionType => {
  if (recipient.classification.slug == "service-provider") {
    return "service-provider";
  }
  if (
    recipient.classification.slug == "third-party" ||
    recipient.dataRecipientType == "third_party_recipient"
  ) {
    return "third-party";
  }

  if (recipient.dataRecipientType == "storage_location") {
    return "storage-location";
  }

  if (
    recipient.classification.slug == "contractor" ||
    recipient.dataRecipientType == "contractor"
  ) {
    return "contractor";
  }

  return "unknown";
};

const getChipLabel = (deletionType: DeletionType): string => {
  switch (deletionType) {
    case "service-provider":
      return "Service Provider";
    case "third-party":
      return "Third Party";
    case "storage-location":
      return "Storage Location";
    case "contractor":
      return "Contractor";
    case "unknown":
    default:
      return "Unknown";
  }
};

const getChipColor = (deletionType: DeletionType): string => {
  switch (deletionType) {
    case "service-provider":
      return "primary";
    case "third-party":
      return "secondary";
    case "storage-location":
      return "orange";
    case "contractor":
      return "success";
    case "unknown":
    default:
      return "default";
  }
};
