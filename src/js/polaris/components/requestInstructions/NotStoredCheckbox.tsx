import { Checkbox, FormControlLabel } from "@mui/material";
import React from "react";
import { ProcessingMethod } from "../../../common/service/server/Types";
import { HelpPopover } from "../HelpPopover";

export const NotStoredCheckbox: React.FC<{
  onChange: (checked: boolean) => void;
  method: ProcessingMethod | null | undefined;
  disabled: boolean;
}> = ({ onChange, method, disabled }) => {
  return (
    <>
      <FormControlLabel
        control={
          <Checkbox
            onChange={(_, checked: boolean) => onChange(checked)}
            checked={method === "INACCESSIBLE_OR_NOT_STORED"}
            value="INACCESSIBLE_OR_NOT_STORED"
            disabled={disabled}
          />
        }
        label="Data is inaccessible or not stored "
      />
      <HelpPopover label="Select if the system does not store data, or the data is inaccessible (e.g., routinely deleted within 30 days.)" />
    </>
  );
};
