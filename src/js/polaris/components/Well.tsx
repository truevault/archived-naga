import React from "react";
import clsx from "clsx";

type WellProps = {
  variant?: "light" | "white";
  children: React.ReactNode;
};
export const Well: React.FC<WellProps> = ({ variant = "white", children }) => {
  return (
    <div
      className={clsx("well", {
        "well--light": variant == "light",
        "well--white": variant == "white",
      })}
    >
      {children}
    </div>
  );
};
