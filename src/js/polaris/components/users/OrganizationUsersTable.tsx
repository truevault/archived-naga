import { Button, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import React from "react";
import { InlineLoading } from "../../../common/components/Loading";
import { useActionRequest } from "../../../common/hooks/api";
import { OrganizationUser } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { Error as RequestsError } from "../../copy/requests";
import { Fmt } from "../../formatters";
import { useSelf } from "../../hooks";

export const OrganizationUsersTable = ({ users, onNeedsRefresh }) => {
  return (
    <>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell style={{ width: "40%" }}>User</TableCell>
            <TableCell style={{ width: "30%" }} align="right">
              Last Login
            </TableCell>
            <TableCell style={{ width: "30%" }} align="right">
              Actions
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map((user) => (
            <TableRow key={user.email}>
              <TableCell component="th" scope="row">
                <div className={"user-name-col"}>{`${user.firstName} ${user.lastName}`}</div>
                <div className={"user-email-col"}>{user.email}</div>
              </TableCell>
              <TableCell align="right">
                {user.status != "INVITED" ? Fmt.Date.fmtDate(user.lastLogin) : <i>Pending</i>}{" "}
              </TableCell>
              <TableCell align="right">
                <UserActions user={user} onNeedsRefresh={onNeedsRefresh} />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </>
  );
};

const UserActions = ({
  user,
  onNeedsRefresh,
}: {
  user: OrganizationUser;
  onNeedsRefresh: () => void;
}) => {
  const [self] = useSelf();
  const { fetch: deleteUser, request: deleteRequest } = useActionRequest({
    api: () => Api.organizationUser.deleteUser(user.organization, user.email),
    messages: { defaultError: RequestsError.DeleteUser },
    onSuccess: onNeedsRefresh,
  });

  if (deleteRequest.running) {
    return <InlineLoading />;
  }

  if (self.email !== user.email) {
    const label = user.status === "INVITED" ? "Cancel Invite" : "Remove user";
    return (
      <Button color="error" variant="text" onClick={deleteUser}>
        {label}
      </Button>
    );
  }

  return null;
};
