import React from "react";
import { Paper } from "@mui/material";

type EmptyStateProps = {
  image: string;
  message: string;
};

export const EmptyState = ({ image, message }: EmptyStateProps) => {
  return (
    <div className="empty-state">
      <Paper>
        <img className="empty-state-image" src={image} />
        <div className="empty-state-message text-body-1-bold">{message}</div>
      </Paper>
    </div>
  );
};
