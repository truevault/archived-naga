import React, { ReactNode } from "react";
import { useHistory } from "react-router";
import clsx from "clsx";
import { HelpPopover } from "./HelpPopover";
import { UnderlinedHeading } from "./typography/Headings";

type ActionRowsHeaderProps = {
  label: string;
  labelHelpText?: React.ReactNode;
  statusLabel?: string;
  wideStatus?: boolean;
  className?: string;
  small?: boolean;
};

export const ActionRowsHeader: React.FC<ActionRowsHeaderProps> = ({
  label,
  labelHelpText,
  statusLabel,
  wideStatus,
  className,
  small = false,
}) => {
  const headingClass = clsx(`flex-grow mb-0 ${className}`, {
    "text-t1 text-transform-none pb-sm": small,
  });
  const statusClass = clsx("action-row--status", {
    "action-row--status__wide": wideStatus,
  });

  const rowClass = clsx("action-row-header");

  return (
    <div className={rowClass}>
      <UnderlinedHeading className={headingClass}>
        {label}

        {labelHelpText && (
          <span className="ml-xs">
            <HelpPopover label={labelHelpText} color="inherit" fontSize="inherit" />
          </span>
        )}
      </UnderlinedHeading>

      <div className={statusClass}>
        <UnderlinedHeading className="mb-0 w-100">{statusLabel}</UnderlinedHeading>
      </div>
    </div>
  );
};

type ActionRowProps = {
  iconUrl?: string;
  label: string;
  status?: ReactNode;
  wideStatus?: boolean;
  topBorder?: boolean;
  bottomBorder?: boolean;
  rowHref?: string;
  onRowClick?: () => void;
};

export const ActionRow: React.FC<ActionRowProps> = ({
  iconUrl,
  label,
  status,
  wideStatus,
  topBorder,
  bottomBorder = true,
  rowHref,
  onRowClick,
}) => {
  const history = useHistory();
  if (rowHref && !onRowClick) {
    onRowClick = () => history.push(rowHref);
  }

  const rowClass = clsx("action-row", {
    "action-row__clickable": Boolean(onRowClick),
    "action-row__top-border": topBorder,
    "action-row__bottom-border": bottomBorder,
  });

  const statusClass = clsx("action-row--status", {
    "action-row--status__wide": wideStatus,
  });
  return (
    <div className={rowClass} onClick={onRowClick}>
      {iconUrl && (
        <div className="action-row--icon">
          <img src={iconUrl} />
        </div>
      )}
      <div className="action-row--label">
        <span className="text-medium">{label}</span>
      </div>
      {status && <div className={statusClass}>{status}</div>}
    </div>
  );
};

type EmptyActionRowProps = {
  label: string;
};

export const EmptyActionRow: React.FC<EmptyActionRowProps> = ({ label }) => {
  return (
    <div className="action-row">
      <span className="text-style-italic">{label}</span>
    </div>
  );
};
