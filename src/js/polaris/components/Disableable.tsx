import React from "react";
import { Tooltip, Button, ButtonProps } from "@mui/material";

export type DisableableButtonProps = ButtonProps & { disabledTooltip: string };
export const DisableableButton = ({ disabledTooltip, ...buttonProps }: DisableableButtonProps) => {
  return (
    <Disableable disabledTooltip={disabledTooltip} disabled={buttonProps.disabled ?? false}>
      <Button {...buttonProps} />
    </Disableable>
  );
};

interface DisableableProps {
  disabledTooltip: React.ReactNode;
  disabled: boolean;
  children: React.ReactNode;
  isBlock?: boolean;
}

export const Disableable = ({
  disabledTooltip,
  disabled,
  children,
  isBlock = false,
}: DisableableProps) => {
  if (disabled && disabledTooltip) {
    return (
      <Tooltip title={disabledTooltip}>
        {isBlock ? <span>{children}</span> : <div>{children}</div>}
      </Tooltip>
    );
  } else {
    return <>{children}</>;
  }
};
