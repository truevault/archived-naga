import React from "react";
import { IconButton } from "@mui/material";
import { Close as CloseIcon } from "@mui/icons-material";

type Props = {
  onClose?: () => void;
  className?: string;
};
export const CloseButton: React.FC<Props> = ({ onClose, className }) => {
  return (
    <IconButton className={className} onClick={onClose} size="large">
      <CloseIcon className={className} />
    </IconButton>
  );
};
