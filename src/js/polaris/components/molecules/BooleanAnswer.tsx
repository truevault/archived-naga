import { Check, Close } from "@mui/icons-material";
import React from "react";

interface BooleanAnswerProps {
  answer: boolean;
}

export const BooleanAnswer: React.FC<BooleanAnswerProps> = ({ answer }) => {
  const Icon = answer ? Check : Close;
  const color = answer ? "color-green" : "color-red";
  return (
    <div className="text-t3">
      <span className={`${color} mr-xs align-middle`} style={{ paddingTop: 2 }}>
        <Icon fontSize="inherit" />
      </span>
      {answer ? "Yes" : "No"}
    </div>
  );
};
