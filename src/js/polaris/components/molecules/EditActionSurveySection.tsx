import React from "react";
import { HelpPopover } from "../HelpPopover";
import { BooleanAnswer } from "./BooleanAnswer";
import { SurveyAnswer } from "./SurveyView";

export interface EditActionSurveyItemProps {
  id: string;
  label: React.ReactNode;
  helpText?: React.ReactNode;
  response?: boolean;
  isNested?: boolean;
  hideBorder?: boolean;
}

interface EditActionSurveySectionProps {
  className?: string;
  dense?: true;
  items: EditActionSurveyItemProps[];
  noItemsView?: React.ReactNode | null;
}

export const EditActionSurveySection: React.FC<EditActionSurveySectionProps> = ({
  className,
  items,
  dense,
  noItemsView = null,
}) => {
  return (
    <div className={className}>
      {items.length === 0
        ? noItemsView
        : items.map((item) => (
            <div key={item.id}>
              <SurveyAnswer
                id={item.id}
                label={
                  <span>
                    {item.label}
                    {item.helpText && <HelpPopover className="ml-sm" label={item.helpText} />}
                  </span>
                }
                response={<BooleanAnswer answer={item.response} />}
                isNested={item.isNested}
                hideResponse={item.response == null}
                hideBorder={item.hideBorder}
                dense={dense}
              />
            </div>
          ))}
    </div>
  );
};
