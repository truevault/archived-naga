import clsx from "clsx";
import React from "react";
import { DataRecipientLogo } from "../../DataRecipientLogo";

type VendorListItemProps = {
  name: string;
  category?: string;
  logoUrl: string;

  onClick?: () => void;
  onRemove?: () => void;
};
export const VendorListItem: React.FC<VendorListItemProps> = ({
  name,
  category,
  logoUrl,
  onClick,
  onRemove,
}) => {
  return (
    <div
      className={clsx("flex flex-row align-items-center px-md py-sm", {
        "vendor-list-item--clickable": Boolean(onClick),
      })}
      onClick={onClick}
    >
      <DataRecipientLogo logoUrl={logoUrl} size={32} />

      <div className="flex-grow">
        <p className="m-0">{name}</p>
        {Boolean(category) && <p className="m-0 text-component-help">{category}</p>}
      </div>
      {Boolean(onRemove) && (
        <div>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              onRemove();
            }}
          >
            Remove
          </a>
        </div>
      )}
    </div>
  );
};
