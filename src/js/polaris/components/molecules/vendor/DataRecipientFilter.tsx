import React, { useState } from "react";

import {
  Checkbox,
  Chip,
  FormControl,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { unreachable } from "../../../util";
import _ from "lodash";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";

const FILTER_OPTIONS = [
  "INCOMPLETE",
  "STORAGE_LOCATIONS",
  "THIRD_PARTIES",
  "SERVICE_PROVIDERS",
  "CONTRACTORS",
] as const;
type AllFilterOptions = typeof FILTER_OPTIONS;
export type DataRecipientFilterOptions = AllFilterOptions[number];

type UseDataRecipientFilter = {
  props: DataRecipientFilterProps;
};
export const useDataRecipientFilter = (): UseDataRecipientFilter => {
  const [filterOptions, setFilterOptions] = useState<DataRecipientFilterOptions[] | undefined>(
    undefined,
  );

  const removeFilter = (option: DataRecipientFilterOptions) => {
    setFilterOptions((options) => {
      const newOptions = _.cloneDeep(options) ?? [];
      const optionSet = new Set(newOptions);
      optionSet.delete(option);

      return optionSet.size == 0 ? undefined : [...optionSet];
    });
  };

  return {
    props: {
      filters: filterOptions,
      removeFilter,
      setFilterOptions,
    },
  };
};

type DataRecipientFilterProps = {
  filters: DataRecipientFilterOptions[] | undefined;
  removeFilter: (option: DataRecipientFilterOptions) => void;
  setFilterOptions: React.Dispatch<React.SetStateAction<DataRecipientFilterOptions[]>>;
};

export const DataRecipientFilter: React.FC<DataRecipientFilterProps> = ({
  filters,
  removeFilter,
  setFilterOptions,
}) => {
  const handleChange = (event: SelectChangeEvent<DataRecipientFilterOptions>) => {
    const {
      target: { value },
    } = event;
    // On autofill we get a stringified value.
    const filters: DataRecipientFilterOptions[] =
      typeof value === "string" ? (value.split(",") as DataRecipientFilterOptions[]) : value;
    setFilterOptions(filters.length == 0 ? undefined : filters);
  };

  return (
    <div className="d-flex my-xl flex-row align-items-center">
      <FormControl className="mr-mdlg min-width-300 max-width-512">
        <InputLabel id="data-recipient-filter-select">Filter By</InputLabel>
        <Select
          id="data-recipient-filter-select"
          multiple
          // @ts-ignore
          value={filters ?? []}
          onChange={handleChange}
          input={<OutlinedInput label="Filter by" />}
          renderValue={(selected) =>
            // @ts-ignore
            selected?.map((filter) => {
              return (
                <Chip
                  key={filter}
                  className="mr-md"
                  label={filterLabel(filter)}
                  onDelete={() => removeFilter(filter)}
                />
              );
            })
          }
        >
          {FILTER_OPTIONS.map((option) => (
            <MenuItem key={option} value={option}>
              <Checkbox checked={filters !== undefined && filters?.indexOf(option) > -1} />
              <ListItemText primary={filterLabel(option)} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export const applyDataRecipientFilters = (
  dataRecipients: OrganizationDataRecipientDto[],
  filters: DataRecipientFilterOptions[] | undefined,
): OrganizationDataRecipientDto[] => {
  if (!filters) {
    return dataRecipients;
  }

  let resultSet = new Set<OrganizationDataRecipientDto>();
  filters.forEach((f) => {
    const filteredArr = applyFilter(dataRecipients, f);
    resultSet = new Set([...resultSet, ...filteredArr]);
  });

  return Array.from(resultSet);
};

const applyFilter = (
  r: OrganizationDataRecipientDto[],
  filter: DataRecipientFilterOptions,
): OrganizationDataRecipientDto[] => {
  switch (filter) {
    case "CONTRACTORS":
      return r.filter(
        (d) => d.classification.slug == "contractor" || d.dataRecipientType == "contractor",
      );
    case "INCOMPLETE":
      return r.filter((d) => !d.completed);
    case "SERVICE_PROVIDERS":
      return r.filter((d) => d.classification.slug == "service-provider");
    case "STORAGE_LOCATIONS":
      return r.filter((d) => d.dataRecipientType == "storage_location");
    case "THIRD_PARTIES":
      return r.filter(
        (d) =>
          d.classification.slug == "third-party" || d.dataRecipientType == "third_party_recipient",
      );
    default:
      return unreachable(filter);
  }
};

const filterLabel = (filter: DataRecipientFilterOptions): string => {
  switch (filter) {
    case "CONTRACTORS":
      return "Contractors";
    case "INCOMPLETE":
      return "Incomplete";
    case "SERVICE_PROVIDERS":
      return "Service Providers";
    case "STORAGE_LOCATIONS":
      return "Storage Locations";
    case "THIRD_PARTIES":
      return "Third Parties";
    default:
      return unreachable(filter);
  }
};
