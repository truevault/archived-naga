import React, { ReactNode } from "react";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { VendorListItem } from "./VendorListItem";

type VendorListProps = {
  vendors: VendorDto[];
  empty?: ReactNode;
  onClick?: (vendorId: string) => void;
  onRemove?: (vendorId: string) => void;
};
export const VendorList: React.FC<VendorListProps> = ({ vendors, empty, onClick, onRemove }) => {
  if (vendors.length == 0) {
    return empty ? <>{empty}</> : null;
  }

  return (
    <>
      {vendors.map((v: VendorDto) => {
        return (
          <div key={v.id}>
            <VendorListItem
              name={v.name}
              category={v.category}
              logoUrl={v.logoUrl}
              onClick={onClick ? () => onClick(v.id) : undefined}
              onRemove={onRemove ? () => onRemove(v.id) : undefined}
            />
          </div>
        );
      })}
    </>
  );
};
