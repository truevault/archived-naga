import { Typography } from "@mui/material";
import clsx from "clsx";
import React from "react";

export const SectionHeader: React.FC<{ className?: string; helpText?: React.ReactNode }> = ({
  children,
  className,
  helpText,
}) => (
  <header className={clsx(className, "mb-lg")}>
    <Typography variant="h6" className="text-component-question">
      {children}
    </Typography>

    {helpText && <Typography className="text-component-help">{helpText}</Typography>}
  </header>
);
