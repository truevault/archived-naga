import React from "react";
import clsx from "clsx";

interface SurveyViewProps {
  title: string;
  survey: SurveyAnswerProps[];
}

export const SurveyView: React.FC<SurveyViewProps> = ({ title, survey }) => (
  <div className="survey">
    <h3 className="text-t3 text-weight-medium mb-md mt-xxl">{title}</h3>
    {survey.map((answer) => (
      <SurveyAnswer key={answer.id} {...answer} />
    ))}
  </div>
);

interface SurveyAnswerProps {
  id: string;
  label: React.ReactNode;
  response?: React.ReactNode;
  isNested?: boolean;
  hideResponse?: boolean;
  hideBorder?: boolean;
  dense?: true;
}

// TODO: Verify that consistent padding is ok. This is 16px vertical padding, not 18px;
export const SurveyAnswer: React.FC<SurveyAnswerProps> = ({
  label,
  response,
  isNested = false,
  hideResponse = false,
  hideBorder = false,
  dense,
}) => {
  const verticalSpace = dense ? "sm" : "md";
  return (
    <div
      className={clsx(`flex-row flex--between px-xs pb-${verticalSpace}`, {
        [`pt-${verticalSpace}`]: !isNested,
        "border-color-neutral-400 border-top": !isNested && !hideBorder,
      })}
    >
      <span className={`${isNested ? "pl-lg" : ""} flex--expand-self`}>{label}</span>
      {!hideResponse && <span className="w-128">{response ?? "No Response"}</span>}
    </div>
  );
};
