import { Person as PersonIcon } from "@mui/icons-material";
import DomainIcon from "@mui/icons-material/Domain";
import {
  Avatar as MUIAvatar,
  Divider,
  Fade,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  ListSubheader,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";
import { useQueryClient } from "@tanstack/react-query";
import React, { useCallback, useContext, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router";
import { InlineLoading } from "../../../../common/components/Loading";
import { useActionRequest } from "../../../../common/hooks/api";
import { OmniBarContext } from "../../../../common/hooks/useOmniBar";
import { useSelf } from "../../../../common/hooks/useSelf";
import { selfActions } from "../../../../common/reducers/selfSlice";
import { Api } from "../../../../common/service/Api";
import { UserDetailsDto } from "../../../../common/service/server/dto/UserDto";
import { OrganizationPublicId } from "../../../../common/service/server/Types";
import { Error as RequestsError, Success as RequestsSuccess } from "../../../copy/requests";
import { useOrganization, usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { Routes } from "../../../root/routes";
import { ellipsis } from "../../../util/ellipsis";

const useMenu = (id) => {
  const [el, setEl] = React.useState(null);
  const isOpen = Boolean(el);
  const menuId = isOpen ? id : undefined;

  const click = (event) => {
    event.preventDefault();
    setEl(event.currentTarget);
  };

  const close = () => setEl(null);

  return {
    el,
    isOpen,
    id: menuId,
    click,
    close,
  };
};

const rejectKeyboardShortcut = (event: KeyboardEvent) =>
  event.target instanceof Node && isFormField(event.target);

const isFormField = (element: Node): boolean => {
  if (!(element instanceof HTMLElement)) {
    return false;
  }

  const name = element.nodeName.toLowerCase();
  const type = (element.getAttribute("type") || "").toLowerCase();
  return (
    name === "select" ||
    name === "textarea" ||
    (name === "input" &&
      type !== "submit" &&
      type !== "reset" &&
      type !== "checkbox" &&
      type !== "radio") ||
    element.isContentEditable
  );
};

const NavMenu = ({ menu, children, additionalClassNames = "", position = "right" }) => {
  return (
    <Menu
      className={`nav-popover-menu ${additionalClassNames}`}
      keepMounted
      TransitionComponent={Fade}
      id={menu.id}
      open={menu.isOpen}
      anchorEl={menu.el}
      onClose={menu.close}
      anchorOrigin={
        position === "bottom"
          ? {
              vertical: "bottom",
              horizontal: "left",
            }
          : {
              vertical: "center",
              horizontal: "right",
            }
      }
      transformOrigin={
        position === "bottom"
          ? {
              vertical: "top",
              horizontal: "left",
            }
          : {
              vertical: "center",
              horizontal: "left",
            }
      }
    >
      {children}
    </Menu>
  );
};

export const UserNavItem: React.FC<{ horizontal?: boolean }> = ({ horizontal = true }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const queryClient = useQueryClient();
  const menu = useMenu("nav-profile-popover");
  const [self] = useSelf();
  const primaryOrgId = usePrimaryOrganizationId();
  const params = useParams() as any;
  const { openOmniBar } = useContext(OmniBarContext);

  const organizationId = params.organizationId || primaryOrgId;
  const [org] = useOrganization(organizationId);

  const { fetch: updateActiveOrganizationSelf, request: switchOrgRequest } = useActionRequest({
    api: (organization: OrganizationPublicId) =>
      Api.user.updateSelfActiveOrganization(organization),
    messages: {
      defaultError: RequestsError.SetActiveOrganization,
      success: RequestsSuccess.SetActiveOrganization,
    },
    notifyError: true,
    notifySuccess: false,
    onSuccess: async (user) => {
      dispatch(selfActions.set(user as UserDetailsDto));
      await queryClient.invalidateQueries({ refetchType: "none" }, { cancelRefetch: true });
      history.push(Routes.root);
    },
  });

  const switchOrg = useCallback(
    async (e: any) => {
      e.preventDefault();
      openOmniBar({
        onClose: (data) => {
          if (data?.organization) {
            updateActiveOrganizationSelf(data.organization);
          }
          menu.close();
        },
      });
    },
    [menu, openOmniBar, updateActiveOrganizationSelf],
  );

  const organizationSwitch = (
    <ListItemSecondaryAction className="user-menu__organization_switch">
      Switch
    </ListItemSecondaryAction>
  );

  useEffect(() => {
    const listener = (e) => {
      if (rejectKeyboardShortcut(e)) {
        return;
      }

      // '/' key with no modifiers
      if (e.keyCode == 191 && !e.metaKey && !e.shiftKey && !e.ctrlKey && !e.altKey) {
        switchOrg(e);
      }
    };

    document.addEventListener("keyup", listener);

    return () => document.removeEventListener("keyup", listener);
  }, [switchOrg]);

  const UserDisplayText = () => {
    // NOTE: display the organization name in non-production environments
    //       to provide useful context in videos attached to defects
    return org && window.location.hostname !== "polaris.truevault.com" ? (
      <div style={{ flexDirection: "row" }}>
        <div>
          {self.firstName} {self.lastName}
        </div>
        <div style={{ fontSize: ".7em", marginTop: "-5px", opacity: "0.5" }}>{org.name}</div>
      </div>
    ) : (
      <>
        {self.firstName} {self.lastName}
      </>
    );
  };

  return (
    <>
      <a className="primary-nav__bottom-link text-decoration-none" href="#" onClick={menu.click}>
        <PersonIcon className={`primary-nav__bottom-icon`} />
        <UserDisplayText />
      </a>

      <NavMenu
        menu={menu}
        additionalClassNames="user-menu"
        position={horizontal ? "right" : "bottom"}
      >
        <ListSubheader className="user-menu__name" disableGutters disableSticky>
          <Typography variant="inherit" noWrap>
            {ellipsis(`${self?.firstName} ${self?.lastName}`, 32)}
          </Typography>
        </ListSubheader>

        {org ? (
          <MenuItem onClick={switchOrg} disabled={switchOrgRequest.running}>
            <ListItemAvatar>
              <MUIAvatar
                variant="square"
                alt="organization icon"
                src={org?.faviconUrl?.toString()}
                style={{ width: "16px", height: "16px" }}
              >
                <DomainIcon style={{ width: "100%", height: "100%" }} />
              </MUIAvatar>
            </ListItemAvatar>
            <ListItemText disableTypography secondary={organizationSwitch}>
              <Typography variant="inherit" noWrap className="user-menu__organization_name">
                {org?.name}
              </Typography>
            </ListItemText>
          </MenuItem>
        ) : (
          <div className="text-center">
            <InlineLoading />
          </div>
        )}

        {self.adminSite && <Divider />}
        {self.adminSite && (
          <MenuItem component={"a"} href={self.adminSite}>
            Admin
          </MenuItem>
        )}
      </NavMenu>
    </>
  );
};
