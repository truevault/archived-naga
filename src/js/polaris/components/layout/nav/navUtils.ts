import { GetCompliantStep } from "../../../../common/service/server/Types";
import { getStepLogic } from "../../../surveys/steps/GetCompliantStepLogic";
import { STEPS } from "../../../surveys/steps/GetCompliantSteps";

export const getCurrentProgressContextFromLocation = (
  locationPathname: string,
): GetCompliantStep => {
  const stepIdx = STEPS.findIndex((s) => getStepLogic(s).matchesRoute(locationPathname));

  if (stepIdx >= 0) {
    return STEPS[stepIdx];
  }
};
