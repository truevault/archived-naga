import {
  HelpOutline as HelpOutlineIcon,
  Settings as SettingsIcon,
  LibraryBooks as KnowledgeCenterIcon,
} from "@mui/icons-material";
import React from "react";
import { Link, useLocation } from "react-router-dom";
import { LogoutMenuItem } from "./LogoutMenuItem";
import { UserNavItem } from "./UserNavItem";

type BaseNavProps = {
  showUserNavItem: boolean;
  navContent: React.ReactNode;
  settingsRoute?: string;
  showKnowledgeCenter?: boolean;
  showHelp?: boolean;
};

export const BaseNav: React.FC<BaseNavProps> = ({
  showUserNavItem,
  navContent,
  settingsRoute,
  showKnowledgeCenter,
  showHelp = true,
}) => {
  const location = useLocation();

  const isSettings = location.pathname.startsWith(settingsRoute);

  return (
    <div className="primary-nav">
      <div className="primary-nav__logo">
        <img src="/assets/images/nav-icon-truevault.svg" />
      </div>

      <div className="primary-nav__content">{navContent}</div>

      <div className="primary-nav__bottom">
        <div className="primary-nav__bottom-links">
          {settingsRoute && (
            <Link to={settingsRoute} className="primary-nav__bottom-link">
              <SettingsIcon
                className={`primary-nav__bottom-icon ${
                  isSettings ? "primary-nav__bottom-icon--selected" : ""
                }`}
              />
              <span className={isSettings ? "text-weight-black" : ""}>Settings</span>
            </Link>
          )}

          {showKnowledgeCenter && (
            <a
              href={"https://knowledge.truevault.com/"}
              className="primary-nav__bottom-link"
              target="_blank"
              rel="noreferrer noopener"
            >
              <KnowledgeCenterIcon className={`primary-nav__bottom-icon`} />
              <span>Knowledge Center</span>
            </a>
          )}

          {showHelp && (
            <div
              className="primary-nav__bottom-link"
              onClick={() => (window as any).Beacon("open")}
            >
              <HelpOutlineIcon className="primary-nav__bottom-icon" />
              <span>Help</span>
            </div>
          )}

          {showUserNavItem && <UserNavItem />}

          <LogoutMenuItem />
        </div>
      </div>
    </div>
  );
};
