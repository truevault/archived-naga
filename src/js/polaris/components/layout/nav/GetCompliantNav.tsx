import { HelpOutline as HelpOutlineIcon, Settings as SettingsIcon } from "@mui/icons-material";
import React, { useEffect, useMemo, useRef } from "react";
import { Link, useLocation } from "react-router-dom";
import { useSelf } from "../../../../common/hooks/useSelf";
import { GetCompliantStep } from "../../../../common/service/server/Types";
import { useOrganizationVendors, usePrimaryOrganization } from "../../../hooks";
import { useProgressKeys } from "../../../hooks/useProgressKeys";
import { useGetCompliantContext } from "../../../root/GetCompliantContext";
import { Routes } from "../../../root/routes";
import {
  getIndividualStepsForMajorStep,
  getMajorStepLogic,
  getMajorStepStatus,
  majorStepContains,
} from "../../../surveys/steps/GetCompliantMajorStepLogic";
import { getStepLogic, StepLogicContext } from "../../../surveys/steps/GetCompliantStepLogic";
import {
  GetCompliantMajorStep,
  MAJOR_STEPS,
  StepComparison,
} from "../../../surveys/steps/GetCompliantSteps";
import { BaseNav } from "./BaseNav";
import { GetCompliantNavProgressLink, NavIndentation } from "./GetCompliantNavProgressLink";
import { LogoutMenuItem } from "./LogoutMenuItem";
import { getCurrentProgressContextFromLocation } from "./navUtils";
import { UserNavItem } from "./UserNavItem";

interface GetCompliantNavProps {
  currentProgress?: GetCompliantStep;
  showNavSteps: boolean;
}

export const GetCompliantNav: React.FC<GetCompliantNavProps> = ({
  currentProgress,
  showNavSteps = true,
}) => {
  const location = useLocation();

  const { progress } = useGetCompliantContext();

  const [org] = usePrimaryOrganization();
  const [recipients] = useOrganizationVendors(org!!.id);
  const { progress: orgProgress } = useProgressKeys(org!!.id);
  currentProgress = currentProgress || progress;

  const activeStep: GetCompliantStep = getCurrentProgressContextFromLocation(location.pathname);

  const stepContext = useMemo<StepLogicContext>(
    () => ({ org, recipients, orgProgress }),
    [org, recipients, orgProgress],
  );

  const navItems = MAJOR_STEPS.filter((major) => {
    const steps = getIndividualStepsForMajorStep(major);
    return (
      steps.some((minor) => {
        const { skip, visible } = getStepLogic(minor);
        return !skip(stepContext) && visible(stepContext);
      }) && major != "Done"
    );
  }).map((major) => {
    return (
      <MajorStepLink
        key={major}
        major={major}
        currentProgress={currentProgress}
        activeStep={activeStep}
        context={stepContext}
      />
    );
  });

  return (
    <BaseNav
      showUserNavItem
      settingsRoute={Routes.getCompliant.settings.root}
      showKnowledgeCenter={true}
      navContent={showNavSteps && navItems}
      showHelp={false}
    />
  );
};

type MajorStepLinkProps = {
  currentProgress: GetCompliantStep;
  activeStep: GetCompliantStep;
  major: GetCompliantMajorStep;
  context: StepLogicContext;
};
const MajorStepLink: React.FC<MajorStepLinkProps> = ({
  major,
  currentProgress,
  activeStep,
  context,
}) => {
  const logic = getMajorStepLogic(major);
  const status = getMajorStepStatus(major, currentProgress);
  const isActive = majorStepContains(major, activeStep);
  const isDisabled = !isActive && status.progress == "NOT_STARTED";

  const individualSteps = getIndividualStepsForMajorStep(major);
  const children =
    individualSteps.length > 1 ? (
      <MinorSteps
        steps={individualSteps}
        context={context}
        activeStep={activeStep}
        currentStep={currentProgress}
      />
    ) : undefined;

  return (
    <GetCompliantNavProgressLink
      key={logic.label}
      route={status.href}
      label={logic.label}
      isDone={status.progress == "DONE"}
      startExpanded={isActive}
      isActive={isActive}
      isDisabled={isDisabled}
    >
      {children}
    </GetCompliantNavProgressLink>
  );
};

type MinorStepsProps = {
  steps: GetCompliantStep[];
  context: StepLogicContext;
  activeStep: GetCompliantStep;
  currentStep: GetCompliantStep;
};
export const MinorSteps: React.FC<MinorStepsProps> = ({
  steps,
  activeStep,
  currentStep,
  context,
}) => {
  const minorSteps: React.ReactNode[] = [];

  let lastMinor: string | undefined = undefined;
  let collectedSteps: GetCompliantStep[] = [];

  for (const step of steps) {
    const logic = getStepLogic(step);
    if (logic.minorStep === undefined || lastMinor === undefined || logic.minorStep != lastMinor) {
      // push the previous step into the list
      if (collectedSteps.length == 0) {
        // do nothing
      } else if (collectedSteps.length === 1) {
        // push a "individual" minor step
        minorSteps.push(
          <IndividualStepLink
            key={collectedSteps[0]}
            step={collectedSteps[0]}
            context={context}
            activeStep={activeStep}
            currentStep={currentStep}
          />,
        );
      } else {
        // push a "group" minor step
        minorSteps.push(
          <MinorStepLink
            key={collectedSteps[0]}
            steps={collectedSteps}
            context={context}
            activeStep={activeStep}
            currentStep={currentStep}
          />,
        );
      }

      collectedSteps = [];
    }

    lastMinor = logic.minorStep;
    collectedSteps.push(step);
  }

  // push the previous step into the list
  if (collectedSteps.length == 0) {
    // do nothing
  } else if (collectedSteps.length === 1) {
    // push a "individual" minor step
    minorSteps.push(
      <IndividualStepLink
        key={collectedSteps[0]}
        step={collectedSteps[0]}
        context={context}
        activeStep={activeStep}
        currentStep={currentStep}
      />,
    );
  } else {
    // push a "group" minor step
    minorSteps.push(
      <MinorStepLink
        key={collectedSteps[0]}
        steps={collectedSteps}
        context={context}
        activeStep={activeStep}
        currentStep={currentStep}
      />,
    );
  }

  return <>{minorSteps}</>;
};

type MinorStepLinkProps = {
  steps: GetCompliantStep[];
  context: StepLogicContext;
  activeStep: GetCompliantStep;
  currentStep: GetCompliantStep;
};
export const MinorStepLink: React.FC<MinorStepLinkProps> = ({
  steps,
  activeStep,
  currentStep,
  context,
}) => {
  const first = steps[0];
  const firstLogic = getStepLogic(first);
  const label = firstLogic.minorStep!!;
  const isActive = steps.some((s) => s === activeStep);
  const isDisabled = !isActive && steps.every((s) => StepComparison.isAfter(s, currentStep));

  return (
    <GetCompliantNavProgressLink
      route={firstLogic.route}
      label={label}
      indented={1}
      hideIcon={true}
      isDone={false}
      isActive={isActive}
      isDisabled={isDisabled}
      collapsible={false}
    >
      {steps.map((s) => (
        <IndividualStepLink
          key={s}
          step={s}
          indented={2}
          activeStep={activeStep}
          currentStep={currentStep}
          context={context}
        />
      ))}
    </GetCompliantNavProgressLink>
  );
};

type IndividualStepLinkProps = {
  step: GetCompliantStep;
  context: StepLogicContext;
  activeStep: GetCompliantStep;
  currentStep: GetCompliantStep;
  indented?: NavIndentation;
};
export const IndividualStepLink: React.FC<IndividualStepLinkProps> = ({
  step,
  indented,
  activeStep,
  currentStep,
  context,
}) => {
  const linkRef = useRef(null);

  const isActive = activeStep === step;
  const logic = getStepLogic(step);

  useEffect(() => {
    if (linkRef.current && isActive) {
      linkRef.current.scrollIntoView();
    }
  }, [linkRef.current, isActive]);

  if (!logic.visible(context)) {
    return null;
  }

  const skip = logic.skip(context);

  return (
    <GetCompliantNavProgressLink
      route={logic.route}
      label={logic.label}
      indented={indented ?? true}
      hideIcon={true}
      isDone={false}
      isActive={isActive}
      isDisabled={!isActive && (StepComparison.isAfter(step, currentStep) || skip)}
      ref={linkRef}
    />
  );
};

type GetCompliantHeaderNavProps = {
  useMinimalNav: boolean;
};

export const GetCompliantHeaderNav: React.FC<GetCompliantHeaderNavProps> = ({
  useMinimalNav = false,
}) => {
  const [self] = useSelf();
  const isLoggedIn = Boolean(self);

  return (
    <div className="header-nav">
      <div className="header-nav__logo">
        <img src="/assets/images/nav-icon-truevault.svg" />
      </div>

      <div className="header-nav__items">
        {isLoggedIn && !useMinimalNav && (
          <div>
            <UserNavItem horizontal={false} />
          </div>
        )}

        {isLoggedIn && !useMinimalNav && (
          <SettingsMenuItem settingsRoute={Routes.getCompliant.settings.root} />
        )}
        <HelpMenuItem />
        {isLoggedIn && <LogoutMenuItem />}
      </div>
    </div>
  );
};

type SettingsMenuItemProps = {
  settingsRoute: string;
};
const SettingsMenuItem: React.FC<SettingsMenuItemProps> = ({ settingsRoute }) => {
  const location = useLocation();
  const isSettings = location.pathname.startsWith(settingsRoute);

  return (
    <div>
      <Link to={settingsRoute} className="primary-nav__bottom-link">
        <SettingsIcon
          className={`primary-nav__bottom-icon ${
            isSettings ? "primary-nav__bottom-icon--selected" : ""
          }`}
        />
        <span className={isSettings ? "text-weight-black" : ""}>Settings</span>
      </Link>
    </div>
  );
};

const HelpMenuItem = () => {
  return (
    <div>
      <div className="primary-nav__bottom-link" onClick={() => (window as any).Beacon("open")}>
        <HelpOutlineIcon className="primary-nav__bottom-icon" />
        <span>Help</span>
      </div>
    </div>
  );
};
