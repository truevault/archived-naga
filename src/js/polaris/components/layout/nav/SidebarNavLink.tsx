import { Chip } from "@mui/material";
import React from "react";
import { NavLink } from "react-router-dom";

interface SidebarNavLinkProps {
  route: string;
  label: string;
  isDisabled: boolean;
  notificationCount?: number;
  isActive: (match: any, location: any) => boolean;
  refresh?: () => void;
}

export const SidebarNavLink: React.FC<SidebarNavLinkProps> = ({
  route,
  label,
  isDisabled,
  notificationCount = 0,
  isActive,
  refresh,
}) => {
  const iA = (match, location) => isActive(match, location);
  return (
    <div className={`primary-nav__link ${isDisabled ? "disabled" : ""}`}>
      <NavLink to={isDisabled ? "#" : route} isActive={iA} onClick={refresh}>
        <span className="ml-sm">{label}</span>
        {notificationCount > 0 && (
          <div className="ml-sm">
            <Chip
              color="primary"
              variant="muted"
              size="xs"
              label={Math.min(notificationCount, 99)}
            />
          </div>
        )}
      </NavLink>
    </div>
  );
};
