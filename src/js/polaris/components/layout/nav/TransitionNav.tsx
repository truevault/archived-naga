import React from "react";
import { BaseNav } from "./BaseNav";

export const TransitionNav = () => {
  return <BaseNav showUserNavItem={false} showHelp={false} navContent={null} />;
};
