import { MonitorHeart } from "@mui/icons-material";
import { Button, Tooltip } from "@mui/material";
import _ from "lodash";
import React from "react";
import { usePrimaryOrganization } from "../../../hooks/useOrganization";
import { RouteHelpers, Routes } from "../../../root/routes";
import { Survey2023Q1WelcomeBackDialog } from "../../organisms/dialogs/Survey2023Q1WelcomeBackDialog";
import { Survey2023Q3WelcomeBackDialog } from "../../organisms/dialogs/Survey2023Q3WelcomeBackDialog";
import { BaseNav } from "./BaseNav";
import { NavDivider, NavItem, NAV_ITEMS, DIVIDER_LITERAL } from "./config";
import { SidebarNavLink } from "./SidebarNavLink";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { NewSurveyBanner } from "../../organisms/callouts/NewSurveyBanner";

export const StayCompliantNav: React.FC = () => {
  const [organization, , refreshOrg] = usePrimaryOrganization();

  const navItems = (org: OrganizationDto) =>
    NAV_ITEMS.map((navItem: NavItem | NavDivider, idx: number) => {
      if (navItem === DIVIDER_LITERAL) {
        return <hr key={`divider-${idx}`} className="primary-nav__divider" />;
      }

      const { item, route, label, hidden, itemAliases = [] } = navItem as NavItem;

      const isHidden =
        typeof hidden == "function" ? hidden(org) : typeof hidden == "undefined" ? false : hidden;

      let notificationCount = 0;

      if (item == "REQUEST_INBOX") {
        notificationCount = org.activeRequestCount;
      }
      if (item == "TASK_LIST") {
        notificationCount = org.activeTaskCount;
      }

      const routeStr = _.isFunction(route) ? route(org.id) : route;
      const isActive = (_m, l) =>
        !hidden &&
        [route, routeStr, ...itemAliases].filter((r) => l.pathname.startsWith(r)).length > 0;

      if (isHidden) {
        return null;
      }

      return (
        <SidebarNavLink
          key={item}
          label={label}
          route={routeStr}
          isDisabled={false}
          isActive={isActive}
          notificationCount={notificationCount}
          refresh={refreshOrg}
        />
      );
    });

  const navContent = (org?: OrganizationDto) => {
    let tooltip = "";
    let route = "/";
    switch (org.nextSurvey) {
      case "s2023_1":
        tooltip =
          "It’s time for your 2023 Compliance Check Up! Click here to renew your compliance for the new year.";
        route = Routes.surveys["2023"].root;
        break;
      case "s2023_3":
        tooltip =
          "It’s time for your Q3 2023 Check Up! Click here to keep your compliance up to date.";
        route = Routes.surveys.midyear2023.root;
        break;
    }
    return (
      org && (
        <>
          {org.nextSurvey == "s2023_1" && (
            <>
              <NewSurveyBanner surveyName="Q1 2023 Checkup" route={route} />
              <div className="text-center mb-sm">
                <Tooltip title={tooltip} placement="right">
                  <Button
                    startIcon={<MonitorHeart />}
                    variant="contained"
                    className="primary-nav__survey-alert-button"
                    href={route}
                  >
                    Q1 2023 Checkup
                  </Button>
                </Tooltip>
              </div>
              <div className="text-center mb-xl">
                <Button
                  startIcon={<MonitorHeart />}
                  variant="contained"
                  className="primary-nav__survey-alert-button"
                  disabled={true}
                >
                  Q3 2023 Checkup
                </Button>
              </div>
            </>
          )}
          {org.nextSurvey == "s2023_3" && (
            <>
              <NewSurveyBanner surveyName="Q3 2023 Checkup" route={route} />
              <div className="text-center mb-xl">
                <Tooltip title={tooltip} placement="right">
                  <Button
                    startIcon={<MonitorHeart />}
                    variant="contained"
                    className="primary-nav__survey-alert-button"
                    href={route}
                  >
                    Q3 2023 Checkup
                  </Button>
                </Tooltip>
              </div>
            </>
          )}
          {navItems(org)}
          {org.nextSurvey == "s2023_1" && <Survey2023Q1WelcomeBackDialog />}
          {org.nextSurvey == "s2023_3" && <Survey2023Q3WelcomeBackDialog />}
        </>
      )
    );
  };

  return (
    <BaseNav
      showUserNavItem
      settingsRoute={RouteHelpers.organization.settings(organization?.id)}
      navContent={navContent(organization)}
    />
  );
};
