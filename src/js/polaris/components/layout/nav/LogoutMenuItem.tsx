import { ExitToApp as ExitToAppIcon } from "@mui/icons-material";
import React from "react";
import { useQueryClient } from "@tanstack/react-query";
import { useDispatch } from "react-redux";
import { useActionRequest } from "../../../../common/hooks/api";
import { selfActions } from "../../../../common/reducers/selfSlice";
import { Api } from "../../../../common/service/Api";

export const LogoutMenuItem = () => {
  const dispatchRedux = useDispatch();
  const queryClient = useQueryClient();

  const { fetch: logout } = useActionRequest({
    api: async () => await Api.login.logout(),
    onSuccess: () => {
      queryClient.invalidateQueries(["self"]);
      dispatchRedux(selfActions.purge());
    },
  });

  return (
    <div className="primary-nav__bottom-link" onClick={logout}>
      <ExitToAppIcon className="primary-nav__bottom-icon" />
      <span>Logout</span>
    </div>
  );
};
