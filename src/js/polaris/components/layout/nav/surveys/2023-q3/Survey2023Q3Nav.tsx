import React from "react";
import { ArrowBackOutlined } from "@mui/icons-material";
import { usePrimaryOrganization } from "../../../../../hooks";
import { RouteHelpers, Routes } from "../../../../../root/routes";
import { BaseNav } from "../../BaseNav";
import { GetCompliantNavProgressLink } from "../../GetCompliantNavProgressLink";
import { NavLink, useLocation } from "react-router-dom";
import {
  Survey2023Q3Step,
  Survey2023Q3StepComparison,
} from "../../../../../surveys/steps/Survey2023Q3Steps";

type SurveyStep = {
  step: Survey2023Q3Step;
  label: string;
  route: string;
  altRoutes?: string[];
};

const SURVEY_STEPS: SurveyStep[] = [
  {
    step: "SellingSharing",
    label: "Selling and Sharing",
    route: Routes.surveys.midyear2023.sellingSharing,
  },
  {
    step: "DataCollection",
    label: "Data Collection",
    route: Routes.surveys.midyear2023.dataCollection,
  },
  {
    step: "DataRecipients",
    label: "Data Recipients",
    route: Routes.surveys.midyear2023.dataRecipients,
  },
  {
    step: "DataSources",
    label: "Data Sources",
    route: Routes.surveys.midyear2023.dataSources,
  },
  {
    step: "WebsiteAudit",
    label: "Website Audit",
    route: Routes.surveys.midyear2023.websiteAudit,
  },
  {
    step: "NextSteps",
    label: "Next Steps",
    route: Routes.surveys.midyear2023.nextSteps,
  },
];

type Survey2023Q3NavProps = {
  progress: Survey2023Q3Step;
};

export const Survey2023Q3Nav: React.FC<Survey2023Q3NavProps> = ({ progress }) => {
  const location = useLocation();
  const [org] = usePrimaryOrganization();

  const navItems = [
    <div className={`primary-nav__link`} key="back">
      <NavLink to={Routes.root} isActive={() => false}>
        <ArrowBackOutlined className="primary-nav__progress-icon" />
        <span className="ml-sm">Back to Stay Compliant</span>
      </NavLink>
    </div>,
    ...SURVEY_STEPS.map((step) => {
      const isActive =
        step.route == location.pathname || step.altRoutes?.some((r) => r == location.pathname);

      const isBeforeProgress = Survey2023Q3StepComparison.isBefore(step.step, progress);
      const isAfterProgress = Survey2023Q3StepComparison.isAfter(step.step, progress);

      return (
        <GetCompliantNavProgressLink
          key={step.step}
          route={step.route}
          label={step.label}
          isActive={isActive}
          isDone={isBeforeProgress}
          isDisabled={isAfterProgress && !isActive}
          indented
        />
      );
    }),
  ];

  return (
    <BaseNav
      showUserNavItem
      settingsRoute={RouteHelpers.organization.settings(org.id)}
      navContent={navItems}
    />
  );
};
