import React from "react";
import { ArrowBackOutlined } from "@mui/icons-material";
import { usePrimaryOrganization } from "../../../../../hooks";
import { RouteHelpers, Routes } from "../../../../../root/routes";
import { BaseNav } from "../../BaseNav";
import { GetCompliantNavProgressLink } from "../../GetCompliantNavProgressLink";
import { NavLink, useLocation } from "react-router-dom";
import {
  Survey2023Q1Step,
  Survey2023Q1StepComparison,
} from "../../../../../surveys/steps/Survey2023Q1Steps";

type SurveyStep = {
  step: Survey2023Q1Step;
  label: string;
  route: string;
  altRoutes?: string[];
};

const SURVEY_STEPS: SurveyStep[] = [
  {
    step: "BusinessSurvey",
    label: "Business Survey",
    route: Routes.surveys["2023"].businessSurvey,
  },
  {
    step: "CollectionPurposes",
    label: "Collection Purposes",
    route: Routes.surveys["2023"].collectionPurposes,
    altRoutes: [Routes.surveys["2023"].corePrivacyConcepts],
  },
  {
    step: "DataRetention",
    label: "Data Retention",
    route: Routes.surveys["2023"].dataRetention,
  },
  {
    step: "DataRecipients",
    label: "Data Recipients",
    route: Routes.surveys["2023"].dataRecipients,
  },
  {
    step: "Tasks",
    label: "Tasks",
    route: Routes.surveys["2023"].tasks,
  },
];

type Survey2023Q1NavProps = {
  progress: Survey2023Q1Step;
};

export const Survey2023Q1Nav: React.FC<Survey2023Q1NavProps> = ({ progress }) => {
  const location = useLocation();
  const [org] = usePrimaryOrganization();

  const navItems = [
    <div className={`primary-nav__link`} key="back">
      <NavLink to={Routes.root} isActive={() => false}>
        <ArrowBackOutlined className="primary-nav__progress-icon" />
        <span className="ml-sm">Back to Stay Compliant</span>
      </NavLink>
    </div>,
    ...SURVEY_STEPS.map((step) => {
      const isActive =
        step.route == location.pathname || step.altRoutes?.some((r) => r == location.pathname);

      const isBeforeProgress = Survey2023Q1StepComparison.isBefore(step.step, progress);
      const isAfterProgress = Survey2023Q1StepComparison.isAfter(step.step, progress);

      return (
        <GetCompliantNavProgressLink
          key={step.step}
          route={step.route}
          label={step.label}
          isActive={isActive}
          isDone={isBeforeProgress}
          isDisabled={isAfterProgress && !isActive}
          indented
        />
      );
    }),
  ];

  return (
    <BaseNav
      showUserNavItem
      settingsRoute={RouteHelpers.organization.settings(org.id)}
      navContent={navItems}
    />
  );
};
