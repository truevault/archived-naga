import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { StayCompliantNavItem } from "../../../../common/service/server/Types";
import { RouteHelpers, Routes } from "../../../root/routes";

export type NavItem = {
  item: StayCompliantNavItem;
  label: string;
  route: string | ((params: any) => string);
  hidden?: boolean | ((org: OrganizationDto) => boolean);
  // opt. other routes that causes this one to show as ACTIVE
  itemAliases?: string[];
};

export const DIVIDER_LITERAL = "DIVIDER";
export type NavDivider = "DIVIDER";

export const NAV_ITEMS: (NavItem | NavDivider)[] = [
  {
    item: "REQUEST_INBOX",
    label: "Request Inbox",
    route: Routes.requests.active,
    itemAliases: ["/requests/"],
  },
  {
    item: "REQUEST_INSTRUCTIONS",
    label: "Request Instructions",
    route: RouteHelpers.organization.requestHandling.base,
  },
  {
    item: "RECORD_LOOKUP",
    label: "Record Lookup",
    route: Routes.records.lookup,
  },
  DIVIDER_LITERAL,
  { item: "TASK_LIST", label: "Task List", route: Routes.tasks.todo },
  { item: "DATA_MAP", label: "Data Map", route: Routes.dataMap.root },
  DIVIDER_LITERAL,
  {
    item: "COOKIES",
    label: "Cookies & Opt-Outs",
    route: Routes.cookies.root,
    hidden: (org) => !org.featureGdpr,
  },
  { item: "PRIVACY_CENTER", label: "Privacy Center", route: Routes.privacyCenter.root },
  { item: "KNOWLEDGE_CENTER", label: "Knowledge Center", route: Routes.knowledgeCenter.root },
];
