import {
  ExpandLess,
  ExpandMore,
  FiberManualRecord as FiberManualRecordIcon,
  FiberManualRecordOutlined as FiberManualRecordOutlinedIcon,
} from "@mui/icons-material";
import { IconButton } from "@mui/material";
import clsx from "clsx";
import React, { forwardRef, useState } from "react";
import { NavLink } from "react-router-dom";

export type NavIndentation = boolean | 1 | 2;
interface GetCompliantNavProgressLinkProps {
  route: string;
  label: string;
  isDone: boolean;
  isDisabled: boolean;
  isActive: boolean;
  indented?: NavIndentation;

  hideIcon?: boolean;

  startExpanded?: boolean;
  collapsible?: boolean;

  children?: React.ReactNode;
}

export const GetCompliantNavProgressLink = forwardRef<
  HTMLDivElement,
  GetCompliantNavProgressLinkProps
>(
  (
    {
      route,
      label,
      isDone,
      isDisabled,
      isActive,

      indented = false,
      hideIcon = false,

      startExpanded = false,
      collapsible = true,

      children,
    },
    ref,
  ) => {
    const [expanded, setExpanded] = useState(startExpanded);
    const IconClass = isDone ? FiberManualRecordIcon : FiberManualRecordOutlinedIcon;

    const toggleExpanded = (e) => {
      e.preventDefault();
      setExpanded(!expanded);
    };

    const collapseAction =
      collapsible && Boolean(children) ? (
        <IconButton size="small" onClick={toggleExpanded} className="ml-xs">
          {!expanded ? <ExpandMore /> : <ExpandLess />}
        </IconButton>
      ) : undefined;

    return (
      <>
        <div
          className={clsx("primary-nav__link", {
            disabled: isDisabled,
            indented: indented === true || indented === 1,
            ["level-3"]: indented === 2,
          })}
          ref={ref}
        >
          <NavLink to={isDisabled ? "#" : route} isActive={() => isActive}>
            {!hideIcon && <IconClass className="primary-nav__progress-icon" />}
            <span className="ml-sm">{label}</span>
            {collapseAction}
          </NavLink>
        </div>
        {(!collapsible || expanded) && children}
      </>
    );
  },
);
