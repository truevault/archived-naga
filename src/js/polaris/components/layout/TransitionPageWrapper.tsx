import React, { ReactNode } from "react";
import { PageWrapper } from "../../../common/layout/PageWrapper";
import { GetCompliantPageContent } from "../../../common/layout/PageContent";
import { TransitionNav } from "./nav/TransitionNav";

type TransitionPageWrapperProps = {
  children: ReactNode;
  header?: ReactNode;
};

export const TransitionPageWrapper: React.FC<TransitionPageWrapperProps> = ({
  children,
  header,
}) => {
  return (
    <PageWrapper cls="get-compliant-wrapper">
      <TransitionNav />
      <GetCompliantPageContent header={header}>{children}</GetCompliantPageContent>
    </PageWrapper>
  );
};

TransitionPageWrapper.displayName = "TransitionPageWrapper";
