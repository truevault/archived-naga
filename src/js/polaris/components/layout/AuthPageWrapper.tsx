import React from "react";
import { Paper } from "@mui/material";

export const AuthPageWrapper = ({ image, children }) => {
  return (
    <div className="page page--contained">
      <Paper className="page-box">
        <div className="page-box--inner">
          <div className="page-box__img-wrapper">
            <img src={image} className="page-box__img" />
          </div>

          <div className="page-box__content">{children}</div>
        </div>
      </Paper>
    </div>
  );
};
