import React from "react";
import { KeyboardBackspace } from "@mui/icons-material";

type PageHeaderBackProps = {
  to: string;
  children: React.ReactNode;
};

export const PageHeaderBack: React.FC<PageHeaderBackProps> = ({ to, children }) => {
  return (
    <a className="text-decoration-none neutral-700 text-weight-regular" href={to}>
      <KeyboardBackspace className="font-size-unset mr-xs" />
      {children}
    </a>
  );
};
