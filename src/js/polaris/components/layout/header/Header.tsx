import React, { ReactNode } from "react";
import EditIcon from "@mui/icons-material/Edit";
import clsx from "clsx";
import { Callout, CalloutVariant } from "../../Callout";
import { ProgressBars, ProgressBarsProps } from "../../ProgressBar";
import { DataRecipientLogo } from "../../DataRecipientLogo";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";

type PageHeaderProps = {
  titleContent: React.ReactNode;
  titleHeaderContent?: string | ReactNode;
  subtitleContent?: string;
  descriptionContent?: string | ReactNode;
  descriptionHtmlContent?: string;
  helpContent?: string | ReactNode;
  notesContent?: string | ReactNode;
  progressBarContent?: ReactNode;
  progress?: ProgressBarsProps;
  wide?: boolean;
  logo?: React.ReactNode;
  actions?: React.ReactNode;

  descriptionLarge?: boolean;
  className?: string;
};

export const PageHeader: React.FC<PageHeaderProps> = ({
  titleHeaderContent,
  titleContent,
  subtitleContent,
  progressBarContent,
  progress,
  helpContent,
  descriptionContent,
  descriptionHtmlContent,
  notesContent,
  wide,
  descriptionLarge = false,
  className,
  logo,
  actions,
}) => {
  if (progressBarContent && progress) {
    console.warn("You passed both progressBarContent and progress. Ignoring progress.");
  }

  const progressBar = progressBarContent || (progress && <ProgressBars {...progress} />);

  return (
    <div className={clsx("header-container mb-xl", className, { "header-container--wide": wide })}>
      <div style={{ display: "flex", flexDirection: "column" }}>
        {titleHeaderContent && (
          <h3 className="header-container--title-header">{titleHeaderContent}</h3>
        )}
        <div style={{ display: "flex", flexDirection: "row" }}>
          <div style={{ display: "flex", flexDirection: "row", alignItems: "flex-start" }}>
            {logo && <div className="mr-md">{logo}</div>}

            <div>
              <h1 className="header-container--title text-color-purple-dark">{titleContent}</h1>

              {subtitleContent && <h2 className="header-container--subtitle">{subtitleContent}</h2>}

              {progressBar && (
                <div className="header-container--progress-content">{progressBar}</div>
              )}

              {descriptionContent && (
                <div
                  className={clsx("header-container--description", {
                    "text-large": descriptionLarge,
                  })}
                >
                  {descriptionContent}
                </div>
              )}

              {descriptionHtmlContent && (
                <div
                  className={clsx("header-container--description", {
                    "text-large": descriptionLarge,
                  })}
                  dangerouslySetInnerHTML={{ __html: descriptionHtmlContent }}
                />
              )}
            </div>
          </div>

          {actions && <div className="text-t1 ml-auto">{actions}</div>}
        </div>
      </div>

      {helpContent && <div className="text-component-help">{helpContent}</div>}
      {notesContent && (
        <Callout className="header-container--notes" variant={CalloutVariant.Purple}>
          {notesContent}
        </Callout>
      )}
    </div>
  );
};

type VendorPageHeaderProps = {
  vendor?: VendorDto | OrganizationDataRecipientDto;
  titleContent: string;
  titleHeaderContent?: string;
  descriptionContent?: string | ReactNode;
  wide?: boolean;
  actionUrl?: string;
  actionCta?: string;
};

export const VendorPageHeader: React.FC<VendorPageHeaderProps> = ({
  vendor,
  actionUrl,
  actionCta = "Edit Settings",
  ...rest
}) => {
  return (
    <PageHeader
      logo={<DataRecipientLogo dataRecipient={vendor} size={48} />}
      actions={
        Boolean(actionUrl) && (
          <a href={actionUrl}>
            <EditIcon fontSize="inherit" className="mr-xxs" />
            {actionCta}
          </a>
        )
      }
      {...rest}
    />
  );
};
