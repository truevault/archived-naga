import React, { ReactNode } from "react";
import { PageLoading } from "../../../common/components/Loading";
import { NetworkRequestGate } from "../../../common/components/NetworkRequestGate";
import { OmniBarProvider } from "../../../common/hooks/useOmniBar";
import { PageContent } from "../../../common/layout/PageContent";
import { PageWrapper } from "../../../common/layout/PageWrapper";
import { NetworkRequestState } from "../../../common/models";
import { OmniBar } from "../dialog/OmniBar";
import { StayCompliantNav } from "./nav/StayCompliantNav";

type StandardPageWrapperProps = {
  includeNav?: boolean;
  children?: ReactNode;
  footer?: ReactNode;
  fullWidth?: boolean;
  fullWidthHeader?: ReactNode;
  hero?: ReactNode;
  navContent?: ReactNode;
  requests?: NetworkRequestState[];
};

export const StandardPageWrapper = ({
  includeNav = true,
  children,
  footer,
  fullWidth,
  fullWidthHeader,
  hero,
  navContent,
  requests = [],
}: StandardPageWrapperProps) => {
  return (
    <PageWrapper>
      <NetworkRequestGate requests={requests}>
        <OmniBarProvider>
          <OmniBar></OmniBar>
          {includeNav && (navContent || <StayCompliantNav />)}
          <PageContent
            hero={hero}
            footer={footer}
            fullWidth={fullWidth}
            fullWidthHeader={fullWidthHeader}
          >
            {children}
          </PageContent>
        </OmniBarProvider>
      </NetworkRequestGate>
    </PageWrapper>
  );
};

export const LoadingPageWrapper = (props: StandardPageWrapperProps) => {
  return (
    <StandardPageWrapper {...props}>
      <PageLoading />
    </StandardPageWrapper>
  );
};
