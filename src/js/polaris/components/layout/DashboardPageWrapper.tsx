import React, { ReactNode } from "react";
import { OmniBarProvider } from "../../../common/hooks/useOmniBar";
import { PageWrapper } from "../../../common/layout/PageWrapper";
import { GetCompliantPageContent } from "../../../common/layout/PageContent";
import { OmniBar } from "../dialog/OmniBar";
import { GetCompliantHeaderNav } from "./nav/GetCompliantNav";
import { NetworkRequestState } from "../../../common/models";
import { NetworkRequestGate } from "../../../common/components/NetworkRequestGate";

type Props = {
  footer?: ReactNode;
  showNavSteps?: boolean;
  fullWidth?: boolean;
  disablePadding?: boolean;
  useMinimalNav?: boolean;
  requests?: NetworkRequestState[];
};

export const DashboardPageWrapper: React.FC<Props> = ({
  footer,
  useMinimalNav,
  requests = [],
  children,
}) => {
  return (
    <PageWrapper>
      <OmniBarProvider>
        <OmniBar></OmniBar>
        <GetCompliantPageContent
          noNav
          footer={footer}
          header={<GetCompliantHeaderNav useMinimalNav={useMinimalNav} />}
        >
          <NetworkRequestGate requests={requests}>{children}</NetworkRequestGate>
        </GetCompliantPageContent>
      </OmniBarProvider>
    </PageWrapper>
  );
};
