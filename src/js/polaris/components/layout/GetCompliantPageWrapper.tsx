import React, { ReactNode } from "react";
import { PageWrapper } from "../../../common/layout/PageWrapper";
import { GetCompliantStep } from "../../../common/service/server/Types";
import { GetCompliantNav } from "./nav/GetCompliantNav";
import { GetCompliantPageContent } from "../../../common/layout/PageContent";
import { useGetCompliantContext } from "../../root/GetCompliantContext";
import { OmniBarProvider } from "../../../common/hooks/useOmniBar";
import { OmniBar } from "../dialog/OmniBar";
import { NetworkRequestState } from "../../../common/models";
import { NetworkRequestGate } from "../../../common/components/NetworkRequestGate";

type GetCompliantPageWrapperProps = {
  currentProgress?: GetCompliantStep;
  children: ReactNode;
  header?: ReactNode;
  progressFooter?: ReactNode;
  showNavSteps?: boolean;
  fullWidth?: boolean;
  disablePadding?: boolean;
  requests?: NetworkRequestState[];
};

export const GetCompliantPageWrapper: React.FC<GetCompliantPageWrapperProps> = ({
  currentProgress,
  children,
  header,
  progressFooter,
  showNavSteps = true,
  fullWidth = false,
  disablePadding = false,
  requests = [],
}) => {
  const { progress } = useGetCompliantContext();

  return (
    <PageWrapper cls="get-compliant-wrapper">
      <OmniBarProvider>
        <OmniBar></OmniBar>

        <GetCompliantNav
          currentProgress={currentProgress || progress}
          showNavSteps={showNavSteps}
        />
        <GetCompliantPageContent
          header={header}
          footer={progressFooter}
          fullWidth={fullWidth}
          disablePadding={disablePadding}
        >
          <NetworkRequestGate requests={requests}>{children}</NetworkRequestGate>
        </GetCompliantPageContent>
      </OmniBarProvider>
    </PageWrapper>
  );
};

GetCompliantPageWrapper.displayName = "GetCompliantPageWrapper";
