import { FormApi } from "final-form";
import React, { useCallback } from "react";
import { useActionRequest } from "../../../../common/hooks/api";
import { NetworkRequestState } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import { PrivacyCenterDto } from "../../../../common/service/server/dto/PrivacyCenterDto";
import { OrganizationPublicId } from "../../../../common/service/server/Types";
import { Error as OrganizationError } from "../../../copy/organization";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../../hooks";
import { usePrivacyCenter } from "../../../hooks/usePrivacyCenter";
import { PrivacyCenterCaResidencySettingsRow } from "../../organisms/settings/rows/PrivacyCenterCaResidencySettingsRow";
import { PrivacyCenterDpoRow } from "../../organisms/settings/rows/PrivacyCenterDpoRow";
import { PrivacyCenterFaviconSettingRow } from "../../organisms/settings/rows/PrivacyCenterFavIconSettingRow";
import { PrivacyCenterHeaderLogoSettingRow } from "../../organisms/settings/rows/PrivacyCenterHeaderLogoSettingRow";
import { PrivacyCenterLogoLinkSettingRow } from "../../organisms/settings/rows/PrivacyCenterLogoLinkSettingRow";
import { PrivacyCenterRequestFormSettingsRow } from "../../organisms/settings/rows/PrivacyCenterRequestFormSettingsRow";
import { PrivacyCenterUrlSettingRow } from "../../organisms/settings/rows/PrivacyCenterUrlSettingsRow";

type PrivacyCenterSettingsProps = {
  organizationId: OrganizationPublicId;
  privacyCenterUrl: string;
  logoUrl: string;
  logoLinkUrl: string;
  dpoName: string | undefined;
  dpoEmail: string | undefined;
  dpoPhone: string | undefined;
  dpoAddress: string | undefined;
  dpoDoesNotApply: boolean;
  requiresDpo: boolean;
  hasDpoTask: boolean;
  faviconUrl: string;
  disableCaResidencyConfirmation: boolean;
  customFieldEnabled: boolean;
  customFieldHelp: string;
  customFieldLabel: string;
  handleFormSubmit: (values) => Promise<void>;
  handleDoItLaterTask: () => Promise<void>;
  handleDpoDoesNotApply: (doesNotApply: boolean) => Promise<void>;
  handleCustomFieldSubmit: (values) => void; // It's possible this can be the same as handleFormSubmit, haven't looked into it
  includeUrlField?: boolean;
  includeRequestFormField?: boolean;
  includeDpoField?: boolean;
  topDivider?: boolean;
};
export const PrivacyCenterSettings: React.FC<PrivacyCenterSettingsProps> = ({
  organizationId,
  privacyCenterUrl,
  logoUrl,

  logoLinkUrl,
  dpoName,
  dpoEmail,
  dpoPhone,
  dpoAddress,
  requiresDpo,
  hasDpoTask,
  faviconUrl,
  disableCaResidencyConfirmation,
  customFieldEnabled,
  customFieldHelp,
  customFieldLabel,
  handleFormSubmit,
  handleDoItLaterTask,
  handleCustomFieldSubmit,
  includeUrlField = false,
  includeRequestFormField = false,
  includeDpoField = true,
  topDivider = false,
}) => {
  return (
    <div>
      {topDivider && <hr className="mb-lg" />}
      {includeUrlField && (
        <>
          <PrivacyCenterUrlSettingRow url={privacyCenterUrl} />
          <hr className="my-lg" />
        </>
      )}

      <PrivacyCenterHeaderLogoSettingRow
        organizationId={organizationId}
        logoUrl={logoUrl}
        handleFormSubmit={handleFormSubmit}
      />
      <hr className="my-lg" />
      <PrivacyCenterLogoLinkSettingRow
        logoLinkUrl={logoLinkUrl}
        handleFormSubmit={handleFormSubmit}
      />
      <hr className="my-lg" />
      <PrivacyCenterFaviconSettingRow
        organizationId={organizationId}
        faviconUrl={faviconUrl}
        handleFormSubmit={handleFormSubmit}
      />

      {includeRequestFormField && (
        <>
          <hr className="my-lg" />
          <PrivacyCenterRequestFormSettingsRow
            customFieldEnabled={customFieldEnabled}
            customFieldHelp={customFieldHelp}
            customFieldLabel={customFieldLabel}
            handleCustomFieldSubmit={handleCustomFieldSubmit}
          />
        </>
      )}

      <hr className="my-lg" />
      <PrivacyCenterCaResidencySettingsRow
        caResidencyFields={{ disableCaResidencyConfirmation }}
        handleFormSubmit={handleFormSubmit}
      />

      {includeDpoField && (
        <>
          <hr className="my-lg" />
          <PrivacyCenterDpoRow
            hideFooter
            dpoName={dpoName}
            dpoEmail={dpoEmail}
            dpoPhone={dpoPhone}
            dpoAddress={dpoAddress}
            requiresDpo={requiresDpo}
            hasDpoTask={hasDpoTask}
            handleFormSubmit={handleFormSubmit}
            handleAddTask={handleDoItLaterTask}
          />
        </>
      )}
    </div>
  );
};

type CustomFields =
  | { customFieldEnabled: false }
  | {
      customFieldEnabled: true;
      customFieldHelp: string;
      customFieldLabel: string;
    };

export const usePrivacyCenterSettings = (): UsePrivacyCenterSettings => {
  const organizationId = usePrimaryOrganizationId();

  const [organization, orgRequest] = usePrimaryOrganization();
  const [privacyCenter, privacyCentersRequest, fetch] = usePrivacyCenter(organizationId);

  const updatePrivacyCenterApi = useCallback(
    (submitValues) =>
      Api.privacyCenter.updatePrivacyCenter(organization.id, privacyCenter.id, submitValues),
    [organization?.id, privacyCenter?.id],
  );

  const { fetch: updatePrivacyCenterValues } = useActionRequest({
    api: updatePrivacyCenterApi,
    messages: {
      forceError: OrganizationError.UpdateOrganization,
    },
    onSuccess: () => fetch(),
  });

  const handleCustomFieldSubmit = useCallback(
    async (fields: CustomFields, form: FormApi<CustomFields>) => {
      await Api.organization.updateCustomField(organization.id, fields);
      form.reset(fields);
    },
    [organization.id],
  );

  const handleFormSubmit = useCallback(
    (values) => updatePrivacyCenterValues(Object.assign({}, privacyCenter, values)),
    [updatePrivacyCenterValues, privacyCenter],
  );

  const handleDoItLaterTask = async () => {
    await Api.tasks.createAddDpoTask(organization.id);
    fetch();
  };

  const updatePolicyApi = useCallback(
    async (values) => {
      await Api.privacyCenter.updatePrivacyCenter(organizationId, privacyCenter?.id, values);
    },
    [organizationId, privacyCenter?.id],
  );

  const { fetch: updatePrivacyPolicy, request: updatePrivacyPolicyRequest } = useActionRequest({
    api: updatePolicyApi,
    onSuccess: () => fetch(),
  });

  const handleDpoDoesNotApply = useCallback(
    (doesNotApply: boolean) =>
      updatePrivacyPolicy({ ...privacyCenter, dpoOfficerDoesNotApply: doesNotApply }),
    [privacyCenter, updatePrivacyPolicy],
  );

  return {
    settings: {
      organizationId,
      privacyCenterUrl: privacyCenter?.url,
      logoUrl: privacyCenter?.logoUrl,
      dpoName: privacyCenter?.dpoOfficerName,
      dpoEmail: privacyCenter?.dpoOfficerEmail,
      dpoPhone: privacyCenter?.dpoOfficerPhone,
      dpoAddress: privacyCenter?.dpoOfficerAddress,
      requiresDpo: privacyCenter?.requiresDpo ?? false,
      hasDpoTask: privacyCenter?.hasDpoTask ?? false,
      dpoDoesNotApply: privacyCenter?.dpoOfficerDoesNotApply,

      logoLinkUrl: privacyCenter?.logoLinkUrl,
      faviconUrl: privacyCenter?.faviconUrl,
      disableCaResidencyConfirmation: privacyCenter?.disableCaResidencyConfirmation,
      customFieldEnabled: organization.customFieldEnabled,
      customFieldHelp: organization.customFieldHelp,
      customFieldLabel: organization.customFieldLabel,
      handleFormSubmit: handleFormSubmit,
      handleDoItLaterTask: handleDoItLaterTask,
      handleDpoDoesNotApply: handleDpoDoesNotApply,
      // @ts-ignore
      handleCustomFieldSubmit: handleCustomFieldSubmit,

      includeDpoField: organization.featureGdpr,
    },
    orgRequest,
    privacyCentersRequest,
    privacyCenter,
    updatePrivacyPolicyRequest,
  };
};

type UsePrivacyCenterSettings = {
  settings: PrivacyCenterSettingsProps;
  orgRequest: NetworkRequestState;
  privacyCentersRequest: NetworkRequestState;
  privacyCenter: PrivacyCenterDto | null;
  updatePrivacyPolicyRequest: NetworkRequestState;
};
