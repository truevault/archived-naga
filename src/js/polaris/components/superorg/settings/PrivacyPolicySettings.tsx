import React, { useCallback, useEffect, useMemo, useState } from "react";
import { WYSIWYGEditor } from "../../../../common/components/input/WYSIWYGEditor";
import { useActionRequest, useDataRequest } from "../../../../common/hooks/api";
import { NetworkRequestState } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import {
  AddOrUpdatePrivacyCenterPolicySection,
  PrivacyCenterPolicySectionDto,
} from "../../../../common/service/server/dto/PrivacyCenterDto";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";

interface PrivacyPolicySettingProps {
  privacyCenterId: string;
  updatePrivacyCenterPolicy: (values) => void;
  privacyPolicyHtml: string | null;
  setPrivacyPolicyHtml: (html: string) => void;
  showHelp?: boolean;
}
export const PrivacyPolicySettings: React.FC<PrivacyPolicySettingProps> = ({
  // PrivacyCenter.tsx is taken, when we add component this will be refactored
  privacyCenterId,
  updatePrivacyCenterPolicy,
  privacyPolicyHtml,
  setPrivacyPolicyHtml,
  showHelp = true,
}) => {
  return (
    <>
      {showHelp && (
        <p className="text-body-2">
          The privacy notice we’ll generate from your Data Map will describe the personal
          information your business collects and how you share or disclose personal data. Many
          businesses have existing privacy policy language, such as an introductory paragraph or
          statement, to include at the top of the privacy notice we’ll generate. If you have
          something like that, you can copy and paste your existing language in the text field
          below. You can add, edit, or remove the language at any time. If you do not want to add
          any language at this time, you can leave the text area blank and click “Done” to continue.
        </p>
      )}

      <div className="privacy-notices--privacy-policy-editor">
        {privacyPolicyHtml !== null && (
          <WYSIWYGEditor
            defaultValue={privacyPolicyHtml || ""}
            height="640px"
            onChange={(contents) => {
              setPrivacyPolicyHtml(contents);
              updatePrivacyCenterPolicy({
                privacyCenterId: privacyCenterId,
                noPrivacyPolicy: Boolean(contents),
                contents,
                publish: false,
              });
            }}
            onBlur={(event, contents) => {
              setPrivacyPolicyHtml(contents);
              updatePrivacyCenterPolicy({
                privacyCenterId: privacyCenterId,
                noPrivacyPolicy: Boolean(contents),
                contents,
                publish: true,
              });
            }}
          />
        )}
      </div>
    </>
  );
};

export const usePrivacyPolicySettings = (): UsePrivacyPolicySettings => {
  const organizationId = usePrimaryOrganizationId();
  const [privacyPolicyHtml, setPrivacyPolicyHtml] = useState<string>(null);
  const [privacyPolicySection, setPrivacyPolicySection] =
    useState<PrivacyCenterPolicySectionDto | null>(null);

  const privacyCentersApi = useCallback(
    () => Api.privacyCenter.getPrivacyCenters(organizationId),
    [organizationId],
  );
  const { result: centers, request: privacyCentersRequest } = useDataRequest({
    queryKey: ["privacyCenters", organizationId],
    api: privacyCentersApi,
  });

  const privacyCenters = centers || [];

  const privacyCenter = useMemo(
    () => (privacyCenters.length > 0 ? privacyCenters[0] : undefined),
    [privacyCenters],
  );

  const getPolicyApi = useCallback(
    () => Api.privacyCenter.getPrivacyCenterPolicy(organizationId, privacyCenter?.id),
    [organizationId, privacyCenter?.id],
  );

  const { result: privacyCenterPolicy, request: privacyCenterPolicyRequest } = useDataRequest({
    api: getPolicyApi,
    notReady: !privacyCenter?.id,
    queryKey: ["privacyPolicy", organizationId, privacyCenter?.id],
  });

  useEffect(() => {
    if (privacyPolicySection) {
      setPrivacyPolicyHtml(privacyPolicySection?.body || "");
    }
  }, [privacyPolicySection]);

  const { fetch: updatePrivacyCenterPolicy } = useActionRequest({
    api: ({ privacyCenterId, noPrivacyPolicy, contents, publish }) => {
      return Api.privacyCenter.updatePrivacyCenterPolicySection(
        organizationId,
        privacyCenterId || privacyCenter?.id,
        privacyPolicySection?.id,
        Object.assign({}, privacyPolicySection, {
          body: contents,
          noPrivacyPolicy,
          publish,
        }) as AddOrUpdatePrivacyCenterPolicySection,
      );
    },
  });

  const { fetch: addPrivacyCenterPolicy } = useActionRequest({
    api: ({ privacyCenterId }) =>
      Api.privacyCenter.addPrivacyCenterPolicySection(organizationId, privacyCenterId, {
        name: "Main",
        anchor: "main",
        addToMenu: false,
        noPrivacyPolicy: false,
        body: "",
        publish: false,
      }),
    onSuccess: (result: PrivacyCenterPolicySectionDto) => setPrivacyPolicySection(result),
  });

  //Effects
  useEffect(() => {
    if (privacyCenterPolicyRequest.success && privacyCenterPolicy !== undefined) {
      if (privacyCenterPolicy.policySections.length > 0) {
        setPrivacyPolicySection(privacyCenterPolicy.policySections[0]);
      } else {
        addPrivacyCenterPolicy({ privacyCenterId: privacyCenter?.id });
      }
    }
  }, [privacyCenterPolicyRequest, privacyCenterPolicy]);

  return {
    settings: {
      privacyCenterId: privacyCenterPolicy?.privacyCenterId,
      updatePrivacyCenterPolicy: updatePrivacyCenterPolicy,
      privacyPolicyHtml: privacyPolicyHtml,
      setPrivacyPolicyHtml: setPrivacyPolicyHtml,
    },
    privacyPolicySection,
    privacyCentersRequest,
  };
};

type UsePrivacyPolicySettings = {
  settings: PrivacyPolicySettingProps;
  privacyPolicySection: PrivacyCenterPolicySectionDto | null;
  privacyCentersRequest: NetworkRequestState;
};
