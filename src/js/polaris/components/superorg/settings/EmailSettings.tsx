import React, { useCallback, useMemo } from "react";
import { useActionRequest } from "../../../../common/hooks/api";
import { NetworkRequestState } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import { OrganizationMessageSettingsDto } from "../../../../common/service/server/dto/OrganizationDto";
import { Error as OrganizationError } from "../../../copy/organization";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../../hooks";
import { useOrganizationMailboxes } from "../../../hooks/useOrganization";
import { EmailAccountSettingRow } from "../../organisms/settings/rows/EmailAccountSettingRow";
import { EmailLogoSettingRow } from "../../organisms/settings/rows/EmailLogoSettingRow";
import { EmailSignatureSettingRow } from "../../organisms/settings/rows/EmailSignatureSettingRow";

interface EmailSettingsProps {
  mailbox: string;
  mailboxIsConnected: boolean;
  mailboxOauthUrl: string;
  testMessageRunning: boolean;
  messageSettings: OrganizationMessageSettingsDto;
  sendTestMessage: () => void;
  deleteMailbox: () => void;
  updateMessageSettings: (values) => void;
  updateSignature: (signature: string) => void;
  topDivider?: boolean;
}
export const EmailSettings: React.FC<EmailSettingsProps> = ({
  mailbox,
  mailboxIsConnected,
  mailboxOauthUrl,
  sendTestMessage,
  testMessageRunning,
  deleteMailbox,
  updateMessageSettings,
  messageSettings,
  updateSignature,

  topDivider = false,
}) => {
  return (
    <div>
      {topDivider && <hr className="mb-lg" />}
      <EmailAccountSettingRow
        mailbox={mailbox}
        mailboxIsConnected={mailboxIsConnected}
        mailboxOauthUrl={mailboxOauthUrl}
        sendTestMessage={sendTestMessage}
        testMessageRunning={testMessageRunning}
        deleteMailbox={deleteMailbox}
      />
      <hr className="my-lg" />
      <EmailLogoSettingRow
        headerImageUrl={messageSettings.headerImageUrl}
        updateMessageSettings={updateMessageSettings}
      />
      <hr className="my-lg" />
      <EmailSignatureSettingRow
        signature={messageSettings.messageSignature}
        defaultSignature={messageSettings.defaultMessageSignature}
        onSignatureChange={updateSignature}
      />
    </div>
  );
};

type UseEmailSettings = {
  settings: EmailSettingsProps;
  orgRequest: NetworkRequestState;
  mailboxesRequest: NetworkRequestState;
  refreshMailboxes: () => void;
};
export const useEmailSettings = (): UseEmailSettings => {
  const organizationId = usePrimaryOrganizationId();

  const [organization, orgRequest, orgRefresh] = usePrimaryOrganization();
  const [mailboxes, mailboxesRequest, refreshMailboxes] = useOrganizationMailboxes(organizationId);

  const mailbox = useMemo(() => (mailboxes?.length > 0 ? mailboxes[0] : null), [mailboxes]);

  const updateMessageApi = useCallback(
    (signature: string) => {
      return Api.organization.updateMessageSettings(
        organization.id,
        Object.assign({}, organization.messageSettings, { messageSignature: signature }),
      );
    },
    [organization.id, organization.messageSettings],
  );

  const { fetch: updateMessageSignature } = useActionRequest({
    api: updateMessageApi,
    messages: {
      forceError: OrganizationError.UpdateOrganization,
    },
    onSuccess: () => orgRefresh(),
  });

  const sendTestMessageApi = useCallback(
    () => Api.organization.testMessage(organization.id),
    [organization.id],
  );

  const { fetch: sendTestMessage, request: testMessageRequest } = useActionRequest({
    api: sendTestMessageApi,
    messages: {
      success: "Test message sent",
      forceError: "Test message failed to send",
    },
  });

  const updateMessageSettingsApi = useCallback(
    ({ logoUrl }) =>
      Api.organization.updateMessageSettings(
        organization.id,
        Object.assign({}, organization.messageSettings, { headerImageUrl: logoUrl }),
      ),
    [organization.id, organization.messageSettings],
  );

  const { fetch: updateMessageSettings } = useActionRequest({
    api: updateMessageSettingsApi,
    messages: {
      forceError: OrganizationError.UpdateOrganization,
    },
    onSuccess: () => orgRefresh(),
  });

  const updateSignature = (signature: string) => {
    updateMessageSignature(signature);
  };

  const deleteMailboxApi = useCallback(
    () => Api.organization.deleteOrganizationMailbox(organization.id, mailbox?.mailbox),
    [organization.id, mailbox?.mailbox],
  );

  const { fetch: deleteMailbox } = useActionRequest({
    api: deleteMailboxApi,
    onSuccess: () => {
      orgRefresh();
    },
  });

  const mailboxIsConnected = mailbox && organization.mailbox.mailboxStatus === "OK";

  return {
    settings: {
      mailbox: mailbox?.mailbox,
      mailboxIsConnected: mailboxIsConnected,
      mailboxOauthUrl: organization.mailbox.mailboxOauthUrl,
      messageSettings: organization.messageSettings,
      testMessageRunning: testMessageRequest.running,
      sendTestMessage: sendTestMessage,
      deleteMailbox: deleteMailbox,
      updateMessageSettings: updateMessageSettings,
      updateSignature: updateSignature,
    },
    orgRequest,
    mailboxesRequest,
    refreshMailboxes,
  };
};
