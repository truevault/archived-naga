import React, { useMemo } from "react";
import { FormControlLabel, FormControlLabelProps, Radio, RadioProps } from "@mui/material";
import { InlineLoading } from "../../../common/components/Loading";
import { Disableable } from "../Disableable";
import _ from "lodash";
import clsx from "clsx";

type RadioControlProps = {
  value?: unknown;
  loading?: boolean;
  disabled?: boolean;
  disabledTooltip?: string | React.ReactNode;
  label: string | React.ReactNode;
  recommended?: boolean;
  narrow?: boolean;
  formControlProps?: Partial<FormControlLabelProps>;
  radioProps?: Partial<RadioProps>;
};
export const RadioControlLabel: React.FC<RadioControlProps> = ({
  value,
  loading = false,
  disabled = false,
  disabledTooltip,
  label,
  recommended,
  narrow,
  formControlProps,
  radioProps,
}) => {
  const controlProps = useMemo(() => {
    const controlClasses = clsx({
      highlight: recommended,
      "mr-xs": narrow,
    });
    return _.merge({ className: controlClasses }, formControlProps);
  }, [recommended, narrow, formControlProps]);

  return (
    <Disableable disabled={disabled} disabledTooltip={disabledTooltip}>
      <FormControlLabel
        value={value}
        control={
          loading ? (
            <InlineLoading size="sm" className="mr-sm" style={{ marginLeft: 15 }} />
          ) : (
            <Radio
              color="primary"
              {...radioProps}
              disabled={radioProps?.disabled || disabled || loading}
            />
          )
        }
        label={label}
        {...controlProps}
        disabled={formControlProps?.disabled || disabled || loading}
      />
    </Disableable>
  );
};
