import { InputAdornment, InputAdornmentProps } from "@mui/material";
import React from "react";

export const Adornment: React.FC<InputAdornmentProps> = InputAdornment;
