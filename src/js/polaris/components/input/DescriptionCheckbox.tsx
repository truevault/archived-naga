import {
  Checkbox,
  CheckboxProps,
  FormControlLabel,
  FormControlLabelProps,
  FormGroup,
  FormGroupProps,
  Typography,
  Tooltip,
} from "@mui/material";
import clsx from "clsx";
import React from "react";
import { InlineLoading } from "../../../common/components/Loading";

type DescriptionCheckboxVariant = "light-purple" | "yellow";

export interface DescriptionCheckboxProps extends CheckboxProps {
  label: React.ReactNode;
  description?: React.ReactNode;
  loading?: boolean;
  checkboxClassname?: string;
  formGroupProps?: FormGroupProps;
  formControlProps?: FormControlLabelProps;
  disabled?: boolean;
  disabledTooltip?: string;
  variant?: DescriptionCheckboxVariant;
  afterChange?: () => void;
}

export const DescriptionCheckbox: React.FC<DescriptionCheckboxProps> = ({
  id,
  label,
  description,
  className,
  checkboxClassname,
  formGroupProps = {},
  formControlProps = {},
  disabled,
  disabledTooltip,
  variant,
  loading,
  ...props
}) => {
  const labelContent = (
    <div className="description-checkbox__description">
      <Typography variant="body1" className="description-checkbox__label">
        {label}
        {loading && (
          <span className="ml-md">
            <InlineLoading size="sm" />
          </span>
        )}
      </Typography>
      {description && (
        <Typography
          variant="body2"
          color="textSecondary"
          className="description-checkbox__subtitle"
        >
          {description}
        </Typography>
      )}
    </div>
  );

  const formGroupClasses = clsx(
    "description-checkbox",
    className,
    formGroupProps.className,
    Boolean(description) && "description-checkbox--with-description",
    variant ? `description-checkbox--${variant}` : null,
  );

  const formControlLabelClasses = clsx("description-checkbox__label", formControlProps.className);

  const checkboxClasses = clsx("description-checkbox__checkbox", checkboxClassname);

  let controlLabel = (
    <FormControlLabel
      {...formControlProps}
      disabled={formControlProps?.disabled || disabled || loading}
      className={formControlLabelClasses}
      control={<Checkbox id={id} {...props} className={checkboxClasses} disabled={disabled} />}
      label={labelContent}
    />
  );

  if (disabled && disabledTooltip) {
    controlLabel = <Tooltip title={disabledTooltip}>{controlLabel}</Tooltip>;
  }

  return (
    <FormGroup row {...formGroupProps} className={formGroupClasses}>
      {controlLabel}
    </FormGroup>
  );
};
