import React from "react";
import { Checkbox, CheckboxProps, FormControlLabel, FormControlLabelProps } from "@mui/material";
import { InlineLoading } from "../../../common/components/Loading";

type CheckboxControlProps = {
  loading?: boolean;
  label: string | React.ReactNode;
  checked: boolean;
  onChange: (e: React.ChangeEvent<HTMLInputElement>, checked: boolean) => void;
  formControlProps?: FormControlLabelProps;
  checkboxProps?: CheckboxProps;
};
export const CheckboxControlLabel: React.FC<CheckboxControlProps> = ({
  loading = false,
  label,
  checked,
  onChange,
  formControlProps = {},
  checkboxProps = {},
}) => {
  return (
    <FormControlLabel
      {...formControlProps}
      disabled={formControlProps?.disabled || loading}
      control={
        <Checkbox {...checkboxProps} onChange={onChange} checked={checked} color="primary" />
      }
      label={
        <>
          {label}
          {loading && (
            <span className="ml-md">
              <InlineLoading size="sm" />
            </span>
          )}
        </>
      }
    />
  );
};
