import { FormHelperText } from "@mui/material";
import React from "react";

export const InputHelpText: React.FC<{
  error?: boolean;
  errorMessage?: React.ReactNode;
  helpText?: React.ReactNode;
  hide?: boolean;
}> = ({ error = false, errorMessage = null, helpText = null, hide = false }) =>
  hide ? null : (
    <>
      {errorMessage && error ? (
        <FormHelperText>{errorMessage}</FormHelperText>
      ) : (
        <>{helpText && <FormHelperText>{helpText}</FormHelperText>}</>
      )}
    </>
  );
