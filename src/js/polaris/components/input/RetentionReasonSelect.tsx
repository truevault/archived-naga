import { Select, SelectChangeEvent } from "@mui/material";
import clsx from "clsx";
import React from "react";
import { MultiSelectChips, SimpleSelect } from "../../../common/components/input/Select";
import { OrganizationVendor } from "../../../common/models";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import { RetentionReasonDto } from "../../../common/service/server/dto/RetentionReasonDto";
import { VendorDto } from "../../../common/service/server/dto/VendorDto";

type Props = {
  reasons: RetentionReasonDto[];
  instruction: RequestHandlingInstructionDto;
  vendor: OrganizationVendor | VendorDto;
  onChange: (value: string | string[]) => void;
} & React.ComponentProps<typeof Select>;

export const RetentionReasonSelect: React.FC<Props> = ({
  vendor,
  onChange,
  reasons,
  instruction,
  className,
  ...rest
}) => {
  const value = instruction?.retentionReasons || [];

  const onChangeMultiple = (event: SelectChangeEvent<string[]>) => {
    onChange(event.target.value);
  };

  const renderValue = (selected: string[]) => {
    const selectedReasons = selected
      .map((slug) => reasons?.find((r) => r.slug == slug)?.reason)
      .filter((s) => s);

    const truncatedReasons =
      selectedReasons.length > 2
        ? selectedReasons.slice(0, 2).concat([`And ${selectedReasons.length - 2} More...`])
        : selectedReasons;

    return <MultiSelectChips selectedValues={truncatedReasons} />;
  };

  return (
    <SimpleSelect
      // @ts-ignore
      id={vendor.vendorId || vendor.id}
      onChange={onChangeMultiple}
      placeholder="Select a retention reason"
      value={value}
      multiple
      showChecks
      fullWidth
      displayEmpty={true}
      renderValue={renderValue}
      className={clsx("min-width-256", className)}
      options={reasons.map((r) => ({ value: r.slug, label: r.reason }))}
      {...rest}
    />
  );
};
