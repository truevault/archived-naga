import React, { ReactNode } from "react";

interface StandardLabelProps {
  label: string;
  subLabel?: string;
  helpText?: ReactNode | string;
}

export const StandardLabel: React.FC<StandardLabelProps> = ({
  label,
  subLabel,
  helpText,
}: StandardLabelProps) => {
  return (
    <div className="input-label-container">
      <div className="input-label--label">
        {label}
        <span className="input-label--light">{subLabel}</span>
      </div>
      {helpText && <div className="input-label--help-text">{helpText}</div>}
    </div>
  );
};
