import React, { useEffect } from "react";
import { OrganizationVendor } from "../../../common/models";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import {
  RetentionReason,
  RetentionReasonDto,
} from "../../../common/service/server/dto/RetentionReasonDto";
import { DataRecipientLogo } from "../DataRecipientLogo";
import { Disableable } from "../Disableable";
import { VendorDeletionExceptionSetting } from "../vendor/VendorDeletionExceptionSetting";

type VendorRowProps = {
  vendor: OrganizationDataRecipientDto;
  retentionReasons: RetentionReasonDto[];
  instruction?: RequestHandlingInstructionDto;
  onRadioChange: (
    vendor: OrganizationDataRecipientDto,
    e: any,
    // instruction: RequestHandlingInstructionDto,
  ) => void;
  onReasonChange: (vendor: OrganizationDataRecipientDto, reasons: RetentionReason[]) => void;
  onAffirmationChange: (instruction: RequestHandlingInstructionDto, affirmed: boolean) => void;
  showError?: boolean;
  disabled?: boolean;
  showShortAffirmation?: boolean;
};

export const DeletionExceptionsVendorRow: React.FC<VendorRowProps> = ({
  vendor,
  retentionReasons,
  instruction,
  onRadioChange,
  onReasonChange,
  onAffirmationChange,
  showError = false,
  disabled = false,
  showShortAffirmation = false,
}) => {
  return (
    <>
      <hr />
      <div key={vendor.vendorId} className="py-mdlg flex-row align-items-start">
        <div className="w-256 flex-row align-items-center">
          <DataRecipientLogo size={24} className="mr-md" dataRecipient={vendor} />
          <div className="text-t3 text-weight-bold">{vendor.name}</div>
        </div>

        <div className="flex-grow">
          <VendorDeletionExceptionGroup
            vendor={vendor}
            retentionReasons={retentionReasons}
            instruction={instruction}
            onRadioChange={onRadioChange}
            onReasonChange={onReasonChange}
            onAffirmationChange={onAffirmationChange}
            showError={showError}
            disabled={disabled}
            showShortAffirmation={showShortAffirmation}
          />
        </div>
      </div>
    </>
  );
};

type VendorDeletionExceptionGroupProps = {
  vendor: OrganizationVendor;
  retentionReasons: RetentionReasonDto[];
  instruction?: RequestHandlingInstructionDto;
  onRadioChange: (vendor: OrganizationDataRecipientDto, e: any) => void;
  onReasonChange: (vendor: OrganizationDataRecipientDto, reasons: RetentionReason[]) => void;
  onAffirmationChange: (instruction: RequestHandlingInstructionDto, affirmed: boolean) => void;
  showError?: boolean;
  disabled?: boolean;
  showShortAffirmation?: boolean;
};

const VendorDeletionExceptionGroup: React.FC<VendorDeletionExceptionGroupProps> = ({
  vendor,
  retentionReasons,
  instruction,
  onRadioChange,
  onReasonChange,
  onAffirmationChange,
  showError = false,
  disabled = false,
  showShortAffirmation = false,
}) => {
  const locked = vendor.isPlatform || vendor.isInstalled;

  if (locked && (!instruction || !instruction.processingMethod)) {
    instruction = instruction || ({} as RequestHandlingInstructionDto);
    instruction.processingMethod = "DELETE";
  }

  const defaultRetentionReasons = vendor.defaultRetentionReasons;

  useEffect(() => {
    if (!instruction && defaultRetentionReasons.length > 0) {
      onReasonChange(vendor, defaultRetentionReasons);
    }
    if (!instruction && defaultRetentionReasons.length == 0) {
      onRadioChange(vendor, { target: { value: "DELETE" } });
    }
  }, [instruction, defaultRetentionReasons, onReasonChange, onRadioChange, vendor]);

  const hasError =
    instruction &&
    instruction.processingMethod == "RETAIN" &&
    !instruction.retentionReasons?.length;

  return (
    <Disableable
      disabled={vendor.isPlatform || vendor.isInstalled}
      disabledTooltip="Shopify automatically controls data deletion and retention for apps installed on its platform."
    >
      <VendorDeletionExceptionSetting
        vendor={vendor}
        retentionReasons={retentionReasons}
        instruction={instruction}
        onRadioChange={onRadioChange}
        onReasonChange={onReasonChange}
        onAffirmationChange={onAffirmationChange}
        disabled={disabled || locked}
        error={hasError && showError}
        errorMessage="Please select a retention reason"
        helpText={locked ? null : "At least one retention reason is required if exceptions apply"}
        showShortAffirmation={showShortAffirmation}
      />
    </Disableable>
  );
};
