import React from "react";
import {
  ProgressFooter,
  ProgressFooterActions,
  ProgressFooterSaveNotification,
} from "../../pages/get-compliant/ProgressFooter";
import { StandardPageWrapper } from "../layout/StandardPageWrapper";

import { Routes } from "../../root/routes";
import { PageHeader } from "../layout/header/Header";
import { PageHeaderBack } from "../layout/header/PageHeaderBack";
import {
  DataRetentionModule,
  useDataRetentionModule,
} from "../organisms/data-retention/DataRetentionModule";
import { Paras } from "../typography/Paras";
import { Para } from "../typography/Para";

const Header: React.FC = () => {
  return (
    <PageHeader
      titleContent="Retention Policy"
      titleHeaderContent={<PageHeaderBack to={Routes.privacyCenter.root}>Notices</PageHeaderBack>}
      descriptionContent={
        <Paras>
          <Para>
            Privacy laws allow your business to retain consumer data only as long as necessary to
            serve the purpose for which it was collected. For each category of information you
            collect, your privacy policy must disclose either (a) the period for which your business
            stores personal data, or (b) the criteria used to determine that period.
          </Para>
          <Para>
            In this step, we’ll help you establish your business’s retention period for the data it
            collects.{" "}
          </Para>
        </Paras>
      }
    />
  );
};

export const StayCompliantDataRetentionPage: React.FC<void> = () => {
  const dataRetention = useDataRetentionModule();

  return (
    <StandardPageWrapper
      requests={[
        dataRetention.requests.dataMapRequest,
        dataRetention.requests.dataRetentionRequest,
      ]}
      footer={
        <ProgressFooter
          content={<ProgressFooterSaveNotification request={dataRetention.updateRequest} />}
          actions={
            <ProgressFooterActions
              nextContent="Done"
              nextUrl={Routes.privacyCenter.root}
              nextDisabled={!dataRetention.isFinished}
            />
          }
        />
      }
    >
      <Header />

      <DataRetentionModule {...dataRetention.props} />
    </StandardPageWrapper>
  );
};
