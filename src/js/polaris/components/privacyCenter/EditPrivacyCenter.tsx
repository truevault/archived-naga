import { Add, Info } from "@mui/icons-material";
import { DatePicker } from "@mui/lab";
import { Button, Stack, TextField } from "@mui/material";
import _ from "lodash";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { ExternalLink } from "../../../common/components/ExternalLink";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { useActionRequest, useDataRequest } from "../../../common/hooks/api";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { NetworkRequestState, Organization } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { PrivacyNoticeDto } from "../../../common/service/server/controller/PrivacyNoticeController";
import { CollectionGroupDetailsDto } from "../../../common/service/server/dto/CollectionGroupDto";
import { OrganizationDto } from "../../../common/service/server/dto/OrganizationDto";
import {
  PrivacyCenterDto,
  PrivacyCenterPolicySectionDto,
  UpdatePrivacyCenterPolicy,
} from "../../../common/service/server/dto/PrivacyCenterDto";
import { UUIDString } from "../../../common/service/server/Types";
import { ConfirmDialog } from "../../components/dialog";
import { useDataSubjectTypes, usePrimaryOrganization } from "../../hooks";
import { useFlashQueryParam } from "../../hooks/modalHooks";
import { usePrivacyCenter, usePrivacyPolicy } from "../../hooks/usePrivacyCenter";
import { RouteHelpers, Routes } from "../../root/routes";
import { CONSUMER_INCENTIVES_SURVEY_NAME } from "../../surveys/consumerCollectionIncentivesSurvey";
import {
  isCaApplicantGroup,
  isCaContractorGroup,
  isCaEmployeeGroup,
  isEeaUkEmployeeGroup,
} from "../../util/consumerGroups";
import { PrivacyCenterUrls } from "../../util/privacyCenterUtil";
import { stripTags } from "../../util/text";
import { Callout, CalloutVariant } from "../Callout";
import { CustomSectionCallout, FadedSectionBody, SectionCallout } from "../SectionCallout";
import { Well } from "../Well";

type UseEditPrivacyCenter = {
  requests: {
    privacyCenter: NetworkRequestState;
    privacyPolicy: NetworkRequestState;
    incentivesSurvey: NetworkRequestState;
    notice: NetworkRequestState;
    collectionGroups: NetworkRequestState;
  };
  props: EditPrivacyCenterProps;
  onSave: () => Promise<void>;
};
export const useEditPrivacyCenter = (): UseEditPrivacyCenter => {
  const [org] = usePrimaryOrganization();
  const orgId = org.id;

  const [newSectionId] = useFlashQueryParam<UUIDString | undefined>(
    "new",
    undefined,
    (val: unknown) => val as UUIDString,
  );

  const [remotePrivacyCenter, privacyCenterRequest, privacyCenterRefresh] = usePrivacyCenter(orgId);
  const [remotePrivacyPolicy, privacyPolicyRequest, privacyPolicyRefresh] = usePrivacyPolicy(
    orgId,
    remotePrivacyCenter?.id,
  );
  const [collectionGroups, collectionGroupsRequest] = useDataSubjectTypes(orgId);
  const { answers: incentivesAnswers, surveyRequest: incentivesSurveyRequest } = useSurvey(
    orgId,
    CONSUMER_INCENTIVES_SURVEY_NAME,
  );

  const [localCustomNotices, setLocalCustomNotices] = useState<PrivacyCenterPolicySectionDto[]>([]);
  const [localLastUpdated, setLocalLastUpdated] = useState("");

  const offersFinancialIncetives = incentivesAnswers["offer-consumer-incentives"] === "true";

  const { result: noticeDto, request: noticeRequest } = useDataRequest({
    queryKey: ["notice", "opt-out", orgId],
    api: () => Api.privacyNotice.getOptOutPrivacyNotice(orgId),
  });

  // update local values when remote data is updated
  useEffect(() => {
    if (remotePrivacyPolicy?.policyLastUpdated) {
      setLocalLastUpdated(remotePrivacyPolicy.policyLastUpdated);
    }
  }, [remotePrivacyPolicy?.policyLastUpdated]);

  useEffect(() => {
    if (remotePrivacyPolicy?.policySections) {
      setLocalCustomNotices(remotePrivacyPolicy.policySections);
    }
  }, [remotePrivacyPolicy?.policySections]);

  const { fetch: savePolicy } = useActionRequest({
    api: (values: UpdatePrivacyCenterPolicy) =>
      Api.privacyCenter.updatePrivacyCenterPolicy(orgId, remotePrivacyCenter.id, values),
  });

  const [saving, setSaving] = useState(false);
  const [hasSaved, setHasSaved] = useState(false);

  const handleSave = async () => {
    setSaving(true);
    try {
      const values: UpdatePrivacyCenterPolicy = {
        policyLastUpdated: localLastUpdated,
        policySectionsOrder: _.mapValues(
          _.keyBy(localCustomNotices, (a) => a.id),
          (a) => a.displayOrder,
        ),
      };
      const result = await savePolicy(values);

      await refresh();

      setHasSaved(true);
      return result;
    } finally {
      setSaving(false);
    }
  };

  const newSection = useMemo(() => {
    if (newSectionId) {
      return localCustomNotices.find((n) => n.id == newSectionId);
    }
  }, [newSectionId, localCustomNotices]);

  const dirtyLastUpdatedAt = remotePrivacyPolicy?.policyLastUpdated != localLastUpdated;
  const dirtySectionOrder = useMemo(() => {
    if (remotePrivacyPolicy?.policySections) {
      for (
        let idx = 0;
        idx < localCustomNotices.length && idx < remotePrivacyPolicy.policySections.length;
        idx++
      ) {
        if (
          remotePrivacyPolicy.policySections[idx].displayOrder !=
          localCustomNotices[idx].displayOrder
        ) {
          return true;
        }
      }
      return false;
    }
  }, [remotePrivacyPolicy?.policySections, localCustomNotices]);

  const refresh = async () => {
    const prs = [privacyCenterRefresh(), privacyPolicyRefresh()];
    await Promise.all(prs);
  };

  const deleteSectionApi = useCallback(
    async (sectionId) => {
      await Api.privacyCenter.deletePrivacyCenterPolicySection(
        orgId,
        remotePrivacyCenter?.id,
        sectionId,
      );
    },
    [orgId, remotePrivacyCenter?.id],
  );

  const { fetch: deleteSection } = useActionRequest({ api: deleteSectionApi });

  const handleDeleteSection = async (id: UUIDString) => {
    await deleteSection(id);
    await refresh();
  };

  const jobApplicantsGroup = useMemo(
    () => collectionGroups.find((cg) => isCaApplicantGroup(cg)),
    [collectionGroups],
  );
  const employeeGroup = useMemo(
    () => collectionGroups.find((cg) => isCaEmployeeGroup(cg)),
    [collectionGroups],
  );
  const contractorGroup = useMemo(
    () => collectionGroups.find((cg) => isCaContractorGroup(cg)),
    [collectionGroups],
  );
  const eeaUkEmployeeGroup = useMemo(
    () => collectionGroups.find((cg) => isEeaUkEmployeeGroup(cg)),
    [collectionGroups],
  );

  const builtInNotices = useMemo(() => {
    return getBuiltInNotices(org, remotePrivacyCenter, offersFinancialIncetives);
  }, [org, remotePrivacyCenter, offersFinancialIncetives]);

  const mergedNotices = useMemo(() => {
    return [...builtInNotices, ...localCustomNotices].sort(
      (a, b) => a.displayOrder - b.displayOrder,
    );
  }, [builtInNotices, localCustomNotices]);

  const handleMoveSectionUp = (id) => {
    const local = _.cloneDeep(localCustomNotices);

    const mergedIndex = mergedNotices.findIndex((n) => n.custom == true && n.id == id);
    const localIndex = local.findIndex((n) => n.custom == true && n.id == id);
    // this conditional ensures that we were able to find the identified section
    if (mergedIndex < 0 || localIndex < 0) {
      return;
    }
    // this conditional ensures that the found section is not the first section (which cannot be moved up)
    if (mergedIndex == 0) {
      return;
    }

    const notice = local[localIndex];
    const priorIndex = mergedIndex - 1;
    const prior = mergedNotices[priorIndex];

    if (prior.custom) {
      const priorLocal = local.find((n) => n.custom && n.id == prior.id);
      // swap the two custom display orders
      let tmp = priorLocal.displayOrder;
      priorLocal.displayOrder = notice.displayOrder;
      notice.displayOrder = tmp;
    } else {
      if (priorIndex == 0) {
        notice.displayOrder = 0;
      } else {
        const priorPriorDisplayOrder = mergedNotices[priorIndex - 1].displayOrder;
        notice.displayOrder = priorPriorDisplayOrder + 1;
      }
    }

    setLocalCustomNotices(local);
  };

  const handleMoveSectionDown = (id) => {
    const local = _.cloneDeep(localCustomNotices);

    const mergedIndex = mergedNotices.findIndex((n) => n.custom == true && n.id == id);
    const localIndex = local.findIndex((n) => n.custom == true && n.id == id);
    // this conditional ensures that we were able to find the identified section
    if (mergedIndex < 0 || localIndex < 0) {
      return;
    }
    // this conditional ensures that the found section is not the last section (which cannot be moved down)
    if (mergedIndex >= mergedNotices.length - 1) {
      return;
    }

    const notice = local[localIndex];
    const nextIndex = mergedIndex + 1;
    const next = mergedNotices[nextIndex];

    if (next.custom) {
      const nextLocal = local.find((n) => n.custom && n.id == next.id);
      // swap the two custom display orders
      let tmp = nextLocal.displayOrder;
      nextLocal.displayOrder = notice.displayOrder;
      notice.displayOrder = tmp;
    } else {
      notice.displayOrder = next.displayOrder + 1;

      // if the next section isn't the last section, move all subsequent custom notices before the next built in section *down*
      // by one, so our notice inserts at the top.
      if (nextIndex < mergedNotices.length - 1) {
        // otherwise, we have to increase all *custom* notices before the next built in by 1
        let nextNextIndex = nextIndex + 1;

        while (nextNextIndex <= mergedNotices.length - 1) {
          let nextNext = mergedNotices[nextNextIndex] as CustomNoticeSection;
          if (!nextNext.custom) {
            break;
          }

          const localNextNext = local.find((n) => n.custom && n.id == nextNext.id);
          localNextNext.displayOrder += 1;
          nextNextIndex += 1;
        }
      }
    }

    setLocalCustomNotices(local);
  };

  return {
    requests: {
      privacyCenter: privacyCenterRequest,
      privacyPolicy: privacyPolicyRequest,
      incentivesSurvey: incentivesSurveyRequest,
      notice: noticeRequest,
      collectionGroups: collectionGroupsRequest,
    },
    props: {
      org,
      privacyCenter: remotePrivacyCenter,
      lastUpdatedAt: localLastUpdated,
      notices: mergedNotices,
      newSection,
      optOut: noticeDto,

      jobApplicantsGroup,
      employeeGroup,
      contractorGroup,
      eeaUkEmployeeGroup,
      hasSaved,
      saving,

      dirtyLastUpdatedAt,
      dirtySectionOrder,

      onChangeLastUpdatedAt: (value) => setLocalLastUpdated(value),

      onSave: handleSave,
      onMoveSectionUp: handleMoveSectionUp,
      onMoveSectionDown: handleMoveSectionDown,
      onDeleteSection: handleDeleteSection,
    },
    onSave: handleSave,
  };
};

type EditPrivacyCenterProps = {
  org: OrganizationDto;
  privacyCenter: PrivacyCenterDto;
  lastUpdatedAt: string;
  notices: NoticeSection[];
  newSection: PrivacyCenterPolicySectionDto | undefined;
  optOut: PrivacyNoticeDto;

  jobApplicantsGroup: CollectionGroupDetailsDto | undefined;
  employeeGroup: CollectionGroupDetailsDto | undefined;
  contractorGroup: CollectionGroupDetailsDto | undefined;
  eeaUkEmployeeGroup: CollectionGroupDetailsDto | undefined;

  hasSaved: boolean;
  saving: boolean;

  dirtyLastUpdatedAt: boolean;
  dirtySectionOrder: boolean;
  onChangeLastUpdatedAt: (value: string) => void;

  onSave: () => Promise<void>;
  onMoveSectionUp: (sectionId: string) => void;
  onMoveSectionDown: (sectionId: string) => void;
  onDeleteSection: (sectionId: string) => void;
};
export const EditPrivacyCenter: React.FC<EditPrivacyCenterProps> = ({
  org,
  privacyCenter,
  lastUpdatedAt,
  notices,
  newSection,
  optOut,
  jobApplicantsGroup,
  employeeGroup,
  contractorGroup,
  eeaUkEmployeeGroup,
  hasSaved,
  saving,
  onSave,
  dirtyLastUpdatedAt,
  dirtySectionOrder,
  onChangeLastUpdatedAt,
  onMoveSectionUp,
  onMoveSectionDown,
  onDeleteSection,
}) => {
  const [confirmDeleteId, setConfirmDeleteId] = useState<UUIDString | null>(null);
  if (!privacyCenter) {
    return null;
  }

  const anyEmployment =
    Boolean(jobApplicantsGroup) ||
    Boolean(employeeGroup) ||
    Boolean(contractorGroup) ||
    Boolean(eeaUkEmployeeGroup);

  let builtInOrdinal = 0;
  const renderedNotices = notices.map((notice, idx) => {
    if (!notice.custom) {
      const builtIn = notice as BuiltInNoticeSection;
      builtInOrdinal++;
      return <BuiltInNotice key={builtIn.slug} notice={builtIn} ordinal={builtInOrdinal} />;
    } else {
      const s = notice as PrivacyCenterPolicySectionDto;
      return (
        <CustomSectionCallout
          key={s.id}
          section={s}
          privacyCenter={privacyCenter}
          isNew={s.id == newSection?.id}
          isFirst={idx == 0}
          isLast={idx == notices.length - 1}
          onMoveUp={() => onMoveSectionUp(s.id)}
          onMoveDown={() => onMoveSectionDown(s.id)}
          onDelete={() => setConfirmDeleteId(s.id)}
        />
      );
    }
  });

  const anyDirty = dirtyLastUpdatedAt || dirtySectionOrder;

  const dirtyWarning = (
    <>
      {anyDirty && (
        <Callout
          title="You have unsaved changes."
          variant={CalloutVariant.Yellow}
          icon={Info}
          className="mb-mdlg"
        >
          <div className="flex flex-row justify-space-between">
            <ul>
              {dirtySectionOrder && <li>Changes to section order</li>}
              {dirtyLastUpdatedAt && <li>Changes to ‘Last Updated’ Date</li>}
            </ul>
            <div>
              <LoadingButton loading={saving} variant="contained" color="primary" onClick={onSave}>
                Save and publish changes
              </LoadingButton>
            </div>
          </div>
        </Callout>
      )}
    </>
  );

  return (
    <>
      {dirtyWarning}

      {Boolean(newSection) && !hasSaved && (
        <Callout variant={CalloutVariant.Green} className="mb-mdlg">
          Section <strong>{newSection.name}</strong> successfully added to your privacy policy. You
          can adjust the location of the section by using the arrows.
        </Callout>
      )}

      <Stack spacing={4}>
        <Well>
          <h4 className="mt-md intermediate text-color-primary-dark">Privacy Policy</h4>

          <div className="mb-lg">
            <DatePicker
              label="Last Updated"
              inputFormat="MM/DD/yyyy"
              value={lastUpdatedAt}
              onChange={onChangeLastUpdatedAt}
              renderInput={(params) => <TextField {...params} />}
            />
          </div>

          <Stack spacing={3}>{renderedNotices}</Stack>

          <ConfirmDialog
            open={Boolean(confirmDeleteId)}
            onClose={() => setConfirmDeleteId(null)}
            onConfirm={() => {
              onDeleteSection(confirmDeleteId);
              setConfirmDeleteId(null);
            }}
            confirmTitle="Remove section?"
            contentText="Are you sure you want to remove this section? Clicking ‘yes’ will remove it from your privacy policy."
            confirmText="Yes"
            cancelText="No"
          />
        </Well>

        {Boolean(optOut.label) && (
          <Well variant="light">
            <h4 className="mt-md intermediate text-color-primary-dark">
              Notice of Right to Opt-Out
            </h4>

            <h4 className="intermediate">{optOut.label}</h4>

            <SectionCallout
              title="Introduction"
              variant="white"
              actions={<a href={RouteHelpers.privacyCenter.editPrivacyIntro("opt-out")}>Edit</a>}
            >
              <FadedSectionBody body={stripTags(optOut.notice)} />
            </SectionCallout>
          </Well>
        )}

        {anyEmployment && (
          <Well variant="light">
            <h4 className="mt-md intermediate text-color-primary-dark">HR Notices</h4>

            <p>
              View your{" "}
              <a
                href={RouteHelpers.instructions.id(org.hrInstructionsId)}
                target="_blank"
                rel="noreferrer"
              >
                HR instructions
              </a>{" "}
              for publishing your employment notices below.
            </p>

            <h4 className="intermediate">Employee Notices</h4>

            {Boolean(jobApplicantsGroup) && (
              <>
                <h5>
                  <ExternalLink
                    heavy
                    icon
                    href={PrivacyCenterUrls.getJobApplicantsNoticeUrl(privacyCenter)}
                  >
                    Notice to {jobApplicantsGroup.name}
                  </ExternalLink>
                </h5>
                <SectionCallout
                  title="Introduction"
                  variant="white"
                  actions={
                    <a href={RouteHelpers.privacyCenter.editPrivacyIntro(jobApplicantsGroup.id)}>
                      Edit
                    </a>
                  }
                >
                  <FadedSectionBody body={stripTags(jobApplicantsGroup.privacyNoticeIntroText)} />
                </SectionCallout>
              </>
            )}

            {Boolean(employeeGroup) && (
              <>
                <h5>
                  <ExternalLink
                    heavy
                    icon
                    href={PrivacyCenterUrls.getEmployeeNoticeUrl(privacyCenter)}
                  >
                    Notice to {employeeGroup.name}
                  </ExternalLink>
                </h5>
                <SectionCallout
                  title="Introduction"
                  variant="white"
                  actions={
                    <a href={RouteHelpers.privacyCenter.editPrivacyIntro(employeeGroup.id)}>Edit</a>
                  }
                >
                  <FadedSectionBody body={stripTags(employeeGroup.privacyNoticeIntroText)} />
                </SectionCallout>
              </>
            )}

            {Boolean(contractorGroup) && (
              <>
                <h5>
                  <ExternalLink
                    heavy
                    icon
                    href={PrivacyCenterUrls.getContractorNoticeUrl(privacyCenter)}
                  >
                    Notice to {contractorGroup.name}
                  </ExternalLink>
                </h5>
                <SectionCallout
                  title="Introduction"
                  variant="white"
                  actions={
                    <a href={RouteHelpers.privacyCenter.editPrivacyIntro(contractorGroup.id)}>
                      Edit
                    </a>
                  }
                >
                  <FadedSectionBody body={stripTags(contractorGroup.privacyNoticeIntroText)} />
                </SectionCallout>
              </>
            )}

            {Boolean(eeaUkEmployeeGroup) && (
              <>
                <h5>
                  <ExternalLink
                    heavy
                    icon
                    href={PrivacyCenterUrls.getEeaUkEmployeeNoticeUrl(privacyCenter)}
                  >
                    Notice to {eeaUkEmployeeGroup.name}
                  </ExternalLink>
                </h5>
                <SectionCallout
                  title="Introduction"
                  variant="white"
                  actions={
                    <a href={RouteHelpers.privacyCenter.editPrivacyIntro(eeaUkEmployeeGroup.id)}>
                      Edit
                    </a>
                  }
                >
                  <FadedSectionBody body={stripTags(eeaUkEmployeeGroup.privacyNoticeIntroText)} />
                </SectionCallout>
              </>
            )}
          </Well>
        )}
      </Stack>

      {dirtyWarning}
    </>
  );
};

type BuiltInNoticeProps = {
  notice: BuiltInNoticeSection;
  ordinal: number;
};

const BuiltInNotice: React.FC<BuiltInNoticeProps> = ({ notice, ordinal }) => {
  const headingCopy = `${ordinal}. ${notice.heading}`;
  const head = notice.route ? (
    <ExternalLink href={notice.route} heavy icon>
      {headingCopy}
    </ExternalLink>
  ) : (
    headingCopy
  );
  return (
    <>
      {ordinal == 1 && (
        <div style={{ margin: "auto" }}>
          <Button
            variant="text"
            startIcon={<Add />}
            href={`${Routes.privacyCenter.addPrivacySection}?before=${notice.slug}`}
          >
            Add Section
          </Button>
        </div>
      )}
      <Well variant="light">
        <h4 className="intermediate">{head}</h4>

        <ChildBuiltInNotices notices={notice.childNotices} />
      </Well>
      <div style={{ margin: "auto" }} className="mt-lg">
        <Button
          variant="text"
          startIcon={<Add />}
          href={`${Routes.privacyCenter.addPrivacySection}?after=${notice.slug}`}
        >
          Add Section
        </Button>
      </div>
    </>
  );
};

type ChildBuiltInNoticesProps = {
  notices: BuiltInNoticeSubsection[] | undefined;
};
const ChildBuiltInNotices: React.FC<ChildBuiltInNoticesProps> = ({ notices }) => {
  if (!notices) {
    return null;
  }

  return (
    <>
      {notices.map((notice) => {
        const actions = (
          <>
            {notice.editRoute ? (
              <a href={notice.editRoute} className="text-weight-medium">
                Edit
              </a>
            ) : undefined}
          </>
        );
        return (
          <SectionCallout
            key={notice.heading}
            title={notice.heading}
            actions={actions}
            variant={notice.variant ?? notice.editRoute ? "white" : "dark"}
          >
            <Stack spacing={2}>
              <div>{notice.body}</div>

              <ChildBuiltInNotices notices={notice.childNotices} />
            </Stack>
          </SectionCallout>
        );
      })}
    </>
  );
};

const Faded = ({ body }) => <FadedSectionBody body={stripTags(body)} />;

const getBuiltInNotices = (
  org: Organization,
  privacyCenter: PrivacyCenterDto,
  offersFinancialIncetives: boolean,
): BuiltInNoticeSection[] => {
  if (!privacyCenter) {
    return [];
  }

  const dataMapBody = (
    <>
      This section is updated dynamically when you make changes to your{" "}
      <a href={Routes.dataMap.root} target="_blank" rel="noreferrer">
        Data Map
      </a>
      .
    </>
  );

  const allNotices: BuiltInNoticeSection[] = [
    {
      heading: "Information We Collect",
      slug: "collect",
      displayOrder: 10000,
      custom: false,
      childNotices: [
        { heading: "Information You Provide to Us", body: dataMapBody },
        {
          heading: "Information Collected Automatically",
          body: dataMapBody,
          childNotices: [
            {
              heading: "Cookie Policy",
              editRoute: RouteHelpers.privacyCenter.editPrivacyIntro("cookie"),
              body: <Faded body={privacyCenter.cookiePolicyIntroText} />,
            },
          ],
        },
        { heading: "Information from Other Sources", body: dataMapBody },
        {
          heading: "How Long We Keep Your Data",
          editRoute: Routes.privacyCenter.dataRetention,
          body: <Faded body={privacyCenter.retentionIntroText} />,
        },
      ],
    },
    {
      heading: "How we disclose information",
      slug: "disclose",
      displayOrder: 20000,
      custom: false,
      childNotices: [{ heading: "Data Disclosures", body: dataMapBody }],
    },
    {
      heading: "California Privacy Notice (CCPA)",
      slug: "ccpa",
      displayOrder: 30000,
      custom: false,
      route: PrivacyCenterUrls.getCAPrivacyNoticeUrl(privacyCenter),
      childNotices: [
        {
          heading: "Introduction",
          editRoute: RouteHelpers.privacyCenter.editPrivacyIntro("ccpa"),
          body: <Faded body={privacyCenter.ccpaPrivacyNoticeIntroText} />,
        },
        { heading: "Business and Commercial Purposes for Collection", body: dataMapBody },
        { heading: "Information “Sharing” and “Selling”", body: dataMapBody },
        // { heading: "Notice of Financial Incentive", editRoute: Routes.privacyCenter.financialIncentives, body: loremBody },
      ],
    },
    {
      heading: "Virginia Privacy Notice (VCDPA)",
      slug: "vcdpa",
      displayOrder: 40000,
      custom: false,
      route: PrivacyCenterUrls.getVAPrivacyNoticeUrl(privacyCenter),
      childNotices: [
        {
          heading: "Introduction",
          editRoute: RouteHelpers.privacyCenter.editPrivacyIntro("vcdpa"),
          body: <Faded body={privacyCenter.vcdpaPrivacyNoticeIntroText} />,
        },
      ],
    },
    {
      heading: "Colorado Privacy Notice (CPA)",
      slug: "cpa",
      displayOrder: 50000,
      custom: false,
      route: PrivacyCenterUrls.getCOPrivacyNoticeUrl(privacyCenter),
      childNotices: [
        {
          heading: "Introduction",
          editRoute: RouteHelpers.privacyCenter.editPrivacyIntro("cpa"),
          body: <Faded body={privacyCenter.cpaPrivacyNoticeIntroText} />,
        },
      ],
    },
    {
      heading: "Connecticut Privacy Notice (CTDPA)",
      slug: "ctdpa",
      displayOrder: 60000,
      custom: false,
      route: PrivacyCenterUrls.getCTPrivacyNoticeUrl(privacyCenter),
      childNotices: [
        {
          heading: "Introduction",
          editRoute: RouteHelpers.privacyCenter.editPrivacyIntro("ctdpa"),
          body: <Faded body={privacyCenter.ctdpaPrivacyNoticeIntroText} />,
        },
      ],
    },
    {
      heading: "Notice of Financial Incentives",
      slug: "financial",
      displayOrder: 70000,
      custom: false,
      childNotices: [
        {
          heading: "Incentives",
          editRoute: Routes.privacyCenter.financialIncentives,
          body: <Faded body={org.financialIncentiveDetails} />,
        },
      ],
    },
    {
      heading: "EEA/UK Privacy Notice (GDPR)",
      slug: "gdpr",
      displayOrder: 80000,
      custom: false,
      route: PrivacyCenterUrls.getGDPRNoticeUrl(privacyCenter),
      childNotices: [
        {
          heading: "Introduction",
          editRoute: RouteHelpers.privacyCenter.editPrivacyIntro("gdpr"),
          body: <Faded body={privacyCenter.gdprPrivacyNoticeIntroText} />,
        },
        { heading: "Collection and Disclosure of Personal Data", body: dataMapBody },
        { heading: "Lawful Bases and Legitimate Interests", body: dataMapBody },
        { heading: "International Data Transfers", body: dataMapBody },
      ],
    },
  ];

  const visible: BuiltInNoticeSlug[] = ["collect", "disclose", "ccpa"];
  if (org.featureMultistate) {
    visible.push("vcdpa");
    visible.push("cpa");
    visible.push("ctdpa");
  }
  if (offersFinancialIncetives) {
    visible.push("financial");
  }
  if (org.featureGdpr) {
    visible.push("gdpr");
  }

  return allNotices.filter((n) => visible.includes(n.slug));
};

type BuiltInNoticeSlug =
  | "collect"
  | "disclose"
  | "ccpa"
  | "vcdpa"
  | "cpa"
  | "ctdpa"
  | "financial"
  | "gdpr";
type BuiltInNoticeSection = {
  heading: string;
  slug: BuiltInNoticeSlug;
  displayOrder: number;
  custom: false;

  route?: string;

  childNotices?: BuiltInNoticeSubsection[];
};

type BuiltInNoticeSubsection = {
  heading: string;
  variant?: "dark" | "white";

  body: React.ReactNode;

  editRoute?: string;

  childNotices?: BuiltInNoticeSubsection[];
};

type CustomNoticeSection = PrivacyCenterPolicySectionDto;
type NoticeSection = BuiltInNoticeSection | CustomNoticeSection;
