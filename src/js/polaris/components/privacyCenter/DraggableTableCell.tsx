import React from "react";
import { TableCell } from "@mui/material";

type TableCellProps = {
  align?: "left" | "center" | "right" | "justify" | "inherit";
  children: React.ReactNode;
  isDragOccurring: boolean;
  style?: any;
  width?: any;
};

type TableCellSnapshot = {
  width: number;
  height: number;
};

export class DraggableTableCell extends React.Component<TableCellProps> {
  ref: HTMLElement;
  getSnapshotBeforeUpdate(prevProps: TableCellProps): TableCellSnapshot {
    if (!this.ref) {
      return null;
    }

    const isDragStarting: boolean = this.props.isDragOccurring && !prevProps.isDragOccurring;

    if (!isDragStarting) {
      return null;
    }

    const { width, height } = this.ref.getBoundingClientRect();

    const snapshot: TableCellSnapshot = {
      width,
      height,
    };

    return snapshot;
  }

  componentDidUpdate(_pp: any, _ps: any, snapshot: TableCellSnapshot) {
    const ref: HTMLElement = this.ref;
    if (!ref) {
      return;
    }

    if (snapshot) {
      if (ref.style.width === snapshot.width.toString()) {
        return;
      }
      ref.style.width = `${snapshot.width}px`;
      return;
    }

    if (this.props.isDragOccurring) {
      return;
    }

    // inline styles not applied
    if (ref.style.width == null) {
      return;
    }

    // no snapshot and drag is finished - clear the inline styles
    ref.style.removeProperty("width");
  }

  setRef = (ref: HTMLElement) => {
    this.ref = ref;
  };

  render() {
    return (
      <TableCell {...this.props} ref={this.setRef}>
        {this.props.children}
      </TableCell>
    );
  }
}
