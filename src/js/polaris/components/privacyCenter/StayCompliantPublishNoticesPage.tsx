import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Loading } from "../../../common/components/Loading";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { usePrimaryOrganization, usePrimaryOrganizationId } from "../../hooks";
import {
  ProgressFooter,
  ProgressFooterActions,
  ProgressFooterSaveNotification,
} from "../../pages/get-compliant/ProgressFooter";
import {
  PrivacyNoticeChecklistItems,
  usePrivacyNoticeChecklistItems,
} from "../../pages/stay-compliant/PrivacyNoticeChecklistItems";
import { RouteHelpers, Routes } from "../../root/routes";
import { PageHeader } from "../layout/header/Header";
import { PageHeaderBack } from "../layout/header/PageHeaderBack";
import { Para } from "../typography/Para";
import { Paras } from "../typography/Paras";

const PrivacyNoticeChecklistHeader: React.FC = () => {
  const orgId = usePrimaryOrganizationId();
  const description = (
    <Paras>
      <Para>
        Complete the steps below to update your business’s Privacy Policy links and post privacy
        disclosures on your business’s website and anywhere else it collects information. If you
        need assistance from your teammates,{" "}
        <Link className="text-medium text-link" to={RouteHelpers.organization.users(orgId)}>
          invite them to Polaris
        </Link>{" "}
        or share the link below. Once these instructions are shared with your team, you can click
        “Done.” You do not have to wait until these steps are completed to proceed.
      </Para>
    </Paras>
  );

  return (
    <PageHeader
      titleContent="Publish Notices"
      titleHeaderContent={<PageHeaderBack to={Routes.privacyCenter.root}>Notices</PageHeaderBack>}
      descriptionContent={description}
    />
  );
};

export const StayCompliantPublishNoticesPage: React.FC<void> = () => {
  const [org] = usePrimaryOrganization();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  const { checklistProps, ready } = usePrivacyNoticeChecklistItems(org.devInstructionsId);

  useEffect(() => {
    if (ready) {
      setReadyToRender(true);
    }
  }, [ready]);

  if (!readyToRender) {
    return (
      <StandardPageWrapper>
        <Loading />
      </StandardPageWrapper>
    );
  }

  return (
    <StandardPageWrapper
      footer={
        <ProgressFooter
          content={<ProgressFooterSaveNotification />}
          actions={<ProgressFooterActions nextContent="Done" nextUrl={Routes.privacyCenter.root} />}
        />
      }
    >
      <PrivacyNoticeChecklistHeader />
      {checklistProps?.notices && <PrivacyNoticeChecklistItems {...checklistProps} />}
    </StandardPageWrapper>
  );
};
