import { Button, Checkbox, FormControlLabel, InputAdornment } from "@mui/material";
import { useQueryClient } from "@tanstack/react-query";
import React from "react";
import { Field, Form, useField } from "react-final-form";
import { Text } from "../../../common/components/input/Text";
import { WYSIWYGEditorInput } from "../../../common/components/input/WYSIWYGEditor";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import {
  PrivacyCenterPolicyDto,
  PrivacyCenterPolicySectionDto,
} from "../../../common/service/server/dto/PrivacyCenterDto";
import { OrganizationPublicId } from "../../../common/service/server/Types";
import { Error as InputError } from "../../copy/input";
import { slugify, Validation } from "../../util";

type PrivacyPolicySectionEditorWrapperProps = {
  organizationId: OrganizationPublicId;
  onClose: (sId: string) => void;
  section: any;
  before: any;
  after: any;
  privacyPolicy: any;
  refresh?: () => void;
};

export const PrivacyPolicySectionEditorWrapper: React.FC<
  PrivacyPolicySectionEditorWrapperProps
> = ({ organizationId, onClose, section, privacyPolicy, before, after, refresh = null }) => {
  const qc = useQueryClient();
  const { fetchWithResult } = useActionRequest({
    api: async (values) => {
      if (!section.id) {
        return Api.privacyCenter.addPrivacyCenterPolicySection(
          organizationId,
          privacyPolicy.privacyCenterId,
          { ...values, before, after },
        );
      } else {
        return Api.privacyCenter.updatePrivacyCenterPolicySection(
          organizationId,
          privacyPolicy.privacyCenterId,
          section.id,
          values,
        );
      }
    },
  });

  const formValidation = (values) => {
    const errors = {} as any;

    Validation.requiredString(values, "name", errors);
    Validation.requiredString(values, "anchor", errors);
    Validation.requiredHtmlContent(values, "body", errors);
    validateUniqueAnchor(values, errors);
    validateUniqueName(values, errors);

    return errors;
  };

  const validateUniqueName = (values, errors) => {
    const name = values["name"];
    if (!!name && name !== "" && section.name !== name) {
      const takenNames = privacyPolicy.policySections.map((s) => s?.name);
      if (takenNames.includes(name)) {
        errors["name"] = InputError.MustBeUnique;
      }
    }
  };

  const validateUniqueAnchor = (values, errors) => {
    const anchor = values["anchor"];
    if (!!anchor && anchor !== "" && section.anchor !== anchor) {
      const takenAnchors = privacyPolicy.policySections.map((s) => s?.anchor);
      if (takenAnchors.includes(anchor)) {
        errors["anchor"] = InputError.MustBeUnique;
      }
    }
  };

  const smt = async (values) => {
    const result = await fetchWithResult(values);
    qc.invalidateQueries({ queryKey: ["privacyCenter", organizationId] });
    qc.invalidateQueries({
      queryKey: ["privacyPolicy", organizationId, privacyPolicy.privacyCenterId],
    });
    if (refresh) {
      await refresh();
    }
    onClose(result.id);
  };

  return (
    <Form
      onSubmit={smt}
      initialValues={section}
      validate={formValidation}
      render={({ handleSubmit, submitting, ...rest }) => {
        return (
          <form onSubmit={handleSubmit}>
            <PrivacyPolicySectionEditor
              section={section}
              privacyPolicy={privacyPolicy}
              submitting={submitting}
              onClose={onClose}
              {...rest}
            />
          </form>
        );
      }}
    />
  );
};

const PrivacyPolicySectionEditor = ({
  onClose,
  section,
  submitting,
}: {
  privacyPolicy: PrivacyCenterPolicyDto;
  section: PrivacyCenterPolicySectionDto;
  submitting: boolean;
  onClose: (sId: string) => void;
}) => {
  const nameField = useField("name");
  const anchorField = useField("anchor");
  const addToMenu = useField("addToMenu", { type: "checkbox" });
  const showHeader = useField("showHeader", { type: "checkbox" });

  const isNewSection = !section?.id;

  const handleNameChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const newName = e.target.value;

    // If the anchor is empty or matches what would be generated from the old name, update it as well
    if (isNewSection) {
      anchorField.input.onChange(slugify(newName));
    }
    nameField.input.onChange(newName);
  };

  const handleCancel = () => {
    onClose(section?.id);
  };

  return (
    <div className="flex-col flex--expand-self">
      <div className="flex-row">
        <Text
          className="input--settings-text-narrow"
          field="name"
          label="Section Name"
          required
          onChange={handleNameChange}
        />
        <FormControlLabel
          control={<Checkbox color="primary" {...showHeader.input} />}
          label="Show header"
        />
      </div>
      <div className="flex-row">
        <Text
          className="input--settings-text-narrow"
          field="anchor"
          label="Section Anchor"
          required
          InputProps={{
            startAdornment: <InputAdornment position="start">#</InputAdornment>,
          }}
        />
        <FormControlLabel
          control={<Checkbox color="primary" {...addToMenu.input} />}
          label="Show in navigation"
        />
      </div>

      <div>
        <Field
          name="body"
          render={({ meta }) => {
            return (
              <div className="input--settings-text">
                <WYSIWYGEditorInput field="body" height="640px" />
                {meta.error && (
                  <div className="text-error text-medium mt-xs mb-0">{meta.error}</div>
                )}
              </div>
            );
          }}
        />
      </div>

      <div className="flex-row flex-end">
        <div className="mt-md">
          <Button
            type="button"
            onClick={handleCancel}
            color="primary"
            variant="text"
            disabled={submitting}
          >
            Cancel
          </Button>
        </div>

        <div className="mt-md">
          <LoadingButton type="submit" color="primary" loading={submitting}>
            Save Changes
          </LoadingButton>
        </div>
      </div>
    </div>
  );
};
