import { Button } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { RouteComponentProps, useHistory } from "react-router";
import { WYSIWYGEditor } from "../../../common/components/input/WYSIWYGEditor";
import { useActionRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { PageHeader } from "../../components/layout/header/Header";
import { usePrimaryOrganization } from "../../hooks";
import { useCollectionGroup } from "../../hooks/useCollectionGroups";
import { usePrivacyCenter } from "../../hooks/usePrivacyCenter";
import { LoadingPageWrapper, StandardPageWrapper } from "../layout/StandardPageWrapper";
import { LoadingButton } from "../../../common/components/LoadingButton";

type PrivacyCenterCustomText = {
  title?: string;
  label: string;
  dataField: string;
};
const PRIVACY_CENTER_SECTIONS: Record<string, PrivacyCenterCustomText> = {
  ccpa: { label: "CCPA Privacy Notice", dataField: "ccpaPrivacyNoticeIntroText" },
  gdpr: { label: "GDPR Privacy Notice", dataField: "gdprPrivacyNoticeIntroText" },
  vcdpa: { label: "VCDPA Privacy Notice", dataField: "vcdpaPrivacyNoticeIntroText" },
  cpa: { label: "CPA Privacy Notice", dataField: "cpaPrivacyNoticeIntroText" },
  ctdpa: { label: "CTDPA Privacy Notice", dataField: "ctdpaPrivacyNoticeIntroText" },
  "opt-out": {
    label: "Notice of Right to Opt-Out",
    dataField: "optOutNoticeIntroText",
    title: "Edit Notice of Right to Opt-Out",
  },
  cookie: {
    label: "Cookie Policy",
    dataField: "cookiePolicyIntroText",
    title: "Edit Cookie Policy",
  },
};

type MatchProps = {
  introId: string;
};
export const StayCompliantIntroEditScreen: React.FC<RouteComponentProps<MatchProps>> = ({
  match,
}) => {
  const history = useHistory();
  const introId = match.params.introId.toLowerCase();

  const onDone = () => history.goBack();

  return PRIVACY_CENTER_SECTIONS[introId] ? (
    <PrivacyCenterSectionEdit section={introId} onDone={onDone} />
  ) : (
    <CollectionGroupEdit collectionGroupId={introId} onDone={onDone} />
  );
};

const PrivacyCenterSectionEdit = ({ section, onDone }) => {
  const [org, orgRequest] = usePrimaryOrganization();

  const [everRendered, setEverRendered] = useState(false);

  const [privacyCenter, privacyCenterRequest] = usePrivacyCenter(org?.id);

  const [introText, setIntroText] = useState<string>();

  const config = PRIVACY_CENTER_SECTIONS[section];

  const { fetch: handleUpdate, request } = useActionRequest({
    api: async () => {
      return await Api.privacyCenter.updatePrivacyCenter(org.id, privacyCenter.id, {
        ...privacyCenter,
        ...{ [config.dataField]: introText },
      });
    },
    onSuccess: onDone,
  });

  const defaultText = useMemo(() => privacyCenter[config.dataField] ?? "", [privacyCenter]);

  useEffect(() => {
    setIntroText(defaultText);
  }, [defaultText]);

  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest, privacyCenterRequest)) {
      setEverRendered(true);
    }
  }, [orgRequest, privacyCenterRequest, org]);

  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  return (
    <StandardPageWrapper>
      <PageHeader
        titleContent={config.title ?? "Edit Privacy Notice Introduction"}
        descriptionContent={`Customize the introductory message of the ${config.label}`}
      />{" "}
      <WYSIWYGEditor
        defaultValue={defaultText}
        height="300px"
        onBlur={(event, contents) => {
          setIntroText(contents);
        }}
        onChange={(contents) => {
          setIntroText(contents);
        }}
      />
      <div className="flex-row flex-end">
        <div className="mt-md">
          <Button
            type="button"
            onClick={onDone}
            color="primary"
            variant="text"
            disabled={request.running}
          >
            Cancel
          </Button>
        </div>

        <div className="mt-md">
          <LoadingButton
            type="submit"
            color="primary"
            loading={request.running}
            onClick={handleUpdate}
          >
            Save Changes
          </LoadingButton>
        </div>
      </div>
    </StandardPageWrapper>
  );
};

const CollectionGroupEdit = ({ collectionGroupId, onDone }) => {
  const [org, orgRequest] = usePrimaryOrganization();

  const [collectionGroup, collectionGroupRequest] = useCollectionGroup(org.id, collectionGroupId);

  const [everRendered, setEverRendered] = useState(false);

  const [introText, setIntroText] = useState<string>(collectionGroup?.privacyNoticeIntroText ?? "");

  const { fetch: handleUpdate, request } = useActionRequest({
    api: async (collectionGroupId) => {
      return await Api.consumerGroups.updateCollectionGroup(org.id, collectionGroupId, {
        privacyNoticeIntroText: introText,
      });
    },
    onSuccess: () => {
      onDone();
    },
  });

  useEffect(() => {
    if (NetworkRequest.areFinished(orgRequest, collectionGroupRequest)) {
      setEverRendered(true);
    }
  }, [orgRequest, collectionGroupRequest, org]);

  if (!everRendered) {
    return <LoadingPageWrapper />;
  }

  return (
    <StandardPageWrapper>
      <PageHeader
        titleContent="Edit Employment Notice Introduction"
        descriptionContent={`Customize the introduction language of the notice for ${collectionGroup.name} in your privacy policy`}
      />
      <WYSIWYGEditor
        defaultValue={collectionGroup.privacyNoticeIntroText ?? ""}
        height="300px"
        onBlur={(event, contents) => {
          setIntroText(contents);
        }}
        onChange={(contents) => {
          setIntroText(contents);
        }}
      />
      <div className="flex-row flex-end">
        <div className="mt-md">
          <Button
            type="button"
            onClick={() => onDone()}
            color="primary"
            variant="text"
            disabled={request.running}
          >
            Cancel
          </Button>
        </div>

        <div className="mt-md">
          <LoadingButton
            type="submit"
            color="primary"
            loading={request.running}
            onClick={() => handleUpdate(collectionGroupId)}
          >
            Save Changes
          </LoadingButton>
        </div>
      </div>
    </StandardPageWrapper>
  );
};
