import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { Loading } from "..";
import {
  PrivacyNoticeChecklistItems,
  PublishNoticesTabs,
  usePrivacyNoticeChecklistItems,
} from "../../pages/stay-compliant/PrivacyNoticeChecklistItems";
import { DashboardPageWrapper } from "../layout/DashboardPageWrapper";
import { PageHeader } from "../layout/header/Header";

type UrlParams = {
  id: string;
};

export const PublicPublishNoticesPage: React.FC = () => {
  const { id } = useParams<UrlParams>();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  const { checklistProps, ready } = usePrivacyNoticeChecklistItems(id);

  useEffect(() => {
    if (ready) {
      setReadyToRender(true);
    }
  }, [ready]);

  if (!readyToRender) {
    return (
      <DashboardPageWrapper>
        <Loading />
      </DashboardPageWrapper>
    );
  }

  const type = checklistProps.notices.type;
  const title = type == "dev" ? "Developer Instructions" : "HR Instructions";
  const tabs: PublishNoticesTabs[] =
    type == "dev" ? ["web", "cookie", "mobile"] : ["in-store", "employment"];

  return (
    <DashboardPageWrapper>
      <PageHeader
        titleContent={title}
        descriptionContent="Someone from your team has shared these instructions for posting required notices and links to your website. These steps are required to comply with consumer privacy laws."
      />
      <PrivacyNoticeChecklistItems {...checklistProps} instructions={false} visible={tabs} />
    </DashboardPageWrapper>
  );
};
