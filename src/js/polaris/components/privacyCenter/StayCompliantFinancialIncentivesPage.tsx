import React, { useEffect, useState } from "react";
import { Loading } from "../../../common/components/Loading";
import { StandardPageWrapper } from "../../components/layout/StandardPageWrapper";
import { useSurvey } from "../../../common/hooks/useSurvey";
import { NetworkRequest } from "../../../common/models";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import {
  ProgressFooter,
  ProgressFooterActions,
  ProgressFooterSaveNotification,
} from "../../pages/get-compliant/ProgressFooter";

import { Routes } from "../../root/routes";
import {
  consumerCollectionIncentivesSurvey,
  CONSUMER_INCENTIVES_SURVEY_NAME,
} from "../../surveys/consumerCollectionIncentivesSurvey";
import { isFinished, prepareSurvey } from "../../surveys/survey";
import { Callout, CalloutVariant } from "../Callout";
import { SurveyQuestion } from "../get-compliant/survey/SurveyQuestion";
import { PageHeader } from "../layout/header/Header";
import { PageHeaderBack } from "../layout/header/PageHeaderBack";
import { PrivacyNoticeActions } from "../organisms/privacy-notice/Foo";
import {
  getCopyAction,
  getIncentiveNoticeHtml,
} from "../../pages/stay-compliant/PrivacyNoticeChecklistItems";
import { usePrivacyCenter } from "../../hooks/usePrivacyCenter";

const Header: React.FC = () => {
  return (
    <PageHeader
      titleContent="Financial Incentives"
      titleHeaderContent={<PageHeaderBack to={Routes.privacyCenter.root}>Notices</PageHeaderBack>}
    />
  );
};

export const StayCompliantFinancialIncentivesPage: React.FC<void> = () => {
  const orgId = usePrimaryOrganizationId();

  // State
  const [readyToRender, setReadyToRender] = useState(false);

  const { answers, setAnswer, surveyRequest, updateRequest } = useSurvey(
    orgId,
    CONSUMER_INCENTIVES_SURVEY_NAME,
  );
  const [privacyCenter, privacyCenterRequest] = usePrivacyCenter(orgId);

  const survey = consumerCollectionIncentivesSurvey(answers, false);
  const questions = prepareSurvey(survey, answers);

  const allQuestionsAnswered = isFinished(survey, answers);

  useEffect(() => {
    if (NetworkRequest.areFinished(surveyRequest, privacyCenterRequest)) {
      setReadyToRender(true);
    }
  }, [surveyRequest, privacyCenterRequest]);

  if (!readyToRender) {
    return (
      <StandardPageWrapper>
        <Loading />
      </StandardPageWrapper>
    );
  }

  return (
    <StandardPageWrapper
      footer={
        <ProgressFooter
          content={<ProgressFooterSaveNotification request={updateRequest} />}
          actions={
            <ProgressFooterActions
              nextDisabled={!allQuestionsAnswered}
              nextContent="Done"
              nextUrl={Routes.privacyCenter.root}
            />
          }
        />
      }
    >
      <Header />
      <div>
        {questions.map((q, i) => {
          if (q.slug === "offer-consumer-incentives") {
            return null;
          }
          return (
            <div className="mb-xl" key={i}>
              <SurveyQuestion
                key={q.slug}
                question={q}
                answer={answers[q.slug]}
                onAnswer={(ans) =>
                  setAnswer(q.slug, ans, q.plaintextQuestion ?? (q.question as string))
                }
              />
            </div>
          );
        })}
        {allQuestionsAnswered && answers["offer-consumer-incentives"] != "false" && (
          <>
            <Callout variant={CalloutVariant.Yellow} className="mt-lg">
              <p>
                Post the Financial Incentive link at all points on your website where a consumer can
                opt-in to the financial incentive (or price or service difference) described in your
                Notice of Financial Incentive.{" "}
                <a
                  href="https://help.truevault.com/article/169-where-to-post-your-notice-of-financial-incentive"
                  target="_blank"
                  rel="noreferrer"
                >
                  See tips and examples
                </a>
                .
              </p>
              <PrivacyNoticeActions
                content={
                  <span className="text-t1 text-mono">
                    {getIncentiveNoticeHtml(privacyCenter.url)}
                  </span>
                }
                actions={[
                  getCopyAction(
                    "add-ca-privacy-notice-website-collection-script",
                    "copy HTML",
                    getIncentiveNoticeHtml(privacyCenter.url),
                  ),
                ]}
              />
            </Callout>
          </>
        )}
      </div>
    </StandardPageWrapper>
  );
};
