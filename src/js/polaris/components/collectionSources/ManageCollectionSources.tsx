import React, { useEffect, useMemo, useState } from "react";
import { Api } from "../../../common/service/Api";
import { OrganizationDataSourceDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../../common/service/server/dto/VendorDto";
import { CustomVendorFormValues } from "../../pages/get-compliant/data-recipients/AddDataRecipients";
import { VendorTile } from "../../pages/stay-compliant/data-recipients/VendorTile";
import { useDefaultVendors } from "../../util/resources/vendorUtils";
import { DEFAULT_DATA_SOURCE_NAMES, VendorSearchContents } from "../VendorSearchContents";

type ManageCollectionSourcesProps = {
  orgId: string;
  sources: OrganizationDataSourceDto[];
  catalog: VendorDto[];
  className?: string;
};

export const ManageCollectionSources: React.FC<ManageCollectionSourcesProps> = ({
  orgId,
  sources,
  catalog,
  className,
}) => {
  const [orgSources, setOrgSources] = useState<OrganizationDataSourceDto[]>([]);

  useEffect(() => {
    if (sources) {
      setOrgSources(sources);
    }
  }, [sources, setOrgSources]);

  const handleAddVendor = async (v: string) => {
    const source = await Api.orgDataSource.addOrgDataSource(orgId, { vendorId: v });
    setOrgSources([...orgSources, source]);
  };

  const handleAddCustomVendor = async (fields: CustomVendorFormValues) => {
    const source = await Api.orgDataSource.addOrgDataSource(orgId, {
      custom: fields,
    });
    setOrgSources([...orgSources, source]);
  };

  const handleRemoveVendor = async (v: string) => {
    setOrgSources(orgSources.filter((s) => s.vendorId != v));
    await Api.orgDataSource.removeOrgDataSource(orgId, v);
  };

  return (
    <div className={className}>
      <YourCollectionSources orgSources={orgSources} onRemoveVendor={handleRemoveVendor} />
      <div className="mt-xl">
        <AddAdditionalSources
          catalog={catalog}
          orgSources={orgSources}
          onAddVendor={handleAddVendor}
          onAddCustomVendor={handleAddCustomVendor}
        />
      </div>
    </div>
  );
};

type YourCollectionSourcesProps = {
  orgSources: OrganizationDataSourceDto[];
  onRemoveVendor: (v: string) => void;
};
const YourCollectionSources: React.FC<YourCollectionSourcesProps> = ({
  orgSources,
  onRemoveVendor,
}) => {
  return (
    <>
      <h3 className="mb-xs">Your Collection Sources</h3>

      <div className="add-vendors--search-contents">
        <div className="add-vendors__search-vendor-tiles">
          {orgSources
            .sort((a, b) => a.name.localeCompare(b.name))
            .map((s) => {
              return (
                <div className="mb-xl" key={s.vendorId}>
                  <VendorTile
                    name={s.name}
                    category={s.category}
                    logoUrl={s.logoUrl}
                    added={true}
                    handleRemoveVendor={() => onRemoveVendor(s.vendorId)}
                    variant="click-through"
                    twoWide
                  />
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
};

type AddAdditionalSourcesProps = {
  catalog: VendorDto[];
  orgSources: OrganizationDataSourceDto[];
  onAddVendor: (v: string) => void;
  onAddCustomVendor: (values: CustomVendorFormValues) => void;
};

const AddAdditionalSources: React.FC<AddAdditionalSourcesProps> = ({
  catalog,
  orgSources,
  onAddVendor,
  onAddCustomVendor,
}) => {
  const catalogMap = useMemo(() => catalog && new Map(catalog.map((v) => [v.id, v])), [catalog]);
  const defaultVendors = useDefaultVendors(catalog, DEFAULT_DATA_SOURCE_NAMES) ?? [];
  const sourcesAsSet = useMemo(() => new Set(orgSources.map((s) => s.vendorId)), [orgSources]);

  return (
    <>
      <h3>Add Additional Sources</h3>
      <p className="text-muted mt-0">
        Read more about{" "}
        <a
          href="https://help.truevault.com/article/158-determining-your-collection-sources"
          target="_blank"
          rel="noreferrer noopener"
        >
          determining your collection sources
        </a>
        .
      </p>
      <VendorSearchContents
        allVendorsMap={catalogMap}
        enabledVendors={sourcesAsSet}
        defaultVendors={defaultVendors}
        type="source"
        context={["CONSUMER"]}
        handleAddVendor={onAddVendor}
        handleAddCustomVendor={onAddCustomVendor}
        customModal={{
          header: "Add Source",
          confirmLabel: "Add Source",
          includeUrl: false,
        }}
        variant="click-through"
        twoWide
      />
    </>
  );
};
