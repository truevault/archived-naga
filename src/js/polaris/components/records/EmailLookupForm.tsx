import { Alert, FormGroup } from "@mui/material";
import React, { useState } from "react";
import { Form } from "react-final-form";
import { useHistory } from "react-router";
import { Text } from "../../../common/components/input/Text";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { Organization } from "../../../common/models";
import { Api } from "../../../common/service/Api";
import { RouteHelpers } from "../../root/routes";
import { composeValidators, validateEmail, validateRequired } from "../../util/validation";

export const EmailLookupForm: React.FC<{ org: Organization }> = ({ org }) => {
  const [error, setError] = useState(null);
  const history = useHistory();

  const handleSubmit = async ({ email }) => {
    try {
      const requests = await Api.dataRequest.getRequestsForConsumer(org.id, email);
      if (requests.length > 0) {
        history.push(RouteHelpers.consumer.detail(encodeURIComponent(email)));
      } else {
        setError(`No requests found for ${email}`);
      }
    } catch (err) {
      console.error(error);
      setError(err.message);
    }
  };

  return (
    <>
      {error && (
        <Alert color="error" className="mb-lg">
          {error}
        </Alert>
      )}
      <Form
        onSubmit={handleSubmit}
        render={({ handleSubmit, valid, submitting }) => {
          return (
            <form onSubmit={handleSubmit}>
              <FormGroup className="mb-md">
                <Text
                  field="email"
                  validate={composeValidators(validateRequired, validateEmail)}
                  required
                  type="email"
                  label="Consumer Email"
                />
              </FormGroup>

              <LoadingButton
                loading={submitting}
                type="submit"
                variant="contained"
                disabled={!valid || submitting}
              >
                Search
              </LoadingButton>
            </form>
          );
        }}
      />
    </>
  );
};
