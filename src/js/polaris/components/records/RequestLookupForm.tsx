import { LoadingButton } from "@mui/lab";
import { FormGroup, FormHelperText, FormLabel } from "@mui/material";
import _ from "lodash";
import React, { useMemo } from "react";
import { Form } from "react-final-form";
import { useHistory } from "react-router";
import { Text } from "../../../common/components/input/Text";
import { Select } from "../../../common/components/input/Select";
import { Organization } from "../../../common/models";
import {
  CCPA_TYPE_FILTER_OPTIONS,
  GDPR_TYPE_FILTER_OPTIONS,
  STATE_FILTER_OPTIONS,
} from "../../pages/requests/RequestsTable";
import { RouteHelpers } from "../../root/routes";
import { validateDate } from "../../util/validation";

export const RequestLookupForm: React.FC<{ org: Organization }> = ({ org }) => {
  const history = useHistory();

  const handleSubmit = async (values) => {
    history.push(RouteHelpers.records.requestsResults(values));
  };

  const requestStatusOptions = useMemo(
    () =>
      org.featureGdpr
        ? _.concat([], CCPA_TYPE_FILTER_OPTIONS, GDPR_TYPE_FILTER_OPTIONS)
        : _.concat([], CCPA_TYPE_FILTER_OPTIONS),
    [org],
  );

  return (
    <Form
      onSubmit={handleSubmit}
      render={({ handleSubmit, valid, submitting }) => {
        return (
          <form onSubmit={handleSubmit}>
            <FormGroup className="mb-md">
              <Select options={requestStatusOptions} field="requestType" label="Request Type" />
            </FormGroup>

            <FormGroup className="mb-md">
              <Select options={STATE_FILTER_OPTIONS} field="requestState" label="Request State" />
            </FormGroup>

            <FormGroup className="mb-md">
              <FormLabel className="mb-md">Request Date (optional)</FormLabel>
              <div className="d-flex flex-row align-items-center">
                <Text
                  validate={validateDate}
                  type="date"
                  field="startDate"
                  label="Start Date"
                  fullWidth={false}
                  InputLabelProps={{ shrink: true }}
                  required
                />
                <FormHelperText>to</FormHelperText>
                <Text
                  validate={validateDate}
                  type="date"
                  field="endDate"
                  label="End Date"
                  fullWidth={false}
                  InputLabelProps={{ shrink: true }}
                  required
                />
              </div>
            </FormGroup>

            <LoadingButton
              loading={submitting}
              type="submit"
              variant="contained"
              disabled={!valid || submitting}
            >
              Search
            </LoadingButton>
          </form>
        );
      }}
    />
  );
};
