import { Check } from "@mui/icons-material";
import { CircularProgress } from "@mui/material";
import clsx from "clsx";
import React from "react";

interface Props {
  className?: string | undefined;
  checked: boolean;
  loading?: boolean;
  disabled?: boolean;
  onChange: (newVal: boolean) => void;
}

export const ToggleButton: React.FC<Props> = ({
  className,
  checked,
  onChange,
  children,
  loading = false,
  disabled = false,
}) => {
  disabled = disabled || loading;

  const classes = clsx(`p-md box-rounded d-flex flex-row align-items-center`, className, {
    "cursor-pointer": !disabled,
    "opacity-50": disabled,
    "border-color-neutral-300": !checked || disabled,
    "border-color-primary": checked && !disabled,
    "background-color-neutral-100": disabled,
    "background-color-primary-100": checked && !disabled,
    "background-color-primary-200--hover": checked && !disabled,
    "background-color-neutral-100--hover": !checked && !disabled,
  });

  return (
    <label className={classes} style={{ pointerEvents: disabled ? "none" : undefined }}>
      <input
        className="sr-only"
        type="checkbox"
        checked={checked}
        onChange={disabled ? undefined : (e) => onChange(e.target.checked)}
      />

      {children}

      {loading ? (
        <CircularProgress size={24} color="primary" className="ml-auto" />
      ) : (
        <>{checked && <Check className="color-primary-600 ml-auto" />}</>
      )}
    </label>
  );
};
