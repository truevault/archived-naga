import React from "react";
import { SvgIconProps, Tooltip } from "@mui/material";
import HelpIcon from "@mui/icons-material/Help";
import clsx from "clsx";

type HelpPopoverProps = {
  label: NonNullable<React.ReactNode>;
  className?: string;
  alignMiddle?: boolean;
} & SvgIconProps;

export const HelpPopover: React.FC<HelpPopoverProps> = ({
  label,
  className,
  color = "primary",
  alignMiddle = true,
  ...rest
}) => {
  return (
    <Tooltip title={label}>
      <HelpIcon
        className={clsx(className, "tooltip", { "align-middle": alignMiddle })}
        fontSize="small"
        {...rest}
        color={color}
      />
    </Tooltip>
  );
};
