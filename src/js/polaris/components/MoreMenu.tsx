import React, { ReactNode } from "react";

// import React from 'react';
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import MoreVertIcon from "@mui/icons-material/MoreVert";

type Props = {
  id: string;
  children: (clickClose: (any) => () => void) => ReactNode;
};

// TODO: useMenu once it's available
export const MoreMenu = ({ id, children }: Props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isOpen = Boolean(anchorEl);
  const open = (event) => setAnchorEl(event.currentTarget);
  const close = () => setAnchorEl(null);
  const clickClose = (wrapped) => () => {
    wrapped();
    close();
  };

  return (
    <div className="more-menu">
      <IconButton
        aria-label="more"
        aria-controls={id}
        aria-haspopup="true"
        onClick={open}
        size="large"
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id={id}
        anchorEl={anchorEl}
        keepMounted
        open={isOpen}
        onClose={close}
        className="nav-popover-menu"
      >
        {children(clickClose)}
      </Menu>
    </div>
  );
};
