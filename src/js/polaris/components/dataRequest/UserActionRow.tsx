import React, { ReactNode } from "react";
import { UserDto } from "../../../common/service/server/dto/UserDto";
import { DataRequestEventType } from "../../../common/service/server/Types";
import clsx from "clsx";

type UserActionRowProps = {
  user?: UserDto;
  type?: DataRequestEventType;
  children?: ReactNode;
  isForm?: boolean;
};

export const UserActionRow: React.FC<UserActionRowProps> = ({ type, children, isForm = false }) => {
  const classes = clsx("user-action-row", {
    "user-action-row--message": type == "MESSAGE_RECEIVED",
    "user-action-row--sent": type == "MESSAGE_SENT",
    "user-action-row--form": isForm,
  });

  return (
    <div className={classes}>
      <div className="user-action-row__action">{children}</div>
    </div>
  );
};
