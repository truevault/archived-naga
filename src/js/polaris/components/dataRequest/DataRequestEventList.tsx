import _ from "lodash";
import React, { useMemo } from "react";
import { useSelf } from "../../../common/hooks/useSelf";
import { DataRequestDetail } from "../../../common/models";
import { DataSubjectRequestEventEmbeddedDto } from "../../../common/service/server/dto/DataSubjectRequestEventDto";
import { UserDetailsDto } from "../../../common/service/server/dto/UserDto";
import { Iso8601Date } from "../../../common/service/server/Types";
import { Fmt } from "../../formatters";
import { DATE_FORMAT_MEDIUM } from "../../formatters/date";
import { Callout, CalloutVariant } from "../Callout";
import { UserActionRow } from "./UserActionRow";

type Props = { request: DataRequestDetail; reverse?: boolean };

export const EventList = ({ request, reverse = false }: Props) => {
  const [self] = useSelf();

  // Simple check to determine if closed callout should be shown
  const closedDate = request.closedAt ? new Date(request.closedAt).valueOf() : undefined;
  const showClosedMsg = Boolean(closedDate && Date.now() - closedDate <= 5000);

  const orderedEvents = useMemo(
    () => (reverse ? _.reverse(request.events) : request.events),
    [reverse, request],
  );

  return (
    <>
      {showClosedMsg && (
        <Callout variant={CalloutVariant.Green} className={"mb-std"}>
          <p>The request has been closed and moved to the “Closed” tab.</p>
        </Callout>
      )}

      <div className={"user-action-rows"}>
        {orderedEvents
          .filter((e: DataSubjectRequestEventEmbeddedDto) => e.isUserFacingEvent)
          .map((e: DataSubjectRequestEventEmbeddedDto) => (
            <EventRow key={e.eventId} event={e} self={self} request={request} />
          ))}
      </div>
    </>
  );
};

const fmtDateForRow = (date?: Iso8601Date, relative = false) => {
  return relative ? Fmt.Date.fmtDateFromNow(date) : Fmt.Date.fmtDate(date, DATE_FORMAT_MEDIUM);
};

const showDeclaration = ({
  event,
  request,
}: {
  event: DataSubjectRequestEventEmbeddedDto;
  request: DataRequestDetail;
}) =>
  event.eventType === "CREATED" &&
  request.dataRequestType.type === "CCPA_RIGHT_TO_KNOW" &&
  request.selfDeclarationProvided;

const EventRow = ({
  event,
  self,
  request,
}: {
  event: DataSubjectRequestEventEmbeddedDto;
  self: UserDetailsDto;
  request: DataRequestDetail;
}) => {
  let message = event.message;

  if (event.eventType === "CREATED") {
    message = undefined;
  }

  return (
    <UserActionRow user={event.actor} type={event.eventType}>
      <div className="user-action-row__header">
        <div className="user-action-row__type">{event.eventTypeTitle}</div>
        <div className={"user-action-row__identity"}>
          {event.isRequesterAction
            ? Fmt.DataSubject.fmtFullName(request)
            : Fmt.User.fmtFullName(event.actor, self)}
        </div>
        <div className={"user-action-row__date"}>{fmtDateForRow(event.eventDate)}</div>
      </div>
      {event.eventType != "REQUESTOR_CONTACTED" && (
        <div className={"user-action-row__desc"}>{event.eventTypeDescription}</div>
      )}
      {showDeclaration({ event, request }) && (
        <>
          <div className={"user-action-row__declaration_signature"}>
            Declaration electronically signed at{" "}
            <strong>{Fmt.Date.fmtDate(event.createdAt, Fmt.Date.DATE_TIME_FORMAT_YMD)}</strong>
          </div>
          <blockquote className={"user-action-row__declaration"}>
            &ldquo;{request.dataRequestType.declarationText}&rdquo;
          </blockquote>
        </>
      )}
      {message && (
        <div
          className="user-action-row__message"
          dangerouslySetInnerHTML={{
            __html: message,
          }}
        />
      )}
    </UserActionRow>
  );
};
