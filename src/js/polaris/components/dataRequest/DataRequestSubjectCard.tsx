import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { Chip } from "@mui/material";
import Button from "@mui/material/Button";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Grow from "@mui/material/Grow";
import MenuItem from "@mui/material/MenuItem";
import MenuList from "@mui/material/MenuList";
import Paper from "@mui/material/Paper";
import Popper from "@mui/material/Popper";
import moment from "moment";
import React, { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { DataSubjectRequestDetailDto } from "../../../common/service/server/dto/DataSubjectRequestDto";
import { Iso8601Date } from "../../../common/service/server/Types";
import { Error as RequestsError } from "../../copy/requests";
import { Fmt } from "../../formatters";
import { DATE_FORMAT_MEDIUM } from "../../formatters/date";
import { fmtLongRequestType, fmtStateShort } from "../../formatters/request";
import { EditDataRequestDialog } from "../dialog/EditDataRequestDialog";
import { ExtendDataRequestDialog } from "../dialog/ExtendDataRequestDialog";
import { DueDate } from "../DueDateCard";

type DataRequestSubjectCardProps = {
  request: DataSubjectRequestDetailDto;
  primaryOrgId: string;
  className?: string;
};

export const fmtDateForRow = (date?: Iso8601Date, relative = false) => {
  return relative ? Fmt.Date.fmtDateFromNow(date) : Fmt.Date.fmtDate(date, DATE_FORMAT_MEDIUM);
};

export const daysRemaining = (deadlineMoment) => daysBetween(moment(), deadlineMoment);

const daysBetween = (startMoment, endMoment) => Math.round(endMoment.diff(startMoment, "days"));

export const SUBMIT_FIELD_NAMES = [
  "requestDate",
  "subjectEmailAddress",
  "subjectMailingAddress",
  "subjectPhoneNumber",
  "subjectFirstName",
  "subjectLastName",
  "dataRequestTypeId",
  "submissionMethod",
  "notice",
];

export const SubjectCard = ({
  request,
  primaryOrgId,
  className = "",
}: DataRequestSubjectCardProps) => {
  const [showEditDialog, setShowEditDialog] = useState(false);
  const [showExtendDialog, setShowExtendDialog] = useState(false);
  const [submitValues, setSubmitValues] = useState<any>(null);
  const [submitExtendValues, setSubmitExtendValues] = useState(null);

  const history = useHistory();

  const [openMenu, setOpenMenu] = useState(false);
  const anchorRef = useRef<HTMLDivElement | null>(null);

  const handleSubmit = (values) => {
    setSubmitValues(Object.assign({}, values));
  };

  const handleExtendSubmit = (values) => {
    setSubmitExtendValues(Object.assign({}, values));
  };

  const handleCloseMenu = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpenMenu(false);
  };

  const { fetch: triggerUpdate, request: updateRequestState } = useActionRequest({
    api: () => {
      const existingValues = Object.fromEntries(
        Object.entries(request).filter(([k]) => SUBMIT_FIELD_NAMES.includes(k)),
      );
      const processedSubmitValues = Object.fromEntries(
        Object.entries(submitValues)
          .filter(([k]) => SUBMIT_FIELD_NAMES.includes(k))
          .map(([k, v]) => {
            return k === "requestDate" && typeof (v as any).toISOString == "function"
              ? [k, (v as any).toISOString()]
              : [k, v];
          }),
      );
      const values = Object.assign({}, existingValues, processedSubmitValues) as any;
      return Api.dataRequest.updateRequest(primaryOrgId, request.id, values);
    },
    messages: { defaultError: RequestsError.UpdateRequest },
  });

  const { request: extendRequestState, fetch: triggerExtend } = useActionRequest({
    api: async () => {
      const values = Object.assign({}, submitExtendValues) as any;
      return Api.dataRequest.extendRequest(primaryOrgId, request.id, values);
    },
    onSuccess: () => window.location.reload(),
  });

  // make the request whenever submit values are set
  useEffect(() => {
    if (submitValues) {
      triggerUpdate();
      setSubmitValues(null);
    }
  }, [triggerUpdate, submitValues]);

  useEffect(() => {
    if (submitExtendValues) {
      triggerExtend();
      setSubmitExtendValues(null);
    }
  }, [triggerExtend, submitExtendValues]);

  // Only allow extension menu when NOT an Opt-Out and hasn't been extended.

  const showEditLink =
    request.submissionSource === "POLARIS" &&
    !request.isAutocreated &&
    (request.state == "VERIFY_CONSUMER" || request.state == "REMOVE_CONSUMER");

  const showExtendMenu = request.dataRequestType.canExtend;
  request.dataRequestType.canExtend &&
    request.state != "CLOSED" &&
    !request.events.some((e) => e.eventType === "REQUEST_DEADLINE_UPDATED");

  const showDueDate = request.state !== "CLOSED";

  return (
    <div className={`request-details-info-card ${className}`}>
      <div className={"text-t5 text-weight-medium "}>
        {fmtLongRequestType(request.dataRequestType.type)}

        {request.isDuplicate ? (
          <Chip
            clickable
            onClick={() => history.push(`/requests/${request.duplicatesRequestId}`)}
            className="ml-sm"
            label="DUPLICATE"
            color="warning"
          />
        ) : null}
        {request.context == "EMPLOYEE" ? (
          <Chip label="HR" color="secondary" className="ml-sm" />
        ) : undefined}
        <span className="ml-sm color-neutral-400">
          {fmtStateShort(request.dataRequestType.regulationName)}
        </span>
      </div>
      <div className="text-t2">{Fmt.DataSubject.fmtFullName(request)}</div>
      {showEditLink && (
        <>
          <Link
            className={"request-edit"}
            onClick={() => {
              setShowEditDialog(true);
            }}
            to={"#"}
          >
            Edit request
          </Link>

          <EditDataRequestDialog
            request={request}
            open={showEditDialog}
            onClose={() => {
              setShowEditDialog(false);
            }}
            onDone={() => {
              setShowEditDialog(false);
            }}
            updateRequestState={updateRequestState}
            handleSubmit={handleSubmit}
          />
        </>
      )}
      <div className="text-t2">
        {request.id} {request.isGpcOptOut && <>(submitted via GPC)</>}
      </div>
      <div className={"request-date-fields"} ref={anchorRef}>
        {showDueDate && <DueDate date={request.dueDate} condensed />}
        {showExtendMenu && (
          <>
            <Button
              variant="contained"
              onClick={() => {
                setOpenMenu(true);
              }}
            >
              <ArrowDropDownIcon />
            </Button>
            <ExtendDataRequestDialog
              request={request}
              open={showExtendDialog}
              onClose={() => {
                setShowExtendDialog(false);
              }}
              onDone={() => {
                setShowExtendDialog(false);
              }}
              updateRequestState={extendRequestState}
              handleSubmit={handleExtendSubmit}
            />
          </>
        )}
      </div>

      <Popper
        open={openMenu}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        placement="bottom-end"
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin: placement === "bottom" ? "center top" : "center bottom",
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleCloseMenu}>
                <MenuList id="split-button-menu" className="split-button-popover-menu">
                  <MenuItem
                    key={"handleDueDate"}
                    onClick={() => {
                      setShowExtendDialog(true);
                      setOpenMenu(false);
                    }}
                  >
                    Extend Request Due Date
                  </MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
};
