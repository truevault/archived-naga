import React from "react";
import { ChevronRight, ExpandMore } from "@mui/icons-material";
import { Collapse, IconButton, Typography } from "@mui/material";
import clsx from "clsx";
import { useState } from "react";
import { DataRequestDetail } from "../../../common/models";
import { EventList } from "./DataRequestEventList";
import { Fmt } from "../../formatters";

type Props = { request: DataRequestDetail; className?: string; startOpen?: boolean };

export const DataRequestEventCard: React.FC<Props> = ({
  request,
  className,
  startOpen = false,
}) => {
  const [open, setOpen] = useState(startOpen);

  const ExpandIcon = open ? ExpandMore : ChevronRight;

  return (
    <section className={clsx(className, "data_request_event_card")}>
      <header className="data_request_event_card__header d-flex flex-row mb-md">
        <div>
          <Typography className={"text-t5 text-weight-bold "}>
            {request.dataRequestType.name} on {Fmt.Date.fmtDate(request.requestDate)}
          </Typography>

          <Typography className="text-t2">{Fmt.DataSubject.fmtFullName(request)}</Typography>
          <Typography className="text-t2">
            <code>{request.id}</code>
          </Typography>
        </div>

        <div className="ml-auto">
          <IconButton color="primary" onClick={() => setOpen(!open)}>
            <ExpandIcon />
          </IconButton>
        </div>
      </header>

      <Collapse in={open}>
        <div className="ml-lg">
          <EventList request={request} />
        </div>
      </Collapse>
    </section>
  );
};
