import React from "react";
import { DataRequestDetail } from "../../../../common/models";

import { EventList } from "../DataRequestEventList";
import { NoWorkflowRequestSteps } from "./steps/NoWorkflowRequestSteps";
import { ObjectionRequestSteps } from "./steps/ObjectionRequestSteps";
import { PortabilityRequestSteps } from "./steps/PortabilityRequestSteps";
import { RectificationRequestSteps } from "./steps/RectificationRequestSteps";
import { RequestToDeleteSteps } from "./steps/RequestToDeleteSteps";
import { RequestToKnowSteps } from "./steps/RequestToKnowSteps";
import { RequestToOptOutSteps } from "./steps/RequestToOptOutSteps";
import { WithdrawConsentRequestSteps } from "./steps/WithdrawConsentRequestSteps";
import { RequestToLimitSteps } from "./steps/RequestToLimitSteps";

type Props = {
  request: DataRequestDetail;
};

export const DataRequestBody: React.FC<Props> = ({ request }) => {
  if (request.state == "CLOSED") {
    return <EventList request={request} reverse={true} />;
  }

  switch (request.dataRequestType.workflow) {
    case "DELETE":
      return <RequestToDeleteSteps request={request} />;
    case "OPT_OUT":
      return <RequestToOptOutSteps request={request} />;
    case "KNOW":
      return <RequestToKnowSteps request={request} />;
    case "PORTABILITY":
      return <PortabilityRequestSteps request={request} />;
    case "RECTIFICATION":
      return <RectificationRequestSteps request={request} />;
    case "OBJECTION":
      return <ObjectionRequestSteps request={request} />;
    case "WITHDRAW_CONSENT":
      return <WithdrawConsentRequestSteps request={request} />;
    case "LIMIT":
      return <RequestToLimitSteps request={request} />;
    case "NONE":
    default:
      return <NoWorkflowRequestSteps request={request} />;
  }
};
