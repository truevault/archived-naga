import _ from "lodash";
import React, { createContext, useContext, useEffect, useMemo, useState } from "react";
import { DataRequestDetail, NetworkRequestState } from "../../../../common/models";
import { DataSubjectRequestDetailDto } from "../../../../common/service/server/dto/DataSubjectRequestDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorOutcomeEmbeddedDto } from "../../../../common/service/server/dto/VendorOutcomeDto";
import { UpdateVendorOutcomeMutation } from "../../../hooks/useRequests";

type SubjectStatus = "NOT_FOUND" | "FOUND" | "PROCESSED_AS_SERVICE_PROVIDER";

type DataRequestContext = {
  steps: RequestStep[];
  activeStep: number;
  lastStep: boolean;
  request: DataRequestDetail;
  refreshRequest: () => void;
  mutations: {
    updateVendorOutcomeMutation: UpdateVendorOutcomeMutation;
  };
  consumerGroups: {
    subjectStatus: SubjectStatus;
    setSubjectStatus: (status: SubjectStatus) => void;
    selected: string[];
    setSelected: (selected: string[]) => void;
  };
  vendors: {
    all: OrganizationDataRecipientDto[];
    vendorOutcomes: VendorOutcomeEmbeddedDto[];
  };
  attachment: {
    files?: File[];
    fileError?: string;
    addFiles: (file: File[]) => void;
    removeFile: (index: number) => void;
  };
  stepCompleted: boolean;
  setStepCompleted: (x: boolean) => void;
  updateRequests: NetworkRequestState[];
  setUpdateRequests: (...x: NetworkRequestState[]) => void;
};

const noOp = () => {
  /* NO OP */
};

const DEFAULT_STATE: DataRequestContext = {
  steps: [],
  activeStep: -1,
  lastStep: false,
  request: null,
  refreshRequest: noOp,
  mutations: {
    updateVendorOutcomeMutation: {} as any,
  },
  consumerGroups: {
    subjectStatus: "NOT_FOUND",
    setSubjectStatus: noOp,
    selected: [],
    setSelected: noOp,
  },
  vendors: {
    all: [],
    vendorOutcomes: [],
  },
  attachment: {
    files: [],
    fileError: undefined,
    addFiles: noOp,
    removeFile: noOp,
  },
  stepCompleted: false,
  setStepCompleted: noOp,
  updateRequests: [],
  setUpdateRequests: noOp,
};

const DataRequestProcessingContext = createContext(DEFAULT_STATE);

export const useDataRequestProcessingContext = () =>
  useContext<DataRequestContext>(DataRequestProcessingContext);

const stepStatus = (name: string, step: string, history: string[]) => {
  if (step === name) {
    return "current";
  }

  if (step.endsWith("(not required)")) {
    return "skipped";
  }

  return history.includes(name) ? "completed" : "skipped";
};

// TODO: This is beginning to become unruly, we should consider better data modeling here
const noContactRequiredDeletionSteps = ["verify consumer", "delete info", "notify consumer"];
const contactRequiredDeletionSteps = [
  "verify consumer",
  "delete info",
  "contact vendors",
  "notify consumer",
];
const contactNoLongerRequiredDeletionSteps = [
  "verify consumer",
  "delete info",
  "contact vendors\n(not required)",
  "notify consumer",
];
const optOutSteps = ["remove consumer", "notify consumer"];
const rightToKnowSteps = ["verify consumer", "retrieve data", "send data"];
const rectificationSteps = ["verify consumer", "correct data", "notify consumer"];
const objectionSteps = ["verify consumer", "evaluate objection", "notify consumer"];
const consentSteps = ["verify consumer", "remove consent", "notify consumer"];
const limitSteps = ["verify consumer", "limit data", "contact vendors", "notify consumer"];

const generateSteps = (
  steps: string[],
  request: DataSubjectRequestDetailDto | null,
): RequestStep[] => {
  if (!request) {
    return [];
  }
  const step = request.state as string;
  const history = (request.stateHistory as string[]).map((s) =>
    s.toLowerCase().replaceAll("_", " "),
  );

  return steps.map((name) => ({
    name,
    status: stepStatus(name, step.toLowerCase().replaceAll("_", " "), history),
  }));
};

function generateDeletionSteps(request: DataSubjectRequestDetailDto): RequestStep[] {
  if (request.contactVendorsHidden && !request.contactVendorsEverShown) {
    return generateSteps(noContactRequiredDeletionSteps, request);
  } else if (request.contactVendorsHidden && request.contactVendorsEverShown) {
    return generateSteps(contactNoLongerRequiredDeletionSteps, request);
  }

  // Our default state is an assumption that we require contacting
  // the vendor; we should always fall back to these steps
  return generateSteps(contactRequiredDeletionSteps, request);
}

const useRequestSteps = (request: DataSubjectRequestDetailDto | null): RequestStep[] => {
  return useMemo(() => {
    if (!request || !request.dataRequestType?.workflow) return [];

    switch (request.dataRequestType.workflow) {
      case "DELETE":
        return generateDeletionSteps(request);
      case "OPT_OUT":
        return generateSteps(optOutSteps, request);
      case "KNOW":
      case "PORTABILITY":
        return generateSteps(rightToKnowSteps, request);
      case "RECTIFICATION":
        return generateSteps(rectificationSteps, request);
      case "OBJECTION":
        return generateSteps(objectionSteps, request);
      case "WITHDRAW_CONSENT":
        return generateSteps(consentSteps, request);
      case "LIMIT":
        return generateSteps(limitSteps, request);
      case "NONE":
        return generateSteps([], request);
    }
  }, [request]);
};

interface RequestStep {
  name: string;
  status: "current" | "skipped" | "completed" | "incomplete";
}

type Props = {
  request: DataRequestDetail;
  vendors: OrganizationDataRecipientDto[];
  refreshRequest: () => void;
  updateVendorOutcomeMutation: UpdateVendorOutcomeMutation;
};

export const DataRequestProcessingProvider: React.FC<Props> = ({
  children,
  request,
  vendors,
  refreshRequest,
  updateVendorOutcomeMutation,
}) => {
  const [cgSubjectStatus, setCgSubjectStatus] = useState<SubjectStatus>("NOT_FOUND");
  const [cgSelected, setCgSelected] = useState<string[]>([]);

  const [vendorOutcomes, setVendorOutcomes] = useState<VendorOutcomeEmbeddedDto[]>([]);

  const [attachmentFiles, setAttachmentFiles] = useState<File[]>([]);
  const [attachmentError, setAttachmentError] = useState<string | undefined>(undefined);
  const [stepCompleted, setStepCompleted] = useState(false);
  const [updateRequests, setUpdateRequests] = useState<NetworkRequestState[]>([]);

  useEffect(() => {
    setVendorOutcomes(_.sortBy(request.vendorOutcomes, (v) => v.vendor.name));
  }, [request.vendorOutcomes]);

  useEffect(() => {
    setCgSubjectStatus(request.consumerGroupResult || "NOT_FOUND");
  }, [request.consumerGroupResult]);

  useEffect(() => {
    setCgSelected(request.consumerGroups);
  }, [request.consumerGroups]);

  const steps = useRequestSteps(request);

  const activeStep =
    request && request.state === "CLOSED"
      ? steps.length
      : steps.findIndex((s) => s.status === "current");

  const lastStep = steps.length > 0 && activeStep === steps.length - 1;

  const addFiles = (files?: File[]) => {
    if (_.isNil(files) || files.length < 1) {
      return;
    }

    files.forEach((file) => {
      if (!file) {
        return;
      }

      if (file && file.size > 15000000) {
        setAttachmentError("Any one file cannot be larger than 15MB");
      } else {
        setAttachmentError(undefined);
        setAttachmentFiles((files) => [...files, file]);
      }
    });
  };

  const removeFile = (index: number) => {
    setAttachmentFiles((files) => files.filter((_, i) => i !== index));
  };

  const value: DataRequestContext = {
    request,
    refreshRequest,
    mutations: {
      updateVendorOutcomeMutation,
    },
    consumerGroups: {
      subjectStatus: cgSubjectStatus,
      setSubjectStatus: setCgSubjectStatus,
      selected: cgSelected,
      setSelected: setCgSelected,
    },
    vendors: {
      all: vendors,
      vendorOutcomes,
    },
    attachment: {
      files: attachmentFiles,
      fileError: attachmentError,
      addFiles,
      removeFile,
    },
    steps,
    activeStep,
    lastStep,
    stepCompleted,
    setStepCompleted,
    updateRequests,
    setUpdateRequests: (...x: NetworkRequestState[]) => setUpdateRequests(x),
  };

  return (
    <DataRequestProcessingContext.Provider value={value}>
      {children}
    </DataRequestProcessingContext.Provider>
  );
};
