import React from "react";
import { DataRequestDetail } from "../../../../../common/models";
import { RemoveConsentStep } from "./rectification/RemoveConsentStep";
import { NotifyConsumer } from "./shared/NotifyConsumer";
import { VerifyConsumer } from "./shared/VerifyConsumer";

type Props = {
  request: DataRequestDetail;
};

export const WithdrawConsentRequestSteps: React.FC<Props> = ({ request }) => {
  switch (request.state) {
    case "VERIFY_CONSUMER":
      return <VerifyConsumer />;
    case "REMOVE_CONSENT":
      return <RemoveConsentStep />;
    case "NOTIFY_CONSUMER":
      return <NotifyConsumer />;
  }
  return null;
};
