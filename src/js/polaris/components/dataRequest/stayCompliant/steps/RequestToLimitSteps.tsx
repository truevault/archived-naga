import React from "react";
import { DataRequestDetail } from "../../../../../common/models";
import { LimitDataStep } from "./limit/LimitDataStep";
import { NotifyConsumer } from "./shared/NotifyConsumer";
import { VerifyConsumer } from "./shared/VerifyConsumer";
import { ContactVendors } from "./limit/ContactVendors";

type Props = {
  request: DataRequestDetail;
};

export const RequestToLimitSteps: React.FC<Props> = ({ request }) => {
  switch (request.state) {
    case "VERIFY_CONSUMER":
      return <VerifyConsumer />;
    case "LIMIT_DATA":
      return <LimitDataStep />;
    case "CONTACT_VENDORS":
      return <ContactVendors />;
    case "NOTIFY_CONSUMER":
      return <NotifyConsumer />;
  }
  return null;
};
