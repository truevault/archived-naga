import React from "react";
import { DataRequestDetail } from "../../../../../common/models";
import { PortabilityRetrieveInfo } from "./know/PortabilityRetrieveInfo";
import { SendData } from "./know/SendData";
import { VerifyConsumer } from "./shared/VerifyConsumer";

type Props = {
  request: DataRequestDetail;
};

export const PortabilityRequestSteps: React.FC<Props> = ({ request }) => {
  switch (request.state) {
    case "VERIFY_CONSUMER":
      return <VerifyConsumer />;
    case "RETRIEVE_DATA":
      return <PortabilityRetrieveInfo />;
    case "SEND_DATA":
      return (
        <SendData
          instructions={
            "The message below will be sent to the consumer. Attach the files you retrieved in the last step. After the message is sent, close the request."
          }
        />
      );
  }
  return null;
};
