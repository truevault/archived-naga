import React from "react";
import { DataRequestDetail } from "../../../../../common/models";
import { EvaluateObjectionStep } from "./objection/EvaluateObjectionStep";
import { NotifyConsumer } from "./shared/NotifyConsumer";
import { VerifyConsumer } from "./shared/VerifyConsumer";

type Props = {
  request: DataRequestDetail;
};

export const ObjectionRequestSteps: React.FC<Props> = ({ request }) => {
  switch (request.state) {
    case "VERIFY_CONSUMER":
      return <VerifyConsumer />;
    case "EVALUATE_OBJECTION":
      return <EvaluateObjectionStep />;
    case "NOTIFY_CONSUMER":
      return <NotifyConsumer />;
  }
  return null;
};
