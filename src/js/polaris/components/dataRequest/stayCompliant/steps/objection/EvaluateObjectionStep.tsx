import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { useActionRequest } from "../../../../../../common/hooks/api";
import { NetworkRequest } from "../../../../../../common/models";
import { Api } from "../../../../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorOutcomeOption } from "../../../../../../common/service/server/Types";
import { useAuthorizations } from "../../../../../hooks/useAuthorizations";
import { usePrimaryOrganization } from "../../../../../hooks/useOrganization";
import { useOrganizationVendors } from "../../../../../hooks/useOrganizationVendors";
import { Routes } from "../../../../../root/routes";
import { alphabetically } from "../../../../../surveys/util";
import { Abbr } from "../../../../Abbr";
import { Callout, CalloutVariant } from "../../../../Callout";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";
import { RetainedVendorRow } from "../delete/RetainedVendorRow";
import { VendorDeleteRow } from "../delete/VendorDeleteRow";
import { useSurvey } from "../../../../../../common/hooks/useSurvey";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../../../../pages/get-compliant/data-recipients/shared";
import { isIntentionalDisclosure, isRequestContextApplicable } from "../../../../../util/vendors";
import { useDataMap } from "../../../../../hooks/useDataMap";

export type VendorOutcomeHandler = (
  vendor: OrganizationDataRecipientDto,
  outcome: VendorOutcomeOption,
) => void;

export type VendorOutcomeCollection = Record<string, string>;

export const EvaluateObjectionStep = () => {
  const [org, orgRequest] = usePrimaryOrganization();
  const [allVendors, vendorsRequest] = useOrganizationVendors(org.id, undefined, true);
  const { request, refreshRequest, setStepCompleted } = useDataRequestProcessingContext();
  const [vendorOutcomes, setVendorOutcomes] = useState<VendorOutcomeCollection | undefined>(
    undefined,
  );
  const [readyToRender, setReadyToRender] = useState(false);

  const { authorizations, request: authorizationsRequest } = useAuthorizations(org.id);

  const api = useCallback(
    async ({ values, resetState }) => {
      try {
        const resp = await Api.dataRequest.updateVendor(org.id, request.id, values);

        if (resp.id == undefined) {
          resetState();
        }
        refreshRequest();
      } catch (err) {
        resetState();
        throw err;
      }
    },
    [org.id, refreshRequest, request.id],
  );

  const { fetch: updateVendorOutcome } = useActionRequest({
    api,
    allowConcurrent: true,
    onError: refreshRequest,
  });

  const onSelectionChange: VendorOutcomeHandler = (vendor, value) => {
    const resetState = () =>
      setVendorOutcomes((prev) => Object.assign({}, prev, { [vendor.id]: vendorOutcomes }));

    setVendorOutcomes((prev) => Object.assign({}, prev, { [vendor.vendorId]: value }));

    updateVendorOutcome({
      values: { vendor: vendor.vendorId, outcome: value },
      resetState: resetState,
    });
  };

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(vendorsRequest, orgRequest, authorizationsRequest)) {
      setReadyToRender(true);
    }
  }, [orgRequest, vendorsRequest, authorizationsRequest]);

  const { answers } = useSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME);
  const [dataMap] = useDataMap(org.id);

  const vendors = useMemo(() => {
    return allVendors?.filter((v) => {
      const instruction = v.instructions.find((vi) => vi.requestType == "DELETE");
      const instructionOk = instruction?.processingMethod != "INACCESSIBLE_OR_NOT_STORED";
      const contextOk = isRequestContextApplicable(dataMap, request.context, v);

      return instructionOk && contextOk && !isIntentionalDisclosure(answers, v);
    });
  }, [allVendors, request.context, answers, dataMap]);

  const installedVendors = useMemo(() => {
    return vendors?.filter((v) => !v.isInstalled)?.sort(alphabetically) ?? [];
  }, [vendors]);

  const deleteVendors = useMemo(() => {
    return installedVendors.filter(
      (v) =>
        !v.instructions.some(
          (i) =>
            i.requestType == "DELETE" &&
            i.processingMethod === "RETAIN" &&
            !i.hasRetentionExceptions,
        ),
    );
  }, [installedVendors]);

  const retainVendors = useMemo(() => {
    return installedVendors.filter((v) =>
      v.instructions.some((i) => i.requestType == "DELETE" && i.processingMethod === "RETAIN"),
    );
  }, [installedVendors]);

  useEffect(() => {
    const outcomes = request.vendorOutcomes;
    const everyOutcomeCompleted = deleteVendors.every((vendor) => {
      const outcome = outcomes?.find((o) => o.vendor.id == vendor.vendorId);
      return outcome?.outcome != null || vendor.isInstalled === true;
    });

    setStepCompleted(
      Boolean(deleteVendors) && deleteVendors.length <= outcomes.length && everyOutcomeCompleted,
    );
  }, [setStepCompleted, deleteVendors, request.vendorOutcomes, vendors]);

  // TODO: This should not be a use effect; why is this a use effect
  useEffect(() => {
    const vendorOutcomes = request.vendorOutcomes.reduce((acc, outcome) => {
      return Object.assign({}, acc, { [outcome.vendor.id]: outcome.outcome });
    }, {});
    setVendorOutcomes((prev) => Object.assign({}, prev, vendorOutcomes));
  }, [request, vendors]);

  if (!readyToRender) return null;

  return (
    <>
      <div className="text-t3 mb-std">
        To fulfill an Objection to Processing request, you are required to evaluate the consumer’s
        objections and stop processing their data unless you can demonstrate legitimate, overriding
        interests. If you need to retain data to fulfill a contract, like a customer order, you can
        process data for that purpose. Otherwise, businesses typically handle this like they would
        an Erasure Request.
      </div>

      {Boolean(request.objectionRequest) && (
        <Callout variant={CalloutVariant.Clear} className="mb-xl" title="Subject's Objection">
          {request.objectionRequest}
        </Callout>
      )}

      {vendorOutcomes &&
        deleteVendors &&
        deleteVendors.map((vendor) => (
          <VendorDeleteRow
            key={vendor.id}
            vendor={vendor}
            vendorOutcomes={vendorOutcomes}
            authorizations={authorizations}
            onSelectionChange={onSelectionChange}
            org={org}
            request={request}
          />
        ))}

      <div className="text-t5 text-weight-medium mb-md"> Data Retention </div>
      <div className="text-t3 mb-mdlg">
        The systems below are marked for retention based on one or more{" "}
        <Abbr
          className="text-decoration-underline"
          title="Your business may retain personal data under certain exceptions, such as for internal use and to comply with a legal obligation."
        >
          <strong>exceptions to deletion</strong>
        </Abbr>
        . Do not delete data stored with these Data Recipients if it is kept under an exception to
        deletion. Data retention settings can be updated in your{" "}
        <Link to={`${Routes.dataMap.root}?tab=data_recipients`}>Data Map</Link>.
      </div>
      {retainVendors &&
        retainVendors.map((vendor) => <RetainedVendorRow key={vendor.id} vendor={vendor} />)}
    </>
  );
};
