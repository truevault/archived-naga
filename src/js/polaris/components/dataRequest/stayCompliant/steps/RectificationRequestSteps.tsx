import React from "react";
import { DataRequestDetail } from "../../../../../common/models";
import { CorrectDataStep } from "./rectification/CorrectDataStep";
import { NotifyConsumer } from "./shared/NotifyConsumer";
import { VerifyConsumer } from "./shared/VerifyConsumer";

type Props = {
  request: DataRequestDetail;
};

export const RectificationRequestSteps: React.FC<Props> = ({ request }) => {
  switch (request.state) {
    case "VERIFY_CONSUMER":
      return <VerifyConsumer />;
    case "CORRECT_DATA":
      return <CorrectDataStep />;
    case "NOTIFY_CONSUMER":
      return <NotifyConsumer />;
  }
  return null;
};
