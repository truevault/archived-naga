import { Email as EmailIcon, Person } from "@mui/icons-material";
import { FormControl, FormControlLabel, Radio, RadioGroup } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import { useActionRequest, useDataRequest } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { Fmt } from "../../../../../formatters";
import { usePrimaryOrganization } from "../../../../../hooks/useOrganization";
import { Callout, CalloutVariant } from "../../../../Callout";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";

export const VerifyConsumer = () => {
  const [org] = usePrimaryOrganization();

  const instructionsApi = useCallback(
    () => Api.requestHandlingInstructions.getCGInstructions(org.id),
    [org.id],
  );

  const { result: fetchedInstructions } = useDataRequest({
    queryKey: ["instructions", org.id],
    api: instructionsApi,
  });

  const { request, refreshRequest, setStepCompleted } = useDataRequestProcessingContext();
  const [subjectStatus, setSubjectStatus] = useState<string>(request?.consumerGroupResult ?? "");

  const fullName = Fmt.DataSubject.fmtFullName(request);
  const email = request.subjectEmailAddress;
  const { customFieldEnabled, customFieldLabel } = org;
  const customFieldValue = request.organizationCustomInput;

  const { fetch: updateStatus } = useActionRequest({
    api: async (args = {}) => {
      const { status, resetState } = args;
      try {
        const resp = await Api.dataRequest.setSubjectStatus(org.id, request.id, {
          subjectStatus: status,
        });
        if (resp.id == undefined) {
          resetState();
        }
        refreshRequest();
      } catch (err) {
        resetState();
        throw err;
      }
    },
    onError: refreshRequest,
  });

  const onSelectionChange = (e) => {
    const status = e.target.value;
    const resetState = () => {
      const currentStatus = subjectStatus;
      setSubjectStatus(() => currentStatus);
    };

    setSubjectStatus(status);

    updateStatus({ status: status, resetState: resetState });
  };

  const everContacted = request.requestorContacted || request.vendorContacted;

  useEffect(() => {
    setStepCompleted(!!request.consumerGroupResult);
  }, [request, setStepCompleted]);

  const personMarginBottom = fetchedInstructions ? "mb-std" : "mb-md";

  useEffect(() => {
    setSubjectStatus(request.consumerGroupResult ?? "");
  }, [request.consumerGroupResult]);

  return (
    <>
      {request.emailVerified && (
        <Callout icon={EmailIcon} variant={CalloutVariant.Green} className="mb-std w-640">
          <p>
            {`${fullName}'s`} email has been verified{" "}
            {request.emailVerifiedAutomatically && "by Polaris"}
            {request.emailVerifiedManually && "by your organization"}.
          </p>
        </Callout>
      )}

      <div className="text-t3 mb-lg">Does your business have data about this consumer?</div>

      {customFieldEnabled && (
        <Callout
          icon={Person}
          variant={CalloutVariant.LightPurple}
          className={`${personMarginBottom} w-640`}
        >
          <p className="mt-0 mb-xs">Name: {fullName}</p>
          <p className="mt-0 mb-xs">Email: {email}</p>

          <p className="mt-0 mb-xs">
            {customFieldLabel}: {customFieldValue ?? "Not Provided"}
          </p>
        </Callout>
      )}

      {Boolean(fetchedInstructions) && (
        <Callout variant={CalloutVariant.Clear} className={"mb-std"}>
          <p>{fetchedInstructions}</p>
        </Callout>
      )}

      <FormControl component="fieldset">
        <RadioGroup
          aria-label="Verify Consumer Options"
          name="verify-consumer-options"
          onChange={onSelectionChange}
          value={subjectStatus}
        >
          <FormControlLabel
            value="FOUND"
            control={<Radio />}
            label={<span className={"text-t2"}>Yes, we have data about them</span>}
            className={"mb-mdlg"}
            disabled={everContacted}
          />
          <FormControlLabel
            control={<Radio />}
            value="NOT_FOUND"
            label={<span className={"text-t2"}>No, I can't find a record of them</span>}
            className={"mb-mdlg"}
            disabled={everContacted}
          />
          {org?.isServiceProvider && (
            <FormControlLabel
              value="PROCESSED_AS_SERVICE_PROVIDER"
              control={<Radio />}
              label={<span className={"text-t2"}>Processed as a Service Provider</span>}
              className={"mb-mdlg"}
              disabled={everContacted}
            />
          )}
        </RadioGroup>
      </FormControl>
    </>
  );
};
