import React, { useEffect } from "react";
import { useApiTrigger } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { Error as RequestsError } from "../../../../../copy/requests";
import { Fmt } from "../../../../../formatters";
import { usePrimaryOrganization } from "../../../../../hooks";
import { useClosedMessageTemplate } from "../../../../../hooks/useTemplates";
import { Callout, CalloutVariant } from "../../../../Callout";
import { EmailButton } from "../../../../organisms/EmailButton";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";

export const NotifyConsumer = () => {
  const [org] = usePrimaryOrganization();
  const { request, refreshRequest, setStepCompleted } = useDataRequestProcessingContext();

  const [template] = useClosedMessageTemplate(org.id, request.id);

  const fullName = Fmt.DataSubject.fmtFullName(request);

  const [contactRequestor] = useApiTrigger(
    () => Api.dataRequest.contactRequestor(org.id, request.id),
    refreshRequest,
    RequestsError.TransitionProcessing,
  );

  useEffect(() => {
    setStepCompleted(!!request.requestorContacted);
  }, [request, setStepCompleted]);

  return (
    <>
      <div className="text-t3 mb-std">The message below will be sent to the consumer.</div>
      {request.requestorContacted && (
        <div className="w-640">
          <Callout variant={CalloutVariant.Green} className="mb-std">
            Done! Message successfully sent to {fullName}. You can now close this request.
          </Callout>
        </div>
      )}
      {!request.requestorContacted && (
        <>
          <Callout variant={CalloutVariant.Gray} className="mb-md">
            <div className="text-t3">
              <p>
                <strong>To: {request.subjectEmailAddress}</strong>
              </p>
              <p>
                <strong>Subject: A message about your request [{request.id}]</strong>
              </p>
              <div dangerouslySetInnerHTML={{ __html: template }} />
            </div>
          </Callout>
          <EmailButton
            color="primary"
            variant="contained"
            className="btn-md mb-std"
            onClick={contactRequestor}
          >
            Send
          </EmailButton>
        </>
      )}
    </>
  );
};
