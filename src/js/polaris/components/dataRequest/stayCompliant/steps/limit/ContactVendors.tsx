import React, { useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { useApiTrigger } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { ContactVendorRequest } from "../../../../../../common/service/server/dto/UpdateVendorRequest";
import { Error as RequestsError } from "../../../../../copy/requests";
import { usePrimaryOrganization } from "../../../../../hooks";
import { useContactVendorsMessageTemplate } from "../../../../../hooks/useTemplates";
import { alphabetically } from "../../../../../surveys/util";
import { Callout, CalloutVariant } from "../../../../Callout";
import { VendorEmailDisplayDialog } from "../../../../dialog/VendorEmailDisplayDialog";
import { EmailButton } from "../../../../organisms/EmailButton";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../../../../pages/get-compliant/data-recipients/shared";
import { useSurvey } from "../../../../../../common/hooks/useSurvey";
import { isIntentionalDisclosure, isRequestContextApplicable } from "../../../../../util/vendors";
import { useDataMap } from "../../../../../hooks/useDataMap";

export const ContactVendors = () => {
  const [org] = usePrimaryOrganization();
  const {
    request,
    refreshRequest,
    setStepCompleted,
    vendors: { all: vendors },
  } = useDataRequestProcessingContext();

  const [template] = useContactVendorsMessageTemplate(org.id, request.id);

  const [notificationModalOpen, setNotificationModalOpen] = useState(false);

  const closeModal = () => setNotificationModalOpen(false);

  const [updateVendorOutcome] = useApiTrigger(
    (values: ContactVendorRequest) => Api.dataRequest.contactVendors(org.id, request.id, values),
    refreshRequest,
    RequestsError.TransitionProcessing,
  );

  const { answers } = useSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME);
  const [dataMap] = useDataMap(org.id);

  const filteredVendors = useMemo(() => {
    const collectedCategories =
      dataMap?.collection
        .filter((x) => {
          if (request.context === "EMPLOYEE") {
            return x.collectionGroupType === "EMPLOYMENT";
          }
          return x.collectionGroupType !== "EMPLOYMENT";
        })
        .flatMap((x) => x.collected) ?? [];
    const sensitivePI =
      dataMap?.categories.filter(
        (x) => x.surveyClassification === "SENSITIVE" && collectedCategories.includes(x.id),
      ) ?? [];
    const sensitivePIVendorSet = sensitivePI.reduce((acc, cur) => {
      dataMap.disclosure.forEach((d) => {
        if (d.disclosed.includes(cur.id)) {
          acc.add(d.vendorId);
        }
      });
      return acc;
    }, new Set());
    return vendors
      ?.filter(
        (v) =>
          !v.isInstalled ||
          (request.dataRequestType.regulationName === "GDPR" &&
            v.gdprProcessorSetting === "Processor"),
      )
      .filter(
        (v) =>
          sensitivePIVendorSet.has(v.vendorId) &&
          isRequestContextApplicable(dataMap, request.context, v) &&
          !isIntentionalDisclosure(answers, v),
      );
  }, [vendors, answers, dataMap, request.context]);

  const contactVendorsList = useMemo(
    () =>
      filteredVendors
        //   ?.filter((v) => {
        //     const vendorOutcome = request.vendorOutcomes.find((x) => x.vendor.id === v.vendorId);
        //     const outcome = vendorOutcome?.outcome;
        //     const instruction = v.instructions.find((vi) => vi.requestType == "DELETE");

        //     const noDeletionInstructions = !v.deletionInstructions;
        //     const noDoNotContact = !instruction?.deleteDoNotContact;
        //     const noDataNotFound = outcome !== "NO_DATA";
        //     const noDataRetained = outcome !== "DATA_RETAINED";

        //     return noDeletionInstructions && noDoNotContact && noDataNotFound && noDataRetained;
        //   })
        .sort(alphabetically),
    [filteredVendors, request.vendorOutcomes],
  );

  const noSendVendorList = contactVendorsList?.filter((v) => !v.email && !v.defaultEmail) || [];

  const sendVendorList = useMemo(
    () =>
      contactVendorsList?.filter(
        (v) =>
          (v.email || v.defaultEmail) &&
          (request.vendorOutcomes.find((x) => x.vendor.id === v.vendorId && !x.processed) ||
            request.vendorOutcomes.every((x) => x.vendor.id !== v.vendorId)),
      ),
    [request, contactVendorsList],
  );
  const completedVendorList =
    contactVendorsList?.filter(
      (v) =>
        (v.email || v.defaultEmail) &&
        request.vendorOutcomes.find((x) => x.vendor.id === v.vendorId && x.processed),
    ) || [];

  useEffect(() => {
    if (sendVendorList) {
      setStepCompleted(sendVendorList.length === 0);
    }
  }, [sendVendorList, setStepCompleted]);

  if (!vendors) return null;

  const dispatchEmail = () => {
    const values = { vendors: sendVendorList.map((v) => v.vendorId) };
    updateVendorOutcome(values);
  };

  return (
    <>
      <div className="text-t3 mb-std">
        To fulfill your legal obligation, the message below will be emailed to your Data Recipients.
      </div>

      {completedVendorList.length > 0 && (
        <div className="w-640">
          <Callout variant={CalloutVariant.Green} className="mb-std">
            <p>
              Message successfully sent to{" "}
              <strong>{completedVendorList.map((v) => v.name).join(", ")}.</strong>
            </p>
          </Callout>
        </div>
      )}

      {sendVendorList.length > 0 && (
        <>
          <Callout variant={CalloutVariant.Gray} className="mb-md">
            <div className="text-t3">
              <p>
                <strong>To:</strong>{" "}
                <Link to="#" onClick={() => setNotificationModalOpen(true)}>
                  See Recipients
                </Link>
              </p>
              <p>
                <strong>
                  Subject: {request.dataRequestType.regulationName}{" "}
                  {request.dataRequestType.longName}
                </strong>
              </p>

              <div dangerouslySetInnerHTML={{ __html: template }} />
            </div>
          </Callout>

          <EmailButton
            color="primary"
            variant="contained"
            className="btn-md mb-std"
            onClick={dispatchEmail}
          >
            Send
          </EmailButton>
        </>
      )}

      {noSendVendorList.length > 0 &&
        (noSendVendorList.length != contactVendorsList.length ? (
          <div className="w-896">
            <Callout variant={CalloutVariant.Yellow}>
              <p>
                The notification cannot be sent to the following Data Recipients because there is no
                email address added for them:{" "}
                <strong>{noSendVendorList.map((v) => v.name).join(", ")}.</strong>
              </p>
            </Callout>
          </div>
        ) : (
          <div className="w-896">
            <Callout variant={CalloutVariant.Yellow}>
              <p>
                The notification cannot be sent because none of your Data Recipients can be
                contacted. Add email addresses for the following Data Recipients in{" "}
                <Link to="/organization/requestHandling/requestToDelete">Request Instructions</Link>
                , then return to this step to preview and send the notification:{" "}
                <strong>{noSendVendorList.map((v) => v.name).join(", ")}.</strong>
              </p>
            </Callout>
          </div>
        ))}

      <VendorEmailDisplayDialog
        open={notificationModalOpen}
        onClose={closeModal}
        vendors={sendVendorList}
      />
    </>
  );
};
