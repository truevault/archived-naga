import { Checkbox, FormControlLabel } from "@mui/material";
import React, { useCallback, useEffect } from "react";
import { useActionRequest } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { usePrimaryOrganization } from "../../../../../hooks/useOrganization";
import { Callout, CalloutVariant } from "../../../../Callout";
import { SUBMIT_FIELD_NAMES } from "../../../DataRequestSubjectCard";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";
import { useDataMap } from "../../../../../hooks/useDataMap";

export const LimitDataStep = () => {
  const [org] = usePrimaryOrganization();
  const [dataMap] = useDataMap(org.id);

  const { request, refreshRequest, setStepCompleted } = useDataRequestProcessingContext();

  const collectedCategories =
    dataMap?.collection
      .filter((x) => {
        if (request.context === "EMPLOYEE") {
          return x.collectionGroupType === "EMPLOYMENT";
        }
        return x.collectionGroupType !== "EMPLOYMENT";
      })
      .flatMap((x) => x.collected) ?? [];
  const sensitivePI =
    dataMap?.categories
      .filter((x) => x.surveyClassification === "SENSITIVE" && collectedCategories.includes(x.id))
      .sort((a, b) => a.picGroupOrder - b.picGroupOrder || a.order - b.order) ?? [];

  const api = useCallback(
    ({ request, finished }) => {
      const existingValues = Object.fromEntries(
        Object.entries(request).filter(([k]) => SUBMIT_FIELD_NAMES.includes(k)),
      );
      const values = Object.assign({}, existingValues, { limitationFinished: finished }) as any;
      return Api.dataRequest.updateRequest(org.id, request.id, values);
    },
    [request],
  );

  const { fetch: updateField } = useActionRequest({ api });

  const handleSelectionUpdated = async (isFinished: boolean) => {
    await updateField({ request, finished: isFinished });
    refreshRequest();
  };

  useEffect(() => {
    setStepCompleted(request.limitationFinished ?? false);
  }, [request.limitationFinished]);

  return (
    <>
      <div className="text-t3 mb-std">
        Your business is required to stop using this consumer's Sensitive Personal Information for
        any purposes other than providing products or services requested by the consumer, and for
        internal record-keeping purposes, such as to fulfill legal and compliance obligations. Check
        the box below when you've made any necessary changes to your data processing about this
        consumer.
      </div>

      <Callout
        variant={CalloutVariant.Clear}
        title="Based on your Data Map, your business handles the following Sensitive Personal Information:"
        className="mb-md"
      >
        <ul>
          {sensitivePI.map((pi) => (
            <li key={pi.id}>{pi.name}</li>
          ))}
        </ul>
      </Callout>

      <FormControlLabel
        control={
          <Checkbox
            checked={request.limitationFinished ?? false}
            onChange={({ target }) => handleSelectionUpdated(target.checked)}
          />
        }
        label="We've limited our use of this consumer's Sensitive Personal Information to uses that are necessary to provide products or services to the consumer, or for legal/compliance obligations."
      />
    </>
  );
};
