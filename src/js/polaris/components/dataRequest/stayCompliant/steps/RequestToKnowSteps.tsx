import React from "react";
import { DataRequestDetail } from "../../../../../common/models";
import { RetrieveInfo } from "./know/RetrieveInfo";
import { SendData } from "./know/SendData";
import { VerifyConsumer } from "./shared/VerifyConsumer";

type Props = {
  request: DataRequestDetail;
};

export const RequestToKnowSteps: React.FC<Props> = ({ request }) => {
  switch (request.state) {
    case "VERIFY_CONSUMER":
      return <VerifyConsumer />;
    case "RETRIEVE_DATA":
      return <RetrieveInfo />;
    case "SEND_DATA":
      return <SendData />;
  }
  return null;
};
