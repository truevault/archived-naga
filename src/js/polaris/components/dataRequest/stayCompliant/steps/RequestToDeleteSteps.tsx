import React from "react";
import { DataRequestDetail } from "../../../../../common/models";
import { ContactVendors } from "./delete/ContactVendors";
import { DeleteInfo } from "./delete/DeleteInfo";
import { NotifyConsumer } from "./shared/NotifyConsumer";
import { VerifyConsumer } from "./shared/VerifyConsumer";

type Props = {
  request: DataRequestDetail;
};

export const RequestToDeleteSteps: React.FC<Props> = ({ request }) => {
  switch (request.state) {
    case "VERIFY_CONSUMER":
      return <VerifyConsumer />;
    case "DELETE_INFO":
      return <DeleteInfo />;
    case "CONTACT_VENDORS":
      return <ContactVendors />;
    case "NOTIFY_CONSUMER":
      return <NotifyConsumer />;
  }
  return null;
};
