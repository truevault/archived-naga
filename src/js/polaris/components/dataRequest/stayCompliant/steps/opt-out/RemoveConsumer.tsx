import { FormControl, FormControlLabel, Radio, RadioGroup } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { useActionRequest, useDataRequest } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { usePrimaryOrganization } from "../../../../../hooks/useOrganization";
import { useOrganizationVendors } from "../../../../../hooks/useOrganizationVendors";
import { alphabetically } from "../../../../../surveys/util";
import { Callout, CalloutVariant } from "../../../../Callout";
import { DataRecipientLogo } from "../../../../DataRecipientLogo";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";
import { useSurvey } from "../../../../../../common/hooks/useSurvey";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../../../../pages/get-compliant/data-recipients/shared";
import { isIntentionalDisclosure, isRequestContextApplicable } from "../../../../../util/vendors";
import { useDataMap } from "../../../../../hooks/useDataMap";

export const RemoveConsumer = () => {
  const [org] = usePrimaryOrganization();

  const [allVendors] = useOrganizationVendors(org.id, undefined, true, true);
  const { request, refreshRequest, setStepCompleted } = useDataRequestProcessingContext();
  const [vendorOutcomes, setVendorOutcomes] = useState<Record<string, string>>({});

  const { result: fetchedInstructions } = useDataRequest({
    queryKey: ["fetchedOptOutInstructions", org.id],
    api: () => Api.requestHandlingInstructions.getOptOutInstructions(org.id),
  });
  // This does not match where the data is stored based on the OptOutInstructionPage
  // It may very well be that we want to update the OptOutInstructionPage Page instead
  // const [fetchedInstructions] = useRequestInstructions(org.id, workflowType);

  const { fetch: updateVendorOutcome } = useActionRequest({
    api: async ({ values }) => {
      const resp = await Api.dataRequest.updateVendor(org.id, request.id, values);
      if (resp.id == undefined) {
        throw new Error("invalid response from server");
      }
    },
    onError: (networkRequestState) => {
      if (networkRequestState.errorMessage === undefined) {
        networkRequestState.errorMessage =
          "There was a problem saving your selection. Please try again.";
      }
    },
  });

  const onSelectionChange = async (vendor, value) => {
    try {
      await updateVendorOutcome({
        values: { vendor: vendor.vendorId, outcome: value },
      });
      setVendorOutcomes((prev) => Object.assign({}, prev, { [vendor.vendorId]: value }));
    } catch (e) {
      // no-op
    } finally {
      refreshRequest();
    }
  };

  const { answers } = useSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME);
  const [dataMap] = useDataMap(org.id);

  const vendors = useMemo(
    () =>
      allVendors.filter(
        (v) =>
          (v.isUploadVendor || v.usesCustomAudience) &&
          !isIntentionalDisclosure(answers, v) &&
          isRequestContextApplicable(dataMap, request.context, v),
      ),
    [allVendors, answers, dataMap, request.context],
  );

  useEffect(() => {
    setStepCompleted(vendors && vendors.length <= request.vendorOutcomes.length);
  }, [request, setStepCompleted, vendors]);

  useEffect(() => {
    const vendorOutcomes = request.vendorOutcomes.reduce((acc, outcome) => {
      return Object.assign({}, acc, { [outcome.vendor.id]: outcome.outcome });
    }, {});
    setVendorOutcomes((prev) => Object.assign({}, prev, vendorOutcomes));
  }, [request, vendors]);

  const sortedVendors = useMemo(() => {
    return vendors?.sort(alphabetically);
  }, [vendors]);

  return (
    <>
      <div className="text-t3 mb-std">
        Remove the consumer from any data sharing and selling processes or lists involving the Data
        Recipients below. Your defaults and instructions can be updated in{" "}
        <Link className="text-weight-regular" to="/organization/requestHandling">
          Request Instructions.
        </Link>
      </div>

      {Boolean(fetchedInstructions) && (
        <Callout variant={CalloutVariant.Clear} className={"mb-std"}>
          <p>{fetchedInstructions}</p>
        </Callout>
      )}

      {sortedVendors &&
        sortedVendors.map((vendor) => {
          return (
            <div key={`vendor-${vendor.id}`}>
              <hr />
              <div className={"flex-container pt-std pb-std border-top--grey"}>
                <div className="text-t4 w-256 flex-container flex--align-start pl-xs">
                  <DataRecipientLogo dataRecipient={vendor} size={32} />
                  <div className="ml-mdlg">{vendor.name}</div>
                </div>
                <div className="flex-grow">
                  <FormControl component="fieldset">
                    <RadioGroup
                      aria-label="Vendor handling response"
                      name={`vendor-${vendor.id}-data-handling`}
                      onChange={(e) => {
                        onSelectionChange(vendor, e.target.value);
                      }}
                      value={vendorOutcomes[vendor.vendorId] || ""}
                      row
                    >
                      <FormControlLabel
                        value="NO_DATA"
                        control={<Radio />}
                        label={<span className={"text-t2"}>No data found</span>}
                      />
                      <FormControlLabel
                        control={<Radio />}
                        value="CONSUMER_REMOVED"
                        label={<span className={"text-t2"}>Consumer removed</span>}
                      />
                    </RadioGroup>
                  </FormControl>

                  {vendor.optOutInstructions && (
                    <Callout variant={CalloutVariant.Gray} className={"my-md"}>
                      <div className="text-weight-bold">Opt-Out Instructions</div>
                      <div
                        className="text-t2"
                        dangerouslySetInnerHTML={{ __html: vendor.optOutInstructions }}
                      ></div>
                    </Callout>
                  )}
                </div>
              </div>
            </div>
          );
        })}
    </>
  );
};
