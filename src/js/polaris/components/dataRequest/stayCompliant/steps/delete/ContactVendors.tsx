import React, { useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { useApiTrigger } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { ContactVendorRequest } from "../../../../../../common/service/server/dto/UpdateVendorRequest";
import { Error as RequestsError } from "../../../../../copy/requests";
import { usePrimaryOrganization } from "../../../../../hooks";
import { useContactVendorsMessageTemplate } from "../../../../../hooks/useTemplates";
import { Routes } from "../../../../../root/routes";
import { alphabetically } from "../../../../../surveys/util";
import { Callout, CalloutVariant } from "../../../../Callout";
import { VendorEmailDisplayDialog } from "../../../../dialog/VendorEmailDisplayDialog";
import { EmailButton } from "../../../../organisms/EmailButton";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";
import { useSurvey } from "../../../../../../common/hooks/useSurvey";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../../../../pages/get-compliant/data-recipients/shared";
import { isIntentionalDisclosure, isRequestContextApplicable } from "../../../../../util/vendors";
import { useDataMap } from "../../../../../hooks/useDataMap";

export const ContactVendors = () => {
  const [org] = usePrimaryOrganization();
  const {
    request,
    refreshRequest,
    setStepCompleted,
    vendors: { all: vendors },
  } = useDataRequestProcessingContext();
  const [dataMap] = useDataMap(org.id);

  const { answers } = useSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME);

  const [template] = useContactVendorsMessageTemplate(org.id, request.id);

  const [notificationModalOpen, setNotificationModalOpen] = useState(false);

  const closeModal = () => setNotificationModalOpen(false);

  const [updateVendorOutcome] = useApiTrigger(
    (values: ContactVendorRequest) => Api.dataRequest.contactVendors(org.id, request.id, values),
    refreshRequest,
    RequestsError.TransitionProcessing,
  );

  const filteredVendors = useMemo(
    () =>
      vendors
        ?.filter(
          (v) =>
            !v.isInstalled ||
            (request.dataRequestType.regulationName === "GDPR" &&
              v.gdprProcessorSetting === "Processor"),
        )
        .filter(
          (v) =>
            isRequestContextApplicable(dataMap, request.context, v) &&
            !isIntentionalDisclosure(answers, v),
        ),
    [vendors, answers, dataMap, request.context],
  );

  const missingDoNotContactExplanation = useMemo(() => {
    return filteredVendors
      ?.filter((v) => {
        const instruction = v.instructions.find((vi) => vi.requestType == "DELETE");

        const thirdParty =
          v.classification.slug == "third-party" || v.dataRecipientType == "third_party_recipient";
        const doNotContact = instruction?.deleteDoNotContact;
        const impossible = instruction?.doNotContactReason == "IMPOSSIBLE_OR_DIFFICULT";
        const noExplanation = !instruction?.doNotContactExplanation;

        return thirdParty && doNotContact && impossible && noExplanation;
      })
      .sort(alphabetically);
  }, [filteredVendors]);

  const contactVendorsList = useMemo(
    () =>
      filteredVendors
        ?.filter((v) => {
          const vendorOutcome = request.vendorOutcomes.find((x) => x.vendor.id === v.vendorId);
          const outcome = vendorOutcome?.outcome;
          const instruction = v.instructions.find((vi) => vi.requestType == "DELETE");

          const noDeletionInstructions = !v.deletionInstructions;
          const noDoNotContact = !instruction?.deleteDoNotContact;
          const noDataNotFound = outcome !== "NO_DATA";
          const noDataRetained = outcome !== "DATA_RETAINED";
          const noProcessingRetain = instruction?.processingMethod !== "RETAIN";

          return (
            noDeletionInstructions &&
            noDoNotContact &&
            noDataNotFound &&
            noDataRetained &&
            noProcessingRetain
          );
        })
        .sort(alphabetically),
    [filteredVendors, request.vendorOutcomes],
  );

  const noSendVendorList = contactVendorsList?.filter((v) => !v.email && !v.defaultEmail) || [];

  const sendVendorList = useMemo(
    () =>
      contactVendorsList?.filter(
        (v) =>
          (v.email || v.defaultEmail) &&
          (request.vendorOutcomes.find((x) => x.vendor.id === v.vendorId && !x.processed) ||
            request.vendorOutcomes.every((x) => x.vendor.id !== v.vendorId)),
      ),
    [request, contactVendorsList],
  );
  const completedVendorList =
    contactVendorsList?.filter(
      (v) =>
        (v.email || v.defaultEmail) &&
        request.vendorOutcomes.find((x) => x.vendor.id === v.vendorId && x.processed),
    ) || [];

  useEffect(() => {
    if (sendVendorList) {
      setStepCompleted(
        sendVendorList.length === 0 &&
          noSendVendorList.length === 0 &&
          missingDoNotContactExplanation.length === 0,
      );
    }
  }, [sendVendorList, noSendVendorList, missingDoNotContactExplanation, setStepCompleted]);

  if (!vendors) return null;

  const dispatchEmail = () => {
    const values = { vendors: sendVendorList.map((v) => v.vendorId) };
    updateVendorOutcome(values);
  };

  return (
    <>
      {request.contactVendorsEverHidden && (
        <Callout
          collapsible
          className="mb-xxl"
          title={<span className="text-t2">Why was the 'contact vendors' step added?</span>}
          variant={CalloutVariant.Clear}
        >
          'Contact vendors' is required based on your selections in the 'delete info' step.
        </Callout>
      )}

      <div className="text-t3 mb-std">
        To fulfill your data privacy obligation, the message below will be emailed to any vendors
        that do not have official deletion instructions that were linked to in the previous step.
      </div>

      {completedVendorList.length > 0 && (
        <div className="w-640">
          <Callout variant={CalloutVariant.Green} className="mb-std">
            <p>
              Message successfully sent to{" "}
              <strong>{completedVendorList.map((v) => v.name).join(", ")}.</strong>
            </p>
          </Callout>
        </div>
      )}

      {sendVendorList.length > 0 && (
        <>
          <Callout variant={CalloutVariant.Gray} className="mb-md">
            <div className="text-t3">
              <p>
                <strong>To:</strong>{" "}
                <Link to="#" onClick={() => setNotificationModalOpen(true)}>
                  See Recipients
                </Link>
              </p>
              <p>
                <strong>
                  Subject: {request.dataRequestType.regulationName} {request.dataRequestType.name}
                </strong>
              </p>

              <div dangerouslySetInnerHTML={{ __html: template }} />
            </div>
          </Callout>
        </>
      )}

      {noSendVendorList.length > 0 &&
        (noSendVendorList.length != contactVendorsList.length ? (
          <div className="w-896 my-md">
            <Callout variant={CalloutVariant.Yellow}>
              <p>
                The notification cannot be sent to the following Data Recipients because there is no
                email address added for them:{" "}
                <strong>{noSendVendorList.map((v) => v.name).join(", ")}.</strong>
              </p>
              <p>
                {" "}
                Visit{" "}
                <a href={Routes.organization.requestHandling.requestToDelete}>
                  Manage Request to Delete Settings
                </a>{" "}
                to add a contact email.
              </p>
            </Callout>
          </div>
        ) : (
          <div className="w-896 my-md">
            <Callout variant={CalloutVariant.Yellow}>
              <p>
                The notification cannot be sent because none of your Data Recipients can be
                contacted. Add email addresses for the following Data Recipients in{" "}
                <a href={Routes.organization.requestHandling.requestToDelete}>
                  Manage Request to Delete Settings
                </a>
                , then return to this step to preview and send the notification:{" "}
                <strong>{noSendVendorList.map((v) => v.name).join(", ")}.</strong>
              </p>
            </Callout>
          </div>
        ))}

      {missingDoNotContactExplanation?.length > 0 && (
        <div className="w-896 my-md">
          <Callout variant={CalloutVariant.Yellow}>
            <p>
              This step cannot be completed because the following Third Parties are marked as “Do
              Not Contact” but lack required information:{" "}
              <strong>{missingDoNotContactExplanation.map((v) => v.name).join(", ")}</strong>.
            </p>
            <p>
              Visit{" "}
              <a href={Routes.organization.requestHandling.requestToDelete}>
                Manage Request to Delete Settings
              </a>{" "}
              to add the missing information or update contact settings.
            </p>
          </Callout>
        </div>
      )}

      {(sendVendorList.length > 0 || noSendVendorList.length > 0) && (
        <EmailButton
          color="primary"
          variant="contained"
          className="btn-md mb-std"
          onClick={dispatchEmail}
          disabled={noSendVendorList?.length > 0}
          disabledTooltip={
            noSendVendorList?.length > 0
              ? "There are data recipients with no email address associated"
              : undefined
          }
        >
          Send
        </EmailButton>
      )}

      <VendorEmailDisplayDialog
        open={notificationModalOpen}
        onClose={closeModal}
        vendors={sendVendorList}
      />
    </>
  );
};
