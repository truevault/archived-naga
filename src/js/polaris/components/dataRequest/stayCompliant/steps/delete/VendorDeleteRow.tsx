import { FormControl, RadioGroup } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { LoadingButton } from "../../../../../../common/components/LoadingButton";
import { NetworkRequestGate } from "../../../../../../common/components/NetworkRequestGate";
import { useActionRequest } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { DataSubjectRequestDetailDto } from "../../../../../../common/service/server/dto/DataSubjectRequestDto";
import { OrganizationDataRecipientDto } from "../../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  OrganizationAuthorizationsDto,
  OrganizationDto,
} from "../../../../../../common/service/server/dto/OrganizationDto";
import { VendorOutcomeOption } from "../../../../../../common/service/server/Types";
import { usePlatformApps } from "../../../../../hooks/useOrganizationVendors";
import { installedAppsRespectingDeletionForVendor } from "../../../../../util/resources/vendorUtils";
import { isGoogleAnalytics, isShopify } from "../../../../../util/vendors";
import { Callout, CalloutVariant } from "../../../../Callout";
import { DataRecipientLogo } from "../../../../DataRecipientLogo";
import { VendorEmailDisplayDialog } from "../../../../dialog/VendorEmailDisplayDialog";
import { RadioControlLabel } from "../../../../input/RadioControlLabel";
import { VendorOutcomeCollection, VendorOutcomeHandler } from "./DeleteInfo";

export const VendorDeleteRow: React.FC<{
  vendor: OrganizationDataRecipientDto;
  vendorOutcomes: VendorOutcomeCollection;
  authorizations: OrganizationAuthorizationsDto;
  org: OrganizationDto;
  onSelectionChange: VendorOutcomeHandler;
  request: DataSubjectRequestDetailDto;
  updatePending?: boolean;
}> = ({
  vendor,
  onSelectionChange,
  vendorOutcomes,
  authorizations,
  org,
  request,
  updatePending = false,
}) => {
  const instruction = vendor.instructions.find((vi) => vi.requestType == "DELETE");

  const isPartialDeleteVendor = Boolean(instruction?.hasRetentionExceptions);
  const deleteLabel = isPartialDeleteVendor ? "Data partially deleted" : "Data deleted";
  const deleteValue = isPartialDeleteVendor ? "DATA_RETAINED" : "DATA_DELETED";

  const disabled = isGoogleAnalytics(vendor);
  const value = vendorOutcomes[vendor.vendorId] || "";

  return (
    <div
      key={`vendor-${vendor.id}`}
      className={"flex-container pt-std pb-std border-top border-top--grey"}
    >
      <div className="text-t4 w-256 flex-container flex--align-start pl-xs">
        <DataRecipientLogo dataRecipient={vendor} className="align-top mr-mdlg" />

        <span>{vendor.name}</span>
      </div>
      <div className="flex-grow">
        <FormControl component="fieldset" className={"mb-md w-100"}>
          <RadioGroup
            className={"flex--justify-start gap-xl"}
            aria-label="Vendor handling response"
            name={`vendor-${vendor.id}-data-handling`}
            onChange={(e) => onSelectionChange(vendor, e.target.value as VendorOutcomeOption)}
            value={value}
            row
          >
            <RadioControlLabel
              value="NO_DATA"
              disabled={disabled}
              disabledTooltip={"No data to delete"}
              label={<span className={"text-t2"}>No data to delete</span>}
              recommended={instruction?.processingMethod == "INACCESSIBLE_OR_NOT_STORED"}
              narrow
              loading={updatePending && value == "NO_DATA"}
            />

            <RadioControlLabel
              value={deleteValue}
              disabled={disabled}
              disabledTooltip={deleteLabel}
              label={<span className={"text-t2"}>{deleteLabel}</span>}
              recommended={instruction?.processingMethod == "DELETE"}
              narrow
              loading={updatePending && value in ["DATA_RETAINED", "DATA_DELETED"]}
            />
          </RadioGroup>
        </FormControl>

        {isShopify(vendor) && <ShopifyFooter org={org} vendor={vendor} />}

        {isGoogleAnalytics(vendor) && (
          <GoogleAnalyticsFooter
            org={org}
            request={request}
            authorizations={authorizations}
            outcome={vendorOutcomes[vendor.vendorId]}
            onSelectionChange={(selection) =>
              onSelectionChange(vendor, selection as VendorOutcomeOption)
            }
          />
        )}

        {vendor.deletionInstructions && (
          <Callout variant={CalloutVariant.Gray} className={"mb-md"}>
            <div className="text-weight-bold">Deletion Instructions</div>
            <div
              className="text-t2"
              dangerouslySetInnerHTML={{ __html: vendor.deletionInstructions }}
            ></div>
          </Callout>
        )}

        {instruction?.processingInstructions && (
          <Callout variant={CalloutVariant.Clear} className={"mb-md"}>
            <p className="text-t2">{instruction.processingInstructions}</p>
          </Callout>
        )}
      </div>
    </div>
  );
};

type ShopifyFooterProps = {
  org: OrganizationDto;
  vendor: OrganizationDataRecipientDto;
  // authorizations: OrganizationAuthorizationsDto;
  // request: DataSubjectRequestDetailDto;
  // outcome: string | null;
  // onSelectionChange(selection: string);
};
const ShopifyFooter: React.FC<ShopifyFooterProps> = ({ org, vendor }) => {
  const [platformApps, platformAppsRequest] = usePlatformApps(org.id, vendor.vendorId);
  const installedApps = installedAppsRespectingDeletionForVendor(vendor, platformApps);

  const [showShopifyInstalledApps, setShowShopifyInstalledApps] = useState(false);

  const toggleShopifyInstalledApps = useCallback(
    (evt?: Event) => {
      evt?.preventDefault();
      setShowShopifyInstalledApps((x) => !x);
    },
    [setShowShopifyInstalledApps],
  );

  return (
    <NetworkRequestGate requests={[platformAppsRequest]}>
      {installedApps?.length > 0 ? (
        <Callout variant={CalloutVariant.Yellow} className={"mb-md"}>
          <VendorEmailDisplayDialog
            open={showShopifyInstalledApps}
            // @ts-ignore
            onClose={toggleShopifyInstalledApps}
            vendors={installedApps}
            title="Shopify Installed Apps"
          />
          Shopify will automatically delete data from your{" "}
          <a href="#" onClick={(e) => toggleShopifyInstalledApps(e as any)}>
            installed apps
          </a>
          .
        </Callout>
      ) : (
        <></>
      )}
    </NetworkRequestGate>
  );
};

type GoogleAnaltyicsFooterProps = {
  org: OrganizationDto;
  authorizations: OrganizationAuthorizationsDto;
  request: DataSubjectRequestDetailDto;
  outcome: string | null;
  onSelectionChange(selection: string);
};

const GoogleAnalyticsFooter: React.FC<GoogleAnaltyicsFooterProps> = ({
  org,
  authorizations,
  request,
  outcome,
  onSelectionChange,
}) => {
  const canAutodelete =
    request.hasGaUserId && Boolean(authorizations.hasGoogle) && Boolean(org.gaWebProperty);
  const hasAutodeleted = outcome == "DATA_DELETED";
  const hasMarkedNoData = outcome == "NO_DATA";

  const api = useCallback(
    () => Api.dataRequest.autodeleteGoogleAnalytics(org.id, request.id),
    [org.id, request.id],
  );

  const { fetch, request: autodeleteRequest } = useActionRequest({
    api,
    notifySuccess: true,
    notifyError: true,
    onSuccess: () => {
      onSelectionChange("DATA_DELETED");
    },
    onError: (networkRequestState) => {
      if (networkRequestState.errorMessage === undefined) {
        networkRequestState.errorMessage = "There was an error while processing your request.";
      }
    },
    messages: {
      success: "Google Analytics data autodeleted successfully",
    },
  });

  useEffect(() => {
    if (!canAutodelete && !hasMarkedNoData) {
      onSelectionChange("NO_DATA");
    }
  }, [canAutodelete, hasMarkedNoData]);

  if (!canAutodelete) {
    return (
      <GoogleAnalyticsAutodeleteWarning
        orgId={org.id}
        hasGaUserId={request.hasGaUserId}
        hasGoogleAuthorization={authorizations.hasGoogle}
        hasGaWebProperty={Boolean(org.gaWebProperty)}
      />
    );
  }
  /**/
  return (
    <div className="mt-sm">
      <LoadingButton
        variant="contained"
        color="primary"
        loading={autodeleteRequest.running}
        disabled={hasAutodeleted || autodeleteRequest.running}
        onClick={fetch}
      >
        Delete Consumer Data
      </LoadingButton>
    </div>
  );
};

type GoogleAnalyticsAutodeleteWarningProps = {
  orgId: string;
  hasGaUserId: boolean;
  hasGoogleAuthorization: boolean;
  hasGaWebProperty: boolean;
};
const GoogleAnalyticsAutodeleteWarning: React.FC<GoogleAnalyticsAutodeleteWarningProps> = ({
  orgId,
  hasGoogleAuthorization,
  hasGaWebProperty,
}) => {
  if (!hasGaWebProperty) {
    return (
      <Callout variant={CalloutVariant.Yellow}>
        We cannot autodelete information from <strong>Google Analytics</strong> for this request
        because there is no Web Property ID set in the{" "}
        <Link to={`/organization/${orgId}/settings/integrations`}>organization settings</Link>. We
        have marked as <strong>no data to delete</strong> automatically.
      </Callout>
    );
  }

  if (!hasGoogleAuthorization) {
    return (
      <Callout variant={CalloutVariant.Yellow}>
        We cannot autodelete information from <strong>Google Analytics</strong> for this request
        because Google Analytics has not been authorized in the{" "}
        <Link to={`/organization/${orgId}/settings/integrations`}>organization settings</Link>. We
        have marked as <strong>no data to delete</strong> automatically.
      </Callout>
    );
  }

  return (
    <Callout variant={CalloutVariant.Yellow}>
      We cannot autodelete information from <strong>Google Analytics</strong> for this request
      because the consumer's Google Analytics User ID was not available at the time this request was
      submitted. We have marked as <strong>no data to delete</strong> automatically.
    </Callout>
  );
};
