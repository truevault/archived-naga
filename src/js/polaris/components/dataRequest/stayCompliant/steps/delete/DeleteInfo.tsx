import React, { useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { NetworkRequest } from "../../../../../../common/models";
import { OrganizationDataRecipientDto } from "../../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorOutcomeOption } from "../../../../../../common/service/server/Types";
import { useAuthorizations } from "../../../../../hooks/useAuthorizations";
import { usePrimaryOrganization } from "../../../../../hooks/useOrganization";
import { useOrganizationVendors } from "../../../../../hooks/useOrganizationVendors";
import { Routes } from "../../../../../root/routes";
import { alphabetically } from "../../../../../surveys/util";
import { Abbr } from "../../../../Abbr";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";
import { RetainedVendorRow } from "./RetainedVendorRow";
import { VendorDeleteRow } from "./VendorDeleteRow";
import { useSurvey } from "../../../../../../common/hooks/useSurvey";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../../../../pages/get-compliant/data-recipients/shared";
import { isIntentionalDisclosure, isRequestContextApplicable } from "../../../../../util/vendors";
import { useDataMap } from "../../../../../hooks/useDataMap";

export type VendorOutcomeHandler = (
  vendor: OrganizationDataRecipientDto,
  outcome: VendorOutcomeOption,
) => void;

export type VendorOutcomeCollection = Record<string, string>;

export const DeleteInfo = () => {
  const [org, orgRequest] = usePrimaryOrganization();
  const [allVendors, vendorsRequest] = useOrganizationVendors(org.id, undefined, true, true);
  const { request, setStepCompleted, mutations } = useDataRequestProcessingContext();
  const [readyToRender, setReadyToRender] = useState(false);
  const [dataMap] = useDataMap(org.id);

  const { answers } = useSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME);

  const { authorizations, request: authorizationsRequest } = useAuthorizations(org.id);

  const onSelectionChange: VendorOutcomeHandler = (vendor, value) => {
    mutations.updateVendorOutcomeMutation.mutate({ vendor: vendor.vendorId, outcome: value });
  };

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(vendorsRequest, orgRequest, authorizationsRequest)) {
      setReadyToRender(true);
    }
  }, [orgRequest, vendorsRequest, authorizationsRequest]);

  const vendors = useMemo(() => {
    return allVendors?.filter((v) => {
      const instruction = v.instructions.find((vi) => vi.requestType == "DELETE");
      const instructionOk = instruction?.processingMethod != "INACCESSIBLE_OR_NOT_STORED";
      const contextOk = isRequestContextApplicable(dataMap, request.context, v);
      return instructionOk && contextOk && !isIntentionalDisclosure(answers, v);
    });
  }, [allVendors, request.context, answers, dataMap]);

  const installedVendors = useMemo(() => {
    return vendors?.filter((v) => !v.isInstalled)?.sort(alphabetically) ?? [];
  }, [vendors]);

  const deleteVendors = useMemo(() => {
    return installedVendors.filter(
      (v) =>
        !v.instructions.some(
          (i) =>
            i.requestType == "DELETE" &&
            i.processingMethod === "RETAIN" &&
            !i.hasRetentionExceptions,
        ),
    );
  }, [installedVendors]);

  const retainVendors = useMemo(() => {
    return installedVendors.filter((v) =>
      v.instructions.some((i) => i.requestType == "DELETE" && i.processingMethod === "RETAIN"),
    );
  }, [installedVendors]);

  useEffect(() => {
    const outcomes = request.vendorOutcomes;
    const everyOutcomeCompleted = deleteVendors.every((vendor) => {
      const outcome = outcomes?.find((o) => o.vendor.id == vendor.vendorId);
      return outcome?.outcome != null || vendor.isInstalled === true;
    });

    setStepCompleted(
      Boolean(deleteVendors) && deleteVendors.length <= outcomes.length && everyOutcomeCompleted,
    );
  }, [setStepCompleted, deleteVendors, request.vendorOutcomes, vendors]);

  const vendorOutcomes = useMemo(() => {
    const outcomes = request.vendorOutcomes.reduce(
      (acc, outcome) => Object.assign({}, acc, { [outcome.vendor.id]: outcome.outcome }),
      {},
    );

    request._local?.pendingVendorOutcomeUpdates?.forEach((pending) => {
      outcomes[pending.vendorId] = pending.outcome;
    });

    return outcomes;
  }, [request.vendorOutcomes, request._local?.pendingVendorOutcomeUpdates]);

  const pendingUpdates = useMemo(() => {
    const pending = request._local?.pendingVendorOutcomeUpdates?.map((pending) => pending.vendorId);
    return new Set(pending);
  }, [request._local?.pendingVendorOutcomeUpdates]);

  if (!readyToRender) return null;

  return (
    <>
      <div className="text-t3 mb-std">
        Select how you’ve processed data handled by each Data Recipient. Your processing defaults
        are marked; select them only if they apply for this request. Your defaults and instructions
        can be updated in{" "}
        <Link className="text-weight-regular" to="/organization/requestHandling">
          Request Instructions.
        </Link>
      </div>

      {deleteVendors &&
        deleteVendors.map((vendor) => (
          <VendorDeleteRow
            key={vendor.id}
            vendor={vendor}
            authorizations={authorizations}
            vendorOutcomes={vendorOutcomes}
            updatePending={pendingUpdates.has(vendor.vendorId)}
            onSelectionChange={onSelectionChange}
            org={org}
            request={request}
          />
        ))}

      {retainVendors && retainVendors.length > 0 && (
        <>
          <div className="text-t5 text-weight-medium mb-md"> Data Retention </div>

          <div className="text-t3 mb-mdlg">
            The systems below are marked for retention based on one or more{" "}
            <Abbr title="Your business may retain personal data under certain exceptions, such as for internal use and to comply with a legal obligation.">
              <strong>exceptions to deletion</strong>
            </Abbr>
            . Do not delete data stored with these Data Recipients if it is kept under an exception
            to deletion. Data retention settings can be updated in your{" "}
            <Link to={`${Routes.dataMap.root}?tab=data_recipients`}>Data Map</Link>.
          </div>

          {retainVendors.map((vendor) => (
            <RetainedVendorRow key={vendor.id} vendor={vendor} />
          ))}
        </>
      )}
    </>
  );
};
