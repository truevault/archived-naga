import { Check } from "@mui/icons-material";
import React from "react";
import { OrganizationDataRecipientDto } from "../../../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Callout, CalloutVariant } from "../../../../Callout";
import { DataRecipientLogo } from "../../../../DataRecipientLogo";

export const RetainedVendorRow: React.FC<{
  vendor: OrganizationDataRecipientDto;
}> = ({ vendor }) => {
  const someDataRetained = vendor.instructions.some(
    (i) => i.requestType == "DELETE" && i.processingMethod === "RETAIN" && i.hasRetentionExceptions,
  );

  return (
    <Callout variant={CalloutVariant.Gray} className={"mb-md"}>
      <div className="flex-row flex--between flex--align-center">
        <div className="flex-row--simple">
          <DataRecipientLogo dataRecipient={vendor} className="align-top mr-mdlg" />

          <div className="text-t4 text-weight-medium"> {vendor.name} </div>
        </div>
        <div className="flex-row--simple">
          <Check color="primary" fontSize="small" />
          <div className="text-t1 ml-xxs">{someDataRetained ? "Some" : "All"} data retained</div>
        </div>
      </div>
    </Callout>
  );
};
