import { Alert, Button } from "@mui/material";
import React, { ReactNode, useEffect, useMemo, useRef } from "react";
import { useApiTrigger } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { Error as RequestsError } from "../../../../../copy/requests";
import { Fmt } from "../../../../../formatters";
import { usePrimaryOrganization } from "../../../../../hooks";
import { useClosedMessageTemplate } from "../../../../../hooks/useTemplates";
import { Callout, CalloutVariant } from "../../../../Callout";
import { AttachmentList } from "../../../../organisms/AttachmentList";
import { EmailButton } from "../../../../organisms/EmailButton";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";

type SendDataProps = {
  instructions?: ReactNode;
};

export const SendData: React.FC<SendDataProps> = ({ instructions }) => {
  const [org] = usePrimaryOrganization();
  const { request, refreshRequest, attachment, setStepCompleted } =
    useDataRequestProcessingContext();

  const [template] = useClosedMessageTemplate(org.id, request.id);

  const [contactRequestor] = useApiTrigger(
    () => Api.dataRequest.contactRequestor(org.id, request.id, attachment.files),
    refreshRequest,
    RequestsError.TransitionProcessing,
  );

  const fullName = Fmt.DataSubject.fmtFullName(request);

  const uploadEl = useRef<HTMLInputElement>(null);
  const removeFile = (index: number) => {
    attachment?.removeFile(index);
  };

  const fileUploadNeeded =
    request.consumerGroupResult !== "NOT_FOUND" &&
    request.consumerGroupResult !== "PROCESSED_AS_SERVICE_PROVIDER";

  const names = useMemo(() => attachment?.files?.map((f) => f.name) ?? [], [attachment]);

  const instr = instructions || (
    <>
      The message below will be sent to the consumer. If you have data to send to the consumer from
      the previous step, upload it and click "Send" below. If you have no data to send, click "Send"
      to send the message. After the message is sent, close the request.
    </>
  );

  useEffect(() => {
    setStepCompleted(!!request.requestorContacted);
  }, [request.requestorContacted, setStepCompleted]);

  return (
    <>
      <div className="text-t3 mb-std">{instr}</div>

      {request.requestorContacted && (
        <div className="w-640">
          <Callout variant={CalloutVariant.Green} className="mb-std">
            Done! Message successfully sent to {fullName}. You can now close this request.
          </Callout>
        </div>
      )}

      {!request.requestorContacted && (
        <>
          {fileUploadNeeded && (
            <div className="mb-lg">
              {attachment.fileError && (
                <div className="mb-sm">
                  <Alert severity="error">{attachment.fileError}</Alert>
                </div>
              )}

              <AttachmentList className="mb-md" onRemove={removeFile} attachmentNames={names} />

              <label htmlFor="note-upload" className="mr-md">
                <input
                  style={{ display: "none" }}
                  ref={uploadEl}
                  id="note-upload"
                  type="file"
                  multiple
                  onChange={(e) => {
                    if (e.target.files) {
                      attachment?.addFiles(Array.from(e.target.files));
                    }
                  }}
                />
                <Button
                  variant="contained"
                  className="btn-md"
                  onClick={() => uploadEl.current?.click()}
                >
                  Upload
                </Button>
              </label>
            </div>
          )}

          <Callout variant={CalloutVariant.Gray} className="mb-md">
            <div className="text-t3">
              <p>
                <strong>To: {request.subjectEmailAddress}</strong>
              </p>
              <p>
                <strong>Subject: A message about your request [{request.id}]</strong>
              </p>
              <div dangerouslySetInnerHTML={{ __html: template }} />
            </div>
          </Callout>

          <EmailButton
            color="primary"
            variant="contained"
            className="btn-md mb-std"
            onClick={contactRequestor}
            disabled={fileUploadNeeded && names.length == 0}
            disabledTooltip="You must upload at least one file before sending the message."
          >
            Send
          </EmailButton>
        </>
      )}
    </>
  );
};
