import { FormControl, FormControlLabel, Radio, RadioGroup } from "@mui/material";
import clsx from "clsx";
import React, { useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { ExternalLink } from "../../../../../../common/components/ExternalLink";
import { InlineLoading } from "../../../../../../common/components/Loading";
import { useActionRequest } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { usePrimaryOrganization } from "../../../../../hooks/useOrganization";
import { useOrganizationVendors } from "../../../../../hooks/useOrganizationVendors";
import { alphabetically } from "../../../../../surveys/util";
import { Callout, CalloutVariant } from "../../../../Callout";
import { DataRecipientLogo } from "../../../../DataRecipientLogo";
import { SensitiveInfoCallout } from "../../../../dialog/SensitiveInfoDialog";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";
import { useSurvey } from "../../../../../../common/hooks/useSurvey";
import { SELLING_AND_SHARING_SURVEY_NAME } from "../../../../../pages/get-compliant/data-recipients/shared";
import { isIntentionalDisclosure, isRequestContextApplicable } from "../../../../../util/vendors";
import { useDataMap } from "../../../../../hooks/useDataMap";

export const RetrieveInfo = () => {
  const [org] = usePrimaryOrganization();

  const [allVendors] = useOrganizationVendors(org.id, undefined, true, true);

  const { request, refreshRequest, setStepCompleted, setUpdateRequests } =
    useDataRequestProcessingContext();
  const [vendorOutcomes, setVendorOutcomes] = useState<Record<string, string>>({});

  const [dataMap] = useDataMap(org.id);

  const { fetch: updateVendorOutcome, request: updateOutcomeRequest } = useActionRequest({
    api: async ({ values, resetState }) => {
      try {
        const resp = await Api.dataRequest.updateVendor(org.id, request.id, values);
        if (resp.id == undefined) {
          resetState();
        }
        refreshRequest();
      } catch (err) {
        resetState();
        throw err;
      }
    },
    allowConcurrent: true,
    onError: refreshRequest,
  });

  const onSelectionChange = (vendor, value) => {
    const resetState = () =>
      setVendorOutcomes((prev) => Object.assign({}, prev, { [vendor.id]: vendorOutcomes }));

    setVendorOutcomes((prev) => Object.assign({}, prev, { [vendor.vendorId]: value }));
    updateVendorOutcome({
      values: { vendor: vendor.vendorId, outcome: value },
      resetState: resetState,
    });
  };

  const { answers } = useSurvey(org.id, SELLING_AND_SHARING_SURVEY_NAME);

  const vendors = useMemo(() => {
    return allVendors?.filter((v) => {
      const instruction = v.instructions.find((vi) => vi.requestType == "KNOW");
      const instructionOk = instruction?.processingMethod != "INACCESSIBLE_OR_NOT_STORED";

      const contextOk = isRequestContextApplicable(dataMap, request.context, v);
      return instructionOk && contextOk && !isIntentionalDisclosure(answers, v);
    });
  }, [allVendors, answers, dataMap, request.context]);

  // set the update requests
  useEffect(() => {
    setUpdateRequests(updateOutcomeRequest);
  }, [updateOutcomeRequest]);

  useEffect(() => {
    const hasVendors = Boolean(vendors);
    const everyVendorHasOutcome = vendors?.every((v) => {
      const outcome = request.vendorOutcomes.find((vo) => vo.vendor.id == v.vendorId);
      return outcome && outcome.outcome != null;
    });

    setStepCompleted(hasVendors && everyVendorHasOutcome);
  }, [request, setStepCompleted, vendors]);

  useEffect(() => {
    const vendorOutcomes = request.vendorOutcomes.reduce((acc, outcome) => {
      return Object.assign({}, acc, { [outcome.vendor.id]: outcome.outcome });
    }, {});
    setVendorOutcomes((prev) => Object.assign({}, prev, vendorOutcomes));
  }, [request, vendors]);

  const sortedVendors = useMemo(() => {
    return vendors?.sort(alphabetically);
  }, [vendors]);

  return (
    <>
      <div className="text-t3 mb-std">
        Retrieve any personal information you have about the consumer. Save the data in a document
        or other easily accessible location for the next step. Your defaults and instructions can be
        updated in{" "}
        <Link className="text-weight-regular" to="/organization/requestHandling">
          Request Instructions.
        </Link>
      </div>

      <div className="mb-std">
        <SensitiveInfoCallout />
      </div>
      {sortedVendors &&
        sortedVendors.map((vendor) => {
          const instruction = vendor.instructions.find((vi) => vi.requestType == "KNOW");

          const localOutcome = vendorOutcomes[vendor.vendorId] || "";
          const remoteOutcome =
            request.vendorOutcomes.find((vo) => vo.vendor.id == vendor.vendorId)?.outcome || "";

          return (
            <div key={vendor.id}>
              <hr />
              <div
                key={`vendor-${vendor.id}`}
                className={"flex-container pt-std pb-std border-top--grey"}
              >
                <div className="text-t4 w-256 flex-container flex--align-start pl-xs">
                  <DataRecipientLogo
                    className="w-space-large align-top mr-mdlg"
                    dataRecipient={vendor}
                  />
                  <span>{vendor.name}</span>
                </div>
                <div className="flex-grow">
                  <FormControl component="fieldset" className={"mb-md"}>
                    <RadioGroup
                      aria-label="Vendor handling response"
                      className="align-items-center"
                      name={`vendor-${vendor.id}-data-handling`}
                      onChange={(e) => {
                        onSelectionChange(vendor, e.target.value);
                      }}
                      value={localOutcome}
                      row
                    >
                      <FormControlLabel
                        value="NO_DATA"
                        control={<Radio />}
                        label={<span className={"text-t2 pr-xs"}>No data to retrieve</span>}
                        className={clsx({
                          highlight: instruction?.processingMethod == "INACCESSIBLE_OR_NOT_STORED",
                        })}
                      />
                      <FormControlLabel
                        control={<Radio />}
                        value="DATA_RETRIEVED"
                        label={<span className={"text-t2 pr-xs"}>Data retrieved</span>}
                        className={clsx({
                          highlight: instruction?.processingMethod != "INACCESSIBLE_OR_NOT_STORED",
                        })}
                      />

                      {localOutcome != remoteOutcome && (
                        <div style={{ marginTop: 4, opacity: 0.5 }}>
                          <InlineLoading />
                        </div>
                      )}
                    </RadioGroup>
                  </FormControl>

                  {vendor.ccpaRequestToKnowLink && (
                    <Callout
                      variant={CalloutVariant.Gray}
                      title="Access Instructions"
                      className="mb-md"
                    >
                      <ExternalLink
                        href={vendor.ccpaRequestToKnowLink}
                        target="_blank"
                        className="text-style-normal"
                      >
                        {vendor.name} Instructions
                      </ExternalLink>
                    </Callout>
                  )}

                  {vendor.accessInstructions && (
                    <Callout
                      variant={CalloutVariant.Gray}
                      title="Access Instructions"
                      className={"mb-md"}
                      __dangerousChildrenHtml={vendor.accessInstructions}
                    />
                  )}

                  {instruction?.processingInstructions && (
                    <Callout variant={CalloutVariant.Clear} className="mb-md">
                      <p>{instruction.processingInstructions}</p>
                    </Callout>
                  )}
                </div>
              </div>
            </div>
          );
        })}
    </>
  );
};
