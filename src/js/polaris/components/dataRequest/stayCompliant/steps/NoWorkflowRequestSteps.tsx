import React from "react";
import { DataRequestDetail } from "../../../../../common/models";
import { Callout, CalloutVariant } from "../../../Callout";
import { Email as EmailIcon, Person } from "@mui/icons-material";
import { Fmt } from "../../../../formatters";
import { usePrimaryOrganization } from "../../../../hooks/useOrganization";

type Props = {
  request: DataRequestDetail;
};

export const NoWorkflowRequestSteps: React.FC<Props> = ({ request }) => {
  const [org] = usePrimaryOrganization();
  const fullName = Fmt.DataSubject.fmtFullName(request);
  const { customFieldEnabled, customFieldLabel } = org;
  const email = request.subjectEmailAddress;
  const customFieldValue = request.organizationCustomInput;

  return (
    <>
      {request.emailVerified && (
        <Callout icon={EmailIcon} variant={CalloutVariant.Green} className="mb-std w-640">
          <p>{`${fullName}'s`} email has been verified by Polaris.</p>
        </Callout>
      )}

      <Callout icon={Person} variant={CalloutVariant.LightPurple} className="mb-md w-640">
        <p className="mt-0 mb-xs">Name: {fullName}</p>
        <p className="mt-0 mb-xs">Email: {email}</p>
        {customFieldEnabled && (
          <p className="mt-0 mb-xs">
            {customFieldLabel}: {customFieldValue ?? "Not Provided"}
          </p>
        )}
      </Callout>
    </>
  );
};
