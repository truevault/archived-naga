import { Checkbox, FormControlLabel } from "@mui/material";
import React, { useCallback, useEffect } from "react";
import { useActionRequest } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { fmtLongRequestType } from "../../../../../formatters/request";
import { usePrimaryOrganization } from "../../../../../hooks/useOrganization";
import { Callout, CalloutVariant } from "../../../../Callout";
import { SUBMIT_FIELD_NAMES } from "../../../DataRequestSubjectCard";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";

export const CorrectDataStep = () => {
  const [org] = usePrimaryOrganization();

  const { request, refreshRequest, setStepCompleted } = useDataRequestProcessingContext();

  const api = useCallback(
    ({ request, finished }) => {
      const existingValues = Object.fromEntries(
        Object.entries(request).filter(([k]) => SUBMIT_FIELD_NAMES.includes(k)),
      );
      const values = Object.assign({}, existingValues, { correctionFinished: finished }) as any;
      return Api.dataRequest.updateRequest(org.id, request.id, values);
    },
    [request],
  );

  const { fetch: updateField } = useActionRequest({ api });

  const handleSelectionUpdated = async (isFinished: boolean) => {
    await updateField({ request, finished: isFinished });
    refreshRequest();
  };

  useEffect(() => {
    setStepCompleted(request.correctionFinished ?? false);
  }, [request.correctionFinished]);

  return (
    <>
      <div className="text-t3 mb-std">
        The consumer’s rectification message is provided below. Correct the consumer’s personal data
        in your records. If you have questions about the consumer’s message, or if you do not
        believe the requested correction should be made, email the consumer from your privacy email
        to request clarification. When you’ve made the necessary corrections, check the box below
        and proceed to the next step.
      </div>

      <Callout
        variant={CalloutVariant.Clear}
        title={`The consumer provided the following details with their ${fmtLongRequestType(
          request.dataRequestType.type,
        )}:`}
        className="mb-md"
      >
        {request.correctionRequest}
      </Callout>

      <FormControlLabel
        control={
          <Checkbox
            checked={request.correctionFinished ?? false}
            onChange={({ target }) => handleSelectionUpdated(target.checked)}
          />
        }
        label="We've corrected any inaccurate data about this consumer"
      />
    </>
  );
};
