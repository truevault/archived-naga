import { Checkbox, FormControlLabel } from "@mui/material";
import React, { useCallback, useEffect } from "react";
import { useActionRequest } from "../../../../../../common/hooks/api";
import { Api } from "../../../../../../common/service/Api";
import { usePrimaryOrganization } from "../../../../../hooks/useOrganization";
import { Callout, CalloutVariant } from "../../../../Callout";
import { SUBMIT_FIELD_NAMES } from "../../../DataRequestSubjectCard";
import { useDataRequestProcessingContext } from "../../DataRequestProcessingContext";

export const RemoveConsentStep = () => {
  const [org] = usePrimaryOrganization();

  const { request, refreshRequest, setStepCompleted } = useDataRequestProcessingContext();

  const api = useCallback(
    ({ request, finished }) => {
      const existingValues = Object.fromEntries(
        Object.entries(request).filter(([k]) => SUBMIT_FIELD_NAMES.includes(k)),
      );
      const values = Object.assign({}, existingValues, {
        consentWithdrawnFinished: finished,
      }) as any;
      return Api.dataRequest.updateRequest(org.id, request.id, values);
    },
    [request],
  );

  const { fetch: updateField } = useActionRequest({ api });

  const handleSelectionUpdated = async (isFinished: boolean) => {
    await updateField({ request, finished: isFinished });
    refreshRequest();
  };

  useEffect(() => {
    setStepCompleted(request.consentWithdrawnFinished ?? false);
  }, [request.consentWithdrawnFinished]);

  return (
    <>
      <div className="text-t3 mb-std">
        The consumer has withdrawn their consent for your business to process their data for the
        purposes listed below. To fulfill the request, remove the consumer’s data from any systems
        and vendors where data is processed for these purposes. When you’ve done, check the box
        below and proceed to the next step.
      </div>

      <Callout
        variant={CalloutVariant.Clear}
        className="mb-md"
        title="The consumer has withdrawn consent for the following processing activities:"
      >
        <ul>
          {request.consentWithdrawnRequest?.map((pa) => {
            return <li key={pa}>{pa}</li>;
          })}
        </ul>
      </Callout>

      <FormControlLabel
        control={
          <Checkbox
            checked={request.consentWithdrawnFinished ?? false}
            onChange={({ target }) => handleSelectionUpdated(target.checked)}
          />
        }
        label="We've stopped processing the consumer's data for the processing activities listed above."
      />
    </>
  );
};
