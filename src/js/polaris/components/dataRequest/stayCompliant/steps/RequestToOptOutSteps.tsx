import React from "react";
import { DataRequestDetail } from "../../../../../common/models";
import { RemoveConsumer } from "./opt-out/RemoveConsumer";
import { NotifyConsumer } from "./shared/NotifyConsumer";

type Props = {
  request: DataRequestDetail;
};

export const RequestToOptOutSteps: React.FC<Props> = ({ request }) => {
  switch (request.state) {
    case "REMOVE_CONSUMER":
      return <RemoveConsumer />;
    case "NOTIFY_CONSUMER":
      return <NotifyConsumer />;
  }
  return null;
};
