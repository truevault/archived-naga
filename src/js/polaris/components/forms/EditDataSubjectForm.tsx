import React from "react";
import { Form } from "react-final-form";
import { DialogContent, DialogActions, Button } from "@mui/material";
import { Text } from "../../../common/components/input/Text";
import { Validation } from "../../util";
import { DataRequestDetail } from "../../../common/models";

type EditDataSubjectProps = {
  onSubmit: (values: any) => void;
  onClose: () => void;
  request: DataRequestDetail;
};

export const EditDataSubject = ({ onSubmit, onClose, request }: EditDataSubjectProps) => {
  const handleSubmit = (values) => {
    onSubmit(
      Object.assign(
        {},
        {
          subjectEmailAddress: null,
          subjectPhoneNumber: null,
          subjectMailingAddress: null,
        },
        values,
      ),
    );
  };
  return (
    <Form
      onSubmit={handleSubmit}
      initialValues={request}
      validate={validation}
      render={({ handleSubmit, ...rest }) => {
        return (
          <form onSubmit={handleSubmit}>
            <EditDataSubjectForm {...rest} onCancel={onClose} />
          </form>
        );
      }}
    />
  );
};

const validation = (values) => {
  const errors = {} as any;

  Validation.optionalEmail(values, "subjectEmailAddress", errors);

  return errors;
};

const EditDataSubjectForm = ({ onCancel }) => {
  return (
    <>
      <DialogContent>
        <div className="flex-col">
          <div className="flex-row flex--expand mb-lg">
            <Text field="subjectEmailAddress" label="Email Address" helperText="Optional" />
          </div>
          <div className="flex-row flex--expand mb-lg">
            <Text field="subjectPhoneNumber" label="Phone Number" helperText="Optional" />
          </div>
          <div className="flex-row flex--expand mb-lg">
            <Text
              multiline
              rows={3}
              field="subjectMailingAddress"
              label="Mailing Address"
              helperText="Optional"
            />
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel}>Cancel</Button>
        <Button type="submit" color="primary" variant="contained">
          Update
        </Button>
      </DialogActions>
    </>
  );
};
