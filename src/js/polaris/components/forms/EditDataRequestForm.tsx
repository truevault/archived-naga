import React from "react";
import { Form } from "react-final-form";
import { DialogContent, DialogActions, Button } from "@mui/material";
import { Text } from "../../../common/components/input/Text";
import { Select } from "../../../common/components/input/Select";
import { DatePicker } from "../../../common/components/input/Date";
import { DataRequestDetail } from "../../../common/models";
import moment from "moment";
import { requiredDate, requiredEmail, requiredString } from "../../util/validation";

type EditDataRequestProps = {
  onSubmit: (values: any) => void;
  onClose: () => void;
  dataRequestTypeOptions: Array<{ label: string; value: string }>;
  request: DataRequestDetail;
};
const SUBMISSION_METHOD_OPTIONS = [
  { label: "Telephone", value: "TELEPHONE" },
  { label: "Email", value: "EMAIL" },
];

export const EditDataRequest = ({
  onSubmit,
  onClose,
  request,
  dataRequestTypeOptions,
}: EditDataRequestProps) => {
  const localSubmit = (values) => {
    const dateValues = {
      requestDate: values.requestDate ? moment(values.requestDate).format() : undefined,
    };
    onSubmit(Object.assign({}, values, dateValues));
  };

  return (
    <Form
      onSubmit={localSubmit}
      initialValues={{
        requestDate: request.requestDate,
        subjectFirstName: request.subjectFirstName,
        subjectLastName: request.subjectLastName,
        submissionMethod: request.submissionMethod,
        subjectEmailAddress: request.subjectEmailAddress,
        subjectPhoneNumber: request.subjectPhoneNumber,
        dataRequestType: request.dataRequestType.type,
      }}
      validate={validation}
      render={({ handleSubmit, ...rest }) => {
        return (
          <form onSubmit={handleSubmit}>
            <EditDataRequestForm
              {...rest}
              onCancel={onClose}
              dataRequestTypeOptions={dataRequestTypeOptions}
            />
          </form>
        );
      }}
    />
  );
};

const validation = (values) => {
  const errors = {} as any;

  const dateValues = {
    requestDate: moment(values.requestDate),
  };

  requiredString(values, "subjectFirstName", errors);
  requiredString(values, "subjectLastName", errors);
  requiredString(values, "submissionMethod", errors);
  requiredString(values, "dataRequestType", errors);
  requiredEmail(values, "subjectEmailAddress", errors);
  requiredDate(dateValues, "requestDate", errors);

  return errors;
};

const EditDataRequestForm = ({ onCancel, dataRequestTypeOptions }) => {
  return (
    <>
      <DialogContent>
        <div className="flex-col">
          <div className="flex-row flex--expand">
            <Text field="subjectFirstName" label="First Name" autoFocus required />
            <Text field="subjectLastName" label="Last Name" required />
          </div>

          <Select
            field="dataRequestType"
            label="Request Type"
            options={dataRequestTypeOptions}
            required
          />

          <Select
            field="submissionMethod"
            label="Submission Method"
            options={SUBMISSION_METHOD_OPTIONS}
            required
          />

          <Text field="subjectEmailAddress" label="Email Address" type="email" required />

          <Text field="subjectPhoneNumber" label="Telephone Number" type="tel" />

          <DatePicker field="requestDate" label="Request Date" required />
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel}>Cancel</Button>
        <Button type="submit" color="primary" variant="contained">
          Save Changes
        </Button>
      </DialogActions>
    </>
  );
};
