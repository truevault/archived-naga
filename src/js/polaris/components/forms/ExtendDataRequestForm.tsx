import { Button, DialogActions, DialogContent } from "@mui/material";
import moment from "moment";
import React from "react";
import { Form } from "react-final-form";
import { Text } from "../../../common/components/input/Text";
import { DataRequestDetail } from "../../../common/models";
import { DATE_FORMAT_MEDIUM } from "../../formatters/date";
import { requiredString } from "../../util/validation";

type ExtendDataRequestProps = {
  onSubmit: (values: any) => void;
  onClose: () => void;
  request: DataRequestDetail;
};

export const ExtendDataRequest = ({ onSubmit, request }: ExtendDataRequestProps) => {
  const localSubmit = (values) => {
    const dateValues = {
      requestDate: values.requestDate ? moment(values.requestDate).format() : undefined,
      dueDate: values.dueDate ? moment(values.dueDate).format() : undefined,
    };
    onSubmit(Object.assign({}, values, dateValues));
  };

  return (
    <Form
      onSubmit={localSubmit}
      initialValues={{
        notice: "",
      }}
      validate={validation}
      render={({ handleSubmit, ...rest }) => {
        return (
          <form onSubmit={handleSubmit}>
            <ExtendDataRequestForm
              {...rest}
              isGdpr={request.dataRequestType.regulationName == "GDPR"}
              dueDate={request.dueDate}
              requestType={request?.dataRequestType?.name ?? "request"}
            />
          </form>
        );
      }}
    />
  );
};

const validation = (values) => {
  const errors = {} as any;

  requiredString(values, "notice", errors);

  return errors;
};

const ExtendDataRequestForm = ({ isGdpr, dueDate, requestType }) => {
  const days = isGdpr ? 60 : 45;
  const newDueDate = moment(dueDate).add(days, "days");

  return (
    <>
      <DialogContent>
        <p>
          You may extend the due date once. Your new due date will be:{" "}
          <span className="text-weight-bold">{newDueDate.format(DATE_FORMAT_MEDIUM)}</span>
        </p>
        <p>Add an extension reason to the consumer notification email:</p>
        <div className="p-lg callout callout--gray">
          <div className="mb-sm">
            We need additional time to process your {requestType} and may need up to an additional{" "}
            {days} days (for a total of 90 days from the date you submitted your request). We
            require additional time in order to:
          </div>
          <Text isMultiLine={true} field="notice" name="notice" label="Extension Reason" required />
        </div>
      </DialogContent>
      <DialogActions className="border-none p-lg">
        <Button type="submit" color="primary" variant="contained">
          Extend and Notify Requester
        </Button>
      </DialogActions>
    </>
  );
};
