import {
  Button,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  TextField,
  Tooltip,
} from "@mui/material";
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import { Autocomplete } from "@mui/material";
import setFieldData from "final-form-set-field-data";
import debounce from "lodash.debounce";
import React, { useState } from "react";
import { Field, Form } from "react-final-form";
import { OnChange } from "react-final-form-listeners";
import { Link } from "react-router-dom";
import { Text } from "../../../common/components/input/Text";
import { Api } from "../../../common/service/Api";
import {
  OrganizationDataRecipientDto,
  OrganizationVendorNewAndEditDto,
} from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationPublicId } from "../../../common/service/server/Types";
import { Helper as DataInventoryHelper } from "../../../polaris/copy/dataInventory";
import { Routes } from "../../root/routes";
import { Validation } from "../../util";
import { getVendorSettingsObject } from "../../util/vendors";
import { ConfirmDialog } from "../dialog";

const validateSettings = (values) => {
  const errors = {};
  Validation.requiredString(values, "name", errors);
  Validation.optionalUrl(values, "url", errors);
  Validation.optionalEmail(values, "email", errors);
  return errors;
};

const validateNameAsync =
  (
    organizationId: OrganizationPublicId,
    currentName: string,
    existingOrganizationVendors: OrganizationDataRecipientDto[],
    setResult: (res) => void,
  ) =>
  async (value: string) => {
    const cur = (currentName || "").toLowerCase();
    const val = value.trim().toLowerCase();
    const hasAddedResult = <span>You've already added this vendor</span>;
    // Don't warn if we match the current vendor name (only when editing custom vendor)
    if (val === cur) {
      setResult("");
      return;
    }

    // If an already added organization matches the name short-circuit with already-added
    if (existingOrganizationVendors.filter((ov) => ov.name.toLowerCase() === val).length) {
      setResult(hasAddedResult);
      return;
    }

    // If neither short-circuit case then fetch any vendors matching the name from the server
    const existing = await Api.organizationVendor.searchVendors(organizationId, value);
    const match = existing.filter((e) => e.name.toLowerCase() === val);
    // If no matches then short-circuit to no warning result
    if (match.length === 0) {
      setResult("");
      return;
    }
    // If there are any matches then we show already-exists message
    // We know it's not already-added because we would have caught that above
    setResult(
      <span>
        This vendor already exists, find it{" "}
        <a target="_blank" rel="noopener noreferrer" href={Routes.vendors.catalog}>
          here
        </a>
      </span>,
    );
  };

type Props = {
  organizationId: OrganizationPublicId;
  existingOrganizationVendors: OrganizationDataRecipientDto[];
  data: OrganizationVendorNewAndEditDto;
  vendorDisclosedTo?: boolean;
  vendorSoldTo?: boolean;
  onSubmit: (values: any) => void;
};

export const VendorSettings = ({
  organizationId,
  existingOrganizationVendors,
  data,
  vendorDisclosedTo,
  vendorSoldTo,
  onSubmit,
}: Props) => {
  const [vendorValues, setVendorValues] = useState(getVendorSettingsObject(data.vendor));
  const classifications = data.classificationOptions
    .slice(0)
    .sort((a, b) =>
      a.displayOrder == b.displayOrder
        ? a.name.localeCompare(b.name)
        : a.displayOrder - b.displayOrder,
    );

  const thirdPartyClassification = classifications.find((c) => c.slug === "third-party");
  const needsReviewClassification = classifications.find((c) => c.slug === "needs-review");
  const isThirdParty = (classificationId) =>
    classificationId === thirdPartyClassification.id ||
    classificationId === needsReviewClassification.id;

  const [openReclassifyConfirm, setOpenReclassifyConfirm] = useState(false);
  const [openSellingConfirm, setOpenSellingConfirm] = useState(false);
  const [nameWarning, setNameWarning] = useState("");
  const validateNameWarning = debounce(
    validateNameAsync(
      organizationId,
      data.vendor?.name,
      existingOrganizationVendors,
      setNameWarning,
    ),
    500,
  );

  const handleClassificationChange = ({ target: { value } }) =>
    setVendorValues((prev) => {
      return Object.assign({}, prev, {
        classificationId: value,
        ccpaIsSelling: isThirdParty(value) ? prev.ccpaIsSelling : false,
      });
    });

  const handleCategoryChange = (event, value) =>
    setVendorValues((prev) => Object.assign({}, prev, { category: value }));

  const handleLocalSubmit = ({ name, url, email }) => {
    onSubmit(Object.assign({}, vendorValues, { name, url, email }));
  };

  return (
    <Form
      onSubmit={handleLocalSubmit}
      initialValues={data.vendor}
      mutators={{ setFieldData }}
      validate={validateSettings}
      render={({ handleSubmit, form }) => {
        const confirmSubmit = (values) => {
          const isNoData = !!classifications.find(
            (c) => c.id === vendorValues.classificationId && c.slug === "no-data-sharing",
          );
          if (isNoData && (vendorDisclosedTo || vendorSoldTo)) {
            setOpenReclassifyConfirm(true);
          } else if (!vendorValues.ccpaIsSelling && data.vendor?.ccpaIsSelling && vendorSoldTo) {
            setOpenSellingConfirm(true);
          } else {
            handleSubmit(values);
          }
        };
        return (
          <form onSubmit={confirmSubmit} className="service-settings">
            {(!data.vendor || data.vendor?.isCustom) && (
              <>
                <div className="p-xl">
                  <div className="flex-row input-container--settings-text">
                    <Field name="name">
                      {({ meta }) => {
                        const extraConfig: Record<string, any> = {};
                        // We should only override the helperText for warning state
                        if (!meta.error && nameWarning) {
                          extraConfig.helperText = nameWarning;
                        }
                        return (
                          <Text
                            field="name"
                            label="Name"
                            required
                            className={`input--settings-text ${nameWarning ? "input-warn" : ""}`}
                            {...extraConfig}
                          />
                        );
                      }}
                    </Field>
                    <OnChange name="name">
                      {async (value) => {
                        validateNameWarning(value);
                      }}
                    </OnChange>
                  </div>
                  <div className="flex-row mt-lg input-container--settings-text">
                    <Text field="url" label="Website URL" className="input--settings-text" />
                  </div>
                  <div className="flex-row mt-lg input-container--settings-text">
                    <Text field="email" label="Email Address" className="input--settings-text" />
                  </div>
                </div>
                <div className="pt-xl pl-xl pr-xl service-settings--category">
                  <div className="flex-row">
                    <h2 className="mt-0 mb-lg">Category</h2>
                  </div>
                  <div className="flex-row">
                    <Autocomplete
                      id="category-autocomplete"
                      className="service-settings--catgory-autocomplete"
                      options={data.existingCategories.sort()}
                      defaultValue={vendorValues.category}
                      inputValue={vendorValues.category}
                      onInputChange={handleCategoryChange}
                      freeSolo
                      renderInput={(params) => {
                        return (
                          <TextField
                            {...params}
                            id="category"
                            fullWidth={true}
                            variant="outlined"
                            placeholder="E.g., HR Contractor, Business Partner, Affiliate"
                            InputProps={{ ...params.InputProps, type: "search" }}
                            required
                          />
                        );
                      }}
                    />
                  </div>
                </div>
              </>
            )}
            <div className="p-xl">
              <div className="flex-row">
                <h2 className="mt-0 mb-0">Classification</h2>
              </div>
              <FormControl className="mt-md mb-sm w-100" component="fieldset">
                <RadioGroup
                  aria-label="classification"
                  name="classification"
                  value={vendorValues.classificationId}
                  onChange={handleClassificationChange}
                  className="classification-options"
                >
                  {classifications.map(({ id, name, tooltip }) => {
                    return (
                      <div key={`classification-option-${id}`}>
                        <div className="flex-row mt-sm">
                          <div className="flex-col" style={{ flex: 1 }}>
                            <FormControlLabel
                              className="classification-option"
                              value={id}
                              control={<Radio />}
                              label={name.toUpperCase()}
                              title={tooltip}
                            />
                          </div>
                        </div>
                        {isThirdParty(id) && (
                          <RadioGroup
                            name="ccpaIsSelling"
                            value={vendorValues.ccpaIsSelling ? "true" : "false"}
                            onChange={({ target: { value } }) =>
                              setVendorValues((prev) =>
                                Object.assign({}, prev, {
                                  ccpaIsSelling: value === "true",
                                }),
                              )
                            }
                          >
                            <div className="flex-row">
                              <FormControlLabel
                                className="ml-xl mt-sm"
                                value="true"
                                control={<Radio />}
                                label={
                                  <div className="ccpa-is-selling-label">
                                    <span>I "sell" personal information to this third party</span>
                                    <Tooltip
                                      className="ml-sm"
                                      title={DataInventoryHelper.SellingInformationHelpText}
                                    >
                                      <HelpOutlineIcon className="ccpa-is-selling-help-icon" />
                                    </Tooltip>
                                  </div>
                                }
                                disabled={id !== vendorValues.classificationId}
                                checked={
                                  id === vendorValues.classificationId && vendorValues.ccpaIsSelling
                                }
                              />
                            </div>
                            <div className="flex-row">
                              <FormControlLabel
                                className="ml-xl mt-sm"
                                value="false"
                                control={<Radio />}
                                label={`I do not "sell" personal information to this third party`}
                                disabled={id !== vendorValues.classificationId}
                                checked={
                                  id === vendorValues.classificationId &&
                                  !vendorValues.ccpaIsSelling
                                }
                              />
                            </div>
                          </RadioGroup>
                        )}
                      </div>
                    );
                  })}
                </RadioGroup>
              </FormControl>
              <div className="flex-row mt-lg">
                <Button onClick={confirmSubmit} variant="contained" color="primary">
                  Save
                </Button>
                <Button color="primary" className="ml-md" component={Link} to={Routes.vendors.root}>
                  Cancel
                </Button>
              </div>
            </div>
            <ConfirmDialog
              open={openReclassifyConfirm}
              onClose={() => setOpenReclassifyConfirm(false)}
              onConfirm={() => handleSubmit(form.getState().values)}
              confirmTitle="Reclassify vendor?"
              contentText={DataInventoryHelper.NoDataSharingDisclaimer}
              confirmText="Yes"
            />
            <ConfirmDialog
              open={openSellingConfirm}
              onClose={() => setOpenSellingConfirm(false)}
              onConfirm={() => handleSubmit(form.getState().values)}
              confirmTitle="Remove selling?"
              contentText={DataInventoryHelper.NoSellingDisclaimer}
              confirmText="Yes"
            />
          </form>
        );
      }}
    />
  );
};
