import { Button } from "@mui/material";
import Alert from "@mui/material/Alert";
import React from "react";
import { Form } from "react-final-form";
import { Link } from "react-router-dom";
import { Text } from "../../../common/components/input/Text";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { Routes } from "../../root/routes";
import { requiredString, requiredEmail } from "../../util/validation";

type LoginProps = {
  onSubmit: (values: any) => void;
  loginRequest: any;
  error: boolean;
};

export const Login = ({ onSubmit, loginRequest, error }: LoginProps) => {
  return (
    <Form
      onSubmit={onSubmit}
      validate={validation}
      render={(props) => <LoginForm {...props} loginRequest={loginRequest} error={error} />}
    />
  );
};

const validation = (values) => {
  const errors = {} as any;

  requiredEmail(values, "email", errors);
  requiredString(values, "password", errors);

  return errors;
};

const LoginForm = ({ handleSubmit, loginRequest, error }) => {
  return (
    <form onSubmit={handleSubmit}>
      {error && (
        <Alert severity="error" className="mb-md">
          {error}
        </Alert>
      )}

      <Text
        autoFocus
        field="email"
        label="Email Address"
        variant="outlined"
        className="mb-md"
        required
      />
      <Text
        field="password"
        label="Password"
        variant="outlined"
        type="password"
        className="mb-md"
        required
      />

      <LoadingButton
        type="submit"
        color="primary"
        size="large"
        loading={loginRequest.running}
        className="mr-sm"
      >
        Log In
      </LoadingButton>

      <Button component={Link} to={Routes.reset} variant="text">
        Forgot password?
      </Button>
    </form>
  );
};
