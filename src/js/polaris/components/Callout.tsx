import * as MuiIcon from "@mui/icons-material";
import { ChevronRight, ExpandMore, SvgIconComponent } from "@mui/icons-material";
import { Card, CardContent, CardHeader, Collapse, IconButton } from "@mui/material";
import clsx from "clsx";
import React, { createElement, ReactNode, useEffect, useState } from "react";
// import { Rule} from "@mui/icons-material";

export enum CalloutVariant {
  LightGray = "light-gray",
  LightGrayBordered = "light-gray--bordered",
  Gray = "gray",
  Purple = "purple",
  PurpleBordered = "purple--bordered",
  LightPurple = "light-purple",
  LightPurpleBordered = "light-purple--bordered",
  Green = "green",
  Red = "red",
  Yellow = "yellow",
  Clear = "clear",
}

export const Callout: React.FC<{
  icon?: SvgIconComponent | React.ReactNode | string;
  children?: React.ReactNode;
  __dangerousChildrenHtml?: string;
  variant?: CalloutVariant;
  padding?: "md" | "lg" | "0";
  className?: string;
  cardContentClassName?: string;
  title?: React.ReactNode;
  subtitle?: React.ReactNode;
  collapsible?: boolean;
  collapsed?: boolean;
  hideCollapseToggle?: boolean;
  onCollapse?: (collapsed: boolean) => void;
  action?: React.ReactNode;
}> = ({
  children,
  __dangerousChildrenHtml,
  variant = CalloutVariant.Gray,
  title,
  className,
  cardContentClassName,
  collapsible = false,
  collapsed = false,
  hideCollapseToggle = false,
  icon,
  subtitle,
  action,
  onCollapse,
  ...rest
}) => {
  const [showChildren, setShowChildren] = useState(() => {
    if (!collapsible) {
      return true;
    }

    return !collapsed;
  });

  useEffect(() => {
    setShowChildren(!collapsed);
  }, [collapsed]);

  const handleCollapse = () => {
    const newShowState = !showChildren;
    setShowChildren(newShowState);

    if (onCollapse) {
      onCollapse(newShowState);
    }
  };

  let iconComponent: ReactNode | null;
  if (icon && React.isValidElement(icon)) {
    iconComponent = icon;
  } else if (icon && typeof icon === "string") {
    const Icon = MuiIcon[icon];
    if (!Icon) {
      console.warn("Not able to find a MuiIcon for: ", icon);
      console.warn("Available icons are: ", MuiIcon);
    } else {
      iconComponent = createElement(Icon, { color: "inherit", className: "callout__icon" });
    }
  } else if (icon) {
    const Icon: SvgIconComponent = icon as SvgIconComponent;
    iconComponent = <Icon color="inherit" className="callout__icon" />;
  }

  const collapseAction = collapsible ? (
    <IconButton size="small" onClick={handleCollapse}>
      {showChildren ? <ExpandMore /> : <ChevronRight />}
    </IconButton>
  ) : undefined;

  let dangerousHtml: { __html: string } | undefined = undefined;
  if (__dangerousChildrenHtml) {
    dangerousHtml = { __html: __dangerousChildrenHtml };
  }

  return (
    <Card
      className={clsx(
        "callout",
        `callout--${variant}`,
        { "callout--collapsed": !showChildren },
        className,
      )}
      {...rest}
      elevation={0}
    >
      {Boolean(title) && (
        <CardHeader
          avatar={iconComponent}
          title={title}
          subheader={subtitle}
          className="callout__header"
          action={
            <div className="d-flex flex-row align-items-center">
              {action}
              {!hideCollapseToggle && collapseAction}
            </div>
          }
        />
      )}

      {(children || dangerousHtml) && (
        <Collapse in={showChildren}>
          <CardContent className={cardContentClassName}>
            {dangerousHtml ? <div dangerouslySetInnerHTML={dangerousHtml} /> : children}
          </CardContent>
        </Collapse>
      )}
    </Card>
  );
};
