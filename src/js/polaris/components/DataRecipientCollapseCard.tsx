import React from "react";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { Callout, CalloutVariant } from "./Callout";
import { DataRecipientLogo } from "./DataRecipientLogo";

type DataRecipientCollapseCardProps = {
  recipient: OrganizationDataRecipientDto | OrganizationDataSourceDto;
  recipientName?: string;
  collapsed?: boolean;
  variant?: CalloutVariant;
  onExpand?: () => void;
  collapsible?: boolean;
  headerContent?: React.ReactNode;
  className?: string;
};

export const DataRecipientCollapseCard: React.FC<DataRecipientCollapseCardProps> = ({
  recipient,
  recipientName,
  collapsed,
  onExpand,
  children,
  variant = CalloutVariant.Clear,
  collapsible = true,
  headerContent = null,
  className = undefined,
}) => {
  return (
    <Callout
      action={headerContent}
      onCollapse={onExpand}
      collapsible={collapsible}
      collapsed={collapsed}
      icon={<DataRecipientLogo dataRecipient={recipient} size={32} />}
      title={recipientName ?? recipient.name}
      className={className}
      variant={variant}
      data-cy="data-recipient-card"
      data-cy-key={recipient.name}
    >
      {children}
    </Callout>
  );
};
