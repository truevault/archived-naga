import React from "react";
import clsx from "clsx";
import _ from "lodash";
import {
  PrivacyCenterDto,
  PrivacyCenterPolicySectionDto,
} from "../../common/service/server/dto/PrivacyCenterDto";
import { IconButton } from "@mui/material";
import { KeyboardArrowDown, KeyboardArrowUp } from "@mui/icons-material";
import { ExternalLink } from "../../common/components/ExternalLink";
import { PrivacyCenterUrls } from "../util/privacyCenterUtil";
import { RouteHelpers } from "../root/routes";
import { stripTags } from "../util/text";

type CustomSectionCalloutProps = {
  privacyCenter: PrivacyCenterDto;
  section: PrivacyCenterPolicySectionDto;
  isNew: boolean;
  isFirst: boolean;
  isLast: boolean;

  onMoveUp: () => void;
  onMoveDown: () => void;
  onDelete: () => void;
};
export const CustomSectionCallout: React.FC<CustomSectionCalloutProps> = ({
  privacyCenter,
  section,
  isNew,
  isFirst,
  isLast,
  onMoveUp,
  onMoveDown,
  onDelete,
}) => {
  const title = (
    <ExternalLink
      href={PrivacyCenterUrls.getPrivacyPolicySectionUrl(privacyCenter, section.anchor)}
      heavy
      icon
    >
      {section.name}
    </ExternalLink>
  );

  const actions = (
    <>
      <a
        href={RouteHelpers.privacyCenter.editPrivacySection(section.id)}
        className="text-weight-medium"
      >
        Edit
      </a>

      <a
        href="#"
        className="text-weight-medium ml-mdlg"
        onClick={(e) => {
          e.preventDefault();
          onDelete();
        }}
      >
        Remove
      </a>
    </>
  );
  return (
    <SectionCallout
      arrows
      isFirst={isFirst}
      isLast={isLast}
      onMoveUp={onMoveUp}
      onMoveDown={onMoveDown}
      title={title}
      actions={actions}
      highlight={isNew ? "success" : undefined}
    >
      <FadedSectionBody body={stripTags(section.body)} />
    </SectionCallout>
  );
};

export const FadedSectionBody = ({ body }: { body: string }) => {
  return (
    <div className="text-fade">
      <p>{_.truncate(body, { length: 240 })}</p>
    </div>
  );
};

type SectionCalloutProps = {
  title: React.ReactNode;
  actions?: React.ReactNode;
  children: React.ReactNode;

  arrows?: boolean;
  isFirst?: boolean;
  isLast?: boolean;

  variant?: "white" | "dark";

  highlight?: "success";

  onMoveUp?: () => void;
  onMoveDown?: () => void;
};
export const SectionCallout: React.FC<SectionCalloutProps> = ({
  title,
  actions,
  children,
  highlight,
  variant = "white",

  arrows = false,
  isFirst = false,
  isLast = false,
  onMoveUp = () => {},
  onMoveDown = () => {},
}) => {
  return (
    <div
      className={clsx("callout section-callout p-sm mb-mdlg", {
        "border-color-green-300": highlight == "success",
        "border-color-neutral-300": !highlight,
        "section-callout--dark": variant == "dark",
        "section-callout--white": variant == "white",
      })}
    >
      <div className="flex-row flex-grow">
        {arrows && (
          <div className="flex-col">
            {!isFirst && (
              <IconButton aria-label="move up" onClick={onMoveUp}>
                <KeyboardArrowUp fontSize="small" />
              </IconButton>
            )}

            {!isLast && (
              <IconButton aria-label="move down" onClick={onMoveDown}>
                <KeyboardArrowDown fontSize="small" />
              </IconButton>
            )}
          </div>
        )}
        <div className="flex-col w-100">
          <div className="flex-row flex-grow flex--align-center">
            <div className="text-body-1-bold flex-grow mt-xs">{title}</div>
            {Boolean(actions) && <div>{actions}</div>}
          </div>
          <div>{children}</div>
        </div>
      </div>
    </div>
  );
};
