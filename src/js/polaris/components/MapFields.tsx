import { Divider } from "@mui/material";
import _ from "lodash";
import React, { useEffect, useMemo, useState } from "react";
import { CollectionGroupDetailsDto } from "../../common/service/server/dto/CollectionGroupDto";
import { OrganizationDto } from "../../common/service/server/dto/OrganizationDto";
import { PersonalInformationCategoryDto } from "../../common/service/server/dto/PersonalInformationOptionsDto";
import { UUIDString } from "../../common/service/server/Types";
import { GroupIcon } from "../pages/get-compliant/data-map/GroupIcon";
import { interweave } from "../util/array";
import { getGroupedCategories, getOrderedGroups, OrderingVariant } from "../util/dataInventory";
import { Callout, CalloutVariant } from "./Callout";
import { DescriptionCheckbox } from "./input/DescriptionCheckbox";

type MapFieldsProps = {
  organization: OrganizationDto;
  consumerGroup: CollectionGroupDetailsDto;
  personalInformationCategories: PersonalInformationCategoryDto[];
  recommendedPersonalInformationCategories?: string[];
  sharedCategories: Set<UUIDString>;
  lockedInCategories?: Set<UUIDString>;
  noDataDisclosed?: Record<UUIDString, boolean | null>;
  setNoDataDisclosed?: React.Dispatch<React.SetStateAction<Record<string, boolean>>>;
  showNoDataDisclosed?: boolean;
  orderingVariant?: OrderingVariant;
  name?: string;
  generalGuidance?: string;
  specialCircumstancesGuidance?: string;
  update?: (a: any) => void;
  updateSingle?: (category_id: UUIDString, checked: boolean) => void;
  confirmRemoveHandler?: (a: any) => void;
};

const GDPR_GROUP_NAME = "GDPR Sensitive Data";

export const MapFields: React.FC<MapFieldsProps> = ({
  organization,
  consumerGroup,
  sharedCategories,
  lockedInCategories,
  personalInformationCategories,
  recommendedPersonalInformationCategories,
  noDataDisclosed,
  setNoDataDisclosed,
  name,
  generalGuidance,
  specialCircumstancesGuidance,
  update,
  updateSingle,
  confirmRemoveHandler,
  showNoDataDisclosed = true,
  orderingVariant = "default",
}) => {
  recommendedPersonalInformationCategories = recommendedPersonalInformationCategories || [];
  const [lastConsumerGroup, setLastConsumerGroup] = useState<CollectionGroupDetailsDto | null>(
    null,
  );

  const handleNoCategoriesChecked = (noDataMapping: boolean) => {
    if (consumerGroup) {
      setNoDataDisclosed?.((prev) =>
        Object.assign({}, prev, { [consumerGroup.id]: noDataMapping }),
      );
    }
  };

  // This ensures that a server update occurs after the noDataDiscoldedProperty gets set
  useEffect(() => {
    if (consumerGroup == lastConsumerGroup) {
      update?.(new Set());
    }
    setLastConsumerGroup(consumerGroup);
  }, [consumerGroup, noDataDisclosed]);

  const handleCategoryChecked = (categoryId: UUIDString, checked: boolean) => {
    const shared = new Set(sharedCategories);

    if (checked) {
      shared.add(categoryId);
    } else {
      if (confirmRemoveHandler) {
        confirmRemoveHandler(categoryId);
        return;
      } else {
        shared.delete(categoryId);
      }
    }
    if (update) update(shared);
    if (updateSingle) updateSingle(categoryId, checked);
  };

  const [orderedGroups, setOrderedGroups] = useState<string[]>([]);
  const [groupedCategories, setGroupedCategories] = useState<
    Record<string, PersonalInformationCategoryDto[]>
  >({});

  useEffect(() => {
    const uniqueCategories: PersonalInformationCategoryDto[] = _.uniqBy(
      personalInformationCategories,
      "id",
    );
    setOrderedGroups(getOrderedGroups(uniqueCategories, orderingVariant));
    setGroupedCategories(getGroupedCategories(uniqueCategories));
  }, [personalInformationCategories]);

  const hasSelectedPiCategories = useMemo(() => {
    return Boolean(consumerGroup) && sharedCategories?.size > 0;
  }, [consumerGroup, sharedCategories]);

  const noDisclosure = noDataDisclosed ? noDataDisclosed[consumerGroup?.id] ?? null : null;

  const filteredGroups = orderedGroups.filter((g) => {
    const isEeaUk = consumerGroup.processingRegions.includes("EEA_UK");
    const isGdpr = organization.featureGdpr;

    if (g === GDPR_GROUP_NAME && (!isEeaUk || !isGdpr)) {
      return false;
    }

    return true;
  });

  return (
    <div className="map-vendor-data">
      {showNoDataDisclosed && (
        <>
          <div className="map-fields-header">No Data Disclosure</div>
          <DescriptionCheckbox
            key={"no_data_disclosure"}
            id={"no_data_disclosure_input"}
            color="primary"
            label={`${name} does not handle any data about ${consumerGroup.name}`}
            disabled={hasSelectedPiCategories}
            checked={Boolean(noDisclosure)}
            onChange={(_, checked) => {
              handleNoCategoriesChecked(checked);
            }}
          />
          <Divider />
        </>
      )}
      <div className="mb-xl" />

      {Boolean(generalGuidance || specialCircumstancesGuidance) && (
        <>
          {Boolean(generalGuidance) && (
            <>
              <Callout variant={CalloutVariant.Purple} __dangerousChildrenHtml={generalGuidance} />
            </>
          )}
          {Boolean(specialCircumstancesGuidance) && (
            <>
              <div className="mt-xs" />
              <Callout
                variant={CalloutVariant.Yellow}
                __dangerousChildrenHtml={specialCircumstancesGuidance}
              />
            </>
          )}
        </>
      )}

      <div className="mb-lg" />

      {filteredGroups.map((og) => {
        return (
          <div key={`group-${og}`} className="map-vendor-data--category-group">
            <div className={"category-icon"}>
              <GroupIcon groupName={og} />
            </div>

            <div className="category-content">
              <h2 className="category-title">{og}</h2>

              <div className="category-options">
                {interweave(groupedCategories[og].sort((a, b) => a.order - b.order)).map(
                  (col, i) => {
                    if (col === undefined) {
                      return null;
                    }
                    return (
                      <ul key={`col-${i}`}>
                        {col.map((c) => {
                          const inputId = `cg-chk-${c.id}`;
                          const checked =
                            Boolean(sharedCategories?.has(c.id)) ||
                            Boolean(lockedInCategories?.has(c.id));
                          const disabled =
                            (noDisclosure && !hasSelectedPiCategories) ||
                            Boolean(lockedInCategories?.has(c.id));
                          return (
                            <li key={`row-${i}-${c.id}`}>
                              {/* Only want checkboxClassname="description-checkbox--checkbox--aligned" if there's hints to be had */}
                              <DescriptionCheckbox
                                key={inputId}
                                id={inputId}
                                label={c.name}
                                description={c.description}
                                color="primary"
                                checkboxClassname="description-checkbox--checkbox--aligned"
                                variant={
                                  recommendedPersonalInformationCategories?.includes(c.id)
                                    ? "light-purple"
                                    : undefined
                                }
                                checked={checked}
                                disabled={disabled}
                                onChange={(e, checked) => {
                                  handleCategoryChecked(c.id, checked);
                                }}
                              />
                            </li>
                          );
                        })}
                      </ul>
                    );
                  },
                )}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};
