import clsx from "clsx";
import React from "react";

export interface ProgressBarsProps {
  totalProgress: number;
  currentProgress: number;
}

export const ProgressBars: React.FC<ProgressBarsProps> = ({ totalProgress, currentProgress }) => {
  return (
    <>
      {[...Array(totalProgress).keys()].map((index) => {
        return (
          <span
            key={`progress-component-${index}`}
            className={clsx("progress-component", { filled: index < currentProgress })}
          ></span>
        );
      })}
    </>
  );
};
