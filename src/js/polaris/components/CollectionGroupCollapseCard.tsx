import { ChevronRight, ExpandMore } from "@mui/icons-material";
import { Collapse, IconButton, Typography } from "@mui/material";
import clsx from "clsx";
import React from "react";
import { CollectionGroupDetailsDto } from "../../common/service/server/dto/CollectionGroupDto";
import { Callout, CalloutVariant } from "./Callout";

type Props = {
  collectionGroup: CollectionGroupDetailsDto;
  collapsed?: boolean;
  variant?: CalloutVariant;
  onExpand?: () => void;
  collapsible?: boolean;
  headerContent?: React.ReactNode;
  className?: string;
};

export const CollectionGroupCollapseCard: React.FC<Props> = ({
  collectionGroup,
  collapsed,
  onExpand,
  children,
  variant = CalloutVariant.Clear,
  collapsible = true,
  headerContent = null,
  className = undefined,
}) => {
  return (
    <Callout className={className} variant={variant}>
      <header
        className={clsx("flex-row w-100 align-items-center", {
          "mb-md": Boolean(children) && !collapsed,
          "cursor-pointer": collapsible,
        })}
        onClick={collapsible ? onExpand : undefined}
      >
        <div className="flex-grow d-flex flex-row align-items-center">
          <div>
            <h5 className="my-0">{collectionGroup.name}</h5>
            <Typography variant="caption">{collectionGroup.description}</Typography>
          </div>
        </div>

        <div className="ml-auto">
          {headerContent}

          {collapsible && (
            <IconButton onClick={onExpand}>
              {collapsed ? <ChevronRight /> : <ExpandMore />}
            </IconButton>
          )}
        </div>
      </header>

      {Boolean(children) && <Collapse in={!collapsed}>{children}</Collapse>}
    </Callout>
  );
};
