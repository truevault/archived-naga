import clsx from "clsx";
import React from "react";

type SettingRowProps = {
  title: React.ReactNode;
  helpText?: React.ReactNode;
  children: React.ReactNode;
  footer?: React.ReactNode;
  vertical?: boolean;
  maxWidthChildren?: boolean;
};

export const SettingRow: React.FC<SettingRowProps> = ({
  title,
  helpText,
  children,
  maxWidthChildren = false,
  vertical = false,
  footer,
}) => {
  return (
    <>
      <div className={clsx({ "flex-col": vertical, "flex-row": !vertical })}>
        <div className={clsx("flex-col flex--align-start", { "w-300": !vertical })}>
          <div className="text-body-1-medium mb-xs">{title}</div>
          <div className="text-t1 text-muted">{helpText}</div>
        </div>
        <div className={clsx({ "w-100": !vertical && maxWidthChildren, "ml-lg": !vertical })}>
          {children}
        </div>
      </div>
      {Boolean(footer) && <div>{footer}</div>}
    </>
  );
};
