import React from "react";
import { Button } from "@mui/material";
import { Form } from "react-final-form";
import { Checkbox } from "../../../../../common/components/input/Checkbox";
import { Text } from "../../../../../common/components/input/Text";
import { SettingRow } from "./SettingRow";

interface Props {
  customFieldEnabled: boolean;
  customFieldHelp: string;
  customFieldLabel: string;
  handleCustomFieldSubmit: (values) => void;
}
export const PrivacyCenterRequestFormSettingsRow: React.FC<Props> = ({
  customFieldEnabled,
  customFieldHelp,
  customFieldLabel,
  handleCustomFieldSubmit,
}) => {
  const helpText =
    "The default request form asks for name and email; you can request additional information by enabling a custom field";
  return (
    <SettingRow title="Request Form" helpText={helpText} maxWidthChildren={true}>
      <Form
        onSubmit={handleCustomFieldSubmit}
        initialValues={{
          customFieldEnabled: customFieldEnabled,
          customFieldHelp: customFieldHelp,
          customFieldLabel: customFieldLabel,
        }}
        render={(props) => {
          return (
            <form onSubmit={props.handleSubmit} className="flex-grow">
              <Checkbox
                field="customFieldEnabled"
                color="primary"
                label="Enable Custom Field"
                className="mb-md"
              />
              {props.values.customFieldEnabled && (
                <>
                  <label htmlFor={"customFieldLabel"} className="text-t3 mr-md">
                    Text Field Label
                  </label>
                  <Text
                    className="align-middle w-300"
                    fullWidth={false}
                    field="customFieldLabel"
                    required
                    variant="outlined"
                    validate={(str) => {
                      if (!str?.trim()) {
                        return "Required";
                      }
                      if (str.trim().length > 50) {
                        return "Max of 50 Characters";
                      }
                    }}
                  />
                  <label htmlFor="customFieldHelp" className="text-t3 d-block mt-md mb-md">
                    Help Text (max 100 characters)
                  </label>
                  <Text
                    fullWidth
                    field="customFieldHelp"
                    multiline
                    rows={4}
                    variant="outlined"
                    validate={(str) => {
                      if (str?.trim()?.length > 100) {
                        return "Max of 100 Characters";
                      }
                    }}
                  />
                </>
              )}

              {!props.pristine && (
                <Button
                  className="w-192 mt-md"
                  variant="contained"
                  type="submit"
                  disabled={props.invalid || props.submitting}
                >
                  Save
                </Button>
              )}
            </form>
          );
        }}
      />
    </SettingRow>
  );
};
