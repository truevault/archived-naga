import { Button } from "@mui/material";
import React, { useState } from "react";
import { LogoUploadDialog } from "../../..";
import { OrganizationPublicId } from "../../../../../common/service/server/Types";
import { EXTENDED_FILE_TYPES } from "../../../dialog/LogoUploadDialog";
import { CloseButton } from "../../../molecules/CloseButton";
import { SettingRow } from "./SettingRow";

type Props = {
  organizationId: OrganizationPublicId;
  logoUrl: string;
  handleFormSubmit: (values: any) => void;
  vertical?: boolean;
};

const helpText = "Horizontal logo in .svg preferred; maximum file size is 1MB; max height of 100px";

export const PrivacyCenterHeaderLogoSettingRow: React.FC<Props> = ({
  organizationId,
  logoUrl,
  handleFormSubmit,
  vertical = false,
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <SettingRow title="Header Logo" helpText={helpText} vertical={vertical}>
      {logoUrl && (
        <div className="image-control website mb-mdlg">
          <img src={logoUrl} />
          <span className="close-icon">
            <CloseButton onClose={() => handleFormSubmit({ logoUrl: null })} />
          </span>
        </div>
      )}

      <Button variant="contained" className="w-192" onClick={() => setDialogOpen(true)}>
        Upload
      </Button>
      <LogoUploadDialog
        acceptedTypes={EXTENDED_FILE_TYPES}
        logoType="LOGO"
        title="Upload Website Header Logo"
        organizationId={organizationId}
        open={dialogOpen}
        onClose={() => setDialogOpen(false)}
        onDone={(logoUrl: string) => handleFormSubmit({ logoUrl })}
        successMessage={undefined}
      />
    </SettingRow>
  );
};
