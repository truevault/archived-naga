import { Button } from "@mui/material";
import clsx from "clsx";
import React, { useMemo, useState } from "react";
import { ExternalLink } from "../../../../../common/components/ExternalLink";
import { Callout, CalloutVariant } from "../../../Callout";
import { DpoValues, EditDpoDialog } from "../../../dialog/EditDpoDialog";
import { SettingRow } from "./SettingRow";

const getDefaultHelpText = (dpoRequired: boolean) =>
  dpoRequired ? (
    <>
      Add the contact details of your DPO. If you don’t have a DPO, you can add this information
      later. We’ll send a separate reminder with tips on how to select a DPO.
      <br />
      <ExternalLink href="https://help.truevault.com/article/162-data-protection-officers-dpos">
        What is a DPO?
      </ExternalLink>
    </>
  ) : (
    <>
      You aren’t required to have a DPO, but if you have one we’ll include their contact information
      in your Privacy Center.
      <ExternalLink href="https://help.truevault.com/article/162-data-protection-officers-dpos">
        What is a DPO?
      </ExternalLink>
    </>
  );

type Props = {
  dpoName: string | undefined;
  dpoEmail: string | undefined;
  dpoPhone: string | undefined;
  dpoAddress: string | undefined;
  requiresDpo: boolean;
  hasDpoTask: boolean;
  handleFormSubmit: (values) => void;
  handleAddTask: () => Promise<void>;
  helpText?: React.ReactNode;
  vertical?: boolean;
  showSkip?: boolean;
  hideFooter?: boolean;
};
export const PrivacyCenterDpoRow: React.FC<Props> = ({
  dpoName,
  dpoEmail,
  dpoPhone,
  dpoAddress,
  requiresDpo,
  hasDpoTask,
  handleFormSubmit,
  handleAddTask,
  helpText = null,
  vertical = false,
  hideFooter = false,
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  const hasDpo = Boolean(dpoName) && Boolean(dpoEmail);

  const title = requiresDpo ? "Data Protection Officer" : "Data Protection Officer (optional)";

  helpText = useMemo(() => helpText ?? getDefaultHelpText(requiresDpo), [helpText, requiresDpo]);

  const footer = hasDpoTask ? (
    <Callout className="mt-mdlg" padding="md" variant={CalloutVariant.Yellow}>
      Great, we’ll create a DPO Task List item at the end of onboarding.
    </Callout>
  ) : null;

  const canDoItLater = requiresDpo && !hasDpo && !hasDpoTask;

  return (
    <SettingRow
      title={title}
      helpText={helpText}
      footer={hideFooter ? null : footer}
      vertical={vertical}
    >
      {hasDpo && (
        <div className="text-t3 mb-md">
          <p className="mt-0">{dpoName}</p>
          <p>{dpoEmail}</p>
        </div>
      )}

      <div className={clsx("align-items-center", { "flex-col": !vertical, "flex-row": vertical })}>
        <Button variant="contained" onClick={() => setDialogOpen(true)}>
          {hasDpo ? "Edit Data Privacy Officer" : "Set Data Privacy Officer"}
        </Button>

        {canDoItLater && (
          <Button className={clsx(!vertical && "mt-md")} color="secondary" onClick={handleAddTask}>
            Do it later
          </Button>
        )}
      </div>

      <EditDpoDialog
        open={dialogOpen}
        onClose={() => setDialogOpen(false)}
        onDone={(values: DpoValues) =>
          handleFormSubmit({
            dpoOfficerName: values.name,
            dpoOfficerEmail: values.email,
            dpoOfficerPhone: values.phone,
            dpoOfficerAddress: values.address,
          })
        }
        values={{
          name: dpoName,
          email: dpoEmail,
          phone: dpoPhone,
          address: dpoAddress,
        }}
      />
    </SettingRow>
  );
};
