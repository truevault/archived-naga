import { Button } from "@mui/material";
import React, { useState } from "react";
import { LogoUploadDialog } from "../../..";
import { OrganizationPublicId } from "../../../../../common/service/server/Types";
import { CloseButton } from "../../../molecules/CloseButton";
import { SettingRow } from "./SettingRow";

type Props = {
  organizationId: OrganizationPublicId;
  faviconUrl: string;
  handleFormSubmit: (values) => void;
  vertical?: boolean;
};

const helpText = "32x32 or 64x64 logo in .png or .svg file preferred";

export const PrivacyCenterFaviconSettingRow: React.FC<Props> = ({
  organizationId,
  faviconUrl,
  handleFormSubmit,
  vertical = false,
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <SettingRow title="Favicon (optional)" helpText={helpText} vertical={vertical}>
      {faviconUrl && (
        <div className="image-control image-control--small favicon mb-md">
          <img src={faviconUrl} />
          <span className="close-icon">
            <CloseButton onClose={() => handleFormSubmit({ faviconUrl: "" })} />
          </span>
        </div>
      )}

      <Button className="w-192" variant="contained" onClick={() => setDialogOpen(true)}>
        Upload
      </Button>

      <LogoUploadDialog
        logoType="FAVICON"
        title="Upload Favicon"
        organizationId={organizationId}
        open={dialogOpen}
        onClose={() => setDialogOpen(false)}
        onDone={(faviconUrl: string) => handleFormSubmit({ faviconUrl })}
        successMessage={undefined}
      />
    </SettingRow>
  );
};
