import React from "react";
import { Form } from "react-final-form";
import { Checkbox } from "../../../../../common/components/input/Checkbox";
import { SettingRow } from "./SettingRow";

type CAResidencyFields = {
  disableCaResidencyConfirmation: boolean;
};

interface Props {
  caResidencyFields: CAResidencyFields;
  handleFormSubmit: (fields: CAResidencyFields) => void;
}
export const PrivacyCenterCaResidencySettingsRow: React.FC<Props> = ({
  caResidencyFields,
  handleFormSubmit,
}) => {
  const helpText =
    "Check this box to allow people who live outside of required regions to submit privacy requests.";
  return (
    <SettingRow title="Residency" helpText={helpText}>
      <Form
        initialValues={caResidencyFields}
        onSubmit={handleFormSubmit}
        render={({ handleSubmit }) => {
          return (
            <Checkbox
              field="disableCaResidencyConfirmation"
              color="primary"
              label="Turn off residency confirmation"
              className="mb-md"
              afterChange={handleSubmit}
            />
          );
        }}
      />
    </SettingRow>
  );
};
