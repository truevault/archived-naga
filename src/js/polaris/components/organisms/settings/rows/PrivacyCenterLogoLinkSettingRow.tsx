import { Button } from "@mui/material";
import React, { useState } from "react";
import { EditLogoLinkDialog } from "../../../dialog/EditLogoLinkDialog";
import { SettingRow } from "./SettingRow";

type Props = {
  logoLinkUrl: string;
  handleFormSubmit: (values) => void;
  vertical?: boolean;
};

const helpText =
  "Add your website URL so consumers can get back to your site when they click on your logo";

export const PrivacyCenterLogoLinkSettingRow: React.FC<Props> = ({
  logoLinkUrl,
  handleFormSubmit,
  vertical = false,
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <SettingRow title="Home Page URL" helpText={helpText} vertical={vertical}>
      {/* Renamed to home page url, decided to leave*/}
      {Boolean(logoLinkUrl) && <div className="text-t3 mb-md">{logoLinkUrl}</div>}
      <Button className="w-192" variant="contained" onClick={() => setDialogOpen(true)}>
        {logoLinkUrl ? "Edit URL" : "Add URL"}
      </Button>
      <EditLogoLinkDialog
        linkUrl={logoLinkUrl}
        open={dialogOpen}
        onClose={() => setDialogOpen(false)}
        onDone={(logoLinkUrl: string) => handleFormSubmit({ logoLinkUrl })}
      />
    </SettingRow>
  );
};
