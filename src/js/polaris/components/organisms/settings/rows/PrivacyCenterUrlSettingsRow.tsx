import React from "react";
import { SettingRow } from "./SettingRow";

type Props = {
  url: string;
  vertical?: boolean;
};

export const PrivacyCenterUrlSettingRow: React.FC<Props> = ({ url, vertical = false }) => {
  return (
    <SettingRow title="Privacy Center URL" vertical={vertical}>
      <p className="mt-0">
        <a
          href={url}
          target="_blank"
          className="regular-link neutral-900"
          rel="noopener noreferrer"
        >
          {url}
        </a>
      </p>
      <p>
        To update your Privacy Center URL, contact us at{" "}
        <a className="regular-link" href="mailto:help@truevault.com">
          help@truevault.com
        </a>{" "}
        or{" "}
        <a
          href="#"
          className="regular-link"
          onClick={(e) => {
            e.preventDefault();
            (window as any).Beacon("open");
          }}
        >
          chat with us
        </a>
        .
      </p>
    </SettingRow>
  );
};
