import { Alert, Button } from "@mui/material";
import { CheckCircle } from "@mui/icons-material";
import React from "react";
import { ExternalLink } from "../../../../../common/components/ExternalLink";
import { SettingRow } from "./SettingRow";

type Props = {
  mailbox: string;
  mailboxIsConnected: boolean;
  mailboxOauthUrl: string;
  sendTestMessage: () => void;
  testMessageRunning: boolean;
  deleteMailbox: () => void;
  vertical?: boolean;
  label?: string;
};

const helpText =
  "Connect your privacy email account so your team can process consumer privacy requests from Polaris.";

export const EmailAccountSettingRow: React.FC<Props> = ({
  mailbox,
  mailboxIsConnected,
  mailboxOauthUrl,
  sendTestMessage,
  deleteMailbox,
  vertical = false,
  label = "Email Account",
}) => {
  return (
    <SettingRow title={label} helpText={helpText} vertical={vertical}>
      <div className="flex-col">
        {Boolean(mailbox) && !mailboxIsConnected && (
          <Alert color="error">
            There was an issue connecting to your email account; please try again.
          </Alert>
        )}

        {Boolean(mailbox) && mailboxIsConnected ? (
          <>
            <div className="flex-container">
              <div className="flex-row flex-center-vertical">
                <CheckCircle className="email-status-ok mr-sm" />
                <span>{mailbox}</span>
              </div>

              <Button variant="text" color="secondary" className="ml-md" onClick={sendTestMessage}>
                send test email
              </Button>
            </div>
            <Button className="w-fit" variant="contained" onClick={deleteMailbox}>
              Disconnect Email
            </Button>
          </>
        ) : (
          <>
            <Button variant="contained" href={mailboxOauthUrl} className="w-192">
              Connect Email
            </Button>
          </>
        )}
        <p className="text-body-2">
          Using Google Workspace?{" "}
          <ExternalLink
            className="regular-link text-medium"
            href="https://help.truevault.com/article/130-privacy-inbox-setup-g-suite"
            target="_blank"
          >
            Follow the authorization instructions
          </ExternalLink>
        </p>
      </div>
    </SettingRow>
  );
};
