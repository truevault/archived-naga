import React from "react";
import { Form } from "react-final-form";
import { Text } from "../../../../../common/components/input/Text";
import { SettingRow } from "./SettingRow";

type SignatureFormValues = {
  signature: string;
};

type EmailSignatureSettingProps = {
  signature: string;
  defaultSignature: string;
  onSignatureChange: (signature: string) => void;
  vertical?: boolean;
};

const helpText =
  "This signature will be automatically added to every email Polaris sends on your behalf.";

export const EmailSignatureSettingRow: React.FC<EmailSignatureSettingProps> = ({
  signature,
  defaultSignature,
  onSignatureChange,
  vertical,
}) => {
  const handleFormSubmit = (values: SignatureFormValues) => {
    onSignatureChange(values.signature);
  };

  const initialSignature = signature ? signature : defaultSignature;

  return (
    <SettingRow title="Email Signature" vertical={vertical} helpText={helpText} maxWidthChildren>
      <Form
        initialValues={{ signature: initialSignature }}
        onSubmit={handleFormSubmit}
        render={({ values, handleSubmit, form }) => {
          return (
            <>
              <Text
                className="signature-input w-100"
                fullWidth={false}
                required
                validate={(v) => (v ? undefined : "An email signature is required")}
                field="signature"
                multiline
                customHelperText={
                  values.signature !== defaultSignature ? (
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        form.change("signature", defaultSignature);
                      }}
                      href="#"
                    >
                      Reset Signature to Default
                    </a>
                  ) : null
                }
                rows={4}
                variant="outlined"
                onBlur={handleSubmit}
              />
            </>
          );
        }}
      />
    </SettingRow>
  );
};
