import { Button } from "@mui/material";
import React, { useState } from "react";
import { LogoUploadDialog } from "../../..";
import { usePrimaryOrganizationId } from "../../../../hooks/useOrganization";
import { ACCEPTED_FILE_TYPES } from "../../../dialog/LogoUploadDialog";
import { CloseButton } from "../../../molecules/CloseButton";
import { SettingRow } from "./SettingRow";

type Props = {
  headerImageUrl: string;
  updateMessageSettings: ({ logoUrl }) => void;
  vertical?: boolean;
};

const helpText =
  "This logo will be displayed at the top of your emails. Logo in .png or .jpg preferred; maximum file size is 1MB; max height of 100px";

export const EmailLogoSettingRow: React.FC<Props> = ({
  headerImageUrl,
  updateMessageSettings,
  vertical = false,
}) => {
  const orgId = usePrimaryOrganizationId();
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <SettingRow title="Email Logo" helpText={helpText} vertical={vertical}>
        <div className="flex-col flex--align-start">
          {headerImageUrl && (
            <div className="image-control flex-row--simple">
              <img src={headerImageUrl} className="max-width-192 max-height-192" />
              <div className="close-icon flex--align-start">
                <CloseButton onClose={() => updateMessageSettings({ logoUrl: "" })} />
              </div>
            </div>
          )}

          <Button
            className="header-image-upload-button w-192"
            variant="contained"
            onClick={() => setDialogOpen(true)}
          >
            Upload
          </Button>
        </div>
        <LogoUploadDialog
          acceptedTypes={ACCEPTED_FILE_TYPES}
          logoType="MESSAGE_HEADER"
          title="Upload Email Header Logo"
          organizationId={orgId}
          open={dialogOpen}
          onClose={() => setDialogOpen(false)}
          onDone={(logoUrl: string) => updateMessageSettings({ logoUrl })}
          successMessage={undefined}
        />
      </SettingRow>
    </>
  );
};
