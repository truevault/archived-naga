import React from "react";
import { PrivacyNoticeChecklistNoticeItemAction } from "../../../pages/stay-compliant/PrivacyNoticeChecklistItems";

type PrivacyNoticeActionsProps = {
  content: React.ReactNode;
  actions: PrivacyNoticeChecklistNoticeItemAction[];
};

export const PrivacyNoticeActions: React.FC<PrivacyNoticeActionsProps> = ({ content, actions }) => {
  return (
    <div className="privacy-notice-actions">
      <span className="privacy-notice-actions--content">{content}</span>
      <span className="privacy-notice-actions--action-container">
        {actions.map((a, idx) => {
          return (
            <span
              key={`action-${idx}`}
              id={a.id}
              className="privacy-notice-actions--action"
              onClick={a.onClick}
            >
              {a.content}
            </span>
          );
        })}
      </span>
    </div>
  );
};
