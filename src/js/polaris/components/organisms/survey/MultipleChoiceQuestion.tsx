import { Tooltip } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import { QuestionContainer, QuestionLabel } from "../../get-compliant/survey/QuestionContainer";
import { SurveyQuestionProps } from "./RadioSelectionQuestion";
import { SurveyButton } from "./SurveyButton";
import { ConfirmDialog } from "../../dialog";

export type MultipleChoiceQuestionOption = {
  value: string;
  label: string;
  disabled?: boolean;
  disabledTooltip?: string;
  confirmation?: React.ReactNode;
};

type Props = {
  options: MultipleChoiceQuestionOption[];
} & SurveyQuestionProps<string>;

export const useLocalAnswer = (answer) => {
  const [localAnswer, setLocalAnswer] = useState(answer);

  useEffect(() => {
    setLocalAnswer(answer);
  }, [answer]);

  return [localAnswer, setLocalAnswer];
};

export const MultipleChoiceQuestion: React.FC<Props> = ({
  question,
  helpText,
  description,
  recommendation,
  answer,
  setAnswer,
  disabled,
  disabledTooltip,
  options,
  header,
}) => {
  const [localAnswer, setLocalAnswer] = useLocalAnswer(answer);
  const [settingAnswer, setSettingAnswer] = useState(false);

  const handleClick = useCallback(
    async (newVal) => {
      if (disabled) return;

      setSettingAnswer(true);

      try {
        await setAnswer(newVal);
        setLocalAnswer(newVal);
      } finally {
        setSettingAnswer(false);
      }
    },
    [disabled, setAnswer],
  );

  return (
    <QuestionContainer>
      <QuestionLabel
        label={question}
        helpText={helpText}
        header={header}
        description={description}
        recommendation={recommendation}
      />
      <div className="survey-question--mult-choice">
        {options.map((opt) => {
          const key = `mult-choice-answer-${opt.value}`;
          return (
            <MultipleChoiceQuestionButton
              key={key}
              option={opt}
              settingAnswer={settingAnswer}
              localAnswer={localAnswer}
              rowDisabled={disabled}
              rowDisabledTooltip={disabledTooltip}
              onClick={handleClick}
            />
          );
        })}
      </div>
    </QuestionContainer>
  );
};

type MultipleChoiceQuestionButtonProps = {
  localAnswer: string;
  settingAnswer: boolean;
  rowDisabled: boolean;

  onClick: (newVal: string) => void;

  rowDisabledTooltip?: string;
  option: MultipleChoiceQuestionOption;
};
const MultipleChoiceQuestionButton: React.FC<MultipleChoiceQuestionButtonProps> = ({
  option: opt,
  localAnswer,
  settingAnswer,
  rowDisabled,
  rowDisabledTooltip,
  onClick,
}) => {
  const [confirmOpen, setConfirmOpen] = useState(false);
  const selected = localAnswer === opt.value;

  const isDisabled = opt.disabled || rowDisabled;
  let tooltip = undefined;
  if (rowDisabled && rowDisabledTooltip) {
    tooltip = rowDisabledTooltip;
  }
  if (opt.disabled && opt.disabledTooltip) {
    tooltip = opt.disabledTooltip;
  }

  const hasConfirmation = Boolean(opt.confirmation);

  const handleClick = () => {
    if (hasConfirmation) {
      setConfirmOpen(true);
    } else {
      onClick(opt.value);
    }
  };

  const optionEl = (
    <>
      <SurveyButton
        disabled={isDisabled}
        loading={settingAnswer}
        color={selected ? "primary" : "default"}
        onClick={handleClick}
      >
        {opt.label}
      </SurveyButton>
      {hasConfirmation && (
        <ConfirmDialog
          open={confirmOpen}
          onClose={() => setConfirmOpen(false)}
          onConfirm={() => {
            setConfirmOpen(false);
            onClick(opt.value);
          }}
          confirmTitle={"Are you sure?"}
          contentText={opt.confirmation}
          confirmText="Yes"
        />
      )}
    </>
  );

  return isDisabled ? (
    <Tooltip placement="bottom-start" title={tooltip}>
      <span>{optionEl}</span>
    </Tooltip>
  ) : (
    optionEl
  );
};
