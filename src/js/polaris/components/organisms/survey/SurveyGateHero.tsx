import React, { ReactNode, useCallback, useState } from "react";
import { SurveyResponses } from "../../../../common/hooks/useSurvey";
import { useLocalAnswer } from "./MultipleChoiceQuestion";
import { SurveyButton } from "./SurveyButton";

type SurveyGateHeroProps = {
  survey: SurveyResponses;
  description: ReactNode;
  imageUrl: string;
  slug: string;
  label?: string;
  updateResponse: (slug: string, answer: string) => Promise<void>;
  bypass?: boolean;
  imageFirst?: boolean;
  imageWidth?: string;
};
export const SurveyGateHero: React.FC<SurveyGateHeroProps> = ({
  survey,
  slug,
  description,
  imageUrl,
  updateResponse,
  children,
  label = "Continue",
  bypass,
  imageFirst = false,
  imageWidth,
}) => {
  const answer = Boolean(survey[slug]);
  const [localAnswer, setLocalAnswer] = useLocalAnswer(answer);
  const [answering, setAnswering] = useState(false);

  const handleAnswer = useCallback(async () => {
    setAnswering(true);
    try {
      await updateResponse(slug, "true");
      setLocalAnswer(true);
    } finally {
      setAnswering(false);
    }
  }, [slug, updateResponse]);

  const showChildren = localAnswer || bypass;
  const showButton = !showChildren;

  return (
    <>
      <div className="d-flex mt-xl">
        {imageFirst && (
          <div style={{ width: 420 }}>
            <img src={imageUrl} />
          </div>
        )}
        <div>
          <p>{description}</p>
          {showButton && (
            <SurveyButton onClick={handleAnswer} loading={answering}>
              {label}
            </SurveyButton>
          )}
        </div>
        {!imageFirst && (
          <div style={{ width: 420 }}>
            <img width={imageWidth} src={imageUrl} />
          </div>
        )}
      </div>

      {showChildren && children}
    </>
  );
};
