import React from "react";
import { SurveyAnswerFn, SurveyAnswers } from "../../../../common/hooks/useSurvey";
import { SurveyQuestion, SurveyQuestionProps } from "../../get-compliant/survey/SurveyQuestion";

export type QuestionReview = { survey: string; question: SurveyQuestionProps; onlyGdpr?: boolean };

export type BundledSurveys = Record<string, SurveyAnswers>;
export type BundledSurveyUpdates = Record<string, SurveyAnswerFn>;
export type BundledSurveyVisibility = Record<string, Record<string, boolean>>;
export type BundledDisabledReasons = Record<string, Record<string, string | boolean>>;

export type SurveyQuestionWrapperProps = {
  surveys: BundledSurveys;
  surveyUpdaters: BundledSurveyUpdates;
  review: QuestionReview;
  visibility: BundledSurveyVisibility;
  disabledReasons: BundledDisabledReasons;
  handleAnswer?: (answer: string) => Promise<void>;
};

export const SurveyQuestionWrapper: React.FC<SurveyQuestionWrapperProps> = ({
  surveys,
  surveyUpdaters,
  review,
  visibility,
  disabledReasons = {},
  handleAnswer: handleAnswerOverride,
}) => {
  const answers = surveys[review.survey];
  const handleAnswer = surveyUpdaters[review.survey];
  const q = review.question;
  const visible = visibility[review.survey]?.[q.slug] ?? true;
  const disabledReason = disabledReasons[review.survey]?.[q.slug];
  const isDisabled = Boolean(disabledReason);

  if (!visible) {
    return null;
  }

  const onAnswer = handleAnswerOverride
    ? handleAnswerOverride
    : (answer) => handleAnswer(q.slug, answer, q.plaintextQuestion);

  return (
    <SurveyQuestion
      key={q.slug}
      question={{
        ...q,
        visible: visible,
        isDisabled,
        disabledReason: typeof disabledReason === "string" ? disabledReason : null,
      }}
      answer={answers[q.slug]}
      onAnswer={onAnswer}
    />
  );
};
