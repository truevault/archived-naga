import clsx from "clsx";
import React from "react";
import { LoadingButton, LoadingButtonProps } from "../../../../common/components/LoadingButton";

export const SurveyButton: React.FC<LoadingButtonProps> = ({ className, ...rest }) => {
  return (
    <LoadingButton
      variant="outlined"
      {...rest}
      className={clsx("min-width-192 h-48 mr-md", className)}
    />
  );
};
