import { ChevronRight } from "@mui/icons-material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Button, Collapse } from "@mui/material";
import React, { ReactElement, useState } from "react";

type SurveyGateCollapseProps = {
  label: string | ReactElement;
  bypass?: boolean;
};

export const SurveyGateCollapse: React.FC<SurveyGateCollapseProps> = ({
  children,
  label,
  bypass,
}) => {
  const [expanded, setExpanded] = useState(false);

  const isExpanded = expanded || bypass;
  const ExpandIcon = isExpanded ? ExpandMoreIcon : ChevronRight;

  return (
    <>
      <div className="my-lg text-weight-medium">
        <Button
          variant="text"
          color="secondary"
          onClick={(e) => {
            e.preventDefault();
            setExpanded(!expanded);
          }}
          startIcon={<ExpandIcon />}
        >
          {label}
        </Button>
      </div>

      <Collapse in={isExpanded}>{children}</Collapse>
    </>
  );
};
