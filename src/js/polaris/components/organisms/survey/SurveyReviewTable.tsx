import React, { useMemo } from "react";
import { Button, Table, TableBody, TableCell, TableRow } from "@mui/material";
import { SurveyQuestionShortAnswer } from "../../get-compliant/survey/answers/SurveyQuestionShortAnswer";
import clsx from "clsx";
import { Edit } from "@mui/icons-material";
import { EmptyInfo } from "../../Empty";
import {
  BundledDisabledReasons,
  BundledSurveyUpdates,
  BundledSurveyVisibility,
  BundledSurveys,
  QuestionReview,
  SurveyQuestionWrapper,
} from "./SurveyQuestionWrapper";

export type SurveyQuestionReviewTableProps = {
  reviewQuestions: QuestionReview[];
  surveys: BundledSurveys;
  surveyUpdaters: BundledSurveyUpdates;
  visibility: BundledSurveyVisibility;
  disabledReasons: BundledDisabledReasons;
  expanded: string[];
  surveyGateSlug: string;
};

export const SurveyQuestionReviewTable: React.FC<SurveyQuestionReviewTableProps> = ({
  surveys,
  surveyUpdaters,
  visibility,
  disabledReasons,
  expanded,
  reviewQuestions,
  surveyGateSlug,
}) => {
  return (
    <Table className="mt-mdlg">
      <TableBody>
        {reviewQuestions.length === 0 ? (
          <TableRow>
            <TableCell colSpan={99999}>
              <EmptyInfo>You have no survey questions to review</EmptyInfo>
            </TableCell>
          </TableRow>
        ) : (
          <>
            {reviewQuestions.map((q) => {
              return (
                <SurveyQuestionRow
                  surveys={surveys}
                  surveyUpdaters={surveyUpdaters}
                  visibility={visibility}
                  disabledReasons={disabledReasons}
                  review={q}
                  expanded={expanded}
                  key={q.question.slug}
                  surveyGateSlug={surveyGateSlug}
                />
              );
            })}
          </>
        )}
      </TableBody>
    </Table>
  );
};

type SurveyQuestionRowProps = {
  surveys: BundledSurveys;
  surveyUpdaters: BundledSurveyUpdates;
  visibility: BundledSurveyVisibility;
  disabledReasons: BundledDisabledReasons;
  review: QuestionReview;
  expanded: string[];
  surveyGateSlug: string;
};

const SurveyQuestionRow: React.FC<SurveyQuestionRowProps> = ({
  surveys,
  surveyUpdaters,
  visibility,
  disabledReasons,
  review,
  expanded,
  surveyGateSlug,
}) => {
  const isExpanded = useMemo(
    () => expanded.includes(review.question.slug),
    [expanded, review.question.slug],
  );

  const handleExpand = () => {
    const updatedArr = [...expanded];
    updatedArr.push(review.question.slug);
    surveyUpdaters[surveyGateSlug]("updated-survey-questions", JSON.stringify(updatedArr));
  };

  const visible = visibility[review.survey]?.[review.question.slug] ?? true;

  if (!visible) {
    return null;
  }

  const unanswered = !surveys[review.survey][review.question.slug];

  return isExpanded || unanswered ? (
    <SurveyQuestionRowWrapper
      surveys={surveys}
      surveyUpdaters={surveyUpdaters}
      review={review}
      visibility={visibility}
      disabledReasons={disabledReasons}
      onExpand={handleExpand}
    />
  ) : (
    <SurveyQuestionReviewRow
      surveys={surveys}
      surveyUpdaters={surveyUpdaters}
      visibility={visibility}
      disabledReasons={disabledReasons}
      review={review}
      onExpand={handleExpand}
    />
  );
};

type SurveyQuestionRowWrapperProps = {
  surveys: BundledSurveys;
  surveyUpdaters: BundledSurveyUpdates;
  review: QuestionReview;
  visibility: BundledSurveyVisibility;
  disabledReasons: BundledDisabledReasons;
  onExpand?: () => void;
};

// Review mode in a table row
const SurveyQuestionReviewRow: React.FC<SurveyQuestionRowWrapperProps> = ({
  surveys,
  surveyUpdaters,
  review,
  onExpand,
}) => {
  const answers = surveys[review.survey];
  const handleAnswer = surveyUpdaters[review.survey];
  const q = review.question;

  return (
    <TableRow>
      <TableCell>
        <div className="text-component-question">{q.question}</div>
        {Boolean(q.helpText) && <p className="text-component-help">{q.helpText}</p>}
      </TableCell>
      <TableCell className={clsx({ "text-center": q.type === "boolean" })}>
        <SurveyQuestionShortAnswer
          key={q.slug}
          question={q}
          answer={answers[q.slug]}
          onAnswer={(answer) =>
            handleAnswer(q.slug, answer, q.plaintextQuestion ?? (q.question as string))
          }
        />
      </TableCell>
      <TableCell>
        <Button
          variant="text"
          color="secondary"
          onClick={() => {
            onExpand();
          }}
          startIcon={<Edit />}
        >
          Update my Answer
        </Button>
      </TableCell>
    </TableRow>
  );
};

// Edit mode in a table row
const SurveyQuestionRowWrapper: React.FC<SurveyQuestionRowWrapperProps> = ({
  surveys,
  surveyUpdaters,
  review,
  visibility,
  disabledReasons,
  onExpand,
}) => {
  return (
    <TableRow>
      <TableCell colSpan={3}>
        <SurveyQuestionWrapper
          surveys={surveys}
          surveyUpdaters={surveyUpdaters}
          review={{ survey: review.survey, question: { ...review.question, visible: true } }}
          visibility={visibility}
          disabledReasons={disabledReasons}
          handleAnswer={async (answer) => {
            const handleAnswer = surveyUpdaters[review.survey];
            const q = review.question;
            if (onExpand) {
              onExpand();
            }
            await handleAnswer(q.slug, answer, q.plaintextQuestion);
          }}
        />
      </TableCell>
    </TableRow>
  );
};
