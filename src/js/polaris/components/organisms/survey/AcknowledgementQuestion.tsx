import React, { ReactNode, useCallback, useState } from "react";
import { QuestionContainer, QuestionLabel } from "../../get-compliant/survey/QuestionContainer";
import { useLocalAnswer } from "./MultipleChoiceQuestion";
import { SurveyQuestionProps } from "./RadioSelectionQuestion";
import { SurveyButton } from "./SurveyButton";

type AcknowledgementQuestionProps = SurveyQuestionProps<string> & {
  text?: ReactNode;
  buttonLabel?: ReactNode;
};

export const AcknowledgementQuestion: React.FC<AcknowledgementQuestionProps> = ({
  question,
  text,
  answer,
  disabled,
  buttonLabel = "Got it",
  setAnswer,
}) => {
  const [localAnswer, setLocalAnswer] = useLocalAnswer(answer);
  const [answering, setAnswering] = useState(false);

  const handleAnswer = useCallback(async () => {
    setAnswering(true);
    try {
      const newAnswer = new Date().toISOString();
      await setAnswer(newAnswer);
      setLocalAnswer(newAnswer);
    } finally {
      setAnswering(false);
    }
  }, [setAnswer]);

  return (
    <QuestionContainer>
      <QuestionLabel label={question} />
      <div className="survey-question--acknowledgement">{text}</div>
      {!localAnswer && (
        <SurveyButton disabled={disabled} onClick={handleAnswer} loading={answering}>
          {buttonLabel}
        </SurveyButton>
      )}
    </QuestionContainer>
  );
};
