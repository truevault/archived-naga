import { Button } from "@mui/material";
import React, { useEffect, useMemo } from "react";
import { SurveyAnswerFn, SurveyAnswers } from "../../../../common/hooks/useSurvey";
import { CategoryDto } from "../../../../common/service/server/dto/DataMapDto";
import { ProcessingActivityDto } from "../../../../common/service/server/dto/ProcessingActivityDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { Callout, CalloutVariant } from "../../Callout";
import { QuestionHeading } from "../../vendor/QuestionHeading";

export type BusinessPurposeCollectionQuestionProps = {
  activities: ProcessingActivityDto[];
  collectedCategories: CategoryDto[] | undefined;
  question: string | React.ReactNode;
  helpText?: string | React.ReactNode;
  slug: string;
  answers: SurveyAnswers;
  updateResponse: SurveyAnswerFn;
  onAllAnsweredChanged?: (boolean) => void;
};

export const BusinessPurposeCollectionQuestion: React.FC<
  BusinessPurposeCollectionQuestionProps
> = ({
  activities,
  collectedCategories,
  slug,
  answers,
  question,
  helpText,
  updateResponse,
  onAllAnsweredChanged,
}) => {
  const serializedAnswers = answers[slug] || "[]";
  const responses = useMemo(
    () => JSON.parse(serializedAnswers) as SerializedSurveyAnswer[],
    [serializedAnswers],
  );

  const allAnswered = useMemo(() => {
    return activities.every(
      (a) => responses.find((r) => r.activity == a.id) || a.processedPic.length == 0,
    );
  }, [activities, responses]);
  const anyMaybeNot = useMemo(() => {
    return activities.some(
      (a) =>
        responses.find((r) => r.activity == a.id)?.used == "maybe-not" && a.processedPic.length > 0,
    );
  }, [activities, responses]);

  const changeResponse = (answer: SerializedSurveyAnswer) => {
    const newResponse = [...responses];
    const existingIdx = responses.findIndex((r) => r.activity == answer.activity);
    if (existingIdx >= 0) {
      newResponse.splice(existingIdx, 1);
    }
    newResponse.push(answer);
    updateResponse(slug, JSON.stringify(newResponse));
  };

  useEffect(() => onAllAnsweredChanged?.(allAnswered), [allAnswered]);

  return (
    <>
      <QuestionHeading question={question} helpText={helpText} />
      {activities.map((a) => {
        if (a.processedPic.length == 0) {
          return null;
        }

        const response = responses.find((r) => r.activity == a.id);

        return (
          <div className="d-flex align-items-center" key={a.id}>
            <p>
              <strong>{a.name}:</strong>{" "}
              {a.processedPic
                .map((id) => collectedCategories?.find((c) => c.id == id)?.name)
                .filter((x) => Boolean(x))
                .join(", ")}
            </p>
            <div className="flex-grow">&nbsp;</div>
            <Button
              variant="outlined"
              color={response?.used == "yes" ? "primary" : undefined}
              size="small"
              style={{ flexGrow: 0, flexShrink: 0 }}
              onClick={() => changeResponse({ activity: a.id, used: "yes" })}
            >
              Yes
            </Button>
            <Button
              variant="outlined"
              color={response?.used == "maybe-not" ? "primary" : undefined}
              className="ml-sm"
              size="small"
              style={{ flexGrow: 0, flexShrink: 0 }}
              onClick={() => changeResponse({ activity: a.id, used: "maybe-not" })}
            >
              Maybe Not
            </Button>
          </div>
        );
      })}

      {allAnswered && anyMaybeNot && (
        <Callout
          variant={CalloutVariant.Purple}
          title="It looks like you might not need some of the data you collect."
          className="my-lg"
        >
          That’s ok! We will add a task for you to come back in a few weeks and re-assess your data
          usage practices.
        </Callout>
      )}

      {allAnswered && !anyMaybeNot && (
        <Callout variant={CalloutVariant.Purple} className="my-lg">
          <strong>Great!</strong> Looks like you’re only collecting what you need. Way to respect
          your consumer’s privacy.
        </Callout>
      )}
    </>
  );
};

type SerializedSurveyAnswer = {
  activity: UUIDString;
  used: "yes" | "maybe-not";
};
