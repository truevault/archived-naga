import React, { useEffect, useState } from "react";
import { SurveyAnswerFn, SurveyAnswers } from "../../../../common/hooks/useSurvey";
import { CategoryDto } from "../../../../common/service/server/dto/DataMapDto";
import { ProcessingActivityDto } from "../../../../common/service/server/dto/ProcessingActivityDto";
import { BusinessPurposeCollectionQuestion } from "./BusinessPurposeCollectionQuestion";
import { SurveyGateButton } from "./SurveyGateButton";
import { SurveyGateHero } from "./SurveyGateHero";

type Props = {
  answers: SurveyAnswers;
  setAnswer: SurveyAnswerFn;
  activities: ProcessingActivityDto[];
  collectedPic: CategoryDto[];
  onAllAnsweredChanged?: (boolaen) => void;
};

export const CorePrivacyConceptsSurvey: React.FC<Props> = ({
  answers,
  setAnswer,
  activities,
  collectedPic,
  onAllAnsweredChanged,
}) => {
  const [dataMinAllAnswered, setDataMinAllAnswered] = useState(false);
  const [purposeLimitAllAnswered, setPurposeLimitAllAnswered] = useState(false);

  useEffect(() => {
    onAllAnsweredChanged?.(dataMinAllAnswered && purposeLimitAllAnswered);
  }, [dataMinAllAnswered, purposeLimitAllAnswered]);

  return (
    <SurveyGateHero
      slug="cpc-data-minimization-hero"
      imageUrl="/assets/images/surveys/onboarding/data-minimization-hero.svg"
      label="See an example"
      description={
        <>
          <ol className="custom p-0">
            <li>
              <strong>Data Minimization: What is it?</strong>
            </li>
          </ol>
          <p className="ml-xl mb-lg">
            Data Minimization is the idea that your business should only collect and use data that
            is <strong>necessary</strong> and <strong>proportionate</strong> to for its{" "}
            <strong>processing purposes</strong>.
          </p>
        </>
      }
      survey={answers}
      updateResponse={setAnswer}
    >
      <SurveyGateHero
        slug="cpc-data-minimization-example"
        imageUrl="/assets/images/surveys/onboarding/data-minimization-example.svg"
        label="Got it"
        description={
          <>
            <p>
              <strong>For example...</strong>
            </p>
            <p>
              In order to process a customer’s order, a business may need to collect payment
              details, shipping information, and an email address. They might also want to collect
              more information like a social security number — but shouldn’t! Collecting this data
              from your customers wouldn’t qualify as necessary or proportionate, as it is not
              required for the purpose of fulfilling a customer order.
            </p>
          </>
        }
        survey={answers}
        updateResponse={setAnswer}
        imageFirst
      >
        <BusinessPurposeCollectionQuestion
          activities={activities}
          collectedCategories={collectedPic}
          answers={answers}
          updateResponse={setAnswer}
          slug="cpc-data-minimization"
          question="Let’s review the data your business collects. For each purpose, indicate whether the data is necessary and proportionate to its processing purposes."
          helpText="If you select “Maybe Not” for any of these purposes, we’ll create a task for you to review your data collection practices with your team. Don’t worry: we have documentation to help you with the review."
          onAllAnsweredChanged={(allAnswered) => setDataMinAllAnswered(allAnswered)}
        />
        <SurveyGateButton
          slug="cpc-keep-going"
          survey={answers}
          updateResponse={setAnswer}
          label="Keep going..."
        >
          <SurveyGateHero
            slug="cpc-purpose-limitation-hero"
            imageUrl="/assets/images/surveys/onboarding/purpose-limitation-hero.svg"
            label="See an example"
            description={
              <>
                <p>
                  <strong>What Is Purpose Limitation?</strong>
                </p>
                <p>
                  The primary purpose for which you collect and use a consumer's Personal
                  Information must be consistent with what they reasonably expected when they
                  provided it to you. Any secondary purpose you use it for must be compatible with
                  the context in which the data was collected, otherwise you will need the
                  consumer's consent.
                </p>
              </>
            }
            survey={answers}
            updateResponse={setAnswer}
          >
            <SurveyGateHero
              slug="cpc-purpose-limitation-example"
              imageUrl="/assets/images/surveys/onboarding/purpose-limitation-example.svg"
              label="Got it"
              description={
                <>
                  <p>
                    <strong>For example...</strong>
                  </p>
                  <p>
                    Let’s say a business offers cloud storage for a consumer’s photos, but also is
                    using those photos to develop unrelated facial-scanning software. This is
                    unlikely to be consistent with the consumer’s expectations when they uploaded
                    their photos, and is therefore considered an <strong>unexpected use</strong>.
                  </p>
                </>
              }
              survey={answers}
              updateResponse={setAnswer}
              imageFirst
            >
              <BusinessPurposeCollectionQuestion
                activities={activities}
                collectedCategories={collectedPic}
                answers={answers}
                updateResponse={setAnswer}
                slug="cpc-purpose-limitation"
                question="Does your business use the personal data it collects in a manner that is consistent with consumer expectations?"
                helpText="If you select ”Maybe Not,” we’ll create a task for you to review your data collection practices with your team. We have documentation to help you with the review."
                onAllAnsweredChanged={(allAnswered) => setPurposeLimitAllAnswered(allAnswered)}
              />
              <SurveyGateButton
                slug="cpc-done"
                survey={answers}
                updateResponse={setAnswer}
                label="Done"
              />
            </SurveyGateHero>
          </SurveyGateHero>
        </SurveyGateButton>
      </SurveyGateHero>
    </SurveyGateHero>
  );
};
