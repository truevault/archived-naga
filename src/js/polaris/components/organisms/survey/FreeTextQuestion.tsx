import { TextField, Tooltip } from "@mui/material";
import debounce from "lodash.debounce";
import React, { useEffect, useState } from "react";
import { QuestionContainer, QuestionLabel } from "../../get-compliant/survey/QuestionContainer";
import { useLocalAnswer } from "./MultipleChoiceQuestion";
import { SurveyQuestionProps } from "./RadioSelectionQuestion";

const debouncedFn = debounce((fn, ...args) => fn(...args), 500);

type FreeTextQuestionProps = SurveyQuestionProps<string> & {
  freeTextHelpText?: string;
  validateFreeText: (answer: string) => string;
  otherAnswers: string[];
  placeholder?: string;
};

export const FreeTextQuestion: React.FC<FreeTextQuestionProps> = ({
  question,
  helpText,
  freeTextHelpText,
  placeholder,
  answer,
  setAnswer,
  otherAnswers,
  validateFreeText,
  disabled,
  disabledTooltip,
}) => {
  const [freeTextAnswer, setFreeTextAnswer] = useLocalAnswer(answer);
  const [freeTextError, setFreeTextError] = useState("");

  useEffect(() => {
    const error = validateFreeText(freeTextAnswer);
    if (error) {
      setFreeTextError(error);
    } else {
      setFreeTextError("");
      debouncedFn(setAnswer, freeTextAnswer);
    }
  }, [setAnswer, validateFreeText, freeTextAnswer]);

  useEffect(() => {
    if (freeTextError) {
      const error = validateFreeText(freeTextAnswer);
      if (error) {
        setFreeTextError(error);
      } else {
        setFreeTextError("");
        setAnswer(freeTextAnswer);
      }
    }
  }, [setFreeTextError, setAnswer, freeTextAnswer, validateFreeText, freeTextError, otherAnswers]);

  const textField = (
    <TextField
      className="radio-freetext--textfield"
      variant="outlined"
      value={freeTextAnswer}
      onChange={({ target }) => setFreeTextAnswer(target.value)}
      placeholder={placeholder}
      disabled={disabled}
    />
  );

  return (
    <QuestionContainer>
      <QuestionLabel label={question} helpText={helpText}></QuestionLabel>
      {freeTextError && <div className="radio-freetext--error">{freeTextError}</div>}

      <div>
        {freeTextHelpText && <div className="radio-freetext--help-text">{freeTextHelpText}</div>}

        {disabled ? (
          <Tooltip placement="bottom-start" title={disabledTooltip}>
            {textField}
          </Tooltip>
        ) : (
          textField
        )}
      </div>
    </QuestionContainer>
  );
};
