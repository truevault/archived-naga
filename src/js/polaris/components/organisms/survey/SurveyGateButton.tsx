import React, { useCallback, useState } from "react";
import { SurveyResponses } from "../../../../common/hooks/useSurvey";
import { useLocalAnswer } from "./MultipleChoiceQuestion";
import { SurveyButton } from "./SurveyButton";

type SurveyGateButtonProps = {
  survey: SurveyResponses;
  slug: string;
  label?: string;
  updateResponse: (slug: string, answer: string) => Promise<void>;
  bypass?: boolean;
  className?: string;
};
export const SurveyGateButton: React.FC<SurveyGateButtonProps> = ({
  survey,
  slug,
  updateResponse,
  children,
  label = "Continue",
  bypass,
  className,
}) => {
  const answer = Boolean(survey[slug]);
  const [localAnswer, setLocalAnswer] = useLocalAnswer(answer);
  const [answering, setAnswering] = useState(false);

  const handleAnswer = useCallback(async () => {
    setAnswering(true);
    try {
      await updateResponse(slug, "true");
      setLocalAnswer(true);
    } finally {
      setAnswering(false);
    }
  }, [slug, updateResponse]);

  if (localAnswer || bypass) {
    return <>{children}</>;
  } else {
    return (
      <SurveyButton className={className} onClick={handleAnswer} loading={answering}>
        {label}
      </SurveyButton>
    );
  }
};
