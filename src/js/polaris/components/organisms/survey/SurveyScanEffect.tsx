import _ from "lodash";
import React, { Children, useMemo } from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { NetworkRequest } from "../../../../common/models";
import { SurveyGateButton } from "./SurveyGateButton";
import { usePrimaryOrganizationId } from "../../../hooks";
import { DelayFadeTransition } from "../../../pages/get-compliant/FadeTransition";

export const SURVEY_GATES_SURVEY_NAME = "survey-gates";

type Props = {
  label: string;
  slug: string;
  survey?: string;
  delay?: number;
  jitter?: boolean;
};

export const SurveyScanEffect: React.FC<Props> = ({
  label,
  slug,
  survey = SURVEY_GATES_SURVEY_NAME,
  delay = 250,
  jitter = true,
  children,
}) => {
  const orgId = usePrimaryOrganizationId();
  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, survey);

  const initialScanAnswer = useMemo(() => answers[slug] === "true", [answers, slug]);

  if (!NetworkRequest.isFinished(surveyRequest)) {
    return null;
  }

  return (
    <SurveyGateButton label={label} survey={answers} updateResponse={setAnswer} slug={slug}>
      {Children.map(children, (child, idx) => {
        const jitterOffset = jitter ? _.random(0, 75) : 0;
        const finalDelay = 300 + idx * delay + jitterOffset;

        return (
          child && (
            <DelayFadeTransition
              delay={finalDelay}
              cls="survey-question-fade"
              appear
              in={initialScanAnswer}
              unmountOnExit
            >
              {child}
            </DelayFadeTransition>
          )
        );
      })}
    </SurveyGateButton>
  );
};
