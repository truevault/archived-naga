import { FormControlLabel, Radio, RadioGroup, TextField, Tooltip } from "@mui/material";
import debounce from "lodash.debounce";
import React, { useEffect, useState } from "react";
import { RadioSelectionQuestionProps } from "./RadioSelectionQuestion";

const debouncedFn = debounce((fn, ...args) => fn(...args), 500);

type RadioSelectionFreeTextQuestionProps = RadioSelectionQuestionProps & {
  freeTextHelpText?: string;
  validateFreeText: (answer: string) => string;
  otherAnswers: string[];
};

export const RadioSelectionFreeTextQuestion: React.FC<RadioSelectionFreeTextQuestionProps> = ({
  question,
  helpText,
  freeTextHelpText,
  options,
  answer,
  setAnswer,
  otherAnswers,
  validateFreeText,
  disabled,
  disabledTooltip,
}) => {
  const [selectedAnswer, setSelectedAnswer] = useState(
    options.map((o) => o.value).includes(answer) ? answer : answer != null ? "other" : "",
  );
  const [freeTextAnswer, setFreeTextAnswer] = useState(
    answer && !options.map((o) => o.value).includes(answer) ? answer : "",
  );
  const [freeTextError, setFreeTextError] = useState("");

  useEffect(() => {
    if (selectedAnswer && selectedAnswer !== "other") {
      setAnswer(selectedAnswer);
      setFreeTextAnswer("");
      setFreeTextError("");
    } else {
      setAnswer(freeTextAnswer);
    }
  }, [selectedAnswer, freeTextAnswer, setAnswer]);

  useEffect(() => {
    if (selectedAnswer === "other") {
      const error = validateFreeText(freeTextAnswer);
      if (error) {
        setFreeTextError(error);
      } else {
        setFreeTextError("");
        debouncedFn(setAnswer, freeTextAnswer);
      }
    }
  }, [setAnswer, validateFreeText, selectedAnswer, freeTextAnswer]);

  useEffect(() => {
    if (selectedAnswer === "other" && freeTextError) {
      const error = validateFreeText(freeTextAnswer);
      if (error) {
        setFreeTextError(error);
      } else {
        setFreeTextError("");
        setAnswer(freeTextAnswer);
      }
    }
  }, [freeTextAnswer, validateFreeText, freeTextError, selectedAnswer, setAnswer, otherAnswers]);

  const otherEl = (
    <div className="w-100">
      <FormControlLabel
        value={"other"}
        control={<Radio color="primary" />}
        label={"Other"}
        disabled={disabled}
      />
      <div className="radio-freetext--other-container">
        {freeTextHelpText && (
          <div className="radio-freetext--other-help-text">{freeTextHelpText}</div>
        )}
        <TextField
          className="radio-freetext--textfield"
          variant="outlined"
          value={freeTextAnswer ?? ""}
          fullWidth={false}
          onChange={({ target }) => setFreeTextAnswer(target.value)}
          disabled={disabled || selectedAnswer !== "other"}
        />
      </div>
    </div>
  );

  return (
    <div>
      <div className="radio-freetext--question">{question}</div>
      {selectedAnswer === "other" && freeTextError && (
        <div className="radio-freetext--error">{freeTextError}</div>
      )}
      {helpText && <div className="radio-freetext--help-text mt-sm">{helpText}</div>}
      <RadioGroup
        className="radio-freetext--radios"
        value={selectedAnswer}
        onChange={({ target }) => {
          setSelectedAnswer(target.value);
        }}
      >
        {options.map((option, idx) => {
          const key = `radio-option-${idx}-${option.value.replace(new RegExp(" ", "g"), "")}`;
          const tooltipTitle = disabled
            ? disabledTooltip
            : option.disabled
            ? `${option.label} has already been used, please select a unique name`
            : null;
          const radio = (
            <FormControlLabel
              key={!tooltipTitle ? key : null}
              value={option.value}
              control={<Radio color="primary" />}
              label={option.label}
              disabled={disabled || otherAnswers.includes(option.value)}
            />
          );
          return tooltipTitle ? (
            <Tooltip placement="bottom-start" key={key} title={tooltipTitle}>
              {radio}
            </Tooltip>
          ) : (
            radio
          );
        })}
        {disabled ? (
          <Tooltip placement="bottom-start" title={disabledTooltip}>
            {otherEl}
          </Tooltip>
        ) : (
          otherEl
        )}
      </RadioGroup>
    </div>
  );
};
