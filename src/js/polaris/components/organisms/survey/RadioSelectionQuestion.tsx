import { FormControlLabel, Radio, RadioGroup, Tooltip } from "@mui/material";
import React, { ReactNode, useCallback, useState } from "react";

export type SurveyQuestionProps<T> = {
  question: string | ReactNode;
  helpText?: string | ReactNode;
  description?: string | ReactNode;
  recommendation?: string | ReactNode;
  header?: string | ReactNode;
  answer: T;
  setAnswer: (answer: T) => Promise<void>;
  disabled?: boolean;
  disabledTooltip?: string;
};

type RadioSelectionQuestionOption = {
  value: string;
  label: string;
  disabled?: boolean;
};

export type RadioSelectionQuestionProps = SurveyQuestionProps<string> & {
  options: RadioSelectionQuestionOption[];
};

export const RadioSelectionQuestion: React.FC<RadioSelectionQuestionProps> = ({
  question,
  helpText,
  options,
  answer,
  setAnswer,
  disabled,
  disabledTooltip,
}) => {
  const [localAnswer, setLocalAnswer] = useState(answer);
  const [settingAnswer, setSettingAnswer] = useState(false);

  const handleChange = useCallback(
    async ({ target }) => {
      if (disabled) return;

      setSettingAnswer(true);

      try {
        setAnswer(target.value);
        setLocalAnswer(target.value);
      } finally {
        setSettingAnswer(false);
      }
    },
    [disabled, setAnswer],
  );

  return (
    <div className="survey-question--container">
      <div className="survey-question--question">{question}</div>
      {helpText && <div className="survey-question--help-text">{helpText}</div>}

      <RadioGroup
        className="survey-question--radios"
        value={localAnswer || ""}
        onChange={handleChange}
      >
        {options.map((option) => {
          const key = `radio-option-${option.value}`;

          const optionEl = (
            <FormControlLabel
              key={!(disabled || option.disabled) ? key : null}
              value={option.value}
              control={<Radio color="primary" />}
              label={option.label}
              disabled={settingAnswer || disabled || option.disabled}
            />
          );

          return disabled || option.disabled ? (
            <Tooltip key={key} placement="bottom-start" title={disabledTooltip}>
              {optionEl}
            </Tooltip>
          ) : (
            optionEl
          );
        })}
      </RadioGroup>
    </div>
  );
};
