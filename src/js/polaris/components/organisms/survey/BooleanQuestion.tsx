import React from "react";

import { MultipleChoiceQuestion } from "./MultipleChoiceQuestion";
import { SurveyQuestionProps } from "./RadioSelectionQuestion";

export type BooleanQuestionProps = SurveyQuestionProps<string> & {
  yesLabel?: string;
  noLabel?: string;

  yesDisabled?: boolean;
  yesDisabledTooltip?: string;
  yesConfirmation?: React.ReactNode;
  noDisabled?: boolean;
  noDisabledTooltip?: string;
  noConfirmation?: React.ReactNode;
  noFirst?: boolean;
};

export const BooleanQuestion: React.FC<BooleanQuestionProps> = (props) => {
  const options = [
    {
      value: "true",
      label: props.yesLabel || "Yes",
      disabled: props.yesDisabled ?? false,
      disabledTooltip: props.yesDisabledTooltip,
      confirmation: props.yesConfirmation,
    },
    {
      value: "false",
      label: props.noLabel || "No",
      disabled: props.noDisabled ?? false,
      disabledTooltip: props.noDisabledTooltip,
      confirmation: props.noConfirmation,
    },
  ];

  if (props.noFirst) {
    options.reverse();
  }

  return <MultipleChoiceQuestion {...props} options={options} />;
};
