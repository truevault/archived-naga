import React, { useMemo } from "react";
import { SurveyAnswerFn, SurveyAnswers } from "../../../../common/hooks/useSurvey";
import { prepareSurvey, Survey } from "../../../surveys/survey";
import { SurveyQuestionContainer } from "../../get-compliant/survey/SurveyBooleanQuestion";
import { SurveyQuestion, SurveyQuestionProps } from "../../get-compliant/survey/SurveyQuestion";
import { Button, Table, TableBody, TableCell, TableRow } from "@mui/material";
import { EmptyInfo } from "../../Empty";
import { SurveyQuestionShortAnswer } from "../../get-compliant/survey/answers/SurveyQuestionShortAnswer";
import { Edit } from "@mui/icons-material";
import clsx from "clsx";

type SurveyReviewRendererProps = {
  survey: Survey;
  answers: SurveyAnswers;
  setAnswer: SurveyAnswerFn;
  gateAnswers: SurveyAnswers;
  setGateAnswer: SurveyAnswerFn;
  offsetFirst?: boolean;
};

export const SurveyReviewRenderer: React.FC<SurveyReviewRendererProps> = ({
  survey,
  answers,
  setAnswer,
  gateAnswers,
  setGateAnswer,
  ...rest
}) => {
  const expanded: string[] = useMemo(
    () => JSON.parse(gateAnswers?.["updated-survey-questions"] || "[]"),
    [gateAnswers],
  );
  const questionSets = useMemo(
    () => partitionReviewQuestions(prepareSurvey(survey, answers), expanded),
    [survey, answers, expanded],
  );
  return (
    <SurveyQuestionContainer {...rest}>
      {questionSets.map((questions, i) => (
        <RenderQuestionSet
          key={`question-set-${i}`}
          questions={questions}
          answers={answers}
          setAnswer={setAnswer}
          expanded={expanded}
          setGateAnswer={setGateAnswer}
          i={i}
        />
      ))}
    </SurveyQuestionContainer>
  );
};

/**
 * Splits survey questions into scalar types that support review.
 */
const partitionReviewQuestions = (
  questions: SurveyQuestionProps[],
  expanded: string[],
): SurveyQuestionProps[][] => {
  const output = [];
  if (questions.length) {
    let current: SurveyQuestionProps[] = [];
    for (const question of questions) {
      if (question.type.startsWith("custom-") || expanded.includes(question.slug)) {
        output.push(current);
        output.push([question]);
        current = [];
      } else {
        current.push(question);
      }
    }
    output.push(current);
  }
  return output.filter((o) => Array.isArray(o) && o.length > 0);
};

type SurveyReviewSetProps = {
  questions: SurveyQuestionProps[];
  answers: SurveyAnswers;
  setAnswer: SurveyAnswerFn;
  expanded: string[];
  setGateAnswer: SurveyAnswerFn;
  i: number;
};

const RenderQuestionSet: React.FC<SurveyReviewSetProps> = ({
  questions,
  answers,
  setAnswer,
  expanded,
  setGateAnswer,
  i,
}) => {
  if (!questions.length) return null;
  if (questions[0].type.startsWith("custom-") || expanded.includes(questions[0].slug)) {
    return (
      <React.Fragment key={`survey-review-question-${i}`}>
        {questions.map((q) => {
          let questionText: string | undefined = q.plaintextQuestion;
          if (!questionText && typeof q.question == "string") {
            questionText = q.question;
          }
          return (
            <SurveyQuestion
              key={q.slug}
              question={{
                ...q,
                visible: true,
              }}
              answer={answers[q.slug]}
              onAnswer={(answer) => setAnswer(q.slug, answer, questionText)}
            />
          );
        })}
      </React.Fragment>
    );
  } else {
    return (
      <RenderReviewSet
        questions={questions}
        answers={answers}
        setAnswer={setAnswer}
        expanded={expanded}
        setGateAnswer={setGateAnswer}
        i={i}
      />
    );
  }
};

const RenderReviewSet: React.FC<SurveyReviewSetProps> = ({
  questions,
  answers,
  setAnswer,
  expanded,
  setGateAnswer,
}) => {
  return (
    <Table className="mt-mdlg">
      <TableBody>
        {questions.length === 0 ? (
          <TableRow>
            <TableCell colSpan={99999}>
              <EmptyInfo>You have no survey questions to review</EmptyInfo>
            </TableCell>
          </TableRow>
        ) : (
          <>
            {questions.map((q) => {
              return (
                <SurveyQuestionRow
                  answers={answers}
                  setAnswer={setAnswer}
                  question={q}
                  expanded={expanded}
                  key={q.slug}
                  setGateAnswer={setGateAnswer}
                />
              );
            })}
          </>
        )}
      </TableBody>
    </Table>
  );
};

type SurveyQuestionRowProps = {
  question: SurveyQuestionProps;
  answers: SurveyAnswers;
  setAnswer: SurveyAnswerFn;
  expanded: string[];
  setGateAnswer: SurveyAnswerFn;
};

const SurveyQuestionRow: React.FC<SurveyQuestionRowProps> = ({
  question,
  answers,
  setAnswer,
  expanded,
  setGateAnswer,
}) => {
  const handleExpand = () => {
    const updatedArr = [...expanded];
    updatedArr.push(question.slug);
    setGateAnswer("updated-survey-questions", JSON.stringify(updatedArr));
  };

  return expanded.includes(question.slug) ? (
    <SurveyQuestionRowWrapper answers={answers} setAnswer={setAnswer} question={question} />
  ) : (
    <SurveyQuestionReviewRow
      answers={answers}
      setAnswer={setAnswer}
      question={question}
      onExpand={handleExpand}
    />
  );
};

type SurveyQuestionRowWrapperProps = {
  question: SurveyQuestionProps;
  answers: SurveyAnswers;
  setAnswer: SurveyAnswerFn;
};

// Review mode in a table row
const SurveyQuestionReviewRow: React.FC<
  SurveyQuestionRowWrapperProps & { onExpand: () => void }
> = ({ question: q, answers, setAnswer, onExpand }) => {
  return (
    <TableRow>
      <TableCell>
        <div className="text-component-question">{q.question}</div>
        {Boolean(q.helpText) && <p className="text-component-help">{q.helpText}</p>}
      </TableCell>
      <TableCell className={clsx({ "text-center": q.type === "boolean" })}>
        <SurveyQuestionShortAnswer
          key={q.slug}
          question={q}
          answer={answers[q.slug]}
          onAnswer={(answer) =>
            setAnswer(q.slug, answer, q.plaintextQuestion ?? (q.question as string))
          }
        />
      </TableCell>
      <TableCell>
        <Button
          variant="text"
          color="secondary"
          onClick={() => {
            onExpand();
          }}
          startIcon={<Edit />}
        >
          Update my Answer
        </Button>
      </TableCell>
    </TableRow>
  );
};

// Edit mode in a table row
const SurveyQuestionRowWrapper: React.FC<SurveyQuestionRowWrapperProps> = ({
  question,
  answers,
  setAnswer,
}) => {
  return (
    <TableRow>
      <TableCell colSpan={3}>
        <SurveyQuestionWrapper answers={answers} setAnswer={setAnswer} question={question} />
      </TableCell>
    </TableRow>
  );
};

const SurveyQuestionWrapper: React.FC<SurveyQuestionRowWrapperProps> = ({
  question: q,
  answers,
  setAnswer,
}) => {
  return (
    <SurveyQuestion
      key={q.slug}
      question={{
        ...q,
        visible: true,
      }}
      answer={answers[q.slug]}
      onAnswer={(answer) => setAnswer(q.slug, answer, q.plaintextQuestion)}
    />
  );
};
