import { FormControl } from "@mui/material";
import React, { useMemo } from "react";
import { SelectWithRecommendations } from "../../../../common/components/input/Select";
import {
  CookieRecommendationDto,
  OrganizationCookieDto,
} from "../../../../common/service/server/dto/CookieScanDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { useRelevantCookieRecommendations } from "../../../hooks/useCookieScanResult";
import { FIRST_PARTY_RECIPIENT_UUID, hasFirstPartyFlag } from "./CookieScanner";

type DataRecipientProps = {
  cookie: OrganizationCookieDto;
  recipients: OrganizationDataRecipientDto[];
  onChange: (recipient: UUIDString) => void;
  cookieRecommendations: CookieRecommendationDto[];
};

export const CookieDataRecipientSelect: React.FC<DataRecipientProps> = ({
  cookie,
  recipients,
  onChange,
  cookieRecommendations,
}) => {
  // on first load, we need to convert the firstParty flag (if set) into our fake "recipient"
  const selectedValue =
    cookie.firstParty && !hasFirstPartyFlag(cookie.dataRecipients)
      ? FIRST_PARTY_RECIPIENT_UUID
      : cookie.dataRecipients[0] ?? "";

  const options = useMemo(() => {
    const baseOptions = recipients.map((r) => ({ value: r.vendorId, label: r.name }));
    return [
      { value: FIRST_PARTY_RECIPIENT_UUID, label: "First Party", divider: true },
      ...baseOptions,
    ];
  }, [recipients]);

  const relevantRecommendations = useRelevantCookieRecommendations(cookie, cookieRecommendations);

  const recommendations = useMemo(() => {
    return (
      options?.filter((o) => relevantRecommendations.some((r) => r.vendorId === o.value)) ?? []
    );
  }, [options, relevantRecommendations]);

  if (!recipients) {
    return null;
  }

  return (
    <FormControl fullWidth className="w-256">
      <SelectWithRecommendations
        value={selectedValue}
        recommendations={recommendations}
        onChange={(newRecipient: unknown) => {
          onChange(newRecipient as UUIDString);
        }}
        options={options}
      />
    </FormControl>
  );
};
