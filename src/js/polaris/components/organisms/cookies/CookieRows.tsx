import { TableCell, TableRow } from "@mui/material";
import React from "react";
import {
  CookieCategory,
  CookieRecommendationDto,
  OrganizationCookieDto,
} from "../../../../common/service/server/dto/CookieScanDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { CookieTableRow } from "./CookieTableRow";

type CookieRowsProps = {
  domain: string;
  cookies: OrganizationCookieDto[];
  recipients: OrganizationDataRecipientDto[];
  onHide: (name: string) => void;
  onRestore: (name: string) => void;
  onChangeCategory: (name: string, categories: CookieCategory[]) => void;
  onChangeRecipients: (name: string, recipient: UUIDString) => void;
  cookieRecommendations: CookieRecommendationDto[];
};

export const CookieRows: React.FC<CookieRowsProps> = ({
  domain,
  cookies,
  recipients,
  onHide,
  onRestore,
  onChangeCategory,
  onChangeRecipients,
  cookieRecommendations,
}) => {
  return (
    <>
      <TableRow>
        <TableCell colSpan={4} className="heading-row">
          Domain: {domain}
        </TableCell>
      </TableRow>

      {cookies.map((c) => (
        <CookieTableRow
          key={c.name}
          cookie={c}
          recipients={recipients}
          onHide={() => onHide(c.name)}
          onRestore={() => onRestore(c.name)}
          onChangeCategory={(categories) => onChangeCategory(c.name, categories)}
          onChangeRecipients={(recipients) => onChangeRecipients(c.name, recipients)}
          cookieRecommendations={cookieRecommendations}
        />
      ))}
    </>
  );
};
