import React from "react";
import { Column, SortingRule } from "react-table";
import {
  PaginatedTable,
  PaginatedTableProps,
} from "../../../../common/components/table/PaginatedTable";
import { GdprCookieConsentDto } from "../../../../common/service/server/dto/GdprCookieConsentDto";
import { PaginatedMetaDto } from "../../../../common/service/server/dto/PaginatedDto";
import { fmtGdprCookieConsent } from "../../../formatters/enum";
import { makeColumns } from "../../../hooks/makeColumns";

const formatDate = (date?: string): string => {
  if (date === undefined || date === null || date === "") {
    return "";
  }

  return new Date(date).toLocaleDateString(undefined, {
    month: "short",
    day: "numeric",
    year: "numeric",
  });
};

const useColumns = makeColumns((): Column<any>[] => [
  {
    Header: "Visitor ID",
    id: "visitorId",
    accessor: "visitorId",
    Cell: ({ value }) => <code>{value}</code>,
  },
  {
    Header: "Consent Granted",
    id: "consentGranted",
    accessor: "consentGranted",
    Cell: ConsentCell,
  },
  {
    Header: "Date",
    id: "date",
    accessor: "date",
    Cell: ({ value }) => <>{formatDate(value)}</>,
  },
]);

const ConsentCell: React.FC<{ row: any }> = (props) => {
  const value = props.row.original as GdprCookieConsentDto;

  if (value.consentGranted.length == 0) {
    return <>None</>;
  }

  if (value.consentGranted.length == 4) {
    return <>All</>;
  }

  return (
    <>
      {value.consentGranted
        .map(fmtGdprCookieConsent)
        .filter((x) => x)
        .sort()
        .join(", ")}
    </>
  );
};

type Props = {
  data: GdprCookieConsentDto[];
  loading: boolean;
  pageMeta: PaginatedMetaDto;
  onPageChange: (pageIndex: number) => void | Promise<void>;
  onSortChange: (sortBy: SortingRule<any>[]) => void | Promise<void>;
  initialPageIndex: number;
  initialSortBy: SortingRule<any>[];
} & Partial<PaginatedTableProps>;

export const CookieConsentTable: React.FC<Props> = (rest) => {
  const columns = useColumns();

  return (
    <PaginatedTable enableRowClick={() => false} showFilters={false} columns={columns} {...rest} />
  );
};
