import { Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import React from "react";
import {
  CookieCategory,
  CookieRecommendationDto,
  OrganizationCookieDto,
} from "../../../../common/service/server/dto/CookieScanDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { CookieRows } from "../../../components/organisms/cookies/CookieRows";

type Props = {
  grouped: Record<string, OrganizationCookieDto[]>;
  recipients: OrganizationDataRecipientDto[];
  handleHide: (domain: string, name: string) => Promise<void>;
  handleRestore: (domain: string, name: string) => Promise<void>;
  handleChangeCategory: (
    domain: string,
    name: string,
    categories: CookieCategory[],
  ) => Promise<void>;
  handleChangeRecipients: (
    domain: string,
    name: string,
    recipientIds: UUIDString[],
  ) => Promise<void>;
  cookieRecommendations: CookieRecommendationDto[];
};

export const CookieTable: React.FC<Props> = ({
  grouped,
  recipients,
  handleHide,
  handleRestore,
  handleChangeCategory,
  handleChangeRecipients,
  cookieRecommendations,
}) => {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Name</TableCell>
          <TableCell>Type</TableCell>
          <TableCell>Data Recipient</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>

      <TableBody>
        {Object.keys(grouped).map((k) => {
          const val = grouped[k];

          return (
            <CookieRows
              key={k}
              cookies={val}
              recipients={recipients}
              domain={k}
              onHide={(name: string) => handleHide(k, name)}
              onRestore={(name: string) => handleRestore(k, name)}
              cookieRecommendations={cookieRecommendations}
              onChangeCategory={(name: string, categories: CookieCategory[]) =>
                handleChangeCategory(k, name, categories)
              }
              onChangeRecipients={(name: string, recipient: UUIDString) =>
                handleChangeRecipients(k, name, [recipient])
              }
            />
          );
        })}
      </TableBody>
    </Table>
  );
};
