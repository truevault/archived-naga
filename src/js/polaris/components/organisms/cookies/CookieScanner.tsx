import { Cookie } from "@mui/icons-material";
import {
  Button,
  Chip,
  Link,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Tooltip,
} from "@mui/material";
import { Box } from "@mui/system";
import _ from "lodash";
import React, { ReactNode, useEffect, useMemo, useState } from "react";
import { Form } from "react-final-form";
import { Text } from "../../../../common/components/input/Text";
import { Loading } from "../../../../common/components/Loading";
import { useActionRequest } from "../../../../common/hooks/api";
import { usePolling } from "../../../../common/hooks/useInterval";
import { NetworkRequestState } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import {
  CookieCategory,
  CookieRecommendationDto,
  CookieScanResultDto,
  OrganizationCookieDto,
} from "../../../../common/service/server/dto/CookieScanDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { OrganizationPublicId, UUIDString } from "../../../../common/service/server/Types";
import {
  useDataRecipientsCatalog,
  useOrganizationVendors,
  usePrimaryOrganizationId,
} from "../../../hooks";
import { useCookieRecommendations, useCookieScanResult } from "../../../hooks/useCookieScanResult";
import { Routes } from "../../../root/routes";
import { requiredUrl } from "../../../util/validation";
import { Callout, CalloutVariant } from "../../Callout";
import { DataRecipientLogo } from "../../DataRecipientLogo";
import { CookieDeveloperInstructionsDialog } from "../../dialog/CookieDeveloperInstructionsDialog";
import { Adornment } from "../../input/Adornment";
import { Para } from "../../typography/Para";
import { Paras } from "../../typography/Paras";
import { CookieTable } from "./CookieTable";

type UseCookieScanner = {
  requests: {
    cookieScanRequest: NetworkRequestState;
    cookieRecommendationsRequest: NetworkRequestState;
    organizationDataRecipientsRequest: NetworkRequestState;
    vendorCatalogRequest: NetworkRequestState;
  };
  isFinished: boolean;
  props: CookieScannerProps;
};

export type CookieScannerProps = {
  orgId: OrganizationPublicId;
  cookieScan: CookieScanResultDto;
  cookieRecommendations: CookieRecommendationDto[];
  hasMissingVendors: boolean;
  missingVendors: VendorDto[];
  handleScanStart: () => Promise<void>;
  handleResetScan: () => Promise<void>;
  setIsFinished: React.Dispatch<React.SetStateAction<boolean>>;
  showDeveloperInstructions: boolean;
  instructions: ReactNode;
};

export const useCookieScanner = (): UseCookieScanner => {
  const [isFinished, setIsFinished] = useState(false);
  const orgId = usePrimaryOrganizationId();
  const [cookieScan, cookieScanRequest, refresh] = useCookieScanResult(orgId);
  const [cookieRecommendations, cookieRecommendationsRequest] = useCookieRecommendations(orgId);
  const [organizationDataRecipients, organizationDataRecipientsRequest] =
    useOrganizationVendors(orgId);
  const [vendorCatalog, vendorCatalogRequest] = useDataRecipientsCatalog(orgId);

  const missingVendors = useMissingVendorsForRecommendations(
    cookieRecommendations,
    organizationDataRecipients,
    vendorCatalog,
  );
  const hasMissingVendors = useMemo(() => missingVendors.length > 0, [missingVendors]);

  // Handlers
  const { fetch: handleScanStart } = useActionRequest({
    api: async (url: string) => await Api.cookieScanner.startCookieScan(orgId, url),
    onSuccess: () => refresh(),
  });

  const { fetch: handleResetScan } = useActionRequest({
    api: async () => await Api.cookieScanner.resetCookieScan(orgId),
    onSuccess: () => refresh(),
  });

  // Poll for completion
  usePolling(() => refresh(), 10000, cookieScan?.status == "IN_PROGRESS");

  return {
    requests: {
      cookieScanRequest,
      cookieRecommendationsRequest,
      organizationDataRecipientsRequest,
      vendorCatalogRequest,
    },
    isFinished,
    props: {
      orgId,
      cookieScan,
      cookieRecommendations,
      hasMissingVendors,
      missingVendors,
      handleScanStart,
      handleResetScan,
      setIsFinished,
      showDeveloperInstructions: true,
      instructions: null,
    },
  };
};

export const CookieScanner: React.FC<CookieScannerProps> = ({
  orgId,
  cookieScan,
  cookieRecommendations,
  hasMissingVendors,
  missingVendors,
  handleScanStart,
  handleResetScan,
  setIsFinished,
  showDeveloperInstructions,
  instructions,
}) => {
  return (
    <>
      {instructions}

      {(cookieScan?.status == "NOT_STARTED" || cookieScan?.status == "RESET") && (
        <NotStartedBody onStartScan={handleScanStart} />
      )}
      {cookieScan?.status == "IN_PROGRESS" && <LoadingBody url={cookieScan?.scanUrl} />}
      {cookieScan?.status == "COMPLETE" && (
        <>
          <ResultBody
            orgId={orgId}
            scan={cookieScan}
            onIsFinishedChanged={(done) => setIsFinished(done)}
            onStartScan={handleScanStart}
            onResetScan={handleResetScan}
            cookieRecommendations={cookieRecommendations}
            hasMissingVendors={hasMissingVendors}
            missingVendors={missingVendors}
            showDeveloperInstructions={showDeveloperInstructions}
          />
        </>
      )}
      {cookieScan?.status == "ERROR" && (
        <ErrorBody
          url={cookieScan.scanUrl}
          error={cookieScan.error}
          onStartScan={handleScanStart}
        />
      )}
    </>
  );
};

const useMissingVendorsForRecommendations = (
  cookieRecommendations: CookieRecommendationDto[],
  organizationDataRecipients: OrganizationDataRecipientDto[],
  vendorCatalog: VendorDto[],
) => {
  const recommendedMissingVendorIds = useMemo(() => {
    const dataRecipientVendorIds = organizationDataRecipients?.map((dr) => dr.vendorId) ?? [];
    return (
      cookieRecommendations
        ?.filter((r) => !dataRecipientVendorIds.includes(r.vendorId))
        .map((r) => r.vendorId) ?? []
    );
  }, [cookieRecommendations, organizationDataRecipients]);

  const missingVendors = useMemo(
    () => vendorCatalog?.filter((v) => recommendedMissingVendorIds.includes(v.id)) ?? [],
    [vendorCatalog, recommendedMissingVendorIds],
  );

  return missingVendors;
};

// Some hacks to make "this is a first-party cookie" look like a "recipient"
export const FIRST_PARTY_RECIPIENT_UUID = "12345678-90ab-cdef-0123-4567890abcde";
function realRecipients(recipients: UUIDString[]): UUIDString[] {
  return recipients.filter((r) => r !== FIRST_PARTY_RECIPIENT_UUID);
}
export function hasFirstPartyFlag(recipients: UUIDString[]): boolean {
  return recipients.some((r) => r === FIRST_PARTY_RECIPIENT_UUID);
}

type NotStartedProps = StartScanFormProps;
const NotStartedBody: React.FC<NotStartedProps> = (props) => {
  return (
    <>
      <p>
        To get started, enter your website URL below so we can scan your website and generate a list
        of your cookies.
      </p>

      <StartScanForm {...props} />
    </>
  );
};

type StartScanFormProps = {
  onStartScan: (url: string) => void;
};
const StartScanForm: React.FC<StartScanFormProps> = ({ onStartScan }) => {
  const handleUrlSubmit = async (values: any) => {
    onStartScan(values.cookieScanUrl);
  };

  const validate = (values: any) => {
    const errors = {} as { [x: string]: string };
    requiredUrl(values, "cookieScanUrl", errors);
    return errors;
  };

  return (
    <Form
      onSubmit={handleUrlSubmit}
      validate={validate}
      render={({ handleSubmit, valid }) => {
        return (
          <form className="column-form" onSubmit={handleSubmit}>
            <Text
              multiline={false}
              variant="outlined"
              field="cookieScanUrl"
              initialValue={""}
              className="mr-md"
              fullWidth
              InputProps={{
                startAdornment: <Adornment position="start">https://</Adornment>,
              }}
              size="medium"
              required
              autoFocus
            />

            <Button size="large" type="submit" variant="contained" disabled={!valid}>
              Get Cookies
            </Button>
          </form>
        );
      }}
    ></Form>
  );
};

type LoadingBodyProps = {
  url: string;
};
const LoadingBody: React.FC<LoadingBodyProps> = ({ url }) => {
  return (
    <>
      <p>
        We're scanning {url} for cookies now. This usually takes a few minutes. This page will be
        updated when we're done.
      </p>
      <Loading padding="lg" />
    </>
  );
};

type ResultBodyProps = {
  orgId: OrganizationPublicId;
  scan: CookieScanResultDto;
  onIsFinishedChanged: (isFinished: boolean) => void;
  onStartScan: (url: string) => void;
  onResetScan: () => void;
  cookieRecommendations: CookieRecommendationDto[];
  hasMissingVendors: boolean;
  missingVendors: VendorDto[];
  showDeveloperInstructions: boolean;
};
const ResultBody: React.FC<ResultBodyProps> = ({
  orgId,
  scan,
  onIsFinishedChanged,
  onStartScan,
  onResetScan,
  cookieRecommendations,
  hasMissingVendors = false,
  missingVendors = [],
  showDeveloperInstructions,
}) => {
  const [localCookies, setLocalCookies] = useState(scan.cookies);
  const [previewOpen, setPreviewOpen] = useState(false);
  const openPreview = () => setPreviewOpen(true);
  const closePreview = () => setPreviewOpen(false);

  const [recipients, recipientsRequest] = useOrganizationVendors(orgId);

  // when we get a new remote cookies, update our local state
  useEffect(() => {
    setLocalCookies(scan.cookies);
  }, [scan.cookies]);

  const grouped: Record<string, OrganizationCookieDto[]> = useMemo(
    () => _.groupBy(localCookies, (c) => c.domain),
    [localCookies],
  );

  const cookieUpdate = async (
    domain: string,
    name: string,
    up: (val: OrganizationCookieDto) => void,
  ) => {
    setLocalCookies((c) => {
      const updated = [...c];
      const idx = updated.findIndex((c) => c.domain == domain && c.name == name);
      if (idx >= 0) {
        const cookie = updated[idx];
        up(cookie);

        const { hidden, firstParty, categories, dataRecipients } = cookie;

        // Make Api Request
        Api.cookieScanner.updateCookie(orgId, domain, name, {
          hidden,
          firstParty,
          categories,
          dataRecipients: realRecipients(dataRecipients),
        });
      }
      return updated;
    });
  };

  const handleHide = async (domain: string, name: string) => {
    cookieUpdate(domain, name, (cookie) => {
      cookie.hidden = true;
    });
  };

  const handleRestore = async (domain: string, name: string) => {
    cookieUpdate(domain, name, (cookie) => {
      cookie.hidden = false;
    });
  };

  const handleChangeCategory = async (
    domain: string,
    name: string,
    categories: CookieCategory[],
  ) => {
    cookieUpdate(domain, name, (cookie) => {
      cookie.categories = categories;
    });
  };

  const handleChangeRecipients = async (domain: string, name: string, recipients: UUIDString[]) => {
    cookieUpdate(domain, name, (cookie) => {
      cookie.dataRecipients = realRecipients(recipients);
      cookie.firstParty = hasFirstPartyFlag(recipients);
    });
  };

  const isFinished = useMemo(() => {
    return localCookies.every(
      (c) => c.hidden || c.firstParty || (c.dataRecipients.length > 0 && c.categories.length > 0),
    );
  }, [localCookies]);

  // notify parent when is finished changes
  useEffect(() => {
    onIsFinishedChanged(isFinished);
  }, [isFinished]);

  if (recipientsRequest.running) {
    return <Loading />;
  }

  return (
    <>
      <div className="flex flex-row align-items-center mb-lg">
        <h4 className="intermediate mt-0 mb-0">Cookies from {scan.scanUrl}</h4>
        <Button
          className="flex-grow"
          variant="contained"
          color="default"
          onClick={() => onStartScan(scan.scanUrl)}
        >
          Refresh
        </Button>
        <Tooltip title="Resetting your Cookie Scanner will clear all fetched cookies and allow you to set a new domain. This action cannot be undone.">
          <Button
            className="flex-grow"
            variant="contained"
            color="default"
            onClick={() => onResetScan()}
          >
            Reset
          </Button>
        </Tooltip>
      </div>

      {hasMissingVendors && (
        <Callout
          variant={CalloutVariant.Yellow}
          title="We noticed something you may have missed..."
          className="mb-lg"
          icon={Cookie}
        >
          <Paras>
            <Para>
              When scanning your site we found cookies that are usually set by data recipients not
              currently in your data map. We’ve listed them below. If you’d like to add them, return
              to the{" "}
              <Link className="color-yellow-900" href={Routes.getCompliant.dataRecipients.root}>
                Data Map Step
              </Link>
              .
            </Para>
          </Paras>

          <List className="mt-sm">
            {missingVendors.map((v) => (
              <ListItem disableGutters key={v.id}>
                <ListItemIcon>
                  <DataRecipientLogo dataRecipient={v} />
                </ListItemIcon>
                <ListItemText primary={v.name} />
              </ListItem>
            ))}
          </List>
        </Callout>
      )}

      <Paras>
        <Para component="span">
          Below are the cookies we retrieved from the domain above and prepopulated our recommended{" "}
          <Chip color="primary" label="* type" variant="muted" /> and{" "}
          <span className="color-primary-600">* data recipient</span> based on our cookie library.
        </Para>
      </Paras>

      <CookieTable
        grouped={grouped}
        recipients={recipients}
        handleHide={handleHide}
        handleRestore={handleRestore}
        handleChangeCategory={handleChangeCategory}
        handleChangeRecipients={handleChangeRecipients}
        cookieRecommendations={cookieRecommendations}
      />

      {showDeveloperInstructions && (
        <div className="text-right mt-md">
          <Button variant="contained" color="default" onClick={openPreview}>
            Preview Developer Instructions
          </Button>
        </div>
      )}

      <CookieDeveloperInstructionsDialog
        open={previewOpen}
        onClose={closePreview}
        scanUrl={scan.scanUrl}
        cookies={localCookies}
      />
    </>
  );
};

type ErrorBodyProps = {
  url: string | undefined;
  error: string | undefined;
} & StartScanFormProps;

const ErrorBody: React.FC<ErrorBodyProps> = ({ url, error, ...props }) => {
  return (
    <>
      <Callout variant={CalloutVariant.Red}>
        <p>
          <strong>Something went wrong while we were scanning {url}</strong>
        </p>

        <p>{error || "Please try again in a few minutes."}</p>
      </Callout>

      <Box className="mt-lg">
        <StartScanForm {...props} />
      </Box>
    </>
  );
};
