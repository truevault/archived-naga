import { Button, TableCell, TableRow, Tooltip } from "@mui/material";
import React from "react";
import {
  CookieCategory,
  CookieRecommendationDto,
  OrganizationCookieDto,
} from "../../../../common/service/server/dto/CookieScanDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { CookieCategorySelect } from "../../../components/organisms/cookies/CookieCategorySelect";
import { CookieDataRecipientSelect } from "../../../components/organisms/cookies/CookieDataRecipientSelect";
import { fmtDateFromNow } from "../../../formatters/date";

type CookieRowProps = {
  cookie: OrganizationCookieDto;
  recipients: OrganizationDataRecipientDto[];
  onHide: () => void;
  onRestore: () => void;
  onChangeCategory: (categories: CookieCategory[]) => void;
  onChangeRecipients: (recipient: UUIDString) => void;
  cookieRecommendations: CookieRecommendationDto[];
};

export const CookieTableRow: React.FC<CookieRowProps> = ({
  cookie,
  recipients,
  onHide,
  onRestore,
  onChangeCategory,
  onChangeRecipients,
  cookieRecommendations,
}) => {
  const detailsBody = (
    <>
      <p>Value: {cookie.value}</p>
      <p>Name: {cookie.name}</p>
      <p>Domain: {cookie.domain}</p>
      <p>Expires: {fmtDateFromNow(cookie.expires, false, "N/A")}</p>
      <p>HTTP Only: {cookie.httpOnly}</p>
      <p>Secure: {cookie.secure}</p>
      <p>Last Retrieved: {fmtDateFromNow(cookie.lastFetched, false, "N/A")}</p>
    </>
  );

  const details = (
    <Tooltip title={detailsBody}>
      <Button variant="text" color="secondary" onClick={(e) => e.preventDefault()}>
        Details
      </Button>
    </Tooltip>
  );

  if (cookie.hidden) {
    return (
      <TableRow>
        <TableCell colSpan={3}>
          <s>{cookie.name}</s>
        </TableCell>
        <TableCell className="text-right">
          {details}
          <Button
            variant="text"
            color="secondary"
            onClick={(e) => {
              e.preventDefault();
              onRestore();
            }}
          >
            Restore
          </Button>
        </TableCell>
      </TableRow>
    );
  }

  return (
    <TableRow>
      <TableCell>{cookie.name}</TableCell>
      <TableCell>
        <CookieCategorySelect
          cookieRecommendations={cookieRecommendations}
          cookie={cookie}
          onChange={onChangeCategory}
        />
      </TableCell>
      <TableCell>
        <CookieDataRecipientSelect
          cookie={cookie}
          recipients={recipients}
          onChange={onChangeRecipients}
          cookieRecommendations={cookieRecommendations}
        />
      </TableCell>
      <TableCell className="text-right">
        {details}
        <Button variant="text" color="secondary" onClick={() => onHide()}>
          Ignore
        </Button>
      </TableCell>
    </TableRow>
  );
};
