import { FormControl } from "@mui/material";
import React, { useMemo } from "react";
import { SelectWithRecommendations } from "../../../../common/components/input/Select";
import {
  CookieCategory,
  CookieRecommendationDto,
  OrganizationCookieDto,
} from "../../../../common/service/server/dto/CookieScanDto";
import { useRelevantCookieRecommendations } from "../../../hooks/useCookieScanResult";
import { fmtCookieCategory } from "../../../util/cookie";

type CategorySelectProps = {
  cookie: OrganizationCookieDto;
  onChange: (categories: CookieCategory[]) => void;
  cookieRecommendations: CookieRecommendationDto[];
};

const CATEGORY_SELECT_OPTIONS: CookieCategory[] = [
  "ANALYTICS",
  "FUNCTIONALITY",
  "ADVERTISING",
  "PERSONALIZATION",
  "SECURITY",
];

export const CookieCategorySelect: React.FC<CategorySelectProps> = ({
  cookie,
  onChange,
  cookieRecommendations,
}) => {
  const options = useMemo(() => {
    return CATEGORY_SELECT_OPTIONS.map((c) => ({ value: c, label: fmtCookieCategory(c) }));
  }, []);

  const relevantRecommendations = useRelevantCookieRecommendations(cookie, cookieRecommendations);

  const recommendations = useMemo(() => {
    return (
      options?.filter((o) => relevantRecommendations.some((r) => r.categories.includes(o.value))) ??
      []
    );
  }, [options, relevantRecommendations]);

  return (
    <FormControl fullWidth className="w-256">
      <SelectWithRecommendations
        multiple
        value={cookie.categories}
        onChange={(newCategories: any) => onChange(newCategories as CookieCategory[])}
        options={options}
        recommendations={recommendations}
      />
    </FormControl>
  );
};
