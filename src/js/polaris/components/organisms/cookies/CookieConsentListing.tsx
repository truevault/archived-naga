import React from "react";
import { Button } from "@mui/material";
import { useState } from "react";
import { NetworkRequestState } from "../../../../common/models";
import { ApiRouteHelpers, formatUrl } from "../../../../common/root/apiRoutes";
import { PaginatedGdprCookieConsentDto } from "../../../../common/service/server/dto/GdprCookieConsentDto";
import { SortRule, SortRuleDto } from "../../../../common/service/server/dto/SortRuleDto";
import { usePrimaryOrganization } from "../../../hooks";
import { usePaginatedCookieConsents } from "../../../hooks/useCookieConsent";
import { CookieConsentTable } from "./CookieConsentTable";

type UseCookieConsentListing = {
  requests: {
    cookieConsent: NetworkRequestState;
  };
  props: CookieConsentListingProps;
};

type CookieConsentListingProps = {
  cookieConsent: PaginatedGdprCookieConsentDto;
  setPageParam: React.Dispatch<React.SetStateAction<number>>;
  setSortParam: React.Dispatch<React.SetStateAction<SortRuleDto[]>>;
  pageParam: number;
  sortParam: SortRuleDto[];
  loading: boolean;
  downloadUrl: string;
};

export const useCookieConsentListing = (): UseCookieConsentListing => {
  const [org] = usePrimaryOrganization();

  const [pageParam, setPageParam] = useState(0);
  const [sortParam, setSortParam] = useState<SortRule[]>([]);

  const [cookieConsent, cookieConsentRequest] = usePaginatedCookieConsents(
    org.id,
    sortParam,
    Math.max(pageParam - 1, 0),
  );

  return {
    requests: {
      cookieConsent: cookieConsentRequest,
    },
    props: {
      cookieConsent,
      pageParam,
      sortParam,
      setPageParam,
      setSortParam,
      loading: cookieConsentRequest.running,
      downloadUrl: formatUrl(ApiRouteHelpers.organization.cookieConsent.csv(org.id), {}),
    },
  };
};

export const CookieConsentListing: React.FC<CookieConsentListingProps> = ({
  loading,
  downloadUrl,
  cookieConsent,
  setPageParam,
  setSortParam,
  pageParam,
  sortParam,
}) => {
  return (
    <>
      <div className="mt-md">
        {cookieConsent.meta.count > 0 && (
          <Button href={downloadUrl} download variant="contained" color="primary">
            Download as CSV
          </Button>
        )}
      </div>

      <div className="mt-xl">
        <CookieConsentTable
          data={cookieConsent?.data}
          loading={loading}
          pageMeta={cookieConsent?.meta}
          onPageChange={setPageParam}
          onSortChange={setSortParam}
          initialPageIndex={pageParam - 1}
          initialSortBy={sortParam}
        />
      </div>
    </>
  );
};
