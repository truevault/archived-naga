import { Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import React, { useMemo } from "react";
import { OrganizationCookieDto } from "../../../../common/service/server/dto/CookieScanDto";
import { cookieSorter, fmtCookieCategory } from "../../../util/cookie";

type Props = {
  cookies?: OrganizationCookieDto[];
};

export const DeveloperCookieTable: React.FC<Props> = ({ cookies }) => {
  const sorted = useMemo(() => cookies?.sort(cookieSorter) ?? [], [cookies]);

  return (
    <Table className="mt-mdlg">
      <TableHead>
        <TableRow>
          <TableCell>Name</TableCell>
          <TableCell>Domain</TableCell>
          <TableCell>Cookie Type</TableCell>
        </TableRow>
      </TableHead>

      <TableBody>
        {sorted && sorted.length > 0 ? (
          sorted
            .filter((c) => !c.hidden)
            .map((c) => {
              return (
                <TableRow key={`${c.domain}-${c.name}`}>
                  <TableCell>
                    <code>{c.name}</code>
                  </TableCell>
                  <TableCell>
                    <code>{c.domain}</code>
                  </TableCell>
                  <TableCell>{c.categories.map(fmtCookieCategory).join(", ")}</TableCell>
                </TableRow>
              );
            })
        ) : (
          <TableRow>
            <TableCell colSpan={1000}>No Cookies</TableCell>
          </TableRow>
        )}
      </TableBody>
    </Table>
  );
};
