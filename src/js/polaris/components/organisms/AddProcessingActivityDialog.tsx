import { Add } from "@mui/icons-material";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  FormGroup,
  FormHelperText,
  FormLabel,
} from "@mui/material";
import _ from "lodash";
import React, { useMemo, useState } from "react";
import { Field, Form, FormSpy } from "react-final-form";
import { Select, SelectOption } from "../../../common/components/input/Select";
import { Text } from "../../../common/components/input/Text";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { NetworkRequestGate } from "../../../common/components/NetworkRequestGate";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import {
  AutocreatedProcessingActivitySlug,
  DefProcessingActivityDto,
  LawfulBasis,
  ProcessingActivityDto,
} from "../../../common/service/server/dto/ProcessingActivityDto";
import { OrganizationPublicId } from "../../../common/service/server/Types";
import { CloseableDialogTitle } from "../../components/dialog/CloseableDialogTitle";
import { ProcessingActivityLawfulBasisSelect } from "../../components/organisms/ProcessingActivityLawfulBasisSelect";
import { useDefaultProcessingActivites } from "../../hooks/useProcessingActivities";
import { validateRequired } from "../../util/validation";

type AddProcessingActivityFormValues = {
  name?: string;
  defaultId?: string;
  lawfulBases: LawfulBasis[];
};

const defaultActivityToStubActivity = (
  defActivity?: DefProcessingActivityDto,
): ProcessingActivityDto => {
  if (!defActivity) {
    return null;
  }

  return {
    id: `stub-activity-${defActivity.id}`,
    name: defActivity.name,
    defaultId: defActivity.id,
    defaultMandatoryLawfulBases: defActivity.mandatoryLawfulBases,
    defaultOptionalLawfulBases: defActivity.optionalLawfulBases,
    autocreationSlug: defActivity.slug as AutocreatedProcessingActivitySlug,
    isDefault: true,
    lawfulBases: [],
    processedPic: [],
    defaultSlug: defActivity.slug,
    regions: ["UNITED_STATES", "EEA_UK"],
  };
};

const CUSTOM_FLAG = "CUSTOM";

export const AddProcessingActivityDialog: React.FC<{
  orgId: OrganizationPublicId;
  existingActivities: ProcessingActivityDto[];
  onAdd: (newActivity: ProcessingActivityDto) => Promise<void> | void;
}> = ({ orgId, onAdd, existingActivities = [] }) => {
  const [open, setOpen] = useState(false);
  const onClose = () => setOpen(false);

  const [defActivites, defActivityRequest] = useDefaultProcessingActivites();

  const { fetch: createProcessingActivity } = useActionRequest({
    api: (values: AddProcessingActivityFormValues) =>
      Api.processingActivities.createProcessingActivity(orgId, {
        name: values.name,
        defaultId: values.defaultId === CUSTOM_FLAG ? undefined : values.defaultId,
        lawfulBases: values.lawfulBases,
        regions: ["UNITED_STATES", "EEA_UK"],
      }),
    onSuccess: (activity) => {
      onClose();

      if (onAdd) {
        onAdd(activity);
      }
    },
  });

  const onSubmit = (values: AddProcessingActivityFormValues) => {
    createProcessingActivity(values);
  };

  const processingActivityOptions: SelectOption<string>[] = useMemo(() => {
    const defaultProcessingActivityOptions =
      defActivites?.map((a) => ({
        value: a.id,
        label: a.name,
        disabled: existingActivities.some((ea) => ea.defaultId === a.id),
      })) ?? [];

    return [
      { value: CUSTOM_FLAG, label: "Custom Activity...", divider: true },
      ...defaultProcessingActivityOptions,
    ];
  }, [defActivites, existingActivities]);

  return (
    <>
      <Button color="secondary" startIcon={<Add />} onClick={() => setOpen(true)}>
        Add Processing Activity
      </Button>

      <Dialog open={open} onClose={onClose} fullWidth>
        <CloseableDialogTitle onClose={onClose}>Add Processing Activity</CloseableDialogTitle>

        <NetworkRequestGate requests={[defActivityRequest]}>
          <Form onSubmit={onSubmit}>
            {({ submitting, form, handleSubmit, values }) => (
              <form onSubmit={handleSubmit}>
                <DialogContent>
                  <Select
                    options={processingActivityOptions}
                    field="defaultId"
                    label="Processing Activity"
                    required
                    validate={validateRequired}
                    className="mb-md"
                    formControlProps={{ fullWidth: true }}
                  />

                  {values.defaultId === CUSTOM_FLAG && (
                    <Text
                      field="name"
                      label="Processing Activity Name"
                      required
                      validate={validateRequired}
                      className="mb-md"
                    />
                  )}

                  <FormSpy
                    subscription={{ values: true }}
                    onChange={({ values: newValues }) => {
                      if (newValues.defaultId !== values.defaultId) {
                        const defaultActivity = defActivites.find(
                          (a) => a.id === newValues.defaultId,
                        );

                        if (defaultActivity) {
                          const newLawfulBases = [
                            ...defaultActivity.optionalLawfulBases,
                            ...defaultActivity.mandatoryLawfulBases,
                          ];

                          if (!_.isEqual(newValues.lawfulBases, newLawfulBases)) {
                            form.change("lawfulBases", newLawfulBases);
                          }
                        }
                      }
                    }}
                  />

                  <Field
                    name="lawfulBases"
                    validate={validateRequired}
                    render={({ input, meta }) => {
                      const defaultActivity = defActivites.find((a) => a.id === values.defaultId);
                      const processingActivity = defaultActivityToStubActivity(defaultActivity);
                      const hasError = Boolean(meta.touched && meta.error);

                      return (
                        <FormGroup>
                          <FormLabel>Lawful Bases</FormLabel>

                          <ProcessingActivityLawfulBasisSelect
                            activity={processingActivity}
                            value={input.value}
                            label="Lawful Bases"
                            onChange={async (newBases) => await input.onChange(newBases)}
                            error={hasError}
                            fullWidth
                          />

                          {hasError && <FormHelperText>{meta.error}</FormHelperText>}
                        </FormGroup>
                      );
                    }}
                  />
                </DialogContent>

                <DialogActions>
                  <LoadingButton type="submit" loading={submitting}>
                    Add Processing Activity
                  </LoadingButton>
                </DialogActions>
              </form>
            )}
          </Form>
        </NetworkRequestGate>
      </Dialog>
    </>
  );
};
