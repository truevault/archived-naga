import { Button, MobileStepper, Typography } from "@mui/material";
import React, { ReactNode, useState } from "react";
import { FadeTransition } from "../../pages/get-compliant/FadeTransition";

export type ImageStepperStep = {
  title: ReactNode;
  subtitle?: ReactNode;
  imageSrc: string;
  nextButtonLabel?: string;
  canSkip?: boolean;
};

export const ImageStepper: React.FC<{
  steps: ImageStepperStep[];
  handleDone: () => Promise<void>;
  handleFirstBack: () => Promise<void>;
}> = ({ steps, handleDone, handleFirstBack }) => {
  const [activeStepIdx, setActiveStepIdx] = useState(0);
  const maxSteps = steps.length;
  const isFinalStep = activeStepIdx === maxSteps - 1;
  const isFirstStep = activeStepIdx === 0;
  const activeStep = steps[activeStepIdx];

  const handleNext = () => {
    setActiveStepIdx((prevActiveStep) => prevActiveStep + 1);
  };

  const handlePrev = () => {
    setActiveStepIdx((prevActiveStep) => prevActiveStep - 1);
  };

  return (
    <div className="image_stepper">
      <div className="image_stepper__content">
        <div className="image_stepper__frame" style={{ height: 400 }}>
          {steps.map((step, idx) => {
            return (
              <FadeTransition
                cls="survey-question-fade"
                unmountOnExit
                appear
                exit={0}
                enter={300}
                in={idx === activeStepIdx}
                key={step.imageSrc}
              >
                <img className="image_stepper__image" src={step.imageSrc} style={{ height: 400 }} />
              </FadeTransition>
            );
          })}
        </div>

        <header className="image_stepper__header color-primary-800">
          <Typography className="image_stepper__title" variant="h5">
            {activeStep.title}
          </Typography>

          {activeStep.subtitle && (
            <Typography className="image_stepper__subtitle" variant="subtitle2">
              {activeStep.subtitle}
            </Typography>
          )}
        </header>

        <MobileStepper
          className="image_stepper__stepper_nav max-width-400"
          position="static"
          steps={maxSteps}
          activeStep={activeStepIdx}
          backButton={
            <Button
              color="secondary"
              size="small"
              onClick={isFirstStep ? handleFirstBack : handlePrev}
            >
              Back
            </Button>
          }
          nextButton={
            <div>
              <Button
                variant="contained"
                color={isFinalStep ? "primary" : "default"}
                size="small"
                onClick={isFinalStep ? handleDone : handleNext}
              >
                {activeStep.nextButtonLabel ?? "Next"}
              </Button>

              {activeStep.canSkip !== false && (
                <Button color="secondary" className="ml-sm" size="small" onClick={handleDone}>
                  Skip
                </Button>
              )}
            </div>
          }
        />
      </div>
    </div>
  );
};
