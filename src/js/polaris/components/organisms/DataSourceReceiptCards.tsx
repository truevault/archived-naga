import { Button, Typography } from "@mui/material";
import clsx from "clsx";
import pluralize from "pluralize";
import { uniq } from "lodash";
import React, { useState } from "react";
import { CategoryDto } from "../../../common/service/server/dto/DataMapDto";
import { OrganizationDataSourceDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { SOURCE_KEY_CONSUMER_DIRECTLY } from "../../types/Vendor";
import { CalloutVariant } from "../Callout";
import { DataRecipientCollapseCard } from "../DataRecipientCollapseCard";

type DataSourceReceiptCardsProps = {
  sources: OrganizationDataSourceDto[];
  disclosuresByVendorId: Record<string, CategoryDto[]>;
  renderer: (recipient: OrganizationDataSourceDto) => React.ReactNode;
  consumerDirectlyName?: string;
};

export const DataSourceReceiptCards: React.FC<DataSourceReceiptCardsProps> = ({
  sources,
  disclosuresByVendorId,
  renderer,
  consumerDirectlyName,
}) => {
  const [expanded, setExpanded] = useState<null | string>(sources[0]?.vendorId);

  return (
    <>
      {sources.map((r, idx) => {
        const next = idx != sources.length - 1 ? sources[idx + 1] : null;
        const recipientName =
          r.vendorKey == SOURCE_KEY_CONSUMER_DIRECTLY ? consumerDirectlyName : undefined;

        const handleExpand = () => {
          setExpanded(r.vendorId);
          setTimeout(() => window.polaris.scrollSelectorIntoView(`#dr-${r.vendorId}`), 300);
        };

        const handleNext = () => {
          setExpanded(next?.vendorId ?? null);
          setTimeout(() => window.polaris.scrollSelectorIntoView(`#dr-${r.vendorId}`), 300);
        };

        const sourceCount = uniq(disclosuresByVendorId[r.vendorId])?.length ?? 0;
        const sourceLabel = pluralize("Categories", sourceCount, true);

        return (
          <div id={`dr-${r.vendorId}`} key={r.vendorId}>
            <DataRecipientCollapseCard
              recipient={r}
              recipientName={recipientName}
              collapsed={expanded != r.vendorId}
              variant={CalloutVariant.LightPurpleBordered}
              onExpand={handleExpand}
              headerContent={
                <Typography
                  variant="caption"
                  className={clsx({
                    "text-error": sourceCount === 0,
                    "text-muted": sourceCount > 0,
                  })}
                >
                  {sourceLabel} Selected
                </Typography>
              }
            >
              <div>{renderer(r)}</div>

              {Boolean(next) && (
                <Button
                  className="mt-lg mr-lg"
                  variant="contained"
                  size="small"
                  onClick={handleNext}
                >
                  Next Source
                </Button>
              )}
            </DataRecipientCollapseCard>
          </div>
        );
      })}
    </>
  );
};
