import { LoadingButton } from "@mui/lab";
import { FormControl, TableCell, TableRow } from "@mui/material";
import React, { useState } from "react";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { ProcessingRegion } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  LawfulBasis,
  ProcessingActivityDto,
} from "../../../common/service/server/dto/ProcessingActivityDto";
import { usePrimaryOrganizationId } from "../../hooks";
import { ConfirmDialog } from "../dialog";
import { ProcessingActivityLawfulBasisSelect } from "./ProcessingActivityLawfulBasisSelect";
import { Disableable } from "../Disableable";

type ProcessingActivityTableRowProps = {
  activity: ProcessingActivityDto;
  selected: LawfulBasis[];
  region: ProcessingRegion;
  onSetLawfulBases: (bases: LawfulBasis[]) => Promise<void>;
  onRemove?: () => Promise<void>;
  extendedOptions: boolean;
  disableRemovalForMandatory?: boolean;
};

export const ProcessingActivityTableRow: React.FC<ProcessingActivityTableRowProps> = ({
  activity,
  selected,
  region,
  onSetLawfulBases,
  extendedOptions,
  onRemove,
  disableRemovalForMandatory,
}) => {
  const [confirmOpen, setConfirmOpen] = useState(false);
  const orgId = usePrimaryOrganizationId();

  const { fetch: removeProcessingActivity, request: deleteRequest } = useActionRequest({
    api: () =>
      Api.processingActivities.deleteProcessingActivityRegion(orgId, activity.id, [region]),
    onSuccess: () => {
      if (onRemove) {
        onRemove();
      }
    },
  });

  const isAutomatedDecisionMaking = activity?.autocreationSlug == "AUTOMATED_DECISION_MAKING";
  const isColdCalls = activity?.autocreationSlug == "COLD_SALES_COMMUNICATIONS";

  const disableRemove = disableRemovalForMandatory && (isAutomatedDecisionMaking || isColdCalls);

  return (
    <TableRow>
      <TableCell>{activity.name}</TableCell>
      <TableCell>
        <FormControl fullWidth className="w-512">
          <ProcessingActivityLawfulBasisSelect
            value={selected}
            showExtendedOptions={extendedOptions}
            activity={activity}
            onChange={onSetLawfulBases}
          />
        </FormControl>
      </TableCell>

      <TableCell>
        <Disableable
          disabledTooltip="You cannot remove this processing activity due your answers to the questions above"
          disabled={disableRemove}
        >
          <LoadingButton
            color="secondary"
            disabled={disableRemove}
            loading={deleteRequest.running}
            onClick={() => setConfirmOpen(true)}
          >
            Remove
          </LoadingButton>
        </Disableable>

        <ConfirmDialog
          confirmTitle="Are you sure?"
          contentText="Are you sure you want to remove this processing activity?"
          open={confirmOpen}
          onClose={() => setConfirmOpen(false)}
          onConfirm={removeProcessingActivity}
          confirmLoading={deleteRequest.running}
        />
      </TableCell>
    </TableRow>
  );
};
