import { Stack } from "@mui/material";
import React, { useMemo } from "react";
import { CategoryDto } from "../../../common/service/server/dto/DataMapDto";
import { UUIDString } from "../../../common/service/server/Types";
import { DescriptionCheckbox } from "../input/DescriptionCheckbox";

type DataSourceReceiptCheckboxesProps = {
  onChange: (picGroupId: UUIDString, checked: boolean) => void;
  collectedPic: CategoryDto[];
  selected: UUIDString[];
  pending?: UUIDString[];
  includeSelectAll?: boolean;
  onSelectAll?: (received: boolean) => void;
};

export const DataSourceReceiptCheckboxes: React.FC<DataSourceReceiptCheckboxesProps> = ({
  onChange,
  collectedPic = [],
  selected = [],
  pending,
  includeSelectAll = false,
  onSelectAll,
}) => {
  const allSelected = useMemo(
    () => collectedPic.every((pic) => selected?.includes(pic.id)),
    [selected, collectedPic],
  );

  const handleSelectAll = () => {
    onSelectAll?.(!allSelected);
  };

  return (
    <>
      <Stack spacing={2}>
        {includeSelectAll && (
          <>
            <DescriptionCheckbox
              label="Select All"
              checked={allSelected}
              onChange={handleSelectAll}
            />
            <hr className="my-md" />
          </>
        )}

        {collectedPic.map((g) => {
          return (
            <DescriptionCheckbox
              key={g.id}
              label={g.name}
              checked={selected?.includes(g.id)}
              loading={pending && pending.includes(g.id)}
              onChange={(_, checked) => onChange(g.id, checked)}
            />
          );
        })}
      </Stack>
    </>
  );
};
