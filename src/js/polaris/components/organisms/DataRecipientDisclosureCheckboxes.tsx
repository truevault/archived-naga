import { Stack } from "@mui/material";
import React, { useMemo, useState } from "react";
import {
  DataMapDto,
  PICGroup,
  recommendationsForDataRecipient,
} from "../../../common/service/server/dto/DataMapDto";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../common/service/server/Types";
import { DESCRIPTION_OVERRIDES } from "../../pages/get-compliant/data-recipients/DataRecipientCollectionCheckboxes";
import { DescriptionCheckbox } from "../input/DescriptionCheckbox";
import { RecipientGuidance } from "./data-recipients/RecipientGuidance";

type DataRecipientDisclosureCheckboxesProps = {
  toggleDisclosure: (params?: any) => Promise<void>;
  collectedPicGroups: PICGroup[];
  selected: UUIDString[];
  showRecommendations?: boolean;
  dataMap?: DataMapDto;
  dataRecipient: OrganizationDataRecipientDto;
};

export const DataRecipientDisclosureCheckboxes: React.FC<
  DataRecipientDisclosureCheckboxesProps
> = ({
  toggleDisclosure,
  dataMap,
  collectedPicGroups,
  selected,
  dataRecipient,
  showRecommendations = false,
}) => {
  const { recommendedGroups } = useMemo(
    () =>
      dataMap && showRecommendations
        ? recommendationsForDataRecipient(dataMap, dataRecipient)
        : { recommendedGroups: [] },
    [dataMap, dataRecipient, showRecommendations],
  );

  const handleOnChange = async (disclosedId: UUIDString, disclosed: boolean) => {
    await toggleDisclosure({
      vendorId: dataRecipient.vendorId,
      disclosedId,
      disclosed,
    });
  };

  return (
    <>
      <RecipientGuidance recipient={dataRecipient} />
      <Stack spacing={2}>
        {collectedPicGroups.map((picGroup) => (
          <DataRecipientDisclosureCheckbox
            key={picGroup.id}
            picGroup={picGroup}
            selected={selected}
            recommendedGroups={recommendedGroups}
            onChange={handleOnChange}
          />
        ))}
      </Stack>
    </>
  );
};

const DataRecipientDisclosureCheckbox = ({
  picGroup,
  selected,
  recommendedGroups,
  onChange,
}: {
  picGroup: PICGroup;
  selected: UUIDString[];
  recommendedGroups: PICGroup[];
  onChange: (disclosedId: UUIDString, disclosed: boolean) => Promise<void>;
}) => {
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState(selected?.includes(picGroup.id));

  const handleOnChange = async (_, checked: boolean) => {
    setValue(checked);
    setLoading(true);
    try {
      await onChange(picGroup.id, checked);
    } finally {
      setLoading(false);
    }
  };

  const collected = picGroup.categories.map((c) => c.name).join(", ");
  const description = DESCRIPTION_OVERRIDES[picGroup.key] ?? `You collect: ${collected}`;
  const recommended = recommendedGroups?.some((rg) => rg.id === picGroup.id);

  return (
    <DescriptionCheckbox
      label={picGroup.name}
      description={description}
      checked={value}
      variant={recommended ? "light-purple" : null}
      onChange={handleOnChange}
      loading={loading}
    />
  );
};
