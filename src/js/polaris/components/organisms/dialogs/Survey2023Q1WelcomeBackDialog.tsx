import { Button } from "@mui/material";
import React, { useState } from "react";
import { Routes } from "../../../root/routes";
import { HelpInterstitialDialog } from "../../dialog";
import { useSelf } from "../../../hooks";

export const Survey2023Q1WelcomeBackDialog: React.FC = () => {
  const [closeSignal, setCloseSignal] = useState(false);
  const onClose = () => setCloseSignal(true);
  const [self] = useSelf();
  return (
    <HelpInterstitialDialog
      uniqueId="survey-2023-q1-welcome-back"
      title="Complete Your Q1 2023 Survey"
      closeSignal={closeSignal}
      maxWidth={"sm"}
      lastLogin={self.lastLogin}
      customActions={
        <>
          <Button
            type="submit"
            color="default"
            variant="contained"
            onClick={onClose}
            className="mr-sm"
          >
            Maybe Later
          </Button>
          <Button
            type="submit"
            color="primary"
            variant="contained"
            href={Routes.surveys["2023"].root}
            onClick={onClose}
          >
            Let's Go
          </Button>
        </>
      }
    >
      <div className="flex-row">
        <div className="mr-md">
          <p>Some things have changed since the last time you were here.</p>
          <p>
            We have a <strong>10 minute survey</strong> to catch you up with CPRA and new US
            regulations.
          </p>
        </div>
        <img src="/assets/images/surveys/2023q1/welcome-back-hero.svg" />
      </div>
    </HelpInterstitialDialog>
  );
};
