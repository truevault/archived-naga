import React from "react";
import { BooleanQuestion, BooleanQuestionProps } from "./survey/BooleanQuestion";

type Props = { value: boolean; onChange: (answer: boolean) => Promise<void> } & Omit<
  BooleanQuestionProps,
  "answer" | "setAnswer" | "slug" | "type"
>;

export const SimpleBooleanQuestion: React.FC<Props> = ({ value, onChange, ...rest }) => {
  return (
    <div className="survey--question">
      <BooleanQuestion
        {...rest}
        answer={JSON.stringify(value)}
        setAnswer={(answer: string) => onChange(answer === "true")}
      />
    </div>
  );
};
