import { Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import React from "react";
import { ProcessingRegion } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import {
  LawfulBasis,
  ProcessingActivityDto,
} from "../../../common/service/server/dto/ProcessingActivityDto";
import { UUIDString } from "../../../common/service/server/Types";
import { EmptyInfo } from "../Empty";
import { ProcessingActivityTableRow } from "./ProcessingActivityTableRow";

type Props = {
  activities: ProcessingActivityDto[];
  region?: ProcessingRegion;
  onChange: (args: { activityId: UUIDString; bases: LawfulBasis[] }) => Promise<void>;
  onRemove: () => Promise<void>;
  showExtendedOptions?: boolean;
  disableRemovalForMandatory?: boolean;
};

export const ProcessingActivitySettingsTable: React.FC<Props> = ({
  activities,
  region = "EEA_UK",
  onChange,
  onRemove,
  showExtendedOptions = false,
  disableRemovalForMandatory = false,
}) => {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Processing Activity</TableCell>
          <TableCell>Lawful Basis</TableCell>
          <TableCell>Actions</TableCell>
        </TableRow>
      </TableHead>

      <TableBody>
        {activities.length > 0 ? (
          <>
            {activities.map((act) => {
              return (
                <ProcessingActivityTableRow
                  key={act.id}
                  activity={act}
                  selected={act.lawfulBases}
                  region={region}
                  onSetLawfulBases={(bases) => onChange({ activityId: act.id, bases })}
                  onRemove={onRemove}
                  extendedOptions={showExtendedOptions}
                  disableRemovalForMandatory={disableRemovalForMandatory}
                />
              );
            })}
          </>
        ) : (
          <TableRow>
            <TableCell colSpan={3}>
              <EmptyInfo>No processing activities</EmptyInfo>
            </TableCell>
          </TableRow>
        )}
      </TableBody>
    </Table>
  );
};
