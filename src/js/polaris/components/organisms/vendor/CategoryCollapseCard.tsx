import React from "react";
import { Tooltip, Typography } from "@mui/material";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { vendorDisabled } from "../../../pages/design/organisms/data-recipients/CommonDataRecipients";
import { RowVendorTile } from "../../../pages/stay-compliant/data-recipients/RowVendorTile";
import { Callout, CalloutVariant } from "../../Callout";
import { DataRecipientLogo } from "../../DataRecipientLogo";
import { EmptyInfo } from "../../Empty";
import { VendorCategory } from "./VendorCategories";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";

type VendorIconExtraProps = {
  vendors: VendorDto[] | OrganizationDataRecipientDto[];
};
export const VendorIconExtra: React.FC<VendorIconExtraProps> = ({ vendors }) => {
  if (vendors.length == 0) {
    return (
      <Typography variant="body2" className="text-muted">
        None Added
      </Typography>
    );
  }

  return (
    <div className="flex flex-row">
      {vendors.slice(0, 4).map((v) => (
        <Tooltip title={v.name} key={v.id}>
          <DataRecipientLogo className="mr-xs" dataRecipient={v} size={24} />
        </Tooltip>
      ))}
    </div>
  );
};

type VendorColumnProps = {
  vendors: VendorDto[] | OrganizationDataRecipientDto[];
  pendingVendor: UUIDString | null;
  handleRemove: (vendor: VendorDto) => void;
};
export const VendorColumn: React.FC<VendorColumnProps> = ({
  vendors,
  pendingVendor,
  handleRemove,
}) => {
  return (
    <TransitionGroup>
      {vendors.map((vendor) => {
        return (
          <FadeTransition key={`vendor-inventory-${vendor.id}`}>
            <div className="mb-xs">
              <RowVendorTile
                name={vendor.name}
                vendor={vendor}
                category={vendor.category}
                logoUrl={vendor.logoUrl}
                added={true}
                pending={vendor.id == pendingVendor}
                disabled={vendorDisabled(vendor, true)}
                selected={false}
                handleRemoveVendor={() => handleRemove(vendor)}
              />
            </div>
          </FadeTransition>
        );
      })}
    </TransitionGroup>
  );
};

type CategoryCollapseCardProps = {
  category: VendorCategory;
  vendors: VendorDto[] | OrganizationDataRecipientDto[];
  pendingVendor?: UUIDString | null;
  handleRemove: (vendor: VendorDto) => void;
  emptyText?: React.ReactNode;
};

export const CategoryCollapseCard: React.FC<CategoryCollapseCardProps> = ({
  category,
  vendors,
  handleRemove,
  emptyText = "Nothing to see here!",
  pendingVendor = null,
}) => {
  return (
    <Callout
      variant={CalloutVariant.LightPurpleBordered}
      collapsible
      collapsed
      icon={category.icon}
      subtitle={category.description}
      title={category.label}
      action={<VendorIconExtra vendors={vendors} />}
    >
      {vendors.length > 0 ? (
        <VendorColumn vendors={vendors} pendingVendor={pendingVendor} handleRemove={handleRemove} />
      ) : (
        <div className="text-center">
          <EmptyInfo Icon={null}>{emptyText}</EmptyInfo>
        </div>
      )}
    </Callout>
  );
};

const FadeTransition = (props) => (
  <CSSTransition
    {...props}
    classNames="add-vendors--vendor-inventory"
    timeout={{ enter: 250, exit: 250 }}
  />
);
