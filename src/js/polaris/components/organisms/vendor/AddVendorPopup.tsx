import React from "react";
import { ClickAwayListener, Paper, Popper, TextField } from "@mui/material";
import { useEffect, useMemo, useState } from "react";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import {
  CustomVendorDialog,
  CustomVendorFormValues,
} from "../../../pages/get-compliant/data-recipients/AddDataRecipients";
import { VendorList } from "../../molecules/vendor/VendorList";
import { VendorListItem } from "../../molecules/vendor/VendorListItem";
import { AddCustomModalProps, useVendorSearch } from "../../VendorSearchContents";

type VendorSearchPopupProps = {
  open: boolean;
  el: any | null;
  vendors: VendorDto[];

  onClose: () => void;

  onAddVendor: (v: string) => void;
  onAddCustomVendor: (values: CustomVendorFormValues) => void;

  customModal?: AddCustomModalProps;
};
export const VendorSearchPopup: React.FC<VendorSearchPopupProps> = ({
  open,
  el,
  vendors,
  onClose,
  onAddVendor,
  onAddCustomVendor,
  customModal = {},
}) => {
  const [search, setSearch] = useState("");
  const [openCustom, setOpenCustom] = useState(false);

  // reset search term
  useEffect(() => {
    if (!open) {
      setSearch("");
    }
  }, [open]);

  const cats = useMemo(
    () => [...new Set(vendors.map(({ category }) => category).filter((x) => x))],
    [vendors],
  );

  const searchResults = useVendorSearch(vendors, search);

  let resultVendors = searchResults.isSearchLongEnough ? searchResults.results : vendors;
  resultVendors = resultVendors.slice(0, 4);

  const handleAddVendor = (v: string) => {
    onAddVendor(v);
    onClose();
  };

  const popperModifiers = [
    {
      name: "preventOverflow",
      enabled: true,
      options: {
        altBoundary: true,
        altAxis: true,
      },
    },
    {
      name: "flip",
      enabled: false,
      options: {
        flipVariations: false,
        allowedAutoPlacements: ["top-start"],
        altBoundary: true,
      },
    },
  ];

  return (
    <>
      <Popper
        modifiers={popperModifiers}
        open={open}
        anchorEl={el}
        placement="top-start"
        key={resultVendors.length}
      >
        <ClickAwayListener onClickAway={onClose}>
          <Paper elevation={4}>
            <div className="py-md w-512">
              <div className="px-md pb-md">
                <TextField
                  size="small"
                  variant="outlined"
                  placeholder={`Search by name (e.g. Walmart)`}
                  fullWidth
                  autoFocus
                  value={search ?? ""}
                  onChange={({ target }) => setSearch(target.value)}
                />
              </div>

              <VendorList vendors={resultVendors} onClick={(id) => handleAddVendor(id)} />

              <VendorListItem
                logoUrl="/assets/images/icon-vendors-black.svg"
                name="Add custom source..."
                onClick={() => {
                  setOpenCustom(true);
                  onClose();
                }}
              />
            </div>
          </Paper>
        </ClickAwayListener>
      </Popper>

      <CustomVendorDialog
        open={openCustom}
        defaultName={search}
        onClose={() => setOpenCustom(false)}
        onDone={(ov: CustomVendorFormValues) => {
          onAddCustomVendor(ov);
          setOpenCustom(false);
        }}
        enabledVendors={vendors}
        existingCategories={new Set(cats)}
        header={customModal?.header}
        confirmLabel={customModal?.confirmLabel}
        includeUrl={customModal?.includeUrl}
      />
    </>
  );
};
