import AnalyticsIcon from "@mui/icons-material/Analytics";
import BusinessIcon from "@mui/icons-material/Business";
import CloudIcon from "@mui/icons-material/Cloud";
import DnsIcon from "@mui/icons-material/Dns";
import GroupsIcon from "@mui/icons-material/Groups";
import LocalOfferIcon from "@mui/icons-material/LocalOffer";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import MessageIcon from "@mui/icons-material/Message";
import MoreIcon from "@mui/icons-material/More";
import PaymentsIcon from "@mui/icons-material/Payments";
import StorefrontIcon from "@mui/icons-material/Storefront";
import SupportAgentIcon from "@mui/icons-material/SupportAgent";
import WebIcon from "@mui/icons-material/Web";
import { SvgIconTypeMap } from "@mui/material";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import _ from "lodash";
import { Badge, GroupAdd, Inbox, PersonSearch, Savings, School, Timer } from "@mui/icons-material";

type Icon = OverridableComponent<SvgIconTypeMap> & { muiName: string };
export type VendorCategory = {
  category: string;
  label: string;
  description?: string;
  icon: Icon | string;
};

export const KNOWN_VENDOR_CATEGORIES: VendorCategory[] = _.sortBy(
  [
    { category: "Ad Network", label: "Ad Networks", icon: "Radar" },
    {
      category: "Business Operations Tool",
      label: "Business Operations Tools",
      icon: BusinessIcon,
    },
    {
      category: "Cloud Computing & Storage Provider",
      label: "Cloud Computing & Storage Providers",
      icon: CloudIcon,
    },

    { category: "Commerce Software Tool", label: "Commerce Software Tools", icon: StorefrontIcon },
    {
      category: "Collaboration & Productivity Tool",
      label: "Collaboration & Productivity Tool",
      icon: GroupsIcon,
    },
    { category: "Communications Tool", label: "Communications Tools", icon: MessageIcon },
    { category: "Customer Support Tool", label: "Customer Support Tools", icon: SupportAgentIcon },
    { category: "IT Infrastructure Service", label: "IT Infrastructure Services", icon: DnsIcon },
    { category: "Data Analytics Provider", label: "Data Analytics Providers", icon: AnalyticsIcon },
    { category: "Payment Processor", label: "Payment Processors", icon: PaymentsIcon },
    { category: "Sales & Marketing Tool", label: "Sales & Marketing Tools", icon: LocalOfferIcon },
    { category: "Shipping Service", label: "Shipping Services", icon: LocalShippingIcon },
    { category: "Web Hosting Service", label: "Web Hosting Services", icon: WebIcon },
  ],
  "label",
);

export const KNOWN_EMPLOYMENT_VENDOR_CATEGORIES: VendorCategory[] = _.sortBy(
  [
    {
      category: "Payroll & Benefits Management Software",
      label: "Payroll & Benefits Management Software",
      icon: Savings,
    },
    {
      category: "Background Check Provider",
      label: "Background Check Provider",
      icon: PersonSearch,
    },
    {
      category: "Recruitment & Applicant Tracking System",
      label: "Recruitment & Applicant Tracking System",
      icon: Inbox,
    },
    {
      category: "Recruitment Service",
      label: "Recruitment Service",
      icon: GroupAdd,
    },
    {
      category: "Learning & Performance Management Software",
      label: "Learning & Performance Management Software",
      icon: School,
    },
    {
      category: "Employee Time-Tracking Software",
      label: "Employee Time-Tracking Software",
      icon: Timer,
    },
    {
      category: "Workforce Management Software",
      label: "Workforce Management Software",
      icon: Badge,
    },
  ],
  "label",
);

export const OTHER_CATEGORY: VendorCategory = {
  category: "other",
  label: "Other",
  icon: MoreIcon,
};
