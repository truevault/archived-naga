import React, { useMemo, useState } from "react";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { CustomVendorFormValues } from "../../../pages/get-compliant/data-recipients/AddDataRecipients";
import { RemoveVendorDialog } from "../../dialog/RemoveVendorDialog";
import { CategoryCollapseCard } from "./CategoryCollapseCard";
import { KNOWN_VENDOR_CATEGORIES, OTHER_CATEGORY, VendorCategory } from "./VendorCategories";

type VendorInventoryProps = {
  allVendors: VendorDto[];
  orgVendors?: OrganizationDataRecipientDto[];
  enabledVendors: Set<UUIDString>;
  pendingVendor: UUIDString;
  handleEditedVendor: (ov: OrganizationDataRecipientDto, values: CustomVendorFormValues) => void;
  handleRemoveVendor: (v: UUIDString) => void;
  collectionContext: CollectionContext[];
  categories?: VendorCategory[];
  hideEmptyCategories?: boolean;
};
export const CategorizedVendorInventory: React.FC<VendorInventoryProps> = ({
  allVendors,
  enabledVendors,
  pendingVendor,
  handleRemoveVendor,
  collectionContext,
  categories = KNOWN_VENDOR_CATEGORIES,
  hideEmptyCategories,
}) => {
  const allVendorsMap = useMemo(() => new Map(allVendors.map((v) => [v.id, v])), [allVendors]);

  const [confirmRemoveVendor, setConfirmRemoveVendor] = useState(null as VendorDto);
  const handleRemove = (vendor: VendorDto) => {
    setConfirmRemoveVendor(vendor);
  };

  const vendors = useMemo(
    () =>
      Array.from(enabledVendors)
        .map((vendorId) => allVendorsMap.get(vendorId))
        .filter((v) => v)
        .sort((a, b) => a.name.localeCompare(b.name)),
    [enabledVendors, allVendorsMap],
  );

  const filteredCategories = useMemo(() => {
    if (!hideEmptyCategories) {
      return categories;
    }
    const categorySet = new Set(vendors.map((v) => v.category));
    return categories.filter((cat) => categorySet.has(cat.category));
  }, [categories, hideEmptyCategories, vendors]);

  return (
    <div>
      <CategoryCards
        categories={filteredCategories}
        vendors={vendors}
        pendingVendor={pendingVendor}
        handleRemove={handleRemove}
      />

      <RemoveVendorDialog
        open={!!confirmRemoveVendor}
        onClose={() => setConfirmRemoveVendor(null)}
        onAfterRemoved={() => {
          handleRemoveVendor(confirmRemoveVendor?.id);
        }}
        vendorId={confirmRemoveVendor?.id}
        collectionContext={collectionContext}
      />
    </div>
  );
};

type CategoryCardsProps = {
  vendors: VendorDto[];
  categories: VendorCategory[];
  pendingVendor: UUIDString | null;
  handleRemove: (vendor: VendorDto) => void;
};
const CategoryCards: React.FC<CategoryCardsProps> = ({
  categories,
  vendors,
  pendingVendor,
  handleRemove,
}) => {
  const otherVendors = useMemo(() => {
    const known = categories.map((c) => c.category);
    return vendors.filter((v) => !known.includes(v.category));
  }, [vendors, categories]);

  const categorizedVendors = useMemo(() => {
    return categories.map((cat) => {
      return vendors.filter((v) => v.category == cat.category);
    });
  }, [vendors, categories]);

  return (
    <>
      {categories.map((cat, idx) => {
        const catVendors = categorizedVendors[idx];
        return (
          <div className="mb-md" key={cat.category}>
            <CategoryCollapseCard
              category={cat}
              vendors={catVendors}
              handleRemove={handleRemove}
              emptyText={
                <>
                  Nothing to see here! Use the search bar above to add your{" "}
                  <strong>{OTHER_CATEGORY.label}</strong>
                </>
              }
            />
          </div>
        );
      })}

      {otherVendors?.length > 0 && (
        <div className="mb-md">
          <CategoryCollapseCard
            category={OTHER_CATEGORY}
            vendors={otherVendors}
            pendingVendor={pendingVendor}
            handleRemove={handleRemove}
            emptyText={
              <>
                Nothing to see here! Use the search bar above to add your{" "}
                <strong>{OTHER_CATEGORY.label}</strong>
              </>
            }
          />
        </div>
      )}
    </>
  );
};
