import {
  Button,
  ClickAwayListener,
  IconButton,
  InputAdornment,
  Paper,
  Popper,
  TextField,
} from "@mui/material";
import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { NetworkRequestState } from "../../../../common/models";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../../../common/service/server/Types";
import {
  CustomVendorDialog,
  CustomVendorFormValues,
} from "../../../pages/get-compliant/data-recipients/AddDataRecipients";
import SearchIcon from "@mui/icons-material/Search";
import { AddCustomModalProps, UseVendorSearch, useVendorSearch } from "../../VendorSearchContents";
import { Loading } from "../../../../common/components/Loading";
import { RowVendorTile } from "../../../pages/stay-compliant/data-recipients/RowVendorTile";
import { Disableable } from "../../Disableable";
import { vendorDisabled } from "../../../pages/design/organisms/data-recipients/CommonDataRecipients";
import WidgetsOutlinedIcon from "@mui/icons-material/WidgetsOutlined";
import { Clear } from "@mui/icons-material";
import { RemoveVendorDialog } from "../../dialog/RemoveVendorDialog";
import { CollectionContext } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";

type VendorSearchBoxProps = {
  allVendors: VendorDto[];
  collectionContext: CollectionContext[];
  enabledVendors: Set<UUIDString>;
  pendingVendor?: UUIDString;
  customModal?: AddCustomModalProps;
  handleAddVendor: (v: UUIDString) => void;
  handleRemoveVendor?: (v: UUIDString) => void;
  handleAddCustomVendor: (values: CustomVendorFormValues) => void;
  addRequest?: NetworkRequestState;

  showCustomButton?: boolean;
  customButtonLabel?: string;

  customCategory?: string;
  autoFocus?: boolean; // defaults to true

  type?: "source" | "vendor";
  maxResults?: number;
};

const typeLabel = (type: "source" | "vendor"): string => {
  switch (type) {
    case "source":
      return "data source";
    case "vendor":
      return "vendor";
  }
};

export const VendorSearchBox = ({
  allVendors,
  collectionContext,
  enabledVendors,
  pendingVendor,
  handleAddVendor,
  handleRemoveVendor,
  handleAddCustomVendor,
  customModal,
  addRequest,
  showCustomButton = false,
  customButtonLabel,
  customCategory,
  autoFocus = true,
  type = "vendor",
  maxResults,
}: VendorSearchBoxProps) => {
  const [popperOpen, setPopperOpen] = useState(false);
  const [customVendorOpen, setCustomVendorOpen] = useState(false);
  const [confirmRemoveVendor, setConfirmRemoveVendor] = useState(null as VendorDto);
  const [searchTerm, setSearchTerm] = useState("");
  const results = useVendorSearch(allVendors, searchTerm);
  const textInput = useRef(null);

  const handleSearchBlur = () => {
    setPopperOpen(false);
  };
  const handleSearchFocus = () => {
    setPopperOpen(results.isSearchLongEnough);
  };

  useEffect(() => {
    setPopperOpen(results.isSearchLongEnough);
  }, [results.isSearchLongEnough]);

  const handleSearchChange = ({ target }) => setSearchTerm(target.value);

  const resetSearch = () => {
    setSearchTerm("");
  };

  const openConfirmRemoveVendor = useCallback(
    (id: string) => {
      const vendor = allVendors.find((v) => v.id == id);
      setConfirmRemoveVendor(vendor);
    },
    [allVendors],
  );

  const openAddCustomVendor = () => setCustomVendorOpen(true);

  const enabled = useMemo(
    () => allVendors.filter((v) => enabledVendors.has(v.id)),
    [allVendors, enabledVendors],
  );

  const cats = useMemo(
    () => new Set(allVendors.map(({ category }) => category).filter((x) => x)),
    [allVendors],
  );

  const defaultCustomLabel = type == "vendor" ? "Add custom Data Recipient" : "Add custom source";
  customButtonLabel = customButtonLabel ?? defaultCustomLabel;

  const placeholderExample = type == "source" ? "Walmart" : "";
  let placeholder = `Start typing to search ${typeLabel(type)}s by name!`;
  if (placeholderExample) {
    placeholder = `${placeholder} (e.g. ${placeholderExample})`;
  }

  return (
    <>
      <div className="flex-row w-100">
        <ClickAwayListener onClickAway={handleSearchBlur}>
          <div ref={textInput} className="flex-grow" onClick={handleSearchFocus}>
            <TextField
              variant="outlined"
              placeholder={placeholder}
              color="primary"
              fullWidth
              autoFocus={autoFocus}
              value={searchTerm}
              onChange={handleSearchChange}
              InputProps={{
                type: "search",
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
                endAdornment: searchTerm ? (
                  <InputAdornment position="end">
                    <IconButton size="small" onClick={resetSearch} edge="end">
                      <Clear />
                    </IconButton>
                  </InputAdornment>
                ) : undefined,
              }}
            />

            <SearchResultsPopper
              anchorEl={textInput.current}
              open={popperOpen}
              results={results}
              allVendors={allVendors}
              enabledVendors={enabledVendors}
              pendingVendor={pendingVendor}
              customButtonLabel={customButtonLabel}
              handleAddVendor={handleAddVendor}
              handleRemoveVendor={openConfirmRemoveVendor}
              handleAddCustomVendor={openAddCustomVendor}
              addRequest={addRequest}
              maxResults={maxResults}
            />
          </div>
        </ClickAwayListener>

        {showCustomButton && (
          <Button variant="outlined" onClick={openAddCustomVendor}>
            {customButtonLabel}
          </Button>
        )}
      </div>

      <CustomVendorDialog
        open={customVendorOpen}
        defaultName={results.search}
        onClose={() => setCustomVendorOpen(false)}
        onDone={(ov: CustomVendorFormValues) => {
          handleAddCustomVendor(ov);
          setCustomVendorOpen(false);
        }}
        enabledVendors={enabled}
        existingCategories={cats}
        defaultCategory={customCategory}
        showCategories={!customCategory}
        {...customModal}
        type={type}
        header={customButtonLabel}
        confirmLabel={customButtonLabel}
      />
      <RemoveVendorDialog
        open={Boolean(confirmRemoveVendor)}
        onClose={() => setConfirmRemoveVendor(null)}
        onAfterRemoved={() => {
          handleRemoveVendor?.(confirmRemoveVendor?.id);
        }}
        vendorId={confirmRemoveVendor?.id}
        type={type}
        collectionContext={collectionContext}
      />
    </>
  );
};

type SearchResultsPopperProps = {
  anchorEl: null | HTMLElement;
  open: boolean;
  allVendors: VendorDto[];
  results: UseVendorSearch;
  enabledVendors: Set<UUIDString>;
  customButtonLabel?: string;
  pendingVendor?: UUIDString;
  handleAddVendor: (v: UUIDString) => void;
  handleRemoveVendor?: (v: UUIDString) => void;
  handleAddCustomVendor: () => void;
  addRequest?: NetworkRequestState;
  maxResults?: number;
};

export const SearchResultsPopper: React.FC<SearchResultsPopperProps> = ({
  anchorEl,
  results,
  open,
  ...rest
}) => {
  return (
    <Popper
      open={open}
      anchorEl={anchorEl}
      placement="bottom-start"
      disablePortal={true}
      style={{ zIndex: 10 }}
    >
      <Paper elevation={6} className="mt-sm p-sm" style={{ width: anchorEl?.clientWidth }}>
        <div onClick={(e) => e.preventDefault()}>
          <SearchResultsPopperBody results={results} {...rest} />
        </div>
      </Paper>
    </Popper>
  );
};

type SearchResultsPopperBodyProps = {
  allVendors: VendorDto[];
  results: UseVendorSearch;
  enabledVendors: Set<UUIDString>;
  customButtonLabel?: string;
  pendingVendor?: UUIDString;
  handleAddVendor: (v: UUIDString) => void;
  handleRemoveVendor?: (v: UUIDString) => void;
  handleAddCustomVendor: () => void;
  addRequest?: NetworkRequestState;
  maxResults?: number;
};
export const SearchResultsPopperBody: React.FC<SearchResultsPopperBodyProps> = ({
  results,
  enabledVendors,
  pendingVendor,
  customButtonLabel = "Add Custom Data Recipient",
  handleAddVendor,
  handleRemoveVendor,
  handleAddCustomVendor,
  maxResults = 8,
}) => {
  if (!results.isSearchLongEnough) {
    return null;
  }

  if (results.isLoading) {
    return <Loading />;
  }

  return (
    <>
      {results.results.slice(0, maxResults).map((v) => {
        const isAdded = enabledVendors.has(v.id);
        return (
          <div className="mb-xs" key={v.id}>
            <Disableable
              disabled={isAdded && !handleRemoveVendor}
              disabledTooltip={`${v.name} is already in your vendor list.`}
              isBlock={false}
            >
              <RowVendorTile
                name={v.name}
                category={v.category}
                logoUrl={v.logoUrl}
                pending={pendingVendor == v.id}
                disabled={Boolean(pendingVendor) || vendorDisabled(v, isAdded)}
                added={isAdded}
                selected={isAdded}
                handleAddVendor={() => handleAddVendor(v.id)}
                handleRemoveVendor={handleRemoveVendor ? () => handleRemoveVendor(v.id) : null}
                vendor={v}
              />
            </Disableable>
          </div>
        );
      })}

      <RowVendorTile
        name={`${customButtonLabel}…`}
        category=""
        logoUrl=""
        logoIcon={<WidgetsOutlinedIcon />}
        handleRowClick={handleAddCustomVendor}
      />
    </>
  );
};
