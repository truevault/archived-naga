import React, { useMemo } from "react";
import { useAllCollectedPIC } from "../../../../common/hooks/dataMapHooks";
import { SurveyAnswerFn, SurveyAnswers, useSurvey } from "../../../../common/hooks/useSurvey";
import { NetworkRequestState } from "../../../../common/models";
import { CategoryDto } from "../../../../common/service/server/dto/DataMapDto";
import {
  DataRetentionPolicyDetailCategory,
  DataRetentionPolicyDto,
  UpdateDataRetentionPolicyDetailDto,
} from "../../../../common/service/server/dto/DataRetentionPolicyDto";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useDataMap } from "../../../hooks/useDataMap";
import { UpdateDataRentionPolicyFn, useDataRetentionPolicy } from "../../../hooks/useDataRetention";
import { Callout, CalloutVariant } from "../../Callout";
import { SurveyGateButton } from "../survey/SurveyGateButton";
import { DataRetentionCategoryQuestion } from "./DataRetentionCategoryQuestion";
import { SurveyQuestion } from "../../get-compliant/survey/SurveyQuestion";

export const DATA_RETENTION_SURVEY_NAME = "data-retention-survey";

export type UseDataRetentionModule = {
  requests: {
    surveyRequest: NetworkRequestState;
    dataMapRequest: NetworkRequestState;
    dataRetentionRequest: NetworkRequestState;
  };
  isFinished: boolean;
  updateRequest: NetworkRequestState;
  props: DataRetentionModuleProps;
};

export const useDataRetentionModule = (doneSlug?: string): UseDataRetentionModule => {
  const orgId = usePrimaryOrganizationId();

  const { answers, setAnswer, surveyRequest } = useSurvey(orgId, DATA_RETENTION_SURVEY_NAME);

  const [dataMap, dataMapRequest] = useDataMap(orgId);

  const {
    result: dataRetention,
    request: dataRetentionRequest,
    update: updateDataRetention,
    updateRequest,
  } = useDataRetentionPolicy(orgId);

  const collectedPic = useAllCollectedPIC(dataMap);

  const unmappedItems = useMemo(() => {
    if (!dataRetention) {
      return [];
    }
    return collectedPic.filter(
      (pic) => !dataRetention.detail.some((dr) => dr.pic.includes(pic.id)),
    );
  }, [collectedPic, dataRetention]);

  const anyMappedWithNoDescription = useMemo(
    () =>
      !dataRetention
        ? false
        : dataRetention.detail.some((dr) => dr.pic.length > 0 && !dr.description),
    [dataRetention],
  );

  const hasDoneButton = Boolean(doneSlug);

  const doneButtonFinished = !hasDoneButton || answers[doneSlug] === "true";

  const isFinished =
    doneButtonFinished && unmappedItems.length === 0 && !anyMappedWithNoDescription;

  const doneButton = hasDoneButton ? { slug: doneSlug } : undefined;

  return {
    requests: {
      surveyRequest,
      dataMapRequest,
      dataRetentionRequest,
    },
    isFinished,
    updateRequest,
    props: {
      collectedPic,
      dataRetention,
      unmappedItems,
      updateDataRetention,
      answers,
      setAnswer,
      doneButton,
    },
  };
};

export type DataRetentionSurveyDoneButton = {
  slug: string;
};
export type DataRetentionModuleProps = {
  collectedPic: CategoryDto[];
  dataRetention: DataRetentionPolicyDto;
  unmappedItems: CategoryDto[];
  updateDataRetention: UpdateDataRentionPolicyFn;
  answers: SurveyAnswers;
  setAnswer: SurveyAnswerFn;
  doneButton?: DataRetentionSurveyDoneButton;
};
export const DataRetentionModule: React.FC<DataRetentionModuleProps> = ({
  collectedPic,
  dataRetention,
  answers,
  setAnswer,
  updateDataRetention,
  unmappedItems,
  doneButton,
}) => {
  const warningCallout = (
    <>
      {unmappedItems.length > 0 && (
        <Callout variant={CalloutVariant.Yellow}>
          <p>
            The following personal information categories were not selected for a retention policy.
            To continue please ensure that all categories are selected for at least one retention
            policy above.
          </p>

          <ul>
            {unmappedItems.map((unmappedPic) => (
              <li key={unmappedPic.id}>
                <strong>{unmappedPic.name}</strong>
              </li>
            ))}
          </ul>
        </Callout>
      )}
    </>
  );

  return (
    <>
      <p>
        <SurveyQuestion
          question={{
            question: "Your Data Retention intro statement",
            slug: "retention-policy",
            type: "freetext",
            wysiwyg: true,
            visible: true,
          }}
          answer={answers["retention-policy"]}
          onAnswer={(ans) => setAnswer("retention-policy", ans)}
        />
      </p>

      <div className="mt-xl">
        {DATA_RETENTION_CATEGORIES.map((category) => {
          return (
            <DataRetentionCategoryQuestion
              key={category.category}
              label={category.label}
              helpText={category.helpText}
              picOptions={collectedPic}
              detail={dataRetention?.detail.find((d) => d.category == category.category)}
              onUpdate={(dto: UpdateDataRetentionPolicyDetailDto) => {
                updateDataRetention({
                  category: category.category as DataRetentionPolicyDetailCategory,
                  dto,
                });
              }}
            />
          );
        })}

        {doneButton ? (
          <SurveyGateButton
            slug={doneButton.slug}
            survey={answers}
            updateResponse={setAnswer}
            label="Done with Review"
          >
            {warningCallout}
          </SurveyGateButton>
        ) : (
          warningCallout
        )}
      </div>
    </>
  );
};

export const DATA_RETENTION_CATEGORIES = [
  {
    label: "Cookies and online data we collect while you use our website",
    category: "ONLINE_DATA_FROM_WEBSITE",
    helpText:
      "Example: We delete or anonymize data concerning your use of our website within 5 years of collecting it.",
  },
  {
    label: "Data we collect in order to process and ship orders you place with us",
    category: "PROCESS_AND_SHIP_ORDERS",
    helpText:
      "Example: We keep personal information related to products and services you purchase for as long as the personal data is required for us to fulfill our contract with you, and for 7 years from your last purchase with us. We may keep data beyond this period in anonymized form.",
  },
  {
    label: "Data we collect when you contact us for customer support and other inquiries",
    category: "CONTACT_CUSTOMER_SUPPORT",
    helpText:
      "Example: We keep customer feedback and correspondence with our customer service for up to 2 years to help us respond to any questions or complaints. We may keep data beyond this period in anonymized form.",
  },
  {
    label: "Data we collect when you sign up for promotional and marketing communications",
    category: "SIGN_UP_FOR_PROMOTIONAL_AND_MARKETING",
    helpText:
      "Example: Where you have signed up to receive promotional and marketing communications from us, we will retain any data collected until you opt out or request its deletion. We may keep data beyond this period in anonymized form. We will further retain a record of any opt-outs in order to prevent sending you future communications.",
  },
  {
    label: "Data we collect when you review our products, answer surveys, or send feedback",
    category: "REVIEW_PRODUCTS_ANSWER_SURVEYS_SEND_FEEDBACK",
    helpText:
      "Example: We retain review, survey, and feedback data for up to 5 years following your last contact with us.  We may keep data beyond this period in anonymized form to help improve our products and services.",
  },
  {
    label: "Data we collect in connection with privacy requests",
    category: "PRIVACY_REQUESTS",
    helpText:
      "Example: We retain records related to privacy requests as long as necessary to comply with our legal obligations, and for a minimum of 24 months.",
  },
  {
    label: "Data we collect for security purposes",
    category: "SECURITY_PURPOSES",
    helpText:
      "Example: We retain security-related data as long as necessary to comply with our legal obligations and to maintain and improve our information security measures.",
  },
];
