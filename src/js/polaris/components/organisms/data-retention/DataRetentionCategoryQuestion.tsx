import { Autocomplete, TextField } from "@mui/material";
import _ from "lodash";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { CategoryDto } from "../../../../common/service/server/dto/DataMapDto";
import { UpdateDataRetentionPolicyDetailDto } from "../../../../common/service/server/dto/DataRetentionPolicyDto";
import { QuestionHeading } from "../../vendor/QuestionHeading";

type DataRetentionCategoryQuestionProps = {
  label: string;
  detail: UpdateDataRetentionPolicyDetailDto;
  picOptions: CategoryDto[];
  helpText: string;
  onUpdate: (dto: UpdateDataRetentionPolicyDetailDto) => void;
};

export const DataRetentionCategoryQuestion: React.FC<DataRetentionCategoryQuestionProps> = ({
  label,
  detail,
  picOptions,
  helpText,
  onUpdate,
}) => {
  const options = useMemo(
    () =>
      picOptions.map((pic) => ({
        label: pic.name,
        value: pic.id,
      })),
    [picOptions],
  );

  const [selectedCategories, setSelectedCategories] = useState(
    options.filter((o) => detail.pic.includes(o.value)),
  );
  const [description, setDescription] = useState(detail.description);

  const update = useCallback(_.debounce(onUpdate, 750, { maxWait: 3000 }), [onUpdate]);

  useEffect(() => {
    update({
      pic: selectedCategories.map((pic) => pic.value),
      description,
    });
  }, [selectedCategories, description]);

  return (
    <div className="mb-lg">
      <QuestionHeading question={label} />
      <div className="d-flex flex-row w-100">
        <div style={{ width: 325 }}>
          <Autocomplete
            multiple
            id="tags-outlined"
            options={options}
            value={selectedCategories}
            onChange={(e, value) => setSelectedCategories(value as any)}
            filterSelectedOptions
            renderInput={(params) => <TextField {...params} />}
          />
        </div>
        <div className="d-flex flex-grow flex-col">
          <TextField
            className="freetext--textfield"
            variant="outlined"
            value={description}
            fullWidth={true}
            onChange={(e) => setDescription(e.target.value)}
            multiline={true}
            rows={5}
          />
          <p className="text-component-help" style={{ width: 580 }}>
            {helpText}
          </p>
        </div>
      </div>
    </div>
  );
};
