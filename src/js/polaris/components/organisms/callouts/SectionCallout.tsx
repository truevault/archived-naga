import React from "react";
import EditIcon from "@mui/icons-material/Edit";
import { Callout, CalloutVariant } from "../../Callout";

type SectionCalloutProps = {
  title: string;
  className?: string;
  actionUrl?: string;
  actionCta?: React.ReactNode;
  children?: React.ReactNode;
};

export const SectionCallout: React.FC<SectionCalloutProps> = ({
  title,
  className,
  actionUrl,
  actionCta = "Edit",
  children,
}) => {
  return (
    <Callout variant={CalloutVariant.LightGray} className={[className, "px-md", "pt-md"].join(" ")}>
      <div className="flex-row--simple flex--align-center mb-md">
        <div className="text-t5 color-primary-800 text-weight-bold flex--expand-self">{title}</div>
        {Boolean(actionUrl) && (
          <div className="text-t1">
            <a href={actionUrl}>
              <EditIcon fontSize="inherit" className="mr-xxs" />
              {actionCta}
            </a>
          </div>
        )}
      </div>

      {children}
    </Callout>
  );
};
