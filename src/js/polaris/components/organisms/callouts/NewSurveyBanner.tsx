import React from "react";
import { Link } from "react-router-dom";
import ReactDOM from "react-dom";
import { Alert } from "@mui/material";

const NewSurveyAlert: React.FC<{
  surveyName: string;
  route: string;
}> = ({ surveyName, route }) => {
  return (
    <Alert icon={false} className="new-survey-alert">
      It’s time to get up to date with new regulations! Complete your{" "}
      <strong>
        <Link to={route}>{surveyName}</Link>
      </strong>{" "}
      to renew your compliance.
    </Alert>
  );
};

export const NewSurveyBanner: React.FC<{
  surveyName: string;
  route: string;
}> = ({ surveyName, route }) => {
  const mount = document.getElementById("top-banner");
  return ReactDOM.createPortal(<NewSurveyAlert surveyName={surveyName} route={route} />, mount);
};
