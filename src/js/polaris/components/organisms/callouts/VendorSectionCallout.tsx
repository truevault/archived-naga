import React from "react";
import {
  EditActionSurveyItemProps,
  EditActionSurveySection,
} from "../../molecules/EditActionSurveySection";
import { SectionCallout } from "./SectionCallout";

type VendorSectionCalloutProps = {
  title: string;
  className?: string;
  dense?: true;
  items: EditActionSurveyItemProps[];
  noItemsView?: React.ReactNode | null;
  actionUrl?: string;
};

export const VendorSectionCallout: React.FC<VendorSectionCalloutProps> = ({
  title,
  className,
  items,
  dense,
  noItemsView,
  actionUrl,
}) => {
  return (
    <SectionCallout
      title={title}
      className={className}
      actionCta="Edit Settings"
      actionUrl={actionUrl}
    >
      <EditActionSurveySection
        className="text-t3"
        dense={dense}
        items={items}
        noItemsView={noItemsView}
      />
    </SectionCallout>
  );
};
