import { Button, Divider, Link } from "@mui/material";
import clsx from "clsx";
import React, { ReactNode } from "react";
import { MappingProgressEnum } from "../../../../common/service/server/Types";
import CircleOutlinedIcon from "@mui/icons-material/CircleOutlined";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import CachedIcon from "@mui/icons-material/Cached";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";

type ProgressCardProps = {
  progress: ProgressEnum;
  className?: string;
  active?: boolean;
  title: string | ReactNode;
  action?: string | ReactNode;
  details?: string | ReactNode;
  disabled?: boolean;
  href?: string;
  steps: MappingProgressStep[];
};

export const ProgressCard: React.FC<ProgressCardProps> = ({
  progress,
  className,
  active,
  title,
  details,
  disabled,
  action,
  href,
  steps,
}) => {
  const showSteps = steps.length > 1 && active;
  return (
    <div
      className={clsx("progress-card", className, {
        "progress-card--active": active,
        "progress-card--disabled": disabled,
      })}
    >
      <div className="flex-row flex--align-start">
        <div style={{ fontSize: 30 }}>
          <ProgressIcon progress={progress} />
        </div>
        <div className="flex-grow">
          <p className="text-body-1-medium mt-0 mb-md">{title}</p>

          {Boolean(details) && <p className="text-component-help">{details}</p>}

          {showSteps && (
            <>
              <Divider />

              <div className="progress-card__steps-grid">
                {steps.map((s) => {
                  return (
                    <span className="progress-card__step-grid-item" key={s.label}>
                      <ProgressIcon progress={s.progress} className="progress-card__step-icon" />
                      {s.progress === "NOT_STARTED" ? (
                        <span className="color-neutral-600">{s.label}</span>
                      ) : (
                        <Link href={s.route} className="progress-card__link">
                          {s.label}
                        </Link>
                      )}
                    </span>
                  );
                })}
              </div>
            </>
          )}
        </div>
        <div className="w-128">
          <Button
            size="small"
            href={href || "#"}
            variant="contained"
            color={active ? "primary" : "default"}
            fullWidth
            disabled={disabled}
          >
            {action}
          </Button>
        </div>
      </div>
    </div>
  );
};

export type MappingProgressStep = {
  label: string;
  route: string;
  progress: ProgressEnum;
};

export type ProgressEnum = MappingProgressEnum | "NEXT";

type ProgressIconProps = {
  progress: ProgressEnum;
  className?: string;
};

export const ProgressIcon: React.FC<ProgressIconProps> = ({ progress, className }) => {
  switch (progress) {
    case "DONE":
      return (
        <CheckCircleIcon className={clsx(className, "mapping-progress--done")} fontSize="inherit" />
      );
    case "IN_PROGRESS":
      return (
        <CachedIcon
          className={clsx(className, "mapping-progress--in-progress")}
          fontSize="inherit"
        />
      );
    case "NEXT":
      return (
        <DoubleArrowIcon
          className={clsx(className, "mapping-progress--in-progress")}
          fontSize="inherit"
        />
      );
    case "NOT_STARTED":
    default:
      return (
        <CircleOutlinedIcon
          className={clsx(className, "mapping-progress--grey")}
          fontSize="inherit"
        />
      );
  }
};
