import React from "react";
import { Callout, CalloutVariant } from "../../Callout";
import { Info as InfoIcon } from "@mui/icons-material";

type RegulationCalloutProps = {
  title?: string;
  className?: string;
  children: React.ReactNode;
};

const RegulationCallout: React.FC<RegulationCalloutProps> = ({ title, children, className }) => {
  return (
    <Callout variant={CalloutVariant.Purple} title={title} icon={InfoIcon} className={className}>
      {children}
    </Callout>
  );
};

export const GlbaCallout = ({ className = "" }) => (
  <RegulationCallout title="Gramm-Leach-Bliley Act (GLBA)" className={className}>
    <p>
      GLBA may apply to some of the personal information your business collects, uses, sells, or
      discloses. To the extent such personal information is subject to the GLBA, exclude it from
      this step.
    </p>
  </RegulationCallout>
);

export const HipaaCallout = ({ className = "" }) => (
  <RegulationCallout
    title="Health Insurance Portability and Accountability Act (HIPAA)"
    className={className}
  >
    <p>
      State privacy laws do not apply to PHI collected by a covered entity or business associate
      governed by HIPAA regulations. You should exclude HIPAA-regulated PHI from this step.
    </p>
  </RegulationCallout>
);

export const FcraCallout = ({ className = "" }) => (
  <RegulationCallout title="Fair Credit Reporting Act (FCRA)" className={className}>
    <p>
      State privacy laws do not apply to the collection, maintenance, disclosure, sale,
      communication, or use of any personal information bearing on a consumer’s credit worthiness,
      credit standing, credit capacity, character, general reputation, personal characteristics, or
      mode of living, to the extent that such activity is already regulated by the FCRA. Exclude
      information regulated by the FCRA from this step.
    </p>
  </RegulationCallout>
);

export const IncludeInformationCollectedForOtherBusinessesCallout = ({ className = "" }) => (
  <RegulationCallout className={className}>
    Include any personal information your business collects about consumers as a third party (e.g.,
    purchasing data from data brokers, or collecting data from retailers for drop-shipping).
  </RegulationCallout>
);
