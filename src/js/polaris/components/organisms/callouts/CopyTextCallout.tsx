import clsx from "clsx";
import React from "react";
import { Callout } from "../../Callout";
import { Para } from "../../typography/Para";

export type CopyTextCalloutProps = {
  title: string;
  copy: string;
};

export const CopyTextCallout: React.FC<CopyTextCalloutProps> = ({ copy, title }) => {
  const exploded = copy.split("\n").filter((x) => x);
  const paras = exploded.map((para, idx) => {
    return (
      <Para key={`para-${idx}`} className={clsx("mono", { "mt-xs": idx != 0 })}>
        {para}
      </Para>
    );
  });

  return (
    <Callout className="mt-xl" title={title}>
      <div className="contact-vendors--header-contact-template">{paras}</div>
      <div className="flex-row--simple flex-end">
        <div
          className="copy-text px-md mt-md cursor-pointer"
          onClick={() => navigator.clipboard.writeText(copy)}
        >
          copy text
        </div>
      </div>
    </Callout>
  );
};
