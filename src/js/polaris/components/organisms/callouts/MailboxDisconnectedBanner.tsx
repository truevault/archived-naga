import React from "react";
import ReactDOM from "react-dom";
import { Alert } from "@mui/material";
import { RouteHelpers } from "../../../root/routes";
import { usePrimaryOrganizationId } from "../../../hooks";

const MailboxDisconnectedAlert = () => {
  const orgId = usePrimaryOrganizationId();
  return (
    <Alert severity="error">
      Oh no! It looks like your privacy email has been disconnected. Please visit your{" "}
      <a href={RouteHelpers.organization.inbox(orgId)} target="_blank" rel="noreferrer">
        privacy inbox settings
      </a>{" "}
      to reconnect it.
    </Alert>
  );
};

export const MailboxDisconnectedBanner = () => {
  const mount = document.getElementById("top-banner");
  return ReactDOM.createPortal(<MailboxDisconnectedAlert />, mount);
};
