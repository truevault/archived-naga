import { AssignmentTurnedInOutlined } from "@mui/icons-material";
import { Button } from "@mui/material";
import React, { useCallback, useState } from "react";
import { useHistory } from "react-router";
import { TaskDto } from "../../../../common/service/server/dto/TaskDto";
import { Callout, CalloutVariant } from "../../Callout";
import { DueDate } from "../../DueDateCard";

type TaskCardProps = {
  task: TaskDto;
  titleOnly?: boolean;
  allowComplete?: boolean;
  onComplete?: (task: TaskDto) => Promise<void>;
};

export const TaskCard: React.FC<TaskCardProps> = ({
  task,
  titleOnly,
  allowComplete = true,
  onComplete,
}) => {
  const history = useHistory();
  const canComplete =
    allowComplete && Boolean(onComplete) && task.completionType === "MANUAL" && !task.completedAt;

  const [completing, setCompleting] = useState(false);

  const handleComplete = useCallback(async () => {
    setCompleting(true);
    try {
      await onComplete(task);
    } finally {
      setCompleting(false);
    }
  }, [task, onComplete]);

  if (titleOnly) {
    return (
      <Callout variant={CalloutVariant.LightGray} cardContentClassName="p-xs">
        <div className="d-flex align-items-center">
          <div className="mr-sm ml-xs pt-xs">
            <AssignmentTurnedInOutlined className="color-neutral-400" />
          </div>
          <div>
            <div className="d-flex">
              <h6 className="mt-0 mb-0 text-weight-medium flex-grow flex-float">{task.name}</h6>
            </div>
          </div>
        </div>
      </Callout>
    );
  }

  return (
    <Callout variant={CalloutVariant.LightGray}>
      <div className="d-flex">
        <div className="mr-md">
          <AssignmentTurnedInOutlined className="color-neutral-400" />
        </div>
        <div>
          <div className="d-flex">
            <h6 className="mt-0 mb-sm text-weight-medium flex-grow flex-float">{task.name}</h6>
            {Boolean(task.dueAt) && <DueDate icon date={task.dueAt} />}
          </div>

          {Boolean(task.description) && (
            <p dangerouslySetInnerHTML={{ __html: task.description }} />
          )}

          <footer className="d-flex flex-row justify-end">
            {task.callToAction && task.callToActionUrl ? (
              <Button
                variant="contained"
                color="primary"
                onClick={() => history.push(task.callToActionUrl)}
              >
                {task.callToAction}
              </Button>
            ) : null}
            {canComplete && (
              <Button
                variant="contained"
                color="primary"
                disabled={completing}
                onClick={handleComplete}
              >
                Mark this Complete
              </Button>
            )}
          </footer>
        </div>
      </div>
    </Callout>
  );
};

export const EmptyTodoTasks = () => {
  return (
    <div className="max-width-650">
      <Callout variant={CalloutVariant.Clear}>
        You are all caught up on your tasks. Nice work 🎉
      </Callout>
    </div>
  );
};
