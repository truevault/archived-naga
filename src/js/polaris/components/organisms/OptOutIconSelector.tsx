import React, { FC, useCallback, useEffect, useMemo, useState } from "react";
import { Form, useField } from "react-final-form";
import { Text } from "../../../common/components/input/Text";
import { Button, Collapse, InputAdornment, Stack } from "@mui/material";
import { Select } from "../../../common/components/input/Select";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { ChevronRight } from "@mui/icons-material";
import debounce from "lodash.debounce";

const ICONS = ["Blue", "Black", "Transparent"];

const getOptOutNoticeHtml = (url: string, copy: string, icon: string, height = null) =>
  `<a class="truevault-polaris-optout" href="${url}" noreferrer noopener hidden>
  <img src="https://polaris.truevaultcdn.com/static/assets/icons/optout-icon-${icon}.svg" 
    alt="California Consumer Privacy Act (CCPA) Opt-Out Icon" 
    style="vertical-align:middle" ${height && `height="${height}px"`}
  />
  ${copy}
</a>`;

const PreviewWindow = ({
  expanded,
  iconColor,
  iconHeight,
  linkCopy,
  linkColor,
  backgroundColor,
  fontSize,
  setBackgroundColor,
  setLinkColor,
  setFontSize,
}) => {
  const [backgroundColorModified, setBackgroundColorModified] = useState(false);
  const [linkColorModified, setLinkColorModified] = useState(false);

  const backgroundColorField = useField("backgroundColor");
  const linkColorField = useField("linkColor");

  const defaultColors = useMemo(() => {
    switch (iconColor) {
      case "transparent":
        return { background: "#add8E6", font: "#ffffff" };
      default:
        return { background: "#f6f6f6", font: "#333333" };
    }
  }, [iconColor]);
  const defaultFontSize = "14px";

  const onBackgroundColorChange = useCallback(
    debounce((e) => {
      setBackgroundColor(e.target.value);
      setBackgroundColorModified(true);
    }, 200),
    [defaultColors],
  );

  const onLinkColorChange = useCallback(
    debounce((e) => {
      setLinkColor(e.target.value);
      setLinkColorModified(true);
    }, 200),
    [defaultColors],
  );

  useEffect(() => {
    if (
      !backgroundColorModified &&
      !!backgroundColorField?.input?.value &&
      backgroundColorField?.input?.value != defaultColors.background
    ) {
      setBackgroundColor(defaultColors.background);
    }
    if (
      !linkColorModified &&
      !!linkColorField?.input?.value &&
      linkColorField?.input?.value != defaultColors.font
    ) {
      setLinkColor(defaultColors.font);
    }
  }, [iconColor]);

  return (
    <>
      <Collapse in={expanded}>
        <div className="compliance-checklist--notices-item-help-text">
          Configure the preview options to match your website's design
        </div>
        <Stack direction="row" spacing={2} alignItems="flex-start" className="w-100 my-md">
          <Text
            field="backgroundColor"
            label="Background Color (color, hex, rgb)"
            placeholder={defaultColors.background}
            helperText="ex: red, #ff0000, rgb(255, 0, 0)"
            size="small"
            onChange={onBackgroundColorChange}
            InputLabelProps={{ shrink: true }}
            style={{ marginTop: 0 }}
          />
          <Text
            field="linkColor"
            label="Font Color (color, hex, rgb)"
            placeholder={defaultColors.font}
            helperText={null}
            size="small"
            onChange={onLinkColorChange}
            InputLabelProps={{ shrink: true }}
          />
          <Text
            field="fontSize"
            label="Font Size (em, px)"
            placeholder={defaultFontSize}
            helperText="ex: 14px, 1em"
            size="small"
            onChange={(e) => setFontSize(e.target.value)}
            InputLabelProps={{ shrink: true }}
          />
        </Stack>
      </Collapse>
      <div
        className="p-xs w-100"
        style={{
          backgroundColor: backgroundColor || defaultColors.background,
          border: "1px solid lightgrey",
        }}
      >
        <div style={{ float: "left", padding: "6px" }}>
          <a
            className="truevault-polaris-optout"
            style={{
              color: linkColor || defaultColors.font,
              fontSize: fontSize ?? defaultFontSize,
            }}
          >
            <img
              src={`https://staging.polaris.truevaultcdn.com/static/assets/icons/optout-icon-${iconColor}.svg`}
              alt="California Consumer Privacy Act (CCPA) Opt-Out Icon"
              style={{
                verticalAlign: "middle",
                height: `${iconHeight || 14}px`,
              }}
            />
            {` ${linkCopy}`}
          </a>
        </div>
      </div>
    </>
  );
};

export const OptOutIconSelector: FC<{
  linkUrl: string;
  linkCopy: string;
  onUpdate: (icon: string) => void;
}> = ({ linkUrl, linkCopy, onUpdate }) => {
  // icon settings
  const [iconColor, setIconColor] = useState("blue");
  const [iconHeight, setIconHeight] = useState(14);

  // preview settings
  const [backgroundColor, setBackgroundColor] = useState<string>();
  const [linkColor, setLinkColor] = useState<string>();
  const [fontSize, setFontSize] = useState<string>();

  useEffect(
    () => onUpdate(getOptOutNoticeHtml(linkUrl, linkCopy, iconColor, iconHeight)),
    [iconColor, iconHeight],
  );

  const [expanded, setExpanded] = useState(false);
  const ExpandIcon = expanded ? ExpandMoreIcon : ChevronRight;

  return (
    <Form
      initialValues={{
        iconColor: iconColor,
        iconHeight: iconHeight,
        backgroundColor: backgroundColor,
        linkColor: linkColor,
        fontSize: fontSize,
      }}
      onSubmit={() => {}}
    >
      {(_form) => (
        <Stack className="mt-lg pd-sm">
          <div className="compliance-checklist--notices-item-help-text">
            Select your CCPA Opt-Out Icon
          </div>
          <Stack
            key="icon-settings"
            direction="row"
            spacing={2}
            alignItems="flex-start"
            className="mt-md"
          >
            <Select
              field="iconColor"
              label="Icon Color"
              size="small"
              options={ICONS.map((icon) => ({
                value: icon.toLowerCase(),
                label: icon,
              }))}
              onChange={(e: any) => setIconColor(e.target.value)}
              required={true}
              sx={{
                width: 200,
              }}
              formControlProps={{ style: { marginTop: 0 } }}
            />
            <Text
              field="iconHeight"
              label="Icon Height"
              placeholder="14"
              helperText={null}
              size="small"
              onChange={(e: any) => setIconHeight(e.target.value)}
              InputLabelProps={{ shrink: true }}
              sx={{
                width: 120,
              }}
              inputProps={{
                maxLength: 2,
                style: { textAlign: "right" },
              }}
              InputProps={{
                endAdornment: <InputAdornment position="end">px</InputAdornment>,
              }}
            />
            <div className="text-weight-medium" style={{ float: "right" }}>
              <Button
                variant="text"
                color="secondary"
                onClick={() => setExpanded(!expanded)}
                startIcon={<ExpandIcon />}
              >
                Preview Options
              </Button>
            </div>
          </Stack>
          <PreviewWindow
            expanded={expanded}
            iconColor={iconColor}
            iconHeight={iconHeight}
            linkCopy={linkCopy}
            linkColor={linkColor}
            backgroundColor={backgroundColor}
            fontSize={fontSize}
            setBackgroundColor={setBackgroundColor}
            setLinkColor={setLinkColor}
            setFontSize={setFontSize}
          />
        </Stack>
      )}
    </Form>
  );
};
