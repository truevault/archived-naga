import { Button, LinearProgress } from "@mui/material";
import React from "react";
import { Paras } from "../../typography/Paras";
import { Para } from "../../typography/Para";

export type WebsiteAuditProgressProps = {
  url: string;
  onCancel: () => void;
};

export const WebsiteAuditProgress: React.FC<WebsiteAuditProgressProps> = ({ url, onCancel }) => {
  return (
    <div className="text-center mb-xl">
      <Paras>
        <Para>
          We're scanning{" "}
          <strong>
            <a href={url} target="_blank" rel="noreferrer">
              {url}
            </a>
          </strong>
        </Para>
        <Para className="mb-lg">
          This usually takes less than a minute. This page will update when the scan is done.
        </Para>
      </Paras>
      <LinearProgress className="mb-lg" />
      <Button size="large" variant="contained" color="default" onClick={onCancel}>
        Cancel Scan
      </Button>
    </div>
  );
};
