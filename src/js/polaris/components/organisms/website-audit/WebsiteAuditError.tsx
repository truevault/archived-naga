import { Alert, Button } from "@mui/material";
import React from "react";
import { Paras } from "../../typography/Paras";
import { Para } from "../../typography/Para";

export type WebsiteAuditErrorProps = {
  url: string;
  statusCode?: number;
  createTask?: () => Promise<void>;
  showSkip?: boolean;
};

const SCAN_ERROR = (
  <>
    <Para className="mb-md">
      It looks like we’re having trouble scanning your site. Please try again.
    </Para>
    <Para>
      <strong>If you continue to encounter this error,</strong> we've logged the issue and our team
      will reach out once it’s been resolved. You can skip the audit for now, and we’ll create a
      task for you to try again later.
    </Para>
  </>
);

const ERROR_404 = (
  <>
    <Para>
      It looks like we’re having trouble finding your site. Place double check your URL and try
      again.
    </Para>
  </>
);

export const WebsiteAuditError: React.FC<WebsiteAuditErrorProps> = ({
  url,
  statusCode,
  createTask,
  showSkip = true,
}) => {
  let errorText = statusCode === 404 ? ERROR_404 : SCAN_ERROR;
  return (
    <div className="mb-xl">
      <Alert severity="error">
        <Paras>
          <Para>
            <strong>Error scanning {url}</strong>
          </Para>
          <Para>{errorText}</Para>
        </Paras>
      </Alert>
      {statusCode !== 404 && showSkip && (
        <div className="d-flex justify-end mt-md">
          <Button variant="contained" onClick={createTask}>
            Skip for now
          </Button>
        </div>
      )}
    </div>
  );
};
