import { Button } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import React, { ReactNode, useMemo } from "react";
import { Form } from "react-final-form";
import { Text } from "../../../../common/components/input/Text";
import { RefreshFn, useActionRequest } from "../../../../common/hooks/api";
import { usePolling } from "../../../../common/hooks/useInterval";
import { NetworkRequestState } from "../../../../common/models";
import { Api } from "../../../../common/service/Api";
import { useOrganization, usePrimaryOrganizationId } from "../../../hooks";
import { requiredUrl } from "../../../util/validation";
import { Adornment } from "../../input/Adornment";
import { useWebsiteAuditResult } from "../../../hooks/useWebsiteAuditResult";
import { WebsiteAuditResultDto } from "../../../../common/service/server/dto/WebsiteAuditDto";
import { WebsiteAuditError } from "./WebsiteAuditError";
import { WebsiteAuditResults } from "./WebsiteAuditResults";
import { WebsiteAuditProgress } from "./WebsiteAuditProgress";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { Paras } from "../../typography/Paras";
import { Para } from "../../typography/Para";
import { ArrowForward } from "@mui/icons-material";
import { SurveyAnswers, useSurvey } from "../../../../common/hooks/useSurvey";
import { CONSUMER_INCENTIVES_SURVEY_NAME } from "../../../surveys/consumerCollectionIncentivesSurvey";
import { usePrivacyCenter } from "../../../hooks/usePrivacyCenter";

export const WEBSITE_AUDIT_SURVEY_NAME = "website-audit-survey";

const useClasses = makeStyles({
  startScanButton: {
    height: "60px",
    padding: "0 25px",
  },
  smallText: {
    fontSize: "0.85em",
  },
});

type UseWebsiteAudit = {
  requests: {
    websiteAuditRequest: NetworkRequestState;
    orgRequest: NetworkRequestState;
    surveyRequest: NetworkRequestState;
    incentiveSurveyRequest: NetworkRequestState;
    privacyCenterRequest: NetworkRequestState;
  };
  refreshSurvey: RefreshFn;
  props: WebsiteAuditProps;
};

export type WebsiteAuditProps = {
  org: OrganizationDto;
  websiteAuditScan: WebsiteAuditResultDto;
  handleScanStart: (url: string) => Promise<void>;
  handleResetScan: () => Promise<void>;
  handleFormUpdate: (e: React.ChangeEvent<HTMLInputElement>) => void;
  requirements: WebsiteAuditRequirement[];
  suggestions: WebsiteAuditSuggestion[];
  complete: boolean;
  answers: SurveyAnswers;
  completeDisabledTooltip: string;
  showFinancialIncentiveDisclaimer?: boolean;
  onRescan?: () => void;
  createScanTask: () => Promise<void>;
  showSkip?: boolean;
  suggestedUrl?: string;
};

export type WebsiteAuditRequirement = {
  questionSlug: string;
  shouldHide: (scan: WebsiteAuditResultDto) => boolean;
  isSuccessful: (scan: WebsiteAuditResultDto) => boolean;
  successText: string | ReactNode;
  errorText: string | ReactNode | ((scan: WebsiteAuditResultDto) => string | ReactNode);
};

export type WebsiteAuditSuggestion = {
  questionSlug: string;
  shouldDisplay: (scan: WebsiteAuditResultDto) => boolean;
  text: string | ReactNode;
};

export const useWebsiteAudit = (): UseWebsiteAudit => {
  const orgId = usePrimaryOrganizationId();
  const [org, orgRequest] = useOrganization(orgId);
  const classes = useClasses();
  const [privacyCenter, privacyCenterRequest] = usePrivacyCenter(orgId);
  const { answers, setAnswer, surveyRequest, refreshSurvey } = useSurvey(
    orgId,
    WEBSITE_AUDIT_SURVEY_NAME,
  );
  const { answers: incentivesAnswers, surveyRequest: incentiveSurveyRequest } = useSurvey(
    orgId,
    CONSUMER_INCENTIVES_SURVEY_NAME,
  );
  const [websiteAuditScan, websiteAuditRequest, refresh] = useWebsiteAuditResult(orgId);

  // Handlers
  const { fetch: handleScanStart } = useActionRequest({
    api: async (url: string) => await Api.websiteAudit.startWebsiteAudit(orgId, url),
    onSuccess: () => refresh(),
  });

  const { fetch: handleResetScan } = useActionRequest({
    api: async () => await Api.websiteAudit.resetWebsiteAudit(orgId),
    onSuccess: () => refresh(),
  });

  const requirements: WebsiteAuditRequirement[] = [
    {
      questionSlug: "privacy-policy-link",
      shouldHide: (scan) =>
        scan.privacyPolicyLinkTextValid === null || scan.privacyPolicyLinkUrlValid === null,
      isSuccessful: (scan) => scan.privacyPolicyLinkTextValid && scan.privacyPolicyLinkUrlValid,
      successText: "Privacy policy linked in footer points to TrueVault-hosted Privacy Center",
      errorText:
        "Privacy Policy link is missing from website footer or does not link to TrueVault-hosted Privacy Center",
    },
    {
      questionSlug: "opt-out-link",
      shouldHide: (scan) =>
        scan.optOutLinkTextValid === null || scan.optOutLinkYourPrivacyChoicesIcon === null,
      isSuccessful: (scan) =>
        (scan.optOutLinkTextValid || scan.optOutLinkYourPrivacyChoicesIcon) &&
        scan.optOutLinkUrlValid,
      successText: "Opt-out link appears in website footer and links to opt-out notice",
      errorText: (
        <Paras>
          <Para>
            Opt-out link is missing from website footer or does not link to Notice of Right to
            Opt-Out
          </Para>
          <Para className={`w-512 ${classes.smallText}`}>
            California now allows you to replace your “Do Not Sell/Share My Personal Information”
            link with a general opt-out link that can be used for California and other states with
            data privacy laws, including Virginia, Colorado, and Connecticut. You may update your
            link using the old opt-out text, or this new icon below.
          </Para>
          <Para>
            <strong>
              <u>“Do Not Sell or Share My Personal Information”</u>
            </strong>
            <ArrowForward
              color="info"
              className="website-audit__your-privacy-choices-arrow mx-sm"
            />
            <img
              src="https://polaris.truevaultcdn.com/static/assets/icons/optout-icon-blue.svg"
              className="mx-xs website-audit__your-privacy-choices-icon"
              alt="California Consumer Privacy Act (CCPA) Opt-Out Icon"
              width="36"
            />
            <strong className="website-audit__your-privacy-choices">Your Privacy Choices</strong>
          </Para>
        </Paras>
      ),
    },
    {
      questionSlug: "ca-privacy-notice",
      shouldHide: (scan) =>
        scan.caPrivacyNoticeLinkTextValid === null || scan.caPrivacyNoticeLinkUrlValid === null,
      isSuccessful: (scan) => scan.caPrivacyNoticeLinkTextValid && scan.caPrivacyNoticeLinkUrlValid,
      successText: "California Notice at Collection appears in website footer",
      errorText: "California Notice at Collection is missing from website footer.",
    },
    {
      questionSlug: "polaris-js",
      shouldHide: (scan) =>
        scan.polarisJsSetup === null ||
        scan.polarisJsBeforeGoogleAnalytics === null ||
        scan.polarisJsNoDefer === null,
      isSuccessful: (scan) =>
        scan.polarisJsSetup && scan.polarisJsBeforeGoogleAnalytics && scan.polarisJsNoDefer,
      successText: "Polaris JavaScript was added properly to your website",
      errorText: (scan) => {
        if (!scan.polarisJsSetup) {
          return "Polaris JavaScript is not added to your website.";
        }
        const errors = [];
        if (!scan.polarisJsNoDefer) {
          errors.push(
            <>
              Remove the <code>defer</code> attribute from your Polaris JavaScript.
            </>,
          );
        }
        if (!scan.polarisJsBeforeGoogleAnalytics) {
          errors.push(
            <>
              Move the Polaris JavaScript above your Google Tag Manager or Google Analytics code
              snippet.
            </>,
          );
        }
        return (
          <Paras>
            <Para>
              Polaris JavaScript is not set up properly on your website. Please make the following
              changes:
            </Para>
            <ul>
              {errors.map((error, i) => (
                <li key={`polaris-js-err-${i}`}>{error}</li>
              ))}
            </ul>
          </Paras>
        );
      },
    },
  ];

  const suggestions: WebsiteAuditSuggestion[] = [
    {
      questionSlug: "opt-out-link-upgrade",
      shouldDisplay: (scan) =>
        scan.optOutLinkTextValid &&
        !scan.optOutLinkYourPrivacyChoicesIcon &&
        scan.optOutLinkUrlValid,
      text: (
        <Paras>
          <Para>
            California now allows you to replace your “Do Not Sell/Share My Personal Information”
            link with a general opt-out link that can be used for California and other states with
            data privacy laws, including Virginia, Colorado, and Connecticut.
          </Para>
          <Para>
            <strong>
              <u>“Do Not Sell or Share My Personal Information”</u>
            </strong>
            <ArrowForward
              color="info"
              className="website-audit__your-privacy-choices-arrow mx-sm"
            />
            <img
              src="https://polaris.truevaultcdn.com/static/assets/icons/optout-icon-blue.svg"
              className="mx-xs website-audit__your-privacy-choices-icon"
              alt="California Consumer Privacy Act (CCPA) Opt-Out Icon"
              width="36"
            />
            <strong className="website-audit__your-privacy-choices">Your Privacy Choices</strong>
          </Para>
        </Paras>
      ),
    },
  ];

  const complete = useMemo(
    () =>
      websiteAuditScan &&
      requirements.every(
        (t) =>
          t.shouldHide(websiteAuditScan) ||
          ["IGNORE", "CREATE_TASK"].includes(answers[t.questionSlug]) ||
          t.isSuccessful(websiteAuditScan),
      ) &&
      suggestions.every(
        (s) =>
          !s.shouldDisplay(websiteAuditScan) ||
          ["IGNORE", "CREATE_TASK"].includes(answers[s.questionSlug]),
      ),
    [requirements, suggestions, answers, websiteAuditScan],
  );

  const completeDisabledTooltip = useMemo(() => {
    if (
      requirements.some((t) => answers[t.questionSlug] === "RETRY") ||
      suggestions.every((s) => answers[s.questionSlug] === "RETRY")
    ) {
      return "Rescan your site to verify the 'fixed' issues before continuing.";
    }
    return "Please complete the form above to continue.";
  }, [requirements, suggestions, answers]);

  // Poll for completion
  usePolling(() => refresh(), 10000, websiteAuditScan?.status == "IN_PROGRESS");

  const suggestedUrl = useMemo(
    () => privacyCenter?.customUrl?.replace(/^privacy\./i, ""),
    [privacyCenter],
  );

  return {
    requests: {
      websiteAuditRequest,
      orgRequest,
      surveyRequest,
      incentiveSurveyRequest,
      privacyCenterRequest,
    },
    refreshSurvey,
    props: {
      createScanTask: () => setAnswer("defer-website-audit", "CREATE_TASK"),
      showSkip: answers["defer-website-audit"] !== "CREATE_TASK",
      requirements,
      suggestions,
      org,
      websiteAuditScan,
      answers,
      suggestedUrl,
      complete,
      completeDisabledTooltip,
      showFinancialIncentiveDisclaimer: incentivesAnswers["offer-consumer-incentives"] === "true",
      handleScanStart,
      handleResetScan,
      handleFormUpdate: (e: any) => {
        setAnswer(e.target.name, e.target.value);
      },
    },
  };
};

export const WebsiteAudit: React.FC<
  WebsiteAuditProps & {
    isFinished: boolean;
    markFinished: () => void;
  }
> = ({
  org,
  websiteAuditScan,
  handleScanStart,
  handleResetScan,
  handleFormUpdate,
  isFinished,
  markFinished,
  onRescan,
  createScanTask,
  suggestedUrl,
  ...props
}) => {
  return (
    <>
      {websiteAuditScan?.status == "ERROR" && (
        <WebsiteAuditError createTask={createScanTask} url={websiteAuditScan.scanUrl} />
      )}
      <StartScanForm
        onStartScan={handleScanStart}
        suggestedUrl={suggestedUrl}
        inProgress={websiteAuditScan?.status == "IN_PROGRESS"}
      />
      {websiteAuditScan?.status == "IN_PROGRESS" && (
        <WebsiteAuditProgress url={websiteAuditScan?.scanUrl} onCancel={handleResetScan} />
      )}
      {websiteAuditScan?.status == "COMPLETE" && (
        <>
          <WebsiteAuditResults
            {...props}
            org={org}
            url={websiteAuditScan.scanUrl}
            scan={websiteAuditScan}
            restartScan={async () => {
              const url = websiteAuditScan?.scanUrl;
              await handleResetScan();
              await handleScanStart(url);
              onRescan();
            }}
            handleFormUpdate={handleFormUpdate}
            isFinished={isFinished}
            markFinished={markFinished}
          />
        </>
      )}
    </>
  );
};

type StartScanFormProps = {
  onStartScan: (url: string) => void;
  inProgress: boolean;
  suggestedUrl?: string;
};
const StartScanForm: React.FC<StartScanFormProps> = ({
  onStartScan,
  inProgress,
  suggestedUrl = "",
}) => {
  const classes = useClasses();
  const handleUrlSubmit = async (values: any) => {
    onStartScan(values.websiteAuditUrl);
  };

  const validate = (values: any) => {
    const errors = {} as { [x: string]: string };
    requiredUrl(values, "websiteAuditUrl", errors);
    return errors;
  };

  return (
    <Form
      onSubmit={handleUrlSubmit}
      validate={validate}
      render={({ handleSubmit, valid }) => {
        return (
          <form className="d-flex align-items-start mb-xl" onSubmit={handleSubmit}>
            <Text
              multiline={false}
              variant="outlined"
              field="websiteAuditUrl"
              initialValue={suggestedUrl}
              className="mr-md mt-0"
              fullWidth
              InputProps={{
                startAdornment: <Adornment position="start">https://</Adornment>,
              }}
              size="medium"
              required
              autoFocus
            />

            <Button
              size="large"
              color="primary"
              type="submit"
              variant="contained"
              disabled={!valid || inProgress}
              className={classes.startScanButton}
            >
              {inProgress ? "Scanning..." : "Scan My Site"}
            </Button>
          </form>
        );
      }}
    ></Form>
  );
};
