import { Button, FormControl, FormControlLabel, Radio, RadioGroup } from "@mui/material";
import React from "react";
import { Paras } from "../../typography/Paras";
import { Para } from "../../typography/Para";
import {
  AssignmentTurnedInOutlined,
  CheckCircle,
  Error,
  InfoOutlined,
  Launch,
} from "@mui/icons-material";
import { Callout, CalloutVariant } from "../../Callout";
import { WebsiteAuditResultDto } from "../../../../common/service/server/dto/WebsiteAuditDto";
import { RouteHelpers } from "../../../root/routes";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { DisableableButton } from "../../Disableable";
import { WebsiteAuditSuggestion, WebsiteAuditRequirement } from "./WebsiteAudit";
import { SurveyAnswers } from "../../../../common/hooks/useSurvey";

const isScanSuccessful = (scan: WebsiteAuditResultDto): boolean =>
  [
    "privacyPolicyLinkUrlValid",
    "privacyPolicyLinkTextValid",
    "optOutLinkTextValid",
    "optOutLinkUrlValid",
    "optOutLinkYourPrivacyChoicesIcon",
    "caPrivacyNoticeLinkUrlValid",
    "caPrivacyNoticeLinkTextValid",
    "polarisJsSetup",
    "polarisJsBeforeGoogleAnalytics",
    "polarisJsNoDefer",
  ].every((key) => scan[key] === true || scan[key] === null);

export type WebsiteAuditResultsProps = {
  org: OrganizationDto;
  url: string;
  scan: WebsiteAuditResultDto;
  complete: boolean;
  completeDisabledTooltip: string;
  isFinished: boolean;
  restartScan: () => void;
  markFinished: () => void;
  handleFormUpdate: (e: React.ChangeEvent<HTMLInputElement>) => void;
  requirements: WebsiteAuditRequirement[];
  suggestions: WebsiteAuditSuggestion[];
  showFinancialIncentiveDisclaimer?: boolean;
  answers: SurveyAnswers;
};

export const WebsiteAuditResults: React.FC<WebsiteAuditResultsProps> = ({
  org,
  url,
  scan,
  restartScan,
  complete,
  completeDisabledTooltip,
  handleFormUpdate,
  requirements,
  suggestions,
  answers,
  showFinancialIncentiveDisclaimer,
  markFinished,
  isFinished,
}) => {
  const visibleRequirements = requirements.filter(({ shouldHide }) => scan && !shouldHide(scan));

  return (
    <div>
      <Paras>
        <Para color="primary" className="d-flex align-items-center mb-mdlg">
          <AssignmentTurnedInOutlined color="primary" className="mr-xs" />
          <strong>Scan of {url} successfully completed</strong>
        </Para>
      </Paras>
      <StatusCallout success={isScanSuccessful(scan)} />

      <div className="ml-xxl my-xxl">
        {visibleRequirements.map(({ questionSlug, isSuccessful, successText, errorText }) => {
          if (scan && isSuccessful(scan)) {
            return (
              <div className="d-flex align-items-start mb-mdlg">
                <CheckCircle color="success" />
                <div className="ml-xs">{successText}</div>
              </div>
            );
          } else {
            return (
              <>
                <div className="d-flex align-items-start mb-mdlg">
                  <Error className="website-audit__error-icon" />
                  <div className="ml-xs">
                    {typeof errorText === "function" ? errorText(scan) : errorText}
                    <p>
                      <a
                        className="website-audit__view-instructions"
                        href={RouteHelpers.instructions.id(org.devInstructionsId)}
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        <strong>View Developer Instructions</strong>{" "}
                        <Launch className="website-audit__launch-icon" />
                      </a>
                    </p>
                    <FormControl component="fieldset" className={"mb-md"}>
                      <RadioGroup
                        name={questionSlug}
                        aria-label="Vendor handling response"
                        onChange={handleFormUpdate}
                        value={answers[questionSlug]}
                      >
                        <FormControlLabel
                          value="RETRY"
                          control={<Radio />}
                          label={
                            <span className={"text-t2 pr-xs"}>This issue has been fixed.</span>
                          }
                        />
                        <FormControlLabel
                          value="CREATE_TASK"
                          control={<Radio />}
                          label={
                            <span className={"text-t2 pr-xs"}>
                              Create a task for me to complete this later.
                            </span>
                          }
                        />
                        <FormControlLabel
                          value="IGNORE"
                          control={<Radio />}
                          label={
                            <span className={"text-t2 pr-xs"}>
                              Ignore - this issue was identified in error.
                            </span>
                          }
                        />
                      </RadioGroup>
                    </FormControl>
                    {answers[questionSlug] === "RETRY" && (
                      <Callout variant={CalloutVariant.Purple}>
                        To retry, click the "Rescan My Site" button below.
                      </Callout>
                    )}
                    {isFinished && answers[questionSlug] === "CREATE_TASK" && (
                      <Callout variant={CalloutVariant.Green}>
                        We’ll create a task for you shortly to address this issue later.
                      </Callout>
                    )}
                  </div>
                </div>
              </>
            );
          }
        })}

        {suggestions.map(({ questionSlug, shouldDisplay, text }) => {
          if (scan && shouldDisplay(scan)) {
            return (
              <>
                <div className="d-flex align-items-start mb-mdlg">
                  <InfoOutlined color="secondary" />
                  <div className="ml-xs">
                    {text}
                    <p>
                      <a
                        className="website-audit__view-instructions"
                        href={RouteHelpers.instructions.id(org.devInstructionsId)}
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        <strong>View Developer Instructions</strong>{" "}
                        <Launch className="website-audit__launch-icon" />
                      </a>
                    </p>
                    <FormControl component="fieldset" className={"mb-md"}>
                      <RadioGroup
                        name={questionSlug}
                        aria-label="Vendor handling response"
                        onChange={handleFormUpdate}
                        value={answers[questionSlug]}
                      >
                        <FormControlLabel
                          value="RETRY"
                          control={<Radio />}
                          label={
                            <span className={"text-t2 pr-xs"}>This issue has been fixed.</span>
                          }
                        />
                        <FormControlLabel
                          value="CREATE_TASK"
                          control={<Radio />}
                          label={
                            <span className={"text-t2 pr-xs"}>
                              Create a task for me to complete this later.
                            </span>
                          }
                        />
                        <FormControlLabel
                          value="IGNORE"
                          control={<Radio />}
                          label={
                            <span className={"text-t2 pr-xs"}>
                              Ignore - this issue was identified in error.
                            </span>
                          }
                        />
                      </RadioGroup>
                    </FormControl>
                    {answers[questionSlug] === "RETRY" && (
                      <Callout variant={CalloutVariant.Purple}>
                        To retry, click the "Rescan My Site" button below.
                      </Callout>
                    )}
                    {isFinished && answers[questionSlug] === "CREATE_TASK" && (
                      <Callout variant={CalloutVariant.Green}>
                        We’ll create a task for you shortly to address this issue later.
                      </Callout>
                    )}
                  </div>
                </div>
              </>
            );
          }
        })}
      </div>

      {showFinancialIncentiveDisclaimer && (
        <Callout variant={CalloutVariant.Yellow} icon={InfoOutlined}>
          <p>
            Please note: If you’re using a pop-up for financial incentives, the website audit will
            not be able to scan and find your notice of financial incentives. Check that your Notice
            of Financial Incentive appears like the example image below.
          </p>
          <p className="text-center">
            <img src="/assets/images/surveys/2023q3/financial-incentive-example.jpg" width="60%" />
          </p>
        </Callout>
      )}

      {!isFinished && (
        <div className="my-xl d-flex justify-end">
          <Button
            variant="contained"
            color="primary"
            type="submit"
            size="large"
            onClick={restartScan}
          >
            Rescan my Site
          </Button>
          <DisableableButton
            disabled={!complete}
            disabledTooltip={completeDisabledTooltip}
            variant="contained"
            color="primary"
            type="submit"
            size="large"
            className="px-lg ml-mdlg"
            onClick={markFinished}
          >
            Done
          </DisableableButton>
        </div>
      )}
    </div>
  );
};

const StatusCallout: React.FC<{ success: boolean }> = ({ success }) => {
  if (success) {
    return (
      <Callout
        variant={CalloutVariant.Green}
        title="Everything looks good! No issues found."
        className="mb-mdlg"
      />
    );
  }
  return (
    <Callout variant={CalloutVariant.Yellow} title="We've found some issues." className="mb-mdlg">
      Our search indicates you are missing certain requirements from our setup instructions. Please
      note that our website scan may not be 100% accurate. If you feel a result is in error, click
      “Ignore.” Otherwise, you may address the issue now, or create a task to do so later.
    </Callout>
  );
};
