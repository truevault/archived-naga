import { Attachment, Close } from "@mui/icons-material";
import {
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import React from "react";

export const AttachmentList: React.FC<
  {
    attachmentNames?: string[];
    onRemove: (attachmentIndex: number) => void;
  } & React.ComponentProps<typeof List>
> = ({ attachmentNames = [], onRemove, ...rest }) => (
  <List {...rest}>
    {attachmentNames.length > 0 ? (
      <>
        {attachmentNames.map((name, index) => {
          return (
            <ListItem
              onClick={null}
              key={`${name}-${index}`}
              secondaryAction={
                <IconButton
                  size="small"
                  edge="end"
                  aria-label="remove attachment"
                  onClick={() => onRemove(index)}
                >
                  <Close />
                </IconButton>
              }
            >
              <ListItemButton>
                <ListItemIcon>
                  <Attachment />
                </ListItemIcon>
                <ListItemText primary={<code>{name}</code>} />
              </ListItemButton>
            </ListItem>
          );
        })}
      </>
    ) : (
      <ListItem>
        <ListItemText>No attachments</ListItemText>
      </ListItem>
    )}
  </List>
);
