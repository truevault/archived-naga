import _ from "lodash";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { ExternalLink } from "../../../../common/components/ExternalLink";
import { Api } from "../../../../common/service/Api";
import { UploadVendorAgreementResponse } from "../../../../common/service/server/controller/StorageController";
import {
  GDPRProcessorRecommendation,
  OrganizationDataRecipientDto,
  OrganizationVendorSettingsDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import {
  getSelectedDirectSellingVendors,
  getSelectedIntentionalInteractionVendors,
} from "../../../pages/get-compliant/data-recipients/shared";
import { UpdateVendorArgs } from "../../../pages/stay-compliant/DataRecipientEditPage";
import { vendorName } from "../../../util/resources/vendorUtils";
import { getVendorSettingsObject, isAdNetwork, isGoogleAnalytics } from "../../../util/vendors";
import { PROCESSOR_CLASSIFICATION_OPTIONS } from "../../get-compliant/vendors/GDPRProcessorLanguageLocator";
import { SCC_OPTIONS } from "../../get-compliant/vendors/GDPRSccDocumentationLocator";
import {
  VendorFlagOption,
  VendorRadioOption,
  VendorRadioSection,
} from "../../get-compliant/vendors/VendorFlagWithDocumentation";
import { Para } from "../../typography/Para";
import { Paras } from "../../typography/Paras";
import { VendorBooleanQuestion } from "../../vendor/VendorBooleanQuestion";

const calculateDisclosureMeta = (
  vendor: OrganizationDataRecipientDto | VendorDto | null,
  state: DisclosureState,
  custom: boolean,
  customCategory?: string,
) => {
  let orgVendor: OrganizationDataRecipientDto | null = null;
  if (vendor && isOrganizationDataRecipient(vendor)) {
    orgVendor = vendor;
  }

  customCategory = customCategory || vendor?.category;
  const isAd = custom ? customCategory == "Ad Network" : vendor ? isAdNetwork(vendor) : false;
  const classifiedAsContractor = state.classification === "contractor" ?? false;
  const classifiedAsThirdParty = state.classification === "third-party" ?? false;
  const isConsumer = orgVendor?.collectionContext?.includes("CONSUMER") ?? true;

  const showIntInteraction =
    isConsumer &&
    (custom
      ? !classifiedAsContractor && !isAd
      : !classifiedAsContractor &&
        !isAd &&
        (classifiedAsThirdParty || vendor?.isPotentialIntentionalInteractionVendor));

  const showSelling =
    isConsumer &&
    (custom
      ? !classifiedAsContractor && !isAd
      : !classifiedAsContractor &&
        !isAd &&
        (classifiedAsThirdParty || vendor?.serviceProviderRecommendation != "SERVICE_PROVIDER"));

  const showSharing = isConsumer && (custom ? isAd : !classifiedAsContractor && isAd);

  const questionMeta: DisclosureMeta = {
    showClassification: isConsumer,
    showCookies: true,
    showIntInteraction: Boolean(showIntInteraction),
    showCustomAudience: isAd,
    showSharing,
    showSelling,

    lockCcpaIsSelling:
      Boolean(state?.isIntentionalInteraction) || (vendor ? isGoogleAnalytics(vendor) : false),
    lockClassificationThirdParty: Boolean(
      state.isIntentionalInteraction ||
        state.ccpaIsSelling ||
        state.ccpaIsSharing ||
        state.usesCustomAudience,
    ),

    // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
    // showFacebookLdu: vendor ? isFacebookAds(vendor) : false,
    // showGoogleRdp: vendor ? isGoogleAds(vendor) : false,
    showGoogleAnalytics: vendor ? isGoogleAnalytics(vendor) : false,

    showServiceProviderDocumentationInput: vendor
      ? vendor.serviceProviderRecommendation != "SERVICE_PROVIDER" ||
        !vendor.serviceProviderLanguageUrl
      : false,

    recommendClassificationProcessor: orgVendor?.gdprProcessorRecommendation === "Processor",
    recommendClassificationController: orgVendor?.gdprProcessorRecommendation === "Controller",
    lockSccDocumentation: Boolean(orgVendor?.gdprSccDocumentationUrl),
    showSccDocumentation: Boolean(
      !orgVendor?.isExceptedFromSccs && orgVendor?.gdprInternationalDataTransferRulesApply,
    ),
  };

  return questionMeta;
};

export type DisclosureMeta = {
  showClassification: boolean;
  showCookies: boolean;
  showIntInteraction: boolean;
  showCustomAudience: boolean;
  showSharing: boolean;
  showSelling: boolean;

  // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
  // showFacebookLdu: boolean;
  // showGoogleRdp: boolean;
  showGoogleAnalytics: boolean;

  showServiceProviderDocumentationInput: boolean;

  lockCcpaIsSelling?: boolean;
  lockClassificationThirdParty: boolean;

  recommendClassificationProcessor: boolean;
  recommendClassificationController: boolean;

  lockSccDocumentation: boolean;
  showSccDocumentation: boolean;
};

interface VendorClassificationEditProps {
  org: OrganizationDto;
  vendor: OrganizationDataRecipientDto | VendorDto;
  isThirdParty: boolean;

  state: DisclosureState;
  meta: DisclosureMeta;
  updateState: (update: Partial<DisclosureState>) => void;
}

type ClassificationType = "service-provider" | "third-party" | "contractor" | null;

export type DisclosureState = {
  classification: ClassificationType;

  publicTosUrl: string;

  serviceProviderFile: File | null;
  serviceProviderFilename: string | null;

  isIntentionalInteraction: boolean | null;
  ccpaIsSelling: boolean | null;
  ccpaIsSharing: boolean | null;
  ccpaIsDirectSelling: boolean | null;
  usesCustomAudience: boolean | null;

  // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
  // facebookLdu: boolean | null;
  // googleRdp: boolean | null;
  googleAnalytics: boolean | null;
  processorClassification: GDPRProcessorRecommendation | null;
  processorGuaranteeUrl: string | null;
  processorGuaranteeFile: File | null;
  processorGuaranteeFilename: string | null;

  gdprHasSccSetting: boolean | null;
  gdprSccUrl: string | null;
  gdprSccFile: File | null;
  gdprSccFilename: string | null;
};

const isOrganizationDataRecipient = (
  vendor: OrganizationDataRecipientDto | VendorDto,
): vendor is OrganizationDataRecipientDto => {
  return (vendor as OrganizationDataRecipientDto).classificationOptions !== undefined;
};

const initialStateFrom = (
  vendor: OrganizationDataRecipientDto | VendorDto,
  answers: Record<string, string> | null,
): DisclosureState => {
  if (!vendor) {
    return {
      publicTosUrl: "",
      classification: null,
      serviceProviderFile: null,
      serviceProviderFilename: null,
      isIntentionalInteraction: null,
      usesCustomAudience: null,
      ccpaIsSelling: null,
      ccpaIsSharing: null,
      ccpaIsDirectSelling: null,
      // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
      // facebookLdu: null,
      // googleRdp: null,
      googleAnalytics: null,
      processorClassification: null,
      processorGuaranteeUrl: null,
      processorGuaranteeFile: null,
      processorGuaranteeFilename: null,
      gdprHasSccSetting: null,
      gdprSccUrl: null,
      gdprSccFile: null,
      gdprSccFilename: null,
    };
  }

  let orgVendor: OrganizationDataRecipientDto | null = null;
  if (vendor && isOrganizationDataRecipient(vendor)) {
    orgVendor = vendor;
  }

  const serviceProviderClassification = orgVendor?.classificationOptions.find(
    (x) => x.slug === "service-provider",
  );
  const thirdPartyClassification = orgVendor?.classificationOptions.find(
    (x) => x.slug === "third-party",
  );
  const contractorClassification = orgVendor?.classificationOptions.find(
    (x) => x.slug === "contractor",
  );

  let classification: ClassificationType = null;
  if (orgVendor) {
    if (orgVendor?.classification?.id === serviceProviderClassification?.id) {
      classification = "service-provider" as const;
    } else if (orgVendor?.classification?.id === thirdPartyClassification?.id) {
      classification = "third-party" as const;
    } else if (orgVendor?.classification?.id === contractorClassification?.id) {
      classification = "contractor" as const;
    }
  }

  const processorClassification = orgVendor?.gdprProcessorSetting;
  const isProcessor = processorClassification === "Processor";

  const processorGuaranteeUrl = isProcessor
    ? orgVendor?.gdprProcessorGuaranteeUrl
    : orgVendor?.gdprControllerGuaranteeUrl;

  const processorGuaranteeFilename = isProcessor
    ? orgVendor?.gdprProcessorGuaranteeFileName
    : orgVendor?.gdprControllerGuaranteeFileName;

  const intentionalInteractionVendors = answers
    ? getSelectedIntentionalInteractionVendors(answers)
    : [];

  const directSellingVendors = answers ? getSelectedDirectSellingVendors(answers) : [];
  const isIntentionalVendor = orgVendor
    ? intentionalInteractionVendors.some((id) => id == vendor.id)
    : null;
  const isDirectSellingVendor = orgVendor
    ? directSellingVendors.some((id) => id == vendor.id)
    : null;

  // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
  // const isFacebookLdu = answers ? answers["facebook-ldu"] == "true" : null;
  // const isGoogleRdp = answers ? answers["google-rdp"] == "true" : null;
  const isGA = vendor ? isGoogleAnalytics(vendor) : false;
  const isGoogleAnalyticsDataSharing = answers
    ? answers["google-analytics-data-sharing"] == "true"
    : null;
  const gaIsSellingState = isGA ? !isGoogleAnalyticsDataSharing : undefined;

  return {
    publicTosUrl: orgVendor?.publicTosUrl,
    classification,
    ccpaIsSelling: gaIsSellingState ?? orgVendor?.ccpaIsSelling,
    isIntentionalInteraction: isIntentionalVendor,
    usesCustomAudience: orgVendor?.usesCustomAudience,
    ccpaIsDirectSelling: isDirectSellingVendor,
    ccpaIsSharing: orgVendor?.ccpaIsSharing,
    serviceProviderFile: null,
    serviceProviderFilename: orgVendor?.tosFileName,
    // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
    // facebookLdu: isFacebookLdu,
    // googleRdp: isGoogleRdp,
    googleAnalytics: isGoogleAnalyticsDataSharing,
    processorClassification,
    processorGuaranteeUrl,
    processorGuaranteeFile: null,
    processorGuaranteeFilename,

    gdprHasSccSetting: orgVendor?.gdprHasSccSetting,
    gdprSccUrl: orgVendor?.gdprSccUrl,
    gdprSccFile: null,
    gdprSccFilename: orgVendor?.gdprSccFileName,
  };
};

export const finishedDisclosure = (
  state: DisclosureState,
  meta: DisclosureMeta,
  isThirdParty: boolean,
): boolean => {
  if (!meta) {
    return false;
  }

  const hasClassification =
    state.classification != null || isThirdParty || !meta.showClassification;
  const hasServiceProviderDocumentation =
    state.classification != "service-provider" ||
    !meta.showServiceProviderDocumentationInput ||
    state.serviceProviderFile ||
    state.publicTosUrl;

  const hasIntentionalInteraction =
    !meta.showIntInteraction || state.isIntentionalInteraction != null;
  const hasSelling = !meta.showSelling || !!meta.lockCcpaIsSelling || state.ccpaIsSelling != null;
  const hasSharing = !meta.showSharing || state.ccpaIsSharing != null;
  const hasCustomAudience = !meta.showCustomAudience || state.usesCustomAudience != null;
  const isSelling = Boolean(state.ccpaIsSelling);
  const hasDirect = !meta.showCookies || !isSelling || state.ccpaIsDirectSelling != null;

  // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
  // const hasFacebookLdu = !meta.showFacebookLdu || state.facebookLdu != null;
  // const hasGoogleRdp = !meta.showGoogleRdp || state.googleRdp != null;
  const hasGoogleAnalytics = !meta.showGoogleAnalytics || state.googleAnalytics != null;

  return (
    hasClassification &&
    hasCustomAudience &&
    hasServiceProviderDocumentation &&
    hasIntentionalInteraction &&
    hasSelling &&
    hasSharing &&
    hasDirect &&
    // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
    // hasFacebookLdu &&
    // hasGoogleRdp &&
    hasGoogleAnalytics
  );
};

const vendorSettingsFromDisclosureState = (
  vendor: OrganizationDataRecipientDto,
  state: DisclosureState,
): OrganizationVendorSettingsDto => {
  const current = getVendorSettingsObject(vendor);

  const updated = {
    ...current,
    ccpaIsSelling: state.ccpaIsSelling,
    ccpaIsSharing: state.ccpaIsSharing,
    usesCustomAudience: state.usesCustomAudience,
    publicTosUrl: state.publicTosUrl,
    gdprProcessorSetting: state.processorClassification,
    gdprHasSccSetting: state.gdprHasSccSetting,
    gdprSccUrl: state.gdprSccUrl,

    classificationId: vendor.classificationOptions?.find((opt) => opt.slug == state.classification)
      ?.id,
  };

  if (state.processorClassification === "Processor") {
    updated.gdprProcessorGuaranteeUrl = state.processorGuaranteeUrl;
    updated.gdprControllerGuaranteeUrl = null;
  } else if (state.processorClassification === "Controller") {
    updated.gdprProcessorGuaranteeUrl = null;
    updated.gdprControllerGuaranteeUrl = state.processorGuaranteeUrl;
  }

  return updated;
};

export const saveDisclosureUpdates = async (
  org: OrganizationDto,
  vendor: OrganizationDataRecipientDto,
  answers: Record<string, string>,
  state: DisclosureState,
  updateVendor: (v: UpdateVendorArgs) => Promise<void>,
  updateSurveyQuestions: (a: any) => void,
) => {
  // 1 - add file if it exists, & is new
  let agreementResponse: UploadVendorAgreementResponse | null = null;
  if (state.serviceProviderFile && state.serviceProviderFilename != vendor.tosFileName) {
    // do the file upload
    agreementResponse = await Api.storage.postOrganizationVendorAgreement(
      org.id,
      vendor.vendorId,
      state.serviceProviderFile.name,
      state.serviceProviderFile,
    );
  }

  let processorAgreementResponse: UploadVendorAgreementResponse | null = null;
  const isNewProcessorFile =
    state.processorClassification === "Processor" &&
    state.processorGuaranteeFilename != vendor.gdprProcessorGuaranteeFileName;
  const isNewControllerFile =
    state.processorClassification === "Controller" &&
    state.processorGuaranteeFilename != vendor.gdprControllerGuaranteeFileName;
  if (state.processorGuaranteeFile && (isNewProcessorFile || isNewControllerFile)) {
    processorAgreementResponse = await Api.storage.postOrganizationVendorAgreement(
      org.id,
      vendor.vendorId,
      state.processorGuaranteeFile.name,
      state.processorGuaranteeFile,
    );
  }

  let sccDocumentationResponse: UploadVendorAgreementResponse | null = null;
  if (state.gdprSccFile && state.gdprSccFilename != vendor.gdprSccFileName) {
    sccDocumentationResponse = await Api.storage.postOrganizationVendorAgreement(
      org.id,
      vendor.vendorId,
      state.gdprSccFile.name,
      state.gdprSccFile,
    );
  }

  // 2 - delete file if it doesn't exist & used to
  let deletedUpload = false;
  if (!state.serviceProviderFilename && vendor.tosFileName) {
    // do the file delete
    await Api.storage.deleteOrganizationVendorAgreement(org.id, vendor.vendorId, vendor.tosFileKey);
    deletedUpload = true;
  }

  let deletedProcessorUpload = false;
  let deletedControllerUpload = false;
  const hasProcessorFile =
    state.processorClassification === "Processor" && Boolean(vendor.gdprProcessorGuaranteeFileKey);
  const hasControllerFile =
    state.processorClassification === "Controller" &&
    Boolean(vendor.gdprControllerGuaranteeFileKey);
  if (!state.processorGuaranteeFilename && hasProcessorFile) {
    await Api.storage.deleteOrganizationVendorAgreement(
      org.id,
      vendor.vendorId,
      vendor.gdprProcessorGuaranteeFileKey,
    );
    deletedProcessorUpload = true;
  }

  if (!state.processorGuaranteeFilename && hasControllerFile) {
    await Api.storage.deleteOrganizationVendorAgreement(
      org.id,
      vendor.vendorId,
      vendor.gdprControllerGuaranteeFileKey,
    );
    deletedControllerUpload = true;
  }

  let deletedSccDocumentation = false;
  if (!state.gdprSccFilename && vendor.gdprSccFileName) {
    await Api.storage.deleteOrganizationVendorAgreement(
      org.id,
      vendor.vendorId,
      vendor.gdprSccFileKey,
    );
    deletedSccDocumentation = true;
  }

  // 3 - calculate new settings & save
  const settings: OrganizationVendorSettingsDto = vendorSettingsFromDisclosureState(vendor, state);

  if (agreementResponse) {
    settings.tosFileName = agreementResponse.fileName;
    settings.tosFileKey = agreementResponse.fileKey;
    settings.vendorContractReviewedId = null;
  } else {
    settings.tosFileName = vendor.tosFileName;
    settings.tosFileKey = vendor.tosFileKey;
  }

  if (deletedUpload) {
    settings.tosFileName = null;
    settings.tosFileKey = null;
  }

  if (processorAgreementResponse) {
    if (state.processorClassification === "Processor") {
      settings.gdprProcessorGuaranteeFileName = processorAgreementResponse.fileName;
      settings.gdprProcessorGuaranteeFileKey = processorAgreementResponse.fileKey;
    } else if (state.processorClassification === "Controller") {
      settings.gdprControllerGuaranteeFileName = processorAgreementResponse.fileName;
      settings.gdprControllerGuaranteeFileKey = processorAgreementResponse.fileKey;
    }
  }

  if (deletedProcessorUpload) {
    settings.gdprProcessorGuaranteeFileName = null;
    settings.gdprProcessorGuaranteeFileKey = null;
  }

  if (deletedControllerUpload) {
    settings.gdprControllerGuaranteeFileName = null;
    settings.gdprControllerGuaranteeFileKey = null;
  }

  if (sccDocumentationResponse) {
    settings.gdprSccFileName = sccDocumentationResponse.fileName;
    settings.gdprSccFileKey = sccDocumentationResponse.fileKey;
  }

  if (deletedSccDocumentation) {
    settings.gdprSccFileName = null;
    settings.gdprSccFileKey = null;
  }

  await updateVendor({
    vendorId: vendor.vendorId,
    vendorSettings: settings,
  });

  // 4 - calculate new survey answers & save
  const intentionalInteractionVendors = getSelectedIntentionalInteractionVendors(answers);
  const directSellingVendors = getSelectedDirectSellingVendors(answers);

  const questions = [];

  updateVendorToSurveyResponse(
    intentionalInteractionVendors,
    vendor,
    state.isIntentionalInteraction,
    "vendor-intentional-disclosure-selection",
    questions,
  );

  updateVendorToSurveyResponse(
    directSellingVendors,
    vendor,
    state.ccpaIsDirectSelling,
    "vendor-upload-data-selection",
    questions,
  );

  // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
  // if (vendor && isFacebookAds(vendor)) {
  //   updateBooleanResponse("facebook-ldu", state.facebookLdu, questions);
  // }

  // if (vendor && isGoogleAds(vendor)) {
  //   updateBooleanResponse("google-rdp", state.googleRdp, questions);
  // }

  if (vendor && isGoogleAnalytics(vendor)) {
    updateBooleanResponse("google-analytics-data-sharing", state.googleAnalytics, questions);
  }

  if (questions.length > 0) {
    await updateSurveyQuestions({ questions });
  }
};

const updateBooleanResponse = (slug: string, response: boolean, questions: any) => {
  questions.push({ slug: slug, answer: response ? "true" : "false" });
};

const updateVendorToSurveyResponse = (
  existing: string[],
  vendor: OrganizationDataRecipientDto,
  include: boolean,
  slug: string,
  questions,
) => {
  const answerArrayIndex = existing.findIndex((x) => x === vendor.id);

  let updated = false;
  if (!include && answerArrayIndex != -1) {
    updated = true;
    existing.splice(answerArrayIndex, 1);
  }

  if (include && answerArrayIndex === -1) {
    updated = true;
    existing.push(vendor.id);
  }

  if (updated) {
    questions.push({ slug, answer: JSON.stringify(existing) });
  }
};

const CLASSIFICATION_OPTIONS: VendorRadioOption<ClassificationType>[] = [
  {
    label: "Service Provider",
    value: "service-provider",
    type: "file",
    key: "service-provider",
  },
  {
    label: "Third Party",
    value: "third-party",
    type: "normal",
    key: "third-party",
  },
  {
    label: "Contractor",
    value: "contractor",
    type: "file",
    key: "contractor",
  },
];

export const useDataRecipientDisclosureSettings = (
  vendor: OrganizationDataRecipientDto | VendorDto,
  answers: Record<string, string> | null,
  custom: boolean,
  customCategory?: string,
) => {
  const [state, setState] = useState<DisclosureState>(initialStateFrom(null, null));

  const meta = useMemo(
    () => calculateDisclosureMeta(vendor, state, custom, customCategory),
    [vendor, state, custom, customCategory],
  );

  const updateState = useCallback((value: Partial<DisclosureState>) => {
    setState((prev) => ({ ...prev, ...value }));
  }, []);

  // calculate our initial state
  useEffect(() => {
    setState((prev) => ({ ...prev, ...initialStateFrom(vendor, answers) }));
  }, [vendor, answers]);

  // auto-lock editing
  useEffect(() => {
    if (!state.ccpaIsSelling && state.ccpaIsDirectSelling) {
      updateState({ ccpaIsDirectSelling: false });
    }
  }, [state.ccpaIsSelling, state.ccpaIsDirectSelling, updateState]);

  // auto set ccpa is selling
  useEffect(() => {
    if (state.isIntentionalInteraction && state.ccpaIsSelling !== false) {
      updateState({ ccpaIsSelling: false });
    }
  }, [state.ccpaIsSelling, state.isIntentionalInteraction, updateState]);

  // auto-set classification
  useEffect(() => {
    if (state.classification != "third-party" && meta.lockClassificationThirdParty) {
      updateState({
        classification: "third-party",
        publicTosUrl: null,
        serviceProviderFile: null,
        serviceProviderFilename: null,
      });
    }
  }, [state.classification, meta.lockClassificationThirdParty, updateState]);

  return { state, meta, updateState };
};

export const DataRecipientDisclosureSettings: React.FC<VendorClassificationEditProps> = ({
  org,
  vendor,
  isThirdParty,
  state,
  meta,
  updateState,
}) => {
  const processorOptions = useMemo(() => {
    const baseOptions = _.clone(PROCESSOR_CLASSIFICATION_OPTIONS);
    const optionsMap = _.keyBy(baseOptions, "key");

    if (state.processorClassification === "Unknown") {
      optionsMap["unknown"].disabled = true;
      optionsMap["unknown"].visible = true;
    } else {
      optionsMap["unknown"].visible = false;
    }

    return baseOptions;
  }, [state.processorClassification]);

  if (!meta) {
    return null;
  }

  return (
    <div className="mt-xl">
      {meta.showClassification && (
        <section key={"classification"} className="flex-row mb-xl">
          <header className="flex-grow">
            <p className="text-component-question">
              {isThirdParty ? "Classification: Third Party" : "Classification"}
            </p>
            <ServiceProviderLanguage
              vendor={vendor}
              isThirdPartyRecipient={isThirdParty}
              meta={meta}
            />

            {!isThirdParty && (
              <VendorRadioSection
                options={CLASSIFICATION_OPTIONS}
                value={state.classification}
                url={state.publicTosUrl ?? ""}
                fileName={state.serviceProviderFilename ?? ""}
                fileKey={state.serviceProviderFilename ?? ""}
                onSelectionChange={async (o: ClassificationType) =>
                  updateState({ classification: o })
                }
                onUrlChange={async (publicTosUrl) => updateState({ publicTosUrl })}
                hideDocumentationInput={!meta.showServiceProviderDocumentationInput}
                onFileUpload={async (fileName, fileData) =>
                  updateState({ serviceProviderFile: fileData, serviceProviderFilename: fileName })
                }
                onFileDelete={async () =>
                  updateState({ serviceProviderFile: null, serviceProviderFilename: null })
                }
                disabled={meta.lockClassificationThirdParty}
              />
            )}
          </header>
        </section>
      )}

      {/* POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023) */}
      {/* <FacebookLduQuestion
        org={org}
        vendor={vendor}
        isThirdPartyRecipient={isThirdParty}
        state={state}
        update={updateState}
        meta={meta}
      />
      <GoogleRdpQuestion
        org={org}
        vendor={vendor}
        isThirdPartyRecipient={isThirdParty}
        state={state}
        update={updateState}
        meta={meta}
      /> */}

      <IntentionalInteractionQuestion
        org={org}
        vendor={vendor}
        isThirdPartyRecipient={isThirdParty}
        state={state}
        update={updateState}
        meta={meta}
      />
      <SharingQuestion
        org={org}
        vendor={vendor}
        isThirdPartyRecipient={isThirdParty}
        state={state}
        update={updateState}
        meta={meta}
      />
      <GoogleAnalyticsQuestion
        org={org}
        vendor={vendor}
        isThirdPartyRecipient={isThirdParty}
        state={state}
        update={updateState}
        meta={meta}
      />
      <SellingQuestion
        org={org}
        vendor={vendor}
        isThirdPartyRecipient={isThirdParty}
        state={state}
        update={updateState}
        meta={meta}
      />
      <DirectSellingQuestion
        org={org}
        vendor={vendor}
        isThirdPartyRecipient={isThirdParty}
        state={state}
        update={updateState}
        meta={meta}
      />
      <CustomAudienceQuestion
        org={org}
        vendor={vendor}
        isThirdPartyRecipient={isThirdParty}
        state={state}
        update={updateState}
        meta={meta}
      />

      {org.featureGdpr && (
        <>
          <section key={"processor-classification"} className="flex-row mb-xl">
            <header className="flex-grow">
              <p className="text-component-question">Processor Classification</p>

              <div className="text-component-help">
                <ProcessorLanguage vendor={vendor} meta={meta} />
              </div>

              <VendorRadioSection
                options={processorOptions}
                value={state.processorClassification}
                url={state.processorGuaranteeUrl ?? ""}
                fileName={state.processorGuaranteeFilename ?? ""}
                fileKey={state.processorGuaranteeFilename ?? ""}
                onSelectionChange={async (o: GDPRProcessorRecommendation) =>
                  updateState({ processorClassification: o })
                }
                onUrlChange={async (processorGuaranteeUrl) =>
                  updateState({ processorGuaranteeUrl })
                }
                hideDocumentationInput={!meta.showServiceProviderDocumentationInput}
                onFileUpload={async (fileName, fileData) =>
                  updateState({
                    processorGuaranteeFile: fileData,
                    processorGuaranteeFilename: fileName,
                  })
                }
                onFileDelete={async () =>
                  updateState({ processorGuaranteeFile: null, processorGuaranteeFilename: null })
                }
              />
            </header>
          </section>

          {meta.showSccDocumentation && (
            <section key={"international-data-transfer"} className="flex-row mb-xl">
              <header className="flex-grow">
                <p className="text-component-question">International Data Transfer</p>

                <div className="text-component-help">
                  {meta.lockSccDocumentation ? (
                    <Paras>
                      <Para>
                        Based on our research, the {vendor.name} has incorporated SCCs into their
                        documentation.
                      </Para>
                    </Paras>
                  ) : (
                    <Paras>
                      <Para>
                        International data transfer rules apply to {vendor.name}. Data transfers to{" "}
                        {vendor.name} are considered “safe” under GDPR only if {vendor.name} agrees
                        to certain contractual garantees regarding its processing of consumer data.
                        These are referred to as{" "}
                        <strong>Standard Contractual Clauses (SCCs)</strong>. Add a link to{" "}
                        {vendor.name}’s SCCs online, or upload an agreement with the clauses.{" "}
                        <ExternalLink
                          href="https://help.truevault.com/article/164-standard-contractual-clauses"
                          target="_blank"
                        >
                          Read more about SCCs and how to look for them.
                        </ExternalLink>
                      </Para>
                    </Paras>
                  )}
                </div>

                <VendorRadioSection
                  options={SCC_OPTIONS}
                  value={state.gdprHasSccSetting ? "selected" : "not-selected"}
                  url={state.gdprSccUrl ?? ""}
                  fileName={state.gdprSccFilename ?? ""}
                  fileKey={state.gdprSccFilename ?? ""}
                  onSelectionChange={async (o: VendorFlagOption) => {
                    if (o === "selected") {
                      updateState({ gdprHasSccSetting: true });
                    } else {
                      updateState({
                        gdprHasSccSetting: false,
                        gdprSccFile: null,
                        gdprSccFilename: null,
                        gdprSccUrl: null,
                      });
                    }
                  }}
                  onUrlChange={async (gdprSccUrl) => updateState({ gdprSccUrl })}
                  hideDocumentationInput={!meta.showServiceProviderDocumentationInput}
                  onFileUpload={async (fileName, fileData) =>
                    updateState({
                      gdprSccFile: fileData,
                      gdprSccFilename: fileName,
                    })
                  }
                  onFileDelete={async () =>
                    updateState({ gdprSccFile: null, gdprSccFilename: null })
                  }
                  disabled={meta.lockSccDocumentation}
                />
              </header>
            </section>
          )}
        </>
      )}
    </div>
  );
};

type QuestionProps = {
  org: OrganizationDto;
  vendor: OrganizationDataRecipientDto | VendorDto;
  isThirdPartyRecipient: boolean;
  state: DisclosureState;
  meta: DisclosureMeta;
  update: (update: Partial<DisclosureState>) => void;
};

const IntentionalInteractionQuestion: React.FC<QuestionProps> = ({
  vendor,
  isThirdPartyRecipient,
  state,
  update,
  meta,
}) => {
  if (!meta.showIntInteraction) {
    return null;
  }
  return (
    <VendorBooleanQuestion
      question={`Consumers interact with ${vendorName(
        vendor,
        isThirdPartyRecipient,
      )} only as a result of an intentional interaction`}
      condensed={false}
      helpText={
        <div className="text-t1">
          An intentional interaction is when the third party is visible and named on your website or
          application, and the consumer is choosing to interact with and send their data to that
          specific party.
          <br /> <br />
          For example, clicking a "Pay with PayPal" button, choosing FedEx as a shipping option, or
          clicking "share my information with local contractors."
        </div>
      }
      value={state.isIntentionalInteraction}
      onChange={(newVal) => update({ isIntentionalInteraction: newVal })}
    />
  );
};

const SharingQuestion: React.FC<QuestionProps> = ({
  org,
  vendor,
  isThirdPartyRecipient,
  state,
  update,
  meta,
}) => {
  if (!meta.showSharing) {
    return null;
  }
  return (
    <VendorBooleanQuestion
      question={`${org.name} "shares" personal information with ${vendorName(
        vendor,
        isThirdPartyRecipient,
      )}`}
      condensed={false}
      helpText={
        <div className="text-t1">
          “Sharing” means providing personal information to ad networks for purposes of behavioral,
          or interest-based advertising.
        </div>
      }
      value={state.ccpaIsSharing}
      onChange={(newVal) => update({ ccpaIsSharing: newVal })}
    />
  );
};

const SellingQuestion: React.FC<QuestionProps> = ({
  org,
  vendor,
  isThirdPartyRecipient,
  state,
  update,
  meta,
}) => {
  if (!meta.showSelling) {
    return null;
  }
  return (
    <VendorBooleanQuestion
      question={`${org.name} "sells" personal information to ${vendorName(
        vendor,
        isThirdPartyRecipient,
      )}`}
      disabled={meta.lockCcpaIsSelling}
      disabledTooltip="Intentional interactions are excluded from the definition of “selling”"
      condensed={false}
      nextIndented={true}
      helpText={
        <div className="text-t1">
          "Selling" means providing personal data in exchange for money or anything else of value.
        </div>
      }
      value={state.ccpaIsSelling}
      onChange={(newVal) => update({ ccpaIsSelling: newVal })}
    />
  );
};

const DirectSellingQuestion: React.FC<QuestionProps> = ({
  org,
  vendor,
  isThirdPartyRecipient,
  state,
  update,
  meta,
}) => {
  if (!meta.showSelling) {
    return null;
  }
  return (
    <VendorBooleanQuestion
      indented={true}
      disabled={!state.ccpaIsSelling}
      disabledTooltip=""
      question={`${org.name} manually uploads, transmits, or give ${vendorName(
        vendor,
        isThirdPartyRecipient,
      )} access to consumer data`}
      condensed={false}
      helpText={
        <div className="text-t1">
          Answer "no" if they receive data only through automated means on your website, such as a
          cookie.
        </div>
      }
      value={state.ccpaIsDirectSelling}
      onChange={(newVal) => update({ ccpaIsDirectSelling: newVal })}
    />
  );
};

// Do we want these questions to be defined separate from Get Compliant?
const CustomAudienceQuestion: React.FC<QuestionProps> = ({
  org,
  vendor,
  isThirdPartyRecipient,
  state,
  update,
  meta,
}) => {
  if (!meta.showCustomAudience) {
    return null;
  }
  return (
    <VendorBooleanQuestion
      indented={false}
      disabled={!meta.showCustomAudience}
      question={`${org.name} uses the "Custom Audience" feature for ${vendorName(
        vendor,
        isThirdPartyRecipient,
      )}`}
      condensed={false}
      helpText={
        <div className="text-t1">
          <ExternalLink
            className="bold-link"
            href="https://www.facebook.com/business/help/170456843145568?id=2469097953376494"
          >
            Custom Audience
          </ExternalLink>{" "}
          is a service offered by most ad networks in which a business can upload a list of their
          customers or contacts, and the ad network will either deliver ads directly to those people
          and/or use it to create a "look-alike" audience.
        </div>
      }
      value={state.usesCustomAudience}
      onChange={(newVal) => update({ usesCustomAudience: newVal })}
    />
  );
};

// POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
// const FacebookLduQuestion: React.FC<QuestionProps> = ({
//   org,
//   vendor,
//   isThirdPartyRecipient,
//   state,
//   update,
//   meta,
// }) => {
//   if (!meta.showFacebookLdu) {
//     return null;
//   }

//   return (
//     <VendorBooleanQuestion
//       question={`${org.name} will enable LDU for ${vendorName(vendor, isThirdPartyRecipient)}`}
//       condensed={false}
//       helpText={<FacebookLduHelp />}
//       value={state.facebookLdu}
//       onChange={(newVal) => update({ facebookLdu: newVal })}
//     />
//   );
// };

export const FacebookLduHelp = () => {
  return (
    <div className="text-t1">
      Enabling “Limited Data Use” (LDU) in your Facebook Ads configuration will "turn off"
      personalized advertising for California consumers. If personalized advertising is disabled for
      all of your ad networks, your business will not be required to post the "Do Not Share My
      Personal Information" link.{" "}
      <a
        href="https://www.facebook.com/business/help/1151133471911882"
        target="_blank"
        rel="noopener noreferrer"
        className="text-medium text-link"
      >
        How to enable LDU
      </a>
    </div>
  );
};

// POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
// const GoogleRdpQuestion: React.FC<QuestionProps> = ({
//   org,
//   vendor,
//   isThirdPartyRecipient,
//   state,
//   update,
//   meta,
// }) => {
//   if (!meta.showGoogleRdp) {
//     return null;
//   }
//   return (
//     <VendorBooleanQuestion
//       question={`${org.name} will enable RDP for ${vendorName(vendor, isThirdPartyRecipient)}`}
//       condensed={false}
//       helpText={<GoogleRdpHelp />}
//       value={state.googleRdp}
//       onChange={(newVal) => update({ googleRdp: newVal })}
//     />
//   );
// };

export const GoogleRdpHelp = () => {
  return (
    <div className="text-t1">
      Enabling “Restricted Data Processing” (RDP) in your Google Ads configuration will "turn off"
      personalized advertising for California consumers. If personalized advertising is disabled for
      all of your ad networks, your business will not be required to post the "Do Not Share My
      Personal Information" link. Follow the{" "}
      <a
        href={`https://support.google.com/google-ads/answer/9606827#zippy=%2Cedit-your-global-site-tag-to-disable-ad-personalization-signals`}
        target="_blank"
        rel="noopener noreferrer"
        className="text-medium text-link"
      >
        instructions to enable RDP
      </a>{" "}
      and be sure to update your global site tag.
    </div>
  );
};

const GoogleAnalyticsQuestion: React.FC<QuestionProps> = ({
  org,
  vendor,
  isThirdPartyRecipient,
  state,
  update,
  meta,
}) => {
  if (!meta.showGoogleAnalytics) {
    return null;
  }

  return (
    <VendorBooleanQuestion
      question={`Has ${org.name} turned off (or will it turn off) data sharing in ${vendorName(
        vendor,
        isThirdPartyRecipient,
      )}?`}
      condensed={false}
      helpText={<GoogleAnalyticsHelp />}
      value={state.googleAnalytics}
      onChange={(newVal) => update({ googleAnalytics: newVal, ccpaIsSelling: !newVal })}
    />
  );
};

export const GoogleAnalyticsHelp = () => {
  return (
    <div className="text-t1">
      By default, Google accesses consumer data (for example, data collected from website visitors)
      to enhance its services. If your business turns off data sharing, Google will no longer access
      that data. If data sharing is on, you will be required to post a “Do Not Sell My Personal
      Information” link on your website.{" "}
      <a
        href={`https://support.google.com/analytics/answer/1011397?hl=en&ref_topic=2919631`}
        target="_blank"
        rel="noopener noreferrer"
        className="text-medium text-link"
      >
        How to turn off data sharing in Google Analytics
      </a>
    </div>
  );
};

type ServiceProviderLanguageProps = {
  isThirdPartyRecipient: boolean;
  meta: DisclosureMeta;
  vendor: OrganizationDataRecipientDto | VendorDto;
};

const ServiceProviderLanguage: React.FC<ServiceProviderLanguageProps> = ({
  isThirdPartyRecipient,
  vendor,
  meta,
}) => {
  let content = null;

  if (isThirdPartyRecipient) {
    return null;
  }

  if (meta.lockClassificationThirdParty) {
    content = <LockedThirdPartyServiceProviderLanguage />;
  } else if (vendor.serviceProviderLanguageUrl) {
    content = (
      <ExistingServiceProviderLanguage
        vendor={vendor}
        isThirdPartyRecipient={isThirdPartyRecipient}
      />
    );
  } else {
    content = (
      <NoExistingServiceProviderLanguage
        vendor={vendor}
        isThirdPartyRecipient={isThirdPartyRecipient}
      />
    );
  }

  return (
    <>
      <p className="text-component-help">{content}</p>
      <ServiceProviderDocumentationLink
        vendor={vendor}
        isThirdPartyRecipient={isThirdPartyRecipient}
      />
    </>
  );
};

type ExistingServiceProviderLanguageProps = {
  vendor: OrganizationDataRecipientDto | VendorDto;
  isThirdPartyRecipient: boolean;
};

const ExistingServiceProviderLanguage: React.FC<ExistingServiceProviderLanguageProps> = ({
  vendor,
  isThirdPartyRecipient,
}) => {
  const statusLink = (
    <a
      className="text-weight-regular ml-xs"
      href="https://help.truevault.com/article/154-identifying-service-provider-language"
      target="_blank"
      rel="noopener noreferrer"
    >
      Learn more about determining service provider status
    </a>
  );

  if (vendor.serviceProviderRecommendation == "SERVICE_PROVIDER") {
    return (
      <>
        We have found documentation to support classifying this vendor as a service provider to your
        business. Review the documentation and change the selection if needed.
        {statusLink}
      </>
    );
  }

  return (
    <>
      We have reviewed {vendorName(vendor, isThirdPartyRecipient)}'s terms of service and have not
      found service provider language. Review the documentation and change the selection if needed.
      If you can find the service provider language, mark the vendor as a Service Provider.
      Otherwise, leave them as a Third Party. {statusLink}
    </>
  );
};

const LockedThirdPartyServiceProviderLanguage = () => {
  const statusLink = (
    <a
      className="text-weight-regular ml-xs"
      href="https://help.truevault.com/article/154-identifying-service-provider-language"
      target="_blank"
      rel="noopener noreferrer"
    >
      Learn more about determining service provider status
    </a>
  );

  return (
    <>
      Based on your Vendor Settings, this recipient is a Third party to your business. {statusLink}
    </>
  );
};

type NoExistingServiceProviderLanguageProps = {
  vendor: OrganizationDataRecipientDto | VendorDto;
  isThirdPartyRecipient: boolean;
};
const NoExistingServiceProviderLanguage: React.FC<NoExistingServiceProviderLanguageProps> = ({
  vendor,
  isThirdPartyRecipient,
}) => (
  <>
    We have not found a public service provider statement for{" "}
    {vendorName(vendor, isThirdPartyRecipient)}.
    <br />
    <br />
    Locate {vendorName(vendor, isThirdPartyRecipient)}’s terms of service in their public Terms of
    Service online, or in a contract with your business. If you can find service provider language,
    mark the vendor as a Service Provider. Otherwise, select Third Party.
    <a
      className="text-weight-regular ml-xs"
      href="https://help.truevault.com/article/154-identifying-service-provider-language"
      target="_blank"
      rel="noopener noreferrer"
    >
      Learn more about determining service provider status
    </a>
  </>
);

const ServiceProviderDocumentationLink: React.FC<NoExistingServiceProviderLanguageProps> = ({
  vendor,
  isThirdPartyRecipient,
}) => {
  if (!vendor.serviceProviderLanguageUrl) {
    return null;
  }

  if (
    vendor.serviceProviderRecommendation != "SERVICE_PROVIDER" &&
    vendor.serviceProviderRecommendation != "THIRD_PARTY"
  ) {
    return null;
  }

  const label =
    vendor.serviceProviderRecommendation == "SERVICE_PROVIDER"
      ? `${vendorName(vendor, isThirdPartyRecipient)} service provider documentation`
      : `${vendorName(vendor, isThirdPartyRecipient)} terms of service`;

  return (
    <div className="mb-md">
      <a
        className="text-weight-regular text-decoration-none"
        target="_blank"
        rel="noopener noreferrer"
        href={vendor.serviceProviderLanguageUrl}
      >
        {label}
      </a>
    </div>
  );
};

type ProcessorLanguageProps = {
  meta: DisclosureMeta;
  vendor: OrganizationDataRecipientDto | VendorDto;
};

const ProcessorLanguage: React.FC<ProcessorLanguageProps> = ({ vendor, meta }) => {
  if (meta.recommendClassificationProcessor) {
    return (
      <Paras>
        <Para>
          To be considered a "processor" under GDPR, a vendor must have sufficient guarantees in
          place about the security of its data processing and must adhere to an approved code of
          conduct. Otherwise, the vendor is a “controller” of personal data it handles in relation
          to your business.
        </Para>
        <Para>
          We’ve automatically designated {vendor.name} as "Processor" based on our research.
        </Para>
      </Paras>
    );
  } else if (meta.recommendClassificationController) {
    return (
      <Paras>
        <Para>
          We've automatically designated {vendor.name} as "Controller" based on our research.
          <strong>
            Note that your Data Recipients who are Controllers will be disclosed by name in your
            Privacy Policy.
          </strong>
        </Para>
      </Paras>
    );
  } else {
    return (
      <Paras>
        <Para>
          We are unable to make a Processor or Controller recommendation for {vendor.name}
        </Para>
      </Paras>
    );
  }
};
