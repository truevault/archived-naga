import { Divider } from "@mui/material";
import clsx from "clsx";
import React, { ReactNode } from "react";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { DataRecipientLogo } from "../../DataRecipientLogo";

type DataRecipientRowProps = {
  recipient: OrganizationDataRecipientDto;
  annotation?: ReactNode;
  topBorder?: boolean;
  bottomBorder?: boolean;
  condensed?: boolean;
  className?: string;
  logoProps?: React.ComponentProps<typeof DataRecipientLogo>;
  justifyChildrenEnd?: Boolean;
  stackChildren?: Boolean;
};

export const DataRecipientRow: React.FC<DataRecipientRowProps> = ({
  recipient,
  annotation,
  children,
  className,
  logoProps = {},
  topBorder = true,
  bottomBorder = false,
  condensed = false,
  justifyChildrenEnd = false,
}) => {
  return (
    <>
      {topBorder && <Divider className={clsx({ "my-sm": condensed, "my-mdlg": !condensed })} />}

      <div key={recipient.vendorId} className={clsx("flex-row align-items-center", className)}>
        <DataRecipientLogo
          dataRecipient={recipient}
          size={32}
          {...logoProps}
          className={clsx(logoProps.className, "mr-mdlg")}
        />

        <div className="w-256">
          <div className="text-t3 text-weight-bold">{recipient.name}</div>
          {Boolean(annotation) && <div>{annotation}</div>}
        </div>

        <div
          className={clsx("flex-grow", {
            "flex-container": justifyChildrenEnd,
            "flex-end": justifyChildrenEnd,
          })}
        >
          {children}
        </div>
      </div>

      {bottomBorder && <Divider className={clsx({ "my-sm": condensed, "my-md": !condensed })} />}
    </>
  );
};
