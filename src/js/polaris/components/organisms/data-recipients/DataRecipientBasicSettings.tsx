import React, { useCallback, useEffect, useState } from "react";
import { Api } from "../../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { CustomVendorFormValues } from "../../../pages/get-compliant/data-recipients/AddDataRecipients";
import { CustomFields } from "../../../pages/stay-compliant/CustomFields";
import { getVendorSettingsObject } from "../../../util/vendors";

export const saveBasicUpdates = async (
  org: OrganizationDto,
  vendor: OrganizationDataRecipientDto,
  state: BasicState,
) => {
  // @ts-ignore
  const current = getVendorSettingsObject(vendor);
  Object.assign(current, state);

  await Api.organizationVendor.addOrUpdateVendor(org.id, vendor.vendorId, current);
};

type BasicState = CustomVendorFormValues;

interface VendorRetentionEditProps {
  orgVendors: OrganizationDataRecipientDto[];
  vendors: VendorDto[];
  isThirdParty: boolean;
  state: BasicState;
  updateState: (update: Partial<BasicState>) => void;
}

export const useDataRecipientBasicSettings = (initial: BasicState) => {
  const [state, setState] = useState<BasicState | null>(initial);
  const updateState = useCallback((values: Partial<BasicState>) => {
    setState((lastState) => ({ ...lastState, ...values }));
  }, []);

  useEffect(() => {
    updateState(initial);
  }, [initial, updateState]);

  return { state, updateState };
};

export const DataRecipientBasicSettings: React.FC<VendorRetentionEditProps> = ({
  orgVendors,
  vendors,
  isThirdParty,
  state,
  updateState,
}) => {
  const setCustomFields = useCallback(
    (values) => {
      updateState(values);
    },
    [updateState],
  );

  return (
    <div className="mt-xl">
      <div className="text-t5 text-weight-bold color-primary-700 mb-md">Basic Information</div>

      <CustomFields
        enabledVendors={orgVendors}
        vendors={vendors}
        onValuesChanged={setCustomFields}
        isThirdParty={isThirdParty}
        // @ts-ignore
        initialValues={state}
      />
    </div>
  );
};
