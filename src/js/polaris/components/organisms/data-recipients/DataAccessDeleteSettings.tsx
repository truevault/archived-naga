import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import clsx from "clsx";
import React, { useState } from "react";
import {
  DataAccessibility,
  DataDeletability,
  OrganizationDataRecipientDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Disableable } from "../../Disableable";
import { RadioOption } from "./DataRecipientRadioOptions";
import { UpdateVendorFn } from "../../../hooks/get-compliant/vendors/useUpdateVendor";
import { InlineLoading } from "../../../../common/components/Loading";
import { DataRecipientLogo } from "../../DataRecipientLogo";

export const DATA_ACCESS_OPTIONS: RadioOption<DataAccessibility>[] = [
  {
    value: "ACCESSIBLE",
    label: "Yes",
  },
  {
    value: "NOT_ACCESSIBLE",
    label: "No",
  },
  {
    value: "UNKNOWN",
    label: "Not Sure",
  },
];

export const DATA_DELETE_OPTIONS: RadioOption<DataDeletability>[] = [
  {
    value: "DELETABLE",
    label: "Yes",
  },
  {
    value: "NOT_DELETABLE",
    label: "No",
  },
  {
    value: "UNKNOWN",
    label: "Not Sure",
  },
];

export const DataAccessDeleteSettings: React.FC<{
  dataRecipients: OrganizationDataRecipientDto[];
  unselectedAdNetworks: string[];
  updateVendor: UpdateVendorFn;
}> = ({ dataRecipients, unselectedAdNetworks, updateVendor }) => {
  return (
    <div>
      {dataRecipients.map((dr) => (
        <DataRecipientRadioOptionGroup
          key={dr.id}
          recipient={dr}
          updateVendor={updateVendor}
          unselectedAdNetworks={unselectedAdNetworks}
        />
      ))}
    </div>
  );
};

const enableDeletability = (accessibility: DataAccessibility): boolean =>
  ["ACCESSIBLE", "UNKNOWN"].includes(accessibility);
const DataRecipientRadioOptionGroup: React.FC<{
  recipient: OrganizationDataRecipientDto;
  unselectedAdNetworks: string[];
  updateVendor: UpdateVendorFn;
}> = ({ recipient, unselectedAdNetworks, updateVendor }) => {
  const [loadingAccessibility, setLoadingAccessibility] = useState(false);
  const [accessibility, setAccessibility] = useState(recipient.dataAccessibility);

  const [loadingDeletablility, setLoadingDeletablility] = useState(false);
  const [deletability, setDeletability] = useState(recipient.dataDeletability);

  const [showDeletability, setShowDeletability] = useState(
    enableDeletability(recipient.dataAccessibility),
  );

  const onAccessChange = async (newValue: DataAccessibility) => {
    setShowDeletability(enableDeletability(accessibility));
    setAccessibility(newValue);
    setLoadingAccessibility(true);
    try {
      const updated = await updateVendor({
        vendorId: recipient.vendorId,
        dataAccessibility: newValue,
      });
      setShowDeletability(enableDeletability(updated.dataAccessibility));
      setAccessibility(updated.dataAccessibility);
      setDeletability(updated.dataDeletability);
    } finally {
      setLoadingAccessibility(false);
    }
  };

  const onDeleteChange = async (newValue: DataDeletability) => {
    setDeletability(newValue);
    setLoadingDeletablility(true);
    try {
      const updated = await updateVendor({
        vendorId: recipient.vendorId,
        dataDeletability: newValue,
        // NOTE: need to also include current accessibility value due to potential race conditions
        //       between the state of the UI and the state of the re-fetched org vendor data
        dataAccessibility: accessibility,
      });
      setAccessibility(updated.dataAccessibility);
      setDeletability(updated.dataDeletability);
    } finally {
      setLoadingDeletablility(false);
    }
  };

  return (
    <div
      className={clsx("data-recipient-radio-options", "data-recipient-radio-options--multi-group")}
    >
      <div className={"data-recipient-radio-options__label"}>
        <div
          className={clsx(
            "data-recipient-radio-options__logo",
            "data-recipient-radio-options__logo--multi-group",
          )}
        >
          <DataRecipientLogo dataRecipient={recipient} size={24} />
        </div>
        <div className="data-recipient-radio-options__info">
          <div className="text-t3 text-weight-medium text-truncate-ellipsis">{recipient.name}</div>
        </div>
      </div>
      <RadioOptions
        options={DATA_ACCESS_OPTIONS}
        value={accessibility}
        onChange={onAccessChange}
        description={`Does your business have access to consumer data stored with or sent to ${recipient.name}?`}
        loading={loadingAccessibility}
        disabled={
          Boolean(recipient.accessInstructions && !unselectedAdNetworks.includes(recipient.id)) ||
          loadingDeletablility
        }
        disabledTooltip={!loadingDeletablility && "See vendor's documentation in the table below"}
      />
      {showDeletability && (
        <RadioOptions
          options={DATA_DELETE_OPTIONS}
          value={deletability}
          onChange={onDeleteChange}
          loading={loadingDeletablility}
          disabled={loadingAccessibility}
          description={`Can your business delete consumer data stored in ${recipient.name}?`}
        />
      )}
    </div>
  );
};

const RadioOptions: React.FC<{
  value: string;
  options: RadioOption[];
  onChange: (a: string) => void;
  description: string;
  loading: boolean;
  disabled?: boolean;
  disabledTooltip?: string;
}> = ({ value, options, onChange, description, loading, disabled, disabledTooltip }) => {
  return (
    <>
      {description && <div className="mt-xxs text-component-help">{description}</div>}
      <RadioGroup
        value={value || ""}
        onChange={({ target }) => onChange(target.value)}
        style={{ marginTop: -4 }}
        className={`form-group--row`}
      >
        {options.map((opt) => {
          const labelValue = opt.label || opt.value;
          return (
            <Disableable
              disabled={disabled}
              disabledTooltip={disabledTooltip || ""}
              key={opt.value}
            >
              <FormControlLabel
                value={opt.value}
                control={
                  loading && opt.value == value ? (
                    <InlineLoading size="sm" className="mr-sm" style={{ marginLeft: 15 }} />
                  ) : (
                    <Radio color="primary" disabled={disabled || loading} />
                  )
                }
                label={labelValue}
              />
            </Disableable>
          );
        })}
      </RadioGroup>
    </>
  );
};
