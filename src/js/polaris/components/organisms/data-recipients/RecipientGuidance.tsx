import React from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Callout, CalloutVariant } from "../../../components/Callout";

type RecipientGuidanceProps = {
  recipient: OrganizationDataRecipientDto;
  collectionGroup?: CollectionGroupDetailsDto;
};
export const RecipientGuidance: React.FC<RecipientGuidanceProps> = ({
  recipient,
  collectionGroup,
}) => {
  if (!recipient.generalGuidance && !recipient.specialCircumstancesGuidance) {
    return null;
  }

  if (collectionGroup?.collectionGroupType == "EMPLOYMENT") {
    return null;
  }

  return (
    <>
      {Boolean(recipient.generalGuidance) && (
        <Callout
          className="mb-md"
          variant={CalloutVariant.Purple}
          __dangerousChildrenHtml={recipient.generalGuidance}
        />
      )}

      {Boolean(recipient.specialCircumstancesGuidance) && (
        <Callout
          className="mb-md"
          variant={CalloutVariant.Yellow}
          __dangerousChildrenHtml={recipient.specialCircumstancesGuidance}
        />
      )}
    </>
  );
};
