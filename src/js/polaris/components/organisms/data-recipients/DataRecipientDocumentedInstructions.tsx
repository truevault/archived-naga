import { Button } from "@mui/material";
import React, { useMemo } from "react";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { DataRecipientRow } from "./DataRecipientRow";

export const DataRecipientDocumentedInstructions: React.FC<{
  dataRecipients: OrganizationDataRecipientDto[];
}> = ({ dataRecipients }) => (
  <div>
    {dataRecipients.map((dr) => (
      <DataRecipientDocumentedInstruction key={dr.id} dataRecipient={dr} />
    ))}
  </div>
);

const DataRecipientDocumentedInstruction: React.FC<{
  dataRecipient: OrganizationDataRecipientDto;
}> = ({ dataRecipient: dr }) => {
  const accessInstructionsUrl = useMemo(() => {
    // regex looks for href=", and 1+ running characters that are not whitespace follow by "
    // takes first link found in the instructions
    return dr?.accessInstructions?.match('href="([^\\s]+)"')?.at(1);
  }, [dr?.accessInstructions]);
  const deletionInstructionsUrl = useMemo(() => {
    return dr?.deletionInstructions?.match('href="([^\\s]+)"')?.at(1);
  }, [dr?.deletionInstructions]);

  return (
    <DataRecipientRow
      key={dr.id}
      recipient={dr}
      justifyChildrenEnd={true}
      topBorder={false}
      bottomBorder={true}
    >
      {accessInstructionsUrl && (
        <Button href={accessInstructionsUrl} color="secondary" target="_blank">
          View Access Instructions
        </Button>
      )}
      {deletionInstructionsUrl && (
        <Button
          className="ml-mdlg"
          href={deletionInstructionsUrl}
          target="_blank"
          color="secondary"
        >
          View Deletion Instructions
        </Button>
      )}
    </DataRecipientRow>
  );
};
