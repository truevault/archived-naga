import { Checkbox, FormControlLabel, FormGroup, FormHelperText, FormLabel } from "@mui/material";
import React from "react";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { useNonEmploymentCollectionGroups } from "../../../hooks/useCollectionGroups";

export const CollectionGroupSelect: React.FC<{
  label?: string;
  helpText?: string;
  onChange: (selectedIds: string[]) => void;
  selectedGroupIds: string[];
}> = ({ label, helpText, onChange, selectedGroupIds }) => {
  const organizationId = usePrimaryOrganizationId();

  const [collectionGroups, collectionGroupsRequest] =
    useNonEmploymentCollectionGroups(organizationId);

  const handleSelect = (selectedId: string) => {
    if (selectedGroupIds.includes(selectedId)) {
      onChange(selectedGroupIds.filter((id) => id !== selectedId));
    } else {
      onChange([...selectedGroupIds, selectedId]);
    }
  };

  if (collectionGroupsRequest.running) {
    return null;
  }

  return (
    <FormGroup>
      {label && <FormLabel component="legend">{label}</FormLabel>}
      {helpText && <FormHelperText>{helpText}</FormHelperText>}

      {collectionGroups.map((group) => {
        return (
          <FormControlLabel
            key={group.id}
            control={
              <Checkbox
                checked={selectedGroupIds.includes(group.id)}
                onChange={() => handleSelect(group.id)}
              />
            }
            label={group.name}
          />
        );
      })}
    </FormGroup>
  );
};
