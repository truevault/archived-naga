import { Button, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import React from "react";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { EmptyInfo } from "../../Empty";

export const StorageLocationsTable: React.FC<{
  storageLocations: OrganizationDataRecipientDto[];
  onEdit?: (storageLocation: OrganizationDataRecipientDto) => void;
  onRemove?: (storageLocation: OrganizationDataRecipientDto) => void;
}> = ({ storageLocations, onEdit, onRemove }) => {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Description</TableCell>
          <TableCell />
        </TableRow>
      </TableHead>

      <TableBody>
        {storageLocations.length > 0 ? (
          <>
            {storageLocations.map((dr) => {
              return (
                <TableRow key={dr.id}>
                  <TableCell>{dr.name}</TableCell>
                  <TableCell className="text-right">
                    {onEdit && (
                      <Button color="secondary" onClick={() => onEdit(dr)}>
                        Edit
                      </Button>
                    )}

                    {onRemove && (
                      <Button color="secondary" onClick={() => onRemove(dr)}>
                        Remove
                      </Button>
                    )}
                  </TableCell>
                </TableRow>
              );
            })}
          </>
        ) : (
          <TableRow>
            <TableCell colSpan={2}>
              <EmptyInfo>No data storage locations added.</EmptyInfo>
            </TableCell>
          </TableRow>
        )}
      </TableBody>
    </Table>
  );
};
