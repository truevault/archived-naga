import { Edit } from "@mui/icons-material";
import { Button, Toolbar } from "@mui/material";
import React, { useState } from "react";
import { useDisclosureDetails } from "../../../../common/hooks/dataMapHooks";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto, PICGroup } from "../../../../common/service/server/dto/DataMapDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { CalloutVariant } from "../../../components/Callout";
import { DataRecipientCollapseCard } from "../../../components/DataRecipientCollapseCard";
import { Para } from "../../../components/typography/Para";
import { Paras } from "../../../components/typography/Paras";
import { usePrimaryOrganizationId } from "../../../hooks";
import { DataRecipientCollectionCheckboxes } from "../../../pages/get-compliant/data-recipients/DataRecipientCollectionCheckboxes";
import { ConfirmDialog } from "../../dialog";
import { EmptyInfo } from "../../Empty";
import { PicGroupList } from "../collection-groups/PicGroupList";

const CollectionContent: React.FC<{
  collectedPicGroups: PICGroup[];
  dataRecipient: OrganizationDataRecipientDto;
}> = ({ dataRecipient, collectedPicGroups }) => {
  const disclosureType = dataRecipient.ccpaIsSelling ? "sold to" : "shared with";
  return collectedPicGroups.length > 0 ? (
    <Paras>
      <Para className="color-neutral-600">
        The following information is {disclosureType} <strong>{dataRecipient.name}</strong>
      </Para>

      <PicGroupList picGroups={collectedPicGroups} />
    </Paras>
  ) : (
    <EmptyInfo>
      No information is {disclosureType} <strong>{dataRecipient.name}</strong>
    </EmptyInfo>
  );
};

type DisclosureCardProps = {
  className?: string;
  dataMap: DataMapDto;
  collectionGroup: CollectionGroupDetailsDto;
  dataRecipient: OrganizationDataRecipientDto;
  afterSave: () => Promise<void>;
  forceEdit: boolean;
};

export const EditableDisclosureCard: React.FC<DisclosureCardProps> = ({
  className,
  dataMap,
  collectionGroup,
  dataRecipient,
  afterSave,
  forceEdit = false,
}) => {
  const orgId = usePrimaryOrganizationId();
  const [editing, setEditing] = useState(false);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const [hasSelection, setHasSelection] = useState(false);
  const isEditing = editing || forceEdit;

  const { disclosedPicGroups } = useDisclosureDetails(dataMap, collectionGroup, dataRecipient);

  const handleSave = async () => {
    if (!hasSelection) {
      setConfirmOpen(true);
    } else {
      await handleDone();
    }
  };

  const handleDone = async () => {
    await afterSave();
    setEditing(false);
  };

  return (
    <>
      <DataRecipientCollapseCard
        className={className}
        collapsible={false}
        recipient={dataRecipient}
        variant={isEditing ? CalloutVariant.Clear : CalloutVariant.Gray}
        headerContent={
          isEditing ? null : (
            <Button
              onClick={() => setEditing(true)}
              size="small"
              color="secondary"
              startIcon={<Edit />}
            >
              Edit
            </Button>
          )
        }
      >
        {isEditing ? (
          <>
            <DataRecipientCollectionCheckboxes
              orgId={orgId}
              dataMap={dataMap}
              group={collectionGroup}
              recipient={dataRecipient}
              setHasSelection={setHasSelection}
            />

            <Toolbar disableGutters>
              <Button className="ml-auto" variant="contained" color="primary" onClick={handleSave}>
                Save
              </Button>
            </Toolbar>
          </>
        ) : (
          <CollectionContent
            dataRecipient={dataRecipient}
            collectedPicGroups={disclosedPicGroups}
          />
        )}
      </DataRecipientCollapseCard>

      <ConfirmDialog
        open={confirmOpen}
        onClose={() => setConfirmOpen(false)}
        confirmText="Ok"
        confirmTitle="Are you sure?"
        contentText={`Removing all personal information categories for this data source will mark it as not associated for ${collectionGroup.name}`}
        onConfirm={handleDone}
      />
    </>
  );
};
