import { SvgIconComponent } from "@mui/icons-material";
import clsx from "clsx";
import React from "react";
import { Callout, CalloutVariant } from "../../Callout";

export type SettingsItem = {
  id: string;
  label: React.ReactNode;
  subtitle?: React.ReactNode;
  icon?: SvgIconComponent | React.ReactNode;
  response?: any;
  isNested?: boolean;
  helpText?: React.ReactNode;
  action?: React.ReactNode;
};

export const SettingsIndicator: React.FC<{ settingsItem: SettingsItem }> = ({ settingsItem }) => (
  <Callout
    variant={CalloutVariant.LightPurple}
    icon={settingsItem.icon}
    className={clsx("mb-sm", { "ml-xl": settingsItem.isNested })}
    title={settingsItem.label}
    subtitle={settingsItem.subtitle}
    action={settingsItem.action}
  />
);
