import { FormControl } from "@mui/material";
import _ from "lodash";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Api } from "../../../../common/service/Api";
import {
  OrganizationDataRecipientDto,
  OrganizationDataRecipientPlatformDto,
} from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../../common/service/server/dto/OrganizationDto";
import { RequestHandlingInstructionDto } from "../../../../common/service/server/dto/RequestHandlingInstructionDto";
import { RetentionReasonDto } from "../../../../common/service/server/dto/RetentionReasonDto";
import { VendorDto } from "../../../../common/service/server/dto/VendorDto";
import { usePrimaryOrganizationId } from "../../../hooks";
import { useDataMap } from "../../../hooks/useDataMap";
import { vendorName } from "../../../util/resources/vendorUtils";
import { isShopify, isThirdPartyRecipient } from "../../../util/vendors";
import { Disableable } from "../../Disableable";
import { VendorBooleanQuestion } from "../../vendor/VendorBooleanQuestion";
import {
  REASONABLE_INTERNAL_USES,
  VendorDeletionExceptionSetting,
} from "../../vendor/VendorDeletionExceptionSetting";

const calculateInstalledInShopify = (
  vendors: OrganizationDataRecipientDto[],
  appPlatform: OrganizationDataRecipientPlatformDto,
) => {
  const shopify = vendors?.find((v) => isShopify(v));

  if (!shopify) {
    return null;
  }

  if (!appPlatform) {
    return false;
  }

  return appPlatform.installed.includes(shopify.vendorId);
};

const initialStateFrom = (
  vendor: OrganizationDataRecipientDto | VendorDto,
  vendors: OrganizationDataRecipientDto[],
  appPlatform: OrganizationDataRecipientPlatformDto,
  reasons: RetentionReasonDto[],
  instructions: RequestHandlingInstructionDto[],
): RetentionState => {
  if (!vendor || !instructions) {
    return { allReasons: reasons, instruction: null, installedInShopify: null };
  }

  const instruction = instructions.find(
    (x) =>
      x.vendorId === (vendor as OrganizationDataRecipientDto).vendorId ||
      x.vendorId === (vendor as VendorDto).id,
  );

  return {
    allReasons: reasons,
    instruction,
    installedInShopify: calculateInstalledInShopify(vendors, appPlatform),
  };
};

export const saveRetentionUpdates = async (
  org: OrganizationDto,
  vendor: OrganizationDataRecipientDto,
  vendors: OrganizationDataRecipientDto[],
  appPlatforms: OrganizationDataRecipientPlatformDto,
  updateRemoteInstruction: (payload: any) => Promise<void>,
  meta: RetentionMeta,
  state: RetentionState,
) => {
  const id = vendor.vendorId || vendor.id;

  if (meta.showShopify) {
    const shopify = vendors.find((v) => isShopify(v));
    const installedNow = calculateInstalledInShopify(vendors, appPlatforms);
    if (installedNow != state.installedInShopify) {
      if (state.installedInShopify) {
        await Api.organizationVendor.addPlatformApplication(
          org.id,
          shopify.vendorId,
          vendor.vendorId,
        );
      } else {
        await Api.organizationVendor.removePlatformApplication(
          org.id,
          shopify.vendorId,
          vendor.vendorId,
        );
      }
    }
  }

  await updateRemoteInstruction({
    organizationId: org.id,
    requestType: "DELETE",
    vendorId: id,
    processingMethod: state.instruction?.processingMethod || "DELETE",
    retentionReasons: state.instruction?.retentionReasons,
    exceptionsAffirmed: state.instruction?.exceptionsAffirmed,
    hasRetentionExceptions: state.instruction?.hasRetentionExceptions,
    deletedPIC: state.instruction?.deletedPIC,
  });
};

export const finishedRetention = (state: RetentionState, meta: RetentionMeta) => {
  const hasMethod = state.instruction?.processingMethod || meta.lockExceptionToDelete;
  const hasReasons =
    state.instruction?.processingMethod != "RETAIN" ||
    state.instruction?.retentionReasons?.length > 0;

  const needsAffirmation = state.instruction?.retentionReasons?.includes(REASONABLE_INTERNAL_USES);
  const hasAffirmation = state.instruction?.exceptionsAffirmed;

  return hasMethod && hasReasons && (!needsAffirmation || (needsAffirmation && hasAffirmation));
};

type RetentionMeta = {
  showShopify: boolean;
  lockDataAccessible: boolean;
  lockExceptionToDelete: boolean;
  exceptionToDeleteDisabledTooltip: string | undefined;
};

const calcualteRetentionMeta = (
  vendor: OrganizationDataRecipientDto | VendorDto,
  vendors: OrganizationDataRecipientDto[],
  state: RetentionState,
): RetentionMeta => {
  if (!vendor) {
    return {
      showShopify: false,
      lockDataAccessible: false,
      lockExceptionToDelete: false,
      exceptionToDeleteDisabledTooltip: undefined,
    };
  }

  const shopify = vendors?.find((v) => isShopify(v));
  const showShopify =
    Boolean(shopify) &&
    !isThirdPartyRecipient(vendor) &&
    !vendor.isStandalone &&
    !vendor.isPlatform;

  const lockForInaccessible = state.instruction?.processingMethod == "INACCESSIBLE_OR_NOT_STORED";
  const lockForShopify =
    (showShopify && state.installedInShopify && vendor.respectsPlatformRequests) ||
    vendor.isPlatform ||
    isShopify(vendor);

  let tooltip: string | undefined = undefined;
  if (lockForInaccessible) {
    tooltip = "This setting does not apply to inaccessible data.";
  }
  if (lockForShopify) {
    tooltip =
      "Shopify automatically controls data deletion and retention for apps installed on its platform.";
  }

  return {
    showShopify,
    lockDataAccessible: Boolean(vendor["deletionInstructions"]),
    lockExceptionToDelete: lockForInaccessible || lockForShopify,
    exceptionToDeleteDisabledTooltip: tooltip,
  };
};

type RetentionState = {
  allReasons: RetentionReasonDto[];
  instruction: RequestHandlingInstructionDto | null;

  installedInShopify: boolean | null;
};

export const useDataRecipientRetentionSettings = (
  vendor: OrganizationDataRecipientDto | VendorDto,
  vendors: OrganizationDataRecipientDto[],
  appPlatforms: OrganizationDataRecipientPlatformDto,
  reasons: RetentionReasonDto[],
  instructions: RequestHandlingInstructionDto[],
) => {
  const [state, setState] = useState<RetentionState>({
    allReasons: [],
    instruction: null,
    installedInShopify: null,
  });

  const meta = useMemo(
    () => calcualteRetentionMeta(vendor, vendors, state),
    [vendor, vendors, state],
  );

  const updateState = useCallback(
    (value: Partial<RetentionState>) => {
      setState((prevState) => ({ ...prevState, ...value }));
    },
    [setState],
  );

  // calculate our initial state
  useEffect(() => {
    setState(initialStateFrom(vendor, vendors, appPlatforms, reasons, instructions));
  }, [vendor, vendors, appPlatforms, reasons, instructions]);

  // auto-lock editing
  useEffect(() => {
    if (
      state.installedInShopify &&
      vendor.respectsPlatformRequests &&
      state.instruction?.processingMethod == "RETAIN"
    ) {
      const instruction = { ...state.instruction };
      instruction.processingMethod = "DELETE";
      instruction.retentionReasons = [];

      updateState({ instruction });
    }
  }, [
    state.installedInShopify,
    state.instruction,
    updateState,
    state.instruction?.processingMethod,
  ]);

  return { state, meta, updateState };
};

interface VendorRetentionEditProps {
  dataRecipient: OrganizationDataRecipientDto | VendorDto;
  isThirdPartyRecipient: boolean;
  state: RetentionState;
  meta: RetentionMeta;
  updateState: (update: Partial<RetentionState>) => void;
}

export const DataRecipientRetentionSettings: React.FC<VendorRetentionEditProps> = ({
  dataRecipient,
  isThirdPartyRecipient,
  state,
  meta,
  updateState,
}) => {
  const orgId = usePrimaryOrganizationId();
  const [map] = useDataMap(orgId);

  const disclosedCategories = useMemo(() => {
    // @ts-ignore
    const vendorId = dataRecipient.vendorId ?? dataRecipient.id;
    const disclosedPic = _.uniq(
      map?.disclosure.filter((d) => d.vendorId == vendorId).flatMap((d) => d.disclosed) ?? [],
    );
    return map?.categories.filter((pic) => disclosedPic.includes(pic.id)) ?? [];
  }, [dataRecipient, map]);

  const handleRadioChanged = useCallback(
    (vendor, e) => {
      const instruction = { ...state.instruction };
      instruction.processingMethod = e.target.value;
      instruction.retentionReasons = [];

      updateState({ instruction });
    },
    [updateState, state.instruction],
  );

  const handleReasonChanged = useCallback(
    (vendor, reasons) => {
      const instruction = { ...state.instruction };
      instruction.retentionReasons = reasons;
      updateState({ instruction });
    },
    [updateState, state.instruction],
  );

  const handleAffirmationChanged = useCallback(
    (instruction: RequestHandlingInstructionDto, affirmed: boolean) => {
      updateState({
        instruction: {
          ...instruction,
          exceptionsAffirmed: affirmed,
        },
      });
    },
    [updateState],
  );

  const handleRetentionExceptionsChanged = useCallback(
    (instruction: RequestHandlingInstructionDto, hasExceptions: boolean) => {
      updateState({
        instruction: {
          ...instruction,
          hasRetentionExceptions: hasExceptions,
        },
      });
    },
    [updateState],
  );

  const handleDeletedPICChange = useCallback(
    (instruction: RequestHandlingInstructionDto, picId: string, deleted: boolean) => {
      const deletedPIC = _.clone(instruction.deletedPIC ?? []);
      if (deleted) {
        deletedPIC.push(picId);
      } else {
        const idx = deletedPIC.indexOf(picId);
        if (idx >= 0) {
          deletedPIC.splice(idx, 1);
        }
      }

      updateState({
        instruction: {
          ...instruction,
          deletedPIC,
        },
      });
    },
    [updateState],
  );

  return (
    <div className="mt-xl">
      <ShopifyQuestion
        vendor={dataRecipient}
        isThirdPartyRecipient={isThirdPartyRecipient}
        state={state}
        meta={meta}
        update={updateState}
      />

      <VendorBooleanQuestion
        question={`Is data accessible in ${vendorName(dataRecipient, isThirdPartyRecipient)}?`}
        disabled={meta.lockDataAccessible}
        disabledTooltip="This vendor has instructions for how you can access and delete data stored in its system. Those instructions can be viewed in Request Instructions."
        condensed={false}
        helpText={
          <div className="text-t1">
            Some vendors do not make personal information available to businesses. For example, web
            hosting services and ad networks typically don’t give businesses a way to look up
            personal information by consumer name or email address. If data is not accessible,
            answer “No.”
          </div>
        }
        value={
          state.instruction?.processingMethod
            ? state.instruction.processingMethod == "INACCESSIBLE_OR_NOT_STORED"
              ? false
              : true
            : null
        }
        onChange={(newVal) =>
          updateState({
            instruction: {
              ...state.instruction,
              processingMethod: newVal ? "DELETE" : "INACCESSIBLE_OR_NOT_STORED",
            },
          })
        }
      />

      <section key={"deletion-exceptions"} className="flex-row mb-xl">
        <header className="flex-grow">
          <div className="text-t3 mb-sm text-weight-bold">Exceptions to Deletion</div>
          <aside className="text-t1 color-neutral-600 mb-lg">
            Privacy laws allow businesses to retain a consumer’s personal data - even when the
            consumer requests deletion - for certain approved business purposes. If you select “Yes”
            you must select one or more applicable exceptions.
          </aside>

          <FormControl component="fieldset">
            <Disableable
              disabled={meta.lockExceptionToDelete}
              disabledTooltip={meta.exceptionToDeleteDisabledTooltip}
            >
              <VendorDeletionExceptionSetting
                vendor={dataRecipient}
                retentionReasons={state.allReasons}
                instruction={state.instruction}
                disclosed={disclosedCategories}
                onRadioChange={handleRadioChanged}
                onReasonChange={handleReasonChanged}
                onAffirmationChange={handleAffirmationChanged}
                onRetentionExceptionsChange={handleRetentionExceptionsChanged}
                onDeletedPICChange={handleDeletedPICChange}
                disabled={meta.lockExceptionToDelete}
              />
            </Disableable>
          </FormControl>
        </header>
      </section>
    </div>
  );
};

type QuestionProps = {
  vendor: OrganizationDataRecipientDto | VendorDto;
  isThirdPartyRecipient: boolean;
  state: RetentionState;
  meta: RetentionMeta;
  update: (update: Partial<RetentionState>) => void;
};

const ShopifyQuestion: React.FC<QuestionProps> = ({
  vendor,
  isThirdPartyRecipient,
  state,
  update,
  meta,
}) => {
  if (!meta.showShopify) {
    return null;
  }
  return (
    <VendorBooleanQuestion
      question={`${vendorName(vendor, isThirdPartyRecipient)} is an installed app in Shopify`}
      condensed={false}
      helpText={
        <div className="text-t1">
          Shopify handles data deletion for installed apps. To see a list of your installed apps, go
          to the{" "}
          <a href="https://shopify.com/admin/apps" target="_blank" rel="noopener noreferrer">
            Apps
          </a>{" "}
          section of your Shopify account.
        </div>
      }
      value={state.installedInShopify}
      onChange={(newVal) => update({ installedInShopify: newVal })}
    />
  );
};
