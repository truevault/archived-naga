import { RadioGroup } from "@mui/material";
import clsx from "clsx";
import React, { ReactNode, useState } from "react";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RadioControlLabel } from "../../input/RadioControlLabel";
import { DataRecipientLogo } from "../../DataRecipientLogo";
import { UrlOrFileInput } from "../../get-compliant/vendors/UrlOrFileInput";
import { UUIDString } from "../../../../common/service/server/Types";

export type RadioDirection = "col" | "row";

export type RadioOption<TValue extends any = any> = {
  value: TValue;
  label?: string;
};

type Props = {
  recipient: OrganizationDataRecipientDto;
  loading?: boolean;
  options: RadioOption[];
  value: string | null;
  onChange: (updated: string) => void;
  direction?: RadioDirection;
  description?: string | ReactNode;
  fullWidth?: boolean;
  disabled?: boolean;
  disabledTooltip?: string;
  noTopBorder?: boolean;
  showDocumentation?: boolean;
  url?: string;
  fileName?: string;
  fileKey?: string;
  showFilename?: boolean;
  onUrlChanged?: (recipient: OrganizationDataRecipientDto, url: string) => Promise<void>;
  handleFileDelete?: (vendorId: UUIDString, fileKey: string, key?: string) => Promise<void>;
  onFileUpload?: (
    recipient: OrganizationDataRecipientDto,
    filename: string,
    file: File,
  ) => Promise<void>;
};

export const DataRecipientRadioOptions: React.FC<Props> = ({
  recipient,
  loading,
  description,
  fullWidth,
  options,
  value,
  onChange,
  direction = "row",
  disabled,
  disabledTooltip,
  noTopBorder,
  showDocumentation = false,
  url,
  fileName,
  onUrlChanged,
  onFileUpload,
  handleFileDelete,
  showFilename = false,
}) => {
  const [uploadLoading, setUploadLoading] = useState(false);
  const [fileError, setFileError] = useState({});
  const handleFileUpload = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files[0];
    if (file && file.size <= 15000000) {
      setUploadLoading(true);
      setFileError((error) => ({ ...error, msg: null }));
      try {
        await onFileUpload?.(recipient, file.name, file);
      } finally {
        setUploadLoading(false);
      }
    } else {
      setFileError((error) => ({ ...error, msg: "(max file size: 15MB)" }));
    }
  };

  const onFileDeleted = async () => {
    setUploadLoading(true);
    setFileError((error) => ({ ...error, msg: null }));
    try {
      await handleFileDelete?.(recipient.vendorId, recipient.tosFileKey);
    } finally {
      setUploadLoading(false);
    }
  };

  const val = value || "";
  return (
    <div className={clsx("data-recipient-radio-options")}>
      <div className={"data-recipient-radio-options__label"}>
        <div className="data-recipient-radio-options__logo">
          <DataRecipientLogo dataRecipient={recipient} size={24} />
        </div>
        <div className="data-recipient-radio-options__info">
          <div className="text-t3 text-weight-medium text-truncate-ellipsis">{recipient.name}</div>
          {description && <div className="mt-xxs text-component-help">{description}</div>}
        </div>
      </div>
      <div
        className={clsx(
          "data-recipient-radio-options__options",
          { "data-recipient-radio-options__options--full-width": fullWidth },
          { "data-recipient-radio-options__options--no-top-border": noTopBorder },
          { "data-recipient-radio-options__options--column": true },
        )}
      >
        <RadioGroup
          value={val}
          onChange={({ target }) => onChange(target.value)}
          className={`form-group--${direction}`}
          style={{ marginTop: -4 }}
        >
          {options.map((opt) => {
            const labelValue = opt.label || opt.value;
            return (
              <RadioControlLabel
                key={opt.value}
                disabled={disabled}
                disabledTooltip={disabledTooltip}
                formControlProps={{ disabled: disabled || loading || uploadLoading }}
                radioProps={{ disabled: disabled || loading || uploadLoading }}
                value={opt.value}
                label={labelValue}
                loading={loading && opt.value == val}
              />
            );
          })}
        </RadioGroup>
        {showDocumentation && (
          <div className="mt-sm ml-lg">
            <UrlOrFileInput
              url={url}
              onUrlChanged={(newUrl) => onUrlChanged?.(recipient, newUrl)}
              filename={fileName}
              onFileChanged={(e) => handleFileUpload(e)}
              disabled={uploadLoading}
              error={fileError["msg"]}
              showUploadedFile={showFilename}
              handleFileDelete={onFileDeleted}
            />
          </div>
        )}
      </div>
    </div>
  );
};
