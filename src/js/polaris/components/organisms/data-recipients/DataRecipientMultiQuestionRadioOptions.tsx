import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import clsx from "clsx";
import React, { ReactNode } from "react";
import { Disableable } from "../..";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";

export type RadioDirection = "col" | "row";

export type RadioOption<TValue extends any = any> = {
  value: TValue;
  label?: string;
};

type Props = {
  recipient: OrganizationDataRecipientDto;
  options: RadioOption[];
  value: string | null;
  onChange: (updated: string) => void;
  direction?: RadioDirection;
  description?: string | ReactNode;
  fullWidth?: boolean;
  disabled?: boolean;
  disabledTooltip?: string;
  noTopBorder: boolean;
};

export const DataRecipientMultiQuestionRadioOptions: React.FC<Props> = ({
  recipient,
  description,
  fullWidth,
  options,
  value,
  onChange,
  direction = "row",
  disabled,
  disabledTooltip,
  noTopBorder,
}) => {
  return (
    <div className={clsx("data-recipient-multi-question-radio-options")}>
      <div className={"data-recipient-radio-options__label"}>
        <div className="data-recipient-radio-options__logo">
          <img src={recipient.logoUrl} />
        </div>
        <div className="data-recipient-radio-options__info">
          <div className="text-t3 text-weight-medium text-truncate-ellipsis">{recipient.name}</div>
          {description && <div className="mt-xxs text-component-help">{description}</div>}
        </div>
      </div>
      <div
        className={clsx(
          "data-recipient-radio-options__options",
          { "data-recipient-radio-options__options--full-width": fullWidth },
          { "data-recipient-radio-options__options--no-top-border": noTopBorder },
          { "data-recipient-radio-options__options--column": true },
        )}
      >
        <RadioGroup
          value={value || ""}
          onChange={({ target }) => onChange(target.value)}
          className={`form-group--${direction}`}
          style={{ marginTop: -4 }}
        >
          {options.map((opt) => {
            const labelValue = opt.label || opt.value;
            return (
              <Disableable disabled={disabled} disabledTooltip={disabledTooltip} key={opt.value}>
                <FormControlLabel
                  value={opt.value}
                  control={<Radio color="primary" disabled={disabled} />}
                  label={labelValue}
                />
              </Disableable>
            );
          })}
        </RadioGroup>
      </div>
    </div>
  );
};
