import { Button, Typography } from "@mui/material";
import clsx from "clsx";
import pluralize from "pluralize";
import React, { useState } from "react";
import { PICGroup } from "../../../common/service/server/dto/DataMapDto";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
} from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { VENDOR_KEY_TRUEVAULT_POLARIS } from "../../types/Vendor";
import { CalloutVariant } from "../Callout";
import { DataRecipientCollapseCard } from "../DataRecipientCollapseCard";
import { RemoveVendorDialog } from "../dialog/RemoveVendorDialog";
import { DisableableButton } from "../Disableable";

type DataRecipientCollectionCardsProps = {
  recipients: OrganizationDataRecipientDto[];
  context: CollectionContext[];
  disclosuresByVendorId: Record<string, PICGroup[]>;
  renderer: (recipient: OrganizationDataRecipientDto) => React.ReactNode;
};

export const DataRecipientCollectionCards: React.FC<DataRecipientCollectionCardsProps> = ({
  recipients,
  context,
  disclosuresByVendorId,
  renderer,
}) => {
  const [expanded, setExpanded] = useState<null | string>(recipients[0]?.id);

  return (
    <>
      {recipients.map((r, idx) => {
        const next = idx != recipients.length - 1 ? recipients[idx + 1] : null;
        const disclosuresCount = disclosuresByVendorId[r.vendorId]?.length ?? 0;

        return (
          <RecipientCollectionCard
            key={r.vendorId}
            recipient={r}
            next={next}
            context={context}
            expanded={expanded}
            setExpanded={setExpanded}
            disclosures={disclosuresCount}
            renderer={renderer}
          />
        );
      })}
    </>
  );
};

type RecipientCollectionCardProps = {
  recipient: OrganizationDataRecipientDto;
  next: OrganizationDataRecipientDto | null;
  expanded: string | null;
  setExpanded: (id: string | null) => void;
  disclosures: number;
  renderer: (recipient: OrganizationDataRecipientDto) => React.ReactNode;
  context: CollectionContext[];
};
const RecipientCollectionCard: React.FC<RecipientCollectionCardProps> = ({
  recipient: r,
  next,
  context,
  expanded,
  setExpanded,
  disclosures: disclosuresCount,
  renderer,
}) => {
  const [removeVendorOpen, setRemoveVendorOpen] = useState(false);
  const openRemoveVendor = () => setRemoveVendorOpen(true);
  const closeRemoveVendor = () => setRemoveVendorOpen(false);

  const handleExpand = () => {
    setExpanded(r.id);
    setTimeout(() => window.polaris.scrollSelectorIntoView(`#dr-${r.id}`), 300);
  };

  const handleNext = () => {
    setExpanded(next?.id);
    setTimeout(() => window.polaris.scrollSelectorIntoView(`#dr-${r.id}`), 300);
  };

  const disclosuresLabel = pluralize("Disclosures", disclosuresCount, true);

  return (
    <div id={`dr-${r.id}`} key={r.id}>
      <DataRecipientCollapseCard
        recipient={r}
        collapsed={expanded != r.id}
        variant={CalloutVariant.LightPurpleBordered}
        onExpand={handleExpand}
        headerContent={
          <Typography
            variant="caption"
            className={clsx({
              "text-error": disclosuresCount === 0,
              "text-muted": disclosuresCount > 0,
            })}
          >
            {disclosuresLabel} Selected
          </Typography>
        }
      >
        <div>{renderer(r)}</div>

        {Boolean(next) && (
          <Button className="mt-lg mr-lg" variant="contained" size="small" onClick={handleNext}>
            Next Vendor
          </Button>
        )}

        <DisableableButton
          className="mt-lg"
          color="secondary"
          variant="text"
          size="small"
          onClick={openRemoveVendor}
          disabled={r.vendorKey == VENDOR_KEY_TRUEVAULT_POLARIS}
          disabledTooltip="You cannot remove the TrueVault Polaris vendor"
        >
          Remove
        </DisableableButton>

        <RemoveVendorDialog
          open={removeVendorOpen}
          onClose={closeRemoveVendor}
          vendorId={r.vendorId}
          vendorName={r.name}
          collectionContext={context}
        />
      </DataRecipientCollapseCard>
    </div>
  );
};
