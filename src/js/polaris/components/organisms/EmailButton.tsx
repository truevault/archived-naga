import { Alert, ButtonProps } from "@mui/material";
import React from "react";
import { usePrimaryOrganization } from "../../hooks";
import { RouteHelpers } from "../../root/routes";
import { DisableableButton } from "../Disableable";

export const EmailButton: React.FC<ButtonProps & { disabledTooltip?: string }> = (props) => {
  const [org] = usePrimaryOrganization();
  const isDisabled = org.mailbox.mailboxStatus == "NOT_CONNECTED";
  let tooltip = "You cannot send an email while your privacy email is disconnected";
  if (props.disabled && props.disabledTooltip) {
    tooltip = props.disabledTooltip;
  }

  return (
    <>
      {isDisabled && (
        <Alert severity="error" className="mb-md">
          The email cannot be sent because your privacy email has been disconnected. Please visit
          your{" "}
          <a href={RouteHelpers.organization.inbox(org.id)} target="_blank" rel="noreferrer">
            privacy inbox settings
          </a>{" "}
          to reconnect it.
        </Alert>
      )}

      <DisableableButton
        {...props}
        disabledTooltip={tooltip}
        disabled={props.disabled || isDisabled}
      />
    </>
  );
};
