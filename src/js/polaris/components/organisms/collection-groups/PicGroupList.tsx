import { List, ListItem, ListItemIcon, ListItemText } from "@mui/material";
import React from "react";
import { PICGroup } from "../../../../common/service/server/dto/DataMapDto";
import { GroupIcon } from "../../../pages/get-compliant/data-map/GroupIcon";

export const PicGroupList: React.FC<{ picGroups: PICGroup[] }> = ({ picGroups }) => (
  <List className="p-0">
    {picGroups.map((group) => (
      <ListItem disableGutters key={group.id}>
        <ListItemIcon>
          <GroupIcon groupName={group.name} color="inherit" />
        </ListItemIcon>
        <ListItemText
          primary={group.name}
          secondary={group.categories.map((g) => g.name).join(", ")}
        />
      </ListItem>
    ))}
  </List>
);
