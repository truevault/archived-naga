import { Typography } from "@mui/material";
import React from "react";
import { BusinessPurposeDto } from "../../../../common/service/server/dto/PersonalInformationOptionsDto";
import { ToggleButton } from "../../buttons/ToggleButton";

export const CollectionPurposesMultiselect: React.FC<{
  collectionPurposes: BusinessPurposeDto[];
  selected: BusinessPurposeDto[];
  onChange: (p: BusinessPurposeDto, selected: boolean) => void;
}> = ({ collectionPurposes, selected, onChange: handlePurposeChanged }) => {
  return (
    <ul className="vertical-list">
      {collectionPurposes.map((purpose) => (
        <li key={purpose.id} className="mt-mdlg">
          <ToggleButton
            checked={selected.some((s) => s.id === purpose.id)}
            onChange={(selected) => handlePurposeChanged(purpose, selected)}
          >
            <div>
              <Typography>{purpose.name}</Typography>
              {Boolean(purpose.description) && (
                <Typography variant="caption">{purpose.description}</Typography>
              )}
            </div>
          </ToggleButton>
        </li>
      ))}
    </ul>
  );
};
