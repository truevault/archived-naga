import React from "react";
import { GroupIcon } from "../../../pages/get-compliant/data-map/GroupIcon";

export const CollectionGroupHeader: React.FC<{
  groupName: string;
}> = ({ groupName }) => {
  return (
    <header className="align-items-center p-md bg-gray">
      <div className="fill mb-xs">
        <GroupIcon fontSize="large" color="primary" groupName={groupName} />
      </div>
      <div className="fill text-center">
        <strong>{groupName}</strong>
      </div>
    </header>
  );
};
