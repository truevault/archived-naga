import React, { useState } from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { CategoryDto, CollectionDto } from "../../../../common/service/server/dto/DataMapDto";
import { OrganizationPublicId } from "../../../../common/service/server/Types";
import {
  SurveyBooleanQuestion,
  SurveyBooleanQuestionProps,
} from "../../../components/get-compliant/survey/SurveyBooleanQuestion";
import { UNCOMMON_QUESTION_SLUG } from "../../../surveys/consumerCollectionCategoriesSurvey";
import { CollectionCheckboxes } from "../../get-compliant/collection-groups/CollectionCheckboxes";
import { QuestionProps } from "./ConsumerDataCollectionForm";

export const UncommonQuestion: React.FC<
  QuestionProps & {
    group: CollectionGroupDetailsDto;
    orgId: OrganizationPublicId;
    collected: CollectionDto;
    other: CategoryDto[];
    onCollectionChange?: () => Promise<void>;
    question: SurveyBooleanQuestionProps;
  }
> = ({ group, orgId, collected, answers, setAnswer, other, onCollectionChange, question }) => {
  const [forceShowCheckboxes, setForceShowCheckboxes] = useState(false);
  const showCheckboxes = forceShowCheckboxes || answers[UNCOMMON_QUESTION_SLUG] === "true";

  // This is a bit weird, but we don't have a good mechanism for doing this a different way
  question.question = (
    <p>
      {question.question}{" "}
      <a
        href="#"
        onClick={(e) => {
          e.preventDefault();
          setForceShowCheckboxes(true);
        }}
      >
        See full list
      </a>
    </p>
  );

  return (
    <>
      <SurveyBooleanQuestion
        answer={answers[UNCOMMON_QUESTION_SLUG]}
        onAnswer={(ans) => {
          if (ans === "false") {
            setForceShowCheckboxes(false);
          }

          return setAnswer(UNCOMMON_QUESTION_SLUG, ans);
        }}
        disabled={question.isDisabled}
        question={question}
      />

      {showCheckboxes && (
        <CollectionCheckboxes
          group={group}
          orgId={orgId}
          categories={other}
          collected={collected}
          onChange={onCollectionChange}
        />
      )}
    </>
  );
};
