import React, { SetStateAction, useEffect, useMemo } from "react";
import { SurveyResponses } from "../../../../common/hooks/useSurvey";
import { Organization } from "../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import {
  CategoryDto,
  CollectionDto,
  DataMapDto,
} from "../../../../common/service/server/dto/DataMapDto";
import { PICSurveyClassification } from "../../../../common/service/server/dto/PersonalInformationOptionsDto";
import { CollectionCheckboxes } from "../../../components/get-compliant/collection-groups/CollectionCheckboxes";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import {
  INFERENCES_QUESTION_SLUG,
  SENSITIVE_INFERENCES_QUESTION_SLUG,
  UNCOMMON_QUESTION_SLUG,
} from "../../../surveys/consumerCollectionCategoriesSurvey";
import {
  hasAnsweredQuestion,
  parseBoolAnswer,
  prepareSurvey,
  Survey,
} from "../../../surveys/survey";
import { anyCollectedForCategory } from "../../../util/dataInventory";
import {
  SurveyBooleanQuestion,
  SurveyBooleanQuestionProps,
} from "../../get-compliant/survey/SurveyBooleanQuestion";
import { UncommonQuestion } from "./UncommonQuestion";

export const useSurveyClassification = (
  dataMap: DataMapDto,
  classification: PICSurveyClassification,
) => {
  return useMemo(
    () => filterBySurveyClassification(dataMap, classification),
    [dataMap, classification],
  );
};

export const filterBySurveyClassification = (
  dataMap: DataMapDto,
  classification: PICSurveyClassification,
): CategoryDto[] =>
  dataMap?.categories?.filter?.((c) => c.surveyClassification == classification) ?? [];

export type QuestionProps = {
  answers: SurveyResponses;
  setAnswer: (slug: string, answer: string, question?: string) => Promise<void>;
};

export const GDPR_CATEGORY_KEYS = [
  "gdpr-genetics",
  "gdpr-data-revealing-beliefs",
  "gdpr-health-or-sex-data",
  "gdpr-criminal-offenses",
];

export const ConsumerDataCollectionForm: React.FC<{
  dataMap: DataMapDto;
  org: Organization;
  collectionGroup: CollectionGroupDetailsDto;
  answers: SurveyResponses;
  collected: CollectionDto;
  survey: Survey;
  setAnswer: (slug: string, answer: string, question?: string) => Promise<void>;
  onCollectionChange?: () => Promise<void>;
  setSensitiveInferencesQuestionIsShown?: React.Dispatch<SetStateAction<Boolean>>;
}> = ({
  answers,
  setAnswer,
  dataMap,
  org,
  collectionGroup,
  survey,
  collected,
  onCollectionChange,
  setSensitiveInferencesQuestionIsShown,
}) => {
  const orgId = org.id;

  const common = useSurveyClassification(dataMap, "COMMON");
  const sensitive = useSurveyClassification(dataMap, "SENSITIVE");
  const gdprSensitive = useSurveyClassification(dataMap, "GDPR_SENSITIVE");
  const other = useSurveyClassification(dataMap, "UNCOMMON");
  const indirect = useSurveyClassification(dataMap, "INDIRECT");

  const gdprQuestion = org.featureGdpr;

  const questions = prepareSurvey(survey, answers);

  const uncommonQuestion = questions.find((q) => q.slug === UNCOMMON_QUESTION_SLUG);
  const inferencesQuestion = questions.find((q) => q.slug === INFERENCES_QUESTION_SLUG);
  const sensitiveInferencesQuestion = questions.find(
    (q) => q.slug === SENSITIVE_INFERENCES_QUESTION_SLUG,
  );

  const makesInferences = parseBoolAnswer(answers[INFERENCES_QUESTION_SLUG]);
  const collectsSensitiveInformation = anyCollectedForCategory(collected, sensitive);

  const hasAnsweredInferences = hasAnsweredQuestion(answers, INFERENCES_QUESTION_SLUG);
  const hasAnsweredSensitiveInferences = hasAnsweredQuestion(
    answers,
    SENSITIVE_INFERENCES_QUESTION_SLUG,
  );
  const showSensitiveInferences = makesInferences && collectsSensitiveInformation;
  useEffect(() => {
    setSensitiveInferencesQuestionIsShown(showSensitiveInferences);
  }, [showSensitiveInferences, setSensitiveInferencesQuestionIsShown]);

  const showIndirectCollection =
    hasAnsweredInferences && (!showSensitiveInferences || hasAnsweredSensitiveInferences);

  return (
    <>
      <CollectionCheckboxes
        label="We’ve listed below the most commonly collected types of consumer personal information. Which of these does your business collect from its customers?"
        group={collectionGroup}
        orgId={orgId}
        categories={common}
        collected={collected}
        byGroup={false}
        onChange={onCollectionChange}
        allCheckbox
      />

      <SurveyGateButton slug="common-done" survey={answers} updateResponse={setAnswer} label="Done">
        <CollectionCheckboxes
          label='The list below includes categories of personal information that are considered "sensitive." Select any sensitive information your business collects. If it collects none, select "Done".'
          group={collectionGroup}
          orgId={orgId}
          categories={sensitive}
          collected={collected}
          onChange={onCollectionChange}
        />

        <SurveyGateButton
          slug="sensitive-done"
          survey={answers}
          updateResponse={setAnswer}
          label="Done"
        >
          {gdprQuestion && (
            <>
              <CollectionCheckboxes
                label='Listed below are categories of personal data that are considered "sensitive" under GDPR. Some items in this list repeat or overlap with the categories listed as "sensitive" under US law but we have to ask about them separately. Select any sensitive information your business collects. If none apply, select “Done” below.'
                group={collectionGroup}
                orgId={orgId}
                categories={gdprSensitive}
                collected={collected}
                onChange={onCollectionChange}
              />
            </>
          )}

          <SurveyGateButton
            slug="gdpr-sensitive-done"
            survey={answers}
            updateResponse={setAnswer}
            label="Done"
            bypass={!gdprQuestion}
          >
            <UncommonQuestion
              group={collectionGroup}
              collected={collected}
              orgId={orgId}
              answers={answers}
              setAnswer={setAnswer}
              other={other}
              onCollectionChange={onCollectionChange}
              question={uncommonQuestion as SurveyBooleanQuestionProps}
            />

            {answers[UNCOMMON_QUESTION_SLUG] && (
              <div className="my-xl">
                <SurveyGateButton
                  slug="uncommon-done"
                  survey={answers}
                  updateResponse={setAnswer}
                  label="Done"
                  bypass={answers[UNCOMMON_QUESTION_SLUG] !== "true"}
                >
                  <div className="mb-xl">
                    <SurveyBooleanQuestion
                      answer={answers[INFERENCES_QUESTION_SLUG]}
                      onAnswer={(ans) => setAnswer(INFERENCES_QUESTION_SLUG, ans)}
                      question={inferencesQuestion as SurveyBooleanQuestionProps}
                    />
                  </div>

                  {showSensitiveInferences && (
                    <div className="mb-xl">
                      <SurveyBooleanQuestion
                        answer={answers[SENSITIVE_INFERENCES_QUESTION_SLUG]}
                        onAnswer={(ans) => setAnswer(SENSITIVE_INFERENCES_QUESTION_SLUG, ans)}
                        question={sensitiveInferencesQuestion as SurveyBooleanQuestionProps}
                      />
                    </div>
                  )}

                  {showIndirectCollection && (
                    <>
                      <CollectionCheckboxes
                        label="The categories of information below may be collected indirectly from consumers, such as when they browse your website, login to their account, or use your mobile app (if applicable). Select any categories of information that you collect below."
                        helpText="If you use any ad networks or embedded analytics tools, such as Facebook Ads or Google Analytics, you are most likely indirectly collecting this information. If you are unsure, it is best to select these categories."
                        group={collectionGroup}
                        orgId={orgId}
                        categories={indirect}
                        collected={collected}
                        onChange={onCollectionChange}
                        byGroup={false}
                      />

                      <SurveyGateButton
                        slug="inferences-done"
                        survey={answers}
                        updateResponse={setAnswer}
                        label="Done"
                      />
                    </>
                  )}
                </SurveyGateButton>
              </div>
            )}
          </SurveyGateButton>
        </SurveyGateButton>
      </SurveyGateButton>
    </>
  );
};
