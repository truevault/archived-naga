import _ from "lodash";
import React, { useCallback, useMemo } from "react";
import { Organization } from "../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { CategoryDto, DataMapDto } from "../../../../common/service/server/dto/DataMapDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { OrderingVariant } from "../../../util/dataInventory";
import { Button, Grid } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { UpdateFn } from "../../../../common/hooks/api";

const GDPR_GROUP_NAME = "GDPR Sensitive Data";

const useStyles = makeStyles(() => ({
  reviewButtonDivider: {
    borderTop: "3px solid #e8e5ee",
    marginBottom: "7px",
  },
  reviewItemName: {
    maxWidth: "325px",
    paddingTop: "8px",
  },
}));

export const ConsumerDataCollectionReview: React.FC<{
  dataMap: DataMapDto;
  org: Organization;
  collectionGroup: CollectionGroupDetailsDto;
  initialCollectedPIC: UUIDString[];
  onCollectionChange?: () => Promise<void>;
  orderingVariant: OrderingVariant;
  updateCollection: UpdateFn;
}> = ({
  dataMap,
  org,
  collectionGroup,
  initialCollectedPIC,
  orderingVariant = "default",
  updateCollection,
}) => {
  const classes = useStyles({});

  const groupedCategories: Record<UUIDString, CategoryDto[]> = useMemo(() => {
    return _.groupBy(dataMap?.categories ?? [], "picGroupId");
  }, [dataMap]);

  const orderedGroupedCategories: Record<UUIDString, CategoryDto[]> = useMemo(() => {
    return _.transform(
      groupedCategories,
      (result, categories, key) => {
        result[key] =
          orderingVariant === "employee"
            ? _.sortBy(categories, ["picGroupEmployeeOrder", "picGroupName"])
            : _.sortBy(categories, ["picGroupOrder", "picGroupName"]);
      },
      {},
    );
  }, [groupedCategories, orderingVariant]);

  const isGdpr = org?.featureGdpr ?? false;

  const filteredGroups: Record<UUIDString, CategoryDto[]> = useMemo(
    () =>
      _.omitBy(orderedGroupedCategories, (categories) => {
        const firstCat = categories[0];

        if (firstCat.picGroupName === GDPR_GROUP_NAME && !isGdpr) {
          return true;
        }

        return false;
      }),
    [orderedGroupedCategories, isGdpr],
  );

  const initialPIC: CategoryDto[] = useMemo(
    () =>
      Object.values(filteredGroups).reduce(
        (acc, cur) => acc.concat(cur.filter(({ id }) => initialCollectedPIC.includes(id))),
        [],
      ),
    [dataMap, initialCollectedPIC, filteredGroups],
  );

  let [remaining, removed] = useMemo(() => {
    const collected: UUIDString[] =
      dataMap?.collection?.find((c) => c.collectionGroupId == collectionGroup?.id)?.collected || [];
    return _.partition(initialPIC, ({ id }) => collected.includes(id));
  }, [initialPIC, dataMap]);

  const handleChecked = useCallback(
    (id: UUIDString, isCollected: boolean) =>
      updateCollection({ collectionGroupId: collectionGroup.id, id, isCollected }),
    [collectionGroup, updateCollection],
  );

  return (
    <div className="mb-lg">
      {remaining.length > 0 && (
        <Grid container spacing={2}>
          {remaining.map((item) => (
            <Grid
              container
              item
              justifyContent="space-between"
              alignItems="flex-start"
              xs={12}
              sm={6}
              key={`data-collection-review-${item.id}`}
              className="px-md"
            >
              <Grid item className={classes.reviewItemName}>
                {item.name}
              </Grid>
              <Grid item>
                <Button color="primary" onClick={() => handleChecked(item.id, false)}>
                  Remove
                </Button>
              </Grid>
            </Grid>
          ))}
        </Grid>
      )}
      {remaining.length > 0 && removed.length > 0 && <hr className={classes.reviewButtonDivider} />}
      {removed.length > 0 && (
        <Grid container spacing={2}>
          {removed.map((item) => (
            <Grid
              container
              item
              justifyContent="space-between"
              alignItems="flex-start"
              xs={12}
              sm={6}
              key={`data-collection-review-${item.id}`}
              className="px-md"
            >
              <Grid item className={classes.reviewItemName}>
                {item.name}
              </Grid>
              <Grid item>
                <Button color="primary" onClick={() => handleChecked(item.id, true)}>
                  Restore
                </Button>
              </Grid>
            </Grid>
          ))}
        </Grid>
      )}
    </div>
  );
};
