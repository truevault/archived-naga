import { Button, Dialog, DialogActions, DialogContent } from "@mui/material";
import React, { useMemo, useState } from "react";
import { SurveyResponses } from "../../../../common/hooks/useSurvey";
import { Organization } from "../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import {
  CategoryDto,
  DataMapDto,
  groupsFromPIC,
} from "../../../../common/service/server/dto/DataMapDto";
import { OrganizationPublicId } from "../../../../common/service/server/Types";
import { CollectionCheckboxes } from "../../../components/get-compliant/collection-groups/CollectionCheckboxes";
import { SurveyGateButton } from "../../../components/organisms/survey/SurveyGateButton";
import { CloseableDialogTitle } from "../../dialog/CloseableDialogTitle";
import { SurveyBooleanQuestion } from "../../get-compliant/survey/SurveyBooleanQuestion";
import { GDPR_CATEGORY_KEYS } from "./ConsumerDataCollectionForm";
import { EmploymentGroupPurposesQuestion } from "./EmploymentGroupPurposesQuestion";

const EMP_COMMON_KEYS = [
  "name",
  "email-address",
  "postal-address",
  "phone-number",
  "ssn",
  "driver-license-number",
  "california-id-number",
  "passport-number",
  "tax-id-number",
  "military-id-number",
  "other-identifier",
  "non-public-edu-records",
  "professional-info",
  "employment-related-info",
];

const EMP_SENSITIVE_KEYS = [
  "fingerprints",
  "face-hand-palm-vein-patterns",
  "voice-recordings",
  "dna",
  "iris-retina-imagery",
  "keystroke-patterns",
  "health-data",
  "financial-account-creds",
  "financial-account-number",
  "health-insurance-id",
  "account-login",
  "medical-information",
  "protected-class-medical-condition",
  "protected-class-age",
  "protected-class-sex-gender",
  "protected-class-genetic-info",
  "protected-class-familial-status",
];

const EMP_SENSITIVE_GROUP_KEYS = ["other-sensitive-data"];

const EMP_UNCOMMON_EXCLUDED_GROUP_KEYS = [
  "protected-classifications",
  "biometric-info",
  "other-sensitive-data",
];

export const EmployeeDataCollectionForm: React.FC<{
  dataMap: DataMapDto;
  org: Organization;
  collectionGroup: CollectionGroupDetailsDto;
  answers: SurveyResponses;
  setAnswer: (slug: string, answer: string, question?: string) => Promise<void>;
}> = ({ answers, setAnswer, dataMap, org, collectionGroup }) => {
  const orgId = org.id;
  const collected = dataMap.collection.find((c) => c.collectionGroupId == collectionGroup.id);

  // Requests

  const common = useMemo(
    () => dataMap?.categories?.filter((c) => EMP_COMMON_KEYS.includes(c.key)),
    [dataMap?.categories],
  );

  const other = useMemo(
    () =>
      dataMap?.categories?.filter(
        (c) =>
          !EMP_COMMON_KEYS.includes(c.key) &&
          !EMP_SENSITIVE_KEYS.includes(c.key) &&
          !EMP_UNCOMMON_EXCLUDED_GROUP_KEYS.includes(c.picGroupKey) &&
          !GDPR_CATEGORY_KEYS.includes(c.key),
        // !SUPPRESSED_GROUP_KEYS.includes(c.picGroupKey),
      ),
    [dataMap?.categories],
  );

  return (
    <>
      <CollectionCheckboxes
        label={`We’ve listed below the most commonly collected categories of information below. Which of these apply to your ${collectionGroup.name}?`}
        group={collectionGroup}
        orgId={orgId}
        categories={common}
        collected={collected}
        byGroup={false}
      />

      <SurveyGateButton
        slug="emp-common-done"
        survey={answers}
        updateResponse={setAnswer}
        label="Done"
      >
        <SensitiveInfoQuestion
          group={collectionGroup}
          orgId={orgId}
          dataMap={dataMap}
          answers={answers}
          setAnswer={setAnswer}
        >
          <UncommonQuestion
            group={collectionGroup}
            answers={answers}
            setAnswer={setAnswer}
            other={other}
          />
          {answers[UNCOMMON_QUESTION_SLUG] === "true" && (
            <CollectionCheckboxes
              group={collectionGroup}
              orgId={orgId}
              categories={other}
              collected={collected}
            />
          )}

          {Boolean(answers[UNCOMMON_QUESTION_SLUG]) && (
            <EmploymentGroupPurposesQuestion employeeGroup={collectionGroup} />
          )}
        </SensitiveInfoQuestion>
      </SurveyGateButton>
    </>
  );
};

type SensitiveInfoQuestionProps = {
  orgId: OrganizationPublicId;
  group: CollectionGroupDetailsDto;
  answers: SurveyResponses;
  dataMap: DataMapDto;
  setAnswer: (slug: string, answer: string, question?: string) => Promise<void>;
};
const SensitiveInfoQuestion: React.FC<SensitiveInfoQuestionProps> = ({
  group,
  orgId,
  dataMap,
  answers,
  setAnswer,
  children,
}) => {
  if (group.autocreationSlug == "EEA_UK_EMPLOYEE") {
    return (
      <GdprSensitiveInfoQuestion
        group={group}
        orgId={orgId}
        dataMap={dataMap}
        answers={answers}
        setAnswer={setAnswer}
      >
        {children}
      </GdprSensitiveInfoQuestion>
    );
  }
  return (
    <CcpaSensitiveInfoQuestion
      group={group}
      orgId={orgId}
      dataMap={dataMap}
      answers={answers}
      setAnswer={setAnswer}
    >
      {children}
    </CcpaSensitiveInfoQuestion>
  );
};

const GdprSensitiveInfoQuestion: React.FC<SensitiveInfoQuestionProps> = ({
  group,
  orgId,
  dataMap,
  answers,
  setAnswer,
  children,
}) => {
  const healthPic = useMemo(
    () => dataMap.categories.find((c) => c.key == "gdpr-health-or-sex-data"),
    [dataMap.categories],
  );
  const gdprSensitive = useMemo(
    () => dataMap.categories.filter((c) => GDPR_CATEGORY_KEYS.includes(c.key)),
    [dataMap.categories],
  );
  const collected = useMemo(
    () => dataMap.collection.find((c) => c.collectionGroupId == group.id),
    [dataMap, group.id],
  );

  const [healthCollected, setHealthCollected] = useState(
    collected?.collected.includes(healthPic.id),
  );

  const handleChange = (newIds: string[]) => {
    setHealthCollected(newIds.includes(healthPic.id));
  };

  const isFinished = useMemo(() => {
    return (
      !healthCollected ||
      (answers[GDPR_HEALTH_NECESSARY_SLUG] &&
        answers[GDPR_HEALTH_MEDICAL_PURPOSE_SLUG] &&
        answers[GDPR_HEALTH_LEGAL_PROCEEDINGS_SLUG] &&
        answers[GDPR_HEALTH_CONSENT_SLUG])
    );
  }, [
    healthCollected,
    answers[GDPR_HEALTH_NECESSARY_SLUG],
    answers[GDPR_HEALTH_MEDICAL_PURPOSE_SLUG],
    answers[GDPR_HEALTH_LEGAL_PROCEEDINGS_SLUG],
    answers[GDPR_HEALTH_CONSENT_SLUG],
  ]);

  return (
    <>
      <CollectionCheckboxes
        label={`Do you collect any sensitive personal data about ${group.name}?`}
        group={group}
        orgId={orgId}
        categories={gdprSensitive}
        collected={collected}
        byGroup={false}
        allCheckbox={false}
        onChange={handleChange}
      />
      <SurveyGateButton
        slug="emp-sensitive-done"
        survey={answers}
        updateResponse={setAnswer}
        label="Done"
      >
        {healthCollected && (
          <>
            <GdprHealthNecessaryQuestion answers={answers} setAnswer={setAnswer} />
            {Boolean(answers[GDPR_HEALTH_NECESSARY_SLUG]) && (
              <GdprHealthMedicalPurposeQuestion answers={answers} setAnswer={setAnswer} />
            )}
            {Boolean(answers[GDPR_HEALTH_MEDICAL_PURPOSE_SLUG]) && (
              <GdprHealthLegalProceedingsQuestion answers={answers} setAnswer={setAnswer} />
            )}
            {Boolean(answers[GDPR_HEALTH_LEGAL_PROCEEDINGS_SLUG]) && (
              <GdprHealthConsentQuestion answers={answers} setAnswer={setAnswer} />
            )}
          </>
        )}

        {isFinished && children}
      </SurveyGateButton>
    </>
  );
};

const CcpaSensitiveInfoQuestion: React.FC<SensitiveInfoQuestionProps> = ({
  group,
  orgId,
  dataMap,
  answers,
  setAnswer,
  children,
}) => {
  const sensitive = useMemo(
    () =>
      dataMap.categories?.filter(
        (c) =>
          EMP_SENSITIVE_KEYS.includes(c.key) || EMP_SENSITIVE_GROUP_KEYS.includes(c.picGroupKey),
      ),
    [dataMap.categories],
  );
  const collected = useMemo(
    () => dataMap.collection.find((c) => c.collectionGroupId == group.id),
    [dataMap, group.id],
  );
  return (
    <>
      <CollectionCheckboxes
        label={`Do you collect any sensitive personal data about ${group.name}?`}
        group={group}
        orgId={orgId}
        categories={sensitive}
        collected={collected}
        allCheckbox={false}
      />
      <SurveyGateButton
        slug="emp-sensitive-done"
        survey={answers}
        updateResponse={setAnswer}
        label="Done"
      >
        {children}
      </SurveyGateButton>
    </>
  );
};

const UNCOMMON_QUESTION_SLUG = "emp-uncommon-collection";

type QuestionProps = {
  group: CollectionGroupDetailsDto;
  answers: SurveyResponses;
  setAnswer: (slug: string, answer: string, question?: string) => Promise<void>;
};

const UncommonQuestion: React.FC<QuestionProps & { other: CategoryDto[] }> = ({
  group,
  answers,
  setAnswer,
  other,
}) => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const groups = useMemo(() => groupsFromPIC(other), [other]);

  return (
    <>
      <SurveyBooleanQuestion
        answer={answers[UNCOMMON_QUESTION_SLUG]}
        onAnswer={(ans) => setAnswer(UNCOMMON_QUESTION_SLUG, ans)}
        question={{
          type: "boolean",
          slug: UNCOMMON_QUESTION_SLUG,
          question: `Do you collect any uncommon types of personal data about ${group.name}?`,
          helpText: (
            <>
              Such as employee internet activity, search history, fingerprints, or geolocation data?{" "}
              <a
                href="#"
                onClick={(e) => {
                  e.preventDefault();
                  handleOpen();
                }}
              >
                See the full list
              </a>
              .
            </>
          ),
          visible: true,
        }}
      />
      <Dialog fullWidth maxWidth={"md"} className="fullscreen" open={open} onClose={handleClose}>
        <CloseableDialogTitle onClose={handleClose}>
          Uncommon Personal Information Categories
        </CloseableDialogTitle>

        <DialogContent>
          {groups.map((g) => {
            const pics = other.filter((c) => c.picGroupId == g.id);
            return (
              <div key={g.id}>
                <h5>{g.name}</h5>

                <ul>
                  {pics.map((pic) => {
                    return <li key={pic.id}>{pic.name}</li>;
                  })}
                </ul>
              </div>
            );
          })}
        </DialogContent>

        <DialogActions>
          <Button type="submit" color="primary" variant="contained" onClick={handleClose}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

type GDPRQuestionProps = {
  answers: SurveyResponses;
  setAnswer: (slug: string, answer: string, question?: string) => Promise<void>;
};

type HardcodedQuestionProps = {
  answers: SurveyResponses;
  setAnswer: (slug: string, answer: string, question?: string) => Promise<void>;
  slug: string;
  question: string | React.ReactNode;
  helpText?: string | React.ReactNode;
};
const HardcodedQuestion: React.FC<HardcodedQuestionProps> = ({
  answers,
  setAnswer,
  slug,
  question,
  helpText,
}) => {
  return (
    <SurveyBooleanQuestion
      answer={answers[slug]}
      onAnswer={(ans) => setAnswer(slug, ans)}
      question={{
        type: "boolean",
        slug: slug,
        question,
        helpText,
        visible: true,
      }}
    />
  );
};

const GDPR_HEALTH_NECESSARY_SLUG = "gdpr-health-info-necessary";
const GDPR_HEALTH_MEDICAL_PURPOSE_SLUG = "gdpr-health-info-medical-purpose";
const GDPR_HEALTH_LEGAL_PROCEEDINGS_SLUG = "gdpr-health-info-legal-proceedings";
const GDPR_HEALTH_CONSENT_SLUG = "gdpr-health-info-consent";

const GdprHealthNecessaryQuestion: React.FC<GDPRQuestionProps> = ({ answers, setAnswer }) => (
  <HardcodedQuestion
    answers={answers}
    setAnswer={setAnswer}
    slug={GDPR_HEALTH_NECESSARY_SLUG}
    question="Regarding your business’s processing of employee health data, is the processing necessary to enable your business to meet its legal obligations? For example to ensure health and safety at work, or to comply with the requirement not to discriminate against employees on the grounds of sex, age, race or disability?"
  />
);
const GdprHealthMedicalPurposeQuestion: React.FC<GDPRQuestionProps> = ({ answers, setAnswer }) => (
  <HardcodedQuestion
    answers={answers}
    setAnswer={setAnswer}
    slug={GDPR_HEALTH_MEDICAL_PURPOSE_SLUG}
    question="Is the processing for medical purposes, e.g. the provision of care or treatment, and undertaken by a health professional or someone working under an equivalent duty of confidentiality, e.g. an occupational health doctor?"
  />
);
const GdprHealthLegalProceedingsQuestion: React.FC<GDPRQuestionProps> = ({
  answers,
  setAnswer,
}) => (
  <HardcodedQuestion
    answers={answers}
    setAnswer={setAnswer}
    slug={GDPR_HEALTH_LEGAL_PROCEEDINGS_SLUG}
    question="Is the processing in connection with actual or prospective legal proceedings?"
  />
);
const GdprHealthConsentQuestion: React.FC<GDPRQuestionProps> = ({ answers, setAnswer }) => (
  <HardcodedQuestion
    answers={answers}
    setAnswer={setAnswer}
    slug={GDPR_HEALTH_CONSENT_SLUG}
    question="Has the employee given consent explicitly to the processing of his or her medical information?"
    helpText="Answer yes only if the employee knows what data is involved, how it’s used, and has provided a positive indication of agreement, such as a signature. Consent also must be freely given, meaning there are is no penalty if consent is refused."
  />
);
