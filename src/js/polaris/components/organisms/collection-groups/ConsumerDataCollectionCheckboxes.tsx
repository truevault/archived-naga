import _ from "lodash";
import React, { useMemo } from "react";
import { Organization } from "../../../../common/models";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import {
  CategoryDto,
  CollectionDto,
  DataMapDto,
} from "../../../../common/service/server/dto/DataMapDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { CollectionCheckboxes } from "../../../components/get-compliant/collection-groups/CollectionCheckboxes";
import { OrderingVariant } from "../../../util/dataInventory";
import { CollectionGroupHeader } from "./CollectionGroupHeader";

const GDPR_GROUP_NAME = "GDPR Sensitive Data";

export const ConsumerDataCollectionCheckboxes: React.FC<{
  dataMap: DataMapDto;
  org: Organization;
  collectionGroup: CollectionGroupDetailsDto;
  onCollectionChange?: () => Promise<void>;
  orderingVariant: OrderingVariant;
  disabledCategoryIds?: UUIDString[];
  disabledTooltipFn?: (checked: boolean) => string;
  hiddenPicKeys?: string[];
}> = ({
  dataMap,
  org,
  collectionGroup,
  onCollectionChange,
  disabledCategoryIds,
  disabledTooltipFn,
  orderingVariant = "default",
  hiddenPicKeys,
}) => {
  const orgId = org.id;

  const groupedCategories: Record<UUIDString, CategoryDto[]> = useMemo(() => {
    let filteredCategories = dataMap?.categories ?? [];
    if (Array.isArray(hiddenPicKeys)) {
      filteredCategories = filteredCategories.filter((cat) => !hiddenPicKeys.includes(cat.key));
    }
    return _.groupBy(filteredCategories, "picGroupId");
  }, [dataMap]);

  const orderedGroupedCategories: Record<UUIDString, CategoryDto[]> = useMemo(() => {
    return _.transform(
      groupedCategories,
      (result, categories, key) => {
        result[key] =
          orderingVariant === "employee"
            ? _.sortBy(categories, ["picGroupEmployeeOrder", "picGroupName"])
            : _.sortBy(categories, ["picGroupOrder", "picGroupName"]);
      },
      {},
    );
  }, [groupedCategories, orderingVariant]);

  const isGdpr = org?.featureGdpr ?? false;

  const filteredGroups: Record<UUIDString, CategoryDto[]> = useMemo(
    () =>
      _.omitBy(orderedGroupedCategories, (categories) => {
        const firstCat = categories[0];

        if (firstCat.picGroupName === GDPR_GROUP_NAME && !isGdpr) {
          return true;
        }

        return false;
      }),
    [orderedGroupedCategories, isGdpr],
  );

  const collected: CollectionDto = dataMap?.collection?.find(
    (c) => c.collectionGroupId == collectionGroup?.id,
  ) || {
    collected: [],
    collectionGroupId: collectionGroup?.id,
    collectionGroupName: collectionGroup?.name,
    collectionGroupType: collectionGroup?.collectionGroupType,
  };

  return (
    <>
      {_.map(filteredGroups, (categories, groupId) => {
        const cat = categories[0];

        return (
          <React.Fragment key={groupId}>
            <CollectionGroupHeader groupName={cat.picGroupName} />
            <div>
              <CollectionCheckboxes
                byGroup={false}
                key={groupId}
                group={collectionGroup}
                orgId={orgId}
                categories={categories}
                collected={collected}
                onChange={onCollectionChange}
                disabledCategoryIds={disabledCategoryIds}
                disabledTooltipFn={disabledTooltipFn}
                containerClassName="mb-xl ml-mdlg"
              />
            </div>
          </React.Fragment>
        );
      })}
    </>
  );
};
