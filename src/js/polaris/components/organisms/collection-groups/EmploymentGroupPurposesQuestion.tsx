import React from "react";
import { useSurvey } from "../../../../common/hooks/useSurvey";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { usePrimaryOrganizationId } from "../../../hooks";
import { SurveyBooleanQuestion } from "../../get-compliant/survey/SurveyBooleanQuestion";
import { UUIDString } from "../../../../common/service/server/Types";

export const EMPLOYEE_COLLECTION_PURPOSES_SURVEY = "emp-collection-purposes";

export const collectsCommercialInfoSlug = (cg: CollectionGroupDetailsDto) =>
  `does-collect-${cg?.id}`;

export const collectsCommercialInfoSlugById = (id: UUIDString) => `does-collect-${id}`;

type PurposesStepProps = {
  employeeGroup: CollectionGroupDetailsDto;
};

export const EmploymentGroupPurposesQuestion: React.FC<PurposesStepProps> = ({ employeeGroup }) => {
  const organizationId = usePrimaryOrganizationId();
  const qSlug = collectsCommercialInfoSlug(employeeGroup);

  const { answers, setAnswer } = useSurvey(organizationId, EMPLOYEE_COLLECTION_PURPOSES_SURVEY);
  const answer = answers[qSlug];
  const questionText = `Does your business use personal information about ${employeeGroup.name} for commercial purposes?`;

  return (
    <SurveyBooleanQuestion
      answer={answer}
      onAnswer={(ans) => setAnswer(qSlug, ans, questionText)}
      question={{
        type: "boolean",
        slug: qSlug,
        question: questionText,
        helpText:
          "Answer ‘yes’ if your business actively markets to its employees, such as by adding them to its marketing lists.",
        visible: true,
      }}
    />
  );
};
