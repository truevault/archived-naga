import React, { useEffect, useState } from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import {
  BusinessPurposeDto,
  OrgPersonalInformationSnapshotDto,
  PersonalInformationCollectedDto,
} from "../../../../common/service/server/dto/PersonalInformationOptionsDto";
import { OrganizationPublicId } from "../../../../common/service/server/Types";
import { useCollectionGroupPurposesDetails } from "../../../hooks/useCollectionGroups";
import { nonEmploymentBusinessPurposes } from "../../../hooks/useDataMap";
import { toggleSelection } from "../../../surveys/util";
import { CollectionPurposesMultiselect } from "./CollectionPurposesMultiselect";

export const CollectionPurposesForGroup: React.FC<{
  organizationId: OrganizationPublicId;
  collectionGroup: CollectionGroupDetailsDto;
  defaultCollected: PersonalInformationCollectedDto;
  piSnapshot: OrgPersonalInformationSnapshotDto;
  setAnySelected?: (anyPurposesSelected: boolean) => void;
}> = ({ organizationId, collectionGroup, defaultCollected, piSnapshot, setAnySelected }) => {
  const {
    purposesForGroup,
    actions: { removePurpose, addPurpose },
  } = useCollectionGroupPurposesDetails(
    organizationId,
    collectionGroup,
    piSnapshot,
    defaultCollected,
  );

  const [selectedPurposeIds, setSelectedPurposeIds] = useState(() =>
    purposesForGroup.map((p) => p.id),
  );

  const handleChange = async (bp: BusinessPurposeDto, selected: boolean) => {
    setSelectedPurposeIds((prev) => toggleSelection(prev, bp.id, selected));

    if (selected) {
      await addPurpose(bp.id);
    } else {
      await removePurpose(bp.id);
    }
  };

  const relevantPurposes = nonEmploymentBusinessPurposes(defaultCollected.businessPurposes);
  const selectedPurposes = relevantPurposes.filter((p) => selectedPurposeIds.includes(p.id));
  const anySelected = Boolean(selectedPurposes.length);

  // update the outside world when any purposes are selected or deselected
  useEffect(() => {
    Boolean(setAnySelected) && setAnySelected(anySelected);
  }, [anySelected]);

  return (
    <CollectionPurposesMultiselect
      collectionPurposes={relevantPurposes}
      selected={selectedPurposes}
      onChange={handleChange}
    />
  );
};
