import { Typography } from "@mui/material";
import React from "react";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { ToggleButton } from "../../buttons/ToggleButton";

interface Props {
  groupId: UUIDString;
  included: boolean;
}
export const CollectionGroupMultiSelect: React.FC<{
  collectionGroups: CollectionGroupDetailsDto[];
  selected: string[];
  onChange: ({ groupId, included }: Props) => void;
}> = ({ collectionGroups, selected, onChange }) => {
  return (
    <ul className="vertical-list">
      {collectionGroups.map((group) => (
        <li key={group.id} className="mt-mdlg">
          <ToggleButton
            checked={selected.some((s) => s === group.id)}
            onChange={(included) => onChange({ groupId: group.id, included })}
          >
            <div>
              <Typography>{group.name}</Typography>
              {Boolean(group.description) && (
                <Typography variant="caption">
                  <strong>{group.name} are people who:</strong> {group.description}
                </Typography>
              )}
            </div>
          </ToggleButton>
        </li>
      ))}
    </ul>
  );
};
