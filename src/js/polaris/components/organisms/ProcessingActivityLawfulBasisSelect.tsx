import { SelectProps } from "@mui/material";
import _ from "lodash";
import React, { useMemo } from "react";
import { SelectOption, SelectWithRecommendations } from "../../../common/components/input/Select";
import {
  LawfulBasis,
  ProcessingActivityDto,
} from "../../../common/service/server/dto/ProcessingActivityDto";
import {
  labelForLawfulBasis,
  useMandatoryLawfulBasisOptions,
  useRecommendedLawfulBasisOptions,
} from "../../hooks/useProcessingActivities";

const BASE_LAWFUL_BASIS_OPTIONS: LawfulBasis[] = [
  "COMPLY_WITH_LEGAL_OBLIGATIONS",
  "FULFILL_CONTRACTS",
  "LEGITIMATE_INTERESTS",
  "CONSENT",
];

const EXTENDED_LAWFUL_BASIS_OPTIONS: LawfulBasis[] = [
  ...BASE_LAWFUL_BASIS_OPTIONS,
  "PUBLIC_INTEREST",
  "VITAL_INTEREST_OF_INDIVIDUAL",
];

export const ProcessingActivityLawfulBasisSelect: React.FC<
  {
    activity?: ProcessingActivityDto;
    value: LawfulBasis[];
    onChange: (newBases: LawfulBasis[]) => Promise<void>;
    showExtendedOptions?: boolean;
  } & Partial<Omit<SelectProps, "onChange">>
> = ({ activity, value, onChange, showExtendedOptions = false, ...rest }) => {
  const availableMenuOptions: LawfulBasis[] = showExtendedOptions
    ? EXTENDED_LAWFUL_BASIS_OPTIONS
    : BASE_LAWFUL_BASIS_OPTIONS;

  // Ensure that we always include the mandatory lawful bases in the dropdown,
  // in case an extended option is present in the mandatory items
  const finalMenuOptions: LawfulBasis[] = useMemo(
    () => _.union(availableMenuOptions, activity?.defaultMandatoryLawfulBases ?? []),
    [availableMenuOptions, activity],
  );

  const finalOptions: SelectOption[] = finalMenuOptions.map((o) => ({
    value: o,
    label: labelForLawfulBasis(o),
  }));

  const mandatoryOptions: SelectOption[] = useMandatoryLawfulBasisOptions(activity);
  const recommendedOptions: SelectOption[] = useRecommendedLawfulBasisOptions(activity);

  const handleChange = (value: any) => {
    // On autofill we get a stringified value.
    const bases: LawfulBasis[] =
      typeof value === "string" ? (value.split(",") as LawfulBasis[]) : (value as LawfulBasis[]);

    onChange(bases);
  };

  return (
    <SelectWithRecommendations
      options={finalOptions}
      value={value}
      mandatory={mandatoryOptions}
      recommendations={recommendedOptions}
      onChange={handleChange}
      multiple
      {...rest}
    />
  );
};
