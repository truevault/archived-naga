import React, { useEffect, useMemo, useState } from "react";
import { useCollectionDetails, useSourcedDetails } from "../../../../common/hooks/dataMapHooks";
import { Api } from "../../../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { DataMapDto } from "../../../../common/service/server/dto/DataMapDto";
import { OrganizationDataSourceDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationPublicId, UUIDString } from "../../../../common/service/server/Types";
import { DescriptionCheckbox } from "../../../components/input/DescriptionCheckbox";
import { toggleSelection } from "../../../surveys/util";

export const DataRecipientSourceCheckboxes: React.FC<{
  orgId: OrganizationPublicId;
  group: CollectionGroupDetailsDto;
  source: OrganizationDataSourceDto;
  dataMap: DataMapDto;
  setHasSelection: (hasSelection: boolean) => void;
  includeSelectAll: boolean;
}> = ({
  orgId,
  dataMap,
  group: collectionGroup,
  source: dataRecipient,
  setHasSelection,
  includeSelectAll = false,
}) => {
  const { collectedPic } = useCollectionDetails(dataMap, collectionGroup);
  const { sourcedPic } = useSourcedDetails(dataMap, collectionGroup, dataRecipient);

  const [selected, setSelected] = useState(() => sourcedPic.map((g) => g.id));

  const handleChange = (picId: UUIDString, checked: boolean) => {
    setSelected((oldSelected) => toggleSelection(oldSelected, picId, checked));
    Api.dataMap.updateReceived(
      orgId,
      collectionGroup.id,
      dataRecipient.vendorId,
      picId,
      checked,
      false,
    );
  };

  useEffect(() => {
    setHasSelection(selected.length > 0);
    // Set Has Selection is unstable and causes an infinite loop
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selected]);

  const allSelected = useMemo(
    () => collectedPic.every((pic) => selected.includes(pic.id)),
    [selected, collectedPic],
  );

  const handleSelectAll = async () => {
    // Toggle all the remote selected state
    await Promise.all(collectedPic.map((pic) => handleChange(pic.id, !allSelected)));
  };

  return (
    <>
      {includeSelectAll && (
        <DescriptionCheckbox
          className="mb-md p-sm"
          label={"Select All"}
          checked={allSelected}
          onChange={handleSelectAll}
        />
      )}

      {collectedPic.map((pic) => {
        return (
          <DescriptionCheckbox
            className="my-sm p-sm"
            key={pic.id}
            label={pic.name}
            checked={selected.includes(pic.id)}
            onChange={(_, checked) => handleChange(pic.id, checked)}
          />
        );
      })}
    </>
  );
};
