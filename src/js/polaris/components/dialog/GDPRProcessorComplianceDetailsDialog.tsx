import React from "react";
import { HelpDialog } from "./HelpDialog";

interface Props {
  open: boolean;
  onClose: () => void;
}

export const GDPRProcessorComplianceDetailsDialog: React.FC<Props> = ({ open, onClose }) => {
  return (
    <HelpDialog
      open={open}
      onClose={onClose}
      title="GDPR Processor Compliance Details"
      disableBackdropClick={true}
      className="intro-help-dialog"
      maxWidth="md"
    >
      <DialogBody />
    </HelpDialog>
  );
};

const DialogBody = () => {
  return (
    <>
      <p>Contracts with GDPR processors must require the processor to do the following:</p>

      <ul>
        <li>Process the personal data only on the controller's documented instructions.</li>
        <li>
          Ensure that people authorized to access the data are bound by confidentiality agreements.
        </li>
        <li>
          Implement appropriate technical and organizational measures to ensure the security of the
          personal data
        </li>
        <li>
          Engage a subprocessor only with the written authorization of the controller and only if
          the subprocessor is bound by these same rules
        </li>
        <li>
          Assist the controller by appropriate technical and organizational measures to respond to
          privacy requests
        </li>
        <li>
          Assist the controller in its security obligations and any prior consultation with data
          protection authorities, if required
        </li>
        <li>Delete or return personal data once it has finished providing its services.</li>
        <li>
          Make available to the controller all information necessary to demonstrate GDPR compliance
        </li>
      </ul>
    </>
  );
};
