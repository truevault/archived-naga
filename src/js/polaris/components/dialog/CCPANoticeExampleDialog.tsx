import { Button } from "@mui/material";
import React from "react";
import { HelpDialog } from "./HelpDialog";

interface Props {
  open: boolean;
  onClose: () => void;
}

export const CCPANoticeExampleDialog: React.FC<Props> = ({ open, onClose }) => {
  return (
    <HelpDialog
      open={open}
      onClose={onClose}
      title={"CCPA Notice Example"}
      actions={
        <Button variant="contained" color="primary" onClick={onClose}>
          Done
        </Button>
      }
      disableBackdropClick={true}
      className="intro-help-dialog"
      maxWidth="lg"
    >
      <DialogBody />
    </HelpDialog>
  );
};

const DialogBody = () => {
  return (
    <>
      <p>Data collection scenarios will be used in your CCPA Notice. For example:</p>
      <div className="mt-md p-md bg-gray">
        <h4 className="intermediate mt-0">Personal Information Collected in the Last 12 Months</h4>

        <p>
          We collect the following categories of personal information about{" "}
          <u>people who shop online or visit our website</u>:
        </p>

        <ul>
          <li>
            Personal Identifiers, including name, email address, postal address, and telephone
            number
          </li>
          <li>
            Online Identifiers, including unique personal identifier, device identifier, and IP
            address
          </li>
          <li>Internet Activity, including interactions with websites, apps or ads</li>
          <li>Commercial and Financial Information, including purchases</li>
        </ul>

        <p>
          We collect the following categories of personal information about{" "}
          <u>people who subscribe to marketing emails</u>:
        </p>

        <ul>
          <li>Personal Identifiers, including name, email address, and telephone number</li>
          <li>Online Identifiers, including unique personal identifier</li>
          <li>
            Internet Activity, including interactions with websites, apps or ads and email open and
            click-through rates
          </li>
        </ul>

        <p>
          We collect the following categories of personal information about{" "}
          <u>people who shop at our California stores</u>:
        </p>

        <ul>
          <li>Personal Identifiers, including name, email address, and telephone number</li>
        </ul>
      </div>
    </>
  );
};
