import { Button } from "@mui/material";
import React from "react";
import { HelpDialog } from "./HelpDialog";

interface Props {
  open: boolean;
  onClose: () => void;
}

export const ScheduleTrainingDialog: React.FC<Props> = ({ open, onClose }) => {
  return (
    <HelpDialog
      open={open}
      onClose={onClose}
      title={"Welcome!"}
      actions={
        <Button variant="contained" color="primary" onClick={onClose}>
          Done
        </Button>
      }
      className="intro-help-dialog"
      maxWidth="md"
    >
      <DialogBody />
    </HelpDialog>
  );
};

const DialogBody = () => {
  return (
    <>
      <p>
        Welcome to your privacy dashboard! Your Data Map and Privacy Policy are on the left. When
        you receive your first actionable privacy request, we’ll email you to schedule a request
        walkthrough call.
      </p>
    </>
  );
};
