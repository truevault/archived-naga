import React from "react";
import { HelpInterstitialDialog } from "../dialog";

export const VendorHelp: React.FC = () => {
  return (
    <HelpInterstitialDialog
      uniqueId="vendor-help"
      title="Adding Vendors"
      closeActionText="Get Started"
    >
      <p>
        In this step, you'll create a list of the Vendors your business uses. You will use this list
        of vendors in the Data Map section to map your data collection practices, which form the
        basis of your privacy disclosures.
      </p>
      <h3>Don't include every vendor your business uses</h3>
      <p>
        Only include vendors that handle non-aggregated consumer personal information—either because
        the vendor collects it, stores it, or processes it in some way.
      </p>
      <p>Don't include vendors who do not handle consumer personal information.</p>
      <p>
        Try to build a complete list before moving on to the next section. You can go back and edit
        the vendor list at any point, but it’s much easier if you have all of the vendors ready when
        starting the Data Map.
      </p>
    </HelpInterstitialDialog>
  );
};
