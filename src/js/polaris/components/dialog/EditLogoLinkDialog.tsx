import { Button, Dialog, DialogActions, DialogContent } from "@mui/material";
import React from "react";
import { Form } from "react-final-form";
import { Disableable } from "..";
import { Text } from "../../../common/components/input/Text";
import { requiredUrl } from "../../util/validation";
import { StandardLabel } from "../input/Labels";
import { Adornment } from "../input/Adornment";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

type EditLogoLinkDialogProps = {
  open: boolean;
  onClose: () => void;
  onDone: (logoLinkUrl: string) => void;
  linkUrl: string;
};

export const EditLogoLinkDialog: React.FC<EditLogoLinkDialogProps> = ({
  open,
  onClose,
  onDone,
  linkUrl,
}) => {
  const handleSubmit = (e) => {
    onClose();
    onDone(e.logoLinkUrl);
  };

  const closeIfValid = (isValid) => {
    if (isValid) onClose();
  };

  const validate = (values: any) => {
    const errors = {} as { [x: string]: string };
    requiredUrl(values, "logoLinkUrl", errors);
    return errors;
  };

  return (
    <Dialog className="logo-upload-dialog" fullWidth maxWidth={"sm"} open={open}>
      <Form
        onSubmit={handleSubmit}
        validate={validate}
        render={({ handleSubmit, valid }) => {
          return (
            <form onSubmit={handleSubmit}>
              <CloseableDialogTitle
                closeDisabled={!valid}
                disabledTooltip="Please enter a URL to proceed."
                onClose={() => closeIfValid(valid)}
              >
                Logo link URL
              </CloseableDialogTitle>

              <DialogContent>
                <StandardLabel
                  label="URL"
                  helpText={"When a user clicks your logo on the Privacy Center"}
                />
                <Text
                  multiline={false}
                  variant="outlined"
                  field="logoLinkUrl"
                  initialValue={linkUrl}
                  InputProps={{
                    startAdornment: <Adornment position="start">https://</Adornment>,
                  }}
                  required
                  autoFocus
                />
              </DialogContent>

              <DialogActions>
                <Disableable disabledTooltip="Please enter a URL to proceed." disabled={!valid}>
                  <Button type="submit" color="primary" disabled={!valid} variant="contained">
                    Update URL
                  </Button>
                </Disableable>
              </DialogActions>
            </form>
          );
        }}
      />
    </Dialog>
  );
};
