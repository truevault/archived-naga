import _ from "lodash";
import { Button, Dialog, DialogActions, DialogContent, Stack } from "@mui/material";
import React, { useMemo, useState } from "react";
import { useAllCollectedPIC } from "../../../common/hooks/dataMapHooks";
import { Api } from "../../../common/service/Api";
import { usePrimaryOrganization } from "../../hooks";
import { useDataMap } from "../../hooks/useDataMap";
import {
  useCustomActivityCheckboxes,
  useDefaultActivityCheckboxes,
  useProcessingActivites,
} from "../../hooks/useProcessingActivities";
import {
  CollectionPurposeCollapseCard,
  CustomPA,
} from "../../pages/get-compliant/consumer-collection/CollectionPurposes";
import { Callout, CalloutVariant } from "../Callout";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

type GDPRProcessingActivityDialogProps = {
  open: boolean;
  onClose: () => void;
};

export const GDPRProcessingActivitiesDialog: React.FC<GDPRProcessingActivityDialogProps> = ({
  open,
  onClose,
}) => {
  const [org] = usePrimaryOrganization();
  const orgId = org.id;

  const [dataMap] = useDataMap(orgId);
  // const groupTypes = consumerGroups.map((cg) => cg.collectionGroupType);
  const collectedPic = useAllCollectedPIC(dataMap);

  const [activities, __, refreshActivities, assocationMutation] = useProcessingActivites(orgId);

  // CUSTOM ACTIVITIES
  const custom = useMemo(() => activities.filter((a) => !a.defaultId), [activities]);
  const customCheckboxes = useCustomActivityCheckboxes(
    orgId,
    custom,
    "EEA_UK",
    ["EEA_UK"],
    refreshActivities,
  );

  const [addCustomLoading, setAddCustomLoading] = useState(false);
  const handleAddCustomActivity = async (name: string) => {
    if (activities.map((a) => a.name).includes(name)) return;

    setAddCustomLoading(true);
    try {
      await Api.processingActivities.createProcessingActivity(orgId, {
        name: name,
        defaultId: undefined,
        lawfulBases: undefined,
        regions: ["EEA_UK"],
      });
      await refreshActivities();
    } finally {
      setAddCustomLoading(false);
    }
  };

  const handleRemoveCustomActivity = async (id: string) => {
    await Api.processingActivities.deleteProcessingActivityRegion(orgId, id, ["EEA_UK"]);
    await refreshActivities();
  };
  const defCheckboxes = useDefaultActivityCheckboxes(
    orgId,
    activities,
    "EEA_UK",
    ["EEA_UK"],
    refreshActivities,
  );

  const usDefaults = useMemo(
    () =>
      defCheckboxes.props.defaults.filter((def) =>
        activities.find((a) => a.regions.includes("UNITED_STATES") && a.defaultId == def.id),
      ),
    [defCheckboxes, activities],
  );
  const nonusDefaults = useMemo(
    () =>
      defCheckboxes.props.defaults.filter((def) => !usDefaults.find((usDef) => usDef.id == def.id)),
    [defCheckboxes, usDefaults],
  );

  const [usCustoms, nonUsCustoms] = _.partition(customCheckboxes.props.customActivities, (a) =>
    a.regions.includes("UNITED_STATES"),
  );

  return (
    <>
      <Dialog
        className="get-compliant-dialog"
        fullWidth={true}
        maxWidth="md"
        open={open}
        onClose={onClose}
        aria-labelledby="invite-user-dialog-title"
      >
        <CloseableDialogTitle onClose={onClose}>
          Data Processing Activities for GDPR Countries
        </CloseableDialogTitle>

        <DialogContent>
          <p>
            If your data processing activities differ for countries under GDPR, you can update the
            list of activities to reflect only what applies to the EEA/UK.
          </p>

          <Callout variant={CalloutVariant.Yellow} className="mt-md">
            <p>
              Removing or adding data processing activities here only applies to GDPR countries.
            </p>
            <p>
              To make changes to your overall data processing activies, go to Collection Purposes
            </p>
          </Callout>

          <div className="text-h5 mt-xl">Overall Processing Activities</div>
          <div className="text-t1">
            These are processing activities you've already selected for US regions. Including them
            here will apply them to both US and GDPR Regions.
          </div>
          <Stack spacing={2} className="ml-lg mt-xl">
            {usDefaults?.map((defActivity) => {
              return (
                <CollectionPurposeCollapseCard
                  key={defActivity.id}
                  defaultActivity={defActivity}
                  activities={activities}
                  checked={defCheckboxes.props.checked}
                  setChecked={defCheckboxes.props.setChecked}
                  categories={collectedPic}
                  assocationMutation={assocationMutation}
                  collapsible={false}
                  readonly={false}
                  variant="text"
                />
              );
            })}
          </Stack>

          <Stack spacing={2} className="ml-lg mt-md">
            {usCustoms?.map((a) => {
              return (
                <div key={a.id}>
                  <CollectionPurposeCollapseCard
                    activity={a}
                    activities={usCustoms}
                    checked={customCheckboxes.props.checked}
                    setChecked={customCheckboxes.props.setChecked}
                    categories={collectedPic}
                    assocationMutation={assocationMutation}
                    readonly={false}
                    collapsible={false}
                    variant="text"
                  />
                </div>
              );
            })}
          </Stack>

          <div className="text-h5 mt-xl">Additional Processing Activities</div>
          <div className="text-t1"> These only apply to GDPR regions. </div>

          <Stack spacing={2} className="ml-lg mt-xl">
            {nonusDefaults?.map((defActivity) => {
              return (
                <CollectionPurposeCollapseCard
                  key={defActivity.id}
                  defaultActivity={defActivity}
                  activities={activities}
                  checked={defCheckboxes.props.checked}
                  setChecked={defCheckboxes.props.setChecked}
                  categories={collectedPic}
                  assocationMutation={assocationMutation}
                  readonly={false}
                  collapsible={false}
                  variant="text"
                />
              );
            })}
          </Stack>

          <h5>Add custom activities</h5>

          <CustomPA
            custom={nonUsCustoms}
            categories={collectedPic}
            variant="text"
            associationMutation={assocationMutation}
            onAdd={handleAddCustomActivity}
            addLoading={addCustomLoading}
            onRemove={handleRemoveCustomActivity}
            indentAddRow
          />
        </DialogContent>

        <DialogActions>
          <Button variant="contained" onClick={onClose}>
            Cancel
          </Button>
          <Button color="primary" variant="contained" onClick={onClose}>
            Save Changes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
