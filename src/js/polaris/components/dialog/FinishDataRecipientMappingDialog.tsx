import { Button } from "@mui/material";
import React from "react";
import { HelpDialog } from "./HelpDialog";

interface Props {
  open: boolean;
  onClose: () => void;
}

export const FinishDataRecipientMappingDialog: React.FC<Props> = ({ open, onClose }) => {
  return (
    <HelpDialog
      open={open}
      onClose={onClose}
      title={"Finish mapping your new Data Recipients!"}
      actions={
        <Button variant="contained" color="primary" onClick={onClose}>
          Done
        </Button>
      }
      className="intro-help-dialog"
      maxWidth="md"
    >
      <DialogBody />
    </HelpDialog>
  );
};

const DialogBody = () => {
  return (
    <>
      <p>
        Nice work! Your new Data Recipients have been added to your list below. The next step is to
        click into each ‘Incomplete’ Data Recipient and complete any remaining data mapping steps.
      </p>
    </>
  );
};
