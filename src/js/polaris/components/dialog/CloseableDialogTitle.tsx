import { DialogTitleProps } from "@mui/material";
import { Close } from "@mui/icons-material";
import { DialogTitle, IconButton, Typography } from "@mui/material";
import React from "react";
import { Disableable } from "../Disableable";

type CloseableDialogTitleProps = DialogTitleProps & {
  subtitle?: React.ReactNode;
  onClose?: (() => void) | (() => Promise<void>);
  closeDisabled?: boolean;
  disabledTooltip?: React.ReactNode;
};

export const CloseableDialogTitle: React.FC<CloseableDialogTitleProps> = ({
  children,
  onClose,
  subtitle,
  disabledTooltip,
  closeDisabled = false,
  ...rest
}) => (
  <DialogTitle {...rest}>
    <div>
      {children}

      {typeof subtitle === "string" ? (
        <Typography variant="subtitle2" component="span" style={{ display: "block" }}>
          {subtitle}
        </Typography>
      ) : (
        <>{subtitle}</>
      )}
    </div>

    {onClose && (
      <Disableable disabledTooltip={disabledTooltip} disabled={closeDisabled}>
        <IconButton aria-label="close" onClick={onClose} disabled={closeDisabled} size="small">
          <Close />
        </IconButton>
      </Disableable>
    )}
  </DialogTitle>
);
