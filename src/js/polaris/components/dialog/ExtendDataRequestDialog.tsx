import { Button, Dialog, DialogActions, DialogContent } from "@mui/material";
import React, { useEffect } from "react";
import { Forms } from "..";
import { Loading } from "../../../common/components/Loading";
import { DataRequestDetail } from "../../../common/models";
import { NetworkRequestState } from "../../../common/models/NetworkRequest";
import { Error as RequestsError } from "../../copy/requests";
import { Error } from "../Error";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

type ExtendDataRequestDialogProps = {
  open: boolean;
  request: DataRequestDetail;
  onClose: () => void;
  onDone: () => void;
  updateRequestState: NetworkRequestState;
  handleSubmit: (values: any) => void;
};

export const ExtendDataRequestDialog = ({
  open,
  request,
  onClose,
  onDone,
  updateRequestState,
  handleSubmit,
}: ExtendDataRequestDialogProps) => {
  useEffect(() => {
    if (updateRequestState.success) {
      onDone();
    }
  }, [updateRequestState.success, onDone]);

  return (
    <Dialog
      maxWidth="lg"
      open={open}
      onClose={onClose}
      aria-labelledby="extend-request-dialog-title"
      className={"extend-request-dialog"}
    >
      <CloseableDialogTitle onClose={onClose}>Extend Due Date</CloseableDialogTitle>

      {updateRequestState.running && (
        <DialogContent>
          <Loading />
        </DialogContent>
      )}

      {updateRequestState.error && (
        <>
          <DialogContent>
            <Error message={updateRequestState.errorMessage || RequestsError.UpdateRequest} />
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose}>Close</Button>
          </DialogActions>
        </>
      )}

      {!updateRequestState.started && (
        <Forms.ExtendDataRequest onSubmit={handleSubmit} onClose={onClose} request={request} />
      )}
    </Dialog>
  );
};
