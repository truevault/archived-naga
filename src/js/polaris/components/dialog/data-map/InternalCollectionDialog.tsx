import { Button, Dialog, DialogActions, DialogContent } from "@mui/material";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useActionRequest, useDataRequest } from "../../../../common/hooks/api";
import { Api } from "../../../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { usePrimaryOrganizationId, usePrimaryOrganization } from "../../../hooks/useOrganization";
import { isEmploymentGroup } from "../../../util/consumerGroups";
import { MapFields } from "../../MapFields";
import { CloseableDialogTitle } from "../CloseableDialogTitle";

type InternalCollectionDialogProps = {
  consumerGroup: CollectionGroupDetailsDto;
  open: boolean;
  onClose: () => void;
};

export const InternalCollectionDialog = ({
  open,
  onClose,
  consumerGroup,
}: InternalCollectionDialogProps) => {
  const organizationId = usePrimaryOrganizationId();
  const [org] = usePrimaryOrganization();

  const [shared, setShared] = useState<Set<string>>(new Set([]));
  const [collected, setCollected] = useState<Set<string>>(new Set([]));

  const defaultCollectedApi = useCallback(
    () => Api.dataInventory.getDefaultPersonalInformationCollected(organizationId),
    [organizationId],
  );
  const { result: defaultCollected } = useDataRequest({
    queryKey: ["defaultCollected", organizationId],
    api: defaultCollectedApi,
  });

  const piSnapshotApi = useCallback(
    () => Api.dataInventory.getSnapshot(organizationId),
    [organizationId],
  );
  const { result: personalInformationSnapshot, refresh: snapshotRefresh } = useDataRequest({
    queryKey: ["piSnapshot", organizationId],
    api: piSnapshotApi,
  });

  const addCategoryApi = useCallback(
    async ({ consumerGroupId, categoryId }) =>
      Api.dataInventory.addPersonalInformationCategoryCollected(
        organizationId,
        consumerGroupId,
        categoryId,
      ),
    [organizationId],
  );
  const { fetch: addCategory } = useActionRequest({
    api: addCategoryApi,
  });

  const removeCategoryApi = useCallback(
    async ({ consumerGroupId, categoryId }) =>
      Api.dataInventory.removePersonalInformationCategoryCollected(
        organizationId,
        consumerGroupId,
        categoryId,
      ),
    [organizationId],
  );

  const { fetch: removeCategory } = useActionRequest({
    api: removeCategoryApi,
  });

  useEffect(() => {
    if (open) {
      snapshotRefresh();
    }
  }, [open]);

  const snapshot = useMemo(() => {
    return personalInformationSnapshot?.dataInventory?.find(
      (invSnap) => invSnap.dataSubjectTypeId == consumerGroup.id,
    );
  }, [personalInformationSnapshot]);

  useEffect(() => {
    const sharedArr =
      snapshot?.personalInformationCategories?.filter((pi) => pi.exchanged)?.map((pi) => pi.id) ||
      [];
    setShared(new Set(sharedArr));

    const collectedArr =
      snapshot?.personalInformationCategories
        ?.filter((pi) => pi.collected && !pi.exchanged)
        ?.map((pi) => pi.id) || [];
    setCollected(new Set(collectedArr));
  }, [personalInformationSnapshot, consumerGroup, setCollected, setShared]);

  const handleChange = (id, enabled) => {
    const ids = [...collected];
    if (enabled && !collected.has(id)) {
      ids.push(id);
      setCollected(new Set(ids));
      addCategory({ consumerGroupId: consumerGroup.id, categoryId: id });
    }

    if (!enabled && collected.has(id)) {
      const idx = ids.findIndex((arrId) => id == arrId);
      ids.splice(idx, 1);
      setCollected(new Set(ids));
      removeCategory({ consumerGroupId: consumerGroup.id, categoryId: id });
    }
  };

  if (!defaultCollected) {
    return null;
  }

  const employment = isEmploymentGroup(consumerGroup);

  return (
    <Dialog fullWidth maxWidth={"lg"} className="fullscreen" open={open} onClose={onClose}>
      <CloseableDialogTitle
        onClose={onClose}
        subtitle={
          employment ? (
            <>
              <p className="text-t3 text-style-normal">
                What information does your business collect about {consumerGroup.name}?
              </p>

              <p className="text-t3 text-style-normal">
                Exclude any PI that’s collected specifically when the employee falls into another
                group, such as Customers.
              </p>
            </>
          ) : (
            `If your business collects any additional personal information that it does not
                store, access, or share with any vendors or systems, add it below.`
          )
        }
      >
        {employment ? (
          <>{consumerGroup.name}</>
        ) : (
          <>{consumerGroup.name}: Undisclosed Data (Internal Use)</>
        )}
      </CloseableDialogTitle>

      <DialogContent>
        <MapFields
          organization={org}
          consumerGroup={consumerGroup}
          personalInformationCategories={defaultCollected.personalInformationCategories}
          sharedCategories={collected}
          lockedInCategories={shared}
          showNoDataDisclosed={false}
          updateSingle={handleChange}
          orderingVariant={employment ? "employee" : null}
        />
      </DialogContent>

      <DialogActions>
        <Button type="submit" color="primary" variant="contained" onClick={onClose}>
          Done
        </Button>
      </DialogActions>
    </Dialog>
  );
};
