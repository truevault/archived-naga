import { Button, Dialog, DialogActions, DialogContent } from "@mui/material";
import _ from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import { useActionRequest, useDataRequest } from "../../../../common/hooks/api";
import { Api } from "../../../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { OrganizationDataRecipientDto } from "../../../../common/service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../../../../common/service/server/Types";
import { Error as DataInventoryError } from "../../../copy/dataInventory";
import { usePrimaryOrganizationId, usePrimaryOrganization } from "../../../hooks/useOrganization";
import { MapFields } from "../../MapFields";
import { CloseableDialogTitle } from "../CloseableDialogTitle";

type MapVendorFieldsDialogProps = {
  consumerGroup: CollectionGroupDetailsDto;
  vendor: OrganizationDataRecipientDto;
  open: boolean;
  onClose: () => void;
};

export const MapVendorFieldsDialog = ({
  open,
  onClose,
  consumerGroup,
  vendor,
}: MapVendorFieldsDialogProps) => {
  const organizationId = usePrimaryOrganizationId();
  const [org] = usePrimaryOrganization();

  const [ready, setReady] = useState(false);
  const [sharedCategories, setSharedCategories] = useState<Record<UUIDString, Set<UUIDString>>>({});
  const [noDataDisclosed, setNoDataDisclosed] = useState<Record<UUIDString, boolean | null>>({});

  const exchangeApi = useCallback(
    () => Api.dataInventory.getPersonalInformationExchangedVendor(organizationId, vendor.vendorId),
    [organizationId, vendor.vendorId],
  );

  const {
    result: personalInformationExchanged,
    request: getExchangedRequest,
    refresh: runExchange,
  } = useDataRequest({
    queryKey: ["personalInformationExchanged", organizationId, vendor.vendorId],
    api: exchangeApi,
    notReady: !vendor.vendorId || !open,
  });

  // Effects
  useEffect(() => {
    if (getExchangedRequest.success) {
      setSharedCategories(
        personalInformationExchanged.reduce((acc, exch) => {
          const shared = new Set(
            exch.personalInformationCategories
              .filter(({ exchanged }) => exchanged)
              .map(({ id }) => id),
          );
          return Object.assign({}, acc, { [exch.dataSubjectTypeId]: shared });
        }, {} as Record<UUIDString, Set<UUIDString>>),
      );

      setNoDataDisclosed(
        personalInformationExchanged.reduce((acc, exch) => {
          return Object.assign({}, acc, { [exch.dataSubjectTypeId]: exch.noDataMapping });
        }, {} as Record<UUIDString, boolean | null>),
      );
      setReady(true);
    }
  }, [personalInformationExchanged, getExchangedRequest]);

  const updatePiApi = useCallback(
    async ({ categoryIds, consumerGroupId, noDataMapping, resetState }) => {
      try {
        const resp = Api.dataInventory.setPersonalInformationExchanged(
          organizationId,
          vendor.vendorId,
          [
            {
              dataSubjectTypeId: consumerGroupId,
              personalInformationCategories: categoryIds,
              noDataMapping,
            },
          ],
        );
        const { ok } = await resp;
        if (!ok) {
          resetState();
        }
      } catch (err) {
        resetState();
        throw err;
      }
    },
    [organizationId, vendor],
  );

  const { fetch: updatePersonalInformationShared } = useActionRequest({
    api: updatePiApi,
    messages: {
      forceError: DataInventoryError.UpdateInformationDisclosure,
    },
  });

  const updatePI = useCallback(
    _.debounce(updatePersonalInformationShared, 750, {
      maxWait: 3000,
    }),
    [updatePersonalInformationShared],
  );

  const handleCategoriesChanged = async (categoryIds: Set<UUIDString>) => {
    const currentSet = sharedCategories[consumerGroup.id];
    const noMapping = noDataDisclosed[consumerGroup.id];

    const resetState = () =>
      setSharedCategories((prev) => Object.assign({}, prev, { [consumerGroup.id]: currentSet }));
    setSharedCategories((prev) => Object.assign({}, prev, { [consumerGroup.id]: categoryIds }));

    await updatePI({
      consumerGroupId: consumerGroup.id,
      categoryIds: Array.from(categoryIds),
      noDataMapping: categoryIds.size == 0 && noMapping,
      resetState,
    });
  };

  useEffect(() => {
    if (open) {
      runExchange();
    } else {
      setReady(false);
    }
  }, [open, runExchange, setReady]);

  if (!open) {
    return null;
  }

  if (!ready) {
    return null;
  }

  return (
    <Dialog fullWidth maxWidth={"lg"} className="fullscreen" open={open} onClose={onClose}>
      <CloseableDialogTitle
        onClose={onClose}
        subtitle={`Choose the best-fitting category for each piece of data that ${vendor.name} processes or collects.`}
      >
        {consumerGroup.name}: {vendor.name}
      </CloseableDialogTitle>

      <DialogContent>
        <MapFields
          organization={org}
          name={vendor.name}
          consumerGroup={consumerGroup}
          personalInformationCategories={
            personalInformationExchanged?.find((val, index) => index == 0)
              ?.personalInformationCategories
          }
          sharedCategories={sharedCategories[consumerGroup.id]}
          noDataDisclosed={noDataDisclosed}
          setNoDataDisclosed={setNoDataDisclosed}
          generalGuidance={vendor.generalGuidance}
          specialCircumstancesGuidance={vendor.specialCircumstancesGuidance}
          recommendedPersonalInformationCategories={vendor.recommendedPersonalInformationCategories}
          showNoDataDisclosed
          update={handleCategoriesChanged}
        />
      </DialogContent>

      <DialogActions>
        <Button type="submit" color="primary" variant="contained" onClick={onClose}>
          Done
        </Button>
      </DialogActions>
    </Dialog>
  );
};
