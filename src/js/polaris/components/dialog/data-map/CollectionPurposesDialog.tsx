import { Button, Dialog, DialogActions, DialogContent } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useActionRequest } from "../../../../common/hooks/api";
import { Api } from "../../../../common/service/Api";
import { CollectionGroupDetailsDto } from "../../../../common/service/server/dto/CollectionGroupDto";
import { BusinessPurposeDto } from "../../../../common/service/server/dto/PersonalInformationOptionsDto";
import { usePrimaryOrganizationId } from "../../../hooks/useOrganization";
import { isEmploymentGroup } from "../../../util/consumerGroups";
import { DescriptionCheckbox } from "../../input/DescriptionCheckbox";
import { CloseableDialogTitle } from "../CloseableDialogTitle";

type CollectionPurposesDialogProps = {
  consumerGroup: CollectionGroupDetailsDto;
  purposes: BusinessPurposeDto[];
  open: boolean;
  onClose: () => void;
};

export const CollectionPurposesDialog = ({
  open,
  onClose,
  consumerGroup,
  purposes,
}: CollectionPurposesDialogProps) => {
  const organizationId = usePrimaryOrganizationId();

  const [purposeState, setPurposeState] = useState<BusinessPurposeDto[]>([]);

  useEffect(() => {
    if (open) {
      setPurposeState(purposes.slice());
    }
  }, [open, setPurposeState, purposes]);

  const { fetch: updatePurpose } = useActionRequest({
    api: async ({ consumerGroupId, purposeId, enabled }) => {
      if (enabled) {
        return Api.dataInventory.addBusinessPurposeCollected(
          organizationId,
          consumerGroupId,
          purposeId,
        );
      } else {
        return Api.dataInventory.removeBusinessPurposeCollected(
          organizationId,
          consumerGroupId,
          purposeId,
        );
      }
    },
    allowConcurrent: true,
  });

  const handleChange = (id, enabled) => {
    const idx = purposeState.findIndex((p) => p.id == id);
    if (idx >= 0) {
      const purpose = purposeState[idx];
      const updated = {
        ...purpose,
        enabled,
      };
      const newState = [...purposeState];
      newState[idx] = updated;
      setPurposeState(newState);
      updatePurpose({ consumerGroupId: consumerGroup.id, purposeId: id, enabled });
    }
  };

  const employment = isEmploymentGroup(consumerGroup);

  return (
    <Dialog fullWidth maxWidth={"lg"} className="fullscreen" open={open} onClose={onClose}>
      <CloseableDialogTitle
        onClose={onClose}
        subtitle={
          employment
            ? `If your business collects personal information from ${consumerGroup.name} for
                non-employment purposes, select the applicable purposes below.`
            : `Select the business purposes for which your business collects personal information
                about ${consumerGroup.name}.`
        }
      >
        {consumerGroup.name}: Collection Purposes
      </CloseableDialogTitle>

      <DialogContent>
        {purposeState.map((purpose) => {
          return (
            <DescriptionCheckbox
              key={purpose.id}
              label={purpose.name}
              description={purpose.description}
              checked={purpose.enabled}
              onChange={(e) => handleChange(purpose.id, e.target.checked)}
            />
          );
        })}
      </DialogContent>

      <DialogActions>
        <Button type="submit" color="primary" variant="contained" onClick={onClose}>
          Done
        </Button>
      </DialogActions>
    </Dialog>
  );
};
