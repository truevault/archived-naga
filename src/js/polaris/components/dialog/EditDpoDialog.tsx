import { Button, Dialog, DialogActions, DialogContent } from "@mui/material";
import React from "react";
import { Form } from "react-final-form";
import { Disableable } from "..";
import { Text } from "../../../common/components/input/Text";
import { requiredString } from "../../util/validation";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

type EditDpoProps = {
  open: boolean;
  onClose: () => void;
  onDone: (vals: DpoValues) => void;
  values: DpoValues;
};

export type DpoValues = {
  name: string | undefined;
  email: string | undefined;
  phone: string | undefined;
  address: string | undefined;
};

export const EditDpoDialog: React.FC<EditDpoProps> = ({ open, onClose, onDone, values }) => {
  const handleSubmit = (vals: DpoValues) => {
    onClose();
    onDone(vals);
  };

  const validate = (values: DpoValues) => {
    const errors = {} as { [x: string]: string };
    requiredString(values, "name", errors);
    requiredString(values, "email", errors);
    return errors;
  };

  return (
    <Dialog fullWidth maxWidth={"sm"} open={open}>
      <Form
        onSubmit={handleSubmit}
        validate={validate}
        initialValues={values}
        render={({ handleSubmit, valid }) => {
          return (
            <form onSubmit={handleSubmit}>
              <CloseableDialogTitle
                disabledTooltip="Enter DPO details to proceed"
                onClose={onClose}
              >
                Data Protection Officer
              </CloseableDialogTitle>

              <DialogContent className="pt-md">
                <p>Add contact details for your business’s DPO below.</p>

                <div className="flex-col">
                  <Text
                    multiline={false}
                    variant="outlined"
                    field="name"
                    required
                    autoFocus
                    label="Name"
                  />
                  <Text
                    multiline={false}
                    variant="outlined"
                    field="email"
                    type="email"
                    required
                    label="Email Address"
                  />
                  <Text
                    multiline={false}
                    variant="outlined"
                    field="phone"
                    label="Telephone number (optional)"
                    required
                  />
                  <Text
                    multiline={false}
                    variant="outlined"
                    field="address"
                    required
                    label="Physical address (optional)"
                  />
                </div>
              </DialogContent>

              <DialogActions>
                <Button onClick={onClose} color="secondary" className="mr-sm">
                  Cancel
                </Button>

                <Disableable
                  disabledTooltip="Please enter your DPO details to proceed."
                  disabled={!valid}
                >
                  <Button type="submit" color="primary" disabled={!valid} variant="contained">
                    Update
                  </Button>
                </Disableable>
              </DialogActions>
            </form>
          );
        }}
      />
    </Dialog>
  );
};
