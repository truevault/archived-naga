import { Breakpoint, Button } from "@mui/material";
import React, { ReactNode, useCallback, useEffect, useState } from "react";
import { HelpDialog } from "./HelpDialog";

const useLocalStorageState = (
  key: string,
  defaultValue?: string,
  lastLogin?: string,
): [string, React.Dispatch<React.SetStateAction<string>>] => {
  let lsValue = localStorage.getItem(key) || defaultValue;
  if (lastLogin) {
    lsValue = lsValue !== lastLogin ? defaultValue : "false";
  }
  const [value, setValue] = useState(lsValue);

  useEffect(() => {
    if (lastLogin && value === "false") {
      localStorage.setItem(key, lastLogin);
    } else if (!lastLogin) {
      localStorage.setItem(key, value);
    }
  }, [value, key]);

  return [value, setValue];
};

interface HelpInterstitialDialogProps {
  uniqueId: string;
  title: string;
  children: ReactNode | ReactNode[];
  closeActionText?: string;
  customActions?: ReactNode | ReactNode[];
  closeSignal?: boolean;
  maxWidth?: Breakpoint;
  lastLogin?: string;
}

export const HelpInterstitialDialog: React.FC<HelpInterstitialDialogProps> = ({
  uniqueId,
  title,
  children,
  closeActionText = "Get Started",
  customActions,
  closeSignal,
  maxWidth,
  lastLogin,
}) => {
  const [open, setOpen] = useLocalStorageState(uniqueId, "true", lastLogin);
  const isOpen = () => open === "true";
  const onClose = useCallback(() => setOpen("false"), [setOpen]);
  const actions = customActions || (
    <Button variant="contained" color="primary" onClick={onClose}>
      {closeActionText}
    </Button>
  );

  useEffect(() => {
    if (closeSignal) {
      onClose();
    }
  }, [closeSignal, onClose]);

  return (
    <HelpDialog
      open={isOpen()}
      onClose={onClose}
      title={title}
      actions={actions}
      disableBackdropClick={true}
      className="intro-help-dialog"
      maxWidth={maxWidth}
    >
      {children}
    </HelpDialog>
  );
};
