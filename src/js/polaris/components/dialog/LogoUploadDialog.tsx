import ImageIcon from "@mui/icons-material/Image";
import { Button, Dialog, DialogActions, DialogContent } from "@mui/material";
import React, { useRef, useState } from "react";
import { Field, Form } from "react-final-form";
import { LoadingButton } from "../../../common/components/LoadingButton";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { UploadLogoDto } from "../../../common/service/server/controller/StorageController";
import { LogoType, OrganizationPublicId } from "../../../common/service/server/Types";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

const MAX_FILE_SIZE_BYTES = 1 * 1024 * 1024;
export const EXTENDED_FILE_TYPES = [
  "image/gif",
  "image/jpeg",
  "image/jpg",
  "image/png",
  "image/svg+xml",
  "image/svg",
];
export const ACCEPTED_FILE_TYPES = ["image/gif", "image/jpeg", "image/jpg", "image/png"];

interface LogoUploadDialogProps {
  title?: string;
  logoType: LogoType;
  organizationId: OrganizationPublicId;
  open: boolean;
  onClose: () => void;
  onDone: (logoUrl: string) => void;
  successMessage?: string;
  acceptedTypes?: string[];
}

export const LogoUploadDialog: React.FC<LogoUploadDialogProps> = ({
  title,
  logoType,
  organizationId,
  open,
  onClose,
  onDone,
  successMessage = "Image successfully uploaded; now save the form to update your settings!",
  acceptedTypes = ACCEPTED_FILE_TYPES,
}) => {
  const apiFn = (logoDto: UploadLogoDto) =>
    Api.storage.postOrganizationLogo(organizationId, logoDto);

  const dialogTitle = title || `Upload Organization ${logoType === "FAVICON" ? "Favicon" : "Logo"}`;

  const helpImageUrl =
    logoType === "MESSAGE_HEADER" ? "/assets/images/logo-upload-helper-message.svg" : "";

  const [logoFile, setLogoFile] = useState<File | null>(null);

  const logoInputRef = useRef<HTMLInputElement | null>(null);
  const logoPreviewRef = useRef<HTMLDivElement | null>(null);
  const logoPreviewReader = new FileReader();
  logoPreviewReader.onload = (e) => {
    const image = document.createElement("img");
    image.classList.add("logo-image-preview--img");
    if (e.target && logoPreviewRef.current) {
      image.src = e.target.result as string;
      logoPreviewRef.current.childNodes.forEach((child) =>
        logoPreviewRef.current?.removeChild(child),
      );
      logoPreviewRef.current.appendChild(image);
    }
  };

  const { fetch: postLogo, request: postLogoRequest } = useActionRequest({
    api: () => apiFn({ logoType, logo: logoFile as File }),
    messages: {
      forceError: "Unable to upload image, please try again later",
      success: successMessage,
    },
    onSuccess: (result) => {
      if (result.logoUrl) {
        onDone(result.logoUrl);
      }
      onClose();
    },
    notReady: logoFile == null,
  });

  const handleFileChange = (onChange) => (event) => {
    if (event.target?.files?.length > 0) {
      const file = event.target?.files[0];
      setLogoFile(file);
      logoPreviewReader.readAsDataURL(file);
    }
    onChange(event);
  };

  const removeImage = () => {
    setLogoFile(null);
  };

  const imageRecommendation =
    logoType !== "MESSAGE_HEADER" ? "SVG is recommended." : "PNG is recommended.";

  return (
    <Dialog
      className="logo-upload-dialog"
      fullWidth={true}
      maxWidth="md"
      open={open}
      onClose={onClose}
    >
      <Form
        onSubmit={postLogo}
        validate={() => {
          const errors = {};
          if (logoFile && logoFile.size > MAX_FILE_SIZE_BYTES) {
            errors["imageFile"] = "File must be less than 1MiB in size";
          }

          if (logoFile && !acceptedTypes.includes(logoFile.type)) {
            errors["imageFile"] = "File must be an accepted image type";
          }

          return errors;
        }}
        render={({ handleSubmit }) => {
          return (
            <form onSubmit={handleSubmit}>
              <CloseableDialogTitle
                onClose={onClose}
                subtitle={`Maximum file size is 1MB. ${imageRecommendation}`}
              >
                {dialogTitle}
              </CloseableDialogTitle>

              <DialogContent>
                {helpImageUrl && (
                  <div className="logo-upload-dialog--help-image mt-lg">
                    <img src={helpImageUrl} />
                  </div>
                )}
                <div className="logo-upload-dialog--content">
                  <div className="logo-image-preview" id="logo-image-preview">
                    <div
                      hidden={!logoFile}
                      className="logo-image-preview--image"
                      ref={logoPreviewRef}
                    ></div>
                    {!logoFile && (
                      <ImageIcon className="logo-image-preview--img logo-image-preview--icon" />
                    )}
                  </div>
                  <div className="mt-lg w-100 flex-row flex--justify-center">
                    <Field name="imageFile">
                      {({ input: { onChange, ...inputProps }, meta: { error } }) => {
                        const handleChange = handleFileChange(onChange);
                        return (
                          <>
                            <input
                              hidden={true}
                              id="logo-image-file-input"
                              ref={logoInputRef}
                              {...inputProps}
                              onChange={handleChange}
                              type="file"
                              required={true}
                              accept={acceptedTypes.join(",")}
                            />
                            <label htmlFor="logo-image-file-input">
                              <span className="logo-upload-dialog--input-remove">
                                <Button
                                  className="w-192 mr-md"
                                  onClick={removeImage}
                                  disabled={!logoFile}
                                  size="large"
                                >
                                  Remove
                                </Button>
                              </span>
                              <span className="logo-upload-dialog--input-add">
                                <Button
                                  className="w-192"
                                  variant="contained"
                                  onClick={() => logoInputRef.current?.click()}
                                  size="large"
                                >
                                  Choose Image
                                </Button>
                              </span>
                            </label>
                            {error && <span className="text-error ml-md">{error}</span>}
                          </>
                        );
                      }}
                    </Field>
                  </div>
                </div>
              </DialogContent>

              <DialogActions>
                <LoadingButton
                  className="w-192"
                  type="submit"
                  color="primary"
                  loading={postLogoRequest.running}
                  disabled={!logoFile}
                >
                  Upload
                </LoadingButton>
              </DialogActions>
            </form>
          );
        }}
      />
    </Dialog>
  );
};
