import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Forms } from "..";
import { Loading } from "../../../common/components/Loading";
import { useDataRequest } from "../../../common/hooks/api";
import { DataRequestDetail } from "../../../common/models";
import { NetworkRequest, NetworkRequestState } from "../../../common/models/NetworkRequest";
import { Api } from "../../../common/service/Api";
import { Error as RequestsError } from "../../copy/requests";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { Error } from "../Error";

const localeSorter =
  (reverse = false) =>
  (a, b) => {
    return reverse ? b.localeCompare(a) : a.localeCompare(b);
  };

type EditDataRequestDialogProps = {
  open: boolean;
  request: DataRequestDetail;
  onClose: () => void;
  onDone: () => void;
  updateRequestState: NetworkRequestState;
  handleSubmit: (values: any) => void;
};

export const EditDataRequestDialog = ({
  open,
  request,
  onClose,
  onDone,
  updateRequestState,
  handleSubmit,
}: EditDataRequestDialogProps) => {
  const [everRendered, setEverRendered] = useState(false);

  const primaryOrgId = usePrimaryOrganizationId();

  const { request: dataRequestTypesRequest, result: dataRequestTypes } = useDataRequest({
    queryKey: ["dataRequestTypes", primaryOrgId],
    api: () => Api.organization.getDataRequestTypes(primaryOrgId, true),
  });

  // auto-close dialog on success
  useEffect(() => {
    if (updateRequestState.success) {
      onDone();
    }
  }, [updateRequestState.success, onDone]);

  // complete our page load
  useEffect(() => {
    if (NetworkRequest.areFinished(dataRequestTypesRequest)) {
      setEverRendered(true);
    }
  }, [dataRequestTypesRequest]);

  if (!everRendered) {
    return null;
  }

  const drtOptions = (dataRequestTypes || [])
    .slice()
    .filter((drt) => drt.type === request.dataRequestType.type)
    .map((drt) => ({
      label: `${drt.regulationName} - ${drt.name}`,
      value: drt.type,
    }))
    .sort((a, b) => localeSorter()(a.label, b.label));

  return (
    <Dialog
      fullWidth={true}
      maxWidth={"sm"}
      open={open}
      onClose={onClose}
      aria-labelledby="update-request-dialog-title"
    >
      <DialogTitle id="update-request-dialog-title">Edit Request</DialogTitle>

      {updateRequestState.running && (
        <DialogContent>
          <Loading />
        </DialogContent>
      )}

      {updateRequestState.error && (
        <>
          <DialogContent>
            <Error message={updateRequestState.errorMessage || RequestsError.UpdateRequest} />
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose}>Close</Button>
          </DialogActions>
        </>
      )}

      {!updateRequestState.started && (
        <Forms.EditDataRequest
          onSubmit={handleSubmit}
          onClose={onClose}
          request={request}
          dataRequestTypeOptions={drtOptions}
        />
      )}
    </Dialog>
  );
};
