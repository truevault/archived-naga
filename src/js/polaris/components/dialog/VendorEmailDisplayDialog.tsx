import { Button, Dialog, DialogActions, DialogContent, DialogProps } from "@mui/material";
import React from "react";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

type ConfirmDialogProps = {
  open: boolean;
  onClose: () => void;
  vendors: OrganizationDataRecipientDto[];
  title?: string;
  cls?: string;
} & DialogProps;

export const VendorEmailDisplayDialog: React.FC<ConfirmDialogProps> = ({
  open,
  onClose,
  vendors,
  title = "Notification Recipients",
  maxWidth = "md",
  cls,
  ...props
}) => {
  return (
    <Dialog
      fullWidth={true}
      maxWidth={maxWidth}
      open={open}
      onClose={onClose}
      className={`get-compliant-dialog confirm-dialog ${cls ? cls : ""}`}
      {...props}
    >
      <CloseableDialogTitle onClose={onClose}>{title}</CloseableDialogTitle>

      <DialogContent>
        <div className="flex-container flex-row flex--wrap">
          {vendors
            .sort((a, b) => a.name.localeCompare(b.name))
            .map((v) => {
              return (
                <div key={v.id} className="w-50 mr-0 mb-std">
                  <div className="flex-container">
                    <div className="w-space-xl align-top mr-md" style={{ flexShrink: 0 }}>
                      <img
                        src={v.logoUrl || "/assets/images/icon-vendors-black.svg"}
                        className="w-100"
                      />
                    </div>

                    <div className="flex-grow">
                      <div className="text-t5 text-weight-bold">{v.name}</div>
                      <a href={`mailto:${v.email}`}>{v.email || v.defaultEmail}</a>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </DialogContent>
      <DialogActions>
        <Button color="primary" variant="contained" onClick={onClose} className="ml-sm">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};
