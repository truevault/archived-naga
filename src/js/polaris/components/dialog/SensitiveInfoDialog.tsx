import { Warning } from "@mui/icons-material";
import { Dialog, DialogActions, DialogContent, Typography } from "@mui/material";
import React, { ReactNode, useState } from "react";
import { hashStringToNumber } from "../../util";
import { Callout, CalloutVariant } from "../Callout";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

interface SensitiveInfoDialogProps {
  open: boolean;
  onClose: () => void;
  title: string;
  children: ReactNode | ReactNode[];
  actions?: ReactNode | ReactNode[];
  disableBackdropClick?: boolean;
  className?: string;
}
const SensitiveInfoDialog: React.FC<SensitiveInfoDialogProps> = ({
  open,
  onClose,
  title,
  actions,
  children,
  className,
}) => {
  const titleId = `help-dialog-title-${hashStringToNumber(title)}`;
  return (
    <Dialog
      className={`help-dialog ${className || ""}`.trimEnd()}
      open={open}
      fullWidth={true}
      maxWidth={"md"}
      onClose={onClose}
      aria-labelledby={titleId}
    >
      <CloseableDialogTitle onClose={onClose}>{title}</CloseableDialogTitle>
      <DialogContent className="help-dialog-content">{children}</DialogContent>
      {actions && <DialogActions>{actions}</DialogActions>}
    </Dialog>
  );
};

export const SensitiveInfoCallout = () => {
  const [showDialog, setShowDialog] = useState(false);
  return (
    <>
      <Callout variant={CalloutVariant.Purple}>
        <Warning fontSize="small" className="align-top mr-sm" />
        <span className="d-inline-block">
          <Typography variant="body2" component="h5" className="mb-0">
            <strong>Don't disclose sensitive information</strong>
          </Typography>
          <Typography variant="body2" paragraph>
            This includes government ID numbers, financial account numbers, passwords, and biometric
            information
          </Typography>

          <Typography variant="body2">
            <a
              href=""
              onClick={(e) => {
                e.preventDefault();
                setShowDialog(true);
              }}
              className="link-primary"
            >
              See full list
            </a>
          </Typography>
        </span>
      </Callout>
      <SensitiveInfoDialog
        title={"Sensitive Information"}
        open={showDialog}
        onClose={() => setShowDialog(false)}
      >
        <div>
          The following sensitive information should not be disclosed to the requester when
          processing a Request to Know.
        </div>
        <ul>
          <li>Social security number</li>
          <li>Driver’s license number</li>
          <li>Other government-issued ID number</li>
          <li>Financial account number</li>
          <li>Health insurance or medical ID number</li>
          <li>Account password or security questions and answers</li>
          <li>Unique biometric information</li>
        </ul>
      </SensitiveInfoDialog>
    </>
  );
};
