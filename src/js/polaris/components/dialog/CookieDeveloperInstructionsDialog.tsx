import { Button } from "@mui/material";
import React from "react";
import { OrganizationCookieDto } from "../../../common/service/server/dto/CookieScanDto";
import { Callout, CalloutVariant } from "../Callout";
import { DeveloperCookieTable } from "../organisms/cookies/DeveloperCookieTable";
import { HelpDialog } from "./HelpDialog";

type Props = {
  open: boolean;
  onClose: () => void;
} & BodyProps;

export const CookieDeveloperInstructionsDialog: React.FC<Props> = ({
  open,
  onClose,
  ...bodyProps
}) => {
  return (
    <HelpDialog
      open={open}
      onClose={onClose}
      title={"Developer Instructions Preview"}
      actions={
        <Button variant="contained" color="primary" onClick={onClose}>
          Done
        </Button>
      }
      disableBackdropClick={true}
      className="intro-help-dialog"
      maxWidth="md"
    >
      <DialogBody {...bodyProps} />
    </HelpDialog>
  );
};

type BodyProps = {
  scanUrl: string;
  cookies: OrganizationCookieDto[];
};
const DialogBody: React.FC<BodyProps> = ({ scanUrl, cookies }) => {
  return (
    <>
      <p>
        Later in the process, we will generate a link to share with your dev team including
        instructions on how to handle cookie consent. Below is a preview of the content we will
        generate.
      </p>

      <Callout variant={CalloutVariant.Purple}>
        European e-privacy laws require consent for all website cookies beyond those that are
        strictly necessary for the operation of your website. Below are a list of cookies identified
        on <code>{scanUrl}</code> and the category they belong to. Please update your site such that
        each cookie below does not load unless the user has consented to the type of cookie listed.
        <DeveloperCookieTable cookies={cookies} />
      </Callout>
    </>
  );
};
