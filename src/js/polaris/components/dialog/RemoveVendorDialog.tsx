import { useQueryClient } from "@tanstack/react-query";
import React from "react";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { CollectionContext } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { Error as RequestsError } from "../../copy/requests";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { ConfirmDialog } from "./ConfirmDialog";

type RemoveVendorDialogProps = {
  open: boolean;
  onClose: () => void;
  onAfterRemoved?: () => void;
  vendorId: string;
  vendorName?: string;
  type?: "source" | "vendor";
  collectionContext: CollectionContext[];
};
export const RemoveVendorDialog: React.FC<RemoveVendorDialogProps> = ({
  open,
  onClose,
  onAfterRemoved,
  vendorId,
  vendorName,
  type = "vendor",
  collectionContext,
}) => {
  const organizationId = usePrimaryOrganizationId();
  const queryClient = useQueryClient();

  const { fetch: removeVendor } = useActionRequest({
    api: async (vendorId) => {
      return Api.organizationVendor.removeVendor(organizationId, vendorId, collectionContext);
    },
    onSuccess: () => {
      queryClient.invalidateQueries(["organizationVendors", organizationId]);
    },
    messages: {
      forceError: RequestsError.RemoveVendor,
    },
  });

  const handleRemove = async (vendorId) => {
    if (type === "vendor") {
      await removeVendor(vendorId);
    }
    if (onAfterRemoved) {
      onAfterRemoved();
    }
    onClose();
  };

  vendorName = vendorName || "this vendor";

  return (
    <ConfirmDialog
      open={open}
      onClose={onClose}
      onConfirm={() => {
        handleRemove(vendorId);
      }}
      confirmTitle="Are you sure?"
      contentText={`Are you sure you want to remove ${vendorName}?`}
    />
  );
};
