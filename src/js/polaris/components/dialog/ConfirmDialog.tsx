import { LoadingButton } from "../../../common/components/LoadingButton";
import { Button, Dialog, DialogActions, DialogContent, DialogProps } from "@mui/material";
import React, { ReactNode } from "react";
import { hashStringToNumber } from "../../util";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

type ConfirmDialogProps = {
  open: boolean;
  onClose: () => void;
  onCancel?: () => void;
  onConfirm: () => void;
  confirmTitle: string;
  contentText: ReactNode;
  confirmText?: string;
  confirmLoading?: boolean;
  cancelText?: string;
  transitionDuration?: { appear?: number; enter?: number; exit?: number };
  cls?: string;
} & DialogProps;

export const ConfirmDialog: React.FC<ConfirmDialogProps> = ({
  open,
  onClose,
  onCancel = null,
  onConfirm,
  confirmTitle,
  contentText,
  confirmText = "Confirm",
  confirmLoading = false,
  cancelText = "Cancel",
  // By default we exit the dialog immediately to prevent state-based changes appearing in cases
  // where we use a state-value to drive whether the dialog is open or closed
  transitionDuration = { enter: 200, exit: 0 },
  cls,
  maxWidth = "sm",
  ...props
}) => {
  const idSuffix = hashStringToNumber(confirmTitle).toString();
  const titleId = `confirm-dialog-title-${idSuffix}`;
  return (
    <Dialog
      fullWidth={true}
      maxWidth={maxWidth}
      open={open}
      onClose={onClose}
      aria-labelledby={titleId}
      transitionDuration={transitionDuration}
      className={`get-compliant-dialog confirm-dialog ${cls ? cls : ""}`}
      {...props}
    >
      <CloseableDialogTitle onClose={onClose} id={titleId}>
        {confirmTitle}
      </CloseableDialogTitle>

      <DialogContent>{contentText}</DialogContent>
      <DialogActions>
        <Button
          variant="text"
          color="secondary"
          onClick={onCancel || onClose}
          disabled={confirmLoading}
        >
          {cancelText}
        </Button>
        <LoadingButton
          color="primary"
          onClick={onConfirm}
          loading={confirmLoading}
          className="ml-sm"
        >
          {confirmText}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};
