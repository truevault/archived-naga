import { Button, Dialog, DialogActions, DialogContent } from "@mui/material";
import React, { useEffect } from "react";
import { Form } from "react-final-form";
import { Text } from "../../../common/components/input/Text";
import { Loading } from "../../../common/components/Loading";
import { useActionRequest } from "../../../common/hooks/api";
import { Api } from "../../../common/service/Api";
import { OrganizationPublicId } from "../../../common/service/server/Types";
import { Error as RequestsError, Success as RequestsSuccess } from "../../copy/requests";
import { requiredEmail, requiredString } from "../../util/validation";
import { Error } from "../Error";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

const inviteUserValidation = (values) => {
  const errors = {} as any;

  requiredEmail(values, "email", errors);
  requiredString(values, "firstName", errors);
  requiredString(values, "lastName", errors);

  return errors;
};

const inviteUserInitialValues = { email: "" };

const InviteUserForm = ({ handleSubmit, onCancel }) => {
  return (
    <form onSubmit={handleSubmit}>
      <DialogContent>
        <div className="flex-col">
          <div className="flex-row flex--expand">
            <Text autoFocus field="firstName" label="First Name" required />
            <Text field="lastName" label="Last Name" required />
          </div>
          <Text field="email" label="Email Address" required />
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel}>Cancel</Button>
        <Button type="submit" color="primary" variant="contained">
          Add User
        </Button>
      </DialogActions>
    </form>
  );
};

type InviteUserDialogProps = {
  open: boolean;
  organizationId: OrganizationPublicId;
  onClose: () => void;
  onDone: () => void;
};

export const InviteUserDialog = ({
  open,
  organizationId,
  onClose,
  onDone,
}: InviteUserDialogProps) => {
  const {
    request,
    fetch: sendInviteRequest,
    resetState,
  } = useActionRequest({
    api: (values) => Api.organizationUser.inviteUser(organizationId, values),
    messages: {
      success: RequestsSuccess.InviteUser,
      defaultError: RequestsError.InviteUser,
    },
    notifyError: true,
    notifySuccess: true,
    onSuccess: () => {
      onDone();
    },
    onError: (networkRequestState, error) => {
      networkRequestState.errorMessage =
        // @ts-ignore
        error?.response?.data?.message ??
        error?.message ??
        "There was an error while processing your request.";
    },
  });

  const onSubmit = async (values) => await sendInviteRequest(values);

  useEffect(() => {
    if (open) {
      resetState();
    }
  }, [open, resetState]);

  return (
    <Dialog
      className="get-compliant-dialog"
      fullWidth={true}
      maxWidth={"sm"}
      open={open}
      onClose={onClose}
      aria-labelledby="invite-user-dialog-title"
    >
      <CloseableDialogTitle onClose={onClose}>Add a User</CloseableDialogTitle>

      {request.running && (
        <DialogContent>
          <Loading />
        </DialogContent>
      )}

      {request.error && (
        <>
          <DialogContent>
            <Error message={request.errorMessage || RequestsError.InviteUser} />
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose}>Close</Button>
          </DialogActions>
        </>
      )}

      {!request.started && (
        <Form
          onSubmit={onSubmit}
          initialValues={inviteUserInitialValues}
          validate={inviteUserValidation}
          render={({ handleSubmit, form, ...props }) => (
            <InviteUserForm
              handleSubmit={(e) => handleSubmit(e).then(() => form.reset())}
              {...props}
              onCancel={onClose}
            />
          )}
        />
      )}
    </Dialog>
  );
};
