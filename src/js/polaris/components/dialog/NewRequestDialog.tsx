import { Alert, Dialog, DialogContent } from "@mui/material";
import moment from "moment";
import React, { useEffect, useMemo, useState } from "react";
import { Form } from "react-final-form";
import { useHistory } from "react-router";
import { useDataRequest } from "../../../common/hooks/api";
import { NetworkRequest } from "../../../common/models/NetworkRequest";
import { Api } from "../../../common/service/Api";
import { PrivacyRegulationName } from "../../../common/service/server/dto/DataRequestTypeDto";
import { fmtLongRequestTypeWithRegulation } from "../../formatters/request";
import { usePrimaryOrganization } from "../../hooks/useOrganization";
import { NewRequestForm, newRequestFormValidation } from "../../pages/requests/NewRequestForm";
import { RouteHelpers, Routes } from "../../root/routes";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

const localeSorter =
  (reverse = false) =>
  (a, b) => {
    return reverse ? b.localeCompare(a) : a.localeCompare(b);
  };

type NewRequestDialogProps = {
  open: boolean;
  onClose: () => void;
};

export const NewRequestDialog: React.FC<NewRequestDialogProps> = ({ open, onClose }) => {
  const history = useHistory();
  const [org] = usePrimaryOrganization();
  const primaryOrgId = org.id;

  const [everRendered, setEverRendered] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  // Should be typed to form values but I'm lazy. xoxo wyatt
  const handleSubmit = async (values: any) => {
    const submitValues = { ...values, requestDate: values.requestDate?.format() };

    try {
      const response = await Api.dataRequest.createRequest(primaryOrgId, submitValues);

      onClose();
      history.push(
        response.state == "PENDING_EMAIL_VERIFICATION"
          ? Routes.requests.pending
          : RouteHelpers.requests.detail(response.id),
      );
    } catch (err) {
      setErrorMessage(err?.response?.data?.message ?? "Unknown Error Occured");
    }
  };

  const { request: dataRequestTypesRequest, result: dataRequestTypes } = useDataRequest({
    queryKey: ["dataRequestTypes", primaryOrgId],
    api: () => {
      let regulations: PrivacyRegulationName[] = ["CCPA", "VOLUNTARY"];
      if (org.featureMultistate) {
        regulations.push("VCDPA");
        regulations.push("CPA");
        regulations.push("CTDPA");
      }
      if (org.featureGdpr) {
        regulations.push("GDPR");
      }
      return Api.organization.getDataRequestTypes(primaryOrgId, true, regulations);
    },
    dependencies: [org],
  });

  const drtOptions = useMemo(() => {
    return (dataRequestTypes || [])
      .slice()
      .map((drt) => ({
        label: fmtLongRequestTypeWithRegulation(drt.type),
        value: drt.type,
      }))
      .sort((a, b) => localeSorter()(a.label, b.label));
  }, [dataRequestTypes]);

  // complete our page load
  useEffect(() => {
    if (NetworkRequest.areFinished(dataRequestTypesRequest)) {
      setEverRendered(true);
    }
  }, [dataRequestTypesRequest]);

  if (!everRendered) {
    return null;
  }

  return (
    <>
      <Dialog
        className="get-compliant-dialog"
        fullWidth={true}
        maxWidth={"sm"}
        open={open}
        onClose={onClose}
        aria-labelledby="invite-user-dialog-title"
      >
        <CloseableDialogTitle onClose={onClose}>New Request</CloseableDialogTitle>

        <DialogContent>
          {Boolean(errorMessage) && (
            <Alert className="mb-sm" severity="error">
              {errorMessage}
            </Alert>
          )}

          <Form
            keepDirtyOnReinitialize
            onSubmit={handleSubmit}
            validate={newRequestFormValidation}
            initialValues={{
              requestDate: moment().startOf("day"),
            }}
            render={(props) => (
              <NewRequestForm onCancel={onClose} {...props} dataRequestTypeOptions={drtOptions} />
            )}
          />
        </DialogContent>
      </Dialog>
    </>
  );
};
