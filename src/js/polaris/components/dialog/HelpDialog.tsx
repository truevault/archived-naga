import { Breakpoint, Dialog, DialogActions, DialogContent } from "@mui/material";
import clsx from "clsx";
import React, { ReactNode } from "react";
import { hashStringToNumber } from "../../util";
import { CloseableDialogTitle } from "./CloseableDialogTitle";

interface HelpDialogProps {
  open: boolean;
  onClose: () => void;
  title: string;
  children: ReactNode | ReactNode[];
  actions?: ReactNode | ReactNode[];
  disableBackdropClick?: boolean;
  className?: string;
  maxWidth?: Breakpoint;
}

export const HelpDialog: React.FC<HelpDialogProps> = ({
  open,
  onClose,
  title,
  actions,
  children,
  className,
  maxWidth = "md",
}) => {
  const titleId = `help-dialog-title-${hashStringToNumber(title)}`;
  return (
    <Dialog
      className={clsx("help-dialog", className)}
      open={open}
      fullWidth={true}
      maxWidth={maxWidth}
      onClose={onClose}
      aria-labelledby={titleId}
    >
      <CloseableDialogTitle onClose={onClose}>{title}</CloseableDialogTitle>

      <DialogContent className="help-dialog-content">{children}</DialogContent>

      {actions && <DialogActions>{actions}</DialogActions>}
    </Dialog>
  );
};
