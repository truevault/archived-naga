import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import React from "react";
import { PrivacyCenterPolicySectionDto } from "../../../common/service/server/dto/PrivacyCenterDto";
import { PrivacyPolicySectionEditorWrapper } from "../privacyCenter/PrivacyPolicySectionEditor";

type EditDataRequestDialogProps = {
  orgId: string;
  open: boolean;
  privacyPolicy: any;
  section: PrivacyCenterPolicySectionDto;
  refresh: () => void;
  onClose: () => void;
};

export const EditPrivacyPolicySectionDialog = ({
  open,
  orgId,
  privacyPolicy,
  refresh,
  section,
  onClose,
}: EditDataRequestDialogProps) => {
  const isAdd = !section?.id;
  const name = section?.name || "Privacy Policy section";
  const title = isAdd ? `Add ${name}` : `Edit ${name}`;

  return (
    <Dialog
      fullWidth={true}
      maxWidth={"lg"}
      className="get-compliant-dialog"
      open={open}
      onClose={onClose}
      aria-labelledby="update-privacy-policy-section-title"
    >
      <DialogTitle id="update-privacy-policy-section-title">{title}</DialogTitle>

      <DialogContent>
        <PrivacyPolicySectionEditorWrapper
          refresh={refresh}
          organizationId={orgId}
          privacyPolicy={privacyPolicy}
          section={section}
          onClose={onClose}
          before={undefined}
          after={undefined}
        />
      </DialogContent>
    </Dialog>
  );
};
