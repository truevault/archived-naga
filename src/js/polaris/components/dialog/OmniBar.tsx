import DomainIcon from "@mui/icons-material/Domain";
import KeyboardReturnIcon from "@mui/icons-material/KeyboardReturn";
import SearchIcon from "@mui/icons-material/Search";
import {
  Avatar,
  Dialog,
  Divider,
  InputAdornment,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  MenuItem,
  TextField,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import React, { useContext, useEffect, useMemo, useState } from "react";
import { OmniBarContext } from "../../../common/hooks/useOmniBar";
import { useSelf } from "../../../common/hooks/useSelf";
import { usePrimaryOrganizationId } from "../../hooks/useOrganization";
import { OrganizationUserDetailsDto } from "../../../common/service/server/dto/UserDto";

const useStyles = makeStyles(() => ({
  scrollPaper: {
    alignItems: "flex-start",
  },
  paper: {
    marginTop: "33vh",
  },
}));

export const OmniBar = () => {
  const { open, closeOmniBar } = useContext(OmniBarContext);
  const orgId = usePrimaryOrganizationId();
  const classes = useStyles({});
  const [filter, setFilter] = useState("");
  const [selectedIdx, setSelectedIdx] = useState(0);
  const [self] = useSelf();

  const onDone = (data) => closeOmniBar(data);
  const onClose = () => closeOmniBar();

  const unfilteredOptions = useMemo(
    () => self?.organizations?.filter((o) => o.organization !== orgId) ?? [],
    [orgId, self],
  );

  const filteredOptions = useMemo(
    () =>
      filter
        ? unfilteredOptions.filter((r) =>
            r.organizationName.toLowerCase().includes(filter.toLowerCase()),
          )
        : unfilteredOptions,
    [filter, unfilteredOptions],
  );

  const trimmedOptions = filteredOptions.slice(0, 8);

  // reset filter on open
  useEffect(() => {
    if (open) {
      setFilter("");
    }
  }, [open]);

  const clamp = (idx) => Math.floor(Math.min(trimmedOptions.length - 1, Math.max(0, idx)));

  const increment = () => {
    const updatedIdx = selectedIdx + 1;
    setSelectedIdx(clamp(updatedIdx));
  };

  const decrement = () => {
    const updatedIdx = selectedIdx - 1;
    setSelectedIdx(clamp(updatedIdx));
  };

  const onChange = (e) => {
    setFilter(e.target.value);
    setSelectedIdx(0);
  };

  const done = (idx: number | null = null) => {
    const actualIdx = idx != null ? idx : selectedIdx;
    const selected = trimmedOptions[actualIdx];
    if (selected) {
      onDone(selected);
    } else {
      onClose();
    }
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      fullWidth
      transitionDuration={125}
      classes={{
        scrollPaper: classes.scrollPaper,
        paper: classes.paper,
      }}
      className="omnibar"
      BackdropProps={{
        style: {
          backgroundColor: "#000",
          opacity: 0.6,
        },
      }}
    >
      <div className="omnibar__header">
        <TextField
          autoFocus
          placeholder="lookup in Polaris"
          id="omnibar__search"
          type="search"
          size="medium"
          fullWidth
          value={filter}
          onChange={onChange}
          onKeyDown={(event) => {
            if (event.key === "ArrowUp") {
              decrement();
            }
            if (event.key === "ArrowDown") {
              increment();
            }
            if (event.key === "Enter") {
              event.preventDefault();
              done();
            }
          }}
          InputProps={{
            autoComplete: "off",
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon style={{ color: "#dadada", width: "36px", height: "36px" }} />
              </InputAdornment>
            ),
          }}
        />
      </div>

      <Divider />

      <div className="omnibar__results">
        {trimmedOptions.length > 0 ? (
          <OrganizationResults
            organizations={trimmedOptions}
            done={done}
            selectedIdx={selectedIdx}
          />
        ) : (
          <NoResultsItem />
        )}
      </div>
    </Dialog>
  );
};

const OrganizationResults = ({
  organizations,
  done,
  selectedIdx,
}: {
  organizations: any[];
  done: (idx: number) => void;
  selectedIdx: number;
}) => {
  return (
    <>
      <ListItem className="omnibar__type-label">Organizations</ListItem>
      {organizations.map((o, idx) => {
        return (
          <MenuItem key={idx} selected={idx == selectedIdx} onClick={() => done(idx)}>
            <OrganizationRecord organization={o} selected={idx == selectedIdx} />
          </MenuItem>
        );
      })}
    </>
  );
};

const NoResultsItem = () => {
  return <div className="omnisearch__no-results">No Results Found</div>;
};

const OrganizationRecord = ({
  organization,
  selected,
}: {
  organization: OrganizationUserDetailsDto;
  selected: boolean;
}) => {
  const displayEnter = selected && (
    <ListItemSecondaryAction className="omnibar__active-item">
      <div>press</div>
      <KeyboardReturnIcon />
    </ListItemSecondaryAction>
  );
  return (
    <>
      <ListItemAvatar>
        <Avatar
          variant="square"
          alt="organization icon"
          src={organization?.faviconUrl?.toString()}
          style={{ width: "32px", height: "32px" }}
        >
          <DomainIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText disableTypography secondary={displayEnter}>
        <span className="text-nowrap omnibar__item-label">{organization?.organizationName}</span>
      </ListItemText>
    </>
  );
};
