import { Typography, TypographyProps } from "@mui/material";
import React, { ElementType } from "react";

export const Para: React.FC<TypographyProps & { component?: ElementType }> = (props) => (
  <Typography variant="body1" {...props} />
);
