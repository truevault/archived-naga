import React from "react";

type UnderlinedHeadingProps = {
  className?: string;
};

export const UnderlinedHeading: React.FC<UnderlinedHeadingProps> = ({ className, children }) => {
  return (
    <h4
      className={`pb-md text-t1 text-spacing-spread text-muted text-weight-regular text-transform-uppercase border-bottom border-bottom--grey ${className}`}
    >
      {children}
    </h4>
  );
};
