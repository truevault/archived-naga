import React, { Children } from "react";

import { Para } from "./Para";

export const Paras: React.FC = ({ children }) => {
  const size = Children.count(children) - 1;

  const childrenWithProps = React.Children.map(children, (child, idx) => {
    if (React.isValidElement(child) && idx < size) {
      const explicitNoParagaph = child.props.paragraph === false;
      const para = !explicitNoParagaph;

      return child.type === Para ? React.cloneElement(child, { paragraph: para }) : child;
    }
    return child;
  });

  return <>{childrenWithProps}</>;
};
