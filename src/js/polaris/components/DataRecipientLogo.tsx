import clsx from "clsx";
import React from "react";
import { LetterAvatar } from "../../common/components/LetterAvatar";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../common/service/server/dto/VendorDto";

type Props = {
  dataRecipient?: OrganizationDataRecipientDto | OrganizationDataSourceDto | VendorDto;
  logoUrl?: string;
  size?: number;
  className?: string;
};

export const DataRecipientLogo: React.FC<Props> = React.forwardRef<HTMLImageElement, Props>(
  ({ dataRecipient, logoUrl, size = 32, className, ...rest }, ref) => {
    const hasLogo = Boolean(!dataRecipient?.isCustom && (logoUrl || dataRecipient?.logoUrl));
    const hasLetter = Boolean(
      dataRecipient?.name && dataRecipient.name.length > 0 && dataRecipient.name[0],
    );

    if (hasLogo) {
      return (
        <img
          ref={ref}
          className={clsx("data-recipient-logo", className)}
          src={logoUrl || dataRecipient?.logoUrl}
          width={size}
          height={size}
          {...rest}
        />
      );
    }

    if (hasLetter) {
      return (
        <LetterAvatar
          ref={ref}
          // this assertion/cast is covered by the `hasLetter` if stmt
          content={dataRecipient!.name![0] as string}
          size={size}
          className={clsx("data-recipient-logo", className)}
          {...rest}
        />
      );
    }

    return null;
  },
);

DataRecipientLogo.displayName = "DataRecipientLogo";
