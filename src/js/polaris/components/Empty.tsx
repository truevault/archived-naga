import ErrorIcon from "@mui/icons-material/Error";
import React from "react";

type Props = {
  children: React.ReactNode;
};

export const Empty: React.FC<Props> = ({ children }) => (
  <div className="empty-state-message">{children}</div>
);

type TabPanelEmptyProps = {
  message: string;
};

export const TabPanelEmpty: React.FC<TabPanelEmptyProps> = ({ message }) => (
  <Empty>{message}</Empty>
);

type EmptyInfoProps = { Icon?: typeof ErrorIcon } & Props;

export const EmptyInfo: React.FC<EmptyInfoProps> = ({ children, Icon = ErrorIcon }) => {
  return (
    <div className="flex flex-row align-items-center py-sm">
      {Icon && (
        <div className="color-primary mr-sm">
          <Icon style={{ verticalAlign: "middle" }} />
        </div>
      )}

      <div className="flex-grow">
        <p className="m-0 text-component-help">{children}</p>
      </div>
    </div>
  );
};
