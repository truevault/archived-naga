import clsx from "clsx";
import React from "react";

interface LabelledProgressBarsProps {
  items: any[];
  currentItemIndex: number;
  keyLabel: string;
}

export const LabelledProgressBars: React.FC<LabelledProgressBarsProps> = ({
  items,
  currentItemIndex,
  keyLabel,
}) => {
  return (
    <div className="labelled_progress">
      {items.map((item, index) => {
        return (
          <div className={clsx("item", { selected: index === currentItemIndex })} key={index}>
            <span>{item[keyLabel]}</span>
            <div className="progress_indicator"></div>
          </div>
        );
      })}
    </div>
  );
};
