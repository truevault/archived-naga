import React from "react";
import { Tabs as MaterialTabs, Tab, Box } from "@mui/material";
import clsx from "clsx";

type TabOption = { label: string; value: string };
type TabOptions = TabOption[];

type TabsProps = {
  options: TabOptions;
  className?: string;
  ariaLabel?: string;
  tab: string;
  setTab: (tab: string) => void;
  onChange?: (newTab: string) => void;
  textColor: "primary" | "secondary" | "inherit";
};

export const Tabs = ({
  options,
  className,
  ariaLabel,
  tab,
  setTab,
  onChange,
  textColor,
}: TabsProps) => {
  return (
    <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
      <MaterialTabs
        className={clsx("polaris_tabs", className)}
        value={tab}
        indicatorColor="primary"
        textColor={textColor || "primary"}
        onChange={(_, t: string) => {
          setTab(t);
          if (onChange) {
            onChange(t);
          }
        }}
        aria-label={ariaLabel}
        variant="scrollable"
        scrollButtons="auto"
      >
        {options.map((tabOption) => {
          return (
            <Tab
              className="polaris_tabs__tab"
              label={tabOption.label}
              value={tabOption.value}
              key={tabOption.value}
            />
          );
        })}
      </MaterialTabs>
    </Box>
  );
};

export const TabPanel: React.FC<{ value: string; tab: string; className?: string }> = ({
  value,
  tab,
  children,
  className = "",
}) => {
  if (value == tab) {
    return <div className={clsx(`tab-panel`, className)}>{children}</div>;
  }

  return null;
};
