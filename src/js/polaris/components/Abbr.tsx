import { Tooltip, TooltipProps } from "@mui/material";
import React from "react";

export const Abbr: React.FC<TooltipProps> = ({ children, ...rest }) => (
  <Tooltip {...rest}>
    <abbr>{children}</abbr>
  </Tooltip>
);
