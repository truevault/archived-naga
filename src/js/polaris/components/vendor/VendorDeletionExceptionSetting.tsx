import { FormControl, FormControlLabel, Radio, RadioGroup, Stack } from "@mui/material";
import React, { useMemo } from "react";
import { OrganizationVendor } from "../../../common/models";
import { CategoryDto } from "../../../common/service/server/dto/DataMapDto";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { RequestHandlingInstructionDto } from "../../../common/service/server/dto/RequestHandlingInstructionDto";
import {
  RetentionReason,
  RetentionReasonDto,
} from "../../../common/service/server/dto/RetentionReasonDto";
import { VendorDto } from "../../../common/service/server/dto/VendorDto";
import { DescriptionCheckbox } from "../input/DescriptionCheckbox";
import { InputHelpText } from "../input/InputHelpText";
import { RetentionReasonSelect } from "../input/RetentionReasonSelect";

export const REASONABLE_INTERNAL_USES = "reasonable-internal-uses";

const AFFIRMATION_EXPLANATION =
  "Data retained under the “reasonable internal uses” exception may be used only in a manner consistent with what a consumer would expect in the context in which your business collected the data. Check the box to affirm that your business’s use of the data conforms to this requirement. Otherwise, remove the “reasonable internal uses” exception from the drop-down.";

type VendorDeletionExceptionSettingProps = {
  vendor: OrganizationVendor | VendorDto;
  retentionReasons: RetentionReasonDto[];
  instruction: RequestHandlingInstructionDto;
  disclosed?: CategoryDto[];
  onRadioChange: (
    vendor: OrganizationDataRecipientDto | VendorDto,
    e: any,
    instruction: RequestHandlingInstructionDto,
  ) => void;
  onReasonChange: (
    vendor: OrganizationDataRecipientDto | VendorDto,
    reasons: RetentionReason[],
  ) => void;
  onAffirmationChange: (instruction: RequestHandlingInstructionDto, affirmed: boolean) => void;
  onRetentionExceptionsChange?: (
    instruction: RequestHandlingInstructionDto,
    hasException: boolean,
  ) => void;
  onDeletedPICChange?: (
    instruction: RequestHandlingInstructionDto,
    picId: string,
    deleted: boolean,
  ) => void;
  disabled?: boolean;
  error?: boolean;
  errorMessage?: React.ReactNode;
  helpText?: React.ReactNode;
  showShortAffirmation?: boolean;
};

export const VendorDeletionExceptionSetting: React.FC<VendorDeletionExceptionSettingProps> = ({
  vendor,
  retentionReasons,
  instruction,
  disclosed = [],
  onRadioChange,
  onReasonChange,
  onAffirmationChange,
  onRetentionExceptionsChange,
  onDeletedPICChange,
  disabled = false,
  error = undefined,
  errorMessage = null,
  helpText = null,
  showShortAffirmation = false,
}) => {
  const retained = instruction?.processingMethod === "RETAIN";

  const needsAffirmation = useMemo(
    () =>
      instruction?.retentionReasons?.includes(REASONABLE_INTERNAL_USES) ||
      instruction?.exceptionsAffirmed,
    [instruction],
  );

  const [disclosedLeft, disclosedRight] = disclosed.reduce(
    ([a, b], thing, i) => (i % 2 === 0 ? [[...a, thing], b] : [a, [...b, thing]]),
    [[], []],
  );
  return (
    <FormControl sx={{ width: "100%" }} error={error}>
      <RadioGroup
        onChange={(e) => onRadioChange(vendor, e, instruction)}
        value={instruction?.processingMethod || ""}
        className="mb-sm"
      >
        <FormControlLabel
          value={"DELETE"}
          control={<Radio color="primary" />}
          label={"No exceptions apply"}
          disabled={disabled}
        />

        <FormControlLabel
          value={"RETAIN"}
          control={<Radio color="primary" />}
          label={"Exceptions apply"}
          disabled={disabled}
        />
      </RadioGroup>

      {(retained || needsAffirmation) && (
        <div className="ml-xl w-100">
          {retained && (
            <>
              <RetentionReasonSelect
                vendor={vendor}
                fullWidth
                onChange={(e) => onReasonChange(vendor, e)}
                reasons={retentionReasons}
                instruction={instruction}
                disabled={disabled || !retained}
              />

              <InputHelpText error={error} errorMessage={errorMessage} helpText={helpText} />
            </>
          )}

          {needsAffirmation && (
            <DescriptionCheckbox
              label="Internal use is consistent with consumer expectations"
              description={showShortAffirmation ? undefined : AFFIRMATION_EXPLANATION}
              disabled={disabled}
              onChange={(_, checked) => onAffirmationChange(instruction, checked)}
              checked={instruction?.exceptionsAffirmed}
            />
          )}

          {retained && disclosed.length > 0 && (
            <DescriptionCheckbox
              label="Not all data retained, some data will be deleted from this vendor"
              onChange={(_, checked) => onRetentionExceptionsChange?.(instruction, checked)}
              checked={instruction?.hasRetentionExceptions}
            />
          )}

          {retained && instruction.hasRetentionExceptions && disclosed.length > 0 && (
            <div>
              <p>
                <strong>Optional:</strong> Select below any categories of data you know will be
                deleted rather than retained
              </p>

              <div className="d-flex flex-row border box-rounded border p-md">
                <Stack spacing={0} className="w-50">
                  {disclosedLeft.map((d) => {
                    return (
                      <DescriptionCheckbox
                        key={d.id}
                        label={d.name}
                        checked={instruction?.deletedPIC?.includes(d.id)}
                        onChange={(_, checked) => onDeletedPICChange?.(instruction, d.id, checked)}
                      />
                    );
                  })}
                </Stack>

                <Stack spacing={0} className="w-50">
                  {disclosedRight.map((d) => {
                    return (
                      <DescriptionCheckbox
                        key={d.id}
                        label={d.name}
                        checked={instruction?.deletedPIC?.includes(d.id)}
                        onChange={(_, checked) => onDeletedPICChange(instruction, d.id, checked)}
                      />
                    );
                  })}
                </Stack>
              </div>
            </div>
          )}
        </div>
      )}
    </FormControl>
  );
};
