import React, { ReactNode } from "react";
import { Paper } from "@mui/material";
import clsx from "clsx";

type VendorCatalogPanelProps = {
  children: ReactNode;
  padded?: boolean;
  row?: boolean;
  classes?: string;
};
export const Panel = ({ children, row = false, classes }: VendorCatalogPanelProps) => {
  return (
    <Paper square className={classes}>
      <div
        className={clsx(`large-panel`, {
          "large-panel--row": row,
        })}
      >
        {children}
      </div>
    </Paper>
  );
};

export const Sidebar = ({ children }) => {
  return <div className="large-panel__sidebar">{children}</div>;
};

export const Content = ({ children, padded = true }) => {
  return (
    <div
      className={clsx(`large-panel__content`, {
        "large-panel__content--no-padding": !padded,
      })}
    >
      {children}
    </div>
  );
};
