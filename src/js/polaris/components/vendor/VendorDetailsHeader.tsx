import React from "react";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";

type Props = { vendor: OrganizationDataRecipientDto };

export const DetailsHeader = ({ vendor }: Props) => {
  return (
    <div className="service-details-header">
      <div className="flex-row flex--expand">
        <dl className="data-point-1">
          <dt>Name</dt>
          <dd>
            <img src={vendor.logoUrl} className="service-details-header--logo mr-sm" />
            {vendor.name}
          </dd>
        </dl>

        <dl className="data-point-1">
          <dt>Category</dt>
          <dd>{vendor.category || <span>&nbsp;</span>}</dd>
        </dl>

        <dl className="data-point-1">
          <dt>Website</dt>
          <dd>
            <a href={vendor.url} target="_blank" rel="noopener noreferrer">
              {vendor.url}
            </a>
          </dd>
        </dl>

        <dl className="data-point-1"></dl>
      </div>
    </div>
  );
};
