import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import React from "react";
import { Disableable } from "../Disableable";
import { QuestionHeading } from "./QuestionHeading";

type VendorBooleanQuestionProps = {
  question: React.ReactNode;
  condensed: boolean;
  disabled?: boolean;
  disabledTooltip?: string;
  helpText: React.ReactNode;
  value: boolean;
  onChange: (value: boolean) => any;
  indented?: boolean;
  nextIndented?: boolean;
};

export const VendorBooleanQuestion: React.FC<VendorBooleanQuestionProps> = ({
  question,
  condensed,
  disabled,
  disabledTooltip,
  helpText,
  value,
  onChange,
  indented = false,
  nextIndented = false,
}) => {
  let val = "";
  if (value === true) {
    val = "yes";
  }
  if (value === false) {
    val = "no";
  }

  return (
    <QuestionHeading
      question={question}
      helpText={helpText}
      condensed={condensed}
      indented={indented}
      nextIndented={nextIndented}
      disabled={disabled}
    >
      <div
        style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end", minWidth: 256 }}
      >
        <RadioGroup row value={val} onChange={(e) => onChange(e.target.value == "yes")}>
          <Disableable disabled={disabled} disabledTooltip={disabledTooltip}>
            <FormControlLabel
              value="yes"
              label="Yes"
              control={<Radio checked={val == "yes"} />}
              disabled={disabled}
            />
          </Disableable>
          <Disableable disabled={disabled} disabledTooltip={disabledTooltip}>
            <FormControlLabel
              value="no"
              label="No"
              control={<Radio checked={val == "no"} />}
              disabled={disabled}
            />
          </Disableable>
        </RadioGroup>
      </div>
    </QuestionHeading>
  );
};
