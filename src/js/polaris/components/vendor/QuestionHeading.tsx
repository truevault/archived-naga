import React from "react";
import { SubdirectoryArrowRight as SubdirectoryArrowRightIcon } from "@mui/icons-material";
import { HelpPopover } from "../HelpPopover";
import { Paras } from "../typography/Paras";
import clsx from "clsx";

type QuestionHeadingProps = {
  question: React.ReactNode;
  condensed?: boolean;
  helpText?: React.ReactNode;
  vertical?: boolean;
  indented?: boolean;
  nextIndented?: boolean;
  disabled?: boolean;
};

export const QuestionHeading: React.FC<QuestionHeadingProps> = ({
  question,
  condensed = false,
  helpText,
  children,
  vertical,
  indented = false,
  nextIndented = false,
  disabled = false,
}) => {
  const rowClass = clsx({
    "text-disabled": disabled,
    "mb-0": condensed,
    "mb-xl": !condensed && !nextIndented,
    "mb-mdlg": !condensed && nextIndented,
    "pl-lg": indented,
  });
  const direction = vertical ? "column" : "row";

  const marginTop = indented && condensed ? -8 : 0;

  const icon = indented && <SubdirectoryArrowRightIcon />;

  return (
    <div
      style={{ display: "flex", flexDirection: direction, alignItems: "flex-start", marginTop }}
      className={rowClass}
    >
      <div className="mb-0 flex-grow text-component-question">
        {condensed && (
          <span className="flex-center-vertical">
            <div className="subdirectory-arrow"> {icon} </div> {question}{" "}
            {helpText && <HelpPopover label={helpText} className="ml-sm" />}
          </span>
        )}
        {!condensed && (
          <div className="flex-row">
            {Boolean(icon) && <div className="mr-xs subdirectory-arrow">{icon}</div>}
            <div>
              <h5 className="mt-0 mb-xxs text-component-question">{question}</h5>
              {helpText && (
                <div className="text-component-help">
                  <Paras>{helpText}</Paras>
                </div>
              )}
            </div>
          </div>
        )}
      </div>

      {children}
    </div>
  );
};
