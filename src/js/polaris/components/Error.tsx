import React from "react";

type ErrorProps = {
  message: string;
};

export const TabPanelError = ({ message }: ErrorProps) => {
  return <div className="tab-panel-content--fill text-error text-h4">{message}</div>;
};

export const Error = ({ message }: ErrorProps) => (
  <div className="p-md">
    <div className="fill text-error">
      <h6>{message}</h6>
    </div>
  </div>
);
