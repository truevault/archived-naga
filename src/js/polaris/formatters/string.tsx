import React from "react";

export const fmtPreserveNewlines = (str) => {
  if (!str) {
    return null;
  }
  const lines = str.split("\n");
  const br = lines.map((l, i) => <br key={i} />);

  const result = lines.reduce(function (arr, v, i) {
    return arr.concat(v, br[i]);
  }, []);

  result.splice(-1, 1);

  return React.createElement(React.Fragment, null, result);
};

export const oxfordJoin = (arr: string[]): string => {
  if (!arr || arr.length == 0) return "";
  const len = arr.length;
  if (len === 1) {
    return arr[0];
  } else if (len === 2) {
    return arr.join(" and ");
  }
  return arr.map((p, ix) => (len - 1 === ix ? `and ${p}` : p)).join(", ");
};
