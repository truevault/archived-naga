import { unreachable } from "../util";
import {
  DataRequestTypeDto,
  PrivacyRegulationName,
} from "../../common/service/server/dto/DataRequestTypeDto";
import { DataRequestState } from "../../common/service/server/Types";

export const fmtShortRequestType = (type: DataRequestTypeDto) => {
  switch (type) {
    case "CCPA_OPT_OUT":
    case "VCDPA_OPT_OUT":
    case "CPA_OPT_OUT":
    case "CTDPA_OPT_OUT":
      return "Opt-Out";

    case "CCPA_RIGHT_TO_KNOW_CATEGORIES":
    case "CCPA_RIGHT_TO_KNOW":
      return "Know";
    case "CCPA_RIGHT_TO_DELETE":
      return "Delete";
    case "GDPR_ACCESS":
      return "Access";
    case "GDPR_ERASURE":
      return "Erasure";
    case "GDPR_OBJECTION":
      return "Objection";
    case "GDPR_RECTIFICATION":
      return "Rectification";
    case "GDPR_WITHDRAWAL":
      return "Withdrawal";
    case "GDPR_PORTABILITY":
      return "Portability";
    case "CCPA_RIGHT_TO_LIMIT":
      return "Limit";
    case "CCPA_CORRECTION":
      return "Correct";
    case "VCDPA_ACCESS":
      return "Access";
    case "VCDPA_DELETION":
      return "Deletion";
    case "VCDPA_CORRECTION":
      return "Correct";
    case "VCDPA_APPEAL":
      return "Appeal";
    case "CPA_ACCESS":
      return "Access";
    case "CPA_DELETION":
      return "Deletion";
    case "CPA_CORRECTION":
      return "Correct";
    case "CPA_APPEAL":
      return "Appeal";
    case "CTDPA_ACCESS":
      return "Access";
    case "CTDPA_DELETION":
      return "Deletion";
    case "CTDPA_CORRECTION":
      return "Correct";
    case "CTDPA_APPEAL":
      return "Appeal";
    case "VOLUNTARY_RIGHT_TO_KNOW":
      return "Access";
    case "VOLUNTARY_RIGHT_TO_DELETE":
      return "Deletion";
    default:
      return unreachable(type);
  }
};

export const fmtShortRequestTypeWithRegulation = (type: DataRequestTypeDto) => {
  const short = fmtShortRequestType(type);

  switch (type) {
    case "CCPA_OPT_OUT":
    case "CCPA_RIGHT_TO_KNOW_CATEGORIES":
    case "CCPA_RIGHT_TO_KNOW":
    case "CCPA_RIGHT_TO_DELETE":
    case "CCPA_RIGHT_TO_LIMIT":
    case "CCPA_CORRECTION":
      return `CCPA - ${short}`;
    case "VCDPA_OPT_OUT":
    case "VCDPA_ACCESS":
    case "VCDPA_DELETION":
    case "VCDPA_CORRECTION":
    case "VCDPA_APPEAL":
      return `VCDPA - ${short}`;
    case "CPA_OPT_OUT":
    case "CPA_ACCESS":
    case "CPA_DELETION":
    case "CPA_CORRECTION":
    case "CPA_APPEAL":
      return `CPA - ${short}`;
    case "CTDPA_OPT_OUT":
    case "CTDPA_ACCESS":
    case "CTDPA_DELETION":
    case "CTDPA_CORRECTION":
    case "CTDPA_APPEAL":
      return `CTDPA - ${short}`;
    case "GDPR_ACCESS":
    case "GDPR_ERASURE":
    case "GDPR_OBJECTION":
    case "GDPR_RECTIFICATION":
    case "GDPR_WITHDRAWAL":
    case "GDPR_PORTABILITY":
      return `GDPR - ${short}`;
    case "VOLUNTARY_RIGHT_TO_KNOW":
    case "VOLUNTARY_RIGHT_TO_DELETE":
      return `Voluntary - ${short}`;
    default:
      return unreachable(type);
  }
};

export const fmtStateShort = (jurisdiction: PrivacyRegulationName) => {
  switch (jurisdiction) {
    case "CCPA":
      return "CA";
    case "VCDPA":
      return "VA";
    case "CPA":
      return "CO";
    case "CTDPA":
      return "CT";
    case "GDPR":
      return "EU";
    case "VOLUNTARY":
      return "";
    default:
      return unreachable(jurisdiction);
  }
};

export const fmtLongRequestType = (type: DataRequestTypeDto) => {
  switch (type) {
    case "VCDPA_OPT_OUT":
    case "CPA_OPT_OUT":
    case "CTDPA_OPT_OUT":
    case "CCPA_OPT_OUT":
      return "Request to Opt-Out";
    case "CCPA_RIGHT_TO_KNOW_CATEGORIES":
    case "CCPA_RIGHT_TO_KNOW":
      return "Request to Know";
    case "CCPA_RIGHT_TO_DELETE":
      return "Request to Delete";
    case "CCPA_CORRECTION":
      return "Request to Correct";

    case "VCDPA_ACCESS":
      return "Access Request";
    case "VCDPA_DELETION":
      return "Deletion Request";
    case "VCDPA_CORRECTION":
      return "Request to Correct";
    case "VCDPA_APPEAL":
      return "Appeal a Decision";

    case "CPA_ACCESS":
      return "Access Request";
    case "CPA_DELETION":
      return "Deletion Request";
    case "CPA_CORRECTION":
      return "Request to Correct";
    case "CPA_APPEAL":
      return "Appeal a Decision";

    case "CTDPA_ACCESS":
      return "Access Request";
    case "CTDPA_DELETION":
      return "Deletion Request";
    case "CTDPA_CORRECTION":
      return "Request to Correct";
    case "CTDPA_APPEAL":
      return "Appeal a Decision";

    case "GDPR_ACCESS":
      return "Access Request";
    case "GDPR_ERASURE":
      return "Erasure Request";
    case "GDPR_OBJECTION":
      return "Objection to Processing";
    case "GDPR_RECTIFICATION":
      return "Rectification Request";
    case "GDPR_WITHDRAWAL":
      return "Withdrawal of Consent";
    case "GDPR_PORTABILITY":
      return "Portability Request";
    case "CCPA_RIGHT_TO_LIMIT":
      return "Request to Limit";

    case "VOLUNTARY_RIGHT_TO_KNOW":
      return "Access Request";
    case "VOLUNTARY_RIGHT_TO_DELETE":
      return "Deletion Request";

    default:
      return unreachable(type);
  }
};

export const fmtLongRequestTypeWithRegulation = (type: DataRequestTypeDto) => {
  const long = fmtLongRequestType(type);

  switch (type) {
    case "CCPA_OPT_OUT":
    case "CCPA_RIGHT_TO_KNOW_CATEGORIES":
    case "CCPA_RIGHT_TO_KNOW":
    case "CCPA_RIGHT_TO_DELETE":
    case "CCPA_RIGHT_TO_LIMIT":
    case "CCPA_CORRECTION":
      return `CCPA - ${long}`;
    case "VCDPA_OPT_OUT":
    case "VCDPA_ACCESS":
    case "VCDPA_DELETION":
    case "VCDPA_CORRECTION":
    case "VCDPA_APPEAL":
      return `VCDPA - ${long}`;
    case "CPA_OPT_OUT":
    case "CPA_ACCESS":
    case "CPA_DELETION":
    case "CPA_CORRECTION":
    case "CPA_APPEAL":
      return `CPA - ${long}`;
    case "CTDPA_OPT_OUT":
    case "CTDPA_ACCESS":
    case "CTDPA_DELETION":
    case "CTDPA_CORRECTION":
    case "CTDPA_APPEAL":
      return `CTDPA - ${long}`;
    case "GDPR_ACCESS":
    case "GDPR_ERASURE":
    case "GDPR_OBJECTION":
    case "GDPR_RECTIFICATION":
    case "GDPR_WITHDRAWAL":
    case "GDPR_PORTABILITY":
      return `GDPR - ${long}`;
    case "VOLUNTARY_RIGHT_TO_KNOW":
    case "VOLUNTARY_RIGHT_TO_DELETE":
      return `Voluntary - ${long}`;
    default:
      return unreachable(type);
  }
};

type FormattedStatus = "Pending Verification" | "Closed" | "New" | "In Progress";

export const fmtRequestTypeState = (state: DataRequestState): FormattedStatus => {
  switch (state) {
    case "PENDING_EMAIL_VERIFICATION":
      return "Pending Verification";
    case "CLOSED":
      return "Closed";
    case "IDENTITY_VERIFICATION":
    case "NEW":
    case "VERIFY_CONSUMER":
      return "New";
    case "CONSUMER_GROUP_SELECTION":
    case "PROCESSING":
    case "DELETE_INFO":
    case "CONTACT_VENDORS":
    case "NOTIFY_CONSUMER":
    case "REMOVE_CONSUMER":
    case "RETRIEVE_DATA":
    case "SEND_DATA":
    case "CORRECT_DATA":
    case "EVALUATE_OBJECTION":
    case "REMOVE_CONSENT":
    case "LIMIT_DATA":
      return "In Progress";
    default:
      return unreachable(state);
  }
};
