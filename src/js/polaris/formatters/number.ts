export const localizeNumber = (val: number, options?: Intl.NumberFormatOptions): string => {
  // TODO: Default locale is implementation-specific and not necessarily well-detected. This is likely not sufficient localization for all cases.
  return val.toLocaleString(window.navigator.language, options);
};

export const fmtCount = (value: number): string =>
  localizeNumber(value, { maximumFractionDigits: 0 });

export const fmtFloat = (value: number, nan = "-"): string => {
  if (isNaN(value)) {
    return nan;
  }
  return localizeNumber(value, { maximumFractionDigits: 1 });
};

export const fmtPct = (value: number, nan = "-"): string => {
  if (isNaN(value)) {
    return nan;
  }
  const percent = Math.max(Math.min(100, value * 100), 0);

  return `${localizeNumber(percent, { maximumFractionDigits: 0 })}%`;
};
