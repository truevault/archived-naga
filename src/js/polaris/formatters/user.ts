import { UserDetailsDto, UserDto } from "../../common/service/server/dto/UserDto";
import { fmtFullName as formatFullName } from "./name";

export const fmtFullName = (
  user?: UserDetailsDto | UserDto,
  currentUser?: UserDetailsDto,
): string => {
  if (currentUser?.email == user?.email) {
    return "You";
  }

  return formatFullName(user?.firstName, user?.lastName);
};
