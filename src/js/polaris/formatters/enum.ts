import { GdprCookieConsentCategory } from "../../common/service/server/dto/GdprCookieConsentDto";
import {
  DataRequestFilter,
  MailboxStatus,
  ProcessingMethod,
} from "../../common/service/server/Types";
import { unreachable } from "../util";

// export const fmtRequestType = (type: DataRequestType): string => {
//   switch (type) {
//     case "GDPR_ACCESS":
//       return "Access Request";
//     case "GDPR_ERASURE":
//       return "Erasure Request";
//     case "GDPR_OBJECTION":
//       return "Objection to Processing";
//     case "GDPR_RECTIFICATION":
//       return "Rectification Request";
//     case "GDPR_WITHDRAWAL":
//       return "Withdrawal of Consent";
//   }

//   return unreachable(type);
// };

// export const fmtShortRequestType = (type: DataRequestType): string => {
//   switch (type) {
//     case "GDPR_ACCESS":
//       return "Access";
//     case "GDPR_ERASURE":
//       return "Erasure";
//     case "GDPR_OBJECTION":
//       return "Objection";
//     case "GDPR_RECTIFICATION":
//       return "Rectification";
//   }

//   return unreachable(type);
// };

export const fmtFilter = (filter: DataRequestFilter): string => {
  switch (filter) {
    case "active":
      return "Active";
    case "complete":
      return "Closed";
    case "pending":
      return "Pending";
  }
  return unreachable(filter);
};

export const fmtMailboxStatus = (mailboxStatus: MailboxStatus): string => {
  switch (mailboxStatus) {
    case "CONNECTED":
      return "Syncing";
    case "DISCONNECTED":
      return "Not Syncing";
  }

  return unreachable(mailboxStatus);
};

export const fmtGdprCookieConsent = (consent: GdprCookieConsentCategory) => {
  switch (consent) {
    case "ADS":
      return "Ads";
    case "ANALYTICS":
      return "Analytics";
    case "ESSENTIAL":
      return "Essential";
    case "PERSONALIZATION":
      return "Personalization";
  }

  return unreachable(consent);
};

export const fmtProcessingMethod = (method: ProcessingMethod): string => {
  switch (method) {
    case "DELETE":
      return "Delete all";
    case "RETAIN":
      return "Retain some/all data";
    case "INACCESSIBLE_OR_NOT_STORED":
      return "Inaccessible or not stored";
  }

  return unreachable(method);
};
