export const fmtFullName = (first: string, last: string, def = ""): string => {
  const names = [first, last]
    .filter((n) => n)
    .join(" ")
    .trim();

  if (!names) {
    return def;
  }

  return names;
};
