import { DataRequestDetail, DataRequestSummary } from "../../common/models";
import { fmtFullName as formatFullName } from "./name";

export const fmtFullName = (request: DataRequestSummary | DataRequestDetail): string =>
  formatFullName(request.subjectFirstName, request.subjectLastName, "Website Visitor");
