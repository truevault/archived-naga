import * as Enum from "./enum";
import * as DataSubject from "./dataSubject";
import * as User from "./user";
import * as Date from "./date";
import * as String from "./string";
import * as Number from "./number";

export const Fmt = { Enum, DataSubject, User, Date, String, Number };
