import React from "react";
import moment from "moment";
import clsx from "clsx";
import { Iso8601Date } from "../../common/service/server/Types";

export const DATE_FORMAT_SHORT = "L";
export const DATE_FORMAT_MEDIUM = "ll"; // lowercase LL
export const DATE_FORMAT_LONG = "LL";
export const DATE_TIME_FORMAT_SHORT = "lll"; // lowercase LLL
export const DATE_TIME_FORMAT_MEDIUM = "LLL";
export const DATE_TIME_FORMAT_LONG = "LLLL";
export const DATE_TIME_FORMAT_YMD = "YYYY-MM-DD h:mm:ss a";

const remainingString = (days: number) => {
  if (days == 1) {
    return "1 day remaining";
  } else if (days == -1) {
    return "1 day overdue";
  } else if (days < 0) {
    return `${-days} days overdue`;
  } else {
    return `${days} days remaining`;
  }
};

const daysString = (days: number) => {
  if (days == 1) {
    return "1 day";
  } else {
    return `${days} days`;
  }
};

export const fmtDate = (date?: Iso8601Date, format: string = DATE_FORMAT_MEDIUM) => {
  if (!date) {
    return "";
  }
  const dateMoment = moment(date);
  return dateMoment.format(format);
};

export const fmtDateFromNow = (
  date?: Iso8601Date,
  shorten = false,
  invalidMessage: string = "Invalid date",
) => {
  if (!date) {
    return "";
  }

  if (!moment(date).isValid()) {
    return invalidMessage;
  }

  if (shorten) {
    // sorry everywhere else: we've replaced window.navigator.language with 'en-US'
    // because we specifically want these not to be localized.
    moment.updateLocale("en", {
      relativeTime: {
        s: "1s",
        ss: "%ds",
        m: "1min",
        mm: "%dmin",
        h: "1h",
        hh: "%dh",
        d: "1d",
        dd: "%dd",
        M: "1m",
        MM: "%dm",
        y: "1y",
        yy: "%dy",
      },
    });
  }
  moment.locale("en");
  const result = moment(date).fromNow();
  moment.locale(window.navigator.language);

  if (shorten) {
    moment.updateLocale("en", null); // Revert 'en-US' locale update update
  }

  return result;
};

export const fmtCompletedDate = (
  completed?: Iso8601Date,
  withInterval = true,
  received?: Iso8601Date,
  format: string = DATE_FORMAT_MEDIUM,
) => {
  if (!completed) {
    return null;
  }

  const completedMoment = moment(completed);
  const formatted = completedMoment.format(format);

  if (withInterval && received) {
    const receivedMoment = moment(received);
    const days = Math.round(completedMoment.diff(receivedMoment, "days"));

    return `${formatted} (completed in ${daysString(days)})`;
  } else {
    return formatted;
  }
};

export const classesForDaysRemaining = (days: number) => {
  return clsx({
    "text-error": days <= 3,
    "text-warning": days <= 10 && days > 3,
  });
};

export const deadlineDaysRemaining = (deadline?: Iso8601Date) => {
  if (!deadline) {
    return null;
  }

  const deadlineMoment = moment(deadline);
  const days = Math.round(deadlineMoment.diff(moment(), "days"));

  const alertClasses = classesForDaysRemaining(days);

  return (
    <>
      (
      <span className={alertClasses} dir="ltr">
        {remainingString(days)}
      </span>
      )
    </>
  );
};
