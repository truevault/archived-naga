import { Typography } from "@mui/material";
import clsx from "clsx";
import React from "react";

export const ActionTile: React.FC<{
  title: string;
  description?: string;
  onClick?: () => void;
  icon?: React.ReactNode;
  actions?: React.ReactNode;
  selected?: boolean;
}> = ({ title, description, onClick, icon = null, actions = null, selected = false }) => {
  return (
    <div
      onClick={onClick}
      className={clsx(
        "vendor-tile vendor-tile--row border box-rounded flex flex-row p-md align-items-center",
        {
          "vendor-tile--selected": selected,
          "vendor-tile--clickable": Boolean(onClick),
        },
      )}
    >
      {/* LOGO */}
      {icon}

      {/* NAME/CATEGORY */}
      <div className="flex flex-col flex-grow" style={{ minWidth: 0 }}>
        <Typography variant="body2" className="nowrap text-ellipsis">
          {title}
        </Typography>

        {description && (
          <Typography variant="caption" className="text-muted nowrap text-ellipsis">
            {description}
          </Typography>
        )}
      </div>

      {/* END COMPONENT */}
      {!onClick && Boolean(actions) && <div style={{ whiteSpace: "nowrap" }}>{actions}</div>}
    </div>
  );
};
