// Must come before react and react dom
import "react-hot-loader";

import React from "react";
import ReactDOM from "react-dom";
import { App } from "./root/App";

const mountEl = document.getElementById("mount");

if (mountEl) {
  ReactDOM.render(React.createElement(App, null), mountEl);
}
