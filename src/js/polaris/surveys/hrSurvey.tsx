import { SurveyAnswers } from "../../common/hooks/useSurvey";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { Survey } from "./survey";

export const HR_SURVEY_NAME = "hr-survey";

const hrQuestions = (answers: SurveyAnswers): SurveyQuestionProps[] => {
  return [
    californiaEmployeesQuestion(answers), // "business-employees-ca"
    californiaContractorsQuestion(answers), // "business-contractors-ca"
    californiaJobApplicantsQuestion(answers), // "business-job-applicants-ca"
    eeaUkEmployeesQuestion(answers), // "employees-eea-uk"
  ];
};

const hrVisibility = (_answers: SurveyAnswers, isGdpr: boolean): Record<string, boolean> => {
  return {
    "business-employees-ca": true,
    "business-contractors-ca": true,
    "business-job-applicants-ca": true,
    "employees-eea-uk": isGdpr,
  };
};

export const hrSurvey = (answers: SurveyAnswers, isGdpr: boolean): Survey => {
  return {
    questions: hrQuestions(answers),
    visibility: hrVisibility(answers, isGdpr),
    disabled: {},
  };
};

export const californiaEmployeesQuestion = (answers: SurveyAnswers): SurveyQuestionProps => {
  const slug = "business-employees-ca";

  return {
    type: "boolean",
    slug: slug,
    question: "Does your business have employees in California?",
    helpText: "This includes directors, owners, investors, or medical staff.",
    noConfirmation:
      answers[slug] === "true"
        ? "Are you sure you would like to change your answer? All data mapping for this employment group will be deleted."
        : undefined,
  };
};

export const californiaContractorsQuestion = (answers: SurveyAnswers): SurveyQuestionProps => {
  const slug = "business-contractors-ca";

  return {
    type: "boolean",
    slug: slug,
    question: "Does your business hire individual contractors in California?",
    helpText:
      'For example, 1099 employees. Answer "no" if you only hire contractors through agencies or contracting businesses.',
    noConfirmation:
      answers[slug] === "true"
        ? "Are you sure you would like to change your answer? All data mapping for this employment group will be deleted."
        : undefined,
  };
};

export const californiaJobApplicantsQuestion = (answers: SurveyAnswers): SurveyQuestionProps => {
  const slug = "business-job-applicants-ca";

  return {
    type: "boolean",
    slug: slug,
    question: "Does your business collect personal information about job applicants in California?",
    noConfirmation:
      answers[slug] === "true"
        ? "Are you sure you would like to change your answer? All data mapping for this employment group will be deleted."
        : undefined,
  };
};

export const eeaUkEmployeesQuestion = (answers: SurveyAnswers): SurveyQuestionProps => {
  const slug = "employees-eea-uk";

  return {
    type: "boolean",
    slug: slug,
    question:
      "Does your business have workers (e.g., employees or contractors) living in the EEA/UK?",
    noConfirmation:
      answers[slug] === "true"
        ? "Are you sure you would like to change your answer? All data mapping for this employment group will be deleted."
        : undefined,
  };
};
