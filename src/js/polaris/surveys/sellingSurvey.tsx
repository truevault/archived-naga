import uniqBy from "lodash.uniqby";
import React from "react";
import { Link } from "react-router-dom";
import { SurveyAnswers } from "../../common/hooks/useSurvey";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
} from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { Routes } from "../root/routes";
import { findGoogleAnalytics, findShopify } from "../util/vendors";
import { onlyGaSelectedForSelling } from "./getAnswer";
import { Survey } from "./survey";
import { alphabetically, vendorToOption } from "./util";

const sellingInstructions = (): SurveyQuestionProps => {
  return {
    type: "acknowledgement",
    question: 'What is data "selling"?',
    slug: "selling-definition-acknowledgement",
    text: (
      <>
        <p>
          Consumers also have the right to opt-out of data selling. “Selling" is broader than you
          might think and involves exchanging personal information for money or anything else of
          value. It may include disclosing personal information to a vendor and receiving a free
          service in return.
        </p>
        <p>
          “Selling” does not include situations where a consumer uses your business, platform, or
          service to send their data to a specific third party. These are called “intentional
          interactions.”
        </p>
      </>
    ),
  };
};

export const intentionalInteractionQuestion = (
  vendors: OrganizationDataRecipientDto[],
): SurveyQuestionProps => {
  const walletServiceVendors = (vendors ?? [])
    .filter((v) => v.collectionContext.includes("CONSUMER"))
    .filter((v) => v.isAutomaticIntentionalInteractionVendor);
  return {
    type: "boolean",
    slug: "disclosure-for-intentional-interaction",
    question: `Do consumers use your business to disclose their personal information to any third parties through an intentional interaction?`,
    helpText: (
      <>
        An intentional interaction is when the third party is visible and named on your website or
        application. The consumer knowingly uses or sends their data to that specific party. For
        example, clicking a "Pay with PayPal" button or choosing FedEx as a shipping option.
      </>
    ),
    noDisabled: walletServiceVendors.length > 0,
    noDisabledTooltip:
      "Wallet service vendors are automatically treated as intentional interactions.",
  };
};

export const intentionalInteractionVendorQuestion = (
  vendors: OrganizationDataRecipientDto[],
): SurveyQuestionProps => {
  const intentionalInteractionVendors = vendors
    .filter((v) => v.collectionContext.includes("CONSUMER"))
    .filter((v) => v.isPotentialIntentionalInteractionVendor);

  const intentionalInteractionVendorOptions = intentionalInteractionVendors
    .sort(alphabetically)
    .map((v: OrganizationDataRecipientDto) => {
      if (v.isAutomaticIntentionalInteractionVendor) {
        return vendorToOption(v, {
          value: true,
          disabled: true,
          title: "Wallet services are considered an intentional interaction.",
        });
      }

      return vendorToOption(v);
    });

  return {
    type: "checkboxes",
    slug: "vendor-intentional-disclosure-selection",
    question: <>Which parties do consumers intentionally interact with?</>,
    helpText: (
      <>
        Some of your vendors are not included below because this question doesn’t apply to them. If
        you need to add a vendor, go back to{" "}
        <Link className={"bold-link"} to={Routes.getCompliant.dataRecipients.root}>
          Vendors
        </Link>
        .
      </>
    ),
    plaintextQuestion: "Which third parties receive intentionally disclosed personal information?",
    emptyText: "There are no vendors available.",
    options: uniqBy(intentionalInteractionVendorOptions, (v) => v.key),
  };
};

export const createProfilesAboutConsumersQuestion = (): SurveyQuestionProps => {
  return {
    type: "boolean",
    slug: "create-profiles-about-consumers",
    question:
      "Does your business create profiles about consumers which it uses to make decisions having legal or similarly significant effects on consumers?",
    helpText: (
      <>
        “Similarly significant effects” include decisions by your business to provide or deny a
        person money or credit, housing, insurance, education enrollment, criminal justice,
        employment opportunities, health care services, or access to basic necessities, such as food
        and water.
      </>
    ),
  };
};

export const sellInExchangeForMoneyQuestion = (): SurveyQuestionProps => {
  return {
    type: "boolean",
    slug: "sell-in-exchange-for-money",
    question: "Does your business sell personal information to others in exchange for money?",
    helpText: <></>,
  };
};

export const sellingDisclosureQuestion = (): SurveyQuestionProps => {
  return {
    type: "boolean",
    slug: "disclosure-for-selling",
    question:
      "Does your business disclose — or give third parties access to — personal data about its consumers and receive free or discounted services, products, access to data, or anything else of value in return?",
    helpText: (
      <>
        This is considered “selling” under the broad definition in some state privacy laws and it
        requires that you give consumers the ability to opt-out. Answer “yes” if you use Google
        Analytics as a free service, if you disclose consumer data to marketing cooperatives and
        receive access to product, services, or special pricing in return, or if you disclose data
        in similar scenarios.
      </>
    ),
  };
};

export const googleAnalyticsQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "google-analytics-data-sharing",
  question: "Has your business turned off (or will it turn off) data sharing in Google Analytics?",
  helpText: (
    <>
      By default, Google accesses consumer data (for example, data collected from website visitors)
      to enhance its services. If your business turns off data sharing, Google will no longer access
      that data. If data sharing is on, your business is “selling” data and will need to include an
      opt-out link on its website.{" "}
      <a
        href="https://support.google.com/analytics/answer/1011397?hl=en&ref_topic=2919631"
        target="_blank"
        rel="noreferrer noopener"
      >
        How to turn off data sharing in Google Analytics
      </a>
      .
    </>
  ),
});

export const shopifyAudiencesQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "shopify-audiences",
  question: "Has your business installed the Shopify Audiences app and enabled Shopify Audiences?",
  helpText: (
    <>
      With Shopify Audiences enabled, Shopify automatically shares your customers’ data with
      Facebook for marketing purposes. If you enable Shopify Audiences, your business is “selling”
      data to Shopify and will need to include an opt-out link on its website.
    </>
  ),
});

export const sellingVendorsQuestion = (
  vendors: OrganizationDataRecipientDto[],
): SurveyQuestionProps => {
  const sellingVendors = vendors
    .filter((v) => v.collectionContext.includes("CONSUMER"))
    .filter((v) => v.isPotentialSellingVendor);
  const sellingVendorOptions = sellingVendors.sort(alphabetically).map((v) => {
    if (v.isAutomaticSellingVendor) {
      return vendorToOption(v, {
        value: true,
        disabled: true,
        title:
          "You have indicated you are using features which require this data recipient to be considered “selling” data.",
      });
    }
    return vendorToOption(v);
  });

  return {
    type: "checkboxes",
    slug: "vendor-selling-selection",
    question: `Which third parties does your business "sell" to?`,
    helpText: (
      <>
        Some of your vendors are excluded from this list because, based on our research, they have
        publicly stated that they do not use data in a manner that requires consumer opt-out rights.
      </>
    ),
    emptyText: "There are no vendors available.",
    options: uniqBy(sellingVendorOptions, (v) => v.key),
  };
};

export const sellingUploadDataQuestion = (): SurveyQuestionProps => {
  return {
    type: "boolean",
    slug: "selling-upload-data",
    question:
      "Does your business upload, transmit, or grant access to consumer data to any of the third parties it “sells” to?",
    helpText: (
      <>
        For example, if your business sends customer lists to other organizations, such as marketing
        cooperatives.
      </>
    ),
  };
};

export const sellingUploadVendorsSelectionQuestion = (
  vendors: OrganizationDataRecipientDto[],
): SurveyQuestionProps => {
  const uploadVendors = vendors
    .filter((v) => v.collectionContext.includes("CONSUMER"))
    .filter((v) => v.isPotentialUploadVendor);
  const uploadVendorOptionsOptions = uploadVendors.sort(alphabetically).map((v) => {
    if (v.isAutomaticUploadVendor) {
      return vendorToOption(v, {
        value: true,
        disabled: true,
        title: "Selling to this data recipient requires manual transmission of data.",
      });
    }
    return vendorToOption(v);
  });

  return {
    type: "checkboxes",
    slug: "vendor-upload-data-selection",
    question: `Which third parties do you manually send data to?`,
    helpText: (
      <>
        Do not select any third parties where data is collected only through automated means on your
        website, such as via a tracker, cookie, or other systematic method.
      </>
    ),
    emptyText: "There are no vendors available.",
    options: uniqBy(uploadVendorOptionsOptions, (v) => v.key),
  };
};

const sellingQuestions = (
  answers: SurveyAnswers,
  vendors: OrganizationDataRecipientDto[],
): SurveyQuestionProps[] => {
  return [
    sellingInstructions(), // "selling-definition-acknowledgement"
    intentionalInteractionQuestion(vendors), // "disclosure-for-intentional-interaction"
    intentionalInteractionVendorQuestion(vendors), // "vendor-intentional-disclosure-selection"
    googleAnalyticsQuestion(), // "google-analytics-data-sharing"
    shopifyAudiencesQuestion(), // "shopify-audiences"
    createProfilesAboutConsumersQuestion(), // create-profiles-for-consumers
    sellInExchangeForMoneyQuestion(), // sell-in-exchange-for-money
    sellingDisclosureQuestion(), // "disclosure-for-selling"
    sellingVendorsQuestion(vendors), // "vendor-selling-selection",
    sellingUploadDataQuestion(), // "selling-upload-data"
    sellingUploadVendorsSelectionQuestion(vendors), // "vendor-upload-data-selection"
  ];
};

const sellingVisibility = (
  answers: SurveyAnswers,
  vendors: OrganizationDataRecipientDto[],
): Record<string, boolean> => {
  const googleAnalytics = findGoogleAnalytics(vendors);
  const shopify = findShopify(vendors);
  const showGAQuestion = Boolean(googleAnalytics);
  const showShopifyQuestion = Boolean(shopify);
  const onlyGaSelected = onlyGaSelectedForSelling(answers, vendors);

  return {
    "disclosure-for-intentional-interaction": Boolean(
      answers["selling-definition-acknowledgement"],
    ),
    "vendor-intentional-disclosure-selection":
      answers["disclosure-for-intentional-interaction"] == "true",
    "vendor-selling-selection": answers["disclosure-for-selling"] == "true",
    "google-analytics-data-sharing": showGAQuestion,
    "shopify-audiences": showShopifyQuestion,
    "selling-upload-data": answers["disclosure-for-selling"] == "true" && !onlyGaSelected,
    "vendor-upload-data-selection":
      answers["disclosure-for-selling"] == "true" &&
      answers["selling-upload-data"] == "true" &&
      !onlyGaSelected,
  };
};

// Returns true, false, or reason for true
const sellingDisabled = (
  answers: SurveyAnswers,
  vendors: OrganizationDataRecipientDto[],
): Record<string, boolean | string> => {
  const showGAQuestion = Boolean(findGoogleAnalytics(vendors));
  const showShopifyQuestion = Boolean(findShopify(vendors));

  const gaDisclosure = showGAQuestion && answers["google-analytics-data-sharing"] == "false";
  const shopifyDisclosure = showShopifyQuestion && answers["shopify-audiences"] == "true";

  return {
    "disclosure-for-selling":
      (gaDisclosure || shopifyDisclosure) &&
      "‘Yes’ is selected here based on your answers to the previous questions.",
    "selling-upload-data":
      shopifyDisclosure &&
      "‘Yes’ is selected here based on your answers to the previous questions.",
  };
};

export const sellingSurvey = (
  answers: SurveyAnswers,
  vendors: OrganizationDataRecipientDto[],
): Survey => {
  return {
    questions: sellingQuestions(answers, vendors),
    visibility: sellingVisibility(answers, vendors),
    disabled: sellingDisabled(answers, vendors),
  };
};

export const hasIntentionalInteractionVendors = (
  vendors: OrganizationDataRecipientDto[],
  contexts: CollectionContext[] = ["CONSUMER"],
) =>
  (vendors ?? []).some(
    (v) =>
      contexts.some((ctx) => v.collectionContext.includes(ctx)) &&
      v.isAutomaticIntentionalInteractionVendor,
  );
