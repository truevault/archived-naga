import React from "react";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../common/service/server/Types";
import { AddDataSourceQuestionProps } from "../components/get-compliant/survey/custom/AddDataSourceQuestion";
import { SurveyBooleanQuestionProps } from "../components/get-compliant/survey/SurveyBooleanQuestion";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { CustomVendorFormValues } from "../pages/get-compliant/data-recipients/AddDataRecipients";
import {
  SOURCE_KEY_CONSUMER_DIRECTLY,
  SOURCE_KEY_OTHER_INDIVIDUALS,
  VENDOR_CATEGORY_APPLICANT_TRACKING,
  VENDOR_CATEGORY_BACKGROUND_CHECK_PROVIDER,
  VENDOR_CATEGORY_DATA_BROKER,
  VENDOR_CATEGORY_RECRUITMENT_SERVICE,
} from "../types/Vendor";
import { Survey } from "./survey";

const workerDirectlyQuestion = (
  groupsLabel: string,
  sources: OrganizationDataSourceDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "worker-directly",
  question: `Does your business collect personal data — such as contact information, resumes, I-9 forms, or bank account information — directly from ${groupsLabel}?`,
  recommendation: (
    <>
      We recommend answering <strong>yes</strong>. It is <em>very uncommon</em> that a business does
      not collect data from their {groupsLabel} either directly or indirectly.
    </>
  ),
  noConfirmation: sources.some((s) => s.vendorKey == SOURCE_KEY_CONSUMER_DIRECTLY)
    ? "Changing your answer to “No” will remove the Worker source and any mappings that have been made to it. Are you sure?"
    : undefined,
});

const dataBrokersQuestion = (
  groupLabel: string,
  sources: OrganizationDataSourceDto[],
  recipients: OrganizationDataRecipientDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "data-brokers",
  question: `Does your business collect personal information about ${groupLabel} from data brokers? Data brokers are third parties that aggregate personal data about people they do not have a direct relationship with, and sell it to businesses.`,
  helpText: (
    <>
      Read more about{" "}
      <a
        href="https://www.truevault.com/learn/essentials/what-is-a-data-broker"
        target="_blank"
        rel="noreferrer"
      >
        data brokers
      </a>
      .
    </>
  ),
  recommendation: recipients.some((r) => r.category == VENDOR_CATEGORY_DATA_BROKER) ? (
    <>
      We recommend answering <strong>yes</strong>, because you indicated that you have data brokers
      as data recipients.
    </>
  ) : undefined,
  noDisabled: sources.some((s) => s.category == VENDOR_CATEGORY_DATA_BROKER),
  noDisabledTooltip: "To change your answer to “no” remove the data brokers below",
});

const addDataBrokersQuestion = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  handlers: SourceSurveyHandlers,
): AddDataSourceQuestionProps => ({
  type: "custom-add-data-source",
  slug: "add-data-brokers",
  question: "Add the Data Brokers that your business collects personal information from.",
  helpText: (
    <>
      If you don’t receive personal information from any Data Brokers you can change your answer to
      “no” above.
    </>
  ),
  category: VENDOR_CATEGORY_DATA_BROKER,
  collectionContext: "EMPLOYEE",
  allVendors,
  sources,
  ...handlers,
});

const applicantTrackingQuestion = (
  groupLabel: string,
  sources: OrganizationDataSourceDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "applicant-tracking-systems",
  question: `Do you acquire data about ${groupLabel} from employment agencies, recruiters, or other applicant sourcing entities?`,
  noDisabled: sources.some((s) => s.category == VENDOR_CATEGORY_APPLICANT_TRACKING),
  noDisabledTooltip: "To change your answer to “no” remove the applicant sourcing entities below",
});

const addApplicantTrackingQuestion = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  handlers: SourceSurveyHandlers,
): AddDataSourceQuestionProps => ({
  type: "custom-add-data-source",
  slug: "add-applicant-tracking-systems",
  question:
    "Add the Applicant Sourcing Entities that your business collects personal information from.",
  helpText: (
    <>
      If you don’t receive personal information from any Applicant Sourcing Entities you can change
      your answer to “no” above.
    </>
  ),
  category: [VENDOR_CATEGORY_APPLICANT_TRACKING, VENDOR_CATEGORY_RECRUITMENT_SERVICE],
  collectionContext: "EMPLOYEE",
  allVendors,
  sources,
  ...handlers,
});

const backgroundCheckQuestion = (
  groupsLabel: string,
  sources: OrganizationDataSourceDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "background-check-providers",
  question: `Does your business collect personal data about ${groupsLabel} from background check providers?`,
  noDisabled: sources.some((s) => s.category == VENDOR_CATEGORY_BACKGROUND_CHECK_PROVIDER),
  noDisabledTooltip: "To change your answer to “no” remove the background check providers below",
});

const addBackgroundCheckQuestion = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  handlers: SourceSurveyHandlers,
): AddDataSourceQuestionProps => ({
  type: "custom-add-data-source",
  slug: "add-background-check-providers",
  question:
    "Add the Background Check Providers that your business collects personal information from.",
  category: VENDOR_CATEGORY_BACKGROUND_CHECK_PROVIDER,
  collectionContext: "EMPLOYEE",
  allVendors,
  sources,
  ...handlers,
});

export const EMPLOYEE_OTHER_EXCLUDED_CATEGORIES = [
  VENDOR_CATEGORY_DATA_BROKER,
  VENDOR_CATEGORY_APPLICANT_TRACKING,
  VENDOR_CATEGORY_RECRUITMENT_SERVICE,
  VENDOR_CATEGORY_BACKGROUND_CHECK_PROVIDER,
];
export const EMPLOYEE_EXCLUDED_KEYS = [SOURCE_KEY_CONSUMER_DIRECTLY, SOURCE_KEY_OTHER_INDIVIDUALS];
const otherSourcesQuestion = (
  groupsLabel: string,
  sources: OrganizationDataSourceDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "other-sources",
  question: `Would you like to add any additional sources?`,
  recommendation: (
    <>
      We recommend answering <strong>no</strong>, unless you know of specific other sources of
      personal information.
    </>
  ),
  noDisabled: sources.some(
    (s) =>
      !EMPLOYEE_OTHER_EXCLUDED_CATEGORIES.includes(s.category) &&
      !EMPLOYEE_EXCLUDED_KEYS.includes(s.vendorKey),
  ),
  noDisabledTooltip: "To change your answer to “no” remove the other sources below",
});
const addOtherSourcesQuestion = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  handlers: SourceSurveyHandlers,
): AddDataSourceQuestionProps => ({
  type: "custom-add-data-source",
  slug: "add-other-sources",
  question: "Add any other sources that your business receives personal information from.",
  collectionContext: "EMPLOYEE",
  allVendors,
  sources,
  excludeCategories: EMPLOYEE_OTHER_EXCLUDED_CATEGORIES,
  excludeVendorKeys: EMPLOYEE_EXCLUDED_KEYS,
  ...handlers,
});

const sourcesQuestions = (
  groupsLabel: string,
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  recipients: OrganizationDataRecipientDto[],
  handlers: SourceSurveyHandlers,
): SurveyQuestionProps[] => {
  return [
    workerDirectlyQuestion(groupsLabel, sources), // "worker-directly"

    dataBrokersQuestion(groupsLabel, sources, recipients), // "data-brokers"
    addDataBrokersQuestion(allVendors, sources, handlers), // "add-data-brokers"

    applicantTrackingQuestion(groupsLabel, sources), // "applicant-tracking-systems"
    addApplicantTrackingQuestion(allVendors, sources, handlers), // "add-applicant-tracking-systems"

    backgroundCheckQuestion(groupsLabel, sources), // "background-check-providers"
    addBackgroundCheckQuestion(allVendors, sources, handlers), // "add-background-check-providers"

    otherSourcesQuestion(groupsLabel, sources), // "other-sources"
    addOtherSourcesQuestion(allVendors, sources, handlers), // "add-other-sources"
  ];
};

type SourceSurveyHandlers = {
  handleAddVendor: (v: UUIDString) => void;
  handleRemoveVendor: (v: UUIDString) => void;
  handleAddCustomVendor: (values: CustomVendorFormValues) => void;
};

export const employeeSourcesSurvey = (
  answers: Record<string, string>,
  groupsLabel: string,
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  recipients: OrganizationDataRecipientDto[],
  handlers: SourceSurveyHandlers,
): Survey => {
  return {
    questions: sourcesQuestions(groupsLabel, allVendors, sources, recipients, handlers),
    visibility: {
      "add-data-brokers": answers["data-brokers"] === "true",
      "add-applicant-tracking-systems": answers["applicant-tracking-systems"] === "true",
      "add-background-check-providers": answers["background-check-providers"] === "true",
      "add-other-sources": answers["other-sources"] === "true",
    },
    disabled: {},
  };
};
