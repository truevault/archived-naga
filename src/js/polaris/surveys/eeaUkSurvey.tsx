import React from "react";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { Survey } from "./survey";

export const EEA_UK_SURVEY_NAME = "eea-uk-survey";

export const largeScaleQuestion = (): SurveyQuestionProps => {
  const slug = "personal-data-large-scale";

  return {
    type: "boolean",
    slug: slug,
    question: "Does your business process personal data of EEA/UK residents on a “large scale”?",
    helpText: (
      <>
        <p>Large scale processing is not common. Here are some examples where it would apply:</p>
        <ul>
          <li>
            Your business processes unaggregated personal data collected from monitoring a publicly
            accessible area, such as a park, airport or train station, on a large scale
          </li>
          <li>
            Your business routinely processes real-time geolocation data of customers across
            multiple EU countries for statistical purposes
          </li>
          <li>
            Your business is an insurance company, bank, search engine or internet service provider
          </li>
          <li>
            Your business processes the personal data of a significant portion of the population of
            the UK or any EU country (‘processing’ includes tracking online behavior)
          </li>
        </ul>
        <p>
          Click{" "}
          <a
            href="https://help.truevault.com/article/163-large-scale-processing"
            target="_blank"
            rel="norefferer noopener noreferrer"
          >
            here
          </a>{" "}
          to read more about “large scale” data processing.
        </p>
      </>
    ),
  };
};

export const establishedQuestion = (): SurveyQuestionProps => {
  const slug = "established-in-eea-uk";

  return {
    type: "boolean",
    slug: slug,
    question: "Is your business “established” in the EEA/UK?",
    helpText:
      "Answer “Yes” if you have a physical presence there where your business interacts with EU/UK residents, such as a retail store. Otherwise, answer “No.”",
  };
};

export const nonEnglishQuestion = (): SurveyQuestionProps => {
  const slug = "non-english-website";

  return {
    type: "boolean",
    slug: slug,
    question:
      "Does your business offer non-English versions of its website for visitors from Europe?",
    helpText:
      "Answer “Yes” if your website detects a visitor’s location and displays content in the visitor’s local European language, or if a visitor can select a different country or region on your site, which translates the content to the language used in that country or region.",
  };
};

const eeaVisibility = (_answers: Record<string, string>): Record<string, boolean> => {
  return {
    "personal-data-large-scale": true,
    "employees-eea-uk": true,
    "established-in-eea-uk": true,
    "non-english-website": true,
  };
};

const eeaQuestions = (_answers: Record<string, string>): SurveyQuestionProps[] => {
  return [
    largeScaleQuestion(), // personal-data-large-scale
    establishedQuestion(), // established-in-eea-uk
    nonEnglishQuestion(), // non-english-website
  ];
};

export const eeaUkSurvey = (answers: Record<string, string>): Survey => {
  return {
    questions: eeaQuestions(answers),
    visibility: eeaVisibility(answers),
    disabled: {},
  };
};
