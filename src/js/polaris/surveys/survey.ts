import _ from "lodash";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { safeJsonParse } from "./util";

export type Survey = {
  questions: SurveyQuestionProps[];
  visibility: Record<string, boolean>;
  disabled: Record<string, boolean | string>;
};

type QuestionWithVisibility = SurveyQuestionProps & {
  visible: boolean;
  notPresent: boolean;
  isDisabled?: boolean;
  disabledReason?: string;
};

export const hasAnsweredQuestion = (answers: Record<string, unknown>, slug: string) =>
  Boolean(answers[slug]);
export const parseBoolAnswer = (ans: string | null): boolean | null => safeJsonParse(ans) === true;
export const parseListAnswer = <T extends unknown = string>(ans: string | null): T[] =>
  safeJsonParse(ans);

const hasAnswered = (
  answers: Record<string, unknown>,
  question: SurveyQuestionProps,
  debug: boolean,
) => {
  const ans = answers[question.slug] as string | undefined | null;
  const parsedAnswer = safeJsonParse(ans);
  let answered: boolean = false;

  switch (question.type) {
    case "radio-freetext":
      answered = Boolean(ans);
      break;
    case "checkboxes":
      answered = !_.isNil(parsedAnswer) && _.isArray(parsedAnswer) && parsedAnswer.length > 0;
      break;
    case "boolean":
      answered = !_.isNil(ans) && ["true", "false"].includes(ans);
      break;
    default:
      answered = !_.isNil(ans);
      break;
  }

  if (debug) {
    // eslint-disable-next-line no-console
    console.log("HAS ANSWERED: ", question.slug, answers[question.slug], answered);
  }

  return answered;
};

export const prepareSurvey = (
  survey: Survey,
  answers: Record<string, unknown>,
  showAll = false,
  debug = false,
): SurveyQuestionProps[] => {
  const { questions } = survey;
  const visibilityWithDefault = questionsWithVisibility(survey);

  const highestAnsweredIdx = findLastIndex(questions, (q) => hasAnswered(answers, q, debug));

  // find the "next" visible question
  let nextQuestionIdx = visibilityWithDefault.findIndex((q, idx) => {
    return idx > highestAnsweredIdx && q.visible == true;
  });

  if (nextQuestionIdx == -1) {
    nextQuestionIdx = questions.length;
  }

  if (debug) {
    console.log("PREPARE SURVEY INPUT", survey, answers, showAll); // eslint-disable-line no-console
    console.log("NEXT QUESTION IDX: ", nextQuestionIdx); // eslint-disable-line no-console
    console.log("DEFAULT VISIBILITY: ", visibilityWithDefault); // eslint-disable-line no-console
  }

  if (!showAll) {
    // only show the next question
    visibilityWithDefault.forEach((q, idx) => {
      if (idx > nextQuestionIdx) {
        q.visible = false;
      }
    });
  }

  if (debug) {
    console.log("FINAL VISIBILITY: ", visibilityWithDefault); // eslint-disable-line no-console
  }

  return visibilityWithDefault.filter((x) => !x.notPresent);
};

export const isFinished = (
  survey: Survey,
  answers: Record<string, unknown>,
  questionsToExclude?: string[],
): boolean => {
  const questions = questionsWithVisibility(survey);
  const nonExcludedQuestions = questionsToExclude
    ? questions.filter((q) => !questionsToExclude.some((slug) => slug === q.slug))
    : questions;
  const visibleQuestions = nonExcludedQuestions.filter((q) => !q.notPresent && q.visible);

  return visibleQuestions.every((q) => {
    const answer = answers[q.slug];
    return (
      answer !== undefined && answer !== null && answer !== "" && !emptyListAnswer(answer as string)
    );
  });
};

/**
 * Returns the index of the last element in the array where predicate is true, and -1
 * otherwise.
 * @param array The source array to search in
 * @param predicate find calls predicate once for each element of the array, in descending
 * order, until it finds one where predicate returns true. If such an element is found,
 * findLastIndex immediately returns that element index. Otherwise, findLastIndex returns -1.
 */
const findLastIndex = <T>(
  array: Array<T>,
  predicate: (value: T, index: number, obj: T[]) => boolean,
): number => {
  let l = array.length;
  while (l--) {
    if (predicate(array[l], l, array)) {
      return l;
    }
  }
  return -1;
};

const questionsWithVisibility = (survey: Survey): QuestionWithVisibility[] => {
  const { questions, visibility, disabled } = survey;

  return questions.map((q) => {
    const dis = disabled && disabled[q.slug];
    const visible = visibility[q.slug] === undefined ? true : visibility[q.slug];
    const isDisabled = Boolean(dis);
    const disabledReason = typeof dis === "string" ? (dis as string) : undefined;

    return {
      ...q,
      visible,
      notPresent: !visible,
      isDisabled,
      disabledReason,
    };
  });
};

const emptyListAnswer = (val?: string): boolean => {
  try {
    return JSON.parse(val).length === 0;
  } catch (err) {
    // If it doesn't parse as a list, it may be a normal string answer so don't mess with it
    return false;
  }
};
