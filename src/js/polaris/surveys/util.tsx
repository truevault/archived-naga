import _ from "lodash";
import { OrganizationDataSource } from "../../common/models";
import { OrganizationDataRecipientDto } from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../common/service/server/dto/VendorDto";
import { SurveyCheckboxOption } from "../components/get-compliant/survey/SurveyCheckboxesQuestion";

type SortableByName = OrganizationDataRecipientDto | VendorDto | OrganizationDataSource;

type ExtraOptions = Partial<Omit<SurveyCheckboxOption, "key" | "label">>;
export const vendorToOption = (
  v: OrganizationDataRecipientDto,
  opts: ExtraOptions = {},
): SurveyCheckboxOption => ({
  ...opts,
  key: v.id,
  label: v.name,
});

export const alphabetically = (a: SortableByName, b: SortableByName) =>
  a.name.localeCompare(b.name);

export const dataSourceSort = (a: SortableByName, b: SortableByName) => {
  if (a.vendorKey === "source-consumer-directly") {
    return -1;
  } else if (b.vendorKey === "source-consumer-directly") {
    return 1;
  } else if (
    a.vendorKey === "source-other-individuals" &&
    b.vendorKey !== "source-consumer-directly"
  ) {
    return -1;
  }

  return alphabetically(a, b);
};

export const dataSourceSortInverted = (a: SortableByName, b: SortableByName) => {
  if (a.vendorKey === "source-consumer-directly") {
    return 1;
  } else if (b.vendorKey === "source-consumer-directly") {
    return -1;
  } else if (
    a.vendorKey === "source-other-individuals" &&
    b.vendorKey !== "source-consumer-directly"
  ) {
    return 1;
  }

  return alphabetically(a, b);
};

export const noneOption = (): SurveyCheckboxOption => ({
  key: "none",
  label: "None",
});

export const toggleSelection = <TCollection extends unknown>(
  collection: TCollection[],
  item: TCollection,
  selected?: boolean,
): TCollection[] => {
  const index = _.indexOf(collection, item);
  if (index !== -1) {
    if (selected === true) {
      // it's already in the array
      return collection;
    }

    // Remove it from the array
    return _.without(collection, item);
  } else {
    if (selected === false) {
      // It's already not in the array
      return collection;
    }

    // Add it to the array
    return [...collection, item];
  }
};

// Taken from https://stackoverflow.com/a/52171480
export const cyrb53 = function (str: string, seed: number = 0): string {
  let h1 = 0xdeadbeef ^ seed,
    h2 = 0x41c6ce57 ^ seed;

  for (let i = 0, ch; i < str.length; i++) {
    ch = str.charCodeAt(i);
    h1 = Math.imul(h1 ^ ch, 2654435761);
    h2 = Math.imul(h2 ^ ch, 1597334677);
  }

  h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^ Math.imul(h2 ^ (h2 >>> 13), 3266489909);
  h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^ Math.imul(h1 ^ (h1 >>> 13), 3266489909);

  return `${4294967296 * (2097151 & h2) + (h1 >>> 0)}`;
};

export const safeJsonParse = (val) => {
  if (typeof val !== "string") {
    return null;
  }

  try {
    return JSON.parse(val);
  } catch (err) {
    return null;
  }
};
