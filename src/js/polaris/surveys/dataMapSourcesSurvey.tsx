import React from "react";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { VendorDto } from "../../common/service/server/dto/VendorDto";
import { UUIDString } from "../../common/service/server/Types";
import { AddDataSourceQuestionProps } from "../components/get-compliant/survey/custom/AddDataSourceQuestion";
import { SurveyBooleanQuestionProps } from "../components/get-compliant/survey/SurveyBooleanQuestion";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { CustomVendorFormValues } from "../pages/get-compliant/data-recipients/AddDataRecipients";
import {
  SOURCE_KEY_CONSUMER_DIRECTLY,
  SOURCE_KEY_OTHER_INDIVIDUALS,
  VENDOR_CATEGORY_AD_NETWORK,
  VENDOR_CATEGORY_DATA_ANALYTICS_PROVIDER,
  VENDOR_CATEGORY_DATA_BROKER,
  VENDOR_CATEGORY_RETAIL_PARTNER,
} from "../types/Vendor";
import { Survey } from "./survey";

const consumerDirectlyQuestion = (
  sources: OrganizationDataSourceDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "consumer-directly",
  question:
    "We’re pretty sure you collect at least some data from the consumer, but we have to confirm. Is the Consumer a source of personal information for your business?",
  recommendation: (
    <>
      We recommend answering <strong>yes</strong>. It is <em>very uncommon</em> that a business does
      not collect data from their consumers either directly or indirectly.
    </>
  ),
  noConfirmation: sources.some((s) => s.vendorKey == SOURCE_KEY_CONSUMER_DIRECTLY)
    ? "Changing your answer to “No” will remove the Consumer source and any mappings that have been made to it. Are you sure?"
    : undefined,
});

const retailPartnersQuestion = (
  sources: OrganizationDataSourceDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "retail-partners",
  question:
    "Does your business sell to consumers through a retail partner such as Wal-Mart or Amazon Marketplace?",
  helpText: (
    <>
      Select <strong>yes</strong> if your business offers products through a third-party selling
      platform and receives personal information (order information, postal address, etc.) about
      customers via that platform.
    </>
  ),
  noDisabled: sources.some((s) => s.category == VENDOR_CATEGORY_RETAIL_PARTNER),
  noDisabledTooltip: "To change your answer to “no” remove the retail partners below",
});

const addRetailPartnersQuestion = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  handlers: SourceSurveyHandlers,
): AddDataSourceQuestionProps => ({
  type: "custom-add-data-source",
  slug: "add-retail-partners",
  question:
    "You answered that your business sold to consumers through a retail partner above. Now you’ll need to add the retail partners that your business receives personal information from.",
  helpText: (
    <>
      If you don’t receive personal information from any Retail Partners you can change your answer
      to “no” above.
    </>
  ),
  category: VENDOR_CATEGORY_RETAIL_PARTNER,
  allVendors,
  sources,
  ...handlers,
});

const dataBrokersQuestion = (
  sources: OrganizationDataSourceDto[],
  recipients: OrganizationDataRecipientDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "data-brokers",
  question:
    "Does your business collect personal information from data brokers? Data brokers are third parties that aggregate personal data about people they do not have a direct relationship with, and sell it to businesses. ZoomInfo is an example of a data broker.",
  helpText: (
    <>
      Read more about{" "}
      <a
        href="https://www.truevault.com/learn/essentials/what-is-a-data-broker"
        target="_blank"
        rel="noreferrer"
      >
        data brokers
      </a>
      .
    </>
  ),
  recommendation: recipients.some((r) => r.category == VENDOR_CATEGORY_DATA_BROKER) ? (
    <>
      We recommend answering <strong>yes</strong>, because you indicated that you have data brokers
      as data recipients.
    </>
  ) : undefined,
  noDisabled: sources.some(
    (s) => s.category == VENDOR_CATEGORY_DATA_BROKER && s.name !== "Data Brokers",
  ),
  noDisabledTooltip: "To change your answer to “no” remove the data brokers below",
});

const addDataBrokersQuestion = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  handlers: SourceSurveyHandlers,
): AddDataSourceQuestionProps => ({
  type: "custom-add-data-source",
  slug: "add-data-brokers",
  question: "Add the Data Brokers that your business collects personal information from.",
  helpText: (
    <>
      If you don’t receive personal information from any Data Brokers you can change your answer to
      “no” above.
    </>
  ),
  category: VENDOR_CATEGORY_DATA_BROKER,
  allVendors,
  sources,
  ...handlers,
});

const adNetworksQuestion = (sources: OrganizationDataSourceDto[]): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "ad-networks",
  question: "Does your business collect personal information from ad networks, like Facebook Ads?",
  helpText: (
    <>
      Answer <strong>yes</strong> if you collect data from ad networks that can be linked to
      individual consumers. For example, if you run any Leads Campaigns on Facebook Ads or Lead
      Generation Conversion campaigns on LinkedIn Ads.
    </>
  ),
  noDisabled: sources.some(
    (s) => s.category == VENDOR_CATEGORY_AD_NETWORK && s.name !== "Ad Networks",
  ),
  noDisabledTooltip: "To change your answer to “no” remove the ad networks below",
});

const addAdNetworksQuestion = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  handlers: SourceSurveyHandlers,
): AddDataSourceQuestionProps => ({
  type: "custom-add-data-source",
  slug: "add-ad-networks",
  question: "Add the Ad Networks that your business collects personal information from.",
  helpText: (
    <>
      If you don’t receive personal information from any Ad Networks you can change your answer to
      “no” above.
    </>
  ),
  category: VENDOR_CATEGORY_AD_NETWORK,

  allVendors,
  sources,
  ...handlers,
});

const analyticsProviderQuestion = (
  sources: OrganizationDataSourceDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "data-analytics-privders",
  question:
    "Does your business collect personal information from data analytics providers, like Google Analytics?",
  helpText: (
    <>
      Answer <strong>yes</strong> if you collect data from analytics providers that can be linked to
      individual consumers.
    </>
  ),
  noDisabled: sources.some(
    (s) =>
      s.category == VENDOR_CATEGORY_DATA_ANALYTICS_PROVIDER &&
      s.name !== "Data Analytics Providers",
  ),
  noDisabledTooltip: "To change your answer to “no” remove the data analytics providers below",
});

const addAnalyticsProviderQuestion = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  handlers: SourceSurveyHandlers,
): AddDataSourceQuestionProps => ({
  type: "custom-add-data-source",
  slug: "add-data-analytics-privders",
  question:
    "Add the Data Analytics Providers that your business collects personal information from.",
  helpText: (
    <>
      If you don’t receive personal information from any Data Analytics Providers you can change
      your answer to “no” above.
    </>
  ),
  category: VENDOR_CATEGORY_DATA_ANALYTICS_PROVIDER,
  allVendors,
  sources,
  ...handlers,
});

const otherConsumersQuestion = (
  sources: OrganizationDataSourceDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "other-consumers",
  question:
    "Does your business collect personal information about consumers from other consumers, such as via a referral program?",
  noConfirmation: sources.some((s) => s.vendorKey == SOURCE_KEY_OTHER_INDIVIDUALS)
    ? "Changing your answer to “No” will remove the Other Consumers source and any mappings that have been made to it. Are you sure?"
    : undefined,
});

export const OTHER_EXCLUDED_CATEGORIES = [
  VENDOR_CATEGORY_AD_NETWORK,
  VENDOR_CATEGORY_DATA_BROKER,
  VENDOR_CATEGORY_RETAIL_PARTNER,
  VENDOR_CATEGORY_DATA_ANALYTICS_PROVIDER,
];
export const OTHER_EXCLUDED_KEYS = [SOURCE_KEY_CONSUMER_DIRECTLY, SOURCE_KEY_OTHER_INDIVIDUALS];
const otherSourcesQuestion = (
  sources: OrganizationDataSourceDto[],
): SurveyBooleanQuestionProps => ({
  type: "boolean",
  slug: "other-sources",
  question: "Does your business collect personal information from any other sources?",
  recommendation: (
    <>
      We recommend answering <strong>no</strong>, unless you know of specific other sources of
      personal information.
    </>
  ),
  noDisabled: sources.some(
    (s) =>
      !OTHER_EXCLUDED_CATEGORIES.includes(s.category) && !OTHER_EXCLUDED_KEYS.includes(s.vendorKey),
  ),
  noDisabledTooltip: "To change your answer to “no” remove the other sources below",
});
const addOtherSourcesQuestion = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  handlers: SourceSurveyHandlers,
): AddDataSourceQuestionProps => ({
  type: "custom-add-data-source",
  slug: "add-other-sources",
  question: "Add any other sources that your business receives personal information from.",
  allVendors,
  sources,
  excludeCategories: OTHER_EXCLUDED_CATEGORIES,
  excludeVendorKeys: OTHER_EXCLUDED_KEYS,
  ...handlers,
});

const sourcesQuestions = (
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  recipients: OrganizationDataRecipientDto[],
  handlers: SourceSurveyHandlers,
): SurveyQuestionProps[] => {
  return [
    consumerDirectlyQuestion(sources), // "consumer-directly"

    retailPartnersQuestion(sources), // "retail-partners"
    addRetailPartnersQuestion(allVendors, sources, handlers), // "add-retail-partners"

    dataBrokersQuestion(sources, recipients), // "data-brokers"
    addDataBrokersQuestion(allVendors, sources, handlers), // "add-data-brokers"

    adNetworksQuestion(sources), // "ad-networks"
    addAdNetworksQuestion(allVendors, sources, handlers), // "add-ad-networks"

    analyticsProviderQuestion(sources), // "data-analytics-privders"
    addAnalyticsProviderQuestion(allVendors, sources, handlers), // "add-data-analytics-privders"

    otherConsumersQuestion(sources), // "other-consumers"

    otherSourcesQuestion(sources), // "other-sources"
    addOtherSourcesQuestion(allVendors, sources, handlers), // "add-other-sources"
  ];
};

type SourceSurveyHandlers = {
  handleAddVendor: (v: UUIDString) => void;
  handleRemoveVendor: (v: UUIDString) => void;
  handleAddCustomVendor: (values: CustomVendorFormValues) => void;
};

export const dataMapSourcesSurvey = (
  answers: Record<string, string>,
  allVendors: VendorDto[],
  sources: OrganizationDataSourceDto[],
  recipients: OrganizationDataRecipientDto[],
  handlers: SourceSurveyHandlers,
  // vendors: OrganizationDataRecipientDto[],
): Survey => {
  return {
    questions: sourcesQuestions(allVendors, sources, recipients, handlers),
    visibility: {
      "add-retail-partners": answers["retail-partners"] === "true",
      "add-data-brokers": answers["data-brokers"] === "true",
      "add-ad-networks": answers["ad-networks"] === "true",
      "add-data-analytics-privders": answers["data-analytics-privders"] === "true",
      "add-other-sources": answers["other-sources"] === "true",
    },
    disabled: {},
  };
};
