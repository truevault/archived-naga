import React from "react";
import { Check } from "@mui/icons-material";
import { Survey } from "./survey";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { SurveyAcknowledgementQuestionProps } from "../components/get-compliant/survey/SurveyAcknowledgementQuestion";
import { CustomFinancialIncentivesQuestionProps } from "../components/get-compliant/survey/custom/CustomFinancialIncentivesSurveyQuestion";
import { SurveyAnswers } from "../../common/hooks/useSurvey";

export const CONSUMER_INCENTIVES_SURVEY_NAME = "consumer-collection-incentives-survey";

export const OFFERS_CONSUMER_INCENTIVES_SLUG = "offer-consumer-incentives";
export const FINANCIAL_INCENTIVE_ACK_SLUG = "ack-financial-incentives";

export const collectedDataIncentivesExistsQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: OFFERS_CONSUMER_INCENTIVES_SLUG,
  question:
    "Does your business offer financial incentives to consumers for sharing their personal information?",
  helpText: (
    <>
      <p>
        Answer “yes” if your business offers a loyalty or reward program, premium features, or a
        discount in exchange for a consumer providing their personal information. For example, 10%
        off a purchase with newsletter signup.
      </p>
    </>
  ),
  visible: true,
});

export const ackFinancialIncentivesQuestion = (
  isOnboarding: boolean,
): SurveyAcknowledgementQuestionProps => ({
  type: "acknowledgement",
  text: (
    <>
      <p>
        {isOnboarding ? "Got it! In that case, your" : "Your"} privacy policy must include a
        description of key terms of your incentive program so that consumers know what they're
        signing up for. Specifically, you need to provide a:
      </p>

      <CheckItem
        label="Summary of the incentive, including the terms"
        example="Ex: “Consumers who sign up for our marketing emails/SMS texts receive a 10% discount on their first purchase.”"
      />
      <CheckItem
        label="Description of the opt-in process"
        example="Ex: “To opt in, a consumer must enter their email address/phone number into the form and consent to receive emails in exchange for a discount provided via coupon code.”"
      />
      <CheckItem
        label="Statement on how to withdraw from the program"
        example="Ex: “A consumer may unsubscribe from our marketing emails by using the unsubscribe link in the email footer/replying STOP via text at any time.”"
      />
      <CheckItem
        label="Description of the value of the consumer's data"
        example="Ex: “We calculate the value of the offer and financial incentive by using the expense related to the offer.”"
      />

      <p>
        We’ll start you off with some default language to meet these requirements. You can edit it
        to match your specific incentive program.
      </p>
    </>
  ),
  buttonLabel: "Get Started",
  slug: FINANCIAL_INCENTIVE_ACK_SLUG,
  question: "",
});

const CheckItem = ({ label, example }) => {
  return (
    <div className="d-flex flex-row">
      <Check color="primary" className="ml-xl mr-md" />
      <div>
        <p className="mt-0 mb-xs">{label}</p>
        <p className="text-component-help">{example}</p>
      </div>
    </div>
  );
};

export const editFinancialIncentivesQuestion = (): CustomFinancialIncentivesQuestionProps => {
  return {
    type: "custom-edit-financial-incentives",
    question: "",
    slug: "edit-financial-incentives",
  };
};

const collectedQuestions = (isOnboarding: boolean): SurveyQuestionProps[] => {
  return [
    collectedDataIncentivesExistsQuestion(), // OFFERS_CONSUMER_INCENTIVES_SLUG
    ackFinancialIncentivesQuestion(isOnboarding), // FINANCIAL_INCENTIVE_ACK_SLUG
    editFinancialIncentivesQuestion(), // <custom api interaction, no slug>
  ];
};

const collectedVisibility = (answers: SurveyAnswers): Record<string, boolean> => {
  return {
    [OFFERS_CONSUMER_INCENTIVES_SLUG]: true,
    [FINANCIAL_INCENTIVE_ACK_SLUG]: answers[OFFERS_CONSUMER_INCENTIVES_SLUG] == "true",
    "edit-financial-incentives": answers[OFFERS_CONSUMER_INCENTIVES_SLUG] == "true",
  };
};

export const consumerCollectionIncentivesSurvey = (
  answers: SurveyAnswers,
  isOnboarding: boolean,
): Survey => {
  return {
    questions: collectedQuestions(isOnboarding),
    visibility: collectedVisibility(answers),
    disabled: {},
  };
};
