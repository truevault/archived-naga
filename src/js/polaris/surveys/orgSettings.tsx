import React from "react";
import { Organization } from "../../common/models";
import { BUSINESS_SURVEY_NAME } from "./businessSurvey";
import { CONSUMER_INCENTIVES_SURVEY_NAME } from "./consumerCollectionIncentivesSurvey";
import { EEA_UK_SURVEY_NAME } from "./eeaUkSurvey";
import { ParsedSurveyAnswers } from "../../common/hooks/useSurvey";

export type SURVEY_NAME =
  | typeof BUSINESS_SURVEY_NAME
  | typeof CONSUMER_INCENTIVES_SURVEY_NAME
  | typeof EEA_UK_SURVEY_NAME;

export interface OrgSettingQuestion {
  slug: string;
  source: SURVEY_NAME;
  label: string;
  question: string;
  help?: React.ReactNode;
  visibility?: (answers: ParsedSurveyAnswers) => boolean;
}

const ALL_SETTINGS_QUESTIONS: OrgSettingQuestion[] = [
  {
    slug: "business-service-provider",
    source: BUSINESS_SURVEY_NAME,
    label: "Is a service provider",
    question: 'Is your business a "service provider" under the CCPA?',
    help: (
      <>
        <p className="text-t1 mt-0">
          A service provider is a business that uses the personal information that another
          organization shares with it only for purposes of providing a service to the other
          organization.
        </p>
        <p className="text-t1">
          If your business sells such information, uses it to build profiles about consumers to
          provide or enhance services to others, or uses such information for any purposes other
          than providing a service to another organization, answer "No." Read about service
          providers.
        </p>
      </>
    ),
  },
  {
    slug: "business-physical-location",
    source: BUSINESS_SURVEY_NAME,
    label: "Has a physical presence in California",
    question:
      "Does your business have a physical presence in California where it collects information from consumers, like a storefront or office?",
    help: "If you have a physical presence in California but you only collect personal data from employees (e.g., an office building) and do not collect data directly from your B2C (or B2B if applicable) customers at that physical location, answer “No.”",
  },
  {
    slug: "business-products-aimed-at-kids",
    source: BUSINESS_SURVEY_NAME,
    label: "Offers products/services aimed at kids under 16",
    question:
      "Does your business intentionally collect the personal data of children under the age of 16? ",
    help: "Answer “Yes” if your products, services, and/or website are aimed at kids and you do not discourage children under the age of 16 from submitting personal data to your business (for example, a website pop-up or privacy policy statement saying that children should not submit their data on your site).",
  },
  {
    slug: "business-products-aimed-at-kids-under-13",
    source: BUSINESS_SURVEY_NAME,
    label: "Offers products/services aimed at kids under 13",
    question:
      "Does your business intentionally collect personal data of children under the age of 13?",
    help: "Answer “Yes” if your business knowingly collects data from consumers under 13 years old.",
    visibility: (answers) => answers["business-products-aimed-at-kids"],
  },
  {
    slug: "business-hipaa-compliant",
    source: BUSINESS_SURVEY_NAME,
    label: "Collects data regulated by HIPAA",
    question:
      'HIPAA: Is your business a "covered entity" or "business associate" that handles protected health information (PHI)?',
    help: (
      <p className="text-t1 mt-0">
        E.g., a healthcare provider, health plan, healthcare clearinghouse, or a business associate
        that processes PHI on behalf of another entity.
      </p>
    ),
  },
  {
    slug: "business-glba-compliant",
    source: BUSINESS_SURVEY_NAME,
    label: "Collects data regulated by GLBA",
    question:
      "GLBA: Is your business a financial institution that provides a personal financial service or product? ",
    help: (
      <p className="text-t1 mt-0">
        Eg., banks, securities brokers, insurance agents, finance companies, and even auto dealers
        if they extend credit, arrange financing, or give financial advice.
      </p>
    ),
  },
  {
    slug: "business-fcra-compliant",
    source: BUSINESS_SURVEY_NAME,
    label: "Collects data regulated by FCRA",
    question:
      "FCRA: Is your business a financial institution that provides a personal financial service or product?",
    help: (
      <p className="text-t1 mt-0">
        E.g., a property rental company provides data about it evictions to a credit bureau; a car
        dealership pulls a credit reprot for a potential buyer.
      </p>
    ),
  },
  {
    slug: "offer-consumer-incentives",
    source: CONSUMER_INCENTIVES_SURVEY_NAME,
    label: "Offers financial incentives for collecting personal data",
    question:
      "Does your business offer financial incentives to consumers for sharing their personal information?",
    help: (
      <p className="text-t1 mt-0">
        Answer "yes" if your business offers a loyalty or reward program, premium features, or a
        discount in exchange for a consumer providing their personal information. For example, 10%
        off a purchase with newsletter signup.
      </p>
    ),
  },
  {
    slug: "business-has-mobile-apps",
    source: BUSINESS_SURVEY_NAME,
    label: "Has a mobile application",
    question: "Does your business have a mobile application?",
    help: (
      <p className="text-t1 mt-0">
        Answer “yes” if you have an app that consumers can download from the Apple App Store or
        Google Play store.
      </p>
    ),
  },
  {
    slug: "business-california-nonenglish",
    source: BUSINESS_SURVEY_NAME,
    label: "Communicates with consumers in California in other languages",
    question:
      "Does your business regularly communicate with consumers in California in any languages other than English?",
    help: (
      <p className="text-t1 mt-0">
        Answer “yes” if your business regularly sends marketing emails to California consumers in
        other languages, or if its contracts, terms or agreements are ordinarily provided in other
        languages in California.
      </p>
    ),
  },
  {
    slug: "personal-data-large-scale",
    source: EEA_UK_SURVEY_NAME,
    label: "Performs “large scale” data processing of EEA/UK residents",
    question: "Does your business process personal data of EEA/UK residents on a “large scale”?",
    help: (
      <>
        <p className="text-t1 mt-0">
          Large scale processing is not common. Here are some examples where it would apply:
        </p>
        <ul>
          <li>
            Your business processes unaggregated personal data collected from monitoring a publicly
            accessible area, such as a park, airport or train station, on a large scale
          </li>
          <li>
            Your business routinely processes real-time geolocation data of customers across
            multiple EU countries for statistical purposes
          </li>
          <li>
            Your business is an insurance company, bank, search engine or internet service provider
          </li>
          <li>
            Your business processes the personal data of a significant portion of the population of
            the UK or any EU country (‘processing’ includes tracking online behavior)
          </li>
        </ul>
        <p>
          Click{" "}
          <a
            href="https://help.truevault.com/article/163-large-scale-processing"
            target="_blank"
            rel="norefferer noopener noreferrer"
          >
            here
          </a>{" "}
          to read more about “large scale” data processing.
        </p>
      </>
    ),
  },
  {
    slug: "employees-eea-uk",
    source: EEA_UK_SURVEY_NAME,
    label: "Has employees in the EEA/UK",
    question:
      "Does your business have workers (e.g., employees or contractors) living in the EEA/UK?",
  },
  {
    slug: "established-in-eea-uk",
    source: EEA_UK_SURVEY_NAME,
    label: "Is “established” in the EEA/UK",
    question: "Is your business “established” in the EEA/UK?",
    help: (
      <p className="text-t1 mt-0">
        Answer “Yes” if you have a physical presence there where your business interacts with EU/UK
        residents, such as a retail store. Otherwise, answer “No.”
      </p>
    ),
  },
  {
    slug: "non-english-website",
    source: EEA_UK_SURVEY_NAME,
    label: "Offers non-English versions of its website for European visitors",
    question:
      "Does your business offer non-English versions of its website for visitors from Europe?",
    help: (
      <p className="text-t1 mt-0">
        Answer “Yes” if your website detects a visitor’s location and displays content in the
        visitor’s local European language, or if a visitor can select a different country or region
        on your site, which translates the content to the language used in that country or region.
      </p>
    ),
  },
];

export const organizationSettingsQuestions = (org: Organization) => {
  return ALL_SETTINGS_QUESTIONS.filter((q) => org.featureGdpr || q.source !== EEA_UK_SURVEY_NAME);
};
