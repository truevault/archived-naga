import React from "react";
import { ExternalLink } from "../../common/components/ExternalLink";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { Survey } from "./survey";

export const BUSINESS_SURVEY_NAME = "business-survey";

export const COLLECTION_FOR_OTHER_BUSINESSES_SLUG = "consumer-collection-for-other-businesses";

export const physicalLocationQuestion = (): SurveyQuestionProps => {
  const slug = "business-physical-location";

  return {
    type: "boolean",
    slug: slug,
    question:
      "Does your business have a physical presence in California where it interacts directly with consumers, like a storefront?",
    helpText:
      "If you have a physical presence in California but you only collect personal data from employees (e.g., an office building) and do not collect data directly from your B2C (or B2B if applicable) customers at that physical location, answer “No.”",
  };
};

export const businessKidsQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "business-products-aimed-at-kids",
  question:
    "Does your business intentionally collect the personal data of children under the age of 16?",
  helpText:
    "Answer “Yes” if your products, services, and/or website are aimed at kids and you do not discourage children under the age of 16 from submitting personal data to your business (for example, a website pop-up or privacy policy statement saying that children should not submit their data on your site).",
});

export const businessKidsUnder13Question = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "business-products-aimed-at-kids-under-13",
  question:
    "Does your business intentionally collect personal data of children under the age of 13?",
  helpText:
    "Answer “Yes” if your business knowingly collects data from consumers under 13 years old.",
});

export const businessMobileAppsQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "business-has-mobile-apps",
  question: "Does your business have a mobile application?",
  helpText:
    "Answer “yes” if you have an app that consumers can download from the Apple App Store or Google Play store.",
});

export const businessCaliforniaNonenglishQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "business-california-nonenglish",
  question:
    "Does your business regularly communicate with consumers in California in any languages other than English?",
  helpText:
    "Answer “yes” if your business regularly sends marketing emails to California consumers in other languages, or if its contracts, terms or agreements are ordinarily provided in other languages in California.",
});

export const businessTenMillionCaliforniansQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "business-california-10million",
  question:
    "The CCPA has enhanced reporting requirements for companies that handle data about a large portion of the population in California. Does your business have 10 million or more customers in California and/or get 10 million or more unique website visitors from California each year?",
  helpText: null,
});

export const businessServiceProviderQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "business-service-provider",
  question: "Is your business a “service provider” to other businesses?",

  helpText: (
    <>
      Answer ‘yes’ only if you are a B2B business that handles personal data about your customers'
      end users.{" "}
      <ExternalLink href="https://help.truevault.com/article/45-are-you-a-service-provider">
        Read more about “service providers.”
      </ExternalLink>
    </>
  ),
});

export const businessCollectionForOtherBusinessesQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: COLLECTION_FOR_OTHER_BUSINESSES_SLUG,
  question: (
    <>
      Does your business collect (or purchase) personal information about individuals who it does
      not have a direct relationship with?
    </>
  ),
  helpText: (
    <>
      For example, answer 'yes' if your business purchases prospective customers' contact
      information from{" "}
      <a href="https://www.truevault.com/learn/essentials/what-is-a-data-broker">data brokers</a>,
      or collects individuals' shipping information from retailers like Amazon for purposes of
      drop-shipping.
    </>
  ),
});

export const businessHipaaCompliantQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "business-hipaa-compliant",
  header: "Health Insurance Portability and Accountability Act (HIPAA)",
  question:
    "Is your business a “covered entity” or “business associate” that handles protected health information (PHI)?",
  helpText:
    "E.g., a healthcare provider, health plan, healthcare clearinghouse, or a business associate that processes PHI on behalf of another entity.",
});

export const businessGlbaCompliantQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "business-glba-compliant",
  header: "Gramm-Leach-Bliley Act (GLBA)",
  question:
    "Is your business a financial institution that provides a personal financial service or product?",
  helpText:
    "Eg., banks, securities brokers, insurance agents, finance companies, and even auto dealers if they extend credit, arrange financing, or give financial advice.",
});

export const businessFcraCompliantQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "business-fcra-compliant",
  header: "Fair Credit Reporting Act (FCRA)",
  question:
    "Does your business share consumer personal information with consumer reporting agencies or receive said information from them?",
  helpText:
    "E.g., a property rental company provides data about evictions to a credit bureau; a car dealership pulls a credit report for a potential buyer.",
});

const businessQuestions = (_answers: Record<string, string>): SurveyQuestionProps[] => {
  return [
    physicalLocationQuestion(), // "business-physical-location"
    businessKidsQuestion(), // business-products-aimed-at-kids
    businessKidsUnder13Question(), // business-products-aimed-at-kids-under-13
    businessMobileAppsQuestion(), // business-has-mobile-apps
    businessCaliforniaNonenglishQuestion(), // business-california-nonenglish
    businessTenMillionCaliforniansQuestion(), // business-california-10million

    businessCollectionForOtherBusinessesQuestion(), // consumer-collection-for-other-businesses
    businessServiceProviderQuestion(), // business-service-provider

    businessHipaaCompliantQuestion(), // business-hipaa-compliant
    businessGlbaCompliantQuestion(), // business-glba-compliant
    businessFcraCompliantQuestion(), // business-fcra-compliant
  ];
};

const businessVisibility = (answers: Record<string, string>): Record<string, boolean> => {
  return {
    "business-physical-location": true,
    "business-products-aimed-at-kids": true,
    "business-products-aimed-at-kids-under-13":
      answers["business-products-aimed-at-kids"] == "true",
    "business-has-mobile-apps": true,
    "business-california-nonenglish": true,
    "business-california-10million": true,
    [COLLECTION_FOR_OTHER_BUSINESSES_SLUG]: true,
    "business-service-provider": true,
    "business-hipaa-compliant": true,
    "business-glba-compliant": true,
    "business-fcra-compliant": true,
  };
};

export const businessSurvey = (answers: Record<string, string>): Survey => {
  return {
    questions: businessQuestions(answers),
    visibility: businessVisibility(answers),
    disabled: {},
  };
};
