import { Routes } from "../../root/routes";
import { Survey2023Q3Step } from "./Survey2023Q3Steps";

export const getStepLogic = (step: Survey2023Q3Step): StepLogic => {
  return STEP_LOGIC[step];
};

interface StepLogic {
  route: string;
}

const SellingSharing: StepLogic = {
  route: Routes.surveys.midyear2023.sellingSharing,
};

const DataCollection: StepLogic = {
  route: Routes.surveys.midyear2023.dataCollection,
};

const DataRecipients: StepLogic = {
  route: Routes.surveys.midyear2023.dataRecipients,
};

const DataSources: StepLogic = {
  route: Routes.surveys.midyear2023.dataSources,
};

const WebsiteAudit: StepLogic = {
  route: Routes.surveys.midyear2023.websiteAudit,
};

const NextSteps: StepLogic = {
  route: Routes.surveys.midyear2023.nextSteps,
};

type StepMap = Record<Survey2023Q3Step, StepLogic>;
const STEP_LOGIC: StepMap = {
  SellingSharing: SellingSharing,
  DataCollection: DataCollection,
  DataRecipients: DataRecipients,
  DataSources: DataSources,
  WebsiteAudit: WebsiteAudit,
  NextSteps: NextSteps,
};
