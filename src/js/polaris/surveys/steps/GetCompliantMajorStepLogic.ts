import { ProgressEnum } from "../../components/organisms/callouts/ProgressCard";
import { Routes } from "../../root/routes";
import { getStepLogic } from "./GetCompliantStepLogic";
import {
  GetCompliantMajorStep,
  GetCompliantStep,
  StepComparison,
  STEPS,
} from "./GetCompliantSteps";

export type MajorStepLogic = {
  label: string;
  description: string;

  href?: string;
};

type MajorStepStatus = {
  progress: ProgressEnum;
  active: boolean;
  action: string;
  href: string;
};

export const majorStepContains = (
  major: GetCompliantMajorStep,
  minor: GetCompliantStep,
): boolean => {
  const minorLogic = getStepLogic(minor);
  if (!minorLogic) {
    return false;
  }
  return minorLogic.majorStep == major;
};

export const getMajorStepStatus = (
  major: GetCompliantMajorStep,
  current: GetCompliantStep,
): MajorStepStatus => {
  const steps = getIndividualStepsForMajorStep(major);
  const firstMinor = steps[0];
  const lastMinor = steps[steps.length - 1];

  const currentStep = steps.find((s) => s == current) || steps[0];
  const currentStepLogic = getStepLogic(currentStep);

  const hasNotStarted = StepComparison.isBefore(current, firstMinor);
  const hasFinished = StepComparison.isAfter(current, lastMinor);
  const isCurrent = !hasNotStarted && !hasFinished;

  let progress: ProgressEnum = "NOT_STARTED";
  let action: string = "Start";
  if (isCurrent && StepComparison.isEqual(current, firstMinor)) {
    progress = "NEXT";
  } else if (isCurrent) {
    progress = "IN_PROGRESS";
    action = "Resume";
  } else if (hasFinished) {
    progress = "DONE";
    action = "View & Edit";
  }

  const active = progress == "IN_PROGRESS" || progress == "NEXT";
  const majorStepLogic = getMajorStepLogic(major);

  return { progress, active, action, href: majorStepLogic?.href || currentStepLogic.route };
};

export const getIndividualStepsForMajorStep = (
  major: GetCompliantMajorStep,
): GetCompliantStep[] => {
  return STEPS.filter((s) => {
    const logic = getStepLogic(s);
    return logic.majorStep == major;
  });
};

export const getMajorStepLogic = (major: GetCompliantMajorStep): MajorStepLogic => {
  switch (major) {
    case "BusinessSurvey":
      return {
        label: "Business Survey",
        description:
          "Our survey helps classify what areas of privacy laws require your attention so we can tailor your onboarding to match your compliance needs.",
      };
    case "ConsumerDataMap":
      return {
        label: "Consumer Data Map",
        description:
          "To complete your compliance process, we need to know what information you collect, who you share data with, and what data is shared. Data mapping is the core of setting up a privacy compliance program.",
      };
    case "HRDataMap":
      return {
        label: "HR Data Map",
        description:
          "Now that you’ve mapped your data collection and disclosure practice for external consumers, it’s time to map your employees.",
      };
    case "SharingSelling":
      return {
        label: "Sharing/Selling",
        description: "Classify your data recipients",
      };
    case "DataUses":
      return {
        label: "Data Uses",
        description:
          "Next, you’ll identify your sources of data collection and how your business uses personal data it collects.",
        href: Routes.getCompliant.dataCollection.consumerCollection,
      };
    case "DataRetention":
      return {
        label: "Data Retention",
        description: "Next, answer a few questions about your data storage practices.",
      };
    case "PrivacyNotice":
      return {
        label: "Privacy Notice",
        description:
          "Now that your data is all configured, there are a few steps we need to take to publish privacy notices to your website. Depending on your particular setup, you may need a developer to help with this step.",
      };
    case "PrivacyCenter":
      return {
        label: "Privacy Center",
        description:
          "Complete a few steps to get your Privacy Center set up, like adding your business logo.",
      };
  }
};
