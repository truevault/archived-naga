import { Routes } from "../../root/routes";
import { Survey2023Q1Step } from "./Survey2023Q1Steps";

export const getStepLogic = (step: Survey2023Q1Step): StepLogic => {
  return STEP_LOGIC[step];
};

interface StepLogic {
  route: string;
}

const BusinessSurvey: StepLogic = {
  route: Routes.surveys["2023"].businessSurvey,
};

const CollectionPurposes: StepLogic = {
  route: Routes.surveys["2023"].collectionPurposes,
};

const DataRetention: StepLogic = {
  route: Routes.surveys["2023"].dataRetention,
};

const DataRecipients: StepLogic = {
  route: Routes.surveys["2023"].dataRecipients,
};

const Tasks: StepLogic = {
  route: Routes.surveys["2023"].tasks,
};

type StepMap = Record<Survey2023Q1Step, StepLogic>;
const STEP_LOGIC: StepMap = {
  BusinessSurvey: BusinessSurvey,
  CollectionPurposes: CollectionPurposes,
  DataRetention: DataRetention,
  DataRecipients: DataRecipients,
  Tasks: Tasks,
};
