import { Dispatch, useCallback, useMemo, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { NetworkRequestState } from "../../../common/models";
import { getCompliantProgressActions } from "../../../common/reducers/getCompliantProgressSlice";
import { Api } from "../../../common/service/Api";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { useOrganizationVendors, usePrimaryOrganization } from "../../hooks";
import { useProgressKeys } from "../../hooks/useProgressKeys";
import { useStableValue } from "../../hooks/useStable";
import { useGetCompliantContext } from "../../root/GetCompliantContext";
import { getStepLogic, StepLogicContext } from "./GetCompliantStepLogic";

export const STEPS = [
  "BusinessSurvey.BusinessSurvey",
  "DataMap.Introduction",
  "BusinessSurvey.DataCollection",
  "DataRecipients.AddDataRecipients",
  "DataRecipients.Shopify",
  "DataRecipients.Contractors",
  "DataRecipients.ThirdPartyRecipients",
  "DataRecipients.VendorAssociation",
  "DataRecipients.ProcessingRegion",
  "DataRecipients.DataDisclosures",
  "DataRecipients.DataMapDoubleCheck",
  "DataRecipients.DataSources",

  "EmployeeMap.HRSurvey",
  "EmployeeMap.Introduction",
  "EmployeeCollection.DataCollection",
  "EmployeeMap.DataRecipients",
  "EmployeeMap.DataDisclosures",
  "EmployeeMap.DataSources",

  "DataRecipients.DataSharing",
  "DataRecipients.DataSelling",
  "DataRecipients.NoticeOfRightToOptOut",
  "DataRecipients.NoticeOfRightToLimit",
  "Classification.ConfirmServiceProvider",
  "Classification.FindAgreements",
  "Classification.ContactVendors",
  "Classification.GDPRConfirmProcessor",
  "Classification.GDPRFindAgreements",
  "Classification.GDPRContactUnknowns",
  "ConsumerCollection.CollectionPurposes",
  "ConsumerCollection.Minimization",
  "ConsumerCollection.LawfulBases",
  "ConsumerCollection.InternationalDataTransfers",
  "ConsumerCollection.ContactProcessors",

  "DataRetention.DataStorage",
  "DataRetention.ExceptionsToDeletion",
  "DataRetention.RetentionPolicy",
  "DataRetention.CookiePolicy",

  "ConsumerCollection.FinancialIncentives",
  "PrivacyCenter.DataPrivacyOfficer",
  "ConsumerCollection.ReviewPrivacyNotice",
  "ConsumerCollection.AdditionalPrivacyLanguage",
  "ConsumerCollection.ReviewEmploymentPrivacyNotices",

  "DataRetention.VendorIntegrations",
  "DataRetention.CookieScan",
  "PrivacyCenter.EmailAccount",
  "PrivacyCenter.EmailBranding",
  "PrivacyCenter.WebsiteHeader",
  "PrivacyCenter.Favicon",

  "PublishNotices",
  "Complete",
] as const;

type AllGetCompliantStep = typeof STEPS;
export type GetCompliantStep = AllGetCompliantStep[number];

export const MAJOR_STEPS = [
  "BusinessSurvey",
  "ConsumerDataMap",
  "HRDataMap",
  "SharingSelling",
  "DataUses",
  "DataRetention",
  "PrivacyNotice",
  "PrivacyCenter",
  "Done",
] as const;

type AllGetCompliantMajorSteps = typeof MAJOR_STEPS;
export type GetCompliantMajorStep = AllGetCompliantMajorSteps[number];

function isGetCompliantStep(value: string): value is GetCompliantStep {
  return STEPS.includes(value as GetCompliantStep);
}

export const StepComparison = {
  isAfter: (a: GetCompliantStep, b: GetCompliantStep) => {
    const aIdx = STEPS.indexOf(a);
    const bIdx = STEPS.indexOf(b);

    return aIdx > bIdx;
  },
  isBefore: (a: GetCompliantStep, b: GetCompliantStep) => {
    const aIdx = STEPS.indexOf(a);
    const bIdx = STEPS.indexOf(b);

    return aIdx < bIdx;
  },
  isEqual: (a: GetCompliantStep, b: GetCompliantStep) => {
    const aIdx = STEPS.indexOf(a);
    const bIdx = STEPS.indexOf(b);

    return bIdx == aIdx;
  },
};

export const migrateStep = (step: GetCompliantStep | LegacyGetCompliantStep): GetCompliantStep => {
  if (isGetCompliantStep(step)) {
    return step;
  }

  switch (step) {
    case "BUSINESS_SURVEY":
      return "BusinessSurvey.BusinessSurvey";
    case "CONSUMER_GROUPS":
    case "VENDORS_CONSUMER_GROUPS":
    case "DATA_COLLECTION":
    case "ConsumerCollection.UndisclosedData":
    case "BusinessSurvey.CollectionGroups":
    case "BusinessSurvey.GDPRCollectionGroups":
      return "BusinessSurvey.DataCollection";
    case "DATA_SELLING":
      return "DataRecipients.DataSelling";
    case "DataRecipients.Vendors":
    case "VENDORS":
      return "DataRecipients.AddDataRecipients";
    case "CLASSIFY_VENDORS":
      return "Classification.ConfirmServiceProvider";
    case "EMPLOYMENT_DATA_COLLECTION":
    case "EmployeeCollection":
      return "EmployeeMap.Introduction";
    case "DATA_RETENTION":
      return "DataRetention.ExceptionsToDeletion";
    case "DATA_SHARING":
    case "DataMap":
      return "DataRecipients.VendorAssociation";
    case "PrivacyCenter":
      return "PrivacyCenter.EmailAccount";
    case "ConsumerCollection.CollectionSources":
      return "DataRecipients.DataSources";
    case "ConsumerCollection.SourceAssociation":
      return "DataRecipients.DataSources";
    case "ConsumerCollection.SourceReceipts":
      return "DataRecipients.DataSources";
    case "PRIVACY_NOTICES":
    case "PUBLISH_NOTICES":
    case "COMPLIANCE_CHECKLIST":
    case "PRIVACY_REQUESTS":
      return "PublishNotices";
    case "COMPLETE":
      return "Complete";
    case "EmployeeMap.DataReceipt":
      return "EmployeeMap.DataSources";
    case "ConsumerCollection.ConsumerCollection":
      return "ConsumerCollection.CollectionPurposes";

    default:
      return "BusinessSurvey.BusinessSurvey";
  }
};

export type LegacyGetCompliantStep =
  | "BUSINESS_SURVEY"
  | "CONSUMER_GROUPS"
  | "VENDORS"
  | "CLASSIFY_VENDORS"
  | "DATA_COLLECTION"
  | "EMPLOYMENT_DATA_COLLECTION"
  | "DATA_RETENTION"
  | "DATA_SHARING"
  | "PRIVACY_NOTICES"
  | "PUBLISH_NOTICES"
  | "COMPLETE"
  // deprecated at time of migration
  | "VENDORS_CONSUMER_GROUPS"
  | "DATA_SELLING"
  | "COMPLIANCE_CHECKLIST"
  | "PRIVACY_REQUESTS"
  | "DataMap"
  | "ConsumerCollection.UndisclosedData"
  | "EmployeeCollection"
  | "PrivacyCenter"
  | "BusinessSurvey.CollectionGroups"
  | "BusinessSurvey.GDPRCollectionGroups"
  | "DataRecipients.Vendors"
  | "ConsumerCollection.CollectionSources"
  | "ConsumerCollection.SourceAssociation"
  | "ConsumerCollection.SourceReceipts"
  | "DataRecipients.SourceReceipts"
  | "ConsumerCollection.ConsumerCollection"
  | "EmployeeMap.DataReceipt";

// navigation

export const useNextStep = (
  currentStep: GetCompliantStep,
  onNext?: () => Promise<void>,
  forceUrl: string | undefined = undefined,
  skipProgress: boolean = false,
  skip: GetCompliantStep[] | undefined = undefined,
  overrideRecipients?: OrganizationDataRecipientDto[],
): [() => Promise<void>, NetworkRequestState[], boolean] => {
  const [navigating, setNavigating] = useState(false);

  const [org, orgRequest, refreshOrg] = usePrimaryOrganization();
  const [fetchedRecipients, recipientsRequest] = useOrganizationVendors(org.id);
  const { progress: orgProgress, request: orgProgressRequest } = useProgressKeys(org.id, []);
  const { progress: progressStep } = useGetCompliantContext();
  const dispatch = useDispatch();
  const history = useHistory();

  const stableOnNext = useStableValue(onNext);

  const recipients = overrideRecipients || fetchedRecipients;
  const context = useMemo(() => ({ org, recipients, orgProgress }), [org, recipients, orgProgress]);
  const handleNextStep = useCallback(async () => {
    setNavigating(true);

    try {
      if (stableOnNext) {
        await stableOnNext();
      }

      const res = await refreshOrg();

      await gotoNextStep(
        currentStep,
        progressStep,
        { ...context, org: res as any },
        history,
        dispatch,
        forceUrl,
        skipProgress,
        skip,
      );
    } catch (err) {
      console.warn(
        "an exception occurred while navigating to next step. Canceling navigation.",
        err,
      );
      setNavigating(false); // we only care about this if we failed to nav away
    }
  }, [
    stableOnNext,
    refreshOrg,
    currentStep,
    progressStep,
    context,
    history,
    dispatch,
    forceUrl,
    skipProgress,
    skip,
  ]);

  return [handleNextStep, [orgRequest, recipientsRequest, orgProgressRequest], navigating];
};

export const usePrevStep = (
  currentStep: GetCompliantStep,
  prevOnClick?: () => Promise<void>,
  forceUrl: string | undefined = undefined,
  skip: GetCompliantStep[] | undefined = undefined,
): [() => Promise<void>, NetworkRequestState[], boolean] => {
  const [navigating, setNavigating] = useState(false);

  const [org, orgRequest] = usePrimaryOrganization();
  const { progress: orgProgress, request: orgProgressRequest } = useProgressKeys(org.id, []);
  const [recipients, recipientsRequest] = useOrganizationVendors(org.id);
  const history = useHistory();

  const stableOnPrev = useStableValue(prevOnClick);
  const context = useMemo(() => ({ org, recipients, orgProgress }), [org, recipients, orgProgress]);

  const handlePrevStep = useCallback(async () => {
    setNavigating(true);

    try {
      if (stableOnPrev) {
        await stableOnPrev();
      }

      await gotoPrevStep(currentStep, context, history, forceUrl, skip);
    } catch (err) {
      console.warn(
        "an exception occurred while navigating to previous step. Canceling navigation.",
        err,
      );
      setNavigating(false); // we only care about this if we failed to nav away
    }
  }, [currentStep, stableOnPrev, context, history, forceUrl, skip]);

  return [handlePrevStep, [orgRequest, recipientsRequest, orgProgressRequest], navigating];
};

export const gotoNextStep = async (
  presentStep: GetCompliantStep,
  progressStep: GetCompliantStep,
  context: StepLogicContext,
  history: ReturnType<typeof useHistory>,
  dispatch: Dispatch<any>,
  forceUrl: string | undefined = undefined,
  skipProgress: boolean = false,
  skip: GetCompliantStep[] | undefined = undefined,
) => {
  const next = getNextStep(presentStep, context, skip);
  const nextLogic = getStepLogic(next);

  if (!skipProgress && StepComparison.isAfter(next, progressStep)) {
    await Api.organization.updateGetCompliantProgress(context.org.id, next);
    dispatch(getCompliantProgressActions.set(next));
  }

  history.push(forceUrl || nextLogic.route);
};

export const gotoPrevStep = async (
  presentStep: GetCompliantStep,
  context: StepLogicContext,
  history: ReturnType<typeof useHistory>,
  forceUrl: string | undefined = undefined,
  skip: GetCompliantStep[] | undefined = undefined,
) => {
  const prev = getPrevStep(presentStep, context, skip || []);
  const prevLogic = getStepLogic(prev);
  history.push(forceUrl || prevLogic.route);
};

export const getNextStep = (
  currentStep: GetCompliantStep,
  context: StepLogicContext,
  skip: GetCompliantStep[] = [],
): GetCompliantStep => {
  // get the current step index!
  let currIdx = STEPS.indexOf(currentStep);
  if (currIdx < 0) {
    currIdx = 0;
  }

  // walk to the end of the list
  for (let idx = currIdx + 1; idx < STEPS.length; idx++) {
    const step = STEPS[idx];
    if (skip.includes(step)) {
      continue;
    }

    const logic = getStepLogic(step);

    const canVisit = !logic.skip(context);
    if (canVisit) {
      return step;
    }
  }

  const forcedIdx = Math.min(currIdx + 1, STEPS.length - 1);
  return STEPS[forcedIdx];
};

export const getPrevStep = (
  currentStep: GetCompliantStep,
  context: StepLogicContext,
  skip: GetCompliantStep[] = [],
): GetCompliantStep => {
  // get the current step index!
  let currIdx = STEPS.indexOf(currentStep);
  if (currIdx < 0) {
    currIdx = 0;
  }

  // walk to the end of the list
  for (let idx = currIdx - 1; idx >= 0; idx--) {
    const step = STEPS[idx];
    if (skip.includes(step)) {
      continue;
    }

    const logic = getStepLogic(step);

    const canVisit = logic.visible(context) && !logic.skip(context);
    if (canVisit) {
      return step;
    }
  }

  const forcedIdx = Math.max(currIdx - 1, 0);
  return STEPS[forcedIdx];
};
