import { useCallback, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { Api } from "../../../common/service/Api";
import { OrganizationPublicId } from "../../../common/service/server/Types";
import { useStableValue } from "../../hooks/useStable";
import { getStepLogic } from "./Survey2023Q1StepLogic";

export type SetupStep = "Setup.VendorSetup";

export const STEPS = [
  "BusinessSurvey",
  "CollectionPurposes",
  "DataRetention",
  "DataRecipients",
  "Tasks",
] as const;

type AllSurvey2023Q1Step = typeof STEPS;
export type Survey2023Q1Step = AllSurvey2023Q1Step[number];

export const Survey2023Q1StepComparison = {
  isAfter: (a: Survey2023Q1Step, b: Survey2023Q1Step) => {
    const aIdx = STEPS.indexOf(a);
    const bIdx = STEPS.indexOf(b);

    return aIdx > bIdx;
  },
  isBefore: (a: Survey2023Q1Step, b: Survey2023Q1Step) => {
    const aIdx = STEPS.indexOf(a);
    const bIdx = STEPS.indexOf(b);

    return aIdx < bIdx;
  },
  isEqual: (a: Survey2023Q1Step, b: Survey2023Q1Step) => {
    const aIdx = STEPS.indexOf(a);
    const bIdx = STEPS.indexOf(b);

    return bIdx == aIdx;
  },
};

// note: we should merge the implementation of this and the GetCompliantSteps implementation. We should have a single generic set of "step logic" rather
// than duplicating them.

export const getNextStep = (
  currentStep: Survey2023Q1Step,
  // context: StepLogicContext,
  // skip: GetCompliantStep[] = [],
): Survey2023Q1Step => {
  // get the current step index!
  let currIdx = STEPS.indexOf(currentStep);
  if (currIdx < 0) {
    currIdx = 0;
  }

  // walk to the end of the list
  for (let idx = currIdx + 1; idx < STEPS.length; idx++) {
    const step = STEPS[idx];
    // const logic = getStepLogic(step);

    // const canVisit = logic.visible(context) && !logic.skip(context);
    // if (canVisit) {
    //   return step;
    // }
    return step;
  }

  const forcedIdx = Math.min(currIdx + 1, STEPS.length - 1);
  return STEPS[forcedIdx];
};

export const gotoNextStep = async (
  orgId: OrganizationPublicId,
  presentStep: Survey2023Q1Step,
  progressStep: Survey2023Q1Step,
  history: ReturnType<typeof useHistory>,
  skipProgress: boolean = false,
) => {
  const next = getNextStep(presentStep);
  const nextLogic = getStepLogic(next);

  if (!skipProgress && Survey2023Q1StepComparison.isAfter(next, progressStep)) {
    await Api.organizationProgress.updateSurveyProgress(orgId, "s2023_1", next);
  }

  history.push(nextLogic.route);
};

export const gotoPrevStep = async (
  orgId: OrganizationPublicId,
  presentStep: Survey2023Q1Step,
  history: ReturnType<typeof useHistory>,
) => {
  const prev = getPrevStep(presentStep);
  const prevLogic = getStepLogic(prev);
  history.push(prevLogic.route);
};

export const useNextStep = (
  orgId: OrganizationPublicId,
  currentStep: Survey2023Q1Step,
  progressStep: Survey2023Q1Step,
  onNext?: () => Promise<boolean | void>,
  skipProgress: boolean = false,
): [() => Promise<void>, boolean] => {
  const [navigating, setNavigating] = useState(false);
  // const { progress: progressStep } = useGetCompliantContext();
  const dispatch = useDispatch();
  const history = useHistory();

  const stableOnNext = useStableValue(onNext);

  const handleNextStep = useCallback(async () => {
    setNavigating(true);

    try {
      if (stableOnNext) {
        const cancel = await stableOnNext();
        if (cancel) {
          setNavigating(false);
          return;
        }
      }

      await gotoNextStep(orgId, currentStep, progressStep, history);
    } catch (err) {
      console.warn(
        "an exception occurred while navigating to next step. Canceling navigation.",
        err,
      );
      setNavigating(false); // we only care about this if we failed to nav away
    }
  }, [stableOnNext, currentStep, progressStep, history, dispatch, skipProgress]);

  return [handleNextStep, navigating];
};

export const usePrevStep = (
  orgId: OrganizationPublicId,
  currentStep: Survey2023Q1Step,
  prevOnClick?: () => Promise<void>,
): [() => Promise<void>, boolean] => {
  const [navigating, setNavigating] = useState(false);
  const history = useHistory();

  const stableOnPrev = useStableValue(prevOnClick);

  const handlePrevStep = useCallback(async () => {
    setNavigating(true);

    try {
      if (stableOnPrev) {
        await stableOnPrev();
      }

      await gotoPrevStep(orgId, currentStep, history);
    } catch (err) {
      console.warn(
        "an exception occurred while navigating to previous step. Canceling navigation.",
        err,
      );
      setNavigating(false); // we only care about this if we failed to nav away
    }
  }, [orgId, currentStep, stableOnPrev, history]);

  return [handlePrevStep, navigating];
};

export const getPrevStep = (currentStep: Survey2023Q1Step): Survey2023Q1Step => {
  // get the current step index!
  let currIdx = STEPS.indexOf(currentStep);
  if (currIdx < 0) {
    currIdx = 0;
  }

  // walk to the end of the list
  for (let idx = currIdx - 1; idx >= 0; idx--) {
    const step = STEPS[idx];
    // if (skip.includes(step)) {
    //   continue;
    // }

    // const logic = getStepLogic(step);

    // const canVisit = logic.visible(context) && !logic.skip(context);
    // if (canVisit) {
    //   return step;
    // }
    return step;
  }

  const forcedIdx = Math.max(currIdx - 1, 0);
  return STEPS[forcedIdx];
};
