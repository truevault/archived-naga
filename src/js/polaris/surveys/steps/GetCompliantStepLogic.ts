import { OrganizationProgressDto } from "../../../common/service/server/controller/OrganizationProgressController";
import { OrganizationDataRecipientDto } from "../../../common/service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../../../common/service/server/dto/OrganizationDto";
import { PrivacyCenterDto } from "../../../common/service/server/dto/PrivacyCenterDto";
import { RouteMatches, Routes } from "../../root/routes";
import { GetCompliantMajorStep, GetCompliantStep } from "./GetCompliantSteps";
import { isGoogleAnalytics } from "../../util/vendors";

export type StepLogicContext = {
  org: OrganizationDto;
  recipients: OrganizationDataRecipientDto[];
  orgProgress: OrganizationProgressDto[];
  privacyCenter?: PrivacyCenterDto;
};

export const getStepLogic = (step: GetCompliantStep): StepLogic => {
  const logic: Logic = STEP_LOGIC[step];
  const route = logic?.route || Routes.getCompliant.root;

  return {
    visible: logic?.visible || DefaultStepLogic.visible,
    skip: logic?.skip || DefaultStepLogic.skip,
    label: logic?.label || "",
    majorStep: logic?.majorStep,
    minorStep: logic?.minorStep,
    route,
    matchesRoute: logic?.matchesRoute || ((path) => path == route),
  };
};

interface StepLogic {
  majorStep: GetCompliantMajorStep;
  minorStep?: string;
  visible: (context: StepLogicContext) => boolean;
  skip: (context: StepLogicContext) => boolean;
  matchesRoute: (path: string) => boolean;
  label: string;
  route: string;
}

interface Logic {
  visible?: (context: StepLogicContext) => boolean;
  skip?: (context: StepLogicContext) => boolean;
  matchesRoute?: (path: string) => boolean;
  label: string;
  route: string;
  majorStep: GetCompliantMajorStep;
  minorStep?: string;
}

const DefaultStepLogic: Logic = {
  visible: (_context: StepLogicContext) => true,
  skip: (_context: StepLogicContext) => false,
  matchesRoute: (path: string) => path == Routes.getCompliant.root,
  majorStep: "Done",
  label: "NO LABEL",
  route: Routes.getCompliant.root,
};

const BusinessSurvey: Logic = {
  label: "Business Survey",
  route: Routes.getCompliant.businessSurvey.root,
  majorStep: "BusinessSurvey",
};

const DataCollection: Logic = {
  label: "Collection",
  route: Routes.getCompliant.businessSurvey.consumerDataCollection,
  majorStep: "ConsumerDataMap",
  matchesRoute: (p) =>
    p == Routes.getCompliant.businessSurvey.consumerDataCollection ||
    p.startsWith(Routes.getCompliant.businessSurvey.consumerDataCollection),
};

const DataMapIntroduction: Logic = {
  label: "Data Map Introduction",
  skip: ({ orgProgress }) => {
    const introProgress = orgProgress?.find((p) => p.key === "data_map_intro");

    if (introProgress && introProgress.progress.value === "DONE") {
      return true;
    }

    return false;
  },
  visible: () => false,
  route: Routes.getCompliant.dataMap.introduction,
  majorStep: "ConsumerDataMap",
};

const AddDataRecipients: Logic = {
  label: "Vendors",
  route: Routes.getCompliant.dataRecipients.root,
  majorStep: "ConsumerDataMap",
  minorStep: "Recipients",
};

const Shopify: Logic = {
  label: "Shopify Store Apps",
  route: Routes.getCompliant.dataRecipients.platforms,
  // POLARIS-2162 - skip the Shopify Apps page during on-boarding (for now)
  skip: (context) => true || !context.recipients.some((p) => p.isPlatform),
  visible: () => false,
  majorStep: "ConsumerDataMap",
  minorStep: "Recipients",
};

const VendorAssociation: Logic = {
  label: "Associations",
  route: Routes.getCompliant.dataRecipients.vendorAssociation,
  visible: (context) => context.org.requiresAssociations,
  matchesRoute: (p) =>
    p == Routes.getCompliant.dataRecipients.vendorAssociation ||
    p.startsWith(Routes.getCompliant.dataRecipients.vendorAssociation),
  majorStep: "ConsumerDataMap",
  minorStep: "Recipients",
};

const DataDisclosures: Logic = {
  label: "Disclosures",
  route: Routes.getCompliant.dataRecipients.dataDisclosures,
  majorStep: "ConsumerDataMap",
  matchesRoute: (p) => {
    return (
      p == Routes.getCompliant.dataRecipients.dataDisclosures ||
      p == Routes.getCompliant.dataRecipients.doubleCheck
    );
  },
};

const DataMapDoubleCheck: Logic = {
  label: "Disclosues Double Check",
  route: Routes.getCompliant.dataRecipients.doubleCheck,
  // not shown in the nav, but shown as part of data disclosures
  visible: () => false,
  majorStep: "ConsumerDataMap",
};

const DataSources: Logic = {
  label: "Sources",
  route: Routes.getCompliant.dataMap.consumerSources,
  majorStep: "ConsumerDataMap",
};

const Contractors: Logic = {
  label: "Contractors",
  route: Routes.getCompliant.dataRecipients.contractors,
  majorStep: "ConsumerDataMap",
  minorStep: "Recipients",
};

const ThirdParty: Logic = {
  label: "Other Parties",
  route: Routes.getCompliant.dataRecipients.thirdPartyRecipients,
  majorStep: "ConsumerDataMap",
  minorStep: "Recipients",
};

const ProcessingRegion: Logic = {
  label: "Regions",
  route: Routes.getCompliant.dataRecipients.processingRegion,
  visible: ({ org }) => org.featureGdpr,
  skip: ({ org }) => !org.featureGdpr,
  majorStep: "ConsumerDataMap",
  minorStep: "Recipients",
};

const EmployeeMapHRSurvey: Logic = {
  label: "HR Survey",
  route: Routes.getCompliant.employeeMap.hrSurvey,
  majorStep: "HRDataMap",
};

const EmployeeMapIntroduction: Logic = {
  label: "Employee Map Introduction",
  visible: () => false,
  skip: ({ org, orgProgress }) => {
    if (!org.requiresEmployeeMapping) {
      return !org.requiresEmployeeMapping;
    }

    const introProgress = orgProgress?.find((p) => p.key === "employee_map_intro");

    if (introProgress && introProgress.progress.value === "DONE") {
      return true;
    }

    return false;
  },
  route: Routes.getCompliant.employeeMap.introduction,
  majorStep: "HRDataMap",
};

const EmployeeMapDataRecipients: Logic = {
  label: "Recipients",
  route: Routes.getCompliant.employeeMap.dataRecipients,
  visible: ({ org }) => org.requiresEmployeeMapping,
  skip: ({ org }) => !org.requiresEmployeeMapping,
  majorStep: "HRDataMap",
};

const EmployeeMapDataDisclosures: Logic = {
  label: "Disclosures",
  route: Routes.getCompliant.employeeMap.dataDisclosures,
  visible: ({ org }) => org.requiresEmployeeMapping,
  skip: ({ org }) => !org.requiresEmployeeMapping,
  majorStep: "HRDataMap",
};

const EmployeeMapDataSources: Logic = {
  label: "Sources",
  route: Routes.getCompliant.employeeMap.dataSources,
  visible: ({ org }) => org.requiresEmployeeMapping,
  skip: ({ org }) => !org.requiresEmployeeMapping,
  majorStep: "HRDataMap",
};

const DataSharing: Logic = {
  label: "Data Sharing",
  route: Routes.getCompliant.dataRecipients.sharing,
  majorStep: "SharingSelling",
};

const DataSelling: Logic = {
  label: "Data Selling",
  route: Routes.getCompliant.dataRecipients.selling,
  majorStep: "SharingSelling",
};

const NoticeOptOut: Logic = {
  label: "Opt-Outs",
  route: Routes.getCompliant.dataRecipients.notice,
  matchesRoute: (p) =>
    p == Routes.getCompliant.dataRecipients.notice ||
    p == Routes.getCompliant.dataRecipients.noticeLimit,
  majorStep: "SharingSelling",
};

const NoticeLimit: Logic = {
  label: "Notice of Right to Limit",
  route: Routes.getCompliant.dataRecipients.noticeLimit,
  skip: (context) => !context.org.requiresNoticeOfRightToLimit,
  visible: () => false,
  majorStep: "SharingSelling",
};

const ConfirmServiceProvider: Logic = {
  label: "Documentation",
  route: Routes.getCompliant.dataRecipients.classifyServices,
  majorStep: "SharingSelling",
  minorStep: "Classification",
};

const FindAgreements: Logic = {
  label: "Vendor Research",
  route: Routes.getCompliant.dataRecipients.locateOther,
  majorStep: "SharingSelling",
  minorStep: "Classification",
  matchesRoute: (p) =>
    p == Routes.getCompliant.dataRecipients.locateOther ||
    p == Routes.getCompliant.dataRecipients.contact,
};

const ContactVendors: Logic = {
  label: "Contact Vendors",
  route: Routes.getCompliant.dataRecipients.contact,
  majorStep: "SharingSelling",
  minorStep: "Classification",
  visible: () => false,
};

const GDPRConfirmProcessor: Logic = {
  label: "GDPR Documentation",
  route: Routes.getCompliant.dataRecipients.gdpr.confirmProcessors,
  skip: ({ org }) => !org.featureGdpr,
  visible: ({ org }) => org.featureGdpr,
  majorStep: "SharingSelling",
  minorStep: "Classification",
};

const GDPRFindAgreements: Logic = {
  label: "GDPR Research",
  route: Routes.getCompliant.dataRecipients.gdpr.findAgreements,
  skip: ({ org }) => !org.featureGdpr,
  visible: ({ org }) => org.featureGdpr,
  majorStep: "SharingSelling",
  minorStep: "Classification",
  matchesRoute: (p) =>
    p == Routes.getCompliant.dataRecipients.gdpr.findAgreements ||
    p == Routes.getCompliant.dataRecipients.gdpr.contactUnknowns,
};

const GDPRContactUnknowns: Logic = {
  label: "Contact Unknown GDPR Status",
  route: Routes.getCompliant.dataRecipients.gdpr.contactUnknowns,
  skip: ({ org }) => !org.featureGdpr,
  visible: () => false,
  majorStep: "SharingSelling",
  minorStep: "Classification",
};

const CollectionPurposes: Logic = {
  label: "Purposes",
  route: Routes.getCompliant.dataCollection.consumerPurposes,
  majorStep: "DataUses",
};

const ConsumerMinimization: Logic = {
  label: "Minimization",
  route: Routes.getCompliant.dataCollection.corePrivacyConcepts,
  majorStep: "DataUses",
  minorStep: "",
};

const ConsumerLawfulBases: Logic = {
  label: "Lawful Bases",
  route: Routes.getCompliant.dataCollection.gdprLawfulBases,
  skip: ({ org }) => !org.featureGdpr,
  visible: ({ org }) => org.featureGdpr,
  majorStep: "DataUses",
  matchesRoute: (p) => p == Routes.getCompliant.dataCollection.gdprLawfulBases,
};

const InternationalDataTransfers: Logic = {
  label: "Data Transfers",
  route: Routes.getCompliant.dataCollection.internationalDataTransfers,
  skip: ({ org }) => !org.featureGdpr,
  visible: ({ org }) => org.featureGdpr,
  majorStep: "DataUses",
  matchesRoute: (p) =>
    p == Routes.getCompliant.dataCollection.internationalDataTransfers ||
    p == Routes.getCompliant.dataCollection.contactProcessors,
};

const ContactProcessors: Logic = {
  label: "Contact Processors",
  route: Routes.getCompliant.dataCollection.contactProcessors,
  skip: ({ org }) => !org.featureGdpr,
  visible: () => false,
  majorStep: "DataUses",
};

const FinancialIncentives: Logic = {
  label: "Financial Incentives",
  route: Routes.getCompliant.dataCollection.consumerIncentives,
  majorStep: "PrivacyNotice",
};

const ReviewPrivacyNotice: Logic = {
  label: "Preview Notice",
  route: Routes.getCompliant.dataCollection.consumerNoticeReview,
  majorStep: "PrivacyNotice",
};

const ReviewEmploymentPrivacyNotices: Logic = {
  label: "HR Notices",
  route: Routes.getCompliant.dataCollection.employmentNoticeReview,
  visible: ({ org }) => org.requiresEmployeeMapping,
  skip: ({ org }) => !org.requiresEmployeeMapping,
  matchesRoute: (p) =>
    p == Routes.getCompliant.dataCollection.employmentNoticeReview ||
    RouteMatches.getCompliant.employeeMap.employeeReviewForGroup.test(p),
  majorStep: "PrivacyNotice",
};

const AdditionalPrivacyLanguage: Logic = {
  label: "Customization",
  route: Routes.getCompliant.dataCollection.additionalPrivacyLanguage,
  majorStep: "PrivacyNotice",
};

const ExceptionsToDeletion: Logic = {
  label: "Exceptions",
  route: Routes.getCompliant.dataRetention.deletionExceptions,
  majorStep: "DataRetention",
};

const VendorIntegrations: Logic = {
  label: "API Integrations",
  skip: (context) => {
    return !context.recipients.some((dr) => isGoogleAnalytics(dr));
  },
  route: Routes.getCompliant.dataRetention.vendorIntegrations,
  majorStep: "PrivacyCenter",
};

const RetentionPolicy: Logic = {
  label: "Retention Policy",
  route: Routes.getCompliant.dataRetention.retentionPolicy,
  majorStep: "DataRetention",
};

const DataStorage: Logic = {
  label: "Access/Deletion",
  route: Routes.getCompliant.dataRetention.dataStorage,
  majorStep: "DataRetention",
};

const CookieScan: Logic = {
  label: "Cookie Scan",
  route: Routes.getCompliant.dataRetention.gdprCookieScanner,
  skip: ({ org }) => !org.featureGdpr,
  visible: ({ org }) => org.featureGdpr,
  majorStep: "PrivacyCenter",
};

const CookiePolicy: Logic = {
  label: "Cookie Policy",
  route: Routes.getCompliant.dataRetention.cookiePolicy,
  majorStep: "DataRetention",
};

const EmployeeDataCollection: Logic = {
  label: "Collection",
  route: Routes.getCompliant.employeeMap.employee,
  visible: ({ org }) => org.requiresEmployeeMapping,
  skip: ({ org }) => !org.requiresEmployeeMapping,
  majorStep: "HRDataMap",
  matchesRoute: (p) =>
    p == Routes.getCompliant.employeeMap.employee ||
    RouteMatches.getCompliant.employeeMap.employeeCollectionForGroup.test(p),
};

const PrivacyCenterEmailAccount: Logic = {
  label: "Email Account",
  route: Routes.getCompliant.privacyCenter.emailAccount,
  majorStep: "PrivacyCenter",
};

const PrivacyCenterEmailBranding: Logic = {
  label: "Email Branding",
  route: Routes.getCompliant.privacyCenter.emailBranding,
  majorStep: "PrivacyCenter",
};

const PrivacyCenterWebsiteHeader: Logic = {
  label: "Website Header",
  route: Routes.getCompliant.privacyCenter.websiteHeader,
  majorStep: "PrivacyCenter",
};

const PrivacyCenterFavicon: Logic = {
  label: "Favicon Logo",
  route: Routes.getCompliant.privacyCenter.favicon,
  majorStep: "PrivacyCenter",
};

const PrivacyCenterDataPrivacyOfficer: Logic = {
  label: "Privacy Officer",
  route: Routes.getCompliant.privacyCenter.dataPrivacyOfficer,
  skip: ({ org }) => !org.featureGdpr,
  visible: ({ org }) => org.featureGdpr,
  majorStep: "PrivacyNotice",
};

const PublishNotices: Logic = {
  label: "Publish Notices",
  route: Routes.getCompliant.publishNotices.root,
  majorStep: "PrivacyCenter",
};

const Complete: Logic = {
  label: "Schedule Training",
  route: Routes.getCompliant.complete.root,
  majorStep: "Done",
};

type StepMap = Record<GetCompliantStep, Logic>;
const STEP_LOGIC: StepMap = {
  "BusinessSurvey.BusinessSurvey": BusinessSurvey,
  "DataMap.Introduction": DataMapIntroduction,
  "BusinessSurvey.DataCollection": DataCollection,
  "DataRecipients.AddDataRecipients": AddDataRecipients,
  "DataRecipients.Shopify": Shopify,
  "DataRecipients.VendorAssociation": VendorAssociation,
  "DataRecipients.DataDisclosures": DataDisclosures,
  "DataRecipients.DataMapDoubleCheck": DataMapDoubleCheck,
  "DataRecipients.DataSources": DataSources,
  "DataRecipients.Contractors": Contractors,
  "DataRecipients.ThirdPartyRecipients": ThirdParty,
  "DataRecipients.ProcessingRegion": ProcessingRegion,
  "EmployeeMap.HRSurvey": EmployeeMapHRSurvey,
  "EmployeeMap.Introduction": EmployeeMapIntroduction,
  "EmployeeCollection.DataCollection": EmployeeDataCollection,
  "EmployeeMap.DataRecipients": EmployeeMapDataRecipients,
  "EmployeeMap.DataDisclosures": EmployeeMapDataDisclosures,
  "EmployeeMap.DataSources": EmployeeMapDataSources,
  "DataRecipients.DataSharing": DataSharing,
  "DataRecipients.DataSelling": DataSelling,
  "DataRecipients.NoticeOfRightToOptOut": NoticeOptOut,
  "DataRecipients.NoticeOfRightToLimit": NoticeLimit,
  "Classification.ConfirmServiceProvider": ConfirmServiceProvider,
  "Classification.FindAgreements": FindAgreements,
  "Classification.ContactVendors": ContactVendors,
  "Classification.GDPRConfirmProcessor": GDPRConfirmProcessor, //{ visible: ({ org }) => org.featureGdpr },
  "Classification.GDPRFindAgreements": GDPRFindAgreements,
  "Classification.GDPRContactUnknowns": GDPRContactUnknowns,
  "ConsumerCollection.CollectionPurposes": CollectionPurposes,
  "ConsumerCollection.Minimization": ConsumerMinimization,
  "ConsumerCollection.LawfulBases": ConsumerLawfulBases,
  "ConsumerCollection.FinancialIncentives": FinancialIncentives,
  "ConsumerCollection.InternationalDataTransfers": InternationalDataTransfers,
  "ConsumerCollection.ContactProcessors": ContactProcessors,
  "ConsumerCollection.ReviewPrivacyNotice": ReviewPrivacyNotice,
  "ConsumerCollection.ReviewEmploymentPrivacyNotices": ReviewEmploymentPrivacyNotices,
  "ConsumerCollection.AdditionalPrivacyLanguage": AdditionalPrivacyLanguage,
  "DataRetention.ExceptionsToDeletion": ExceptionsToDeletion,
  "DataRetention.VendorIntegrations": VendorIntegrations,
  "DataRetention.RetentionPolicy": RetentionPolicy,
  "DataRetention.DataStorage": DataStorage,
  "DataRetention.CookieScan": CookieScan,
  "DataRetention.CookiePolicy": CookiePolicy,
  "PrivacyCenter.EmailAccount": PrivacyCenterEmailAccount,
  "PrivacyCenter.EmailBranding": PrivacyCenterEmailBranding,
  "PrivacyCenter.WebsiteHeader": PrivacyCenterWebsiteHeader,
  "PrivacyCenter.Favicon": PrivacyCenterFavicon,
  "PrivacyCenter.DataPrivacyOfficer": PrivacyCenterDataPrivacyOfficer,
  PublishNotices: PublishNotices,
  Complete: Complete,
};
