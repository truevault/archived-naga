import React from "react";
import { Link } from "react-router-dom";
import { ExternalLink } from "../../common/components/ExternalLink";
import { SurveyAnswers } from "../../common/hooks/useSurvey";
import { OrganizationDataRecipientDto } from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { getAdNetworkVendors } from "../pages/get-compliant/data-recipients/shared";
import { Routes } from "../root/routes";
import { Survey } from "./survey";
import { alphabetically, noneOption, vendorToOption } from "./util";

const sharingInstructions = (): SurveyQuestionProps => {
  return {
    type: "acknowledgement",
    question: 'What is data "sharing"?',
    slug: "sharing-definition-acknowledgement",
    text: (
      <>
        Consumers have the right to opt-out of behavioral advertising (also known as targeted or
        personalized advertising). Behavioral advertising is when a person’s online activity is
        tracked across different websites or devices through an online identifier like a cookie or
        pixel placed on your website by an ad network like Facebook Ads. The collection and use of
        online activity via the tracking device is considered “sharing.”
      </>
    ),
  };
};

export const shareDataQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: "share-data-for-advertising",
  question:
    "Does your website use tracking technology from ad networks for behavioral advertising?",
  helpText:
    "For example, if your website visitors see advertisements for products they viewed on your site after they leave your site and visit other sites.",
});

export const customAudienceFeatureQuestion = (
  vendors: OrganizationDataRecipientDto[],
): SurveyQuestionProps => {
  const adNetworkVendors = getAdNetworkVendors(vendors);
  const adNetworkVendorOptions = adNetworkVendors
    .filter((v) => v.collectionContext.includes("CONSUMER"))
    .sort(alphabetically)
    .map((adv) => vendorToOption(adv));
  adNetworkVendorOptions.unshift(noneOption());

  return {
    type: "checkboxes",
    slug: "custom-audience-feature",
    question:
      'Does your business use the "Custom Audience" feature (sometimes called "Customer Match") with any of its ad networks? Select any ad networks for which you use this feature, or select "None."',
    helpText: (
      <>
        <ExternalLink
          className="bold-link"
          href="https://www.facebook.com/business/help/170456843145568?id=2469097953376494"
        >
          {" "}
          Custom Audience{" "}
        </ExternalLink>{" "}
        is a service offered by most ad networks in which a business can upload a list of their
        customers or contacts, and the ad network will either deliver ads directly to those people
        and/or use it to create a "look-alike" audience.
      </>
    ),
    options: adNetworkVendorOptions,
    noneOption: true,
  };
};

export const vendorSharingCategories = (vendors): SurveyQuestionProps => {
  const adNetworkVendors = getAdNetworkVendors(vendors);
  const adNetworkVendorOptions = adNetworkVendors
    .filter((v) => v.collectionContext.includes("CONSUMER"))
    .sort(alphabetically)
    .map((adv) => vendorToOption(adv));

  return {
    type: "checkboxes",
    slug: "vendor-sharing-categories",
    question: "Which ad networks does your business use for behavioral advertising?",
    helpText: (
      <>
        If you need to add an ad network as a vendor, go back to{" "}
        <Link className={"bold-link"} to={Routes.getCompliant.dataRecipients.root}>
          Vendors
        </Link>
        .
      </>
    ),
    emptyText: "You do not have any vendors categorized as an ad network.",
    options: adNetworkVendorOptions,
    noneOption: false,
  };
};

// POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
// export const facebookLduQuestion = (): SurveyQuestionProps => {
//   return {
//     type: "boolean",
//     slug: "facebook-ldu",
//     question: "Will your business enable Limited Data Use (LDU) for Facebook Ads?",
//     helpText: (
//       <>
//         You’re not required to enable LDU. Enabling LDU will "turn off" personalized advertising via
//         Facebook Ads for all California visitors to your website. This may impact your advertising
//         for California web visitors, but it means California consumers will not need to click a link
//         on your website to opt-out of “sharing” with Facebook Ads.{" "}
//         <a
//           href="https://www.facebook.com/business/help/1151133471911882"
//           target="_blank"
//           rel="noreferrer noopener"
//         >
//           How to enable LDU
//         </a>
//         .
//       </>
//     ),
//   };
// };

// export const googleRdpQuestion = (): SurveyQuestionProps => {
//   return {
//     type: "boolean",
//     slug: "google-rdp",
//     question: "Will your business enable Restricted Data Processing (RDP) for Google Ads?",
//     helpText: (
//       <>
//         You’re not required to enable RDP. Enabling RDP will "turn off" personalized advertising via
//         Google Ads for all California visitors to your website. This may impact your advertising for
//         California web visitors, but it means California consumers will not need to click a link on
//         your website to opt-out of “sharing” with Google Ads.{" "}
//         <a
//           href="https://support.google.com/google-ads/answer/9606827#zippy=%2Cedit-your-global-site-tag-to-disable-ad-personalization-signals"
//           target="_blank"
//           rel="noreferrer noopener"
//         >
//           How to enable RDP
//         </a>{" "}
//         (be sure to update your global site tag).
//       </>
//     ),
//   };
// };

const sharingQuestions = (
  _answers: SurveyAnswers,
  vendors: OrganizationDataRecipientDto[],
): SurveyQuestionProps[] => {
  return [
    sharingInstructions(), // "sharing-definition-acknowledgement"
    shareDataQuestion(), // "share-data-for-advertising"
    vendorSharingCategories(vendors), // "vendor-sharing-categories"
    // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
    // facebookLduQuestion(), // "facebook-ldu"
    // googleRdpQuestion(), // "google-rdp"
    customAudienceFeatureQuestion(vendors), // "custom-audience-feature"
  ];
};

const sharingVisibility = (
  answers: SurveyAnswers,
  _: OrganizationDataRecipientDto[],
): Record<string, boolean> => {
  // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
  // const rawSharingCategories = answers["vendor-sharing-categories"] || "[]";
  // const sharingCategories: string[] = JSON.parse(rawSharingCategories) || [];
  // const sharingVendors = vendors.filter((v) => sharingCategories.includes(v.id));

  return {
    "share-data-for-advertising": Boolean(answers["sharing-definition-acknowledgement"]),
    "vendor-sharing-categories": answers["share-data-for-advertising"] == "true",
    "custom-audience-feature":
      answers["share-data-for-advertising"] && answers["share-data-for-advertising"].length > 0,
    // POLARIS-2512 - We are removing the LDU and RDP questions for now. Revisit this when Facebook updates Policy or in 3 Months (June 2023)
    // "facebook-ldu":
    //   answers["share-data-for-advertising"] == "true" && Boolean(findFacebookAds(sharingVendors)),
    // "google-rdp":
    //   answers["share-data-for-advertising"] == "true" && Boolean(findGoogleAds(sharingVendors)),
  };
};

export const sharingSurvey = (
  answers: SurveyAnswers,
  vendors: OrganizationDataRecipientDto[],
): Survey => {
  return {
    questions: sharingQuestions(answers, vendors),
    visibility: sharingVisibility(answers, vendors),
    disabled: {},
  };
};
