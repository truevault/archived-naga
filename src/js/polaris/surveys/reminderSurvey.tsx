import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { Survey } from "./survey";

export const REMINDER_SURVEY_NAME = "reminder-survey";

const unsafeTransfersReminderQuestion = (): SurveyQuestionProps => {
  return {
    type: "boolean",
    slug: "reminder-unsafe-transfers",
    question: "Would you like a reminder for you to complete this task?",
    helpText: `Answer "Yes" if you'd find it helpful to have a reminder about this important task. If you've already taken care of it during this onboarding, a reminder isn't necessary and you can answer "No".`,
  };
};

const reminderQuestions = (_answers: Record<string, string>): SurveyQuestionProps[] => {
  return [
    unsafeTransfersReminderQuestion(), // remind-unsafe-transfers
  ];
};

const reminderVisibility = (_: Record<string, string>): Record<string, boolean> => {
  return { "reminder-unsafe-transfers": true };
};

export const reminderSurvey = (answers: Record<string, string>): Survey => {
  return {
    questions: reminderQuestions(answers),
    visibility: reminderVisibility(answers),
    disabled: {},
  };
};
