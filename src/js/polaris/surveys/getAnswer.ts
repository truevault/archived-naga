import _ from "lodash";
import { SurveyAnswers } from "../../common/hooks/useSurvey";
import { OrganizationDataRecipientDto } from "../../common/service/server/dto/OrganizationDataRecipientDto";
import { findGoogleAnalytics } from "../util/vendors";

export const getGoogleAnalyticsDataSharing = (answers) =>
  JSON.parse(answers["google-analytics-data-sharing"] || "false");

export const getShopifyAudiences = (answers): Boolean =>
  JSON.parse(answers["shopify-audiences"] || "false");

export const onlyGaSelectedForSelling = (
  answers: SurveyAnswers,
  vendors: OrganizationDataRecipientDto[],
): boolean => {
  const googleAnalytics = findGoogleAnalytics(vendors);
  const vendorSelection = JSON.parse(answers["vendor-selling-selection"] || "null");

  return !_.isNil(googleAnalytics) && _.isEqual(vendorSelection, [googleAnalytics?.id]);
};
