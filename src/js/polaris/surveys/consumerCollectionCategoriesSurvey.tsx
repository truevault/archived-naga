import { CategoryDto, CollectionDto, DataMapDto } from "../../common/service/server/dto/DataMapDto";
import { SurveyQuestionProps } from "../components/get-compliant/survey/SurveyQuestion";
import { filterBySurveyClassification } from "../components/organisms/collection-groups/ConsumerDataCollectionForm";
import { anyCollectedForCategory, collectedForCategory } from "../util/dataInventory";
import { lexicalJoin } from "../util/text";
import { parseBoolAnswer, Survey } from "./survey";

export const UNCOMMON_QUESTION_SLUG = "uncommon-collection";
export const INFERENCES_QUESTION_SLUG = "inferences";
export const SENSITIVE_INFERENCES_QUESTION_SLUG = "inferences-from-sensitive-data";

export const consumerCollectionSurveyName = (groupId: string): string =>
  `consumer-data-collection-${groupId}`;

const uncommonCollectionQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: UNCOMMON_QUESTION_SLUG,
  disabledReason: "Uncommon collection categories are selected.",
  question:
    "Does your business collect any uncommon personal information, like physical characteristics (height, weight, eye color), or employer name?",
  helpText:
    "If you’re unsure, it’s better to select “No” for now until you’re able to confirm the answer. You can update your selection at any time.",
  visible: true,
});

const inferencesQuestion = (): SurveyQuestionProps => ({
  type: "boolean",
  slug: INFERENCES_QUESTION_SLUG,
  question:
    "Does your business make inferences about consumers based on any of the data it collects? Inferences include information, data, assumptions, or conclusions drawn from any other personal information to create a profile about a consumer reflecting their preferences, characteristics, predispositions, behavior, attitudes, intelligence, abilities, or aptitudes.",
  helpText:
    "For example, if you use characteristics like age, gender, marital status, education level, geography, etc. to predict a consumer’s purchasing preferences for purposes of marketing campaigns, answer “Yes.”",
  visible: true,
});

const sensitiveInferencesQuestion = (
  collectedSensitiveInformation: string[],
): SurveyQuestionProps => ({
  type: "boolean",
  slug: SENSITIVE_INFERENCES_QUESTION_SLUG,
  question: `Does your business infer characteristics about consumers based on ${lexicalJoin(
    collectedSensitiveInformation,
    "disjunction",
  )}?`,
  helpText:
    "Consumers have the right to limit your use of their sensitive personal information to infer characteristics about them. If you answer “Yes” we will prompt you to add a special notice to consumers in a later step.",
  visible: true,
});

const collectionQuestions = (
  collectedSensitiveInformation: CategoryDto[] = [],
): SurveyQuestionProps[] => {
  return [
    uncommonCollectionQuestion(),
    inferencesQuestion(),
    sensitiveInferencesQuestion(collectedSensitiveInformation.map((c) => c.name)),
  ];
};

const collectionVisibility = (answers: Record<string, string>) => {
  return {
    [SENSITIVE_INFERENCES_QUESTION_SLUG]: parseBoolAnswer(answers[INFERENCES_QUESTION_SLUG]),
  };
};

// Returns true, false, or reason for true
const collectionDisabled = (
  answers: Record<string, string>,
  anyUncommonCollected: boolean,
): Record<string, boolean | string> => {
  return {
    [UNCOMMON_QUESTION_SLUG]:
      anyUncommonCollected && "You have marked some uncommon data as collected",
  };
};

export const consumerCollectionSurvey = (
  answers: Record<string, string>,
  dataMap: DataMapDto,
  collected: CollectionDto,
): Survey => {
  const sensitive = filterBySurveyClassification(dataMap, "SENSITIVE");
  const uncommon = filterBySurveyClassification(dataMap, "UNCOMMON");
  const anyUncommonCollected = anyCollectedForCategory(collected, uncommon);
  const collectedSensitiveInformation = collectedForCategory(collected, sensitive);

  return {
    questions: collectionQuestions(collectedSensitiveInformation),
    visibility: collectionVisibility(answers),
    disabled: collectionDisabled(answers, anyUncommonCollected),
  };
};
