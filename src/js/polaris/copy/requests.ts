export const Error = {
  GetOrg: "Something went wrong when trying to fetch the organization. Please try again later.",
  GetOrgUsers: "Something went wrong when trying to fetch the users. Please try again later.",
  GetOrgVendors: "Something went wrong when trying to fetch the vendors. Please try again later.",
  GetTemplates:
    "Something went wrong when trying to fetch the message templates. Please try again later.",
  InviteUser: "Something went wrong when inviting the user. Please try again later.",
  DeleteUser: "Something went wrong when removing the user. Please try again later.",
  ActivateUser: "Something went wrong when removing the user. Please try again later.",
  DeactivateUser: "Something went wrong when removing the user. Please try again later.",
  GetRequests: "Something went wrong when trying to fetch the requests. Please try again later.",
  GetRequest: (id) =>
    id
      ? `Something went wrong when trying to fetch privacy request '${id}'. Please try again later.`
      : "Something went wrong when trying to fetch the privacy request. Please try again later.",
  GetDataSubjectTypes:
    "Something went wrong when trying to fetch the consumer groups. Please try again later.",
  GetRequestHandlingInstructions:
    "Something went wrong when trying to fetch request instructions. Please try again later.",
  CreateDataRequest:
    "Something went wrong when creating the privacy request. Please try again later.",
  TransitionProcessing:
    "Something went wrong when moving the privacy request to processing. Please try again later.",
  Completed:
    "Something went wrong when moving the privacy request to closed. Please try again later.",
  Reopen: "Something went wrong when reopening the privacy request. Please try again later.",
  CreateNote: "Something went wrong when trying to add a note. Please try again later.",
  SendMessage: "Something went wrong when trying to send the reply. Please try again later.",
  GetStats:
    "Something went wrong when trying to fetch the privacy request stats. Please try again later.",
  UpdateRequest:
    "Something went wrong when trying to update the privacy request. Please try again later.",
  UpdateCollectionGroup:
    "Something went wrong when trying to update your collection groups. Please try again later.",
  RemoveRequest: "Something went wrong when removing the privacy request. Please try again later",

  Login: "Invalid credentials.",
  ResetPassword:
    "Something went wrong when trying to send the password reset email. Please try again later.",
  GetWithResetToken: "We couldn't find that reset token.",
  SetPassword: "Something went wrong when trying to update the password. Please try again later.",
  AcceptInvitation:
    "Something went wrong when trying to accept the invitation. Please try again later.",
  GetVendors: "Something went wrong when trying to get the vendor catalog. Please try again later.",
  AddVendor: "Something went wrong when trying to add the vendor. Please try again later.",
  UpdateVendor: "Something went wrong when trying to update the vendor. Please try again later.",
  RemoveVendor: "Something went wrong when trying to remove the vendor. Please try again later.",
  SetActiveOrganization:
    "Something went wrong when trying to set the active organization. Please try again later.",
  TogglePlatformApp: "Something went wrong when trying to update the platform application.",
};

export const Success = {
  InviteUser: `An invitation has been sent.`,
  DeleteUser: "The invitation has been cancelled.",
  ActivateUser: "The user has been activated.",
  DeactivateUser: "The user has been deactivated.",
  CreateDataRequest: (id) =>
    id ? `Privacy request ${id} was created.` : "The privacy request was created.",
  TransitionProcessing: "The privacy request has been updated.",
  Completed: "The privacy request has been completed.",
  Reopen: "The privacy request was reopened.",
  CreateNote: "The note was added successfully.",
  SendMessage: "The message was sent successfully.",
  UpdateRequest: "The privacy request was updated.",
  RemoveRequest: "The privacy request was removed.",
  AcceptInvitation: "You've joined the organization.",
  AddVendor: "The vendor has been added to the organization.",
  UpdateVendor: "The vendor has been updated.",
  RemoveVendor:
    "Removal successful. Please note this may have caused a change to your CCPA Disclosures.",
  SetActiveOrganization: "The active organization updated successfully.",
};
