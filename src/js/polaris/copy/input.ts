export const Error = {
  Required: "Required",
  InvalidEmail: "Invalid Email",
  InvalidUrl: "Invalid URL",
  InvalidDate: "Invalid Date",
  PasswordTooShort: "Must be at least 8 characters",
  PasswordTooLong: "Must be less than 64 characters",
  MustBeUnique: "Must be unique",
};
