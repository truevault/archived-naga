export const Error = {
  InvalidCollectionSelection:
    "Unable to save; you must select at least one personal information category, source, " +
    "and business purpose.",
  UpdateInformationCollection:
    "Something went wrong when saving the information collection settings.",
  UpdateInformationDisclosure:
    "Something went wrong when saving the information disclosure settings.",
  UpdateInformationSelling: "Something went wrong when saving the information selling settings.",
};

export const Success = {
  UpdateInformationCollection: "Updated information collection settings.",
  UpdateInformationDisclosure: "Updated information disclosure settings.",
  UpdateInformationSelling: "Updated information selling settings.",
};

export const Helper = {
  SellingInformationHelpText:
    "Selling involves providing information in exchange for money or anything else of value. " +
    "It includes targeted advertising that tracks consumer behavior across websites/apps " +
    "to serve personalized ads, whether the tracking occurs directly by sharing information " +
    "with the third party, or by allowing third party cookies on your website. For more " +
    `information, see our article Do You "Sell" Personal Information?`,
  NoDataSharingDisclaimer:
    `By reclassifying this vendor as "No Data Sharing," any information sharing ` +
    "you've mapped to it will be lost. Do you still want to reclassify this vendor?",
  NoSellingDisclaimer:
    "Removing selling will remove mapping showing a sale of information to this vendor. " +
    "Your CCPA Disclosures will update automatically. Are you sure you'd like to remove selling?",
};
