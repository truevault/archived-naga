export const Error = {
  GetPrivacyCenters: "Something went wrong when fetching the privacy centers.",
  GetPrivacyCenter: "Something went wrong when fetching the privacy center.",
  GetPrivacyCenterPolicy: "Something went wrong when fetching the privacy center.",
  CreatePrivacyCenter:
    "Something went wrong when trying to create the privacy center. Please try again later.",
  UpdatePrivacyCenter:
    "Something went wrong when trying to update the privacy center. Please try again later.",
  CreatePrivacyPolicySection:
    "Something went wrong when trying to create the privacy policy section. Please try again later.",
  UpdatePrivacyPolicySection:
    "Something went wrong when trying to update the privacy policy section. Please try again later.",
  DeletePrivacyPolicySection:
    "Something went wrong when trying to delete the privacy policy section. Please try again later.",
  DefaultSubdomainCharacters: "Default subdomain may only contain alpha-numeric characters.",
};

export const Success = {
  CreatePrivacyCenter: "The privacy center was created.",
  UpdatePrivacyCenter: "The privacy center was updated.",
  UpdatePrivacyPolicy: "The privacy policy was updated.",
  CreatePrivacyPolicySection: "The section was created.",
  UpdatePrivacyPolicySection: "The section was updated.",
};
