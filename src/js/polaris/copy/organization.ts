export const Error = {
  AddDataSubjectType:
    "Something went wrong when trying to create the consumer group. Please try again later.",
  UpdateCollectionGroup:
    "Something went wrong when trying to update the consumer group. Please try again later.",
  DataSubjectTypeNameConflict: "consumer group name conflicts with an existing type.",
  GetDataSubjectTypes:
    "Something went wrong when trying to get the consumer groups. Please try again later.",
  GetProcessingSettings:
    "Something went wrong when trying to get the settings.  Please try again later.",
  GetProcessingInstructions:
    "Something went wrong when trying to get the instructions.  Please try again later.",
  RemoveDataSubjectType:
    "Something went wrong when trying to remove the consumer group. Please try again later.",
  RemoveProcessingInstruction:
    "Something went wrong when trying to remove the instruction.  Please try again later.",
  UpdateCollectionGroupSettings:
    "Something went wrong when trying to update the consumer group settings. Please try again later.",
  UpdateOrganization:
    "Something went wrong when trying to update the organization. Please try again later.",
  UpdateProcessingInstruction:
    "Something went wrong when trying to update the instruction.  Please try again later.",
  UpdatePrivacyRequestSettings:
    "Something went wrong when trying to update the privacy request settings.  Please try again later.",
  UpdateProcessingVendors:
    "Something went wrong when trying to update the processing vendors.  Please try again later.",
  UpdateVerificationInstruction:
    "Something went wrong when trying to update the verification instructions. Please try again later.",
  UpdateGetCompliantProgress:
    "Something went wrong when trying to update your progress.  Please try again later.",
  UpdateMappingProgress:
    "Something went wrong when trying to update the mapping progress. Please try again later.",
};

export const Success = {
  AddDataSubjectType: "The consumer group was created.",
  UpdateCollectionGroup: "The consumer group was updated.",
  RemoveDataSubjectType: "The consumer group was removed.",
  RemoveVendor: "The vendor was removed.",
  RemoveProcessingInstruction: "The instruction was removed.",
  UpdateCollectionGroupSettings: "The consumer group settings were updated.",
  UpdateOrganization: "The organization was updated.",
  UpdateProcessingInstruction: "The instruction was updated.",
  UpdateProcessingVendors: "The processing vendors were saved.",
  UpdateVerificationInstruction: "The verification instructions were updated.",
};
