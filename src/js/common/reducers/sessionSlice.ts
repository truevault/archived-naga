import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Session } from "../models";
import { SessionDto } from "../service/server/dto/UserDto";

type SessionState = Session | null;

const sessionSlice = createSlice({
  name: "session",
  initialState: null as SessionState,
  reducers: {
    set: (state, action: PayloadAction<SessionDto>) => {
      state = action.payload;
      return state;
    },
    purge: (state) => {
      state = null;
      return state;
    },
  },
});

export const sessionActions = sessionSlice.actions;
export const sessionReducer = sessionSlice.reducer;
