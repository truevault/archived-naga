import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { NetworkRequest } from "../models";
import { NetworkRequestState } from "../models/NetworkRequest";

type RequestKey = string;

type NetworkRequestsState = {
  [key: string]: NetworkRequestState;
};

type RequestError = {
  key: RequestKey;
  message?: string;
};

type SetRequest = {
  key: RequestKey;
  request: NetworkRequestState;
};

const request = createSlice({
  name: "request",
  initialState: {} as NetworkRequestsState,
  reducers: {
    start: (state, action: PayloadAction<RequestKey>) => {
      state[action.payload] = NetworkRequest.start();
      return state;
    },
    success: (state, action: PayloadAction<RequestKey>) => {
      state[action.payload] = NetworkRequest.success(state[action.payload]);
      return state;
    },
    error: (state, action: PayloadAction<RequestError>) => {
      state[action.payload.key] = NetworkRequest.failed(
        state[action.payload.key],
        action.payload.message || "An unknown error occurred",
      );
      return state;
    },
    set: (state, action: PayloadAction<SetRequest>) => {
      state[action.payload.key] = action.payload.request;
      return state;
    },
  },
});

export const requestActions = request.actions;
export const requestReducer = request.reducer;
