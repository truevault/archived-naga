import { combineReducers } from "redux";

import { organizationReducer } from "./organizationSlice";
import { selfReducer } from "./selfSlice";
import { sessionReducer } from "./sessionSlice";
import { requestReducer } from "./networkRequestSlice";
import { getCompliantProgressReducer } from "./getCompliantProgressSlice";
import { getCompliantInvalidatedProgressReducer } from "./getCompliantInvalidatedProgressSlice";

export const rootReducer = combineReducers({
  organizations: organizationReducer,
  self: selfReducer,
  session: sessionReducer,
  requests: requestReducer,
  getCompliantProgress: getCompliantProgressReducer,
  getCompliantInvalidatedProgress: getCompliantInvalidatedProgressReducer,
});
