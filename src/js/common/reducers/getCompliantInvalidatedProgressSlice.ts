import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { GetCompliantStep } from "../../common/service/server/Types";

const getCompliantInvalidatedProgressSlice = createSlice({
  name: "getCompliantInvalidatedProgress",
  initialState: null as GetCompliantStep | null,
  reducers: {
    set: (state, action: PayloadAction<GetCompliantStep>) => {
      state = action.payload;
      return state;
    },
    purge: (state) => {
      state = null;
      return state;
    },
  },
});

export const getCompliantInvalidatedProgressActions = getCompliantInvalidatedProgressSlice.actions;
export const getCompliantInvalidatedProgressReducer = getCompliantInvalidatedProgressSlice.reducer;
