import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { UserDetails } from "../models";

type SelfState = UserDetails | null;

const selfSlice = createSlice({
  name: "self",
  initialState: null as SelfState,
  reducers: {
    set: (state, action: PayloadAction<UserDetails>) => {
      state = action.payload;
      return state;
    },
    purge: (state) => {
      (window as any).Beacon && (window as any).Beacon("close"); // Alway close window when user is purged
      state = null;
      return state;
    },
  },
});

export const selfActions = selfSlice.actions;
export const selfReducer = selfSlice.reducer;
