import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { GetCompliantStep } from "../../common/service/server/Types";

const getCompliantProgressSlice = createSlice({
  name: "getCompliantProgress",
  initialState: null as GetCompliantStep | null,
  reducers: {
    set: (state, action: PayloadAction<GetCompliantStep | null>) => {
      state = action.payload;
      return state;
    },
    purge: (state) => {
      state = null;
      return state;
    },
  },
});

export const getCompliantProgressActions = getCompliantProgressSlice.actions;
export const getCompliantProgressReducer = getCompliantProgressSlice.reducer;
