import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Organization } from "../models";
import { OrganizationPublicId } from "../service/server/Types";

type OrganizationState = {
  [key: string]: Organization;
};

const organizationSlice = createSlice({
  name: "organization",
  initialState: {} as OrganizationState,
  reducers: {
    add: (state, action: PayloadAction<Organization | null>) => {
      if (action.payload) {
        const { id } = action.payload;
        state[id] = action.payload;
      }

      return state;
    },
    purge: (state, action: PayloadAction<OrganizationPublicId>) => {
      delete state[action.payload];
      return state;
    },
  },
});

export const organizationActions = organizationSlice.actions;
export const organizationReducer = organizationSlice.reducer;
