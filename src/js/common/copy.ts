export const Copy = {
  Requests: {
    GetSelf: {
      Error: "Something went wrong when trying to fetch the current user. Please try again later.",
    },
  },
};
