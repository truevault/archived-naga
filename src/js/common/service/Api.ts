import axios from "axios";
import qs from "qs";
import { __ALLOW_DRAFT_MODE__ } from "../../polaris/globals";

import { CollectionGroupsController } from "./server/controller/CollectionGroupsController";
import { CookieScannerController } from "./server/controller/CookieScannerController";
import { WebsiteAuditController } from "./server/controller/WebsiteAuditController";
import { DataInventoryController } from "./server/controller/DataInventoryController";
import { DataMapController } from "./server/controller/DataMapController";
import { DataRequestController } from "./server/controller/DataRequestController";
import { DataRetentionPolicyController } from "./server/controller/DataRetentionPolicyController";
import { GdprCookieConsentController } from "./server/controller/GdprCookieConsentController";
import { LoginController } from "./server/controller/LoginController";
import { OrganizationAuthController } from "./server/controller/OrganizationAuthController";
import { OrganizationController } from "./server/controller/OrganizationController";
import { OrganizationDataSourceController } from "./server/controller/OrganizationDataSourceController";
import { OrganizationProgressController } from "./server/controller/OrganizationProgressController";
import { OrganizationUserController } from "./server/controller/OrganizationUserController";
import { OrganizationVendorController } from "./server/controller/OrganizationVendorController";
import { PolarisDataController } from "./server/controller/PolarisDataController";
import { PrivacyCenterController } from "./server/controller/PrivacyCenterController";
import { PrivacyNoticeController } from "./server/controller/PrivacyNoticeController";
import { ProcessingActivityController } from "./server/controller/ProcessingActivityController";
import { PublicOrganizationController } from "./server/controller/PublicOrganizationController";
import { RequestHandlingInstructionsController } from "./server/controller/RequestHandlingInstructionsController";
import { StorageController } from "./server/controller/StorageController";
import { TaskController } from "./server/controller/TaskController";
import { UserController } from "./server/controller/UserController";
import { VendorController } from "./server/controller/VendorController";

export const stringifyParams = (params) =>
  qs.stringify(params, {
    encode: false,
  });

const axiosInstance = axios.create({
  timeout: 60000,
  withCredentials: true,
  paramsSerializer: stringifyParams,
});

export type ApiMode = "Draft" | "Live";

// NOTE: This is disabled, and will ALWAYS use the live mode unless you run with `npm run transform:dev` instead of `npm run dev`
export const setApiMode = (apiMode: ApiMode, source: string) => {
  if (__ALLOW_DRAFT_MODE__) {
    if (apiMode == "Draft" || apiMode == "Live") {
      axiosInstance.defaults.headers.common["X-Api-Mode"] = apiMode;
    } else {
      console.warn(`Unrecognized api mode (${apiMode}) set from ${source}`);
    }
  } else {
    axiosInstance.defaults.headers.common["X-Api-Mode"] = "Live";
  }
};

export const getApiMode = (): ApiMode =>
  axiosInstance.defaults.headers.common["X-Api-Mode"] as ApiMode;

setApiMode("Live", "Api");

export const setBaseURL = (baseURL) => {
  axiosInstance.defaults.baseURL = baseURL;
};

export const Api = {
  authorization: new OrganizationAuthController(axiosInstance),
  consumerGroups: new CollectionGroupsController(axiosInstance),
  dataInventory: new DataInventoryController(axiosInstance),
  dataRequest: new DataRequestController(axiosInstance),
  login: new LoginController(axiosInstance),
  organization: new OrganizationController(axiosInstance),
  organizationProgress: new OrganizationProgressController(axiosInstance),
  organizationUser: new OrganizationUserController(axiosInstance),
  organizationVendor: new OrganizationVendorController(axiosInstance),
  orgDataSource: new OrganizationDataSourceController(axiosInstance),
  polarisData: new PolarisDataController(axiosInstance),
  privacyCenter: new PrivacyCenterController(axiosInstance),
  privacyNotice: new PrivacyNoticeController(axiosInstance),
  requestHandlingInstructions: new RequestHandlingInstructionsController(axiosInstance),
  storage: new StorageController(axiosInstance),
  user: new UserController(axiosInstance),
  vendor: new VendorController(axiosInstance),
  tasks: new TaskController(axiosInstance),
  dataMap: new DataMapController(axiosInstance),
  dataRetention: new DataRetentionPolicyController(axiosInstance),
  processingActivities: new ProcessingActivityController(axiosInstance),
  cookieScanner: new CookieScannerController(axiosInstance),
  websiteAudit: new WebsiteAuditController(axiosInstance),
  gdprCookieConsent: new GdprCookieConsentController(axiosInstance),

  publicOrg: new PublicOrganizationController(axiosInstance),
};
