import { DataRequestTypeDto, REQUEST_TYPE_FILTERS } from "./dto/DataRequestTypeDto";

// ID Types
export type OrganizationPublicId = string;
export type VendorPublicId = string;
export type PrivacyCenterPublicId = string;
export type PrivacyPolicySectionPublicId = string;
export type DataRequestPublicId = string;

// DTO Types
export type Iso8601Date = string;

export type UUIDString = string;

// enums

export type UserRole = "ADMIN" | "USER";
export type GlobalRole = "ADMIN" | "USER";

export type UserStatus = "ACTIVE" | "INACTIVE" | "INVITED";

export type DataRequestType = DataRequestTypeDto;

export type DataRequestAction = "PROCESSING_NEEDED" | "PROCESSING_NOT_NEEDED";

type OldRequestStates = "NEW" | "IDENTITY_VERIFICATION";
export type DataRequestState =
  | "PENDING_EMAIL_VERIFICATION"
  | "CONSUMER_GROUP_SELECTION"
  | "PROCESSING"
  | "CLOSED"
  // new
  | "VERIFY_CONSUMER"
  | "DELETE_INFO"
  | "CONTACT_VENDORS"
  | "NOTIFY_CONSUMER"
  | "REMOVE_CONSUMER"
  | "RETRIEVE_DATA"
  | "CORRECT_DATA"
  | "EVALUATE_OBJECTION"
  | "REMOVE_CONSENT"
  | "SEND_DATA"
  | "LIMIT_DATA"
  | OldRequestStates;

export type DataRequestSubstate = "DID_NOT_PROCESS" | "UNABLE_TO_VERIFY" | "COMPLETED";

export type DataRequestFilter = "active" | "complete" | "pending";
export type RequestTypeFilter = typeof REQUEST_TYPE_FILTERS[number];

export type TasksFilter = "active" | "complete";

export type DataRequestShowType = "all" | "delete" | "know" | "optout";

export type DataRequestSubmissionMethodEnum = "Email" | "Telephone";

export type DataRequestEventType =
  | "CREATED"
  | "IDENTITY_VERIFIED"
  | "CLOSED"
  | "CLOSED_NOT_VERIFIED"
  | "CLOSED_NOT_FOUND"
  | "CLOSED_SERVICE_PROVIDER"
  | "REOPENED"
  | "MESSAGE_SENT"
  | "MESSAGE_RECEIVED"
  | "NOTE_ADDED"
  | "REQUEST_DATE_UPDATED"
  | "REQUEST_DEADLINE_UPDATED"
  | "REQUEST_UPDATED"
  | "REQUESTOR_CONTACTED";

export type VendorOutcomeOption =
  | "REVIEWED"
  | "NO_DATA"
  | "ERASED"
  | "RETAINED"
  | "NO_DATA_FOUND"
  | "DATA_ATTACHED"
  | "DATA_DUPLICATED"
  | "NO_CORRECTION"
  | "CORRECTED"
  | "NO_CHANGE"
  | "UPDATED"
  | "COMPLETED";

export type MailboxStatus = "CONNECTED" | "DISCONNECTED";

export type MailboxType = "GMAIL" | "EXCHANGE" | "UNKNOWN";

export type OrganizationMailboxStatus = "OK" | "NOT_CONNECTED";

export type LogoType = "LOGO" | "FAVICON" | "MESSAGE_HEADER";

export type StayCompliantNavItem =
  | "REQUEST_INBOX"
  | "REQUEST_INSTRUCTIONS"
  | "RECORD_LOOKUP"
  | "TASK_LIST"
  | "DATA_MAP"
  | "CONSUMER_GROUPS"
  | "DATA_RECIPIENTS"
  | "COOKIES"
  | "PRIVACY_CENTER"
  | "KNOWLEDGE_CENTER";

export type { GetCompliantStep } from "../../../polaris/surveys/steps/GetCompliantSteps";

export type MappingProgressEnum = "NOT_STARTED" | "IN_PROGRESS" | "DONE";

export type PrivacyCenterProgressEnum = "NOT_STARTED" | "IN_PROGRESS" | "DONE";

export type PrivacyNoticeProgressEnum = "NEEDS_REVIEW" | "APPROVED";

export type OptOutPageType = "NONE" | "SHARING" | "SELLING" | "SELLING_AND_SHARING";

export type ConsumerGroupDataMapEnum = "INCOMPLETE";

export type ProgressEnum =
  | MappingProgressEnum
  | PrivacyCenterProgressEnum
  | PrivacyNoticeProgressEnum
  | ConsumerGroupDataMapEnum;

export type PersonalInformationType = "CATEGORY" | "SOURCE" | "PURPOSE";

export type CustomUrlStatus = "ACTIVE" | "VERIFYING";

export type ServiceProviderRecommendataion = "SERVICE_PROVIDER" | "THIRD_PARTY" | "NONE";

export type DataRecipientType =
  | "vendor"
  | "third_party_recipient"
  | "storage_location"
  | "contractor";

export type RequestHandlingInstructionType = "KNOW" | "DELETE" | "OPT_OUT";

export type ProcessingMethod = "DELETE" | "RETAIN" | "INACCESSIBLE_OR_NOT_STORED";

export type VendorClassificationSlug =
  | "service-provider"
  | "needs-review"
  | "third-party"
  | "contractor"
  | "no-data-sharing";

export type SurveySlug = "onboarding" | "s2023_1" | "s2023_3";
