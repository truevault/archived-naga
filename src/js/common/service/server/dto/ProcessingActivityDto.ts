import { UUIDString } from "../Types";
import { ProcessingRegion } from "./OrganizationDataRecipientDto";

export type LawfulBasis =
  | "COMPLY_WITH_LEGAL_OBLIGATIONS"
  | "FULFILL_CONTRACTS"
  | "LEGITIMATE_INTERESTS"
  | "CONSENT"
  | "PUBLIC_INTEREST"
  | "VITAL_INTEREST_OF_INDIVIDUAL";

export type AutocreatedProcessingActivitySlug =
  | "AUTOMATED_DECISION_MAKING"
  | "COLD_SALES_COMMUNICATIONS";

export interface DefProcessingActivityDto {
  id: UUIDString;
  name: string;
  slug: string | undefined;
  materialIconKey: string | undefined;
  displayOrder: number | undefined;
  deprecated: boolean;
  helpText: string | undefined;
  mandatoryLawfulBases: LawfulBasis[];
  optionalLawfulBases: LawfulBasis[];
}

export interface ProcessingActivityDto {
  id: UUIDString;
  name: string;
  lawfulBases: LawfulBasis[];
  autocreationSlug: AutocreatedProcessingActivitySlug | undefined;

  isDefault: Boolean;

  defaultId: UUIDString | undefined;
  defaultSlug: string | undefined;
  defaultMandatoryLawfulBases: LawfulBasis[];
  defaultOptionalLawfulBases: LawfulBasis[];

  regions: ProcessingRegion[];

  processedPic: UUIDString[];

  _local?: LocalProcessingActivityUpdate;
}

export type LocalProcessingActivityUpdate = {
  processedPicPendingUpdates?: ProcessedPICPending[];
};

export interface CreateProcessingActivityRequest {
  name: string;
  defaultId: UUIDString | undefined;
  lawfulBases: LawfulBasis[] | undefined;
  regions: ProcessingRegion[] | undefined;
}

export interface PatchProcessingActivityRequest {
  name: string | undefined;
  lawfulBases: LawfulBasis[] | undefined;
  regions: ProcessingRegion[] | undefined;
}

// Local State

export type ProcessedPICPending = {
  type: "add" | "remove";
  id: UUIDString;
};
