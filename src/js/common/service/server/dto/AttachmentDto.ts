export interface AttachmentDto {
  name: string;
  mimeType: string;
  size: number;
  url: string;
}
