import { OrganizationPublicId, UUIDString } from "../Types";
import { DataRequestTypeDto } from "./DataRequestTypeDto";

export interface ProcessingInstructionDto {
  organizationId: OrganizationPublicId;
  dataSubjectTypeId: UUIDString;
  dataRequestType: DataRequestTypeDto | undefined;
  instruction: string;
}
