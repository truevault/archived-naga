import { UUIDString } from "../Types";

export type PICSurveyClassification =
  | "COMMON"
  | "SENSITIVE"
  | "GDPR_SENSITIVE"
  | "INDIRECT"
  | "UNCOMMON"
  | "SUPPRESSED";

export interface PersonalInformationCategoryDto {
  // pic info
  id: UUIDString;
  key?: string;
  name: string;
  description?: string;
  order: number;

  alwaysPreserveCasing: boolean;
  isSensitive: boolean;

  surveyClassification: PICSurveyClassification;

  // group info
  groupId: UUIDString;
  groupName: string;
  groupOrder: number;
  groupEmployeeOrder: number;

  // org settings
  collected: boolean;
  exchanged: boolean;
}

export interface PersonalInformationSourceDto {
  id?: UUIDString;
  name: string;
  description?: string;
  enabled: boolean;
  displayOrder: number;
  isCustom: boolean;
  category: string;
}

export interface CustomThirdPartyPersonalInformationSourceDto {
  id: UUIDString;
  name: string;
}

export interface BusinessPurposeDto {
  id: UUIDString;
  name: string;
  description?: string;
  enabled: boolean;
  displayOrder: number;
}

export interface PersonalInformationCollectedDto {
  personalInformationCategories: PersonalInformationCategoryDto[];
  personalInformationSources: PersonalInformationSourceDto[];
  businessPurposes: BusinessPurposeDto[];
  exchanged: PersonalInformationExchangedDto[];
}

export interface PersonalInformationExchangedDto {
  dataSubjectTypeId: UUIDString;
  vendorId: UUIDString;
  personalInformationCategories: PersonalInformationCategoryDto[];
  noDataMapping?: boolean;
}

export interface SnapshotCategoryDto extends PersonalInformationCategoryDto {
  vendorsExchangedWith: UUIDString[];
  vendorsSharedWith: UUIDString[];
  vendorsSoldTo: UUIDString[];
}

export interface OrgPersonalInformationSnapshotDto {
  sources: PersonalInformationSourceDto[];
  dataInventory: DataInventorySnapshotDto[];
}

export interface DataInventorySnapshotDto {
  dataSubjectTypeId: UUIDString;
  dataSubjectTypeName: string;
  personalInformationCategories: SnapshotCategoryDto[];
  businessPurposes: BusinessPurposeDto[];
  consumerGroupTypeName: string;
  consumerGroupTypeDisplayOrder: number;
}
