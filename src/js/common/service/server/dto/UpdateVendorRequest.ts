import { UUIDString } from "../Types";

export interface UpdateVendorRequest {
  vendor: UUIDString;
  outcome: string;
}

export interface ContactVendorRequest {
  vendors: UUIDString[];
}
