import { VendorDto } from "./VendorDto";

export interface VendorOutcomeEmbeddedDto {
  vendor: VendorDto;
  processingInstructions?: string;
  processed: boolean;
  flowDownProcessed: boolean;
  outcome: string;
}
