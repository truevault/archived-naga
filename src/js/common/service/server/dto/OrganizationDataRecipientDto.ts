import {
  Iso8601Date,
  MappingProgressEnum,
  OrganizationPublicId,
  ServiceProviderRecommendataion,
  UUIDString,
  DataRecipientType,
  VendorPublicId,
} from "../Types";
import { RetentionReasonDto } from "./RetentionReasonDto";
import { VendorClassificationDto, VendorContractDto, VendorContractReviewedDto } from "./VendorDto";
import { RequestHandlingInstructionEmbeddedDto } from "./RequestHandlingInstructionDto";

interface MappingProgressDto {
  value: MappingProgressEnum;
  label: string;
}

export type ProcessingRegion = "UNITED_STATES" | "EEA_UK";

export type CollectionContext = "CONSUMER" | "EMPLOYEE";

export type GDPRProcessorRecommendation = "Processor" | "Controller" | "Unknown";

export type GDPRContactUnsafeTransfer = "CONTACTED" | "UNSAFE";
export type GDPRContactProcessorStatus = "CONTACTED" | "CONTROLLER";
export type GDPREnsureSccInPlace = "IN_PLACE" | "STOP_USING";
export type DataAccessibility = "ACCESSIBLE" | "NOT_ACCESSIBLE" | "UNKNOWN";
export type DataDeletability = "DELETABLE" | "NOT_DELETABLE" | "UNKNOWN";

export interface OrganizationDataRecipientDto {
  id: string;
  vendorId: UUIDString;
  name: string;
  vendorKey?: string;
  aliases?: string[];
  category: string;
  url: string;
  email: string;
  defaultEmail: string | null;
  logoUrl: string;
  ccpaRequestToDeleteLink?: string;
  ccpaRequestToKnowLink?: string;
  dateAdded: Iso8601Date;
  dataRecipientType: DataRecipientType;
  defaultClassification?: VendorClassificationDto;
  classification: VendorClassificationDto;
  classificationOptions: VendorClassificationDto[];
  vendorContract?: VendorContractDto;
  vendorContractOptions: VendorContractDto[];
  vendorContractReviewed?: VendorContractReviewedDto;
  vendorContractReviewedOptions: VendorContractReviewedDto[];
  organization: OrganizationPublicId;
  organizationName: string;
  status: string;
  publicTosUrl?: string;
  ccpaIsSelling: boolean;
  ccpaIsSharing: boolean;
  usesCustomAudience: boolean;
  tosFileName?: string;
  tosFileKey?: string;
  isCustom: boolean;
  isAccessible: boolean;
  isStandalone: boolean;
  isPlatform: boolean;
  isInstalled: boolean;
  retentionReasons: RetentionReasonDto[];
  contacted: boolean | null;
  serviceProviderRecommendation: ServiceProviderRecommendataion;
  serviceProviderLanguageUrl: string;
  dpaApplicable: boolean;
  dpaUrl: string;
  mappingProgress: MappingProgressDto;
  dataAccessibility: DataAccessibility;
  dataDeletability: DataDeletability;
  automaticallyClassified?: boolean;
  isDataStorage?: boolean;
  dataMapped?: boolean;
  instructions: RequestHandlingInstructionEmbeddedDto[];
  deletionInstructions: string | undefined;
  accessInstructions: string | undefined;
  optOutInstructions: string | undefined;
  generalGuidance: string;
  specialCircumstancesGuidance: string;
  recommendedPersonalInformationCategories: string[];
  respectsPlatformRequests: boolean;

  gdprProcessorRecommendation: GDPRProcessorRecommendation | null;
  gdprProcessorLanguageUrl: string | null;
  gdprInternationalDataTransferRulesApply?: boolean;
  gdprHasSccRecommendation: boolean;
  gdprSccDocumentationUrl: string | null;
  gdprContactUnsafeTransfer?: GDPRContactUnsafeTransfer;
  gdprEnsureSccInPlace?: GDPREnsureSccInPlace;
  gdprContactProcessorStatus?: GDPRContactProcessorStatus;

  gdprProcessorSetting: GDPRProcessorRecommendation | null;
  isGdprProcessorSettingLocked: boolean;
  gdprProcessorGuaranteeUrl?: string;
  gdprProcessorGuaranteeFileName?: string;
  gdprProcessorGuaranteeFileKey?: string;
  gdprControllerGuaranteeUrl?: string;
  gdprControllerGuaranteeFileName?: string;
  gdprControllerGuaranteeFileKey?: string;

  gdprHasSccSetting: boolean | null;
  gdprSccUrl: string | null;
  gdprSccFileName: string | null;
  gdprSccFileKey: string | null;
  gdprHasSccs: boolean;
  gdprNeedsSccs: boolean;
  gdprHasUserProvidedSccs: boolean;

  removedFromExceptionsToScc: boolean;
  isExceptedFromSccs: boolean;
  isPotentiallyExceptedFromSccs: boolean;

  completed: boolean;

  disclosedToByEeaUkGroup: boolean;
  disclosedToByUsGroup: boolean;

  defaultRetentionReasons: string[];

  processingRegions: ProcessingRegion[];
  collectionContext: CollectionContext[];
  isPotentialIntentionalInteractionVendor: boolean;
  isAutomaticIntentionalInteractionVendor: boolean;
  isPotentialSellingVendor: boolean;
  isAutomaticSellingVendor: boolean;
  isPotentialUploadVendor: boolean;
  isAutomaticUploadVendor: boolean;
  isUploadVendor: boolean;
}

export interface OrganizationDataRecipientPlatformDto {
  id: UUIDString;
  vendorId: VendorPublicId;
  vendorKey: string | null;
  name: string;
  logoUrl: string;
  available: OrganizationDataRecipientDto[];
  installed: UUIDString[];
}

export interface OrganizationVendorSettingsDto {
  contacted?: boolean;
  classificationId: UUIDString;
  vendorContractId?: UUIDString;
  contractReviewedId?: UUIDString;
  vendorContractReviewedId?: UUIDString;
  ccpaIsSelling?: boolean;
  ccpaIsSharing?: boolean;
  automaticallyClassified?: boolean;
  isDataStorage?: boolean;
  name?: string | null;
  category?: string | null;
  url?: string | null;
  email?: string | null;
  publicTosUrl?: string | null;
  tosFileName?: string | null;
  tosFileKey?: string | null;
  processingRegions?: ProcessingRegion[];
  collectionContext?: CollectionContext[];
  dataAccessibility?: DataAccessibility;
  dataDeletability?: DataDeletability;
  gdprProcessorSetting?: GDPRProcessorRecommendation | null;
  gdprProcessorGuaranteeUrl?: string | null;
  gdprProcessorGuaranteeFileName?: string | null;
  gdprProcessorGuaranteeFileKey?: string | null;
  gdprControllerGuaranteeUrl?: string | null;
  gdprControllerGuaranteeFileName?: string | null;
  gdprControllerGuaranteeFileKey?: string | null;

  gdprContactUnsafeTransfer?: GDPRContactUnsafeTransfer;
  gdprEnsureSccInPlace?: GDPREnsureSccInPlace;
  gdprContactProcessorStatus?: GDPRContactProcessorStatus;
  gdprHasSccSetting: boolean | null;
  gdprSccUrl: string | null;
  gdprSccFileName: string | null;
  gdprSccFileKey: string | null;

  removedFromExceptionsToScc: boolean | null;

  completed: boolean | null;
}

export interface OrganizationVendorNewAndEditDto {
  vendorContractOptions: VendorContractDto[];
  vendorContractReviewedOptions: VendorContractReviewedDto[];
  classificationOptions: VendorClassificationDto[];
  existingCategories: string[];
  vendor?: OrganizationDataRecipientDto;
}

export interface AddOrUpdateCustomVendorDto {
  name: string;
  category: string;
  url?: string;
  email?: string;
  classificationId?: UUIDString;
  ccpaIsSelling?: boolean;
  publicTosUrl?: string;
  dataAccessibility?: DataAccessibility;
  dataRecipientType?: DataRecipientType;
  collectionContext?: CollectionContext[];
  completed?: boolean;
}

export interface RemoveVendorsDto {
  vendors: UUIDString[];
}

export interface OrganizationDataSourceDto {
  vendorId: UUIDString;
  name: string;
  vendorKey?: string;
  aliases?: string[];
  category?: string;
  url?: string;
  email?: string;
  logoUrl?: string;
  isCustom: boolean;
  collectionContext: CollectionContext[];
}

export interface AddOrganizationDataSourceDto {
  vendorId?: UUIDString;
  custom?: AddCustomOrganizationDataSourceDto;
  collectionContext?: CollectionContext[];
}

export interface AddCustomOrganizationDataSourceDto {
  name: string;
  category: string;
  contactEmail?: string;
  url?: string;
}
