export interface OrganizationFeatureDto {
  name: string;
  description: string;
  enabled: boolean;
}
