import { DataRequestEventType, Iso8601Date } from "../Types";
import { AttachmentDto } from "./AttachmentDto";
import { UserDto } from "./UserDto";

export interface DataSubjectRequestEventEmbeddedDto {
  eventId: number;
  actor?: UserDto;
  eventType: DataRequestEventType;
  eventTypeTitle: string;
  eventTypeDescription: string;
  eventDate: Iso8601Date;
  isUserFacingEvent: boolean;
  isRequesterAction: boolean;
  message?: string;
  attachments: AttachmentDto[];
  createdAt: Iso8601Date;
}
