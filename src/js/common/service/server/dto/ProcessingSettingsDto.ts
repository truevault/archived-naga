import { ProcessingInstructionDto } from "./ProcessingInstructionDto";
import { OrganizationDataRecipientDto } from "./OrganizationDataRecipientDto";

export interface ProcessingSettingsDto {
  processingInstructions: ProcessingInstructionDto[];
  organizationVendors: OrganizationDataRecipientDto[];
}
