import { OrganizationPublicId } from "../Types";

export interface OrganizationComplianceChecklistItemDto {
  organizationId: OrganizationPublicId;
  slug: string;
  description?: string;
  done?: boolean;
}
