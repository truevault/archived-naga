import { UUIDString } from "../Types";

export type DataRetentionPolicyDto = {
  detail: DataRetentionPolicyCategoryDto[];
};

export type DataRetentionPolicyCategoryDto = {
  category: DataRetentionPolicyDetailCategory;
  description: string;
  pic: UUIDString[];
};

export type UpdateDataRetentionPolicyDetailDto = {
  pic: UUIDString[];
  description: string;
};

export type DataRetentionPolicyDetailCategory =
  | "ONLINE_DATA_FROM_WEBSITE"
  | "PROCESS_AND_SHIP_ORDERS"
  | "CONTACT_CUSTOMER_SUPPORT"
  | "SIGN_UP_FOR_PROMOTIONAL_AND_MARKETING"
  | "REVIEW_PRODUCTS_ANSWER_SURVEYS_SEND_FEEDBACK"
  | "PRIVACY_REQUESTS"
  | "SECURITY_PURPOSES";
