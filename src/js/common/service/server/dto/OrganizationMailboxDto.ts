import { MailboxStatus, MailboxType } from "../Types";

export interface OrganizationMailboxDto {
  status: MailboxStatus;
  type: MailboxType;
  mailbox: string;
  headerImageUrl: string;
  physicalAddress: string;
  messageSignature: string;
}
