import { OrganizationPublicId, UUIDString } from "../Types";

export type CookieScanProgress = "NOT_STARTED" | "IN_PROGRESS" | "COMPLETE" | "RESET" | "ERROR";
export type CookieCategory =
  | "ANALYTICS"
  | "FUNCTIONALITY"
  | "ADVERTISING"
  | "PERSONALIZATION"
  | "SECURITY";

export type CookieRecommendationCategory = CookieCategory;

export type CookieRecommendationDto = {
  id: UUIDString;
  name?: string;
  domain?: string;
  categories: CookieCategory[];
  vendorId?: UUIDString;
  notes?: string;
};

export type CookieScanResultDto = {
  organizationId: OrganizationPublicId;
  status: CookieScanProgress;
  scanUrl: string | undefined;
  cookies: OrganizationCookieDto[] | undefined;
  scanStartedAt: string | undefined;
  scanEndedAt: string | undefined;
  error: string | undefined;
};

export type OrganizationCookieDto = {
  // Scan results
  name: string;
  domain: string;
  value: string;
  expires: string;
  httpOnly: string;
  secure: string;
  lastFetched: string;

  // Polaris information
  categories: CookieCategory[];
  dataRecipients: UUIDString[];
  hidden: boolean;
  firstParty: boolean;
};

export type UpdateCookieRequest = {
  categories: CookieCategory[];
  dataRecipients: UUIDString[];
  hidden: boolean;
  firstParty: boolean;
};
