import { UUIDString } from "../Types";

export interface ThirdPartyDataRecipientsDto {
  recipients: ThirdPartyDataRecipientDto[];
}

export interface ThirdPartyDataRecipientDto {
  id?: UUIDString;
  name: string;
  category: string;
}
