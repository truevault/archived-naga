import { Iso8601Date } from "../Types";
import { PaginatedDto } from "./PaginatedDto";

export interface GdprCookieConsentDto {
  visitorId: string;
  consentGranted: GdprCookieConsentCategory[];
  date: Iso8601Date;
}

export type PaginatedGdprCookieConsentDto = PaginatedDto<GdprCookieConsentDto>;

export type GdprCookieConsentCategory = "ANALYTICS" | "PERSONALIZATION" | "ADS" | "ESSENTIAL";
