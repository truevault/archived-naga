import {
  OrganizationPublicId,
  MappingProgressEnum,
  ProcessingMethod,
  RequestHandlingInstructionType,
  UUIDString,
} from "../Types";

export interface RequestHandlingInstructionDto {
  id?: UUIDString;
  organizationId: OrganizationPublicId;
  requestType: RequestHandlingInstructionType;
  vendorId?: UUIDString; // only present on KNOW/DELETE
  processingMethod?: ProcessingMethod; // only present on DELETE and some KNOW (not stored)
  retentionReasons?: string[]; // only present on DELETE
  processingInstructions?: string;
  deleteDoNotContact?: boolean;
  doNotContactReason?: DoNotContactReason;
  doNotContactExplanation?: string;
  exceptionsAffirmed?: boolean;
  deletedPIC?: string[]; // only present on DELETE request with RETAIN method
  hasRetentionExceptions?: boolean; // theoretically only present on DELETE request with RETAIN method
}

export type DoNotContactReason =
  | "CONTACT_VIA_OTHER_MEANS"
  | "IMPOSSIBLE_OR_DIFFICULT"
  | "SELF_SERVICE_DELETION";

export interface RequestHandlingInstructionEmbeddedDto {
  requestType: RequestHandlingInstructionType;
  processingMethod?: string;
  processingInstructions?: string;
  deleteDoNotContact?: boolean;
  hasRetentionExceptions?: boolean;
  doNotContactReason?: DoNotContactReason;
  doNotContactExplanation?: string;
}

export interface UpdateOrganizationCGInstructionsDto {
  organizationId: OrganizationPublicId;
  cgRequestHandlingInstructions: string;
}

export interface RequestHandlingInstructionProgressDto {
  organizationId: OrganizationPublicId;

  collectionGroupProgress: MappingProgressEnum;
  knowProgress: MappingProgressEnum;
  deleteProgress: MappingProgressEnum;
  optOutProgress: MappingProgressEnum;
}
