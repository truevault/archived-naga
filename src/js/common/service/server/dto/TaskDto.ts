import { Iso8601Date, UUIDString } from "../Types";
import { UserDto } from "./UserDto";

export type TaskKey =
  | "CLASSIFY_VENDORS"
  | "UPLOAD_EMAIL_HEADER_LOGO"
  | "ADD_DPO"
  | "AUTHORIZE_GOOGLE_ANALYTICS"
  | "COMPLETE_FINANCIAL_INCENTIVES"
  | "UNSAFE_TRANSFERS_REMINDER"
  | "COMPLETE_DATA_RECIPIENTS"
  | "ADD_EMPLOYMENT_DATA_RECIPIENTS"
  | "MAP_EMPLOYMENT_GROUP"
  | "UPDATE_GOOGLE_ANALYTICS_ID"
  | "WEBSITE_AUDIT_CA_NOTICE_TASK"
  | "WEBSITE_AUDIT_OPT_OUT_LINK_TASK"
  | "WEBSITE_AUDIT_OPT_OUT_LINK_UPGRADE_TASK"
  | "WEBSITE_AUDIT_POLARIS_JS_TASK"
  | "WEBSITE_AUDIT_PRIVACY_LINK_TASK";

export type TaskCompletionType = "MANUAL" | "AUTOMATIC";

export interface TaskDto {
  id: UUIDString;
  taskKey: TaskKey;
  name: string;
  description: string;
  completionType: TaskCompletionType;
  createdAt: Iso8601Date;
  dueAt?: Iso8601Date;
  completedAt?: Iso8601Date;
  completedBy?: UserDto;
  callToAction?: string;
  callToActionUrl?: string;
}
