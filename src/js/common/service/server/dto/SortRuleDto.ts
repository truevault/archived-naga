export type SortRuleDto = {
  id: string;
  desc?: boolean | undefined;
};

export type SortRule = SortRuleDto;
