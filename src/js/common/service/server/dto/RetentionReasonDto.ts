import { UUIDString } from "../Types";

export type RetentionReason = string;

export interface RetentionReasonDto {
  id: UUIDString;
  reason: RetentionReason;
  slug: string;
  description: string;
  displayOrder: number;
}
