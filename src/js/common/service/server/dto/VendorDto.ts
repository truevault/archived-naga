import {
  UUIDString,
  OrganizationPublicId,
  ServiceProviderRecommendataion,
  DataRecipientType,
  VendorClassificationSlug,
} from "../Types";

export interface VendorClassificationDto {
  id: UUIDString;
  name: string;
  slug: VendorClassificationSlug;
  displayOrder: number;
  tooltip: string;
}

export interface VendorContractDto {
  id: UUIDString;
  name: string;
  slug: string;
  displayOrder: number;
  tooltip: string;
  contractUrl?: string;
}

export interface VendorContractReviewedDto {
  id: UUIDString;
  name: string;
  slug: string;
  displayOrder: number;
  tooltip: string;
}

export interface VendorDto {
  id: UUIDString;
  name: string;
  vendorKey?: string;
  aliases?: string[];
  category: string;
  sourceCategory?: string;
  url: string;
  logoUrl: string;
  ccpaRequestToDeleteLink?: string;
  ccpaRequestToKnowLink?: string;
  defaultVendorClassification?: VendorClassificationDto;
  organizationId: OrganizationPublicId;
  email: string;
  serviceProviderRecommendation: ServiceProviderRecommendataion;
  serviceProviderLanguageUrl: string;
  dpaApplicable: boolean;
  dpaUrl: string;
  dataRecipientType: DataRecipientType;
  isCustom: boolean;
  isDataRecipient: boolean;
  isDataSource: boolean;
  isStandalone: boolean | null; // Null means "unset" or "assume false"
  isPlatform: boolean | null; // Null means "unset" or "assume false"
  respectsPlatformRequests: boolean;
  defaultRetentionReasons: string[];
  isPotentialIntentionalInteractionVendor: boolean;
  isAutomaticIntentionalInteractionVendor: boolean;
}
