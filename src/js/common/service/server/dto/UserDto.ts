import {
  Iso8601Date,
  OrganizationPublicId,
  UserRole,
  GlobalRole,
  UserStatus,
  UUIDString,
} from "../Types";
import { Phase } from "../../../../polaris/util/Phase";

export interface UserDto {
  email: string;
  firstName?: string;
  lastName?: string;
  lastLogin?: Iso8601Date;
  globalRole?: GlobalRole;
}

export interface UserDetailsDto {
  id: UUIDString;
  email: string;
  firstName?: string;
  lastName?: string;
  lastLogin: Iso8601Date;
  organizations: OrganizationUserDetailsDto[];
  invitedOrganizations: OrganizationUserDetailsDto[];
  beaconSignature?: string;
  productBoardJwt?: string;
  globalRole?: GlobalRole;
  adminSite?: string;
  activeOrganization?: OrganizationPublicId;
  activeOrganizationPhase?: Phase;
}

export interface SessionDto {
  secondsRemaining: number;
}

export interface OrganizationUserDto {
  firstName: string;
  lastName: string;
  email: string;
  lastLogin?: Iso8601Date;
  role: UserRole;
  status: UserStatus;
  organization: OrganizationPublicId;
  organizationName: string;
}

export interface OrganizationUserDetailsDto {
  role: UserRole;
  status: UserStatus;
  organization: OrganizationPublicId;
  organizationName: string;
  faviconUrl?: string;
}
