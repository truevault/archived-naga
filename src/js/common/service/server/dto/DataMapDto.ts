import _ from "lodash";
import { useMemo } from "react";
import { applyOperations } from "../../../../polaris/hooks/useDataMap";
import { categorySorter } from "../../../../polaris/util/dataInventory";
import { CollectionGroupType } from "../controller/CollectionGroupsController";
import { UUIDString } from "../Types";
import { CollectionGroupDetailsDto } from "./CollectionGroupDto";
import {
  CollectionContext,
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "./OrganizationDataRecipientDto";
import { PICSurveyClassification } from "./PersonalInformationOptionsDto";

export type V2DataMapDto = {
  categories: Record<UUIDString, CategoryDto>;
  collectionGroups: Record<UUIDString, DMCollectionGroupDto>;
  recipients: Record<UUIDString, DMDataRecipientDto>;
  sources: Record<UUIDString, DMDataSourceDto>;
  collection: V2CollectionDto[];
  disclosure: V2DisclosureDto[];
  received: V2ReceivedDto[];
  recipientAssociations: V2RecipientAssociationDto[];
  sourceAssociations: V2SourceAssociationDto[];

  _local?: LocalDataMapPendingUpdates;
};

type LocalDataMapPendingUpdates = {
  operations?: DataMapOperation[];
};

export type V2CollectionDto = {
  groupId: UUIDString;
  picId: UUIDString;
};
export type V2DisclosureDto = {
  groupId: UUIDString;
  vendorId: UUIDString;
  picId: UUIDString;
};
export type V2ReceivedDto = {
  groupId: UUIDString;
  vendorId: UUIDString;
  picId: UUIDString;
};

export type V2RecipientAssociationDto = {
  groupId: UUIDString;
  vendorId: UUIDString;
};
export type V2SourceAssociationDto = {
  groupId: UUIDString;
  vendorId: UUIDString;
};

export type DMCollectionGroupDto = {
  id: UUIDString;
  name: string;
  description: string | null;
  type: CollectionGroupType;
};

export type DMDataRecipientDto = {
  vendorId: UUIDString;
  name: string;
  category: string;
};

export type DMDataSourceDto = {
  vendorId: UUIDString;
  name: string;
  category: string;
  context: CollectionContext[];
  displayOrder: number;
};

export type DataMapTarget =
  | "COLLECTION"
  | "DISCLOSURE"
  | "RECEIVED"
  | "RECIPIENT_ASSOCIATION"
  | "SOURCE_ASSOCIATION";
export type DataMapUpdate = "ADD" | "REMOVE";

export type DataMapCollectionUpdateOperation = {
  target: "COLLECTION";
  update: DataMapUpdate;
  groupId: UUIDString;
  picId: UUIDString;
};

export type DataMapDisclosureUpdateOperation = {
  target: "DISCLOSURE";
  update: DataMapUpdate;
  groupId: UUIDString;
  picGroupId: UUIDString;
  vendorId: UUIDString;
};

export type DataMapReceivedUpdateOperation = {
  target: "RECEIVED";
  update: DataMapUpdate;
  groupId: UUIDString;
  picId: UUIDString;
  vendorId: UUIDString;
};

export type DataMapRecipientAssociationUpdateOperation = {
  target: "RECIPIENT_ASSOCIATION";
  update: DataMapUpdate;
  groupId: UUIDString;
  vendorId: UUIDString;
};

export type DataMapSourceAssociationUpdateOperation = {
  target: "SOURCE_ASSOCIATION";
  update: DataMapUpdate;
  groupId: UUIDString;
  vendorId: UUIDString;
};

export type DataMapOperation =
  | DataMapCollectionUpdateOperation
  | DataMapDisclosureUpdateOperation
  | DataMapReceivedUpdateOperation
  | DataMapRecipientAssociationUpdateOperation
  | DataMapSourceAssociationUpdateOperation;

export type UpdateDataMapRequest = { operations: DataMapOperation[] };

export const transformV2ToV1DataMap = (v2: V2DataMapDto): DataMapDto => {
  // make a copy and apply the local operations
  v2 = _.cloneDeep(v2);
  applyOperations(v2._local?.operations ?? [], v2);

  const collectionGrouped = _.groupBy(v2.collection, (c) => c.groupId);
  const recipientAssociationsGrouped = _.groupBy(v2.recipientAssociations, (c) => c.groupId);
  const sourceAssociationsGrouped = _.groupBy(v2.sourceAssociations, (c) => c.groupId);

  return {
    categories: Object.values(v2.categories),
    collection: Object.keys(collectionGrouped).map((groupId) => {
      const group = v2.collectionGroups[groupId];
      const collected = collectionGrouped[groupId].map((g) => g.picId);

      return {
        collectionGroupId: groupId,
        collectionGroupName: group.name,
        collectionGroupType: group.type,
        collected,
      };
    }),
    disclosure: Object.keys(recipientAssociationsGrouped).flatMap((groupId) => {
      const group = v2.collectionGroups[groupId];
      const associatedVendorIds = recipientAssociationsGrouped[groupId].map((a) => a.vendorId);

      return associatedVendorIds.map((vendorId) => {
        const recipient = v2.recipients[vendorId];
        const disclosed = v2.disclosure
          .filter((d) => d.groupId == groupId && d.vendorId == vendorId)
          .map((d) => d.picId);

        return {
          collectionGroupId: groupId,
          collectionGroupName: group.name,
          vendorId: vendorId,
          vendorName: recipient.name,
          vendorCategory: recipient.category,
          disclosed,
        };
      });
    }),
    received: Object.keys(v2.collectionGroups).flatMap((groupId) => {
      const group = v2.collectionGroups[groupId];
      const associatedVendorIds = sourceAssociationsGrouped[groupId]?.map((a) => a.vendorId) ?? [];
      const receivedVendorIds = _.uniq(
        v2.received.filter((r) => r.groupId == groupId).map((r) => r.vendorId),
      );

      const vendorIds = _.uniq([...associatedVendorIds, ...receivedVendorIds]);

      return vendorIds
        .map((vendorId) => {
          const source = v2.sources[vendorId];
          const received = v2.received
            .filter((d) => d.groupId == groupId && d.vendorId == vendorId)
            .map((d) => d.picId);

          if (!source) {
            return null;
          }

          return {
            collectionGroupId: groupId,
            collectionGroupName: group.name,
            vendorId: vendorId,
            vendorName: source.name,
            vendorCategory: source.category,
            received,
          };
        })
        .filter((x) => x) as SourceDto[];
    }),
  };
};

export type CategoryDto = {
  id: UUIDString;
  key: string | undefined;
  order: number;

  name: string;
  description: String | undefined;

  isSensitive: boolean;
  alwaysPreserveCasing: boolean;

  surveyClassification: PICSurveyClassification;

  // group info
  picGroupId: UUIDString;
  picGroupName: string;
  picGroupOrder: number;
  picGroupEmployeeOrder: number;
  picGroupKey: string | undefined;
};

export type CategoryGroupDtoMap = {
  picGroupId: UUIDString;
  picGroupName: string;
  picGroupOrder: number;
  picGroupEmployeeOrder: number;
  picGroupKey: string | undefined;
  categories: CategoryDto[];
};

export type DisclosureDto = {
  collectionGroupId: UUIDString;
  collectionGroupName: string;
  vendorId: UUIDString;
  vendorName: string;
  vendorCategory: string;
  disclosed: UUIDString[];
};

export type SourceDto = {
  collectionGroupId: UUIDString;
  collectionGroupName: string;
  vendorId: UUIDString;
  vendorName: string;
  received: UUIDString[];
};

export type CollectionDto = {
  collectionGroupId: UUIDString;
  collectionGroupName: string;
  collectionGroupType: CollectionGroupType;
  collected: UUIDString[];
};

export type DataMapDto = {
  categories: CategoryDto[];
  collection: CollectionDto[];
  disclosure: DisclosureDto[];
  received: SourceDto[];
};

export type PICGroup = {
  id: string;
  name: string;
  order: number;
  employmentOrder: number;
  key: string;
  categories: CategoryDto[];
};

export const groupsFromPIC = (pic: CategoryDto[]): PICGroup[] => {
  const grouped = _.groupBy(pic, "picGroupId");

  return _.sortBy(
    _.map(grouped, (c: CategoryDto[]) => ({
      id: _.get(c, "0.picGroupId"),
      name: _.get(c, "0.picGroupName"),
      order: _.get(c, "0.picGroupOrder"),
      employmentOrder: _.get(c, "0.picGroupEmployeeOrder"),
      key: _.get(c, "0.picGroupKey"),
      categories: c.sort(categorySorter),
    })),
    ["order", "name"],
  );
};

type PicRecommendations = {
  recommendedPic: CategoryDto[];
  recommendedGroups: PICGroup[];
};

export const recommendationsForDataRecipient = (
  dataMap: DataMapDto,
  dataRecipient: OrganizationDataRecipientDto,
): PicRecommendations => {
  const recommendedPic = dataMap.categories.filter((c) =>
    dataRecipient.recommendedPersonalInformationCategories.includes(c.id),
  );

  const recommendedGroups = groupsFromPIC(recommendedPic);

  return { recommendedPic, recommendedGroups };
};

type DataMapSourcedDetails = {
  sourceDetails: SourceDto;
  sourcedPic: CategoryDto[];
  sourcedPicGroups: PICGroup[];
};
export const useSourcedDetails = (
  dataMap: DataMapDto,
  collectionGroup: CollectionGroupDetailsDto,
  dataRecipient: OrganizationDataSourceDto,
): DataMapSourcedDetails => {
  const sourceDetails = useMemo(
    () =>
      dataMap.received.find(
        (c) => c.collectionGroupId === collectionGroup.id && c.vendorId === dataRecipient.vendorId,
      ),
    [dataMap, collectionGroup, dataRecipient],
  );

  const sourcedPic = useMemo(
    () => dataMap.categories.filter((c) => sourceDetails?.received.includes(c.id) ?? false),
    [dataMap, sourceDetails],
  );

  const sourcedPicGroups = useMemo(() => groupsFromPIC(sourcedPic), [sourcedPic]);

  // @ts-ignore: this is getting completely overhauled in the Data Map refactor that's open.
  return { sourceDetails, sourcedPic, sourcedPicGroups };
};
