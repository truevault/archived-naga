import { OrganizationPublicId } from "../Types";

export type WebsiteAuditProgress = "NOT_STARTED" | "IN_PROGRESS" | "COMPLETE" | "RESET" | "ERROR";

export type WebsiteAuditResultDto = {
  organizationId: OrganizationPublicId;
  status: WebsiteAuditProgress;
  scanUrl: string | undefined;
  scanStartedAt: string | undefined;
  scanEndedAt: string | undefined;
  error: string | undefined;

  privacyPolicyLinkTextValid: boolean | null;
  privacyPolicyLinkUrlValid: boolean | null;

  caPrivacyNoticeLinkTextValid: boolean | null;
  caPrivacyNoticeLinkUrlValid: boolean | null;

  optOutLinkTextValid: boolean | null;
  optOutLinkUrlValid: boolean | null;
  optOutLinkYourPrivacyChoicesIcon: boolean | null;

  polarisJsSetup: boolean | null;
  polarisJsBeforeGoogleAnalytics: boolean | null;
  polarisJsNoDefer: boolean | null;
};
