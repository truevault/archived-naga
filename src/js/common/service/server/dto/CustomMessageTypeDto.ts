export interface CustomMessageTypeDto {
  slug: string;
  title: string;
  description: string;
  instructions: string;
  sortKey: number;
}
