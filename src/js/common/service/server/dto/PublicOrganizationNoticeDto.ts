import { OrganizationCookieDto } from "./CookieScanDto";

export interface PublicOrganizationNoticeDto {
  type: "dev" | "hr";
  privacyCenter: PublicPrivacyCenterDto;
  isGoogleAnalytics: boolean;
  isGdpr: boolean;
  isSharing: boolean;
  isSelling: boolean;
  showFinancialIncentive: boolean;
  showLimitNotice: boolean;
  hasJobApplicants: boolean;

  hasPhysicalLocation: boolean;
  hasMobileApplication: boolean;

  employmentGroupNotices: EmploymentGroupNotices[];
  cookies: OrganizationCookieDto[];
  cookieScanUrl?: string;
}

export interface EmploymentGroupNotices {
  name: string;
  url: string;
}

export interface PublicPrivacyCenterDto {
  url: string;
  id: string;
}
