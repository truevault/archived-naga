import { CollectionGroupType } from "../controller/CollectionGroupsController";
import {
  MappingProgressEnum,
  PersonalInformationType,
  PrivacyNoticeProgressEnum,
  UUIDString,
} from "../Types";
import { ProcessingRegion } from "./OrganizationDataRecipientDto";
import { BusinessPurposeDto } from "./PersonalInformationOptionsDto";
import { RetentionReasonDto } from "./RetentionReasonDto";

interface PrivacyNoticeProgress {
  value: PrivacyNoticeProgressEnum;
  label: string;
}

export type CollectionGroupDto = string;
export type AutocreatedCollectionGroupSlug =
  | "ONLINE_SHOPPERS"
  | "B2B_PROSPECTS"
  | "CA_EMPLOYEES"
  | "CA_CONTRACTORS"
  | "CA_JOB_APPLICANTS"
  | "EEA_UK_EMPLOYEE"
  | "BUSINESS_CONTACTS";

export interface CollectionGroupMappingProgressDto {
  personalInformationType: PersonalInformationType;
  value: MappingProgressEnum;
  label: string;
}

export interface CollectionGroupRetentionDto {
  consumerGroupId: UUIDString;
  vendorId?: UUIDString;
  retentionReasons: RetentionReasonDto[];
}

export interface CollectionGroupDataRecipientDto {
  consumerGroupId: UUIDString;
  vendorId: UUIDString;
  mappingProgress: MappingProgressEnum;
}

export interface CollectionGroupDetailsDto {
  id: UUIDString;
  name: string;
  description: string | null;
  inUse: boolean;
  enabled: boolean;
  collectionGroupType: CollectionGroupType;
  autocreationSlug: AutocreatedCollectionGroupSlug;
  mappingProgress: CollectionGroupMappingProgressDto[];
  privacyNoticeProgress: PrivacyNoticeProgress;
  parentType?: unknown; // TODO: When this actually exists in the codebase set the type;
  gdprEeaUkDataCollection: boolean | null;
  createdAsDataSubject: boolean;
  processingRegions: ProcessingRegion[];
  businessPurposes: BusinessPurposeDto[];
  privacyNoticeIntroText?: string;
}
