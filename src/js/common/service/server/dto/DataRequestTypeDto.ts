export const DATA_REQUEST_TYPES = [
  "GDPR_ACCESS",
  "GDPR_ERASURE",
  "GDPR_OBJECTION",
  "GDPR_RECTIFICATION",
  "GDPR_WITHDRAWAL",
  "GDPR_PORTABILITY",
  "CCPA_OPT_OUT",
  "CCPA_RIGHT_TO_KNOW_CATEGORIES",
  "CCPA_RIGHT_TO_DELETE",
  "CCPA_RIGHT_TO_KNOW",
  "CCPA_RIGHT_TO_LIMIT",
  "CCPA_CORRECTION",
  "VCDPA_ACCESS",
  "VCDPA_DELETION",
  "VCDPA_CORRECTION",
  "VCDPA_APPEAL",
  "VCDPA_OPT_OUT",
  "CPA_ACCESS",
  "CPA_DELETION",
  "CPA_CORRECTION",
  "CPA_APPEAL",
  "CPA_OPT_OUT",
  "CTDPA_ACCESS",
  "CTDPA_DELETION",
  "CTDPA_CORRECTION",
  "CTDPA_APPEAL",
  "CTDPA_OPT_OUT",
  "VOLUNTARY_RIGHT_TO_KNOW",
  "VOLUNTARY_RIGHT_TO_DELETE",
] as const;

export const REQUEST_TYPE_FILTERS = ["all", "know", "delete", "optout"] as const;

export type DataRequestTypeDto = typeof DATA_REQUEST_TYPES[number];

export const PRIVACY_REGULATION_NAMES = [
  "GDPR",
  "CCPA",
  "VCDPA",
  "CPA",
  "CTDPA",
  "VOLUNTARY",
] as const;
export type PrivacyRegulationName = typeof PRIVACY_REGULATION_NAMES[number];

export const PRIVACY_REQUEST_WORKFLOWS = [
  "KNOW",
  "DELETE",
  "OPT_OUT",
  "PORTABILITY",
  "RECTIFICATION",
  "OBJECTION",
  "WITHDRAW_CONSENT",
  "LIMIT",
  "NONE",
] as const;
export type PrivacyRequestWorkflow = typeof PRIVACY_REQUEST_WORKFLOWS[number];

export interface DataRequestTypeDetailsDto {
  type: DataRequestTypeDto;
  shortName: string;
  name: string;
  description: string;
  regulationName: PrivacyRegulationName;
  longName: string;
  workflow: PrivacyRequestWorkflow;
  requireVerification: boolean;
  canExtend: boolean;
  declarationText?: string;
}
