import { CustomMessageTypeDto } from "./CustomMessageTypeDto";

export interface CustomMessageSampleDto {
  messageType: CustomMessageTypeDto;
  customText: string;
  templateHTML: string;
}
