import { OrganizationPublicId, UUIDString } from "../Types";

export interface ProcessingVendorsDto {
  vendorIds: ProcessingVendorDto[];
}

interface ProcessingVendorDto {
  organizationId: OrganizationPublicId;
  dataSubjectTypeId: UUIDString;
  dataRequestTypeId: UUIDString;
  vendors: UUIDString[];
}
