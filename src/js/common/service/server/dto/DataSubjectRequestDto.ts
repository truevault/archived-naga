import {
  DataRequestPublicId,
  Iso8601Date,
  DataRequestState,
  DataRequestSubstate,
  OrganizationPublicId,
  UUIDString,
} from "../Types";

import { DataSubjectRequestEventEmbeddedDto } from "./DataSubjectRequestEventDto";
import { VendorOutcomeEmbeddedDto } from "./VendorOutcomeDto";
import { DataRequestTypeDetailsDto } from "./DataRequestTypeDto";
import { CollectionGroupDto } from "./CollectionGroupDto";
import { PaginatedDto } from "./PaginatedDto";
import { CollectionContext } from "./OrganizationDataRecipientDto";

export type PaginatedSubjectRequestDto = PaginatedDto<DataSubjectRequestSummaryDto>;

export interface DataSubjectRequestSummaryDto {
  id: DataRequestPublicId;

  state: DataRequestState;
  substate?: DataRequestSubstate;

  context: CollectionContext;

  consumerGroups: CollectionGroupDto[];
  dataRequestType: DataRequestTypeDetailsDto;

  subjectFirstName: string;
  subjectLastName: string;
  subjectEmailAddress: string;
  organizationCustomInput?: string;

  requestDate: Iso8601Date;
  dueDate: Iso8601Date;
  closedAt?: Iso8601Date;
}

export interface DataSubjectRequestDetailDto {
  id: DataRequestPublicId;
  organizationId: OrganizationPublicId;

  state: DataRequestState;
  stateHistory: DataRequestState[];
  substate?: DataRequestSubstate;

  consumerGroups: CollectionGroupDto[];
  dataRequestType: DataRequestTypeDetailsDto;

  subjectFirstName: string;
  subjectLastName: string;
  subjectEmailAddress?: string;
  subjectPhoneNumber?: string;
  subjectMailingAddress?: string;
  subjectIpAddress?: string;
  subjectCustomResponse?: string;
  organizationCustomInput?: string;

  submissionMethod?: string;
  submissionSource: string;

  requestDate: Iso8601Date;
  dueDate: Iso8601Date;
  originalDueDate: Iso8601Date;
  closedAt?: Iso8601Date;

  events: DataSubjectRequestEventEmbeddedDto[];
  vendorOutcomes: VendorOutcomeEmbeddedDto[];

  context: CollectionContext;

  selfDeclarationProvided: boolean;

  correctionRequest: string | null;
  correctionFinished: boolean | null;
  objectionRequest: string | null;
  consentWithdrawnRequest: string[] | null;
  consentWithdrawnFinished: boolean | null;

  consumerGroupProcessingInstructions?: string;
  optOutProcessingInstructions?: string;
  consumerGroupResult?: "NOT_FOUND" | "FOUND" | "PROCESSED_AS_SERVICE_PROVIDER";

  hasGaUserId: boolean;

  helpArticleId?: string;
  contactVendorsEverShown: boolean;
  contactVendorsEverHidden: boolean;
  contactVendorsHidden: boolean;
  requestorContacted: boolean;
  vendorContacted: boolean;
  isAutocreated: boolean;
  isGpcOptOut: boolean;
  limitationFinished: boolean | null;
  isDuplicate: boolean;
  duplicatesRequestId: string | null;

  emailVerified: boolean;
  emailVerifiedAutomatically: boolean;
  emailVerifiedManually: boolean;

  _local?: LocalDataRequestUpdates;
}

export type LocalDataRequestUpdates = {
  pendingVendorOutcomeUpdates?: VendorOutcomeUpdate[];
};

type VendorOutcomeUpdate = {
  vendorId: UUIDString;
  outcome: string;
};
