import {
  OrganizationPublicId,
  PrivacyCenterPublicId,
  PrivacyPolicySectionPublicId,
  Iso8601Date,
  UUIDString,
  CustomUrlStatus,
} from "../Types";

// Read objects

export interface PrivacyCenterDto {
  id: PrivacyCenterPublicId;
  organizationId: OrganizationPublicId;
  name: string;

  faviconUrl: string;
  logoUrl: string;
  logoLinkUrl: string;
  defaultSubdomain: string;
  customUrl?: string;
  customUrlStatus?: CustomUrlStatus;
  customUrlCnameReady?: boolean;
  url?: string;
  hasMobileApplication?: boolean;

  doNotSellMarkdown?: string;
  enableIbaBrowserOptOut: boolean;
  enableIbaDirectOptOut: boolean;
  employeePrivacyNoticeMarkdown?: string;
  jobApplicantPrivacyNoticeMarkdown?: string;

  dpoOfficerName: string | undefined;
  dpoOfficerEmail: string | undefined;
  dpoOfficerPhone: string | undefined;
  dpoOfficerAddress: string | undefined;
  dpoOfficerDoesNotApply: boolean;

  requiresDpo: boolean;
  hasDpoTask: boolean;

  disableCaResidencyConfirmation: boolean;
  ccpaPrivacyNoticeIntroText: string;
  vcdpaPrivacyNoticeIntroText: string;
  cpaPrivacyNoticeIntroText: string;
  ctdpaPrivacyNoticeIntroText: string;
  gdprPrivacyNoticeIntroText: string;
  cookiePolicyIntroText: string;
  retentionIntroText: string;
  optOutNoticeIntroText: string;
}

export interface PrivacyCenterPolicyDto {
  privacyCenterId: PrivacyCenterPublicId;
  policyLastUpdated?: Iso8601Date;
  policySections: PrivacyCenterPolicySectionDto[];
}

export interface PrivacyCenterPolicySectionDto {
  id: PrivacyPolicySectionPublicId;
  custom: boolean;
  privacyCenterId?: PrivacyCenterPublicId;
  name?: string;
  anchor?: string;
  addToMenu?: boolean;
  noPrivacyPolicy?: boolean;
  body?: string;
  displayOrder: number;
  showHeader: boolean;
}

// Write objects

export type CreatePrivacyCenter = {
  name: string;
  faviconUrl: string;
  defaultSubdomain: string;
  customUrl?: string;
};

export type UpdatePrivacyCenter = {
  name: string;
  faviconUrl: string;
  defaultSubdomain: string;
  customUrl?: string;
  customFieldEnable?: boolean;
  customFieldLabel?: string;
  customFieldHelp?: string;

  dpoOfficerName?: string;
  dpoOfficerEmail?: string;
  dpoOfficerPhone?: string;
  dpoOfficerAddress?: string;
  dpoOfficerDoesNotApply: Boolean;
  ccpaPrivacyNoticeIntroText: string;
  gdprPrivacyNoticeIntroText: string;
  vcdpaPrivacyNoticeIntroText: string;
  cookiePolicyIntroText: string;
  optOutNoticeIntroText: string;
};

export type UpdatePrivacyCenterPolicy = {
  policyLastUpdated: Iso8601Date;
  policySectionsOrder: Record<UUIDString, number>;
};

export type AddOrUpdatePrivacyCenterPolicySection = {
  name: string;
  anchor: string;
  addToMenu: boolean;
  noPrivacyPolicy: boolean;
  body: string;
  publish?: boolean;
};
