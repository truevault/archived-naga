export interface StatsResponse {
  active: {
    count: number;
    dueThisWeek: number;
    overdue: number;
  };

  inactive: {
    count: number;
    avgProcessingDays: number;
    pctOnTime: number;
  };
}
