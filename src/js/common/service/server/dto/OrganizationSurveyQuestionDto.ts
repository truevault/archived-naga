import { OrganizationPublicId } from "../Types";

export interface OrganizationSurveyQuestionDto {
  organizationId: OrganizationPublicId;
  surveyName: string;
  slug: string;
  question: string;
  answer?: string;
}
