export interface RequestTemplateMessageDto {
  name: string;
  template: string;
  renderedTemplate: string;
}

export interface RequestTemplateDto {
  messages: RequestTemplateMessageDto[];
  signature: string;
}
