export type FilterDto = {
  id: string;
  value?: string | number;
};

export type Filter = FilterDto;
