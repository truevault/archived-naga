import {
  OrganizationPublicId,
  OrganizationMailboxStatus,
  UUIDString,
  GetCompliantStep,
  PrivacyNoticeProgressEnum,
  MappingProgressEnum,
  OptOutPageType,
  SurveySlug,
} from "../Types";
import { Phase } from "../../../../polaris/util/Phase";

interface PrivacyNoticeProgress {
  value: PrivacyNoticeProgressEnum;
  label: string;
}

interface MappingProgressDto {
  value: MappingProgressEnum;
  label: string;
}

export interface OrganizationDto {
  id: OrganizationPublicId;
  name: string;
  logoUrl: string;
  faviconUrl?: string;
  identityVerificationMessage: string;
  mailbox: OrganizationMailboxStatusDto;
  messageSettings: OrganizationMessageSettingsDto;
  regulations: OrganizationRegulationDto[];
  phase: Phase;
  getCompliantProgress: GetCompliantStep;
  getCompliantInvalidatedProgress: GetCompliantStep[];
  caPrivacyNoticeProgress: PrivacyNoticeProgress;
  optOutPrivacyNoticeProgress: PrivacyNoticeProgress;
  optOutPageType: OptOutPageType;
  exceptionsMappingProgress: MappingProgressDto;
  cookiesMappingProgress: MappingProgressDto;
  isServiceProvider: boolean;
  activeRequestCount: number;
  activeTaskCount: number;
  customFieldEnabled: boolean;
  customFieldLabel: string;
  customFieldHelp: string;
  doesOrgProcessOptOuts: boolean;

  requiresNoticeOfRightToLimit: boolean;
  requiresAssociations: boolean;
  requiresEmployeeMapping: boolean;

  cookiePolicy: string | null;

  financialIncentiveDetails: string | null;

  gaWebProperty: string | null;

  devInstructionsId: string;
  hrInstructionsId: string;

  isSelling: boolean;
  isSharing: boolean;

  featureGdpr: boolean;
  featureMultistate: boolean;

  nextSurvey?: SurveySlug;
  currentSurvey?: SurveySlug;
}

export type OrganizationAuthTokenProvider = "GOOGLE_OAUTH";

export interface OrganizationAuthorizationsDto {
  id: OrganizationPublicId;
  hasGoogle: boolean;
}

export interface OrganizationRegulationDto {
  id: UUIDString;
  slug: string;
  name: string;
  longName: string;
  description: string;
  enabled: boolean;
  identityVerificationInstruction: string;
  settings: Record<string, any>;
}

export interface OrganizationMessageSettingsDto {
  headerImageUrl: string;
  physicalAddress: string;
  messageSignature: string;
  defaultMessageSignature: string;
}

interface OrganizationMailboxStatusDto {
  viewSettings: boolean;
  mailboxStatus: OrganizationMailboxStatus;
  mailboxOauthUrl?: string;
}
