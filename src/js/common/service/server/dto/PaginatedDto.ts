export type PaginatedMetaDto = {
  page: number;
  per: number;
  count: number;
  totalPages: number;
  totalCount: number;
};

export type PaginatedDto<T> = {
  data: T[];
  meta: PaginatedMetaDto;
};
