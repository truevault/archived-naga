export interface OrganizationTemplateDto {
  name: string;
  template: string;
}
