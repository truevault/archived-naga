import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import { PublicOrganizationNoticeDto } from "../dto/PublicOrganizationNoticeDto";

export class PublicOrganizationController {
  constructor(private axios: AxiosInstance) {}

  public getOrganizationNotices = async (organizationId) => {
    const url = Helpers.public.orgNotices(organizationId);
    const response = await this.axios.get<PublicOrganizationNoticeDto>(url, {});

    return response.data as PublicOrganizationNoticeDto;
  };
}
