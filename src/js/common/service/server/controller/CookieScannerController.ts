import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import {
  CookieRecommendationDto,
  CookieScanResultDto,
  UpdateCookieRequest,
} from "../dto/CookieScanDto";

import { OrganizationPublicId } from "../Types";

export class CookieScannerController {
  constructor(private axios: AxiosInstance) {}

  public startCookieScan = async (
    organizationId: OrganizationPublicId,
    scanUrl: String,
  ): Promise<CookieScanResultDto> => {
    const url = Helpers.organization.cookieScanner.root(organizationId);
    const response = await this.axios.post(url, { url: scanUrl });
    return response.data;
  };

  public getCookieScanResult = async (
    organizationId: OrganizationPublicId,
  ): Promise<CookieScanResultDto> => {
    const url = Helpers.organization.cookieScanner.root(organizationId);
    const response = await this.axios.get<CookieScanResultDto>(url);
    return response.data;
  };

  public resetCookieScan = async (organizationId: OrganizationPublicId): Promise<void> => {
    const url = Helpers.organization.cookieScanner.root(organizationId);
    await this.axios.delete(url);
  };

  public updateCookie = async (
    organizationId: OrganizationPublicId,
    domain: string,
    name: string,
    update: UpdateCookieRequest,
  ): Promise<void> => {
    const url = Helpers.organization.cookieScanner.cookie(organizationId, domain, name);
    await this.axios.put(url, update);
  };

  public getCookieRecommendations = async (
    organizationId: OrganizationPublicId,
  ): Promise<CookieRecommendationDto[]> => {
    const url = Helpers.organization.cookieScanner.cookieRecommendations(organizationId);
    const response = await this.axios.get<CookieRecommendationDto[]>(url);
    return response.data;
  };
}
