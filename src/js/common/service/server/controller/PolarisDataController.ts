import { AxiosInstance } from "axios";
import { ApiRoutes } from "../../../root/apiRoutes";
import { RetentionReasonDto } from "../dto/RetentionReasonDto";

export class PolarisDataController {
  constructor(private axios: AxiosInstance) {}

  public getRetentionReasons = async () => {
    const url = ApiRoutes.retentionReasons.root;
    const response = await this.axios.get<RetentionReasonDto[]>(url);

    return response.data as RetentionReasonDto[];
  };
}
