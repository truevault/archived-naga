import { AxiosInstance } from "axios";
import QueryString from "qs";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import { OkReturnDto } from "../dto/OkReturnDto";
import {
  AddOrUpdateCustomVendorDto,
  CollectionContext,
  OrganizationDataRecipientDto,
  OrganizationDataRecipientPlatformDto,
  OrganizationVendorNewAndEditDto,
  OrganizationVendorSettingsDto,
} from "../dto/OrganizationDataRecipientDto";
import { VendorDto } from "../dto/VendorDto";
import { MappingProgressEnum, OrganizationPublicId, UUIDString, VendorPublicId } from "../Types";

export class OrganizationVendorController {
  constructor(private axios: AxiosInstance) {}

  public getVendors = async (
    organizationId: OrganizationPublicId,
    excludeCategories?: String[],
    includeDataStorageLocations: boolean = false,
    excludeIncomplete: boolean | undefined = undefined,
  ): Promise<OrganizationDataRecipientDto[]> => {
    const url = Helpers.organizationVendors(organizationId);
    const response = await this.axios.get<OrganizationDataRecipientDto[]>(url, {
      params: { excludeCategories, excludeIncomplete, includeDataStorageLocations },
      paramsSerializer: (q) => QueryString.stringify(q, { arrayFormat: "comma" }),
    });

    return response.data as OrganizationDataRecipientDto[];
  };

  public getUnmappedVendors = async (
    organizationId: OrganizationPublicId,
    collectionGroupId: UUIDString,
  ): Promise<OrganizationDataRecipientDto[]> => {
    const url = Helpers.organizationUnmappedVendors(organizationId, collectionGroupId);
    const response = await this.axios.get<OrganizationDataRecipientDto[]>(url, {});

    return response.data as OrganizationDataRecipientDto[];
  };

  public getVendorCatalog = async (organizationId: OrganizationPublicId): Promise<VendorDto[]> => {
    const url = Helpers.organizationVendorCatalog(organizationId);
    const response = await this.axios.get<VendorDto[]>(url, {});
    return response.data as VendorDto[];
  };

  public getVendorDetails = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
  ): Promise<OrganizationDataRecipientDto> => {
    const url = Helpers.organizationVendor(organizationId, vendorId);
    const response = await this.axios.get<OrganizationDataRecipientDto>(url, {});

    return response.data;
  };

  public getPlatformApps = async (
    organizationId: OrganizationPublicId,
    platformVendorId: VendorPublicId,
  ): Promise<OrganizationDataRecipientPlatformDto> => {
    const url = Helpers.organizationPlatformApps(organizationId, platformVendorId);
    const response = await this.axios.get<OrganizationDataRecipientPlatformDto>(url, {});

    return response.data;
  };

  public getAppPlatforms = async (
    organizationId: OrganizationPublicId,
    platformVendorId: VendorPublicId,
  ): Promise<OrganizationDataRecipientPlatformDto> => {
    const url = Helpers.organizationAppPlatforms(organizationId, platformVendorId);
    const response = await this.axios.get<OrganizationDataRecipientPlatformDto>(url, {});

    return response.data;
  };

  public getNewData = async (
    organizationId: OrganizationPublicId,
  ): Promise<OrganizationVendorNewAndEditDto> => {
    const url = Helpers.organization.vendors.new(organizationId);
    const response = await this.axios.get<OrganizationVendorNewAndEditDto>(url, {});
    return response.data as OrganizationVendorNewAndEditDto;
  };

  public getEditData = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
  ): Promise<OrganizationVendorNewAndEditDto> => {
    const url = Helpers.organization.vendors.edit(organizationId, vendorId);
    const response = await this.axios.get<OrganizationVendorNewAndEditDto>(url, {});
    return response.data as OrganizationVendorNewAndEditDto;
  };

  public getAddData = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
  ): Promise<OrganizationVendorNewAndEditDto> => {
    const url = Helpers.organization.vendors.add(organizationId, vendorId);
    const response = await this.axios.get<OrganizationVendorNewAndEditDto>(url, {});
    return response.data as OrganizationVendorNewAndEditDto;
  };

  public searchVendors = async (
    organizationId: OrganizationPublicId,
    vendorName: string,
  ): Promise<VendorDto[]> => {
    const url = `${Helpers.organization.vendors.search(organizationId)}?name=${vendorName}`;
    const response = await this.axios.get<VendorDto[]>(url, {});
    return response.data as VendorDto[];
  };

  public addDefaultVendors = async (organizationId: OrganizationPublicId): Promise<void> => {
    const url = Helpers.organizationVendorsDefault(organizationId);
    await this.axios.post(url);
  };

  public addOrUpdateVendor = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
    organizationVendorSettings: OrganizationVendorSettingsDto,
  ): Promise<OrganizationDataRecipientDto> => {
    const url = Helpers.organizationVendor(organizationId, vendorId);
    const response = await this.axios.put<OrganizationDataRecipientDto>(
      url,
      organizationVendorSettings,
    );
    return response.data as OrganizationDataRecipientDto;
  };

  public addCustomVendor = async (
    organizationId: OrganizationPublicId,
    organizationVendorDto: AddOrUpdateCustomVendorDto,
  ): Promise<OrganizationDataRecipientDto> => {
    const url = Helpers.organization.vendors.root(organizationId);
    const response = await this.axios.post<OrganizationDataRecipientDto>(
      url,
      organizationVendorDto,
    );
    return response.data as OrganizationDataRecipientDto;
  };

  public getThirdPartyDataRecipients = async (
    organizationId: OrganizationPublicId,
  ): Promise<VendorDto[]> => {
    const url = Helpers.organization.vendors.thirdPartyRecipients(organizationId);
    const response = await this.axios.get<VendorDto[]>(url, {});
    return response.data as VendorDto[];
  };

  public removeVendor = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
    collectionContext?: CollectionContext[],
  ): Promise<Record<string, boolean>> => {
    const url = Helpers.organizationVendor(organizationId, vendorId);
    const response = await this.axios.delete<Record<string, boolean>>(url, {
      params: { collectionContext },
      paramsSerializer: (q) => QueryString.stringify(q, { arrayFormat: "comma" }),
    });
    return response.data as Record<string, boolean>;
  };

  public removeVendorAgreementFile = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
  ): Promise<OrganizationDataRecipientDto> => {
    const url = Helpers.organizationVendorAgreement(organizationId, vendorId);
    const response = await this.axios.delete<OrganizationDataRecipientDto>(url);
    return response.data as OrganizationDataRecipientDto;
  };

  public addPlatformApplication = async (
    organizationId: string,
    platformId: string,
    appId: string,
  ): Promise<void> => {
    const url = Helpers.organizationPlatformInstall(organizationId, platformId, appId);
    await this.axios.post(url);
  };

  public removePlatformApplication = async (
    organizationId: string,
    platformId: string,
    appId: string,
  ): Promise<void> => {
    const url = Helpers.organizationPlatformInstall(organizationId, platformId, appId);
    await this.axios.delete(url);
  };

  public removeVendors = async (
    organizationId: OrganizationPublicId,
    vendors: UUIDString[],
  ): Promise<Record<string, boolean>> => {
    const url = Helpers.organizationRemoveVendors(organizationId);
    const response = await this.axios.post<Record<string, boolean>>(url, { vendors });
    return response.data as Record<string, boolean>;
  };

  public updateMappingProgress = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
    mappingProgress: MappingProgressEnum,
  ): Promise<OkReturnDto> => {
    const url = Helpers.organizationVendorMappingProgress(
      organizationId,
      vendorId,
      mappingProgress,
    );
    const response = await this.axios.put<OkReturnDto>(url);
    return response.data;
  };
}
