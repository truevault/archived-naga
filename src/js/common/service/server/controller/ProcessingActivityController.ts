import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers, ApiRoutes } from "../../../root/apiRoutes";
import { ProcessingRegion } from "../dto/OrganizationDataRecipientDto";
import {
  CreateProcessingActivityRequest,
  DefProcessingActivityDto,
  ProcessingActivityDto,
  PatchProcessingActivityRequest,
} from "../dto/ProcessingActivityDto";

export type TaskFilter = "active" | "complete";

export class ProcessingActivityController {
  constructor(private axios: AxiosInstance) {}

  public getDefaultProcessingActivities = async (): Promise<DefProcessingActivityDto[]> => {
    const response = await this.axios.get<DefProcessingActivityDto[]>(
      ApiRoutes.processingActivities.root,
    );
    return response.data;
  };

  public getProcessingActivities = async (orgId: string): Promise<ProcessingActivityDto[]> => {
    const url = Helpers.organization.processingActivites.root(orgId);
    const response = await this.axios.get<ProcessingActivityDto[]>(url);
    return response.data;
  };

  public createProcessingActivity = async (
    orgId: string,
    create: CreateProcessingActivityRequest,
  ): Promise<ProcessingActivityDto> => {
    const url = Helpers.organization.processingActivites.root(orgId);
    const response = await this.axios.post<ProcessingActivityDto>(url, create);
    return response.data;
  };

  public manageAutomaticProcessingActivity = async (
    orgId: string,
  ): Promise<ProcessingActivityDto> => {
    const url = Helpers.organization.processingActivites.automatic(orgId);
    const response = await this.axios.post<ProcessingActivityDto>(url);
    return response.data;
  };

  public patchUpdateProcessingActivity = async (
    orgId: string,
    activityId: string,
    update: PatchProcessingActivityRequest,
  ): Promise<ProcessingActivityDto> => {
    const url = Helpers.organization.processingActivites.specific(orgId, activityId);
    const response = await this.axios.patch<ProcessingActivityDto>(url, update);
    return response.data;
  };

  public deleteProcessingActivityRegion = async (
    orgId: string,
    activityId: string,
    regions: ProcessingRegion[],
  ): Promise<void> => {
    const url = Helpers.organization.processingActivites.specific(orgId, activityId);
    await this.axios.delete<void>(url, { params: { regions: regions.join(",") } });
  };

  public associateProcessingActivityAndPic = async (
    orgId: string,
    activityId: string,
    picId: string,
  ): Promise<ProcessingActivityDto> => {
    const url = Helpers.organization.processingActivites.specificAssociationPic(
      orgId,
      activityId,
      picId,
    );
    const response = await this.axios.post<ProcessingActivityDto>(url);
    return response.data;
  };

  public unassociateProcessingActivityAndPic = async (
    orgId: string,
    activityId: string,
    picId: string,
  ): Promise<ProcessingActivityDto> => {
    const url = Helpers.organization.processingActivites.specificAssociationPic(
      orgId,
      activityId,
      picId,
    );
    const response = await this.axios.delete<ProcessingActivityDto>(url);
    return response.data;
  };
}
