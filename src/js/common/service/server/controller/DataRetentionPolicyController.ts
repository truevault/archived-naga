import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import {
  DataRetentionPolicyDetailCategory,
  DataRetentionPolicyDto,
  UpdateDataRetentionPolicyDetailDto,
} from "../dto/DataRetentionPolicyDto";

import { OrganizationPublicId } from "../Types";

export class DataRetentionPolicyController {
  constructor(private axios: AxiosInstance) {}

  public getDataRetention = async (
    organizationId: OrganizationPublicId,
  ): Promise<DataRetentionPolicyDto> => {
    const url = Helpers.organization.dataRetention.root(organizationId);
    const response = await this.axios.get<DataRetentionPolicyDto>(url);
    return response.data;
  };

  public updateDataRetentionCategory = async (
    organizationId: OrganizationPublicId,
    category: DataRetentionPolicyDetailCategory,
    dto: UpdateDataRetentionPolicyDetailDto,
  ): Promise<void> => {
    const url = Helpers.organization.dataRetention.category(organizationId, category);
    await this.axios.put<void>(url, dto);
  };
}
