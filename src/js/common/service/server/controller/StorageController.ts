import { AxiosInstance } from "axios";
import { ApiRouteHelpers } from "../../../root/apiRoutes";
import { OrganizationPublicId, LogoType, PrivacyCenterPublicId, VendorPublicId } from "../Types";

export type UploadLogoResponse = {
  logoPath?: string;
  logoUrl?: string;
  error?: string;
};

export type UploadLogoDto = {
  logoType: LogoType;
  logo: File;
};

export type DeleteVendorAgreement = {
  success?: string;
  error?: string;
};

export type UploadVendorAgreementResponse = {
  vendorId?: string;
  fileName?: string;
  fileKey?: string;
  error?: string;
};

export class StorageController {
  constructor(private axios: AxiosInstance) {}

  private uploadLogo = async (
    url: string,
    logo: File,
    logoType: LogoType,
  ): Promise<UploadLogoResponse> => {
    const formData = new FormData();
    formData.append("logo", logo, logo.name);
    formData.append("logoType", logoType);
    const response = await this.axios.post<UploadLogoResponse>(url, formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });
    return response.data as UploadLogoResponse;
  };

  private uploadVendorAgreement = async (
    url: string,
    filename: string,
    filedata: File,
  ): Promise<UploadVendorAgreementResponse> => {
    const formData = new FormData();
    formData.append("filedata", filedata, filedata.name);
    formData.append("filename", filename);
    const response = await this.axios.post<UploadVendorAgreementResponse>(url, formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });
    return response.data as UploadLogoResponse;
  };

  public postOrganizationLogo = async (
    organizationId: OrganizationPublicId,
    logoDto: UploadLogoDto,
  ): Promise<UploadLogoResponse> => {
    const url = ApiRouteHelpers.storage.logos.organization(organizationId);
    return this.uploadLogo(url, logoDto.logo, logoDto.logoType);
  };

  public postOrganizationVendorAgreement = async (
    organizationId: OrganizationPublicId,
    vendorId: VendorPublicId,
    filename: string,
    filedata: File,
  ): Promise<UploadVendorAgreementResponse> => {
    const url = ApiRouteHelpers.storage.vendorAgreements.vendor(organizationId, vendorId);
    return this.uploadVendorAgreement(url, filename, filedata);
  };

  public deleteOrganizationVendorAgreement = async (
    organizationId: OrganizationPublicId,
    vendorId: VendorPublicId,
    filekey: string,
  ): Promise<UploadVendorAgreementResponse> => {
    const url = ApiRouteHelpers.storage.vendorAgreements.specific(
      organizationId,
      vendorId,
      filekey,
    );

    const response = await this.axios.delete<DeleteVendorAgreement>(url);
    return response.data as DeleteVendorAgreement;
  };

  public postPrivacyCenterLogo = async (
    organizationId: OrganizationPublicId,
    privacyCenterId: PrivacyCenterPublicId,
    logoDto: UploadLogoDto,
  ): Promise<UploadLogoResponse> => {
    const url = ApiRouteHelpers.storage.logos.privacyCenter(organizationId, privacyCenterId);
    return this.uploadLogo(url, logoDto.logo, logoDto.logoType);
  };
}
