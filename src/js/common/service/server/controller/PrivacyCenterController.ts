import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import {
  AddOrUpdatePrivacyCenterPolicySection,
  CreatePrivacyCenter,
  PrivacyCenterDto,
  PrivacyCenterPolicyDto,
  PrivacyCenterPolicySectionDto,
  UpdatePrivacyCenter,
  UpdatePrivacyCenterPolicy,
} from "../dto/PrivacyCenterDto";

import {
  OrganizationPublicId,
  PrivacyCenterPublicId,
  PrivacyPolicySectionPublicId,
} from "../Types";

export class PrivacyCenterController {
  constructor(private axios: AxiosInstance) {}

  public getPrivacyCenters = async (organizationId: OrganizationPublicId) => {
    const url = Helpers.organization.privacyCenters(organizationId);
    const response = await this.axios.get<PrivacyCenterDto[]>(url);
    return response.data as PrivacyCenterDto[];
  };

  public getPrivacyCenter = async (
    organizationId: OrganizationPublicId,
    privacyCenterId: PrivacyCenterPublicId,
  ) => {
    const url = Helpers.organization.privacyCenter.detail(organizationId, privacyCenterId);
    const response = await this.axios.get<PrivacyCenterDto>(url);
    return response.data as PrivacyCenterDto;
  };

  public updatePrivacyCenter = async (
    organizationId: OrganizationPublicId,
    privacyCenterId: PrivacyCenterPublicId,
    updatePrivacyCenter: UpdatePrivacyCenter,
  ) => {
    const url = Helpers.organization.privacyCenter.detail(organizationId, privacyCenterId);
    const response = await this.axios.put<PrivacyCenterDto>(url, updatePrivacyCenter);
    return response.data as PrivacyCenterDto;
  };

  public createPrivacyCenter = async (
    organizationId: OrganizationPublicId,
    createPrivacyCenter: CreatePrivacyCenter,
  ) => {
    const url = Helpers.organization.privacyCenters(organizationId);
    const response = await this.axios.post<PrivacyCenterDto>(url, createPrivacyCenter);
    return response.data as PrivacyCenterDto;
  };

  // PRIVACY POLICY ENDPOINTS

  public getPrivacyCenterPolicy = async (
    organizationId: OrganizationPublicId,
    privacyCenterId: PrivacyCenterPublicId,
  ) => {
    const url = Helpers.organization.privacyCenter.privacyPolicy.policy(
      organizationId,
      privacyCenterId,
    );

    const response = await this.axios.get<PrivacyCenterPolicyDto>(url);
    return response.data as PrivacyCenterPolicyDto;
  };

  public updatePrivacyCenterPolicy = async (
    organizationId: OrganizationPublicId,
    privacyCenterId: PrivacyCenterPublicId,
    updatePolicy: UpdatePrivacyCenterPolicy,
  ) => {
    const url = Helpers.organization.privacyCenter.privacyPolicy.policy(
      organizationId,
      privacyCenterId,
    );
    const response = await this.axios.put<PrivacyCenterPolicyDto>(url, updatePolicy);
    return response.data as PrivacyCenterPolicyDto;
  };

  public addPrivacyCenterPolicySection = async (
    organizationId: OrganizationPublicId,
    privacyCenterId: PrivacyCenterPublicId,
    addSection: AddOrUpdatePrivacyCenterPolicySection,
  ) => {
    const url = Helpers.organization.privacyCenter.privacyPolicy.sections(
      organizationId,
      privacyCenterId,
    );
    const response = await this.axios.post<PrivacyCenterPolicySectionDto>(url, addSection);
    return response.data as PrivacyCenterPolicySectionDto;
  };

  public updatePrivacyCenterPolicySection = async (
    organizationId: OrganizationPublicId,
    privacyCenterId: PrivacyCenterPublicId,
    sectionId: PrivacyPolicySectionPublicId,
    updateSection: AddOrUpdatePrivacyCenterPolicySection,
  ) => {
    const url = Helpers.organization.privacyCenter.privacyPolicy.section.detail(
      organizationId,
      privacyCenterId,
      sectionId,
    );
    const response = await this.axios.put<PrivacyCenterPolicySectionDto>(url, updateSection);
    return response.data as PrivacyCenterPolicySectionDto;
  };

  public deletePrivacyCenterPolicySection = async (
    organizationId: OrganizationPublicId,
    privacyCenterId: PrivacyCenterPublicId,
    sectionId: PrivacyPolicySectionPublicId,
  ) => {
    const url = Helpers.organization.privacyCenter.privacyPolicy.section.detail(
      organizationId,
      privacyCenterId,
      sectionId,
    );
    const response = await this.axios.delete<PrivacyCenterPolicySectionDto>(url);
    return response.data as PrivacyCenterPolicySectionDto;
  };
}
