import { AxiosInstance } from "axios";

import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import {
  RequestHandlingInstructionDto,
  UpdateOrganizationCGInstructionsDto,
} from "../dto/RequestHandlingInstructionDto";
import { OrganizationPublicId, RequestHandlingInstructionType } from "../Types";

export class RequestHandlingInstructionsController {
  constructor(private axios: AxiosInstance) {}

  public getInstructions = async (
    organizationId: OrganizationPublicId,
  ): Promise<RequestHandlingInstructionDto[]> => {
    const url = Helpers.organization.requestHandlingInstructions.root(organizationId);

    const response = await this.axios.get(url);
    return response.data as RequestHandlingInstructionDto[];
  };

  public getInstructionsByType = async (
    organizationId: OrganizationPublicId,
    requestType: RequestHandlingInstructionType,
  ): Promise<RequestHandlingInstructionDto[]> => {
    const url = Helpers.organization.requestHandlingInstructions.forType(
      organizationId,
      requestType,
    );

    const response = await this.axios.get(url);
    return response.data as RequestHandlingInstructionDto[];
  };

  public putInstruction = async (
    organizationId: OrganizationPublicId,
    dto: RequestHandlingInstructionDto,
  ): Promise<RequestHandlingInstructionDto> => {
    const url = Helpers.organization.requestHandlingInstructions.root(organizationId);

    const response = await this.axios.put(url, dto);
    return response.data as RequestHandlingInstructionDto;
  };

  public deleteInstruction = async (
    dto: RequestHandlingInstructionDto,
  ): Promise<RequestHandlingInstructionDto> => {
    const url = Helpers.organization.requestHandlingInstructions.instruction(
      dto.organizationId,
      dto.id,
    );

    const response = await this.axios.delete(url);
    return response.data as RequestHandlingInstructionDto;
  };

  public getCGInstructions = async (organizationId: OrganizationPublicId): Promise<string> => {
    const url = Helpers.organization.requestHandlingInstructions.consumerGroup(organizationId);

    const response = await this.axios.get(url);
    return response.data as string;
  };

  public getOptOutInstructions = async (organizationId: OrganizationPublicId): Promise<string> => {
    const url = Helpers.organization.requestHandlingInstructions.optOut(organizationId);

    const response = await this.axios.get(url);
    return response.data as string;
  };

  public updateConsumerGroupRequestHandlingInstructions = async (
    dto: UpdateOrganizationCGInstructionsDto,
  ): Promise<UpdateOrganizationCGInstructionsDto> => {
    const url = Helpers.organization.requestHandlingInstructions.consumerGroup(dto.organizationId);

    const response = await this.axios.put(url, dto);
    return response.data as UpdateOrganizationCGInstructionsDto;
  };

  public updateOptOutInstructions = async (
    dto: UpdateOrganizationCGInstructionsDto,
  ): Promise<UpdateOrganizationCGInstructionsDto> => {
    const url = Helpers.organization.requestHandlingInstructions.optOut(dto.organizationId);

    const response = await this.axios.put(url, dto);
    return response.data as UpdateOrganizationCGInstructionsDto;
  };
}
