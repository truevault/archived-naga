import { AxiosInstance } from "axios";
import { ApiRoutes, ApiRouteHelpers } from "../../../root/apiRoutes";
import { VendorDto } from "../dto/VendorDto";

export class VendorController {
  constructor(private axios: AxiosInstance) {}

  public getVendorCatalog = async () => {
    const url = ApiRoutes.vendors.root;
    const response = await this.axios.get<VendorDto[]>(url, {});

    return response.data as VendorDto[];
  };

  public getVendorDetails = async (vendorId, orgId) => {
    const url = `${ApiRouteHelpers.vendors.specific(vendorId)}?org=${orgId}`;

    const response = await this.axios.get<VendorDto>(url, {});
    return response.data as VendorDto;
  };
}
