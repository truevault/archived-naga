import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import { TaskDto } from "../dto/TaskDto";

import { OrganizationPublicId, UUIDString } from "../Types";

export type TaskFilter = "active" | "complete";

export class TaskController {
  constructor(private axios: AxiosInstance) {}

  public getTasks = async (
    organizationId: OrganizationPublicId,
    filter: TaskFilter = "active",
  ): Promise<TaskDto[]> => {
    const url = Helpers.organization.tasks.root(organizationId);
    const params = { filter };
    const response = await this.axios.get<TaskDto[]>(url, {
      params,
    });
    return response.data;
  };

  public completeTasks = async (organizationId: OrganizationPublicId): Promise<TaskDto> => {
    const url = Helpers.organization.tasks.completeTasks(organizationId);
    const response = await this.axios.post<TaskDto>(url);
    return response.data;
  };

  public completeTask = async (
    organizationId: OrganizationPublicId,
    taskId: UUIDString,
  ): Promise<TaskDto> => {
    const url = Helpers.organization.tasks.completeTask(organizationId, taskId);
    const response = await this.axios.post<TaskDto>(url);
    return response.data;
  };

  public createAddDpoTask = async (
    organizationId: OrganizationPublicId,
  ): Promise<TaskDto | null> => {
    const url = Helpers.organization.tasks.addDpo(organizationId);
    const response = await this.axios.post<TaskDto | null>(url);
    return response.data;
  };

  public createAuthorizeGoogleAnalytics = async (
    organizationId: OrganizationPublicId,
  ): Promise<TaskDto | null> => {
    const url = Helpers.organization.tasks.addAuthorizeGoogleAnalytics(organizationId);
    const response = await this.axios.post<TaskDto | null>(url);
    return response.data;
  };

  public createCompleteFinancialIncentives = async (
    organizationId: OrganizationPublicId,
  ): Promise<TaskDto | null> => {
    const url = Helpers.organization.tasks.completeFinIncentives(organizationId);
    const response = await this.axios.post<TaskDto | null>(url);
    return response.data;
  };
}
