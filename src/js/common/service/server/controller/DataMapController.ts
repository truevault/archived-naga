import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import { DataMapDto, UpdateDataMapRequest, V2DataMapDto } from "../dto/DataMapDto";

import { OrganizationPublicId } from "../Types";

export class DataMapController {
  constructor(private axios: AxiosInstance) {}

  public getDataMap = async (organizationId: OrganizationPublicId): Promise<V2DataMapDto> => {
    const url = Helpers.organization.dataMap.root(organizationId);
    const response = await this.axios.get<V2DataMapDto>(url);
    return response.data;
  };

  public patchDataMap = async (
    organizationId: OrganizationPublicId,
    update: UpdateDataMapRequest,
  ): Promise<void> => {
    const url = Helpers.organization.dataMap.root(organizationId);
    await this.axios.patch<V2DataMapDto>(url, update);
  };

  public updateCollection = async (
    organizationId: OrganizationPublicId,
    collectionGroupId: string,
    picId: string,
    collected: boolean,
  ): Promise<void> => {
    const url = Helpers.organization.dataMap.picCollection(
      organizationId,
      collectionGroupId,
      picId,
    );
    await this.axios.post<DataMapDto>(url, { collected });
  };

  public updateRecipientAssociation = async (
    organizationId: OrganizationPublicId,
    collectionGroupId: string,
    vendorId: string,
    associated: boolean,
    updateDisclosure: boolean = false,
  ): Promise<void> => {
    const url = Helpers.organization.dataMap.picAssociation(
      organizationId,
      collectionGroupId,
      vendorId,
    );
    await this.axios.post<DataMapDto>(url, { associated, updateDisclosure });
  };

  public updateSourceAssociation = async (
    organizationId: OrganizationPublicId,
    collectionGroupId: string,
    vendorId: string,
    associated: boolean,
    updateReceived: boolean = false,
  ): Promise<void> => {
    const url = Helpers.organization.dataMap.picSourceAssociation(
      organizationId,
      collectionGroupId,
      vendorId,
    );
    await this.axios.post<DataMapDto>(url, { associated, updateReceived });
  };

  public updateDisclosure = async (
    organizationId: OrganizationPublicId,
    collectionGroupId: string,
    vendorId: string,
    picGroupId: string,
    disclosed: boolean,
    updateCollection: boolean,
  ): Promise<void> => {
    const url = Helpers.organization.dataMap.picDisclosure(
      organizationId,
      collectionGroupId,
      vendorId,
      picGroupId,
    );
    await this.axios.post<DataMapDto>(url, { disclosed, updateCollection });
  };

  public updateReceived = async (
    organizationId: OrganizationPublicId,
    collectionGroupId: string,
    vendorId: string,
    picId: string,
    received: boolean,
    updateCollection: boolean,
  ): Promise<void> => {
    const url = Helpers.organization.dataMap.picReceived(
      organizationId,
      collectionGroupId,
      vendorId,
      picId,
    );
    await this.axios.post<DataMapDto>(url, { received, updateCollection });
  };
}
