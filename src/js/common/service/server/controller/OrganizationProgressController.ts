import { AxiosInstance } from "axios";
import QueryString from "qs";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";

import { MappingProgressEnum, OrganizationPublicId, SurveySlug } from "../Types";

export interface OrganizationProgressDto {
  key: string;
  progress: {
    value: MappingProgressEnum;
    label: string;
  };
}

export interface OrganizationSurveyProgressDto {
  survey: SurveySlug;
  progress: string;
}

export class OrganizationProgressController {
  constructor(private axios: AxiosInstance) {}

  public getProgressKeys = async (
    organizationId: OrganizationPublicId,
    keys: string[],
  ): Promise<OrganizationProgressDto[]> => {
    const url = Helpers.organization.progress.root(organizationId);
    const response = await this.axios.get<OrganizationProgressDto[]>(url, {
      params: { keys },
      paramsSerializer: (q) => QueryString.stringify(q, { arrayFormat: "comma" }),
    });

    return response.data as OrganizationProgressDto[];
  };

  public setProgressKey = async (
    organizationId: OrganizationPublicId,
    keyId: string,
    progress: MappingProgressEnum,
  ): Promise<OrganizationProgressDto[]> => {
    const url = Helpers.organization.progress.specific(organizationId, keyId);
    const response = await this.axios.put<OrganizationProgressDto[]>(url, { key: keyId, progress });

    return response.data as OrganizationProgressDto[];
  };

  public getSurveyProgress = async (
    organizationId: OrganizationPublicId,
    survey: SurveySlug,
  ): Promise<OrganizationSurveyProgressDto> => {
    const url = Helpers.organization.progress.specificSurvey(organizationId, survey);
    const response = await this.axios.get<OrganizationSurveyProgressDto>(url);

    return response.data as OrganizationSurveyProgressDto;
  };

  public updateSurveyProgress = async (
    organizationId: OrganizationPublicId,
    survey: SurveySlug,
    progress: string,
  ): Promise<OrganizationSurveyProgressDto> => {
    const url = Helpers.organization.progress.specificSurvey(organizationId, survey);
    const response = await this.axios.put<OrganizationSurveyProgressDto>(url, { progress });

    return response.data as OrganizationSurveyProgressDto;
  };

  public startSurvey = async (
    organizationId: OrganizationPublicId,
    survey: SurveySlug,
  ): Promise<void> => {
    const url = Helpers.organization.progress.startSpecificSurvey(organizationId, survey);
    await this.axios.post(url);
  };

  public completeSurvey = async (
    organizationId: OrganizationPublicId,
    survey: SurveySlug,
  ): Promise<void> => {
    const url = Helpers.organization.progress.completeSpecificSurvey(organizationId, survey);
    await this.axios.post(url);
  };
}
