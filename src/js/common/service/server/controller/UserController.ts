import { AxiosInstance } from "axios";
import { ApiRoutes, ApiRouteHelpers } from "../../../root/apiRoutes";
import { SessionDto, UserDetailsDto } from "../dto/UserDto";

import { OrganizationPublicId } from "../Types";

export class UserController {
  constructor(private axios: AxiosInstance) {}

  public getSelf = async () => {
    const url = ApiRoutes.user.self;
    const response = await this.axios.get<UserDetailsDto>(url);

    return response.data as UserDetailsDto;
  };

  public getSelfSession = async () => {
    const url = ApiRoutes.user.session;
    const response = await this.axios.get<SessionDto>(url);

    return response.data as SessionDto;
  };

  public getSelfByPasswordUpdateId = async (updatePasswordId: string) => {
    const url = `/api/users/updatePassword/${updatePasswordId}`;
    const response = await this.axios.get<UserDetailsDto>(url);

    return response.data as UserDetailsDto;
  };

  public getSelfByInvitationId = async (invitationId: string) => {
    const url = `/api/users/invitations/${invitationId}`;
    const response = await this.axios.get<UserDetailsDto>(url);

    return response.data as UserDetailsDto;
  };

  public updateSelfActiveOrganization = async (organizationId: OrganizationPublicId) => {
    const url = ApiRoutes.user.activeOrganization;
    const response = await this.axios.patch<UserDetailsDto>(url, { organizationId });

    return response.data as UserDetailsDto;
  };

  public activateUser = async (
    user: string,
    organizationId: OrganizationPublicId,
    password: string,
    resetToken: string,
  ): Promise<UserDetailsDto> => {
    const url = ApiRouteHelpers.user.activate(user, organizationId);
    const response = await this.axios.post<UserDetailsDto>(url, {
      password,
      resetToken,
    });
    return response.data as UserDetailsDto;
  };

  public updateUserPassword(
    user: string,
    password: string,
    resetToken: string,
  ): Promise<UserDetailsDto> {
    const url = "/api/users/" + user + "";
    const params = {
      headers: { "Content-Type": "application/json" },
    };

    return this.axios
      .patch<UserDetailsDto>(url, { password, resetToken }, params)
      .then((response) => response.data);
  }

  public acceptInvitation(organizationId: OrganizationPublicId): Promise<UserDetailsDto> {
    const url = `/api/users/accept/${organizationId}`;
    return this.axios.post<UserDetailsDto>(url).then((response) => response.data);
  }
}
