import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import { OrganizationUserDto } from "../dto/UserDto";

import { OrganizationPublicId } from "../Types";

type CreateUserRequest = {
  email: string;
  firstName: string;
  lastName: string;
};

export class OrganizationUserController {
  constructor(private axios: AxiosInstance) {}

  public getUsers = async (organizationId: OrganizationPublicId) => {
    const url = Helpers.organizationAdminUsers(organizationId);
    const response = await this.axios.get<OrganizationUserDto[]>(url, {});

    return response.data as OrganizationUserDto[];
  };

  public inviteUser = async (
    organizationId: OrganizationPublicId,
    createUser: CreateUserRequest,
  ) => {
    const url = Helpers.organizationAdminInviteUser(organizationId);
    const response = await this.axios.post<OrganizationUserDto>(url, createUser);

    return response.data as OrganizationUserDto;
  };

  public activateUser = async (organizationId: OrganizationPublicId, email: string) => {
    const url = Helpers.organizationAdminActivateUser(organizationId, email);
    const response = await this.axios.post<OrganizationUserDto>(url);

    return response.data as OrganizationUserDto;
  };

  public deactivateUser = async (organizationId: OrganizationPublicId, email: string) => {
    const url = Helpers.organizationAdminDeactivateUser(organizationId, email);
    const response = await this.axios.post<OrganizationUserDto>(url);

    return response.data as OrganizationUserDto;
  };

  public deleteUser = async (organizationId: OrganizationPublicId, email: string) => {
    const url = Helpers.organizationAdminSpecificUser(organizationId, email);
    const response = await this.axios.delete<OrganizationUserDto>(url);

    return response.data as OrganizationUserDto;
  };
}
