import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";

import { OrganizationAuthorizationsDto } from "../dto/OrganizationDto";
import { OrganizationPublicId } from "../Types";

export class OrganizationAuthController {
  constructor(private axios: AxiosInstance) {}

  public getAuthorizations = async (organizationId: OrganizationPublicId) => {
    const url = Helpers.organization.authorization.authorizations(organizationId);
    const response = await this.axios.get<OrganizationAuthorizationsDto>(url, {});

    return response.data;
  };

  public unauthorizeGoogleAnalytics = async (organizationId: OrganizationPublicId) => {
    const url = Helpers.organization.authorization.google(organizationId);
    const response = await this.axios.delete(url, {});

    return response.data;
  };
}
