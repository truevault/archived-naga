import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import { CustomMessageSampleDto } from "../dto/CustomMessageSampleDto";
import { CustomMessageTypeDto } from "../dto/CustomMessageTypeDto";
import { DataRequestTypeDetailsDto, PrivacyRegulationName } from "../dto/DataRequestTypeDto";

import { OkReturnDto } from "../dto/OkReturnDto";
import { OrganizationComplianceChecklistItemDto } from "../dto/OrganizationComplianceChecklistItemDto";
import { OrganizationDto } from "../dto/OrganizationDto";
import { OrganizationFeatureDto } from "../dto/OrganizationFeatureDto";
import { OrganizationMailboxDto } from "../dto/OrganizationMailboxDto";
import { OrganizationSurveyQuestionDto } from "../dto/OrganizationSurveyQuestionDto";
import {
  GetCompliantStep,
  MappingProgressEnum,
  OrganizationPublicId,
  PrivacyNoticeProgressEnum,
  UUIDString,
} from "../Types";

export type UpdateOrganization = {
  name: string;
  logoUrl: string | null;
  updateRegulations: UpdateRegulations[];
};

export type UpdateOrganizationGoogleAnalyticsWebPropertyDto = {
  webProperty: string;
};

export type UpdateMessageSettings = {
  headerImageUrl: string;
  physicalAddress: string;
  messageSignature: string;
};

export type UpdateCustomField =
  | { customFieldEnabled: false }
  | {
      customFieldEnabled: true;
      customFieldLabel: string;
      customFieldHelp: string;
    };

export type UpdateFinancialIncentives = { financialIncentiveDetails: string };

export type UpdateRegulations = {
  id: UUIDString;
  enabled: boolean;
};

export type UpdateVerificationInstruction = {
  regulationSlug: string;
  instruction: string;
};

export type UpdateOrganizationSurveyQuestionDto = {
  slug: string;
  question?: string;
  answer?: string;
};

export type UpdateOrganizationComplianceChecklistItemDto = {
  slug: string;
  description?: string;
  done?: boolean;
};

export type UpdateOrganizationPrivacyNoticeProgressDto = {
  caPrivacyNoticeProgress: PrivacyNoticeProgressEnum;
  optOutPrivacyNoticeProgress: PrivacyNoticeProgressEnum;
};

export class OrganizationController {
  constructor(private axios: AxiosInstance) {}

  public getOrganization = async (organizationId) => {
    const url = Helpers.organization.root(organizationId);
    const response = await this.axios.get<OrganizationDto>(url, {});

    return response.data as OrganizationDto;
  };

  public updateOrganization = async (
    organizationId: OrganizationPublicId,
    updatedOrganization: UpdateOrganization,
  ) => {
    const url = Helpers.organization.root(organizationId);
    const response = await this.axios.put<OrganizationDto>(url, updatedOrganization);

    return response.data as OrganizationDto;
  };

  public updateGaWebProperty = async (
    organizationId: OrganizationPublicId,
    update: UpdateOrganizationGoogleAnalyticsWebPropertyDto,
  ) => {
    const url = Helpers.organization.gaWebProperty(organizationId);
    const response = await this.axios.put<OrganizationDto>(url, update);
    return response.data as OrganizationDto;
  };

  public getOrganizationMailboxes = async (organizationId) => {
    const url = Helpers.organization.mailboxes(organizationId);
    const response = await this.axios.get<OrganizationMailboxDto[]>(url);

    return response.data as OrganizationMailboxDto[];
  };

  public getDataRequestTypes = async (
    organizationId: OrganizationPublicId,
    filterVisible: boolean = false,
    regulation: PrivacyRegulationName[] | null = null,
  ) => {
    const query = new URLSearchParams(
      Object.assign(
        { filterVisible: filterVisible.toString() } as Record<string, string>,
        regulation ? { regulation: regulation.join(",") } : {},
      ),
    );
    const url = Helpers.organization.dataRequestTypes(organizationId) + `?${query.toString()}`;
    const response = await this.axios.get<DataRequestTypeDetailsDto[]>(url);

    return response.data as DataRequestTypeDetailsDto[];
  };

  public getOrganizationFeatures = async (organizationId) => {
    const url = Helpers.organization.features(organizationId);
    const response = await this.axios.get<OrganizationFeatureDto[]>(url);

    return response.data as OrganizationFeatureDto[];
  };

  public getOrganizationSurvey = async (
    organizationId: OrganizationPublicId,
    surveyName?: string,
  ): Promise<OrganizationSurveyQuestionDto[]> => {
    const url = Helpers.organization.survey(organizationId, surveyName);
    const response = await this.axios.get<OrganizationSurveyQuestionDto[]>(url);

    return response.data as OrganizationSurveyQuestionDto[];
  };

  public updateOrganizationSurvey = async (
    organizationId: OrganizationPublicId,
    surveyName: string,
    updateQuestionsDto: UpdateOrganizationSurveyQuestionDto[],
  ): Promise<OkReturnDto> => {
    const url = Helpers.organization.survey(organizationId, surveyName);
    const response = await this.axios.post<OkReturnDto>(url, updateQuestionsDto);
    return response.data as OkReturnDto;
  };

  public sendOrganizationSurvey = async (
    organizationId: OrganizationPublicId,
    surveyName: string,
  ): Promise<OkReturnDto> => {
    const url = Helpers.organization.surveySend(organizationId, surveyName);
    const response = await this.axios.post<OkReturnDto>(url);
    return response.data as OkReturnDto;
  };

  public getOrganizationComplianceChecklist = async (
    organizationId: OrganizationPublicId,
  ): Promise<OrganizationComplianceChecklistItemDto[]> => {
    const url = Helpers.organization.complianceChecklist(organizationId);
    const response = await this.axios.get<OrganizationComplianceChecklistItemDto[]>(url);

    return response.data;
  };

  public async getOrganizationMessageTypes(
    organizationId: OrganizationPublicId,
  ): Promise<CustomMessageTypeDto[]> {
    const url = Helpers.organization.customMessageType.list(organizationId);
    const response = await this.axios.get<CustomMessageTypeDto[]>(url);

    return response.data;
  }

  public async getOrganizationMessageSample(
    organizationId: OrganizationPublicId,
    slug: string,
  ): Promise<CustomMessageSampleDto> {
    const url = Helpers.organization.customMessageType.specific(organizationId, slug);
    const response = await this.axios.get<CustomMessageSampleDto>(url);

    return response.data;
  }

  public async updateOrganizationMessageSample(
    organizationId: OrganizationPublicId,
    slug: string,
    text: string,
  ): Promise<OkReturnDto> {
    const url = Helpers.organization.customMessageType.specific(organizationId, slug);
    const response = await this.axios.put<OkReturnDto>(url, { customText: text });

    return response.data;
  }

  public updateOrganizationComplianceChecklist = async (
    organizationId: OrganizationPublicId,
    updateQuestionsDto: UpdateOrganizationComplianceChecklistItemDto[],
  ): Promise<OkReturnDto> => {
    const url = Helpers.organization.complianceChecklist(organizationId);
    const response = await this.axios.post<OkReturnDto>(url, updateQuestionsDto);
    return response.data as OkReturnDto;
  };

  public updateMessageSettings = async (
    organizationId: OrganizationPublicId,
    updatedMessageSettings: UpdateMessageSettings,
  ) => {
    const url = Helpers.organization.messageSettings(organizationId);
    const response = await this.axios.put<OrganizationDto>(url, updatedMessageSettings);

    return response.data as OrganizationDto;
  };

  public updateCustomField = async (
    organizationId: OrganizationPublicId,
    updatedMessageSettings: UpdateCustomField,
  ) => {
    const url = Helpers.organization.customField(organizationId);
    const response = await this.axios.put<OrganizationDto>(url, updatedMessageSettings);

    return response.data as OrganizationDto;
  };

  public updateFinancialIncentives = async (
    organizationId: OrganizationPublicId,
    updatedIncentive: string,
  ) => {
    const url = Helpers.organization.financialIncentives(organizationId);
    const response = await this.axios.put<OrganizationDto>(url, {
      financialIncentiveDetails: updatedIncentive,
    } as UpdateFinancialIncentives);

    return response.data as OrganizationDto;
  };

  public updateVerificationInstruction = async (
    organizationId: OrganizationPublicId,
    updateVerificationInstruction: UpdateVerificationInstruction,
  ) => {
    const url = Helpers.organization.verificationInstruction(organizationId);
    const response = await this.axios.put<OrganizationDto>(url, updateVerificationInstruction);

    return response.data as OrganizationDto;
  };

  public setGetCompliantDone = async (
    organizationId: OrganizationPublicId,
    force: boolean,
  ): Promise<boolean> => {
    const url = Helpers.organization.getCompliantDone(organizationId);
    const params = { force };
    const response = await this.axios.post<boolean>(url, null, { params });
    return response.data as boolean;
  };

  public updateGetCompliantProgress = async (
    organizationId: OrganizationPublicId,
    progressStep: GetCompliantStep,
  ) => {
    const url = Helpers.organization.getCompliantProgress.specific(organizationId, progressStep);
    const response = await this.axios.put<OrganizationDto>(url);
    return response.data as OrganizationDto;
  };

  public addGetCompliantInvalidatedProgress = async (
    organizationId: OrganizationPublicId,
    invalidatedProgressStep: GetCompliantStep,
  ) => {
    const url = Helpers.organization.getCompliantInvalidatedProgress.add(
      organizationId,
      invalidatedProgressStep,
    );
    const response = await this.axios.patch<OrganizationDto>(url);
    return response.data as OrganizationDto;
  };

  public removeGetCompliantInvalidatedProgress = async (
    organizationId: OrganizationPublicId,
    invalidatedProgressStep: GetCompliantStep,
  ) => {
    const url = Helpers.organization.getCompliantInvalidatedProgress.remove(
      organizationId,
      invalidatedProgressStep,
    );
    const response = await this.axios.patch<OrganizationDto>(url);
    return response.data as OrganizationDto;
  };

  public updatePrivacyNoticeProgress = async (
    organizationId: OrganizationPublicId,
    privacyNoticeProgress: UpdateOrganizationPrivacyNoticeProgressDto,
  ) => {
    const url = Helpers.organization.privacyNoticeProgress.specific(organizationId);
    const response = await this.axios.put<OrganizationDto>(url, privacyNoticeProgress);
    return response.data as OrganizationDto;
  };

  public updateExceptionsMappingProgress = async (
    organizationId: OrganizationPublicId,
    mappingProgress: MappingProgressEnum,
  ): Promise<OkReturnDto> => {
    const url = Helpers.organization.exceptionsMappingProgress(organizationId, mappingProgress);
    const response = await this.axios.put<OkReturnDto>(url);
    return response.data as OkReturnDto;
  };

  public updateCookiesMappingProgress = async (
    organizationId: OrganizationPublicId,
    mappingProgress: MappingProgressEnum,
  ): Promise<OkReturnDto> => {
    const url = Helpers.organization.cookiesMappingProgress(organizationId, mappingProgress);
    const response = await this.axios.put<OkReturnDto>(url);
    return response.data as OkReturnDto;
  };

  public testMessage = async (organizationId: OrganizationPublicId) => {
    const url = Helpers.organization.testMessage(organizationId);
    const response = await this.axios.post(url);

    return response.data;
  };

  public deleteOrganizationMailbox = async (organizationId, mailbox) => {
    const url = Helpers.organization.mailbox(organizationId, mailbox);
    const response = await this.axios.delete(url, {});

    return response.data;
  };
}
