import { AxiosInstance } from "axios";
import _ from "lodash";
import QueryString from "qs";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import { DataRequestTypeDto } from "../dto/DataRequestTypeDto";
import {
  DataSubjectRequestDetailDto,
  DataSubjectRequestSummaryDto,
  PaginatedSubjectRequestDto,
} from "../dto/DataSubjectRequestDto";
import { Filter } from "../dto/FilterDto";
import { OkReturnDto } from "../dto/OkReturnDto";
import { SortRule } from "../dto/SortRuleDto";
import { ContactVendorRequest, UpdateVendorRequest } from "../dto/UpdateVendorRequest";
import {
  DataRequestFilter,
  DataRequestPublicId,
  DataRequestState,
  DataRequestSubstate,
  DataRequestType,
  Iso8601Date,
  OrganizationPublicId,
  RequestTypeFilter,
  UUIDString,
} from "../Types";

type SubjectStatus = any;

export type CreateDataRequest = {
  subjectFirstName: string;
  subjectLastName: string;
  dataSubjectType: string;
  dataRequestType: DataRequestType;
  requestDate: Iso8601Date;

  subjectEmailAddress?: string;
  subjectPhoneNumber?: string;
  subjectMailingAddress?: string;
  organizationCustomInput?: string;
  submissionMethod?: string;
  notes?: string;
};

export type TransitionDataRequest = {
  state: DataRequestState;
  substate?: DataRequestSubstate;
  notes?: string;
  attachment?: File;
};

export type VerifyDataRequest = {
  consumerGroupId: UUIDString;
  notes?: string;
};

export type MessageDto = {
  message: string;
  attachments?: FileList[];
};

export type UpdateRequest = {
  requestDate: Iso8601Date;
  subjectEmailAddress?: string;
  subjectPhoneNumber?: string;
  subjectMailingAddress?: string;
  contactVendorsHidden?: boolean;
  subjectFirstName: string;
  subjectLastName: string;
  submissionMethod: string;
  requestType: DataRequestType;
  dataRequestType: DataRequestTypeDto;
};

export type extendRequest = {
  notice: string;
};

export type StepConsumerGroupUpdate = {
  subjectStatus: SubjectStatus;
  consumerGroups?: string[];
};

export const filtersToParams = (filters: Filter[]) =>
  filters
    .filter((f) => Boolean(f.value))
    .reduce<Record<any, string | number>>((acc, f) => {
      acc ||= {};
      // we have filtered out `undefined` in the above filter
      acc[f.id] = f.value as string | number;
      return acc;
    }, {});

export class DataRequestController {
  constructor(private axios: AxiosInstance) {}

  public getRequests = async (
    organizationId: OrganizationPublicId,
    filter?: DataRequestFilter,
    filters?: Filter[],
    sortBy?: SortRule[],
    type: RequestTypeFilter = "all",
    page = 0,
    per = 15,
  ): Promise<PaginatedSubjectRequestDto> => {
    const url = Helpers.organizationRequests(organizationId);
    let params = { filter, type, page, per };

    if (filters) {
      const filterParams = filtersToParams(filters);
      params = Object.assign({}, params, filterParams);
    }

    params["sort"] = _.get(sortBy, "0.id");
    params["desc"] = _.get(sortBy, "0.desc");

    const response = await this.axios.get<PaginatedSubjectRequestDto>(url, {
      params,
    });
    return response.data as PaginatedSubjectRequestDto;
  };

  public getStats = async (
    organizationId: OrganizationPublicId,
  ): Promise<DataSubjectRequestSummaryDto> => {
    const url = Helpers.organizationRequestStats(organizationId);
    const response = await this.axios.get<DataSubjectRequestSummaryDto>(url);

    return response.data as DataSubjectRequestSummaryDto;
  };

  public getRequestsForConsumer = async (
    organizationId: OrganizationPublicId,
    consumerId: string,
  ): Promise<DataSubjectRequestDetailDto[]> => {
    const url = Helpers.consumer.requests(organizationId);
    const response = await this.axios.get<DataSubjectRequestDetailDto[]>(url, {
      params: { subjectEmailAddress: consumerId },
      paramsSerializer: QueryString.stringify,
    });

    return response.data as DataSubjectRequestDetailDto[];
  };

  public getRequest = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequest(organizationId, requestId);
    const response = await this.axios.get<DataSubjectRequestDetailDto>(url);

    return response.data as DataSubjectRequestDetailDto;
  };

  public getRequestClosedTemplate = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
  ): Promise<string> => {
    const url = Helpers.organizationRequestClosedTemplate(organizationId, requestId);
    const response = await this.axios.get<string>(url);
    return response.data;
  };

  public getRequestContactVendorsMessageTemplate = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
  ): Promise<string> => {
    const url = Helpers.organizationRequestContactVendorsMessageTemplate(organizationId, requestId);
    const response = await this.axios.get<string>(url);
    return response.data;
  };

  public updateRequest = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    updateRequest: UpdateRequest,
  ): Promise<DataSubjectRequestSummaryDto> => {
    const url = Helpers.organizationRequest(organizationId, requestId);
    const response = await this.axios.put<DataSubjectRequestSummaryDto>(url, updateRequest);
    return response.data as DataSubjectRequestSummaryDto;
  };

  public extendRequest = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    extendRequest: extendRequest,
  ): Promise<DataSubjectRequestSummaryDto> => {
    const url = Helpers.organizationRequestExtend(organizationId, requestId);
    const response = await this.axios.put<DataSubjectRequestSummaryDto>(url, extendRequest);
    return response.data as DataSubjectRequestSummaryDto;
  };

  public createRequest = async (
    organizationId: OrganizationPublicId,
    createRequest: CreateDataRequest,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequests(organizationId);
    const response = await this.axios.post<DataSubjectRequestDetailDto>(url, createRequest);
    return response.data as DataSubjectRequestDetailDto;
  };

  public removeRequest = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
  ): Promise<OkReturnDto> => {
    const url = Helpers.organizationRequest(organizationId, requestId);
    const response = await this.axios.delete(url);
    return response.data as OkReturnDto;
  };

  public transitionRequest = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    transitionRequest: TransitionDataRequest,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestState(organizationId, requestId);

    const formData = new FormData();

    if (transitionRequest.state) {
      formData.append("state", transitionRequest.state);
    }
    if (transitionRequest.substate) {
      formData.append("substate", transitionRequest.substate);
    }
    if (transitionRequest.notes) {
      formData.append("notes", transitionRequest.notes);
    }
    if (transitionRequest.attachment) {
      formData.append("attachment", transitionRequest.attachment);
    }

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    const response = await this.axios.put<DataSubjectRequestDetailDto>(url, formData, config);
    return response.data as DataSubjectRequestDetailDto;
  };

  public verifyRequest = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    verifyRequest: VerifyDataRequest,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestVerify(organizationId, requestId);
    const response = await this.axios.post<DataSubjectRequestDetailDto>(url, verifyRequest);
    return response.data as DataSubjectRequestDetailDto;
  };

  public updateVendor = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    updateVendorRequest: UpdateVendorRequest,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestVendor(organizationId, requestId);
    const response = await this.axios.put<DataSubjectRequestDetailDto>(url, updateVendorRequest);
    return response.data as DataSubjectRequestDetailDto;
  };

  public autodeleteGoogleAnalytics = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
  ) => {
    const url = Helpers.organizationRequestAutodeleteGoogleAnalytics(organizationId, requestId);
    await this.axios.post(url);
  };

  public contactVendors = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    updateVendorRequest: ContactVendorRequest,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestContactVendors(organizationId, requestId);
    const response = await this.axios.put<DataSubjectRequestDetailDto>(url, updateVendorRequest);
    return response.data as DataSubjectRequestDetailDto;
  };

  public contactRequestor = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    attachments: File[] = [],
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestContactRequestor(organizationId, requestId);

    const formData = new FormData();

    if (attachments.length > 0) {
      attachments.forEach((file) => formData.append("attachment", file));
    }

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    const response = await this.axios.put<DataSubjectRequestDetailDto>(url, formData, config);
    return response.data as DataSubjectRequestDetailDto;
  };

  public createNote = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    note: MessageDto,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestNote(organizationId, requestId);
    const formData = new FormData();
    formData.append("message", note.message);
    if (note.attachments) {
      // attachments is an array containing one FileList, containing the Files to be attached
      Array.from(note.attachments[0]).forEach((f) => {
        formData.append("attachments[]", f);
      });
    }

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    const response = await this.axios.post<DataSubjectRequestDetailDto>(url, formData, config);
    return response.data as DataSubjectRequestDetailDto;
  };

  public reopen = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestReopen(organizationId, requestId);
    const response = await this.axios.put<DataSubjectRequestDetailDto>(url);
    return response.data as DataSubjectRequestDetailDto;
  };

  public stepConsumerGroup = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    payload: StepConsumerGroupUpdate,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestStepConsumerGroup(organizationId, requestId);
    const response = await this.axios.put<DataSubjectRequestDetailDto>(url, payload);
    return response.data as DataSubjectRequestDetailDto;
  };

  public setSubjectStatus = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
    payload: StepConsumerGroupUpdate,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestSetSubjectStatus(organizationId, requestId);
    const response = await this.axios.put<DataSubjectRequestDetailDto>(url, payload);
    return response.data as DataSubjectRequestDetailDto;
  };

  public stepRequestForward = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestStepForward(organizationId, requestId);
    const response = await this.axios.get<DataSubjectRequestDetailDto>(url);
    return response.data as DataSubjectRequestDetailDto;
  };

  public stepRequestBack = async (
    organizationId: OrganizationPublicId,
    requestId: DataRequestPublicId,
  ): Promise<DataSubjectRequestDetailDto> => {
    const url = Helpers.organizationRequestStepBack(organizationId, requestId);
    const response = await this.axios.get<DataSubjectRequestDetailDto>(url);
    return response.data as DataSubjectRequestDetailDto;
  };
}
