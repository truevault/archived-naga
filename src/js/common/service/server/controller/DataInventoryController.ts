import { AxiosInstance } from "axios";
import { ApiRouteHelpers } from "../../../root/apiRoutes";
import { OrganizationPublicId, UUIDString } from "../Types";
import {
  OrgPersonalInformationSnapshotDto,
  PersonalInformationCollectedDto,
  PersonalInformationExchangedDto,
} from "../dto/PersonalInformationOptionsDto";
import { OkReturnDto } from "../dto/OkReturnDto";

export type UpdatePersonalInformationCollected = {
  personalInformationCategories?: UUIDString[];
  personalInformationSources?: UUIDString[];
  customPersonalInformationSources?: string[];
  businessPurposes?: UUIDString[];
};

export type UpdatePersonalInformationExchanged = {
  dataSubjectTypeId: UUIDString;
  personalInformationCategories: UUIDString[];
  noDataMapping: boolean;
};

export class DataInventoryController {
  constructor(private axios: AxiosInstance) {}

  public getSnapshot = async (
    organizationId: OrganizationPublicId,
  ): Promise<OrgPersonalInformationSnapshotDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.snapshot(organizationId);
    const response = await this.axios.get(url);
    return response.data as OrgPersonalInformationSnapshotDto;
  };

  public getDefaultPersonalInformationCollected = async (
    organizationId: OrganizationPublicId,
  ): Promise<PersonalInformationCollectedDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.default(organizationId);
    const response = await this.axios.get<PersonalInformationCollectedDto>(url);
    return response.data as PersonalInformationCollectedDto;
  };

  public getPersonalInformationCollected = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
  ): Promise<PersonalInformationCollectedDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.batch(
      organizationId,
      dataSubjectTypeId,
    );
    const response = await this.axios.get<PersonalInformationCollectedDto>(url);
    return response.data as PersonalInformationCollectedDto;
  };

  public setPersonalInformationCollected = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    updatePersonalInformationCollected: UpdatePersonalInformationCollected,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.batch(
      organizationId,
      dataSubjectTypeId,
    );
    const response = await this.axios.put(url, updatePersonalInformationCollected);
    return response.data;
  };

  public deleteBatchPersonalInformationCategoryCollected = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    categoryIds: UUIDString[],
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.clearInternallyStored(
      organizationId,
      dataSubjectTypeId,
    );
    const payload = {};
    payload["data"] = categoryIds;

    const response = await this.axios.delete(url, payload);
    return response.data;
  };

  public addPersonalInformationCategoryCollected = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    categoryId: UUIDString,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.specificCategory(
      organizationId,
      dataSubjectTypeId,
      categoryId,
    );
    const response = await this.axios.put(url, {});
    return response.data;
  };

  public removePersonalInformationCategoryCollected = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    categoryId: UUIDString,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.specificCategory(
      organizationId,
      dataSubjectTypeId,
      categoryId,
    );
    const response = await this.axios.delete(url);
    return response.data;
  };

  public addPersonalInformationSourceCollected = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    sourceId: UUIDString,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.specificSource(
      organizationId,
      dataSubjectTypeId,
      sourceId,
    );
    const response = await this.axios.put(url);
    return response.data;
  };

  public removePersonalInformationSourceCollected = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    sourceId: UUIDString,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.specificSource(
      organizationId,
      dataSubjectTypeId,
      sourceId,
    );
    const response = await this.axios.delete(url);
    return response.data;
  };

  public addPersonalInformationCustomSource = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    sourceName: string,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.customSource(
      organizationId,
      dataSubjectTypeId,
      sourceName,
    );
    const response = await this.axios.put(url);
    return response.data;
  };

  public removePersonalInformationCustomSource = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    sourceName: string,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.customSource(
      organizationId,
      dataSubjectTypeId,
      sourceName,
    );
    const response = await this.axios.delete(url);
    return response.data;
  };

  public addBusinessPurposeCollected = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    purposeId: UUIDString,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.specificPurpose(
      organizationId,
      dataSubjectTypeId,
      purposeId,
    );
    const response = await this.axios.put(url);
    return response.data;
  };

  public removeBusinessPurposeCollected = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    purposeId: UUIDString,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.collection.specificPurpose(
      organizationId,
      dataSubjectTypeId,
      purposeId,
    );
    const response = await this.axios.delete(url);
    return response.data;
  };

  public getPersonalInformationExchanged = async (
    organizationId: OrganizationPublicId,
  ): Promise<PersonalInformationExchangedDto[]> => {
    const url = ApiRouteHelpers.organization.dataInventory.exchange.root(organizationId);
    const response = await this.axios.get<PersonalInformationExchangedDto[]>(url);
    return response.data as PersonalInformationExchangedDto[];
  };

  public getPersonalInformationExchangedConsumerGroup = async (
    organizationId: OrganizationPublicId,
    consumerGroupId: UUIDString,
  ): Promise<PersonalInformationExchangedDto[]> => {
    const url = ApiRouteHelpers.organization.dataInventory.exchange.specificConsumerGroup(
      organizationId,
      consumerGroupId,
    );
    const response = await this.axios.get<PersonalInformationExchangedDto[]>(url);
    return response.data as PersonalInformationExchangedDto[];
  };

  public getPersonalInformationExchangedVendor = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
  ): Promise<PersonalInformationExchangedDto[]> => {
    const url = ApiRouteHelpers.organization.dataInventory.exchange.specificVendor(
      organizationId,
      vendorId,
    );

    const response = await this.axios.get<PersonalInformationExchangedDto[]>(url);
    return response.data as PersonalInformationExchangedDto[];
  };

  public setPersonalInformationExchanged = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
    updatePersonalInformationDisclosed: UpdatePersonalInformationExchanged[],
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.dataInventory.exchange.specificVendor(
      organizationId,
      vendorId,
    );
    const response = await this.axios.put(url, updatePersonalInformationDisclosed);
    return response.data as OkReturnDto;
  };
}
