import { AxiosInstance } from "axios";
import _ from "lodash";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import { PaginatedGdprCookieConsentDto } from "../dto/GdprCookieConsentDto";
import { SortRule } from "../dto/SortRuleDto";

import { OrganizationPublicId } from "../Types";

export type TaskFilter = "active" | "complete";

export class GdprCookieConsentController {
  constructor(private axios: AxiosInstance) {}

  public getCookieConsent = async (
    organizationId: OrganizationPublicId,
    sortBy?: SortRule[],
    page = 0,
    per = 15,
  ): Promise<PaginatedGdprCookieConsentDto> => {
    const url = Helpers.organization.cookieConsent.root(organizationId);
    let params = { page, per };

    params["sort"] = _.get(sortBy, "0.id");
    params["desc"] = _.get(sortBy, "0.desc");

    const response = await this.axios.get<PaginatedGdprCookieConsentDto>(url, { params });
    return response.data;
  };
}
