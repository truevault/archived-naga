import { AxiosInstance } from "axios";
import { ApiRouteHelpers } from "../../../root/apiRoutes";
import { CollectionGroup } from "../../../models";
import {
  MappingProgressEnum,
  OrganizationPublicId,
  PersonalInformationType,
  PrivacyNoticeProgressEnum,
  UUIDString,
} from "../Types";
import { OkReturnDto } from "../dto/OkReturnDto";
import {
  CollectionGroupDataRecipientDto,
  CollectionGroupDetailsDto,
} from "../dto/CollectionGroupDto";
import { ProcessingRegion } from "../dto/OrganizationDataRecipientDto";
import QueryString from "qs";

type VendorMappingProgressDto = {
  mappingProgress: MappingProgressEnum;
  vendorIds: UUIDString[];
};

export const AllCollectionGroupTypes = [
  "BUSINESS_TO_CONSUMER",
  "BUSINESS_TO_BUSINESS",
  "EMPLOYMENT",
] as const;
export type CollectionGroupType = typeof AllCollectionGroupTypes[number];

export type CreateCollectionGroup = {
  name: CollectionGroup;
  description: string;
  enabled: boolean;
  privacyCenterEnabled: boolean;
  collectionGroupType: CollectionGroupType;
  gdprDataSubject: boolean;
  processingRegions?: ProcessingRegion[];
};

export type UpdateCollectionGroup = {
  name?: CollectionGroup;
  description?: string;
  enabled?: boolean;
  privacyCenterEnabled?: boolean;
  collectionGroupType?: CollectionGroupType;
  gdprEeaUkDataCollection?: boolean;
  processingRegions?: ProcessingRegion[];
  privacyNoticeIntroText?: string;
};

export class CollectionGroupsController {
  constructor(private axios: AxiosInstance) {}

  public getCollectionGroups = async (
    organizationId: OrganizationPublicId,
    collectionGroupTypes: CollectionGroupType[] | undefined = undefined,
    excludeCollectionGroupTypes: CollectionGroupType[] | undefined = undefined,
  ): Promise<CollectionGroupDetailsDto[]> => {
    const url = ApiRouteHelpers.organization.collectionGroups.root(organizationId);
    const response = await this.axios.get<CollectionGroupDetailsDto[]>(url, {
      params: { collectionGroupTypes, excludeCollectionGroupTypes },
      paramsSerializer: (q) => QueryString.stringify(q, { arrayFormat: "comma" }),
    });
    return response.data as CollectionGroupDetailsDto[];
  };

  public createCollectionGroup = async (
    organizationId: OrganizationPublicId,
    createDataSubjectType: CreateCollectionGroup,
  ): Promise<CollectionGroupDetailsDto> => {
    const url = ApiRouteHelpers.organization.collectionGroups.root(organizationId);
    const response = await this.axios.post<CollectionGroupDetailsDto>(url, createDataSubjectType);
    return response.data as CollectionGroupDetailsDto;
  };

  public createDefaultCollectionGroups = async (orgId: OrganizationPublicId): Promise<void> => {
    const url = ApiRouteHelpers.organization.collectionGroups.default(orgId);
    await this.axios.post<CollectionGroupDetailsDto>(url);
  };

  public deleteDataSubjectType = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
  ): Promise<unknown> => {
    const url = ApiRouteHelpers.organization.collectionGroups.specific(
      organizationId,
      dataSubjectTypeId,
    );
    await this.axios.delete(url);
    return {};
  };

  public updateCollectionGroup = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    updateDto: UpdateCollectionGroup,
  ): Promise<CollectionGroupDetailsDto> => {
    const url = ApiRouteHelpers.organization.collectionGroups.specific(
      organizationId,
      dataSubjectTypeId,
    );
    const response = await this.axios.put<CollectionGroupDetailsDto>(url, updateDto);
    return response.data as CollectionGroupDetailsDto;
  };

  public addVendor = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    vendorId: UUIDString,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.collectionGroups.vendor(
      organizationId,
      dataSubjectTypeId,
      vendorId,
    );
    const response = await this.axios.put<OkReturnDto>(url);
    return response.data as OkReturnDto;
  };

  public removeVendor = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    vendorId: UUIDString,
  ): Promise<OkReturnDto> => {
    const url = ApiRouteHelpers.organization.collectionGroups.vendor(
      organizationId,
      dataSubjectTypeId,
      vendorId,
    );
    const response = await this.axios.delete<OkReturnDto>(url);
    return response.data as OkReturnDto;
  };

  public updateMappingProgress = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    personalInformationType: PersonalInformationType,
    mappingProgress: MappingProgressEnum,
  ): Promise<CollectionGroupDetailsDto> => {
    const url = ApiRouteHelpers.organization.collectionGroups.mappingProgress(
      organizationId,
      dataSubjectTypeId,
      personalInformationType,
      mappingProgress,
    );
    const response = await this.axios.put<CollectionGroupDetailsDto>(url);
    return response.data as CollectionGroupDetailsDto;
  };

  public updateVendorMappingProgress = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    vendorMappingProgressDto: VendorMappingProgressDto,
  ): Promise<CollectionGroupDataRecipientDto[]> => {
    const url = ApiRouteHelpers.organization.collectionGroups.vendorMappingProgress(
      organizationId,
      dataSubjectTypeId,
    );
    const response = await this.axios.put<CollectionGroupDataRecipientDto[]>(
      url,
      vendorMappingProgressDto,
    );
    return response.data as CollectionGroupDataRecipientDto[];
  };

  public updatePrivacyNoticeProgress = async (
    organizationId: OrganizationPublicId,
    dataSubjectTypeId: UUIDString,
    privacyNoticeProgress: PrivacyNoticeProgressEnum,
  ): Promise<CollectionGroupDetailsDto> => {
    const url = ApiRouteHelpers.organization.collectionGroups.privacyNoticeProgress(
      organizationId,
      dataSubjectTypeId,
      privacyNoticeProgress,
    );
    const response = await this.axios.put<CollectionGroupDetailsDto>(url);
    return response.data as CollectionGroupDetailsDto;
  };
}
