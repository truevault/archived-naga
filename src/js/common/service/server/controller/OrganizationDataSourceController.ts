import { AxiosInstance } from "axios";
import QueryString from "qs";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import {
  AddOrganizationDataSourceDto,
  CollectionContext,
  OrganizationDataSourceDto,
} from "../dto/OrganizationDataRecipientDto";
import { OrganizationPublicId, UUIDString } from "../Types";

export class OrganizationDataSourceController {
  constructor(private axios: AxiosInstance) {}

  public getOrgDataSources = async (
    organizationId: OrganizationPublicId,
  ): Promise<OrganizationDataSourceDto[]> => {
    const url = Helpers.organization.dataSources.root(organizationId);
    const response = await this.axios.get<OrganizationDataSourceDto[]>(url, {});

    return response.data as OrganizationDataSourceDto[];
  };

  public addOrgDataSource = async (
    organizationId: OrganizationPublicId,
    addSource: AddOrganizationDataSourceDto,
  ): Promise<OrganizationDataSourceDto> => {
    const url = Helpers.organization.dataSources.root(organizationId);
    const response = await this.axios.post<OrganizationDataSourceDto>(url, addSource);
    return response.data as OrganizationDataSourceDto;
  };

  public removeOrgDataSource = async (
    organizationId: OrganizationPublicId,
    vendorId: UUIDString,
    collectionContext?: CollectionContext[],
  ): Promise<void> => {
    const url = Helpers.organization.dataSources.specific(organizationId, vendorId);
    await this.axios.delete<void>(url, {
      params: { collectionContext },
      paramsSerializer: (q) => QueryString.stringify(q, { arrayFormat: "comma" }),
    });
  };
}
