import { AxiosInstance } from "axios";
// import { AtlasUserDto } from "../generated";

import { ApiRoutes } from "../../../root/apiRoutes";
import { UserDetailsDto } from "../dto/UserDto";

// On the backend this isn't a standard Controller, so it cannot be automatically generated.
export class LoginController {
  constructor(private axios: AxiosInstance) {}

  public login(username: string, password: string): Promise<UserDetailsDto> {
    const url = "/api/login";
    const transformRequest = (jsonData = {}) =>
      Object.keys(jsonData)
        .map((x) => `${encodeURIComponent(x)}=${encodeURIComponent(jsonData[x])}`)
        .join("&");
    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      transformRequest,
    };
    const body = { username, password };

    return this.axios.post<UserDetailsDto>(url, body, config).then((response) => response.data);
  }

  public logout(): Promise<any> {
    return this.axios.get(ApiRoutes.user.logout);
  }

  public resetPassword(email: string): Promise<void> {
    const url = "/api/users/forgotPassword";
    const params = {
      headers: { "Content-Type": "text/plain" },
    };

    return this.axios.post(url, email, params).then((response) => response.data);
  }
}
