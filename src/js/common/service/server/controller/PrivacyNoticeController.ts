import { AxiosInstance } from "axios";
import { ApiRouteHelpers } from "../../../root/apiRoutes";

import { OrganizationPublicId, UUIDString } from "../Types";

export type PrivacyNoticeDto = {
  notice: string;
  label: string;
  url: string;
};

export class PrivacyNoticeController {
  constructor(private axios: AxiosInstance) {}

  public getCaPrivacyNotice = async (
    organizationId: OrganizationPublicId,
  ): Promise<PrivacyNoticeDto> => {
    const url = ApiRouteHelpers.organization.privacyNotice.ca(organizationId);
    const resp = await this.axios.get<PrivacyNoticeDto>(url);
    return resp.data;
  };

  public getOptOutPrivacyNotice = async (
    organizationId: OrganizationPublicId,
  ): Promise<PrivacyNoticeDto> => {
    const url = ApiRouteHelpers.organization.privacyNotice.optOut(organizationId);
    const resp = await this.axios.get<PrivacyNoticeDto>(url);
    return resp.data;
  };

  public getConsumerGroupPrivacyNotice = async (
    organizationId: OrganizationPublicId,
    consumerGroupId: UUIDString,
  ): Promise<PrivacyNoticeDto> => {
    const url = ApiRouteHelpers.organization.privacyNotice.consumerGroup(
      organizationId,
      consumerGroupId,
    );
    const resp = await this.axios.get<PrivacyNoticeDto>(url);
    return resp.data;
  };
}
