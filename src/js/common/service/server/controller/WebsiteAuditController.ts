import { AxiosInstance } from "axios";
import { ApiRouteHelpers as Helpers } from "../../../root/apiRoutes";
import { WebsiteAuditResultDto } from "../dto/WebsiteAuditDto";
import { OrganizationPublicId } from "../Types";

export class WebsiteAuditController {
  constructor(private axios: AxiosInstance) {}

  public startWebsiteAudit = async (
    organizationId: OrganizationPublicId,
    scanUrl: String,
  ): Promise<WebsiteAuditResultDto> => {
    const url = Helpers.organization.websiteAudit.root(organizationId);
    const response = await this.axios.post(url, { websiteUrl: scanUrl });
    return response.data;
  };

  public getWebsiteAuditResult = async (
    organizationId: OrganizationPublicId,
  ): Promise<WebsiteAuditResultDto> => {
    const url = Helpers.organization.websiteAudit.root(organizationId);
    const response = await this.axios.get<WebsiteAuditResultDto>(url);
    return response.data;
  };

  public resetWebsiteAudit = async (
    organizationId: OrganizationPublicId,
  ): Promise<WebsiteAuditResultDto> => {
    const url = Helpers.organization.websiteAudit.reset(organizationId);
    const response = await this.axios.post<WebsiteAuditResultDto>(url);
    return response.data;
  };
}
