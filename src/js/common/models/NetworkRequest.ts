import moment from "moment";

import { Iso8601Date } from "../service/server/Types";

export type NetworkRequestState = {
  started: boolean;
  running: boolean;
  success: boolean;
  error: boolean;

  startedAt?: Iso8601Date;
  errorMessage?: string;
  errorStatus?: string;
  successMessage?: string;
};

const unstarted = (): NetworkRequestState => ({
  started: false,
  running: false,
  success: false,
  error: false,
});

const start = (): NetworkRequestState => ({
  started: true,
  startedAt: moment().toISOString(),
  success: false,
  running: true,
  error: false,
});

const success = (original?: NetworkRequestState, message?: string): NetworkRequestState => ({
  started: true,
  startedAt: (original && original.startedAt) || moment().toISOString(),
  success: true,
  running: false,
  error: false,
  successMessage: message,
});

const failed = (
  original: NetworkRequestState,
  message: string,
  status: string | undefined = undefined,
): NetworkRequestState => ({
  started: true,
  startedAt: (original && original.startedAt) || moment().toISOString(),
  success: false,
  running: false,
  error: true,
  errorMessage: message,
  errorStatus: status,
});

const isFinished = (r: NetworkRequestState) => r.started && !r.running;

const areFinished = (...requests: NetworkRequestState[]) => requests.every((r) => isFinished(r));
const areUnstarted = (...requests: NetworkRequestState[]) => requests.every((r) => !r.started);

const anyFailed = (...requests: NetworkRequestState[]) => requests.some((r) => r.error);
const anyRunning = (...requests: NetworkRequestState[]) => requests.some((r) => r.running);

export const NetworkRequest = {
  start,
  success,
  failed,
  unstarted,

  isFinished,
  areFinished,
  areUnstarted,
  anyFailed,
  anyRunning,
};
