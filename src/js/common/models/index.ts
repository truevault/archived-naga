import {
  DataSubjectRequestDetailDto,
  DataSubjectRequestSummaryDto,
} from "../service/server/dto/DataSubjectRequestDto";
import { DataSubjectRequestEventEmbeddedDto } from "../service/server/dto/DataSubjectRequestEventDto";
import {
  CollectionGroupDto,
  CollectionGroupDetailsDto,
} from "../service/server/dto/CollectionGroupDto";
import {
  OrganizationDataRecipientDto,
  OrganizationDataRecipientPlatformDto,
  OrganizationDataSourceDto,
} from "../service/server/dto/OrganizationDataRecipientDto";
import { OrganizationDto } from "../service/server/dto/OrganizationDto";
import { OrganizationUserDto, SessionDto, UserDetailsDto } from "../service/server/dto/UserDto";

// trivial types are simply exported here
export type Organization = OrganizationDto;
export type OrganizationUser = OrganizationUserDto;
export type OrganizationVendor = OrganizationDataRecipientDto;
export type OrganizationPlatformVendor = OrganizationDataRecipientPlatformDto;
export type OrganizationDataSource = OrganizationDataSourceDto;

export type UserDetails = UserDetailsDto;
export type CollectionGroup = CollectionGroupDto;
export type DataSubjectTypeDetails = CollectionGroupDetailsDto;

export type DataRequestSummary = DataSubjectRequestSummaryDto;
export type DataRequestDetail = DataSubjectRequestDetailDto;

export type DataRequestEvent = DataSubjectRequestEventEmbeddedDto;

export type Session = SessionDto;
export type { NetworkRequestState } from "./NetworkRequest";

export { NetworkRequest } from "./NetworkRequest";
