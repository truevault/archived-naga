import React, { ReactNode, useRef, useState } from "react";
import ReactDOM from "react-dom";
import clsx from "clsx";
import { PolarisGlobals } from "../../declaration";
import { Collapse, IconButton, Tooltip } from "@mui/material";
import { ExpandLess, ExpandMore } from "@mui/icons-material";

export const PageContentFooterPortal: React.FC<{ visible: boolean }> = ({
  children,
  visible = false,
}) => {
  const footerTarget = document.getElementById("page-footer");

  if (!footerTarget || !visible) {
    return null;
  }

  return ReactDOM.createPortal(
    <div className="page-content--footer">{children}</div>,
    footerTarget,
  );
};

const usePageContentRef = (): [React.LegacyRef<HTMLDivElement>, PolarisGlobals] => {
  const pageContentRef = useRef<HTMLDivElement>(null);

  const scrollPolarisTo = (offset: number) => {
    // Only scroll if delta is outside fudge-factor
    if (
      pageContentRef.current?.scrollTop &&
      Math.abs(pageContentRef.current?.scrollTop - offset) > 10
    ) {
      pageContentRef.current?.scrollTo({ top: offset, behavior: "smooth" });
    }
  };

  const scrollSelectorIntoView = (selector: string) => {
    if (!pageContentRef.current) {
      return;
    }

    const elem = pageContentRef.current.querySelector(selector);
    if (!elem) {
      return;
    }

    const offset = Math.max((elem as HTMLElement).offsetTop - 20);
    scrollPolarisTo(offset);
  };

  const globalFns: PolarisGlobals = { scrollPolarisTo, scrollSelectorIntoView };

  return [pageContentRef, globalFns];
};

type PageContentProps = {
  children?: ReactNode;
  footer?: ReactNode;
  fullWidth?: boolean;
  fullWidthHeader?: ReactNode;
  admin?: boolean;
  hero?: ReactNode;
};

export const PageContent: React.FC<PageContentProps> = ({
  children = null,
  hero,
  fullWidth,
  fullWidthHeader,
  footer,
  admin,
}) => {
  const [pageContentRef, globalFns] = usePageContentRef();
  const [pageHeroCollapsed, setPageHeroCollapsed] = useState(false);
  window.polaris = globalFns;

  return (
    <div ref={pageContentRef} className={clsx("page-content", { "page-content--admin": admin })}>
      {fullWidthHeader}

      <div id="page-hero">
        {hero && (
          <div
            className={clsx("page-content--hero", {
              "page-content--hero--collapsed": pageHeroCollapsed,
            })}
          >
            <div className="page-content--hero-inner">
              <Collapse in={!pageHeroCollapsed}>{hero}</Collapse>
            </div>

            <Tooltip title={pageHeroCollapsed ? "Expand" : "Collapse"}>
              <IconButton
                className="page-content--hero-collapse-button"
                onClick={() => setPageHeroCollapsed(!pageHeroCollapsed)}
              >
                {pageHeroCollapsed ? <ExpandLess /> : <ExpandMore />}
              </IconButton>
            </Tooltip>
          </div>
        )}
      </div>

      <div className={clsx("page-content--inner", { "page-content--full-width": fullWidth })}>
        {children}
      </div>

      <div id="page-footer">{footer && <div className="page-content--footer">{footer}</div>}</div>
    </div>
  );
};

type GetCompliantPageContentProps = {
  children?: ReactNode;
  header?: ReactNode;
  footer?: ReactNode;
  fullWidth?: boolean;
  noNav?: boolean;
  disablePadding?: boolean;
};
export const GetCompliantPageContent: React.FC<GetCompliantPageContentProps> = ({
  children = null,
  header = null,
  footer = null,
  fullWidth = false,
  noNav = false,
  disablePadding = false,
}) => {
  const [pageContentRef, globalFns] = usePageContentRef();
  window.polaris = globalFns;

  return (
    <div className="page-content" ref={pageContentRef}>
      {header && (
        <div className={clsx("page-content--header", { "page-content--nonav": noNav })}>
          {header}
        </div>
      )}

      <div
        className={clsx("page-content--inner", {
          "page-content--full-width": fullWidth,
          "page-content--no-padding": disablePadding,
          "page-content--nonav": noNav,
        })}
      >
        {children}
      </div>

      <div id="page-footer">{footer && <div className="page-content--footer">{footer}</div>}</div>
    </div>
  );
};

GetCompliantPageContent.displayName = "GetCompliantPageContent";
