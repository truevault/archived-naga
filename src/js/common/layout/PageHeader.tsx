import clsx from "clsx";
import React, { ReactNode } from "react";

export type PageHeaderProps = {
  header: string;
  description?: string;
  cls?: string;
  children?: ReactNode;
};

export const PageHeader = ({ header, description, cls, children = null }: PageHeaderProps) => {
  return (
    <div className={clsx("page-header", cls)}>
      <div className="page-header__header">
        <h1>{header}</h1>
        {description && <p>{description}</p>}
      </div>

      {children && <div className="page-header__actions">{children}</div>}
    </div>
  );
};
