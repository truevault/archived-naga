import clsx from "clsx";
import React from "react";

export const PageWrapper = ({ children, cls = "" }) => (
  <div className={clsx("page", cls)}>{children}</div>
);
