import React from "react";

import { useState } from "react";

type OmniBarContextProps = {
  open: boolean;
  openOmniBar: (hooks: OmniBarHooks) => void;
  closeOmniBar: (data?: any) => void;
};

type OmniBarHooks = { onClose?: (data) => void };

export const OmniBarContext = React.createContext<OmniBarContextProps>({
  open: false,
  openOmniBar: (_hooks: OmniBarHooks) => {},
  closeOmniBar: (_data?: any) => {},
});

export const OmniBarConsumer = OmniBarContext.Consumer;

export const OmniBarProvider = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [hooks, setHooks] = useState({} as OmniBarHooks);

  const closeOmniBar = (data: any) => {
    setOpen(false);
    hooks.onClose && hooks.onClose(data);
  };

  const openOmniBar = (hooks: OmniBarHooks) => {
    setOpen(true);
    setHooks(hooks);
  };

  return (
    <OmniBarContext.Provider value={{ open, openOmniBar, closeOmniBar }}>
      {children}
    </OmniBarContext.Provider>
  );
};
