import { useState } from "react";
import { Copy } from "../copy";
import { UserDetails } from "../models";
import { NetworkRequestState } from "../models/NetworkRequest";
import { Api } from "../service/Api";
import { SessionDto } from "../service/server/dto/UserDto";
import { useDataRequest } from "./api";
import { useInterval } from "./useInterval";

//
// Self request
//
export const useSelf = (): [UserDetails, NetworkRequestState] => {
  const { result: self, request } = useDataRequest({
    queryKey: ["self"],
    api: Api.user.getSelf,
    cacheTime: 0,
    messages: { defaultError: Copy.Requests.GetSelf.Error },
  });

  //@ts-ignore: disable-next-line Self should never be null unless not signed in so we don't care
  return [self, request];
};

//
// Session Request
//
const SESSION_CHECK_INTERVAL = 60000;

export const useSession = (): [SessionDto, NetworkRequestState] => {
  const [started, setStarted] = useState(false);

  const {
    result: self,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["self", "session"],
    api: Api.user.getSelfSession,
    messages: { defaultError: Copy.Requests.GetSelf.Error },
    notReady: !started,
  });

  // issue the session request on a timer
  useInterval(() => {
    setStarted(true);
    refresh();
  }, SESSION_CHECK_INTERVAL);

  //@ts-ignore: disable-next-line Self should never be null unless not signed in so we don't care
  return [self, request];
};
