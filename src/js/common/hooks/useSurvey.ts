import _ from "lodash";
import { useCallback, useMemo } from "react";
import { safeJsonParse } from "../../polaris/surveys/util";
import { Api } from "../service/Api";
import { UpdateOrganizationSurveyQuestionDto } from "../service/server/controller/OrganizationController";
import { OkReturnDto } from "../service/server/dto/OkReturnDto";
import { OrganizationPublicId } from "../service/server/Types";
import { useActionRequest, useDataRequest } from "./api";

export type SurveyAnswers = Record<string, string | undefined>;
export type SurveyResponses = SurveyAnswers;
export type ParsedSurveyAnswers = Record<string, boolean | null>;
export type SurveyAnswerFn = (slug: string, answer: string, question?: string) => Promise<void>;

export const useSurvey = (
  orgId: OrganizationPublicId,
  slug: string | undefined,
  onAnswer?: () => Promise<void>,
) => {
  const api = useCallback(() => Api.organization.getOrganizationSurvey(orgId, slug), [orgId, slug]);

  const {
    result: remoteSurvey,
    request: surveyRequest,
    refresh: refreshSurvey,
  } = useDataRequest({
    queryKey: ["survey", orgId, slug],
    api,
    notReady: slug == undefined,
  });

  const updateApi = useCallback(
    ({ questions }: { questions: UpdateOrganizationSurveyQuestionDto[] }) => {
      // Doesn't seem to make sense to update a survey with no slug
      if (slug) return Api.organization.updateOrganizationSurvey(orgId, slug, questions);
      return Promise.resolve<OkReturnDto>({ ok: false });
    },
    [orgId, slug],
  );

  const { fetch: updateSurveyQuestions, request: updateRequest } = useActionRequest({
    api: updateApi,
    allowConcurrent: true,
    notReady: slug == undefined,
    onSuccess: async () => {
      await refreshSurvey();

      if (onAnswer) {
        await onAnswer();
      }
    },
  });

  const setAnswer = useCallback<SurveyAnswerFn>(
    async (slug: string, answer: string, question?: string) => {
      await updateSurveyQuestions({
        questions: [
          {
            slug,
            question,
            answer,
          },
        ],
      });
    },
    [updateSurveyQuestions],
  );

  const answers = useMemo<SurveyAnswers>(
    () => (remoteSurvey ? Object.fromEntries(remoteSurvey.map((q) => [q.slug, q.answer])) : {}),
    [remoteSurvey],
  );

  return {
    answers,
    setAnswer,
    remoteSurvey,
    surveyRequest,
    updateRequest,
    updateSurveyQuestions,
    refreshSurvey,
  };
};

export const useParsedSurveyAnswers = (surveyAnswers: SurveyAnswers) =>
  useMemo<ParsedSurveyAnswers>(
    () =>
      _.transform(
        surveyAnswers,
        (res, val, key) => {
          res[key] = safeJsonParse(val);
        },
        {},
      ),
    [surveyAnswers],
  );
