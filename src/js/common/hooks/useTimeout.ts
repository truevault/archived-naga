import React from "react";

// Sourced from https://www.joshwcomeau.com/snippets/react-hooks/use-timeout/
export default function useTimeout(callback, delay) {
  const timeoutRef = React.useRef<number | null>(null);
  const savedCallback = React.useRef(callback);
  React.useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);
  React.useEffect(() => {
    const tick = () => savedCallback.current();
    if (typeof delay === "number") {
      timeoutRef.current = window.setTimeout(tick, delay);
      return () => {
        if (timeoutRef.current !== null) {
          window.clearTimeout(timeoutRef.current);
        }
      };
    }
  }, [delay]);
  return timeoutRef;
}
