import _ from "lodash";
import { useMemo } from "react";
import { CollectionGroupType } from "../service/server/controller/CollectionGroupsController";
import { CollectionGroupDetailsDto } from "../service/server/dto/CollectionGroupDto";
import {
  CategoryDto,
  CollectionDto,
  V2DataMapDto,
  DataMapReceivedUpdateOperation,
  DataMapDto,
  DisclosureDto,
  groupsFromPIC,
  PICGroup,
  SourceDto,
} from "../service/server/dto/DataMapDto";
import {
  OrganizationDataRecipientDto,
  OrganizationDataSourceDto,
} from "../service/server/dto/OrganizationDataRecipientDto";
import { UUIDString } from "../service/server/Types";

export type DataMapCollectionDetails = {
  collectionDetails?: CollectionDto;
  collectedPic: CategoryDto[];
  collectedPicGroups: PICGroup[];
};

const DEFAULT_EXCLUDE_GROUP_TYPE: CollectionGroupType[] = ["EMPLOYMENT"];

export const useAllCollectedPIC = (
  dataMap: DataMapDto | undefined,
  excludeCollectionGroupTypes: CollectionGroupType[] = DEFAULT_EXCLUDE_GROUP_TYPE,
  onlyCollectionGroupTypes?: CollectionGroupType[],
) => {
  const collectedItems: Set<string> = useMemo(() => {
    if (!dataMap) {
      return new Set<string>([]);
    }

    let collected = dataMap.collection;

    if (excludeCollectionGroupTypes) {
      collected = collected.filter(
        (collection) => !excludeCollectionGroupTypes.includes(collection.collectionGroupType),
      );
    }

    if (onlyCollectionGroupTypes) {
      collected = collected.filter((collection) =>
        onlyCollectionGroupTypes.includes(collection.collectionGroupType),
      );
    }

    return new Set(collected.flatMap((c) => c.collected));
  }, [dataMap, excludeCollectionGroupTypes, onlyCollectionGroupTypes]);

  const collectedPic = useMemo(() => {
    if (!dataMap || !dataMap.categories) {
      return [];
    }
    return dataMap.categories.filter((c) => collectedItems.has(c.id));
  }, [dataMap, collectedItems]);

  return collectedPic;
};

export const useAllCollectedPICByCollectionGroup = (dataMap: DataMapDto) => {
  const collectedCategoryIdsByGroupId = useMemo(
    () => (dataMap ? _.groupBy(dataMap.collection, "collectionGroupId") : {}),
    [dataMap],
  );

  const collectedPICByGroupId: Record<UUIDString, CategoryDto[]> = useMemo(
    () =>
      _.reduce(
        collectedCategoryIdsByGroupId,
        (acc, collected, groupId) => {
          const collectedPic = collected.flatMap((c) =>
            dataMap.categories.filter((cat) => c.collected.includes(cat.id)),
          );
          acc[groupId] = collectedPic;
          return acc;
        },
        {},
      ),
    [collectedCategoryIdsByGroupId, dataMap],
  );

  return collectedPICByGroupId;
};

export const useAllCollectedPICGroups = (
  dataMap: DataMapDto,
  excludeCollectionGroupTypes: CollectionGroupType[] = DEFAULT_EXCLUDE_GROUP_TYPE,
  onlyCollectionGroupTypes?: CollectionGroupType[],
) => {
  const collectedPic = useAllCollectedPIC(
    dataMap,
    excludeCollectionGroupTypes,
    onlyCollectionGroupTypes,
  );

  return useMemo(() => groupsFromPIC(collectedPic), [collectedPic]);
};

export const useCollectedPICGroupsByCollectionGroup = (
  dataMap: DataMapDto,
): Record<UUIDString, PICGroup[]> => {
  const collectedPICByGroupId = useAllCollectedPICByCollectionGroup(dataMap);

  return useMemo(
    () =>
      _.reduce(
        collectedPICByGroupId,
        (acc, collectedPIC, groupId) => {
          acc[groupId as UUIDString] = groupsFromPIC(collectedPIC);
          return acc;
        },
        {},
      ),
    [collectedPICByGroupId],
  );
};

export const useDisclosedPICGroupsByVendorId = (
  dataMap: DataMapDto,
  collectionGroups?: UUIDString[],
): Record<UUIDString, PICGroup[]> => {
  const disclosedCategoryIdsByVendorId = useMemo(() => {
    if (!dataMap) {
      return {};
    }

    const relevant = collectionGroups
      ? dataMap?.disclosure.filter((d) => collectionGroups.includes(d.collectionGroupId))
      : dataMap?.disclosure;

    return _.groupBy(relevant, "vendorId");
  }, [dataMap, collectionGroups]);

  const disclosedPICByGroupId: Record<UUIDString, CategoryDto[]> = useMemo(
    () =>
      _.reduce(
        disclosedCategoryIdsByVendorId,
        (acc, disclosed, groupId) => {
          const disclosedPic = disclosed.flatMap((d) =>
            dataMap.categories.filter((cat) => d.disclosed.includes(cat.id)),
          );

          acc[groupId] = disclosedPic;
          return acc;
        },
        {},
      ),
    [disclosedCategoryIdsByVendorId, dataMap],
  );

  return useMemo(
    () =>
      _.reduce(
        disclosedPICByGroupId,
        (acc, disclosedPIC, groupId) => {
          acc[groupId as UUIDString] = groupsFromPIC(disclosedPIC);
          return acc;
        },
        {},
      ),
    [disclosedPICByGroupId],
  );
};

export const usePendingReceivedPICByVendorId = (
  dataMap: V2DataMapDto,
  collectionGroups?: UUIDString[],
): Record<UUIDString, UUIDString[]> => {
  const allPending: DataMapReceivedUpdateOperation[] = dataMap?._local?.operations?.filter?.(
    (op) => op.target == "RECEIVED" && (!collectionGroups || collectionGroups.includes(op.groupId)),
  ) as DataMapReceivedUpdateOperation[];
  const groupedPending = _.groupBy(allPending, (op) => op.vendorId);
  return _.mapValues(groupedPending, (v) => _.uniq(v.map((op) => op.picId)));
};

export const useReceivedPICByVendorId = (
  dataMap: DataMapDto | undefined,
  collectionGroups?: UUIDString[],
): Record<UUIDString, CategoryDto[]> => {
  const receivedCategoryIdsByVendorId = useMemo(() => {
    if (!dataMap) {
      return {};
    }

    const relevant = collectionGroups
      ? dataMap.received.filter((r) => collectionGroups.includes(r.collectionGroupId))
      : dataMap.received;

    return _.groupBy(relevant, "vendorId");
  }, [dataMap, collectionGroups]);

  return useMemo(
    () =>
      _.reduce(
        receivedCategoryIdsByVendorId,
        (acc, received, groupId) => {
          const receivedPic = received.flatMap(
            (d) => dataMap?.categories.filter((cat) => d.received.includes(cat.id)) || [],
          );

          acc[groupId] = receivedPic;
          return acc;
        },
        {},
      ),
    [receivedCategoryIdsByVendorId, dataMap],
  );
};

export const useCollectionDetails = (
  dataMap: DataMapDto,
  collectionGroup: CollectionGroupDetailsDto,
): DataMapCollectionDetails => {
  const collectionDetails = useMemo(
    () => dataMap.collection.find((c) => c.collectionGroupId === collectionGroup.id),
    [dataMap, collectionGroup],
  );

  const collectedPic = useMemo(
    () => dataMap.categories.filter((c) => collectionDetails?.collected.includes(c.id) ?? false),
    [collectionDetails, dataMap],
  );

  const collectedPicGroups = useMemo(() => groupsFromPIC(collectedPic), [collectedPic]);

  return { collectionDetails, collectedPic, collectedPicGroups };
};

export type DataMapDisclosureDetails = {
  disclosureDetails: DisclosureDto | undefined;
  disclosedPic: CategoryDto[];
  disclosedPicGroups: PICGroup[];
};

export const useDisclosureDetails = (
  dataMap: DataMapDto,
  collectionGroup: CollectionGroupDetailsDto,
  dataRecipient: OrganizationDataRecipientDto,
): DataMapDisclosureDetails => {
  const disclosureDetails = useMemo(
    () =>
      dataMap.disclosure.find(
        (c) => c.collectionGroupId === collectionGroup.id && c.vendorId === dataRecipient.vendorId,
      ),
    [dataMap, collectionGroup, dataRecipient],
  );

  const disclosedPic = useMemo(
    () => dataMap.categories.filter((c) => disclosureDetails?.disclosed.includes(c.id) ?? false),
    [dataMap, disclosureDetails],
  );

  const disclosedPicGroups = useMemo(() => groupsFromPIC(disclosedPic), [disclosedPic]);

  return { disclosureDetails, disclosedPic, disclosedPicGroups };
};

export type DataMapSourcedDetails = {
  sourceDetails: SourceDto | undefined;
  sourcedPic: CategoryDto[];
  sourcedPicGroups: PICGroup[];
};
export const useSourcedDetails = (
  dataMap: DataMapDto,
  collectionGroup: CollectionGroupDetailsDto,
  dataRecipient: OrganizationDataSourceDto,
): DataMapSourcedDetails => {
  const sourceDetails = useMemo(
    () =>
      dataMap.received.find(
        (c) => c.collectionGroupId === collectionGroup.id && c.vendorId === dataRecipient.vendorId,
      ),
    [dataMap, collectionGroup, dataRecipient],
  );

  const sourcedPic = useMemo(
    () => dataMap.categories.filter((c) => sourceDetails?.received.includes(c.id) ?? false),
    [dataMap, sourceDetails],
  );

  const sourcedPicGroups = useMemo(() => groupsFromPIC(sourcedPic), [sourcedPic]);

  return { sourceDetails, sourcedPic, sourcedPicGroups };
};
