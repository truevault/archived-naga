import { useCallback, useEffect, useRef } from "react";

// Sorry not sorry
// Apparently the need for this goes away in react 18 so I'm fine with this "anti-pattern" for now

export function useIsMounted() {
  const isMounted = useRef(false);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  return useCallback(() => isMounted.current, []);
}
