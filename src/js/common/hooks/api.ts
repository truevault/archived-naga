import _ from "lodash";
import { AxiosError } from "axios";
import { useSnackbar } from "notistack";
import React, { useCallback, useEffect, useState } from "react";
import {
  QueryKey,
  useMutation,
  useQueries,
  useQuery,
  useQueryClient,
  UseQueryResult,
} from "@tanstack/react-query";
import { NetworkRequest, NetworkRequestState } from "../models/NetworkRequest";
import { useIsMounted } from "./useIsMounted";

export type FetchFn = () => Promise<void>;
export type RefreshFn = () => Promise<void>;
export type UpdateFn = (a?: any) => Promise<void>;
export type InvalidateFn = () => Promise<void> | void;

type ApiMessages = {
  defaultError?: string;
  forceError?: string;
  success?: string;
};

type ApiArgs<R> = {
  api: (v?: any) => Promise<R>;
  messages?: ApiMessages;
  allowConcurrent?: boolean;
  dependencies?: React.DependencyList;
  notReady?: boolean;
  notifyError?: boolean;
  notifySuccess?: boolean;
  onSuccess?: (result: R, request: NetworkRequestState, args?: any) => void;
  onError?: (request: NetworkRequestState, error: Error | AxiosError) => void;
};

export type ApiData<R> = {
  result: R | undefined;
  request: NetworkRequestState;
  refresh: RefreshFn;
  invalidate: InvalidateFn;
  queryKey: QueryKey;
};

type DataRequestArgs<R> = {
  queryKey: QueryKey;
  notReady?: boolean;
  staleTime?: number;
  cacheTime?: number;
  invalidation?: InvalidationOptions;
} & ApiArgs<R>;

export type InvalidationOptions = _.DebounceSettings & {
  wait?: number;
};

export const useDataRequest = <R>(args: DataRequestArgs<R>): ApiData<R> => {
  const {
    queryKey,
    api,
    messages,
    notReady,
    notifySuccess = false,
    notifyError = true,
    invalidation,
  } = args;

  const queryClient = useQueryClient();
  const { enqueueSnackbar } = useSnackbar();
  const { networkRequestState, setRequestStatus } = useNetworkStateWithStatus(messages);

  const {
    data: result,
    refetch,
    status,
  } = useQuery<R, Error | AxiosError>(queryKey, api, {
    keepPreviousData: true,
    enabled: !notReady,
    staleTime: args.staleTime,
    cacheTime: args.cacheTime,
    onSuccess: (data) => {
      if (args.onSuccess) {
        args.onSuccess(data, networkRequestState);
      }

      if (networkRequestState.successMessage) {
        if (notifySuccess && networkRequestState.successMessage) {
          enqueueSnackbar(networkRequestState.successMessage, {
            variant: "success",
          });
        }
      }
    },
    onError: (err) => {
      console.error(err);

      if (args.onError) {
        args.onError(networkRequestState, err);
      }

      if (notifyError && networkRequestState.errorMessage) {
        enqueueSnackbar(networkRequestState.errorMessage, {
          variant: "error",
        });
      }
    },
  });

  useEffect(() => {
    setRequestStatus(status);
  }, [status, setRequestStatus]);

  // @ts-ignore
  const refresh: RefreshFn = useCallback(async () => {
    const result = await refetch();
    return result.data;
  }, [refetch]);

  const _invalidate = () => queryClient.invalidateQueries(queryKey);
  const invalidate = useCallback(
    invalidation?.wait ? _.debounce(_invalidate, invalidation.wait, invalidation) : _invalidate,
    [],
  );

  return { queryKey, result, request: networkRequestState, refresh, invalidate };
};

export const useDataRequests = <T>(
  queries: { queryKey: unknown[]; queryFn: (v?: any) => Promise<T> }[],
): [UseQueryResult<T>[], NetworkRequestState, () => Promise<void>] => {
  const { networkRequestState, setNetworkRequestState, setRequestStatus } =
    useNetworkStateWithStatus();

  const results = useQueries({
    queries,
  });

  useEffect(() => {
    if (results.every((q) => q.status === "success")) {
      setRequestStatus("success");
    } else if (results.some((q) => q.status === "error")) {
      setRequestStatus("error");
    } else if (results.some((q) => q.status === "loading")) {
      setRequestStatus("loading");
    } else {
      setNetworkRequestState(NetworkRequest.unstarted());
    }
  }, [results, setNetworkRequestState, setRequestStatus]);

  const refreshAll = useCallback(async () => {
    await Promise.all(results.map((q) => q.refetch()));
  }, [results]);

  return [results, networkRequestState, refreshAll];
};

type ApiAction<R, P extends any = any> = {
  fetch: (a?: P) => Promise<void>;
  fetchWithResult: (a?: P) => Promise<R>;
  result: R | undefined;
  request: NetworkRequestState;
  resetState: () => void;
};

export const useActionRequest = <R, P extends any = any>(args: ApiArgs<R>): ApiAction<R, P> => {
  const { enqueueSnackbar } = useSnackbar();
  const { api, messages, notReady, notifyError = true, notifySuccess = false } = args;
  const { networkRequestState, setNetworkRequestState, setRequestStatus } =
    useNetworkStateWithStatus(messages);
  const isMounted = useIsMounted();

  const {
    data: result,
    mutateAsync,
    status,
    reset,
  } = useMutation(api, {
    onSuccess: (data) => {
      if (args.onSuccess) {
        args.onSuccess(data, networkRequestState);
      }

      const successMessage = networkRequestState.successMessage || args.messages?.success;
      if (successMessage) {
        if (notifySuccess && successMessage) {
          enqueueSnackbar(successMessage, {
            variant: "success",
          });
        }
      }

      reset();
      if (isMounted()) {
        setNetworkRequestState((prev) => NetworkRequest.success(prev, messages?.success));
      }
    },
    onError: (err) => {
      console.error(err);

      if (args.onError) {
        args.onError(networkRequestState, err as Error | AxiosError);
      }

      if (notifyError && networkRequestState.errorMessage) {
        enqueueSnackbar(networkRequestState.errorMessage, {
          variant: "error",
        });
      }

      reset();
      if (isMounted()) {
        setNetworkRequestState((prev) =>
          NetworkRequest.failed(
            prev,
            networkRequestState.errorMessage ??
              messages?.defaultError ??
              "An unknown error occurred while processing this request",
          ),
        );
      }
    },
  });

  // this variation of fetch returns Promise<void> and is suitable for fire-and-forget updates
  const fetchCb = useCallback(
    async (a?: any) => {
      if (!notReady) {
        await mutateAsync(a);
      }
    },
    [mutateAsync, notReady],
  );

  // this variation of fetch returns Promise<R>, and is suitable for immediate usage of the response data
  const fetchWithResultCb = useCallback(
    async (a: P | undefined) => {
      if (!notReady) {
        return await mutateAsync(a);
      }
      throw new Error("Cannot fetch because the request is not yet ready.");
    },
    [mutateAsync, notReady],
  );

  useEffect(() => {
    setRequestStatus(status);
  }, [status, setRequestStatus]);

  const resetState = useCallback(() => {
    setNetworkRequestState(NetworkRequest.unstarted());
  }, [setNetworkRequestState]);

  return {
    fetch: fetchCb,
    fetchWithResult: fetchWithResultCb,
    result,
    request: networkRequestState,
    resetState,
  };
};

/**
 * @deprecated The method should not be used
 */
export const useApiTrigger = <T, R>(
  api: (values: T) => Promise<any>,
  done: (response?: R) => void,
  errorCopy: string,
  successCopy?: string,
): UseApiTriggerResult => {
  const { fetch: start, request } = useActionRequest<R>({
    api: api,
    messages: { defaultError: errorCopy, success: successCopy },
    notifySuccess: true,
    notifyError: true,
    onSuccess: done,
  });

  return [start, request];
};

type NetworkRequestStatus = "error" | "success" | "loading" | "idle" | "unstarted";

export const useNetworkStateWithStatus = (messages?: ApiMessages) => {
  const [status, setRequestStatus] = useState<NetworkRequestStatus>("unstarted");
  const [networkRequestState, setNetworkRequestState] = useState<NetworkRequestState>(
    NetworkRequest.unstarted(),
  );

  useEffect(() => {
    if (status === "error") {
      setNetworkRequestState((prev) =>
        NetworkRequest.failed(prev, messages?.defaultError ?? "Unknown Error"),
      );
    } else if (status === "success") {
      setNetworkRequestState((prev) => NetworkRequest.success(prev, messages?.success));
    } else if (status === "idle") {
      setNetworkRequestState((prev) => {
        if (prev.running) {
          return NetworkRequest.unstarted();
        }

        return prev;
      });
    } else if (status === "loading") {
      setNetworkRequestState(NetworkRequest.start());
    }
  }, [status, messages?.defaultError, messages?.success]);

  return { networkRequestState, setNetworkRequestState, setRequestStatus };
};

type UseApiTriggerResult = [
  (v: any) => void, // trigger
  NetworkRequestState,
];

export type DataContext<R> = {
  results: R[];
  request: NetworkRequestState;
  refresh: RefreshFn;
  invalidate: InvalidateFn;
  updateItem: UpdateItemFn<R>;
  updateItemRequest: NetworkRequestState;
};

export type UpdateItemFn<R> = (params?: any) => Promise<R>;
