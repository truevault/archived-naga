import { useMemo } from "react";
import { Error as RequestsError } from "../../polaris/copy/requests";
import { NetworkRequestState } from "../models/NetworkRequest";
import { Api } from "../service/Api";
import { VendorDto } from "../service/server/dto/VendorDto";
import { OrganizationPublicId } from "../service/server/Types";
import { RefreshFn, useDataRequest } from "./api";

type CatalogType = "DATA_SOURCE" | "DATA_RECIPIENT" | "ALL";

export const useDataRecipientsCatalog = (
  organizationId: OrganizationPublicId,
): [VendorDto[], NetworkRequestState, RefreshFn] =>
  useVendorCatalog(organizationId, "DATA_RECIPIENT");

export const useDataSourceCatalog = (
  organizationId: OrganizationPublicId,
): [VendorDto[], NetworkRequestState, RefreshFn] => useVendorCatalog(organizationId, "DATA_SOURCE");

const useVendorCatalog = (
  organizationId: OrganizationPublicId,
  catalogType: CatalogType = "ALL",
): [VendorDto[], NetworkRequestState, RefreshFn] => {
  const {
    result: allVendors,
    request,
    refresh,
  } = useDataRequest({
    queryKey: ["vendorCatalog", organizationId],
    staleTime: 10 * 60 * 1000, // 10 minutes
    cacheTime: 10 * 60 * 1000,
    api: () => Api.organizationVendor.getVendorCatalog(organizationId),
    messages: {
      defaultError: RequestsError.GetVendors,
    },
    dependencies: [organizationId],
  });

  const vendors = useMemo(() => {
    const catalogVendors = allVendors?.filter((v) => v.dataRecipientType == "vendor");

    if (catalogVendors) {
      if (catalogType == "ALL") {
        return catalogVendors;
      } else if (catalogType == "DATA_RECIPIENT") {
        return catalogVendors.filter((v) => v.isDataRecipient);
      } else if (catalogType == "DATA_SOURCE") {
        return catalogVendors.filter((v) => v.isDataSource);
      }
    }
    return [];
  }, [allVendors, catalogType]);

  return [vendors, request, refresh];
};
