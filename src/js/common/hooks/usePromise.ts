import { useEffect, useState } from "react";

type ResolvedPromise<T> = { status: "resolved"; value: T | null };
type FailedPromise = { status: "error"; error: unknown };
type PendingPromise = { status: "pending" };
export type PromiseStatus<T> = ResolvedPromise<T> | FailedPromise | PendingPromise;

export function usePromise<T>(promise: Promise<T>): PromiseStatus<T> {
  const [status, setStatus] = useState<PromiseStatus<T>["status"]>("pending");
  const [error, setError] = useState<unknown>(null);
  const [value, setValue] = useState<T | null>(null);

  useEffect(() => {
    let cancelled = false;

    setStatus("pending");
    setError(null);
    setValue(null);

    promise
      .then((x) => {
        if (!cancelled) {
          setStatus("resolved");
          setError(null);
          setValue(x);
        }
      })
      .catch((err: unknown) => {
        if (!cancelled) {
          setStatus("error");
          setError(err);
          setValue(null);
        }
      });

    return () => {
      cancelled = true;
    };
  }, [promise]);

  if (status === "pending") {
    return { status };
  } else if (status === "resolved") {
    return { status, value };
  } else {
    return { status, error };
  }
}
