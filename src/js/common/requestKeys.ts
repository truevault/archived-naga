import { OrganizationPublicId } from "./service/server/Types";

export const getOrganization = (organizationId: OrganizationPublicId) =>
  `getOrganization:${organizationId}`;

export const getSelf = () => `getSelf`;
export const getSession = () => `getSession`;
