import { Search } from "@mui/icons-material";
import { InputAdornment, OutlinedInput } from "@mui/material";
import React from "react";
import { FilterProps } from "react-table";

export const DefaultFilter: React.FC<FilterProps<any>> = ({
  column: { filterValue, setFilter, Header },
}) => {
  return (
    <OutlinedInput
      value={filterValue || ""}
      onChange={(e) => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      type="search"
      size="small"
      sx={{ background: "white" }}
      fullWidth
      startAdornment={
        <InputAdornment position="start">
          <Search />
        </InputAdornment>
      }
      placeholder={`Filter ${Header}`}
    />
  );
};
