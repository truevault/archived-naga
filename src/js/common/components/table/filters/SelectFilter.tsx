import { Autocomplete, TextField } from "@mui/material";
import _ from "lodash";
import React from "react";
import { FilterProps } from "react-table";

export type SelectOption = { value: any; label: string };
export type SelectFilterProps = FilterProps<any> & { options: SelectOption[]; loading?: boolean };

export const SelectFilter: React.FC<SelectFilterProps> = ({
  column: { id, filterValue, setFilter, Header },
  loading = false,
  options = [],
}) => {
  const selectedOption = options.find((o) => o.value == filterValue);

  return (
    <Autocomplete
      id={`${id}-filter`}
      loading={loading}
      options={options}
      sx={{ background: "white" }}
      value={selectedOption ?? null}
      onChange={(e, newValue) => setFilter(newValue?.value)}
      isOptionEqualToValue={_.isEqual}
      renderInput={(params) => (
        <TextField {...params} placeholder={`Filter ${Header}`} size="small" />
      )}
    />
  );
};
