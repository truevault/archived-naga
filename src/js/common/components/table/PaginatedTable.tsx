import { InfoOutlined } from "@mui/icons-material";
import {
  Collapse,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useCallback, useEffect, useMemo } from "react";
import {
  Column,
  Filters,
  Row,
  SortingRule,
  useFilters,
  usePagination,
  useSortBy,
  useTable,
} from "react-table";
import { PaginatedMetaDto } from "../../../common/service/server/dto/PaginatedDto";
import { Callout } from "../../../polaris/components/Callout";
import { EmptyInfo } from "../../../polaris/components/Empty";
import { DefaultFilter } from "./filters/DefaultFilter";
import { PaginatedFooter } from "./PaginatedFooter";

type ClickableTableRowProps = {
  row: Row<any>;
  canClick?: boolean;
  onRowClick?: (row: Row<any>) => void | Promise<void>;
};

const ClickableTableRow: React.FC<ClickableTableRowProps> = ({
  row,
  canClick,
  onRowClick = (_row) => {},
}) => {
  const handleClick = useCallback(() => onRowClick(row), [row, onRowClick]);

  return (
    <TableRow
      hover={canClick}
      onClick={canClick ? handleClick : undefined}
      {...row.getRowProps()}
      key={row.id}
    >
      {row.cells.map((cell, idx) => {
        return (
          <TableCell {...cell.getCellProps()} key={idx}>
            {cell.render("Cell")}
          </TableCell>
        );
      })}
    </TableRow>
  );
};

export type PaginatedTableProps = {
  data: any[];
  columns: Column<any>[];
  pageMeta?: PaginatedMetaDto;
  showFilters?: boolean;
  filterPanelOpen?: boolean;
  initialPageIndex?: number;
  initialSortBy?: SortingRule<any>[];
  initialFilters?: Filters<any>;
  onPageChange?: (pageIndex: number) => void | Promise<void>;
  onSortChange?: (sortBy: SortingRule<any>[]) => void | Promise<void>;
  onFiltersChange?: (filters: Filters<any>) => void | Promise<void>;
  onRowClick?: (row: any) => void;
  enableRowClick?: (row: any) => boolean;
  emptyState?: React.ReactNode;
};

export const PaginatedTable: React.FC<PaginatedTableProps> = ({
  data,
  columns,
  pageMeta,
  showFilters = true,
  filterPanelOpen = true,
  onPageChange = (_pageIndex: number) => {},
  onSortChange,
  onFiltersChange,
  initialPageIndex = 0,
  initialSortBy = [],
  initialFilters = [],
  onRowClick = (_row: any) => {},
  enableRowClick = (_row: any) => false,
  emptyState,
}) => {
  const defaultColumn = useMemo(
    () => ({
      Filter: DefaultFilter,
    }),
    [],
  );

  const { getTableProps, getTableBodyProps, headerGroups, prepareRow, page, gotoPage, state } =
    useTable(
      {
        columns,
        data,
        defaultColumn,
        manualPagination: true,
        manualSortBy: true,
        manualFilters: true,
        pageCount: pageMeta?.totalPages ?? 0,
        initialState: {
          pageIndex: initialPageIndex,
          sortBy: initialSortBy,
          filters: initialFilters,
        },
      },
      useFilters,
      useSortBy,
      usePagination,
    );

  const { pageIndex, pageSize, sortBy, filters } = state;

  const updatePageIndex = useCallback(
    (newPageIndex) => {
      onPageChange(newPageIndex + 1);
      gotoPage(newPageIndex);
    },
    [gotoPage, onPageChange],
  );

  useEffect(() => {
    if (onSortChange) {
      onSortChange(sortBy);
    }
  }, [sortBy, onSortChange]);

  useEffect(() => {
    if (onFiltersChange) {
      onFiltersChange(filters);
    }
  }, [filters, onFiltersChange]);

  return (
    <>
      {showFilters && (
        <Collapse in={filterPanelOpen}>
          <Box sx={{ my: 2 }}>
            <Callout>
              {headerGroups.map((headerGroup, index) => (
                <div
                  className="flex-row flex--expand flex--between"
                  {...headerGroup.getHeaderGroupProps()}
                  key={`hg-${index}`}
                >
                  {headerGroup.headers
                    .filter((c) => c.canFilter)
                    .map((column) => (
                      <div {...column.getHeaderProps()} key={column.id}>
                        {column.render("Filter")}
                      </div>
                    ))}
                </div>
              ))}
            </Callout>
          </Box>
        </Collapse>
      )}

      <TableContainer className="mb-lg">
        <Table className={"request-table"} {...getTableProps()}>
          <TableHead>
            {headerGroups.map((headerGroup, index) => (
              <TableRow {...headerGroup.getHeaderGroupProps()} key={`hg-${index}`}>
                {headerGroup.headers.map((column) => (
                  <TableCell
                    component="th"
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                    key={column.id}
                  >
                    {column.render("Header")}

                    {column.canSort && (
                      <TableSortLabel
                        active={column.isSorted}
                        direction={column.isSortedDesc ? "desc" : "asc"}
                      />
                    )}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableHead>

          <TableBody {...getTableBodyProps()}>
            {page.length > 0 ? (
              <>
                {page.map((row) => {
                  prepareRow(row);
                  return (
                    <ClickableTableRow
                      key={row.id}
                      row={row}
                      canClick={enableRowClick(row)}
                      onRowClick={onRowClick}
                    />
                  );
                })}
              </>
            ) : (
              <TableRow>
                <TableCell colSpan={10000000}>
                  {emptyState ? emptyState : <EmptyInfo Icon={InfoOutlined}>No Requests</EmptyInfo>}
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>

      {pageMeta && pageMeta.totalPages > 1 && (
        <PaginatedFooter
          meta={{
            page: pageIndex,
            per: pageSize,
            count: page.length,
            totalPages: pageMeta.totalPages,
            totalCount: pageMeta.totalCount,
          }}
          setPage={updatePageIndex}
        />
      )}
    </>
  );
};
