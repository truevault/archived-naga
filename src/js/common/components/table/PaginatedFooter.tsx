import { Pagination } from "@mui/material";
import React from "react";
import { PaginatedMetaDto } from "../../../common/service/server/dto/PaginatedDto";

type PaginatedFooterProps = {
  meta: PaginatedMetaDto;
  setPage: (page: number) => void;
};

export const PaginatedFooter: React.FC<PaginatedFooterProps> = ({ meta, setPage }) => {
  const handleChange = (_event, value: number) => {
    setPage(value - 1);
  };

  return (
    <div className="flex-row--simple flex--center">
      <Pagination count={meta.totalPages} page={meta.page + 1} onChange={handleChange} />
    </div>
  );
};
