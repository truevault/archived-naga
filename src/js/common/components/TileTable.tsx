import clsx from "clsx";
import React from "react";

export type CellProps<D> = {
  data: D;
};

export type ColumnDefinition<D> = {
  width?: string;
  heading: string | null;
  cellClass?: string;
  component?: React.FC<CellProps<D>>;
  accessor?: (row: D, col: ColumnDefinition<D>) => React.ReactNode;
  key?: string;
};

export type TableDefinition<D> = {
  columns: ColumnDefinition<D>[];
  data: D[];
};

export type TileTableProps<D extends unknown = any> = {
  table: TableDefinition<D>;
  variant?: "default" | "filled";
};

export const TileTable: React.FC<TileTableProps> = ({ table, variant = "default" }) => {
  const hasHeadings = table.columns?.some((col) => Boolean(col.heading)) ?? false;

  return (
    <table className={`tile-table tile-table--${variant}`}>
      {hasHeadings && (
        <thead className="tile-table__headings">
          <tr>
            {table.columns.map((col, idx) => {
              return (
                <th key={idx} className="tile-table__th" style={{ width: col.width }}>
                  {col.heading}
                </th>
              );
            })}
          </tr>
        </thead>
      )}

      <tbody>
        {table.data.map((row, rowIdx) => {
          return (
            <tr key={rowIdx} className="tile-table__tr">
              {table.columns.map((col, colIdx) => {
                return <TileTableCell className={col.cellClass} key={colIdx} row={row} col={col} />;
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export type TileTableCellProps = {
  row: any;
  className?: string;
  col: ColumnDefinition<any>;
};

const TileTableCell: React.FC<TileTableCellProps> = ({ row, col, className }) => {
  let renderObject: any;
  if (col.component) {
    renderObject = React.createElement(col.component, { data: row });
  } else if (col.accessor) {
    renderObject = col.accessor(row, col);
  } else if (col.key) {
    renderObject = row[col.key];
  }

  return (
    <td className={clsx("tile-table__td", className)} style={{ width: col.width }}>
      {renderObject}
    </td>
  );
};
