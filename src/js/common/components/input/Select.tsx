import {
  Box,
  Checkbox,
  Chip,
  FormControl,
  FormControlProps,
  FormHelperText,
  InputLabel,
  ListItemText,
  MenuItem,
  Select as MaterialSelect,
  SelectProps as MaterialSelectProps,
} from "@mui/material";
import clsx from "clsx";
import React from "react";
import { useField } from "react-final-form";
import { toggleSelection } from "../../../polaris/surveys/util";

export type SelectOption<T extends any = any> = {
  value: T;
  label: string;
  divider?: boolean;
  disabled?: boolean;
};

export type SimpleSelectProps = {
  options: SelectOption[];
  includeBlank?: boolean;
  showChecks?: boolean;
} & MaterialSelectProps;

type SelectProps = SimpleSelectProps & {
  field: string;
  label: React.ReactNode;
  required?: boolean;
  cls?: string;
  formControlProps?: FormControlProps;
  validate?: (val: string) => string | undefined;
};

export const SimpleSelect: React.FC<SimpleSelectProps> = ({
  options,
  includeBlank,
  showChecks = false,
  ...rest
}) => {
  return (
    <MaterialSelect {...rest}>
      {includeBlank && <MenuItem value="">&nbsp;</MenuItem>}

      {options.map((o) => {
        const isChecked = rest.multiple
          ? (rest.value as any[]).indexOf(o.value) > -1
          : rest.value === o.value;

        return (
          <MenuItem
            key={o.value}
            value={o.value}
            divider={o.divider}
            disabled={rest.disabled || o.disabled}
          >
            {showChecks && <Checkbox checked={isChecked} />}
            {o.label}
          </MenuItem>
        );
      })}
    </MaterialSelect>
  );
};

export const MultiSelectChips: React.FC<{
  selectedValues: string[];
  onDelete?: (index: number, value: string) => void | Promise<void>;
}> = ({ selectedValues, onDelete }) => {
  return (
    <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
      {selectedValues.map((value, idx) => (
        <Chip
          key={value}
          label={value}
          className="mr-xs"
          variant="muted"
          onDelete={onDelete ? () => onDelete(idx, value) : undefined}
          onMouseDown={(event) => {
            event.stopPropagation();
          }}
        />
      ))}
    </Box>
  );
};

export const Select = ({
  field,
  required = false,
  label,
  variant = "outlined",
  id,
  options,
  cls = "",
  includeBlank = false,
  validate,
  formControlProps = {},
  ...selectProps
}: SelectProps) => {
  const f = useField(field, { validate });
  const defaultHelperText = required ? null : "Optional";
  const hasError = Boolean(f.meta.touched && f.meta.error);
  const helperText = hasError ? f.meta.error : defaultHelperText;

  const inputLabel = React.useRef(null);

  const labelId = `${field}-label`;
  const defaultId = `${field}-select`;

  return (
    <FormControl className={cls} margin="normal" {...formControlProps}>
      {Boolean(label) && (
        <InputLabel ref={inputLabel} id={labelId}>
          {label}
        </InputLabel>
      )}

      <SimpleSelect
        fullWidth
        labelId={labelId}
        error={Boolean(f.meta.touched && f.meta.error)}
        label={label}
        id={id || defaultId}
        variant={variant}
        options={options}
        includeBlank={includeBlank}
        {...f.input}
        {...selectProps}
        value={f.input.value || []}
      />

      {Boolean(helperText) && <FormHelperText error={hasError}>{helperText}</FormHelperText>}
    </FormControl>
  );
};

type SelectWithRecommendationsProps = {
  recommendations?: SelectOption[];
  mandatory?: SelectOption[];
  onChange: (newValue: unknown) => void;
} & SimpleSelectProps;

export const SelectWithRecommendations: React.FC<SelectWithRecommendationsProps> = ({
  value,
  options,
  recommendations = [],
  mandatory = [],
  multiple,
  onChange,
  ...rest
}) => {
  const isSelectedForMultiple = (o: SelectOption) => {
    const isActuallySelected = ((value as any[]) ?? []).includes(o.value);
    const isMandatorySelection = mandatory.some((m) => m.value === o.value);

    return isActuallySelected || isMandatorySelection;
  };

  const selectedOptions = options.filter((o) =>
    multiple ? isSelectedForMultiple(o) : o.value === value,
  );

  const handleRemove = (valToRemove: any) => {
    const newValue = toggleSelection(value as any[], valToRemove, false);
    onChange(newValue);
  };

  const handleChange = (e) => onChange(e.target.value);

  const selectedValue = multiple
    ? selectedOptions.map((o) => o.value)
    : selectedOptions[0]?.value ?? "";

  return (
    <MaterialSelect
      value={selectedValue}
      onChange={handleChange}
      multiple={multiple}
      {...rest}
      renderValue={() =>
        multiple ? (
          <MultiRecommendationRenderValue
            selectedOptions={selectedOptions}
            mandatory={mandatory}
            recommendations={recommendations}
            onRemove={handleRemove}
          />
        ) : (
          <SingleRecommendationRenderValue
            selectedOptions={selectedOptions}
            mandatory={mandatory}
            recommendations={recommendations}
          />
        )
      }
    >
      {options.map((o) => {
        const isMandatory = mandatory.some((m) => m.value === o.value);
        const isRecommended = recommendations.some((r) => r.value === o.value) || isMandatory;
        const isSelected = multiple
          ? isMandatory || isRecommended || selectedValue.includes(o.value)
          : isMandatory || selectedValue == o.value;

        return (
          <MenuItem value={o.value} key={o.value} disabled={isMandatory} divider={o.divider}>
            {multiple && <Checkbox checked={isSelected} />}

            <RecommendedLabel option={o} isRecommended={isRecommended} />
          </MenuItem>
        );
      })}
    </MaterialSelect>
  );
};

const SingleRecommendationRenderValue: React.FC<{
  selectedOptions: SelectOption[];
  mandatory: SelectOption[];
  recommendations: SelectOption[];
}> = ({ selectedOptions, mandatory, recommendations }) => {
  const selectedOption = selectedOptions[0];

  if (!selectedOption) {
    return null;
  }

  const isMandatory = mandatory.some((m) => m.value === selectedOption.value);
  const isDefault = recommendations.some((r) => r.value === selectedOption.value) || isMandatory;

  return <RecommendedLabel option={selectedOption} isRecommended={isDefault} />;
};

const RecommendedLabel: React.FC<{ option: SelectOption; isRecommended: boolean }> = ({
  option,
  isRecommended = false,
}) => (
  <ListItemText
    className={clsx({ "color-primary-800": isRecommended })}
    primary={
      <>
        {isRecommended && <>* </>}
        {option.label}
      </>
    }
  />
);

const MultiRecommendationRenderValue: React.FC<{
  selectedOptions: SelectOption[];
  mandatory: SelectOption[];
  recommendations: SelectOption[];
  onRemove: (val: any) => void;
}> = ({ selectedOptions, mandatory, recommendations, onRemove }) => (
  <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
    {selectedOptions.map((selectedOption: SelectOption) => {
      const isMandatory = mandatory.some((m) => m.value === selectedOption.value);
      const isDefault =
        recommendations.some((r) => r.value === selectedOption.value) || isMandatory;

      const label = isDefault ? `* ${selectedOption.label}` : selectedOption.label;

      return (
        <Chip
          key={selectedOption.value}
          label={label}
          color={isDefault ? "primary" : undefined}
          variant="muted"
          onDelete={isMandatory ? undefined : () => onRemove(selectedOption.value)}
          onMouseDown={(event) => {
            event.stopPropagation();
          }}
        />
      );
    })}
  </Box>
);
