/* global RedactorX */

import React, { useEffect, useRef, useState } from "react";
import { useField } from "react-final-form";
import "../../../../static/assets/redactor/redactorx.min.css";
import "../../../../static/assets/redactor/redactorx.min.js";

interface WYSIWYGEditorProps {
  onChange?: (contents: string) => void;
  onBlur?: (event: FocusEvent, contents: string) => void;
  defaultValue?: string;
  value?: string;
  height?: string;
  disable?: boolean;
  name?: string;
  id?: string;
}

export const WYSIWYGEditor: React.FC<WYSIWYGEditorProps> = ({
  onChange,
  onBlur,
  defaultValue,
  value,
  height = null,
  name,
  id,
}) => {
  const editorEl = useRef<HTMLTextAreaElement>(null);
  const [red, setRedactor] = useState<any>(undefined);
  useEffect(() => {
    if (editorEl.current) {
      // @ts-ignore: RedactorX is a global variable
      const redactor = RedactorX(editorEl.current, {
        content: defaultValue,
        context: true,
        editor: {
          minHeight: height,
        },
        link: {
          target: "_blank",
        },
        buttons: {
          context: ["link"],
        },
        subscribe: {
          "editor.change": function (e) {
            if (onChange) {
              onChange(e.params.html);
            }
          },
          "editor.blur": function (e) {
            if (onBlur) {
              const contents = redactor.editor.getContent();
              onBlur(e, contents);
            }
          },
        },
      });
      setRedactor(redactor);
    }
  }, [onChange, defaultValue, onBlur, height]);

  useEffect(() => {
    if (value !== undefined && red !== undefined && red.editor.getContent() != value) {
      red.editor.setContent({ html: value });
    }
  }, [value]);

  // TODO ref, not sure how to fix this one
  return <textarea ref={editorEl} name={name} id={id}></textarea>;
};

interface WYSIWYGEditorInputProps extends WYSIWYGEditorProps {
  field: string;
}
export const WYSIWYGEditorInput: React.FC<WYSIWYGEditorInputProps> = ({ field, ...props }) => {
  const f = useField(field);
  const value = f.input.value;

  return (
    <WYSIWYGEditor
      {...props}
      defaultValue={value}
      onChange={(contents) => f.input.onChange(contents)}
    />
  );
};
