import { TextField, TextFieldProps } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
import { useField } from "react-final-form";

type TextProps = TextFieldProps & {
  field: string;
  required?: boolean;
  customHelperText?: React.ReactNode;
  cls?: string;
  isMultiLine?: boolean;
  initialValue?: string;
  convertBoolean?: boolean;
  validate?: (val: string) => string | undefined;
};

const useStyles = makeStyles((_theme) => ({
  input: {
    background: "white",
  },
}));

export const Text = ({
  field,
  required = false,
  fullWidth = true,
  variant = "outlined",
  isMultiLine = false,
  id,
  error,
  customHelperText,
  initialValue,
  cls,
  validate,
  convertBoolean = false,
  onChange,
  ...textFieldProps
}: TextProps) => {
  const f = useField(field, { initialValue, validate });

  const classes = useStyles();
  const defaultHelperText = required ? null : "Optional";
  const hasError = Boolean(f.meta.touched && f.meta.error) ?? error;
  const helperText = hasError ? f.meta.error : customHelperText ?? defaultHelperText;

  const handleChange = (e: any) => {
    if (convertBoolean) {
      if (e.target.value == "true") {
        e.target.value = true;
      }

      if (e.target.value == "false") {
        e.target.value = false;
      }
    }

    if (f?.input?.onChange) {
      f?.input?.onChange(e);
    }

    if (onChange) {
      onChange(e);
    }
  };

  return (
    <TextField
      id={id || field}
      InputProps={{ className: classes.input }}
      fullWidth={fullWidth}
      error={hasError}
      margin="normal"
      helperText={helperText}
      variant={variant as any}
      className={cls}
      multiline={isMultiLine}
      rows={isMultiLine ? 4 : 1}
      {...f.input}
      {...textFieldProps}
      onChange={handleChange}
    />
  );
};
