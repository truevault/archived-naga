import { DatePicker as MUIDatePicker } from "@mui/lab";
import { TextField } from "@mui/material";
import React from "react";
import { useField } from "react-final-form";

type DatePickerProps = Partial<React.ComponentProps<typeof MUIDatePicker>> & {
  field: string;
  label: string;
  required?: boolean;
  className?: string;
  form?: string;
  clearable?: true;
  id?: string;
};

export const DatePicker: React.FC<DatePickerProps> = ({
  field,
  required = false,
  className,
  form,
  clearable,
  id,
  ...datePickerProps
}) => {
  const f = useField(field);
  const defaultHelperText = required ? null : "Optional";
  const helperText = (f.meta.touched && f.meta.error) || defaultHelperText;
  const error = Boolean(f.meta.touched && f.meta.error);

  return (
    <MUIDatePicker
      renderInput={(props) => (
        <TextField
          {...props}
          {...form}
          className={className}
          format="MM/DD/YYYY"
          helperText={helperText}
          error={error}
          id={id || field}
        />
      )}
      clearable={clearable}
      {...f.input}
      {...datePickerProps}
      value={f.input.value || null}
    />
  );
};
