import React from "react";
import { FieldRenderProps, useField } from "react-final-form";
import {
  DescriptionCheckbox,
  DescriptionCheckboxProps,
} from "../../../polaris/components/input/DescriptionCheckbox";

type BooleanFieldProps = FieldRenderProps<boolean>;

interface AdapterProps extends DescriptionCheckboxProps {
  input: BooleanFieldProps["input"];
  label: React.ReactNode;
}

const DescriptionCheckboxAdapter: React.FC<AdapterProps> = ({
  input: { onChange, checked },
  label,
  afterChange,
  ...rest
}) => {
  const handleChange = (evt, checked) => {
    onChange(checked);
    if (afterChange) {
      afterChange();
    }
  };
  return <DescriptionCheckbox onChange={handleChange} checked={checked} label={label} {...rest} />;
};

export interface CheckboxProps extends DescriptionCheckboxProps {
  field: string;
}

export const Checkbox: React.FC<CheckboxProps> = ({ field, ...rest }) => {
  const f = useField(field, { type: "checkbox" });

  return <DescriptionCheckboxAdapter input={f.input} {...rest} />;
};
