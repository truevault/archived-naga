import React, { useEffect, useState } from "react";
import { NetworkRequest, NetworkRequestState } from "../models";
import { PageLoading } from "./Loading";

type PageRequestGateProps = {
  requests: NetworkRequestState[];
  loading?: boolean;
};

export const NetworkRequestGate: React.FC<PageRequestGateProps> = ({
  requests,
  loading = false,
  children,
}) => {
  const [readyToRender, setReadyToRender] = useState(false);

  // Effects
  useEffect(() => {
    if (NetworkRequest.areFinished(...requests)) {
      setReadyToRender(true);
    }
    // Our dependency is actually the spread requests, not
    // the requests as an array.
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [...requests]);

  return <>{!loading && readyToRender ? children : <PageLoading />}</>;
};
