import { Avatar, AvatarProps as MUIAvatarProps } from "@mui/material";
import clsx from "clsx";
import React from "react";
import { stringToColorClass } from "../../polaris/util/hash";

type AvatarProps = {
  content: string;
  size?: number;
} & MUIAvatarProps;

function stringAvatar(name: string, size: number | undefined) {
  return {
    sx: {
      width: size,
      height: size,
    },
    children: `${name[0]}`,
  };
}

export const LetterAvatar = React.forwardRef<any, AvatarProps>(
  ({ content, size, className, ...rest }, ref) => {
    const sizeClass = size != null && size < 32 ? "text-t2" : undefined;
    const containerClasses = clsx(
      "letter_avatar",
      className,
      stringToColorClass(content),
      sizeClass,
    );

    return (
      <Avatar ref={ref} {...stringAvatar(content, size)} className={containerClasses} {...rest} />
    );
  },
);

LetterAvatar.displayName = "LetterAvatar";
