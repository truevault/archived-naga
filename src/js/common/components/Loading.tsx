import React from "react";
import { CircularProgress } from "@mui/material";

export const PageLoading = () => (
  <div className="fill page-content--full">
    <CircularProgress size="6em" />
  </div>
);

export const TabPanelLoading = () => (
  <div className="tab-panel-content--fill">
    <CircularProgress color="primary" size="4rem" />
  </div>
);

type LoadingProps = {
  padding?: "sm" | "md" | "lg" | "xl";
  message?: string;
};

export const Loading: React.FC<LoadingProps> = ({ padding = "md", message }) => (
  <div className={`p-${padding}`}>
    <div className="fill">
      <CircularProgress color="primary" size="3rem" />
      {message && <div style={{ paddingLeft: "10px" }}>{message}</div>}
    </div>
  </div>
);

type InlineLoadingProps = {
  size?: "sm" | "md";
  className?: string;
  style?: React.CSSProperties;
};
export const InlineLoading: React.FC<InlineLoadingProps> = ({ size = "md", className, style }) => (
  <CircularProgress
    color="primary"
    size={size == "sm" ? "1rem" : "1.5rem"}
    className={className}
    style={style}
  />
);
