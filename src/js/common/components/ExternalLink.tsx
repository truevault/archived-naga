import React from "react";
import clsx from "clsx";
import { Launch } from "@mui/icons-material";

// type ExternalLinkProps = React.DetailedHTMLProps<
//   React.AnchorHTMLAttributes<HTMLAnchorElement>,
//   HTMLAnchorElement
// >;

type Props = {
  icon?: boolean;
  heavy?: boolean;
} & React.AnchorHTMLAttributes<HTMLAnchorElement>;

export const ExternalLink: React.FC<Props> = ({
  rel,
  children,
  className,
  heavy,
  icon,
  target,
  ...props
}) => {
  let newRel = rel || "";
  if (!newRel.search("noopener")) {
    newRel = newRel.concat(" noopener");
  }
  if (!newRel.search("noreferrer")) {
    newRel = newRel.concat(" noreferrer");
  }

  target = target || "_blank";

  const classes = clsx("external-link", className, {
    "text-weight-medium": heavy,
    "text-decoration-none": heavy,
  });

  return (
    <a rel={newRel} target={target} {...props} className={classes}>
      {children}
      {icon && <Launch fontSize="inherit" className="ml-xs" />}
    </a>
  );
};
