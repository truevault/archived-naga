interface SnackbarAnchor {
  horizontal: "right" | "left";
  vertical: "top" | "bottom";
}

export const snackbarAnchor: SnackbarAnchor = {
  horizontal: "right",
  vertical: "bottom",
};
