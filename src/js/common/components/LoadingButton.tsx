import { Button, ButtonProps, CircularProgress, CircularProgressProps } from "@mui/material";
import clsx from "clsx";
import React from "react";

const loadingColorFromButtonColor = (
  buttonColor: ButtonProps["color"],
): CircularProgressProps["color"] => {
  if (buttonColor === "default") {
    return "primary";
  }

  return buttonColor;
};

const spinnerSizeFromButtonSize = (
  buttonSize: "xs" | "small" | "medium" | "large" | undefined,
): number => {
  if (buttonSize === "small" || buttonSize === "xs") {
    return 14;
  }

  if (buttonSize === "large") {
    return 20;
  }

  return 18;
};

export type LoadingButtonProps = ButtonProps & {
  loading?: boolean;
  component?: React.ElementType;
  disabledTooltip?: string;
};

export const LoadingButton: React.FC<LoadingButtonProps> = React.forwardRef(
  (
    { loading, disabled, className, children, color, style, size, component = "button", ...props },
    ref,
  ) => {
    return (
      <Button
        variant="contained"
        {...props}
        style={Object.assign({}, style, { position: "relative" })}
        disabled={loading || disabled}
        color={color}
        size={size}
        className={clsx("loading_button", className)}
        component={component}
        ref={ref}
      >
        {children}

        {loading && (
          <div className="btn-loading-overlay">
            <CircularProgress
              size={spinnerSizeFromButtonSize(size)}
              color={loadingColorFromButtonColor(color)}
            />
          </div>
        )}
      </Button>
    );
  },
);

LoadingButton.displayName = "LoadingButton";
