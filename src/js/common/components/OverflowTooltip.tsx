import React, { useRef, useEffect, useState } from "react";
import Tooltip, { TooltipProps } from "@mui/material/Tooltip";
import makeStyles from "@mui/styles/makeStyles";
import clsx from "clsx";

interface OverflowTooltipProps extends TooltipProps {
  cls?: string;
}

const useStyles = makeStyles({
  tooltipContent: {
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
  },
});

export const OverflowTooltip = (props: OverflowTooltipProps) => {
  const [hoverStatus, setHover] = useState(false);

  const styles = useStyles();
  const textRef = useRef<HTMLDivElement>(null);

  const setHoverIfOverflow = () => {
    const currentRef = textRef?.current || null;
    // Current ref is never for some reason
    setHover(Boolean(currentRef && currentRef.scrollWidth > currentRef.clientWidth));
  };

  useEffect(() => {
    setHoverIfOverflow();
    window.addEventListener("resize", setHoverIfOverflow);
    return () => window.removeEventListener("resize", setHoverIfOverflow);
  }, []);

  // TODO deal with ref
  return (
    <Tooltip title={props.title} placement={props.placement} disableHoverListener={!hoverStatus}>
      <div ref={textRef} className={clsx(styles.tooltipContent, props.cls)}>
        {props.children}
      </div>
    </Tooltip>
  );
};
