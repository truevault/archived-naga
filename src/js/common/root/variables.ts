const get = (v) => {
  if (typeof window !== "undefined") {
    return window.getComputedStyle(document.documentElement).getPropertyValue(`--${v}`).trim();
  }

  return undefined;
};

export const variables = {
  colors: {
    primary: get("color-primary"),
    secondary: get("color-secondary"),
    warning: get("color-warning"),
    error: get("color-error"),
    success: get("color-success"),
    textMain: get("color-text-main"),
  },
  gutter: {
    gutterLg: get("gutter-lg"),
  },
};
