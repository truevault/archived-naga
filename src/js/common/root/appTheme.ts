import { createTheme } from "@mui/material";

const get = (v) => {
  if (typeof window !== "undefined") {
    return window.getComputedStyle(document.documentElement).getPropertyValue(`--${v}`).trim();
  }

  return undefined;
};

declare module "@mui/material/Button" {
  interface ButtonPropsSizeOverrides {
    xs: true;
  }

  interface ButtonPropsVariantOverrides {
    flat: true;
  }

  interface ButtonPropsColorOverrides {
    default: true;
  }
}

declare module "@mui/material/Chip" {
  interface ChipPropsSizeOverrides {
    xs: true;
  }

  interface ChipPropsVariantOverrides {
    muted: true;
  }
}

const rawTheme = {
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  palette: {
    primary: {
      dark: get("color-primary-dark"),
      main: get("color-primary-main"),
      light: get("color-primary-light"),
      contrastText: "#FFFFFF",
    },
    secondary: {
      dark: get("color-secondary-dark"),
      main: get("color-secondary-main"),
      light: get("color-secondary-light"),
      contrastText: "#FFFFFF",
    },
    info: {
      dark: get("color-info-dark"),
      main: get("color-info-main"),
      light: get("color-info-light"),
    },
    warning: {
      dark: get("color-warning-dark"),
      main: get("color-warning-main"),
      light: get("color-warning-light"),
    },
    error: {
      dark: get("color-error-dark"),
      main: get("color-error-main"),
      light: get("color-error-light"),
      contrastText: "#FFFFFF",
    },
    success: {
      dark: get("color-success-dark"),
      main: get("color-success-main"),
      light: get("color-success-light"),
      contrastText: "#FFFFFF",
    },
    text: {
      primary: get("color-text-primary"),
      secondary: get("color-text-secondary"),
      disabled: get("color-text-disabled"),
      hint: get("color-text-disabled"),
    },
    grey: {
      "100": get("color-grey-100"),
      "200": get("color-grey-200"),
      "300": get("color-grey-300"),
      "400": get("color-grey-400"),
      "500": get("color-grey-500"),
      "600": get("color-grey-600"),
      "700": get("color-grey-700"),
      "800": get("color-grey-800"),
      "900": get("color-grey-900"),
    },
    default: {
      main: get("color-grey-900"),
      dark: get("black"),
    },
  },
  typography: {
    fontSize: 14,
    fontWeight: 400,
    button: {
      fontSize: "1.125rem",
      textTransform: "capitalize" as any,
    },
  },
  shape: {
    borderRadius: 5,
  },
};

// @ts-ignore
const baseTheme = createTheme(rawTheme);

export const theme = createTheme(baseTheme, {
  palette: {
    orange: baseTheme.palette.augmentColor({
      color: { main: get("color-orange-500") },
      name: "orange",
    }),
  },
  components: {
    MuiChip: {
      styleOverrides: {
        colorSuccess: {
          backgroundColor: get("color-green-200"),
          color: get("color-green-700"),
        },
        colorSecondary: {
          backgroundColor: get("color-secondary-200"),
          color: get("color-secondary-700"),
        },
        colorError: {
          backgroundColor: get("color-red-200"),
          color: get("color-red-700"),
        },
        colorOrange: {
          backgroundColor: get("color-orange-200"),
          color: get("color-orange-900"),
        },
      },
      variants: [
        {
          props: { size: "xs" },
        },
        {
          props: { variant: "muted" },
        },
      ],
    },
    MuiButton: {
      defaultProps: {
        color: "default",
      },
      variants: [
        {
          props: { size: "xs" },
        },
        {
          props: { variant: "flat" },
        },
        {
          props: { variant: "contained", color: "default" },
        },
        {
          props: { variant: "outlined", color: "default" },
        },
        {
          props: { variant: "text", color: "default" },
        },
      ],
    },
  },
});
