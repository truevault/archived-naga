// import { applyMiddleware, createStore } from "redux";
import * as Sentry from "@sentry/react";
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";

import { rootReducer } from "../reducers/root";

const sentryReduxEnhancer = Sentry.createReduxEnhancer();

function createStore() {
  const store = configureStore({
    reducer: rootReducer,
    middleware: [...getDefaultMiddleware()],
    enhancers: [sentryReduxEnhancer],
  });

  return store;
}

export const store = createStore();
