import QueryString from "qs";
import { OrganizationPublicId } from "../service/server/Types";

// TODO: Use a real parser here
const rep = (original: string, params: Record<string, string>) => {
  return original
    .replace(":orgId", params.orgId)
    .replace(":organizationId", params.organizationId)
    .replace(":collectionGroupId", params.collectionGroupId)
    .replace(":picId", params.picId)
    .replace(":picGroupId", params.picGroupId)
    .replace(":user", params.user)
    .replace(":email", params.email)
    .replace(":requestId", params.requestId)
    .replace(":vendorId", params.vendorId)
    .replace(":platformId", params.platformId)
    .replace(":appId", params.appId)
    .replace(":id", params.id)
    .replace(":mailbox", params.mailbox)
    .replace(":privacyCenterId", params.privacyCenterId)
    .replace(":sectionId", params.sectionId)
    .replace(":dataSubjectTypeId", params.dataSubjectTypeId)
    .replace(":consumerGroupId", params.consumerGroupId)
    .replace(":dataRequestTypeId", params.dataRequestTypeId)
    .replace(":regulationSlug", params.regulationSlug)
    .replace(":progressStep", params.progressStep)
    .replace(":invalidatedProgressStep", params.invalidatedProgressStep)
    .replace(":mappingProgress", params.mappingProgress)
    .replace(":personalInformationType", params.personalInformationType)
    .replace(":category", params.category)
    .replace(":categoryId", params.categoryId)
    .replace(":sourceId", params.sourceId)
    .replace(":sourceName", params.sourceName)
    .replace(":purposeId", params.purposeId)
    .replace(":surveyName", params.surveyName)
    .replace(":privacyNoticeProgress", params.privacyNoticeProgress)
    .replace(":requestHandlingInstructionId", params.requestHandlingInstructionId)
    .replace(":domain", params.domain)
    .replace(":name", params.name)
    .replace(":requestHandlingInstructionType", params.requestHandlingInstructionType)
    .replace(":keyId", params.keyId)
    .replace(":fileKey", params.fileKey)
    .replace(":slug", params.slug)
    .replace(":consumerId", params.consumerId)
    .replace(":taskId", params.taskId);
};

export const ApiRoutes = {
  public: {
    orgNotices: "/api/public/org/:orgId/notices",
  },
  user: {
    self: "/api/users/self",
    session: "/api/users/self/session",
    activeOrganization: "/api/users/self/activeOrganization",
    logout: "/api/logout",
    activate: "/api/users/:user/activate/:organizationId",
  },
  vendors: {
    root: "/api/vendors",
    specific: "/api/vendors/:vendorId",
  },
  processingActivities: {
    root: "/api/processingActivities",
  },
  retentionReasons: {
    root: "/api/retentionReasons",
  },
  organization: {
    root: "/api/organizations/:organizationId",
    gaWebProperty: "/api/organizations/:organizationId/gaWebProperty",
    getCompliantDone: "/api/organizations/:organizationId/getCompliantDone",
    progress: {
      root: "/api/organizations/:organizationId/progress",
      specificSurvey: "/api/organizations/:organizationId/progress/survey/:slug",
      startSpecificSurvey: "/api/organizations/:organizationId/progress/survey/:slug/start",
      completeSpecificSurvey: "/api/organizations/:organizationId/progress/survey/:slug/done",
      specific: "/api/organizations/:organizationId/progress/:keyId",
    },
    processingActivities: {
      root: "/api/organizations/:organizationId/processingActivities",
      automatic: "/api/organizations/:organizationId/processingActivities/automatic",
      specific: "/api/organizations/:organizationId/processingActivities/:id",
      specificAssociationPic:
        "/api/organizations/:organizationId/processingActivities/:id/association/:picId",
    },
    collectionGroups: {
      root: "/api/organizations/:organizationId/collectionGroups",
      default: "/api/organizations/:organizationId/collectionGroups/default",
      specific: "/api/organizations/:organizationId/collectionGroups/:dataSubjectTypeId",
      mappingProgress:
        "/api/organizations/:organizationId/collectionGroups/:dataSubjectTypeId/mappingProgress/:personalInformationType/:mappingProgress",
      vendor: {
        specific:
          "/api/organizations/:organizationId/collectionGroups/:dataSubjectTypeId/vendors/:vendorId",
        mappingProgress:
          "/api/organizations/:organizationId/collectionGroups/:dataSubjectTypeId/vendors/mappingProgress",
      },
      privacyNoticeProgress:
        "/api/organizations/:organizationId/collectionGroups/:dataSubjectTypeId/privacyNoticeProgress/:privacyNoticeProgress",
    },
    dataRetention: {
      root: "/api/organizations/:organizationId/dataRetention",
      category: "/api/organizations/:organizationId/dataRetention/:category",
    },
    dataMap: {
      root: "/api/organizations/:organizationId/dataMap",
      picCollection:
        "/api/organizations/:organizationId/dataMap/collectionGroup/:collectionGroupId/collection/:picId",
      picAssociation:
        "/api/organizations/:organizationId/dataMap/collectionGroup/:collectionGroupId/association/:vendorId",
      picSourceAssociation:
        "/api/organizations/:organizationId/dataMap/collectionGroup/:collectionGroupId/sourceAssociation/:vendorId",
      picDisclosure:
        "/api/organizations/:organizationId/dataMap/collectionGroup/:collectionGroupId/disclosure/:vendorId/:picGroupId",
      picReceived:
        "/api/organizations/:organizationId/dataMap/collectionGroup/:collectionGroupId/received/:vendorId/:picGroupId",
    },
    dataInventory: {
      snapshot: "/api/organizations/:organizationId/dataInventory/snapshot",
      collection: {
        root: "/api/organizations/:organizationId/dataInventory/collection",
        default: "/api/organizations/:organizationId/dataInventory/collection/default",
        batch: "/api/organizations/:organizationId/dataInventory/collection/:dataSubjectTypeId",
        clearInternallyStored:
          "/api/organizations/:organizationId/dataInventory/collection/:dataSubjectTypeId/category/internallyStored",
        specificCategory:
          "/api/organizations/:organizationId/dataInventory/collection/:dataSubjectTypeId/category/:categoryId",
        specificSource:
          "/api/organizations/:organizationId/dataInventory/collection/:dataSubjectTypeId/source/:sourceId",
        customSource:
          "/api/organizations/:organizationId/dataInventory/collection/:dataSubjectTypeId/customSource/:sourceName",
        specificPurpose:
          "/api/organizations/:organizationId/dataInventory/collection/:dataSubjectTypeId/purpose/:purposeId",
      },
      exchange: {
        root: "/api/organizations/:organizationId/dataInventory/exchange",
        specificConsumerGroup:
          "/api/organizations/:organizationId/dataInventory/exchange/consumerGroup/:consumerGroupId",
        specificVendor:
          "/api/organizations/:organizationId/dataInventory/exchange/vendor/:vendorId",
      },
    },
    templates: "/api/organizations/:organizationId/templates",
    mailboxes: "/api/organizations/:organizationId/mailboxes",
    mailbox: "/api/organizations/:organizationId/mailboxes/:mailbox",
    dataRequestTypes: "/api/organizations/:organizationId/dataRequestTypes",
    features: "/api/organizations/:organizationId/features",
    survey: "/api/organizations/:organizationId/survey/:surveyName",
    surveySend: "/api/organizations/:organizationId/survey/:surveyName/send",
    complianceChecklist: "/api/organizations/:organizationId/complianceChecklist",
    customMessageType: {
      list: "/api/organizations/:organizationId/templates",
      specific: "/api/organizations/:organizationId/templates/:slug",
    },
    messageSettings: "/api/organizations/:organizationId/messageSettings",
    customField: "/api/organizations/:organizationId/customField",
    financialIncentives: "/api/organizations/:organizationId/financialIncentives",
    verificationInstruction: "/api/organizations/:organizationId/verificationInstruction",
    testMessage: "/api/organizations/:organizationId/messageSettings/test",
    admin: {
      users: "/api/organizations/:organizationId/admin/users",
      specificUser: "/api/organizations/:organizationId/admin/users/:email",
      activateUser: "/api/organizations/:organizationId/admin/users/:email/activate",
      deactivateUser: "/api/organizations/:organizationId/admin/users/:email/deactivate",
      inviteUser: "/api/organizations/:organizationId/admin/users/invite",
    },
    dataSources: {
      root: "/api/organizations/:organizationId/dataSources",
      specific: "/api/organizations/:organizationId/dataSources/:id",
    },
    privacyCenters: "/api/organizations/:organizationId/privacyCenters",
    privacyCenter: {
      detail: "/api/organizations/:organizationId/privacyCenters/:privacyCenterId",
      privacyPolicy: {
        policy: "/api/organizations/:organizationId/privacyCenters/:privacyCenterId/policy",
        sections:
          "/api/organizations/:organizationId/privacyCenters/:privacyCenterId/policy/sections",
        sectionDetail:
          "/api/organizations/:organizationId/privacyCenters/:privacyCenterId/policy/sections/:sectionId",
      },
    },
    websiteAudit: {
      root: "/api/organizations/:organizationId/websiteAudit",
      reset: "/api/organizations/:organizationId/websiteAudit/reset",
    },
    cookieScanner: {
      root: "/api/organizations/:organizationId/cookieScanner",
      cookie: "/api/organizations/:organizationId/cookieScanner/cookie/:domain/:name",
      cookieRecommendations: "/api/organizations/:organizationId/cookieScanner/recommendations",
    },
    cookieConsent: {
      root: "/api/organizations/:organizationId/cookieConsent",
    },
    authorization: {
      authorizations: "/api/organizations/:organizationId/authorizations",
      google: "/api/organizations/:organizationId/authorize/google",
    },
    request: {
      root: "/api/organizations/:organizationId/requests",
      stats: "/api/organizations/:organizationId/requests/stats",
      specific: "/api/organizations/:organizationId/requests/:requestId",
      templates: {
        closed: "/api/organizations/:organizationId/requests/:requestId/templates/closed",
        contactVendor:
          "/api/organizations/:organizationId/requests/:requestId/templates/contactVendor",
      },
      autodelete: {
        googleAnalytics:
          "/api/organizations/:organizationId/requests/:requestId/autodelete/googleAnalytics",
      },
      state: "/api/organizations/:organizationId/requests/:requestId/state",
      vendor: "/api/organizations/:organizationId/requests/:requestId/vendor",
      contactVendors: "/api/organizations/:organizationId/requests/:requestId/contactVendors",
      contactRequestor: "/api/organizations/:organizationId/requests/:requestId/contactRequestor",
      verify: "/api/organizations/:organizationId/requests/:requestId/verify",
      note: "/api/organizations/:organizationId/requests/:requestId/note",
      reply: "/api/organizations/:organizationId/requests/:requestId/reply",
      reopen: "/api/organizations/:organizationId/requests/:requestId/reopen",
      stepConsumerGroup:
        "/api/organizations/:organizationId/requests/:requestId/step/consumerGroup",
      setSubjectStatus: "/api/organizations/:organizationId/requests/:requestId/subjectStatus",
      stepForward: "/api/organizations/:organizationId/requests/:requestId/step/forward",
      stepBack: "/api/organizations/:organizationId/requests/:requestId/step/back",
      extend: "/api/organizations/:organizationId/requests/:requestId/extend",
    },
    consumer: {
      requests: "/api/organizations/:organizationId/requests/consumer",
      export: "/api/organizations/:organizationId/requests/export",
    },
    task: {
      root: "/api/organizations/:organizationId/tasks",
      completeTasks: "/api/organizations/:organizationId/tasks/complete",
      completeTask: "/api/organizations/:organizationId/tasks/:taskId/complete",
      addDpo: "/api/organizations/:organizationId/tasks/addDpo",
      addAuthorizeGoogleAnalytics:
        "/api/organizations/:organizationId/tasks/authorizeGoogleAnalytics",
      completeFinIncentives: "/api/organizations/:organizationId/tasks/completeFinancialIncentives",
    },
    requestHandlingInstructions: {
      root: "/api/organizations/:organizationId/requestHandling",
      forType: "/api/organizations/:organizationId/requestHandling/:requestHandlingInstructionType",
      consumerGroup: "/api/organizations/:organizationId/requestHandling/cgInstructions",
      optOut: "/api/organizations/:organizationId/requestHandling/optOutInstructions",
      progress: "/api/organizations/:organizationId/requestHandling/mappingProgress",
      instruction:
        "/api/organizations/:organizationId/requestHandling/:requestHandlingInstructionId",
    },
    vendor: {
      catalog: "/api/organizations/:organizationId/vendors/catalog",
      root: "/api/organizations/:organizationId/vendors",
      unmapped: "/api/organizations/:organizationId/vendors/unmapped/:collectionGroupId",
      remove: "/api/organizations/:organizationId/vendors/remove",
      default: "/api/organizations/:organizationId/vendors/default",
      thirdPartyRecipients: "/api/organizations/:organizationId/vendors/thirdPartyRecipients",
      new: "/api/organizations/:organizationId/vendors/new",
      search: "/api/organizations/:organizationId/vendors/search",
      specific: "/api/organizations/:organizationId/vendors/:vendorId",
      platformApps: "/api/organizations/:organizationId/recipientPlatforms/:vendorId/apps",
      appPlatforms: "/api/organizations/:organizationId/recipientPlatforms/:vendorId/platforms",
      appInstall: "/api/organizations/:organizationId/recipientPlatforms/:platformId/apps/:appId",
      add: "/api/organizations/:organizationId/vendors/:vendorId/new",
      edit: "/api/organizations/:organizationId/vendors/:vendorId/edit",
      agreement: "/api/organizations/:organizationId/vendors/:vendorId/agreement",
      mappingProgress:
        "/api/organizations/:organizationId/vendors/:vendorId/mappingProgress/:mappingProgress",
    },
    getCompliantProgress: {
      specific: "/api/organizations/:organizationId/getCompliantProgress/:progressStep",
    },
    getCompliantInvalidatedProgress: {
      add: "/api/organizations/:organizationId/getCompliantInvalidatedProgress/:invalidatedProgressStep/add",
      remove:
        "/api/organizations/:organizationId/getCompliantInvalidatedProgress/:invalidatedProgressStep/remove",
    },
    exceptionsMappingProgress:
      "/api/organizations/:organizationId/exceptionsMappingProgress/:mappingProgress",
    cookiesMappingProgress:
      "/api/organizations/:organizationId/cookiesMappingProgress/:mappingProgress",
    privacyNoticeProgress: {
      specific: "/api/organizations/:organizationId/privacyNoticeProgress",
    },
    privacyNotice: {
      ca: "/api/organizations/:organizationId/privacyNotices/ca",
      optOut: "/api/organizations/:organizationId/privacyNotices/optOut",
      consumerGroup:
        "/api/organizations/:organizationId/privacyNotices/consumerGroup/:consumerGroupId",
    },
  },
  storage: {
    logos: {
      organization: "/api/storage/logos/organization/:organizationId",
      privacyCenter:
        "/api/storage/logos/organization/:organizationId/privacyCenter/:privacyCenterId",
    },
    vendorAgreements: {
      root: "/api/storage/vendoragreements/organization/:organizationId/vendor/:vendorId",
      specific:
        "/api/storage/vendoragreements/organization/:organizationId/vendor/:vendorId/:fileKey",
    },
  },
};

export const formatUrl = (base: string, params: any, encode: boolean = true) => {
  const join = base.includes("?") ? "&" : "?";
  return `${base}${join}${QueryString.stringify(params, { encode })}`;
};

export const ApiRouteHelpers = {
  public: {
    orgNotices: (orgId) => rep("/api/public/org/:orgId/notices", { orgId }),
  },
  user: {
    activate: (user, organizationId) => rep(ApiRoutes.user.activate, { user, organizationId }),
  },
  organization: {
    root: (organizationId) => rep(ApiRoutes.organization.root, { organizationId }),
    gaWebProperty: (organizationId) =>
      rep(ApiRoutes.organization.gaWebProperty, { organizationId }),
    templates: (organizationId) => rep(ApiRoutes.organization.templates, { organizationId }),
    mailboxes: (organizationId) => rep(ApiRoutes.organization.mailboxes, { organizationId }),
    mailbox: (organizationId, mailbox) =>
      rep(ApiRoutes.organization.mailbox, { organizationId, mailbox }),
    dataRequestTypes: (organizationId) =>
      rep(ApiRoutes.organization.dataRequestTypes, { organizationId }),
    features: (organizationId) => rep(ApiRoutes.organization.features, { organizationId }),
    survey: (organizationId, surveyName) =>
      rep(ApiRoutes.organization.survey, { organizationId, surveyName }),
    surveySend: (organizationId, surveyName) =>
      rep(ApiRoutes.organization.surveySend, { organizationId, surveyName }),
    complianceChecklist: (organizationId) =>
      rep(ApiRoutes.organization.complianceChecklist, { organizationId }),
    customMessageType: {
      list: (organizationId) =>
        rep(ApiRoutes.organization.customMessageType.list, { organizationId }),
      specific: (organizationId, slug) =>
        rep(ApiRoutes.organization.customMessageType.specific, { organizationId, slug }),
    },
    messageSettings: (organizationId) =>
      rep(ApiRoutes.organization.messageSettings, { organizationId }),
    customField: (organizationId) => rep(ApiRoutes.organization.customField, { organizationId }),
    financialIncentives: (organizationId) =>
      rep(ApiRoutes.organization.financialIncentives, { organizationId }),
    verificationInstruction: (organizationId) =>
      rep(ApiRoutes.organization.verificationInstruction, { organizationId }),
    testMessage: (organizationId) => rep(ApiRoutes.organization.testMessage, { organizationId }),
    privacyCenters: (organizationId) =>
      rep(ApiRoutes.organization.privacyCenters, { organizationId }),
    progress: {
      root: (organizationId) => rep(ApiRoutes.organization.progress.root, { organizationId }),
      specific: (organizationId, keyId) =>
        rep(ApiRoutes.organization.progress.specific, { organizationId, keyId }),
      specificSurvey: (organizationId, slug) =>
        rep(ApiRoutes.organization.progress.specificSurvey, { organizationId, slug }),

      startSpecificSurvey: (organizationId, slug) =>
        rep(ApiRoutes.organization.progress.startSpecificSurvey, { organizationId, slug }),
      completeSpecificSurvey: (organizationId, slug) =>
        rep(ApiRoutes.organization.progress.completeSpecificSurvey, { organizationId, slug }),
    },
    authorization: {
      authorizations: (organizationId) =>
        rep(ApiRoutes.organization.authorization.authorizations, { organizationId }),
      google: (organizationId) =>
        rep(ApiRoutes.organization.authorization.google, { organizationId }),
    },
    processingActivites: {
      root: (organizationId) =>
        rep(ApiRoutes.organization.processingActivities.root, { organizationId }),
      automatic: (organizationId) =>
        rep(ApiRoutes.organization.processingActivities.automatic, { organizationId }),
      specific: (organizationId, id) =>
        rep(ApiRoutes.organization.processingActivities.specific, { organizationId, id }),
      specificAssociationPic: (organizationId, id, picId) =>
        rep(ApiRoutes.organization.processingActivities.specificAssociationPic, {
          organizationId,
          id,
          picId,
        }),
    },
    privacyCenter: {
      detail: (organizationId, privacyCenterId) =>
        rep(ApiRoutes.organization.privacyCenter.detail, {
          organizationId,
          privacyCenterId,
        }),
      privacyPolicy: {
        policy: (organizationId, privacyCenterId) =>
          rep(ApiRoutes.organization.privacyCenter.privacyPolicy.policy, {
            organizationId,
            privacyCenterId,
          }),
        sections: (organizationId, privacyCenterId) =>
          rep(ApiRoutes.organization.privacyCenter.privacyPolicy.sections, {
            organizationId,
            privacyCenterId,
          }),
        section: {
          detail: (organizationId, privacyCenterId, sectionId) =>
            rep(ApiRoutes.organization.privacyCenter.privacyPolicy.sectionDetail, {
              organizationId,
              privacyCenterId,
              sectionId,
            }),
          // Not really needed since it's identical to `sections` but might be helpful somewhere?
          new: (organizationId, privacyCenterId) =>
            rep(ApiRoutes.organization.privacyCenter.privacyPolicy.sections, {
              organizationId,
              privacyCenterId,
            }),
        },
      },
    },
    requestHandlingInstructions: {
      root: (organizationId) =>
        rep(ApiRoutes.organization.requestHandlingInstructions.root, { organizationId }),
      progress: (organizationId) =>
        rep(ApiRoutes.organization.requestHandlingInstructions.progress, { organizationId }),
      consumerGroup: (organizationId) =>
        rep(ApiRoutes.organization.requestHandlingInstructions.consumerGroup, {
          organizationId,
        }),
      optOut: (organizationId) =>
        rep(ApiRoutes.organization.requestHandlingInstructions.optOut, { organizationId }),
      forType: (organizationId, requestHandlingInstructionType) =>
        rep(ApiRoutes.organization.requestHandlingInstructions.forType, {
          organizationId,
          requestHandlingInstructionType,
        }),
      instruction: (organizationId, requestHandlingInstructionId) =>
        rep(ApiRoutes.organization.requestHandlingInstructions.instruction, {
          organizationId,
          requestHandlingInstructionId,
        }),
    },
    websiteAudit: {
      root: (organizationId) => rep(ApiRoutes.organization.websiteAudit.root, { organizationId }),
      reset: (organizationId) => rep(ApiRoutes.organization.websiteAudit.reset, { organizationId }),
    },
    cookieScanner: {
      root: (organizationId) => rep(ApiRoutes.organization.cookieScanner.root, { organizationId }),
      cookie: (organizationId, domain, name) =>
        rep(ApiRoutes.organization.cookieScanner.cookie, { organizationId, domain, name }),
      cookieRecommendations: (organizationId: OrganizationPublicId) =>
        rep(ApiRoutes.organization.cookieScanner.cookieRecommendations, { organizationId }),
    },
    cookieConsent: {
      root: (organizationId) => rep(ApiRoutes.organization.cookieConsent.root, { organizationId }),
      csv: (organizationId) =>
        `${rep(ApiRoutes.organization.cookieConsent.root, { organizationId })}.csv`,
    },
    tasks: {
      root: (organizationId) => rep(ApiRoutes.organization.task.root, { organizationId }),
      completeTasks: (organizationId) =>
        rep(ApiRoutes.organization.task.completeTasks, { organizationId }),
      completeTask: (organizationId, taskId) =>
        rep(ApiRoutes.organization.task.completeTask, { organizationId, taskId }),
      addDpo: (organizationId) => rep(ApiRoutes.organization.task.addDpo, { organizationId }),
      addAuthorizeGoogleAnalytics: (organizationId) =>
        rep(ApiRoutes.organization.task.addAuthorizeGoogleAnalytics, { organizationId }),
      completeFinIncentives: (organizationId) =>
        rep(ApiRoutes.organization.task.completeFinIncentives, { organizationId }),
    },
    collectionGroups: {
      root: (organizationId) =>
        rep(ApiRoutes.organization.collectionGroups.root, { organizationId }),
      default: (organizationId) =>
        rep(ApiRoutes.organization.collectionGroups.default, { organizationId }),
      specific: (organizationId, dataSubjectTypeId) =>
        rep(ApiRoutes.organization.collectionGroups.specific, {
          organizationId,
          dataSubjectTypeId,
        }),
      vendor: (organizationId, dataSubjectTypeId, vendorId) =>
        rep(ApiRoutes.organization.collectionGroups.vendor.specific, {
          organizationId,
          dataSubjectTypeId,
          vendorId,
        }),
      mappingProgress: (
        organizationId,
        dataSubjectTypeId,
        personalInformationType,
        mappingProgress,
      ) =>
        rep(ApiRoutes.organization.collectionGroups.mappingProgress, {
          organizationId,
          dataSubjectTypeId,
          personalInformationType,
          mappingProgress,
        }),
      vendorMappingProgress: (organizationId, dataSubjectTypeId) =>
        rep(ApiRoutes.organization.collectionGroups.vendor.mappingProgress, {
          organizationId,
          dataSubjectTypeId,
        }),
      privacyNoticeProgress: (organizationId, dataSubjectTypeId, privacyNoticeProgress) =>
        rep(ApiRoutes.organization.collectionGroups.privacyNoticeProgress, {
          organizationId,
          dataSubjectTypeId,
          privacyNoticeProgress,
        }),
    },
    getCompliantDone: (organizationId) =>
      rep(ApiRoutes.organization.getCompliantDone, { organizationId }),
    dataRetention: {
      root: (organizationId) => rep(ApiRoutes.organization.dataRetention.root, { organizationId }),
      category: (organizationId, category) =>
        rep(ApiRoutes.organization.dataRetention.category, { organizationId, category }),
    },
    dataMap: {
      root: (organizationId) => rep(ApiRoutes.organization.dataMap.root, { organizationId }),
      picCollection: (organizationId, collectionGroupId, picId) =>
        rep(ApiRoutes.organization.dataMap.picCollection, {
          organizationId,
          collectionGroupId,
          picId,
        }),
      picAssociation: (organizationId, collectionGroupId, vendorId) =>
        rep(ApiRoutes.organization.dataMap.picAssociation, {
          organizationId,
          collectionGroupId,
          vendorId,
        }),
      picSourceAssociation: (organizationId, collectionGroupId, vendorId) =>
        rep(ApiRoutes.organization.dataMap.picSourceAssociation, {
          organizationId,
          collectionGroupId,
          vendorId,
        }),
      picDisclosure: (organizationId, collectionGroupId, vendorId, picGroupId) =>
        rep(ApiRoutes.organization.dataMap.picDisclosure, {
          organizationId,
          collectionGroupId,
          vendorId,
          picGroupId,
        }),
      picReceived: (organizationId, collectionGroupId, vendorId, picGroupId) =>
        rep(ApiRoutes.organization.dataMap.picReceived, {
          organizationId,
          collectionGroupId,
          vendorId,
          picGroupId,
        }),
    },
    dataInventory: {
      snapshot: (organizationId) =>
        rep(ApiRoutes.organization.dataInventory.snapshot, { organizationId }),
      collection: {
        root: (organizationId) =>
          rep(ApiRoutes.organization.dataInventory.collection.root, { organizationId }),
        default: (organizationId) =>
          rep(ApiRoutes.organization.dataInventory.collection.default, {
            organizationId,
          }),
        batch: (organizationId, dataSubjectTypeId) =>
          rep(ApiRoutes.organization.dataInventory.collection.batch, {
            organizationId,
            dataSubjectTypeId,
          }),
        clearInternallyStored: (organizationId, dataSubjectTypeId) =>
          rep(ApiRoutes.organization.dataInventory.collection.clearInternallyStored, {
            organizationId,
            dataSubjectTypeId,
          }),
        specificCategory: (organizationId, dataSubjectTypeId, categoryId) =>
          rep(ApiRoutes.organization.dataInventory.collection.specificCategory, {
            organizationId,
            dataSubjectTypeId,
            categoryId,
          }),
        specificSource: (organizationId, dataSubjectTypeId, sourceId) =>
          rep(ApiRoutes.organization.dataInventory.collection.specificSource, {
            organizationId,
            dataSubjectTypeId,
            sourceId,
          }),
        customSource: (organizationId, dataSubjectTypeId, sourceName) =>
          rep(ApiRoutes.organization.dataInventory.collection.customSource, {
            organizationId,
            dataSubjectTypeId,
            sourceName,
          }),
        specificPurpose: (organizationId, dataSubjectTypeId, purposeId) =>
          rep(ApiRoutes.organization.dataInventory.collection.specificPurpose, {
            organizationId,
            dataSubjectTypeId,
            purposeId,
          }),
      },
      exchange: {
        root: (organizationId) =>
          rep(ApiRoutes.organization.dataInventory.exchange.root, { organizationId }),
        specificConsumerGroup: (organizationId, consumerGroupId) =>
          rep(ApiRoutes.organization.dataInventory.exchange.specificConsumerGroup, {
            organizationId,
            consumerGroupId,
          }),
        specificVendor: (organizationId, vendorId) =>
          rep(ApiRoutes.organization.dataInventory.exchange.specificVendor, {
            organizationId,
            vendorId,
          }),
      },
    },
    dataSources: {
      root: (organizationId) => rep(ApiRoutes.organization.dataSources.root, { organizationId }),
      specific: (organizationId, id) =>
        rep(ApiRoutes.organization.dataSources.specific, { organizationId, id }),
    },
    vendors: {
      root: (organizationId) => rep(ApiRoutes.organization.vendor.root, { organizationId }),
      thirdPartyRecipients: (organizationId) =>
        rep(ApiRoutes.organization.vendor.thirdPartyRecipients, { organizationId }),
      catalog: (organizationId) => rep(ApiRoutes.organization.vendor.catalog, { organizationId }),
      new: (organizationId) => rep(ApiRoutes.organization.vendor.new, { organizationId }),
      search: (organizationId) => rep(ApiRoutes.organization.vendor.search, { organizationId }),
      specific: (organizationId, vendorId) =>
        rep(ApiRoutes.organization.vendor.specific, { organizationId, vendorId }),
      add: (organizationId, vendorId) =>
        rep(ApiRoutes.organization.vendor.add, { organizationId, vendorId }),
      edit: (organizationId, vendorId) =>
        rep(ApiRoutes.organization.vendor.edit, { organizationId, vendorId }),
      agreement: (organizationId, vendorId) =>
        rep(ApiRoutes.organization.vendor.agreement, { organizationId, vendorId }),
    },
    getCompliantProgress: {
      specific: (organizationId, progressStep) =>
        rep(ApiRoutes.organization.getCompliantProgress.specific, {
          organizationId,
          progressStep,
        }),
    },
    getCompliantInvalidatedProgress: {
      add: (organizationId, invalidatedProgressStep) =>
        rep(ApiRoutes.organization.getCompliantInvalidatedProgress.add, {
          organizationId,
          invalidatedProgressStep,
        }),
      remove: (organizationId, invalidatedProgressStep) =>
        rep(ApiRoutes.organization.getCompliantInvalidatedProgress.remove, {
          organizationId,
          invalidatedProgressStep,
        }),
    },
    exceptionsMappingProgress: (organizationId, mappingProgress) =>
      rep(ApiRoutes.organization.exceptionsMappingProgress, {
        organizationId,
        mappingProgress,
      }),
    cookiesMappingProgress: (organizationId, mappingProgress) =>
      rep(ApiRoutes.organization.cookiesMappingProgress, {
        organizationId,
        mappingProgress,
      }),
    privacyNoticeProgress: {
      specific: (organizationId) =>
        rep(ApiRoutes.organization.privacyNoticeProgress.specific, { organizationId }),
    },
    privacyNotice: {
      ca: (organizationId) => rep(ApiRoutes.organization.privacyNotice.ca, { organizationId }),
      optOut: (organizationId) =>
        rep(ApiRoutes.organization.privacyNotice.optOut, { organizationId }),
      consumerGroup: (organizationId, consumerGroupId) =>
        rep(ApiRoutes.organization.privacyNotice.consumerGroup, {
          organizationId,
          consumerGroupId,
        }),
    },
  },
  vendors: {
    specific: (vendorId) => rep(ApiRoutes.vendors.specific, { vendorId }),
  },
  storage: {
    logos: {
      organization: (organizationId) =>
        rep(ApiRoutes.storage.logos.organization, { organizationId }),
      privacyCenter: (organizationId, privacyCenterId) =>
        rep(ApiRoutes.storage.logos.organization, { organizationId, privacyCenterId }),
    },
    vendorAgreements: {
      vendor: (organizationId, vendorId) =>
        rep(ApiRoutes.storage.vendorAgreements.root, { organizationId, vendorId }),
      specific: (organizationId, vendorId, fileKey) =>
        rep(ApiRoutes.storage.vendorAgreements.specific, {
          organizationId,
          vendorId,
          fileKey,
        }),
    },
  },

  consumer: {
    requests: (organizationId) => rep(ApiRoutes.organization.consumer.requests, { organizationId }),
    export: (organizationId) => rep(ApiRoutes.organization.consumer.export, { organizationId }),
  },

  organizationRequests: (organizationId) =>
    rep(ApiRoutes.organization.request.root, { organizationId }),

  organizationRequestsCsv: (organizationId) =>
    `${rep(ApiRoutes.organization.request.root, { organizationId })}.csv`,

  organizationRequestStats: (organizationId) =>
    rep(ApiRoutes.organization.request.stats, { organizationId }),

  organizationRequest: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.specific, { organizationId, requestId }),
  organizationRequestExtend: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.extend, { organizationId, requestId }),

  organizationRequestClosedTemplate: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.templates.closed, { organizationId, requestId }),

  organizationRequestContactVendorsMessageTemplate: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.templates.contactVendor, { organizationId, requestId }),

  organizationRequestState: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.state, { organizationId, requestId }),

  organizationRequestVerify: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.verify, { organizationId, requestId }),

  organizationRequestAutodeleteGoogleAnalytics: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.autodelete.googleAnalytics, { organizationId, requestId }),

  organizationRequestVendor: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.vendor, { organizationId, requestId }),

  organizationRequestContactVendors: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.contactVendors, { organizationId, requestId }),

  organizationRequestContactRequestor: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.contactRequestor, { organizationId, requestId }),

  organizationRequestNote: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.note, { organizationId, requestId }),

  organizationRequestReply: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.reply, { organizationId, requestId }),

  organizationRequestReopen: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.reopen, { organizationId, requestId }),

  organizationRequestStepConsumerGroup: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.stepConsumerGroup, { organizationId, requestId }),

  organizationRequestSetSubjectStatus: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.setSubjectStatus, { organizationId, requestId }),

  organizationRequestStepForward: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.stepForward, { organizationId, requestId }),

  organizationRequestStepBack: (organizationId, requestId) =>
    rep(ApiRoutes.organization.request.stepBack, { organizationId, requestId }),

  organizationVendors: (organizationId) =>
    rep(ApiRoutes.organization.vendor.root, { organizationId }),

  organizationUnmappedVendors: (organizationId, collectionGroupId) =>
    rep(ApiRoutes.organization.vendor.unmapped, { organizationId, collectionGroupId }),

  organizationRemoveVendors: (organizationId) =>
    rep(ApiRoutes.organization.vendor.remove, { organizationId }),

  organizationVendorsDefault: (organizationId) =>
    rep(ApiRoutes.organization.vendor.default, { organizationId }),

  organizationVendor: (organizationId, vendorId) =>
    rep(ApiRoutes.organization.vendor.specific, { organizationId, vendorId }),

  organizationVendorAgreement: (organizationId, vendorId) =>
    rep(ApiRoutes.organization.vendor.agreement, { organizationId, vendorId }),

  organizationPlatformApps: (organizationId, vendorId) =>
    rep(ApiRoutes.organization.vendor.platformApps, { organizationId, vendorId }),

  organizationAppPlatforms: (organizationId, vendorId) =>
    rep(ApiRoutes.organization.vendor.appPlatforms, { organizationId, vendorId }),

  organizationPlatformInstall: (organizationId, platformId, appId) =>
    rep(ApiRoutes.organization.vendor.appInstall, { organizationId, platformId, appId }),

  organizationVendorCatalog: (organizationId) =>
    rep(ApiRoutes.organization.vendor.catalog, { organizationId }),

  organizationVendorMappingProgress: (organizationId, vendorId, mappingProgress) =>
    rep(ApiRoutes.organization.vendor.mappingProgress, {
      organizationId,
      vendorId,
      mappingProgress,
    }),

  organizationAdminUsers: (organizationId) =>
    rep(ApiRoutes.organization.admin.users, { organizationId }),

  organizationAdminSpecificUser: (organizationId, email) =>
    rep(ApiRoutes.organization.admin.specificUser, { organizationId, email }),

  organizationAdminActivateUser: (organizationId, email) =>
    rep(ApiRoutes.organization.admin.activateUser, { organizationId, email }),

  organizationAdminDeactivateUser: (organizationId, email) =>
    rep(ApiRoutes.organization.admin.deactivateUser, { organizationId, email }),

  organizationAdminInviteUser: (organizationId) =>
    rep(ApiRoutes.organization.admin.inviteUser, { organizationId }),
};
