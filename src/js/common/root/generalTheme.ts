import { createTheme } from "@mui/material/styles";
import { variables } from "./variables";

const rawTheme = {
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  palette: {
    primary: {
      main: variables.colors.primary,
    },
    secondary: {
      main: variables.colors.secondary,
    },
    warning: {
      main: variables.colors.warning,
    },
    error: {
      main: variables.colors.error,
    },
    text: {
      primary: variables.colors.textMain,
    },
  },
};

// @ts-ignore
export const theme = createTheme(variables.colors.primary ? rawTheme : undefined);
