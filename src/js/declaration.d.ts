export type PolarisGlobals = {
  scrollPolarisTo: (offset: number) => void;
  scrollSelectorIntoView: (selector: string) => void;
};

declare global {
  interface Window {
    polaris: PolarisGlobals;
  }
}

window.polaris = window.polaris || {};
