package polaris.models.entity.request

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.models.dto.orgDraftSchema.v1.BusinessSurvey
import polaris.models.dto.orgDraftSchema.v1.OrgDraftSchemaV1
import polaris.models.dto.orgDraftSchema.v1.Surveys
import polaris.repositories.OrganizationDraftRepository
import polaris.repositories.OrganizationRepository
import util.ObjectFactory
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class OrganizationDraftIT : BaseIntegrationTest() {
    @Autowired
    private lateinit var draftRepository: OrganizationDraftRepository

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Test
    fun serialize__readValue() {
        val org = persist(ObjectFactory.defaultOrganization())
        val draft = persist(ObjectFactory.defaultOrgDraft(org))

        flushAndClear()

        val fetchedDrafts = draftRepository.findAllByOrganization(org)
        val fetchedDraft = fetchedDrafts.firstOrNull()

        assertNotNull(fetchedDraft)
        assertEquals(true, fetchedDraft.data.surveys.`business-survey`.`business-physical-location`)
    }

    @Test
    fun serialize__writeDataValue() {
        val org = persist(ObjectFactory.defaultOrganization())
        val draft = persist(ObjectFactory.defaultOrgDraft(org))

        draft.data = OrgDraftSchemaV1(Surveys(BusinessSurvey(`business-california-nonenglish` = true)))
        draftRepository.save(draft)

        flushAndClear()

        val fetchedDrafts = draftRepository.findAllByOrganization(org)
        val fetchedDraft = fetchedDrafts.firstOrNull()

        assertNotNull(fetchedDraft)
        assertNull(fetchedDraft.data.surveys.`business-survey`.`business-physical-location`)
        assertEquals(true, fetchedDraft.data.surveys.`business-survey`.`business-california-nonenglish`)
    }

    @Test
    fun currentDraft__orgHasExpectedCurrentDraft() {
        val org = persist(ObjectFactory.defaultOrganization())
        val draft = persist(ObjectFactory.defaultOrgDraft(org))
        val otherDraft = persist(ObjectFactory.defaultOrgDraft(org))

        org.currentDraft = draft
        persist(org)

        flushAndClear()

        var fetchedOrg = organizationRepository.findByPublicId(org.publicId)

        assertEquals(draft.id, fetchedOrg?.currentDraft!!.id)

        fetchedOrg.currentDraft = otherDraft
        persist(fetchedOrg)

        flushAndClear()

        fetchedOrg = organizationRepository.findByPublicId(org.publicId)

        assertEquals(otherDraft.id, fetchedOrg?.currentDraft!!.id)
    }
}
