package polaris.models.entity.request

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner
import kotlin.test.assertEquals

@RunWith(SpringRunner::class)
class VendorSelectionOptionTest {

    @Test
    fun serialize__readValue() {
        val mapper = ObjectMapper()

        val noData = mapper.readValue("\"NO_DATA\"", VendorOutcomeOption::class.java)
        val erased = mapper.readValue("\"ERASED\"", VendorOutcomeOption::class.java)

        assertEquals(VendorOutcomeOption.NO_DATA, noData)
        assertEquals(VendorOutcomeOption.ERASED, erased)
    }

    @Test
    fun serialize__readValueOfArray() {
        val mapper = jacksonObjectMapper()

        val noData: List<VendorOutcomeOption> = mapper.readValue("[\"NO_DATA\"]")

        assertEquals(1, noData.size)
        assertEquals(VendorOutcomeOption.NO_DATA, noData[0])
    }
}
