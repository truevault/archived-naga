package polaris.models.entity.request.organization

import com.fasterxml.jackson.core.JsonProcessingException
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import polaris.BaseUnitTest
import polaris.models.entity.organization.OrganizationSurveyQuestion
import kotlin.test.assertEquals

class OrganizationSurveyQuestionTest : BaseUnitTest() {
    @ParameterizedTest(name = "{index}: sanitizeAnswerList - with valid JSON answer {0}, expect {1}")
    @CsvSource(
        // null use case
        "|",
        // valid JSON, non-array use cases
        "a|a",
        "\"a\"|\"a\"",
        "\"a b c\"|\"a b c\"",
        // valid JSON array use cases
        "[]|[]",
        "[\"\"]|[\"\"]",
        "[\"none\"]|[\"none\"]",
        "[\"none\",\"none\"]|[\"none\"]",
        "[\"a\"]|[\"a\"]",
        "[\"a\",\"none\"]|[\"a\"]",
        "[\"none\",\"a\"]|[\"a\"]",
        "[\"none\",\"a\",\"none\"]|[\"a\"]",
        "[\"none\",\"a\",\"b\"]|[\"a\",\"b\"]",
        "[\"a\",\"none\",\"b\"]|[\"a\",\"b\"]",
        "[\"a\",\"b\",\"none\"]|[\"a\",\"b\"]",
        "[\"string with none in it\",\"a\"]|[\"string with none in it\",\"a\"]",
        "[\"wordwithnoneinit\",\"a\"]|[\"wordwithnoneinit\",\"a\"]",
        "[1,2,3]|[1,2,3]",
        // non-"none" (though questionable) JSON edge cases (returned as-is)
        "[unquoted]|[unquoted]",
        "[\"quoted\",unquoted]|[\"quoted\",unquoted]",
        delimiter = '|'
    )
    fun `sanitizeAnswerList - with valid JSON answer`(answer: String?, expectedResult: String?) {
        assertEquals(expectedResult, OrganizationSurveyQuestion.sanitizeAnswerList(answer))
    }

    @ParameterizedTest(name = "{index}: sanitizeAnswerList - with invalid JSON answer {0}, expect {1}")
    @CsvSource(
        // invalid JSON use cases with "none"
        "[\"none\",unquoted]",
        delimiter = '|'
    )
    fun `sanitizeAnswerList - with invalid JSON answer`(answer: String?) {
        assertThrows<JsonProcessingException> { OrganizationSurveyQuestion.sanitizeAnswerList(answer) }
    }
}
