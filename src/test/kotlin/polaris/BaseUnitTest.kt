package polaris

import org.junit.Before
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
abstract class BaseUnitTest {
    @Before
    fun initMocks() {
        MockitoAnnotations.initMocks(this)
    }
}
