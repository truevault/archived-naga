package polaris

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.transaction.annotation.Transactional
import polaris.services.support.EmailService
import polaris.util.PersistenceUtils

@SpringBootTest(classes = [Application::class])
@Transactional
abstract class BaseIntegrationTest {

    @Autowired
    private lateinit var persistenceUtils: PersistenceUtils

    @MockBean
    protected lateinit var emailService: EmailService

    // ///////////////////////////////////////////////
    // PERSISTENCE HELPERS
    //
    // Use these helpers for setting up test data
    // and simulating transaction boundaries to ensure
    // your methods under test are actually hitting the DB
    // rather than the first-level cache.
    // ///////////////////////////////////////////////

    fun <T> persist(entity: T): T {
        return persistenceUtils.persist(entity)
    }

    fun <T> persistAndFlush(entity: T): T {
        return persistenceUtils.persistAndFlush(entity)
    }

    fun <T> persistAll(entities: List<T>): List<T> {
        return persistenceUtils.persistAll(entities)
    }

    fun <T> persistAllAndFlush(entities: List<T>): List<T> {
        return persistenceUtils.persistAllAndFlush(entities)
    }

    fun clear() {
        return persistenceUtils.clear()
    }

    /**
     * Flush all changes to the DB and clear the persistence context of all entities.
     *
     * This should be called AFTER the test setup and BEFORE the method under test is invoked to
     * ensure that all subsequent queries make actual calls to the DB rather than using the
     * cached entities in the persistence context.
     */
    fun flushAndClear() {
        return persistenceUtils.flushAndClear()
    }
}
