package polaris.services

import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.models.dto.organization.RequestHandlingInstructionDto
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.RequestHandlingInstructionsRepository
import polaris.repositories.VendorRepository
import polaris.services.primary.RequestHandlingInstructionsService
import util.Factory
import javax.transaction.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class RequestHandlingInstructionsServiceTest {
    companion object {
        private val log = LoggerFactory.getLogger(RequestHandlingInstructionsServiceTest::class.java)
    }

    @Autowired
    private lateinit var orgRepo: OrganizationRepository
    @Autowired
    private lateinit var dataRecipientRepo: OrganizationDataRecipientRepository
    @Autowired
    private lateinit var vendorRepository: VendorRepository
    @Autowired
    private lateinit var requestHandlingInstructionsRepository: RequestHandlingInstructionsRepository

    @Autowired
    private lateinit var rhiService: RequestHandlingInstructionsService

    @Test
    fun test__createOrUpdate__creates() {
        val org = Factory.createOrganization(orgRepo)
        val googleAds = Factory.createVendor(vendorRepository, name = "Google Ads")
        val orgGoogleAds = Factory.createOrganizationVendor(dataRecipientRepo, o = org, vendor = googleAds)

        orgRepo.refresh(org)

        var googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        assertNull(googleAdsInstructions)

        val dto = RequestHandlingInstructionDto(
            organizationId = org.publicId,
            requestType = DataRequestInstructionType.DELETE,
            vendorId = googleAds.id.toString(),
            processingMethod = ProcessingMethod.DELETE
        )

        rhiService.createOrUpdate(org.publicId, dto)
        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        assertNotNull(googleAdsInstructions)
    }

    @Test
    fun test__createOrUpdate__updates() {
        val org = Factory.createOrganization(orgRepo)
        val googleAds = Factory.createVendor(vendorRepository, name = "Google Ads")
        val orgGoogleAds = Factory.createOrganizationVendor(dataRecipientRepo, o = org, vendor = googleAds)

        orgRepo.refresh(org)

        var googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        assertNull(googleAdsInstructions)

        val dto = RequestHandlingInstructionDto(
            organizationId = org.publicId,
            requestType = DataRequestInstructionType.DELETE,
            vendorId = googleAds.id.toString(),
            processingMethod = ProcessingMethod.DELETE
        )

        rhiService.createOrUpdate(org.publicId, dto)
        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)

        assertNotNull(googleAdsInstructions)
        assertEquals(ProcessingMethod.DELETE, googleAdsInstructions.processingMethod)

        val updateDto = RequestHandlingInstructionDto(
            organizationId = org.publicId,
            requestType = DataRequestInstructionType.DELETE,
            vendorId = googleAds.id.toString(),
            processingMethod = ProcessingMethod.INACCESSIBLE_OR_NOT_STORED
        )
        rhiService.createOrUpdate(org.publicId, updateDto)

        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        assertNotNull(googleAdsInstructions)
        assertEquals(ProcessingMethod.INACCESSIBLE_OR_NOT_STORED, googleAdsInstructions.processingMethod)
    }

    @Test
    fun test__createOrUpdate__patchesWhenDeleteFlagFalse() {
        val org = Factory.createOrganization(orgRepo)
        val googleAds = Factory.createVendor(vendorRepository, name = "Google Ads")
        val orgGoogleAds = Factory.createOrganizationVendor(dataRecipientRepo, o = org, vendor = googleAds)

        orgRepo.refresh(org)

        var googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        assertNull(googleAdsInstructions)

        val dto = RequestHandlingInstructionDto(
            organizationId = org.publicId,
            requestType = DataRequestInstructionType.DELETE,
            vendorId = googleAds.id.toString(),
            processingMethod = ProcessingMethod.DELETE,
            processingInstructions = "These are my instructions"
        )

        rhiService.createOrUpdate(org.publicId, dto, deleteOnNull = false)
        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)

        assertNotNull(googleAdsInstructions)
        assertEquals("These are my instructions", googleAdsInstructions.processingInstructions)

        val updateDto = RequestHandlingInstructionDto(
            organizationId = org.publicId,
            requestType = DataRequestInstructionType.DELETE,
            vendorId = googleAds.id.toString(),
            processingMethod = ProcessingMethod.INACCESSIBLE_OR_NOT_STORED
        )
        rhiService.createOrUpdate(org.publicId, updateDto, deleteOnNull = false)

        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        assertNotNull(googleAdsInstructions)
        assertEquals("These are my instructions", googleAdsInstructions.processingInstructions)
    }

    @Test
    fun test__createOrUpdate__putsWhenDeleteFlagTrue() {
        val org = Factory.createOrganization(orgRepo)
        val googleAds = Factory.createVendor(vendorRepository, name = "Google Ads")
        val orgGoogleAds = Factory.createOrganizationVendor(dataRecipientRepo, o = org, vendor = googleAds)

        orgRepo.refresh(org)

        var googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        assertNull(googleAdsInstructions)

        val dto = RequestHandlingInstructionDto(
            organizationId = org.publicId,
            requestType = DataRequestInstructionType.DELETE,
            vendorId = googleAds.id.toString(),
            processingMethod = ProcessingMethod.DELETE,
            processingInstructions = "These are my instructions"
        )

        rhiService.createOrUpdate(org.publicId, dto, deleteOnNull = true)
        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)

        assertNotNull(googleAdsInstructions)
        assertEquals("These are my instructions", googleAdsInstructions.processingInstructions)

        val updateDto = RequestHandlingInstructionDto(
            organizationId = org.publicId,
            requestType = DataRequestInstructionType.DELETE,
            vendorId = googleAds.id.toString(),
            processingMethod = ProcessingMethod.INACCESSIBLE_OR_NOT_STORED
        )
        rhiService.createOrUpdate(org.publicId, updateDto, deleteOnNull = true)

        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        assertNotNull(googleAdsInstructions)
        assertEquals(null, googleAdsInstructions.processingInstructions)
    }
}
