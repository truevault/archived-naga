package polaris.services

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.anyBoolean
import org.mockito.Mockito.lenient
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.kotlin.any
import org.mockito.kotlin.anyOrNull
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.whenever
import polaris.BaseUnitTest
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubstate
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequestEvent
import polaris.repositories.DataRequestRepository
import polaris.services.primary.DataSubjectRequestAutoCloseService
import polaris.services.primary.DataSubjectRequestService
import polaris.services.support.EmailService
import polaris.services.support.OptOutService
import util.ObjectFactory
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.stream.Stream

class DataSubjectRequestAutoCloseTest : BaseUnitTest() {

    @Mock
    private lateinit var dataRequestRepository: DataRequestRepository

    @Mock
    private lateinit var dataSubjectRequestService: DataSubjectRequestService

    @Mock
    private lateinit var emailService: EmailService

    @Mock
    private lateinit var optOutService: OptOutService

    @InjectMocks
    private lateinit var subject: DataSubjectRequestAutoCloseService

    companion object {
        @JvmStatic
        fun staleDataSubjectRequestArgs(): Stream<Arguments> = Stream.of(
            Arguments.of(DataRequestType.CCPA_OPT_OUT, false, 0, 0, 0),
            Arguments.of(DataRequestType.CCPA_OPT_OUT, true, 0, 0, 0),
            Arguments.of(DataRequestType.CCPA_RIGHT_TO_DELETE, false, 1, 0, 1),
            Arguments.of(DataRequestType.CCPA_RIGHT_TO_DELETE, true, 1, 1, 1),
            Arguments.of(DataRequestType.CCPA_RIGHT_TO_KNOW, false, 0, 0, 1),
            Arguments.of(DataRequestType.CCPA_RIGHT_TO_KNOW, true, 0, 0, 1),
            Arguments.of(DataRequestType.CCPA_RIGHT_TO_KNOW_CATEGORIES, false, 0, 0, 0),
            Arguments.of(DataRequestType.CCPA_RIGHT_TO_KNOW_CATEGORIES, true, 0, 0, 0),
            Arguments.of(DataRequestType.GDPR_ACCESS, true, 0, 0, 1),
            Arguments.of(DataRequestType.GDPR_ERASURE, true, 0, 0, 1),
            Arguments.of(DataRequestType.GDPR_OBJECTION, true, 0, 0, 1),
            Arguments.of(DataRequestType.GDPR_WITHDRAWAL, true, 0, 0, 1),
            Arguments.of(DataRequestType.GDPR_RECTIFICATION, true, 0, 0, 1),
            Arguments.of(DataRequestType.GDPR_PORTABILITY, true, 0, 0, 1)
        )
    }

    @ParameterizedTest
    @MethodSource("staleDataSubjectRequestArgs")
    fun `autoClose - single stale data subject request`(
        requestType: DataRequestType,
        hasOptOut: Boolean,
        expectedOptOutsProcessed: Int,
        expectedOptOutsCreated: Int,
        expectedEmailsSent: Int
    ) {
        // given
        val now = ZonedDateTime.now()
        val organization = ObjectFactory.defaultOrganization()
        val staleRequest = ObjectFactory.defaultDataRequest(
            organization,
            requestType = requestType,
            requestDate = now.minusDays(30),
            requestDueDate = now.minusDays(1)
        )
        var eventCreated: DataSubjectRequestEvent? = null

        // stubs
        lenient().doReturn(hasOptOut)
            .whenever(optOutService).processesOptOut(any())

        lenient().doReturn(ObjectFactory.defaultDataRequest(organization, DataRequestType.CCPA_OPT_OUT))
            .whenever(dataSubjectRequestService).create(anyOrNull(), anyOrNull(), anyOrNull(), anyBoolean())

        doReturn("foo")
            .whenever(emailService).getSubjectRequestClosedNotVerifiedBody(staleRequest)

        doAnswer {
            val eventsCreated = (it.arguments[1] as List<*>).filterIsInstance<DataSubjectRequestEvent>()
            eventCreated = eventsCreated[0]
            return@doAnswer eventsCreated
        }.whenever(dataSubjectRequestService).addEventsToRequest(any(), anyOrNull())

        // when
        subject.autoClose(staleRequest, DataRequestType.CCPA_OPT_OUT)

        // then
        assertEquals(DataRequestState.CLOSED, staleRequest.state)
        assertEquals(DataRequestSubstate.SUBJECT_NOT_VERIFIED, staleRequest.substate)
        assertTrue(ChronoUnit.MILLIS.between(now, staleRequest.closedAt) < 500)
        assertTrue(staleRequest.isAutoclosed)

        assertNotNull(eventCreated)
        eventCreated?.message?.let {
            if (expectedOptOutsCreated == 1) {
                assertTrue(it.startsWith("Converted to Request to Opt-out, New request:"))
            }
            assertTrue(it.endsWith("foo"))
        }

        verify(optOutService, times(expectedOptOutsProcessed))
            .processesOptOut(any())

        verify(dataSubjectRequestService, times(expectedOptOutsCreated))
            .create(anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull())

        verify(emailService, times(expectedEmailsSent))
            .sendSubjectRequestClosedNotVerifiedBody(any())
    }
}
