package polaris.services

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.kotlin.any
import org.mockito.kotlin.anyOrNull
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.doThrow
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException
import polaris.BaseIntegrationTest
import polaris.models.entity.organization.Organization
import polaris.models.entity.request.DataRequestEventType
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubstate
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import polaris.repositories.DataRequestRepository
import polaris.repositories.OrganizationRepository
import polaris.services.primary.DataSubjectRequestAutoCloseService
import polaris.services.support.email.EmailResponse
import util.ObjectFactory
import java.time.ZonedDateTime
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class DataSubjectRequestAutoCloseIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var dataRequestRepository: DataRequestRepository

    @Autowired
    private lateinit var subject: DataSubjectRequestAutoCloseService

    private lateinit var now: ZonedDateTime
    private lateinit var backupTime: ZonedDateTime

    private var orgs: MutableList<Organization> = ArrayList()
    private var dsrs: MutableList<DataSubjectRequest> = ArrayList()

    @BeforeEach
    fun setup() {
        now = ZonedDateTime.now()
        backupTime = now.minusDays(30)

        whenever(emailService.getSubjectRequestClosedNotVerifiedBody(anyOrNull()))
            .doReturn("foo")
    }

    @Test
    fun `autoCloseRequests - with multiple orgs, multiple DSRs, and 1 org is soft-deleted`() {
        // given
        val activeOrg1 = setupOrganization("org 1")
        val activeOrg2 = setupOrganization("org 2")
        val deletedOrg1 = setupOrganization("org 3 - soft deleted")

        setupDataRequest(activeOrg1, DataRequestType.CCPA_RIGHT_TO_KNOW)
        setupDataRequest(activeOrg1, DataRequestType.CCPA_RIGHT_TO_DELETE)
        setupDataRequest(deletedOrg1, DataRequestType.CCPA_RIGHT_TO_KNOW)
        setupDataRequest(deletedOrg1, DataRequestType.CCPA_RIGHT_TO_DELETE)
        setupDataRequest(activeOrg2, DataRequestType.CCPA_RIGHT_TO_KNOW)
        setupDataRequest(activeOrg2, DataRequestType.CCPA_RIGHT_TO_DELETE)

        // soft-delete 1 org (PL-1693)
        // TODO: move the organization deletion logic into service layer (currently it lives in controller)
        organizationRepository.delete(deletedOrg1)

        val expectedAutoClosedDsrIds = listOf(dsrs[0].id, dsrs[1].id, dsrs[4].id, dsrs[5].id)
        val expectedUnprocessedDsrIds = listOf(dsrs[2].id, dsrs[3].id)

        // when
        flushAndClear()
        subject.autoCloseRequests()
        flushAndClear()

        // then

        // all DSRs should have been processed
        val remainingAutoCloseableDsrs = dataRequestRepository.findAutoCloseable(now, backupTime)
        assertEquals(0, remainingAutoCloseableDsrs.size)

        // all DSRs should have been auto-closed
        val actualAutoClosedDsrs = dataRequestRepository.findAllById(expectedAutoClosedDsrIds)
        assertEquals(4, actualAutoClosedDsrs.size)
        for (dsr in actualAutoClosedDsrs) {
            verifyWasAutoClosed(dsr)
        }

        // NOTE: this JPA exception currently occurs whenever fetching
        //       a DataSubjectRequest entity associated to a soft-deleted Organization.
        // TODO: after org deletion logic is moved to service layer, refactor
        //       this spec to fetch unprocessed DSRs from DB and assert their state
        assertThrows<JpaObjectRetrievalFailureException> {
            dataRequestRepository.findAllById(expectedUnprocessedDsrIds)
        }

        // all DSRs should have at least attempted to send email
        verify(emailService, times(expectedAutoClosedDsrIds.size))
            .sendSubjectRequestClosedNotVerifiedBody(any())
    }

    @Disabled("Enable after addressing TODO")
    @Test
    fun `autoCloseRequests - with multiple orgs that have direct transmission selling data recipients`() {
        // given

        // TODO: implement organization setup that results in additional opt-outs created
        val activeOrg1 = setupOrganization("org 1", true)
        val activeOrg2 = setupOrganization("org 2", true)

        setupDataRequest(activeOrg1, DataRequestType.CCPA_RIGHT_TO_KNOW)
        setupDataRequest(activeOrg1, DataRequestType.CCPA_RIGHT_TO_DELETE)
        setupDataRequest(activeOrg2, DataRequestType.CCPA_RIGHT_TO_KNOW)
        setupDataRequest(activeOrg2, DataRequestType.CCPA_RIGHT_TO_DELETE)

        val expectedAutoClosedDsrIds = listOf(dsrs[0].id, dsrs[1].id, dsrs[2].id, dsrs[3].id)

        // when
        flushAndClear()
        subject.autoCloseRequests()
        flushAndClear() // simulate end of transactional boundary

        // then

        // all DSRs should have been processed
        val remainingAutoCloseableDsrs = dataRequestRepository.findAutoCloseable(now, backupTime)
        assertEquals(0, remainingAutoCloseableDsrs.size)

        val autoCreatedRequests = dataRequestRepository.findByOrganizationAndStateIsIn(
            activeOrg1,
            listOf(DataRequestState.REMOVE_CONSUMER, DataRequestState.PENDING_EMAIL_VERIFICATION)
        )
        assertEquals(2, autoCreatedRequests.size)
        assertEquals(
            2,
            autoCreatedRequests.filter { it.requestType == DataRequestType.CCPA_OPT_OUT && it.state == DataRequestState.REMOVE_CONSUMER }.size
        )

        // all DSRs should have been auto-closed
        val actualAutoClosedDsrs = dataRequestRepository.findAllById(expectedAutoClosedDsrIds)
        assertEquals(4, actualAutoClosedDsrs.size)
        for (dsr in actualAutoClosedDsrs) {
            verifyWasAutoClosed(dsr)
        }

        // all DSRs should have at least attempted to send email
        verify(emailService, times(expectedAutoClosedDsrIds.size))
            .sendSubjectRequestClosedNotVerifiedBody(any())
    }

    @Test
    fun `autoCloseRequests - with exception while sending email`() {
        // given
        val activeOrg1 = setupOrganization("org 1")
        val activeOrg2 = setupOrganization("org 2")

        setupDataRequest(activeOrg1, DataRequestType.CCPA_RIGHT_TO_KNOW)
        setupDataRequest(activeOrg1, DataRequestType.CCPA_RIGHT_TO_DELETE)
        setupDataRequest(activeOrg2, DataRequestType.CCPA_RIGHT_TO_KNOW)
        setupDataRequest(activeOrg2, DataRequestType.CCPA_RIGHT_TO_DELETE)

        val expectedAutoClosedDsrIds = listOf(dsrs[0].id, dsrs[1].id, dsrs[2].id, dsrs[3].id)

        // simulate random exception from email provider
        whenever(emailService.sendSubjectRequestClosedNotVerifiedBody(anyOrNull()))
            .doReturn(EmailResponse(statusCode = 200))
            .doThrow(java.lang.RuntimeException("ouch!"))
            .doReturn(EmailResponse(statusCode = 200))
            .doReturn(EmailResponse(statusCode = 200))

        // when
        flushAndClear()
        subject.autoCloseRequests()
        flushAndClear() // simulate end of transactional boundary

        // then

        // all DSRs should have been processed
        val remainingAutoCloseableDsrs = dataRequestRepository.findAutoCloseable(now, backupTime)
        assertEquals(0, remainingAutoCloseableDsrs.size)

        // all DSRs should have been auto-closed
        val actualAutoClosedDsrs = dataRequestRepository.findAllById(expectedAutoClosedDsrIds)
        assertEquals(4, actualAutoClosedDsrs.size)
        for (dsr in actualAutoClosedDsrs) {
            verifyWasAutoClosed(dsr)
        }

        // all DSRs should have at least attempted to send email
        verify(emailService, times(expectedAutoClosedDsrIds.size))
            .sendSubjectRequestClosedNotVerifiedBody(any())
    }

    @Test
    fun `closeAsDuplicate - sets duplicate flags as expected`() {
        val org = setupOrganization("org 1")
        val original = setupDataRequest(org, DataRequestType.CCPA_RIGHT_TO_DELETE)
        val duplicate = setupDataRequest(org, DataRequestType.CCPA_RIGHT_TO_DELETE)

        flushAndClear()
        subject.closeAsDuplicate(duplicate, original)
        flushAndClear()

        assertNotNull(duplicate.duplicatesRequest)
        assertEquals(duplicate.duplicatesRequest!!.id, original.id)
        assertTrue(duplicate.isDuplicate())
        assertNotNull(duplicate.closedAt)
        assertEquals(duplicate.state, DataRequestState.CLOSED)
        assertEquals(duplicate.substate, DataRequestSubstate.DUPLICATE_REQUEST)
    }

    // ///////////////////////////////
    // Helpers
    // ///////////////////////////////

    private fun setupOrganization(name: String, hasDirectTransmissionSellingDataRecipients: Boolean = false): Organization {
        var org = ObjectFactory.defaultOrganization(name = name)

        if (hasDirectTransmissionSellingDataRecipients) {
            // TODO: implement setup for direct transmission selling data recipients
        }

        org = persistAndFlush(org)
        orgs.add(org)
        return org
    }

    private fun setupDataRequest(organization: Organization, type: DataRequestType): DataSubjectRequest {
        val dsr = persistAndFlush(
            ObjectFactory.defaultDataRequest(
                organization,
                requestType = type,
                requestDate = now.minusDays(30),
                requestDueDate = now.minusDays(1)
            )
        )
        dsrs.add(dsr)
        return dsr
    }

    private fun verifyWasAutoClosed(dsr: DataSubjectRequest) {
        with(dsr) {
            assertEquals(DataRequestState.CLOSED, this.state)
            assertEquals(DataRequestSubstate.SUBJECT_NOT_VERIFIED, this.substate)
            assertNotNull(this.closedAt)
            assertTrue(this.isAutoclosed)
            assertEquals(1, dsr.dataSubjectRequestEvents.size)
            with(dsr.dataSubjectRequestEvents[0]) {
                assertNull(this.actor)
                assertFalse(this.isDataSubject)
                assertEquals(DataRequestEventType.CLOSED_NOT_VERIFIED, this.eventType)
                this.message?.let {
                    assertTrue(it.endsWith("foo"))
                }
            }
        }
    }
}
