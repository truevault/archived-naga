package polaris.services

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.models.entity.vendor.GDPRProcessorRecommendation
import polaris.repositories.*
import polaris.services.primary.OrganizationVendorService
import util.Factory
import java.time.ZonedDateTime
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class OrganizationVendorServiceTest {
    companion object {
        private val log = LoggerFactory.getLogger(OrganizationVendorServiceTest::class.java)
    }

    @Autowired
    private lateinit var orgRepo: OrganizationRepository
    @Autowired
    private lateinit var dataRecipientRepo: OrganizationDataRecipientRepository
    @Autowired
    private lateinit var vendorRepository: VendorRepository
    @Autowired
    private lateinit var organizationVendorService: OrganizationVendorService

    @Test()
    fun moveOutdatedUnknownOrgDataRecipientsToController__movesExpectedDataRecipients() {
        val org = Factory.createOrganization(orgRepo)

        val googleAds = Factory.createVendor(vendorRepository, name = "Google Ads")
        val facebookAds = Factory.createVendor(vendorRepository, name = "Facebook Ads")
        val pinterest = Factory.createVendor(vendorRepository, name = "Pinterest")

        var orgGoogleAds = Factory.createOrganizationVendor(dataRecipientRepo, o = org, vendor = googleAds)
        var orgFacebookAds = Factory.createOrganizationVendor(dataRecipientRepo, o = org, vendor = facebookAds)
        var orgPinterest = Factory.createOrganizationVendor(dataRecipientRepo, o = org, vendor = pinterest)

        // A processor; this should not change
        orgGoogleAds.gdprProcessorSetting = GDPRProcessorRecommendation.Processor
        orgGoogleAds.gdprProcessorSettingSetAt = ZonedDateTime.now().minusWeeks(4)
        dataRecipientRepo.save(orgGoogleAds)

        // Unknown and set more than 3 weeks ago, this should change
        orgFacebookAds.gdprProcessorSetting = GDPRProcessorRecommendation.Unknown
        orgFacebookAds.gdprProcessorSettingSetAt = ZonedDateTime.now().minusWeeks(4)
        dataRecipientRepo.save(orgFacebookAds)

        // Unknown but set within the 3-week window, this should not change
        orgPinterest.gdprProcessorSetting = GDPRProcessorRecommendation.Unknown
        orgPinterest.gdprProcessorSettingSetAt = ZonedDateTime.now().minusWeeks(1)
        dataRecipientRepo.save(orgPinterest)

        dataRecipientRepo.flush()
        organizationVendorService.moveOutdatedUnknownOrgDataRecipientsToController()
        dataRecipientRepo.flush()

        dataRecipientRepo.refresh(orgGoogleAds)
        dataRecipientRepo.refresh(orgFacebookAds)
        dataRecipientRepo.refresh(orgPinterest)

        assertEquals(GDPRProcessorRecommendation.Processor, orgGoogleAds.activeGdprProcessorSetting())
        assertEquals(GDPRProcessorRecommendation.Controller, orgFacebookAds.activeGdprProcessorSetting())
        assertEquals(GDPRProcessorRecommendation.Unknown, orgPinterest.activeGdprProcessorSetting())
    }
}
