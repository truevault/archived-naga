package polaris.services.primary

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.models.entity.feature.Features
import util.ObjectFactory
import kotlin.test.assertEquals

class ConsentConfigurationServiceIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var subject: ConsentConfigurationService

    @DisplayName("buildConsentConfiguration")
    @ParameterizedTest(name = "{index}: with multistate={0}, gdpr={1}")
    @CsvSource(
        "false,false",
        "true,true"
    )
    fun `buildConsentConfiguration - with various settings`(
        isMultistate: Boolean,
        isGdpr: Boolean
    ) {
        val features = mutableListOf<String>()
        if (isMultistate) features.add(Features.MULTISTATE)
        if (isGdpr) features.add(Features.GDPR)

        val org = ObjectFactory.defaultOrganization(features = features.toList())

        val privacyCenter = ObjectFactory.defaultPrivacyCenter(org = org)

        val config = subject.buildConsentConfiguration(org, privacyCenter)

        // verify Consent settings
        config.consent.let {
            assertEquals(ButtonPosition.LEFT, it.buttonPosition)
            assertEquals(null, it.consentCountryFilter)
            // NOTE: return just "CA" for now until we refactor how the region filter can
            //       be used to more intelligently inject/show/hide region-specific links
            // assertEquals(if (isMultistate) listOf("CA", "VA") else listOf("CA"), it.regionFilter)
            assertEquals(if (isMultistate) listOf("CA") else listOf("CA"), it.consentRegionFilter)
            assertEquals(null, it.cookiePolicyUrl)
            assertEquals(false, it.debug)
            assertEquals(isGdpr, it.enableConsentManager)
            assertEquals(null, it.polarisUrl)
            assertEquals(privacyCenter.publicId, it.privacyCenterId)
        }

        // verify CCPA settings
        config.ccpa.let {
            assertEquals(null, it.consumerLinkElementId)
            assertEquals(null, it.consumerLinkElementClass)
            assertEquals(20000, it.consumerLinkUnhideSleepMs)
            assertEquals(null, it.countryFilter)
            assertEquals(false, it.debug)
            assertEquals(null, it.domain)
            assertEquals(null, it.googleAnalyticsTrackingId)
            assertEquals(false, it.lspaCovered)
            assertEquals(30, it.notApplicableExpirationDays)
            assertEquals(365, it.optOutExpirationDays)
            assertEquals(null, it.optOutLinkClass)
            assertEquals(privacyCenter.publicId, it.privacyCenterId)
            // NOTE: return just "CA" for now until we refactor how the region filter can
            //       be used to more intelligently inject/show/hide region-specific links
            // assertEquals(if (isMultistate) listOf("CA", "VA") else listOf("CA"), it.regionFilter)
            assertEquals(if (isMultistate) listOf("CA") else listOf("CA"), it.regionFilter)
        }
    }
}
