package polaris.services.primary

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.anyOrNull
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.controllers.MailboxNotConnectedException
import polaris.models.entity.email.QueuedEmailStatusType
import polaris.repositories.QueuedEmailRepository
import polaris.services.support.email.EmailResponse
import util.ObjectFactory
import java.util.UUID
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class QueuedEmailServiceIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var queuedEmailRepository: QueuedEmailRepository

    @Autowired
    private lateinit var subject: QueuedEmailService

    private lateinit var queuedEmailIds: List<UUID>

    @BeforeEach
    fun setup() {
        val orgs = persistAll(
            listOf(
                ObjectFactory.defaultOrganization(name = "org 1"),
                ObjectFactory.defaultOrganization(name = "org 2")
            )
        )

        val dsrs = persistAll(
            listOf(
                ObjectFactory.defaultDataRequest(orgs[0]),
                ObjectFactory.defaultDataRequest(orgs[1]),
                ObjectFactory.defaultDataRequest(orgs[0])
            )
        )

        val queuedEmails = persistAll(
            listOf(
                ObjectFactory.defaultQueuedEmail(
                    dsrs[0],
                    subject = "test 1",
                    senderEmailAddress = "org1@qa.truevault.com"
                ),
                ObjectFactory.defaultQueuedEmail(
                    dsrs[1],
                    subject = "test 2",
                    senderEmailAddress = "org2@qa.truevault.com"
                ),
                ObjectFactory.defaultQueuedEmail(
                    dsrs[2],
                    subject = "test 3",
                    senderEmailAddress = "org1@qa.truevault.com"
                )
            )
        )

        queuedEmailIds = queuedEmails.map { it.id }

        flushAndClear()
    }

    @Test
    fun `findAndProcessEmails - with all mailboxes connected`() {
        // stubs
        whenever(emailService.sendRequestGeneralEmail(anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull()))
            .doAnswer {
                val sender = it.arguments[2]
                return@doAnswer if (sender == "org2@qa.truevault.com")
                    EmailResponse(statusCode = 400, responseBody = "error")
                else
                    EmailResponse(statusCode = 200, responseBody = "ok")
            }

        // when
        subject.findAndProcessEmails()

        // then
        verify(emailService, times(3))
            .sendRequestGeneralEmail(anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull())

        val processedEmails = queuedEmailRepository.findAllById(queuedEmailIds).toList()
        assertEquals(3, processedEmails.size)

        with(processedEmails.find { it.id == queuedEmailIds[0] }) {
            assertEquals("org1@qa.truevault.com", this?.senderEmailAddress)
            assertEquals(QueuedEmailStatusType.SENT, this?.status)
            assertNotEquals(this?.lastUpdatedAt, this?.createdAt)
            assertTrue(this?.lastUpdatedAt!! > this.createdAt)
        }

        with(processedEmails.find { it.id == queuedEmailIds[1] }) {
            assertEquals("org2@qa.truevault.com", this?.senderEmailAddress)
            assertEquals(QueuedEmailStatusType.FAILURE, this?.status)
            assertTrue(this?.lastUpdatedAt!! > this.createdAt)
        }

        with(processedEmails.find { it.id == queuedEmailIds[2] }) {
            assertEquals("org1@qa.truevault.com", this?.senderEmailAddress)
            assertEquals(QueuedEmailStatusType.SENT, this?.status)
            assertTrue(this?.lastUpdatedAt!! > this.createdAt)
        }
    }

    @Test
    fun `findAndProcessEmails - with an unconnected mailbox`() {
        // stubs
        whenever(emailService.sendRequestGeneralEmail(anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull()))
            .doAnswer {
                val sender = it.arguments[2]
                if (sender == "org1@qa.truevault.com")
                    throw MailboxNotConnectedException("connect me!")
                return@doAnswer EmailResponse(statusCode = 200, responseBody = "ok")
            }

        // when
        subject.findAndProcessEmails()

        // then
        verify(emailService, times(2))
            .sendRequestGeneralEmail(anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull())

        val processedEmails = queuedEmailRepository.findAllById(queuedEmailIds).toList()
        assertEquals(3, processedEmails.size)

        with(processedEmails.find { it.id == queuedEmailIds[0] }) {
            assertEquals("org1@qa.truevault.com", this?.senderEmailAddress)
            assertEquals(QueuedEmailStatusType.FAILURE, this?.status)
            assertTrue(this?.lastUpdatedAt!! > this.createdAt)
        }

        with(processedEmails.find { it.id == queuedEmailIds[1] }) {
            assertEquals("org2@qa.truevault.com", this?.senderEmailAddress)
            assertEquals(QueuedEmailStatusType.SENT, this?.status)
            assertTrue(this?.lastUpdatedAt!! > this.createdAt)
        }

        with(processedEmails.find { it.id == queuedEmailIds[2] }) {
            assertEquals("org1@qa.truevault.com", this?.senderEmailAddress)
            assertEquals(QueuedEmailStatusType.PENDING, this?.status)
            assertEquals(this?.lastUpdatedAt, this?.createdAt)
        }
    }

    @Test
    fun `findAndProcessEmails - with unhandled runtime exception`() {
        // stubs
        whenever(emailService.sendRequestGeneralEmail(anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull()))
            .thenReturn(EmailResponse(statusCode = 200, responseBody = "ok"))
            .thenThrow(RuntimeException("UNHANDLED ERROR"))
            .thenReturn(EmailResponse(statusCode = 200, responseBody = "ok"))

        // when
        var exceptionThrown: Exception? = null
        try {
            subject.findAndProcessEmails()
        } catch (ex: Exception) {
            exceptionThrown = ex
            clear() // simulate transaction rolling back
        }

        // then
        assertNotNull(exceptionThrown)
        assertTrue(exceptionThrown !is MailboxNotConnectedException)

        verify(emailService, times(2))
            .sendRequestGeneralEmail(anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull(), anyOrNull())

        val processedEmails = queuedEmailRepository.findAllById(queuedEmailIds).toList()
        assertEquals(3, processedEmails.size)

        with(processedEmails.find { it.id == queuedEmailIds[0] }) {
            assertEquals("org1@qa.truevault.com", this?.senderEmailAddress)
            assertEquals(QueuedEmailStatusType.SENT, this?.status)
            assertTrue(this?.lastUpdatedAt!! > this.createdAt)
        }

        with(processedEmails.find { it.id == queuedEmailIds[1] }) {
            assertEquals("org2@qa.truevault.com", this?.senderEmailAddress)
            assertEquals(QueuedEmailStatusType.PENDING, this?.status)
            assertEquals(this?.lastUpdatedAt, this?.createdAt)
        }

        with(processedEmails.find { it.id == queuedEmailIds[2] }) {
            assertEquals("org1@qa.truevault.com", this?.senderEmailAddress)
            assertEquals(QueuedEmailStatusType.PENDING, this?.status)
            assertEquals(this?.lastUpdatedAt, this?.createdAt)
        }
    }
}
