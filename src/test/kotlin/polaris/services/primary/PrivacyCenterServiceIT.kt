package polaris.services.primary

import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import polaris.BaseIntegrationTest
import polaris.models.dto.privacycenter.UpdatePrivacyCenter
import polaris.services.support.PrivacyCenterBuilderService
import util.ObjectFactory
import kotlin.test.assertEquals

class PrivacyCenterServiceIT : BaseIntegrationTest() {

    // MOCKS
    @MockBean
    private lateinit var pcBuilderService: PrivacyCenterBuilderService

    @MockBean
    private lateinit var taskUpdateService: TaskUpdateService

    // SUBJECT
    @Autowired
    private lateinit var subject: PrivacyCenterService

    @Test
    fun `updatePrivacyCenterRequestEmail - when email is blank`() {
        val org = persist(ObjectFactory.defaultOrganization(name = "Organization 1"))
        val pc = persist(ObjectFactory.defaultPrivacyCenter(org, requestEmail = "admin@qa.truevault.com"))

        subject.updatePrivacyCenterRequestEmail(pc, "")

        assertEquals("admin@qa.truevault.com", pc.requestEmail)
        verifyNoInteractions(pcBuilderService)
    }

    @Test
    fun `updatePrivacyCenterRequestEmail - when email is valid`() {
        val org = persist(ObjectFactory.defaultOrganization(name = "Organization 1"))
        val pc = persist(ObjectFactory.defaultPrivacyCenter(org, requestEmail = "admin@qa.truevault.com"))

        subject.updatePrivacyCenterRequestEmail(pc, "jl@example.com")

        assertEquals("jl@example.com", pc.requestEmail)
        verify(pcBuilderService).insertPrivacyCenterBuildRequest(pc, "email updated")
    }

    @Test
    fun `update - rebuilds PrivacyCenter and publishes UpdateTasksEvent`() {
        val org = persist(ObjectFactory.defaultOrganization(name = "Organization 1"))
        val pc = persist(ObjectFactory.defaultPrivacyCenter(org, requestEmail = "admin@qa.truevault.com"))
        val user = persist(ObjectFactory.defaultUser())

        persist(ObjectFactory.randomOrganizationUser(user = user, organization = org))

        val updateDto = UpdatePrivacyCenter(
            customFieldEnabled = true,
            defaultSubdomain = "www",
            name = "test",
            dpoOfficerName = "John Lennon",
            dpoOfficerEmail = "jl@example.com",
            ccpaPrivacyNoticeIntroText = "ccpaPrivacyNoticeIntroText",
            gdprPrivacyNoticeIntroText = "gdprPrivacyNoticeIntroText",
            vcdpaPrivacyNoticeIntroText = "vcdpaPrivacyNoticeIntroText",
            cpaPrivacyNoticeIntroText = "cpaPrivacyNoticeIntroText",
            ctdpaPrivacyNoticeIntroText = "ctdpaPrivacyNoticeIntroText",
            optOutNoticeIntroText = "optOutNoticeIntroText",
            retentionIntroText = "retentionIntroText",
            cookiePolicyIntroText = "cookiePolicyIntroText"
        )
        subject.update(pc.publicId, org, updateDto, user)

        verify(pcBuilderService).insertPrivacyCenterBuildRequest(pc, "privacy center updated")
        verify(taskUpdateService).updateTasks(org.publicId, create = false, close = true)
    }
}
