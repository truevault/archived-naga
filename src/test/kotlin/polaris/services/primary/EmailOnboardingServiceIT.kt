package polaris.services.primary

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.models.dto.request.CreateDataRequest
import polaris.models.entity.organization.Organization
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.user.User
import util.ObjectFactory

class EmailOnboardingServiceIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var dataSubjectRequestService: DataSubjectRequestService

    @Autowired
    private lateinit var subject: OnboardingEmailService

    private lateinit var organization: Organization
    private lateinit var user: User

    @BeforeEach
    fun setupOrganization() {
        organization = persist(ObjectFactory.defaultOrganization(name = "Organization 1"))
        user = persist(ObjectFactory.defaultUser(email = "user@example.com"))
        persist(ObjectFactory.randomOrganizationUser(user, organization))

        val vendor = persist(ObjectFactory.defaultVendor(name = "Facebook"))
        persist(ObjectFactory.defaultDataRecipient(organization, vendor))
        persist(ObjectFactory.defaultCollectionGroup("Customer", organization))
    }

    @DisplayName("sendOnboardingEmailOnFirst")
    @ParameterizedTest(name = "{index}: sendOnboardingEmailOnFirst - with walkthroughEmailSent={0} and trainingComplete={1}")
    @CsvSource(
        "false,false,0", // does not send before training complete
        "false,true,1", // sends once when training is complete
        "true,false,0", // does not send before training complete
        "true,true,0" // does not send more than once
    )
    fun `sendOnboardingEmailOnFirst - with various args`(
        walkthroughEmailSent: Boolean,
        trainingComplete: Boolean,
        expectedEmailsSent: Int
    ) {
        organization.walkthroughEmailSent = walkthroughEmailSent
        organization.trainingComplete = trainingComplete
        persist(organization)

        val request = dataSubjectRequestService.create(
            user, organization.publicId,
            CreateDataRequest(
                subjectFirstName = "John",
                subjectLastName = "Doe",
                subjectEmailAddress = "jdoe@example.com",
                dataRequestType = DataRequestType.CCPA_RIGHT_TO_DELETE,
                requestDate = "2020-03-13T00:00:00.000Z"
            )
        )

        subject.sendOnboardingEmailOnFirst(request)

        verify(emailService, times(expectedEmailsSent)).sendOnboardingEmail(request)
    }
}
