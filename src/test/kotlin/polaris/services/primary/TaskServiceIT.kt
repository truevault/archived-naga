package polaris.services.primary

import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import polaris.BaseIntegrationTest
import polaris.models.dto.springevents.UpdateTasksSpringEvent
import polaris.models.entity.organization.TaskKey
import polaris.services.support.DpoService
import util.ObjectFactory
import java.time.ZonedDateTime
import kotlin.test.assertEquals

class TaskServiceIT : BaseIntegrationTest() {

    @MockBean
    private lateinit var dpoService: DpoService

    @Autowired
    private lateinit var subject: TaskService

    @Test
    fun `forOrg - returns expected set for active`() {
        val org = persist(ObjectFactory.defaultOrganization())
        val taskA = persist(ObjectFactory.task(org, TaskKey.COMPLETE_DATA_RECIPIENTS))
        val taskB = persist(ObjectFactory.task(org, TaskKey.ADD_DPO))
        val taskC = persist(ObjectFactory.task(org, TaskKey.COMPLETE_FINANCIAL_INCENTIVES, ZonedDateTime.now()))

        flushAndClear()

        val tasks = subject.forOrg(org, TaskFilter.active)
        val taskIds = tasks.map { it.id }

        // Contains all incomplete tasks
        assert(taskIds.containsAll(listOf(taskA.id, taskB.id)))

        // Does not contain completed task
        assert(!taskIds.contains(taskC.id))
    }

    @Test
    fun `forOrg - returns expected set for complete`() {
        val org = persist(ObjectFactory.defaultOrganization())
        val taskA = persist(ObjectFactory.task(org, TaskKey.COMPLETE_DATA_RECIPIENTS))
        val taskB = persist(ObjectFactory.task(org, TaskKey.ADD_DPO))
        val taskC = persist(ObjectFactory.task(org, TaskKey.COMPLETE_FINANCIAL_INCENTIVES, ZonedDateTime.now()))

        flushAndClear()

        val tasks = subject.forOrg(org, TaskFilter.complete)
        val taskIds = tasks.map { it.id }

        // Does not contain incomplete tasks
        assert(!taskIds.contains(taskA.id))
        assert(!taskIds.contains(taskB.id))

        // Contains completed task
        assert(taskIds.contains(taskC.id))
    }

    @Test
    fun `forOrg - suppresses duplicate tasks`() {
        val org = persist(ObjectFactory.defaultOrganization())
        persist(ObjectFactory.task(org, TaskKey.COMPLETE_DATA_RECIPIENTS))
        persist(ObjectFactory.task(org, TaskKey.COMPLETE_DATA_RECIPIENTS))

        flushAndClear()

        val tasks = subject.forOrg(org, TaskFilter.active)

        assert(tasks.size == 1)
    }

    @Test
    fun `openTaskCount - returns expected count`() {
        val org = persist(ObjectFactory.defaultOrganization())
        persist(ObjectFactory.task(org, TaskKey.COMPLETE_DATA_RECIPIENTS))
        persist(ObjectFactory.task(org, TaskKey.ADD_DPO))
        persist(ObjectFactory.task(org, TaskKey.COMPLETE_FINANCIAL_INCENTIVES, ZonedDateTime.now()))

        flushAndClear()

        val count = subject.openTaskCount(org)

        assertEquals(2, count)
    }

    @Test
    fun `openTaskCount - suppresses duplicates in count`() {
        val org = persist(ObjectFactory.defaultOrganization())
        persist(ObjectFactory.task(org, TaskKey.COMPLETE_DATA_RECIPIENTS))
        persist(ObjectFactory.task(org, TaskKey.COMPLETE_DATA_RECIPIENTS))

        flushAndClear()

        val count = subject.openTaskCount(org)

        assertEquals(1, count)
    }

    @ParameterizedTest(name = "handleUpdateTask - with auto-close flag {0}")
    @CsvSource(
        "false,1,0",
        "true,0,1"
    )
    fun `handleUpdateTask - with auto-close flag`(
        autoCloseFlag: Boolean,
        expectedOpenCount: Int,
        expectedCompletedCount: Int
    ) {
        val org = persist(ObjectFactory.defaultOrganization(name = "Organization 1"))
        val user = persist(ObjectFactory.defaultUser())
        persist(ObjectFactory.randomOrganizationUser(user = user, organization = org))

        `when`(dpoService.isDpoRequired(org)).thenReturn(true)

        val dpoTask = subject.createDpoTask(org)
        val initialOpenTasks = subject.forOrg(org, TaskFilter.active)
        assertEquals(1, initialOpenTasks.size)
        assert(initialOpenTasks.contains(dpoTask))

        flushAndClear()

        val updateTasksEvent =
            UpdateTasksSpringEvent(org.publicId, autoCreate = false, autoClose = autoCloseFlag, actorId = user.id)
        subject.handleUpdateTasks(updateTasksEvent)

        flushAndClear()

        val finalOpenTasks = subject.forOrg(org, TaskFilter.active)
        val finalCompletedTasks = subject.forOrg(org, TaskFilter.complete)

        assertEquals(expectedOpenCount, finalOpenTasks.size)
        assertEquals(expectedCompletedCount, finalCompletedTasks.size)
        assertEquals(!autoCloseFlag, finalOpenTasks.contains(dpoTask) && !finalCompletedTasks.contains(dpoTask))
        assertEquals(autoCloseFlag, !finalOpenTasks.contains(dpoTask) && finalCompletedTasks.contains(dpoTask))
    }
}
