package polaris.services

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.controllers.InvalidNote
import polaris.models.dto.request.CreateDataRequest
import polaris.models.dto.request.CreateNoteRequest
import polaris.models.dto.request.TransitionDataRequest
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.regulation.Regulation
import polaris.models.entity.request.DataRequestEventType
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestSubstate
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.user.User
import polaris.models.entity.vendor.Vendor
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.DataRequestEventRepository
import polaris.repositories.DataRequestRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationUserRepository
import polaris.repositories.RegulationRepository
import polaris.repositories.UserRepository
import polaris.repositories.VendorRepository
import polaris.services.primary.DataRequestFilter
import polaris.services.primary.DataRequestSearchParam
import polaris.services.primary.DataRequestSort
import polaris.services.primary.DataRequestSortId
import polaris.services.primary.DataSubjectRequestService
import polaris.services.primary.RequestStateFilter
import polaris.services.primary.RequestTypeFilter
import polaris.services.support.EmailService
import util.Factory
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class DataSubjectRequestVendorTest {

    @Autowired
    private lateinit var regulationRepository: RegulationRepository

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var organizationUserRepository: OrganizationUserRepository

    @Autowired
    private lateinit var collectionGroupRepository: CollectionGroupRepository

    @Autowired
    private lateinit var dataRequestEventRepository: DataRequestEventRepository

    @Autowired
    private lateinit var dataRequestRepository: DataRequestRepository

    @Autowired
    private lateinit var vendorRepository: VendorRepository

    @Autowired
    private lateinit var organizationDataRecipientRepository: OrganizationDataRecipientRepository

    @Autowired
    private lateinit var dataSubjectRequestService: DataSubjectRequestService

    @MockBean
    private lateinit var emailService: EmailService

    private lateinit var regulation: Regulation
    private lateinit var organization: Organization

    private lateinit var user: User
    private lateinit var dataRequestType: DataRequestType
    private lateinit var collectionGroup: CollectionGroup
    private lateinit var vendor: Vendor
    private lateinit var organizationDataRecipient: OrganizationDataRecipient

    @Before
    fun createOrganization() {
        regulation = regulationRepository.findBySlug("CCPA")!!
        organization = Factory.createOrganization(organizationRepository, name = "Organization 1")
        vendor = Factory.createVendor(vendorRepository, name = "Facebook")
        user = Factory.createUser(userRepository = userRepository, organizationUserRepository = organizationUserRepository, organization = organization, email = "user@example.com")

        dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW
        collectionGroup = Factory.createCollectionGroup("Customer", organization, collectionGroupRepository)
        organizationDataRecipient = Factory.createOrganizationVendor(organizationDataRecipientRepository = organizationDataRecipientRepository, o = organization, vendor = vendor)
    }

    @Test(expected = OrganizationNotFound::class)
    fun findAllForOrganization__throwsForInvalidOrganization() {
        dataSubjectRequestService.findAllForOrganization("foobar", filter = null)
    }

    @Test()
    fun findAllForOrganization__allNoResults() {
        val r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null)

        assertNotNull(r)
        assertEquals(0, r.size)
    }

    @Test()
    fun findAllForOrganization__findsByType() {
        val ccpaOptOut = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT)
        val ccpaRightToDelete = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_RIGHT_TO_DELETE)
        val ccpaRightToKnow = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_RIGHT_TO_KNOW)
        val gdprAccess = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_ACCESS)
        val gdprErasure = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_ERASURE)
        val gdprObjection = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_OBJECTION)
        val gdprRectification = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_RECTIFICATION)
        val gdprWithdrawal = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_WITHDRAWAL)

        var r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all)

        assertNotNull(r)
        assertEquals(8, r.size)

        assert(r.contains(ccpaOptOut))
        assert(r.contains(ccpaRightToDelete))
        assert(r.contains(ccpaRightToKnow))
        assert(r.contains(gdprAccess))
        assert(r.contains(gdprErasure))
        assert(r.contains(gdprWithdrawal))
        assert(r.contains(gdprRectification))

        // Test Delete
        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.delete)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(ccpaRightToDelete))

        // Test Opt Out
        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.optout)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(ccpaOptOut))

        // Test Know
        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.know)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(ccpaRightToKnow))

        // Test Access
        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.access)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(gdprAccess))

        // Test Erasure
        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.erasure)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(gdprErasure))

        // Test objection
        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.objection)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(gdprObjection))

        // Test rectification
        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.rectification)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(gdprRectification))

        // Test withdrawal
        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.withdrawal)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(gdprWithdrawal))
    }

    @Test()
    fun findAllForOrganization__findsByState() {
        val closed = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED)
        val pending = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.PENDING_EMAIL_VERIFICATION)
        val consumer_group_selection = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CONSUMER_GROUP_SELECTION)
        val verify_consumer = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.VERIFY_CONSUMER)
        val delete_info = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.DELETE_INFO)
        val contact_vendors = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CONTACT_VENDORS)
        val notify_consumer = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.NOTIFY_CONSUMER)
        val remove_consumer = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.REMOVE_CONSUMER)
        val retrieve_data = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.RETRIEVE_DATA)
        val send_data = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.SEND_DATA)
        val processing = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.PROCESSING)

        var r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = DataRequestFilter.active, type = RequestTypeFilter.all)

        assertNotNull(r)
        assertEquals(9, r.size)
        assert(r.contains(consumer_group_selection))
        assert(r.contains(verify_consumer))
        assert(r.contains(delete_info))
        assert(r.contains(contact_vendors))
        assert(r.contains(notify_consumer))
        assert(r.contains(remove_consumer))
        assert(r.contains(retrieve_data))
        assert(r.contains(send_data))
        assert(r.contains(processing))

        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = DataRequestFilter.pending, type = RequestTypeFilter.all)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(pending))

        r = dataSubjectRequestService.findAllForOrganization(organization.publicId, filter = DataRequestFilter.complete, type = RequestTypeFilter.all)

        assertNotNull(r)
        assertEquals(1, r.size)
        assert(r.contains(closed))
    }

    @Test()
    fun paginatedFindForOrganization__findsByType() {
        val ccpaOptOut = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT)
        val ccpaRightToDelete = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_RIGHT_TO_DELETE)
        val ccpaRightToKnow = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_RIGHT_TO_KNOW)
        val gdprAccess = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_ACCESS)
        val gdprErasure = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_ERASURE)
        val gdprObjection = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_OBJECTION)
        val gdprRectification = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_RECTIFICATION)
        val gdprWithdrawal = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.GDPR_WITHDRAWAL)

        var r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all)

        assertNotNull(r)
        assertEquals(8, r.totalElements)

        assert(r.contains(ccpaOptOut))
        assert(r.contains(ccpaRightToDelete))
        assert(r.contains(ccpaRightToKnow))
        assert(r.contains(gdprAccess))
        assert(r.contains(gdprErasure))
        assert(r.contains(gdprObjection))
        assert(r.contains(gdprRectification))
        assert(r.contains(gdprWithdrawal))

        // Test Delete
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.delete)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(ccpaRightToDelete))

        // Test Opt Out
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.optout)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(ccpaOptOut))

        // Test Know
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.know)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(ccpaRightToKnow))

        // Test Access
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.access)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(gdprAccess))

        // Test Erasure
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.erasure)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(gdprErasure))

        // Test objection
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.objection)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(gdprObjection))

        // Test rectification
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.rectification)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(gdprRectification))

        // Test withdrawal
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.withdrawal)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(gdprWithdrawal))
    }

    @Test()
    fun paginatedFindForOrganization__findsByState() {
        val closed = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED)
        val pending = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.PENDING_EMAIL_VERIFICATION)
        val consumer_group_selection = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CONSUMER_GROUP_SELECTION)
        val verify_consumer = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.VERIFY_CONSUMER)
        val delete_info = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.DELETE_INFO)
        val contact_vendors = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CONTACT_VENDORS)
        val notify_consumer = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.NOTIFY_CONSUMER)
        val remove_consumer = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.REMOVE_CONSUMER)
        val retrieve_data = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.RETRIEVE_DATA)
        val send_data = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.SEND_DATA)
        val processing = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.PROCESSING)

        var r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = DataRequestFilter.active, type = RequestTypeFilter.all)

        assertNotNull(r)
        assertEquals(9, r.totalElements)
        assert(r.contains(consumer_group_selection))
        assert(r.contains(verify_consumer))
        assert(r.contains(delete_info))
        assert(r.contains(contact_vendors))
        assert(r.contains(notify_consumer))
        assert(r.contains(remove_consumer))
        assert(r.contains(retrieve_data))
        assert(r.contains(send_data))
        assert(r.contains(processing))

        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = DataRequestFilter.pending, type = RequestTypeFilter.all)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(pending))

        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = DataRequestFilter.complete, type = RequestTypeFilter.all)

        assertNotNull(r)
        assertEquals(1, r.totalElements)
        assert(r.contains(closed))
    }

    @Test()
    fun paginatedFindForOrganization__paginates() {
        Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED)
        Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.PENDING_EMAIL_VERIFICATION)
        Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CONSUMER_GROUP_SELECTION)

        val r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, page = 1, per = 1)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assertEquals(3, r.totalPages)
        assertEquals(3, r.totalElements)
    }

    @Test()
    fun paginatedFindForOrganization__searchesByEmail() {
        val test1 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED, subjectEmailAddress = "test1@example.com")
        val test2 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.PENDING_EMAIL_VERIFICATION, subjectEmailAddress = "test2@example.com")
        val test3 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CONSUMER_GROUP_SELECTION, subjectEmailAddress = "test3@example.com")

        var searchList = listOf(DataRequestSearchParam.SubjectEmail("test1@example.com"))
        var r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(test1))

        searchList = listOf(DataRequestSearchParam.SubjectEmail("test"))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertEquals(3, r.numberOfElements)
        assert(r.contains(test1))
        assert(r.contains(test2))
        assert(r.contains(test3))

        searchList = listOf(DataRequestSearchParam.SubjectEmail("foobar"))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertEquals(0, r.numberOfElements)
    }

    @Test()
    fun paginatedFindForOrganization__searchesByDueDate() {
        val dr1 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED, dueDate = ZonedDateTime.now())
        val dr2 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.PENDING_EMAIL_VERIFICATION, dueDate = ZonedDateTime.now().plusDays(4))
        val dr3 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CONSUMER_GROUP_SELECTION, dueDate = ZonedDateTime.now().plusDays(25))

        var searchList = listOf(DataRequestSearchParam.DueDate(1))
        var r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(dr1))

        searchList = listOf(DataRequestSearchParam.DueDate(7))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(2, r.numberOfElements)
        assert(r.contains(dr1))
        assert(r.contains(dr2))

        searchList = listOf(DataRequestSearchParam.DueDate(30))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertEquals(3, r.numberOfElements)
        assert(r.contains(dr1))
        assert(r.contains(dr2))
        assert(r.contains(dr3))
    }

    @Test()
    fun paginatedFindForOrganization__searchesByType() {
        val dr1 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT)
        val dr2 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_RIGHT_TO_KNOW)
        val dr3 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_RIGHT_TO_DELETE)

        var searchList = listOf(DataRequestSearchParam.RequestType(DataRequestType.CCPA_OPT_OUT))
        var r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(dr1))

        searchList = listOf(DataRequestSearchParam.RequestType(DataRequestType.CCPA_RIGHT_TO_KNOW))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(dr2))

        searchList = listOf(DataRequestSearchParam.RequestType(DataRequestType.CCPA_RIGHT_TO_DELETE))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(dr3))
    }

    @Test()
    fun paginatedFindForOrganization__searchesByState() {
        val closed = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED)
        val pending = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.PENDING_EMAIL_VERIFICATION)
        val verify_consumer = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.VERIFY_CONSUMER)
        val consumer_group_selection = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CONSUMER_GROUP_SELECTION)
        val delete_info = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.DELETE_INFO)
        val contact_vendors = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CONTACT_VENDORS)
        val notify_consumer = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.NOTIFY_CONSUMER)
        val remove_consumer = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.REMOVE_CONSUMER)
        val retrieve_data = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.RETRIEVE_DATA)
        val send_data = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.SEND_DATA)
        val processing = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.PROCESSING)

        var searchList = listOf(DataRequestSearchParam.RequestState(RequestStateFilter.new.toString()))
        var r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(verify_consumer))

        searchList = listOf(DataRequestSearchParam.RequestState(RequestStateFilter.pending.toString()))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(pending))

        searchList = listOf(DataRequestSearchParam.RequestState(RequestStateFilter.closed.toString()))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(closed))

        searchList = listOf(DataRequestSearchParam.RequestState(RequestStateFilter.in_progress.toString()))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(8, r.numberOfElements)
        assert(r.contains(consumer_group_selection))
        assert(r.contains(delete_info))
        assert(r.contains(contact_vendors))
        assert(r.contains(notify_consumer))
        assert(r.contains(remove_consumer))
        assert(r.contains(retrieve_data))
        assert(r.contains(send_data))
        assert(r.contains(processing))
    }

    @Test()
    fun paginatedFindForOrganization__searchesByRequestDate() {
        val now = ZonedDateTime.now()

        val request = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED, requestDate = now)
        val oldRequest = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED, requestDate = now.minusDays(100))

        //
        // Test Between
        //
        var searchList = listOf(DataRequestSearchParam.RequestDateRange(requestStartDate = now.minusDays(10), requestEndDate = now.plusDays(1)))
        var r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(request))

        searchList = listOf(DataRequestSearchParam.RequestDateRange(requestStartDate = now.minusDays(1000), requestEndDate = now.plusDays(1)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(2, r.numberOfElements)
        assert(r.contains(request))
        assert(r.contains(oldRequest))

        searchList = listOf(DataRequestSearchParam.RequestDateRange(requestStartDate = now.minusDays(1000), requestEndDate = now.minusDays(100)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(oldRequest))

        //
        // Test After
        //
        searchList = listOf(DataRequestSearchParam.RequestDateRange(requestStartDate = now.minusDays(1000)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(2, r.numberOfElements)
        assert(r.contains(request))
        assert(r.contains(oldRequest))

        searchList = listOf(DataRequestSearchParam.RequestDateRange(requestStartDate = now.minusDays(10)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(request))

        //
        // Test Before
        //
        searchList = listOf(DataRequestSearchParam.RequestDateRange(requestEndDate = now.plusDays(1)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(2, r.numberOfElements)
        assert(r.contains(request))
        assert(r.contains(oldRequest))

        searchList = listOf(DataRequestSearchParam.RequestDateRange(requestEndDate = now.minusDays(10)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(oldRequest))
    }

    @Test()
    fun paginatedFindForOrganization__searchesByCreatedAt() {
        val now = ZonedDateTime.now()

        val request = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED, createdAt = now)
        val oldRequest = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, state = DataRequestState.CLOSED, createdAt = now.minusDays(100))

        //
        // Test Between
        //
        var searchList = listOf(DataRequestSearchParam.CreatedRange(createdStartDate = now.minusDays(10), createdEndDate = now.plusDays(1)))
        var r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(request))

        searchList = listOf(DataRequestSearchParam.CreatedRange(createdStartDate = now.minusDays(1000), createdEndDate = now.plusDays(1)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(2, r.numberOfElements)
        assert(r.contains(request))
        assert(r.contains(oldRequest))

        searchList = listOf(DataRequestSearchParam.CreatedRange(createdStartDate = now.minusDays(1000), createdEndDate = now.minusDays(100)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(oldRequest))

        //
        // Test After
        //
        searchList = listOf(DataRequestSearchParam.CreatedRange(createdStartDate = now.minusDays(1000)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(2, r.numberOfElements)
        assert(r.contains(request))
        assert(r.contains(oldRequest))

        searchList = listOf(DataRequestSearchParam.CreatedRange(createdStartDate = now.minusDays(10)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(request))

        //
        // Test Before
        //
        searchList = listOf(DataRequestSearchParam.CreatedRange(createdEndDate = now.plusDays(1)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(2, r.numberOfElements)
        assert(r.contains(request))
        assert(r.contains(oldRequest))

        searchList = listOf(DataRequestSearchParam.CreatedRange(createdEndDate = now.minusDays(10)))
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, search = searchList)

        assertNotNull(r)
        assertEquals(1, r.numberOfElements)
        assert(r.contains(oldRequest))
    }

    @Test()
    fun paginatedFindForOrganization__sorts() {
        val dr1 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_OPT_OUT, subjectEmailAddress = "a@example.com", dueDate = ZonedDateTime.now().plusDays(10), requestDate = ZonedDateTime.now().minusDays(10), closedAt = ZonedDateTime.now().minusDays(1))
        val dr2 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_RIGHT_TO_KNOW, subjectEmailAddress = "b@example.com", dueDate = ZonedDateTime.now().plusDays(5), requestDate = ZonedDateTime.now().minusDays(5), closedAt = ZonedDateTime.now())
        val dr3 = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, o = organization, u = user, drt = DataRequestType.CCPA_RIGHT_TO_DELETE, subjectEmailAddress = "z@example.com", dueDate = ZonedDateTime.now().plusDays(1), requestDate = ZonedDateTime.now().minusDays(3), closedAt = ZonedDateTime.now().minusDays(2))

        // Sort by Type
        var sort = DataRequestSort(id = DataRequestSortId.type, desc = true)
        var r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr2, r.content.get(0))
        assertEquals(dr3, r.content.get(1))
        assertEquals(dr1, r.content.get(2))

        sort = DataRequestSort(id = DataRequestSortId.type, desc = false)
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr2, r.content.get(2))
        assertEquals(dr3, r.content.get(1))
        assertEquals(dr1, r.content.get(0))

        // Sort by Email
        sort = DataRequestSort(id = DataRequestSortId.subjectEmailAddress, desc = false)
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr1, r.content.get(0))
        assertEquals(dr2, r.content.get(1))
        assertEquals(dr3, r.content.get(2))

        sort = DataRequestSort(id = DataRequestSortId.subjectEmailAddress, desc = true)
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr1, r.content.get(2))
        assertEquals(dr2, r.content.get(1))
        assertEquals(dr3, r.content.get(0))

        // Sort by Date
        sort = DataRequestSort(id = DataRequestSortId.dueIn, desc = false)
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr1, r.content.get(2))
        assertEquals(dr2, r.content.get(1))
        assertEquals(dr3, r.content.get(0))

        sort = DataRequestSort(id = DataRequestSortId.dueIn, desc = true)
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr1, r.content.get(0))
        assertEquals(dr2, r.content.get(1))
        assertEquals(dr3, r.content.get(2))

        // Sort by Closed Date
        sort = DataRequestSort(id = DataRequestSortId.closedAt, desc = false)
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr1, r.content.get(1))
        assertEquals(dr2, r.content.get(2))
        assertEquals(dr3, r.content.get(0))

        sort = DataRequestSort(id = DataRequestSortId.closedAt, desc = true)
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr1, r.content.get(1))
        assertEquals(dr2, r.content.get(0))
        assertEquals(dr3, r.content.get(2))

        // Sort by Submitted Date
        sort = DataRequestSort(id = DataRequestSortId.submitted, desc = false)
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr1, r.content.get(0))
        assertEquals(dr2, r.content.get(1))
        assertEquals(dr3, r.content.get(2))

        sort = DataRequestSort(id = DataRequestSortId.submitted, desc = true)
        r = dataSubjectRequestService.paginatedFindForOrganization(organization.publicId, filter = null, type = RequestTypeFilter.all, sort = sort)

        assertNotNull(r)
        assertEquals(3, r.numberOfElements)
        assertEquals(dr1, r.content.get(2))
        assertEquals(dr2, r.content.get(1))
        assertEquals(dr3, r.content.get(0))
    }

    @Test()
    fun create__sendsEmailVerification() {
        val r = dataSubjectRequestService.create(
            user, organization.publicId,
            CreateDataRequest(
                subjectFirstName = "John",
                subjectLastName = "Doe",
                subjectEmailAddress = "jdoe@example.com",
                dataRequestType = dataRequestType,
                requestDate = "2020-03-13T00:00:00.000Z"
            )
        )

        verify(emailService, times(1)).sendEmailAddressVerification(r)
    }

    @Test()
    fun create__doesNotSendEmailVerificationWithNoEmailAddress() {
        val r = dataSubjectRequestService.create(
            user, organization.publicId,
            CreateDataRequest(
                subjectFirstName = "John",
                subjectLastName = "Doe",
                subjectEmailAddress = null,
                dataRequestType = dataRequestType,
                requestDate = "2020-03-13T00:00:00.000Z"
            )
        )

        verify(emailService, never()).sendEmailAddressVerification(r)
    }

    @Test()
    fun create__setsADueDate45DaysAfterRequestDate() {
        val r = dataSubjectRequestService.create(
            user, organization.publicId,
            CreateDataRequest(
                subjectFirstName = "John",
                subjectLastName = "Doe",
                dataRequestType = dataRequestType,
                requestDate = "2020-03-13T00:00:00.000Z"
            )
        )

        val localLRequestDate = ZonedDateTime.of(LocalDateTime.parse("2020-03-13T00:00:00"), ZoneId.systemDefault())
        assertEquals(localLRequestDate, r.requestDate)
        assertEquals(
            localLRequestDate.plusDays(45).withHour(0).withMinute(0).withSecond(0),
            r.requestDueDate
        )
    }

    @Test()
    fun create__addsACreatedEvent() {
        val r = dataSubjectRequestService.create(
            user, organization.publicId,
            CreateDataRequest(
                subjectFirstName = "John",
                subjectLastName = "Doe",
                dataRequestType = dataRequestType,
                requestDate = "2020-03-13T00:00:00.000Z"
            )
        )

        assertEquals(1, r.dataSubjectRequestEvents.size)
        assertEquals(user, r.dataSubjectRequestEvents[0].actor)
        assertEquals(DataRequestEventType.CREATED, r.dataSubjectRequestEvents[0].eventType)
    }

    @Test()
    fun create__addsACreatedEventWithNote() {
        val r = dataSubjectRequestService.create(
            user, organization.publicId,
            CreateDataRequest(
                subjectFirstName = "John",
                subjectLastName = "Doe",
                dataRequestType = dataRequestType,
                requestDate = "2020-03-13T00:00:00.000Z",
                notes = "Hello I am a note"
            )
        )

        assertEquals(1, r.dataSubjectRequestEvents.size)
        assertEquals(user, r.dataSubjectRequestEvents[0].actor)
        assertEquals(DataRequestEventType.CREATED, r.dataSubjectRequestEvents[0].eventType)
        assertEquals("Hello I am a note", r.dataSubjectRequestEvents[0].message)
    }

    @Test(expected = InvalidNote::class)
    fun createNote__rejectsIfNoteIsEmpty() {
        val request = create(DataRequestState.PROCESSING)
        dataSubjectRequestService.createNote(user, organization.publicId, request.publicId, CreateNoteRequest("  ", null))
    }

    @Test()
    fun createNote__addsANote() {
        val r = create(DataRequestState.PROCESSING)
        dataSubjectRequestService.createNote(user, organization.publicId, r.publicId, CreateNoteRequest("Hello world", null))

        assertEquals(2, r.dataSubjectRequestEvents.size)
        assertEquals(user, r.dataSubjectRequestEvents[1].actor)
        assertEquals(DataRequestEventType.NOTE_ADDED, r.dataSubjectRequestEvents[1].eventType)
        assertEquals("Hello world", r.dataSubjectRequestEvents[1].message)
    }

    @Test()
    fun transition__fromProcessingToClosed() {
        // todo: this should pass iff all "processing steps" are done
        val request = create(DataRequestState.PROCESSING)
        val updated = transition(request, DataRequestState.CLOSED, substate = DataRequestSubstate.COMPLETED)

        assertEquals(DataRequestState.CLOSED, updated.state)
    }

    private fun create(state: DataRequestState, actor: User = user, requestType: DataRequestType = dataRequestType) =
        Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, organization, actor, drt = requestType, state = state)

    private fun transition(dataRequest: DataSubjectRequest, to: DataRequestState, notes: String? = null, substate: DataRequestSubstate? = null): DataSubjectRequest {
        val transitionRequest = TransitionDataRequest(state = to, notes = notes, substate = substate)
        return dataSubjectRequestService.transition(user, dataRequest.organization!!.publicId, dataRequest.publicId, transitionRequest, mutableListOf())
    }
}
