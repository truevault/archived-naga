package polaris.services

import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.models.entity.organization.DataRecipientDisclosedPIC
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.services.support.DataMapService
import util.ObjectFactory
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class DataMapServiceIT : BaseIntegrationTest() {
    companion object {
        private val log = LoggerFactory.getLogger(DataMapServiceIT::class.java)
    }

    @Autowired
    private lateinit var subject: DataMapService

    @Test
    fun `getUnmappedDataRecipients - returns expected set`() {
        // given
        val org = persist(ObjectFactory.defaultOrganization())
        val vendorA = persist(ObjectFactory.defaultVendor(name = "Vendor A"))
        val vendorB = persist(ObjectFactory.defaultVendor(name = "Vendor B"))
        val vendorC = persist(ObjectFactory.defaultVendor(name = "Vendor C"))

        val dataRecipientA = persist(OrganizationDataRecipient(organization = org, vendor = vendorA))
        val dataRecipientB = persist(OrganizationDataRecipient(organization = org, vendor = vendorB))
        val dataRecipientC = persist(OrganizationDataRecipient(organization = org, vendor = vendorC))

        val collectionGroupA = persist(ObjectFactory.defaultCollectionGroup(name = "Cool Cats", org = org))
        val collectionGroupB = persist(ObjectFactory.defaultCollectionGroup(name = "Lame Cats", org = org))

        // Associate Collection Group A to Vendor A, Vendor B, and Vendor C; Vendor C has no data disclosure marked
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientA,
                collectionGroup = collectionGroupA
            )
        )
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientB,
                collectionGroup = collectionGroupA
            )
        )
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientC,
                collectionGroup = collectionGroupA,
                noDataDisclosure = true
            )
        )

        // Associate Collection Group B to Vendor B
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientB,
                collectionGroup = collectionGroupB
            )
        )

        // Collect a PIC from both Collection Groups
        val picGroup = persist(ObjectFactory.defaultPICGroup("test", "test"))
        val pic = persist(ObjectFactory.defaultPersonalInformationCategory("test", "test", group = picGroup))
        val collectedPicA = persist(ObjectFactory.collectedPICToCollectionGroup(collectionGroupA, pic))
        val collectedPicB = persist(ObjectFactory.collectedPICToCollectionGroup(collectionGroupB, pic))

        // Disclose the PIC from CGA to Vendor A
        persist(DataRecipientDisclosedPIC(collectedPIC = collectedPicA, vendor = vendorA))

        // Disclose the PIC from CGA to Vendor B
        persist(DataRecipientDisclosedPIC(collectedPIC = collectedPicB, vendor = vendorB))

        // when
        flushAndClear()
        val unmappedVendorsForA = subject.getUnmappedDataRecipients(org, collectionGroupA)
        val unmappedVendorsForB = subject.getUnmappedDataRecipients(org, collectionGroupB)

        // then
        assertEquals(1, unmappedVendorsForA.size)
        assertNotNull(unmappedVendorsForA.find { it.id == dataRecipientB.id })

        assertEquals(0, unmappedVendorsForB.size)
    }

    @Test
    fun `getFullDataMap - generates Data Map DTO`() {
        // given
        val org = persist(ObjectFactory.defaultOrganization())
        val vendorA = persist(ObjectFactory.defaultVendor(name = "Vendor A", recipientCategory = "Test"))
        val vendorB = persist(ObjectFactory.defaultVendor(name = "Vendor B", recipientCategory = "Test"))
        val vendorC = persist(ObjectFactory.defaultVendor(name = "Vendor C", recipientCategory = "Test"))

        val dataRecipientA = persist(OrganizationDataRecipient(organization = org, vendor = vendorA))
        val dataRecipientB = persist(OrganizationDataRecipient(organization = org, vendor = vendorB))
        val dataRecipientC = persist(OrganizationDataRecipient(organization = org, vendor = vendorC))

        val collectionGroupA = persist(ObjectFactory.defaultCollectionGroup(name = "Cool Cats", org = org))
        val collectionGroupB = persist(ObjectFactory.defaultCollectionGroup(name = "Lame Cats", org = org))

        // Associate Collection Group A to Vendor A, Vendor B, and Vendor C; Vendor C has no data disclosure marked
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientA,
                collectionGroup = collectionGroupA
            )
        )
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientB,
                collectionGroup = collectionGroupA
            )
        )
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientC,
                collectionGroup = collectionGroupA,
                noDataDisclosure = true
            )
        )

        // Associate Collection Group B to Vendor B
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientB,
                collectionGroup = collectionGroupB
            )
        )

        // Collect a PIC from both Collection Groups
        val picGroup = persist(ObjectFactory.defaultPICGroup("test", "test"))
        val pic = persist(ObjectFactory.defaultPersonalInformationCategory("test", "test", group = picGroup))
        val collectedPicA = persist(ObjectFactory.collectedPICToCollectionGroup(collectionGroupA, pic))
        val collectedPicB = persist(ObjectFactory.collectedPICToCollectionGroup(collectionGroupB, pic))

        // Disclose the PIC from CGA to Vendor A
        persist(DataRecipientDisclosedPIC(collectedPIC = collectedPicA, vendor = vendorA))

        // Disclose the PIC from CGA to Vendor B
        persist(DataRecipientDisclosedPIC(collectedPIC = collectedPicB, vendor = vendorB))

        // when
        flushAndClear()
        val dataMap = subject.getFullDataMap(org)

        // then

        // this seems wrong; it appears we're getting doubles from the previous test
        assertEquals(4, dataMap.disclosure.size)
        val disclosureCategoryIds = dataMap.disclosure.flatMap { it.disclosed }.map { it }.distinct()
        assert(disclosureCategoryIds.containsAll(listOf(pic.id.toString())))

        assertEquals(2, dataMap.collectionGroups.size)
        val collectionGroupNames = dataMap.collectionGroups.map { it.value.name }.toList()
        assert(collectionGroupNames.containsAll(listOf(collectionGroupA.name, collectionGroupB.name)))

        assertEquals(3, dataMap.recipients.size)
        val recipientNames = dataMap.recipients.map { it.value.name }.toList()
        assert(
            recipientNames.containsAll(
                listOf(
                    dataRecipientA.vendor?.name,
                    dataRecipientB.vendor?.name,
                    dataRecipientC.vendor?.name
                )
            )
        )
    }

    @Test
    fun `getFullDataMap - works when a recipient is missing a category`() {
        // given
        val org = persist(ObjectFactory.defaultOrganization())
        val vendorA = persist(ObjectFactory.defaultVendor(name = "Vendor A", recipientCategory = "Test"))
        val vendorB = persist(ObjectFactory.defaultVendor(name = "Vendor B", recipientCategory = "Test"))
        val vendorC = persist(ObjectFactory.defaultVendor(name = "Vendor C"))

        val dataRecipientA = persist(OrganizationDataRecipient(organization = org, vendor = vendorA))
        val dataRecipientB = persist(OrganizationDataRecipient(organization = org, vendor = vendorB))
        val dataRecipientC = persist(OrganizationDataRecipient(organization = org, vendor = vendorC))

        val collectionGroupA = persist(ObjectFactory.defaultCollectionGroup(name = "Cool Cats", org = org))
        val collectionGroupB = persist(ObjectFactory.defaultCollectionGroup(name = "Lame Cats", org = org))

        // Associate Collection Group A to Vendor A, Vendor B, and Vendor C; Vendor C has no data disclosure marked
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientA,
                collectionGroup = collectionGroupA
            )
        )
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientB,
                collectionGroup = collectionGroupA
            )
        )
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientC,
                collectionGroup = collectionGroupA,
                noDataDisclosure = true
            )
        )

        // Associate Collection Group B to Vendor B
        persist(
            ObjectFactory.dataRecipientToCollectionGroup(
                dataRecipient = dataRecipientB,
                collectionGroup = collectionGroupB
            )
        )

        // Collect a PIC from both Collection Groups
        val picGroup = persist(ObjectFactory.defaultPICGroup("test", "test"))
        val pic = persist(ObjectFactory.defaultPersonalInformationCategory("test", "test", group = picGroup))
        val collectedPicA = persist(ObjectFactory.collectedPICToCollectionGroup(collectionGroupA, pic))
        val collectedPicB = persist(ObjectFactory.collectedPICToCollectionGroup(collectionGroupB, pic))

        // Disclose the PIC from CGA to Vendor A
        persist(DataRecipientDisclosedPIC(collectedPIC = collectedPicA, vendor = vendorA))

        // Disclose the PIC from CGA to Vendor B
        persist(DataRecipientDisclosedPIC(collectedPIC = collectedPicB, vendor = vendorB))

        // when
        flushAndClear()
        val dataMap = subject.getFullDataMap(org)

        // then
        assertEquals(2, dataMap.recipients.size)
        val recipientNames = dataMap.recipients.map { it.value.name }.toList()
        assert(!recipientNames.contains(dataRecipientC.vendor?.name))
    }
}
