package polaris.services

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.models.dto.request.ConfirmIdentityDataRequest
import polaris.models.dto.request.CreateDataRequest
import polaris.models.dto.request.ExtendDataRequest
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.regulation.Regulation
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.user.User
import polaris.models.entity.vendor.Vendor
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.DataRequestEventRepository
import polaris.repositories.DataRequestRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationUserRepository
import polaris.repositories.PrivacyCenterRepository
import polaris.repositories.RegulationRepository
import polaris.repositories.UserRepository
import polaris.repositories.VendorRepository
import polaris.services.primary.DataSubjectRequestService
import polaris.services.support.EmailService
import polaris.services.support.email.EmailResponse
import util.Factory
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class DataSubjectRequestConfirmTest {
    @Autowired
    private lateinit var regulationRepository: RegulationRepository

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var privacyCenterRepository: PrivacyCenterRepository

    @Autowired
    private lateinit var organizationUserRepository: OrganizationUserRepository

    @Autowired
    private lateinit var collectionGroupRepository: CollectionGroupRepository

    @Autowired
    private lateinit var dataRequestEventRepository: DataRequestEventRepository

    @Autowired
    private lateinit var dataRequestRepository: DataRequestRepository

    @Autowired
    private lateinit var vendorRepository: VendorRepository

    @Autowired
    private lateinit var organizationDataRecipientRepository: OrganizationDataRecipientRepository

    @Autowired
    private lateinit var dataSubjectRequestService: DataSubjectRequestService

    @MockBean
    private lateinit var emailService: EmailService

    private lateinit var regulation: Regulation
    private lateinit var organization: Organization
    private lateinit var user: User
    private lateinit var dataRequestType: DataRequestType
    private lateinit var collectionGroup: CollectionGroup
    private lateinit var vendor: Vendor
    private lateinit var organizationDataRecipient: OrganizationDataRecipient
    private lateinit var privacyCenter: PrivacyCenter

    @Before
    fun createOrganizations() {
        regulation = regulationRepository.findBySlug("CCPA")!!
        organization = Factory.createOrganization(organizationRepository, name = "Organization 1")
        vendor = Factory.createVendor(vendorRepository, name = "Facebook")
        user = Factory.createUser(userRepository = userRepository, organizationUserRepository = organizationUserRepository, organization = organization, email = "user@example.com")

        dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW
        collectionGroup = Factory.createCollectionGroup("Customer", organization, collectionGroupRepository)
        organizationDataRecipient = Factory.createOrganizationVendor(organizationDataRecipientRepository = organizationDataRecipientRepository, o = organization, vendor = vendor)

        privacyCenter = Factory.createPrivacyCenter(privacyCenterRepository, organization, requestEmail = "admin@truevault.com")
    }

    fun dueDateTestHelper(dataRequestType: DataRequestType): DataSubjectRequest {
        val r = dataSubjectRequestService.create(
            user, organization.publicId,
            CreateDataRequest(
                subjectFirstName = "John",
                subjectLastName = "Doe",
                dataRequestType = dataRequestType,
                requestDate = "2020-03-13T00:00:00.000Z",
                selfDeclarationProvided = true
            )
        )

        val localRequestDate = ZonedDateTime.of(LocalDateTime.parse("2020-03-13T00:00:00"), ZoneId.systemDefault())
        val localVerifyDate = ZonedDateTime.of(LocalDateTime.parse("2020-03-18T00:00:00"), ZoneId.systemDefault())
        Assert.assertEquals(localRequestDate, r.requestDate)

        // Should be null until Email Verification
        Assert.assertNull(r.requestDueDate)

        dataSubjectRequestService.confirmEmail(null, organization.publicId, r.publicId, ConfirmIdentityDataRequest(r.confirmationToken), privacyCenter, localVerifyDate, mutableListOf())
        Assert.assertTrue(r.emailVerifiedAutomatically)
        Assert.assertEquals(localVerifyDate, r.emailVerifiedDate)
        Assert.assertEquals(
            localVerifyDate.plusDays(30).withHour(0).withMinute(0).withSecond(0),
            r.requestDueDate
        )
        return r
    }

    @Test()
    fun create__setsDueDateOnlyAfterEmailVerificationGdprAccess() {
        dueDateTestHelper(DataRequestType.GDPR_ACCESS)
    }

    @Test()
    fun create__setsDueDateOnlyAfterEmailVerificationGdprErasure() {
        dueDateTestHelper(DataRequestType.GDPR_ERASURE)
    }

    @Test()
    fun modify__extend_due_date() {
        val r = dueDateTestHelper(DataRequestType.GDPR_ACCESS)
        dataSubjectRequestService.extend(user, organization.publicId, r.publicId, ExtendDataRequest("I must extend!"))
        Mockito.verify(emailService, Mockito.times(1)).sendExtendDueDate(user, r, "I must extend!")
        val mockResponse = EmailResponse(
            statusCode = 200,
            responseBody = "",
            responseHeaders = mutableMapOf()
        )
        Mockito.`when`(emailService.sendExtendDueDate(user, r, "I must extend!")).thenReturn(mockResponse)
        val localVerifyDate = ZonedDateTime.of(LocalDateTime.parse("2020-03-18T00:00:00"), ZoneId.systemDefault())

        Assert.assertEquals(localVerifyDate.plusDays(90).withHour(0).withMinute(0).withSecond(0), r.requestDueDate)
    }
}
