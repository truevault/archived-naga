package polaris.services

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.models.entity.OrganizationUserAlreadyExists
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.user.UserRole
import polaris.models.entity.user.UserStatus
import polaris.repositories.OrganizationMailboxRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationUserRepository
import polaris.repositories.PrivacyCenterRepository
import polaris.repositories.UserRepository
import polaris.services.primary.UserService
import util.Factory
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class UserVendorTest {
    @Autowired
    private lateinit var ar: OrganizationRepository

    @Autowired
    private lateinit var organizationMailboxRepository: OrganizationMailboxRepository

    @Autowired
    private lateinit var privacyCenterRepository: PrivacyCenterRepository

    @Autowired
    private lateinit var ur: UserRepository

    @Autowired
    private lateinit var aur: OrganizationUserRepository

    @Autowired
    private lateinit var userService: UserService

    private lateinit var a: Organization
    private lateinit var m: OrganizationMailbox
    private lateinit var p: PrivacyCenter

    @Before
    fun createOrganization() {
        a = Factory.createOrganization(ar, name = "Organization 1")
        p = Factory.createPrivacyCenter(privacyCenterRepository, a)
        m = Factory.createOrgMailbox(organizationMailboxRepository, a, status = MailboxStatus.CONNECTED)
        ar.save(a)
    }

    @Test
    fun createUserNoInvitation__setsTheOrganization() {
        val u = userService.createUserNoInvitation(email = "hello@example.com", password = "hello", firstName = "hello", lastName = "example", organization = a, role = UserRole.ADMIN)

        assertNotNull(u)
        assertEquals(1, u.organization_users.size)
        assertEquals(a, u.organization_users.first().organization!!)
    }

    @Test
    fun findByEmail__findsExistingOrganization() {
        val u = Factory.createUser(
            userRepository = ur, organizationUserRepository = aur,
            organization = a, email = "hello@example.com"
        )

        val user = userService.findByEmail(u.email)
        assertNotNull(user)
    }

    @Test
    fun findByEmail__nullForNonExistantOrganization() {
        val user = userService.findByEmail("hello@example.com")
        assertNull(user)
    }

    @Test
    fun springSecurityRolesForUser__returnsTheCorrectRoles() {
        val a2 = Factory.createOrganization(organizationRepository = ar, name = "Organization 2")
        val a3 = Factory.createOrganization(organizationRepository = ar, name = "Organization 3")

        val u = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", role = UserRole.ADMIN, status = UserStatus.ACTIVE)
        var roles = userService.springSecurityRolesForUser(u)
        assertEquals(1, roles.size)
        assertTrue(roles.contains("${a.publicId}:ADMIN"))

        Factory.addUserToOrganization(userRepository = ur, organizationUserRepository = aur, organization = a2, user = u, status = UserStatus.ACTIVE)
        roles = userService.springSecurityRolesForUser(u)
        assertEquals(2, roles.size)
        assertTrue(roles.contains("${a.publicId}:ADMIN"))
        assertTrue(roles.contains("${a2.publicId}:ADMIN"))

        Factory.addUserToOrganization(userRepository = ur, organizationUserRepository = aur, organization = a3, user = u, status = UserStatus.INVITED)
        roles = userService.springSecurityRolesForUser(u)
        assertEquals(2, roles.size)
        assertTrue(roles.contains("${a.publicId}:ADMIN"))
        assertTrue(roles.contains("${a2.publicId}:ADMIN"))
    }

    @Test
    fun allUsersForOrganization__returnsOrganizationsInAllStatuses() {
        val u = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", status = UserStatus.ACTIVE)
        val u2 = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u2@example.com", status = UserStatus.INVITED)
        val u3 = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u3@example.com", status = UserStatus.INACTIVE)

        val users = userService.allUsersForOrganization(a.publicId).map { it.user }

        assertEquals(3, users.size)
        assertTrue(users.contains(u))
        assertTrue(users.contains(u2))
        assertTrue(users.contains(u3))
    }

    @Test
    fun allUsersForOrganization__returnsOnlyUsersForGivenOrganization() {
        val a2 = Factory.createOrganization(organizationRepository = ar, name = "Organization 2")

        val u = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", status = UserStatus.ACTIVE)
        val u2 = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u2@example.com", status = UserStatus.INVITED)
        val u3 = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a2, email = "u3@example.com", status = UserStatus.ACTIVE)

        val users = userService.allUsersForOrganization(a.publicId).map { it.user }

        assertEquals(2, users.size)
        assertTrue(users.contains(u))
        assertTrue(users.contains(u2))

        assertFalse(users.contains(u3))
    }

    @Test
    fun deactivate__deactivatesTheUserOrganization() {
        val u = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", status = UserStatus.ACTIVE)
        val organizationUser = userService.updateUserStatus(u.email, a.publicId, UserStatus.INACTIVE)

        assertTrue(organizationUser.status == UserStatus.INACTIVE)
        ur.refresh(u)
    }

    @Test
    fun deactivate__deactivateInactiveIsNoop() {
        val u = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", status = UserStatus.INACTIVE)

        val organizationUser = userService.updateUserStatus(u.email, a.publicId, UserStatus.INACTIVE)
        assertTrue(organizationUser.status == UserStatus.INACTIVE)
        ur.refresh(u)
    }

    @Test
    fun delete__invitedUser() {
        val u = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", status = UserStatus.INVITED)

        val organizationsBefore = userService.allUsersForOrganization(a.publicId)

        assertEquals(1, organizationsBefore.size)

        userService.delete(u.email, a.publicId)

        val organizationsAfter = userService.allUsersForOrganization(a.publicId)
        assertEquals(0, organizationsAfter.size)
    }

    @Test
    fun delete__activeUser() {
        val u = Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", status = UserStatus.ACTIVE)
        userService.delete(u.email, a.publicId) // throws
    }

    @Test(expected = OrganizationUserAlreadyExists::class)
    fun inviteUser__activeUser() {
        Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", status = UserStatus.ACTIVE)
        userService.inviteUser("u1@example.com", "u1", "example", a.publicId)
    }

    @Test(expected = OrganizationUserAlreadyExists::class)
    fun inviteUser__inactiveUser() {
        Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", status = UserStatus.INACTIVE)
        userService.inviteUser("u1@example.com", "u1", "example", a.publicId)
    }

    @Test()
    fun inviteUser__invitedUser() {
        Factory.createUser(userRepository = ur, organizationUserRepository = aur, organization = a, email = "u1@example.com", status = UserStatus.INVITED)
        val au = userService.inviteUser("u1@example.com", "u1", "example", a.publicId)

        assertEquals(UserStatus.INVITED, au.status)
    }

    @Test()
    fun inviteUser__newUser() {
        val startingCount = ur.count()
        val au = userService.inviteUser("u1@example.com", "u1", "example", a.publicId)

        assertEquals(UserStatus.INVITED, au.status)
        assertEquals("u1@example.com", au.user?.email)
        assertEquals(startingCount + 1, ur.count())
    }

    @Test()
    fun inviteUser__newUserHasNullPasswordAndPasswordResetToken() {
        val au = userService.inviteUser("u1@example.com", "u1", "example", a.publicId)

        assertNull(au.user!!.password)
        assertNotNull(au.user!!.updatePasswordId)
    }

    @Test(expected = IllegalArgumentException::class)
    fun inviteUser__emptyEmail() {
        val au = userService.inviteUser("", "empty", "email", a.publicId)

        assertNull(au.user!!.password)
        assertNotNull(au.user!!.updatePasswordId)
    }

//  @Test(expected = IllegalArgumentException::class)
//  fun inviteUser__invalidEmail() {
//    val au = userService.inviteUser("notanemail", "not", "email", a.publicId)
//
//    assertNull(au.user!!.password)
//    assertNotNull(au.user!!.updatePasswordId)
//  }
}
