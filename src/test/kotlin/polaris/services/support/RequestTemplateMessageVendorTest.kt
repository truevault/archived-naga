package polaris.services.support

import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.regulation.Regulation
import polaris.models.entity.request.*
import polaris.models.entity.user.User
import polaris.models.entity.vendor.Vendor
import polaris.repositories.*
import util.Factory
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class RequestTemplateMessageVendorTest {

    @Autowired
    private lateinit var regulationRepository: RegulationRepository

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var organizationUserRepository: OrganizationUserRepository

    @Autowired
    private lateinit var collectionGroupRepository: CollectionGroupRepository

    @Autowired
    private lateinit var dataRequestEventRepository: DataRequestEventRepository

    @Autowired
    private lateinit var dataRequestRepository: DataRequestRepository

    @Autowired
    private lateinit var vendorRepository: VendorRepository

    @Autowired
    private lateinit var organizationDataRecipientRepository: OrganizationDataRecipientRepository

    @Autowired
    private lateinit var requestTemplateService: RequestTemplateMessageService

    private lateinit var regulation: Regulation
    private lateinit var organization: Organization
    private lateinit var dataSubjectRequest: DataSubjectRequest

    private lateinit var user: User
    private lateinit var collectionGroup: CollectionGroup
    private lateinit var dataRequestType: DataRequestType
    private lateinit var vendor: Vendor
    private lateinit var orgVendor: OrganizationDataRecipient

    @Before
    fun createTestData() {
        regulation = regulationRepository.findBySlug("CCPA")!!
        organization = Factory.createOrganization(organizationRepository, name = "Organization 1")
        vendor = Factory.createVendor(vendorRepository, name = "Facebook")
        user = Factory.createUser(userRepository = userRepository, organizationUserRepository = organizationUserRepository, organization = organization, email = "user@example.com")

        collectionGroup = Factory.createCollectionGroup("Customer", organization, collectionGroupRepository)
        dataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW
        orgVendor = Factory.createOrganizationVendor(organizationDataRecipientRepository = organizationDataRecipientRepository, o = organization, vendor = vendor)

        dataSubjectRequest = Factory.createDataRequest(dataRequestRepository, dataRequestEventRepository, organization, user, dataRequestType, subjectFirstName = "Bob", subjectLastName = "Smith")
    }

    @After
    fun deleteTestData() {
    }

    @Test
    fun dataRequestMessageTemplate__works() {
        val template = "organization-specific:\n" +
            "\n" +
            "\${organization.name}\n" +
            "\n" +
            "\${organization.privacy_policy_url}\n" +
            "\n" +
            "\${organization.request_toll_free_phone_number}\n" +
            "\n" +
            "\${organization.request_form_url}\n" +
            "\n" +
            "request-specific:\n" +
            "\n" +
            "\${request.id}\n" +
            "\n" +
            "\${request.request_type}\n" +
            "\n" +
            "\${request.data_subject.type}\n" +
            "\n" +
            "\${request.request_date}\n" +
            "\n" +
            "\${request.deadline}\n" +
            "\n" +
            "DST First Name: '\${request.data_subject.first_name}'\n" +
            "\n" +
            "DST Last Name: '\${request.data_subject.last_name}'\n" +
            "\n" +
            "DST Email: '\${request.data_subject.email}'\n" +
            "\n" +
            "\${user.first_name}\n" +
            "\n" +
            "\${user.last_name}\n" +
            "\n" +
            "\${user.full_name}"

        val result = requestTemplateService.dataRequestMessageTemplate(user, organization, dataSubjectRequest, template)

        assertNotNull(result)

        assert(result.contains("DST First Name: 'Bob'"))
        assert(result.contains("DST Last Name: 'Smith'"))
        assert(result.contains("DST Email: ''"))
    }
}
