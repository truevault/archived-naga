package polaris.services.support

import freemarker.template.Configuration
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.mockito.Mock
import polaris.BaseUnitTest
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestType
import polaris.repositories.CustomMessageRepository
import polaris.services.support.email.EmailSender
import util.ObjectFactory
import java.util.UUID

class EmailServiceTest : BaseUnitTest() {

    @Mock
    private lateinit var freemarkerConfig: Configuration

    @Mock
    private lateinit var emailSender: EmailSender

    @Mock
    private lateinit var signatureTemplateMessageService: SignatureTemplateMessageService

    @Mock
    private lateinit var optOutService: OptOutService

    @Mock
    private lateinit var subjectRequestRelationService: DataMapService

    @Mock
    private lateinit var customMessageRepo: CustomMessageRepository

    private lateinit var subject: EmailService

    @BeforeEach
    fun setup() {
        subject = EmailService(
            "unit-test",
            "qa.truevault.com",
            "Unit Test",
            "unit-test@qa.truevault.com",
            "localhost",
            freemarkerConfig,
            emailSender,
            signatureTemplateMessageService,
            optOutService,
            subjectRequestRelationService,
            customMessageRepo
        )
    }

    @ParameterizedTest(name = "{index}: shouldNewSendDSRNotification - with requestType={0}, state={1}, orgSuppressed={2}")
    @CsvSource(
        "CCPA_RIGHT_TO_DELETE,PENDING_EMAIL_VERIFICATION,false,false", // type does not qualify, state does not qualify
        "CCPA_RIGHT_TO_DELETE,CLOSED,false,false", // type does not qualify, state does not qualify
        "CCPA_RIGHT_TO_DELETE,VERIFY_CONSUMER,false,true", // type does not qualify, state qualifies
        "CCPA_OPT_OUT,PENDING_EMAIL_VERIFICATION,false,true", // type qualifies, state does not qualify
        "CCPA_OPT_OUT,CLOSED,false,true", // type qualifies, state does not qualify
        "CCPA_OPT_OUT,VERIFY_CONSUMER,false,true", // type qualifies, state qualifies
        "CCPA_OPT_OUT,VERIFY_CONSUMER,true,false" // org is suppressed
    )
    fun `shouldNewSendDSRNotification - with various args`(
        requestType: DataRequestType,
        state: DataRequestState,
        orgSupressed: Boolean,
        expected: Boolean
    ) {
        // given
        val orgId = if (orgSupressed) "53faa226-6996-4310-b4f4-84cfaa6900cc" else "55555555-6996-4310-b4f4-84cfaa6900cc"
        val organization = ObjectFactory.defaultOrganization(id = UUID.fromString(orgId))
        val request = ObjectFactory.defaultDataRequest(
            organization,
            requestType = requestType,
            state = state
        )

        // when
        val actual = subject.shouldNewSendDSRNotification(request)

        // then
        assert(actual == expected)
    }
}
