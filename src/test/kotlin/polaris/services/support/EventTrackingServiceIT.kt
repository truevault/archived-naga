package polaris.services.support

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import polaris.BaseIntegrationTest
import polaris.models.entity.organization.EventData
import polaris.models.entity.organization.MappingProgressEnum
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationUser
import polaris.models.entity.user.User
import polaris.models.entity.user.UserStatus
import polaris.repositories.EventRepository
import polaris.repositories.OrganizationRepository
import polaris.services.support.events.EventTrackingService
import polaris.util.PersistenceUtils
import util.ObjectFactory
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class EventTrackingServiceIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var eventRepo: EventRepository

    @Autowired
    private lateinit var orgRepo: OrganizationRepository

    @Autowired
    private lateinit var subject: EventTrackingService

    @Autowired
    private lateinit var executor: ThreadPoolTaskExecutor

    @Autowired
    private lateinit var persistenceUtils: PersistenceUtils

    private lateinit var org: Organization
    private lateinit var user: User
    private lateinit var orgUser: OrganizationUser

    @BeforeEach
    fun createTestData() {
        // need to persist these entities in a separate transaction
        // since the event listener thread needs to fetch from DB.
        // otherwise the listener throws an exception due to not being
        // able to locate the organization entity from the DB.
        org = persistenceUtils.persistAndFlushWithNewTx(ObjectFactory.defaultOrganization())
        user = persistenceUtils.persistAndFlushWithNewTx(
            User(
                email = "orgUser@example.com",
                password = "1234",
                firstName = "test",
                lastName = "user"
            )
        )
        orgUser =
            persistenceUtils.persistAndFlushWithNewTx(OrganizationUser(user = user, organization = org, status = UserStatus.ACTIVE))
    }

    @Test
    fun `publish - persists events asynchronously`() {
        assertNotNull(orgRepo.findByIdOrNull(org.id))

        val oldEventCount = eventRepo.count()

        subject.publish(
            listOf(
                EventData.GetCompliantProgressUpdate("Step 1", "Step 2"),
                EventData.GenericProgressUpdate("key", null, MappingProgressEnum.IN_PROGRESS)
            ),
            org, user
        )
        executor.threadPoolExecutor.awaitTermination(5, TimeUnit.SECONDS)

        val newEventCount = eventRepo.count()

        assertEquals(2, newEventCount - oldEventCount)
    }
}
