package polaris.services.support

import freemarker.template.Configuration
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.models.dto.request.VendorOutcomeValue
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.organization.OrganizationUser
import polaris.models.entity.privacycenter.CustomUrlStatus
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.VendorOutcome
import polaris.models.entity.user.User
import polaris.models.entity.user.UserStatus
import polaris.models.entity.vendor.Vendor
import polaris.repositories.CustomMessageRepository
import polaris.repositories.OrganizationMailboxRepository
import polaris.repositories.OrganizationRepository
import polaris.services.support.email.EmailSender
import util.ObjectFactory

/**
 * The BaseIntegrationTest class defines a "mock" instance of EmailService
 * for general use across integration tests.
 *
 * This test suite therefore instantiates a "real" instance of EmailService using
 * actual Spring beans for the dependencies. This is suitable for testing
 * freemarker template rendering since it uses the {@link polaris.services.support.email.LocalEmailSender}
 * to render the emails in a browser.
 *
 * @see polaris.services.support.email.LocalEmailSender
 */
class EmailServiceIT : BaseIntegrationTest() {
    @Autowired
    private lateinit var freemarkerConfig: Configuration

    @Autowired
    private lateinit var emailSender: EmailSender

    @Autowired
    private lateinit var signatureTemplateMessageService: SignatureTemplateMessageService

    @Autowired
    private lateinit var optOutService: OptOutService

    @Autowired
    private lateinit var subjectRequestRelationService: DataMapService

    @Autowired
    private lateinit var customMessageRepo: CustomMessageRepository

    @Autowired
    private lateinit var organizationMailboxRepository: OrganizationMailboxRepository

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    private lateinit var subject: EmailService

    @BeforeEach
    fun setup() {
        subject = EmailService(
            "integration-test",
            "qa.truevault.com",
            "Integration Test",
            "integration-test@qa.truevault.com",
            "localhost",
            freemarkerConfig,
            emailSender,
            signatureTemplateMessageService,
            optOutService,
            subjectRequestRelationService,
            customMessageRepo
        )
    }

    @Test
    fun `sendNewPrivacyInboxMessageNotification - new message with no Mine parsing errors`() {
        val organizationMailbox = setupOrganizationMailbox()
        subject.sendNewPrivacyInboxMessageNotification(
            organizationMailbox,
            "test@example.com",
            "Email, no Mine parse errors"
        )
    }

    @Test
    fun `sendNewPrivacyInboxMessageNotification - new message with Mine parsing errors`() {
        val organizationMailbox = setupOrganizationMailbox()
        subject.sendNewPrivacyInboxMessageNotification(
            organizationMailbox,
            "test@example.com",
            "Email, with Mine parse errors",
            true
        )
    }

    @DisplayName("Data Subject Request Closed: CCPA Opt-Out Email")
    @ParameterizedTest(name = "{index}: Data Subject Request Closed: CCPA Opt-Out Email with NO_DATA={0}, DATA_DELETED={1}, DATA_RETAINED={2}")
    @CsvSource(
        "false,false,false",
        "false,false,true",
        "false,true,false",
        "false,true,true",
        "true,false,false",
        "true,false,true",
        "true,true,false",
        "true,true,true"
    )
    fun `Data Subject Request Closed - CCPA Opt-Out Email - with vendor outcome`(
        hasNoDataOutcome: Boolean,
        hasDeletedOutcome: Boolean,
        hasDataRetainedOutcome: Boolean
    ) {
        val organization = setupOrganization(3)
        val privacyCenter = organization.privacyCenters.first()
        val user = organization.activeUsers().first().user!!

        val dataSubjectRequest = persist(
            ObjectFactory.defaultDataRequest(
                organization = organization,
                requestType = DataRequestType.CCPA_OPT_OUT,
                subjectEmailAddress = "johndoe@example.com"
            )
        )

        val addOutcome = { outcome: VendorOutcomeValue, vendor: Vendor ->
            val vendorOutcome = persist(
                VendorOutcome(
                    dataSubjectRequestId = dataSubjectRequest.id,
                    vendorId = vendor.id,
                    vendor = vendor,
                    outcome = outcome
                )
            )
            dataSubjectRequest.vendorOutcomes.add(vendorOutcome)
        }

        if (hasNoDataOutcome) addOutcome(VendorOutcomeValue.NO_DATA, organization.organizationDataRecipients[0].vendor!!)
        if (hasDeletedOutcome) addOutcome(VendorOutcomeValue.DATA_DELETED, organization.organizationDataRecipients[1].vendor!!)
        if (hasDataRetainedOutcome) addOutcome(VendorOutcomeValue.DATA_RETAINED, organization.organizationDataRecipients[2].vendor!!)

        val body = subject.getSubjectRequestClosedOptOutBody(user, dataSubjectRequest)

        assertTrue(body.contains("Hi ${dataSubjectRequest.subjectFirstName}"))
        assertTrue(body.contains("We have opted you out of any sale of information. If you wish to opt back in, please email us at ${privacyCenter.requestEmail}."))
        assertTrue(body.contains("If you submitted a request via email or telephone and would like to opt-out of browser-based sharing while visiting our website, visit Do Not Sell Or Share My Personal Information"))
        assertTrue(body.contains(privacyCenter.getUrl("/opt-out")))
    }

    private fun setupOrganization(numVendors: Int = 1): Organization {
        val organization = persist(ObjectFactory.defaultOrganization())
        val user = persist(
            User(
                email = "orgUser@example.com",
                password = "1234",
                firstName = "test",
                lastName = "user"
            )
        )
        val privacyCenter = persist(ObjectFactory.defaultPrivacyCenter(org = organization))
        privacyCenter.customUrl = "privacy.google.com"
        privacyCenter.customUrlStatus = CustomUrlStatus.ACTIVE

        persist(OrganizationUser(user = user, organization = organization, status = UserStatus.ACTIVE))

        for (i in 0..numVendors) {
            val vendor = persist(ObjectFactory.defaultVendor(name = "Vendor $i"))
            val orgVendor = persist(ObjectFactory.defaultDataRecipient(organization = organization, vendor = vendor))
            orgVendor.ccpaIsSelling = true
            orgVendor.ccpaIsSharing = true
        }

        flushAndClear()

        return organizationRepository.findById(organization.id).get()
    }

    private fun setupOrganizationMailbox(): OrganizationMailbox {
        val organization = setupOrganization()
        val organizationMailbox = persist(ObjectFactory.defaultOrganizationMailbox(organization = organization))

        flushAndClear()

        return organizationMailboxRepository.findById(organizationMailbox.id).get()
    }
}
