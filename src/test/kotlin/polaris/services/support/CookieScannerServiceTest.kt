package polaris.services.support

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import polaris.BaseUnitTest
import polaris.controllers.util.LookupService
import polaris.models.entity.cookieRecommendation.CookieRecommendation
import polaris.models.entity.organization.OrganizationCookie
import polaris.repositories.CookieRecommendationRepository
import polaris.repositories.OrgCookieRepo
import polaris.repositories.OrganizationCookieScanRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.services.dto.OrganizationCookieDtoService

class CookieScannerServiceTest : BaseUnitTest() {

    @Mock
    private lateinit var awsClientService: AwsClientService
    @Mock
    private lateinit var orgCookieRepo: OrgCookieRepo
    @Mock
    private lateinit var orgCookieScanRepo: OrganizationCookieScanRepository
    @Mock
    private lateinit var cookieRecommendationRepository: CookieRecommendationRepository
    @Mock
    private lateinit var organizationCookieDtoService: OrganizationCookieDtoService
    @Mock
    private lateinit var dataRecipientRepo: OrganizationDataRecipientRepository
    @Mock
    private lateinit var lookup: LookupService

    private lateinit var subject: CookieScannerService

    @BeforeEach
    fun setup() {
        // note: cannot use @InjectMocks due to the final string argument cookieScannerQueueUrl, which cannot be mocked because String is final.
        subject = CookieScannerService(awsClientService, orgCookieRepo, orgCookieScanRepo, cookieRecommendationRepository, organizationCookieDtoService, dataRecipientRepo, lookup, "dummy-url")
    }

    @Test
    fun `matchesRecommendation - matches for exact name and domain match`() {
        assertMatches("__csruif", ".www.truevault.com", "__csruif", ".www.truevault.com")
    }

    @Test
    fun `matchesRecommendation - matches for exact name and partial domain match`() {
        assertMatches("__csruif", ".www.truevault.com", "__csruif", "www.truevault.com")
        assertMatches("__csruif", ".www.truevault.com", "__csruif", "truevault.com")
    }

    @Test
    fun `matchesRecommendation - matches for exact name and wildcard domain match`() {
        assertMatches("__csruif", ".www.truevault.com", "__csruif", "*")
        assertMatches("__csruif", null, "__csruif", "*")
        assertMatches("__csruif", null, "__csruif", " * ")
    }

    @Test
    fun `matchesRecommendation - does not match for partial name and exact domain match`() {
        assertDoesNotMatch("__csruif", ".www.truevault.com", "__foo", ".www.truevault.com")
    }

    @Test
    fun `matchesRecommendation - does not match for no name and exact domain match`() {
        assertDoesNotMatch("__csruif", ".www.truevault.com", "foo", ".www.truevault.com")
        assertDoesNotMatch(null, ".www.truevault.com", "foo", ".www.truevault.com")
    }

    @Test
    fun `matchesRecommendation - does not match for exact name and no domain match`() {
        assertDoesNotMatch("__csruif", ".www.truevault.com", "__csruif", ".hubspot.com")
        assertDoesNotMatch("__csruif", null, "__csruif", ".hubspot.com")
    }

    private fun assertMatches(cookieName: String?, cookieDomain: String?, recName: String, recDomain: String, message: String? = null) {
        val cookie = OrganizationCookie(name = cookieName, domain = cookieDomain)
        val domain = CookieRecommendation(name = recName, domain = recDomain)

        val result = subject.matchesRecommendation(cookie, domain)

        assertTrue(result, message)
    }

    private fun assertDoesNotMatch(cookieName: String?, cookieDomain: String?, recName: String, recDomain: String, message: String? = null) {
        val cookie = OrganizationCookie(name = cookieName, domain = cookieDomain)
        val domain = CookieRecommendation(name = recName, domain = recDomain)

        val result = subject.matchesRecommendation(cookie, domain)

        assertFalse(result, message)
    }
}
