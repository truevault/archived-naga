package polaris.services.support

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.models.entity.GdprCookieConsentState
import polaris.repositories.GdprCookieConsentTrackingRepo
import polaris.repositories.OrganizationRepository
import polaris.repositories.PrivacyCenterRepository
import util.Factory
import javax.persistence.EntityManager
import javax.transaction.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class GdprCookieConsentServiceTest {

    @Autowired
    private lateinit var orgRepo: OrganizationRepository

    @Autowired
    private lateinit var privacyCenterRepo: PrivacyCenterRepository
    //
    @Autowired
    private lateinit var gdprCookieConsentTrackingRepo: GdprCookieConsentTrackingRepo

    // SUBJECT
    @Autowired
    private lateinit var service: GdprCookieConsentService

    @Autowired
    private lateinit var entityManager: EntityManager

    @Test()
    fun addCookieConsent__addValidConsent() {
        val org = Factory.createOrganization(orgRepo, name = "Org 1")
        Factory.createPrivacyCenter(privacyCenterRepo, org, requestEmail = "old@truevault.com")
        entityManager.flush()
        entityManager.clear()
        val pc = orgRepo.findByIdOrNull(org.id)!!.privacyCenters.first()

        service.addCookieConsent(pc, key = "abcd", consentState = GdprCookieConsentState(adsPermitted = false, essentialPermitted = true, analyticsPermitted = false, personalizationPermitted = true), ip = "127.32.16.54", userAgent = "Firefox", sourceUrl = "privacy.example.com/page")

        val found = gdprCookieConsentTrackingRepo.findByKey("abcd")
        assertNotNull(found)
        assertEquals("127.32.0.0", found.ipV4)
        assertNull(found.ipV6)
        assertEquals("Firefox", found.userAgent)
    }

    @Test()
    fun calculateMaskedIpV4Value__validIp4Address() {
        assertEquals("32.243.0.0", service.calculateMaskedIpV4Value("32.243.21.16"))
    }

    @Test()
    fun calculateMaskedIpV4Value__invalidIp4Address() {
        assertEquals(null, service.calculateMaskedIpV4Value("32.260.21.16"))
        assertEquals(null, service.calculateMaskedIpV4Value("2001:0db8:85a3:0000:0000:8a2e:0370:7334"))
    }

    @Test()
    fun calculateMaskedIpV4Value__emptyString() {
        assertEquals(null, service.calculateMaskedIpV4Value(""))
    }

    @Test()
    fun calculateMaskedIpV6Value__validIp6Address() {
        assertEquals("2001:db8:85a3::8a2e:300:0", service.calculateMaskedIpV6Value("2001:0db8:85a3:0000:0000:8a2e:0370:7334"))
    }

    @Test()
    fun calculateMaskedIpV6Value__invalidIp6Address() {
        assertEquals(null, service.calculateMaskedIpV6Value("32.243.21.16"))
        assertEquals(null, service.calculateMaskedIpV6Value("2001:gggg:85a3:0000:0000:8a2e:0370:7334"))
    }

    @Test()
    fun calculateMaskedIpV6Value__emptyString() {
        assertEquals(null, service.calculateMaskedIpV6Value(""))
    }
}
