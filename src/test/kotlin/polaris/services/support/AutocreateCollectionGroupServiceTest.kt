package polaris.services.support

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.feature.Features
import polaris.models.entity.organization.BUSINESS_SURVEY_NAME
import polaris.models.entity.organization.HR_SURVEY_NAME
import polaris.models.entity.organization.Organization
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.FeatureRepository
import polaris.repositories.OrganizationFeatureRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import util.Factory
import javax.persistence.EntityManager
import javax.transaction.Transactional
import kotlin.test.assertEquals

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class AutocreateCollectionGroupServiceTest {
    @Autowired
    private lateinit var orgRepo: OrganizationRepository
    @Autowired
    private lateinit var featureRepo: FeatureRepository
    @Autowired
    private lateinit var orgFeatureRepo: OrganizationFeatureRepository
    @Autowired
    private lateinit var surveyRepo: OrganizationSurveyQuestionRepository
    @Autowired
    private lateinit var cgRepo: CollectionGroupRepository
    @Autowired
    private lateinit var entityManager: EntityManager

    @Autowired
    private lateinit var service: AutocreateCollectionGroupService

    private lateinit var org: Organization

    @Before
    fun createTestData() {
        org = Factory.createOrganization(orgRepo, name = "Organization 1")
    }

    @Test
    fun createDefaultCollectionGroups__createsAllExpectedGroups() {
        assert(cgRepo.findAllByOrganization(org).isEmpty())

        Factory.createOrgFeature(repo = orgFeatureRepo, featureRepo = featureRepo, org = org, feature = Features.GDPR)
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "business-employees-ca", answer = "true")
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "employees-eea-uk", answer = "true")

        service.createDefaultCollectionGroups(org.publicId)

        // now check to see if we have the expected collection groups in the database
        val cg = cgRepo.findAllByOrganization(org)
        assertEquals(3, cg.size)
        assertEquals(1, cg.filter { it.autocreationSlug == AutocreatedCollectionGroupSlug.ONLINE_SHOPPERS }.size)
        assertEquals(1, cg.filter { it.autocreationSlug == AutocreatedCollectionGroupSlug.CA_EMPLOYEES }.size)
        assertEquals(1, cg.filter { it.autocreationSlug == AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE }.size)
        assert(cg.none { it.autocreationSlug == AutocreatedCollectionGroupSlug.B2B_PROSPECTS })
        assert(cg.none { it.autocreationSlug == AutocreatedCollectionGroupSlug.CA_CONTRACTORS })
        assert(cg.none { it.autocreationSlug == AutocreatedCollectionGroupSlug.CA_JOB_APPLICANTS })
    }

    @Test
    fun createDefaultCollectionGroups__updatesAutocreatedSlugs() {
        assert(org.autocreatedCollectionGroupSlugs.isEmpty())

        Factory.createOrgFeature(repo = orgFeatureRepo, featureRepo = featureRepo, org = org, feature = Features.GDPR)
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "business-employees-ca", answer = "true")
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "employees-eea-uk", answer = "true")
        service.createDefaultCollectionGroups(org.publicId)

        // now check to see if we have the expected collection groups in the database
        assert(org.autocreatedCollectionGroupSlugs.contains(AutocreatedCollectionGroupSlug.ONLINE_SHOPPERS))
        assert(org.autocreatedCollectionGroupSlugs.contains(AutocreatedCollectionGroupSlug.CA_EMPLOYEES))
        assert(org.autocreatedCollectionGroupSlugs.contains(AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE))
    }

    @Test
    fun createDefaultCollectionGroups__skipsGroupsInAutocreated() {
        Factory.createOrgFeature(repo = orgFeatureRepo, featureRepo = featureRepo, org = org, feature = Features.GDPR)
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "business-employees-ca", answer = "true")
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "employees-eea-uk", answer = "true")

        org.autocreatedCollectionGroupSlugs = listOf(AutocreatedCollectionGroupSlug.ONLINE_SHOPPERS, AutocreatedCollectionGroupSlug.CA_EMPLOYEES)
        orgRepo.save(org)

        service.createDefaultCollectionGroups(org.publicId)

        val cg = cgRepo.findAllByOrganization(org)
        assertEquals(1, cg.size)
        assertEquals(1, cg.filter { it.autocreationSlug == AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE }.size)
    }

    @Test
    fun createDefaultCollectionGroups__skipsGroupsAlreadyCreated() {
        Factory.createOrgFeature(repo = orgFeatureRepo, featureRepo = featureRepo, org = org, feature = Features.GDPR)
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "business-employees-ca", answer = "true")
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "employees-eea-uk", answer = "true")
        Factory.createCollectionGroup("Online Shoppers", org = org, collectionGroupRepository = cgRepo, autocreatedSlug = AutocreatedCollectionGroupSlug.ONLINE_SHOPPERS)

        entityManager.flush()
        entityManager.clear()

        val newOrg = orgRepo.findById(org.id).get()

        assertEquals(1, newOrg.collectionGroups.size)

        service.createDefaultCollectionGroups(newOrg.publicId)

        val cg = cgRepo.findAllByOrganization(newOrg)

        assertEquals(3, cg.size)
        assertEquals(1, cg.filter { it.autocreationSlug == AutocreatedCollectionGroupSlug.ONLINE_SHOPPERS }.size)
        assertEquals(1, cg.filter { it.autocreationSlug == AutocreatedCollectionGroupSlug.CA_EMPLOYEES }.size)
        assertEquals(1, cg.filter { it.autocreationSlug == AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE }.size)
    }

    @Test
    fun getAutocreateGroupsForOrg__OnlineShoppers_AlwaysCreated() {
        val groups = service.getAutocreateGroupsForOrg(org)
        assert(groups.contains(AutocreatedCollectionGroupSlug.ONLINE_SHOPPERS))
    }

    @Test
    fun getAutocreateGroupsForOrg__EEAUKEmp_NotIfNoGdpr() {
        val groups = service.getAutocreateGroupsForOrg(org)
        assert(!groups.contains(AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE))
    }

    @Test
    fun getAutocreateGroupsForOrg__EEAUKEmp_CreatedIfGdpr() {
        Factory.createOrgFeature(repo = orgFeatureRepo, featureRepo = featureRepo, org = org, feature = Features.GDPR)
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "employees-eea-uk", answer = "true")
        val groups = service.getAutocreateGroupsForOrg(org)
        assert(groups.contains(AutocreatedCollectionGroupSlug.EEA_UK_EMPLOYEE))
    }

    @Test
    fun getAutocreateGroupsForOrg__Prospects_NotIfNoAnswer() {
        val groups = service.getAutocreateGroupsForOrg(org)
        assert(!groups.contains(AutocreatedCollectionGroupSlug.B2B_PROSPECTS))
    }

    @Test
    fun getAutocreateGroupsForOrg__Prospects_NotIfNo() {
        Factory.createSurveyResponse(surveyRepo, org, survey = BUSINESS_SURVEY_NAME, slug = "business-buy-from-data-brokers", answer = "false")
        val groups = service.getAutocreateGroupsForOrg(org)
        assert(!groups.contains(AutocreatedCollectionGroupSlug.B2B_PROSPECTS))
    }

    @Test
    fun getAutocreateGroupsForOrg__CaEmployees_NotIfNoAnswer() {
        val groups = service.getAutocreateGroupsForOrg(org)
        assert(!groups.contains(AutocreatedCollectionGroupSlug.CA_EMPLOYEES))
    }

    @Test
    fun getAutocreateGroupsForOrg__CaEmployees_NotIfNo() {
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "business-employees-ca", answer = "false")
        val groups = service.getAutocreateGroupsForOrg(org)
        assert(!groups.contains(AutocreatedCollectionGroupSlug.CA_EMPLOYEES))
    }

    @Test
    fun getAutocreateGroupsForOrg__CaEmployees_IfYes() {
        Factory.createSurveyResponse(surveyRepo, org, survey = HR_SURVEY_NAME, slug = "business-employees-ca", answer = "true")
        val groups = service.getAutocreateGroupsForOrg(org)
        assert(groups.contains(AutocreatedCollectionGroupSlug.CA_EMPLOYEES))
    }
}
