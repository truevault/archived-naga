package polaris.services.support

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.models.dto.cookiescan.CookieScanResultDto
import polaris.models.dto.cookiescan.CookiescanCookie
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationCookie
import polaris.repositories.OrgCookieRepo
import polaris.repositories.OrganizationRepository
import util.Factory
import javax.transaction.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class CookieScanServiceTest {
    @Autowired
    private lateinit var orgRepo: OrganizationRepository

    @Autowired
    private lateinit var orgCookieRepo: OrgCookieRepo

    @Autowired
    private lateinit var service: CookieScannerService

    private lateinit var org: Organization

    @Before
    fun createTestData() {
        org = Factory.createOrganization(orgRepo, name = "Organization 1", publicId = "COOKIESCAN")
    }

    @Test
    fun addCookieScanResult__addsNewCookies() {

        service.addCookieScanResult(
            CookieScanResultDto(
                organizationId = "COOKIESCAN",
                url = "https://www.apsis.io",
                cookies = listOf(
                    CookiescanCookie(
                        name = "something",
                        value = "secret",
                        domain = ".apsis.io",
                        secure = true,

                        expires = null,
                        httpOnly = null,
                        path = null,
                        priority = null,
                        sameParty = null,
                        sameSite = null,
                        session = null
                    )
                )
            )
        )

        val cookies = orgCookieRepo.findAllByOrganization(org)
        assertEquals(1, cookies.size, "Expected cookies to have one entry")
        assertCookie(cookies, "something", ".apsis.io", "secret")
    }

    @Test
    fun addCookieScanResult__updatesExistingCookies() {

        service.addCookieScanResult(
            CookieScanResultDto(
                organizationId = "COOKIESCAN",
                url = "https://www.apsis.io",
                cookies = listOf(
                    CookiescanCookie(
                        name = "something",
                        value = "secret",
                        domain = ".apsis.io",
                        secure = true,

                        expires = null,
                        httpOnly = null,
                        path = null,
                        priority = null,
                        sameParty = null,
                        sameSite = null,
                        session = null
                    )
                )
            )
        )

        service.addCookieScanResult(
            CookieScanResultDto(
                organizationId = "COOKIESCAN",
                url = "https://www.apsis.io",
                cookies = listOf(
                    CookiescanCookie(
                        name = "something",
                        value = "no secret",
                        domain = ".apsis.io",
                        secure = true,

                        expires = null,
                        httpOnly = null,
                        path = null,
                        priority = null,
                        sameParty = null,
                        sameSite = null,
                        session = null
                    ),
                    CookiescanCookie(
                        name = "other",
                        value = "another thing",
                        domain = ".apsis.io",
                        secure = true,

                        expires = null,
                        httpOnly = null,
                        path = null,
                        priority = null,
                        sameParty = null,
                        sameSite = null,
                        session = null
                    )
                )
            )
        )

        val cookies = orgCookieRepo.findAllByOrganization(org)
        assertEquals(2, cookies.size, "Expected cookies to have two entries")
        assertCookie(cookies, "something", ".apsis.io", "no secret")
        assertCookie(cookies, "other", ".apsis.io", "another thing")
    }

    private fun assertCookie(cookies: List<OrganizationCookie>, name: String, domain: String, value: String? = null) {
        val cookie = cookies.find { it.name == name && it.domain == domain }

        assertNotNull(cookie, "Expected to find cookie ($name, $domain) in the provided cookies. Did not find any cookies. Now I'm hungry :/")

        if (value != null) {
            assertEquals(value, cookie.lastScannedValue, "Expected cookie ($name, $domain) to have the given value")
        }
    }
}
