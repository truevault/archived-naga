package polaris.services.support.oauth

import org.joda.time.DateTime
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.anyString
import org.mockito.Mockito.`when`
import org.mockito.kotlin.any
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.never
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import polaris.BaseUnitTest
import polaris.controllers.GoogleAnalyticsOAuthException
import polaris.controllers.util.LookupService
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationAuthToken
import polaris.models.entity.organization.OrganizationAuthTokenProvider
import polaris.repositories.OrganizationAuthTokenRepository
import polaris.services.primary.TaskUpdateService
import util.ObjectFactory
import java.lang.RuntimeException
import java.time.ZonedDateTime
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class GoogleAuthTokenServiceTest : BaseUnitTest() {

    @Mock
    private lateinit var authTokenRepo: OrganizationAuthTokenRepository

    @Mock
    private lateinit var googleOauth: GoogleOauthSupport

    @Mock
    private lateinit var lookup: LookupService

    @Mock
    private lateinit var taskUpdateService: TaskUpdateService

    @InjectMocks
    private lateinit var subject: GoogleAuthTokenService

    @Test
    fun `fetchAndPersistGoogleAnalyticsAuthToken - when request tokens from Google API succeeds`() {
        val organization = ObjectFactory.defaultOrganization()
        val code = "1234"
        val tokenResponse = createGaAccessTokenResponse("123", "456")

        `when`(googleOauth.requestRefreshToken(organization, code)).thenReturn(tokenResponse)
        `when`(googleOauth.assertRefreshTokenIsValid(tokenResponse)).thenCallRealMethod()

        subject.fetchAndPersistGoogleAnalyticsAuthToken(organization, code)

        verify(googleOauth, never()).revokeAuthToken(anyString())
        verify(authTokenRepo, times(1)).save(any<OrganizationAuthToken>())
        verify(taskUpdateService, times(1)).updateTasks(organization, create = false, close = true)
    }

    @Test
    fun `fetchAndPersistGoogleAnalyticsAuthToken - when Google API returns null access_token`() {
        val organization = ObjectFactory.defaultOrganization()
        val code = "1234"
        val tokenResponse = createGaAccessTokenResponse(null, null)

        `when`(googleOauth.requestRefreshToken(organization, code)).thenReturn(tokenResponse)
        `when`(googleOauth.assertRefreshTokenIsValid(tokenResponse)).thenCallRealMethod()

        assertThrows<GoogleAnalyticsOAuthException> {
            subject.fetchAndPersistGoogleAnalyticsAuthToken(organization, code)
        }

        verify(googleOauth, never()).revokeAuthToken(anyString())
        verify(authTokenRepo, never()).save(any<OrganizationAuthToken>())
        verify(taskUpdateService, never()).updateTasks(organization, create = false, close = true)
    }

    @Test
    fun `fetchAndPersistGoogleAnalyticsAuthToken - when Google API returns non-null access_token and null refresh_token`() {
        val organization = ObjectFactory.defaultOrganization()
        val code = "1234"
        val tokenResponse = createGaAccessTokenResponse("123", null)

        `when`(googleOauth.requestRefreshToken(organization, code)).thenReturn(tokenResponse)
        `when`(googleOauth.assertRefreshTokenIsValid(tokenResponse)).thenCallRealMethod()

        assertThrows<GoogleAnalyticsOAuthException> {
            subject.fetchAndPersistGoogleAnalyticsAuthToken(organization, code)
        }

        verify(googleOauth, times(1)).revokeAuthToken(tokenResponse.access_token!!)
        verify(authTokenRepo, never()).save(any<OrganizationAuthToken>())
        verify(taskUpdateService, never()).updateTasks(organization, create = false, close = true)
    }

    @Test
    fun `upsertGoogleAnalyticsAuthToken - when auth token does not exist`() {
        val organization = ObjectFactory.defaultOrganization()
        val tokenResponse = createGaAccessTokenResponse("123", "456")

        var saved: OrganizationAuthToken? = null

        `when`(
            authTokenRepo.findByOrganizationAndProvider(
                organization,
                OrganizationAuthTokenProvider.GOOGLE_OAUTH
            )
        ).thenReturn(null)
        `when`(authTokenRepo.save(any<OrganizationAuthToken>())).doAnswer {
            saved = it.arguments[0] as OrganizationAuthToken
            return@doAnswer saved
        }

        subject.upsertGoogleAnalyticsAuthToken(organization, tokenResponse)

        verify(authTokenRepo, times(1)).save(any<OrganizationAuthToken>())
        with(saved!!) {
            assertEquals(organization.id, this.organizationId)
            assertEquals(OrganizationAuthTokenProvider.GOOGLE_OAUTH, this.provider)
            assertEquals("123", this.auth)
            assertEquals("456", this.refresh)
            assertNotNull(this.expiresAt)
            assertNotNull(this.updatedAt)
            assertNotNull(this.createdAt)
        }
    }

    @Test
    fun `upsertGoogleAnalyticsAuthToken - when auth token exists`() {
        val organization = ObjectFactory.defaultOrganization()
        val tokenResponse = createGaAccessTokenResponse("123", "456")
        val existingAuthToken = OrganizationAuthToken(
            organization = organization,
            provider = OrganizationAuthTokenProvider.GOOGLE_OAUTH,
            auth = "stale123",
            refresh = "stale456",
            createdAt = ZonedDateTime.now().minusDays(100),
            updatedAt = ZonedDateTime.now().minusDays(100),
            expiresAt = ZonedDateTime.now().minusDays(10)
        )
        val existingCreatedAt = existingAuthToken.createdAt
        val existingUpdatedAt = existingAuthToken.updatedAt
        val existingExpiresAt = existingAuthToken.expiresAt

        var saved: OrganizationAuthToken? = null

        `when`(
            authTokenRepo.findByOrganizationAndProvider(
                organization,
                OrganizationAuthTokenProvider.GOOGLE_OAUTH
            )
        ).thenReturn(existingAuthToken)
        `when`(authTokenRepo.save(any<OrganizationAuthToken>())).doAnswer {
            saved = it.arguments[0] as OrganizationAuthToken
            return@doAnswer saved
        }

        subject.upsertGoogleAnalyticsAuthToken(organization, tokenResponse)

        verify(authTokenRepo, times(1)).save(any<OrganizationAuthToken>())
        assertEquals(existingAuthToken, saved)
        with(saved!!) {
            assertEquals("123", this.auth)
            assertEquals("456", this.refresh)
            assertNotEquals(existingExpiresAt, this.expiresAt)
            assertNotEquals(existingUpdatedAt, this.updatedAt)
            assertEquals(existingCreatedAt, this.createdAt)
        }
    }

    @Test
    fun `deleteGoogleAnalyticsAuthToken - when revoke from Google API succeeds`() {
        val organization = setupOrg()
        val authToken = setupAuthToken(organization, "123", "456")

        `when`(googleOauth.revokeAuthToken(authToken.auth!!)).thenReturn(true)

        val errorCode = subject.deleteGoogleAnalyticsAuthToken(organization.publicId)

        verify(googleOauth, times(1)).revokeAuthToken(authToken.auth!!)
        verify(authTokenRepo, times(1)).delete(authToken)
        assertNull(errorCode)
    }

    @Test
    fun `deleteGoogleAnalyticsAuthToken - when revoke from Google API fails with Google error code`() {
        val organization = setupOrg()
        val authToken = setupAuthToken(organization, "123", "456")

        `when`(googleOauth.revokeAuthToken(authToken.auth!!)).thenThrow(GoogleAnalyticsOAuthException("invalid_token"))

        val result = subject.deleteGoogleAnalyticsAuthToken(organization.publicId)

        verify(googleOauth, times(1)).revokeAuthToken(authToken.auth!!)
        verify(authTokenRepo, times(1)).delete(authToken)
        assertEquals("invalid_token", result)
    }

    @Test
    fun `deleteGoogleAnalyticsAuthToken - when revoke from Google API fails with non-Google runtime exception`() {
        val organization = setupOrg()
        val authToken = setupAuthToken(organization, "123", "456")

        `when`(googleOauth.revokeAuthToken(authToken.auth!!)).thenThrow(RuntimeException("random exception"))

        assertThrows<RuntimeException> {
            subject.deleteGoogleAnalyticsAuthToken(organization.publicId)
        }

        verify(googleOauth, times(1)).revokeAuthToken(authToken.auth!!)
        verify(authTokenRepo, times(1)).delete(authToken)
    }

    private fun setupOrg(): Organization {
        val organization = ObjectFactory.defaultOrganization()
        `when`(lookup.orgOrThrow(organization.publicId)).thenReturn(organization)
        return organization
    }

    private fun setupAuthToken(
        organization: Organization,
        auth: String? = null,
        refresh: String? = null
    ): OrganizationAuthToken {
        val authToken = OrganizationAuthToken(organization = organization, auth = auth, refresh = refresh)
        `when`(
            authTokenRepo.findByOrganizationAndProvider(
                organization,
                OrganizationAuthTokenProvider.GOOGLE_OAUTH
            )
        ).thenReturn(authToken)
        return authToken
    }

    private fun createGaAccessTokenResponse(accessToken: String? = "123", refreshToken: String? = "456") =
        GaAccessTokenResponse(
            access_token = accessToken,
            refresh_token = refreshToken,
            expires_in = DateTime.now().plus(100000).millis,
            token_type = null,
            scope = null,
            error = null,
            error_description = null
        )
}
