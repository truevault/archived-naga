package polaris.services.support.email

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import polaris.BaseIntegrationTest
import polaris.models.entity.organization.MailboxStatus
import polaris.repositories.OrganizationRepository
import util.ObjectFactory
import kotlin.test.assertEquals

class NylasServiceIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var subject: NylasService

    @Test
    fun setMailboxSuccess__updatesPrivacyCenterEmail() {
        // given
        val org = persist(ObjectFactory.defaultOrganization(name = "Org 1"))
        persist(ObjectFactory.defaultPrivacyCenter(org, requestEmail = "old@qa.truevault.com"))
        persist(
            ObjectFactory.defaultOrganizationMailbox(
                organization = org,
                status = MailboxStatus.DISCONNECTED,
                mailbox = "new@qa.truevault.com",
                nylasAccountId = "NYLAS-12345"
            )
        )

        // when
        flushAndClear()
        subject.setMailboxSuccess("NYLAS-12345")
        flushAndClear()

        // then
        val pc = organizationRepository.findByIdOrNull(org.id)!!.privacyCenters.first()
        assertEquals("new@qa.truevault.com", pc.requestEmail)
    }
}
