package polaris.services.support.email

import com.nylas.AccountDetail
import com.nylas.NylasAccount
import com.nylas.RequestFailedException
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.mockito.Mockito
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import polaris.BaseUnitTest
import polaris.models.entity.organization.MailboxStatus
import polaris.repositories.OrganizationMailboxRepository
import polaris.services.primary.PrivacyCenterService
import polaris.services.support.EmailService
import util.ObjectFactory
import java.time.ZonedDateTime
import java.util.stream.Stream
import kotlin.test.assertEquals

class NylasServiceTest : BaseUnitTest() {

    private val polarisDomain = "polaris.truevault.com"
    private val organizationMailboxRepository = Mockito.mock(OrganizationMailboxRepository::class.java)
    private val nylasClientFactory = Mockito.mock(NylasClientFactory::class.java)
    private val emailService = Mockito.mock(EmailService::class.java)
    private val privacyCenterService = Mockito.mock(PrivacyCenterService::class.java)

    private val subject = NylasService(
        polarisDomain,
        organizationMailboxRepository,
        nylasClientFactory,
        emailService,
        privacyCenterService
    )

    companion object {
        @JvmStatic
        fun updateMailboxStatusArgs(): Stream<Arguments> = Stream.of(
            Arguments.of(MailboxStatus.CONNECTED, "running", 0, NylasService.MailboxUpdateResult.REMAINS_CONNECTED),
            Arguments.of(MailboxStatus.CONNECTED, "partial", 0, NylasService.MailboxUpdateResult.REMAINS_CONNECTED),
            Arguments.of(MailboxStatus.CONNECTED, "stopped", 1, NylasService.MailboxUpdateResult.UNPLANNED_DISCONNECT),
            Arguments.of(MailboxStatus.DISCONNECTED, "running", 1, NylasService.MailboxUpdateResult.CONNECTED),
            Arguments.of(
                MailboxStatus.DISCONNECTED,
                "partial",
                0,
                NylasService.MailboxUpdateResult.REMAINS_DISCONNECTED
            ),
            Arguments.of(
                MailboxStatus.DISCONNECTED,
                "stopped",
                0,
                NylasService.MailboxUpdateResult.REMAINS_DISCONNECTED
            )
        )

        @JvmStatic
        fun updateMailboxStatusWithExceptionArgs(): Stream<Arguments> = Stream.of(
            Arguments.of(MailboxStatus.CONNECTED, 1, NylasService.MailboxUpdateResult.UNPLANNED_DISCONNECT),
            Arguments.of(MailboxStatus.DISCONNECTED, 0, NylasService.MailboxUpdateResult.REMAINS_DISCONNECTED)
        )
    }

    @ParameterizedTest(name = "updateMailboxStatus - with current local state {0} and Nylas sync state {1}")
    @MethodSource("updateMailboxStatusArgs")
    fun `updateMailboxStatus - with current local state and Nylas sync state`(
        initialMailboxStatus: MailboxStatus,
        syncState: String,
        expectedSaves: Int,
        expectedResult: NylasService.MailboxUpdateResult
    ) {
        // given
        val org = ObjectFactory.defaultOrganization()
        val orgMailbox = ObjectFactory.defaultOrganizationMailbox(organization = org, status = initialMailboxStatus)

        val accountDetail = Mockito.mock(AccountDetail::class.java)
        Mockito.`when`(accountDetail.id).thenReturn("1234")
        Mockito.`when`(accountDetail.name).thenReturn("John Doe")
        Mockito.`when`(accountDetail.emailAddress).thenReturn("johndoe@test.com")
        Mockito.`when`(accountDetail.provider).thenReturn("GMAIL")
        Mockito.`when`(accountDetail.syncState).thenReturn(syncState)
        Mockito.`when`(accountDetail.linkedAt).thenReturn(ZonedDateTime.now().toInstant().toEpochMilli())

        val nylasAccount = Mockito.mock(NylasAccount::class.java)
        Mockito.`when`(nylasAccount.fetchAccountByAccessToken()).thenReturn(accountDetail)
        Mockito.`when`(nylasClientFactory.nylasAccount(orgMailbox.nylasAccessToken)).thenReturn(nylasAccount)

        // when
        val result = subject.updateMailboxStatus(orgMailbox)

        // then
        verify(organizationMailboxRepository, times(expectedSaves)).save(orgMailbox)
        assertEquals(expectedResult, result)
    }

    @ParameterizedTest(name = "updateMailboxStatus - with current local state {0} and RequestFailedException")
    @MethodSource("updateMailboxStatusWithExceptionArgs")
    fun `updateMailboxStatus - with current local state and RequestFailedException`(
        initialMailboxStatus: MailboxStatus,
        expectedSaves: Int,
        expectedResult: NylasService.MailboxUpdateResult
    ) {
        // given
        val org = ObjectFactory.defaultOrganization()
        val orgMailbox = ObjectFactory.defaultOrganizationMailbox(organization = org, status = initialMailboxStatus)

        val nylasAccount = Mockito.mock(NylasAccount::class.java)
        Mockito.`when`(nylasAccount.fetchAccountByAccessToken()).thenThrow(RequestFailedException(400, "failed"))
        Mockito.`when`(nylasClientFactory.nylasAccount(orgMailbox.nylasAccessToken)).thenReturn(nylasAccount)

        // when
        val result = subject.updateMailboxStatus(orgMailbox)

        // then
        verify(organizationMailboxRepository, times(expectedSaves)).save(orgMailbox)
        assertEquals(expectedResult, result)
    }
}
