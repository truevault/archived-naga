package polaris.services.support.email

import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import polaris.BaseUnitTest
import java.util.stream.Stream
import kotlin.test.assertEquals
import kotlin.test.assertFalse

class PoweredByMineTest : BaseUnitTest() {

    private val subject: PoweredByMineService = PoweredByMineService()

    // //////////////////////////////
    // Non-Mine Emails
    // //////////////////////////////

    @Test
    fun `isMineEmail - nonMineEmail`() {
        assertFalse(subject.isMineEmail(NON_MINE_EMAIL))
    }

    @Test
    fun `extractMineEmailContents - nonMineEmail`() {
        val contents = subject.extractMineEmailContents(NON_MINE_EMAIL, "Jack Doe", "jackdoe@example.com")

        // should fall back to the message metadata
        assertEquals("Jack", contents.firstName)
        assertEquals("Doe", contents.lastName)
        assertEquals("jackdoe@example.com", contents.email)
    }

    // //////////////////////////////
    // Mine Emails
    // //////////////////////////////

    companion object {
        @JvmStatic
        fun parseableMineEmails(): Stream<Arguments?>? = Stream.of(
            Arguments.of(RECORDED_MINE_EMAIL),
            Arguments.of(RAW_MINE_EMAIL_LI_VARIANT),
            Arguments.of(RAW_MINE_EMAIL_BR_VARIANT_1),
            Arguments.of(RAW_MINE_EMAIL_BR_VARIANT_2),
            Arguments.of(RAW_MINE_EMAIL_BR_VARIANT_3),
            Arguments.of(RAW_MINE_EMAIL_BR_VARIANT_4),
            Arguments.of(RAW_MINE_EMAIL_BR_VARIANT_5)
        )
    }

    @ParameterizedTest
    @MethodSource("parseableMineEmails")
    fun `isMineEmail - parseable Mine email variants`(content: String) {
        assert(subject.isMineEmail(content))
    }

    @ParameterizedTest
    @MethodSource("parseableMineEmails")
    fun `extractMineEmailContents - parseable Mine email variants with non-empty name and email`(content: String) {
        val contents = subject.extractMineEmailContents(content, "Jack Doe", "jackdoe@example.com")

        // should use the name and email from the message body itself
        assertEquals("Jonathan", contents.firstName)
        assertEquals("Doe", contents.lastName)
        assertEquals("jdoe@example.com", contents.email)
    }

    @Test
    fun `extractMineEmailContents - unparseable Mine email with non-empty name and email`() {
        val contents = subject.extractMineEmailContents(RAW_MINE_EMAIL_NOT_PARSEABLE, "Jack Doe", "jackdoe@example.com")

        // should fall back to the message metadata
        assertEquals("Jack", contents.firstName)
        assertEquals("Doe", contents.lastName)
        assertEquals("jackdoe@example.com", contents.email)
    }

    @Test
    fun `extractMineEmailContents - unparseable Mine email with empty name and non-empty email`() {
        val contents = subject.extractMineEmailContents(RAW_MINE_EMAIL_NOT_PARSEABLE, "", "jackdoe@example.com")

        // should fall back to the message metadata, with email as the first name
        assertEquals("jackdoe@example.com", contents.firstName)
        assertEquals(null, contents.lastName)
        assertEquals("jackdoe@example.com", contents.email)
    }
}

const val NON_MINE_EMAIL: String = """
<div dir=3D"ltr">A totally different message<br clear=3D"all"><div><div><di=
v dir=3D"ltr" class=3D"gmail_signature" data-smartmail=3D"gmail_signature">=
<div dir=3D"ltr"><div><div dir=3D"ltr"><div><div dir=3D"ltr"><div><div dir=
=3D"ltr"><span style=3D"font-size:12.8px">=E2=80=94</span><span style=3D"fo=
nt-size:12.8px"><br></span></div><div dir=3D"ltr"><span style=3D"font-size:=
12.8px">Noah Callaway</span><div style=3D"font-size:12.8px">Director of Eng=
ineering, Apsis Labs</div><div style=3D"font-size:12.8px">Pronouns: he, him=
, his</div><div style=3D"font-size:12.8px"><a href=3D"tel:541.968.2049" sty=
le=3D"color:rgb(17,85,204)" target=3D"_blank">541.968.2049</a></div></div><=
/div></div></div></div></div></div></div></div></div></div>
"""

const val RECORDED_MINE_EMAIL: String = """
<div dir="ltr">
  <div dir="ltr">
    <p>
      Hi TrueVault,<br /><br />My name is User, I&#39;ve used your service in the past.<br />However, I&#39;m now making
      the conscious decision to reduce my digital footprint and as a result, I ask you to please delete any personal
      data of <span>mine</span> you have stored on your systems.<br /><br />I have initiated this request myself and it
      was sent from my inbox (see From address of this email).<br />Would appreciate your cooperation and an email
      confirming when the deletion has been completed.<br /><br />My details are:
    </p>
    <ul>
      <li style="margin-left: 15px">Name: Jonathan Doe<br /></li>
      <li style="margin-left: 15px">Email: <a href="mailto:jdoe@example.com" target="_blank">jdoe@example.com</a><br /></li>
    </ul>
    <br />Context: I received an email from your company on&nbsp;<b>2021 November 29</b>&nbsp;which indicates your
    systems hold my data.<br /><br />Companies: If you would like additional context and assistance to complete this
    request, please&nbsp;<a
      href="https://api.saymine.com/user-request?existing=False&amp;category=Health%20&amp;%20Wellness&amp;experiment=False&amp;actionId=4VJVDOSX&amp;utm_source=product&amp;utm_medium=email&amp;utm_campaign=dsra&amp;utm_content=geologie.com"
      target="_blank"
      >visit the secured <span>Mine</span> for Business portal.</a
    ><br /><br /><b>Thanks,<br />Noah User<br /><span>Powered</span> by <span>Mine</span>®</b>
    <div>
      <div dir="ltr">
        <div dir="ltr">
          <div>
            <div dir="ltr">
              <div>
                <div dir="ltr">
                  <div>
                    <div dir="ltr">
                      <span style="font-size: 12.8px">—</span
                      ><font color="#888888"
                        ><span style="font-size: 12.8px"><br /></span
                      ></font>
                    </div>
                    <font color="#888888"
                      ><div dir="ltr">
                        <span style="font-size: 12.8px">Jonathan Doe</span>
                        <div style="font-size: 12.8px">Director of Engineering, Apsis Labs</div>
                        <div style="font-size: 12.8px">Pronouns: he, him, his</div>
                        <div style="font-size: 12.8px">
                          <a href="tel:541.968.2049" style="color: rgb(17, 85, 204)" target="_blank">541.968.2049</a>
                        </div>
                      </div></font
                    >
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
"""

const val RAW_MINE_EMAIL_LI_VARIANT: String = """
<html lang=3D"en">
<head>
    <meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Dutf-8=
">
    <meta name=3D"viewport" content=3D"width=3Ddevice-width, initial-scale=
=3D1, minimum-scale=3D1, maximum-scale=3D1">
   =20
    <meta http-equiv=3D"X-UA-Compatible" content=3D"IE=3DEdge">
   =20
   =20
   =20
</head>
<body>
<p>
    Hi Geologie (<a href=3D"http://geologie.com">geologie.com</a>),<br>
    <br>
    My name is Jonathan Doe, I&#39;ve used your service in the past.<br>
    However, I&#39;m now making the conscious decision to reduce my digital=
 footprint and as a result
    I ask you to please delete any personal data of mine you have stored on=
 your systems.<br>
    <br>
    I have initiated this request myself and it was sent from my own person=
al inbox (see From address of this email). <br>Would appreciate your cooper=
ation and an email confirming when the deletion has been completed.<br>
    <br>
    My personal details are:
<ul>
    <li>Name: Jonathan Doe</li>
    <li>Email: <a href=3D"mailto:jdoe@example.com">jdoe@example.com</=
a></li>
</ul><br>
Context: I received an email from your company on <strong>2021 November 29<=
/strong> which indicates your systems hold my personal data.<br>
<br>
Companies: If you would like additional context and assistance to complete =
this request, please <a href=3D"https://api.saymine.com/user-request?existi=
ng=3DFalse&amp;category=3DHealth &amp; Wellness&amp;experiment=3DFalse&amp;=
actionId=3D4VJVDOSX&amp;utm_source=3Dproduct&amp;utm_medium=3Demail&amp;utm=
_campaign=3Ddsra&amp;utm_content=3Dgeologie.com">visit the secured Mine for=
 Business portal.</a><br>
<br>
<strong>
    Thanks,<br>
    Jonathan Doe<br>
    Powered by Mine=C2=AE<br>
    Mine Request: 4VJVDOSX
</strong>
</p>
</body>
</html>
"""

const val RAW_MINE_EMAIL_BR_VARIANT_1: String = """
<html class="sg-campaigns"><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    </head>
    <body style="max-width:600px;padding:15px;margin:0 auto;font-size:16px;font-family:lato,sans-serif,arial;line-height:1.5;color:#000000">
           <p>  Hi Geologie (<a href="http://geologie.com">geologie.com</a>),,
            <br>
            <br>My name is Jonathan Doe, I&#39;ve used your service in the past. 
            <br>However, I&#39;m now making the conscious decision to reduce my digital footprint and as a result I ask you to please delete any personal data of mine you have stored on your systems.
           <br>
           <br>I have initiated this request myself and it was sent from my own personal inbox (see From address of this email). 
           <br>Would appreciate your cooperation and an email confirming when the deletion has been completed.
            <br>
            <br>My personal details are: 
            <br>•	Name: Jonathan Doe
            <br>•	Email: <a href="mailto:jdoe@example.com">jdoe@example.com</a>
            <br>
            <br>Context: I received an email from your company on 2022 September 16 which indicates your systems hold my personal data.
            <br>
            <br>Companies: If you would like additional context and assistance to complete this request, please <a href="https://api.saymine.com/user-request?existing=False&amp;category=Health &amp; Wellness&amp;experiment=False&amp;actionId=KTGL5SYX&amp;utm_source=product&amp;utm_medium=email&amp;utm_campaign=dsrsg&amp;utm_content=geologie.com">visit the secured Mine for Business portal.</a>            
            <p>Thanks,
            <br>Jonathan Doe
            <br>Powered by Mine®
            <br>Mine Request: KTGL5SYX
            <br>
            </p>
        <div style="font-size:11px">
        <br>2021 Mine® All rights reserved <br> 23 Derech Begin - Tel Aviv-Yafo - 6618356 - Israel
        </div>
    </p></body>
</html>
"""

const val RAW_MINE_EMAIL_BR_VARIANT_2 = """
<div dir=3D"ltr"><br><br><div class=3D"gmail_quote"><div dir=3D"ltr" class=
=3D"gmail_attr">---------- Forwarded message ---------<br>From: <span dir=
=3D"auto">&lt;<a href=3D"mailto:jdoe@example.com" target=3D"_blank">j=
doe@example.com</a>&gt;</span><br>Date: Sat, Oct 8, 2022 at 6:16 PM<b=
r>Subject: Dadgrass data erasure request - from Jonathan Doe - request: VVJ=
MVYL4<br>To:  &lt;<a href=3D"mailto:privacy@dadgrass.com" target=3D"_blank"=
>privacy@dadgrass.com</a>&gt;<br></div><br><br>
     =20
     =20
    <div style=3D"max-width:600px;padding:15px;margin:0 auto;font-size:16px=
;font-family:lato,sans-serif,arial;line-height:1.5;color:#000000">
           <p>  Hi Dadgrass (<a href=3D"http://dadgrass.com" target=3D"_bla=
nk">dadgrass.com</a>),,
            <br>
            <br>My name is Jonathan Doe, I&#39;ve used your service in the =
past.=20
            <br>However, I&#39;m now making the conscious decision to reduc=
e my digital footprint and as a result I ask you to please delete any perso=
nal data of mine you have stored on your systems.
           <br>
           <br>I have initiated this request myself and it was sent from my=
 own personal inbox (see From address of this email).=20
           <br>Would appreciate your cooperation and an email confirming wh=
en the deletion has been completed.

            <br>
            <br>My personal details are:=20
            <br>=E2=80=A2	Name: Jonathan Doe
            <br>=E2=80=A2	Email: <a href=3D"mailto:jdoe@example.com" =
target=3D"_blank">jdoe@example.com</a>
            <br>
            <br>Context: I received an email from your company on 2022 Apri=
l 23 which indicates your systems hold my personal data.
            <br>
            <br>Companies: If you would like additional context and assista=
nce to complete this request, please visit the secured Mine for Business po=
rtal.
           =20
            </p><p>Thanks,
            <br>Jonathan Doe
            <br>Powered by Mine=C2=AE
            <br>Mine Request: VVJMVYL4
            <br>
            </p>
        <div style=3D"font-size:11px">
        <br>2021 Mine=C2=AE All rights reserved <br> 23 Derech Begin - Tel =
Aviv-Yafo - 6618356 - Israel
        </div>
    <p></p></div>

</div></div>
"""

const val RAW_MINE_EMAIL_BR_VARIANT_3 = """
<div dir=3D"ltr"><p style=3D"color:rgb(0,0,0);font-family:lato,sans-serif,a=
rial;font-size:16px;font-style:normal;font-variant-caps:normal;font-weight:=
400;letter-spacing:normal;text-align:start;text-indent:0px;text-transform:n=
one;white-space:normal;word-spacing:0px;text-decoration:none">Hi Soma (<a h=
ref=3D"http://soma.store.truevaultstaging.com" target=3D"_blank">soma.store=
.truevaultstaging.com</a><a href=3D"http://dadgrass.com/" target=3D"_blank"=
></a>),,<span>=C2=A0</span><br><br>My name is Jonathan Doe, I&#39;ve used your=
 service in the past.<span>=C2=A0</span><br>However,
 I&#39;m now making the conscious decision to reduce my digital footprint=
=20
and as a result I ask you to please delete any personal data of mine you
 have stored on your systems.<span>=C2=A0</span><br><br>I have initiated th=
is request myself and it was sent from my own personal inbox (see From addr=
ess of this email).<span>=C2=A0</span><br>Would appreciate your cooperation=
 and an email confirming when the deletion has been completed.<span>=C2=A0<=
/span><br><br>My personal details are:<span>=C2=A0</span><br>=E2=80=A2 Name=
: Jonathan Doe<span> </span><br>=E2=80=A2 Email:<span>=C2=A0</span><a href=3D"=
mailto:jdoe@example.com" target=3D"_blank">jdoe@example.co=
m</a><span>=C2=A0</span><br><br>Context: I received an email from your comp=
any on 2022 September 03 which indicates your systems hold my personal data=
.<span>=C2=A0</span><br><br>Companies:
 If you would like additional context and assistance to complete this=20
request, please visit the secured Mine for Business portal.</p><p style=3D"=
color:rgb(0,0,0);font-family:lato,sans-serif,arial;font-size:16px;font-styl=
e:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;tex=
t-align:start;text-indent:0px;text-transform:none;white-space:normal;word-s=
pacing:0px;text-decoration:none">Thanks,<span>=C2=A0</span><br>Jonathan Doe<spa=
n> </span><br>Powered by Mine=C2=AE<span>=C2=A0</span><br>Mine Request: JEV=
2ZZZ1<span> </span></p></div>
"""

const val RAW_MINE_EMAIL_BR_VARIANT_4 = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--<![endif]--><!--[if (gte mso 9)|(IE)]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]--><!--[if (gte mso 9)|(IE)]>
  <style type="text/css">
    body {width: 600px;margin: 0 auto;}
    table {border-collapse: collapse;}
    table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
    img {-ms-interpolation-mode: bicubic;}
  </style>
<![endif]-->
</head>
<body style="max-width: 600px; padding: 15px; margin:0 auto; font-size:16px; font-family: lato, sans-serif, arial; line-height: 1.5; color: #000000;">
<p>Hi Polywood (polywood.com),, <br>
<br>
My name is Jonathan Doe, I've used your service in the past. <br>
However, I'm now making the conscious decision to reduce my digital footprint and as a result I ask you to please delete any personal data of mine you have stored on your systems.
<br>
<br>
I have initiated this request myself and it was sent from my own personal inbox (see From address of this email).
<br>
Would appreciate your cooperation and an email confirming when the deletion has been completed.
<br>
<br>
My personal details are: <br>
• Name: Jonathan Doe <br>
• Email: jdoe@example.com <br>
<br>
Context: I received an email from your company on 2022 August 25 which indicates your systems hold my personal data.
<br>
<br>
Companies: If you would like additional context and assistance to complete this request, please visit the secured Mine for Business portal.
</p>
<p>Thanks, <br>
Jonathan Doe <br>
Powered by Mine® <br>
Mine Request: 4ZU6JKKW <br>
</p>
<div style="font-size:11px"><br>
2021 Mine® All rights reserved <br>
23 Derech Begin - Tel Aviv-Yafo - 6618356 - Israel </div>
</body>
</html>
"""

const val RAW_MINE_EMAIL_BR_VARIANT_5 = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml"><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
      <!--[if !mso]><!-->
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <!--<![endif]-->
      <!--[if (gte mso 9)|(IE)]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if (gte mso 9)|(IE)]>
  <style type="text/css">
    body {width: 600px;margin: 0 auto;}
    table {border-collapse: collapse;}
    table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
    img {-ms-interpolation-mode: bicubic;}
  </style>
<![endif]-->
    </head>
    <body style="max-width: 600px; padding: 15px; margin:0 auto; font-size:16px; font-family: lato, sans-serif, arial; line-height: 1.5; color: #000000;">
           <p>  Hi Geologie (geologie.com),,
            <br>
            <br>My name is Jonathan Doe, I've used your service in the past. 
            <br>However, I'm now making the conscious decision to reduce my digital footprint and as a result I ask you to please delete any personal data of mine you have stored on your systems.
           <br>
           <br>I have initiated this request myself and it was sent from my own personal inbox (see From address of this email). 
           <br>Would appreciate your cooperation and an email confirming when the deletion has been completed.

            <br>
            <br>My personal details are: 
            <br>•	Name: Jonathan Doe
            <br>•	Email: jdoe@example.com
            <br>
            <br>Context: I received an email from your company on 2022 September 16 which indicates your systems hold my personal data.
            <br>
            <br>Companies: If you would like additional context and assistance to complete this request, please <a href="https://api.saymine.com/user-request?existing=False&amp;category=Health &amp; Wellness&amp;experiment=False&amp;actionId=QRDAXVAI&amp;utm_source=product&amp;utm_medium=email&amp;utm_campaign=dsrsg&amp;utm_content=geologie.com">visit the secured Mine for Business portal.</a>            
            <p>Thanks,
            <br>Jonathan Doe
            <br>Powered by Mine®
            <br>Mine Request: QRDAXVAI
            <br>
            </p>
        <div style="font-size:11px">
        <br/>2021 Mine® All rights reserved <br> 23 Derech Begin - Tel Aviv-Yafo - 6618356 - Israel
        </div>
    </body>
</html>
"""

const val RAW_MINE_EMAIL_NOT_PARSEABLE: String = """
<html class="sg-campaigns"><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    </head>
    <body style="max-width:600px;padding:15px;margin:0 auto;font-size:16px;font-family:lato,sans-serif,arial;line-height:1.5;color:#000000">
           <p>  Hi Geologie (<a href="http://geologie.com">geologie.com</a>),,
            <br>
            <br>My name is Jonathan Doe, I&#39;ve used your service in the past. 
            <br>However, I&#39;m now making the conscious decision to reduce my digital footprint and as a result I ask you to please delete any personal data of mine you have stored on your systems.
           <br>
           <br>I have initiated this request myself and it was sent from my own personal inbox (see From address of this email). 
           <br>Would appreciate your cooperation and an email confirming when the deletion has been completed.
            <br>
            <br>My personal details are: 
            <br>•	XXXNameXXX: Jonathan Doe
            <br>•	XXXEmailXXX: <a href="mailto:jdoe@example.com">jdoe@example.com</a>
            <br>
            <br>Context: I received an email from your company on 2022 September 16 which indicates your systems hold my personal data.
            <br>
            <br>Companies: If you would like additional context and assistance to complete this request, please <a href="https://api.saymine.com/user-request?existing=False&amp;category=Health &amp; Wellness&amp;experiment=False&amp;actionId=KTGL5SYX&amp;utm_source=product&amp;utm_medium=email&amp;utm_campaign=dsrsg&amp;utm_content=geologie.com">visit the secured Mine for Business portal.</a>            
            <p>Thanks,
            <br>Jonathan Doe
            <br>Powered by Mine®
            <br>Mine Request: KTGL5SYX
            <br>
            </p>
        <div style="font-size:11px">
        <br>2021 Mine® All rights reserved <br> 23 Derech Begin - Tel Aviv-Yafo - 6618356 - Israel
        </div>
    </p></body>
</html>
"""
