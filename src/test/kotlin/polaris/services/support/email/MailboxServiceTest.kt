package polaris.services.support.email

import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyString
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import polaris.BaseUnitTest
import polaris.controllers.PrivacyInboxInUseException
import polaris.controllers.util.LookupService
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.MailboxType
import polaris.models.entity.user.User
import polaris.repositories.OrganizationMailboxRepository
import polaris.services.primary.PrivacyCenterService
import polaris.services.primary.UserService
import polaris.services.support.EmailService
import util.ObjectFactory
import kotlin.test.assertFailsWith

class MailboxServiceTest : BaseUnitTest() {

    @Mock
    private lateinit var nylasService: NylasService

    @Mock
    private lateinit var organizationMailboxRepository: OrganizationMailboxRepository

    @Mock
    private lateinit var privacyCenterService: PrivacyCenterService

    @Mock
    private lateinit var userService: UserService

    @Mock
    private lateinit var emailService: EmailService

    @Mock
    private lateinit var lookupService: LookupService

    @InjectMocks
    private lateinit var subject: MailboxService

    @Test
    fun `createMailbox - connect mailbox (initial)`() {
        // given
        val organization = ObjectFactory.defaultOrganization()
        val mailboxDetails = createMailboxDetails()

        // stubs
        `when`(lookupService.orgOrThrow(organization.id)).thenReturn(organization)
        `when`(privacyCenterService.findByEmailAddress(mailboxDetails.mailbox)).thenReturn(null)
        `when`(userService.findByEmail(mailboxDetails.mailbox)).thenReturn(null)
        `when`(organizationMailboxRepository.findByNylasAccountId(mailboxDetails.nylasAccountId)).thenReturn(null)

        // when
        subject.createMailbox(organization.id, mailboxDetails)

        // then
        verify(privacyCenterService, times(1)).findByEmailAddress(mailboxDetails.mailbox)
        verify(userService, times(1)).findByEmail(mailboxDetails.mailbox)
        verify(organizationMailboxRepository, times(0)).delete(any())
        verify(organizationMailboxRepository, times(1)).save(any())
    }

    @ParameterizedTest(name = "createMailbox - reconnect mailbox with status {0} (same org)")
    @ValueSource(strings = ["CONNECTED", "DISCONNECTED"])
    fun `createMailbox - reconnect mailbox (same org)`(
        mailboxStatus: MailboxStatus
    ) {
        // given
        val organization = ObjectFactory.defaultOrganization()
        val privacyCenter = ObjectFactory.defaultPrivacyCenter(org = organization)
        val organizationMailbox =
            ObjectFactory.defaultOrganizationMailbox(organization = organization, status = mailboxStatus)
        val mailboxDetails = createMailboxDetails()

        // stubs
        `when`(lookupService.orgOrThrow(organization.id)).thenReturn(organization)
        `when`(privacyCenterService.findByEmailAddress(mailboxDetails.mailbox)).thenReturn(privacyCenter)
        `when`(userService.findByEmail(mailboxDetails.mailbox)).thenReturn(null)
        `when`(organizationMailboxRepository.findByNylasAccountId(mailboxDetails.nylasAccountId)).thenReturn(
            organizationMailbox
        )

        // when
        subject.createMailbox(organization.id, mailboxDetails)

        // then
        verify(privacyCenterService, times(1)).findByEmailAddress(mailboxDetails.mailbox)
        verify(userService, times(1)).findByEmail(mailboxDetails.mailbox)
        verify(organizationMailboxRepository, times(1)).delete(any())
        verify(organizationMailboxRepository, times(1)).save(any())
    }

    @Test
    fun `createMailbox - connect mailbox with email in use by another org`() {
        // given
        val organization = ObjectFactory.defaultOrganization()
        val organization2 = ObjectFactory.defaultOrganization(name = "Other Org")
        val privacyCenter2 = ObjectFactory.defaultPrivacyCenter(org = organization2)
        val mailboxDetails = createMailboxDetails()

        // stubs
        `when`(lookupService.orgOrThrow(organization.id)).thenReturn(organization)
        `when`(privacyCenterService.findByEmailAddress(mailboxDetails.mailbox)).thenReturn(privacyCenter2)

        // when
        assertFailsWith<PrivacyInboxInUseException> {
            subject.createMailbox(organization.id, mailboxDetails)
        }

        // then
        verify(privacyCenterService, times(1)).findByEmailAddress(mailboxDetails.mailbox)
        verify(userService, times(0)).findByEmail(anyString())
        verify(organizationMailboxRepository, times(0)).delete(any())
        verify(organizationMailboxRepository, times(0)).save(any())
    }

    @Test
    fun `createMailbox - connect mailbox with email in use by another user`() {
        // given
        val organization = ObjectFactory.defaultOrganization()
        val user2 = User()
        val mailboxDetails = createMailboxDetails()

        // stubs
        `when`(lookupService.orgOrThrow(organization.id)).thenReturn(organization)
        `when`(privacyCenterService.findByEmailAddress(mailboxDetails.mailbox)).thenReturn(null)
        `when`(userService.findByEmail(mailboxDetails.mailbox)).thenReturn(user2)

        // when
        assertFailsWith<PrivacyInboxInUseException> {
            subject.createMailbox(organization.id, mailboxDetails)
        }

        // then
        verify(privacyCenterService, times(1)).findByEmailAddress(mailboxDetails.mailbox)
        verify(userService, times(1)).findByEmail(mailboxDetails.mailbox)
        verify(organizationMailboxRepository, times(0)).delete(any())
        verify(organizationMailboxRepository, times(0)).save(any())
    }

    fun createMailboxDetails(): MailboxDetails {
        return MailboxDetails(
            MailboxType.EXCHANGE, "test@qa.truevault.com", "myaccesstoken", "myaccountid"
        )
    }
}
