package polaris.util

import org.junit.Test
import polaris.util.states.*
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class StateMachineTest {

    enum class TestStates { START, FIRST, SECOND, THIRD, END, FAILURE }

    @Test
    fun stateMachine_startsWithInitialState() {
        val def = StateMachine.define<Int, TestStates> { initial(TestStates.START) }
        val sm = StateMachine(0, null, def)

        assertEquals(TestStates.START, sm.current)
    }

    @Test
    fun transition_toDefaultNextState() {
        val sm = StateMachine(0, null, standard())

        assertTrue(sm.canTransition())
        sm.transition()
        assertEquals(TestStates.FIRST, sm.current)

        assertTrue(sm.canTransition())
        sm.transition()
        assertEquals(TestStates.SECOND, sm.current)

        assertTrue(sm.canTransition())
        sm.transition()
        assertEquals(TestStates.THIRD, sm.current)

        assertTrue(sm.canTransition())
        sm.transition()
        assertEquals(TestStates.END, sm.current)

        assertFalse(sm.canTransition())
    }

    @Test
    fun transition_fromAnyState() {
        val sm = StateMachine(0, null, standard())

        assertTrue(sm.canTransition())
        sm.transition()
        assertEquals(TestStates.FIRST, sm.current)

        assertTrue(sm.canTransition(TestStates.FAILURE))
        sm.transition(TestStates.FAILURE)
        assertEquals(TestStates.FAILURE, sm.current)

        assertFalse(sm.canTransition())
    }

    @Test
    fun transition_toDefaultNextStepsFindsAvailable() {
        var sm = StateMachine(2, null, branching())

        assertTrue(sm.canTransition())
        sm.transition()
        assertEquals(TestStates.FIRST, sm.current)

        sm = StateMachine(7, null, branching())

        assertTrue(sm.canTransition())
        sm.transition()
        assertEquals(TestStates.SECOND, sm.current)

        sm = StateMachine(4, null, branching())
        assertFalse(sm.canTransition())
    }

    @Test
    fun transition_toSpecificState() {
        val sm = StateMachine(0, null, standard())

        sm.transition(TestStates.FIRST)
        assertEquals(TestStates.FIRST, sm.current)

        sm.transition(TestStates.SECOND)
        assertEquals(TestStates.SECOND, sm.current)

        sm.transition(TestStates.THIRD)
        assertEquals(TestStates.THIRD, sm.current)
    }

    @Test
    fun canTransition_toSpecificState() {
        val sm = StateMachine(0, null, standard())
        assertTrue(sm.canTransition(TestStates.FIRST))
        assertFalse(sm.canTransition(TestStates.SECOND))

        sm.transition(TestStates.FIRST)
        assertTrue(sm.canTransition(TestStates.SECOND))
        assertFalse(sm.canTransition(TestStates.FIRST))
        assertFalse(sm.canTransition(TestStates.THIRD))
    }

    @Test
    fun transition_tracksHistory() {
        val sm = StateMachine(0, null, standard())

        assertEquals(emptyList(), sm.state.history)

        sm.transition(TestStates.FIRST)
        assertEquals(listOf(TestStates.START), sm.state.history)

        sm.transition(TestStates.SECOND)
        assertEquals(listOf(TestStates.START, TestStates.FIRST), sm.state.history)

        sm.transition(TestStates.THIRD)
        assertEquals(listOf(TestStates.START, TestStates.FIRST, TestStates.SECOND), sm.state.history)
    }

    @Test
    fun stateMachine_canLoadFromState() {
        val sm = StateMachine(0, null, standard())

        sm.transition(TestStates.FIRST)
        assertEquals(TestStates.FIRST, sm.current)

        val sm2 = StateMachine(0, sm.state, standard())

        sm2.transition(TestStates.SECOND)
        assertEquals(TestStates.SECOND, sm2.current)

        sm2.transition(TestStates.THIRD)
        assertEquals(TestStates.THIRD, sm2.current)
    }

    @Test(expected = NoTransitionException::class)
    fun transition_notToInvalid() {
        val sm = StateMachine(0, null, standard())

        sm.transition(TestStates.SECOND)
    }

    @Test(expected = TransitionGuardFailedException::class)
    fun transition_guardsPrevent() {
        val sm = StateMachine(
            3, null,
            standard().extend {
                guard(TestStates.START, TestStates.FIRST) { it -> GuardResult(it > 5, "It must be greater than 5") }
            }
        )

        sm.transition(TestStates.FIRST)
    }

    @Test
    fun transition_guardsAllow() {
        val sm = StateMachine(
            7, null,
            standard().extend {
                guard(TestStates.START, TestStates.FIRST) { it -> GuardResult(it > 5, "It must be greater than 5") }
            }
        )

        sm.transition(TestStates.FIRST)
        assertEquals(TestStates.FIRST, sm.current)
    }

    @Test
    fun define_setsInitialState() {
        val def = StateMachine.define<Unit, TestStates> {
            initial(TestStates.START)
        }

        assertEquals(TestStates.START, def.initial)
    }

    @Test(expected = NoInitialStateException::class)
    fun define_validatesInitialState() {
        StateMachine.define<Unit, TestStates> {
            // initial() is omitted
        }
    }

    private fun standard() = StateMachine.define<Int, TestStates> {
        initial(TestStates.START)

        transition(TestStates.START, TestStates.FIRST)
        transition(TestStates.FIRST, TestStates.SECOND)
        transition(TestStates.SECOND, TestStates.THIRD)
        transition(TestStates.THIRD, TestStates.END)

        fromAny(TestStates.FAILURE)
    }

    private fun branching() = StateMachine.define<Int, TestStates> {
        initial(TestStates.START)

        transition(TestStates.START, TestStates.FIRST)
        transition(TestStates.START, TestStates.SECOND)

        transition(TestStates.FIRST, TestStates.END)
        transition(TestStates.SECOND, TestStates.END)

        guard(TestStates.START, TestStates.FIRST) { GuardResult(it < 3, "Context was greater than three") }
        guard(TestStates.START, TestStates.SECOND) { GuardResult(it > 5, "Context was less than five") }
    }
}
