package polaris.util

import org.junit.Test
import kotlin.test.assertEquals

internal class EmailParserTest {

    @Test
    fun parsesMessage() {
        assertEquals(expected, EmailParser.parseMessage(sampleReplyMessage))
    }

    @Test
    fun parsesDoubleReplyMessage() {
        assertEquals(expected, EmailParser.parseMessage(sampleDoubleReplyMessage))
    }

    @Test
    fun parsesCleanMessage() {
        assertEquals(expected, EmailParser.parseMessage(expected))
    }

    @Test
    fun removesTrailingBrs() {
        assertEquals(expected, EmailParser.parseMessage(sampleTrailingBrs))
    }

    @Test
    fun removesLeadingBrs() {
        assertEquals(expected, EmailParser.parseMessage(sampleLeadingBrs))
    }

    private val expected = "Reply with signature<br /><br />-- <br />Eric GreerSenior Software Engineer<i>Apsis Labs</i><i><a href=\"http://www.apsis.io\" rel=\"nofollow\">www.apsis.io</a></i><i><br /></i>"
    private var sampleReplyMessage = """
Reply with signature<br /><br />-- <br />Eric GreerSenior Software Engineer<i>Apsis Labs</i><i><a href="http://www.apsis.io" rel="nofollow">www.apsis.io</a></i><i><br /></i><br /><br />On Tue, Mar 3, 2020 at 9:21 AM &lt;<a href="mailto:privacy&#64;truevaultstaging.com" rel="nofollow">privacy&#64;truevaultstaging.com</a>&gt; wrote:<br /><u></u>







    <br /><br />
    Lets send a message to Eric!



    © 2019 TrueVault. All rights reserved.<br />
    <br />
    TrueVault, Inc.<br />
    2261 Market Street #4949<br />
    San Francisco, CA 94114<br />
    USA
"""

    private var sampleDoubleReplyMessage = """
Reply with signature<br /><br />-- <br />Eric GreerSenior Software Engineer<i>Apsis Labs</i><i><a href="http://www.apsis.io" rel="nofollow">www.apsis.io</a></i><i><br /></i><br /><br />On Tue, Mar 3, 2020 at 9:22 AM Eric Greer &lt;<a href="mailto:eric&#64;apsis.io" rel="nofollow">eric&#64;apsis.io</a>&gt; wrote:<br />I don&#39;t know about that message.<br /><br />This is a reply with a signatue<br /><br />-- <br />Eric GreerSenior Software Engineer<i>Apsis Labs</i><i><a href="http://www.apsis.io" rel="nofollow">www.apsis.io</a></i><i><br /></i><br /><br />On Tue, Mar 3, 2020 at 9:21 AM &lt;<a href="mailto:privacy&#64;truevaultstaging.com" rel="nofollow">privacy&#64;truevaultstaging.com</a>&gt; wrote:<br /><u></u>







    <br /><br />
    Lets send a message to Eric!



    © 2019 TrueVault. All rights reserved.<br />
    <br />
    TrueVault, Inc.<br />
    2261 Market Street #4949<br />
    San Francisco, CA 94114<br />
    USA
"""

    private val sampleTrailingBrs = "$expected<br><br /> <br >      <br />"
    private val sampleLeadingBrs = "<br><br /> <br >      <br />  $expected"
}
