package polaris.util

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager

@Service
@Transactional
class PersistenceUtils {

    @Autowired
    private lateinit var entityManager: EntityManager

    // ///////////////////////////////////////////////
    // PERSISTENCE HELPERS
    //
    // Use these helpers for setting up test data
    // and simulating transaction boundaries to ensure
    // your methods under test are actually hitting the DB
    // rather than the first-level cache.
    // ///////////////////////////////////////////////

    fun <T> persist(entity: T): T {
        entityManager.persist(entity)
        return entity
    }

    fun <T> persistAndFlush(entity: T): T {
        persist(entity)
        entityManager.flush()
        return entity
    }

    /**
     * Persists and flushes this entity inside a separate transaction.
     * Use this for setting up test data for use with Async threads.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun <T> persistAndFlushWithNewTx(entity: T): T {
        entityManager.persist(entity)
        entityManager.flush()
        return entity
    }

    fun <T> persistAll(entities: List<T>): List<T> {
        entities.forEach {
            entityManager.persist(it)
        }
        return entities
    }

    fun <T> persistAllAndFlush(entities: List<T>): List<T> {
        persistAll(entities)
        entityManager.flush()
        return entities
    }

    fun clear() {
        entityManager.clear()
    }

    /**
     * Flush all changes to the DB and clear the persistence context of all entities.
     *
     * This should be called AFTER the test setup and BEFORE the method under test is invoked to
     * ensure that all subsequent queries make actual calls to the DB rather than using the
     * cached entities in the persistence context.
     */
    fun flushAndClear() {
        entityManager.flush()
        entityManager.clear()
    }

    fun <T> clean(entity: T) {
        entityManager.remove(entity)
    }
    fun <T> cleanAll(entities: List<T>) {
        entities.forEach {
            clean(it)
        }
    }
}
