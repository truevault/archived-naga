package polaris.util.survey.interceptors

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.controllers.util.LookupService
import polaris.models.entity.compliance.PICSurveyClassification
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.Organization
import polaris.repositories.CollectionGroupCollectedPICRepo
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.services.primary.SurveyService
import polaris.services.support.DataMapService
import util.ObjectFactory

const val DUMMY_QUESTION_TEXT = "Uncommon Question Text"
const val QUESTION_SLUG = "uncommon-collection"

class UncommonCollectionSurveyInterceptorIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository
    @Autowired
    private lateinit var collectedPICRepo: CollectionGroupCollectedPICRepo
    @Autowired
    private lateinit var dataMapService: DataMapService
    @Autowired
    private lateinit var lookup: LookupService
    @Autowired
    private lateinit var surveyService: SurveyService

    private lateinit var org: Organization
    private lateinit var cg: CollectionGroup
    private lateinit var subject: UncommonCollectionSurveyInterceptor

    @BeforeEach
    fun setup() {
        org = persistAndFlush(ObjectFactory.defaultOrganization())
        cg = persistAndFlush(ObjectFactory.defaultCollectionGroup(org = org))
        subject = UncommonCollectionSurveyInterceptor(surveyService, organizationSurveyQuestionRepository, dataMapService, collectedPICRepo, lookup)
    }

    @Test()
    fun `getAnswer - changes null response to true for uncommon collection pic`() {
        addUncommonCollection(cg)

        val answer = subject.getAnswer(org, surveyFor(cg), QUESTION_SLUG)
        assertEquals("true", answer)
    }

    @Test()
    fun `getAnswer - changes false response to true for uncommon collection pic`() {
        addUncommonCollection(cg)

        subject.write(org, "false", DUMMY_QUESTION_TEXT, surveyFor(cg), QUESTION_SLUG)
        val answer = subject.getAnswer(org, surveyFor(cg), QUESTION_SLUG)
        assertEquals("true", answer)
    }

    @Test()
    fun `getAnswer - does not modify true response`() {
        addUncommonCollection(cg)

        subject.write(org, "true", DUMMY_QUESTION_TEXT, surveyFor(cg), QUESTION_SLUG)
        val answer = subject.getAnswer(org, surveyFor(cg), QUESTION_SLUG)
        assertEquals("true", answer)
    }

    @Test()
    fun `getAnswer - does not modify false response if no uncommon collection pic`() {
        subject.write(org, "false", DUMMY_QUESTION_TEXT, surveyFor(cg), QUESTION_SLUG)
        val answer = subject.getAnswer(org, surveyFor(cg), QUESTION_SLUG)
        assertEquals("false", answer)
    }

    @Test()
    fun `getAnswer - does not modify null response if no uncommon collection pic`() {
        val answer = subject.getAnswer(org, surveyFor(cg), QUESTION_SLUG)
        assertEquals(null, answer)
    }

    private fun surveyFor(cg: CollectionGroup): String = "consumer-data-collection-${cg.id}"

    private fun addUncommonCollection(cg: CollectionGroup) {
        val picGroup = persistAndFlush(ObjectFactory.defaultPICGroup(groupKey = "rare-things"))
        val pic = persistAndFlush(ObjectFactory.defaultPersonalInformationCategory(name = "Uncommon", key = "uncommon", group = picGroup))
        assertEquals(PICSurveyClassification.UNCOMMON, pic.getClassification())

        persistAndFlush(ObjectFactory.collectedPICToCollectionGroup(cg, pic))
    }
}
