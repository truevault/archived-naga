package polaris.util.survey.interceptors

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.models.SurveySlugs.Companion.CUSTOM_AUDIENCE_QUESTION_SLUG
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.SELLING_AND_SHARING_SURVEY_NAME
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.ProcessingMethod
import polaris.models.entity.vendor.KnownVendorCategories
import polaris.models.entity.vendor.Vendor
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.RequestHandlingInstructionsRepository
import polaris.repositories.VendorClassificationRepository
import polaris.services.primary.OrganizationVendorSurveyService
import polaris.services.primary.RequestHandlingInstructionsService
import polaris.services.primary.ServiceProviderRecommendationService
import polaris.services.primary.SurveyService
import util.ObjectFactory
import javax.transaction.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

@Transactional
internal class SellingAndSharingSurveyInterceptorIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var orgRepo: OrganizationRepository

    @Autowired
    private lateinit var requestHandlingInstructionsRepository: RequestHandlingInstructionsRepository

    @Autowired
    private lateinit var organizationSurveyQuestionRepository: OrganizationSurveyQuestionRepository

    @Autowired
    private lateinit var organizationDataRecipientRepository: OrganizationDataRecipientRepository

    @Autowired
    private lateinit var requestHandlingInstructionsService: RequestHandlingInstructionsService

    @Autowired
    private lateinit var vendorClassificationRepository: VendorClassificationRepository

    @Autowired
    private lateinit var serviceProviderRecommendationService: ServiceProviderRecommendationService

    @Autowired
    private lateinit var organizationVendorSurveyService: OrganizationVendorSurveyService

    @Autowired
    private lateinit var surveyService: SurveyService

    private lateinit var sellingAndSharingSurveyInterceptor: SellingAndSharingSurveyInterceptor

    private lateinit var org: Organization
    private lateinit var googleAds: Vendor
    private lateinit var facebookAds: Vendor
    private lateinit var orgGoogleAds: OrganizationDataRecipient
    private lateinit var orgFacebookAds: OrganizationDataRecipient

    @BeforeEach
    fun setup() {
        sellingAndSharingSurveyInterceptor = SellingAndSharingSurveyInterceptor(surveyService, organizationSurveyQuestionRepository, requestHandlingInstructionsService, organizationDataRecipientRepository, organizationVendorSurveyService, vendorClassificationRepository, serviceProviderRecommendationService)

        org = persistAndFlush(ObjectFactory.defaultOrganization())

        googleAds = persistAndFlush(ObjectFactory.defaultVendor(name = "Google Ads", recipientCategory = KnownVendorCategories.AD_NETWORK.category))
        facebookAds = persistAndFlush(ObjectFactory.defaultVendor(name = "Facebook Ads", recipientCategory = KnownVendorCategories.AD_NETWORK.category))

        orgGoogleAds = persistAndFlush(ObjectFactory.defaultDataRecipient(organization = org, vendor = googleAds))
        orgFacebookAds = persistAndFlush(ObjectFactory.defaultDataRecipient(organization = org, vendor = facebookAds))
    }

    @Test
    fun write__setsProcessingMethodForUnselectedAdNetworks2() {
        orgRepo.refresh(org)

        var googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        var facebookAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgFacebookAds)

        assertEquals(null, googleAdsInstructions)
        assertEquals(null, facebookAdsInstructions)

        // Answer selecting Google Ads; expect Facebook Ads to be inaccessible
        var answers = "[\"${orgGoogleAds.id}\"]"

        sellingAndSharingSurveyInterceptor.write(org, answers, CUSTOM_AUDIENCE_QUESTION_SLUG, SELLING_AND_SHARING_SURVEY_NAME, CUSTOM_AUDIENCE_QUESTION_SLUG)

        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        facebookAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgFacebookAds)

        assertNotNull(googleAdsInstructions)
        assertNotNull(facebookAdsInstructions)
        assertEquals(ProcessingMethod.INACCESSIBLE_OR_NOT_STORED, facebookAdsInstructions.processingMethod)
        assertEquals(ProcessingMethod.DELETE, googleAdsInstructions.processingMethod)

        // Answer selecting Facebook Ads; expect Google Ads to be inaccessible
        answers = "[\"${orgFacebookAds.id}\"]"

        sellingAndSharingSurveyInterceptor.write(org, answers, CUSTOM_AUDIENCE_QUESTION_SLUG, SELLING_AND_SHARING_SURVEY_NAME, CUSTOM_AUDIENCE_QUESTION_SLUG)

        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        facebookAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgFacebookAds)

        assertNotNull(googleAdsInstructions)
        assertNotNull(facebookAdsInstructions)
        assertEquals(ProcessingMethod.INACCESSIBLE_OR_NOT_STORED, googleAdsInstructions.processingMethod)
        assertEquals(ProcessingMethod.DELETE, facebookAdsInstructions.processingMethod)

        // Answer selecting none; expect everything to be inaccessible
        answers = "[\"none\"]"

        sellingAndSharingSurveyInterceptor.write(org, answers, CUSTOM_AUDIENCE_QUESTION_SLUG, SELLING_AND_SHARING_SURVEY_NAME, CUSTOM_AUDIENCE_QUESTION_SLUG)

        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        facebookAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgFacebookAds)

        assertEquals(ProcessingMethod.INACCESSIBLE_OR_NOT_STORED, googleAdsInstructions?.processingMethod)
        assertEquals(ProcessingMethod.INACCESSIBLE_OR_NOT_STORED, facebookAdsInstructions?.processingMethod)
    }

    @Test
    fun write__setsCorrectProcessingMethodWhenWeirdSelectionMade() {
        orgRepo.refresh(org)

        // Answer is invalid with both "none" and non-"none" answers; expect "none" to be removed from answer and therefore Facebook set to DELETE
        val answers = "[\"none\", \"${orgFacebookAds.id}\"]"

        sellingAndSharingSurveyInterceptor.write(org, answers, CUSTOM_AUDIENCE_QUESTION_SLUG, SELLING_AND_SHARING_SURVEY_NAME, CUSTOM_AUDIENCE_QUESTION_SLUG)

        val googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        val facebookAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgFacebookAds)

        assertEquals(ProcessingMethod.INACCESSIBLE_OR_NOT_STORED, googleAdsInstructions?.processingMethod)
        assertEquals(ProcessingMethod.DELETE, facebookAdsInstructions?.processingMethod)
    }

    @Test
    fun write__doesNotDiscardExistingAdNetworkSettings() {

        val existingGoogleAdsInstructions = persistAndFlush(
            ObjectFactory.requestHandlingInstruction(
                org, orgGoogleAds,
                DataRequestInstructionType.DELETE, ProcessingMethod.RETAIN, mutableListOf("reasons"), "These are my custom notes"
            )
        )

        orgRepo.refresh(org)

        var googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        var facebookAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgFacebookAds)

        assertNull(facebookAdsInstructions)
        assertNotNull(googleAdsInstructions)
        assertEquals(existingGoogleAdsInstructions.id, googleAdsInstructions.id)
        assertEquals(ProcessingMethod.RETAIN, googleAdsInstructions.processingMethod)
        assertEquals("These are my custom notes", googleAdsInstructions.processingInstructions)

        // Select Google Ads
        val answers = "[\"${orgGoogleAds.id}\"]"
        sellingAndSharingSurveyInterceptor.write(org, answers, CUSTOM_AUDIENCE_QUESTION_SLUG, SELLING_AND_SHARING_SURVEY_NAME, CUSTOM_AUDIENCE_QUESTION_SLUG)

        // Assert that Google Ads (which was selected) has changed processing methods, but not notes, and that Facebook Ads (not selected) has
        // been set to INACCESSIBLE OR NOT STORED
        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        facebookAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgFacebookAds)

        assertNotNull(googleAdsInstructions)
        assertEquals(ProcessingMethod.DELETE, googleAdsInstructions.processingMethod)
        assertEquals("These are my custom notes", googleAdsInstructions.processingInstructions)

        assertNotNull(facebookAdsInstructions)
        assertEquals(ProcessingMethod.INACCESSIBLE_OR_NOT_STORED, facebookAdsInstructions.processingMethod)
    }

    @Test
    fun write__setsProcessingMethodWhenRequestHandlingInstructionsAlreadyExistWithAlternativeProcessingMethod() {
        val existingGoogleAdsInstructions = persistAndFlush(
            ObjectFactory.requestHandlingInstruction(
                org, orgGoogleAds,
                DataRequestInstructionType.DELETE, ProcessingMethod.DELETE
            )
        )

        orgRepo.refresh(org)
        sellingAndSharingSurveyInterceptor.write(org, "[\"${orgGoogleAds.id}\"]", CUSTOM_AUDIENCE_QUESTION_SLUG, SELLING_AND_SHARING_SURVEY_NAME, CUSTOM_AUDIENCE_QUESTION_SLUG)

        var googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)

        assertNotNull(googleAdsInstructions)
        assertEquals(existingGoogleAdsInstructions.id, googleAdsInstructions.id)
        assertEquals(ProcessingMethod.DELETE, googleAdsInstructions.processingMethod)

        // Select Facebook Ads
        val answers = "[\"${orgFacebookAds.id}\"]"
        sellingAndSharingSurveyInterceptor.write(org, answers, CUSTOM_AUDIENCE_QUESTION_SLUG, SELLING_AND_SHARING_SURVEY_NAME, CUSTOM_AUDIENCE_QUESTION_SLUG)

        // Assert that Google Ads (which was not selected) is now updated to INACCESSIBLE OR NOT STORED
        googleAdsInstructions = requestHandlingInstructionsRepository.findByOrganizationAndRequestTypeAndOrganizationDataRecipient(org, dataRequestType = DataRequestInstructionType.DELETE, organizationDataRecipient = orgGoogleAds)
        assertNotNull(googleAdsInstructions)
        assertEquals(ProcessingMethod.INACCESSIBLE_OR_NOT_STORED, googleAdsInstructions.processingMethod)
    }
}
