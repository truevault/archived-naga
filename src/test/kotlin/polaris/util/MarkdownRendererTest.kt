package polaris.util

import org.junit.Test
import kotlin.test.assertEquals

internal class MarkdownRendererTest {

    @Test
    fun parsesStrikethrough() {
        assertEquals(expectedStrikethrough, MarkdownRenderer.renderToHtml(strikethroughSource))
    }

    @Test
    fun parsesAutolink() {
        assertEquals(expectedAutolink, MarkdownRenderer.renderToHtml(autolinkSource))
    }

    @Test
    fun parsesTaskList() {
        assertEquals(expectedTaskList, MarkdownRenderer.renderToHtml(taskListSource))
    }

    @Test
    fun parsesTable() {
        assertEquals(expectedTable, MarkdownRenderer.renderToHtml(tableSource))
    }

    private val strikethroughSource = "Here's some ~~struck~~ text."
    private val expectedStrikethrough = "<p>Here's some <del>struck</del> text.</p>\n"

    private val autolinkSource = "This will link: http://www.commonmark.org/help"
    private val expectedAutolink = "<p>This will link: <a href=\"http://www.commonmark.org/help\">http://www.commonmark.org/help</a></p>\n"

    private val taskListSource = """
  - [ ] Item 1 (unchecked)
      - [x] Item 1a (checked)
  - [x] Item 2 (checked)
    """.trimIndent()

    private val expectedTaskList = """
  <ul>
  <li><input type="checkbox" disabled=""> Item 1 (unchecked)
  <ul>
  <li><input type="checkbox" disabled="" checked=""> Item 1a (checked)</li>
  </ul>
  </li>
  <li><input type="checkbox" disabled="" checked=""> Item 2 (checked)</li>
  </ul>

    """.trimIndent()

    private val tableSource = """
  | col 1  |  col2  |
  |--------|--------|
  | row 1a | row 1b |
  | row 2a | row 2b |
    """.trimIndent()

    private val expectedTable = """
  <table>
  <thead>
  <tr>
  <th>col 1</th>
  <th>col2</th>
  </tr>
  </thead>
  <tbody>
  <tr>
  <td>row 1a</td>
  <td>row 1b</td>
  </tr>
  <tr>
  <td>row 2a</td>
  <td>row 2b</td>
  </tr>
  </tbody>
  </table>

    """.trimIndent()
}
