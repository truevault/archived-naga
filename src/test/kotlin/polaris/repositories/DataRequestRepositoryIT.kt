package polaris.repositories

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.models.entity.request.DataRequestState
import util.ObjectFactory
import java.time.ZonedDateTime
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DataRequestRepositoryIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var subject: DataRequestRepository

    @Test
    fun `findAutoCloseable`() {
        // given
        val now = ZonedDateTime.now()
        val backupTime = now.minusDays(10)

        // add 3 orgs
        val orgs = persistAllAndFlush(
            listOf(
                ObjectFactory.defaultOrganization(name = "Org 1"),
                ObjectFactory.defaultOrganization(name = "Org 2"),
                ObjectFactory.defaultOrganization(name = "Org 3")
            )
        )

        // add 1 DSR per org
        val dsrs = persistAllAndFlush(
            listOf(
                ObjectFactory.defaultDataRequest(orgs[0], requestDate = now.minusDays(30), requestDueDate = now.minusDays(1)),
                ObjectFactory.defaultDataRequest(orgs[1], requestDate = now.minusDays(30), requestDueDate = now.minusDays(1)),
                ObjectFactory.defaultDataRequest(orgs[2], requestDate = now.minusDays(30), requestDueDate = now.minusDays(1))
            )
        )

        // soft-delete 1 org (PL-1693)
        organizationRepository.delete(orgs[1])

        flushAndClear()

        // when
        val results = subject.findAutoCloseable(now, backupTime)

        // then
        assertEquals(2, results.size)

        results.forEach {
            assertTrue(dsrs.any { d -> d.publicId == it.publicId })
            assertEquals(DataRequestState.PENDING_EMAIL_VERIFICATION, it.state)
            assertTrue(it.requestDueDate?.isBefore(now) ?: false)
        }
    }
}
