package polaris.repositories

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.models.entity.organization.MailboxStatus
import util.ObjectFactory
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class OrganizationMailboxRepositoryIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var subject: OrganizationMailboxRepository

    @Test
    fun `findAllActiveMailboxes`() {
        // given
        // 4 orgs
        val orgs = persistAllAndFlush(
            listOf(
                ObjectFactory.defaultOrganization(name = "Org 1"),
                ObjectFactory.defaultOrganization(name = "Org 2"),
                ObjectFactory.defaultOrganization(name = "Org 3"),
                ObjectFactory.defaultOrganization(name = "Org 4")
            )
        )

        // 1 mailbox per org, with various statuses
        val mailboxes = persistAllAndFlush(
            listOf(
                ObjectFactory.defaultOrganizationMailbox(
                    organization = orgs[0],
                    mailbox = "mailbox1@example.com",
                    nylasAccountId = "mail-1111",
                    status = MailboxStatus.CONNECTED
                ),
                ObjectFactory.defaultOrganizationMailbox(
                    organization = orgs[1],
                    mailbox = "mailbox2@example.com",
                    nylasAccountId = "mail-2222",
                    status = MailboxStatus.CONNECTED
                ),
                ObjectFactory.defaultOrganizationMailbox(
                    organization = orgs[2],
                    mailbox = "mailbox3@example.com",
                    nylasAccountId = "mail-3333",
                    status = MailboxStatus.DISCONNECTED
                ),
                ObjectFactory.defaultOrganizationMailbox(
                    organization = orgs[3],
                    mailbox = "mailbox4@example.com",
                    nylasAccountId = "mail-4444",
                    status = MailboxStatus.INACTIVE
                )
            )
        )

        // 2 orgs with users
        val users = persistAllAndFlush(
            listOf(
                ObjectFactory.defaultUser(email = "user1a@qa.truevault.com"),
                ObjectFactory.defaultUser(email = "user2a@qa.truevault.com"),
                ObjectFactory.defaultUser(email = "user2b@qa.truevault.com")
            )
        )
        val orgUsers = persistAllAndFlush(
            listOf(
                ObjectFactory.randomOrganizationUser(users[0], orgs[0]),
                ObjectFactory.randomOrganizationUser(users[1], orgs[2]),
                ObjectFactory.randomOrganizationUser(users[2], orgs[2])
            )
        )

        val softDeletedOrg = orgs[1]
        val inactiveMailbox = mailboxes[3]

        // soft-delete 1 org (PL-2038)
        organizationRepository.delete(softDeletedOrg)

        flushAndClear()

        // when
        val results = subject.findAllActiveMailboxes()

        flushAndClear()

        // then
        assertEquals(2, results.size)
        assertTrue(results.none { it.organization?.id == softDeletedOrg.id })
        assertTrue(results.none { it.status == MailboxStatus.INACTIVE })
        assertTrue(results.none { it.id == inactiveMailbox.id })

        // verify 1st result
        assertEquals("mailbox1@example.com", results[0].mailbox)
        assertEquals(MailboxStatus.CONNECTED, results[0].status)
        assertEquals("Org 1", results[0].organization?.name)
        assertEquals(1, results[0].organization?.organizationUsers?.size)

        // verify 2nd result
        assertEquals("mailbox3@example.com", results[1].mailbox)
        assertEquals(MailboxStatus.DISCONNECTED, results[1].status)
        assertEquals("Org 3", results[1].organization?.name)
        assertEquals(2, results[1].organization?.organizationUsers?.size)
    }
}
