package polaris.controllers.webhook

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.mockito.kotlin.any
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.springframework.beans.factory.annotation.Autowired
import polaris.BaseIntegrationTest
import polaris.models.dto.request.PhoneDataRequestDto
import polaris.models.dto.request.TwilioDataRequestDto
import polaris.models.entity.OrganizationNotFound
import util.ObjectFactory
import java.time.ZonedDateTime
import kotlin.test.assertFailsWith

class TwilioControllerIT : BaseIntegrationTest() {

    @Autowired
    private lateinit var subject: TwilioController

    @Test
    fun `receiveTwilioDataRequest - with no org with matching toll-free number`() {
        val twilioDataRequest = createTwilioDataRequest()

        assertFailsWith<OrganizationNotFound>(
            message = "No exception found",
            block = {
                subject.receiveTwilioDataRequest(twilioDataRequest)
            }
        )

        verify(emailService, times(0)).sendNewRequestByPhoneNotification(any())
    }

    @DisplayName("receiveTwilioDataRequest - with org with matching toll-free number")
    @ParameterizedTest(name = "{index}: receiveTwilioDataRequest - with org with matching toll-free number {0}")
    @CsvSource(
        "12344567890",
        "(234) 456-7890",
        "1-234-456-7890"
    )
    fun `receiveTwilioDataRequest - with org with matching toll-free number`(
        tollFreePhone: String
    ) {
        val organization = ObjectFactory.defaultOrganization()
        organization.requestTollFreePhoneNumber = tollFreePhone
        persist(organization)

        val phoneDataRequest = PhoneDataRequestDto(
            requestDate = ZonedDateTime.now(),
            requestType = "Know",
            org = organization,
            consumerPhoneNumber = "+12075556789",
            consumerNameRecording = "http://example.com",
            consumerEmailRecording = "http://example.com"
        )

        val twilioDataRequest = createTwilioDataRequest()

        subject.receiveTwilioDataRequest(twilioDataRequest)

        val argumentCaptor = argumentCaptor<PhoneDataRequestDto>()

        verify(emailService, times(1)).sendNewRequestByPhoneNotification(argumentCaptor.capture())

        argumentCaptor.firstValue.let {
            // assert that the request date is "close enough"
            assert(it.requestDate.toEpochSecond() - phoneDataRequest.requestDate.toEpochSecond() < 2)

            assertEquals(phoneDataRequest.requestType, it.requestType)
            assertEquals(phoneDataRequest.org, it.org)
            assertEquals(phoneDataRequest.consumerPhoneNumber, it.consumerPhoneNumber)
            assertEquals(phoneDataRequest.consumerNameRecording, it.consumerNameRecording)
            assertEquals(phoneDataRequest.consumerEmailRecording, it.consumerEmailRecording)
        }
    }

    private fun createTwilioDataRequest(): TwilioDataRequestDto {
        return TwilioDataRequestDto(
            phoneNumberCalled = "+112344567890",
            requestType = "Know",
            consumerPhoneNumber = "+12075556789",
            consumerNameRecording = "http://example.com",
            consumerEmailRecording = "http://example.com"
        )
    }
}
