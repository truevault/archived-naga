package polaris.controllers.util

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import polaris.Application
import polaris.models.entity.OrganizationNotFound
import polaris.models.entity.organization.Organization
import polaris.repositories.OrganizationRepository
import util.Factory
import javax.transaction.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class])
@Transactional
class LookupServiceTest {

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var lookupService: LookupService

    private lateinit var organization: Organization

    @Before
    fun createOrganization() {
        organization = Factory.createOrganization(organizationRepository, name = "Organization 1")
    }

    @Test
    fun organizationByPhoneNumberOrThrow__findsOrgForCorrectNumber() {
        organization.requestTollFreePhoneNumber = "1-800-555-5555"
        organizationRepository.save(organization)

        assertEquals(lookupService.organizationByPhoneNumberOrThrow("1-800-555-5555"), organization)
    }

    @Test
    fun organizationByPhoneNumberOrThrow__doesNotFindOrgWhenOrgHasInvalidPhoneNumberInDatabase() {
        // We actually had this value in the prod database, I'm not just an idiot when writing
        // these tests
        organization.requestTollFreePhoneNumber = "[object Object]"
        organizationRepository.save(organization)

        assertFailsWith<OrganizationNotFound> { lookupService.organizationByPhoneNumberOrThrow("1-800-222-2222") }
    }

    @Test
    fun organizationByPhoneNumberOrThrow__throwsNotFoundForWrongNumber() {
        organization.requestTollFreePhoneNumber = "1-800-555-5555"
        organizationRepository.save(organization)

        assertFailsWith<OrganizationNotFound> { lookupService.organizationByPhoneNumberOrThrow("1-800-222-2222") }
    }

    @Test
    fun organizationByPhoneNumberOrThrow__findsOrgForCloseMatches() {
        organization.requestTollFreePhoneNumber = "1-800-555-5555"
        organizationRepository.save(organization)

        val phoneNumbers = arrayOf(
            "+18005555555",
            "18005555555",
            "8005555555",
            "(800) 555-5555",
            "800.555.5555"
        )

        phoneNumbers.forEach { assertEquals(lookupService.organizationByPhoneNumberOrThrow(it), organization) }
    }
}
