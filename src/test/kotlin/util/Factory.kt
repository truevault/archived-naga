package util

import org.springframework.security.crypto.password.PasswordEncoder
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.feature.OrganizationFeature
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.MailboxType
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.organization.OrganizationSurveyQuestion
import polaris.models.entity.organization.OrganizationUser
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.request.DataSubjectRequestEvent
import polaris.models.entity.user.User
import polaris.models.entity.user.UserRole
import polaris.models.entity.user.UserStatus
import polaris.models.entity.vendor.Vendor
import polaris.repositories.CollectionGroupRepository
import polaris.repositories.DataRequestEventRepository
import polaris.repositories.DataRequestRepository
import polaris.repositories.FeatureRepository
import polaris.repositories.OrganizationDataRecipientRepository
import polaris.repositories.OrganizationFeatureRepository
import polaris.repositories.OrganizationMailboxRepository
import polaris.repositories.OrganizationRepository
import polaris.repositories.OrganizationSurveyQuestionRepository
import polaris.repositories.OrganizationUserRepository
import polaris.repositories.PrivacyCenterRepository
import polaris.repositories.UserRepository
import polaris.repositories.VendorRepository
import polaris.util.IdGenerator
import java.time.ZonedDateTime
import java.util.UUID

object Factory {

    fun createOrganization(
        organizationRepository: OrganizationRepository,
        id: UUID = UUID.randomUUID(),
        publicId: String = IdGenerator.generate(),
        name: String = "My Organization",
        identityVerificationMessage: String = "Look at the person"
    ): Organization {
        return organizationRepository.save(
            ObjectFactory.defaultOrganization(
                id = id,
                publicId = publicId,
                name = name,
                identityVerificationMessage = identityVerificationMessage
            )
        )
    }

    fun createOrgFeature(
        repo: OrganizationFeatureRepository,
        featureRepo: FeatureRepository,
        org: Organization,
        feature: String,
        enabled: Boolean = true
    ): OrganizationFeature {
        val f = featureRepo.findByName(feature)
        return repo.save(OrganizationFeature(organization = org, feature = f, enabled = enabled))
    }

    fun createSurveyResponse(
        repo: OrganizationSurveyQuestionRepository,
        org: Organization,
        survey: String,
        slug: String,
        answer: String
    ) {
        repo.save(
            OrganizationSurveyQuestion(
                organization = org,
                surveyName = survey,
                slug = slug,
                answer = answer
            )
        )
    }

    fun createCollectionGroup(
        name: String,
        org: Organization,
        collectionGroupRepository: CollectionGroupRepository,
        autocreatedSlug: AutocreatedCollectionGroupSlug? = null
    ): CollectionGroup {
        return collectionGroupRepository.save(
            ObjectFactory.defaultCollectionGroup(
                name = name,
                org = org,
                autocreatedSlug = autocreatedSlug
            )
        )
    }

    fun createVendor(
        vendorRepository: VendorRepository,
        id: UUID = UUID.randomUUID(),
        name: String,
        url: String = "http://www.google.com",
        recipientCategory: String? = null,
        vendorKey: String? = null
    ): Vendor {
        return vendorRepository.save(
            ObjectFactory.defaultVendor(
                id = id,
                name = name,
                url = url,
                vendorKey = vendorKey,
                recipientCategory = recipientCategory
            )
        )
    }

    fun createOrganizationVendor(
        organizationDataRecipientRepository: OrganizationDataRecipientRepository,
        o: Organization,
        vendor: Vendor
    ): OrganizationDataRecipient {
        val os: OrganizationDataRecipient = organizationDataRecipientRepository.save(
            OrganizationDataRecipient(
                organization = o,
                vendor = vendor
            )
        )
        return os
    }

    fun createOrgMailbox(
        orgMailboxRepo: OrganizationMailboxRepository,
        org: Organization,
        status: MailboxStatus = MailboxStatus.CONNECTED,
        mailbox: String = "mailbox@example.com",
        type: MailboxType = MailboxType.GMAIL,
        nylasAccountId: String = "mail-1234"
    ): OrganizationMailbox {
        return orgMailboxRepo.save(
            ObjectFactory.defaultOrganizationMailbox(
                id = UUID.randomUUID(),
                organization = org,
                mailbox = mailbox,
                status = status,
                type = type,
                nylasAccountId = nylasAccountId
            )
        )
    }

    fun createPrivacyCenter(
        privacyCenterRepo: PrivacyCenterRepository,
        org: Organization,
        requestEmail: String = "privacy@qa.truevault.com",
        id: UUID = UUID.randomUUID()
    ): PrivacyCenter {
        return privacyCenterRepo.save(
            ObjectFactory.defaultPrivacyCenter(
                id = id,
                org = org,
                requestEmail = requestEmail
            )
        )
    }

    fun createDataRequest(
        dataRequestRepository: DataRequestRepository,
        dataRequestEventRepository: DataRequestEventRepository,
        o: Organization,
        u: User,
        drt: DataRequestType,
        id: UUID = UUID.randomUUID(),
        publicId: String = IdGenerator.generate(),
        state: DataRequestState = DataRequestState.PROCESSING,
        subjectFirstName: String = "John",
        subjectLastName: String = "Doe",
        subjectEmailAddress: String? = null,
        dueDate: ZonedDateTime = ZonedDateTime.now().plusDays(30),
        requestDate: ZonedDateTime = ZonedDateTime.now().plusDays(30),
        closedAt: ZonedDateTime = ZonedDateTime.now().plusDays(30),
        createdAt: ZonedDateTime = ZonedDateTime.now()
    ): DataSubjectRequest {
        val request = dataRequestRepository.save(
            ObjectFactory.defaultDataRequest(
                id = id,
                publicId = publicId,
                organization = o,
                state = state,
                subjectEmailAddress = subjectEmailAddress,
                subjectFirstName = subjectFirstName,
                subjectLastName = subjectLastName,
                requestType = drt,
                requestDueDate = dueDate,
                requestDate = requestDate,
                closedAt = closedAt,
                createdAt = createdAt
            )
        )
        dataRequestEventRepository.save(DataSubjectRequestEvent(actor = u, dataSubjectRequest = request))
        dataRequestRepository.refresh(request)
        return request
    }

    fun createUser(
        userRepository: UserRepository,
        organizationUserRepository: OrganizationUserRepository,
        organizationRepository: OrganizationRepository? = null,
        passwordEncoder: PasswordEncoder? = null,
        id: UUID = UUID.randomUUID(),
        email: String = "",
        password: String? = null,
        organization: Organization? = null,
        role: UserRole = UserRole.ADMIN,
        status: UserStatus = UserStatus.ACTIVE
    ): User {
        val userOrganization = organization ?: organizationRepository?.let { createOrganization(it) }
            ?: throw IllegalArgumentException("An organization repository or an organization must be provided to Factory.createUser")

        val encryptedPassword = password?.let {
            passwordEncoder?.encode(it)
                ?: throw IllegalArgumentException("If a password is given to Factory.createUser a passwordEncoder must also be provided.")
        }

        val user = userRepository.save(
            User(
                id = id,
                email = email,
                password = encryptedPassword
            )
        )

        addUserToOrganization(
            organizationUserRepository = organizationUserRepository,
            userRepository = userRepository,
            user = user,
            organization = userOrganization,
            role = role,
            status = status
        )

        return user
    }

    fun addUserToOrganization(
        organizationUserRepository: OrganizationUserRepository,
        userRepository: UserRepository? = null,
        organizationRepository: OrganizationRepository? = null,
        user: User,
        organization: Organization,
        role: UserRole = UserRole.ADMIN,
        status: UserStatus = UserStatus.ACTIVE
    ) {
        val organizationUser = OrganizationUser(user = user, organization = organization, role = role, status = status)
        organizationUserRepository.save(organizationUser)

        userRepository?.refresh(user)
        organizationRepository?.refresh(organization)
    }
}
