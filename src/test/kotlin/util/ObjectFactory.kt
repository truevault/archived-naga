package util

import polaris.models.dto.orgDraftSchema.v1.BusinessSurvey
import polaris.models.dto.orgDraftSchema.v1.OrgDraftSchemaV1
import polaris.models.dto.orgDraftSchema.v1.Surveys
import polaris.models.entity.compliance.AutocreatedCollectionGroupSlug
import polaris.models.entity.compliance.PICGroup
import polaris.models.entity.compliance.PersonalInformationCategory
import polaris.models.entity.email.QueuedEmail
import polaris.models.entity.email.QueuedEmailStatusType
import polaris.models.entity.feature.Feature
import polaris.models.entity.feature.OrganizationFeature
import polaris.models.entity.organization.CollectionGroup
import polaris.models.entity.organization.CollectionGroupCollectedPIC
import polaris.models.entity.organization.DataRecipientCollectionGroup
import polaris.models.entity.organization.MailboxStatus
import polaris.models.entity.organization.MailboxType
import polaris.models.entity.organization.OrgDraftSchemaVersion
import polaris.models.entity.organization.Organization
import polaris.models.entity.organization.OrganizationDataRecipient
import polaris.models.entity.organization.OrganizationDraft
import polaris.models.entity.organization.OrganizationMailbox
import polaris.models.entity.organization.OrganizationUser
import polaris.models.entity.organization.RequestHandlingInstruction
import polaris.models.entity.organization.Task
import polaris.models.entity.organization.TaskKey
import polaris.models.entity.privacycenter.PrivacyCenter
import polaris.models.entity.request.DataRequestInstructionType
import polaris.models.entity.request.DataRequestState
import polaris.models.entity.request.DataRequestType
import polaris.models.entity.request.DataSubjectRequest
import polaris.models.entity.request.ProcessingMethod
import polaris.models.entity.user.User
import polaris.models.entity.user.UserRole
import polaris.models.entity.user.UserStatus
import polaris.models.entity.vendor.Vendor
import polaris.util.IdGenerator
import java.time.ZonedDateTime
import java.util.UUID

/**
 * Factory methods for creating detached entities and DTOs
 */
object ObjectFactory {

    fun defaultVendor(
        id: UUID = UUID.randomUUID(),
        name: String = "Google",
        url: String = "http://www.google.com",
        recipientCategory: String? = null,
        vendorKey: String? = null
    ): Vendor {
        return Vendor(
            id = id,
            name = name,
            url = url,
            vendorKey = vendorKey,
            recipientCategory = recipientCategory
        )
    }

    fun defaultOrganization(
        id: UUID = UUID.randomUUID(),
        publicId: String = IdGenerator.generate(),
        name: String = "My Organization",
        identityVerificationMessage: String = "Look at the person",
        features: List<String> = emptyList()
    ): Organization {
        return Organization(
            id = id,
            publicId = publicId,
            name = name,
            identityVerificationMessage = identityVerificationMessage,
            organizationFeatures = features.map { OrganizationFeature(feature = Feature(name = it), enabled = true) }
        )
    }

    fun defaultOrgDraft(
        org: Organization,
        schemaVersion: OrgDraftSchemaVersion = OrgDraftSchemaVersion.UNKNOWN,
        data: OrgDraftSchemaV1 = OrgDraftSchemaV1(Surveys(BusinessSurvey(`business-physical-location` = true)))
    ): OrganizationDraft {
        return OrganizationDraft(
            organization = org, schemaVersion = schemaVersion, data = data
        )
    }

    fun defaultDataRecipient(
        organization: Organization,
        vendor: Vendor
    ): OrganizationDataRecipient {
        return OrganizationDataRecipient(
            organization = organization,
            vendor = vendor
        )
    }

    fun defaultOrganizationMailbox(
        id: UUID = UUID.randomUUID(),
        organization: Organization,
        status: MailboxStatus = MailboxStatus.CONNECTED,
        mailbox: String = "mailbox@example.com",
        type: MailboxType = MailboxType.GMAIL,
        nylasAccountId: String = "mail-1234"
    ): OrganizationMailbox {
        return OrganizationMailbox(
            id = id,
            organization = organization,
            mailbox = mailbox,
            status = status,
            type = type,
            nylasAccountId = nylasAccountId
        )
    }

    fun randomOrganizationUser(
        user: User,
        organization: Organization,
        role: UserRole = UserRole.ADMIN,
        status: UserStatus = UserStatus.ACTIVE
    ): OrganizationUser {
        return OrganizationUser(
            user = user,
            organization = organization,
            role = role,
            status = status
        )
    }

    fun defaultUser(
        id: UUID = UUID.randomUUID(),
        email: String = "johndoe@qa.truevault.com",
        firstName: String = "John",
        lastName: String = "Doe"
    ): User {
        return User(
            id = id,
            email = email,
            firstName = firstName,
            lastName = lastName
        )
    }

    fun defaultPrivacyCenter(
        org: Organization,
        id: UUID = UUID.randomUUID(),
        requestEmail: String = "privacy@qa.truevault.com",
        defaultSubdomain: String = UUID.randomUUID().toString()
    ): PrivacyCenter {
        return PrivacyCenter(
            id = id,
            organization = org,
            requestEmail = requestEmail,
            defaultSubdomain = defaultSubdomain
        )
    }

    fun defaultDataRequest(
        organization: Organization,
        requestType: DataRequestType = DataRequestType.CCPA_RIGHT_TO_KNOW,
        id: UUID = UUID.randomUUID(),
        publicId: String = IdGenerator.generate(),
        state: DataRequestState = DataRequestState.PENDING_EMAIL_VERIFICATION,
        subjectFirstName: String? = "John",
        subjectLastName: String? = "Doe",
        subjectEmailAddress: String? = null,
        requestDate: ZonedDateTime = ZonedDateTime.now(),
        requestDueDate: ZonedDateTime? = ZonedDateTime.now().plusDays(30),
        createdAt: ZonedDateTime = ZonedDateTime.now(),
        closedAt: ZonedDateTime? = null
    ): DataSubjectRequest {
        return DataSubjectRequest(
            id = id,
            publicId = publicId,
            organization = organization,
            requestType = requestType,
            state = state,
            subjectEmailAddress = subjectEmailAddress,
            subjectFirstName = subjectFirstName,
            subjectLastName = subjectLastName,
            requestDueDate = requestDueDate,
            requestDate = requestDate,
            createdAt = createdAt,
            closedAt = closedAt
        )
    }

    fun defaultQueuedEmail(
        dataSubjectRequest: DataSubjectRequest,
        senderEmailAddress: String = "sender@qa.truevault.com",
        recipientEmailAddress: String = "recipient@qa.truevault.com",
        subject: String = "hello",
        message: String = "world",
        status: QueuedEmailStatusType = QueuedEmailStatusType.NEW,
        createdAt: ZonedDateTime = ZonedDateTime.now()
    ): QueuedEmail {
        return QueuedEmail(
            dataSubjectRequest = dataSubjectRequest,
            senderEmailAddress = senderEmailAddress,
            recipientEmailAddress = recipientEmailAddress,
            subject = subject,
            message = message,
            status = status,
            createdAt = createdAt,
            lastUpdatedAt = createdAt
        )
    }

    fun defaultCollectionGroup(
        name: String = "Online Shoppers",
        org: Organization,
        autocreatedSlug: AutocreatedCollectionGroupSlug? = null
    ): CollectionGroup {
        return CollectionGroup(name = name, organization = org, autocreationSlug = autocreatedSlug)
    }

    fun defaultPICGroup(name: String = "Personal Identifiers", groupKey: String = "personal-identifiers"): PICGroup {
        return PICGroup(name = name, groupKey = groupKey)
    }

    fun defaultPersonalInformationCategory(
        name: String = "Name",
        description: String = "",
        key: String = "name",
        group: PICGroup
    ): PersonalInformationCategory {
        return PersonalInformationCategory(
            name = name,
            description = description,
            group = group,
            picKey = key,
            deprecated = false
        )
    }

    fun dataRecipientToCollectionGroup(
        dataRecipient: OrganizationDataRecipient,
        collectionGroup: CollectionGroup,
        noDataDisclosure: Boolean? = null
    ): DataRecipientCollectionGroup {
        return DataRecipientCollectionGroup(
            vendorId = dataRecipient.vendor!!.id,
            collectionGroupId = collectionGroup.id,
            noDataDisclosure = noDataDisclosure
        )
    }

    fun collectedPICToCollectionGroup(
        collectionGroup: CollectionGroup,
        pic: PersonalInformationCategory
    ): CollectionGroupCollectedPIC {
        return CollectionGroupCollectedPIC(collectionGroup = collectionGroup, pic = pic)
    }

    fun requestHandlingInstruction(
        org: Organization,
        dataRecipient: OrganizationDataRecipient,
        type: DataRequestInstructionType = DataRequestInstructionType.DELETE,
        method: ProcessingMethod = ProcessingMethod.RETAIN,
        retentionReasons: MutableList<String> = mutableListOf(),
        instructions: String = "These are my custom instructions"
    ): RequestHandlingInstruction {
        return RequestHandlingInstruction(
            organization = org,
            organizationDataRecipient = dataRecipient,
            requestType = type,
            processingMethod = method,
            retentionReasons = retentionReasons,
            processingInstructions = instructions
        )
    }

    fun task(org: Organization, taskKey: TaskKey, completedAt: ZonedDateTime? = null): Task {
        return Task(organization = org, taskKey = taskKey, completedAt = completedAt)
    }
}
