import React from "react";

export const AdminJsonOutput = ({ data }) => {
  return (
    <pre
      className="mt-xl"
      style={{
        color: "#afb6c7",
        font: "monospace",
      }}
    >
      {JSON.stringify(data, null, 4)}
    </pre>
  );
};
