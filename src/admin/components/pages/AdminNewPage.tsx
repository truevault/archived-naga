import setFieldData from "final-form-set-field-data";
import { useRouter } from "next/router";
import React from "react";
import { Form } from "react-final-form";
import { useActionRequest } from "../../../js/common/hooks/api";
import { axios } from "../../lib/net";
import { AdminPageWrapper } from "../AdminPageWrapper";
import { AdminResourceForm } from "../form/AdminResourceForm";

export const AdminNewPage = ({ config, data }) => {
  return (
    <AdminPageWrapper header={`New ${config.singular}`}>
      <FieldsSection config={config} data={data} />
    </AdminPageWrapper>
  );
};

const FieldsSection = ({ config, data }) => {
  const router = useRouter();

  const done = (res) => {
    const editPath = `${config.path}/edit.html?id=${res.id}`;
    router.push(editPath);
  };

  const postUrl = config.url;

  const { fetch } = useActionRequest({
    api: async (values) => (await axios.post(postUrl, values)).data,
    onSuccess: done,
    messages: {
      success: `${config.singular} created successfully`,
    },
  });

  const onSubmit = async (values) => {
    fetch(values);
  };

  return (
    <Form
      mutators={{ setFieldData }}
      onSubmit={onSubmit}
      initialValues={data || {}}
      // validate={validation}
      render={({ handleSubmit, form, ...rest }) => {
        return (
          <form onSubmit={handleSubmit}>
            <AdminResourceForm config={config} mutators={form.mutators} {...rest} isNew={true} />
          </form>
        );
      }}
    />
  );
};
