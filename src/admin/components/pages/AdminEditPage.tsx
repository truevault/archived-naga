import { Button } from "@mui/material";
import setFieldData from "final-form-set-field-data";
import { useRouter } from "next/router";
import React, { useCallback } from "react";
import { Form } from "react-final-form";
import { PageLoading } from "../../../js/common/components/Loading";
import { useActionRequest, useDataRequest } from "../../../js/common/hooks/api";
import { NetworkRequest } from "../../../js/common/models";
import { AdminConfig } from "../../lib/adminConfig";
import { axios } from "../../lib/net";
import { AdminJsonOutput } from "../AdminJsonOutput";
import { AdminPageWrapper } from "../AdminPageWrapper";
import { AdminResourceForm } from "../form/AdminResourceForm";

type EditPageProps = {
  config: AdminConfig;
  data: any;
};

export const AdminEditPage: React.FC<EditPageProps> = ({ config, data }) => {
  const router = useRouter();
  const id = router.query.id;
  const editUrl = config.url + id;

  const hasId = Boolean(id);

  const getEdit = async () => {
    return (await axios.get(editUrl)).data;
  };

  const { request, result, refresh } = useDataRequest({
    queryKey: ["edit", config.label],
    api: getEdit,
    notReady: !hasId,
  });

  if (!NetworkRequest.areFinished(request)) {
    return <PageLoading />;
  }

  return (
    <AdminPageWrapper
      header={`Edit ${config.singular}`}
      headerActions={<HeaderActions config={config} result={result} />}
    >
      <FieldsSection
        data={{ ...result, ...data }}
        config={config}
        editUrl={editUrl}
        refresh={refresh}
      />
      <AdminJsonOutput data={result} />
    </AdminPageWrapper>
  );
};

type HeaderActionsProps = {
  config: AdminConfig;
  result: any;
};
const HeaderActions: React.FC<HeaderActionsProps> = ({ config, result }) => {
  const router = useRouter();
  const id = router.query.id;
  const deleteUrl = config.url + id;
  const toGetCompliantUrl = config.url + id + "/toGetCompliant";
  const publishConfigUrl = config.url + id + "/publishCmpConfigs";

  const onDelete = async () => {
    if (config.can?.delete)
      if (confirm("Clicking <OK> to proceed with the soft deletion")) {
        await axios.delete(deleteUrl);
        router.push(config.path);
      }
  };

  const onReturnToGetCompliant = async () => {
    if (config.customActions?.returnToGetCompliant) {
      if (confirm("Clicking <OK> to proceed with the returning to get compliant")) {
        await axios.post(toGetCompliantUrl);
        router.push(config.path);
      }
    }
  };

  const onPublishConfigs = async () => {
    if (config.customActions?.publishCmpConfigs) {
      await axios.post(publishConfigUrl);
      router.push(config.path);
    }
  };

  const returnToGetCompliantBtn =
    config?.customActions?.returnToGetCompliant && result.phase == "STAY_COMPLIANT" ? (
      <Button
        color="secondary"
        variant="outlined"
        className="mr-md"
        onClick={onReturnToGetCompliant}
      >
        Return to Get Compliant
      </Button>
    ) : null;

  const publishConfigButton =
    config?.customActions?.publishCmpConfigs && result.phase == "STAY_COMPLIANT" ? (
      <Button color="secondary" variant="outlined" className="mr-md" onClick={onPublishConfigs}>
        Publish CMP Configs
      </Button>
    ) : null;

  const deleteBtn = config.can?.delete ? (
    <Button color="error" variant="contained" onClick={onDelete}>
      Delete {config.singular}
    </Button>
  ) : null;

  return (
    <div>
      {returnToGetCompliantBtn}
      {publishConfigButton}
      {deleteBtn}
    </div>
  );
};

const FieldsSection = ({ data, config, editUrl, refresh }) => {
  const fieldKeys: string[] = config.fields.map((f) => f.field);
  const { fetch } = useActionRequest({
    api: async (values) => (await axios.put(editUrl, values)).data,
    onSuccess: refresh,
    notifySuccess: true,
    messages: {
      success: `${config.singular} updated successfully`,
    },
  });

  const onSubmit = useCallback(
    async (values) => {
      const _values = Object.fromEntries(
        Object.entries(values).filter(([key]) => fieldKeys.includes(key)),
      );
      fetch(_values);
    },
    [fetch],
  );

  return (
    <Form
      mutators={{ setFieldData }}
      onSubmit={onSubmit}
      initialValues={data}
      subscription={{ values: false }}
      render={({ handleSubmit, form, ...rest }) => {
        return (
          <form onSubmit={handleSubmit}>
            <AdminResourceForm config={config} mutators={form.mutators} {...rest} />
          </form>
        );
      }}
    />
  );
};
