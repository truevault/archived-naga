import React, { useMemo } from "react";
import { AdminPageWrapper } from "../../components/AdminPageWrapper";
import axios from "axios";
import { ButtonLink } from "../ButtonLink";
import { NetworkRequest } from "../../../js/common/models";
import { PageLoading } from "../../../js/common/components/Loading";
import * as env from "../../lib/env.json";
import { useTable } from "react-table";
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
} from "@mui/material";
import { useDataRequest } from "../../../js/common/hooks/api";

const axiosInstance = axios.create({
  baseURL: env.polaris,
  withCredentials: true,
});

const ImageCell = ({ value }) => {
  return <img src={value} width="25" />;
};

const buildColumns = (config) => {
  return config.fields
    .filter((f) => f.index)
    .map((f) => {
      const opt: { Header: any; accessor: any; Cell?: any } = {
        Header: f.label,
        accessor: getAccessorForField(f),
      };
      if (f.indexType == "image") {
        opt["Cell"] = ImageCell;
      }
      return opt;
    });
};

const getAccessorForField = (field) => {
  if (field?.index?.accessor) {
    return field.index.accessor;
  }

  if (field.type == "relationship") {
    return field.field + ".label";
  }

  if (field?.input?.type === "select") {
    const lookup: Record<string, string> = field.input.options.reduce(
      (agg, { label, value }: Record<string, string>) => Object.assign(agg, { [value]: label }),
      {},
    );
    return (row: any) => {
      const actualValue = row[field.field];
      if (Array.isArray(actualValue)) {
        return actualValue.map((it) => lookup[it] ?? it).join(", ");
      }

      return lookup[actualValue] ?? actualValue;
    };
  }

  return field.field;
};

export const AdminIndexPage = ({ config }) => {
  const indexUrl = config.url;

  const getIndex = async () => {
    return (await axiosInstance.get(indexUrl)).data;
  };

  const { request, result } = useDataRequest({ queryKey: ["index", config.label], api: getIndex });
  const cols = useMemo(() => buildColumns(config), [config]);

  if (!NetworkRequest.areFinished(request)) {
    return <PageLoading />;
  }

  return (
    <AdminPageWrapper header={config.label} headerActions={<HeaderAction config={config} />}>
      <AdminTable config={config} data={result} columns={cols} />

      <div className="my-md" />

      {/* <AdminJsonOutput data={result} /> */}
    </AdminPageWrapper>
  );
};

const HeaderAction = ({ config }) => {
  if (config.can?.create === false) {
    return null;
  }

  return (
    <ButtonLink
      variant="contained"
      color="primary"
      href={`${config.path}/new`}
      as={`${config.path}/new.html`}
    >
      Create New {config.singular || config.label}
    </ButtonLink>
  );
};

const AdminTable = ({ config, columns, data }) => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable({
    columns,
    data,
  });

  return (
    <TableContainer component={Paper} {...getTableProps()}>
      <Table>
        <TableHead>
          {headerGroups.map((headerGroup, ridx) => (
            <TableRow key={`table-header-row-${ridx}`} {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column, cidx) => (
                <TableCell
                  key={`table-header-row-${ridx}-cell-${cidx}`}
                  {...column.getHeaderProps()}
                >
                  {column.render("Header")}
                </TableCell>
              ))}
              <TableCell align="right">Actions</TableCell>
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {rows.map((row, ridx) => {
            prepareRow(row);
            return (
              <TableRow key={`table-body-row-${ridx}`} {...row.getRowProps()}>
                {row.cells.map((cell, cidx) => {
                  return (
                    <TableCell key={`table-body-row-${ridx}-cell-${cidx}`} {...cell.getCellProps()}>
                      {cell.render("Cell")}
                    </TableCell>
                  );
                })}

                <TableCell align="right">
                  <ButtonLink
                    size="small"
                    variant="outlined"
                    color="primary"
                    href={`${config.path}/edit?id=${row.original.id}`}
                    as={`${config.path}/edit.html?id=${row.original.id}`}
                  >
                    edit
                  </ButtonLink>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
