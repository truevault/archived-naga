import React from "react";
import { Text } from "../../../../js/common/components/input/Text";
import { useField } from "react-final-form";

export const ClientOrganizations = ({ field }) => {
  const clientOrganizationsF = useField("clientOrganizations");

  const onChange = ({ target }) => {
    const orgs = target.value.split("\n").map((id) => {
      return { id: id };
    });
    clientOrganizationsF.input.onChange(orgs);
  };

  return (
    <Text
      multiline={true}
      variant="outlined"
      field={"clientOrganizationText"}
      label={field.label}
      onChange={onChange}
      rows={1}
      rowsMax={10}
      value={clientOrganizationsF.input.value.map((o) => o.id).join("\n")}
    />
  );
};
