import React, { useEffect, useMemo, useState } from "react";
import { Api } from "../../../../js/common/service/Api";
import { RetentionReasonDto } from "../../../../js/common/service/server/dto/RetentionReasonDto";
import { FieldConfig } from "../../../lib/adminConfig";
import { InputRowSelect } from "../inputs/AdminInputSelect";

type Props = {
  field: FieldConfig;
};
export const CustomRetentionReasonSelect: React.FC<Props> = ({ field }) => {
  // get retention reasons...
  const [reasons, setReasons] = useState<RetentionReasonDto[]>([]);

  useEffect(() => {
    const doit = async () => {
      const remoteReasons = await Api.polarisData.getRetentionReasons();
      setReasons(remoteReasons);
    };

    doit();
  }, []);

  const selectField: FieldConfig = useMemo(() => {
    const options = reasons.map((r) => ({
      value: r.id,
      label: r.reason,
    }));

    return {
      ...field,
      input: {
        ...field.input,
        type: "select",
        options,
      },
    };
  }, [reasons]);

  return <InputRowSelect field={selectField} />;
};
