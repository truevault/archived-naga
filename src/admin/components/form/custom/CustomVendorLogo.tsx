import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
} from "@mui/material";
import {
  AttachFile as AttachFileIcon,
  CloudUpload as CloudUploadIcon,
  Delete as DeleteIcon,
} from "@mui/icons-material";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import { Field, Form, useField } from "react-final-form";
import { InlineLoading } from "../../../../js/common/components/Loading";
import { Text } from "../../../../js/common/components/input/Text";
import { requiredUrl } from "../../../../js/polaris/util/validation";
import { axios, externalAxios } from "../../../lib/net";

export const VendorLogo = ({ field }) => {
  const [showExternalUploadDialog, setShowExternalUploadDialog] = useState(false);
  const [showLocalUploadDialog, setShowLocalUploadDialog] = useState(false);
  const [submitLoading, setSubmitLoading] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const logoFilenameF = useField("logoFilename");
  const logoUrlF = useField("logoUrl");

  const handleRemove = () => {
    logoFilenameF.input.onChange(null);
    logoUrlF.input.onChange(null);
  };

  const fetchImage = async (imageUrl) => {
    const response = await externalAxios.get(imageUrl, { responseType: "blob" });
    return response.data;
  };

  const uploadLogo = async (filename, data) => {
    const formData = new FormData();
    formData.append("logo", data, filename);
    try {
      const response = await axios.post("/admin/vendors/logo", formData, {
        headers: { "Content-Type": "multipart/form-data" },
      });
      return response.data;
    } catch (err) {
      return { error: err };
    }
  };

  const handleUploadResponse = (logoData, setShowDialog) => {
    if (logoData.error) {
      enqueueSnackbar(`Unable to upload image: ${logoData.error}`, { variant: "error" });
    } else {
      logoFilenameF.input.onChange(logoData.logoFilename);
      logoUrlF.input.onChange(logoData.logoUrl);
      setShowDialog(false);
    }
    setSubmitLoading(false);
  };

  const processExternalLogoImage = async (imageUrl) => {
    const imageData = await fetchImage(imageUrl);
    const filename = imageUrl.split("/").slice(-1)[0];
    const logoData = await uploadLogo(filename, imageData);
    handleUploadResponse(logoData, setShowExternalUploadDialog);
  };

  const processLocalLogoImage = async (file) => {
    const logoData = await uploadLogo(file.name, file);
    handleUploadResponse(logoData, setShowLocalUploadDialog);
    setSubmitLoading(false);
  };

  return (
    <>
      <h4 className="mb-xs">Upload Logo Image</h4>
      <div className="flex-row">
        <div className="flex-col pt-sm w-100">
          <div className="flex-row">
            <Text
              field="logoFilenameText"
              variant="outlined"
              disabled={true}
              label={field.label}
              value={logoFilenameF.input.value}
            />
            <span style={{ display: "block" }}>
              <IconButton
                onClick={() => setShowExternalUploadDialog(true)}
                color="inherit"
                title="Upload From External Source"
                size="large"
              >
                <CloudUploadIcon />
              </IconButton>
            </span>
            <span style={{ display: "block" }}>
              <IconButton
                onClick={() => setShowLocalUploadDialog(true)}
                color="inherit"
                title="Upload From Local File"
                size="large"
              >
                <AttachFileIcon />
              </IconButton>
            </span>
            <span style={{ display: "block" }}>
              <IconButton
                onClick={() => handleRemove()}
                color="inherit"
                title="Remove"
                size="large"
              >
                <DeleteIcon />
              </IconButton>
            </span>
          </div>
        </div>
        <div className="flex-col">
          {logoUrlF.input.value && <img title="Preview" src={logoUrlF.input.value} width="75" />}
        </div>
      </div>
      <UploadExternalLogoDialog
        open={showExternalUploadDialog}
        onClose={() => setShowExternalUploadDialog(false)}
        onSubmit={({ imageUrl }) => {
          setSubmitLoading(true);
          processExternalLogoImage(imageUrl);
        }}
        submitLoading={submitLoading}
      />
      <UploadLocalLogoDialog
        open={showLocalUploadDialog}
        onClose={() => setShowLocalUploadDialog(false)}
        onSubmit={(files) => {
          if (files.length >= 1) {
            setSubmitLoading(true);
            processLocalLogoImage(files[0]);
          }
        }}
        submitLoading={submitLoading}
      />
    </>
  );
};

type UploadLogoDialogProps = {
  open: boolean;
  onClose: () => void;
  onSubmit: (values) => void;
  submitLoading: boolean;
};

const UploadExternalLogoDialog = ({
  open,
  onClose,
  onSubmit,
  submitLoading,
}: UploadLogoDialogProps) => {
  return (
    <Dialog fullWidth={true} maxWidth="sm" open={open} onClose={onClose}>
      <DialogTitle>Upload Logo Image</DialogTitle>
      <Form
        onSubmit={onSubmit}
        validate={(values) => {
          const errors = {} as { [x: string]: string };
          requiredUrl(values, "imageUrl", errors);
          return errors;
        }}
        render={({ handleSubmit }) => {
          return (
            <form onSubmit={handleSubmit}>
              <DialogContent>
                <Text field="imageUrl" variant="outlined" label="Image URL" />
              </DialogContent>
              <DialogActions>
                <Button color="primary" onClick={onClose} disabled={submitLoading}>
                  Cancel
                </Button>
                <Button type="submit" color="primary" variant="contained" disabled={submitLoading}>
                  {submitLoading ? <InlineLoading /> : "Upload"}
                </Button>
              </DialogActions>
            </form>
          );
        }}
      />
    </Dialog>
  );
};

const UploadLocalLogoDialog = ({
  open,
  onClose,
  onSubmit,
  submitLoading,
}: UploadLogoDialogProps) => {
  const [files, setFiles] = useState(null);
  const localSubmit = () => {
    onSubmit(files);
    setFiles(null);
  };
  return (
    <Dialog fullWidth={true} maxWidth="sm" open={open} onClose={onClose}>
      <DialogTitle>Upload Logo Image</DialogTitle>
      <Form
        onSubmit={localSubmit}
        render={({ handleSubmit }) => {
          return (
            <form onSubmit={handleSubmit}>
              <DialogContent>
                <Field name="imageFile">
                  {({ input: { onChange, ...inputProps } }) => {
                    const handleChange = (event) => {
                      setFiles(event.target?.files);
                      onChange(event);
                    };
                    return (
                      <input {...inputProps} onChange={handleChange} type="file" required={true} />
                    );
                  }}
                </Field>
              </DialogContent>
              <DialogActions>
                <Button color="primary" onClick={onClose} disabled={submitLoading}>
                  Cancel
                </Button>
                <Button type="submit" color="primary" variant="contained" disabled={submitLoading}>
                  {submitLoading ? <InlineLoading /> : "Upload"}
                </Button>
              </DialogActions>
            </form>
          );
        }}
      />
    </Dialog>
  );
};
