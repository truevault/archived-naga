import React, { useEffect, useMemo, useState } from "react";
import { Api } from "../../../../js/common/service/Api";
import { DefProcessingActivityDto } from "../../../../js/common/service/server/dto/ProcessingActivityDto";
import { FieldConfig } from "../../../lib/adminConfig";
import { InputRowSelect } from "../inputs/AdminInputSelect";

type Props = {
  field: FieldConfig;
};
export const CustomProcessingActivitySelect: React.FC<Props> = ({ field }) => {
  // get retention reasons...
  const [activities, setActivities] = useState<DefProcessingActivityDto[]>([]);

  useEffect(() => {
    const doit = async () => {
      const remoteActivities = await Api.processingActivities.getDefaultProcessingActivities();
      setActivities(remoteActivities);
    };

    doit();
  }, []);

  const selectField: FieldConfig = useMemo(() => {
    const options = activities.map((r) => ({
      value: r.name,
      label: r.name,
    }));

    return {
      ...field,
      input: {
        ...field.input,
        type: "select",
        options,
      },
    };
  }, [activities]);

  return <InputRowSelect field={selectField} />;
};
