import _ from "lodash";
import React, { useEffect, useMemo, useState } from "react";
import { useField } from "react-final-form";
import { useDataRequest } from "../../../../js/common/hooks/api";
import { Api } from "../../../../js/common/service/Api";
import { PersonalInformationCategoryDto } from "../../../../js/common/service/server/dto/PersonalInformationOptionsDto";
import { DescriptionCheckbox } from "../../../../js/polaris/components/input/DescriptionCheckbox";
import { usePrimaryOrganizationId } from "../../../../js/polaris/hooks/useOrganization";
import { GroupIcon } from "../../../../js/polaris/pages/get-compliant/data-map/GroupIcon";
import { interweave } from "../../../../js/polaris/util/array";
import { getGroupedCategories, getOrderedGroups } from "../../../../js/polaris/util/dataInventory";

export const MapFields = ({ field }) => {
  const organizationId = usePrimaryOrganizationId();
  const { result: fetchedDefaultCollected, request: defaultCollectedRequest } = useDataRequest({
    queryKey: ["field", field.field],
    api: () => Api.dataInventory.getDefaultPersonalInformationCollected(organizationId),
  });

  const f = useField(field.field);
  const value: Array<string> = f.input.value || [];

  const [orderedGroups, setOrderedGroups] = useState<string[]>([]);
  const [groupedCategories, setGroupedCategories] = useState<
    Record<string, PersonalInformationCategoryDto[]>
  >({});
  const [readyToRender, setReadyToRender] = useState<boolean>(false);

  useEffect(() => {
    if (defaultCollectedRequest.success) {
      const uniqueCategories: PersonalInformationCategoryDto[] = _.uniqBy(
        fetchedDefaultCollected.personalInformationCategories,
        "id",
      );

      setOrderedGroups(getOrderedGroups(uniqueCategories, "default"));
      setGroupedCategories(getGroupedCategories(uniqueCategories));
      setReadyToRender(true);
    }
  }, [fetchedDefaultCollected, defaultCollectedRequest]);

  const selectedCategories = useMemo(() => {
    const categories = new Set<string>();
    value.forEach((v) => categories.add(v));
    return categories;
  }, [value]);

  const handleCategoryChecked = (id: string, checked: boolean) => {
    const current = new Set(selectedCategories);
    if (checked) {
      current.add(id);
    } else {
      current.delete(id);
    }

    f.input.onChange(Array.from(current));
  };

  if (!readyToRender) return null;

  return (
    <div className="map-vendor-data">
      {orderedGroups.map((og) => (
        <div key={`group-${og}`} className="map-vendor-data--category-group">
          <div className={"category-icon"}>
            <GroupIcon groupName={og} />
          </div>

          <div className="category-content">
            <h2 className="category-title">{og}</h2>

            <div className="category-options">
              {interweave(groupedCategories[og].sort((a, b) => a.order - b.order)).map((col, i) => {
                return (
                  <ul key={`col-${i}`}>
                    {col.map((c) => {
                      const inputId = `cg-chk-${c.id}`;
                      return (
                        <li key={`row-${i}-${c.id}`}>
                          <DescriptionCheckbox
                            key={inputId}
                            id={inputId}
                            label={c.name}
                            description={c.description}
                            color="primary"
                            checked={selectedCategories.has(c.id)}
                            onChange={(e, checked) => {
                              handleCategoryChecked(c.id, checked);
                            }}
                          />
                        </li>
                      );
                    })}
                  </ul>
                );
              })}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};
