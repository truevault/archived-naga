import React, { useEffect, useState } from "react";
import { useField } from "react-final-form";
import dynamic from "next/dynamic";

dynamic(() => import("../../../vendor/redactor/redactorx.min.js"), {
  ssr: false,
});

export const WYSIWYGInput = ({ field }) => {
  const f = useField(field.field);
  const value = f.input.value;

  const [redactor, setRedactor] = useState(undefined);

  useEffect(() => {
    if (typeof window !== "undefined") {
      const doit = async () => {
        const red = (await import("../../../vendor/redactor/redactorx.min.js")).default;
        setRedactor(() => red);
      };
      doit();
    }
  }, [setRedactor]);

  useEffect(() => {
    if (redactor) {
      const red = redactor(`#${field.field}`, {
        content: value,
        context: true,
        link: {
          target: "_blank",
        },
        buttons: {
          context: ["link"],
        },
        subscribe: {
          "editor.blur": function () {
            f.input.onChange(red.editor.getContent());
          },
        },
      });
      return () => {
        red.destroy();
      };
    }
  }, [redactor, f.input?.onChange, value, field.field]);

  return (
    <>
      <h4 className="mb-xs">{field.label}</h4>
      <textarea name={field.field} id={`${field.field}`}></textarea>
    </>
  );
};
