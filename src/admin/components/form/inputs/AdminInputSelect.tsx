import React from "react";
import { MenuItem } from "@mui/material";
import { Text } from "../../../../js/common/components/input/Text";

export const InputRowSelect = ({ field }) => {
  const readonly = field?.input?.readonly || false;
  const options = field?.input?.options || [];
  const defaultFalse = field?.input?.defaultFalse || false;
  const multiple = field?.input?.multiple || false;
  const blankValue = multiple ? [] : "";

  const finalOptions = defaultFalse ? options : [{ label: "", value: blankValue }, ...options];

  return (
    <Text
      size="small"
      variant="outlined"
      field={field.field}
      label={field.label}
      disabled={readonly}
      select={true}
      SelectProps={{
        defaultValue: blankValue,
        multiple,
      }}
      convertBoolean
    >
      {finalOptions.map((o) => {
        return (
          <MenuItem key={o.value} value={o.value}>
            {o.label || o.value}
          </MenuItem>
        );
      })}
    </Text>
  );
};
