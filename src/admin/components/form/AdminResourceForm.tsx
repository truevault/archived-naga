import {
  Autocomplete,
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Paper,
  Tab,
  Tabs,
  TextField,
} from "@mui/material";
import clsx from "clsx";
import React, { useCallback, useMemo, useState } from "react";
import { Field, useField } from "react-final-form";
import { OnChange } from "react-final-form-listeners";
import validator from "validator";
import { Text } from "../../../js/common/components/input/Text";
import { DatePicker } from "../../../js/common/components/input/Date";
import { AdminConfig, FieldConfig } from "../../lib/adminConfig";
import { ButtonLink } from "../ButtonLink";
import { VendorLogo } from "./custom/CustomVendorLogo";
import { ClientOrganizations } from "./custom/CustomClientOrganizations";
import { MapFields } from "./custom/CustomMapFields";
import { CustomRetentionReasonSelect } from "./custom/CustomRetentionReasonSelect";
import { CustomProcessingActivitySelect } from "./custom/CustomProcessingActivitySelect";
import { InputRowSelect } from "./inputs/AdminInputSelect";
import { WYSIWYGInput } from "./inputs/AdminInputWYSIWYG";

const composeValidators =
  (...validators) =>
  (value) =>
    validators.reduce((error, validator) => error || validator(value), undefined);

const getValidatorFn = (field, validatorConfig) => {
  const { type } = validatorConfig;
  if (type === "required") {
    return (value) => (!value ? `${field.label} is required` : undefined);
  } else if (type === "format") {
    if (validatorConfig.format === "URL") {
      return (value) => (value && !validator.isURL(value) ? "Must be a valid URL" : undefined);
    } else if (validatorConfig.format === "email") {
      return (value) => (value && !validator.isEmail(value) ? "Must be a valid email" : undefined);
    }
  } else if (type === "unique") {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const existingValues = useField(validatorConfig.existingValuesField).input.value;
    const compareFn = validatorConfig.compareFn
      ? validatorConfig.compareFn(existingValues)
      : (value) => (existingValues.includes(value) ? `${field.label} must be unique` : undefined);
    return (value) => compareFn(value, existingValues);
  }
  return () => undefined;
};

const AsyncWarning = ({ field, validator, mutators: { setFieldData } }) => (
  <OnChange name={field}>
    {async (value) => {
      setFieldData(field, {
        warning: await validator(value),
      });
    }}
  </OnChange>
);

export const AdminResourceForm = ({ config, mutators, isNew = false }: any) => {
  return (
    <>
      <SaveCancel isNew={isNew} config={config} />

      <div style={{ maxWidth: "950px" }}>
        <Paper className="p-md">
          <AdminTabs config={config} isNew={isNew} mutators={mutators} />
        </Paper>

        <SaveCancel isNew={isNew} config={config} />
      </div>
    </>
  );
};

export const SaveCancel = ({ isNew, config }) => {
  return (
    <div className="my-md">
      <Button variant="contained" color="primary" type="submit">
        {isNew ? "Create" : "Save"}
      </Button>
      <ButtonLink className="ml-md" variant="outlined" color="primary" href={config.path}>
        Cancel
      </ButtonLink>
    </div>
  );
};

const calculateTabs = (config: AdminConfig) => {
  const allTabs = config.fields.map((f) => f.tab).filter((x) => x);
  const defaultTab = allTabs.length > 0 ? allTabs[0] : undefined;
  const tabs = [...new Set(allTabs)];

  return { tabs, defaultTab };
};

type AdminTabsProps = {
  config: AdminConfig;
  isNew: boolean;
  mutators: any;
};
const AdminTabs: React.FC<AdminTabsProps> = ({ config, isNew, mutators }) => {
  const { tabs, defaultTab } = useMemo(() => calculateTabs(config), [config]);
  const [value, setValue] = useState(0);

  const hasTabs = Boolean(tabs) && tabs.length > 1;

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const fieldsForTab = useCallback(
    (tab: string | undefined) => {
      if (!hasTabs) {
        return config.fields;
      }
      return config.fields.filter(
        (f) => f.tab == tab || (f.tab === undefined && tab == defaultTab),
      );
    },
    [config.fields, hasTabs, defaultTab],
  );

  const renderFields = (fields: FieldConfig[]) => {
    return fields.map((f) => (
      <InputRow key={f.field} field={f} isNew={isNew} mutators={mutators} />
    ));
  };

  if (hasTabs) {
    const tab = tabs[value];
    return (
      <>
        <Tabs value={value} onChange={handleChange} className="mb-lg">
          {tabs.map((t) => {
            return <Tab label={t} key={t} />;
          })}
        </Tabs>

        {renderFields(fieldsForTab(tab))}
      </>
    );
  }

  return <>{renderFields(fieldsForTab(undefined))}</>;
};

const InputRow = ({ field, isNew, mutators }) => {
  const f = inputField(field, isNew, mutators);
  return f ? <div className="my-md">{f}</div> : null;
};

const inputField = (field, isNew, mutators) => {
  const type = field?.input?.type || field?.type || "text";
  const showNew = field?.show?.new !== false;
  const showEdit = field?.show?.edit !== false;

  const show = isNew ? showNew : showEdit;

  if (!show) {
    return null;
  }

  switch (type) {
    case "text":
      return <InputRowText field={field} mutators={mutators} />;
    case "number":
      return <InputRowText field={field} mutators={mutators} />;
    case "date":
      return <InputRowDate field={field} />;
    case "autocomplete":
      return <InputRowAutocomplete field={field} />;
    case "select":
      return <InputRowSelect field={field} />;
    case "relationship":
      return <InputRowText field={field} accessor="label" mutators={mutators} />;
    case "checkbox":
      return <InputRowCheckbox field={field} />;
    case "heading":
      return <h3>{field.label}</h3>;
    case "wysiwyg":
      return <WYSIWYGInput field={field} />;
    case "custom.vendors.retentionReasons":
      return <CustomRetentionReasonSelect field={field} />;
    case "custom.vendors.processingActivity":
      return <CustomProcessingActivitySelect field={field} />;
    case "custom.vendors.logo":
      return <VendorLogo field={field} />;
    case "custom.organizations.clientOrganizations":
      return <ClientOrganizations field={field} />;
    case "custom.mapfields":
      return <MapFields field={field} />;
    default:
      console.warn("unknown field type: ", type, field);
      return null;
  }
};

const InputRowText = ({ field, accessor = null, mutators }) => {
  const isrelationship = field?.type == "relationship";
  const readonly = field?.input?.readonly || isrelationship || false;
  const fullfield = accessor ? `${field.field}.${accessor}` : field.field;
  const warn = !field.warning
    ? null
    : composeValidators(...field.warning.map((warnConfig) => getValidatorFn(field, warnConfig)));
  const validate = !field.validation
    ? null
    : composeValidators(
        ...field.validation.map((validatorConfig) => getValidatorFn(field, validatorConfig)),
      );
  const isRequired = field.validation?.filter(({ type }) => type === "required").length > 0;
  return (
    <>
      <Field name={field.field} validate={validate}>
        {({ meta }) => {
          const extraConfig: Record<string, string> = {};
          // We should only override the helperText for warning state
          if (!meta.error && meta.data.warning) {
            extraConfig.helperText = meta.data.warning;
          }
          return (
            <div>
              <Text
                size="small"
                variant="outlined"
                field={fullfield}
                label={field.label}
                type={field?.input?.type ?? field?.type}
                multiline={field?.input?.multiline}
                rows={field?.input?.rows}
                disabled={readonly}
                required={isRequired}
                className={clsx({ "input-warn": meta.data.warning })}
                {...extraConfig}
              />
            </div>
          );
        }}
      </Field>
      {warn && <AsyncWarning field={fullfield} validator={warn} mutators={mutators} />}
    </>
  );
};

const InputRowDate = ({ field }) => {
  return (
    <DatePicker
      // @ts-ignore
      size="small"
      variant="dialog"
      field={field.field}
      label={field.label}
      disabled={field?.input?.readonly || false}
    />
  );
};

const InputRowAutocomplete = ({ field }) => {
  const options = useField(field.input.valuesField).input.value || [];
  const valueF = useField(field.field);

  const fieldValue = valueF.input.value;
  const selectedOption = options.find((o) => o.value === fieldValue || o === fieldValue);
  return (
    <Autocomplete
      freeSolo
      id={`${field.field}-autocomplete`}
      options={options}
      value={selectedOption ?? ""}
      isOptionEqualToValue={(o, v) => o === v || o?.value === v}
      onChange={(e, selected) => {
        if (typeof selected === "object" && "value" in selected) {
          valueF.input.onChange(selected?.value);
        } else {
          valueF.input.onChange(selected);
        }
      }}
      renderInput={(params) => {
        return (
          <TextField
            {...params}
            id={field.field}
            fullWidth={true}
            variant="outlined"
            label={field.label}
            InputProps={{ ...params.InputProps }}
          />
        );
      }}
    />
  );
};

const InputRowCheckbox = ({ field }) => {
  const f = useField(field.field, { type: "checkbox" });

  return (
    <FormGroup>
      <FormControlLabel control={<Checkbox {...f.input} />} label={field.label} />
    </FormGroup>
  );
};
