import React, { useContext } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import clsx from "clsx";

import * as env from "../lib/env.json";
import { config } from "../lib/adminConfig";
import { OmniBarContext } from "../../js/common/hooks/useOmniBar";
import { useActionRequest } from "../../js/common/hooks/api";
import { OrganizationPublicId } from "../../js/common/service/server/Types";
import { Api } from "../../js/common/service/Api";
import { Success as RequestsSuccess, Error as RequestsError } from "../../js/polaris/copy/requests";

export const AdminNav = () => {
  const { openOmniBar } = useContext(OmniBarContext);
  const { fetch: updateActiveOrganizationSelf } = useActionRequest({
    api: (organizationId: OrganizationPublicId) =>
      Api.user.updateSelfActiveOrganization(organizationId),
    notifySuccess: true,
    messages: {
      forceError: RequestsError.SetActiveOrganization,
      success: RequestsSuccess.SetActiveOrganization,
    },
  });

  const switchOrg = async (e: any) => {
    e.preventDefault();
    openOmniBar({
      onClose: (data) => {
        if (data?.organization) {
          updateActiveOrganizationSelf(data.organization);
        }
      },
    });
  };

  return (
    <div className="primary-nav primary-nav--admin">
      <div className="primary-nav__logo">
        <h2>Polaris Admin</h2>
      </div>

      <div className="primary-nav__content">
        {Object.entries(config).map(([key, val]) => {
          return (
            <div className={`primary-nav__link`} key={key}>
              <NavLink to={`${val.path}`} label={val.label} />
            </div>
          );
        })}
      </div>

      <div className="primary-nav__bottom">
        <div className="primary-nav__bottom-links">
          <a className="primary-nav__bottom-link" href="#" onClick={switchOrg}>
            Switch Organization
          </a>

          <a className="primary-nav__bottom-link" href={env.polaris}>
            Back to Polaris
          </a>
        </div>
      </div>
    </div>
  );
};

const NavLink = ({ to, label, isActive = null }) => {
  const router = useRouter();
  const active = isActive ? isActive(router.pathname) : router.pathname == to;
  return (
    <Link href={to}>
      <a className={clsx({ active })}>{label}</a>
    </Link>
  );
};
