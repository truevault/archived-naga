import React from "react";
import { PageWrapper } from "../../js/common/layout/PageWrapper";
import { PageContent } from "../../js/common/layout/PageContent";
import { PageHeader } from "../../js/common/layout/PageHeader";
import { PrivatePage } from "./PrivatePage";
import { AdminNav } from "./AdminNav";
import { OmniBarProvider } from "../../js/common/hooks/useOmniBar";
import { OmniBar } from "../../js/polaris/components/dialog/OmniBar";

export const AdminPageWrapper = ({ header, headerActions = null, children }) => {
  return (
    <PageWrapper>
      <PrivatePage>
        <OmniBarProvider>
          <OmniBar></OmniBar>
          <AdminNav />
          <PageContent fullWidth admin>
            <PageHeader header={header}>{headerActions}</PageHeader>
            {children}
          </PageContent>
        </OmniBarProvider>
      </PrivatePage>
    </PageWrapper>
  );
};
