import React from "react";
import { useSelf } from "../../js/common/hooks/useSelf";
import { useEffect } from "react";
import { NetworkRequest } from "../../js/common/models";
import { PageLoading } from "../../js/common/components/Loading";

import * as env from "../lib/env.json";

export const PrivatePage = ({ children }) => {
  const [self, selfRequest] = useSelf();

  if (!NetworkRequest.areFinished(selfRequest)) {
    return <PageLoading />;
  }

  if (!self || !self.globalRole || self.globalRole !== "ADMIN") {
    return <Redirect to={`${env.polaris}/login?admin=${env.self}`} />;
  }

  return children;
};

const Redirect = ({ to }) => {
  useEffect(() => {
    window.location.href = to;
  }, []);

  return <p>Redirecting...</p>;
};
