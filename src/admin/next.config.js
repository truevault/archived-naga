// eslint-disable-next-line
const webpack = require("webpack");
// eslint-disable-next-line
const webpackVars = require("../../webpack.vars");

// eslint-disable-next-line
module.exports = {
  typescript: {
    ignoreBuildErrors: true,
  },
  experimental: {
    externalDir: true,
  },
  webpack: function (config, _options) {
    config.plugins.push(new webpack.DefinePlugin(webpackVars.globals));
    return config;
  },
};
