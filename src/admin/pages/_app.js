// import App from 'next/app'
import MomentUtils from "@date-io/moment";
import AdapterMoment from "@mui/lab/AdapterMoment";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { StyledEngineProvider, ThemeProvider } from "@mui/material/styles";
import { SnackbarProvider } from "notistack";
import PropTypes from "prop-types";
import React from "react";
import { Provider } from "react-redux";
import { snackbarAnchor } from "../../js/common/components/SnackbarConfig";
import { theme } from "../../js/common/root/generalTheme";
import { store } from "../../js/common/root/redux";
import { setBaseURL } from "../../js/common/service/Api";
import "../../main/resources/static/assets/main.css";
import "../../main/resources/static/assets/redactor/redactorx.min.css";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import * as env from "../lib/env.json";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

setBaseURL(env.polaris);

function AdminApp({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <SnackbarProvider anchorOrigin={snackbarAnchor}>
          <StyledEngineProvider injectFirst>
            <ThemeProvider theme={theme}>
              <LocalizationProvider dateAdapter={AdapterMoment} utils={MomentUtils}>
                <Component {...pageProps} />
              </LocalizationProvider>
            </ThemeProvider>
          </StyledEngineProvider>
        </SnackbarProvider>
      </Provider>
    </QueryClientProvider>
  );
}

AdminApp.propTypes = {
  Component: PropTypes.any,
  pageProps: PropTypes.any,
};

export default AdminApp;
