import { AdminRoutes } from "../adminRoutes";

import Router from "next/router";
import { useEffect } from "react";

const IndexPage = () => {
  useEffect(() => {
    Router.push(AdminRoutes.Vendors);
  }, []);

  return null;
};

export default IndexPage;
