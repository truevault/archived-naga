import React from "react";
import { AdminNewPage } from "../../components/pages/AdminNewPage";
import { config } from "../../lib/adminConfig";

const NewProcessingActivity = () => (
  <AdminNewPage
    config={config.defProcessingActivity}
    data={{ mandatoryLawfulBases: [], optionalLawfulBases: [] }}
  />
);

NewProcessingActivity.displayName = "NewProcessingActivity";

export default NewProcessingActivity;
