import React from "react";
import { AdminEditPage } from "../../components/pages/AdminEditPage";
import { config } from "../../lib/adminConfig";

const EditProcessingActivity = () => <AdminEditPage config={config.defProcessingActivity} />;

EditProcessingActivity.displayName = "EditProcessingActivity";

export default EditProcessingActivity;
