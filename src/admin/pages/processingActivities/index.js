import React from "react";
import { AdminIndexPage } from "../../components/pages/AdminIndexPage";
import { config } from "../../lib/adminConfig";

const IndexProcessingActivities = () => <AdminIndexPage config={config.defProcessingActivity} />;

IndexProcessingActivities.displayName = "IndexProcessingActivities";

export default IndexProcessingActivities;
