import React, { useMemo } from "react";
import { PageLoading } from "../../../js/common/components/Loading";
import { useDataRequest } from "../../../js/common/hooks/api";
import { NetworkRequest } from "../../../js/common/models";
import { AdminEditPage } from "../../components/pages/AdminEditPage";
import { config } from "../../lib/adminConfig";
import { axios } from "../../lib/net";

const EditCookieRecommendations = () => {
  const getVendors = async () => {
    return (await axios.get(config.vendors.url)).data;
  };

  const { request, result: vendors } = useDataRequest({
    queryKey: ["vendors"],
    api: getVendors,
  });

  const vendorOptions = useMemo(
    () => vendors?.map((v) => ({ label: v.name, value: v.id })),
    [vendors],
  );

  if (!NetworkRequest.areFinished(request)) {
    return <PageLoading />;
  }

  return <AdminEditPage config={config.cookieRecommendations} data={{ vendorOptions }} />;
};

EditCookieRecommendations.displayName = "EditCookieRecommendations";

export default EditCookieRecommendations;
