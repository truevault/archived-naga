import React from "react";
import { AdminIndexPage } from "../../components/pages/AdminIndexPage";
import { config } from "../../lib/adminConfig";

const IndexCookieRecommendations = () => <AdminIndexPage config={config.cookieRecommendations} />;

IndexCookieRecommendations.displayName = "IndexCookieRecommendations";

export default IndexCookieRecommendations;
