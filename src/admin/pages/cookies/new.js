import React, { useMemo } from "react";
import { AdminNewPage } from "../../components/pages/AdminNewPage";
import { PageLoading } from "../../../js/common/components/Loading";
import { NetworkRequest } from "../../../js/common/models";
import { useDataRequest } from "../../../js/common/hooks/api";
import { config } from "../../lib/adminConfig";
import { axios } from "../../lib/net";

const NewCookieRecommendation = () => {
  const getVendors = async () => {
    return (await axios.get(config.vendors.url)).data;
  };

  const { request, result: vendors } = useDataRequest({
    queryKey: ["vendors"],
    api: getVendors,
  });

  const vendorOptions = useMemo(
    () => vendors?.map((v) => ({ label: v.name, value: v.id })),
    [vendors],
  );

  if (!NetworkRequest.areFinished(request)) {
    return <PageLoading />;
  }

  return (
    <AdminNewPage config={config.cookieRecommendations} data={{ vendorOptions, categories: [] }} />
  );
};

NewCookieRecommendation.displayName = "NewCookieRecommendation";

export default NewCookieRecommendation;
