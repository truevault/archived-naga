import React from "react";
import { AdminNewPage } from "../../components/pages/AdminNewPage";
import { PageLoading } from "../../../js/common/components/Loading";
import { NetworkRequest } from "../../../js/common/models";
import { useDataRequest } from "../../../js/common/hooks/api";
import { config } from "../../lib/adminConfig";
import { axios } from "../../lib/net";

const NewVendors = () => {
  const getNewData = async () => {
    return (await axios.get(config.vendors.url + "new")).data;
  };

  const { request, result } = useDataRequest({
    queryKey: ["newVendor"],
    api: getNewData,
  });

  if (!NetworkRequest.areFinished(request)) {
    return <PageLoading />;
  }

  return <AdminNewPage config={config.vendors} data={result} />;
};

NewVendors.displayName = "NewVendors";

export default NewVendors;
