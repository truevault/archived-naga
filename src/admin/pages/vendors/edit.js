import React from "react";
import { AdminEditPage } from "../../components/pages/AdminEditPage";
import { config } from "../../lib/adminConfig";

const EditVendors = () => <AdminEditPage config={config.vendors} />;

EditVendors.displayName = "EditVendors";

export default EditVendors;
