import React from "react";
import { AdminIndexPage } from "../../components/pages/AdminIndexPage";
import { config } from "../../lib/adminConfig";

const IndexVendors = () => <AdminIndexPage config={config.vendors} />;

IndexVendors.displayName = "IndexVendors";

export default IndexVendors;
