import React from "react";
import { AdminNewPage } from "../../components/pages/AdminNewPage";
import { config } from "../../lib/adminConfig";

const NewPrivacyCenter = () => <AdminNewPage config={config.privacyCenters} />;

NewPrivacyCenter.displayName = "NewPrivacyCenter";

export default NewPrivacyCenter;
