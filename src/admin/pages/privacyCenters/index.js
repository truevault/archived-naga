import React from "react";
import { AdminIndexPage } from "../../components/pages/AdminIndexPage";
import { config } from "../../lib/adminConfig";

const IndexPrivacyCenters = () => <AdminIndexPage config={config.privacyCenters} />;

IndexPrivacyCenters.displayName = "IndexPrivacyCenters";

export default IndexPrivacyCenters;
