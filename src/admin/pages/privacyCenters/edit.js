import React from "react";
import { AdminEditPage } from "../../components/pages/AdminEditPage";
import { config } from "../../lib/adminConfig";

const EditPrivacyCenters = () => <AdminEditPage config={config.privacyCenters} />;

EditPrivacyCenters.displayName = "EditPrivacyCenters";

export default EditPrivacyCenters;
