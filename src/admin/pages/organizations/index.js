import React from "react";
import { AdminIndexPage } from "../../components/pages/AdminIndexPage";
import { config } from "../../lib/adminConfig";
const IndexOrganization = () => <AdminIndexPage config={config.organizations} />;

IndexOrganization.displayName = "IndexOrganization";

export default IndexOrganization;
