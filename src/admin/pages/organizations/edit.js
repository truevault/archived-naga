import React from "react";
import { AdminEditPage } from "../../components/pages/AdminEditPage";
import { config } from "../../lib/adminConfig";

const EditOrganization = () => <AdminEditPage config={config.organizations} />;

EditOrganization.displayName = "EditOrganization";

export default EditOrganization;
