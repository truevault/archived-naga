import React from "react";
import { AdminNewPage } from "../../components/pages/AdminNewPage";
import { config } from "../../lib/adminConfig";

const NewOrganization = () => <AdminNewPage config={config.organizations} />;

NewOrganization.displayName = "NewOrganization";

export default NewOrganization;
