export type FieldValidation = any;
export type FieldWarning = any;
export type FieldInput = any;
export type FieldShow = {
  new?: boolean;
  edit?: boolean;
};
export type FieldConfig = {
  field: string;
  label: string;
  type?: string;
  index?: boolean;
  indexType?: string;
  show?: FieldShow;
  tab?: string;
  validation?: FieldValidation[];
  warning?: FieldWarning[];
  input?: FieldInput;
};

export type AdminCan = {
  create?: boolean;
  delete?: boolean;
};

export type AdminCustomActions = {
  returnToGetCompliant?: boolean;
  publishCmpConfigs?: boolean;
};

export type AdminConfig = {
  label: string;
  singular: string;
  path: string;
  url: string;
  can?: AdminCan;
  customActions?: AdminCustomActions;
  fields: FieldConfig[];
};

const vendorsConfig: AdminConfig = {
  label: "Vendors",
  singular: "Vendor",
  path: "/vendors",
  url: "/admin/vendors/",
  fields: [
    {
      field: "name",
      label: "Name",
      index: true,
      validation: [{ type: "required" }],
      warning: [
        {
          type: "unique",
          existingValuesField: "existingNames",
          compareFn: (existing) => (value) =>
            existing.map((it) => it.toLowerCase()).includes(value.toLowerCase())
              ? "Vendor already exists with this name"
              : undefined,
        },
      ],
      tab: "Basic Information",
    },
    {
      field: "aliases",
      label: "Aliases",
      tab: "Basic Information",
    },
    {
      field: "url",
      label: "URL",
      index: true,
      validation: [{ type: "required" }, { type: "format", format: "URL" }],
      tab: "Basic Information",
    },
    {
      field: "category",
      label: "CCPA Category",
      index: true,
      input: { type: "autocomplete", valuesField: "categories" },
      tab: "Basic Information",
    },
    {
      field: "disclosureSourceCategory",
      label: "CCPA Disclosure Category Name",
      index: true,
      input: { type: "autocomplete", valuesField: "categories" },
      tab: "Basic Information",
    },
    {
      field: "gdprProcessingPurpose",
      label: "Processing Purpose (GDPR)",
      input: {
        type: "custom.vendors.processingActivity",
      },
      tab: "Basic Information",
    },
    {
      field: "gdprMultiPurposeVendor",
      label: "Multi-purpose vendor",
      input: { type: "checkbox" },
      tab: "Basic Information",
    },
    {
      field: "isDataRecipient",
      label: "Data Recipient",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Basic Information",
    },
    {
      field: "isDataSource",
      label: "Data Source",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Basic Information",
    },
    {
      field: "isPlatform",
      label: "Platform",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Basic Information",
    },
    {
      field: "isStandalone",
      label: "Standalone",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Basic Information",
    },
    {
      field: "respectsPlatformRequests",
      label: "Respects Platform Requests",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Basic Information",
    },
    {
      field: "vendorKey",
      label: "Vendor Key",
      tab: "Basic Information",
    },
    {
      field: "logoUrl",
      label: "Logo",
      index: true,
      indexType: "image",
      show: { new: false, edit: false },
      tab: "Basic Information",
    },
    {
      field: "logoFilename",
      label: "Logo Filename",
      input: { type: "custom.vendors.logo" },
      tab: "Basic Information",
    },

    {
      field: "isWalletService",
      label: "Wallet Service",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Basic Information",
    },

    {
      field: "isMigrationOnly",
      label: "Migration Only",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Basic Information",
    },

    {
      field: "generalGuidance",
      label: "General Guidance",
      input: {
        type: "wysiwyg",
        multiline: true,
        rows: 5,
      },
      tab: "Data Processed",
    },
    {
      field: "specialCircumstancesGuidance",
      label: "Special Circumstances Guidance",
      input: {
        type: "wysiwyg",
        multiline: true,
        rows: 5,
      },
      tab: "Data Processed",
    },

    {
      field: "recommendedPersonalInformationCategories",
      label: "Recommended Personal Information Categories",
      input: {
        type: "custom.mapfields",
      },
      tab: "Data Processed",
    },

    {
      field: "header--ccpa",
      label: "CCPA Settings",
      type: "heading",
      tab: "Documentation",
    },

    {
      field: "serviceProviderRecommendation",
      label: "Service Provider Recommendation",
      index: true,
      input: {
        type: "select",
        options: [
          { value: "SERVICE_PROVIDER", label: "Service Provider" },
          { value: "THIRD_PARTY", label: "Third Party" },
          { value: "NONE", label: "None" },
        ],
      },
      tab: "Documentation",
    },
    {
      field: "lastReviewed",
      label: "Last Reviewed",
      index: true,
      show: { new: false },
      input: {
        type: "date",
      },
      tab: "Documentation",
    },

    {
      field: "serviceProviderLanguageUrl",
      label: "Service Provider Language URL",
      validation: [{ type: "format", format: "URL" }],
      tab: "Documentation",
    },

    {
      field: "dpaApplicable",
      label: "DPA Applicable",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Documentation",
    },
    {
      field: "dpaUrl",
      label: "DPA URL",
      validation: [{ type: "format", format: "URL" }],
      tab: "Documentation",
    },
    {
      field: "header--gdpr",
      label: "GDPR Settings",
      type: "heading",
      tab: "Documentation",
    },
    {
      field: "gdprProcessorRecommendation",
      label: "Processor Recommendation",
      input: {
        type: "select",
        options: [{ value: "Processor" }, { value: "Controller" }],
      },
      tab: "Documentation",
    },
    {
      field: "gdprLastReviewed",
      label: "Last Reviewed",
      index: true,
      show: { new: false },
      input: {
        type: "date",
      },
      tab: "Documentation",
    },
    {
      field: "gdprProcessorLanguageUrl",
      label: "Processor Language URL",
      validation: [{ type: "format", format: "URL" }],
      tab: "Documentation",
    },
    {
      field: "gdprDpaApplicable",
      label: "DPA Applicable",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Documentation",
    },
    {
      field: "gdprDpaUrl",
      label: "DPA URL",
      validation: [{ type: "format", format: "URL" }],
      tab: "Documentation",
    },
    {
      field: "gdprInternationalDataTransferRulesApply",
      label: "International Data Transfer Rules Apply",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Documentation",
    },
    {
      field: "gdprHasSccRecommendation",
      label: "SCC Recommendation",
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
      tab: "Documentation",
    },
    {
      field: "gdprSccDocumentationUrl",
      label: "SCC Documentation URL",
      validation: [{ type: "format", format: "URL" }],
      tab: "Documentation",
    },
    {
      field: "gdprLawfulBasis",
      label: "Lawful Basis",
      input: {
        type: "select",
        options: [
          { value: "Legitimate interest" },
          { value: "Fulfill a contract" },
          { value: "Comply with a legal obligation" },
          { value: "Consent" },
        ],
      },
      tab: "Documentation",
    },

    {
      field: "email",
      label: "Privacy Request Email",
      validation: [{ type: "format", format: "email" }],
      tab: "Privacy Requests",
    },

    {
      field: "defaultRetentionReasons",
      label: "Default Exceptions to Deletion",
      input: {
        type: "custom.vendors.retentionReasons",
        multiple: true,
      },
      tab: "Privacy Requests",
    },

    {
      field: "deletionInstructions",
      label: "Deletion Instructions",
      input: {
        type: "wysiwyg",
        multiline: true,
        rows: 5,
      },
      tab: "Privacy Requests",
    },
    {
      field: "optOutInstructions",
      label: "Opt-Out Instructions",
      input: {
        type: "wysiwyg",
        multiline: true,
        rows: 5,
      },
      tab: "Privacy Requests",
    },
    {
      field: "gdprAccessInstructions",
      label: "Access Instructions",
      input: {
        type: "wysiwyg",
        multiline: true,
        rows: 5,
      },
      tab: "Privacy Requests",
    },
  ],
};

const organizationsConfig: AdminConfig = {
  label: "Organizations",
  singular: "Organization",
  path: "/organizations",
  url: "/admin/organizations/",
  can: {
    delete: true,
  },
  customActions: {
    returnToGetCompliant: true,
    publishCmpConfigs: true,
  },
  fields: [
    {
      field: "publicId",
      label: "Public ID",
      input: { type: "text", readonly: true },
      index: true,
    },
    { field: "name", label: "Name", index: true, validation: [{ type: "required" }] },
    { field: "logoUrl", label: "Logo URL" },
    { field: "faviconUrl", label: "Favicon URL" },
    { field: "privacyPolicyUrl", label: "Privacy Policy URL" },
    { field: "requestTollFreePhoneNumber", label: "Request Phone Number" },
    { field: "requestFormUrl", label: "Request Form URL" },
    {
      field: "clientOrganizations",
      label: "Client Organizations",
      input: { type: "custom.organizations.clientOrganizations" },
      show: { new: false },
    },
    {
      field: "phase",
      label: "Phase",
      validation: [{ type: "required" }],
      input: {
        type: "select",
        options: [
          { value: "SETUP", label: "Setup" },
          { value: "GET_COMPLIANT", label: "Get Compliant" },
          { value: "STAY_COMPLIANT", label: "Stay Compliant" },
        ],
      },
    },
    {
      field: "trainingComplete",
      label: "Training Complete",
      validation: [{ type: "required" }],
      input: {
        type: "select",
        defaultFalse: true,
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
    },
    {
      field: "featureGdpr",
      label: "GDPR",
      validation: [{ type: "required" }],
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
    },
    {
      field: "featureMultistate",
      label: "Multistate",
      validation: [{ type: "required" }],
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
    },
  ],
};

const privacyCentersConfig: AdminConfig = {
  label: "Privacy Centers",
  singular: "Privacy Center",
  path: "/privacyCenters",
  url: "/admin/privacyCenters/",
  can: {
    create: false,
  },
  fields: [
    {
      field: "publicId",
      label: "Public ID",
      input: { type: "text", readonly: true },
      index: true,
    },
    { field: "name", label: "Name", index: true },
    {
      field: "organization",
      label: "Organization",
      type: "relationship",
      index: true,
    },
    { field: "defaultSubdomain", label: "Default Subdomain" },
    { field: "faviconUrl", label: "Favicon URL" },
    {
      field: "customUrlStatus",
      label: "Custom URL Status",
      input: {
        type: "select",
        options: [
          { value: "VERIFYING", label: "Verifying" },
          { value: "ACTIVE", label: "Active" },
        ],
      },
    },
    { field: "customUrl", label: "Custom URL" },
    { field: "cloudfrontDistribution", label: "Cloudfront Distribution" },

    { field: "logoUrl", label: "Logo URL" },
    { field: "privacyPolicyUrl", label: "Privacy Policy URL" },
    { field: "requestTollFreePhoneNumber", label: "Request Phone Number" },
    { field: "requestFormUrl", label: "Request Form URL" },
    { field: "requestEmail", label: "Request Email" },
  ],
};

const cookieRecommendationsConfig: AdminConfig = {
  label: "Cookie Recommendations",
  singular: "Cookie Recommendation",
  path: "/cookies",
  url: "/admin/cookies/",
  can: {
    create: true,
  },
  fields: [
    { field: "name", label: "Cookie Name", index: true, validation: [{ type: "required" }] },
    { field: "domain", label: "Cookie Domain", index: true, validation: [{ type: "required" }] },
    {
      field: "categories",
      label: "Cookie Category",
      input: {
        type: "select",
        multiple: true,
        options: [
          { value: "ADVERTISING", label: "Advertising" },
          { value: "ANALYTICS", label: "Analytics" },
          { value: "FUNCTIONALITY", label: "Functionality" },
          { value: "PERSONALIZATION", label: "Personalization" },
          { value: "SECURITY", label: "Security" },
        ],
      },
      index: true,
    },
    {
      field: "vendorId",
      label: "Vendor",
      input: { type: "autocomplete", valuesField: "vendorOptions" },
      index: false,
    },
    {
      field: "vendorName",
      label: "Vendor",
      input: { type: "relationship" },
      show: { new: false, edit: false },
      index: true,
    },
    {
      field: "notes",
      label: "Notes",
      input: {
        type: "wysiwyg",
        multiline: true,
        rows: 5,
      },
      index: false,
    },
  ],
};

const defProcessingActivityConfig: AdminConfig = {
  label: "Processing Activities",
  singular: "Processing Activity",
  path: "/processingActivities",
  url: "/admin/defaultProcessingActivities/",
  fields: [
    {
      field: "id",
      label: "ID",
      input: { type: "text", readonly: true },
    },
    { field: "name", label: "Name", index: true, validation: [{ type: "required" }] },
    { field: "slug", label: "Slug", index: true },
    { field: "displayOrder", label: "Order", index: true, input: { type: "number" } },
    {
      field: "deprecated",
      label: "Deprecated",
      index: true,
      input: {
        type: "select",
        options: [
          { value: true, label: "Yes" },
          { value: false, label: "No" },
        ],
      },
    },
    { field: "helpText", label: "Help Text" },
    { field: "materialIconKey", label: "Material Icon Key" },
    {
      field: "mandatoryLawfulBases",
      label: "Mandatory Lawful Bases",
      validation: [{ type: "required" }],
      index: true,
      input: {
        type: "select",
        multiple: true,
        options: [
          { value: "COMPLY_WITH_LEGAL_OBLIGATIONS", label: "Complying with legal obligations" },
          { value: "FULFILL_CONTRACTS", label: "Fulfilling contracts" },
          { value: "LEGITIMATE_INTERESTS", label: "Legitimate interests" },
          { value: "CONSENT", label: "Consent" },
          { value: "PUBLIC_INTEREST", label: "Public interest" },
          { value: "VITAL_INTEREST_OF_INDIVIDUAL", label: "Vital interest of the individual" },
        ],
      },
    },

    {
      field: "optionalLawfulBases",
      label: "Optional Lawful Bases",
      validation: [{ type: "required" }],
      index: true,
      input: {
        type: "select",
        multiple: true,
        options: [
          { value: "COMPLY_WITH_LEGAL_OBLIGATIONS", label: "Complying with legal obligations" },
          { value: "FULFILL_CONTRACTS", label: "Fulfilling contracts" },
          { value: "LEGITIMATE_INTERESTS", label: "Legitimate interests" },
          { value: "CONSENT", label: "Consent" },
          { value: "PUBLIC_INTEREST", label: "Public interest" },
          { value: "VITAL_INTEREST_OF_INDIVIDUAL", label: "Vital interest of the individual" },
        ],
      },
    },
  ],
};

export const config = {
  vendors: vendorsConfig,
  organizations: organizationsConfig,
  privacyCenters: privacyCentersConfig,
  cookieRecommendations: cookieRecommendationsConfig,
  defProcessingActivity: defProcessingActivityConfig,
};
