import axiosLib from "axios";
import * as env from "./env.json";

export const axios = axiosLib.create({
  baseURL: env.polaris,
  withCredentials: true,
});

export const externalAxios = axiosLib.create({
  timeout: 10000,
});
