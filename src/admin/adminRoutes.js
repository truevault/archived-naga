export const AdminRoutes = {
  Root: "/",
  Organizations: "/organizations",
  Vendors: "/vendors",
  Cookies: "/cookies",
  ProcessingActivities: "/processingActivities",
};
