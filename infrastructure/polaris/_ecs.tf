
# Latest Amazon Linux 2 ECS Optimized image. More details or other options are available here:
# https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
data "aws_ssm_parameter" "ecs_ami" {
  # To view this parameter in AWS console visit:
  # https://us-west-2.console.aws.amazon.com/systems-manager/parameters/aws/service/ecs/optimized-ami/amazon-linux-2/recommended/image_id/description?region=us-west-2#
  name = "/aws/service/ecs/optimized-ami/amazon-linux-2/recommended/image_id"
}


resource "aws_ecs_cluster" "polaris" {
  name = local.prefix

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_task_definition" "polaris" {
  family = local.prefix

  # This is setup with a dummy container useful for testing http access
  container_definitions = templatefile("${path.module}/templates/base-container-definition.tmpl", { container_name = local.workspace.polaris_container_name })

  requires_compatibilities = ["EC2"]
  network_mode             = "bridge"
  execution_role_arn       = aws_iam_role.ecs_execution.arn
  task_role_arn            = aws_iam_role.ecs_task.arn

  # Changes to the Container Definition are ignored so the deployed version is managed by CI
  lifecycle {
    ignore_changes = [container_definitions]
  }
}

resource "aws_ecs_service" "polaris" {
  name            = local.prefix
  cluster         = aws_ecs_cluster.polaris.id
  task_definition = aws_ecs_task_definition.polaris.arn

  deployment_minimum_healthy_percent = local.workspace.deployment_minimum_healthy_percent
  deployment_maximum_percent         = 100

  scheduling_strategy    = "DAEMON"
  enable_execute_command = true

  load_balancer {
    container_name   = local.workspace.polaris_container_name
    container_port   = 80
    target_group_arn = aws_lb_target_group.https.arn
  }

  deployment_circuit_breaker {
    enable   = true
    rollback = true
  }

  depends_on = [
    aws_lb_listener.https
  ]

  lifecycle {
    ignore_changes = [desired_count, task_definition]
  }
}
