# Secrets Manager IAM Access Policy
data "aws_iam_policy_document" "secrets" {
  statement {
    effect = "Allow"

    actions = [
      "secretsmanager:GetSecretValue"
    ]

    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = [
      "arn:aws:secretsmanager:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:secret:${terraform.workspace}/${local.name}/*"
    ]
  }
}

resource "aws_iam_policy" "secrets" {
  name   = "${local.prefix}-secrets"
  path   = "/"
  policy = data.aws_iam_policy_document.secrets.json
}
