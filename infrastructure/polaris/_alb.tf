resource "aws_alb" "polaris" { #tfsec:ignore:aws-elb-alb-not-public
  name               = local.prefix
  load_balancer_type = "application"
  subnets            = data.aws_subnets.public_subnets.ids

  security_groups = [
    aws_security_group.lb.id
  ]

  drop_invalid_header_fields = true
  access_logs {
    bucket  = local.workspace.alb_logs_bucket
    prefix  = "${terraform.workspace}/web"
    enabled = local.workspace.alb_logs_enabled
  }
}

resource "aws_security_group" "lb" {
  name_prefix = "${local.prefix}-lb"
  vpc_id      = data.aws_vpc.truevault.id
  description = "${local.name} ${terraform.workspace} Load Balancer"

  ingress {
    description = "Allow all HTTP access from anywhere"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:aws-vpc-no-public-ingress-sgr
  }

  ingress {
    description = "Allow all HTTPs access from anywhere"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:aws-vpc-no-public-ingress-sgr
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:aws-vpc-no-public-egress-sgr
  }

  tags = {
    Name = "${local.prefix}-lb"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_alb.polaris.arn
  port              = 80
  protocol          = "HTTP"

  # Redirect to HTTPS
  default_action {
    type = "redirect"
    redirect {
      status_code = "HTTP_301"
      protocol    = "HTTPS"
      port        = 443
    }
  }

  depends_on = [aws_alb.polaris]
}

resource "aws_lb_target_group" "https" {
  name     = local.prefix
  port     = 80
  protocol = "HTTP"

  vpc_id = data.aws_vpc.truevault.id

  deregistration_delay = 90

  load_balancing_algorithm_type = "least_outstanding_requests"

  health_check {
    protocol            = "HTTP"
    healthy_threshold   = 3
    interval            = 15
    timeout             = 5
    unhealthy_threshold = 5
    matcher             = "200"
    path                = "/actuator/health"
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [aws_alb.polaris]
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_alb.polaris.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-FS-1-2-2019-08"
  certificate_arn   = aws_acm_certificate.polaris.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.https.arn
  }
}


# Additional Certificates
resource "aws_lb_listener_certificate" "polaris" {
  listener_arn    = aws_lb_listener.https.arn
  certificate_arn = aws_acm_certificate.polaris.arn
}

# Rules
resource "aws_lb_listener_rule" "block_ip" {
  # TODO: Replace this rule with a WAF POLARIS-1548
  listener_arn = aws_lb_listener.https.arn
  priority     = 10

  action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      status_code  = "403"
    }
  }

  condition {
    source_ip {
      values = [
        "198.246.249.35/32" #: Blocked due to hitting LB directly ever 5s from 12/30/2022 on. (Registed to Men's Warehouse)
      ]
    }
  }
}

# ------- WAF ------
# TODO: POLARIS-1548 WAF Association
# data "aws_wafv2_web_acl" "main" {
#   name  = "tf-waf-default"
#   scope = "REGIONAL"
# }

# resource "aws_wafv2_web_acl_association" "polaris" {
#   resource_arn = aws_alb.polaris.arn
#   web_acl_arn  = data.aws_wafv2_web_acl.main.arn
# }
