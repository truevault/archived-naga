data "aws_route53_zone" "domain" {
  count        = local.workspace.create_dns_records ? 1 : 0
  name         = local.workspace.tld_name
  private_zone = false
}

resource "aws_route53_record" "polaris_alb" {
  count   = local.workspace.create_dns_records ? 1 : 0
  zone_id = data.aws_route53_zone.domain.0.id
  name    = local.dns_name
  type    = "A"

  alias {
    name                   = aws_alb.polaris.dns_name
    zone_id                = aws_alb.polaris.zone_id
    evaluate_target_health = false
  }

  weighted_routing_policy {
    weight = 100
  }

  set_identifier = "terraform"

  allow_overwrite = true
}
