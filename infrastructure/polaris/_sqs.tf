resource "aws_sqs_queue" "website_audit" {
  name = "website-audit-${terraform.workspace}"
  visibility_timeout_seconds = 45
  redrive_policy = jsonencode({
    "deadLetterTargetArn" = "${local.workspace.sqs_website_audit_arn}-dlq",
    "maxReceiveCount" = 5
  })
}

resource "aws_sqs_queue" "website_audit_dlq" {
  name = "website-audit-${terraform.workspace}-dlq"
}
