# Workspaces: stage, prod

terraform {
  backend "s3" {
    bucket               = "polaris-terraform-state"
    key                  = "polaris.tfstate"
    region               = "us-west-2"
    workspace_key_prefix = "infrastructure/polaris"
  }
}


locals {
  name   = "polaris"
  prefix = "polaris-${terraform.workspace}"
  env = {
    defaults = {
      # TODO: Change to final domain names (polaris)
      subdomain_name         = "${local.prefix}-ecs"
      create_dns_records     = true
      alb_logs_bucket        = "polaris-lb-logs"
      alb_logs_enabled       = true
      keypair                = "tv-polaris"
      polaris_container_name = "web"

      # This should be 0 on single instances environment and 50% on multi instance environment
      deployment_minimum_healthy_percent = 0

      avg_cpu_target = 70.0
      instance_class = "t3a.small"

      desired_capacity   = 1
      max_instance_count = 2
      min_instance_count = 1
    }

    stage = {
      subdomain_name                   = "polaris"
      tld_name                         = "truevaultstaging.com"
      sqs_ppb_arn                      = "arn:aws:sqs:us-west-2:571427946403:ppb-stage"
      sqs_privacy_site_builder_arn     = "arn:aws:sqs:us-east-1:571427946403:polaris-privacy-site-builder-staging"
      sqs_privacy_site_provisioner_arn = "arn:aws:sqs:us-east-1:571427946403:polaris-privacy-site-provisioner-staging"
      sqs_cookie_scanner_arn           = "arn:aws:sqs:us-west-2:571427946403:cookie-scanner-stage"
      sqs_website_audit_arn            = "arn:aws:sqs:us-west-2:571427946403:website-audit-stage"
    }

    prod = {
      create_dns_records               = false
      subdomain_name                   = "polaris"
      tld_name                         = "truevault.com"
      sqs_ppb_arn                      = "arn:aws:sqs:us-west-2:571427946403:ppb-prod"
      sqs_privacy_site_builder_arn     = "arn:aws:sqs:us-east-1:571427946403:polaris-privacy-site-builder-prod"
      sqs_privacy_site_provisioner_arn = "arn:aws:sqs:us-east-1:571427946403:polaris-privacy-site-provisioner-prod"
      sqs_cookie_scanner_arn           = "arn:aws:sqs:us-west-2:571427946403:cookie-scanner-prod"
      sqs_website_audit_arn            = "arn:aws:sqs:us-west-2:571427946403:website-audit-prod"

      instance_class = "t3a.medium"

      desired_capacity   = 2
      max_instance_count = 4
      min_instance_count = 2

      deployment_minimum_healthy_percent = 50
    }
  }
  workspace = merge(local.env.defaults, local.env[terraform.workspace])
  dns_name  = "${local.workspace.subdomain_name}.${local.workspace.tld_name}"

  common_tags = {
    application = local.name,
    environment = terraform.workspace,
    terraform   = "true"
  }
}

# The default provider configuration is us-east-1
provider "aws" {
  region = "us-west-2"

  default_tags {
    tags = local.common_tags
  }
}

# Data
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

data "aws_elb_service_account" "main" {}

data "aws_vpc" "truevault" {
  tags = {
    Name = "TV 10.10.0.0/16"
  }
}

data "aws_subnets" "public_subnets" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.truevault.id]
  }


  tags = {
    Reach = "public"
  }
}

data "aws_subnets" "private_subnets" {

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.truevault.id]
  }

  tags = {
    Reach = "private"
  }
}
