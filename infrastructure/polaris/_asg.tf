resource "aws_security_group" "instance" {
  name_prefix = "${local.prefix}-instance"
  vpc_id      = data.aws_vpc.truevault.id
  description = "${local.name} (${terraform.workspace})"

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:aws-vpc-no-public-egress-sgr
  }

  tags = {
    Name = "${local.prefix}-instance"
  }
}

resource "aws_security_group_rule" "instance_http" {
  description              = "Allow all HTTP traffic from ALB"
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  security_group_id        = aws_security_group.instance.id
  source_security_group_id = aws_security_group.lb.id
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "instance_https" {
  description              = "Allow all HTTPs traffic from ALB"
  type                     = "ingress"
  from_port                = 443
  to_port                  = 443
  security_group_id        = aws_security_group.instance.id
  source_security_group_id = aws_security_group.lb.id
  protocol                 = "tcp"
}

resource "aws_launch_template" "polaris" { #tfsec:ignore:aws-autoscaling-enforce-http-token-imds
  name_prefix            = "${local.prefix}-"
  image_id               = data.aws_ssm_parameter.ecs_ami.value
  instance_type          = local.workspace.instance_class
  key_name               = local.workspace.keypair
  update_default_version = true
  iam_instance_profile {
    name = aws_iam_instance_profile.instance.name
  }

  vpc_security_group_ids = [aws_security_group.instance.id]

  user_data = base64encode(<<EOT
#!/bin/bash
echo "ECS_CLUSTER=${aws_ecs_cluster.polaris.name}" >> /etc/ecs/ecs.config
echo "Installing AWS CLI" >> /var/log/user-data.log
yum install -y aws-cli
EOT
  )


  # This meatadata config is always being detected as a change so terraform is reapplying changes so we are tfsec ignoring with aws-autoscaling-enforce-http-token-imds
  # More Information about the risk we are ignoring:
  # - https://aquasecurity.github.io/tfsec/v1.17.0/checks/aws/autoscaling/enforce-http-token-imds/
  # - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance#metadata-options
  # metadata_options {
  #   http_tokens = "required"
  # }

  tag_specifications {
    resource_type = "instance"
    tags = merge(local.common_tags, {
      Name = "${local.prefix}"
    })
  }
  tag_specifications {
    resource_type = "volume"
    tags = merge(local.common_tags, {
      Name = "${local.prefix}"
    })
  }

  tags = {
    Name = "${local.prefix}"
  }

  depends_on = [aws_ecs_cluster.polaris]
}

resource "aws_autoscaling_group" "polaris" {
  name_prefix         = local.prefix
  desired_capacity    = local.workspace.desired_capacity
  max_size            = local.workspace.max_instance_count
  min_size            = local.workspace.min_instance_count
  vpc_zone_identifier = data.aws_subnets.private_subnets.ids
  target_group_arns   = []

  enabled_metrics           = ["GroupDesiredCapacity", "GroupInServiceCapacity", "GroupPendingCapacity", "GroupMinSize", "GroupMaxSize", "GroupInServiceInstances", "GroupPendingInstances", "GroupStandbyInstances", "GroupStandbyCapacity", "GroupTerminatingCapacity", "GroupTerminatingInstances", "GroupTotalCapacity", "GroupTotalInstances"]
  health_check_grace_period = 300
  health_check_type         = "ELB"

  launch_template {
    id      = aws_launch_template.polaris.id
    version = aws_launch_template.polaris.latest_version
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = local.workspace.deployment_minimum_healthy_percent
    }
  }

  tag {
    key                 = "Name"
    value               = local.prefix
    propagate_at_launch = true
  }

  tag {
    key                 = "application"
    value               = local.name
    propagate_at_launch = true
  }

  tag {
    key                 = "environment"
    value               = terraform.workspace
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "app-cpu" {
  name                   = "${local.prefix}-instance-cpu"
  policy_type            = "TargetTrackingScaling"
  autoscaling_group_name = aws_autoscaling_group.polaris.name

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = local.workspace["avg_cpu_target"]
  }
}
