# This file contains the service user and permissions used by the application.
# TODO: Ideally this moves into the task role and we remove the explicit IAM credentials
# AWS Access Key

resource "aws_iam_user" "service" {
  name = "${local.prefix}-service"
}

resource "aws_iam_access_key" "service" {
  user = aws_iam_user.service.name
}

resource "aws_secretsmanager_secret" "aws_keys" { #tfsec:ignore:aws-ssm-secret-use-customer-key
  name = "${terraform.workspace}/${local.name}/aws_keys"
}

resource "aws_secretsmanager_secret_version" "aws_keys" {
  secret_id = aws_secretsmanager_secret.aws_keys.id

  secret_string = jsonencode({
    aws_access_key_id     = aws_iam_access_key.service.id
    aws_secret_access_key = aws_iam_access_key.service.secret
  })
}

# Application Specific Roles

data "aws_iam_policy_document" "ses" {
  statement {
    actions = [
      "ses:SendRawEmail",
    ]
    effect = "Allow"

    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "ses" {
  name        = "${local.prefix}-send-email"
  description = "Access to SES for ${local.name} ${terraform.workspace}"
  policy      = data.aws_iam_policy_document.ses.json
}

resource "aws_iam_user_policy_attachment" "ses" {
  user       = aws_iam_user.service.name
  policy_arn = aws_iam_policy.ses.arn
}

data "aws_iam_policy_document" "sqs" {
  statement {
    actions = [
      "sqs:SendMessage",
    ]
    effect = "Allow"
    resources = [
      local.workspace.sqs_website_audit_arn,
      local.workspace.sqs_cookie_scanner_arn,
      local.workspace.sqs_ppb_arn,
      local.workspace.sqs_privacy_site_builder_arn,
      local.workspace.sqs_privacy_site_provisioner_arn
    ]
  }
}

resource "aws_iam_policy" "sqs" {
  name        = "${local.prefix}-Privacy-Policy-Builder-SQS-Insert"
  description = "Access to SQS for ${local.name} ${terraform.workspace}"
  policy      = data.aws_iam_policy_document.sqs.json
}

resource "aws_iam_user_policy_attachment" "sqs" {
  user       = aws_iam_user.service.name
  policy_arn = aws_iam_policy.sqs.arn
}

data "aws_iam_policy_document" "s3_attachments" {
  statement {
    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:DeleteObject"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:s3:::polaris-images/*",
      "arn:aws:s3:::naga-attachments/*",
      "arn:aws:s3:::polaris-attachments/*",
      "arn:aws:s3:::polaris-vendor-agreements/*",
      "arn:aws:s3:::polaris-dbdumps/*",
      "arn:aws:s3:::polaris-config/*"
    ]
  }
}

resource "aws_iam_policy" "s3_attachments" {
  name        = "${local.prefix}-attachments-rw"
  description = "RW access to S3 attachments for ${local.name} ${terraform.workspace}"
  policy      = data.aws_iam_policy_document.s3_attachments.json
}

resource "aws_iam_user_policy_attachment" "s3_attachments" {
  user       = aws_iam_user.service.name
  policy_arn = aws_iam_policy.s3_attachments.arn
}

resource "aws_iam_user_policy_attachment" "secrets" {
  user       = aws_iam_user.service.name
  policy_arn = aws_iam_policy.secrets.arn
}

# Access to CloudFront to create invalidations
data "aws_iam_policy_document" "cloudfront" {
  statement {
    actions = [
      "cloudfront:CreateInvalidation"
    ]
    effect = "Allow"

    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = [
      "*"
    ]

    condition {
      test     = "StringEquals"
      variable = "aws:ResourceTag/environment"
      values   = [terraform.workspace]
    }

    condition {
      test     = "StringEquals"
      variable = "aws:ResourceTag/application"
      values   = ["polaris"]
    }
  }
}

resource "aws_iam_policy" "cloudfront" {
  name        = "${local.prefix}-cloudfront-rw"
  description = "Access to Cloudfront for ${local.name} ${terraform.workspace}"
  policy      = data.aws_iam_policy_document.cloudfront.json
}

resource "aws_iam_user_policy_attachment" "cloudfront" {
  user       = aws_iam_user.service.name
  policy_arn = aws_iam_policy.cloudfront.arn
}
