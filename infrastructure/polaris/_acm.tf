resource "aws_acm_certificate" "polaris" {
  domain_name       = local.dns_name
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "polaris_acm" {
  for_each = local.workspace.create_dns_records ? {
    for dvo in aws_acm_certificate.polaris.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  } : {}

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.domain.0.id
}

resource "aws_acm_certificate_validation" "polaris_acm" {
  count                   = local.workspace.create_dns_records ? 1 : 0
  certificate_arn         = aws_acm_certificate.polaris.arn
  validation_record_fqdns = [for record in aws_route53_record.polaris_acm : record.fqdn]
}
