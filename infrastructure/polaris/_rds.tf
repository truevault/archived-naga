resource "aws_security_group" "rds" {
  name_prefix = "${local.prefix}-rds"
  vpc_id      = data.aws_vpc.truevault.id
  description = "${local.name} (${terraform.workspace})"

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:aws-vpc-no-public-egress-sgr
  }

  tags = {
    Name = "${local.prefix}-rds"
  }
}

resource "aws_security_group_rule" "rds_postgres" {
  description              = "Allow Postgress traffic from application security group"
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  security_group_id        = aws_security_group.rds.id
  source_security_group_id = aws_security_group.instance.id
  protocol                 = "tcp"
}
