data "aws_route53_zone" "staging" {
  name         = "truevaultstaging.com."
  private_zone = false
}

# data "aws_route53_zone" "prod" {
#   name         = "truevault.com."
#   private_zone = false
# }

#--- S3 buckets ------
resource "aws_s3_bucket" "admin_hosting_staging" {
  bucket = "admin-polaris.truevaultstaging.com"
  acl    = "public-read"
  website {
    index_document = "index.html"
  }

  tags = {
    environment = "staging"
    application = "polaris-admin"
  }
}

resource "aws_s3_bucket" "admin_hosting" {
  bucket = "admin-polaris.truevault.com"
  acl    = "public-read"

  website {
    index_document = "index.html"
  }

  tags = {
    environment = "staging"
    application = "polaris-admin"
  }
}


#--- Certificate and Validation ------
resource "aws_acm_certificate" "admin_staging" {
  domain_name       = "admin-polaris.truevaultstaging.com"
  validation_method = "DNS"

  tags = {
    environment = "staging"
    application = "polaris-admin"
  }

  lifecycle {
    create_before_destroy = true
  }

  provider = aws.east
}

resource "aws_acm_certificate" "admin" {
  domain_name       = "admin-polaris.truevault.com"
  validation_method = "DNS"

  tags = {
    environment = "prod"
    application = "polaris-admin"
  }

  lifecycle {
    create_before_destroy = true
  }

  provider = aws.east
}

resource "aws_route53_record" "staging_cert_validation" {
  name    = aws_acm_certificate.admin_staging.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.admin_staging.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.staging.id
  records = [aws_acm_certificate.admin_staging.domain_validation_options.0.resource_record_value]
  ttl     = 60

  provider = aws.east
}

resource "aws_acm_certificate_validation" "staging" {
  certificate_arn         = aws_acm_certificate.admin_staging.arn
  validation_record_fqdns = [aws_route53_record.staging_cert_validation.fqdn]

  provider = aws.east
}

# ---------- Cloudfront ----------

resource "aws_cloudfront_distribution" "admin_staging" {
  origin {
    domain_name = aws_s3_bucket.admin_hosting_staging.bucket_regional_domain_name
    origin_id   = "s3-root"
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Staging Polaris Admin"
  default_root_object = "index.html"

  aliases = compact([
    "admin-polaris.truevaultstaging.com"
  ])

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "s3-root"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    # compress               = true # TODO Enable?

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
  }

  price_class = "PriceClass_All"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    environment = "staging"
    application = "polaris-admin"
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.admin_staging.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2018"
  }
}

resource "aws_cloudfront_distribution" "admin" {
  origin {
    domain_name = aws_s3_bucket.admin_hosting.bucket_regional_domain_name
    origin_id   = "s3-root"
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Polaris Admin"
  default_root_object = "index.html"

  aliases = compact([
    "admin-polaris.truevault.com"
  ])

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "s3-root"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    # compress               = true # TODO Enable?

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
  }

  price_class = "PriceClass_All"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    environment = "production"
    application = "polaris-admin"
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.admin.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2018"
  }
}

#--- DNS Entry --------------------------
resource "aws_route53_record" "admin_staging" {
  name    = "admin-polaris.truevaultstaging.com"
  type    = "A"
  zone_id = data.aws_route53_zone.staging.id

  alias {
    name                   = aws_cloudfront_distribution.admin_staging.domain_name
    zone_id                = aws_cloudfront_distribution.admin_staging.hosted_zone_id
    evaluate_target_health = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
