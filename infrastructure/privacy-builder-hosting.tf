resource "aws_s3_bucket" "hosting-local" {
  bucket = "hosting-local.truevaultprivacycenter.com"
  acl    = "public-read"

  website {
    index_document = "index.html"
  }
}

resource "aws_s3_bucket" "hosting-staging" {
  bucket = "hosting-staging.truevaultprivacycenter.com"
  acl    = "public-read"
  website {
    index_document = "index.html"
  }
}

resource "aws_s3_bucket" "hosting" {
  bucket = "hosting.truevaultprivacycenter.com"
  acl    = "public-read"
  # policy = "${file("policy.json")}"

  website {
    index_document = "index.html"
  }
}
