# Workspaces: default

terraform {
  backend "s3" {
    bucket               = "polaris-terraform-state"
    key                  = "polaris_config.tfstate"
    region               = "us-west-2"
    workspace_key_prefix = "infrastructure/polaris_config"
  }
}


locals {
  name   = "polaris-config"

  common_tags = {
    application = local.name,
    environment = terraform.workspace,
    terraform   = "true"
  }
}

provider "aws" {
  region = "us-west-2"

  default_tags {
    tags = local.common_tags
  }
}


data "aws_cloudfront_distribution" "staging" {
  id = "E2S6BK1MGL6KS9"
}

data "aws_cloudfront_distribution" "production" {
  id = "E3JOOT1DAJUY9C"
}


resource "aws_s3_bucket" "config" {
  bucket = local.name
}

resource "aws_s3_bucket_acl" "config" {
  bucket = aws_s3_bucket.config.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "config" {
  bucket = aws_s3_bucket.config.id

  versioning_configuration {
    status = "Enabled"
  }
}


data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.config.arn}/staging/*"]

    principals {
      type        = "Service"
      identifiers = ["cloudfront.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = [data.aws_cloudfront_distribution.staging.arn]
    }
  }

  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.config.arn}/prod/*"]

    principals {
      type        = "Service"
      identifiers = ["cloudfront.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = [data.aws_cloudfront_distribution.production.arn]
    }
  }
}

resource "aws_s3_bucket_policy" "config" {
  bucket = aws_s3_bucket.config.id
  policy = data.aws_iam_policy_document.s3_policy.json
}
