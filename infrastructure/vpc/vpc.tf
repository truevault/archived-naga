module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.2"

  cidr = local.cidr

  azs             = data.aws_availability_zones.available.names
  private_subnets = ["10.10.16.0/20", "10.10.48.0/20", "10.10.80.0/20"]
  public_subnets  = ["10.10.0.0/20", "10.10.32.0/20", "10.10.64.0/20"]

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_nat_gateway     = true
  single_nat_gateway     = false
  one_nat_gateway_per_az = true

  manage_default_network_acl    = true
  manage_default_route_table    = true
  manage_default_security_group = true

  public_dedicated_network_acl  = true
  private_dedicated_network_acl = true


  # Nameing
  name = local.name

  # Tags
  public_route_table_tags = {
    Reach = "public"
  }

  public_subnet_tags = {
    Reach = "public"
  }

  private_route_table_tags = {
    Reach = "private"
  }

  private_subnet_tags = {
    Reach = "private"
  }

  public_acl_tags = {
    Reach = "public"
  }

  private_acl_tags = {
    Reach = "private"
  }

  igw_tags = {
    Name = local.legacy_name
  }

  vpc_tags = {
    Name = local.legacy_name
  }

  default_security_group_tags = {
    Name = "${local.name}-default"
  }
}


# TODO: Can we remove Quicksight?
resource "aws_subnet" "quicksight" {
  vpc_id     = module.vpc.vpc_id
  cidr_block = "10.10.192.0/20"

  tags = {
    Name = "quicksight"
  }
}
