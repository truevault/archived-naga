terraform {
  backend "s3" {
    bucket               = "polaris-terraform-state"
    key                  = "tv_vpc.tfstate"
    region               = "us-west-2"
    workspace_key_prefix = "infrastructure/tv_vpc"
  }
}

locals {
  name        = "TV"
  cidr        = "10.10.0.0/16"
  legacy_name = "${local.name} ${local.cidr}"

  common_tags = {
    application = "vpc"
    terraform   = "true"
  }

}

provider "aws" {
  region = "us-west-2"
  default_tags {
    tags = local.common_tags
  }
}

data "aws_availability_zones" "available" {
  state         = "available"
  exclude_names = ["us-west-2d"] # We do not have any subnets in this zone.
}
