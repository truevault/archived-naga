output "_admin_center__staging__cloudfront" {
  description = "the cloudfront distribution for the staging privacy center"
  value       = aws_cloudfront_distribution.admin_staging.id
}

output "_admin_center__production__cloudfront" {
  description = "the cloudfront distribution for the production privacy center"
  value       = aws_cloudfront_distribution.admin.id
}
