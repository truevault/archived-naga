terraform {
  backend "s3" {
    bucket = "polaris-terraform-state"
    key    = "polaris-privacy-builder"
    region = "us-west-2"
  }
}

# The default provider configuration is us-west-2
provider "aws" {
  region = "us-west-2"
}

# Additional provider configuration for east coast region for cloudfront config
provider "aws" {
  alias  = "east"
  region = "us-east-1"
}
