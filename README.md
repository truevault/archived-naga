# Archived
Moved to: https://github.com/truevault/polaris-naga

# Polaris

## Getting Started

### Running Polaris
1. run `./setup-local.sh`
1. run `docker-compose build app`
1. run `docker-compose up`
1. run `docker-compose ps` (verify container statuses are "healthy")

#### WSL 2

Before you `docker-compose up` be sure you do the following:

1. `sudo chown root:root .` (Gross, sorry.)
1. `sudo chmod 777 .` (Honestly, gross. So sorry.)


### Using Polaris
1. run `bin/seed_db.sh`
1. visit `http://localhost:5000`
1. login with a user introduced by the seed dataset
   - `admin@qa.truevault.com` / `admin` - Polaris Admin user
   - `admin@flannellabs.com` / `admin` - oganization user
   - `demo@truevault.com` / `admin` - organization user


## Developer Notes

### Docker Compose

The web, assets, and admin services all use a shared `truevault/polaris-app` image which is built and cached in your local `Docker` repository by running `docker-compose build app`.

Be sure to rebuild the `truevault/polaris-app` image following any changes to `Dockerfile` as well as periodically after pulling to catch any updates to the container from other commits.

### Helpful Bin Scripts

There are several helpful scripts in the `bin` directory. Most of them are fairly self explanatory. The following may not be:

- `bin/all_get_compliant.sh` puts all orgs into `get-compliant`
- `bin/all_stay_compliant.sh` puts all orgs into `stay-compliant`
- `bin/verify_all_request_emails.sh` marks all newly opened requests as verified emails, and ready to enter consumer groups

### Running Tests

Out-of-the-box, the tests only run successfully in the `web` container. You can ssh into the `web` container by running `bin/ssh_to_container`.

- `./gradlew test` - run all tests
- `./gradlew test --tests polaris.services.PrivacyCenterServiceTest` - test a specific class
- `./graldew test --tests polaris.services.PrivacyCenterServiceTest.updatePrivacyCenterRequestEmail__doesNotUpdateIfEmailBlank` - test a specific method

### Restarting

The application runs using `runner`. You can restart the polaris server without restarting the docker-compose container by running:

(from host) `./gradlew restart`
(from container) `runner restart polaris`

### Resetting the Initial Survey

Visit `http://localhost:5000/debug/resetSurvey`.

As of this writing, it only resets your progress step, allowing a return to the first page - it does not clean any of your answers.

### Faking the Cookie Scan

1. Open the web browser console to the network tab.
2. Set the "scanned" url in the web browser and initiate a scan.
3. Run `bin/stub_cookie_scan.sh` with that URL and your org's `public_id`.
4. If Step 2 is still running (waiting for timeout), refresh the page and you should now have results. Stop here.
5. If Step 2 failed quickly with a 500, you probably can't reach SQS, and will need to stub the scan itelf. Proceed to Step 6.
6. Run `bin/psql.sh` to open postgres if it's not already open.
7. Find your `organization_id`.
8. `insert into organization_cookie_scan (organization_id, status, scan_url) values ('ORG_UUID', 'COMPLETE', 'URL_FROM_STEP_3');`
9. Refresh the scanner page; you should now have results.

### Asset Server

The default docker environment starts an asset server running on `localhost:5001`. This allows you to edit and reload javascript and css without having to wait for spring to build.

### Email Sending

Emails are either sent via SendGrid or simply written to disk. Which to use is controlled by the spring profile `live-email`. When the `live-email` profile is active, the necessary `SENDGRID_*` environment variables should be set.

Locally, these can be written to a `.env` file that looks like:

    AWS_ACCESS_KEY_ID=<key>
    AWS_SECRET_ACCESS_KEY=<secret-key>
    SENDGRID_API_KEY=<api-key>

### IntelliJ

IntelliJ comes with several run configurations built in:

`Polaris (On Host)` - this will launch a copy of polaris running directly on the host. It will spin up a docker-compose environment for supporting resources (such as postgres) that are contained in `docker-compose-no-web.yml`.
`Polaris (Docker)` - this will start the `docker-compose.yml` docker container.
`Remote Debug Polaris (Docker)` - this is _almost_ working. It will start a remote debug session that successfully connects to the process running in the docker container. I haven't gotten breakpoints to trigger in this environment, which is why we're retaining the Polaris (On Host) configuration for the time being.

You should also install the `ktlint` plugin for intellij. This may require you to install `ktlint` on your host system in addition to the `ktlint` installed inside the container.

### Database Migrations

Migrations use flyway: (https://flywaydb.org/documentation/)

- Migrations are in: `src/main/resources/db/migration`
- Version format is semantic so `major`.`minor`.`patch`.`dev`, starting with `V0.3.2.1`
  - The standard semantic values (major, minor, patch) should correspond to a polaris release
  - `dev` migrations exist only during of a polaris release. Before release, all `dev` migrations should get squashed down into a single migration for that release (e.g. `V0.3.2.1`, `V0.3.2.2` and `V0.3.2.3` all get concatenated into `V0.3.2`)

To create a new migration, you can either run: `bin/create_migration my_migration_name` or `npm run create:migration -- my_migration_name`

### Privacy Center Screenshots

We periodically need to generate privacy center screenshots. For consistency, the simplest way to do this is to:

1. Use Chrome
2. Open developer tools, and using the Device emulator, set mode to "Responsive" and dimensions to `1361x850`
3. Load the privacy center, best done on develop
4. Swap the URL of the privacy center logo for the TrueVault logo, set the max-height of the logo to `30px`
5. In the dev tools panel, press `super-shift-p` to bring up the command palette, and select `Capture Screenshot`
6. Navigate to https://browserframe.com/ and upload the screenshot
7. Select **Chrome • Mac • Light** from the frame options
8. Download your framed image
9. For future screenshots, we should commit both the _framed_ and _unframed_ variant of the screenshot

If browser frame becomes unavailable, we will need to regenerate _all_ screenshots with some new framing mechanism. God help us if that's the case.

## Operational Notes

## Admin Work

If you need to do work on the admin application you'll need 2 things:

1.  To be logged in as a global admin. (The seeds create an `admin@qa.truevault.com`/`admin` user.)
2.  To be running the docker admin service (`docker-compose up admin`).

## Operations

### SSH to instance

To SSH to an instance, you'll need the `tv-polaris.pem` file.

`ssh -i ~/.ssh/tv-polaris.pem ec2-user@[instance-ip]`

### DB Clone to Staging

see [DB Santizer](./ops/db-sanitizer/README.md)

### Known Instances

Below is a list of known instance IDs or IP addresses to SSH to. These instances may changes and be out of date, so this is a convenience reference only. If these are out of date, more up to date instance information can be found in the AWS Console.

- QA: `34.219.201.227`
- Prod: `34.220.179.107`

- Builder Stage: `18.236.216.238`
- Builder Prod: `35.160.135.137`

## Infrastructure

Terraform is used to manage the Polaris infrastructure.

- [tfenv](https://github.com/tfutils/tfenv) - we use this to manage the Terraform version and locks it into the version in `.terraform-version` file

### Privacy Center / Admin Console (`/infrastructure`)

Terraform - infrastructure for the privacy center builder hosting and admin console hosting lives in `/infrastructure` folder

TODO: move this to a sub folder.


### VPC - (`/infrastructure/vpc`)

This contains the resources needed to manage the TV VPC. It uses the AWS provided module to manage the VPC and its complexities. For more information on the module see https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/3.14.2

To make changes (Note there are no workspaces):

- `terraform init`
- `terraform plan `
- `terraform apply`
