#!/bin/bash

if [ -f ".env" ]; then
    echo ".env already exists."
else
    cp .env.example .env
fi
