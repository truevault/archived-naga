// these are the raw environment variables
const vars = {
  SHA_VERSION: process.env.SHA_VERSION || process.env.GIT_SHA_VERSION || "unset",
  ENVIRONMENT: process.env.SENTRY_ENVIRONMENT || process.env.NODE_ENV || "production",
  SENTRY_DSN: process.env.SENTRY_DSN || "",
  ALLOW_DRAFT_MODE: process.env.ALLOW_DRAFT_MODE || false,
  IS_DEVELOPMENT: process.env.NODE_ENV === "development",
  BEACON_INIT_KEY: process.env.BEACON_INIT_KEY || "",
};

// these are the variables to be used by globals.ts via the DefinePlugin
const globals = Object.entries(vars).reduce((acc, [key, value]) => {
  acc[`__${key}__`] = JSON.stringify(value);
  return acc;
}, {});

module.exports = {
  vars,
  globals,
};
