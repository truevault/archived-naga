------------------------------------
-- SANITIZE DATA
------------------------------------
update polaris_user set email = regexp_replace(email, '^(.*)@(.*)$', '\1+\2@qa.truevault.com') where email not like '%@truevault.com';
update polaris_user set email = regexp_replace(email, '@.*$', '@qa.truevault.com') where email like '%@truevault.com';
update organization_user set status = 'INACTIVE' where status = 'INVITED' or organization_id not in (select o.id from organization o where o.name in ('Apsis Labs', 'Flannel Labs', 'Master Admin Account (Do Not Remove)', 'Soma Store', 'TrueVault Staging'));
update vendor set email = regexp_replace(email, '^(.*)@(.*)$', '\1+\2@qa.truevault.com');
update data_subject_request set subject_email_address = regexp_replace(subject_email_address, '@.*$', '@qa.truevault.com');
update organization_mailbox set mailbox = regexp_replace(mailbox, '^(.*)@(.*)$', '\1+\2@qa.truevault.com'), nylas_account_id = id, nylas_access_token = '', status = 'INACTIVE';
update organization set ga_web_property = null where ga_web_property is not null;
update privacy_center set cloudfront_distribution = null where cloudfront_distribution is not null;
update privacy_center set request_email = regexp_replace(request_email, '^(.*)@(.*)$', '\1+\2@qa.truevault.com') where request_email is not null and request_email not like '%@qa.truevault.com';
update privacy_center set dpo_officer_email = regexp_replace(dpo_officer_email, '^(.*)@(.*)$', '\1+\2@qa.truevault.com') where dpo_officer_email is not null and dpo_officer_email not like '%@qa.truevault.com';
delete from gdpr_cookie_consent_tracking where created_at < now() - interval '2 weeks';
truncate table organization_auth_token;

----------------------------------------------------
-- INSERT TEST DATA: Soma Store, TrueVault Staging
----------------------------------------------------

-- add admin@qa.truevault.com user (password=jtn0taq6EZE4fvq!gnm, stored in 1Password in Engineering vault)
insert into polaris_user
    (id, email, password, first_name, last_name, last_login, update_password_id, created_at, global_role, active_organization_id, deleted_at)
values
    ('ea80d6c9-f02b-44c8-a8e0-d7fd94ecb67a', 'admin@qa.truevault.com', '$2a$10$M0.nYD99vF/YhSccu64BS.3b6hbDkrKgq0yI55V1mPvXNgHt95vO6', 'Admin', 'User', null, null, now(), 'ADMIN', null, null);

insert into organization_user
    (user_id, organization_id, role, status, created_at, deleted_at)
values
    ('ea80d6c9-f02b-44c8-a8e0-d7fd94ecb67a', 'dc285f42-d7bd-47f5-bb6f-e49ecf6fdcdd', 'ADMIN', 'ACTIVE', now(), null);

-- deprecate TrueVault (to prep for TrueVault Staging)
update organization set name = 'ZZZ-TrueVault' where public_id = 'ZAXGPOHEH';
update privacy_center set name = 'ZZZ-TrueVault', default_subdomain = 'ZZZ-truevault' where public_id = 'QYCRY7UL5';

-- create the Organizations
insert into organization (
    id, public_id, name, training_complete, send_method, identity_verification_message
)
values
    ('69760ba2-44d4-4c43-9b1e-adc01963a2d1', 'SOMASTORE', 'Soma Store', true, 'NYLAS', ''),
    ('26f9d689-6f17-4264-baa1-d5a3fadb8c3f', 'UXWSDP2MQ', 'TrueVault Staging', true, 'NYLAS', '')
;

-- create the Privacy Centers
insert into privacy_center (
    id, public_id, organization_id, name, favicon_url, default_subdomain,
    cloudfront_distribution, custom_url, custom_url_status, created_at,
    privacy_policy_url, request_form_url, request_email, logo_url, policy_last_updated,
    logo_link_url, custom_url_cname_ready, has_mobile_application, ca_residency_confirmation
)
values
    ('221cb187-198b-4136-8543-b0cea168820b','SOMA','69760ba2-44d4-4c43-9b1e-adc01963a2d1','Soma Privacy Center','https://www.truevault.com/hubfs/favicon.png','soma.privacy',
     'E2W55ME8AU61GW','soma.privacy.truevaultstaging.com','ACTIVE',now(),
     'https://soma.privacy.truevaultstaging.com/privacy-policy','https://soma.privacy.truevaultstaging.com/privacy-request','privacy-staging@truevault.onmicrosoft.com',NULL,now(),
     'https://soma.store.truevaultstaging.com',false,false,true),
    ('1e44908b-885c-478d-b88c-1dde4d6b6036','BFK4MU1CF','26f9d689-6f17-4264-baa1-d5a3fadb8c3f','TrueVault Staging Privacy Center','https://www.truevault.com/hubfs/favicon.png','truevault.staging',
     'E298K18UKLNN2K','truevault.staging.truevaultprivacycenter.com','ACTIVE',now(),
     'https://truevault.staging.truevaultprivacycenter.com/privacy-policy','https://truevault.staging.truevaultprivacycenter.com/privacy-request','truevault+privacy@qa.truevault.com',null,now(),
     NULL,false,false,true)
;

-- associate to the Master Account (for the org switcher)
insert into organization_vendor_client (vendor_id, client_id)
values
    ('dc285f42-d7bd-47f5-bb6f-e49ecf6fdcdd', '69760ba2-44d4-4c43-9b1e-adc01963a2d1'),
    ('dc285f42-d7bd-47f5-bb6f-e49ecf6fdcdd', '26f9d689-6f17-4264-baa1-d5a3fadb8c3f')
;

-- connect the Nylas Dev mailboxes
insert into organization_mailbox
    (id, organization_id, status, type, mailbox, nylas_access_token, nylas_account_id, header_image_url, physical_address)
values
    ('08261e62-e73a-41f3-b0fa-75433a7944fc','69760ba2-44d4-4c43-9b1e-adc01963a2d1','CONNECTED','EXCHANGE','privacy-staging@truevault.onmicrosoft.com','Y4qECyYmhC96Y6tmPXWRsszHqwxRKA','8pwtxp7tag19yi0v8otf9qla6', '', ''),
    ('81859c6d-21df-4e9d-b828-64074be4da5f','26f9d689-6f17-4264-baa1-d5a3fadb8c3f','CONNECTED','GMAIL','privacy-staging@truevault.com','tHF4yWyIfqslDH2ZrMkI0XTTm8JzV8','okafusqophuxn2pkcxdl34m3', '', '')
;

----------------------------------------------------
-- copy the ZZZ-TrueVault data map to the test orgs
----------------------------------------------------

-- copy the collection groups
insert into collection_group (
    organization_id, name, created_at, enabled, privacy_notice_progress, description, gdpr_eea_uk_data_collection,
    collection_group_type, autocreation_slug, processing_regions, created_as_data_subject, privacy_notice_intro_text
)
with seed_org (id) as (values ('26f9d689-6f17-4264-baa1-d5a3fadb8c3f'::uuid), ('69760ba2-44d4-4c43-9b1e-adc01963a2d1'::uuid))
select
    seed_org.id, name, now(), enabled, privacy_notice_progress, description, gdpr_eea_uk_data_collection,
    collection_group_type, autocreation_slug, processing_regions, created_as_data_subject, privacy_notice_intro_text
from seed_org
    join collection_group cg on cg.organization_id = '038a2038-679c-4ce2-97e0-cd9f33eee0a2' and cg.enabled is true and cg.deleted_at is null
;

-- copy the collection group collected pic
insert into collection_group_collected_pic (
    collection_group_id, pic_id
)
with seed_org (id) as (values ('26f9d689-6f17-4264-baa1-d5a3fadb8c3f'::uuid), ('69760ba2-44d4-4c43-9b1e-adc01963a2d1'::uuid))
select
    cg.id, tv_cgcp.pic_id
from seed_org
    join collection_group cg on cg.organization_id = seed_org.id
    join collection_group tv_cg on tv_cg.name = cg.name and tv_cg.organization_id = '038a2038-679c-4ce2-97e0-cd9f33eee0a2' and tv_cg.enabled is true and tv_cg.deleted_at is null
    join collection_group_collected_pic tv_cgcp on tv_cgcp.collection_group_id = tv_cg.id and tv_cgcp.deleted_at is null
;

-- copy the organization data recipients (non-custom vendors)
insert into organization_data_recipient (
    organization_id, vendor_id, service_type_id, created_at, status, ccpa_is_selling, ccpa_is_cookie_sharing, contacted, mapping_progress,
    ccpa_is_sharing, is_data_storage, public_tos_url, email, tos_file_name, tos_file_key, deleted_at, processing_regions,
    gdpr_processor_setting, gdpr_processor_guarantee_url, gdpr_processor_guarantee_file_name, gdpr_processor_guarantee_file_key, gdpr_has_scc_setting,
    gdpr_scc_url, gdpr_scc_file_name, gdpr_scc_file_key, gdpr_contact_unsafe_transfer, gdpr_ensure_scc_in_place, gdpr_processor_setting_set_at,
    gdpr_controller_guarantee_url, gdpr_controller_guarantee_file_name, gdpr_controller_guarantee_file_key, gdpr_contact_processor_status,
    completed, removed_from_exceptions_to_scc, automatically_classified, data_accessibility, gdpr_processor_locked_setting, consumer_context,
    data_deletability, uses_custom_audience, is_upload_vendor
)
with seed_org (id) as (values ('26f9d689-6f17-4264-baa1-d5a3fadb8c3f'::uuid), ('69760ba2-44d4-4c43-9b1e-adc01963a2d1'::uuid))
select
    seed_org.id, vendor_id, service_type_id, created_at, status, ccpa_is_selling, ccpa_is_cookie_sharing, contacted, mapping_progress,
    ccpa_is_sharing, is_data_storage, public_tos_url, email, tos_file_name, tos_file_key, deleted_at, processing_regions,
    gdpr_processor_setting, gdpr_processor_guarantee_url, gdpr_processor_guarantee_file_name, gdpr_processor_guarantee_file_key, gdpr_has_scc_setting,
    gdpr_scc_url, gdpr_scc_file_name, gdpr_scc_file_key, gdpr_contact_unsafe_transfer, gdpr_ensure_scc_in_place, gdpr_processor_setting_set_at,
    gdpr_controller_guarantee_url, gdpr_controller_guarantee_file_name, gdpr_controller_guarantee_file_key, gdpr_contact_processor_status,
    completed, removed_from_exceptions_to_scc, automatically_classified, data_accessibility, gdpr_processor_locked_setting, consumer_context,
    data_deletability, uses_custom_audience, is_upload_vendor
from seed_org
    join organization_data_recipient tv_odr on tv_odr.organization_id = '038a2038-679c-4ce2-97e0-cd9f33eee0a2' and tv_odr.deleted_at is null
where tv_odr.vendor_id in (select id from vendor where organization_id is null);

-- copy the data recipient disclosed pic
insert into data_recipient_disclosed_pic (
    collected_pic_id, service_id
)
with seed_org (id) as (values ('26f9d689-6f17-4264-baa1-d5a3fadb8c3f'::uuid), ('69760ba2-44d4-4c43-9b1e-adc01963a2d1'::uuid))
select
    cgcp.id, tv_drdp.service_id
from seed_org
    join collection_group cg on cg.organization_id = seed_org.id
    join collection_group_collected_pic cgcp on cg.id = cgcp.collection_group_id and cgcp.deleted_at is null
    join collection_group tv_cg on tv_cg.name = cg.name and tv_cg.organization_id = '038a2038-679c-4ce2-97e0-cd9f33eee0a2' and tv_cg.deleted_at is null
    join collection_group_collected_pic tv_cgcp on tv_cgcp.pic_id = cgcp.pic_id and tv_cgcp.collection_group_id = tv_cg.id and tv_cgcp.deleted_at is null
    join data_recipient_disclosed_pic tv_drdp on tv_cgcp.id = tv_drdp.collected_pic_id
    join vendor tv_v on tv_drdp.service_id = tv_v.id
where tv_v.organization_id in (select id from vendor where organization_id is null);

