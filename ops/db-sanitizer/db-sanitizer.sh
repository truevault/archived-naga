#!/usr/bin/env bash

###########################################################
# DB Sanitizer script
###########################################################

set -e

###########################################################
# Configuration
###########################################################
S3_BUCKET_NAME=${S3_BUCKET_NAME:-"polaris-dbdumps"}
S3_BUCKET="s3://${S3_BUCKET_NAME}"
PG_DUMP_DIR="./pgdump"
PG_DUMP_SCHEMA_PATTERNS="(public)"
PG_DUMP_BASE_FILENAME="${POSTGRES_DATABASE}"
TEMP_DB_NAME="${POSTGRES_DATABASE}_tmp"

###########################################################
# Utilities
###########################################################
lowercase () {
  echo $1 | tr '[:upper:]' '[:lower:]'
}

prompt () {
  local input
  local DEFAULT="$2"
  while [[ -z "$input" ]]; do
      read -p "$1 ${DEFAULT}" input
  done
  echo $(lowercase ${input})
}

now () {
  echo "$(date +%s)"
}

to_duration() {
  echo "$(( ${1} / 60 ))m $(( ${1} % 60 ))s"
}

# insert the provided datestamp (or today's datestamp if none provided) into the filename
add_datestamp() {
  local file_name=$1
  local datestamp=${2:-"$(TZ=America/Los_Angeles date '+%Y%m%d')"}

  # prefix is everything before the last "-"
  local prefix="${file_name%-*}"

  # suffix is everything after the last "-"
  local suffix="${file_name##*-}"

  echo "${prefix}-${datestamp}-${suffix}"
}

# extract the datestamp if it exists, otherwise return today's datestamp
extract_datestamp() {
  local file_name=$1
  if [[ "$file_name" =~ [0-9]{8} ]]; then
    # remove everything after the last "-"
    file_name=${file_name%-*}
    # remove everything before the last "-"
    file_name=${file_name##*-}
    echo "$file_name"
  else
    echo "$(TZ=America/Los_Angeles date '+%Y%m%d')"
  fi
}

elapsed () {
  echo "$(to_duration $(($(now) - $1)))"
}

assert() {
  if [[ -z "${1}" ]]; then
    echo -e "${2}"
    exit
  fi
}

###########################################################
# Functions
###########################################################

assert_not_on_prod () {
  local command="$1"

  if [[ "${POSTGRES_URL}" == *"prod"* ]] ||
     [[ "${SENTRY_ENVIRONMENT}" == "prod"* ]] ||
     [[ "${DISPLAY_ENV}" == "prod"* ]] ||
     [[ "${POLARIS_IMAGES_PREFIX}" == "prod"* ]] ||
     [[ "${SPRING_COOKIE_DOMAIN}" == ".truevault.com" ]]; then
    echo
    echo "ERROR: Invalid Command -- the selected command '$command' cannot be used on production!"
    echo
    echo "One of the following environment variables failed the prod test:"
    echo "  POSTGRES_URL=${POSTGRES_URL}"
    echo "  SENTRY_ENVIRONMENT=${SENTRY_ENVIRONMENT}"
    echo "  DISPLAY_ENV=${DISPLAY_ENV}"
    echo "  POLARIS_IMAGES_PREFIX=${POLARIS_IMAGES_PREFIX}"
    echo "  SPRING_COOKIE_DOMAIN=${SPRING_COOKIE_DOMAIN}"
    echo
    exit 1
  fi
}

reset_dump_dir () {
  echo
  echo "Cleaning ${PG_DUMP_DIR}..."
  rm -rf ${PG_DUMP_DIR}
  mkdir -p ${PG_DUMP_DIR}
}

dump_db_to_file () {
  local db_host=$1
  local db_name=$2
  local dump_file="${PG_DUMP_DIR}/$3"
  local source_db="${db_host}:${db_name}"
  local format="${dump_file##*.}"
  local start_time=$(now)

  echo "Dumping database ${source_db} to ${dump_file}..."
  if [[ $format == "sql" ]]; then
    PGPASSWORD=$POSTGRES_PASSWORD pg_dump -h "${db_host}" -U "${POSTGRES_USERNAME}" "${db_name}" > "${dump_file}"
  else
    PGPASSWORD=$POSTGRES_PASSWORD pg_dump -h "${db_host}" -U "${POSTGRES_USERNAME}" -Fc -v -f "${dump_file}" -d "${db_name}"
  fi

  echo "COMPLETED: Finished dumping database ${source_db} to file ${dump_file}. (Elapsed time: $(elapsed "$start_time"))"
}

compress_sql_dump () {
  local dump_file="${PG_DUMP_DIR}/$1"
  local compressed_file="${dump_file}.tar.gz"

  echo
  echo "creating compressed dump file ${compressed_file}..."

  tar -czvf "${compressed_file}" "${dump_file}"
}

extract_sql_dump () {
  local compressed_file="${PG_DUMP_DIR}/$1"

  echo
  echo "extracting dump file ${compressed_file}..."

  tar -xzvf "${compressed_file}"
}

restore_to_db () {
  local db_host=$1
  local db_name=$2
  local dump_file_name=$3
  local dump_file="${PG_DUMP_DIR}/${dump_file_name}"
  local target_db="${db_host}:${db_name}"
  local format="${dump_file_name##*.}"
  local start_time=$(now)

  echo
  echo "Restoring database from dump file ${dump_file} to ${target_db}..."

  if [[ $format == "sql" ]]; then
    echo
    echo "Dropping and re-creating public schema in ${target_db}..."
    PGPASSWORD=$POSTGRES_PASSWORD psql --pset expanded=auto -h ${db_host} -U ${POSTGRES_USERNAME} ${db_name} -c "drop schema public cascade; create schema public;"

    echo
    echo "Restoring from SQL dump file..."
    PGPASSWORD=$POSTGRES_PASSWORD psql -h ${db_host} -U ${POSTGRES_USERNAME} ${db_name} < ${dump_file}
  else
    echo
    echo "Dropping active connections from ${target_db}..."
    TERMINATE_CONNECTIONS_SQL="UPDATE pg_database SET datallowconn = 'false' WHERE datname = '${db_name}'; SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '${db_name}';"
    echo $TERMINATE_CONNECTIONS_SQL | PGPASSWORD=$POSTGRES_PASSWORD psql -h ${db_host} -d postgres -U ${POSTGRES_USERNAME} -w -v "ON_ERROR_STOP=1" -a -P pager=off

    echo "Dropping existing database ${target_db}..."
    PGPASSWORD=$POSTGRES_PASSWORD dropdb -h ${db_host} -U ${POSTGRES_USERNAME} -w -e --if-exists ${db_name}

    echo "Creating new database ${target_db}..."
    PGPASSWORD=$POSTGRES_PASSWORD createdb -h ${db_host} -U ${POSTGRES_USERNAME} -w -e -T template0 ${db_name}

    echo "Restoring from dump file with pg_restore..."
    PGPASSWORD=$POSTGRES_PASSWORD pg_restore -h ${db_host} -d ${db_name} -U ${POSTGRES_USERNAME} -v --no-privileges --no-owner "${dump_file}"

    echo "COMPLETED: Finished restoring dump file to ${target_db}. (Elapsed time: $(elapsed $start_time))"
  fi
}

download_dump_from_S3 () {
  local dump_file_url=$1
  local start_time=$(now)

  reset_dump_dir

  echo "Downloading dump file ${dump_file_url}..."
  aws s3 cp "${dump_file_url}" "${PG_DUMP_DIR}" --no-progress

  echo "COMPLETED: Downloaded dump file ${dump_file_url} to ${PG_DUMP_DIR}. (Elapsed time: $(elapsed "$start_time"))"
}

upload_dump_to_S3 () {
  local local_file="${PG_DUMP_DIR}/${1}"
  local start_time=$(now)

  echo
  echo "Uploading dump file ${local_file} to ${S3_BUCKET}..."
  aws s3 cp "${local_file}" "${S3_BUCKET}" --no-progress

  echo "COMPLETED: Uploaded dump file ${local_file} to ${S3_BUCKET}. (Elapsed time: $(elapsed "$start_time"))"
}

latest_from_s3 () {
  local filter=$1
  local latest=$(aws s3api list-objects-v2 --bucket "${S3_BUCKET_NAME}" --query "reverse(sort_by(Contents[?contains(Key, '${filter}')], &LastModified))[:1].Key" --output=text)
  echo "${latest}"
}

sanitize_db () {
  local db_host=$1
  local db_name=$2
  local target_db="$db_host:$db_name"
  local start_time=$(now)

  echo
  echo "Sanitizing database ${target_db}..."
  PGPASSWORD=$POSTGRES_PASSWORD psql -h ${db_host} -U ${POSTGRES_USERNAME} ${db_name} < sanitize.sql

  echo "COMPLETED: Finished sanitizing ${target_db}. (Elapsed time: $(elapsed $start_time))"
}

sanity_check_db () {
  local db_host=$1
  local db_name=$2
  local target_db="$db_host:$db_name"
  local start_time=$(now)

  echo
  echo "Running data sanity check on ${target_db}..."
  PGPASSWORD=$POSTGRES_PASSWORD psql -h ${db_host} -U ${POSTGRES_USERNAME} ${db_name} < sanity-check.sql

  echo "COMPLETED: Finished sanity check on ${target_db}. Please inspect the output for anomalies. (Elapsed time: $(elapsed $start_time))"
}

raw_dump () {
  local dump_file_name=$(add_datestamp "${PG_DUMP_BASE_FILENAME}-raw.dump")

  echo
  if [[ $(prompt ">> Create raw dump file ${dump_file_name}? [y/n]") == "y" ]]; then
    reset_dump_dir
    dump_db_to_file "${POSTGRES_URL}" "${POSTGRES_DATABASE}" "${dump_file_name}"

    echo
    if [[ $(prompt ">> Upload raw dump file ${dump_file_name} to S3? [y/n]") == "y" ]]; then
      upload_dump_to_S3 "${dump_file_name}"
      reset_dump_dir
    fi
  fi
}

sanitize () {
  assert_not_on_prod "sanitize"

  local raw_dump_file_name=$(latest_from_s3 "-raw.dump")

  if [[ $raw_dump_file_name == "" ]]; then
    echo "Could not locate latest dump file. exiting script."
    exit 1
  fi

  local raw_dump_file_url="${S3_BUCKET}/${raw_dump_file_name}"
  local datestamp=$(extract_datestamp "${raw_dump_file_name}")

  echo
  if [[ $MODE == "auto" ]] || [[ $(prompt ">> Download raw dump file ${raw_dump_file_url}? [y/n]") == "y" ]]; then
    download_dump_from_S3 "${raw_dump_file_url}"
  fi

  echo
  if [[ $MODE == "auto" ]] || [[ $(prompt ">> Restore and sanitize ${raw_dump_file_name} to ${TEMP_DB_NAME}? [y/n]") == "y" ]]; then
    restore_to_db "${POSTGRES_URL}" "${TEMP_DB_NAME}" "${raw_dump_file_name}"
    sanitize_db "${POSTGRES_URL}" "${TEMP_DB_NAME}"
    sanity_check_db "${POSTGRES_URL}" "${TEMP_DB_NAME}"
  fi

  local sanitized_dump_file_name=$(add_datestamp "${PG_DUMP_BASE_FILENAME}-sanitized.dump" "${datestamp}")
  echo
  if [[ $MODE == "auto" ]] || [[ $(prompt ">> Export sanitized DB to dump file ${sanitized_dump_file_name}? [y/n]") == "y" ]]; then
    dump_db_to_file "${POSTGRES_URL}" "${TEMP_DB_NAME}" "${sanitized_dump_file_name}"

    echo
    if [[ $MODE == "auto" ]] || [[ $(prompt ">> Upload sanitized dump file ${sanitized_dump_file_name} to S3? [y/n]") == "y" ]]; then
      upload_dump_to_S3 "${sanitized_dump_file_name}"
    fi
  fi

  local sanitized_sql_file_name="${sanitized_dump_file_name//.dump/.sql}"
  local sanitized_sql_file_name_compressed="${sanitized_sql_file_name}.tar.gz"
  echo
  if [[ $MODE == "auto" ]] || [[ $(prompt ">> Export sanitized DB to SQL dump file ${sanitized_sql_file_name}? [y/n]") == "y" ]]; then
    dump_db_to_file "${POSTGRES_URL}" "${TEMP_DB_NAME}" "${sanitized_sql_file_name}"
    compress_sql_dump "${sanitized_sql_file_name}"

    echo
    if [[ $MODE == "auto" ]] || [[ $(prompt ">> Upload sanitized SQL dump file ${sanitized_sql_file_name_compressed} to S3? [y/n]") == "y" ]]; then
      upload_dump_to_S3 "${sanitized_sql_file_name_compressed}"
    fi
  fi
}

restore () {
  assert_not_on_prod "restore"

  local datestamp=$1

  local format=$([[ "${FORMAT}" == "sql" ]] && echo "-sanitized.sql.tar.gz" || echo "-sanitized.dump")
  local dump_file_name=$([[ "$datestamp" == "" ]] && echo $(latest_from_s3 "${format}") || echo $(add_datestamp "${PG_DUMP_BASE_FILENAME}${format}" "${datestamp}"))
  local dump_file_url="${S3_BUCKET}/${dump_file_name}"

  if [[ $dump_file_name == "" ]]; then
    echo "Could not locate latest dump file. exiting script."
    exit 1
  fi

  echo
  if [[ $MODE == "auto" ]] || [[ $(prompt ">> Download sanitized dump file ${dump_file_url}? [y/n]") == "y" ]]; then
    download_dump_from_S3 "${dump_file_url}"
  fi

  echo
  if [[ $MODE == "auto" ]] || [[ $(prompt ">> Restore sanitized dump file ${dump_file_name} to ${POSTGRES_DATABASE}? [y/n]") == "y" ]]; then
    if [[ "${dump_file_name}" == *".tar.gz" ]]; then
      extract_sql_dump "${dump_file_name}"
      dump_file_name="${dump_file_name//.tar.gz/}"
    fi

    restore_to_db "${POSTGRES_URL}" "${POSTGRES_DATABASE}" "${dump_file_name}"
    sanity_check_db "${POSTGRES_URL}" "${POSTGRES_DATABASE}"
  fi
}

usage () {
  echo "usage: db-sanitizer.sh [-options] <dump|sanitize|restore>"
  echo
  echo "options:"
  echo "  -d  datestamp: <YYYY-MM-DD>, default is latest date-stamped dump from s3 bucket"
  echo "  -f  format: <sql|dump>, default is dump"
  echo "  -m  mode: <auto|manual>, default is manual"
  echo
}

###########################################################
# Main
###########################################################

FORMAT=dump
MODE=manual
DATESTAMP=

# process optional flags
while getopts ':d:f:m:' opt; do
  case "${opt}" in
    d) DATESTAMP=$OPTARG ;;
    f) FORMAT=$OPTARG ;;
    m) MODE=$OPTARG ;;
    \?) echo "Invalid option: $OPTARG" 1>&2; exit ;;
    :) echo "Invalid option: $OPTARG requires an argument" 1>&2; exit ;;
  esac
done
shift $((OPTIND -1))

if [[ -n $1 ]]; then
    command=$1; shift
else
    command=""
fi

assert "${command}" "$(usage)"
assert "${POSTGRES_URL}" "ERROR: POSTGRES_URL is not set!"
assert "${POSTGRES_DATABASE}" "ERROR: POSTGRES_DATABASE is not set!"
assert "${POSTGRES_USERNAME}" "ERROR: POSTGRES_USERNAME is not set!"
assert "${POSTGRES_PASSWORD}" "ERROR: POSTGRES_PASSWORD is not set!"
assert "${AWS_ACCESS_KEY_ID}" "ERROR: AWS_ACCESS_KEY_ID is not set!"
assert "${AWS_SECRET_ACCESS_KEY}" "ERROR: AWS_SECRET_ACCESS_KEY is not set!"

case "${command}" in
  sanitize|restore)
    assert_not_on_prod "${command}"
    ;;&
  dump|sanitize|restore)
    echo "+----------------------------------------------+"
    echo "| DB Sanitizer - ${command}"
    echo "+----------------------------------------------+"
    echo
    echo "POSTGRES_URL: ${POSTGRES_URL}"
    echo "POSTGRES_DATABASE: ${POSTGRES_DATABASE}"
    echo "POSTGRES_USERNAME: ${POSTGRES_USERNAME}"
    echo "S3_BUCKET: ${S3_BUCKET}"
    echo "AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID}"
    if [[ "${command}" == "sanitize" ]]; then
      echo "TEMP_DB_NAME (for sanitization): ${TEMP_DB_NAME}"
    fi
    echo "MODE: ${MODE}"
    echo "FORMAT: ${FORMAT}"
    echo "DATESTAMP: ${DATESTAMP}"
    echo

    if [[ $MODE != "auto" ]] && [[ ! $(prompt "Proceed? [y/n]") == "y" ]]; then
      exit 0
    fi

    start_time=$(now)

    case "${command}" in
      dump) raw_dump ;;
      sanitize) sanitize;;
      restore) restore "$DATESTAMP";;
    esac

    echo
    echo "DONE! (Total elapsed time: $(elapsed "$start_time"))"
    echo
    ;;
  *)
    echo
    echo "unknown command: '${command}'"
    echo
    usage
    ;;
esac