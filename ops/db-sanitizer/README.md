# Polaris DB Sanitizer

### Overview

- `db-sanitizer.sh` - guides you through the process of creating a sanitized production DB suitable
for QA and local development.

- `sanitize.sql` - contains the necessary SQL statements to scrub the data. This should be updated
whenever we add new fields that contain sensitive or "scrub-worthy" data.

- `sanity-check.sql` - contains queries that allow you to inspect and verify the scrubbed data.

### db-sanitizer.sh

#### Usage:

```bash
> ./db-sanitizer.sh
usage: db-sanitizer.sh [-options] <dump|sanitize|restore>

options:
  -d  datestamp: <YYYY-MM-DD>, default is latest date-stamped dump from s3 bucket
  -f  format: <sql|dump>, default is dump
  -m  mode: <auto|manual>, default is manual
```

#### Examples
```bash
# capture a raw dump (executed on prod)
> ./db-sanitizer dump

# sanitize the latest raw dump using "auto" mode
> ./db-sanitize -m auto sanitize

# restore the latest dump
> ./db-sanitize restore

# restore a specific sanitized dump on staging using SQL format (this is required on staging)
> ./db-sanitize -f sql -d 20230511 restore
```
### Step-By-Step: Create a sanitized DB

> (optional) Take a prod DB snapshot from AWS console

#### 1) Capture raw production DB dump
1. SSH to `prod` polaris EC2 instance
2. connect to polaris Docker container
    1. `sudo su -`
    2. `docker ps`
    3. `docker exec -it <container-id> bash`
3. cd to db-sanitizer directory and run the script with `dump` command
    1. `cd /polaris/ops/db-sanitizer`
    2. `./db-sanitizer.sh dump`
4. follow the prompts to do the following:
    1. take a dump from prod DB
    2. upload raw db dump file to S3

#### 2) Create sanitized DB dump
1. From local machine, connect to polaris Docker container
    1. `bin/ssh_to_container`
       1. *NOTE: you may need to manually supply `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` with
           proper permissions to access the S3 bucket.*
           ```bash
            > AWS_ACCESS_KEY_ID=XXXXX AWS_SECRET_ACCESS_KEY=XXXXX ./db-sanitizer.sh sanitize
           ```
2. cd to db-sanitizer directory and run the script with `sanitize` command
    1. `cd /polaris/ops/db-sanitizer`
    2. `./db-sanitizer.sh sanitize`
3. follow the prompts to do the following:
    1. download raw db dump file from S3
    2. restore `polaris_tmp` DB from `<dump-file>`
    3. sanitize `polaris_tmp` database
    4. take db dumps from sanitized DB (.dump and .sql)
        - NOTE: we need to use the `.sql` format on `staging` since we can't drop and re-create the RDS instance
        - TODO: implement DNS for the staging DB name so we can drop/recreate the instance and just update the DNS
    5. for `.sql` dump, compress to `.tar.gz`
    6. upload sanitized db dump files to S3 (to be used by devs and QA envs)

#### 3) Restore sanitized DB dump
1. connect to polaris Docker container
    1. on local
        1. `bin/ssh_to_container`
    2. on EC2 (`staging`, `qa`)
        1. *BE VERY VERY CONFIDENT YOU ARE NOT ON PRODUCTION*
        2. SSH to target host (e.g. `staging`, `qa`)
        3. `sudo su -`
        4. `docker ps`
        5. `docker exec -it <container-id> bash`
2. cd to db-sanitizer directory and run the script with `restore` command
    1. `cd /polaris/ops/db-sanitizer`
    2. on `local` or `qa`
        1. `./db-sanitizer.sh restore`
    3. on `staging`
        1. `./db-sanitizer.sh -f sql -d <YYYYMMDD> restore`, where `<YYYYMMDD>` is the datestamp of the sanitized SQL dump file
3. follow the prompts to do the following:
    1. download sanitized db dump file from S3
    2. restore `polaris` DB from `<dump-file>`

#### NOTES

- The script does its best to clean up after itself, but just in case, remove the raw DB dump file from:

    1. prod container
    2. staging container
    3. local machine

