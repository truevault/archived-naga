------------------------------------
-- FUNCTIONS
------------------------------------
CREATE OR REPLACE FUNCTION uuid_or_null(str text)
    RETURNS uuid AS $$
BEGIN
    RETURN str::uuid;
EXCEPTION WHEN invalid_text_representation THEN
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------
-- DATA SANITY CHECK
------------------------------------
-- active polaris users
select o.name organization, pu.email, ou.status
from organization_user ou
         join organization o on ou.organization_id = o.id
         join polaris_user pu on ou.user_id = pu.id
where ou.status = 'ACTIVE'
order by o.name, pu.email, ou.status;

-- polaris user email domains/status
select regexp_replace(email, '^.*@(.*)$', '\1') as polaris_user_email_domain, ou.status, count(1) from organization_user ou join polaris_user pu on ou.user_id = pu.id group by 1, 2 order by 1, 2;

-- organization mailboxes
select regexp_replace(om.mailbox, '^.*@(.*)$', '\1') as org_mailbox_domain,
       status,
       case when om.nylas_account_id is null or uuid_or_null(om.nylas_account_id) is not null then 'scrubbed' else 'not scrubbed' end as nylas_account,
       case when om.nylas_access_token = '' then 'scrubbed' else 'not scrubbed' end as nylas_token,
       count(1)
from organization_mailbox om
         join organization o on om.organization_id = o.id
where o.name not in ('Soma Store', 'TrueVault Staging')
group by 1, 2, 3, 4 order by 1;

-- organization
select name as organization, ga_web_property from organization where ga_web_property is not null;
select o.name as organization, count(oat.*) as auth_tokens from organization_auth_token oat join organization o on oat.organization_id = o.id group by 1;

-- privacy center
select regexp_replace(request_email, '^.*@(.*)$', '\1') as pc_request_email_domain,
       regexp_replace(dpo_officer_email, '^.*@(.*)$', '\1') as pc_dpo_email_domain,
       case when cloudfront_distribution is null or starts_with(cloudfront_distribution, 'ZZZ-') = true then 'scrubbed' else 'not scrubbed' end as pc_cf_dist,
       count(1)
from privacy_center
where request_email is not null
  and public_id not in ('SOMA', 'BFK4MU1CF')
group by 1, 2, 3 order by 1, 2, 3;

-- vendors
select regexp_replace(email, '^.*@(.*)$', '\1') as vendor_email_domain, count(1) from vendor where email is not null and email <> '' group by 1 order by 1;

-- data subject requests
select regexp_replace(subject_email_address, '^.*@(.*)$', '\1') as dsr_subject_email_domain, count(1) from data_subject_request where subject_email_address is not null group by 1 order by 1;

-- GDPR cookie consent tracking
select count(1) gdpr_consent_tracking_records, min(created_at) oldest, max(created_at) newest from gdpr_cookie_consent_tracking;

-- check seed data
select o.name as organization, o.public_id,
       pc.name as privacy_center, pc.public_id, pc.custom_url, pc.default_subdomain, pc.cloudfront_distribution,
       om.mailbox, om.status, om.nylas_account_id, om.nylas_access_token
from organization o
         left join privacy_center pc on o.id = pc.organization_id
         left join organization_mailbox om on o.id = om.organization_id
where o.name in ('Soma Store', 'TrueVault Staging', 'TrueVault', 'ZZZ-TrueVault')
order by o.name;

-- check seed data map
select
    o.name org
    ,cg.name collection_group
    ,count(distinct(dr.name)) data_recipients
    ,count(distinct(pic.name)) disclosed_pic
from organization o
    left join collection_group cg on o.id = cg.organization_id
    left join collection_group_collected_pic cgcp on cg.id = cgcp.collection_group_id and cgcp.deleted_at is null
    left join data_recipient_disclosed_pic drdp on cgcp.id = drdp.collected_pic_id
    left join personal_information_category pic on cgcp.pic_id = pic.id and pic.deprecated is false
    left join organization_data_recipient odr on o.id = odr.organization_id and drdp.service_id = odr.vendor_id and odr.deleted_at is null
    left join vendor dr on dr.id = odr.vendor_id
where o.name in ('TrueVault Staging', 'Soma Store', 'ZZZ-TrueVault')
group by o.name, cg.name
order by o.name, cg.name;

-- /DATA SANITY CHECK --