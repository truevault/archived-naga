DO $$
DECLARE _OldId UUID;
DECLARE _NewId UUID;
BEGIN
  _OldId := '<old-id>';
  _NewId := '<new-id>';

  -- Requests

  insert into data_subject_request (
    public_id,
    reply_key,
    organization_id,
    state,
    state_history,
    substate,
    subject_first_name,
    subject_last_name,
    subject_email_address,
    subject_phone_number,
    subject_mailing_address,
    request_date,
    request_due_date,
    request_original_due_date,
    created_at,
    closed_at,
    data_request_type_id,
    subject_ip_address,
    self_declaration_provided,
    confirmation_token,
    email_verified,
    submission_method,
    submission_source
    )
  select
    'L' || odsr.public_id,
    odsr.reply_key,
    _NewId, -- organization id
    odsr.state,
    odsr.state_history,
    odsr.substate,
    odsr.subject_first_name,
    odsr.subject_last_name,
    odsr.subject_email_address,
    odsr.subject_phone_number,
    odsr.subject_mailing_address,
    odsr.request_date,
    odsr.request_due_date,
    odsr.request_original_due_date,
    odsr.created_at,
    odsr.closed_at,
    odsr.data_request_type_id,
    odsr.subject_ip_address,
    odsr.self_declaration_provided,
    uuid_generate_v4(), -- confirmation_token
    odsr.email_verified,
    odsr.submission_method,
    odsr.submission_source
  from data_subject_request odsr
  where
    odsr.organization_id = _OldId
  ON CONFLICT (public_id) DO NOTHING;


  -- Request Consumer Groups

  insert into data_subject_request_consumer_groups (data_subject_request_id, data_subject_type_id)
  select ndsr.id, dsrcg.data_subject_type_id
  from data_subject_request_consumer_groups dsrcg
  inner join data_subject_request odsr on odsr.id = dsrcg.data_subject_request_id
  inner join data_subject_request ndsr on ndsr.public_id = 'L' || odsr.public_id
  where odsr.organization_id = _OldId;


  -- Request Events

  insert into data_subject_request_event (data_subject_request_id, user_id, event_type, message, is_data_subject, created_at)
  select
    ndsr.id,
    odsre.user_id,
    odsre.event_type,
    odsre.message,
    odsre.is_data_subject,
    odsre.created_at
  from data_subject_request_event odsre
  inner join data_subject_request odsr on odsr.id = odsre.data_subject_request_id
  inner join data_subject_request ndsr on ndsr.public_id = 'L' || odsr.public_id
  where odsr.organization_id = _OldId;

END $$;