-- POLARIS-1730
-- This is the destructive followup to 20220823-POLARIS-1730-migrate_indirect_collection_and_disclosure_pics.sql
-- the application will not work as expected until this has been run, but by separating
-- the dstructive queries we give ourselves an opportunity to spot check

 -- Soft Delete all the old associations

UPDATE collection_group_collected_pic
SET deleted_at=NOW()
WHERE pic_id IN
    (SELECT id
     FROM personal_information_category
     WHERE LOWER(name) IN ('internet or electronic activity',
                           'social media handle',
                           'other online identifier',
                           'device identifier',
                           'customer number',
                           'unique pseudonym',
                           'user alias',
                           'online identifier',
                           'other persistent identifiers',
                           'unique personal identifier',
                           'account login',
                           'ip address',
                           'interactions with websites, apps or ads',
                           'email open and click-through rates',
                           'browsing history',
                           'search history'));

 -- Delete the old disclosures

DELETE
FROM data_recipient_disclosed_pic
WHERE collected_pic_id IN
    (SELECT id
     FROM collection_group_collected_pic
     WHERE pic_id IN
         (SELECT id
          FROM personal_information_category
          WHERE LOWER(name) IN ('internet or electronic activity',
                                'social media handle',
                                'other online identifier',
                                'device identifier',
                                'customer number',
                                'unique pseudonym',
                                'user alias',
                                'online identifier',
                                'other persistent identifiers',
                                'unique personal identifier',
                                'account login',
                                'ip address',
                                'interactions with websites, apps or ads',
                                'email open and click-through rates',
                                'browsing history',
                                'search history')));

 -- Delete the old receipts

DELETE
FROM data_source_received_pic
WHERE collected_pic_id IN
    (SELECT id
     FROM collection_group_collected_pic
     WHERE pic_id IN
         (SELECT id
          FROM personal_information_category
          WHERE LOWER(name) IN ('internet or electronic activity',
                                'social media handle',
                                'other online identifier',
                                'device identifier',
                                'customer number',
                                'unique pseudonym',
                                'user alias',
                                'online identifier',
                                'other persistent identifiers',
                                'unique personal identifier',
                                'account login',
                                'ip address',
                                'interactions with websites, apps or ads',
                                'email open and click-through rates',
                                'browsing history',
                                'search history')));

