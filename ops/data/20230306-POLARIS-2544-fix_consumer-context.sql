-- POLARIS-2544
-- Adding in the consumer context where it was missing due to contractors and third party recipients, assuming the default ["CONSUMER"] since it looks like that's the only type that was missing.

UPDATE organization_data_recipient odr SET consumer_context = '["CONSUMER"]' FROM vendor WHERE data_recipient_type IN ('contractor', 'third_party_recipient') AND consumer_context = '[]';