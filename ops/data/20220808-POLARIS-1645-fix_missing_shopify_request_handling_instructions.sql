-- POLARIS-1645
-- INSERT request handling instructions for all Orgs that are joined
-- to the built-in shopify but do not have request handling instructions
-- already in place
INSERT INTO request_handling_instructions (organization_id, organization_service_id, request_type, processing_method, retention_reasons)
SELECT odr.organization_id AS organization_id,
       odr.id AS organization_service_id,
       'DELETE' AS request_type,
       'DELETE' AS processing_method,
       '[]' AS retention_reasons
FROM organization_data_recipient odr
WHERE vendor_id=
    (SELECT id
     FROM vendor
     WHERE name='Shopify'
       AND organization_id IS NULL)
  AND
    (SELECT count(*)
     FROM request_handling_instructions rhi
     WHERE rhi.organization_id = odr.organization_id
       AND rhi.organization_service_id = odr.id) = 0;

