-- POLARIS-1730
-- This script must be run after the deprecation migration has been run
-- and will locate all the now-deprecated PIC collection, disclosure,
-- and receipt for the deprecated categories, and create the correct
-- collection, disclosure, and receipt entries for the new consolidated
-- PICs
 -- Associate new categories to any org that had collected now-deprecated PICs
 -- Add Collection for Online Identifiers

INSERT INTO collection_group_collected_pic (pic_id, collection_group_id)
SELECT
  (SELECT id
   FROM personal_information_category
   WHERE name = 'Online Identifiers') AS pic_id,
       t1.collection_group_id
FROM
  (SELECT collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) IN ('internet or electronic activity',
                              'social media handle',
                              'other online identifier',
                              'device identifier',
                              'customer number',
                              'unique pseudonym',
                              'user alias',
                              'online identifier',
                              'other persistent identifiers',
                              'unique personal identifier',
                              'account login',
                              'ip address'))
     AND deleted_at IS NULL) t1
GROUP BY collection_group_id,
         pic_id;

 -- Add Collections for Internet Activity

INSERT INTO collection_group_collected_pic (pic_id, collection_group_id)
SELECT
  (SELECT id
   FROM personal_information_category
   WHERE name = 'Internet Activity') AS pic_id,
       t1.collection_group_id
FROM
  (SELECT collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) IN ('interactions with websites, apps or ads',
                              'email open and click-through rates',
                              'browsing history',
                              'search history'))
     AND deleted_at IS NULL) t1
GROUP BY collection_group_id,
         pic_id;

 -- Add Disclosures
-- Disclosures for Online Identifiers

INSERT INTO data_recipient_disclosed_pic (collected_pic_id, service_id)
SELECT c1.id AS collected_pic_id,
       d1.service_id AS service_id
FROM
  (SELECT id,
          pic_id,
          collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) IN ('internet or electronic activity',
                              'social media handle',
                              'other online identifier',
                              'device identifier',
                              'customer number',
                              'unique pseudonym',
                              'user alias',
                              'online identifier',
                              'other persistent identifiers',
                              'unique personal identifier',
                              'account login',
                              'ip address'))
     AND deleted_at IS NULL) t1
JOIN data_recipient_disclosed_pic d1 ON d1.collected_pic_id = t1.id
JOIN
  (SELECT id,
          collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) = 'online identifiers')
   GROUP BY id,
            collection_group_id) c1 ON c1.collection_group_id = t1.collection_group_id
GROUP BY c1.id,
         d1.service_id;

 -- Disclosures for Internet Activity

INSERT INTO data_recipient_disclosed_pic (collected_pic_id, service_id)
SELECT c1.id AS collected_pic_id,
       d1.service_id AS service_id
FROM
  (SELECT id,
          pic_id,
          collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) IN ('interactions with websites, apps or ads',
                              'email open and click-through rates',
                              'browsing history',
                              'search history'))
     AND deleted_at IS NULL) t1
JOIN data_recipient_disclosed_pic d1 ON d1.collected_pic_id = t1.id
JOIN
  (SELECT id,
          collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) = 'internet activity')
   GROUP BY id,
            collection_group_id) c1 ON c1.collection_group_id = t1.collection_group_id
GROUP BY c1.id,
         d1.service_id;

 -- Add Receipts
-- Receipts for Online Identifiers

INSERT INTO data_source_received_pic (collected_pic_id, vendor_id)
SELECT c1.id AS collected_pic_id,
       r1.vendor_id AS vendor_id
FROM
  (SELECT id,
          pic_id,
          collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) IN ('internet or electronic activity',
                              'social media handle',
                              'other online identifier',
                              'device identifier',
                              'customer number',
                              'unique pseudonym',
                              'user alias',
                              'online identifier',
                              'other persistent identifiers',
                              'unique personal identifier',
                              'account login',
                              'ip address'))
     AND deleted_at IS NULL) t1
JOIN data_source_received_pic r1 ON r1.collected_pic_id = t1.id
JOIN
  (SELECT id,
          collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) = 'online identifiers')
   GROUP BY id,
            collection_group_id) c1 ON c1.collection_group_id = t1.collection_group_id
GROUP BY c1.id,
         r1.vendor_id;

 -- Receipts for Internet Activity

INSERT INTO data_source_received_pic (collected_pic_id, vendor_id)
SELECT c1.id AS collected_pic_id,
       r1.vendor_id AS vendor_id
FROM
  (SELECT id,
          pic_id,
          collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) IN ('interactions with websites, apps or ads',
                              'email open and click-through rates',
                              'browsing history',
                              'search history'))) t1
JOIN data_source_received_pic r1 ON r1.collected_pic_id = t1.id
JOIN
  (SELECT id,
          collection_group_id
   FROM collection_group_collected_pic
   WHERE pic_id IN
       (SELECT id
        FROM personal_information_category
        WHERE LOWER(name) = 'internet activity')
     AND deleted_at IS NULL
   GROUP BY id,
            collection_group_id) c1 ON c1.collection_group_id = t1.collection_group_id
GROUP BY c1.id,
         r1.vendor_id;
