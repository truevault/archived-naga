-- POLARIS-2028 - backfill Mine Email submission method
UPDATE data_subject_request SET submission_method = 'EMAIL' WHERE submission_source = 'MINE_EMAIL' AND submission_method IS NULL;
