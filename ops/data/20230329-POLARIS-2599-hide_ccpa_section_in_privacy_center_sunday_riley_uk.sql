-- POLARIS-2599 - hide CCPA section for Sunday Riley UK
update privacy_center set hide_ccpa_section = true where public_id = '6ZEKS27YF';
