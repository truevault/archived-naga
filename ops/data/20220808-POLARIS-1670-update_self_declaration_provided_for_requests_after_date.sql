-- POLARIS-1670
-- UPDATE all KNOW DSRs after 2021-10-13 to have self-declaration provided to true
UPDATE data_subject_request SET self_declaration_provided = true
  WHERE created_at > '2021-10-13'
  AND self_declaration_provided = false
  AND request_type = 'CCPA_RIGHT_TO_KNOW'
  AND submission_source = 'PRIVACY_CENTER';
