-- POLARIS-1598
-- Identify organizations that have multiple instruction types
-- across organization_id, request_type, and organization_service_id
--
-- These records indicate multiple instructions for the same
-- vendor and request type for a single organization, and is
-- an unexpected state for the data model
DELETE
FROM request_handling_instructions
WHERE id IN
    ( SELECT id
     FROM
       ( SELECT id,
                organization_id,
                organization_service_id,
                request_type,
                created_at
        FROM request_handling_instructions
        GROUP BY id,
                 organization_id,
                 organization_service_id,
                 request_type,
                 created_at ) s1
     JOIN
       ( SELECT organization_id,
                organization_service_id,
                request_type,
                MAX(created_at) AS most_recent_created_at
        FROM request_handling_instructions
        GROUP BY organization_id,
                 organization_service_id,
                 request_type ) s2 ON (s1.organization_id = s2.organization_id
                                       AND s1.organization_service_id = s2.organization_service_id
                                       AND s1.request_type = s2.request_type
                                       AND s1.created_at != s2.most_recent_created_at));

