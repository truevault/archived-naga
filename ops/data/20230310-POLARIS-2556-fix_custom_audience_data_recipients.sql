-- POLARIS-2556
-- Update all data recipients attr uses_custom_audience who are included in the custom audience answer
-- but aren't currently marked as such
update 
  "organization_data_recipient" 
set 
  uses_custom_audience = true 
from 
  (
    SELECT 
      Unnest(
        String_to_array(
          Replace(
            Replace(
              Replace(answer, '"', ''), 
              '[', 
              ''
            ), 
            ']', 
            ''
          ), 
          ','
        ):: int[]
      ) AS custom_vendor_id, 
      organization_id 
    FROM 
      organization_survey_question 
    WHERE 
      slug = 'custom-audience-feature' 
      and answer != '["none"]'
  ) as data_table 
where 
  "organization_data_recipient".organization_id = data_table.organization_id 
  and "organization_data_recipient".id = data_table.custom_vendor_id 
  and "organization_data_recipient".uses_custom_audience = false;
