-- POLARIS-2170 - backfill consumer context for all organization data recipients
UPDATE organization_data_recipient SET consumer_context = '["CONSUMER"]';
