const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: "xfjji8",
  viewportWidth: 1440,
  viewportHeight: 900,
  watchForFileChanges: false,
  modifyObstructiveCode: false,

  e2e: {
    baseUrl: "http://localhost:5000",
    experimentalSessionAndOrigin: true,
    excludeSpecPattern: ["**/examples/**"],

    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require("./cypress/plugins/index")(on, config);
    },
  },

  env: {
    personas_enabled: true,
  },
});
