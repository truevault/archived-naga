/// <reference types = "cypress"/>

import page from "../../../fixtures/pages/business-survey.json";

describe("polaris: get compliant: business survey", () => {
  beforeEach(() => {
    cy.visitAs(page.url, "gdpr");
  });

  it("guides user through CCPA and GDPR Business Survey questions, one at a time", () => {
    cy.verifyHeader(page.header);
    cy.verifyNextEnabled(false, "Done with Business Survey");

    for (let i = 0; i < page.ccpa_questions.length; i++) {
      cy.answerQuestionDataCy("survey.ccpa", i, page.ccpa_questions);
    }

    for (let i = 0; i < page.gdpr_questions.length; i++) {
      cy.answerQuestionDataCy("survey.gdpr", i, page.gdpr_questions);
    }

    cy.clickNext("Done with Business Survey");
  });
});
