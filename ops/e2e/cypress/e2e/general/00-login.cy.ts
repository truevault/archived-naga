/// <reference types = "cypress"/>

import { User } from "../../support/models";

describe("polaris: log in", () => {
  beforeEach(() => {
    cy.clearCookies();
    cy.visit("/");
  });

  it("invalid email should not log the user in", () => {
    tryLogin("incorrect@email.com", "incorrect");
    verifyErrorMessage();
  });

  it("invalid password should not log the user in", () => {
    tryLogin("admin@qa.truevault.com", "incorrect");
    verifyErrorMessage();
  });

  it("valid credentials should log the user in", () => {
    cy.executeAs("ccpa", (user: User) => {
      // tryLogin("admin@qa.truevault.com", "jtn0taq6EZE4fvq!gnm");
      tryLogin(user.email, user.password);
      // which credentials to use, now that prod data is on staging?

      cy.url().should("contain", "/dashboard");
    });
  });
});

const tryLogin = (email, password) => {
  Cypress.on("fail", (error, runnable) => {
    if (!error.message.includes("401")) {
      throw error;
    }
  });

  cy.get("#email").clear().type(email);
  cy.get("#password").clear().type(password);
  cy.contains("Log In").click();
};

const verifyErrorMessage = () => {
  cy.get(".MuiAlert-message", { timeout: 30000 })
    .should("be.visible")
    .should("contain", "Incorrect Email/Password");
  cy.url().should("include", "/login");
};
