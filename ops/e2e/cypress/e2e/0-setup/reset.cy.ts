/// <reference types = "cypress"/>

import { Persona } from "../../support/models";
import { orgFromPersona } from "../../support/utils";

describe("polaris: reset and seed automation test data", () => {
  it("reset the test run ID", () => {
    cy.task("context:reset-test-run-id").then((testRunId: string) => {
      cy.log(`testRunId initialized: ${testRunId}`);
    });
  });

  it("seed the test personas", () => {
    cy.task("context:get-test-run-id").then((testRunId: string) => {
      cy.log(`TEST_RUN_ID=${testRunId}`);
      cy.fixture("personas").then((personas: Map<string, Persona>) => {
        for (const persona of Object.values(personas)) {
          const org = orgFromPersona(persona, testRunId);
          cy.request({
            method: "POST",
            url: "/debug/seed/org",
            body: {
              name: org.name,
              domain: org.domain,
              features: org.features,
              username: org.user.username,
              email: org.user.email,
              password: org.user.password,
            },
          }).then((resp) => {
            expect(resp.status).to.eq(200);
            cy.log(
              `seeded org: name=${org.name}, email=${org.user.email}, features=${persona.features}`,
            );
          });
        }
      });
    });
  });
});
