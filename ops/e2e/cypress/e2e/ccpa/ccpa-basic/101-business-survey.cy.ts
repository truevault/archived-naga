/// <reference types = "cypress"/>

import page from "../../../fixtures/pages/business-survey.json";

describe("polaris: get compliant: business survey", () => {
  beforeEach(() => {
    cy.visitAs(page.url, "ccpa");
  });

  it("guides user through CCPA Business Survey questions, one at a time", () => {
    cy.verifyHeader(page.header);
    cy.verifyNextEnabled(false, "Done with Business Survey");

    for (let i = 0; i < page.ccpa_questions.length; i++) {
      cy.answerQuestionDataCy("survey.ccpa", i, page.ccpa_questions);
    }

    cy.clickNext("Done with Business Survey");
  });
});
