/// <reference types = "cypress"/>

describe("polaris: get compliant: data collection", () => {
  beforeEach(() => {
    cy.visitAs("/get-compliant/data-map/introduction", "ccpa");
  });

  it("should show the introductory Data Map visuals", () => {
    cy.get("h5").should("have.text", "Next, let’s build your Data Map");
    cy.clickButton("Next");

    cy.get("h5").should("have.text", "Your business collects Personal Information about Consumers");
    cy.clickButton("Next");

    cy.get("h5").should(
      "have.text",
      "This information comes from Data Sources, like the Consumer themselves...",
    );
    cy.clickButton("Next");

    cy.get("h5").should(
      "have.text",
      "...and your business discloses it to Data Recipients, like your Payment Processor.",
    );
    cy.clickButton("Next");

    cy.get("h5").should(
      "have.text",
      "Your Data Map shows the flow of personal information through your business — where it comes from and where it goes...",
    );
    cy.clickButton("Next");

    cy.get("h5").should(
      "have.text",
      "...and it's TrueVault's most important tool in helping you get compliant and stay compliant.",
    );
    cy.clickButton("Next");

    cy.get("h5").should("have.text", "Ready to go?");
    cy.clickButton("Get Started");
  });
});
