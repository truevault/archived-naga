import * as fs from "fs";

import personas from "../fixtures/personas.json";
import { Persona, Organization, User } from "./models";

const FEATURES = {
  ccpa: "GetCompliantPhase",
  gdpr: "GDPR",
};

const TEST_RUN_ID_FILE = "cypress.testRunId.txt";

export const resetTestRunId = (size = 6) => {
  const now = Date.now().toString();
  const testRunId = now.substring(now.length - size);
  fs.writeFileSync(TEST_RUN_ID_FILE, testRunId, "utf-8");

  return testRunId;
};

export const getTestRunId = () => {
  if (!fs.existsSync(TEST_RUN_ID_FILE)) {
    return resetTestRunId();
  }

  return fs.readFileSync(TEST_RUN_ID_FILE, "utf8");
};

export const userFromPersonaId = (personaId: string) => {
  return userFromPersona(personas[personaId], getTestRunId());
};

export const userFromPersona = (persona: Persona, testRunId: string): User => {
  const { orgPrefix, domain } = persona;
  const username = `admin+${orgPrefix}.${testRunId}`;
  const email = `${username}@${domain}`;
  const password = "admin";
  return {
    username,
    email,
    password,
  };
};

export const orgFromPersonaId = (personaId: string) => {
  return orgFromPersona(personas[personaId], getTestRunId());
};

export const orgFromPersona = (persona: Persona, testRunId: string): Organization => {
  const { orgPrefix, domain, features } = persona;
  const user = userFromPersona(persona, testRunId);
  const orgName = `${orgPrefix}-${testRunId}`;

  return {
    name: orgName,
    domain,
    features: features.map((f) => FEATURES[f]),
    user,
  };
};
