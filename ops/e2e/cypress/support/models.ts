export type Header = {
  title: string;
  description?: string | string[];
};

export type Question = {
  question: string;
  header?: string;
  help?: string;
};

export type Persona = {
  orgPrefix: string;
  domain: string;
  features: string[];
};

export type User = {
  email: string;
  username: string;
  password: string;
};

export type Organization = {
  name: string;
  domain: string;
  features: string[];
  user: User;
};
