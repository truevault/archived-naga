/// <reference types="cypress" />
// ***********************************************
// Custom commands for common Polaris interactions
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

import { Header, Question, User } from "./models";

declare global {
  namespace Cypress {
    interface Chainable {
      answerQuestion(target: string, i: number, questions: Question[]): Chainable<Element>;
      answerQuestionDataCy(target: string, i: number, questions: Question[]): Chainable<Element>;
      clickButton(text: string, target?: string): Chainable<Element>;
      clickCheckbox(text: string, labelContainer?: string, target?: string): Chainable<Element>;
      clickNext(text?: string): Chainable<Element>;
      clickToggle(target?: string): Chainable<Element>;
      executeAs(personaId: string, action: (user: User) => void): Chainable<Element>;
      getFooter(): Chainable<Element>;
      getHeader(): Chainable<Element>;
      getModal(): Chainable<Element>;
      getNext(text?: string): Chainable<Element>;
      logInAs(personaId: string): Chainable<Element>;
      login(username: string, password: string): Chainable<Element>;
      logout(): Chainable<Element>;
      verifyHeader({ title, description }: Header): Chainable<Element>;
      verifyModalDismissed(): Chainable<Element>;
      verifyNextEnabled(isEnabled: boolean, text?: string): Chainable<Element>;
      visitAs(url: string, personaId: string): Chainable<Element>;
    }
  }
}

///////////////////////////////////
// Global/Navigation Methods
///////////////////////////////////

Cypress.Commands.add("executeAs", (personaId: string, action: (user: User) => void) => {
  const personasEnabled = Cypress.env("personas_enabled");

  if (personasEnabled) {
    cy.task("context:user-from-persona", personaId).then((user: User) => {
      action(user);
    });
  } else {
    cy.fixture("users").then((users) => {
      const user = users[personaId];
      action(user);
    });
  }
});

Cypress.Commands.add("logInAs", (personaId: string) => {
  const personasEnabled = Cypress.env("personas_enabled");

  if (personasEnabled) {
    cy.task("context:user-from-persona", personaId).then((user: User) => {
      cy.login(user.email, user.password);
    });
  } else {
    cy.fixture("users").then((users) => {
      const user = users[personaId];
      cy.login(user.email, user.password);
    });
  }
});

Cypress.Commands.add("login", (username: string, password: string) => {
  cy.session({ username, password }, () => {
    cy.request({
      method: "POST",
      url: "/api/login",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: {
        username,
        password,
      },
    });

    cy.getCookie("SESSION").should("exist");
  });
});

Cypress.Commands.add("logout", () => {
  cy.visit("/api/logout");
});

Cypress.Commands.add("visitAs", (url: string, personaId: string) => {
  cy.logInAs(personaId);
  cy.visit(url);
});

///////////////////////////////////
// Page Interaction Methods
///////////////////////////////////

Cypress.Commands.add("getHeader", () => {
  cy.get(".header-container");
});

Cypress.Commands.add("verifyHeader", ({ title, description = null }: Header) => {
  cy.getHeader().within(() => {
    cy.get(".header-container--title").contains("h1", title).should("be.visible");

    if (description) {
      const tokens = typeof description === "string" ? [description] : description;
      tokens.forEach((token) => {
        cy.get(".header-container--description").should("contain", token);
      });
    }
  });
});

Cypress.Commands.add("getFooter", () => {
  cy.get(".progress-footer");
});

Cypress.Commands.add("getNext", (text = "Next") => {
  cy.getFooter().contains(".progress-footer-actions--next-btn", text);
});

Cypress.Commands.add("clickNext", (text = "Next") => {
  cy.verifyNextEnabled(true, text).click();
});

Cypress.Commands.add("verifyNextEnabled", (isEnabled: boolean, text = "Next") => {
  if (isEnabled) {
    cy.getNext(text).should("be.enabled").should("not.have.css", "opacity", "0.15");
  } else {
    cy.getNext(text).should("not.be.enabled").should("have.css", "opacity", "0.15");
  }
});

Cypress.Commands.add("getModal", () => {
  cy.get('[role="presentation"] [role="dialog"]').should("be.visible");
});

Cypress.Commands.add("verifyModalDismissed", () => {
  cy.get('[role="presentation"] [role="dialog"]').should("not.exist");
});

Cypress.Commands.add("clickButton", (text: string, target = null) => {
  if (!!target) {
    cy.get(target).within(() => {
      cy.contains(".MuiButton-root", text).scrollIntoView().click();
    });
  } else {
    cy.contains(".MuiButton-root", text).scrollIntoView().click();
  }
});

Cypress.Commands.add(
  "clickCheckbox",
  (text: string, labelContainer = ".description-checkbox__label", target = null) => {
    if (!!target) {
      cy.get(target).within(() => {
        cy.contains(labelContainer, text).click();
      });
    } else {
      cy.contains(labelContainer, text).click();
    }
  },
);

Cypress.Commands.add("clickToggle", (target = null) => {
  if (!!target) {
    cy.get(target).within(() => {
      cy.get('button > svg[data-testid="ChevronRightIcon"]').scrollIntoView().click();
    });
  } else {
    cy.get('button > svg[data-testid="ChevronRightIcon"]').scrollIntoView().click();
  }
});

Cypress.Commands.add("answerQuestion", (target: string, i: number, questions: Question[]) => {
  const { header, question, help } = questions[i];

  cy.get(
    `${target} > .survey--question:nth-of-type(${i + 1}) > .survey-question--container`,
  ).within(($question) => {
    if (!!header) {
      cy.contains(`.survey-question--header`, header)
        .scrollIntoView()
        .should("be.visible")
        .and("have.css", "font-size", "20px");
    }

    cy.contains(`.text-component-question`, question).scrollIntoView().should("be.visible");

    if (!!help) {
      cy.get(`.text-component-help`).should("contain.text", help);
    }

    cy.get(`.survey-question--mult-choice`).within(($answers) => {
      cy.contains(`button`, "Yes")
        .scrollIntoView()
        .should("be.visible")
        .click()
        .should("have.class", "MuiButton-outlinedPrimary");

      cy.contains(`button`, "No").should("be.visible");
    });
  });
});

Cypress.Commands.add("answerQuestionDataCy", (target: string, i: number, questions: Question[]) => {
  cy.answerQuestion(`[data-cy="${target}"]`, i, questions);
});
