const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssPresetEnv = require("postcss-preset-env");
const CopyPlugin = require("copy-webpack-plugin");
const { DefinePlugin, SourceMapDevToolPlugin } = require("webpack");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const webpackVars = require("./webpack.vars");

const static = path.resolve(__dirname, "./src/main/resources/static");
const output = static + "/assets";

module.exports = {
  entry: {
    main: ["./src/js/polaris/index.ts", "./src/sass/main.scss"],
    print: "./src/sass/print.scss",
  },
  output: {
    filename: "[name].js",
    path: output,
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].css",
    }),
    new CopyPlugin({ patterns: [{ from: "src/static", to: static }] }),
    new SourceMapDevToolPlugin({
      append: `\n//# sourceMappingURL=[url]?${webpackVars.vars.SHA_VERSION}`,
      filename: "[file].map[query]",
      module: true,
      columns: true,
      noSources: false,
      namespace: "",
    }),
    new DefinePlugin(webpackVars.globals),
  ],
  optimization: {
    minimizer: [new TerserPlugin({ terserOptions: { sourceMap: true } })],
  },
  devtool: false,
  devServer: {
    static: static,
    devMiddleware: {
      publicPath: "/assets/",
    },
    hot: true,
    port: 5001,
    devMiddleware: {
      publicPath: "/assets/",
      writeToDisk: true,
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react", "@babel/preset-typescript"],
            plugins: [
              "react-hot-loader/babel",
              "@babel/proposal-class-properties",
              "@babel/proposal-object-rest-spread",
              [
                "@babel/plugin-transform-runtime",
                {
                  corejs: 3,
                },
              ],
            ],
          },
        },
      },
      {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: "css-loader",
          },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                ident: "postcss",
                plugins: [postcssPresetEnv()],
              },
            },
          },
          {
            loader: "sass-loader",
            options: {
              sassOptions: {
                precision: 10,
              },
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        issuer: /\.[jt]sx?$/,
        use: ["@svgr/webpack"],
      },
      {
        test: /\.(gif|png|jpg|svg)(\?.*$|$)/,
        issuer: /\.(scss|css)$/,
        type: "asset/inline",
      },
      {
        test: /\.(gif|png|jpg)(\?.*$|$)/,
        issuer: /\.(jsx|tsx)$/,
        type: "asset/inline",
      },
    ],
  },
  resolve: {
    extensions: ["*", ".js", ".jsx", ".tsx", ".ts"],
    alias: {
      "react-dom": "@hot-loader/react-dom",
    },
  },
};
