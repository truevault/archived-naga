#!/usr/bin/env bash

set -euo pipefail

# Requires STAGE, AWS_REGISTRY_URL, BITBUCKET_BUILD_NUMBER to be set

ECS_SERVICE=polaris-$STAGE
ECS_CLUSTER=polaris-$STAGE

if [ $STAGE = "prod" ]
then
  export WEB_MEMORY=3072
else
  export WEB_MEMORY=1024
fi

echo "Setting memory for web $WEB_MEMORY"

# Populate Task Definition
export IMAGE_NAME="${AWS_REGISTRY_URL}:$BITBUCKET_BUILD_NUMBER"
envsubst < deploy/task-definition.json > task-definition-envsubst.json

echo "Registering Task Defintion..."
UPDATED_TASK_DEFINITION=$(aws ecs register-task-definition --cli-input-json file://task-definition-envsubst.json | jq '.taskDefinition.taskDefinitionArn' --raw-output)

# Deploy Latest task definition
echo "Deploying ${UPDATED_TASK_DEFINITION} to ${ECS_SERVICE} service on the ${ECS_CLUSTER} cluster..."
aws ecs update-service --service $ECS_SERVICE --cluster $ECS_CLUSTER --task-definition ${UPDATED_TASK_DEFINITION}

echo "Waiting for deployment to complete..."
aws ecs wait services-stable --service $ECS_SERVICE --cluster $ECS_CLUSTER

echo "Checking Deployment Status..."
DEPLOYED_TASK_DEFINTION=$(aws ecs describe-services --service $ECS_SERVICE --cluster $ECS_CLUSTER --query "services[0].taskDefinition" --output text)

echo "Deployed ARN: ${DEPLOYED_TASK_DEFINTION}"
if [ $UPDATED_TASK_DEFINITION != $DEPLOYED_TASK_DEFINTION ]
then
  echo "Deployment Failed. Please check ECS container logs for more details. Deployment results:"
  aws ecs describe-services --service $ECS_SERVICE --cluster $ECS_CLUSTER --query "services[0].deployments[*]"
  exit 1
fi

echo "Deployment complete"
