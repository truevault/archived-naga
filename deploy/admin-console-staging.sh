#!/usr/bin/env bash

set -euo pipefail

ENV_FILE=env.staging.json
S3_BUCKET=admin-polaris.truevaultstaging.com
CLOUDFRONT_DISTRIBUTION=E2G49AYCPEXVC2

echo "npm install."
npm install

echo "copy vendors"
npm run admin:vendors

echo "build css"
npm run build

echo "Copy environment file."
cp src/admin/lib/$ENV_FILE src/admin/lib/env.json

echo "Next build."
npm run admin:build

echo "Next export."
npm run admin:export

echo "S3 copy."
aws s3 cp --recursive --acl public-read src/admin/out s3://$S3_BUCKET

echo "CloudFront invalidation."
aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_DISTRIBUTION --paths "/*"

echo "Reset env file."
cp src/admin/lib/env.local.json src/admin/lib/env.json