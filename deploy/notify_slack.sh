#!/bin/sh

SLACK_URL=https://hooks.slack.com/services/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
SLACK_MESSAGE="$1"

if [ -n "${SLACK_MESSAGE}" ]; then
  curl -X POST -H 'Content-type: application/json' --data '{"text":"'"${SLACK_MESSAGE}"'"}' ${SLACK_URL}
fi