FROM amazoncorretto:11 as base

ENV LANG C.utf8

ARG KOTLIN_VERSION=1.3.72
ARG NODE_VERSION=16.14.2

LABEL description="Kotlin ${KOTLIN_VERSION} / Node ${NODE_VERSION}"

# install node
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
ENV NVM_DIR=/root/.nvm
RUN yum install -y tar gzip \
  && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.35.3/install.sh | bash
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION} \
  && nvm use v${NODE_VERSION} \
  && nvm alias default v${NODE_VERSION}

# install wkhtmltopdf
RUN yum install -y libpng \
        libjpeg \
        openssl \
        icu \
        libX11 \
        libXext \
        libXrender \
        xorg-x11-fonts-Type1 \
        xorg-x11-fonts-75dpi

RUN ARCH=$(uname -m) \
  && yum install -y https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox-0.12.6-1.amazonlinux2.$ARCH.rpm

# install kotlin
ENV KOTLIN_ROOT /opt/kotlinc
ENV PATH $PATH:$KOTLIN_ROOT/bin
ENV PATH $PATH:/opt/gradle/gradle-6.0.1/bin
RUN yum -y install wget unzip \
  && wget https://github.com/JetBrains/kotlin/releases/download/v${KOTLIN_VERSION}/kotlin-compiler-${KOTLIN_VERSION}.zip -O /tmp/kotlin.zip \
  && mkdir -p /opt \
  && unzip /tmp/kotlin.zip -d /opt \
  && rm -v /tmp/kotlin.zip

# install ktlint
RUN curl -sSLO https://github.com/pinterest/ktlint/releases/download/0.45.1/ktlint \
  && chmod a+x ktlint \
  && mv ktlint /usr/local/bin/
ENV PATH $PATH:/usr/local/bin/

# install psql client
RUN echo "y" | amazon-linux-extras install postgresql11

# install other useful utilities
RUN yum install -y procps util-linux aws-cli

# install runner
ADD https://github.com/apsislabs/runner/releases/download/v0.1.1/runner-0.1.1-x86_64-unknown-linux-musl /usr/local/bin/runner
RUN chmod +x /usr/local/bin/runner


FROM base as app

# setup app workdir
ENV APP_HOME /polaris
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

# install node/npm dependencies
COPY package.json $APP_HOME/
COPY package-lock.json $APP_HOME/
RUN npm install

# install juice as a global CLI
RUN npm install -g juice

# Resolve and cache all gradle dependencies
COPY ./gradle $APP_HOME/gradle
COPY ./gradlew $APP_HOME/gradlew
COPY ./*.gradle $APP_HOME/
RUN ./gradlew resolveDependencies

# copy rarely-changed source
COPY ./bin $APP_HOME/bin
COPY ./ops $APP_HOME/ops
COPY ./webpack.*.js $APP_HOME/
COPY ./.editorconfig $APP_HOME/.editorconfig

# copy often-changed source, some sub-directories excluded via .dockerignore
COPY ./src $APP_HOME/src

# setup sha versioning
ARG GIT_SHA_VERSION
ENV SHA_VERSION=${GIT_SHA_VERSION:-unset}
LABEL description="Polaris - git-sha:${GIT_SHA_VERSION}"

# setup secrets
ARG SENTRY_DSN_ARG
ENV SENTRY_DSN=${SENTRY_DSN_ARG}

# Build app
RUN ./gradlew bootJar

CMD ["java", "-jar", "build/libs/polaris-0.1.0.jar"]
