set -euo pipefail

SCRIPT_DIR="$(dirname $0)"

bash $SCRIPT_DIR/insert_gc_features.sh
$SCRIPT_DIR/psql.sh -c "update organization_feature of set enabled = not enabled from feature f WHERE (f.id = of.feature_id and f.name = 'GetCompliantPhase' and of.organization_id = ( SELECT id FROM organization WHERE name = 'Flannel Labs'));"