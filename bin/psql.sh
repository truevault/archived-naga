#!/usr/bin/env bash

set -euo pipefail

docker-compose exec -e PGPASSWORD=secure db psql --pset expanded=auto -h db -U polaris polaris "$@"
