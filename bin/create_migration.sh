#!/usr/bin/env bash

set -euo pipefail

MIGRATION_PATH="./src/main/resources/db/migration/common/"
MAJOR_VERSION="1"
MINOR_VERSION="0"
PATCH_VERSION="2"

timestamp=$(date '+%Y%m%d%H%M%S')
base=$1
lower="$(echo $1 | awk '{print tolower($0)}')"
underscored="${lower// /_}"
underscored="${underscored//-/_}"

migration_filename="V${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION}.${timestamp}__${underscored}.sql"
full_path="${MIGRATION_PATH}${migration_filename}"

touch $full_path
cat <<EOT >> $full_path
-- TODO: Add JIRA ticket # and description
-- TODO: Does this migration introduce any sensitive or scrub-worthy fields? If so, be sure to update ops/db-sanitizer/sanitize.sql
EOT

echo "Created migration at $full_path..."
