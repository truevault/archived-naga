#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(dirname $0)"

$SCRIPT_DIR/psql.sh -c "drop schema public cascade; create schema public";
./gradlew restart

while true
  do
    sleep 2
    if curl localhost:5000/debug/seed; then
      break
    fi
  done