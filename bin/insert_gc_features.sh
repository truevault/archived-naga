#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(dirname $0)"

SQL=$(cat << EOM
insert into organization_feature (organization_id, feature_id, enabled) (
  with feature (id) as (SELECT id from feature where name = 'GetCompliantPhase'),
  org (org_id) as (select o.id, o.name, of.organization_id, f.id from feature f inner join organization_feature of on of.feature_id = f.id right join organization o on o.id = of.organization_id where f.id is NULL)
  select org.org_id, feature.id, true as enabled from org, feature
);
EOM
)

$SCRIPT_DIR/psql.sh -c "$SQL"