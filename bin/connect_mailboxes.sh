#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(dirname $0)"

SQL=$(cat << EOM
  INSERT INTO organization_mailbox (id, organization_id, status, type, mailbox, nylas_account_id)
  SELECT uuid_generate_v4(), o.id, 'CONNECTED', 'GMAIL', 'Default Mailbox', uuid_generate_v4()
  FROM organization o
  LEFT JOIN organization_mailbox om ON om.organization_id = o.id
  WHERE om.id IS NULL;
EOM
)

$SCRIPT_DIR/psql.sh -c "$SQL"
