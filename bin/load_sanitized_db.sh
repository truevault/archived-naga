#!/usr/bin/env bash

set -eo pipefail

if [[ -n "${AWS_ACCESS_KEY_ID}" ]] && [[ -n "${AWS_SECRET_ACCESS_KEY}" ]]; then
  # when running locally, supply your personal AWS access key and secret
  docker-compose exec -T -w /polaris/ops/db-sanitizer -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" web bash -c "./db-sanitizer.sh -m auto restore"
else
  docker-compose exec -T -w /polaris/ops/db-sanitizer web bash -c "./db-sanitizer.sh -m auto restore"
fi
