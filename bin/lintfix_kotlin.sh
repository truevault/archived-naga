#!/usr/bin/env bash

set -euo pipefail
docker-compose exec -T web ktlint -F "$@"

