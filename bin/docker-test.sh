#!/usr/bin/env bash

set -euo pipefail

echo "Starting test-run"
aws --endpoint-url=$SQS_URL sqs create-queue --queue-name $PRIVACY_CENTER_BUILD_QUEUE --region us-west-2
aws --endpoint-url=$SQS_URL sqs create-queue --queue-name $COOKIE_SCANNER_QUEUE --region us-west-2
aws --endpoint-url=$SQS_URL sqs create-queue --queue-name $WEBSITE_AUDIT_QUEUE --region us-west-2
echo "created queue..."
echo "starting polaris..."
echo "runner version:"
runner --version
./gradlew test
