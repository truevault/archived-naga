#!/usr/bin/env bash

set -euo pipefail


echo "Starting Polaris Web Application"

until aws --endpoint-url=$SQS_URL sqs create-queue --queue-name $COOKIE_SCANNER_QUEUE --region us-west-2; do
  echo 'Localstack is not ready yet.'
  sleep 1
done

until aws --endpoint-url=$SQS_URL sqs create-queue --queue-name $WEBSITE_AUDIT_QUEUE --region us-west-2; do
  echo 'Localstack is not ready yet.'
  sleep 1
done

until aws --endpoint-url=$SQS_URL sqs create-queue --queue-name $PRIVACY_CENTER_BUILD_QUEUE --region us-west-2; do
  echo 'Localstack is not ready yet.'
  sleep 1
done

echo "created queue..."
echo "starting polaris..."
echo "runner version:"
runner --version
runner serve -f polaris -- ./gradlew bootRun
