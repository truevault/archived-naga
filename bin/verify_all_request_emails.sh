#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(dirname $0)"
$SCRIPT_DIR/psql.sh -c "update data_subject_request set email_verified_automatically = true, state = 'VERIFY_CONSUMER' where state = 'PENDING_EMAIL_VERIFICATION';"
