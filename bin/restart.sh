#!/usr/bin/env bash

set -euo pipefail
docker-compose exec -T web runner restart polaris
