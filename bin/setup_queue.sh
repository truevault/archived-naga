#!/usr/bin/env bash

set -euo pipefail
docker-compose exec -T web bash -c "aws --region us-west-2 --endpoint-url=http://localstack:4566 sqs create-queue --queue-name local-cookie-scanner"
docker-compose exec -T web bash -c "aws --region us-west-2 --endpoint-url=http://localstack:4566 sqs create-queue --queue-name local-website-audit"
docker-compose exec -T web bash -c "aws --region us-west-2 --endpoint-url=http://localstack:4566 sqs create-queue --queue-name local-privacy-center-builder"
