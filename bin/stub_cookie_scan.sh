#!/usr/bin/env bash

set -euo pipefail

if [[ $# -eq 0 ]] ; then
      echo -e "\nParams not provided!"
      echo -e "\n\tExample usage: bin/stub_cookie_scan.sh {SCANNED_URL} {ORG_PUBLIC_ID}\n"
      exit 1
fi

URL=$1
ORG_ID=$2

JSON="{
      \"organizationId\": \"$ORG_ID\",
      \"url\": \"$URL\",
      \"cookies\": [
            {
                  \"name\": \"__cf_bm\",
                  \"value\": \"9AzdjBaheFlPWt8WlSdfUnfboUBP_lo2P.xfOg4CC4k-1658247362-0-Ad/kcp/Ao/r4HK8pckHfzeVna8XrzpICTYDUAVImZD+PWciNztzT2eCaYbrHKMKqkwyI/lY2At72oAdnbY0zVCU=\",
                  \"domain\": \".hubspot.com\",
                  \"path\": \"/\",
                  \"expires\": \"2022-07-19T16:46:02.058Z\",
                  \"httpOnly\": true,
                  \"secure\": true,
                  \"session\": false
            },
            {
                  \"name\": \"__cfruid\",
                  \"value\": \"f9d0958fdb94e2ce789054cf0650042c782da157-1658247361\",
                  \"domain\": \".truevault.com\",
                  \"path\": \"/\",
                  \"expires\": \"2022-07-19T16:46:02.058Z\",
                  \"httpOnly\": true,
                  \"secure\": true,
                  \"session\": false
            },
            {
                  \"name\": \"_ga\",
                  \"value\": \"GA1.2.391889805.1654549382\",
                  \"domain\": \".truevault.com\",
                  \"path\": \"/\",
                  \"expires\": \"2024-07-18T16:19:06.000Z\",
                  \"httpOnly\": false,
                  \"secure\": false,
                  \"session\": false
            }
      ]
}"

curl -X POST http://localhost:5000/webhooks/cookiescan/result -H 'Content-Type: application/json' -H 'Authorization: Bearer cookiescan-inbound-key' -d "$JSON"
