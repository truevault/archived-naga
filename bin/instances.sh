#!/usr/bin/env bash

set -euo pipefail

STAGE=${1:-stage}
PARAMETER=$2

if [ -z "$AWS_PROFILE" ]; then
  echo "\$AWS_PROFILE is not set. Aborting."
  exit 1
fi

if [ -z "$PARAMETER" ]; then
  aws ec2 describe-instances --region us-west-2 --filters "Name=tag:application,Values=polaris" "Name=tag:Name,Values=polaris-$STAGE" | jq -c '.Reservations[].Instances[] | {name: .Tags[]|select(.Key=="Name")|.Value, instance: .InstanceId, ip: .NetworkInterfaces[].Association.PublicIp, state: .State.Name, }'
else
  aws ec2 describe-instances --region us-west-2 --filters "Name=tag:application,Values=polaris" "Name=tag:Name,Values=polaris-$STAGE" | jq -c '.Reservations[].Instances[] | {name: .Tags[]|select(.Key=="Name")|.Value, instance: .InstanceId, ip: .NetworkInterfaces[].Association.PublicIp, state: .State.Name, }' | head -n 1 | jq -r ".$PARAMETER"
fi
