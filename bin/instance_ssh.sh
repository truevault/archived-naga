#!/usr/bin/env bash

set -euo pipefail

STAGE=${1:-stage}
PEM=${2:-~/.ssh/tv-polaris.pem}

ssh -i $PEM ec2-user@$(bin/instances.sh $STAGE ip)