#!/usr/bin/env bash

set -euo pipefail

awslocal s3 mb s3://polaris-images
awslocal s3 mb s3://polaris-attachments
awslocal s3 mb s3://polaris-vendor-agreements
awslocal s3 mb s3://polaris-config

awslocal --endpoint-url=http://localhost:4566 s3api put-bucket-acl --bucket polaris-images --acl public-read
awslocal --endpoint-url=http://localhost:4566 s3api put-bucket-acl --bucket polaris-attachments --acl public-read
awslocal --endpoint-url=http://localhost:4566 s3api put-bucket-acl --bucket polaris-vendor-agreements --acl public-read

awslocal s3 sync /docker-entrypoint-initaws.d/images s3://polaris-images/local/logos/services

awslocal s3 sync /docker-entrypoint-initaws.d/agreements s3://polaris-vendor-agreements/local/
